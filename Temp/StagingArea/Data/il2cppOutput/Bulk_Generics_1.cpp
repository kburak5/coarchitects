﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include "t2149.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t2149_TI;
#include "t2149MD.h"

#include "t29.h"
#include "t14.h"
#include "t44.h"
#include "t7.h"
#include "t914.h"
#include "t21.h"
#include "t20.h"
#include "t40.h"
extern TypeInfo t14_TI;
extern TypeInfo t914_TI;
#include "t914MD.h"
#include "t20MD.h"
extern MethodInfo m10446_MI;
extern MethodInfo m3964_MI;
extern MethodInfo m3969_MI;
extern MethodInfo m19663_MI;
struct t20;
#include "t915.h"
struct t20;
 t29 * m19490_gshared (t20 * __this, int32_t p0, MethodInfo* method);
#define m19490(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
#define m19663(__this, p0, method) (t14 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<ModifyTransform>
extern Il2CppType t20_0_0_1;
FieldInfo t2149_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2149_TI, offsetof(t2149, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2149_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2149_TI, offsetof(t2149, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2149_FIs[] =
{
	&t2149_f0_FieldInfo,
	&t2149_f1_FieldInfo,
	NULL
};
extern MethodInfo m10443_MI;
static PropertyInfo t2149____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2149_TI, "System.Collections.IEnumerator.Current", &m10443_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2149____Current_PropertyInfo = 
{
	&t2149_TI, "Current", &m10446_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2149_PIs[] =
{
	&t2149____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2149____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
static ParameterInfo t2149_m10442_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10442_GM;
MethodInfo m10442_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2149_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2149_m10442_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10442_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10443_GM;
MethodInfo m10443_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2149_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10443_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10444_GM;
MethodInfo m10444_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2149_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10444_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10445_GM;
MethodInfo m10445_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2149_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10445_GM};
extern Il2CppType t14_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10446_GM;
MethodInfo m10446_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2149_TI, &t14_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10446_GM};
static MethodInfo* t2149_MIs[] =
{
	&m10442_MI,
	&m10443_MI,
	&m10444_MI,
	&m10445_MI,
	&m10446_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
extern MethodInfo m10445_MI;
extern MethodInfo m10444_MI;
static MethodInfo* t2149_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10443_MI,
	&m10445_MI,
	&m10444_MI,
	&m10446_MI,
};
extern TypeInfo t136_TI;
extern TypeInfo t324_TI;
extern TypeInfo t3968_TI;
static TypeInfo* t2149_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3968_TI,
};
static Il2CppInterfaceOffsetPair t2149_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3968_TI, 7},
};
extern TypeInfo t14_TI;
static Il2CppRGCTXData t2149_RGCTXData[3] = 
{
	&m10446_MI/* Method Usage */,
	&t14_TI/* Class Usage */,
	&m19663_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2149_0_0_0;
extern Il2CppType t2149_1_0_0;
extern TypeInfo t110_TI;
extern Il2CppGenericClass t2149_GC;
extern TypeInfo t20_TI;
TypeInfo t2149_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2149_MIs, t2149_PIs, t2149_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2149_TI, t2149_ITIs, t2149_VT, &EmptyCustomAttributesCache, &t2149_TI, &t2149_0_0_0, &t2149_1_0_0, t2149_IOs, &t2149_GC, NULL, NULL, NULL, t2149_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2149)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5092_TI;

#include "Assembly-CSharp_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<ModifyTransform>
extern MethodInfo m26462_MI;
static PropertyInfo t5092____Count_PropertyInfo = 
{
	&t5092_TI, "Count", &m26462_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26463_MI;
static PropertyInfo t5092____IsReadOnly_PropertyInfo = 
{
	&t5092_TI, "IsReadOnly", &m26463_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5092_PIs[] =
{
	&t5092____Count_PropertyInfo,
	&t5092____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26462_GM;
MethodInfo m26462_MI = 
{
	"get_Count", NULL, &t5092_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26462_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26463_GM;
MethodInfo m26463_MI = 
{
	"get_IsReadOnly", NULL, &t5092_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26463_GM};
extern Il2CppType t14_0_0_0;
extern Il2CppType t14_0_0_0;
static ParameterInfo t5092_m26464_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t14_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26464_GM;
MethodInfo m26464_MI = 
{
	"Add", NULL, &t5092_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5092_m26464_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26464_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26465_GM;
MethodInfo m26465_MI = 
{
	"Clear", NULL, &t5092_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26465_GM};
extern Il2CppType t14_0_0_0;
static ParameterInfo t5092_m26466_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t14_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26466_GM;
MethodInfo m26466_MI = 
{
	"Contains", NULL, &t5092_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5092_m26466_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26466_GM};
extern Il2CppType t3517_0_0_0;
extern Il2CppType t3517_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5092_m26467_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3517_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26467_GM;
MethodInfo m26467_MI = 
{
	"CopyTo", NULL, &t5092_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5092_m26467_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26467_GM};
extern Il2CppType t14_0_0_0;
static ParameterInfo t5092_m26468_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t14_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26468_GM;
MethodInfo m26468_MI = 
{
	"Remove", NULL, &t5092_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5092_m26468_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26468_GM};
static MethodInfo* t5092_MIs[] =
{
	&m26462_MI,
	&m26463_MI,
	&m26464_MI,
	&m26465_MI,
	&m26466_MI,
	&m26467_MI,
	&m26468_MI,
	NULL
};
extern TypeInfo t603_TI;
extern TypeInfo t5094_TI;
static TypeInfo* t5092_ITIs[] = 
{
	&t603_TI,
	&t5094_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5092_0_0_0;
extern Il2CppType t5092_1_0_0;
struct t5092;
extern Il2CppGenericClass t5092_GC;
TypeInfo t5092_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5092_MIs, t5092_PIs, NULL, NULL, NULL, NULL, NULL, &t5092_TI, t5092_ITIs, NULL, &EmptyCustomAttributesCache, &t5092_TI, &t5092_0_0_0, &t5092_1_0_0, NULL, &t5092_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<ModifyTransform>
extern Il2CppType t3968_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26469_GM;
MethodInfo m26469_MI = 
{
	"GetEnumerator", NULL, &t5094_TI, &t3968_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26469_GM};
static MethodInfo* t5094_MIs[] =
{
	&m26469_MI,
	NULL
};
static TypeInfo* t5094_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5094_0_0_0;
extern Il2CppType t5094_1_0_0;
struct t5094;
extern Il2CppGenericClass t5094_GC;
TypeInfo t5094_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5094_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5094_TI, t5094_ITIs, NULL, &EmptyCustomAttributesCache, &t5094_TI, &t5094_0_0_0, &t5094_1_0_0, NULL, &t5094_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5093_TI;



// Metadata Definition System.Collections.Generic.IList`1<ModifyTransform>
extern MethodInfo m26470_MI;
extern MethodInfo m26471_MI;
static PropertyInfo t5093____Item_PropertyInfo = 
{
	&t5093_TI, "Item", &m26470_MI, &m26471_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5093_PIs[] =
{
	&t5093____Item_PropertyInfo,
	NULL
};
extern Il2CppType t14_0_0_0;
static ParameterInfo t5093_m26472_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t14_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26472_GM;
MethodInfo m26472_MI = 
{
	"IndexOf", NULL, &t5093_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5093_m26472_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26472_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t14_0_0_0;
static ParameterInfo t5093_m26473_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t14_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26473_GM;
MethodInfo m26473_MI = 
{
	"Insert", NULL, &t5093_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5093_m26473_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26473_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5093_m26474_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26474_GM;
MethodInfo m26474_MI = 
{
	"RemoveAt", NULL, &t5093_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5093_m26474_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26474_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5093_m26470_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t14_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26470_GM;
MethodInfo m26470_MI = 
{
	"get_Item", NULL, &t5093_TI, &t14_0_0_0, RuntimeInvoker_t29_t44, t5093_m26470_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26470_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t14_0_0_0;
static ParameterInfo t5093_m26471_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t14_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26471_GM;
MethodInfo m26471_MI = 
{
	"set_Item", NULL, &t5093_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5093_m26471_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26471_GM};
static MethodInfo* t5093_MIs[] =
{
	&m26472_MI,
	&m26473_MI,
	&m26474_MI,
	&m26470_MI,
	&m26471_MI,
	NULL
};
static TypeInfo* t5093_ITIs[] = 
{
	&t603_TI,
	&t5092_TI,
	&t5094_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5093_0_0_0;
extern Il2CppType t5093_1_0_0;
struct t5093;
extern Il2CppGenericClass t5093_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5093_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5093_MIs, t5093_PIs, NULL, NULL, NULL, NULL, NULL, &t5093_TI, t5093_ITIs, NULL, &t1908__CustomAttributeCache, &t5093_TI, &t5093_0_0_0, &t5093_1_0_0, NULL, &t5093_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2150.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2150_TI;
#include "t2150MD.h"

#include "t41.h"
#include "t557.h"
#include "mscorlib_ArrayTypes.h"
#include "t2151.h"
extern TypeInfo t316_TI;
extern TypeInfo t29_TI;
extern TypeInfo t2151_TI;
extern TypeInfo t21_TI;
#include "t2151MD.h"
extern MethodInfo m10449_MI;
extern MethodInfo m10451_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<ModifyTransform>
extern Il2CppType t316_0_0_33;
FieldInfo t2150_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2150_TI, offsetof(t2150, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2150_FIs[] =
{
	&t2150_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t14_0_0_0;
static ParameterInfo t2150_m10447_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t14_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10447_GM;
MethodInfo m10447_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2150_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2150_m10447_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m10447_GM};
extern Il2CppType t316_0_0_0;
extern Il2CppType t316_0_0_0;
static ParameterInfo t2150_m10448_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10448_GM;
MethodInfo m10448_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2150_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2150_m10448_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10448_GM};
static MethodInfo* t2150_MIs[] =
{
	&m10447_MI,
	&m10448_MI,
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m1332_MI;
extern MethodInfo m10448_MI;
extern MethodInfo m10452_MI;
static MethodInfo* t2150_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m10448_MI,
	&m10452_MI,
};
extern Il2CppType t2152_0_0_0;
extern TypeInfo t2152_TI;
extern MethodInfo m19673_MI;
extern TypeInfo t14_TI;
extern MethodInfo m10454_MI;
extern TypeInfo t14_TI;
static Il2CppRGCTXData t2150_RGCTXData[8] = 
{
	&t2152_0_0_0/* Type Usage */,
	&t2152_TI/* Class Usage */,
	&m19673_MI/* Method Usage */,
	&t14_TI/* Class Usage */,
	&m10454_MI/* Method Usage */,
	&m10449_MI/* Method Usage */,
	&t14_TI/* Class Usage */,
	&m10451_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2150_0_0_0;
extern Il2CppType t2150_1_0_0;
struct t2150;
extern Il2CppGenericClass t2150_GC;
TypeInfo t2150_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2150_MIs, NULL, t2150_FIs, NULL, &t2151_TI, NULL, NULL, &t2150_TI, NULL, t2150_VT, &EmptyCustomAttributesCache, &t2150_TI, &t2150_0_0_0, &t2150_1_0_0, NULL, &t2150_GC, NULL, NULL, NULL, t2150_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2150), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2152.h"
#include "t42.h"
#include "t43.h"
#include "t353.h"
#include "t305.h"
extern TypeInfo t2152_TI;
extern TypeInfo t42_TI;
extern TypeInfo t305_TI;
#include "t556MD.h"
#include "t42MD.h"
#include "t353MD.h"
#include "t305MD.h"
#include "t2152MD.h"
extern MethodInfo m2790_MI;
extern MethodInfo m1554_MI;
extern MethodInfo m2957_MI;
extern MethodInfo m1597_MI;
extern MethodInfo m2789_MI;
extern MethodInfo m1935_MI;
extern MethodInfo m2791_MI;
extern MethodInfo m2953_MI;
extern MethodInfo m2951_MI;
struct t556;
#include "t556.h"
struct t556;
 void m19555_gshared (t29 * __this, t29 * p0, MethodInfo* method);
#define m19555(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)
#define m19673(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<ModifyTransform>
extern Il2CppType t2152_0_0_1;
FieldInfo t2151_f0_FieldInfo = 
{
	"Delegate", &t2152_0_0_1, &t2151_TI, offsetof(t2151, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2151_FIs[] =
{
	&t2151_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2151_m10449_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10449_GM;
MethodInfo m10449_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2151_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2151_m10449_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10449_GM};
extern Il2CppType t2152_0_0_0;
static ParameterInfo t2151_m10450_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2152_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10450_GM;
MethodInfo m10450_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2151_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2151_m10450_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10450_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2151_m10451_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10451_GM;
MethodInfo m10451_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2151_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2151_m10451_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10451_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2151_m10452_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10452_GM;
MethodInfo m10452_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2151_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2151_m10452_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10452_GM};
static MethodInfo* t2151_MIs[] =
{
	&m10449_MI,
	&m10450_MI,
	&m10451_MI,
	&m10452_MI,
	NULL
};
static MethodInfo* t2151_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m10451_MI,
	&m10452_MI,
};
extern TypeInfo t2152_TI;
extern TypeInfo t14_TI;
static Il2CppRGCTXData t2151_RGCTXData[5] = 
{
	&t2152_0_0_0/* Type Usage */,
	&t2152_TI/* Class Usage */,
	&m19673_MI/* Method Usage */,
	&t14_TI/* Class Usage */,
	&m10454_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2151_0_0_0;
extern Il2CppType t2151_1_0_0;
extern TypeInfo t556_TI;
struct t2151;
extern Il2CppGenericClass t2151_GC;
TypeInfo t2151_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2151_MIs, NULL, t2151_FIs, NULL, &t556_TI, NULL, NULL, &t2151_TI, NULL, t2151_VT, &EmptyCustomAttributesCache, &t2151_TI, &t2151_0_0_0, &t2151_1_0_0, NULL, &t2151_GC, NULL, NULL, NULL, t2151_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2151), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t35.h"
#include "t67.h"


// Metadata Definition UnityEngine.Events.UnityAction`1<ModifyTransform>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2152_m10453_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10453_GM;
MethodInfo m10453_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2152_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2152_m10453_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10453_GM};
extern Il2CppType t14_0_0_0;
static ParameterInfo t2152_m10454_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t14_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10454_GM;
MethodInfo m10454_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2152_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2152_m10454_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10454_GM};
extern Il2CppType t14_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2152_m10455_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t14_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10455_GM;
MethodInfo m10455_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2152_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2152_m10455_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m10455_GM};
extern Il2CppType t66_0_0_0;
extern Il2CppType t66_0_0_0;
static ParameterInfo t2152_m10456_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10456_GM;
MethodInfo m10456_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2152_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2152_m10456_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10456_GM};
static MethodInfo* t2152_MIs[] =
{
	&m10453_MI,
	&m10454_MI,
	&m10455_MI,
	&m10456_MI,
	NULL
};
extern MethodInfo m1699_MI;
extern MethodInfo m1700_MI;
extern MethodInfo m1701_MI;
extern MethodInfo m1702_MI;
extern MethodInfo m1703_MI;
extern MethodInfo m1704_MI;
extern MethodInfo m1705_MI;
extern MethodInfo m10455_MI;
extern MethodInfo m10456_MI;
static MethodInfo* t2152_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m10454_MI,
	&m10455_MI,
	&m10456_MI,
};
extern TypeInfo t373_TI;
extern TypeInfo t374_TI;
static Il2CppInterfaceOffsetPair t2152_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2152_1_0_0;
extern TypeInfo t195_TI;
struct t2152;
extern Il2CppGenericClass t2152_GC;
TypeInfo t2152_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2152_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2152_TI, NULL, t2152_VT, &EmptyCustomAttributesCache, &t2152_TI, &t2152_0_0_0, &t2152_1_0_0, t2152_IOs, &t2152_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2152), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3970_TI;

#include "t18.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<PointerMovement>
extern MethodInfo m26475_MI;
static PropertyInfo t3970____Current_PropertyInfo = 
{
	&t3970_TI, "Current", &m26475_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3970_PIs[] =
{
	&t3970____Current_PropertyInfo,
	NULL
};
extern Il2CppType t18_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26475_GM;
MethodInfo m26475_MI = 
{
	"get_Current", NULL, &t3970_TI, &t18_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26475_GM};
static MethodInfo* t3970_MIs[] =
{
	&m26475_MI,
	NULL
};
static TypeInfo* t3970_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3970_0_0_0;
extern Il2CppType t3970_1_0_0;
struct t3970;
extern Il2CppGenericClass t3970_GC;
TypeInfo t3970_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t3970_MIs, t3970_PIs, NULL, NULL, NULL, NULL, NULL, &t3970_TI, t3970_ITIs, NULL, &EmptyCustomAttributesCache, &t3970_TI, &t3970_0_0_0, &t3970_1_0_0, NULL, &t3970_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2153.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2153_TI;
#include "t2153MD.h"

extern TypeInfo t18_TI;
extern MethodInfo m10461_MI;
extern MethodInfo m19675_MI;
struct t20;
#define m19675(__this, p0, method) (t18 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<PointerMovement>
extern Il2CppType t20_0_0_1;
FieldInfo t2153_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2153_TI, offsetof(t2153, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2153_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2153_TI, offsetof(t2153, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2153_FIs[] =
{
	&t2153_f0_FieldInfo,
	&t2153_f1_FieldInfo,
	NULL
};
extern MethodInfo m10458_MI;
static PropertyInfo t2153____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2153_TI, "System.Collections.IEnumerator.Current", &m10458_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2153____Current_PropertyInfo = 
{
	&t2153_TI, "Current", &m10461_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2153_PIs[] =
{
	&t2153____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2153____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2153_m10457_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10457_GM;
MethodInfo m10457_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2153_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2153_m10457_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10457_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10458_GM;
MethodInfo m10458_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2153_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10458_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10459_GM;
MethodInfo m10459_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2153_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10459_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10460_GM;
MethodInfo m10460_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2153_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10460_GM};
extern Il2CppType t18_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10461_GM;
MethodInfo m10461_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2153_TI, &t18_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10461_GM};
static MethodInfo* t2153_MIs[] =
{
	&m10457_MI,
	&m10458_MI,
	&m10459_MI,
	&m10460_MI,
	&m10461_MI,
	NULL
};
extern MethodInfo m10460_MI;
extern MethodInfo m10459_MI;
static MethodInfo* t2153_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10458_MI,
	&m10460_MI,
	&m10459_MI,
	&m10461_MI,
};
static TypeInfo* t2153_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3970_TI,
};
static Il2CppInterfaceOffsetPair t2153_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3970_TI, 7},
};
extern TypeInfo t18_TI;
static Il2CppRGCTXData t2153_RGCTXData[3] = 
{
	&m10461_MI/* Method Usage */,
	&t18_TI/* Class Usage */,
	&m19675_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2153_0_0_0;
extern Il2CppType t2153_1_0_0;
extern Il2CppGenericClass t2153_GC;
TypeInfo t2153_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2153_MIs, t2153_PIs, t2153_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2153_TI, t2153_ITIs, t2153_VT, &EmptyCustomAttributesCache, &t2153_TI, &t2153_0_0_0, &t2153_1_0_0, t2153_IOs, &t2153_GC, NULL, NULL, NULL, t2153_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2153)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5095_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<PointerMovement>
extern MethodInfo m26476_MI;
static PropertyInfo t5095____Count_PropertyInfo = 
{
	&t5095_TI, "Count", &m26476_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26477_MI;
static PropertyInfo t5095____IsReadOnly_PropertyInfo = 
{
	&t5095_TI, "IsReadOnly", &m26477_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5095_PIs[] =
{
	&t5095____Count_PropertyInfo,
	&t5095____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26476_GM;
MethodInfo m26476_MI = 
{
	"get_Count", NULL, &t5095_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26476_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26477_GM;
MethodInfo m26477_MI = 
{
	"get_IsReadOnly", NULL, &t5095_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26477_GM};
extern Il2CppType t18_0_0_0;
extern Il2CppType t18_0_0_0;
static ParameterInfo t5095_m26478_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t18_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26478_GM;
MethodInfo m26478_MI = 
{
	"Add", NULL, &t5095_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5095_m26478_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26478_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26479_GM;
MethodInfo m26479_MI = 
{
	"Clear", NULL, &t5095_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26479_GM};
extern Il2CppType t18_0_0_0;
static ParameterInfo t5095_m26480_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t18_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26480_GM;
MethodInfo m26480_MI = 
{
	"Contains", NULL, &t5095_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5095_m26480_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26480_GM};
extern Il2CppType t3518_0_0_0;
extern Il2CppType t3518_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5095_m26481_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3518_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26481_GM;
MethodInfo m26481_MI = 
{
	"CopyTo", NULL, &t5095_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5095_m26481_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26481_GM};
extern Il2CppType t18_0_0_0;
static ParameterInfo t5095_m26482_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t18_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26482_GM;
MethodInfo m26482_MI = 
{
	"Remove", NULL, &t5095_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5095_m26482_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26482_GM};
static MethodInfo* t5095_MIs[] =
{
	&m26476_MI,
	&m26477_MI,
	&m26478_MI,
	&m26479_MI,
	&m26480_MI,
	&m26481_MI,
	&m26482_MI,
	NULL
};
extern TypeInfo t5097_TI;
static TypeInfo* t5095_ITIs[] = 
{
	&t603_TI,
	&t5097_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5095_0_0_0;
extern Il2CppType t5095_1_0_0;
struct t5095;
extern Il2CppGenericClass t5095_GC;
TypeInfo t5095_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5095_MIs, t5095_PIs, NULL, NULL, NULL, NULL, NULL, &t5095_TI, t5095_ITIs, NULL, &EmptyCustomAttributesCache, &t5095_TI, &t5095_0_0_0, &t5095_1_0_0, NULL, &t5095_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<PointerMovement>
extern Il2CppType t3970_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26483_GM;
MethodInfo m26483_MI = 
{
	"GetEnumerator", NULL, &t5097_TI, &t3970_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26483_GM};
static MethodInfo* t5097_MIs[] =
{
	&m26483_MI,
	NULL
};
static TypeInfo* t5097_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5097_0_0_0;
extern Il2CppType t5097_1_0_0;
struct t5097;
extern Il2CppGenericClass t5097_GC;
TypeInfo t5097_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5097_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5097_TI, t5097_ITIs, NULL, &EmptyCustomAttributesCache, &t5097_TI, &t5097_0_0_0, &t5097_1_0_0, NULL, &t5097_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5096_TI;



// Metadata Definition System.Collections.Generic.IList`1<PointerMovement>
extern MethodInfo m26484_MI;
extern MethodInfo m26485_MI;
static PropertyInfo t5096____Item_PropertyInfo = 
{
	&t5096_TI, "Item", &m26484_MI, &m26485_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5096_PIs[] =
{
	&t5096____Item_PropertyInfo,
	NULL
};
extern Il2CppType t18_0_0_0;
static ParameterInfo t5096_m26486_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t18_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26486_GM;
MethodInfo m26486_MI = 
{
	"IndexOf", NULL, &t5096_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5096_m26486_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26486_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t18_0_0_0;
static ParameterInfo t5096_m26487_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t18_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26487_GM;
MethodInfo m26487_MI = 
{
	"Insert", NULL, &t5096_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5096_m26487_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26487_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5096_m26488_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26488_GM;
MethodInfo m26488_MI = 
{
	"RemoveAt", NULL, &t5096_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5096_m26488_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26488_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5096_m26484_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t18_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26484_GM;
MethodInfo m26484_MI = 
{
	"get_Item", NULL, &t5096_TI, &t18_0_0_0, RuntimeInvoker_t29_t44, t5096_m26484_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26484_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t18_0_0_0;
static ParameterInfo t5096_m26485_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t18_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26485_GM;
MethodInfo m26485_MI = 
{
	"set_Item", NULL, &t5096_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5096_m26485_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26485_GM};
static MethodInfo* t5096_MIs[] =
{
	&m26486_MI,
	&m26487_MI,
	&m26488_MI,
	&m26484_MI,
	&m26485_MI,
	NULL
};
static TypeInfo* t5096_ITIs[] = 
{
	&t603_TI,
	&t5095_TI,
	&t5097_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5096_0_0_0;
extern Il2CppType t5096_1_0_0;
struct t5096;
extern Il2CppGenericClass t5096_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5096_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5096_MIs, t5096_PIs, NULL, NULL, NULL, NULL, NULL, &t5096_TI, t5096_ITIs, NULL, &t1908__CustomAttributeCache, &t5096_TI, &t5096_0_0_0, &t5096_1_0_0, NULL, &t5096_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2154.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2154_TI;
#include "t2154MD.h"

#include "t2155.h"
extern TypeInfo t2155_TI;
#include "t2155MD.h"
extern MethodInfo m10464_MI;
extern MethodInfo m10466_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<PointerMovement>
extern Il2CppType t316_0_0_33;
FieldInfo t2154_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2154_TI, offsetof(t2154, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2154_FIs[] =
{
	&t2154_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t18_0_0_0;
static ParameterInfo t2154_m10462_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t18_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10462_GM;
MethodInfo m10462_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2154_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2154_m10462_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m10462_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2154_m10463_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10463_GM;
MethodInfo m10463_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2154_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2154_m10463_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10463_GM};
static MethodInfo* t2154_MIs[] =
{
	&m10462_MI,
	&m10463_MI,
	NULL
};
extern MethodInfo m10463_MI;
extern MethodInfo m10467_MI;
static MethodInfo* t2154_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m10463_MI,
	&m10467_MI,
};
extern Il2CppType t2156_0_0_0;
extern TypeInfo t2156_TI;
extern MethodInfo m19685_MI;
extern TypeInfo t18_TI;
extern MethodInfo m10469_MI;
extern TypeInfo t18_TI;
static Il2CppRGCTXData t2154_RGCTXData[8] = 
{
	&t2156_0_0_0/* Type Usage */,
	&t2156_TI/* Class Usage */,
	&m19685_MI/* Method Usage */,
	&t18_TI/* Class Usage */,
	&m10469_MI/* Method Usage */,
	&m10464_MI/* Method Usage */,
	&t18_TI/* Class Usage */,
	&m10466_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2154_0_0_0;
extern Il2CppType t2154_1_0_0;
struct t2154;
extern Il2CppGenericClass t2154_GC;
TypeInfo t2154_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2154_MIs, NULL, t2154_FIs, NULL, &t2155_TI, NULL, NULL, &t2154_TI, NULL, t2154_VT, &EmptyCustomAttributesCache, &t2154_TI, &t2154_0_0_0, &t2154_1_0_0, NULL, &t2154_GC, NULL, NULL, NULL, t2154_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2154), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2156.h"
extern TypeInfo t2156_TI;
#include "t2156MD.h"
struct t556;
#define m19685(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<PointerMovement>
extern Il2CppType t2156_0_0_1;
FieldInfo t2155_f0_FieldInfo = 
{
	"Delegate", &t2156_0_0_1, &t2155_TI, offsetof(t2155, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2155_FIs[] =
{
	&t2155_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2155_m10464_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10464_GM;
MethodInfo m10464_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2155_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2155_m10464_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10464_GM};
extern Il2CppType t2156_0_0_0;
static ParameterInfo t2155_m10465_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2156_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10465_GM;
MethodInfo m10465_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2155_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2155_m10465_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10465_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2155_m10466_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10466_GM;
MethodInfo m10466_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2155_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2155_m10466_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10466_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2155_m10467_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10467_GM;
MethodInfo m10467_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2155_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2155_m10467_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10467_GM};
static MethodInfo* t2155_MIs[] =
{
	&m10464_MI,
	&m10465_MI,
	&m10466_MI,
	&m10467_MI,
	NULL
};
static MethodInfo* t2155_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m10466_MI,
	&m10467_MI,
};
extern TypeInfo t2156_TI;
extern TypeInfo t18_TI;
static Il2CppRGCTXData t2155_RGCTXData[5] = 
{
	&t2156_0_0_0/* Type Usage */,
	&t2156_TI/* Class Usage */,
	&m19685_MI/* Method Usage */,
	&t18_TI/* Class Usage */,
	&m10469_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2155_0_0_0;
extern Il2CppType t2155_1_0_0;
struct t2155;
extern Il2CppGenericClass t2155_GC;
TypeInfo t2155_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2155_MIs, NULL, t2155_FIs, NULL, &t556_TI, NULL, NULL, &t2155_TI, NULL, t2155_VT, &EmptyCustomAttributesCache, &t2155_TI, &t2155_0_0_0, &t2155_1_0_0, NULL, &t2155_GC, NULL, NULL, NULL, t2155_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2155), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<PointerMovement>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2156_m10468_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10468_GM;
MethodInfo m10468_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2156_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2156_m10468_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10468_GM};
extern Il2CppType t18_0_0_0;
static ParameterInfo t2156_m10469_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t18_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10469_GM;
MethodInfo m10469_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2156_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2156_m10469_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10469_GM};
extern Il2CppType t18_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2156_m10470_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t18_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10470_GM;
MethodInfo m10470_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2156_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2156_m10470_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m10470_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2156_m10471_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10471_GM;
MethodInfo m10471_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2156_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2156_m10471_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10471_GM};
static MethodInfo* t2156_MIs[] =
{
	&m10468_MI,
	&m10469_MI,
	&m10470_MI,
	&m10471_MI,
	NULL
};
extern MethodInfo m10470_MI;
extern MethodInfo m10471_MI;
static MethodInfo* t2156_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m10469_MI,
	&m10470_MI,
	&m10471_MI,
};
static Il2CppInterfaceOffsetPair t2156_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2156_1_0_0;
struct t2156;
extern Il2CppGenericClass t2156_GC;
TypeInfo t2156_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2156_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2156_TI, NULL, t2156_VT, &EmptyCustomAttributesCache, &t2156_TI, &t2156_0_0_0, &t2156_1_0_0, t2156_IOs, &t2156_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2156), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3972_TI;

#include "t19.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<ScreenResolution>
extern MethodInfo m26489_MI;
static PropertyInfo t3972____Current_PropertyInfo = 
{
	&t3972_TI, "Current", &m26489_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3972_PIs[] =
{
	&t3972____Current_PropertyInfo,
	NULL
};
extern Il2CppType t19_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26489_GM;
MethodInfo m26489_MI = 
{
	"get_Current", NULL, &t3972_TI, &t19_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26489_GM};
static MethodInfo* t3972_MIs[] =
{
	&m26489_MI,
	NULL
};
static TypeInfo* t3972_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3972_0_0_0;
extern Il2CppType t3972_1_0_0;
struct t3972;
extern Il2CppGenericClass t3972_GC;
TypeInfo t3972_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t3972_MIs, t3972_PIs, NULL, NULL, NULL, NULL, NULL, &t3972_TI, t3972_ITIs, NULL, &EmptyCustomAttributesCache, &t3972_TI, &t3972_0_0_0, &t3972_1_0_0, NULL, &t3972_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2157.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2157_TI;
#include "t2157MD.h"

extern TypeInfo t19_TI;
extern MethodInfo m10476_MI;
extern MethodInfo m19687_MI;
struct t20;
#define m19687(__this, p0, method) (t19 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<ScreenResolution>
extern Il2CppType t20_0_0_1;
FieldInfo t2157_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2157_TI, offsetof(t2157, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2157_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2157_TI, offsetof(t2157, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2157_FIs[] =
{
	&t2157_f0_FieldInfo,
	&t2157_f1_FieldInfo,
	NULL
};
extern MethodInfo m10473_MI;
static PropertyInfo t2157____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2157_TI, "System.Collections.IEnumerator.Current", &m10473_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2157____Current_PropertyInfo = 
{
	&t2157_TI, "Current", &m10476_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2157_PIs[] =
{
	&t2157____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2157____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2157_m10472_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10472_GM;
MethodInfo m10472_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2157_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2157_m10472_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10472_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10473_GM;
MethodInfo m10473_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2157_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10473_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10474_GM;
MethodInfo m10474_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2157_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10474_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10475_GM;
MethodInfo m10475_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2157_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10475_GM};
extern Il2CppType t19_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10476_GM;
MethodInfo m10476_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2157_TI, &t19_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10476_GM};
static MethodInfo* t2157_MIs[] =
{
	&m10472_MI,
	&m10473_MI,
	&m10474_MI,
	&m10475_MI,
	&m10476_MI,
	NULL
};
extern MethodInfo m10475_MI;
extern MethodInfo m10474_MI;
static MethodInfo* t2157_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10473_MI,
	&m10475_MI,
	&m10474_MI,
	&m10476_MI,
};
static TypeInfo* t2157_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3972_TI,
};
static Il2CppInterfaceOffsetPair t2157_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3972_TI, 7},
};
extern TypeInfo t19_TI;
static Il2CppRGCTXData t2157_RGCTXData[3] = 
{
	&m10476_MI/* Method Usage */,
	&t19_TI/* Class Usage */,
	&m19687_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2157_0_0_0;
extern Il2CppType t2157_1_0_0;
extern Il2CppGenericClass t2157_GC;
TypeInfo t2157_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2157_MIs, t2157_PIs, t2157_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2157_TI, t2157_ITIs, t2157_VT, &EmptyCustomAttributesCache, &t2157_TI, &t2157_0_0_0, &t2157_1_0_0, t2157_IOs, &t2157_GC, NULL, NULL, NULL, t2157_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2157)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5098_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<ScreenResolution>
extern MethodInfo m26490_MI;
static PropertyInfo t5098____Count_PropertyInfo = 
{
	&t5098_TI, "Count", &m26490_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26491_MI;
static PropertyInfo t5098____IsReadOnly_PropertyInfo = 
{
	&t5098_TI, "IsReadOnly", &m26491_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5098_PIs[] =
{
	&t5098____Count_PropertyInfo,
	&t5098____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26490_GM;
MethodInfo m26490_MI = 
{
	"get_Count", NULL, &t5098_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26490_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26491_GM;
MethodInfo m26491_MI = 
{
	"get_IsReadOnly", NULL, &t5098_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26491_GM};
extern Il2CppType t19_0_0_0;
extern Il2CppType t19_0_0_0;
static ParameterInfo t5098_m26492_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t19_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26492_GM;
MethodInfo m26492_MI = 
{
	"Add", NULL, &t5098_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5098_m26492_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26492_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26493_GM;
MethodInfo m26493_MI = 
{
	"Clear", NULL, &t5098_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26493_GM};
extern Il2CppType t19_0_0_0;
static ParameterInfo t5098_m26494_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t19_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26494_GM;
MethodInfo m26494_MI = 
{
	"Contains", NULL, &t5098_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5098_m26494_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26494_GM};
extern Il2CppType t3519_0_0_0;
extern Il2CppType t3519_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5098_m26495_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3519_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26495_GM;
MethodInfo m26495_MI = 
{
	"CopyTo", NULL, &t5098_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5098_m26495_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26495_GM};
extern Il2CppType t19_0_0_0;
static ParameterInfo t5098_m26496_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t19_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26496_GM;
MethodInfo m26496_MI = 
{
	"Remove", NULL, &t5098_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5098_m26496_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26496_GM};
static MethodInfo* t5098_MIs[] =
{
	&m26490_MI,
	&m26491_MI,
	&m26492_MI,
	&m26493_MI,
	&m26494_MI,
	&m26495_MI,
	&m26496_MI,
	NULL
};
extern TypeInfo t5100_TI;
static TypeInfo* t5098_ITIs[] = 
{
	&t603_TI,
	&t5100_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5098_0_0_0;
extern Il2CppType t5098_1_0_0;
struct t5098;
extern Il2CppGenericClass t5098_GC;
TypeInfo t5098_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5098_MIs, t5098_PIs, NULL, NULL, NULL, NULL, NULL, &t5098_TI, t5098_ITIs, NULL, &EmptyCustomAttributesCache, &t5098_TI, &t5098_0_0_0, &t5098_1_0_0, NULL, &t5098_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<ScreenResolution>
extern Il2CppType t3972_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26497_GM;
MethodInfo m26497_MI = 
{
	"GetEnumerator", NULL, &t5100_TI, &t3972_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26497_GM};
static MethodInfo* t5100_MIs[] =
{
	&m26497_MI,
	NULL
};
static TypeInfo* t5100_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5100_0_0_0;
extern Il2CppType t5100_1_0_0;
struct t5100;
extern Il2CppGenericClass t5100_GC;
TypeInfo t5100_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5100_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5100_TI, t5100_ITIs, NULL, &EmptyCustomAttributesCache, &t5100_TI, &t5100_0_0_0, &t5100_1_0_0, NULL, &t5100_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5099_TI;



// Metadata Definition System.Collections.Generic.IList`1<ScreenResolution>
extern MethodInfo m26498_MI;
extern MethodInfo m26499_MI;
static PropertyInfo t5099____Item_PropertyInfo = 
{
	&t5099_TI, "Item", &m26498_MI, &m26499_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5099_PIs[] =
{
	&t5099____Item_PropertyInfo,
	NULL
};
extern Il2CppType t19_0_0_0;
static ParameterInfo t5099_m26500_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t19_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26500_GM;
MethodInfo m26500_MI = 
{
	"IndexOf", NULL, &t5099_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5099_m26500_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26500_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t19_0_0_0;
static ParameterInfo t5099_m26501_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t19_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26501_GM;
MethodInfo m26501_MI = 
{
	"Insert", NULL, &t5099_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5099_m26501_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26501_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5099_m26502_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26502_GM;
MethodInfo m26502_MI = 
{
	"RemoveAt", NULL, &t5099_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5099_m26502_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26502_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5099_m26498_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t19_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26498_GM;
MethodInfo m26498_MI = 
{
	"get_Item", NULL, &t5099_TI, &t19_0_0_0, RuntimeInvoker_t29_t44, t5099_m26498_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26498_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t19_0_0_0;
static ParameterInfo t5099_m26499_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t19_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26499_GM;
MethodInfo m26499_MI = 
{
	"set_Item", NULL, &t5099_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5099_m26499_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26499_GM};
static MethodInfo* t5099_MIs[] =
{
	&m26500_MI,
	&m26501_MI,
	&m26502_MI,
	&m26498_MI,
	&m26499_MI,
	NULL
};
static TypeInfo* t5099_ITIs[] = 
{
	&t603_TI,
	&t5098_TI,
	&t5100_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5099_0_0_0;
extern Il2CppType t5099_1_0_0;
struct t5099;
extern Il2CppGenericClass t5099_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5099_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5099_MIs, t5099_PIs, NULL, NULL, NULL, NULL, NULL, &t5099_TI, t5099_ITIs, NULL, &t1908__CustomAttributeCache, &t5099_TI, &t5099_0_0_0, &t5099_1_0_0, NULL, &t5099_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2158.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2158_TI;
#include "t2158MD.h"

#include "t2159.h"
extern TypeInfo t2159_TI;
#include "t2159MD.h"
extern MethodInfo m10479_MI;
extern MethodInfo m10481_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<ScreenResolution>
extern Il2CppType t316_0_0_33;
FieldInfo t2158_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2158_TI, offsetof(t2158, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2158_FIs[] =
{
	&t2158_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t19_0_0_0;
static ParameterInfo t2158_m10477_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t19_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10477_GM;
MethodInfo m10477_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2158_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2158_m10477_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m10477_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2158_m10478_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10478_GM;
MethodInfo m10478_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2158_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2158_m10478_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10478_GM};
static MethodInfo* t2158_MIs[] =
{
	&m10477_MI,
	&m10478_MI,
	NULL
};
extern MethodInfo m10478_MI;
extern MethodInfo m10482_MI;
static MethodInfo* t2158_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m10478_MI,
	&m10482_MI,
};
extern Il2CppType t2160_0_0_0;
extern TypeInfo t2160_TI;
extern MethodInfo m19697_MI;
extern TypeInfo t19_TI;
extern MethodInfo m10484_MI;
extern TypeInfo t19_TI;
static Il2CppRGCTXData t2158_RGCTXData[8] = 
{
	&t2160_0_0_0/* Type Usage */,
	&t2160_TI/* Class Usage */,
	&m19697_MI/* Method Usage */,
	&t19_TI/* Class Usage */,
	&m10484_MI/* Method Usage */,
	&m10479_MI/* Method Usage */,
	&t19_TI/* Class Usage */,
	&m10481_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2158_0_0_0;
extern Il2CppType t2158_1_0_0;
struct t2158;
extern Il2CppGenericClass t2158_GC;
TypeInfo t2158_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2158_MIs, NULL, t2158_FIs, NULL, &t2159_TI, NULL, NULL, &t2158_TI, NULL, t2158_VT, &EmptyCustomAttributesCache, &t2158_TI, &t2158_0_0_0, &t2158_1_0_0, NULL, &t2158_GC, NULL, NULL, NULL, t2158_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2158), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2160.h"
extern TypeInfo t2160_TI;
#include "t2160MD.h"
struct t556;
#define m19697(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<ScreenResolution>
extern Il2CppType t2160_0_0_1;
FieldInfo t2159_f0_FieldInfo = 
{
	"Delegate", &t2160_0_0_1, &t2159_TI, offsetof(t2159, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2159_FIs[] =
{
	&t2159_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2159_m10479_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10479_GM;
MethodInfo m10479_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2159_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2159_m10479_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10479_GM};
extern Il2CppType t2160_0_0_0;
static ParameterInfo t2159_m10480_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2160_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10480_GM;
MethodInfo m10480_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2159_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2159_m10480_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10480_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2159_m10481_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10481_GM;
MethodInfo m10481_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2159_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2159_m10481_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10481_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2159_m10482_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10482_GM;
MethodInfo m10482_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2159_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2159_m10482_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10482_GM};
static MethodInfo* t2159_MIs[] =
{
	&m10479_MI,
	&m10480_MI,
	&m10481_MI,
	&m10482_MI,
	NULL
};
static MethodInfo* t2159_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m10481_MI,
	&m10482_MI,
};
extern TypeInfo t2160_TI;
extern TypeInfo t19_TI;
static Il2CppRGCTXData t2159_RGCTXData[5] = 
{
	&t2160_0_0_0/* Type Usage */,
	&t2160_TI/* Class Usage */,
	&m19697_MI/* Method Usage */,
	&t19_TI/* Class Usage */,
	&m10484_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2159_0_0_0;
extern Il2CppType t2159_1_0_0;
struct t2159;
extern Il2CppGenericClass t2159_GC;
TypeInfo t2159_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2159_MIs, NULL, t2159_FIs, NULL, &t556_TI, NULL, NULL, &t2159_TI, NULL, t2159_VT, &EmptyCustomAttributesCache, &t2159_TI, &t2159_0_0_0, &t2159_1_0_0, NULL, &t2159_GC, NULL, NULL, NULL, t2159_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2159), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<ScreenResolution>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2160_m10483_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10483_GM;
MethodInfo m10483_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2160_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2160_m10483_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10483_GM};
extern Il2CppType t19_0_0_0;
static ParameterInfo t2160_m10484_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t19_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10484_GM;
MethodInfo m10484_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2160_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2160_m10484_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10484_GM};
extern Il2CppType t19_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2160_m10485_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t19_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10485_GM;
MethodInfo m10485_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2160_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2160_m10485_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m10485_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2160_m10486_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10486_GM;
MethodInfo m10486_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2160_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2160_m10486_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10486_GM};
static MethodInfo* t2160_MIs[] =
{
	&m10483_MI,
	&m10484_MI,
	&m10485_MI,
	&m10486_MI,
	NULL
};
extern MethodInfo m10485_MI;
extern MethodInfo m10486_MI;
static MethodInfo* t2160_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m10484_MI,
	&m10485_MI,
	&m10486_MI,
};
static Il2CppInterfaceOffsetPair t2160_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2160_1_0_0;
struct t2160;
extern Il2CppGenericClass t2160_GC;
TypeInfo t2160_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2160_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2160_TI, NULL, t2160_VT, &EmptyCustomAttributesCache, &t2160_TI, &t2160_0_0_0, &t2160_1_0_0, t2160_IOs, &t2160_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2160), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3974_TI;

#include "t48.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.EventHandle>
extern MethodInfo m26503_MI;
static PropertyInfo t3974____Current_PropertyInfo = 
{
	&t3974_TI, "Current", &m26503_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3974_PIs[] =
{
	&t3974____Current_PropertyInfo,
	NULL
};
extern Il2CppType t48_0_0_0;
extern void* RuntimeInvoker_t48 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26503_GM;
MethodInfo m26503_MI = 
{
	"get_Current", NULL, &t3974_TI, &t48_0_0_0, RuntimeInvoker_t48, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26503_GM};
static MethodInfo* t3974_MIs[] =
{
	&m26503_MI,
	NULL
};
static TypeInfo* t3974_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3974_0_0_0;
extern Il2CppType t3974_1_0_0;
struct t3974;
extern Il2CppGenericClass t3974_GC;
TypeInfo t3974_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t3974_MIs, t3974_PIs, NULL, NULL, NULL, NULL, NULL, &t3974_TI, t3974_ITIs, NULL, &EmptyCustomAttributesCache, &t3974_TI, &t3974_0_0_0, &t3974_1_0_0, NULL, &t3974_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2161.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2161_TI;
#include "t2161MD.h"

extern TypeInfo t48_TI;
extern MethodInfo m10491_MI;
extern MethodInfo m19699_MI;
struct t20;
 int32_t m19699 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m10487_MI;
 void m10487 (t2161 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m10488_MI;
 t29 * m10488 (t2161 * __this, MethodInfo* method){
	{
		int32_t L_0 = m10491(__this, &m10491_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t48_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m10489_MI;
 void m10489 (t2161 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m10490_MI;
 bool m10490 (t2161 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m10491 (t2161 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m19699(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m19699_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventHandle>
extern Il2CppType t20_0_0_1;
FieldInfo t2161_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2161_TI, offsetof(t2161, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2161_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2161_TI, offsetof(t2161, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2161_FIs[] =
{
	&t2161_f0_FieldInfo,
	&t2161_f1_FieldInfo,
	NULL
};
static PropertyInfo t2161____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2161_TI, "System.Collections.IEnumerator.Current", &m10488_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2161____Current_PropertyInfo = 
{
	&t2161_TI, "Current", &m10491_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2161_PIs[] =
{
	&t2161____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2161____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2161_m10487_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10487_GM;
MethodInfo m10487_MI = 
{
	".ctor", (methodPointerType)&m10487, &t2161_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2161_m10487_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10487_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10488_GM;
MethodInfo m10488_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10488, &t2161_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10488_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10489_GM;
MethodInfo m10489_MI = 
{
	"Dispose", (methodPointerType)&m10489, &t2161_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10489_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10490_GM;
MethodInfo m10490_MI = 
{
	"MoveNext", (methodPointerType)&m10490, &t2161_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10490_GM};
extern Il2CppType t48_0_0_0;
extern void* RuntimeInvoker_t48 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10491_GM;
MethodInfo m10491_MI = 
{
	"get_Current", (methodPointerType)&m10491, &t2161_TI, &t48_0_0_0, RuntimeInvoker_t48, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10491_GM};
static MethodInfo* t2161_MIs[] =
{
	&m10487_MI,
	&m10488_MI,
	&m10489_MI,
	&m10490_MI,
	&m10491_MI,
	NULL
};
static MethodInfo* t2161_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10488_MI,
	&m10490_MI,
	&m10489_MI,
	&m10491_MI,
};
static TypeInfo* t2161_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3974_TI,
};
static Il2CppInterfaceOffsetPair t2161_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3974_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2161_0_0_0;
extern Il2CppType t2161_1_0_0;
extern Il2CppGenericClass t2161_GC;
TypeInfo t2161_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2161_MIs, t2161_PIs, t2161_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2161_TI, t2161_ITIs, t2161_VT, &EmptyCustomAttributesCache, &t2161_TI, &t2161_0_0_0, &t2161_1_0_0, t2161_IOs, &t2161_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2161)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5101_TI;

#include "UnityEngine.UI_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventHandle>
extern MethodInfo m26504_MI;
static PropertyInfo t5101____Count_PropertyInfo = 
{
	&t5101_TI, "Count", &m26504_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26505_MI;
static PropertyInfo t5101____IsReadOnly_PropertyInfo = 
{
	&t5101_TI, "IsReadOnly", &m26505_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5101_PIs[] =
{
	&t5101____Count_PropertyInfo,
	&t5101____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26504_GM;
MethodInfo m26504_MI = 
{
	"get_Count", NULL, &t5101_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26504_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26505_GM;
MethodInfo m26505_MI = 
{
	"get_IsReadOnly", NULL, &t5101_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26505_GM};
extern Il2CppType t48_0_0_0;
extern Il2CppType t48_0_0_0;
static ParameterInfo t5101_m26506_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t48_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26506_GM;
MethodInfo m26506_MI = 
{
	"Add", NULL, &t5101_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5101_m26506_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26506_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26507_GM;
MethodInfo m26507_MI = 
{
	"Clear", NULL, &t5101_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26507_GM};
extern Il2CppType t48_0_0_0;
static ParameterInfo t5101_m26508_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t48_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26508_GM;
MethodInfo m26508_MI = 
{
	"Contains", NULL, &t5101_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5101_m26508_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26508_GM};
extern Il2CppType t3800_0_0_0;
extern Il2CppType t3800_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5101_m26509_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3800_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26509_GM;
MethodInfo m26509_MI = 
{
	"CopyTo", NULL, &t5101_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5101_m26509_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26509_GM};
extern Il2CppType t48_0_0_0;
static ParameterInfo t5101_m26510_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t48_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26510_GM;
MethodInfo m26510_MI = 
{
	"Remove", NULL, &t5101_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5101_m26510_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26510_GM};
static MethodInfo* t5101_MIs[] =
{
	&m26504_MI,
	&m26505_MI,
	&m26506_MI,
	&m26507_MI,
	&m26508_MI,
	&m26509_MI,
	&m26510_MI,
	NULL
};
extern TypeInfo t5103_TI;
static TypeInfo* t5101_ITIs[] = 
{
	&t603_TI,
	&t5103_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5101_0_0_0;
extern Il2CppType t5101_1_0_0;
struct t5101;
extern Il2CppGenericClass t5101_GC;
TypeInfo t5101_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5101_MIs, t5101_PIs, NULL, NULL, NULL, NULL, NULL, &t5101_TI, t5101_ITIs, NULL, &EmptyCustomAttributesCache, &t5101_TI, &t5101_0_0_0, &t5101_1_0_0, NULL, &t5101_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.EventHandle>
extern Il2CppType t3974_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26511_GM;
MethodInfo m26511_MI = 
{
	"GetEnumerator", NULL, &t5103_TI, &t3974_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26511_GM};
static MethodInfo* t5103_MIs[] =
{
	&m26511_MI,
	NULL
};
static TypeInfo* t5103_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5103_0_0_0;
extern Il2CppType t5103_1_0_0;
struct t5103;
extern Il2CppGenericClass t5103_GC;
TypeInfo t5103_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5103_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5103_TI, t5103_ITIs, NULL, &EmptyCustomAttributesCache, &t5103_TI, &t5103_0_0_0, &t5103_1_0_0, NULL, &t5103_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5102_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.EventHandle>
extern MethodInfo m26512_MI;
extern MethodInfo m26513_MI;
static PropertyInfo t5102____Item_PropertyInfo = 
{
	&t5102_TI, "Item", &m26512_MI, &m26513_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5102_PIs[] =
{
	&t5102____Item_PropertyInfo,
	NULL
};
extern Il2CppType t48_0_0_0;
static ParameterInfo t5102_m26514_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t48_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26514_GM;
MethodInfo m26514_MI = 
{
	"IndexOf", NULL, &t5102_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5102_m26514_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26514_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t48_0_0_0;
static ParameterInfo t5102_m26515_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t48_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26515_GM;
MethodInfo m26515_MI = 
{
	"Insert", NULL, &t5102_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5102_m26515_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26515_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5102_m26516_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26516_GM;
MethodInfo m26516_MI = 
{
	"RemoveAt", NULL, &t5102_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5102_m26516_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26516_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5102_m26512_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t48_0_0_0;
extern void* RuntimeInvoker_t48_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26512_GM;
MethodInfo m26512_MI = 
{
	"get_Item", NULL, &t5102_TI, &t48_0_0_0, RuntimeInvoker_t48_t44, t5102_m26512_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26512_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t48_0_0_0;
static ParameterInfo t5102_m26513_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t48_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26513_GM;
MethodInfo m26513_MI = 
{
	"set_Item", NULL, &t5102_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5102_m26513_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26513_GM};
static MethodInfo* t5102_MIs[] =
{
	&m26514_MI,
	&m26515_MI,
	&m26516_MI,
	&m26512_MI,
	&m26513_MI,
	NULL
};
static TypeInfo* t5102_ITIs[] = 
{
	&t603_TI,
	&t5101_TI,
	&t5103_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5102_0_0_0;
extern Il2CppType t5102_1_0_0;
struct t5102;
extern Il2CppGenericClass t5102_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5102_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5102_MIs, t5102_PIs, NULL, NULL, NULL, NULL, NULL, &t5102_TI, t5102_ITIs, NULL, &t1908__CustomAttributeCache, &t5102_TI, &t5102_0_0_0, &t5102_1_0_0, NULL, &t5102_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5104_TI;

#include "t49.h"


// Metadata Definition System.Collections.Generic.ICollection`1<System.Enum>
extern MethodInfo m26517_MI;
static PropertyInfo t5104____Count_PropertyInfo = 
{
	&t5104_TI, "Count", &m26517_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26518_MI;
static PropertyInfo t5104____IsReadOnly_PropertyInfo = 
{
	&t5104_TI, "IsReadOnly", &m26518_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5104_PIs[] =
{
	&t5104____Count_PropertyInfo,
	&t5104____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26517_GM;
MethodInfo m26517_MI = 
{
	"get_Count", NULL, &t5104_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26517_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26518_GM;
MethodInfo m26518_MI = 
{
	"get_IsReadOnly", NULL, &t5104_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26518_GM};
extern Il2CppType t49_0_0_0;
extern Il2CppType t49_0_0_0;
static ParameterInfo t5104_m26519_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t49_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26519_GM;
MethodInfo m26519_MI = 
{
	"Add", NULL, &t5104_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5104_m26519_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26519_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26520_GM;
MethodInfo m26520_MI = 
{
	"Clear", NULL, &t5104_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26520_GM};
extern Il2CppType t49_0_0_0;
static ParameterInfo t5104_m26521_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t49_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26521_GM;
MethodInfo m26521_MI = 
{
	"Contains", NULL, &t5104_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5104_m26521_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26521_GM};
extern Il2CppType t3520_0_0_0;
extern Il2CppType t3520_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5104_m26522_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3520_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26522_GM;
MethodInfo m26522_MI = 
{
	"CopyTo", NULL, &t5104_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5104_m26522_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26522_GM};
extern Il2CppType t49_0_0_0;
static ParameterInfo t5104_m26523_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t49_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26523_GM;
MethodInfo m26523_MI = 
{
	"Remove", NULL, &t5104_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5104_m26523_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26523_GM};
static MethodInfo* t5104_MIs[] =
{
	&m26517_MI,
	&m26518_MI,
	&m26519_MI,
	&m26520_MI,
	&m26521_MI,
	&m26522_MI,
	&m26523_MI,
	NULL
};
extern TypeInfo t5106_TI;
static TypeInfo* t5104_ITIs[] = 
{
	&t603_TI,
	&t5106_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5104_0_0_0;
extern Il2CppType t5104_1_0_0;
struct t5104;
extern Il2CppGenericClass t5104_GC;
TypeInfo t5104_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5104_MIs, t5104_PIs, NULL, NULL, NULL, NULL, NULL, &t5104_TI, t5104_ITIs, NULL, &EmptyCustomAttributesCache, &t5104_TI, &t5104_0_0_0, &t5104_1_0_0, NULL, &t5104_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Enum>
extern Il2CppType t3976_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26524_GM;
MethodInfo m26524_MI = 
{
	"GetEnumerator", NULL, &t5106_TI, &t3976_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26524_GM};
static MethodInfo* t5106_MIs[] =
{
	&m26524_MI,
	NULL
};
static TypeInfo* t5106_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5106_0_0_0;
extern Il2CppType t5106_1_0_0;
struct t5106;
extern Il2CppGenericClass t5106_GC;
TypeInfo t5106_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5106_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5106_TI, t5106_ITIs, NULL, &EmptyCustomAttributesCache, &t5106_TI, &t5106_0_0_0, &t5106_1_0_0, NULL, &t5106_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3976_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Enum>
extern MethodInfo m26525_MI;
static PropertyInfo t3976____Current_PropertyInfo = 
{
	&t3976_TI, "Current", &m26525_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3976_PIs[] =
{
	&t3976____Current_PropertyInfo,
	NULL
};
extern Il2CppType t49_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26525_GM;
MethodInfo m26525_MI = 
{
	"get_Current", NULL, &t3976_TI, &t49_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26525_GM};
static MethodInfo* t3976_MIs[] =
{
	&m26525_MI,
	NULL
};
static TypeInfo* t3976_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3976_0_0_0;
extern Il2CppType t3976_1_0_0;
struct t3976;
extern Il2CppGenericClass t3976_GC;
TypeInfo t3976_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t3976_MIs, t3976_PIs, NULL, NULL, NULL, NULL, NULL, &t3976_TI, t3976_ITIs, NULL, &EmptyCustomAttributesCache, &t3976_TI, &t3976_0_0_0, &t3976_1_0_0, NULL, &t3976_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2162.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2162_TI;
#include "t2162MD.h"

extern TypeInfo t49_TI;
extern MethodInfo m10496_MI;
extern MethodInfo m19710_MI;
struct t20;
#define m19710(__this, p0, method) (t49 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Enum>
extern Il2CppType t20_0_0_1;
FieldInfo t2162_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2162_TI, offsetof(t2162, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2162_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2162_TI, offsetof(t2162, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2162_FIs[] =
{
	&t2162_f0_FieldInfo,
	&t2162_f1_FieldInfo,
	NULL
};
extern MethodInfo m10493_MI;
static PropertyInfo t2162____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2162_TI, "System.Collections.IEnumerator.Current", &m10493_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2162____Current_PropertyInfo = 
{
	&t2162_TI, "Current", &m10496_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2162_PIs[] =
{
	&t2162____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2162____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2162_m10492_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10492_GM;
MethodInfo m10492_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2162_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2162_m10492_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10492_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10493_GM;
MethodInfo m10493_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2162_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10493_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10494_GM;
MethodInfo m10494_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2162_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10494_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10495_GM;
MethodInfo m10495_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2162_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10495_GM};
extern Il2CppType t49_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10496_GM;
MethodInfo m10496_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2162_TI, &t49_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10496_GM};
static MethodInfo* t2162_MIs[] =
{
	&m10492_MI,
	&m10493_MI,
	&m10494_MI,
	&m10495_MI,
	&m10496_MI,
	NULL
};
extern MethodInfo m10495_MI;
extern MethodInfo m10494_MI;
static MethodInfo* t2162_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10493_MI,
	&m10495_MI,
	&m10494_MI,
	&m10496_MI,
};
static TypeInfo* t2162_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3976_TI,
};
static Il2CppInterfaceOffsetPair t2162_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3976_TI, 7},
};
extern TypeInfo t49_TI;
static Il2CppRGCTXData t2162_RGCTXData[3] = 
{
	&m10496_MI/* Method Usage */,
	&t49_TI/* Class Usage */,
	&m19710_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2162_0_0_0;
extern Il2CppType t2162_1_0_0;
extern Il2CppGenericClass t2162_GC;
TypeInfo t2162_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2162_MIs, t2162_PIs, t2162_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2162_TI, t2162_ITIs, t2162_VT, &EmptyCustomAttributesCache, &t2162_TI, &t2162_0_0_0, &t2162_1_0_0, t2162_IOs, &t2162_GC, NULL, NULL, NULL, t2162_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2162)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5105_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Enum>
extern MethodInfo m26526_MI;
extern MethodInfo m26527_MI;
static PropertyInfo t5105____Item_PropertyInfo = 
{
	&t5105_TI, "Item", &m26526_MI, &m26527_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5105_PIs[] =
{
	&t5105____Item_PropertyInfo,
	NULL
};
extern Il2CppType t49_0_0_0;
static ParameterInfo t5105_m26528_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t49_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26528_GM;
MethodInfo m26528_MI = 
{
	"IndexOf", NULL, &t5105_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5105_m26528_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26528_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t49_0_0_0;
static ParameterInfo t5105_m26529_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t49_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26529_GM;
MethodInfo m26529_MI = 
{
	"Insert", NULL, &t5105_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5105_m26529_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26529_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5105_m26530_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26530_GM;
MethodInfo m26530_MI = 
{
	"RemoveAt", NULL, &t5105_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5105_m26530_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26530_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5105_m26526_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t49_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26526_GM;
MethodInfo m26526_MI = 
{
	"get_Item", NULL, &t5105_TI, &t49_0_0_0, RuntimeInvoker_t29_t44, t5105_m26526_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26526_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t49_0_0_0;
static ParameterInfo t5105_m26527_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t49_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26527_GM;
MethodInfo m26527_MI = 
{
	"set_Item", NULL, &t5105_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5105_m26527_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26527_GM};
static MethodInfo* t5105_MIs[] =
{
	&m26528_MI,
	&m26529_MI,
	&m26530_MI,
	&m26526_MI,
	&m26527_MI,
	NULL
};
static TypeInfo* t5105_ITIs[] = 
{
	&t603_TI,
	&t5104_TI,
	&t5106_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5105_0_0_0;
extern Il2CppType t5105_1_0_0;
struct t5105;
extern Il2CppGenericClass t5105_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5105_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5105_MIs, t5105_PIs, NULL, NULL, NULL, NULL, NULL, &t5105_TI, t5105_ITIs, NULL, &t1908__CustomAttributeCache, &t5105_TI, &t5105_0_0_0, &t5105_1_0_0, NULL, &t5105_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5107_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IFormattable>
extern MethodInfo m26531_MI;
static PropertyInfo t5107____Count_PropertyInfo = 
{
	&t5107_TI, "Count", &m26531_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26532_MI;
static PropertyInfo t5107____IsReadOnly_PropertyInfo = 
{
	&t5107_TI, "IsReadOnly", &m26532_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5107_PIs[] =
{
	&t5107____Count_PropertyInfo,
	&t5107____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26531_GM;
MethodInfo m26531_MI = 
{
	"get_Count", NULL, &t5107_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26531_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26532_GM;
MethodInfo m26532_MI = 
{
	"get_IsReadOnly", NULL, &t5107_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26532_GM};
extern Il2CppType t288_0_0_0;
extern Il2CppType t288_0_0_0;
static ParameterInfo t5107_m26533_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t288_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26533_GM;
MethodInfo m26533_MI = 
{
	"Add", NULL, &t5107_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5107_m26533_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26533_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26534_GM;
MethodInfo m26534_MI = 
{
	"Clear", NULL, &t5107_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26534_GM};
extern Il2CppType t288_0_0_0;
static ParameterInfo t5107_m26535_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t288_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26535_GM;
MethodInfo m26535_MI = 
{
	"Contains", NULL, &t5107_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5107_m26535_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26535_GM};
extern Il2CppType t3521_0_0_0;
extern Il2CppType t3521_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5107_m26536_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3521_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26536_GM;
MethodInfo m26536_MI = 
{
	"CopyTo", NULL, &t5107_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5107_m26536_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26536_GM};
extern Il2CppType t288_0_0_0;
static ParameterInfo t5107_m26537_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t288_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26537_GM;
MethodInfo m26537_MI = 
{
	"Remove", NULL, &t5107_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5107_m26537_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26537_GM};
static MethodInfo* t5107_MIs[] =
{
	&m26531_MI,
	&m26532_MI,
	&m26533_MI,
	&m26534_MI,
	&m26535_MI,
	&m26536_MI,
	&m26537_MI,
	NULL
};
extern TypeInfo t5109_TI;
static TypeInfo* t5107_ITIs[] = 
{
	&t603_TI,
	&t5109_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5107_0_0_0;
extern Il2CppType t5107_1_0_0;
struct t5107;
extern Il2CppGenericClass t5107_GC;
TypeInfo t5107_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5107_MIs, t5107_PIs, NULL, NULL, NULL, NULL, NULL, &t5107_TI, t5107_ITIs, NULL, &EmptyCustomAttributesCache, &t5107_TI, &t5107_0_0_0, &t5107_1_0_0, NULL, &t5107_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IFormattable>
extern Il2CppType t3978_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26538_GM;
MethodInfo m26538_MI = 
{
	"GetEnumerator", NULL, &t5109_TI, &t3978_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26538_GM};
static MethodInfo* t5109_MIs[] =
{
	&m26538_MI,
	NULL
};
static TypeInfo* t5109_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5109_0_0_0;
extern Il2CppType t5109_1_0_0;
struct t5109;
extern Il2CppGenericClass t5109_GC;
TypeInfo t5109_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5109_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5109_TI, t5109_ITIs, NULL, &EmptyCustomAttributesCache, &t5109_TI, &t5109_0_0_0, &t5109_1_0_0, NULL, &t5109_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3978_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IFormattable>
extern MethodInfo m26539_MI;
static PropertyInfo t3978____Current_PropertyInfo = 
{
	&t3978_TI, "Current", &m26539_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3978_PIs[] =
{
	&t3978____Current_PropertyInfo,
	NULL
};
extern Il2CppType t288_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26539_GM;
MethodInfo m26539_MI = 
{
	"get_Current", NULL, &t3978_TI, &t288_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26539_GM};
static MethodInfo* t3978_MIs[] =
{
	&m26539_MI,
	NULL
};
static TypeInfo* t3978_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3978_0_0_0;
extern Il2CppType t3978_1_0_0;
struct t3978;
extern Il2CppGenericClass t3978_GC;
TypeInfo t3978_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t3978_MIs, t3978_PIs, NULL, NULL, NULL, NULL, NULL, &t3978_TI, t3978_ITIs, NULL, &EmptyCustomAttributesCache, &t3978_TI, &t3978_0_0_0, &t3978_1_0_0, NULL, &t3978_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2163.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2163_TI;
#include "t2163MD.h"

extern TypeInfo t288_TI;
extern MethodInfo m10501_MI;
extern MethodInfo m19721_MI;
struct t20;
#define m19721(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.IFormattable>
extern Il2CppType t20_0_0_1;
FieldInfo t2163_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2163_TI, offsetof(t2163, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2163_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2163_TI, offsetof(t2163, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2163_FIs[] =
{
	&t2163_f0_FieldInfo,
	&t2163_f1_FieldInfo,
	NULL
};
extern MethodInfo m10498_MI;
static PropertyInfo t2163____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2163_TI, "System.Collections.IEnumerator.Current", &m10498_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2163____Current_PropertyInfo = 
{
	&t2163_TI, "Current", &m10501_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2163_PIs[] =
{
	&t2163____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2163____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2163_m10497_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10497_GM;
MethodInfo m10497_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2163_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2163_m10497_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10497_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10498_GM;
MethodInfo m10498_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2163_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10498_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10499_GM;
MethodInfo m10499_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2163_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10499_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10500_GM;
MethodInfo m10500_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2163_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10500_GM};
extern Il2CppType t288_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10501_GM;
MethodInfo m10501_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2163_TI, &t288_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10501_GM};
static MethodInfo* t2163_MIs[] =
{
	&m10497_MI,
	&m10498_MI,
	&m10499_MI,
	&m10500_MI,
	&m10501_MI,
	NULL
};
extern MethodInfo m10500_MI;
extern MethodInfo m10499_MI;
static MethodInfo* t2163_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10498_MI,
	&m10500_MI,
	&m10499_MI,
	&m10501_MI,
};
static TypeInfo* t2163_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3978_TI,
};
static Il2CppInterfaceOffsetPair t2163_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3978_TI, 7},
};
extern TypeInfo t288_TI;
static Il2CppRGCTXData t2163_RGCTXData[3] = 
{
	&m10501_MI/* Method Usage */,
	&t288_TI/* Class Usage */,
	&m19721_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2163_0_0_0;
extern Il2CppType t2163_1_0_0;
extern Il2CppGenericClass t2163_GC;
TypeInfo t2163_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2163_MIs, t2163_PIs, t2163_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2163_TI, t2163_ITIs, t2163_VT, &EmptyCustomAttributesCache, &t2163_TI, &t2163_0_0_0, &t2163_1_0_0, t2163_IOs, &t2163_GC, NULL, NULL, NULL, t2163_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2163)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5108_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IFormattable>
extern MethodInfo m26540_MI;
extern MethodInfo m26541_MI;
static PropertyInfo t5108____Item_PropertyInfo = 
{
	&t5108_TI, "Item", &m26540_MI, &m26541_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5108_PIs[] =
{
	&t5108____Item_PropertyInfo,
	NULL
};
extern Il2CppType t288_0_0_0;
static ParameterInfo t5108_m26542_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t288_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26542_GM;
MethodInfo m26542_MI = 
{
	"IndexOf", NULL, &t5108_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5108_m26542_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26542_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t288_0_0_0;
static ParameterInfo t5108_m26543_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t288_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26543_GM;
MethodInfo m26543_MI = 
{
	"Insert", NULL, &t5108_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5108_m26543_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26543_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5108_m26544_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26544_GM;
MethodInfo m26544_MI = 
{
	"RemoveAt", NULL, &t5108_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5108_m26544_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26544_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5108_m26540_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t288_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26540_GM;
MethodInfo m26540_MI = 
{
	"get_Item", NULL, &t5108_TI, &t288_0_0_0, RuntimeInvoker_t29_t44, t5108_m26540_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26540_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t288_0_0_0;
static ParameterInfo t5108_m26541_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t288_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26541_GM;
MethodInfo m26541_MI = 
{
	"set_Item", NULL, &t5108_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5108_m26541_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26541_GM};
static MethodInfo* t5108_MIs[] =
{
	&m26542_MI,
	&m26543_MI,
	&m26544_MI,
	&m26540_MI,
	&m26541_MI,
	NULL
};
static TypeInfo* t5108_ITIs[] = 
{
	&t603_TI,
	&t5107_TI,
	&t5109_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5108_0_0_0;
extern Il2CppType t5108_1_0_0;
struct t5108;
extern Il2CppGenericClass t5108_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5108_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5108_MIs, t5108_PIs, NULL, NULL, NULL, NULL, NULL, &t5108_TI, t5108_ITIs, NULL, &t1908__CustomAttributeCache, &t5108_TI, &t5108_0_0_0, &t5108_1_0_0, NULL, &t5108_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5110_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IConvertible>
extern MethodInfo m26545_MI;
static PropertyInfo t5110____Count_PropertyInfo = 
{
	&t5110_TI, "Count", &m26545_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26546_MI;
static PropertyInfo t5110____IsReadOnly_PropertyInfo = 
{
	&t5110_TI, "IsReadOnly", &m26546_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5110_PIs[] =
{
	&t5110____Count_PropertyInfo,
	&t5110____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26545_GM;
MethodInfo m26545_MI = 
{
	"get_Count", NULL, &t5110_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26545_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26546_GM;
MethodInfo m26546_MI = 
{
	"get_IsReadOnly", NULL, &t5110_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26546_GM};
extern Il2CppType t289_0_0_0;
extern Il2CppType t289_0_0_0;
static ParameterInfo t5110_m26547_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t289_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26547_GM;
MethodInfo m26547_MI = 
{
	"Add", NULL, &t5110_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5110_m26547_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26547_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26548_GM;
MethodInfo m26548_MI = 
{
	"Clear", NULL, &t5110_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26548_GM};
extern Il2CppType t289_0_0_0;
static ParameterInfo t5110_m26549_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t289_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26549_GM;
MethodInfo m26549_MI = 
{
	"Contains", NULL, &t5110_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5110_m26549_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26549_GM};
extern Il2CppType t3522_0_0_0;
extern Il2CppType t3522_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5110_m26550_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3522_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26550_GM;
MethodInfo m26550_MI = 
{
	"CopyTo", NULL, &t5110_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5110_m26550_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26550_GM};
extern Il2CppType t289_0_0_0;
static ParameterInfo t5110_m26551_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t289_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26551_GM;
MethodInfo m26551_MI = 
{
	"Remove", NULL, &t5110_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5110_m26551_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26551_GM};
static MethodInfo* t5110_MIs[] =
{
	&m26545_MI,
	&m26546_MI,
	&m26547_MI,
	&m26548_MI,
	&m26549_MI,
	&m26550_MI,
	&m26551_MI,
	NULL
};
extern TypeInfo t5112_TI;
static TypeInfo* t5110_ITIs[] = 
{
	&t603_TI,
	&t5112_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5110_0_0_0;
extern Il2CppType t5110_1_0_0;
struct t5110;
extern Il2CppGenericClass t5110_GC;
TypeInfo t5110_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5110_MIs, t5110_PIs, NULL, NULL, NULL, NULL, NULL, &t5110_TI, t5110_ITIs, NULL, &EmptyCustomAttributesCache, &t5110_TI, &t5110_0_0_0, &t5110_1_0_0, NULL, &t5110_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IConvertible>
extern Il2CppType t3980_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26552_GM;
MethodInfo m26552_MI = 
{
	"GetEnumerator", NULL, &t5112_TI, &t3980_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26552_GM};
static MethodInfo* t5112_MIs[] =
{
	&m26552_MI,
	NULL
};
static TypeInfo* t5112_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5112_0_0_0;
extern Il2CppType t5112_1_0_0;
struct t5112;
extern Il2CppGenericClass t5112_GC;
TypeInfo t5112_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5112_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5112_TI, t5112_ITIs, NULL, &EmptyCustomAttributesCache, &t5112_TI, &t5112_0_0_0, &t5112_1_0_0, NULL, &t5112_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3980_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IConvertible>
extern MethodInfo m26553_MI;
static PropertyInfo t3980____Current_PropertyInfo = 
{
	&t3980_TI, "Current", &m26553_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3980_PIs[] =
{
	&t3980____Current_PropertyInfo,
	NULL
};
extern Il2CppType t289_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26553_GM;
MethodInfo m26553_MI = 
{
	"get_Current", NULL, &t3980_TI, &t289_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26553_GM};
static MethodInfo* t3980_MIs[] =
{
	&m26553_MI,
	NULL
};
static TypeInfo* t3980_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3980_0_0_0;
extern Il2CppType t3980_1_0_0;
struct t3980;
extern Il2CppGenericClass t3980_GC;
TypeInfo t3980_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t3980_MIs, t3980_PIs, NULL, NULL, NULL, NULL, NULL, &t3980_TI, t3980_ITIs, NULL, &EmptyCustomAttributesCache, &t3980_TI, &t3980_0_0_0, &t3980_1_0_0, NULL, &t3980_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2164.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2164_TI;
#include "t2164MD.h"

extern TypeInfo t289_TI;
extern MethodInfo m10506_MI;
extern MethodInfo m19732_MI;
struct t20;
#define m19732(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.IConvertible>
extern Il2CppType t20_0_0_1;
FieldInfo t2164_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2164_TI, offsetof(t2164, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2164_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2164_TI, offsetof(t2164, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2164_FIs[] =
{
	&t2164_f0_FieldInfo,
	&t2164_f1_FieldInfo,
	NULL
};
extern MethodInfo m10503_MI;
static PropertyInfo t2164____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2164_TI, "System.Collections.IEnumerator.Current", &m10503_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2164____Current_PropertyInfo = 
{
	&t2164_TI, "Current", &m10506_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2164_PIs[] =
{
	&t2164____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2164____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2164_m10502_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10502_GM;
MethodInfo m10502_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2164_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2164_m10502_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10502_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10503_GM;
MethodInfo m10503_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2164_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10503_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10504_GM;
MethodInfo m10504_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2164_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10504_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10505_GM;
MethodInfo m10505_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2164_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10505_GM};
extern Il2CppType t289_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10506_GM;
MethodInfo m10506_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2164_TI, &t289_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10506_GM};
static MethodInfo* t2164_MIs[] =
{
	&m10502_MI,
	&m10503_MI,
	&m10504_MI,
	&m10505_MI,
	&m10506_MI,
	NULL
};
extern MethodInfo m10505_MI;
extern MethodInfo m10504_MI;
static MethodInfo* t2164_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10503_MI,
	&m10505_MI,
	&m10504_MI,
	&m10506_MI,
};
static TypeInfo* t2164_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3980_TI,
};
static Il2CppInterfaceOffsetPair t2164_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3980_TI, 7},
};
extern TypeInfo t289_TI;
static Il2CppRGCTXData t2164_RGCTXData[3] = 
{
	&m10506_MI/* Method Usage */,
	&t289_TI/* Class Usage */,
	&m19732_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2164_0_0_0;
extern Il2CppType t2164_1_0_0;
extern Il2CppGenericClass t2164_GC;
TypeInfo t2164_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2164_MIs, t2164_PIs, t2164_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2164_TI, t2164_ITIs, t2164_VT, &EmptyCustomAttributesCache, &t2164_TI, &t2164_0_0_0, &t2164_1_0_0, t2164_IOs, &t2164_GC, NULL, NULL, NULL, t2164_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2164)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5111_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IConvertible>
extern MethodInfo m26554_MI;
extern MethodInfo m26555_MI;
static PropertyInfo t5111____Item_PropertyInfo = 
{
	&t5111_TI, "Item", &m26554_MI, &m26555_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5111_PIs[] =
{
	&t5111____Item_PropertyInfo,
	NULL
};
extern Il2CppType t289_0_0_0;
static ParameterInfo t5111_m26556_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t289_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26556_GM;
MethodInfo m26556_MI = 
{
	"IndexOf", NULL, &t5111_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5111_m26556_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26556_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t289_0_0_0;
static ParameterInfo t5111_m26557_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t289_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26557_GM;
MethodInfo m26557_MI = 
{
	"Insert", NULL, &t5111_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5111_m26557_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26557_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5111_m26558_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26558_GM;
MethodInfo m26558_MI = 
{
	"RemoveAt", NULL, &t5111_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5111_m26558_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26558_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5111_m26554_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t289_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26554_GM;
MethodInfo m26554_MI = 
{
	"get_Item", NULL, &t5111_TI, &t289_0_0_0, RuntimeInvoker_t29_t44, t5111_m26554_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26554_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t289_0_0_0;
static ParameterInfo t5111_m26555_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t289_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26555_GM;
MethodInfo m26555_MI = 
{
	"set_Item", NULL, &t5111_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5111_m26555_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26555_GM};
static MethodInfo* t5111_MIs[] =
{
	&m26556_MI,
	&m26557_MI,
	&m26558_MI,
	&m26554_MI,
	&m26555_MI,
	NULL
};
static TypeInfo* t5111_ITIs[] = 
{
	&t603_TI,
	&t5110_TI,
	&t5112_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5111_0_0_0;
extern Il2CppType t5111_1_0_0;
struct t5111;
extern Il2CppGenericClass t5111_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5111_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5111_MIs, t5111_PIs, NULL, NULL, NULL, NULL, NULL, &t5111_TI, t5111_ITIs, NULL, &t1908__CustomAttributeCache, &t5111_TI, &t5111_0_0_0, &t5111_1_0_0, NULL, &t5111_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5113_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IComparable>
extern MethodInfo m26559_MI;
static PropertyInfo t5113____Count_PropertyInfo = 
{
	&t5113_TI, "Count", &m26559_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26560_MI;
static PropertyInfo t5113____IsReadOnly_PropertyInfo = 
{
	&t5113_TI, "IsReadOnly", &m26560_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5113_PIs[] =
{
	&t5113____Count_PropertyInfo,
	&t5113____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26559_GM;
MethodInfo m26559_MI = 
{
	"get_Count", NULL, &t5113_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26559_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26560_GM;
MethodInfo m26560_MI = 
{
	"get_IsReadOnly", NULL, &t5113_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26560_GM};
extern Il2CppType t290_0_0_0;
extern Il2CppType t290_0_0_0;
static ParameterInfo t5113_m26561_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t290_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26561_GM;
MethodInfo m26561_MI = 
{
	"Add", NULL, &t5113_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5113_m26561_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26561_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26562_GM;
MethodInfo m26562_MI = 
{
	"Clear", NULL, &t5113_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26562_GM};
extern Il2CppType t290_0_0_0;
static ParameterInfo t5113_m26563_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t290_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26563_GM;
MethodInfo m26563_MI = 
{
	"Contains", NULL, &t5113_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5113_m26563_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26563_GM};
extern Il2CppType t3523_0_0_0;
extern Il2CppType t3523_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5113_m26564_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3523_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26564_GM;
MethodInfo m26564_MI = 
{
	"CopyTo", NULL, &t5113_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5113_m26564_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26564_GM};
extern Il2CppType t290_0_0_0;
static ParameterInfo t5113_m26565_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t290_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26565_GM;
MethodInfo m26565_MI = 
{
	"Remove", NULL, &t5113_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5113_m26565_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26565_GM};
static MethodInfo* t5113_MIs[] =
{
	&m26559_MI,
	&m26560_MI,
	&m26561_MI,
	&m26562_MI,
	&m26563_MI,
	&m26564_MI,
	&m26565_MI,
	NULL
};
extern TypeInfo t5115_TI;
static TypeInfo* t5113_ITIs[] = 
{
	&t603_TI,
	&t5115_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5113_0_0_0;
extern Il2CppType t5113_1_0_0;
struct t5113;
extern Il2CppGenericClass t5113_GC;
TypeInfo t5113_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5113_MIs, t5113_PIs, NULL, NULL, NULL, NULL, NULL, &t5113_TI, t5113_ITIs, NULL, &EmptyCustomAttributesCache, &t5113_TI, &t5113_0_0_0, &t5113_1_0_0, NULL, &t5113_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IComparable>
extern Il2CppType t3982_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26566_GM;
MethodInfo m26566_MI = 
{
	"GetEnumerator", NULL, &t5115_TI, &t3982_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26566_GM};
static MethodInfo* t5115_MIs[] =
{
	&m26566_MI,
	NULL
};
static TypeInfo* t5115_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5115_0_0_0;
extern Il2CppType t5115_1_0_0;
struct t5115;
extern Il2CppGenericClass t5115_GC;
TypeInfo t5115_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5115_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5115_TI, t5115_ITIs, NULL, &EmptyCustomAttributesCache, &t5115_TI, &t5115_0_0_0, &t5115_1_0_0, NULL, &t5115_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3982_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IComparable>
extern MethodInfo m26567_MI;
static PropertyInfo t3982____Current_PropertyInfo = 
{
	&t3982_TI, "Current", &m26567_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3982_PIs[] =
{
	&t3982____Current_PropertyInfo,
	NULL
};
extern Il2CppType t290_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26567_GM;
MethodInfo m26567_MI = 
{
	"get_Current", NULL, &t3982_TI, &t290_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26567_GM};
static MethodInfo* t3982_MIs[] =
{
	&m26567_MI,
	NULL
};
static TypeInfo* t3982_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3982_0_0_0;
extern Il2CppType t3982_1_0_0;
struct t3982;
extern Il2CppGenericClass t3982_GC;
TypeInfo t3982_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t3982_MIs, t3982_PIs, NULL, NULL, NULL, NULL, NULL, &t3982_TI, t3982_ITIs, NULL, &EmptyCustomAttributesCache, &t3982_TI, &t3982_0_0_0, &t3982_1_0_0, NULL, &t3982_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2165.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2165_TI;
#include "t2165MD.h"

extern TypeInfo t290_TI;
extern MethodInfo m10511_MI;
extern MethodInfo m19743_MI;
struct t20;
#define m19743(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.IComparable>
extern Il2CppType t20_0_0_1;
FieldInfo t2165_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2165_TI, offsetof(t2165, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2165_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2165_TI, offsetof(t2165, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2165_FIs[] =
{
	&t2165_f0_FieldInfo,
	&t2165_f1_FieldInfo,
	NULL
};
extern MethodInfo m10508_MI;
static PropertyInfo t2165____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2165_TI, "System.Collections.IEnumerator.Current", &m10508_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2165____Current_PropertyInfo = 
{
	&t2165_TI, "Current", &m10511_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2165_PIs[] =
{
	&t2165____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2165____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2165_m10507_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10507_GM;
MethodInfo m10507_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2165_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2165_m10507_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10507_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10508_GM;
MethodInfo m10508_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2165_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10508_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10509_GM;
MethodInfo m10509_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2165_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10509_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10510_GM;
MethodInfo m10510_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2165_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10510_GM};
extern Il2CppType t290_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10511_GM;
MethodInfo m10511_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2165_TI, &t290_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10511_GM};
static MethodInfo* t2165_MIs[] =
{
	&m10507_MI,
	&m10508_MI,
	&m10509_MI,
	&m10510_MI,
	&m10511_MI,
	NULL
};
extern MethodInfo m10510_MI;
extern MethodInfo m10509_MI;
static MethodInfo* t2165_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10508_MI,
	&m10510_MI,
	&m10509_MI,
	&m10511_MI,
};
static TypeInfo* t2165_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3982_TI,
};
static Il2CppInterfaceOffsetPair t2165_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3982_TI, 7},
};
extern TypeInfo t290_TI;
static Il2CppRGCTXData t2165_RGCTXData[3] = 
{
	&m10511_MI/* Method Usage */,
	&t290_TI/* Class Usage */,
	&m19743_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2165_0_0_0;
extern Il2CppType t2165_1_0_0;
extern Il2CppGenericClass t2165_GC;
TypeInfo t2165_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2165_MIs, t2165_PIs, t2165_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2165_TI, t2165_ITIs, t2165_VT, &EmptyCustomAttributesCache, &t2165_TI, &t2165_0_0_0, &t2165_1_0_0, t2165_IOs, &t2165_GC, NULL, NULL, NULL, t2165_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2165)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5114_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IComparable>
extern MethodInfo m26568_MI;
extern MethodInfo m26569_MI;
static PropertyInfo t5114____Item_PropertyInfo = 
{
	&t5114_TI, "Item", &m26568_MI, &m26569_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5114_PIs[] =
{
	&t5114____Item_PropertyInfo,
	NULL
};
extern Il2CppType t290_0_0_0;
static ParameterInfo t5114_m26570_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t290_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26570_GM;
MethodInfo m26570_MI = 
{
	"IndexOf", NULL, &t5114_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5114_m26570_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26570_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t290_0_0_0;
static ParameterInfo t5114_m26571_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t290_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26571_GM;
MethodInfo m26571_MI = 
{
	"Insert", NULL, &t5114_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5114_m26571_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26571_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5114_m26572_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26572_GM;
MethodInfo m26572_MI = 
{
	"RemoveAt", NULL, &t5114_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5114_m26572_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26572_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5114_m26568_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t290_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26568_GM;
MethodInfo m26568_MI = 
{
	"get_Item", NULL, &t5114_TI, &t290_0_0_0, RuntimeInvoker_t29_t44, t5114_m26568_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26568_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t290_0_0_0;
static ParameterInfo t5114_m26569_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t290_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26569_GM;
MethodInfo m26569_MI = 
{
	"set_Item", NULL, &t5114_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5114_m26569_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26569_GM};
static MethodInfo* t5114_MIs[] =
{
	&m26570_MI,
	&m26571_MI,
	&m26572_MI,
	&m26568_MI,
	&m26569_MI,
	NULL
};
static TypeInfo* t5114_ITIs[] = 
{
	&t603_TI,
	&t5113_TI,
	&t5115_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5114_0_0_0;
extern Il2CppType t5114_1_0_0;
struct t5114;
extern Il2CppGenericClass t5114_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5114_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5114_MIs, t5114_PIs, NULL, NULL, NULL, NULL, NULL, &t5114_TI, t5114_ITIs, NULL, &t1908__CustomAttributeCache, &t5114_TI, &t5114_0_0_0, &t5114_1_0_0, NULL, &t5114_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5116_TI;

#include "t110.h"


// Metadata Definition System.Collections.Generic.ICollection`1<System.ValueType>
extern MethodInfo m26573_MI;
static PropertyInfo t5116____Count_PropertyInfo = 
{
	&t5116_TI, "Count", &m26573_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26574_MI;
static PropertyInfo t5116____IsReadOnly_PropertyInfo = 
{
	&t5116_TI, "IsReadOnly", &m26574_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5116_PIs[] =
{
	&t5116____Count_PropertyInfo,
	&t5116____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26573_GM;
MethodInfo m26573_MI = 
{
	"get_Count", NULL, &t5116_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26573_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26574_GM;
MethodInfo m26574_MI = 
{
	"get_IsReadOnly", NULL, &t5116_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26574_GM};
extern Il2CppType t110_0_0_0;
extern Il2CppType t110_0_0_0;
static ParameterInfo t5116_m26575_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t110_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26575_GM;
MethodInfo m26575_MI = 
{
	"Add", NULL, &t5116_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5116_m26575_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26575_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26576_GM;
MethodInfo m26576_MI = 
{
	"Clear", NULL, &t5116_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26576_GM};
extern Il2CppType t110_0_0_0;
static ParameterInfo t5116_m26577_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t110_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26577_GM;
MethodInfo m26577_MI = 
{
	"Contains", NULL, &t5116_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5116_m26577_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26577_GM};
extern Il2CppType t3524_0_0_0;
extern Il2CppType t3524_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5116_m26578_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3524_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26578_GM;
MethodInfo m26578_MI = 
{
	"CopyTo", NULL, &t5116_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5116_m26578_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26578_GM};
extern Il2CppType t110_0_0_0;
static ParameterInfo t5116_m26579_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t110_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26579_GM;
MethodInfo m26579_MI = 
{
	"Remove", NULL, &t5116_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5116_m26579_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26579_GM};
static MethodInfo* t5116_MIs[] =
{
	&m26573_MI,
	&m26574_MI,
	&m26575_MI,
	&m26576_MI,
	&m26577_MI,
	&m26578_MI,
	&m26579_MI,
	NULL
};
extern TypeInfo t5118_TI;
static TypeInfo* t5116_ITIs[] = 
{
	&t603_TI,
	&t5118_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5116_0_0_0;
extern Il2CppType t5116_1_0_0;
struct t5116;
extern Il2CppGenericClass t5116_GC;
TypeInfo t5116_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5116_MIs, t5116_PIs, NULL, NULL, NULL, NULL, NULL, &t5116_TI, t5116_ITIs, NULL, &EmptyCustomAttributesCache, &t5116_TI, &t5116_0_0_0, &t5116_1_0_0, NULL, &t5116_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.ValueType>
extern Il2CppType t3984_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26580_GM;
MethodInfo m26580_MI = 
{
	"GetEnumerator", NULL, &t5118_TI, &t3984_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26580_GM};
static MethodInfo* t5118_MIs[] =
{
	&m26580_MI,
	NULL
};
static TypeInfo* t5118_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5118_0_0_0;
extern Il2CppType t5118_1_0_0;
struct t5118;
extern Il2CppGenericClass t5118_GC;
TypeInfo t5118_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5118_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5118_TI, t5118_ITIs, NULL, &EmptyCustomAttributesCache, &t5118_TI, &t5118_0_0_0, &t5118_1_0_0, NULL, &t5118_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3984_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.ValueType>
extern MethodInfo m26581_MI;
static PropertyInfo t3984____Current_PropertyInfo = 
{
	&t3984_TI, "Current", &m26581_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3984_PIs[] =
{
	&t3984____Current_PropertyInfo,
	NULL
};
extern Il2CppType t110_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26581_GM;
MethodInfo m26581_MI = 
{
	"get_Current", NULL, &t3984_TI, &t110_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26581_GM};
static MethodInfo* t3984_MIs[] =
{
	&m26581_MI,
	NULL
};
static TypeInfo* t3984_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3984_0_0_0;
extern Il2CppType t3984_1_0_0;
struct t3984;
extern Il2CppGenericClass t3984_GC;
TypeInfo t3984_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t3984_MIs, t3984_PIs, NULL, NULL, NULL, NULL, NULL, &t3984_TI, t3984_ITIs, NULL, &EmptyCustomAttributesCache, &t3984_TI, &t3984_0_0_0, &t3984_1_0_0, NULL, &t3984_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2166.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2166_TI;
#include "t2166MD.h"

extern MethodInfo m10516_MI;
extern MethodInfo m19754_MI;
struct t20;
#define m19754(__this, p0, method) (t110 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.ValueType>
extern Il2CppType t20_0_0_1;
FieldInfo t2166_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2166_TI, offsetof(t2166, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2166_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2166_TI, offsetof(t2166, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2166_FIs[] =
{
	&t2166_f0_FieldInfo,
	&t2166_f1_FieldInfo,
	NULL
};
extern MethodInfo m10513_MI;
static PropertyInfo t2166____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2166_TI, "System.Collections.IEnumerator.Current", &m10513_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2166____Current_PropertyInfo = 
{
	&t2166_TI, "Current", &m10516_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2166_PIs[] =
{
	&t2166____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2166____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2166_m10512_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10512_GM;
MethodInfo m10512_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2166_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2166_m10512_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10512_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10513_GM;
MethodInfo m10513_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2166_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10513_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10514_GM;
MethodInfo m10514_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2166_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10514_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10515_GM;
MethodInfo m10515_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2166_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10515_GM};
extern Il2CppType t110_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10516_GM;
MethodInfo m10516_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2166_TI, &t110_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10516_GM};
static MethodInfo* t2166_MIs[] =
{
	&m10512_MI,
	&m10513_MI,
	&m10514_MI,
	&m10515_MI,
	&m10516_MI,
	NULL
};
extern MethodInfo m10515_MI;
extern MethodInfo m10514_MI;
static MethodInfo* t2166_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10513_MI,
	&m10515_MI,
	&m10514_MI,
	&m10516_MI,
};
static TypeInfo* t2166_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3984_TI,
};
static Il2CppInterfaceOffsetPair t2166_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3984_TI, 7},
};
extern TypeInfo t110_TI;
static Il2CppRGCTXData t2166_RGCTXData[3] = 
{
	&m10516_MI/* Method Usage */,
	&t110_TI/* Class Usage */,
	&m19754_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2166_0_0_0;
extern Il2CppType t2166_1_0_0;
extern Il2CppGenericClass t2166_GC;
TypeInfo t2166_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2166_MIs, t2166_PIs, t2166_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2166_TI, t2166_ITIs, t2166_VT, &EmptyCustomAttributesCache, &t2166_TI, &t2166_0_0_0, &t2166_1_0_0, t2166_IOs, &t2166_GC, NULL, NULL, NULL, t2166_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2166)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5117_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.ValueType>
extern MethodInfo m26582_MI;
extern MethodInfo m26583_MI;
static PropertyInfo t5117____Item_PropertyInfo = 
{
	&t5117_TI, "Item", &m26582_MI, &m26583_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5117_PIs[] =
{
	&t5117____Item_PropertyInfo,
	NULL
};
extern Il2CppType t110_0_0_0;
static ParameterInfo t5117_m26584_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t110_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26584_GM;
MethodInfo m26584_MI = 
{
	"IndexOf", NULL, &t5117_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5117_m26584_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26584_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t110_0_0_0;
static ParameterInfo t5117_m26585_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t110_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26585_GM;
MethodInfo m26585_MI = 
{
	"Insert", NULL, &t5117_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5117_m26585_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26585_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5117_m26586_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26586_GM;
MethodInfo m26586_MI = 
{
	"RemoveAt", NULL, &t5117_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5117_m26586_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26586_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5117_m26582_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t110_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26582_GM;
MethodInfo m26582_MI = 
{
	"get_Item", NULL, &t5117_TI, &t110_0_0_0, RuntimeInvoker_t29_t44, t5117_m26582_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26582_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t110_0_0_0;
static ParameterInfo t5117_m26583_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t110_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26583_GM;
MethodInfo m26583_MI = 
{
	"set_Item", NULL, &t5117_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5117_m26583_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26583_GM};
static MethodInfo* t5117_MIs[] =
{
	&m26584_MI,
	&m26585_MI,
	&m26586_MI,
	&m26582_MI,
	&m26583_MI,
	NULL
};
static TypeInfo* t5117_ITIs[] = 
{
	&t603_TI,
	&t5116_TI,
	&t5118_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5117_0_0_0;
extern Il2CppType t5117_1_0_0;
struct t5117;
extern Il2CppGenericClass t5117_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5117_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5117_MIs, t5117_PIs, NULL, NULL, NULL, NULL, NULL, &t5117_TI, t5117_ITIs, NULL, &t1908__CustomAttributeCache, &t5117_TI, &t5117_0_0_0, &t5117_1_0_0, NULL, &t5117_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3986_TI;

#include "t50.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.EventSystem>
extern MethodInfo m26587_MI;
static PropertyInfo t3986____Current_PropertyInfo = 
{
	&t3986_TI, "Current", &m26587_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3986_PIs[] =
{
	&t3986____Current_PropertyInfo,
	NULL
};
extern Il2CppType t50_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26587_GM;
MethodInfo m26587_MI = 
{
	"get_Current", NULL, &t3986_TI, &t50_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26587_GM};
static MethodInfo* t3986_MIs[] =
{
	&m26587_MI,
	NULL
};
static TypeInfo* t3986_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3986_0_0_0;
extern Il2CppType t3986_1_0_0;
struct t3986;
extern Il2CppGenericClass t3986_GC;
TypeInfo t3986_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t3986_MIs, t3986_PIs, NULL, NULL, NULL, NULL, NULL, &t3986_TI, t3986_ITIs, NULL, &EmptyCustomAttributesCache, &t3986_TI, &t3986_0_0_0, &t3986_1_0_0, NULL, &t3986_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2167.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2167_TI;
#include "t2167MD.h"

extern TypeInfo t50_TI;
extern MethodInfo m10521_MI;
extern MethodInfo m19765_MI;
struct t20;
#define m19765(__this, p0, method) (t50 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventSystem>
extern Il2CppType t20_0_0_1;
FieldInfo t2167_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2167_TI, offsetof(t2167, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2167_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2167_TI, offsetof(t2167, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2167_FIs[] =
{
	&t2167_f0_FieldInfo,
	&t2167_f1_FieldInfo,
	NULL
};
extern MethodInfo m10518_MI;
static PropertyInfo t2167____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2167_TI, "System.Collections.IEnumerator.Current", &m10518_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2167____Current_PropertyInfo = 
{
	&t2167_TI, "Current", &m10521_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2167_PIs[] =
{
	&t2167____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2167____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2167_m10517_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10517_GM;
MethodInfo m10517_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2167_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2167_m10517_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10517_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10518_GM;
MethodInfo m10518_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2167_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10518_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10519_GM;
MethodInfo m10519_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2167_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10519_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10520_GM;
MethodInfo m10520_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2167_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10520_GM};
extern Il2CppType t50_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10521_GM;
MethodInfo m10521_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2167_TI, &t50_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10521_GM};
static MethodInfo* t2167_MIs[] =
{
	&m10517_MI,
	&m10518_MI,
	&m10519_MI,
	&m10520_MI,
	&m10521_MI,
	NULL
};
extern MethodInfo m10520_MI;
extern MethodInfo m10519_MI;
static MethodInfo* t2167_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10518_MI,
	&m10520_MI,
	&m10519_MI,
	&m10521_MI,
};
static TypeInfo* t2167_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3986_TI,
};
static Il2CppInterfaceOffsetPair t2167_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3986_TI, 7},
};
extern TypeInfo t50_TI;
static Il2CppRGCTXData t2167_RGCTXData[3] = 
{
	&m10521_MI/* Method Usage */,
	&t50_TI/* Class Usage */,
	&m19765_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2167_0_0_0;
extern Il2CppType t2167_1_0_0;
extern Il2CppGenericClass t2167_GC;
TypeInfo t2167_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2167_MIs, t2167_PIs, t2167_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2167_TI, t2167_ITIs, t2167_VT, &EmptyCustomAttributesCache, &t2167_TI, &t2167_0_0_0, &t2167_1_0_0, t2167_IOs, &t2167_GC, NULL, NULL, NULL, t2167_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2167)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5119_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventSystem>
extern MethodInfo m26588_MI;
static PropertyInfo t5119____Count_PropertyInfo = 
{
	&t5119_TI, "Count", &m26588_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26589_MI;
static PropertyInfo t5119____IsReadOnly_PropertyInfo = 
{
	&t5119_TI, "IsReadOnly", &m26589_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5119_PIs[] =
{
	&t5119____Count_PropertyInfo,
	&t5119____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26588_GM;
MethodInfo m26588_MI = 
{
	"get_Count", NULL, &t5119_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26588_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26589_GM;
MethodInfo m26589_MI = 
{
	"get_IsReadOnly", NULL, &t5119_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26589_GM};
extern Il2CppType t50_0_0_0;
extern Il2CppType t50_0_0_0;
static ParameterInfo t5119_m26590_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t50_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26590_GM;
MethodInfo m26590_MI = 
{
	"Add", NULL, &t5119_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5119_m26590_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26590_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26591_GM;
MethodInfo m26591_MI = 
{
	"Clear", NULL, &t5119_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26591_GM};
extern Il2CppType t50_0_0_0;
static ParameterInfo t5119_m26592_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t50_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26592_GM;
MethodInfo m26592_MI = 
{
	"Contains", NULL, &t5119_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5119_m26592_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26592_GM};
extern Il2CppType t3801_0_0_0;
extern Il2CppType t3801_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5119_m26593_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3801_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26593_GM;
MethodInfo m26593_MI = 
{
	"CopyTo", NULL, &t5119_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5119_m26593_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26593_GM};
extern Il2CppType t50_0_0_0;
static ParameterInfo t5119_m26594_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t50_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26594_GM;
MethodInfo m26594_MI = 
{
	"Remove", NULL, &t5119_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5119_m26594_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26594_GM};
static MethodInfo* t5119_MIs[] =
{
	&m26588_MI,
	&m26589_MI,
	&m26590_MI,
	&m26591_MI,
	&m26592_MI,
	&m26593_MI,
	&m26594_MI,
	NULL
};
extern TypeInfo t5121_TI;
static TypeInfo* t5119_ITIs[] = 
{
	&t603_TI,
	&t5121_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5119_0_0_0;
extern Il2CppType t5119_1_0_0;
struct t5119;
extern Il2CppGenericClass t5119_GC;
TypeInfo t5119_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5119_MIs, t5119_PIs, NULL, NULL, NULL, NULL, NULL, &t5119_TI, t5119_ITIs, NULL, &EmptyCustomAttributesCache, &t5119_TI, &t5119_0_0_0, &t5119_1_0_0, NULL, &t5119_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.EventSystem>
extern Il2CppType t3986_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26595_GM;
MethodInfo m26595_MI = 
{
	"GetEnumerator", NULL, &t5121_TI, &t3986_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26595_GM};
static MethodInfo* t5121_MIs[] =
{
	&m26595_MI,
	NULL
};
static TypeInfo* t5121_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5121_0_0_0;
extern Il2CppType t5121_1_0_0;
struct t5121;
extern Il2CppGenericClass t5121_GC;
TypeInfo t5121_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5121_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5121_TI, t5121_ITIs, NULL, &EmptyCustomAttributesCache, &t5121_TI, &t5121_0_0_0, &t5121_1_0_0, NULL, &t5121_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5120_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.EventSystem>
extern MethodInfo m26596_MI;
extern MethodInfo m26597_MI;
static PropertyInfo t5120____Item_PropertyInfo = 
{
	&t5120_TI, "Item", &m26596_MI, &m26597_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5120_PIs[] =
{
	&t5120____Item_PropertyInfo,
	NULL
};
extern Il2CppType t50_0_0_0;
static ParameterInfo t5120_m26598_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t50_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26598_GM;
MethodInfo m26598_MI = 
{
	"IndexOf", NULL, &t5120_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5120_m26598_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26598_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t50_0_0_0;
static ParameterInfo t5120_m26599_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t50_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26599_GM;
MethodInfo m26599_MI = 
{
	"Insert", NULL, &t5120_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5120_m26599_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26599_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5120_m26600_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26600_GM;
MethodInfo m26600_MI = 
{
	"RemoveAt", NULL, &t5120_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5120_m26600_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26600_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5120_m26596_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t50_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26596_GM;
MethodInfo m26596_MI = 
{
	"get_Item", NULL, &t5120_TI, &t50_0_0_0, RuntimeInvoker_t29_t44, t5120_m26596_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26596_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t50_0_0_0;
static ParameterInfo t5120_m26597_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t50_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26597_GM;
MethodInfo m26597_MI = 
{
	"set_Item", NULL, &t5120_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5120_m26597_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26597_GM};
static MethodInfo* t5120_MIs[] =
{
	&m26598_MI,
	&m26599_MI,
	&m26600_MI,
	&m26596_MI,
	&m26597_MI,
	NULL
};
static TypeInfo* t5120_ITIs[] = 
{
	&t603_TI,
	&t5119_TI,
	&t5121_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5120_0_0_0;
extern Il2CppType t5120_1_0_0;
struct t5120;
extern Il2CppGenericClass t5120_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5120_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5120_MIs, t5120_PIs, NULL, NULL, NULL, NULL, NULL, &t5120_TI, t5120_ITIs, NULL, &t1908__CustomAttributeCache, &t5120_TI, &t5120_0_0_0, &t5120_1_0_0, NULL, &t5120_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5122_TI;

#include "t55.h"


// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.UIBehaviour>
extern MethodInfo m26601_MI;
static PropertyInfo t5122____Count_PropertyInfo = 
{
	&t5122_TI, "Count", &m26601_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26602_MI;
static PropertyInfo t5122____IsReadOnly_PropertyInfo = 
{
	&t5122_TI, "IsReadOnly", &m26602_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5122_PIs[] =
{
	&t5122____Count_PropertyInfo,
	&t5122____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26601_GM;
MethodInfo m26601_MI = 
{
	"get_Count", NULL, &t5122_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26601_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26602_GM;
MethodInfo m26602_MI = 
{
	"get_IsReadOnly", NULL, &t5122_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26602_GM};
extern Il2CppType t55_0_0_0;
extern Il2CppType t55_0_0_0;
static ParameterInfo t5122_m26603_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t55_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26603_GM;
MethodInfo m26603_MI = 
{
	"Add", NULL, &t5122_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5122_m26603_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26603_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26604_GM;
MethodInfo m26604_MI = 
{
	"Clear", NULL, &t5122_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26604_GM};
extern Il2CppType t55_0_0_0;
static ParameterInfo t5122_m26605_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t55_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26605_GM;
MethodInfo m26605_MI = 
{
	"Contains", NULL, &t5122_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5122_m26605_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26605_GM};
extern Il2CppType t3802_0_0_0;
extern Il2CppType t3802_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5122_m26606_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3802_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26606_GM;
MethodInfo m26606_MI = 
{
	"CopyTo", NULL, &t5122_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5122_m26606_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26606_GM};
extern Il2CppType t55_0_0_0;
static ParameterInfo t5122_m26607_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t55_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26607_GM;
MethodInfo m26607_MI = 
{
	"Remove", NULL, &t5122_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5122_m26607_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26607_GM};
static MethodInfo* t5122_MIs[] =
{
	&m26601_MI,
	&m26602_MI,
	&m26603_MI,
	&m26604_MI,
	&m26605_MI,
	&m26606_MI,
	&m26607_MI,
	NULL
};
extern TypeInfo t5124_TI;
static TypeInfo* t5122_ITIs[] = 
{
	&t603_TI,
	&t5124_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5122_0_0_0;
extern Il2CppType t5122_1_0_0;
struct t5122;
extern Il2CppGenericClass t5122_GC;
TypeInfo t5122_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5122_MIs, t5122_PIs, NULL, NULL, NULL, NULL, NULL, &t5122_TI, t5122_ITIs, NULL, &EmptyCustomAttributesCache, &t5122_TI, &t5122_0_0_0, &t5122_1_0_0, NULL, &t5122_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.UIBehaviour>
extern Il2CppType t3988_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26608_GM;
MethodInfo m26608_MI = 
{
	"GetEnumerator", NULL, &t5124_TI, &t3988_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26608_GM};
static MethodInfo* t5124_MIs[] =
{
	&m26608_MI,
	NULL
};
static TypeInfo* t5124_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5124_0_0_0;
extern Il2CppType t5124_1_0_0;
struct t5124;
extern Il2CppGenericClass t5124_GC;
TypeInfo t5124_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5124_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5124_TI, t5124_ITIs, NULL, &EmptyCustomAttributesCache, &t5124_TI, &t5124_0_0_0, &t5124_1_0_0, NULL, &t5124_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3988_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.UIBehaviour>
extern MethodInfo m26609_MI;
static PropertyInfo t3988____Current_PropertyInfo = 
{
	&t3988_TI, "Current", &m26609_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3988_PIs[] =
{
	&t3988____Current_PropertyInfo,
	NULL
};
extern Il2CppType t55_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26609_GM;
MethodInfo m26609_MI = 
{
	"get_Current", NULL, &t3988_TI, &t55_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26609_GM};
static MethodInfo* t3988_MIs[] =
{
	&m26609_MI,
	NULL
};
static TypeInfo* t3988_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3988_0_0_0;
extern Il2CppType t3988_1_0_0;
struct t3988;
extern Il2CppGenericClass t3988_GC;
TypeInfo t3988_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t3988_MIs, t3988_PIs, NULL, NULL, NULL, NULL, NULL, &t3988_TI, t3988_ITIs, NULL, &EmptyCustomAttributesCache, &t3988_TI, &t3988_0_0_0, &t3988_1_0_0, NULL, &t3988_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2168.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2168_TI;
#include "t2168MD.h"

extern TypeInfo t55_TI;
extern MethodInfo m10526_MI;
extern MethodInfo m19776_MI;
struct t20;
#define m19776(__this, p0, method) (t55 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.UIBehaviour>
extern Il2CppType t20_0_0_1;
FieldInfo t2168_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2168_TI, offsetof(t2168, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2168_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2168_TI, offsetof(t2168, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2168_FIs[] =
{
	&t2168_f0_FieldInfo,
	&t2168_f1_FieldInfo,
	NULL
};
extern MethodInfo m10523_MI;
static PropertyInfo t2168____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2168_TI, "System.Collections.IEnumerator.Current", &m10523_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2168____Current_PropertyInfo = 
{
	&t2168_TI, "Current", &m10526_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2168_PIs[] =
{
	&t2168____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2168____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2168_m10522_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10522_GM;
MethodInfo m10522_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2168_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2168_m10522_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10522_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10523_GM;
MethodInfo m10523_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2168_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10523_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10524_GM;
MethodInfo m10524_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2168_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10524_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10525_GM;
MethodInfo m10525_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2168_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10525_GM};
extern Il2CppType t55_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10526_GM;
MethodInfo m10526_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2168_TI, &t55_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10526_GM};
static MethodInfo* t2168_MIs[] =
{
	&m10522_MI,
	&m10523_MI,
	&m10524_MI,
	&m10525_MI,
	&m10526_MI,
	NULL
};
extern MethodInfo m10525_MI;
extern MethodInfo m10524_MI;
static MethodInfo* t2168_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10523_MI,
	&m10525_MI,
	&m10524_MI,
	&m10526_MI,
};
static TypeInfo* t2168_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3988_TI,
};
static Il2CppInterfaceOffsetPair t2168_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3988_TI, 7},
};
extern TypeInfo t55_TI;
static Il2CppRGCTXData t2168_RGCTXData[3] = 
{
	&m10526_MI/* Method Usage */,
	&t55_TI/* Class Usage */,
	&m19776_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2168_0_0_0;
extern Il2CppType t2168_1_0_0;
extern Il2CppGenericClass t2168_GC;
TypeInfo t2168_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2168_MIs, t2168_PIs, t2168_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2168_TI, t2168_ITIs, t2168_VT, &EmptyCustomAttributesCache, &t2168_TI, &t2168_0_0_0, &t2168_1_0_0, t2168_IOs, &t2168_GC, NULL, NULL, NULL, t2168_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2168)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5123_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.UIBehaviour>
extern MethodInfo m26610_MI;
extern MethodInfo m26611_MI;
static PropertyInfo t5123____Item_PropertyInfo = 
{
	&t5123_TI, "Item", &m26610_MI, &m26611_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5123_PIs[] =
{
	&t5123____Item_PropertyInfo,
	NULL
};
extern Il2CppType t55_0_0_0;
static ParameterInfo t5123_m26612_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t55_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26612_GM;
MethodInfo m26612_MI = 
{
	"IndexOf", NULL, &t5123_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5123_m26612_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26612_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t55_0_0_0;
static ParameterInfo t5123_m26613_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t55_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26613_GM;
MethodInfo m26613_MI = 
{
	"Insert", NULL, &t5123_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5123_m26613_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26613_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5123_m26614_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26614_GM;
MethodInfo m26614_MI = 
{
	"RemoveAt", NULL, &t5123_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5123_m26614_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26614_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5123_m26610_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t55_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26610_GM;
MethodInfo m26610_MI = 
{
	"get_Item", NULL, &t5123_TI, &t55_0_0_0, RuntimeInvoker_t29_t44, t5123_m26610_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26610_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t55_0_0_0;
static ParameterInfo t5123_m26611_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t55_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26611_GM;
MethodInfo m26611_MI = 
{
	"set_Item", NULL, &t5123_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5123_m26611_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26611_GM};
static MethodInfo* t5123_MIs[] =
{
	&m26612_MI,
	&m26613_MI,
	&m26614_MI,
	&m26610_MI,
	&m26611_MI,
	NULL
};
static TypeInfo* t5123_ITIs[] = 
{
	&t603_TI,
	&t5122_TI,
	&t5124_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5123_0_0_0;
extern Il2CppType t5123_1_0_0;
struct t5123;
extern Il2CppGenericClass t5123_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5123_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5123_MIs, t5123_PIs, NULL, NULL, NULL, NULL, NULL, &t5123_TI, t5123_ITIs, NULL, &t1908__CustomAttributeCache, &t5123_TI, &t5123_0_0_0, &t5123_1_0_0, NULL, &t5123_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2169.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2169_TI;
#include "t2169MD.h"

#include "t2170.h"
extern TypeInfo t2170_TI;
#include "t2170MD.h"
extern MethodInfo m10529_MI;
extern MethodInfo m10531_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.EventSystems.EventSystem>
extern Il2CppType t316_0_0_33;
FieldInfo t2169_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2169_TI, offsetof(t2169, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2169_FIs[] =
{
	&t2169_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t50_0_0_0;
static ParameterInfo t2169_m10527_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t50_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10527_GM;
MethodInfo m10527_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2169_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2169_m10527_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m10527_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2169_m10528_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10528_GM;
MethodInfo m10528_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2169_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2169_m10528_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10528_GM};
static MethodInfo* t2169_MIs[] =
{
	&m10527_MI,
	&m10528_MI,
	NULL
};
extern MethodInfo m10528_MI;
extern MethodInfo m10532_MI;
static MethodInfo* t2169_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m10528_MI,
	&m10532_MI,
};
extern Il2CppType t2171_0_0_0;
extern TypeInfo t2171_TI;
extern MethodInfo m19786_MI;
extern TypeInfo t50_TI;
extern MethodInfo m10534_MI;
extern TypeInfo t50_TI;
static Il2CppRGCTXData t2169_RGCTXData[8] = 
{
	&t2171_0_0_0/* Type Usage */,
	&t2171_TI/* Class Usage */,
	&m19786_MI/* Method Usage */,
	&t50_TI/* Class Usage */,
	&m10534_MI/* Method Usage */,
	&m10529_MI/* Method Usage */,
	&t50_TI/* Class Usage */,
	&m10531_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2169_0_0_0;
extern Il2CppType t2169_1_0_0;
struct t2169;
extern Il2CppGenericClass t2169_GC;
TypeInfo t2169_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2169_MIs, NULL, t2169_FIs, NULL, &t2170_TI, NULL, NULL, &t2169_TI, NULL, t2169_VT, &EmptyCustomAttributesCache, &t2169_TI, &t2169_0_0_0, &t2169_1_0_0, NULL, &t2169_GC, NULL, NULL, NULL, t2169_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2169), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2171.h"
extern TypeInfo t2171_TI;
#include "t2171MD.h"
struct t556;
#define m19786(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.EventSystem>
extern Il2CppType t2171_0_0_1;
FieldInfo t2170_f0_FieldInfo = 
{
	"Delegate", &t2171_0_0_1, &t2170_TI, offsetof(t2170, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2170_FIs[] =
{
	&t2170_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2170_m10529_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10529_GM;
MethodInfo m10529_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2170_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2170_m10529_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10529_GM};
extern Il2CppType t2171_0_0_0;
static ParameterInfo t2170_m10530_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2171_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10530_GM;
MethodInfo m10530_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2170_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2170_m10530_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10530_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2170_m10531_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10531_GM;
MethodInfo m10531_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2170_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2170_m10531_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10531_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2170_m10532_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10532_GM;
MethodInfo m10532_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2170_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2170_m10532_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10532_GM};
static MethodInfo* t2170_MIs[] =
{
	&m10529_MI,
	&m10530_MI,
	&m10531_MI,
	&m10532_MI,
	NULL
};
static MethodInfo* t2170_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m10531_MI,
	&m10532_MI,
};
extern TypeInfo t2171_TI;
extern TypeInfo t50_TI;
static Il2CppRGCTXData t2170_RGCTXData[5] = 
{
	&t2171_0_0_0/* Type Usage */,
	&t2171_TI/* Class Usage */,
	&m19786_MI/* Method Usage */,
	&t50_TI/* Class Usage */,
	&m10534_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2170_0_0_0;
extern Il2CppType t2170_1_0_0;
struct t2170;
extern Il2CppGenericClass t2170_GC;
TypeInfo t2170_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2170_MIs, NULL, t2170_FIs, NULL, &t556_TI, NULL, NULL, &t2170_TI, NULL, t2170_VT, &EmptyCustomAttributesCache, &t2170_TI, &t2170_0_0_0, &t2170_1_0_0, NULL, &t2170_GC, NULL, NULL, NULL, t2170_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2170), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.EventSystem>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2171_m10533_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10533_GM;
MethodInfo m10533_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2171_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2171_m10533_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10533_GM};
extern Il2CppType t50_0_0_0;
static ParameterInfo t2171_m10534_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t50_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10534_GM;
MethodInfo m10534_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2171_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2171_m10534_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10534_GM};
extern Il2CppType t50_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2171_m10535_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t50_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10535_GM;
MethodInfo m10535_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2171_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2171_m10535_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m10535_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2171_m10536_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10536_GM;
MethodInfo m10536_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2171_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2171_m10536_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10536_GM};
static MethodInfo* t2171_MIs[] =
{
	&m10533_MI,
	&m10534_MI,
	&m10535_MI,
	&m10536_MI,
	NULL
};
extern MethodInfo m10535_MI;
extern MethodInfo m10536_MI;
static MethodInfo* t2171_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m10534_MI,
	&m10535_MI,
	&m10536_MI,
};
static Il2CppInterfaceOffsetPair t2171_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2171_1_0_0;
struct t2171;
extern Il2CppGenericClass t2171_GC;
TypeInfo t2171_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2171_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2171_TI, NULL, t2171_VT, &EmptyCustomAttributesCache, &t2171_TI, &t2171_0_0_0, &t2171_1_0_0, t2171_IOs, &t2171_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2171), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t51.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t51_TI;
#include "t51MD.h"

#include "t52.h"
#include "t2179.h"
#include "t2176.h"
#include "t2177.h"
#include "t338.h"
#include "t2211.h"
#include "t2178.h"
extern TypeInfo t52_TI;
extern TypeInfo t44_TI;
extern TypeInfo t585_TI;
extern TypeInfo t1649_TI;
extern TypeInfo t915_TI;
extern TypeInfo t2172_TI;
extern TypeInfo t2179_TI;
extern TypeInfo t40_TI;
extern TypeInfo t2174_TI;
extern TypeInfo t2175_TI;
extern TypeInfo t2173_TI;
extern TypeInfo t2176_TI;
extern TypeInfo t338_TI;
extern TypeInfo t2177_TI;
extern TypeInfo t2211_TI;
#include "t915MD.h"
#include "t29MD.h"
#include "t602MD.h"
#include "t2176MD.h"
#include "t338MD.h"
#include "t2177MD.h"
#include "t2179MD.h"
#include "t2211MD.h"
extern MethodInfo m1292_MI;
extern MethodInfo m10628_MI;
extern MethodInfo m4198_MI;
extern MethodInfo m19978_MI;
extern MethodInfo m3975_MI;
extern MethodInfo m10601_MI;
extern MethodInfo m1331_MI;
extern MethodInfo m10596_MI;
extern MethodInfo m5952_MI;
extern MethodInfo m10572_MI;
extern MethodInfo m10586_MI;
extern MethodInfo m10597_MI;
extern MethodInfo m10603_MI;
extern MethodInfo m10607_MI;
extern MethodInfo m10574_MI;
extern MethodInfo m10622_MI;
extern MethodInfo m5139_MI;
extern MethodInfo m10624_MI;
extern MethodInfo m26615_MI;
extern MethodInfo m26616_MI;
extern MethodInfo m26617_MI;
extern MethodInfo m26618_MI;
extern MethodInfo m3972_MI;
extern MethodInfo m1428_MI;
extern MethodInfo m10605_MI;
extern MethodInfo m10576_MI;
extern MethodInfo m10578_MI;
extern MethodInfo m10811_MI;
extern MethodInfo m5112_MI;
extern MethodInfo m19980_MI;
extern MethodInfo m10592_MI;
extern MethodInfo m10594_MI;
extern MethodInfo m2950_MI;
extern MethodInfo m10886_MI;
extern MethodInfo m10805_MI;
extern MethodInfo m10599_MI;
extern MethodInfo m1294_MI;
extern MethodInfo m5171_MI;
extern MethodInfo m10892_MI;
extern MethodInfo m19982_MI;
extern MethodInfo m19990_MI;
extern MethodInfo m5951_MI;
struct t20;
struct t20;
 void m19788_gshared (t29 * __this, t316** p0, int32_t p1, MethodInfo* method);
#define m19788(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
#define m19978(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
struct t20;
#include "t2209.h"
struct t20;
#include "t2001.h"
 int32_t m10120_gshared (t29 * __this, t316* p0, t29 * p1, int32_t p2, int32_t p3, MethodInfo* method);
#define m10120(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
#define m19980(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
struct t20;
struct t20;
 void m19857_gshared (t29 * __this, t316* p0, int32_t p1, int32_t p2, t29* p3, MethodInfo* method);
#define m19857(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
#define m19982(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
struct t20;
#include "t295.h"
struct t20;
#include "t2181.h"
 void m19964_gshared (t29 * __this, t316* p0, int32_t p1, t2181 * p2, MethodInfo* method);
#define m19964(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)
#define m19990(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)


 t2179  m10596 (t51 * __this, MethodInfo* method){
	{
		t2179  L_0 = {0};
		m10805(&L_0, __this, &m10805_MI);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>
extern Il2CppType t44_0_0_32849;
FieldInfo t51_f0_FieldInfo = 
{
	"DefaultCapacity", &t44_0_0_32849, &t51_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t2172_0_0_1;
FieldInfo t51_f1_FieldInfo = 
{
	"_items", &t2172_0_0_1, &t51_TI, offsetof(t51, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t51_f2_FieldInfo = 
{
	"_size", &t44_0_0_1, &t51_TI, offsetof(t51, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t51_f3_FieldInfo = 
{
	"_version", &t44_0_0_1, &t51_TI, offsetof(t51, f3), &EmptyCustomAttributesCache};
extern Il2CppType t2172_0_0_49;
FieldInfo t51_f4_FieldInfo = 
{
	"EmptyArray", &t2172_0_0_49, &t51_TI, offsetof(t51_SFs, f4), &EmptyCustomAttributesCache};
static FieldInfo* t51_FIs[] =
{
	&t51_f0_FieldInfo,
	&t51_f1_FieldInfo,
	&t51_f2_FieldInfo,
	&t51_f3_FieldInfo,
	&t51_f4_FieldInfo,
	NULL
};
static const int32_t t51_f0_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t51_f0_DefaultValue = 
{
	&t51_f0_FieldInfo, { (char*)&t51_f0_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t51_FDVs[] = 
{
	&t51_f0_DefaultValue,
	NULL
};
extern MethodInfo m10558_MI;
static PropertyInfo t51____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t51_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m10558_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10560_MI;
static PropertyInfo t51____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t51_TI, "System.Collections.ICollection.IsSynchronized", &m10560_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10562_MI;
static PropertyInfo t51____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t51_TI, "System.Collections.ICollection.SyncRoot", &m10562_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10564_MI;
static PropertyInfo t51____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t51_TI, "System.Collections.IList.IsFixedSize", &m10564_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10566_MI;
static PropertyInfo t51____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t51_TI, "System.Collections.IList.IsReadOnly", &m10566_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10568_MI;
extern MethodInfo m10570_MI;
static PropertyInfo t51____System_Collections_IList_Item_PropertyInfo = 
{
	&t51_TI, "System.Collections.IList.Item", &m10568_MI, &m10570_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t51____Capacity_PropertyInfo = 
{
	&t51_TI, "Capacity", &m10622_MI, &m10624_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m1291_MI;
static PropertyInfo t51____Count_PropertyInfo = 
{
	&t51_TI, "Count", &m1291_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t51____Item_PropertyInfo = 
{
	&t51_TI, "Item", &m1292_MI, &m10628_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t51_PIs[] =
{
	&t51____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t51____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t51____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t51____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t51____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t51____System_Collections_IList_Item_PropertyInfo,
	&t51____Capacity_PropertyInfo,
	&t51____Count_PropertyInfo,
	&t51____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1288_GM;
MethodInfo m1288_MI = 
{
	".ctor", (methodPointerType)&m10537_gshared, &t51_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1288_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t51_m10538_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10538_GM;
MethodInfo m10538_MI = 
{
	".ctor", (methodPointerType)&m10539_gshared, &t51_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t51_m10538_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10538_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10540_GM;
MethodInfo m10540_MI = 
{
	".cctor", (methodPointerType)&m10541_gshared, &t51_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10540_GM};
extern Il2CppType t2173_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10542_GM;
MethodInfo m10542_MI = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator", (methodPointerType)&m10543_gshared, &t51_TI, &t2173_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 27, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10542_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t51_m10544_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10544_GM;
MethodInfo m10544_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10545_gshared, &t51_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t51_m10544_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10544_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10546_GM;
MethodInfo m10546_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10547_gshared, &t51_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10546_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t51_m10548_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10548_GM;
MethodInfo m10548_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10549_gshared, &t51_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t51_m10548_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10548_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t51_m10550_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10550_GM;
MethodInfo m10550_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10551_gshared, &t51_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t51_m10550_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10550_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t51_m10552_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10552_GM;
MethodInfo m10552_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10553_gshared, &t51_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t51_m10552_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10552_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t51_m10554_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10554_GM;
MethodInfo m10554_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10555_gshared, &t51_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t51_m10554_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10554_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t51_m10556_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10556_GM;
MethodInfo m10556_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10557_gshared, &t51_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t51_m10556_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10556_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10558_GM;
MethodInfo m10558_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10559_gshared, &t51_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10558_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10560_GM;
MethodInfo m10560_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10561_gshared, &t51_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10560_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10562_GM;
MethodInfo m10562_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10563_gshared, &t51_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10562_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10564_GM;
MethodInfo m10564_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10565_gshared, &t51_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10564_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10566_GM;
MethodInfo m10566_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10567_gshared, &t51_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10566_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t51_m10568_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10568_GM;
MethodInfo m10568_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10569_gshared, &t51_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t51_m10568_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10568_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t51_m10570_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10570_GM;
MethodInfo m10570_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10571_gshared, &t51_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t51_m10570_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10570_GM};
extern Il2CppType t52_0_0_0;
extern Il2CppType t52_0_0_0;
static ParameterInfo t51_m10572_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10572_GM;
MethodInfo m10572_MI = 
{
	"Add", (methodPointerType)&m10573_gshared, &t51_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t51_m10572_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10572_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t51_m10574_ParameterInfos[] = 
{
	{"newCount", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10574_GM;
MethodInfo m10574_MI = 
{
	"GrowIfNeeded", (methodPointerType)&m10575_gshared, &t51_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t51_m10574_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10574_GM};
extern Il2CppType t2174_0_0_0;
extern Il2CppType t2174_0_0_0;
static ParameterInfo t51_m10576_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2174_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10576_GM;
MethodInfo m10576_MI = 
{
	"AddCollection", (methodPointerType)&m10577_gshared, &t51_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t51_m10576_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10576_GM};
extern Il2CppType t2175_0_0_0;
extern Il2CppType t2175_0_0_0;
static ParameterInfo t51_m10578_ParameterInfos[] = 
{
	{"enumerable", 0, 134217728, &EmptyCustomAttributesCache, &t2175_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10578_GM;
MethodInfo m10578_MI = 
{
	"AddEnumerable", (methodPointerType)&m10579_gshared, &t51_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t51_m10578_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10578_GM};
extern Il2CppType t2175_0_0_0;
static ParameterInfo t51_m10580_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2175_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10580_GM;
MethodInfo m10580_MI = 
{
	"AddRange", (methodPointerType)&m10581_gshared, &t51_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t51_m10580_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10580_GM};
extern Il2CppType t2176_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10582_GM;
MethodInfo m10582_MI = 
{
	"AsReadOnly", (methodPointerType)&m10583_gshared, &t51_TI, &t2176_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10582_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10584_GM;
MethodInfo m10584_MI = 
{
	"Clear", (methodPointerType)&m10585_gshared, &t51_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10584_GM};
extern Il2CppType t52_0_0_0;
static ParameterInfo t51_m10586_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10586_GM;
MethodInfo m10586_MI = 
{
	"Contains", (methodPointerType)&m10587_gshared, &t51_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t51_m10586_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10586_GM};
extern Il2CppType t2172_0_0_0;
extern Il2CppType t2172_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t51_m10588_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2172_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10588_GM;
MethodInfo m10588_MI = 
{
	"CopyTo", (methodPointerType)&m10589_gshared, &t51_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t51_m10588_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10588_GM};
extern Il2CppType t2177_0_0_0;
extern Il2CppType t2177_0_0_0;
static ParameterInfo t51_m10590_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2177_0_0_0},
};
extern Il2CppType t52_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10590_GM;
MethodInfo m10590_MI = 
{
	"Find", (methodPointerType)&m10591_gshared, &t51_TI, &t52_0_0_0, RuntimeInvoker_t29_t29, t51_m10590_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10590_GM};
extern Il2CppType t2177_0_0_0;
static ParameterInfo t51_m10592_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2177_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10592_GM;
MethodInfo m10592_MI = 
{
	"CheckMatch", (methodPointerType)&m10593_gshared, &t51_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t51_m10592_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10592_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2177_0_0_0;
static ParameterInfo t51_m10594_ParameterInfos[] = 
{
	{"startIndex", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"count", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"match", 2, 134217728, &EmptyCustomAttributesCache, &t2177_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10594_GM;
MethodInfo m10594_MI = 
{
	"GetIndex", (methodPointerType)&m10595_gshared, &t51_TI, &t44_0_0_0, RuntimeInvoker_t44_t44_t44_t29, t51_m10594_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m10594_GM};
extern Il2CppType t2179_0_0_0;
extern void* RuntimeInvoker_t2179 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10596_GM;
MethodInfo m10596_MI = 
{
	"GetEnumerator", (methodPointerType)&m10596, &t51_TI, &t2179_0_0_0, RuntimeInvoker_t2179, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10596_GM};
extern Il2CppType t52_0_0_0;
static ParameterInfo t51_m10597_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10597_GM;
MethodInfo m10597_MI = 
{
	"IndexOf", (methodPointerType)&m10598_gshared, &t51_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t51_m10597_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10597_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t51_m10599_ParameterInfos[] = 
{
	{"start", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"delta", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10599_GM;
MethodInfo m10599_MI = 
{
	"Shift", (methodPointerType)&m10600_gshared, &t51_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t51_m10599_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10599_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t51_m10601_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10601_GM;
MethodInfo m10601_MI = 
{
	"CheckIndex", (methodPointerType)&m10602_gshared, &t51_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t51_m10601_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10601_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t52_0_0_0;
static ParameterInfo t51_m10603_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10603_GM;
MethodInfo m10603_MI = 
{
	"Insert", (methodPointerType)&m10604_gshared, &t51_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t51_m10603_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10603_GM};
extern Il2CppType t2175_0_0_0;
static ParameterInfo t51_m10605_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2175_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10605_GM;
MethodInfo m10605_MI = 
{
	"CheckCollection", (methodPointerType)&m10606_gshared, &t51_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t51_m10605_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10605_GM};
extern Il2CppType t52_0_0_0;
static ParameterInfo t51_m10607_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10607_GM;
MethodInfo m10607_MI = 
{
	"Remove", (methodPointerType)&m10608_gshared, &t51_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t51_m10607_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10607_GM};
extern Il2CppType t2177_0_0_0;
static ParameterInfo t51_m10609_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2177_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10609_GM;
MethodInfo m10609_MI = 
{
	"RemoveAll", (methodPointerType)&m10610_gshared, &t51_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t51_m10609_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10609_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t51_m1294_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1294_GM;
MethodInfo m1294_MI = 
{
	"RemoveAt", (methodPointerType)&m10611_gshared, &t51_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t51_m1294_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1294_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10612_GM;
MethodInfo m10612_MI = 
{
	"Reverse", (methodPointerType)&m10613_gshared, &t51_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10612_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10614_GM;
MethodInfo m10614_MI = 
{
	"Sort", (methodPointerType)&m10615_gshared, &t51_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10614_GM};
extern Il2CppType t2178_0_0_0;
extern Il2CppType t2178_0_0_0;
static ParameterInfo t51_m10616_ParameterInfos[] = 
{
	{"comparison", 0, 134217728, &EmptyCustomAttributesCache, &t2178_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10616_GM;
MethodInfo m10616_MI = 
{
	"Sort", (methodPointerType)&m10617_gshared, &t51_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t51_m10616_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10616_GM};
extern Il2CppType t2172_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10618_GM;
MethodInfo m10618_MI = 
{
	"ToArray", (methodPointerType)&m10619_gshared, &t51_TI, &t2172_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10618_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10620_GM;
MethodInfo m10620_MI = 
{
	"TrimExcess", (methodPointerType)&m10621_gshared, &t51_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10620_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10622_GM;
MethodInfo m10622_MI = 
{
	"get_Capacity", (methodPointerType)&m10623_gshared, &t51_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10622_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t51_m10624_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10624_GM;
MethodInfo m10624_MI = 
{
	"set_Capacity", (methodPointerType)&m10625_gshared, &t51_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t51_m10624_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10624_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1291_GM;
MethodInfo m1291_MI = 
{
	"get_Count", (methodPointerType)&m10626_gshared, &t51_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1291_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t51_m1292_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t52_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1292_GM;
MethodInfo m1292_MI = 
{
	"get_Item", (methodPointerType)&m10627_gshared, &t51_TI, &t52_0_0_0, RuntimeInvoker_t29_t44, t51_m1292_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1292_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t52_0_0_0;
static ParameterInfo t51_m10628_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10628_GM;
MethodInfo m10628_MI = 
{
	"set_Item", (methodPointerType)&m10629_gshared, &t51_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t51_m10628_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 32, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10628_GM};
static MethodInfo* t51_MIs[] =
{
	&m1288_MI,
	&m10538_MI,
	&m10540_MI,
	&m10542_MI,
	&m10544_MI,
	&m10546_MI,
	&m10548_MI,
	&m10550_MI,
	&m10552_MI,
	&m10554_MI,
	&m10556_MI,
	&m10558_MI,
	&m10560_MI,
	&m10562_MI,
	&m10564_MI,
	&m10566_MI,
	&m10568_MI,
	&m10570_MI,
	&m10572_MI,
	&m10574_MI,
	&m10576_MI,
	&m10578_MI,
	&m10580_MI,
	&m10582_MI,
	&m10584_MI,
	&m10586_MI,
	&m10588_MI,
	&m10590_MI,
	&m10592_MI,
	&m10594_MI,
	&m10596_MI,
	&m10597_MI,
	&m10599_MI,
	&m10601_MI,
	&m10603_MI,
	&m10605_MI,
	&m10607_MI,
	&m10609_MI,
	&m1294_MI,
	&m10612_MI,
	&m10614_MI,
	&m10616_MI,
	&m10618_MI,
	&m10620_MI,
	&m10622_MI,
	&m10624_MI,
	&m1291_MI,
	&m1292_MI,
	&m10628_MI,
	NULL
};
extern MethodInfo m10546_MI;
extern MethodInfo m10544_MI;
extern MethodInfo m10548_MI;
extern MethodInfo m10584_MI;
extern MethodInfo m10550_MI;
extern MethodInfo m10552_MI;
extern MethodInfo m10554_MI;
extern MethodInfo m10556_MI;
extern MethodInfo m10588_MI;
extern MethodInfo m10542_MI;
static MethodInfo* t51_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m10546_MI,
	&m1291_MI,
	&m10560_MI,
	&m10562_MI,
	&m10544_MI,
	&m10564_MI,
	&m10566_MI,
	&m10568_MI,
	&m10570_MI,
	&m10548_MI,
	&m10584_MI,
	&m10550_MI,
	&m10552_MI,
	&m10554_MI,
	&m10556_MI,
	&m1294_MI,
	&m1291_MI,
	&m10558_MI,
	&m10572_MI,
	&m10584_MI,
	&m10586_MI,
	&m10588_MI,
	&m10607_MI,
	&m10542_MI,
	&m10597_MI,
	&m10603_MI,
	&m1294_MI,
	&m1292_MI,
	&m10628_MI,
};
extern TypeInfo t674_TI;
extern TypeInfo t868_TI;
extern TypeInfo t2207_TI;
static TypeInfo* t51_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2174_TI,
	&t2175_TI,
	&t2207_TI,
};
static Il2CppInterfaceOffsetPair t51_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2174_TI, 20},
	{ &t2175_TI, 27},
	{ &t2207_TI, 28},
};
extern TypeInfo t51_TI;
extern TypeInfo t2172_TI;
extern TypeInfo t2179_TI;
extern TypeInfo t52_TI;
extern TypeInfo t2174_TI;
extern TypeInfo t2176_TI;
static Il2CppRGCTXData t51_RGCTXData[37] = 
{
	&t51_TI/* Static Usage */,
	&t2172_TI/* Array Usage */,
	&m10596_MI/* Method Usage */,
	&t2179_TI/* Class Usage */,
	&t52_TI/* Class Usage */,
	&m10572_MI/* Method Usage */,
	&m10586_MI/* Method Usage */,
	&m10597_MI/* Method Usage */,
	&m10601_MI/* Method Usage */,
	&m10603_MI/* Method Usage */,
	&m10607_MI/* Method Usage */,
	&m1292_MI/* Method Usage */,
	&m10628_MI/* Method Usage */,
	&m10574_MI/* Method Usage */,
	&m10622_MI/* Method Usage */,
	&m10624_MI/* Method Usage */,
	&m26615_MI/* Method Usage */,
	&m26616_MI/* Method Usage */,
	&m26617_MI/* Method Usage */,
	&m26618_MI/* Method Usage */,
	&m10605_MI/* Method Usage */,
	&t2174_TI/* Class Usage */,
	&m10576_MI/* Method Usage */,
	&m10578_MI/* Method Usage */,
	&t2176_TI/* Class Usage */,
	&m10811_MI/* Method Usage */,
	&m19980_MI/* Method Usage */,
	&m10592_MI/* Method Usage */,
	&m10594_MI/* Method Usage */,
	&m10886_MI/* Method Usage */,
	&m10805_MI/* Method Usage */,
	&m10599_MI/* Method Usage */,
	&m1294_MI/* Method Usage */,
	&m10892_MI/* Method Usage */,
	&m19982_MI/* Method Usage */,
	&m19990_MI/* Method Usage */,
	&m19978_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t51_0_0_0;
extern Il2CppType t51_1_0_0;
struct t51;
extern Il2CppGenericClass t51_GC;
extern CustomAttributesCache t1261__CustomAttributeCache;
TypeInfo t51_TI = 
{
	&g_mscorlib_dll_Image, NULL, "List`1", "System.Collections.Generic", t51_MIs, t51_PIs, t51_FIs, NULL, &t29_TI, NULL, NULL, &t51_TI, t51_ITIs, t51_VT, &t1261__CustomAttributeCache, &t51_TI, &t51_0_0_0, &t51_1_0_0, t51_IOs, &t51_GC, NULL, t51_FDVs, NULL, t51_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t51), 0, -1, sizeof(t51_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, true, false, false, 49, 9, 5, 0, 0, 33, 6, 6};
#include "t294.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t294_TI;
#include "t294MD.h"

#include "t2185.h"
#include "t2184.h"
#include "t2180.h"
#include "t2195.h"
extern TypeInfo t2185_TI;
extern TypeInfo t2182_TI;
extern TypeInfo t2183_TI;
extern TypeInfo t346_TI;
extern TypeInfo t2184_TI;
extern TypeInfo t2180_TI;
extern TypeInfo t2195_TI;
#include "t2184MD.h"
#include "t2180MD.h"
#include "t2185MD.h"
#include "t2195MD.h"
extern MethodInfo m10627_MI;
extern MethodInfo m10629_MI;
extern MethodInfo m19788_MI;
extern MethodInfo m10602_MI;
extern MethodInfo m10630_MI;
extern MethodInfo m10573_MI;
extern MethodInfo m10587_MI;
extern MethodInfo m10598_MI;
extern MethodInfo m10604_MI;
extern MethodInfo m10608_MI;
extern MethodInfo m10575_MI;
extern MethodInfo m10623_MI;
extern MethodInfo m10625_MI;
extern MethodInfo m26254_MI;
extern MethodInfo m26259_MI;
extern MethodInfo m26261_MI;
extern MethodInfo m26253_MI;
extern MethodInfo m10606_MI;
extern MethodInfo m10577_MI;
extern MethodInfo m10579_MI;
extern MethodInfo m10637_MI;
extern MethodInfo m10120_MI;
extern MethodInfo m10593_MI;
extern MethodInfo m10595_MI;
extern MethodInfo m10742_MI;
extern MethodInfo m10631_MI;
extern MethodInfo m10600_MI;
extern MethodInfo m10611_MI;
extern MethodInfo m10748_MI;
extern MethodInfo m19857_MI;
extern MethodInfo m19964_MI;


extern MethodInfo m10537_MI;
 void m10537_gshared (t294 * __this, MethodInfo* method)
{
	{
		m1331(__this, &m1331_MI);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->f1 = (((t294_SFs*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->f4);
		return;
	}
}
extern MethodInfo m10539_MI;
 void m10539_gshared (t294 * __this, int32_t p0, MethodInfo* method)
{
	{
		m1331(__this, &m1331_MI);
		if ((((int32_t)p0) >= ((int32_t)0)))
		{
			goto IL_0015;
		}
	}
	{
		t915 * L_0 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_0, (t7*) &_stringLiteral1163, &m3975_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0015:
	{
		__this->f1 = ((t316*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), p0));
		return;
	}
}
extern MethodInfo m10541_MI;
 void m10541_gshared (t29 * __this, MethodInfo* method)
{
	{
		((t294_SFs*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->f4 = ((t316*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), 0));
		return;
	}
}
extern MethodInfo m10543_MI;
 t29* m10543_gshared (t294 * __this, MethodInfo* method)
{
	{
		t2185  L_0 = (( t2185  (*) (t294 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(__this, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		t2185  L_1 = L_0;
		t29 * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), &L_1);
		return (t29*)L_2;
	}
}
extern MethodInfo m10545_MI;
 void m10545_gshared (t294 * __this, t20 * p0, int32_t p1, MethodInfo* method)
{
	{
		t316* L_0 = (__this->f1);
		int32_t L_1 = (__this->f2);
		m5952(NULL, (t20 *)(t20 *)L_0, 0, p0, p1, L_1, &m5952_MI);
		return;
	}
}
extern MethodInfo m10547_MI;
 t29 * m10547_gshared (t294 * __this, MethodInfo* method)
{
	{
		t2185  L_0 = (( t2185  (*) (t294 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(__this, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		t2185  L_1 = L_0;
		t29 * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), &L_1);
		return (t29 *)L_2;
	}
}
extern MethodInfo m10549_MI;
 int32_t m10549_gshared (t294 * __this, t29 * p0, MethodInfo* method)
{
	int32_t V_0 = 0;
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			VirtActionInvoker1< t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), __this, ((t29 *)Castclass(p0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4))));
			int32_t L_0 = (__this->f2);
			V_0 = ((int32_t)(L_0-1));
			// IL_0015: leave.s IL_002a
			goto IL_002a;
		}

IL_0017:
		{
			// IL_0017: leave.s IL_001f
			goto IL_001f;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t585_TI, e.ex->object.klass))
			goto IL_0019;
		if(il2cpp_codegen_class_is_assignable_from (&t1649_TI, e.ex->object.klass))
			goto IL_001c;
		throw e;
	}

IL_0019:
	{ // begin catch(System.NullReferenceException)
		// IL_001a: leave.s IL_001f
		goto IL_001f;
	} // end catch (depth: 1)

IL_001c:
	{ // begin catch(System.InvalidCastException)
		// IL_001d: leave.s IL_001f
		goto IL_001f;
	} // end catch (depth: 1)

IL_001f:
	{
		t305 * L_1 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_1, (t7*) &_stringLiteral1176, &m1935_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_002a:
	{
		return V_0;
	}
}
extern MethodInfo m10551_MI;
 bool m10551_gshared (t294 * __this, t29 * p0, MethodInfo* method)
{
	bool V_0 = false;
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			bool L_0 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), __this, ((t29 *)Castclass(p0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4))));
			V_0 = L_0;
			// IL_000d: leave.s IL_0019
			goto IL_0019;
		}

IL_000f:
		{
			// IL_000f: leave.s IL_0017
			goto IL_0017;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t585_TI, e.ex->object.klass))
			goto IL_0011;
		if(il2cpp_codegen_class_is_assignable_from (&t1649_TI, e.ex->object.klass))
			goto IL_0014;
		throw e;
	}

IL_0011:
	{ // begin catch(System.NullReferenceException)
		// IL_0012: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0014:
	{ // begin catch(System.InvalidCastException)
		// IL_0015: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0017:
	{
		return 0;
	}

IL_0019:
	{
		return V_0;
	}
}
extern MethodInfo m10553_MI;
 int32_t m10553_gshared (t294 * __this, t29 * p0, MethodInfo* method)
{
	int32_t V_0 = 0;
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = (int32_t)VirtFuncInvoker1< int32_t, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), __this, ((t29 *)Castclass(p0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4))));
			V_0 = L_0;
			// IL_000d: leave.s IL_0019
			goto IL_0019;
		}

IL_000f:
		{
			// IL_000f: leave.s IL_0017
			goto IL_0017;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t585_TI, e.ex->object.klass))
			goto IL_0011;
		if(il2cpp_codegen_class_is_assignable_from (&t1649_TI, e.ex->object.klass))
			goto IL_0014;
		throw e;
	}

IL_0011:
	{ // begin catch(System.NullReferenceException)
		// IL_0012: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0014:
	{ // begin catch(System.InvalidCastException)
		// IL_0015: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0017:
	{
		return (-1);
	}

IL_0019:
	{
		return V_0;
	}
}
extern MethodInfo m10555_MI;
 void m10555_gshared (t294 * __this, int32_t p0, t29 * p1, MethodInfo* method)
{
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		(( void (*) (t294 * __this, int32_t p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(__this, p0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			VirtActionInvoker2< int32_t, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), __this, p0, ((t29 *)Castclass(p1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4))));
			// IL_0014: leave.s IL_0029
			goto IL_0029;
		}

IL_0016:
		{
			// IL_0016: leave.s IL_001e
			goto IL_001e;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t585_TI, e.ex->object.klass))
			goto IL_0018;
		if(il2cpp_codegen_class_is_assignable_from (&t1649_TI, e.ex->object.klass))
			goto IL_001b;
		throw e;
	}

IL_0018:
	{ // begin catch(System.NullReferenceException)
		// IL_0019: leave.s IL_001e
		goto IL_001e;
	} // end catch (depth: 1)

IL_001b:
	{ // begin catch(System.InvalidCastException)
		// IL_001c: leave.s IL_001e
		goto IL_001e;
	} // end catch (depth: 1)

IL_001e:
	{
		t305 * L_0 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_0, (t7*) &_stringLiteral1176, &m1935_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0029:
	{
		return;
	}
}
extern MethodInfo m10557_MI;
 void m10557_gshared (t294 * __this, t29 * p0, MethodInfo* method)
{
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			VirtFuncInvoker1< bool, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10), __this, ((t29 *)Castclass(p0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4))));
			// IL_000d: leave.s IL_0017
			goto IL_0017;
		}

IL_000f:
		{
			// IL_000f: leave.s IL_0017
			goto IL_0017;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t585_TI, e.ex->object.klass))
			goto IL_0011;
		if(il2cpp_codegen_class_is_assignable_from (&t1649_TI, e.ex->object.klass))
			goto IL_0014;
		throw e;
	}

IL_0011:
	{ // begin catch(System.NullReferenceException)
		// IL_0012: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0014:
	{ // begin catch(System.InvalidCastException)
		// IL_0015: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0017:
	{
		return;
	}
}
extern MethodInfo m10559_MI;
 bool m10559_gshared (t294 * __this, MethodInfo* method)
{
	{
		return 0;
	}
}
extern MethodInfo m10561_MI;
 bool m10561_gshared (t294 * __this, MethodInfo* method)
{
	{
		return 0;
	}
}
extern MethodInfo m10563_MI;
 t29 * m10563_gshared (t294 * __this, MethodInfo* method)
{
	{
		return __this;
	}
}
extern MethodInfo m10565_MI;
 bool m10565_gshared (t294 * __this, MethodInfo* method)
{
	{
		return 0;
	}
}
extern MethodInfo m10567_MI;
 bool m10567_gshared (t294 * __this, MethodInfo* method)
{
	{
		return 0;
	}
}
extern MethodInfo m10569_MI;
 t29 * m10569_gshared (t294 * __this, int32_t p0, MethodInfo* method)
{
	{
		t29 * L_0 = (t29 *)VirtFuncInvoker1< t29 *, int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11), __this, p0);
		t29 * L_1 = L_0;
		return ((t29 *)L_1);
	}
}
extern MethodInfo m10571_MI;
 void m10571_gshared (t294 * __this, int32_t p0, t29 * p1, MethodInfo* method)
{
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			VirtActionInvoker2< int32_t, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), __this, p0, ((t29 *)Castclass(p1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4))));
			// IL_000d: leave.s IL_0022
			goto IL_0022;
		}

IL_000f:
		{
			// IL_000f: leave.s IL_0017
			goto IL_0017;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t585_TI, e.ex->object.klass))
			goto IL_0011;
		if(il2cpp_codegen_class_is_assignable_from (&t1649_TI, e.ex->object.klass))
			goto IL_0014;
		throw e;
	}

IL_0011:
	{ // begin catch(System.NullReferenceException)
		// IL_0012: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0014:
	{ // begin catch(System.InvalidCastException)
		// IL_0015: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0017:
	{
		t305 * L_0 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_0, (t7*) &_stringLiteral401, &m1935_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0022:
	{
		return;
	}
}
 void m10573_gshared (t294 * __this, t29 * p0, MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (__this->f2);
		t316* L_1 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)(((int32_t)(((t20 *)L_1)->max_length))))))
		{
			goto IL_0017;
		}
	}
	{
		(( void (*) (t294 * __this, int32_t p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)(__this, 1, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
	}

IL_0017:
	{
		t316* L_2 = (__this->f1);
		int32_t L_3 = (__this->f2);
		int32_t L_4 = L_3;
		V_0 = L_4;
		__this->f2 = ((int32_t)(L_4+1));
		*((t29 **)(t29 **)SZArrayLdElema(L_2, V_0)) = (t29 *)p0;
		int32_t L_5 = (__this->f3);
		__this->f3 = ((int32_t)(L_5+1));
		return;
	}
}
 void m10575_gshared (t294 * __this, int32_t p0, MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (__this->f2);
		V_0 = ((int32_t)(L_0+p0));
		t316* L_1 = (__this->f1);
		if ((((int32_t)V_0) <= ((int32_t)(((int32_t)(((t20 *)L_1)->max_length))))))
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_2 = (( int32_t (*) (t294 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)(__this, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		int32_t L_3 = m5139(NULL, ((int32_t)((int32_t)L_2*(int32_t)2)), 4, &m5139_MI);
		int32_t L_4 = m5139(NULL, L_3, V_0, &m5139_MI);
		(( void (*) (t294 * __this, int32_t p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)(__this, L_4, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
	}

IL_002e:
	{
		return;
	}
}
 void m10577_gshared (t294 * __this, t29* p0, MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16), p0);
		V_0 = L_0;
		if (V_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		(( void (*) (t294 * __this, int32_t p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)(__this, V_0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		t316* L_1 = (__this->f1);
		int32_t L_2 = (__this->f2);
		InterfaceActionInvoker2< t316*, int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17), p0, L_1, L_2);
		int32_t L_3 = (__this->f2);
		__this->f2 = ((int32_t)(L_3+V_0));
		return;
	}
}
 void m10579_gshared (t294 * __this, t29* p0, MethodInfo* method)
{
	t29 * V_0 = {0};
	t29* V_1 = {0};
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		t29* L_0 = (t29*)InterfaceFuncInvoker0< t29* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18), p0);
		V_1 = L_0;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0017;
		}

IL_0009:
		{
			t29 * L_1 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19), V_1);
			V_0 = L_1;
			VirtActionInvoker1< t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), __this, V_0);
		}

IL_0017:
		{
			bool L_2 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m3972_MI, V_1);
			if (L_2)
			{
				goto IL_0009;
			}
		}

IL_001f:
		{
			// IL_001f: leave.s IL_002c
			leaveInstructions[0] = 0x2C; // 1
			THROW_SENTINEL(IL_002c);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_0021;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_0021;
	}

IL_0021:
	{ // begin finally (depth: 1)
		{
			if (V_1)
			{
				goto IL_0025;
			}
		}

IL_0024:
		{
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x2C:
					goto IL_002c;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}

IL_0025:
		{
			InterfaceActionInvoker0::Invoke(&m1428_MI, V_1);
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x2C:
					goto IL_002c;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}
	} // end finally (depth: 1)

IL_002c:
	{
		return;
	}
}
extern MethodInfo m10581_MI;
 void m10581_gshared (t294 * __this, t29* p0, MethodInfo* method)
{
	t29* V_0 = {0};
	{
		(( void (*) (t294 * __this, t29* p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)->method)(__this, p0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20));
		V_0 = ((t29*)IsInst(p0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21)));
		if (!V_0)
		{
			goto IL_001a;
		}
	}
	{
		(( void (*) (t294 * __this, t29* p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22)->method)(__this, V_0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22));
		goto IL_0021;
	}

IL_001a:
	{
		(( void (*) (t294 * __this, t29* p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23)->method)(__this, p0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
	}

IL_0021:
	{
		int32_t L_0 = (__this->f3);
		__this->f3 = ((int32_t)(L_0+1));
		return;
	}
}
extern MethodInfo m10583_MI;
 t2184 * m10583_gshared (t294 * __this, MethodInfo* method)
{
	{
		t2184 * L_0 = (t2184 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		(( void (*) (t2184 * __this, t29* p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25)->method)(L_0, __this, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25));
		return L_0;
	}
}
extern MethodInfo m10585_MI;
 void m10585_gshared (t294 * __this, MethodInfo* method)
{
	{
		t316* L_0 = (__this->f1);
		t316* L_1 = (__this->f1);
		m5112(NULL, (t20 *)(t20 *)L_0, 0, (((int32_t)(((t20 *)L_1)->max_length))), &m5112_MI);
		__this->f2 = 0;
		int32_t L_2 = (__this->f3);
		__this->f3 = ((int32_t)(L_2+1));
		return;
	}
}
 bool m10587_gshared (t294 * __this, t29 * p0, MethodInfo* method)
{
	{
		t316* L_0 = (__this->f1);
		int32_t L_1 = (__this->f2);
		int32_t L_2 = (( int32_t (*) (t29 * __this, t316* p0, t29 * p1, int32_t p2, int32_t p3, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26)->method)(NULL, L_0, p0, 0, L_1, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
		return ((((int32_t)((((int32_t)L_2) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
extern MethodInfo m10589_MI;
 void m10589_gshared (t294 * __this, t316* p0, int32_t p1, MethodInfo* method)
{
	{
		t316* L_0 = (__this->f1);
		int32_t L_1 = (__this->f2);
		m5952(NULL, (t20 *)(t20 *)L_0, 0, (t20 *)(t20 *)p0, p1, L_1, &m5952_MI);
		return;
	}
}
extern MethodInfo m10591_MI;
 t29 * m10591_gshared (t294 * __this, t2180 * p0, MethodInfo* method)
{
	int32_t V_0 = 0;
	t29 * V_1 = {0};
	t29 * G_B3_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t294_TI));
		(( void (*) (t29 * __this, t2180 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27)->method)(NULL, p0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		int32_t L_0 = (__this->f2);
		int32_t L_1 = (( int32_t (*) (t294 * __this, int32_t p0, int32_t p1, t2180 * p2, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28)->method)(__this, 0, L_0, p0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28));
		V_0 = L_1;
		if ((((int32_t)V_0) == ((int32_t)(-1))))
		{
			goto IL_0027;
		}
	}
	{
		t316* L_2 = (__this->f1);
		int32_t L_3 = V_0;
		G_B3_0 = (*(t29 **)(t29 **)SZArrayLdElema(L_2, L_3));
		goto IL_0030;
	}

IL_0027:
	{
		Initobj (&t29_TI, (&V_1));
		G_B3_0 = V_1;
	}

IL_0030:
	{
		return G_B3_0;
	}
}
 void m10593_gshared (t29 * __this, t2180 * p0, MethodInfo* method)
{
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1033, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		return;
	}
}
 int32_t m10595_gshared (t294 * __this, int32_t p0, int32_t p1, t2180 * p2, MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = ((int32_t)(p0+p1));
		V_1 = p0;
		goto IL_0022;
	}

IL_0008:
	{
		t316* L_0 = (__this->f1);
		int32_t L_1 = V_1;
		bool L_2 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29), p2, (*(t29 **)(t29 **)SZArrayLdElema(L_0, L_1)));
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		return V_1;
	}

IL_001e:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0022:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0008;
		}
	}
	{
		return (-1);
	}
}
 t2185  m10630 (t294 * __this, MethodInfo* method){
	{
		t2185  L_0 = {0};
		m10631(&L_0, __this, &m10631_MI);
		return L_0;
	}
}
 int32_t m10598_gshared (t294 * __this, t29 * p0, MethodInfo* method)
{
	{
		t316* L_0 = (__this->f1);
		int32_t L_1 = (__this->f2);
		int32_t L_2 = (( int32_t (*) (t29 * __this, t316* p0, t29 * p1, int32_t p2, int32_t p3, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26)->method)(NULL, L_0, p0, 0, L_1, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
		return L_2;
	}
}
 void m10600_gshared (t294 * __this, int32_t p0, int32_t p1, MethodInfo* method)
{
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_000b;
		}
	}
	{
		p0 = ((int32_t)(p0-p1));
	}

IL_000b:
	{
		int32_t L_0 = (__this->f2);
		if ((((int32_t)p0) >= ((int32_t)L_0)))
		{
			goto IL_0031;
		}
	}
	{
		t316* L_1 = (__this->f1);
		t316* L_2 = (__this->f1);
		int32_t L_3 = (__this->f2);
		m5952(NULL, (t20 *)(t20 *)L_1, p0, (t20 *)(t20 *)L_2, ((int32_t)(p0+p1)), ((int32_t)(L_3-p0)), &m5952_MI);
	}

IL_0031:
	{
		int32_t L_4 = (__this->f2);
		__this->f2 = ((int32_t)(L_4+p1));
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_0056;
		}
	}
	{
		t316* L_5 = (__this->f1);
		int32_t L_6 = (__this->f2);
		m5112(NULL, (t20 *)(t20 *)L_5, L_6, ((-p1)), &m5112_MI);
	}

IL_0056:
	{
		return;
	}
}
 void m10602_gshared (t294 * __this, int32_t p0, MethodInfo* method)
{
	{
		if ((((int32_t)p0) < ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_0 = (__this->f2);
		if ((((uint32_t)p0) <= ((uint32_t)L_0)))
		{
			goto IL_0018;
		}
	}

IL_000d:
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0018:
	{
		return;
	}
}
 void m10604_gshared (t294 * __this, int32_t p0, t29 * p1, MethodInfo* method)
{
	{
		(( void (*) (t294 * __this, int32_t p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(__this, p0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		int32_t L_0 = (__this->f2);
		t316* L_1 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)(((int32_t)(((t20 *)L_1)->max_length))))))
		{
			goto IL_001e;
		}
	}
	{
		(( void (*) (t294 * __this, int32_t p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)(__this, 1, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
	}

IL_001e:
	{
		(( void (*) (t294 * __this, int32_t p0, int32_t p1, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31)->method)(__this, p0, 1, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31));
		t316* L_2 = (__this->f1);
		*((t29 **)(t29 **)SZArrayLdElema(L_2, p0)) = (t29 *)p1;
		int32_t L_3 = (__this->f3);
		__this->f3 = ((int32_t)(L_3+1));
		return;
	}
}
 void m10606_gshared (t294 * __this, t29* p0, MethodInfo* method)
{
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1177, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		return;
	}
}
 bool m10608_gshared (t294 * __this, t29 * p0, MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker1< int32_t, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), __this, p0);
		V_0 = L_0;
		if ((((int32_t)V_0) == ((int32_t)(-1))))
		{
			goto IL_0013;
		}
	}
	{
		VirtActionInvoker1< int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32), __this, V_0);
	}

IL_0013:
	{
		return ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
extern MethodInfo m10610_MI;
 int32_t m10610_gshared (t294 * __this, t2180 * p0, MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t294_TI));
		(( void (*) (t29 * __this, t2180 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27)->method)(NULL, p0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		V_0 = 0;
		V_1 = 0;
		V_0 = 0;
		goto IL_0028;
	}

IL_000e:
	{
		t316* L_0 = (__this->f1);
		int32_t L_1 = V_0;
		bool L_2 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29), p0, (*(t29 **)(t29 **)SZArrayLdElema(L_0, L_1)));
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		goto IL_0031;
	}

IL_0024:
	{
		V_0 = ((int32_t)(V_0+1));
	}

IL_0028:
	{
		int32_t L_3 = (__this->f2);
		if ((((int32_t)V_0) < ((int32_t)L_3)))
		{
			goto IL_000e;
		}
	}

IL_0031:
	{
		int32_t L_4 = (__this->f2);
		if ((((uint32_t)V_0) != ((uint32_t)L_4)))
		{
			goto IL_003c;
		}
	}
	{
		return 0;
	}

IL_003c:
	{
		int32_t L_5 = (__this->f3);
		__this->f3 = ((int32_t)(L_5+1));
		V_1 = ((int32_t)(V_0+1));
		goto IL_0084;
	}

IL_0050:
	{
		t316* L_6 = (__this->f1);
		int32_t L_7 = V_1;
		bool L_8 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29), p0, (*(t29 **)(t29 **)SZArrayLdElema(L_6, L_7)));
		if (L_8)
		{
			goto IL_0080;
		}
	}
	{
		t316* L_9 = (__this->f1);
		int32_t L_10 = V_0;
		V_0 = ((int32_t)(L_10+1));
		t316* L_11 = (__this->f1);
		int32_t L_12 = V_1;
		*((t29 **)(t29 **)SZArrayLdElema(L_9, L_10)) = (t29 *)(*(t29 **)(t29 **)SZArrayLdElema(L_11, L_12));
	}

IL_0080:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0084:
	{
		int32_t L_13 = (__this->f2);
		if ((((int32_t)V_1) < ((int32_t)L_13)))
		{
			goto IL_0050;
		}
	}
	{
		if ((((int32_t)((int32_t)(V_1-V_0))) <= ((int32_t)0)))
		{
			goto IL_00a2;
		}
	}
	{
		t316* L_14 = (__this->f1);
		m5112(NULL, (t20 *)(t20 *)L_14, V_0, ((int32_t)(V_1-V_0)), &m5112_MI);
	}

IL_00a2:
	{
		__this->f2 = V_0;
		return ((int32_t)(V_1-V_0));
	}
}
 void m10611_gshared (t294 * __this, int32_t p0, MethodInfo* method)
{
	{
		if ((((int32_t)p0) < ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_0 = (__this->f2);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0018;
		}
	}

IL_000d:
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0018:
	{
		(( void (*) (t294 * __this, int32_t p0, int32_t p1, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31)->method)(__this, p0, (-1), IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31));
		t316* L_2 = (__this->f1);
		int32_t L_3 = (__this->f2);
		m5112(NULL, (t20 *)(t20 *)L_2, L_3, 1, &m5112_MI);
		int32_t L_4 = (__this->f3);
		__this->f3 = ((int32_t)(L_4+1));
		return;
	}
}
extern MethodInfo m10613_MI;
 void m10613_gshared (t294 * __this, MethodInfo* method)
{
	{
		t316* L_0 = (__this->f1);
		int32_t L_1 = (__this->f2);
		m5171(NULL, (t20 *)(t20 *)L_0, 0, L_1, &m5171_MI);
		int32_t L_2 = (__this->f3);
		__this->f3 = ((int32_t)(L_2+1));
		return;
	}
}
extern MethodInfo m10615_MI;
 void m10615_gshared (t294 * __this, MethodInfo* method)
{
	{
		t316* L_0 = (__this->f1);
		int32_t L_1 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t2195_TI));
		t2195 * L_2 = (( t2195 * (*) (t29 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 33)->method)(NULL, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 33));
		(( void (*) (t29 * __this, t316* p0, int32_t p1, int32_t p2, t29* p3, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34)->method)(NULL, L_0, 0, L_1, L_2, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34));
		int32_t L_3 = (__this->f3);
		__this->f3 = ((int32_t)(L_3+1));
		return;
	}
}
extern MethodInfo m10617_MI;
 void m10617_gshared (t294 * __this, t2181 * p0, MethodInfo* method)
{
	{
		t316* L_0 = (__this->f1);
		int32_t L_1 = (__this->f2);
		(( void (*) (t29 * __this, t316* p0, int32_t p1, t2181 * p2, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35)->method)(NULL, L_0, L_1, p0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35));
		int32_t L_2 = (__this->f3);
		__this->f3 = ((int32_t)(L_2+1));
		return;
	}
}
extern MethodInfo m10619_MI;
 t316* m10619_gshared (t294 * __this, MethodInfo* method)
{
	t316* V_0 = {0};
	{
		int32_t L_0 = (__this->f2);
		V_0 = ((t316*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), L_0));
		t316* L_1 = (__this->f1);
		int32_t L_2 = (__this->f2);
		m5951(NULL, (t20 *)(t20 *)L_1, (t20 *)(t20 *)V_0, L_2, &m5951_MI);
		return V_0;
	}
}
extern MethodInfo m10621_MI;
 void m10621_gshared (t294 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->f2);
		(( void (*) (t294 * __this, int32_t p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)(__this, L_0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		return;
	}
}
 int32_t m10623_gshared (t294 * __this, MethodInfo* method)
{
	{
		t316* L_0 = (__this->f1);
		return (((int32_t)(((t20 *)L_0)->max_length)));
	}
}
 void m10625_gshared (t294 * __this, int32_t p0, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->f2);
		if ((((uint32_t)p0) >= ((uint32_t)L_0)))
		{
			goto IL_000f;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m4198(L_1, &m4198_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_000f:
	{
		t316** L_2 = &(__this->f1);
		(( void (*) (t29 * __this, t316** p0, int32_t p1, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36)->method)(NULL, L_2, p0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36));
		return;
	}
}
extern MethodInfo m10626_MI;
 int32_t m10626_gshared (t294 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->f2);
		return L_0;
	}
}
 t29 * m10627_gshared (t294 * __this, int32_t p0, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->f2);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		t316* L_2 = (__this->f1);
		int32_t L_3 = p0;
		return (*(t29 **)(t29 **)SZArrayLdElema(L_2, L_3));
	}
}
 void m10629_gshared (t294 * __this, int32_t p0, t29 * p1, MethodInfo* method)
{
	{
		(( void (*) (t294 * __this, int32_t p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(__this, p0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		int32_t L_0 = (__this->f2);
		if ((((uint32_t)p0) != ((uint32_t)L_0)))
		{
			goto IL_001b;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_001b:
	{
		t316* L_2 = (__this->f1);
		*((t29 **)(t29 **)SZArrayLdElema(L_2, p0)) = (t29 *)p1;
		return;
	}
}
// Metadata Definition System.Collections.Generic.List`1<System.Object>
extern Il2CppType t44_0_0_32849;
FieldInfo t294_f0_FieldInfo = 
{
	"DefaultCapacity", &t44_0_0_32849, &t294_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t316_0_0_1;
FieldInfo t294_f1_FieldInfo = 
{
	"_items", &t316_0_0_1, &t294_TI, offsetof(t294, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t294_f2_FieldInfo = 
{
	"_size", &t44_0_0_1, &t294_TI, offsetof(t294, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t294_f3_FieldInfo = 
{
	"_version", &t44_0_0_1, &t294_TI, offsetof(t294, f3), &EmptyCustomAttributesCache};
extern Il2CppType t316_0_0_49;
FieldInfo t294_f4_FieldInfo = 
{
	"EmptyArray", &t316_0_0_49, &t294_TI, offsetof(t294_SFs, f4), &EmptyCustomAttributesCache};
static FieldInfo* t294_FIs[] =
{
	&t294_f0_FieldInfo,
	&t294_f1_FieldInfo,
	&t294_f2_FieldInfo,
	&t294_f3_FieldInfo,
	&t294_f4_FieldInfo,
	NULL
};
static const int32_t t294_f0_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t294_f0_DefaultValue = 
{
	&t294_f0_FieldInfo, { (char*)&t294_f0_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t294_FDVs[] = 
{
	&t294_f0_DefaultValue,
	NULL
};
static PropertyInfo t294____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t294_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m10559_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t294____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t294_TI, "System.Collections.ICollection.IsSynchronized", &m10561_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t294____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t294_TI, "System.Collections.ICollection.SyncRoot", &m10563_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t294____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t294_TI, "System.Collections.IList.IsFixedSize", &m10565_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t294____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t294_TI, "System.Collections.IList.IsReadOnly", &m10567_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t294____System_Collections_IList_Item_PropertyInfo = 
{
	&t294_TI, "System.Collections.IList.Item", &m10569_MI, &m10571_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t294____Capacity_PropertyInfo = 
{
	&t294_TI, "Capacity", &m10623_MI, &m10625_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t294____Count_PropertyInfo = 
{
	&t294_TI, "Count", &m10626_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t294____Item_PropertyInfo = 
{
	&t294_TI, "Item", &m10627_MI, &m10629_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t294_PIs[] =
{
	&t294____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t294____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t294____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t294____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t294____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t294____System_Collections_IList_Item_PropertyInfo,
	&t294____Capacity_PropertyInfo,
	&t294____Count_PropertyInfo,
	&t294____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10537_GM;
MethodInfo m10537_MI = 
{
	".ctor", (methodPointerType)&m10537_gshared, &t294_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10537_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t294_m10539_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10539_GM;
MethodInfo m10539_MI = 
{
	".ctor", (methodPointerType)&m10539_gshared, &t294_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t294_m10539_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10539_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10541_GM;
MethodInfo m10541_MI = 
{
	".cctor", (methodPointerType)&m10541_gshared, &t294_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10541_GM};
extern Il2CppType t346_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10543_GM;
MethodInfo m10543_MI = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator", (methodPointerType)&m10543_gshared, &t294_TI, &t346_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 27, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10543_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t294_m10545_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10545_GM;
MethodInfo m10545_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10545_gshared, &t294_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t294_m10545_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10545_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10547_GM;
MethodInfo m10547_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10547_gshared, &t294_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10547_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t294_m10549_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10549_GM;
MethodInfo m10549_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10549_gshared, &t294_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t294_m10549_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10549_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t294_m10551_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10551_GM;
MethodInfo m10551_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10551_gshared, &t294_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t294_m10551_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10551_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t294_m10553_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10553_GM;
MethodInfo m10553_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10553_gshared, &t294_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t294_m10553_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10553_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t294_m10555_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10555_GM;
MethodInfo m10555_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10555_gshared, &t294_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t294_m10555_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10555_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t294_m10557_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10557_GM;
MethodInfo m10557_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10557_gshared, &t294_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t294_m10557_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10557_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10559_GM;
MethodInfo m10559_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10559_gshared, &t294_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10559_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10561_GM;
MethodInfo m10561_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10561_gshared, &t294_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10561_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10563_GM;
MethodInfo m10563_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10563_gshared, &t294_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10563_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10565_GM;
MethodInfo m10565_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10565_gshared, &t294_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10565_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10567_GM;
MethodInfo m10567_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10567_gshared, &t294_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10567_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t294_m10569_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10569_GM;
MethodInfo m10569_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10569_gshared, &t294_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t294_m10569_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10569_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t294_m10571_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10571_GM;
MethodInfo m10571_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10571_gshared, &t294_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t294_m10571_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10571_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t294_m10573_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10573_GM;
MethodInfo m10573_MI = 
{
	"Add", (methodPointerType)&m10573_gshared, &t294_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t294_m10573_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10573_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t294_m10575_ParameterInfos[] = 
{
	{"newCount", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10575_GM;
MethodInfo m10575_MI = 
{
	"GrowIfNeeded", (methodPointerType)&m10575_gshared, &t294_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t294_m10575_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10575_GM};
extern Il2CppType t2182_0_0_0;
extern Il2CppType t2182_0_0_0;
static ParameterInfo t294_m10577_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2182_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10577_GM;
MethodInfo m10577_MI = 
{
	"AddCollection", (methodPointerType)&m10577_gshared, &t294_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t294_m10577_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10577_GM};
extern Il2CppType t2183_0_0_0;
extern Il2CppType t2183_0_0_0;
static ParameterInfo t294_m10579_ParameterInfos[] = 
{
	{"enumerable", 0, 134217728, &EmptyCustomAttributesCache, &t2183_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10579_GM;
MethodInfo m10579_MI = 
{
	"AddEnumerable", (methodPointerType)&m10579_gshared, &t294_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t294_m10579_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10579_GM};
extern Il2CppType t2183_0_0_0;
static ParameterInfo t294_m10581_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2183_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10581_GM;
MethodInfo m10581_MI = 
{
	"AddRange", (methodPointerType)&m10581_gshared, &t294_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t294_m10581_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10581_GM};
extern Il2CppType t2184_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10583_GM;
MethodInfo m10583_MI = 
{
	"AsReadOnly", (methodPointerType)&m10583_gshared, &t294_TI, &t2184_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10583_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10585_GM;
MethodInfo m10585_MI = 
{
	"Clear", (methodPointerType)&m10585_gshared, &t294_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10585_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t294_m10587_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10587_GM;
MethodInfo m10587_MI = 
{
	"Contains", (methodPointerType)&m10587_gshared, &t294_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t294_m10587_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10587_GM};
extern Il2CppType t316_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t294_m10589_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10589_GM;
MethodInfo m10589_MI = 
{
	"CopyTo", (methodPointerType)&m10589_gshared, &t294_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t294_m10589_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10589_GM};
extern Il2CppType t2180_0_0_0;
extern Il2CppType t2180_0_0_0;
static ParameterInfo t294_m10591_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2180_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10591_GM;
MethodInfo m10591_MI = 
{
	"Find", (methodPointerType)&m10591_gshared, &t294_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t294_m10591_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10591_GM};
extern Il2CppType t2180_0_0_0;
static ParameterInfo t294_m10593_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2180_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10593_GM;
MethodInfo m10593_MI = 
{
	"CheckMatch", (methodPointerType)&m10593_gshared, &t294_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t294_m10593_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10593_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2180_0_0_0;
static ParameterInfo t294_m10595_ParameterInfos[] = 
{
	{"startIndex", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"count", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"match", 2, 134217728, &EmptyCustomAttributesCache, &t2180_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10595_GM;
MethodInfo m10595_MI = 
{
	"GetIndex", (methodPointerType)&m10595_gshared, &t294_TI, &t44_0_0_0, RuntimeInvoker_t44_t44_t44_t29, t294_m10595_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m10595_GM};
extern Il2CppType t2185_0_0_0;
extern void* RuntimeInvoker_t2185 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10630_GM;
MethodInfo m10630_MI = 
{
	"GetEnumerator", (methodPointerType)&m10630, &t294_TI, &t2185_0_0_0, RuntimeInvoker_t2185, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10630_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t294_m10598_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10598_GM;
MethodInfo m10598_MI = 
{
	"IndexOf", (methodPointerType)&m10598_gshared, &t294_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t294_m10598_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10598_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t294_m10600_ParameterInfos[] = 
{
	{"start", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"delta", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10600_GM;
MethodInfo m10600_MI = 
{
	"Shift", (methodPointerType)&m10600_gshared, &t294_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t294_m10600_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10600_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t294_m10602_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10602_GM;
MethodInfo m10602_MI = 
{
	"CheckIndex", (methodPointerType)&m10602_gshared, &t294_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t294_m10602_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10602_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t294_m10604_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10604_GM;
MethodInfo m10604_MI = 
{
	"Insert", (methodPointerType)&m10604_gshared, &t294_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t294_m10604_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10604_GM};
extern Il2CppType t2183_0_0_0;
static ParameterInfo t294_m10606_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2183_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10606_GM;
MethodInfo m10606_MI = 
{
	"CheckCollection", (methodPointerType)&m10606_gshared, &t294_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t294_m10606_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10606_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t294_m10608_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10608_GM;
MethodInfo m10608_MI = 
{
	"Remove", (methodPointerType)&m10608_gshared, &t294_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t294_m10608_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10608_GM};
extern Il2CppType t2180_0_0_0;
static ParameterInfo t294_m10610_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2180_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10610_GM;
MethodInfo m10610_MI = 
{
	"RemoveAll", (methodPointerType)&m10610_gshared, &t294_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t294_m10610_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10610_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t294_m10611_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10611_GM;
MethodInfo m10611_MI = 
{
	"RemoveAt", (methodPointerType)&m10611_gshared, &t294_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t294_m10611_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10611_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10613_GM;
MethodInfo m10613_MI = 
{
	"Reverse", (methodPointerType)&m10613_gshared, &t294_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10613_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10615_GM;
MethodInfo m10615_MI = 
{
	"Sort", (methodPointerType)&m10615_gshared, &t294_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10615_GM};
extern Il2CppType t2181_0_0_0;
extern Il2CppType t2181_0_0_0;
static ParameterInfo t294_m10617_ParameterInfos[] = 
{
	{"comparison", 0, 134217728, &EmptyCustomAttributesCache, &t2181_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10617_GM;
MethodInfo m10617_MI = 
{
	"Sort", (methodPointerType)&m10617_gshared, &t294_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t294_m10617_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10617_GM};
extern Il2CppType t316_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10619_GM;
MethodInfo m10619_MI = 
{
	"ToArray", (methodPointerType)&m10619_gshared, &t294_TI, &t316_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10619_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10621_GM;
MethodInfo m10621_MI = 
{
	"TrimExcess", (methodPointerType)&m10621_gshared, &t294_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10621_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10623_GM;
MethodInfo m10623_MI = 
{
	"get_Capacity", (methodPointerType)&m10623_gshared, &t294_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10623_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t294_m10625_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10625_GM;
MethodInfo m10625_MI = 
{
	"set_Capacity", (methodPointerType)&m10625_gshared, &t294_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t294_m10625_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10625_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10626_GM;
MethodInfo m10626_MI = 
{
	"get_Count", (methodPointerType)&m10626_gshared, &t294_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10626_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t294_m10627_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10627_GM;
MethodInfo m10627_MI = 
{
	"get_Item", (methodPointerType)&m10627_gshared, &t294_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t294_m10627_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10627_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t294_m10629_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10629_GM;
MethodInfo m10629_MI = 
{
	"set_Item", (methodPointerType)&m10629_gshared, &t294_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t294_m10629_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 32, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10629_GM};
static MethodInfo* t294_MIs[] =
{
	&m10537_MI,
	&m10539_MI,
	&m10541_MI,
	&m10543_MI,
	&m10545_MI,
	&m10547_MI,
	&m10549_MI,
	&m10551_MI,
	&m10553_MI,
	&m10555_MI,
	&m10557_MI,
	&m10559_MI,
	&m10561_MI,
	&m10563_MI,
	&m10565_MI,
	&m10567_MI,
	&m10569_MI,
	&m10571_MI,
	&m10573_MI,
	&m10575_MI,
	&m10577_MI,
	&m10579_MI,
	&m10581_MI,
	&m10583_MI,
	&m10585_MI,
	&m10587_MI,
	&m10589_MI,
	&m10591_MI,
	&m10593_MI,
	&m10595_MI,
	&m10630_MI,
	&m10598_MI,
	&m10600_MI,
	&m10602_MI,
	&m10604_MI,
	&m10606_MI,
	&m10608_MI,
	&m10610_MI,
	&m10611_MI,
	&m10613_MI,
	&m10615_MI,
	&m10617_MI,
	&m10619_MI,
	&m10621_MI,
	&m10623_MI,
	&m10625_MI,
	&m10626_MI,
	&m10627_MI,
	&m10629_MI,
	NULL
};
static MethodInfo* t294_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m10547_MI,
	&m10626_MI,
	&m10561_MI,
	&m10563_MI,
	&m10545_MI,
	&m10565_MI,
	&m10567_MI,
	&m10569_MI,
	&m10571_MI,
	&m10549_MI,
	&m10585_MI,
	&m10551_MI,
	&m10553_MI,
	&m10555_MI,
	&m10557_MI,
	&m10611_MI,
	&m10626_MI,
	&m10559_MI,
	&m10573_MI,
	&m10585_MI,
	&m10587_MI,
	&m10589_MI,
	&m10608_MI,
	&m10543_MI,
	&m10598_MI,
	&m10604_MI,
	&m10611_MI,
	&m10627_MI,
	&m10629_MI,
};
extern TypeInfo t2186_TI;
static TypeInfo* t294_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2182_TI,
	&t2183_TI,
	&t2186_TI,
};
static Il2CppInterfaceOffsetPair t294_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2182_TI, 20},
	{ &t2183_TI, 27},
	{ &t2186_TI, 28},
};
extern TypeInfo t294_TI;
extern TypeInfo t316_TI;
extern TypeInfo t2185_TI;
extern TypeInfo t29_TI;
extern TypeInfo t2182_TI;
extern TypeInfo t2184_TI;
static Il2CppRGCTXData t294_RGCTXData[37] = 
{
	&t294_TI/* Static Usage */,
	&t316_TI/* Array Usage */,
	&m10630_MI/* Method Usage */,
	&t2185_TI/* Class Usage */,
	&t29_TI/* Class Usage */,
	&m10573_MI/* Method Usage */,
	&m10587_MI/* Method Usage */,
	&m10598_MI/* Method Usage */,
	&m10602_MI/* Method Usage */,
	&m10604_MI/* Method Usage */,
	&m10608_MI/* Method Usage */,
	&m10627_MI/* Method Usage */,
	&m10629_MI/* Method Usage */,
	&m10575_MI/* Method Usage */,
	&m10623_MI/* Method Usage */,
	&m10625_MI/* Method Usage */,
	&m26254_MI/* Method Usage */,
	&m26259_MI/* Method Usage */,
	&m26261_MI/* Method Usage */,
	&m26253_MI/* Method Usage */,
	&m10606_MI/* Method Usage */,
	&t2182_TI/* Class Usage */,
	&m10577_MI/* Method Usage */,
	&m10579_MI/* Method Usage */,
	&t2184_TI/* Class Usage */,
	&m10637_MI/* Method Usage */,
	&m10120_MI/* Method Usage */,
	&m10593_MI/* Method Usage */,
	&m10595_MI/* Method Usage */,
	&m10742_MI/* Method Usage */,
	&m10631_MI/* Method Usage */,
	&m10600_MI/* Method Usage */,
	&m10611_MI/* Method Usage */,
	&m10748_MI/* Method Usage */,
	&m19857_MI/* Method Usage */,
	&m19964_MI/* Method Usage */,
	&m19788_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t294_0_0_0;
extern Il2CppType t294_1_0_0;
struct t294;
extern Il2CppGenericClass t294_GC;
extern CustomAttributesCache t1261__CustomAttributeCache;
TypeInfo t294_TI = 
{
	&g_mscorlib_dll_Image, NULL, "List`1", "System.Collections.Generic", t294_MIs, t294_PIs, t294_FIs, NULL, &t29_TI, NULL, NULL, &t294_TI, t294_ITIs, t294_VT, &t1261__CustomAttributeCache, &t294_TI, &t294_0_0_0, &t294_1_0_0, t294_IOs, &t294_GC, NULL, t294_FDVs, NULL, t294_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t294), 0, -1, sizeof(t294_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, true, false, false, 49, 9, 5, 0, 0, 33, 6, 6};
#ifndef _MSC_VER
#else
#endif

#include "t1101.h"
extern TypeInfo t7_TI;
extern TypeInfo t1101_TI;
#include "t1101MD.h"
extern MethodInfo m10634_MI;
extern MethodInfo m3974_MI;
extern MethodInfo m1430_MI;
extern MethodInfo m2998_MI;
extern MethodInfo m5150_MI;


 void m10631_gshared (t2185 * __this, t294 * p0, MethodInfo* method)
{
	{
		__this->f0 = p0;
		int32_t L_0 = (p0->f3);
		__this->f2 = L_0;
		return;
	}
}
extern MethodInfo m10632_MI;
 t29 * m10632_gshared (t2185 * __this, MethodInfo* method)
{
	{
		(( void (*) (t2185 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(__this, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (__this->f1);
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3974(L_1, &m3974_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		t29 * L_2 = (__this->f3);
		t29 * L_3 = L_2;
		return ((t29 *)L_3);
	}
}
extern MethodInfo m10633_MI;
 void m10633_gshared (t2185 * __this, MethodInfo* method)
{
	{
		__this->f0 = (t294 *)NULL;
		return;
	}
}
 void m10634_gshared (t2185 * __this, MethodInfo* method)
{
	{
		t294 * L_0 = (__this->f0);
		if (L_0)
		{
			goto IL_0023;
		}
	}
	{
		t2185  L_1 = (*(t2185 *)__this);
		t29 * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		t42 * L_3 = m1430(L_2, &m1430_MI);
		t7* L_4 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2998_MI, L_3);
		t1101 * L_5 = (t1101 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1101_TI));
		m5150(L_5, L_4, &m5150_MI);
		il2cpp_codegen_raise_exception(L_5);
	}

IL_0023:
	{
		int32_t L_6 = (__this->f2);
		t294 * L_7 = (__this->f0);
		int32_t L_8 = (L_7->f3);
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0041;
		}
	}
	{
		t914 * L_9 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_9, (t7*) &_stringLiteral1178, &m3964_MI);
		il2cpp_codegen_raise_exception(L_9);
	}

IL_0041:
	{
		return;
	}
}
extern MethodInfo m10635_MI;
 bool m10635_gshared (t2185 * __this, MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		(( void (*) (t2185 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(__this, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (__this->f1);
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0011;
		}
	}
	{
		return 0;
	}

IL_0011:
	{
		int32_t L_1 = (__this->f1);
		t294 * L_2 = (__this->f0);
		int32_t L_3 = (L_2->f2);
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_004d;
		}
	}
	{
		t294 * L_4 = (__this->f0);
		t316* L_5 = (L_4->f1);
		int32_t L_6 = (__this->f1);
		int32_t L_7 = L_6;
		V_0 = L_7;
		__this->f1 = ((int32_t)(L_7+1));
		int32_t L_8 = V_0;
		__this->f3 = (*(t29 **)(t29 **)SZArrayLdElema(L_5, L_8));
		return 1;
	}

IL_004d:
	{
		__this->f1 = (-1);
		return 0;
	}
}
extern MethodInfo m10636_MI;
 t29 * m10636_gshared (t2185 * __this, MethodInfo* method)
{
	{
		t29 * L_0 = (__this->f3);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.List`1/Enumerator<System.Object>
extern Il2CppType t294_0_0_1;
FieldInfo t2185_f0_FieldInfo = 
{
	"l", &t294_0_0_1, &t2185_TI, offsetof(t2185, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2185_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t2185_TI, offsetof(t2185, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2185_f2_FieldInfo = 
{
	"ver", &t44_0_0_1, &t2185_TI, offsetof(t2185, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t2185_f3_FieldInfo = 
{
	"current", &t29_0_0_1, &t2185_TI, offsetof(t2185, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2185_FIs[] =
{
	&t2185_f0_FieldInfo,
	&t2185_f1_FieldInfo,
	&t2185_f2_FieldInfo,
	&t2185_f3_FieldInfo,
	NULL
};
static PropertyInfo t2185____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2185_TI, "System.Collections.IEnumerator.Current", &m10632_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2185____Current_PropertyInfo = 
{
	&t2185_TI, "Current", &m10636_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2185_PIs[] =
{
	&t2185____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2185____Current_PropertyInfo,
	NULL
};
extern Il2CppType t294_0_0_0;
static ParameterInfo t2185_m10631_ParameterInfos[] = 
{
	{"l", 0, 134217728, &EmptyCustomAttributesCache, &t294_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10631_GM;
MethodInfo m10631_MI = 
{
	".ctor", (methodPointerType)&m10631_gshared, &t2185_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2185_m10631_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10631_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10632_GM;
MethodInfo m10632_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10632_gshared, &t2185_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10632_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10633_GM;
MethodInfo m10633_MI = 
{
	"Dispose", (methodPointerType)&m10633_gshared, &t2185_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10633_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10634_GM;
MethodInfo m10634_MI = 
{
	"VerifyState", (methodPointerType)&m10634_gshared, &t2185_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10634_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10635_GM;
MethodInfo m10635_MI = 
{
	"MoveNext", (methodPointerType)&m10635_gshared, &t2185_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10635_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10636_GM;
MethodInfo m10636_MI = 
{
	"get_Current", (methodPointerType)&m10636_gshared, &t2185_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10636_GM};
static MethodInfo* t2185_MIs[] =
{
	&m10631_MI,
	&m10632_MI,
	&m10633_MI,
	&m10634_MI,
	&m10635_MI,
	&m10636_MI,
	NULL
};
static MethodInfo* t2185_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10632_MI,
	&m10635_MI,
	&m10633_MI,
	&m10636_MI,
};
static TypeInfo* t2185_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t346_TI,
};
static Il2CppInterfaceOffsetPair t2185_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t346_TI, 7},
};
extern TypeInfo t29_TI;
extern TypeInfo t2185_TI;
static Il2CppRGCTXData t2185_RGCTXData[3] = 
{
	&m10634_MI/* Method Usage */,
	&t29_TI/* Class Usage */,
	&t2185_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2185_0_0_0;
extern Il2CppType t2185_1_0_0;
extern Il2CppGenericClass t2185_GC;
extern TypeInfo t1261_TI;
TypeInfo t2185_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t2185_MIs, t2185_PIs, t2185_FIs, NULL, &t110_TI, NULL, &t1261_TI, &t2185_TI, t2185_ITIs, t2185_VT, &EmptyCustomAttributesCache, &t2185_TI, &t2185_0_0_0, &t2185_1_0_0, t2185_IOs, &t2185_GC, NULL, NULL, NULL, t2185_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2185)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 4, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif

#include "t345.h"
extern TypeInfo t345_TI;
#include "t345MD.h"
#include "t2187MD.h"
extern MethodInfo m10666_MI;
extern MethodInfo m1516_MI;
extern MethodInfo m26262_MI;
extern MethodInfo m3960_MI;
extern MethodInfo m4154_MI;
extern MethodInfo m10698_MI;
extern MethodInfo m26258_MI;
extern MethodInfo m26264_MI;


 void m10637_gshared (t2184 * __this, t29* p0, MethodInfo* method)
{
	{
		m1331(__this, &m1331_MI);
		if (p0)
		{
			goto IL_0014;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1179, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0014:
	{
		__this->f0 = p0;
		return;
	}
}
extern MethodInfo m10638_MI;
 void m10638_gshared (t2184 * __this, t29 * p0, MethodInfo* method)
{
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m10639_MI;
 void m10639_gshared (t2184 * __this, MethodInfo* method)
{
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m10640_MI;
 void m10640_gshared (t2184 * __this, int32_t p0, t29 * p1, MethodInfo* method)
{
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m10641_MI;
 bool m10641_gshared (t2184 * __this, t29 * p0, MethodInfo* method)
{
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m10642_MI;
 void m10642_gshared (t2184 * __this, int32_t p0, MethodInfo* method)
{
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m10643_MI;
 t29 * m10643_gshared (t2184 * __this, int32_t p0, MethodInfo* method)
{
	{
		t29 * L_0 = (t29 *)VirtFuncInvoker1< t29 *, int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), __this, p0);
		return L_0;
	}
}
extern MethodInfo m10644_MI;
 void m10644_gshared (t2184 * __this, int32_t p0, t29 * p1, MethodInfo* method)
{
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m10645_MI;
 bool m10645_gshared (t2184 * __this, MethodInfo* method)
{
	{
		return 1;
	}
}
extern MethodInfo m10646_MI;
 void m10646_gshared (t2184 * __this, t20 * p0, int32_t p1, MethodInfo* method)
{
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker2< t20 *, int32_t >::Invoke(&m3960_MI, ((t29 *)Castclass(L_0, InitializedTypeInfo(&t674_TI))), p0, p1);
		return;
	}
}
extern MethodInfo m10647_MI;
 t29 * m10647_gshared (t2184 * __this, MethodInfo* method)
{
	{
		t29* L_0 = (__this->f0);
		t29 * L_1 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m4154_MI, L_0);
		return L_1;
	}
}
extern MethodInfo m10648_MI;
 int32_t m10648_gshared (t2184 * __this, t29 * p0, MethodInfo* method)
{
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m10649_MI;
 void m10649_gshared (t2184 * __this, MethodInfo* method)
{
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m10650_MI;
 bool m10650_gshared (t2184 * __this, t29 * p0, MethodInfo* method)
{
	{
		bool L_0 = (( bool (*) (t29 * __this, t29 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL, p0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		t29* L_1 = (__this->f0);
		bool L_2 = (bool)InterfaceFuncInvoker1< bool, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), L_1, ((t29 *)Castclass(p0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		return L_2;
	}

IL_001a:
	{
		return 0;
	}
}
extern MethodInfo m10651_MI;
 int32_t m10651_gshared (t2184 * __this, t29 * p0, MethodInfo* method)
{
	{
		bool L_0 = (( bool (*) (t29 * __this, t29 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL, p0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		t29* L_1 = (__this->f0);
		int32_t L_2 = (int32_t)InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), L_1, ((t29 *)Castclass(p0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		return L_2;
	}

IL_001a:
	{
		return (-1);
	}
}
extern MethodInfo m10652_MI;
 void m10652_gshared (t2184 * __this, int32_t p0, t29 * p1, MethodInfo* method)
{
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m10653_MI;
 void m10653_gshared (t2184 * __this, t29 * p0, MethodInfo* method)
{
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m10654_MI;
 void m10654_gshared (t2184 * __this, int32_t p0, MethodInfo* method)
{
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m10655_MI;
 bool m10655_gshared (t2184 * __this, MethodInfo* method)
{
	{
		return 0;
	}
}
extern MethodInfo m10656_MI;
 t29 * m10656_gshared (t2184 * __this, MethodInfo* method)
{
	{
		return __this;
	}
}
extern MethodInfo m10657_MI;
 bool m10657_gshared (t2184 * __this, MethodInfo* method)
{
	{
		return 1;
	}
}
extern MethodInfo m10658_MI;
 bool m10658_gshared (t2184 * __this, MethodInfo* method)
{
	{
		return 1;
	}
}
extern MethodInfo m10659_MI;
 t29 * m10659_gshared (t2184 * __this, int32_t p0, MethodInfo* method)
{
	{
		t29* L_0 = (__this->f0);
		t29 * L_1 = (t29 *)InterfaceFuncInvoker1< t29 *, int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), L_0, p0);
		t29 * L_2 = L_1;
		return ((t29 *)L_2);
	}
}
extern MethodInfo m10660_MI;
 void m10660_gshared (t2184 * __this, int32_t p0, t29 * p1, MethodInfo* method)
{
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m10661_MI;
 bool m10661_gshared (t2184 * __this, t29 * p0, MethodInfo* method)
{
	{
		t29* L_0 = (__this->f0);
		bool L_1 = (bool)InterfaceFuncInvoker1< bool, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), L_0, p0);
		return L_1;
	}
}
extern MethodInfo m10662_MI;
 void m10662_gshared (t2184 * __this, t316* p0, int32_t p1, MethodInfo* method)
{
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker2< t316*, int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), L_0, p0, p1);
		return;
	}
}
extern MethodInfo m10663_MI;
 t29* m10663_gshared (t2184 * __this, MethodInfo* method)
{
	{
		t29* L_0 = (__this->f0);
		t29* L_1 = (t29*)InterfaceFuncInvoker0< t29* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), L_0);
		return L_1;
	}
}
extern MethodInfo m10664_MI;
 int32_t m10664_gshared (t2184 * __this, t29 * p0, MethodInfo* method)
{
	{
		t29* L_0 = (__this->f0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), L_0, p0);
		return L_1;
	}
}
extern MethodInfo m10665_MI;
 int32_t m10665_gshared (t2184 * __this, MethodInfo* method)
{
	{
		t29* L_0 = (__this->f0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), L_0);
		return L_1;
	}
}
 t29 * m10666_gshared (t2184 * __this, int32_t p0, MethodInfo* method)
{
	{
		t29* L_0 = (__this->f0);
		t29 * L_1 = (t29 *)InterfaceFuncInvoker1< t29 *, int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), L_0, p0);
		return L_1;
	}
}
// Metadata Definition System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
extern Il2CppType t2186_0_0_1;
FieldInfo t2184_f0_FieldInfo = 
{
	"list", &t2186_0_0_1, &t2184_TI, offsetof(t2184, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2184_FIs[] =
{
	&t2184_f0_FieldInfo,
	NULL
};
static PropertyInfo t2184____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo = 
{
	&t2184_TI, "System.Collections.Generic.IList<T>.Item", &m10643_MI, &m10644_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2184____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2184_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m10645_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2184____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2184_TI, "System.Collections.ICollection.IsSynchronized", &m10655_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2184____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2184_TI, "System.Collections.ICollection.SyncRoot", &m10656_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2184____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2184_TI, "System.Collections.IList.IsFixedSize", &m10657_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2184____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2184_TI, "System.Collections.IList.IsReadOnly", &m10658_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2184____System_Collections_IList_Item_PropertyInfo = 
{
	&t2184_TI, "System.Collections.IList.Item", &m10659_MI, &m10660_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2184____Count_PropertyInfo = 
{
	&t2184_TI, "Count", &m10665_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2184____Item_PropertyInfo = 
{
	&t2184_TI, "Item", &m10666_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2184_PIs[] =
{
	&t2184____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo,
	&t2184____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2184____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2184____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2184____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2184____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2184____System_Collections_IList_Item_PropertyInfo,
	&t2184____Count_PropertyInfo,
	&t2184____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2186_0_0_0;
extern Il2CppType t2186_0_0_0;
static ParameterInfo t2184_m10637_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2186_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10637_GM;
MethodInfo m10637_MI = 
{
	".ctor", (methodPointerType)&m10637_gshared, &t2184_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2184_m10637_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10637_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2184_m10638_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10638_GM;
MethodInfo m10638_MI = 
{
	"System.Collections.Generic.ICollection<T>.Add", (methodPointerType)&m10638_gshared, &t2184_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2184_m10638_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10638_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10639_GM;
MethodInfo m10639_MI = 
{
	"System.Collections.Generic.ICollection<T>.Clear", (methodPointerType)&m10639_gshared, &t2184_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10639_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2184_m10640_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10640_GM;
MethodInfo m10640_MI = 
{
	"System.Collections.Generic.IList<T>.Insert", (methodPointerType)&m10640_gshared, &t2184_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2184_m10640_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10640_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2184_m10641_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10641_GM;
MethodInfo m10641_MI = 
{
	"System.Collections.Generic.ICollection<T>.Remove", (methodPointerType)&m10641_gshared, &t2184_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2184_m10641_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10641_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2184_m10642_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10642_GM;
MethodInfo m10642_MI = 
{
	"System.Collections.Generic.IList<T>.RemoveAt", (methodPointerType)&m10642_gshared, &t2184_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2184_m10642_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10642_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2184_m10643_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10643_GM;
MethodInfo m10643_MI = 
{
	"System.Collections.Generic.IList<T>.get_Item", (methodPointerType)&m10643_gshared, &t2184_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2184_m10643_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10643_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2184_m10644_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10644_GM;
MethodInfo m10644_MI = 
{
	"System.Collections.Generic.IList<T>.set_Item", (methodPointerType)&m10644_gshared, &t2184_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2184_m10644_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10644_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10645_GM;
MethodInfo m10645_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10645_gshared, &t2184_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10645_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2184_m10646_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10646_GM;
MethodInfo m10646_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10646_gshared, &t2184_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2184_m10646_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10646_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10647_GM;
MethodInfo m10647_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10647_gshared, &t2184_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10647_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2184_m10648_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10648_GM;
MethodInfo m10648_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10648_gshared, &t2184_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2184_m10648_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10648_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10649_GM;
MethodInfo m10649_MI = 
{
	"System.Collections.IList.Clear", (methodPointerType)&m10649_gshared, &t2184_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 14, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10649_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2184_m10650_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10650_GM;
MethodInfo m10650_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10650_gshared, &t2184_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2184_m10650_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10650_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2184_m10651_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10651_GM;
MethodInfo m10651_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10651_gshared, &t2184_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2184_m10651_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10651_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2184_m10652_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10652_GM;
MethodInfo m10652_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10652_gshared, &t2184_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2184_m10652_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10652_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2184_m10653_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10653_GM;
MethodInfo m10653_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10653_gshared, &t2184_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2184_m10653_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10653_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2184_m10654_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10654_GM;
MethodInfo m10654_MI = 
{
	"System.Collections.IList.RemoveAt", (methodPointerType)&m10654_gshared, &t2184_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2184_m10654_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10654_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10655_GM;
MethodInfo m10655_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10655_gshared, &t2184_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10655_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10656_GM;
MethodInfo m10656_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10656_gshared, &t2184_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10656_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10657_GM;
MethodInfo m10657_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10657_gshared, &t2184_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10657_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10658_GM;
MethodInfo m10658_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10658_gshared, &t2184_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10658_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2184_m10659_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10659_GM;
MethodInfo m10659_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10659_gshared, &t2184_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2184_m10659_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10659_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2184_m10660_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10660_GM;
MethodInfo m10660_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10660_gshared, &t2184_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2184_m10660_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10660_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2184_m10661_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10661_GM;
MethodInfo m10661_MI = 
{
	"Contains", (methodPointerType)&m10661_gshared, &t2184_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2184_m10661_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10661_GM};
extern Il2CppType t316_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2184_m10662_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10662_GM;
MethodInfo m10662_MI = 
{
	"CopyTo", (methodPointerType)&m10662_gshared, &t2184_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2184_m10662_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10662_GM};
extern Il2CppType t346_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10663_GM;
MethodInfo m10663_MI = 
{
	"GetEnumerator", (methodPointerType)&m10663_gshared, &t2184_TI, &t346_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10663_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2184_m10664_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10664_GM;
MethodInfo m10664_MI = 
{
	"IndexOf", (methodPointerType)&m10664_gshared, &t2184_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2184_m10664_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10664_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10665_GM;
MethodInfo m10665_MI = 
{
	"get_Count", (methodPointerType)&m10665_gshared, &t2184_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10665_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2184_m10666_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10666_GM;
MethodInfo m10666_MI = 
{
	"get_Item", (methodPointerType)&m10666_gshared, &t2184_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2184_m10666_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 33, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10666_GM};
static MethodInfo* t2184_MIs[] =
{
	&m10637_MI,
	&m10638_MI,
	&m10639_MI,
	&m10640_MI,
	&m10641_MI,
	&m10642_MI,
	&m10643_MI,
	&m10644_MI,
	&m10645_MI,
	&m10646_MI,
	&m10647_MI,
	&m10648_MI,
	&m10649_MI,
	&m10650_MI,
	&m10651_MI,
	&m10652_MI,
	&m10653_MI,
	&m10654_MI,
	&m10655_MI,
	&m10656_MI,
	&m10657_MI,
	&m10658_MI,
	&m10659_MI,
	&m10660_MI,
	&m10661_MI,
	&m10662_MI,
	&m10663_MI,
	&m10664_MI,
	&m10665_MI,
	&m10666_MI,
	NULL
};
static MethodInfo* t2184_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m10647_MI,
	&m10665_MI,
	&m10655_MI,
	&m10656_MI,
	&m10646_MI,
	&m10657_MI,
	&m10658_MI,
	&m10659_MI,
	&m10660_MI,
	&m10648_MI,
	&m10649_MI,
	&m10650_MI,
	&m10651_MI,
	&m10652_MI,
	&m10653_MI,
	&m10654_MI,
	&m10665_MI,
	&m10645_MI,
	&m10638_MI,
	&m10639_MI,
	&m10661_MI,
	&m10662_MI,
	&m10641_MI,
	&m10664_MI,
	&m10640_MI,
	&m10642_MI,
	&m10643_MI,
	&m10644_MI,
	&m10663_MI,
	&m10666_MI,
};
static TypeInfo* t2184_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2184_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2182_TI, 20},
	{ &t2186_TI, 27},
	{ &t2183_TI, 32},
};
extern TypeInfo t29_TI;
static Il2CppRGCTXData t2184_RGCTXData[9] = 
{
	&m10666_MI/* Method Usage */,
	&m10698_MI/* Method Usage */,
	&t29_TI/* Class Usage */,
	&m26258_MI/* Method Usage */,
	&m26264_MI/* Method Usage */,
	&m26262_MI/* Method Usage */,
	&m26259_MI/* Method Usage */,
	&m26261_MI/* Method Usage */,
	&m26254_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2184_0_0_0;
extern Il2CppType t2184_1_0_0;
struct t2184;
extern Il2CppGenericClass t2184_GC;
extern CustomAttributesCache t1263__CustomAttributeCache;
TypeInfo t2184_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ReadOnlyCollection`1", "System.Collections.ObjectModel", t2184_MIs, t2184_PIs, t2184_FIs, NULL, &t29_TI, NULL, NULL, &t2184_TI, t2184_ITIs, t2184_VT, &t1263__CustomAttributeCache, &t2184_TI, &t2184_0_0_0, &t2184_1_0_0, t2184_IOs, &t2184_GC, NULL, NULL, NULL, t2184_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2184), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 30, 9, 1, 0, 0, 34, 6, 6};
#include "t2187.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2187_TI;

extern MethodInfo m26255_MI;
extern MethodInfo m10701_MI;
extern MethodInfo m10702_MI;
extern MethodInfo m10699_MI;
extern MethodInfo m10697_MI;
extern MethodInfo m9816_MI;
extern MethodInfo m10690_MI;
extern MethodInfo m10700_MI;
extern MethodInfo m10688_MI;
extern MethodInfo m10693_MI;
extern MethodInfo m10684_MI;
extern MethodInfo m26257_MI;
extern MethodInfo m26265_MI;
extern MethodInfo m26266_MI;
extern MethodInfo m26263_MI;
extern MethodInfo m6004_MI;
extern MethodInfo m9815_MI;
extern MethodInfo m9817_MI;


extern MethodInfo m10667_MI;
 void m10667_gshared (t2187 * __this, MethodInfo* method)
{
	t294 * V_0 = {0};
	t29 * V_1 = {0};
	{
		m1331(__this, &m1331_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t294_TI));
		t294 * L_0 = (t294 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		(( void (*) (t294 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = L_0;
		V_1 = V_0;
		t29 * L_1 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m9816_MI, V_1);
		__this->f1 = L_1;
		__this->f0 = V_0;
		return;
	}
}
extern MethodInfo m10668_MI;
 bool m10668_gshared (t2187 * __this, MethodInfo* method)
{
	{
		t29* L_0 = (__this->f0);
		bool L_1 = (bool)InterfaceFuncInvoker0< bool >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0);
		return L_1;
	}
}
extern MethodInfo m10669_MI;
 void m10669_gshared (t2187 * __this, t20 * p0, int32_t p1, MethodInfo* method)
{
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker2< t20 *, int32_t >::Invoke(&m3960_MI, ((t29 *)Castclass(L_0, InitializedTypeInfo(&t674_TI))), p0, p1);
		return;
	}
}
extern MethodInfo m10670_MI;
 t29 * m10670_gshared (t2187 * __this, MethodInfo* method)
{
	{
		t29* L_0 = (__this->f0);
		t29* L_1 = (t29*)InterfaceFuncInvoker0< t29* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), L_0);
		return L_1;
	}
}
extern MethodInfo m10671_MI;
 int32_t m10671_gshared (t2187 * __this, t29 * p0, MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		t29* L_0 = (__this->f0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), L_0);
		V_0 = L_1;
		t29 * L_2 = (( t29 * (*) (t29 * __this, t29 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL, p0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		VirtActionInvoker2< int32_t, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), __this, V_0, L_2);
		return V_0;
	}
}
extern MethodInfo m10672_MI;
 bool m10672_gshared (t2187 * __this, t29 * p0, MethodInfo* method)
{
	{
		bool L_0 = (( bool (*) (t29 * __this, t29 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL, p0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		t29* L_1 = (__this->f0);
		bool L_2 = (bool)InterfaceFuncInvoker1< bool, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), L_1, ((t29 *)Castclass(p0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))));
		return L_2;
	}

IL_001a:
	{
		return 0;
	}
}
extern MethodInfo m10673_MI;
 int32_t m10673_gshared (t2187 * __this, t29 * p0, MethodInfo* method)
{
	{
		bool L_0 = (( bool (*) (t29 * __this, t29 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL, p0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		t29* L_1 = (__this->f0);
		int32_t L_2 = (int32_t)InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10), L_1, ((t29 *)Castclass(p0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))));
		return L_2;
	}

IL_001a:
	{
		return (-1);
	}
}
extern MethodInfo m10674_MI;
 void m10674_gshared (t2187 * __this, int32_t p0, t29 * p1, MethodInfo* method)
{
	{
		t29 * L_0 = (( t29 * (*) (t29 * __this, t29 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL, p1, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		VirtActionInvoker2< int32_t, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), __this, p0, L_0);
		return;
	}
}
extern MethodInfo m10675_MI;
 void m10675_gshared (t2187 * __this, t29 * p0, MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		t29* L_0 = (__this->f0);
		(( void (*) (t29 * __this, t29* p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(NULL, L_0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		t29 * L_1 = (( t29 * (*) (t29 * __this, t29 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL, p0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		int32_t L_2 = (int32_t)VirtFuncInvoker1< int32_t, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), __this, L_1);
		V_0 = L_2;
		VirtActionInvoker1< int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), __this, V_0);
		return;
	}
}
extern MethodInfo m10676_MI;
 bool m10676_gshared (t2187 * __this, MethodInfo* method)
{
	{
		t29* L_0 = (__this->f0);
		bool L_1 = (( bool (*) (t29 * __this, t29* p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)(NULL, L_0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		return L_1;
	}
}
extern MethodInfo m10677_MI;
 t29 * m10677_gshared (t2187 * __this, MethodInfo* method)
{
	{
		t29 * L_0 = (__this->f1);
		return L_0;
	}
}
extern MethodInfo m10678_MI;
 bool m10678_gshared (t2187 * __this, MethodInfo* method)
{
	{
		t29* L_0 = (__this->f0);
		bool L_1 = (( bool (*) (t29 * __this, t29* p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)(NULL, L_0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		return L_1;
	}
}
extern MethodInfo m10679_MI;
 bool m10679_gshared (t2187 * __this, MethodInfo* method)
{
	{
		t29* L_0 = (__this->f0);
		bool L_1 = (bool)InterfaceFuncInvoker0< bool >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0);
		return L_1;
	}
}
extern MethodInfo m10680_MI;
 t29 * m10680_gshared (t2187 * __this, int32_t p0, MethodInfo* method)
{
	{
		t29* L_0 = (__this->f0);
		t29 * L_1 = (t29 *)InterfaceFuncInvoker1< t29 *, int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16), L_0, p0);
		t29 * L_2 = L_1;
		return ((t29 *)L_2);
	}
}
extern MethodInfo m10681_MI;
 void m10681_gshared (t2187 * __this, int32_t p0, t29 * p1, MethodInfo* method)
{
	{
		t29 * L_0 = (( t29 * (*) (t29 * __this, t29 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL, p1, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		VirtActionInvoker2< int32_t, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17), __this, p0, L_0);
		return;
	}
}
extern MethodInfo m10682_MI;
 void m10682_gshared (t2187 * __this, t29 * p0, MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		t29* L_0 = (__this->f0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), L_0);
		V_0 = L_1;
		VirtActionInvoker2< int32_t, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), __this, V_0, p0);
		return;
	}
}
extern MethodInfo m10683_MI;
 void m10683_gshared (t2187 * __this, MethodInfo* method)
{
	{
		VirtActionInvoker0::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18), __this);
		return;
	}
}
 void m10684_gshared (t2187 * __this, MethodInfo* method)
{
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker0::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19), L_0);
		return;
	}
}
extern MethodInfo m10685_MI;
 bool m10685_gshared (t2187 * __this, t29 * p0, MethodInfo* method)
{
	{
		t29* L_0 = (__this->f0);
		bool L_1 = (bool)InterfaceFuncInvoker1< bool, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), L_0, p0);
		return L_1;
	}
}
extern MethodInfo m10686_MI;
 void m10686_gshared (t2187 * __this, t316* p0, int32_t p1, MethodInfo* method)
{
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker2< t316*, int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20), L_0, p0, p1);
		return;
	}
}
extern MethodInfo m10687_MI;
 t29* m10687_gshared (t2187 * __this, MethodInfo* method)
{
	{
		t29* L_0 = (__this->f0);
		t29* L_1 = (t29*)InterfaceFuncInvoker0< t29* >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), L_0);
		return L_1;
	}
}
 int32_t m10688_gshared (t2187 * __this, t29 * p0, MethodInfo* method)
{
	{
		t29* L_0 = (__this->f0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10), L_0, p0);
		return L_1;
	}
}
extern MethodInfo m10689_MI;
 void m10689_gshared (t2187 * __this, int32_t p0, t29 * p1, MethodInfo* method)
{
	{
		VirtActionInvoker2< int32_t, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), __this, p0, p1);
		return;
	}
}
 void m10690_gshared (t2187 * __this, int32_t p0, t29 * p1, MethodInfo* method)
{
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker2< int32_t, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21), L_0, p0, p1);
		return;
	}
}
extern MethodInfo m10691_MI;
 bool m10691_gshared (t2187 * __this, t29 * p0, MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker1< int32_t, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), __this, p0);
		V_0 = L_0;
		if ((((uint32_t)V_0) != ((uint32_t)(-1))))
		{
			goto IL_000e;
		}
	}
	{
		return 0;
	}

IL_000e:
	{
		VirtActionInvoker1< int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), __this, V_0);
		return 1;
	}
}
extern MethodInfo m10692_MI;
 void m10692_gshared (t2187 * __this, int32_t p0, MethodInfo* method)
{
	{
		VirtActionInvoker1< int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), __this, p0);
		return;
	}
}
 void m10693_gshared (t2187 * __this, int32_t p0, MethodInfo* method)
{
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker1< int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22), L_0, p0);
		return;
	}
}
extern MethodInfo m10694_MI;
 int32_t m10694_gshared (t2187 * __this, MethodInfo* method)
{
	{
		t29* L_0 = (__this->f0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), L_0);
		return L_1;
	}
}
extern MethodInfo m10695_MI;
 t29 * m10695_gshared (t2187 * __this, int32_t p0, MethodInfo* method)
{
	{
		t29* L_0 = (__this->f0);
		t29 * L_1 = (t29 *)InterfaceFuncInvoker1< t29 *, int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16), L_0, p0);
		return L_1;
	}
}
extern MethodInfo m10696_MI;
 void m10696_gshared (t2187 * __this, int32_t p0, t29 * p1, MethodInfo* method)
{
	{
		VirtActionInvoker2< int32_t, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17), __this, p0, p1);
		return;
	}
}
 void m10697_gshared (t2187 * __this, int32_t p0, t29 * p1, MethodInfo* method)
{
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker2< int32_t, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23), L_0, p0, p1);
		return;
	}
}
 bool m10698_gshared (t29 * __this, t29 * p0, MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		if (((t29 *)IsInst(p0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))
		{
			goto IL_0022;
		}
	}
	{
		if (p0)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_0 = m1554(NULL, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24)), &m1554_MI);
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6004_MI, L_0);
		G_B4_0 = ((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		goto IL_0020;
	}

IL_001f:
	{
		G_B4_0 = 0;
	}

IL_0020:
	{
		G_B6_0 = G_B4_0;
		goto IL_0023;
	}

IL_0022:
	{
		G_B6_0 = 1;
	}

IL_0023:
	{
		return G_B6_0;
	}
}
 t29 * m10699_gshared (t29 * __this, t29 * p0, MethodInfo* method)
{
	{
		bool L_0 = (( bool (*) (t29 * __this, t29 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL, p0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		return ((t29 *)Castclass(p0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)));
	}

IL_000f:
	{
		t305 * L_1 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_1, (t7*) &_stringLiteral1176, &m1935_MI);
		il2cpp_codegen_raise_exception(L_1);
	}
}
 void m10700_gshared (t29 * __this, t29* p0, MethodInfo* method)
{
	{
		bool L_0 = (bool)InterfaceFuncInvoker0< bool >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), p0);
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		t345 * L_1 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_1, &m1516_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_000e:
	{
		return;
	}
}
 bool m10701_gshared (t29 * __this, t29* p0, MethodInfo* method)
{
	t29 * V_0 = {0};
	int32_t G_B3_0 = 0;
	{
		V_0 = ((t29 *)IsInst(p0, InitializedTypeInfo(&t674_TI)));
		if (!V_0)
		{
			goto IL_0012;
		}
	}
	{
		bool L_0 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m9815_MI, V_0);
		G_B3_0 = ((int32_t)(L_0));
		goto IL_0013;
	}

IL_0012:
	{
		G_B3_0 = 0;
	}

IL_0013:
	{
		return G_B3_0;
	}
}
 bool m10702_gshared (t29 * __this, t29* p0, MethodInfo* method)
{
	t29 * V_0 = {0};
	int32_t G_B3_0 = 0;
	{
		V_0 = ((t29 *)IsInst(p0, InitializedTypeInfo(&t868_TI)));
		if (!V_0)
		{
			goto IL_0012;
		}
	}
	{
		bool L_0 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m9817_MI, V_0);
		G_B3_0 = ((int32_t)(L_0));
		goto IL_0013;
	}

IL_0012:
	{
		G_B3_0 = 0;
	}

IL_0013:
	{
		return G_B3_0;
	}
}
// Metadata Definition System.Collections.ObjectModel.Collection`1<System.Object>
extern Il2CppType t2186_0_0_1;
FieldInfo t2187_f0_FieldInfo = 
{
	"list", &t2186_0_0_1, &t2187_TI, offsetof(t2187, f0), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t2187_f1_FieldInfo = 
{
	"syncRoot", &t29_0_0_1, &t2187_TI, offsetof(t2187, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2187_FIs[] =
{
	&t2187_f0_FieldInfo,
	&t2187_f1_FieldInfo,
	NULL
};
static PropertyInfo t2187____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2187_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m10668_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2187____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2187_TI, "System.Collections.ICollection.IsSynchronized", &m10676_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2187____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2187_TI, "System.Collections.ICollection.SyncRoot", &m10677_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2187____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2187_TI, "System.Collections.IList.IsFixedSize", &m10678_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2187____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2187_TI, "System.Collections.IList.IsReadOnly", &m10679_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2187____System_Collections_IList_Item_PropertyInfo = 
{
	&t2187_TI, "System.Collections.IList.Item", &m10680_MI, &m10681_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2187____Count_PropertyInfo = 
{
	&t2187_TI, "Count", &m10694_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2187____Item_PropertyInfo = 
{
	&t2187_TI, "Item", &m10695_MI, &m10696_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2187_PIs[] =
{
	&t2187____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2187____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2187____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2187____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2187____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2187____System_Collections_IList_Item_PropertyInfo,
	&t2187____Count_PropertyInfo,
	&t2187____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10667_GM;
MethodInfo m10667_MI = 
{
	".ctor", (methodPointerType)&m10667_gshared, &t2187_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10667_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10668_GM;
MethodInfo m10668_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10668_gshared, &t2187_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10668_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2187_m10669_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10669_GM;
MethodInfo m10669_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10669_gshared, &t2187_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2187_m10669_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10669_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10670_GM;
MethodInfo m10670_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10670_gshared, &t2187_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10670_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2187_m10671_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10671_GM;
MethodInfo m10671_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10671_gshared, &t2187_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2187_m10671_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10671_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2187_m10672_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10672_GM;
MethodInfo m10672_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10672_gshared, &t2187_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2187_m10672_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10672_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2187_m10673_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10673_GM;
MethodInfo m10673_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10673_gshared, &t2187_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2187_m10673_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10673_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2187_m10674_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10674_GM;
MethodInfo m10674_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10674_gshared, &t2187_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2187_m10674_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10674_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2187_m10675_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10675_GM;
MethodInfo m10675_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10675_gshared, &t2187_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2187_m10675_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10675_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10676_GM;
MethodInfo m10676_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10676_gshared, &t2187_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10676_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10677_GM;
MethodInfo m10677_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10677_gshared, &t2187_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10677_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10678_GM;
MethodInfo m10678_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10678_gshared, &t2187_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10678_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10679_GM;
MethodInfo m10679_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10679_gshared, &t2187_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10679_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2187_m10680_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10680_GM;
MethodInfo m10680_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10680_gshared, &t2187_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2187_m10680_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10680_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2187_m10681_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10681_GM;
MethodInfo m10681_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10681_gshared, &t2187_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2187_m10681_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10681_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2187_m10682_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10682_GM;
MethodInfo m10682_MI = 
{
	"Add", (methodPointerType)&m10682_gshared, &t2187_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2187_m10682_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10682_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10683_GM;
MethodInfo m10683_MI = 
{
	"Clear", (methodPointerType)&m10683_gshared, &t2187_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10683_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10684_GM;
MethodInfo m10684_MI = 
{
	"ClearItems", (methodPointerType)&m10684_gshared, &t2187_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 452, 0, 33, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10684_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2187_m10685_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10685_GM;
MethodInfo m10685_MI = 
{
	"Contains", (methodPointerType)&m10685_gshared, &t2187_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2187_m10685_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10685_GM};
extern Il2CppType t316_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2187_m10686_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10686_GM;
MethodInfo m10686_MI = 
{
	"CopyTo", (methodPointerType)&m10686_gshared, &t2187_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2187_m10686_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10686_GM};
extern Il2CppType t346_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10687_GM;
MethodInfo m10687_MI = 
{
	"GetEnumerator", (methodPointerType)&m10687_gshared, &t2187_TI, &t346_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10687_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2187_m10688_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10688_GM;
MethodInfo m10688_MI = 
{
	"IndexOf", (methodPointerType)&m10688_gshared, &t2187_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2187_m10688_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10688_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2187_m10689_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10689_GM;
MethodInfo m10689_MI = 
{
	"Insert", (methodPointerType)&m10689_gshared, &t2187_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2187_m10689_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10689_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2187_m10690_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10690_GM;
MethodInfo m10690_MI = 
{
	"InsertItem", (methodPointerType)&m10690_gshared, &t2187_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2187_m10690_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 34, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10690_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2187_m10691_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10691_GM;
MethodInfo m10691_MI = 
{
	"Remove", (methodPointerType)&m10691_gshared, &t2187_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2187_m10691_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10691_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2187_m10692_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10692_GM;
MethodInfo m10692_MI = 
{
	"RemoveAt", (methodPointerType)&m10692_gshared, &t2187_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2187_m10692_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10692_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2187_m10693_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10693_GM;
MethodInfo m10693_MI = 
{
	"RemoveItem", (methodPointerType)&m10693_gshared, &t2187_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2187_m10693_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 35, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10693_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10694_GM;
MethodInfo m10694_MI = 
{
	"get_Count", (methodPointerType)&m10694_gshared, &t2187_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10694_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2187_m10695_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10695_GM;
MethodInfo m10695_MI = 
{
	"get_Item", (methodPointerType)&m10695_gshared, &t2187_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2187_m10695_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10695_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2187_m10696_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10696_GM;
MethodInfo m10696_MI = 
{
	"set_Item", (methodPointerType)&m10696_gshared, &t2187_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2187_m10696_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10696_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2187_m10697_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10697_GM;
MethodInfo m10697_MI = 
{
	"SetItem", (methodPointerType)&m10697_gshared, &t2187_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2187_m10697_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 36, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10697_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2187_m10698_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10698_GM;
MethodInfo m10698_MI = 
{
	"IsValidItem", (methodPointerType)&m10698_gshared, &t2187_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2187_m10698_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10698_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2187_m10699_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10699_GM;
MethodInfo m10699_MI = 
{
	"ConvertItem", (methodPointerType)&m10699_gshared, &t2187_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t2187_m10699_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10699_GM};
extern Il2CppType t2186_0_0_0;
static ParameterInfo t2187_m10700_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2186_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10700_GM;
MethodInfo m10700_MI = 
{
	"CheckWritable", (methodPointerType)&m10700_gshared, &t2187_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2187_m10700_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10700_GM};
extern Il2CppType t2186_0_0_0;
static ParameterInfo t2187_m10701_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2186_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10701_GM;
MethodInfo m10701_MI = 
{
	"IsSynchronized", (methodPointerType)&m10701_gshared, &t2187_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2187_m10701_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10701_GM};
extern Il2CppType t2186_0_0_0;
static ParameterInfo t2187_m10702_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2186_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10702_GM;
MethodInfo m10702_MI = 
{
	"IsFixedSize", (methodPointerType)&m10702_gshared, &t2187_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2187_m10702_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10702_GM};
static MethodInfo* t2187_MIs[] =
{
	&m10667_MI,
	&m10668_MI,
	&m10669_MI,
	&m10670_MI,
	&m10671_MI,
	&m10672_MI,
	&m10673_MI,
	&m10674_MI,
	&m10675_MI,
	&m10676_MI,
	&m10677_MI,
	&m10678_MI,
	&m10679_MI,
	&m10680_MI,
	&m10681_MI,
	&m10682_MI,
	&m10683_MI,
	&m10684_MI,
	&m10685_MI,
	&m10686_MI,
	&m10687_MI,
	&m10688_MI,
	&m10689_MI,
	&m10690_MI,
	&m10691_MI,
	&m10692_MI,
	&m10693_MI,
	&m10694_MI,
	&m10695_MI,
	&m10696_MI,
	&m10697_MI,
	&m10698_MI,
	&m10699_MI,
	&m10700_MI,
	&m10701_MI,
	&m10702_MI,
	NULL
};
static MethodInfo* t2187_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m10670_MI,
	&m10694_MI,
	&m10676_MI,
	&m10677_MI,
	&m10669_MI,
	&m10678_MI,
	&m10679_MI,
	&m10680_MI,
	&m10681_MI,
	&m10671_MI,
	&m10683_MI,
	&m10672_MI,
	&m10673_MI,
	&m10674_MI,
	&m10675_MI,
	&m10692_MI,
	&m10694_MI,
	&m10668_MI,
	&m10682_MI,
	&m10683_MI,
	&m10685_MI,
	&m10686_MI,
	&m10691_MI,
	&m10688_MI,
	&m10689_MI,
	&m10692_MI,
	&m10695_MI,
	&m10696_MI,
	&m10687_MI,
	&m10684_MI,
	&m10690_MI,
	&m10693_MI,
	&m10697_MI,
};
static TypeInfo* t2187_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2187_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2182_TI, 20},
	{ &t2186_TI, 27},
	{ &t2183_TI, 32},
};
extern TypeInfo t294_TI;
extern TypeInfo t29_TI;
static Il2CppRGCTXData t2187_RGCTXData[25] = 
{
	&t294_TI/* Class Usage */,
	&m10537_MI/* Method Usage */,
	&m26255_MI/* Method Usage */,
	&m26261_MI/* Method Usage */,
	&m26254_MI/* Method Usage */,
	&m10699_MI/* Method Usage */,
	&m10690_MI/* Method Usage */,
	&m10698_MI/* Method Usage */,
	&t29_TI/* Class Usage */,
	&m26258_MI/* Method Usage */,
	&m26264_MI/* Method Usage */,
	&m10700_MI/* Method Usage */,
	&m10688_MI/* Method Usage */,
	&m10693_MI/* Method Usage */,
	&m10701_MI/* Method Usage */,
	&m10702_MI/* Method Usage */,
	&m26262_MI/* Method Usage */,
	&m10697_MI/* Method Usage */,
	&m10684_MI/* Method Usage */,
	&m26257_MI/* Method Usage */,
	&m26259_MI/* Method Usage */,
	&m26265_MI/* Method Usage */,
	&m26266_MI/* Method Usage */,
	&m26263_MI/* Method Usage */,
	&t29_0_0_0/* Type Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2187_0_0_0;
extern Il2CppType t2187_1_0_0;
struct t2187;
extern Il2CppGenericClass t2187_GC;
extern CustomAttributesCache t1262__CustomAttributeCache;
TypeInfo t2187_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Collection`1", "System.Collections.ObjectModel", t2187_MIs, t2187_PIs, t2187_FIs, NULL, &t29_TI, NULL, NULL, &t2187_TI, t2187_ITIs, t2187_VT, &t1262__CustomAttributeCache, &t2187_TI, &t2187_0_0_0, &t2187_1_0_0, t2187_IOs, &t2187_GC, NULL, NULL, NULL, t2187_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2187), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 36, 8, 2, 0, 0, 37, 6, 6};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2001_TI;
#include "t2001MD.h"

#include "t1257.h"
#include "t2194.h"
extern TypeInfo t6627_TI;
extern TypeInfo t1257_TI;
extern TypeInfo t537_TI;
extern TypeInfo t2194_TI;
#include "t931MD.h"
#include "t2194MD.h"
extern Il2CppType t6627_0_0_0;
extern Il2CppType t1257_0_0_0;
extern MethodInfo m2981_MI;
extern MethodInfo m2979_MI;
extern MethodInfo m8828_MI;
extern MethodInfo m10738_MI;
extern MethodInfo m26619_MI;
extern MethodInfo m19789_MI;


extern MethodInfo m10703_MI;
 void m10703_gshared (t2001 * __this, MethodInfo* method)
{
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m10704_MI;
 void m10704_gshared (t29 * __this, MethodInfo* method)
{
	t2194 * L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer = (t2194 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
	(( void (*) (t2194 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
	((t2001_SFs*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->f0 = L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer;
}
extern MethodInfo m10705_MI;
 int32_t m10705_gshared (t2001 * __this, t29 * p0, MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker1< int32_t, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), __this, ((t29 *)Castclass(p0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))));
		return L_0;
	}
}
extern MethodInfo m10706_MI;
 bool m10706_gshared (t2001 * __this, t29 * p0, t29 * p1, MethodInfo* method)
{
	{
		bool L_0 = (bool)VirtFuncInvoker2< bool, t29 *, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), __this, ((t29 *)Castclass(p0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))), ((t29 *)Castclass(p1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))));
		return L_0;
	}
}
extern MethodInfo m10707_MI;
 t2001 * m10707_gshared (t29 * __this, MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return (((t2001_SFs*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->f0);
	}
}
// Metadata Definition System.Collections.Generic.EqualityComparer`1<System.Object>
extern Il2CppType t2001_0_0_49;
FieldInfo t2001_f0_FieldInfo = 
{
	"_default", &t2001_0_0_49, &t2001_TI, offsetof(t2001_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2001_FIs[] =
{
	&t2001_f0_FieldInfo,
	NULL
};
static PropertyInfo t2001____Default_PropertyInfo = 
{
	&t2001_TI, "Default", &m10707_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2001_PIs[] =
{
	&t2001____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10703_GM;
MethodInfo m10703_MI = 
{
	".ctor", (methodPointerType)&m10703_gshared, &t2001_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10703_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10704_GM;
MethodInfo m10704_MI = 
{
	".cctor", (methodPointerType)&m10704_gshared, &t2001_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10704_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2001_m10705_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10705_GM;
MethodInfo m10705_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m10705_gshared, &t2001_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2001_m10705_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10705_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2001_m10706_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10706_GM;
MethodInfo m10706_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m10706_gshared, &t2001_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2001_m10706_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10706_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2001_m26619_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26619_GM;
MethodInfo m26619_MI = 
{
	"GetHashCode", NULL, &t2001_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2001_m26619_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26619_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2001_m19789_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19789_GM;
MethodInfo m19789_MI = 
{
	"Equals", NULL, &t2001_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2001_m19789_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19789_GM};
extern Il2CppType t2001_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10707_GM;
MethodInfo m10707_MI = 
{
	"get_Default", (methodPointerType)&m10707_gshared, &t2001_TI, &t2001_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10707_GM};
static MethodInfo* t2001_MIs[] =
{
	&m10703_MI,
	&m10704_MI,
	&m10705_MI,
	&m10706_MI,
	&m26619_MI,
	&m19789_MI,
	&m10707_MI,
	NULL
};
static MethodInfo* t2001_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m19789_MI,
	&m26619_MI,
	&m10706_MI,
	&m10705_MI,
	NULL,
	NULL,
};
extern TypeInfo t2416_TI;
extern TypeInfo t734_TI;
static TypeInfo* t2001_ITIs[] = 
{
	&t2416_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t2001_IOs[] = 
{
	{ &t2416_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2001_TI;
extern TypeInfo t2001_TI;
extern TypeInfo t2194_TI;
extern TypeInfo t29_TI;
static Il2CppRGCTXData t2001_RGCTXData[9] = 
{
	&t6627_0_0_0/* Type Usage */,
	&t29_0_0_0/* Type Usage */,
	&t2001_TI/* Class Usage */,
	&t2001_TI/* Static Usage */,
	&t2194_TI/* Class Usage */,
	&m10738_MI/* Method Usage */,
	&t29_TI/* Class Usage */,
	&m26619_MI/* Method Usage */,
	&m19789_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2001_0_0_0;
extern Il2CppType t2001_1_0_0;
struct t2001;
extern Il2CppGenericClass t2001_GC;
TypeInfo t2001_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t2001_MIs, t2001_PIs, t2001_FIs, NULL, &t29_TI, NULL, NULL, &t2001_TI, t2001_ITIs, t2001_VT, &EmptyCustomAttributesCache, &t2001_TI, &t2001_0_0_0, &t2001_1_0_0, t2001_IOs, &t2001_GC, NULL, NULL, NULL, t2001_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2001), 0, -1, sizeof(t2001_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<System.Object>
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2416_m26620_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26620_GM;
MethodInfo m26620_MI = 
{
	"Equals", NULL, &t2416_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2416_m26620_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26620_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2416_m26621_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26621_GM;
MethodInfo m26621_MI = 
{
	"GetHashCode", NULL, &t2416_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2416_m26621_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26621_GM};
static MethodInfo* t2416_MIs[] =
{
	&m26620_MI,
	&m26621_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2416_0_0_0;
extern Il2CppType t2416_1_0_0;
struct t2416;
extern Il2CppGenericClass t2416_GC;
TypeInfo t2416_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t2416_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2416_TI, NULL, NULL, &EmptyCustomAttributesCache, &t2416_TI, &t2416_0_0_0, &t2416_1_0_0, NULL, &t2416_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IEquatable`1<System.Object>
extern Il2CppType t29_0_0_0;
static ParameterInfo t6627_m26622_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26622_GM;
MethodInfo m26622_MI = 
{
	"Equals", NULL, &t6627_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6627_m26622_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26622_GM};
static MethodInfo* t6627_MIs[] =
{
	&m26622_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6627_1_0_0;
struct t6627;
extern Il2CppGenericClass t6627_GC;
TypeInfo t6627_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t6627_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6627_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6627_TI, &t6627_0_0_0, &t6627_1_0_0, NULL, &t6627_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3067_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Type>
extern MethodInfo m26623_MI;
static PropertyInfo t3067____Current_PropertyInfo = 
{
	&t3067_TI, "Current", &m26623_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3067_PIs[] =
{
	&t3067____Current_PropertyInfo,
	NULL
};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26623_GM;
MethodInfo m26623_MI = 
{
	"get_Current", NULL, &t3067_TI, &t42_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26623_GM};
static MethodInfo* t3067_MIs[] =
{
	&m26623_MI,
	NULL
};
static TypeInfo* t3067_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3067_0_0_0;
extern Il2CppType t3067_1_0_0;
struct t3067;
extern Il2CppGenericClass t3067_GC;
TypeInfo t3067_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t3067_MIs, t3067_PIs, NULL, NULL, NULL, NULL, NULL, &t3067_TI, t3067_ITIs, NULL, &EmptyCustomAttributesCache, &t3067_TI, &t3067_0_0_0, &t3067_1_0_0, NULL, &t3067_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2188.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2188_TI;
#include "t2188MD.h"

extern MethodInfo m10712_MI;
extern MethodInfo m19791_MI;
struct t20;
#define m19791(__this, p0, method) (t42 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Type>
extern Il2CppType t20_0_0_1;
FieldInfo t2188_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2188_TI, offsetof(t2188, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2188_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2188_TI, offsetof(t2188, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2188_FIs[] =
{
	&t2188_f0_FieldInfo,
	&t2188_f1_FieldInfo,
	NULL
};
extern MethodInfo m10709_MI;
static PropertyInfo t2188____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2188_TI, "System.Collections.IEnumerator.Current", &m10709_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2188____Current_PropertyInfo = 
{
	&t2188_TI, "Current", &m10712_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2188_PIs[] =
{
	&t2188____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2188____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2188_m10708_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10708_GM;
MethodInfo m10708_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2188_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2188_m10708_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10708_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10709_GM;
MethodInfo m10709_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2188_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10709_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10710_GM;
MethodInfo m10710_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2188_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10710_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10711_GM;
MethodInfo m10711_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2188_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10711_GM};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10712_GM;
MethodInfo m10712_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2188_TI, &t42_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10712_GM};
static MethodInfo* t2188_MIs[] =
{
	&m10708_MI,
	&m10709_MI,
	&m10710_MI,
	&m10711_MI,
	&m10712_MI,
	NULL
};
extern MethodInfo m10711_MI;
extern MethodInfo m10710_MI;
static MethodInfo* t2188_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10709_MI,
	&m10711_MI,
	&m10710_MI,
	&m10712_MI,
};
static TypeInfo* t2188_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3067_TI,
};
static Il2CppInterfaceOffsetPair t2188_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3067_TI, 7},
};
extern TypeInfo t42_TI;
static Il2CppRGCTXData t2188_RGCTXData[3] = 
{
	&m10712_MI/* Method Usage */,
	&t42_TI/* Class Usage */,
	&m19791_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2188_0_0_0;
extern Il2CppType t2188_1_0_0;
extern Il2CppGenericClass t2188_GC;
TypeInfo t2188_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2188_MIs, t2188_PIs, t2188_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2188_TI, t2188_ITIs, t2188_VT, &EmptyCustomAttributesCache, &t2188_TI, &t2188_0_0_0, &t2188_1_0_0, t2188_IOs, &t2188_GC, NULL, NULL, NULL, t2188_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2188)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3069_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Type>
extern MethodInfo m26624_MI;
static PropertyInfo t3069____Count_PropertyInfo = 
{
	&t3069_TI, "Count", &m26624_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26625_MI;
static PropertyInfo t3069____IsReadOnly_PropertyInfo = 
{
	&t3069_TI, "IsReadOnly", &m26625_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3069_PIs[] =
{
	&t3069____Count_PropertyInfo,
	&t3069____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26624_GM;
MethodInfo m26624_MI = 
{
	"get_Count", NULL, &t3069_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26624_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26625_GM;
MethodInfo m26625_MI = 
{
	"get_IsReadOnly", NULL, &t3069_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26625_GM};
extern Il2CppType t42_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t3069_m26626_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26626_GM;
MethodInfo m26626_MI = 
{
	"Add", NULL, &t3069_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3069_m26626_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26626_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26627_GM;
MethodInfo m26627_MI = 
{
	"Clear", NULL, &t3069_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26627_GM};
extern Il2CppType t42_0_0_0;
static ParameterInfo t3069_m26628_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26628_GM;
MethodInfo m26628_MI = 
{
	"Contains", NULL, &t3069_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3069_m26628_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26628_GM};
extern Il2CppType t537_0_0_0;
extern Il2CppType t537_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3069_m26629_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t537_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26629_GM;
MethodInfo m26629_MI = 
{
	"CopyTo", NULL, &t3069_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3069_m26629_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26629_GM};
extern Il2CppType t42_0_0_0;
static ParameterInfo t3069_m26630_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26630_GM;
MethodInfo m26630_MI = 
{
	"Remove", NULL, &t3069_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3069_m26630_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26630_GM};
static MethodInfo* t3069_MIs[] =
{
	&m26624_MI,
	&m26625_MI,
	&m26626_MI,
	&m26627_MI,
	&m26628_MI,
	&m26629_MI,
	&m26630_MI,
	NULL
};
extern TypeInfo t3070_TI;
static TypeInfo* t3069_ITIs[] = 
{
	&t603_TI,
	&t3070_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3069_0_0_0;
extern Il2CppType t3069_1_0_0;
struct t3069;
extern Il2CppGenericClass t3069_GC;
TypeInfo t3069_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t3069_MIs, t3069_PIs, NULL, NULL, NULL, NULL, NULL, &t3069_TI, t3069_ITIs, NULL, &EmptyCustomAttributesCache, &t3069_TI, &t3069_0_0_0, &t3069_1_0_0, NULL, &t3069_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Type>
extern Il2CppType t3067_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26631_GM;
MethodInfo m26631_MI = 
{
	"GetEnumerator", NULL, &t3070_TI, &t3067_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26631_GM};
static MethodInfo* t3070_MIs[] =
{
	&m26631_MI,
	NULL
};
static TypeInfo* t3070_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3070_0_0_0;
extern Il2CppType t3070_1_0_0;
struct t3070;
extern Il2CppGenericClass t3070_GC;
TypeInfo t3070_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t3070_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t3070_TI, t3070_ITIs, NULL, &EmptyCustomAttributesCache, &t3070_TI, &t3070_0_0_0, &t3070_1_0_0, NULL, &t3070_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3075_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Type>
extern MethodInfo m26632_MI;
extern MethodInfo m26633_MI;
static PropertyInfo t3075____Item_PropertyInfo = 
{
	&t3075_TI, "Item", &m26632_MI, &m26633_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3075_PIs[] =
{
	&t3075____Item_PropertyInfo,
	NULL
};
extern Il2CppType t42_0_0_0;
static ParameterInfo t3075_m26634_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26634_GM;
MethodInfo m26634_MI = 
{
	"IndexOf", NULL, &t3075_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3075_m26634_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26634_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t3075_m26635_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26635_GM;
MethodInfo m26635_MI = 
{
	"Insert", NULL, &t3075_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3075_m26635_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26635_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3075_m26636_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26636_GM;
MethodInfo m26636_MI = 
{
	"RemoveAt", NULL, &t3075_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t3075_m26636_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26636_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3075_m26632_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26632_GM;
MethodInfo m26632_MI = 
{
	"get_Item", NULL, &t3075_TI, &t42_0_0_0, RuntimeInvoker_t29_t44, t3075_m26632_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26632_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t3075_m26633_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26633_GM;
MethodInfo m26633_MI = 
{
	"set_Item", NULL, &t3075_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3075_m26633_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26633_GM};
static MethodInfo* t3075_MIs[] =
{
	&m26634_MI,
	&m26635_MI,
	&m26636_MI,
	&m26632_MI,
	&m26633_MI,
	NULL
};
static TypeInfo* t3075_ITIs[] = 
{
	&t603_TI,
	&t3069_TI,
	&t3070_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3075_0_0_0;
extern Il2CppType t3075_1_0_0;
struct t3075;
extern Il2CppGenericClass t3075_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t3075_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t3075_MIs, t3075_PIs, NULL, NULL, NULL, NULL, NULL, &t3075_TI, t3075_ITIs, NULL, &t1908__CustomAttributeCache, &t3075_TI, &t3075_0_0_0, &t3075_1_0_0, NULL, &t3075_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5125_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.IReflect>
extern MethodInfo m26637_MI;
static PropertyInfo t5125____Count_PropertyInfo = 
{
	&t5125_TI, "Count", &m26637_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26638_MI;
static PropertyInfo t5125____IsReadOnly_PropertyInfo = 
{
	&t5125_TI, "IsReadOnly", &m26638_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5125_PIs[] =
{
	&t5125____Count_PropertyInfo,
	&t5125____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26637_GM;
MethodInfo m26637_MI = 
{
	"get_Count", NULL, &t5125_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26637_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26638_GM;
MethodInfo m26638_MI = 
{
	"get_IsReadOnly", NULL, &t5125_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26638_GM};
extern Il2CppType t1913_0_0_0;
extern Il2CppType t1913_0_0_0;
static ParameterInfo t5125_m26639_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1913_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26639_GM;
MethodInfo m26639_MI = 
{
	"Add", NULL, &t5125_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5125_m26639_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26639_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26640_GM;
MethodInfo m26640_MI = 
{
	"Clear", NULL, &t5125_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26640_GM};
extern Il2CppType t1913_0_0_0;
static ParameterInfo t5125_m26641_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1913_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26641_GM;
MethodInfo m26641_MI = 
{
	"Contains", NULL, &t5125_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5125_m26641_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26641_GM};
extern Il2CppType t3525_0_0_0;
extern Il2CppType t3525_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5125_m26642_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3525_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26642_GM;
MethodInfo m26642_MI = 
{
	"CopyTo", NULL, &t5125_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5125_m26642_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26642_GM};
extern Il2CppType t1913_0_0_0;
static ParameterInfo t5125_m26643_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1913_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26643_GM;
MethodInfo m26643_MI = 
{
	"Remove", NULL, &t5125_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5125_m26643_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26643_GM};
static MethodInfo* t5125_MIs[] =
{
	&m26637_MI,
	&m26638_MI,
	&m26639_MI,
	&m26640_MI,
	&m26641_MI,
	&m26642_MI,
	&m26643_MI,
	NULL
};
extern TypeInfo t5127_TI;
static TypeInfo* t5125_ITIs[] = 
{
	&t603_TI,
	&t5127_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5125_0_0_0;
extern Il2CppType t5125_1_0_0;
struct t5125;
extern Il2CppGenericClass t5125_GC;
TypeInfo t5125_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5125_MIs, t5125_PIs, NULL, NULL, NULL, NULL, NULL, &t5125_TI, t5125_ITIs, NULL, &EmptyCustomAttributesCache, &t5125_TI, &t5125_0_0_0, &t5125_1_0_0, NULL, &t5125_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.IReflect>
extern Il2CppType t3991_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26644_GM;
MethodInfo m26644_MI = 
{
	"GetEnumerator", NULL, &t5127_TI, &t3991_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26644_GM};
static MethodInfo* t5127_MIs[] =
{
	&m26644_MI,
	NULL
};
static TypeInfo* t5127_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5127_0_0_0;
extern Il2CppType t5127_1_0_0;
struct t5127;
extern Il2CppGenericClass t5127_GC;
TypeInfo t5127_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5127_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5127_TI, t5127_ITIs, NULL, &EmptyCustomAttributesCache, &t5127_TI, &t5127_0_0_0, &t5127_1_0_0, NULL, &t5127_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3991_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.IReflect>
extern MethodInfo m26645_MI;
static PropertyInfo t3991____Current_PropertyInfo = 
{
	&t3991_TI, "Current", &m26645_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3991_PIs[] =
{
	&t3991____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1913_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26645_GM;
MethodInfo m26645_MI = 
{
	"get_Current", NULL, &t3991_TI, &t1913_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26645_GM};
static MethodInfo* t3991_MIs[] =
{
	&m26645_MI,
	NULL
};
static TypeInfo* t3991_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3991_0_0_0;
extern Il2CppType t3991_1_0_0;
struct t3991;
extern Il2CppGenericClass t3991_GC;
TypeInfo t3991_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t3991_MIs, t3991_PIs, NULL, NULL, NULL, NULL, NULL, &t3991_TI, t3991_ITIs, NULL, &EmptyCustomAttributesCache, &t3991_TI, &t3991_0_0_0, &t3991_1_0_0, NULL, &t3991_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2189.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2189_TI;
#include "t2189MD.h"

extern TypeInfo t1913_TI;
extern MethodInfo m10717_MI;
extern MethodInfo m19802_MI;
struct t20;
#define m19802(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.IReflect>
extern Il2CppType t20_0_0_1;
FieldInfo t2189_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2189_TI, offsetof(t2189, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2189_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2189_TI, offsetof(t2189, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2189_FIs[] =
{
	&t2189_f0_FieldInfo,
	&t2189_f1_FieldInfo,
	NULL
};
extern MethodInfo m10714_MI;
static PropertyInfo t2189____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2189_TI, "System.Collections.IEnumerator.Current", &m10714_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2189____Current_PropertyInfo = 
{
	&t2189_TI, "Current", &m10717_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2189_PIs[] =
{
	&t2189____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2189____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2189_m10713_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10713_GM;
MethodInfo m10713_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2189_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2189_m10713_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10713_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10714_GM;
MethodInfo m10714_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2189_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10714_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10715_GM;
MethodInfo m10715_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2189_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10715_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10716_GM;
MethodInfo m10716_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2189_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10716_GM};
extern Il2CppType t1913_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10717_GM;
MethodInfo m10717_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2189_TI, &t1913_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10717_GM};
static MethodInfo* t2189_MIs[] =
{
	&m10713_MI,
	&m10714_MI,
	&m10715_MI,
	&m10716_MI,
	&m10717_MI,
	NULL
};
extern MethodInfo m10716_MI;
extern MethodInfo m10715_MI;
static MethodInfo* t2189_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10714_MI,
	&m10716_MI,
	&m10715_MI,
	&m10717_MI,
};
static TypeInfo* t2189_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3991_TI,
};
static Il2CppInterfaceOffsetPair t2189_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3991_TI, 7},
};
extern TypeInfo t1913_TI;
static Il2CppRGCTXData t2189_RGCTXData[3] = 
{
	&m10717_MI/* Method Usage */,
	&t1913_TI/* Class Usage */,
	&m19802_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2189_0_0_0;
extern Il2CppType t2189_1_0_0;
extern Il2CppGenericClass t2189_GC;
TypeInfo t2189_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2189_MIs, t2189_PIs, t2189_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2189_TI, t2189_ITIs, t2189_VT, &EmptyCustomAttributesCache, &t2189_TI, &t2189_0_0_0, &t2189_1_0_0, t2189_IOs, &t2189_GC, NULL, NULL, NULL, t2189_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2189)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5126_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.IReflect>
extern MethodInfo m26646_MI;
extern MethodInfo m26647_MI;
static PropertyInfo t5126____Item_PropertyInfo = 
{
	&t5126_TI, "Item", &m26646_MI, &m26647_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5126_PIs[] =
{
	&t5126____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1913_0_0_0;
static ParameterInfo t5126_m26648_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1913_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26648_GM;
MethodInfo m26648_MI = 
{
	"IndexOf", NULL, &t5126_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5126_m26648_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26648_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1913_0_0_0;
static ParameterInfo t5126_m26649_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1913_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26649_GM;
MethodInfo m26649_MI = 
{
	"Insert", NULL, &t5126_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5126_m26649_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26649_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5126_m26650_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26650_GM;
MethodInfo m26650_MI = 
{
	"RemoveAt", NULL, &t5126_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5126_m26650_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26650_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5126_m26646_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1913_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26646_GM;
MethodInfo m26646_MI = 
{
	"get_Item", NULL, &t5126_TI, &t1913_0_0_0, RuntimeInvoker_t29_t44, t5126_m26646_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26646_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1913_0_0_0;
static ParameterInfo t5126_m26647_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1913_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26647_GM;
MethodInfo m26647_MI = 
{
	"set_Item", NULL, &t5126_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5126_m26647_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26647_GM};
static MethodInfo* t5126_MIs[] =
{
	&m26648_MI,
	&m26649_MI,
	&m26650_MI,
	&m26646_MI,
	&m26647_MI,
	NULL
};
static TypeInfo* t5126_ITIs[] = 
{
	&t603_TI,
	&t5125_TI,
	&t5127_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5126_0_0_0;
extern Il2CppType t5126_1_0_0;
struct t5126;
extern Il2CppGenericClass t5126_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5126_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5126_MIs, t5126_PIs, NULL, NULL, NULL, NULL, NULL, &t5126_TI, t5126_ITIs, NULL, &t1908__CustomAttributeCache, &t5126_TI, &t5126_0_0_0, &t5126_1_0_0, NULL, &t5126_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5128_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._Type>
extern MethodInfo m26651_MI;
static PropertyInfo t5128____Count_PropertyInfo = 
{
	&t5128_TI, "Count", &m26651_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26652_MI;
static PropertyInfo t5128____IsReadOnly_PropertyInfo = 
{
	&t5128_TI, "IsReadOnly", &m26652_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5128_PIs[] =
{
	&t5128____Count_PropertyInfo,
	&t5128____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26651_GM;
MethodInfo m26651_MI = 
{
	"get_Count", NULL, &t5128_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26651_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26652_GM;
MethodInfo m26652_MI = 
{
	"get_IsReadOnly", NULL, &t5128_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26652_GM};
extern Il2CppType t1914_0_0_0;
extern Il2CppType t1914_0_0_0;
static ParameterInfo t5128_m26653_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1914_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26653_GM;
MethodInfo m26653_MI = 
{
	"Add", NULL, &t5128_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5128_m26653_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26653_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26654_GM;
MethodInfo m26654_MI = 
{
	"Clear", NULL, &t5128_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26654_GM};
extern Il2CppType t1914_0_0_0;
static ParameterInfo t5128_m26655_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1914_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26655_GM;
MethodInfo m26655_MI = 
{
	"Contains", NULL, &t5128_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5128_m26655_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26655_GM};
extern Il2CppType t3526_0_0_0;
extern Il2CppType t3526_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5128_m26656_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3526_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26656_GM;
MethodInfo m26656_MI = 
{
	"CopyTo", NULL, &t5128_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5128_m26656_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26656_GM};
extern Il2CppType t1914_0_0_0;
static ParameterInfo t5128_m26657_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1914_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26657_GM;
MethodInfo m26657_MI = 
{
	"Remove", NULL, &t5128_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5128_m26657_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26657_GM};
static MethodInfo* t5128_MIs[] =
{
	&m26651_MI,
	&m26652_MI,
	&m26653_MI,
	&m26654_MI,
	&m26655_MI,
	&m26656_MI,
	&m26657_MI,
	NULL
};
extern TypeInfo t5130_TI;
static TypeInfo* t5128_ITIs[] = 
{
	&t603_TI,
	&t5130_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5128_0_0_0;
extern Il2CppType t5128_1_0_0;
struct t5128;
extern Il2CppGenericClass t5128_GC;
TypeInfo t5128_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5128_MIs, t5128_PIs, NULL, NULL, NULL, NULL, NULL, &t5128_TI, t5128_ITIs, NULL, &EmptyCustomAttributesCache, &t5128_TI, &t5128_0_0_0, &t5128_1_0_0, NULL, &t5128_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._Type>
extern Il2CppType t3993_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26658_GM;
MethodInfo m26658_MI = 
{
	"GetEnumerator", NULL, &t5130_TI, &t3993_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26658_GM};
static MethodInfo* t5130_MIs[] =
{
	&m26658_MI,
	NULL
};
static TypeInfo* t5130_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5130_0_0_0;
extern Il2CppType t5130_1_0_0;
struct t5130;
extern Il2CppGenericClass t5130_GC;
TypeInfo t5130_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5130_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5130_TI, t5130_ITIs, NULL, &EmptyCustomAttributesCache, &t5130_TI, &t5130_0_0_0, &t5130_1_0_0, NULL, &t5130_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3993_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._Type>
extern MethodInfo m26659_MI;
static PropertyInfo t3993____Current_PropertyInfo = 
{
	&t3993_TI, "Current", &m26659_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3993_PIs[] =
{
	&t3993____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1914_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26659_GM;
MethodInfo m26659_MI = 
{
	"get_Current", NULL, &t3993_TI, &t1914_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26659_GM};
static MethodInfo* t3993_MIs[] =
{
	&m26659_MI,
	NULL
};
static TypeInfo* t3993_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3993_0_0_0;
extern Il2CppType t3993_1_0_0;
struct t3993;
extern Il2CppGenericClass t3993_GC;
TypeInfo t3993_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t3993_MIs, t3993_PIs, NULL, NULL, NULL, NULL, NULL, &t3993_TI, t3993_ITIs, NULL, &EmptyCustomAttributesCache, &t3993_TI, &t3993_0_0_0, &t3993_1_0_0, NULL, &t3993_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2190.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2190_TI;
#include "t2190MD.h"

extern TypeInfo t1914_TI;
extern MethodInfo m10722_MI;
extern MethodInfo m19813_MI;
struct t20;
#define m19813(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices._Type>
extern Il2CppType t20_0_0_1;
FieldInfo t2190_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2190_TI, offsetof(t2190, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2190_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2190_TI, offsetof(t2190, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2190_FIs[] =
{
	&t2190_f0_FieldInfo,
	&t2190_f1_FieldInfo,
	NULL
};
extern MethodInfo m10719_MI;
static PropertyInfo t2190____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2190_TI, "System.Collections.IEnumerator.Current", &m10719_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2190____Current_PropertyInfo = 
{
	&t2190_TI, "Current", &m10722_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2190_PIs[] =
{
	&t2190____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2190____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2190_m10718_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10718_GM;
MethodInfo m10718_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2190_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2190_m10718_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10718_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10719_GM;
MethodInfo m10719_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2190_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10719_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10720_GM;
MethodInfo m10720_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2190_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10720_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10721_GM;
MethodInfo m10721_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2190_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10721_GM};
extern Il2CppType t1914_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10722_GM;
MethodInfo m10722_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2190_TI, &t1914_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10722_GM};
static MethodInfo* t2190_MIs[] =
{
	&m10718_MI,
	&m10719_MI,
	&m10720_MI,
	&m10721_MI,
	&m10722_MI,
	NULL
};
extern MethodInfo m10721_MI;
extern MethodInfo m10720_MI;
static MethodInfo* t2190_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10719_MI,
	&m10721_MI,
	&m10720_MI,
	&m10722_MI,
};
static TypeInfo* t2190_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3993_TI,
};
static Il2CppInterfaceOffsetPair t2190_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3993_TI, 7},
};
extern TypeInfo t1914_TI;
static Il2CppRGCTXData t2190_RGCTXData[3] = 
{
	&m10722_MI/* Method Usage */,
	&t1914_TI/* Class Usage */,
	&m19813_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2190_0_0_0;
extern Il2CppType t2190_1_0_0;
extern Il2CppGenericClass t2190_GC;
TypeInfo t2190_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2190_MIs, t2190_PIs, t2190_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2190_TI, t2190_ITIs, t2190_VT, &EmptyCustomAttributesCache, &t2190_TI, &t2190_0_0_0, &t2190_1_0_0, t2190_IOs, &t2190_GC, NULL, NULL, NULL, t2190_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2190)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5129_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices._Type>
extern MethodInfo m26660_MI;
extern MethodInfo m26661_MI;
static PropertyInfo t5129____Item_PropertyInfo = 
{
	&t5129_TI, "Item", &m26660_MI, &m26661_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5129_PIs[] =
{
	&t5129____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1914_0_0_0;
static ParameterInfo t5129_m26662_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1914_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26662_GM;
MethodInfo m26662_MI = 
{
	"IndexOf", NULL, &t5129_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5129_m26662_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26662_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1914_0_0_0;
static ParameterInfo t5129_m26663_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1914_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26663_GM;
MethodInfo m26663_MI = 
{
	"Insert", NULL, &t5129_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5129_m26663_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26663_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5129_m26664_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26664_GM;
MethodInfo m26664_MI = 
{
	"RemoveAt", NULL, &t5129_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5129_m26664_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26664_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5129_m26660_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1914_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26660_GM;
MethodInfo m26660_MI = 
{
	"get_Item", NULL, &t5129_TI, &t1914_0_0_0, RuntimeInvoker_t29_t44, t5129_m26660_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26660_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1914_0_0_0;
static ParameterInfo t5129_m26661_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1914_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26661_GM;
MethodInfo m26661_MI = 
{
	"set_Item", NULL, &t5129_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5129_m26661_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26661_GM};
static MethodInfo* t5129_MIs[] =
{
	&m26662_MI,
	&m26663_MI,
	&m26664_MI,
	&m26660_MI,
	&m26661_MI,
	NULL
};
static TypeInfo* t5129_ITIs[] = 
{
	&t603_TI,
	&t5128_TI,
	&t5130_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5129_0_0_0;
extern Il2CppType t5129_1_0_0;
struct t5129;
extern Il2CppGenericClass t5129_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5129_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5129_MIs, t5129_PIs, NULL, NULL, NULL, NULL, NULL, &t5129_TI, t5129_ITIs, NULL, &t1908__CustomAttributeCache, &t5129_TI, &t5129_0_0_0, &t5129_1_0_0, NULL, &t5129_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5131_TI;

#include "t296.h"


// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.MemberInfo>
extern MethodInfo m26665_MI;
static PropertyInfo t5131____Count_PropertyInfo = 
{
	&t5131_TI, "Count", &m26665_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26666_MI;
static PropertyInfo t5131____IsReadOnly_PropertyInfo = 
{
	&t5131_TI, "IsReadOnly", &m26666_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5131_PIs[] =
{
	&t5131____Count_PropertyInfo,
	&t5131____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26665_GM;
MethodInfo m26665_MI = 
{
	"get_Count", NULL, &t5131_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26665_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26666_GM;
MethodInfo m26666_MI = 
{
	"get_IsReadOnly", NULL, &t5131_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26666_GM};
extern Il2CppType t296_0_0_0;
extern Il2CppType t296_0_0_0;
static ParameterInfo t5131_m26667_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t296_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26667_GM;
MethodInfo m26667_MI = 
{
	"Add", NULL, &t5131_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5131_m26667_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26667_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26668_GM;
MethodInfo m26668_MI = 
{
	"Clear", NULL, &t5131_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26668_GM};
extern Il2CppType t296_0_0_0;
static ParameterInfo t5131_m26669_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t296_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26669_GM;
MethodInfo m26669_MI = 
{
	"Contains", NULL, &t5131_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5131_m26669_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26669_GM};
extern Il2CppType t1501_0_0_0;
extern Il2CppType t1501_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5131_m26670_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1501_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26670_GM;
MethodInfo m26670_MI = 
{
	"CopyTo", NULL, &t5131_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5131_m26670_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26670_GM};
extern Il2CppType t296_0_0_0;
static ParameterInfo t5131_m26671_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t296_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26671_GM;
MethodInfo m26671_MI = 
{
	"Remove", NULL, &t5131_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5131_m26671_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26671_GM};
static MethodInfo* t5131_MIs[] =
{
	&m26665_MI,
	&m26666_MI,
	&m26667_MI,
	&m26668_MI,
	&m26669_MI,
	&m26670_MI,
	&m26671_MI,
	NULL
};
extern TypeInfo t5133_TI;
static TypeInfo* t5131_ITIs[] = 
{
	&t603_TI,
	&t5133_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5131_0_0_0;
extern Il2CppType t5131_1_0_0;
struct t5131;
extern Il2CppGenericClass t5131_GC;
TypeInfo t5131_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5131_MIs, t5131_PIs, NULL, NULL, NULL, NULL, NULL, &t5131_TI, t5131_ITIs, NULL, &EmptyCustomAttributesCache, &t5131_TI, &t5131_0_0_0, &t5131_1_0_0, NULL, &t5131_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo>
extern Il2CppType t3995_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26672_GM;
MethodInfo m26672_MI = 
{
	"GetEnumerator", NULL, &t5133_TI, &t3995_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26672_GM};
static MethodInfo* t5133_MIs[] =
{
	&m26672_MI,
	NULL
};
static TypeInfo* t5133_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5133_0_0_0;
extern Il2CppType t5133_1_0_0;
struct t5133;
extern Il2CppGenericClass t5133_GC;
TypeInfo t5133_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5133_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5133_TI, t5133_ITIs, NULL, &EmptyCustomAttributesCache, &t5133_TI, &t5133_0_0_0, &t5133_1_0_0, NULL, &t5133_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3995_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.MemberInfo>
extern MethodInfo m26673_MI;
static PropertyInfo t3995____Current_PropertyInfo = 
{
	&t3995_TI, "Current", &m26673_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3995_PIs[] =
{
	&t3995____Current_PropertyInfo,
	NULL
};
extern Il2CppType t296_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26673_GM;
MethodInfo m26673_MI = 
{
	"get_Current", NULL, &t3995_TI, &t296_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26673_GM};
static MethodInfo* t3995_MIs[] =
{
	&m26673_MI,
	NULL
};
static TypeInfo* t3995_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3995_0_0_0;
extern Il2CppType t3995_1_0_0;
struct t3995;
extern Il2CppGenericClass t3995_GC;
TypeInfo t3995_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t3995_MIs, t3995_PIs, NULL, NULL, NULL, NULL, NULL, &t3995_TI, t3995_ITIs, NULL, &EmptyCustomAttributesCache, &t3995_TI, &t3995_0_0_0, &t3995_1_0_0, NULL, &t3995_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2191.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2191_TI;
#include "t2191MD.h"

extern TypeInfo t296_TI;
extern MethodInfo m10727_MI;
extern MethodInfo m19824_MI;
struct t20;
#define m19824(__this, p0, method) (t296 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.MemberInfo>
extern Il2CppType t20_0_0_1;
FieldInfo t2191_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2191_TI, offsetof(t2191, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2191_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2191_TI, offsetof(t2191, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2191_FIs[] =
{
	&t2191_f0_FieldInfo,
	&t2191_f1_FieldInfo,
	NULL
};
extern MethodInfo m10724_MI;
static PropertyInfo t2191____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2191_TI, "System.Collections.IEnumerator.Current", &m10724_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2191____Current_PropertyInfo = 
{
	&t2191_TI, "Current", &m10727_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2191_PIs[] =
{
	&t2191____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2191____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2191_m10723_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10723_GM;
MethodInfo m10723_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2191_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2191_m10723_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10723_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10724_GM;
MethodInfo m10724_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2191_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10724_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10725_GM;
MethodInfo m10725_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2191_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10725_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10726_GM;
MethodInfo m10726_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2191_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10726_GM};
extern Il2CppType t296_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10727_GM;
MethodInfo m10727_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2191_TI, &t296_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10727_GM};
static MethodInfo* t2191_MIs[] =
{
	&m10723_MI,
	&m10724_MI,
	&m10725_MI,
	&m10726_MI,
	&m10727_MI,
	NULL
};
extern MethodInfo m10726_MI;
extern MethodInfo m10725_MI;
static MethodInfo* t2191_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10724_MI,
	&m10726_MI,
	&m10725_MI,
	&m10727_MI,
};
static TypeInfo* t2191_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3995_TI,
};
static Il2CppInterfaceOffsetPair t2191_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3995_TI, 7},
};
extern TypeInfo t296_TI;
static Il2CppRGCTXData t2191_RGCTXData[3] = 
{
	&m10727_MI/* Method Usage */,
	&t296_TI/* Class Usage */,
	&m19824_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2191_0_0_0;
extern Il2CppType t2191_1_0_0;
extern Il2CppGenericClass t2191_GC;
TypeInfo t2191_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2191_MIs, t2191_PIs, t2191_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2191_TI, t2191_ITIs, t2191_VT, &EmptyCustomAttributesCache, &t2191_TI, &t2191_0_0_0, &t2191_1_0_0, t2191_IOs, &t2191_GC, NULL, NULL, NULL, t2191_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2191)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5132_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.MemberInfo>
extern MethodInfo m26674_MI;
extern MethodInfo m26675_MI;
static PropertyInfo t5132____Item_PropertyInfo = 
{
	&t5132_TI, "Item", &m26674_MI, &m26675_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5132_PIs[] =
{
	&t5132____Item_PropertyInfo,
	NULL
};
extern Il2CppType t296_0_0_0;
static ParameterInfo t5132_m26676_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t296_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26676_GM;
MethodInfo m26676_MI = 
{
	"IndexOf", NULL, &t5132_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5132_m26676_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26676_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t296_0_0_0;
static ParameterInfo t5132_m26677_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t296_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26677_GM;
MethodInfo m26677_MI = 
{
	"Insert", NULL, &t5132_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5132_m26677_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26677_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5132_m26678_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26678_GM;
MethodInfo m26678_MI = 
{
	"RemoveAt", NULL, &t5132_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5132_m26678_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26678_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5132_m26674_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t296_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26674_GM;
MethodInfo m26674_MI = 
{
	"get_Item", NULL, &t5132_TI, &t296_0_0_0, RuntimeInvoker_t29_t44, t5132_m26674_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26674_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t296_0_0_0;
static ParameterInfo t5132_m26675_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t296_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26675_GM;
MethodInfo m26675_MI = 
{
	"set_Item", NULL, &t5132_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5132_m26675_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26675_GM};
static MethodInfo* t5132_MIs[] =
{
	&m26676_MI,
	&m26677_MI,
	&m26678_MI,
	&m26674_MI,
	&m26675_MI,
	NULL
};
static TypeInfo* t5132_ITIs[] = 
{
	&t603_TI,
	&t5131_TI,
	&t5133_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5132_0_0_0;
extern Il2CppType t5132_1_0_0;
struct t5132;
extern Il2CppGenericClass t5132_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5132_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5132_MIs, t5132_PIs, NULL, NULL, NULL, NULL, NULL, &t5132_TI, t5132_ITIs, NULL, &t1908__CustomAttributeCache, &t5132_TI, &t5132_0_0_0, &t5132_1_0_0, NULL, &t5132_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5134_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.ICustomAttributeProvider>
extern MethodInfo m26679_MI;
static PropertyInfo t5134____Count_PropertyInfo = 
{
	&t5134_TI, "Count", &m26679_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26680_MI;
static PropertyInfo t5134____IsReadOnly_PropertyInfo = 
{
	&t5134_TI, "IsReadOnly", &m26680_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5134_PIs[] =
{
	&t5134____Count_PropertyInfo,
	&t5134____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26679_GM;
MethodInfo m26679_MI = 
{
	"get_Count", NULL, &t5134_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26679_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26680_GM;
MethodInfo m26680_MI = 
{
	"get_IsReadOnly", NULL, &t5134_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26680_GM};
extern Il2CppType t1657_0_0_0;
extern Il2CppType t1657_0_0_0;
static ParameterInfo t5134_m26681_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1657_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26681_GM;
MethodInfo m26681_MI = 
{
	"Add", NULL, &t5134_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5134_m26681_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26681_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26682_GM;
MethodInfo m26682_MI = 
{
	"Clear", NULL, &t5134_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26682_GM};
extern Il2CppType t1657_0_0_0;
static ParameterInfo t5134_m26683_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1657_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26683_GM;
MethodInfo m26683_MI = 
{
	"Contains", NULL, &t5134_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5134_m26683_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26683_GM};
extern Il2CppType t3527_0_0_0;
extern Il2CppType t3527_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5134_m26684_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3527_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26684_GM;
MethodInfo m26684_MI = 
{
	"CopyTo", NULL, &t5134_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5134_m26684_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26684_GM};
extern Il2CppType t1657_0_0_0;
static ParameterInfo t5134_m26685_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1657_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26685_GM;
MethodInfo m26685_MI = 
{
	"Remove", NULL, &t5134_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5134_m26685_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26685_GM};
static MethodInfo* t5134_MIs[] =
{
	&m26679_MI,
	&m26680_MI,
	&m26681_MI,
	&m26682_MI,
	&m26683_MI,
	&m26684_MI,
	&m26685_MI,
	NULL
};
extern TypeInfo t5136_TI;
static TypeInfo* t5134_ITIs[] = 
{
	&t603_TI,
	&t5136_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5134_0_0_0;
extern Il2CppType t5134_1_0_0;
struct t5134;
extern Il2CppGenericClass t5134_GC;
TypeInfo t5134_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5134_MIs, t5134_PIs, NULL, NULL, NULL, NULL, NULL, &t5134_TI, t5134_ITIs, NULL, &EmptyCustomAttributesCache, &t5134_TI, &t5134_0_0_0, &t5134_1_0_0, NULL, &t5134_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.ICustomAttributeProvider>
extern Il2CppType t3997_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26686_GM;
MethodInfo m26686_MI = 
{
	"GetEnumerator", NULL, &t5136_TI, &t3997_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26686_GM};
static MethodInfo* t5136_MIs[] =
{
	&m26686_MI,
	NULL
};
static TypeInfo* t5136_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5136_0_0_0;
extern Il2CppType t5136_1_0_0;
struct t5136;
extern Il2CppGenericClass t5136_GC;
TypeInfo t5136_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5136_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5136_TI, t5136_ITIs, NULL, &EmptyCustomAttributesCache, &t5136_TI, &t5136_0_0_0, &t5136_1_0_0, NULL, &t5136_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3997_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.ICustomAttributeProvider>
extern MethodInfo m26687_MI;
static PropertyInfo t3997____Current_PropertyInfo = 
{
	&t3997_TI, "Current", &m26687_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3997_PIs[] =
{
	&t3997____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1657_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26687_GM;
MethodInfo m26687_MI = 
{
	"get_Current", NULL, &t3997_TI, &t1657_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26687_GM};
static MethodInfo* t3997_MIs[] =
{
	&m26687_MI,
	NULL
};
static TypeInfo* t3997_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3997_0_0_0;
extern Il2CppType t3997_1_0_0;
struct t3997;
extern Il2CppGenericClass t3997_GC;
TypeInfo t3997_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t3997_MIs, t3997_PIs, NULL, NULL, NULL, NULL, NULL, &t3997_TI, t3997_ITIs, NULL, &EmptyCustomAttributesCache, &t3997_TI, &t3997_0_0_0, &t3997_1_0_0, NULL, &t3997_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2192.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2192_TI;
#include "t2192MD.h"

extern TypeInfo t1657_TI;
extern MethodInfo m10732_MI;
extern MethodInfo m19835_MI;
struct t20;
#define m19835(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.ICustomAttributeProvider>
extern Il2CppType t20_0_0_1;
FieldInfo t2192_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2192_TI, offsetof(t2192, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2192_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2192_TI, offsetof(t2192, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2192_FIs[] =
{
	&t2192_f0_FieldInfo,
	&t2192_f1_FieldInfo,
	NULL
};
extern MethodInfo m10729_MI;
static PropertyInfo t2192____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2192_TI, "System.Collections.IEnumerator.Current", &m10729_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2192____Current_PropertyInfo = 
{
	&t2192_TI, "Current", &m10732_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2192_PIs[] =
{
	&t2192____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2192____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2192_m10728_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10728_GM;
MethodInfo m10728_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2192_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2192_m10728_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10728_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10729_GM;
MethodInfo m10729_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2192_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10729_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10730_GM;
MethodInfo m10730_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2192_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10730_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10731_GM;
MethodInfo m10731_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2192_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10731_GM};
extern Il2CppType t1657_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10732_GM;
MethodInfo m10732_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2192_TI, &t1657_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10732_GM};
static MethodInfo* t2192_MIs[] =
{
	&m10728_MI,
	&m10729_MI,
	&m10730_MI,
	&m10731_MI,
	&m10732_MI,
	NULL
};
extern MethodInfo m10731_MI;
extern MethodInfo m10730_MI;
static MethodInfo* t2192_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10729_MI,
	&m10731_MI,
	&m10730_MI,
	&m10732_MI,
};
static TypeInfo* t2192_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3997_TI,
};
static Il2CppInterfaceOffsetPair t2192_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3997_TI, 7},
};
extern TypeInfo t1657_TI;
static Il2CppRGCTXData t2192_RGCTXData[3] = 
{
	&m10732_MI/* Method Usage */,
	&t1657_TI/* Class Usage */,
	&m19835_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2192_0_0_0;
extern Il2CppType t2192_1_0_0;
extern Il2CppGenericClass t2192_GC;
TypeInfo t2192_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2192_MIs, t2192_PIs, t2192_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2192_TI, t2192_ITIs, t2192_VT, &EmptyCustomAttributesCache, &t2192_TI, &t2192_0_0_0, &t2192_1_0_0, t2192_IOs, &t2192_GC, NULL, NULL, NULL, t2192_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2192)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5135_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.ICustomAttributeProvider>
extern MethodInfo m26688_MI;
extern MethodInfo m26689_MI;
static PropertyInfo t5135____Item_PropertyInfo = 
{
	&t5135_TI, "Item", &m26688_MI, &m26689_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5135_PIs[] =
{
	&t5135____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1657_0_0_0;
static ParameterInfo t5135_m26690_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1657_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26690_GM;
MethodInfo m26690_MI = 
{
	"IndexOf", NULL, &t5135_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5135_m26690_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26690_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1657_0_0_0;
static ParameterInfo t5135_m26691_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1657_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26691_GM;
MethodInfo m26691_MI = 
{
	"Insert", NULL, &t5135_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5135_m26691_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26691_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5135_m26692_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26692_GM;
MethodInfo m26692_MI = 
{
	"RemoveAt", NULL, &t5135_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5135_m26692_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26692_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5135_m26688_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1657_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26688_GM;
MethodInfo m26688_MI = 
{
	"get_Item", NULL, &t5135_TI, &t1657_0_0_0, RuntimeInvoker_t29_t44, t5135_m26688_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26688_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1657_0_0_0;
static ParameterInfo t5135_m26689_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1657_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26689_GM;
MethodInfo m26689_MI = 
{
	"set_Item", NULL, &t5135_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5135_m26689_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26689_GM};
static MethodInfo* t5135_MIs[] =
{
	&m26690_MI,
	&m26691_MI,
	&m26692_MI,
	&m26688_MI,
	&m26689_MI,
	NULL
};
static TypeInfo* t5135_ITIs[] = 
{
	&t603_TI,
	&t5134_TI,
	&t5136_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5135_0_0_0;
extern Il2CppType t5135_1_0_0;
struct t5135;
extern Il2CppGenericClass t5135_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5135_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5135_MIs, t5135_PIs, NULL, NULL, NULL, NULL, NULL, &t5135_TI, t5135_ITIs, NULL, &t1908__CustomAttributeCache, &t5135_TI, &t5135_0_0_0, &t5135_1_0_0, NULL, &t5135_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5137_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MemberInfo>
extern MethodInfo m26693_MI;
static PropertyInfo t5137____Count_PropertyInfo = 
{
	&t5137_TI, "Count", &m26693_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26694_MI;
static PropertyInfo t5137____IsReadOnly_PropertyInfo = 
{
	&t5137_TI, "IsReadOnly", &m26694_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5137_PIs[] =
{
	&t5137____Count_PropertyInfo,
	&t5137____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26693_GM;
MethodInfo m26693_MI = 
{
	"get_Count", NULL, &t5137_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26693_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26694_GM;
MethodInfo m26694_MI = 
{
	"get_IsReadOnly", NULL, &t5137_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26694_GM};
extern Il2CppType t1915_0_0_0;
extern Il2CppType t1915_0_0_0;
static ParameterInfo t5137_m26695_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1915_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26695_GM;
MethodInfo m26695_MI = 
{
	"Add", NULL, &t5137_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5137_m26695_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26695_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26696_GM;
MethodInfo m26696_MI = 
{
	"Clear", NULL, &t5137_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26696_GM};
extern Il2CppType t1915_0_0_0;
static ParameterInfo t5137_m26697_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1915_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26697_GM;
MethodInfo m26697_MI = 
{
	"Contains", NULL, &t5137_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5137_m26697_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26697_GM};
extern Il2CppType t3528_0_0_0;
extern Il2CppType t3528_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5137_m26698_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3528_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26698_GM;
MethodInfo m26698_MI = 
{
	"CopyTo", NULL, &t5137_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5137_m26698_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26698_GM};
extern Il2CppType t1915_0_0_0;
static ParameterInfo t5137_m26699_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1915_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26699_GM;
MethodInfo m26699_MI = 
{
	"Remove", NULL, &t5137_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5137_m26699_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26699_GM};
static MethodInfo* t5137_MIs[] =
{
	&m26693_MI,
	&m26694_MI,
	&m26695_MI,
	&m26696_MI,
	&m26697_MI,
	&m26698_MI,
	&m26699_MI,
	NULL
};
extern TypeInfo t5139_TI;
static TypeInfo* t5137_ITIs[] = 
{
	&t603_TI,
	&t5139_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5137_0_0_0;
extern Il2CppType t5137_1_0_0;
struct t5137;
extern Il2CppGenericClass t5137_GC;
TypeInfo t5137_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5137_MIs, t5137_PIs, NULL, NULL, NULL, NULL, NULL, &t5137_TI, t5137_ITIs, NULL, &EmptyCustomAttributesCache, &t5137_TI, &t5137_0_0_0, &t5137_1_0_0, NULL, &t5137_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
