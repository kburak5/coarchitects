﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1701;
struct t1701_marshaled;

void t1701_marshal(const t1701& unmarshaled, t1701_marshaled& marshaled);
void t1701_marshal_back(const t1701_marshaled& marshaled, t1701& unmarshaled);
void t1701_marshal_cleanup(t1701_marshaled& marshaled);
