﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t994;
struct t7;
struct t781;

 void m4436 (t994 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4437 (t994 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4438 (t994 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4439 (t994 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4440 (t994 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4441 (t994 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4442 (t994 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
