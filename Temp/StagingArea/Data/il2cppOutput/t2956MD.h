﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2956;
struct t29;
struct t20;
#include "t35.h"

 void m16156 (t2956 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m16157 (t2956 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16158 (t2956 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16159 (t2956 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t35 m16160 (t2956 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
