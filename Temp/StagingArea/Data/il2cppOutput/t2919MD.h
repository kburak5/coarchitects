﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2919;
struct t29;
struct t20;
#include "t383.h"

 void m16006 (t2919 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m16007 (t2919 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16008 (t2919 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16009 (t2919 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16010 (t2919 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
