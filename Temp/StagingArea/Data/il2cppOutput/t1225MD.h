﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1225;
struct t782;
struct t7;
struct t781;
struct t1219;
struct t733;
#include "t465.h"
#include "t735.h"

 void m6448 (t1225 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6449 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6450 (t1225 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6451 (t1225 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t782 * m6452 (t1225 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6453 (t1225 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6454 (t1225 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6455 (t1225 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6456 (t1225 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6457 (t1225 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m6458 (t1225 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m6459 (t1225 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1219 * m6460 (t1225 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1219 * m6461 (t1225 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6462 (t1225 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6463 (t29 * __this, t7* p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
