﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t786;
struct t781;
struct t7;
#include "t787.h"

 void m3287 (t786 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3288 (t786 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3289 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3290 (t786 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3291 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3292 (t29 * __this, t786 * p0, t786 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
