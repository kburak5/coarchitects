﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t971;
struct t972;

 void m4294 (t971 * __this, t972 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4295 (t971 * __this, t972 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t972 * m4296 (t971 * __this, t972 * p0, t972 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t972 * m4297 (t971 * __this, t972 * p0, t972 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t972 * m4298 (t971 * __this, t972 * p0, t972 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t972 * m4299 (t971 * __this, uint32_t p0, t972 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
