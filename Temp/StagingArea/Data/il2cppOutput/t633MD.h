﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t633;
struct t7;
struct t1191;
struct t1119;
struct t1125;
struct t1296;
struct t29;
struct t42;

 void m6836 (t633 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6837 (t633 * __this, int32_t p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6838 (t633 * __this, int32_t p0, bool p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6839 (t633 * __this, t7* p0, bool p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6840 (t633 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6841 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t633 * m4009 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t633 * m5212 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t633 * m5216 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t633 * m6842 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t633 * m6843 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6844 (t633 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6845 (t633 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t633 * m6846 (t633 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1191 * m6847 (t633 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6848 (t633 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6849 (t633 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6850 (t633 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6851 (t633 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1119 * m5213 (t633 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6852 (t633 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6853 (t633 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1125 * m6854 (t633 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1296 * m6855 (t633 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6856 (t633 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m6857 (t633 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6858 (t633 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6859 (t633 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6860 (t633 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6861 (t29 * __this, t633 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6862 (t633 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6863 (t633 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6864 (t29 * __this, t633 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6865 (t633 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6866 (t633 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6867 (t633 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1191 * m6868 (t633 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t633 * m6869 (t29 * __this, t7* p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
