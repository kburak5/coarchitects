﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1533;
struct t781;

 void m8245 (t1533 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8246 (t1533 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8247 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8248 (t1533 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
