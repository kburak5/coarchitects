﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1504;
struct t1458;
struct t1507;
struct t29;
struct t1508;
struct t841;
struct t7;
struct t296;
struct t733;
#include "t735.h"

 void m8088 (t1504 * __this, t29 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8089 (t1504 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1507 * m8090 (t1504 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m8091 (t1504 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8092 (t1504 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8093 (t1504 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8094 (t1504 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8095 (t1504 * __this, t1508 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8096 (t1504 * __this, int64_t p0, int32_t p1, int64_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8097 (t1504 * __this, int64_t p0, t841* p1, int64_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8098 (t1504 * __this, int64_t p0, t7* p1, int64_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8099 (t1504 * __this, int64_t p0, t296 * p1, int64_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8100 (t1504 * __this, t29 * p0, t1507 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8101 (t1504 * __this, t29 * p0, int64_t p1, t733 * p2, int64_t p3, t296 * p4, t841* p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
