﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1583;
struct t1575;
struct t29;

 void m8579 (t1583 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1575 * m8580 (t1583 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8581 (t1583 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8582 (t1583 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
