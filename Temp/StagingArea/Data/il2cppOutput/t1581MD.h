﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1581;
struct t7;
struct t1576;
struct t29;

 void m8568 (t1581 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8569 (t1581 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8570 (t1581 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1576 * m8571 (t1581 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8572 (t1581 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8573 (t1581 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
