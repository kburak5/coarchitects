﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2111;
struct t29;
struct t20;

 void m10288_gshared (t2111 * __this, t20 * p0, MethodInfo* method);
#define m10288(__this, p0, method) (void)m10288_gshared((t2111 *)__this, (t20 *)p0, method)
 t29 * m10290_gshared (t2111 * __this, MethodInfo* method);
#define m10290(__this, method) (t29 *)m10290_gshared((t2111 *)__this, method)
 void m10292_gshared (t2111 * __this, MethodInfo* method);
#define m10292(__this, method) (void)m10292_gshared((t2111 *)__this, method)
 bool m10294_gshared (t2111 * __this, MethodInfo* method);
#define m10294(__this, method) (bool)m10294_gshared((t2111 *)__this, method)
 t29 * m10296_gshared (t2111 * __this, MethodInfo* method);
#define m10296(__this, method) (t29 *)m10296_gshared((t2111 *)__this, method)
