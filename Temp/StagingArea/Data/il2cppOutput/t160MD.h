﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t160;
struct t136;
struct t4;
#include "t131.h"

 void m1560 (t160 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m13408 (t29 * __this, t131  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1561 (t160 * __this, t4 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1595 (t160 * __this, t131  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
