﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t273;
struct t155;
struct t2;
struct t24;
struct t156;
#include "t17.h"

 void m1210 (t273 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t155 * m1211 (t273 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1212 (t273 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1213 (t273 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2 * m1214 (t273 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1215 (t273 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1216 (t273 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1217 (t273 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1218 (t273 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1219 (t273 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1220 (t273 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1221 (t273 * __this, t17  p0, t24 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t156 * m1222 (t273 * __this, t156 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
