﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1652;
struct t7;
struct t733;
#include "t735.h"

 void m9297 (t1652 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9298 (t1652 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9299 (t1652 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9300 (t1652 * __this, t7* p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9301 (t1652 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m9302 (t1652 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
