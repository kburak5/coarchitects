﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1672;
struct t1299;
#include "t816.h"
#include "t465.h"

 void m9557 (t1672 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9558 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1672 * m9559 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9560 (t1672 * __this, t465  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9561 (t29 * __this, t465  p0, t1299 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m9562 (t1672 * __this, t465  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m9563 (t1672 * __this, t465  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t816  m9564 (t1672 * __this, t465  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t816  m9565 (t1672 * __this, t465  p0, t816  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
