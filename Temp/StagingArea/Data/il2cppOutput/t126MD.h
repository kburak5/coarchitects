﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t126;
struct t7;
struct t446;
#include "t126.h"

 int32_t m2066 (t126 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2067 (t126 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2068 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2069 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2070 (t29 * __this, t446* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1481 (t29 * __this, t126  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t126  m1482 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
