﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t485;
struct t29;
struct t7;
struct t2909;
struct t2096;
struct t733;
struct t2910;
struct t20;
struct t136;
struct t2912;
struct t722;
#include "t735.h"
#include "t2911.h"
#include "t2913.h"
#include "t725.h"

 void m15906 (t485 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15907 (t485 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4038 (t485 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15908 (t485 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15909 (t485 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15910 (t485 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15911 (t485 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15912 (t485 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15913 (t485 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15914 (t485 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15915 (t485 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15916 (t485 * __this, t2911  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15917 (t485 * __this, t2911  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15918 (t485 * __this, t2910* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15919 (t485 * __this, t2911  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15920 (t485 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15921 (t485 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m15922 (t485 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15923 (t485 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m15924 (t485 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m15925 (t485 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15926 (t485 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15927 (t485 * __this, int32_t p0, t29* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15928 (t485 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15929 (t485 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2911  m15930 (t29 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m15931 (t29 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15932 (t485 * __this, t2910* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15933 (t485 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4039 (t485 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15934 (t485 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15935 (t485 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15936 (t485 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15937 (t485 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15938 (t485 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15939 (t485 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4040 (t485 * __this, t7* p0, int32_t* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2909 * m15940 (t485 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m15941 (t485 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m15942 (t485 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15943 (t485 * __this, t2911  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2913  m15944 (t485 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t725  m15945 (t29 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
