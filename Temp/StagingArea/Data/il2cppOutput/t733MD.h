﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t733;
struct t42;
struct t1521;
struct t7;
struct t29;
struct t1522;
#include "t465.h"

 void m8143 (t733 * __this, t42 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3982 (t733 * __this, t7* p0, t29 * p1, t42 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3985 (t733 * __this, t7* p0, t42 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8144 (t733 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1522 * m8145 (t733 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8146 (t733 * __this, t7* p0, int16_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3984 (t733 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3983 (t733 * __this, t7* p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8147 (t733 * __this, t7* p0, t465  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8148 (t733 * __this, t7* p0, float p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3998 (t733 * __this, t7* p0, int64_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8149 (t733 * __this, t7* p0, uint64_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3997 (t733 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3987 (t733 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m8150 (t733 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3996 (t733 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m3995 (t733 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3994 (t733 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
