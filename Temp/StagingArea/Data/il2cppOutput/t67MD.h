﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t67;
struct t29;
struct t66;
#include "t35.h"

 void m5231 (t67 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6071 (t67 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m5229 (t67 * __this, t29 * p0, t67 * p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6072 (t67 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
