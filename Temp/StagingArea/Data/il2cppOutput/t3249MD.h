﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3249;
struct t29;
struct t20;
#include "t1016.h"

 void m18041 (t3249 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18042 (t3249 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18043 (t3249 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18044 (t3249 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m18045 (t3249 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
