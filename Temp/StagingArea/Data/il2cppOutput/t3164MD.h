﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3164;
struct t557;
struct t7;
struct t29;
struct t556;

 void m17593_gshared (t3164 * __this, MethodInfo* method);
#define m17593(__this, method) (void)m17593_gshared((t3164 *)__this, method)
 t557 * m17594_gshared (t3164 * __this, t7* p0, t29 * p1, MethodInfo* method);
#define m17594(__this, p0, p1, method) (t557 *)m17594_gshared((t3164 *)__this, (t7*)p0, (t29 *)p1, method)
 t556 * m17595_gshared (t3164 * __this, t29 * p0, t557 * p1, MethodInfo* method);
#define m17595(__this, p0, p1, method) (t556 *)m17595_gshared((t3164 *)__this, (t29 *)p0, (t557 *)p1, method)
