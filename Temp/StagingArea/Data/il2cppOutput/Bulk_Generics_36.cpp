﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t6570_TI;

#include "t44.h"
#include "t40.h"
#include "t21.h"
#include "t1568.h"
#include "mscorlib_ArrayTypes.h"

#include "t20.h"

// Metadata Definition System.Collections.Generic.ICollection`1<System.Security.Principal.PrincipalPolicy>
extern MethodInfo m33985_MI;
static PropertyInfo t6570____Count_PropertyInfo = 
{
	&t6570_TI, "Count", &m33985_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33986_MI;
static PropertyInfo t6570____IsReadOnly_PropertyInfo = 
{
	&t6570_TI, "IsReadOnly", &m33986_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6570_PIs[] =
{
	&t6570____Count_PropertyInfo,
	&t6570____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33985_GM;
MethodInfo m33985_MI = 
{
	"get_Count", NULL, &t6570_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33985_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33986_GM;
MethodInfo m33986_MI = 
{
	"get_IsReadOnly", NULL, &t6570_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33986_GM};
extern Il2CppType t1568_0_0_0;
extern Il2CppType t1568_0_0_0;
static ParameterInfo t6570_m33987_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1568_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33987_GM;
MethodInfo m33987_MI = 
{
	"Add", NULL, &t6570_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6570_m33987_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33987_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33988_GM;
MethodInfo m33988_MI = 
{
	"Clear", NULL, &t6570_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33988_GM};
extern Il2CppType t1568_0_0_0;
static ParameterInfo t6570_m33989_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1568_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33989_GM;
MethodInfo m33989_MI = 
{
	"Contains", NULL, &t6570_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6570_m33989_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33989_GM};
extern Il2CppType t3709_0_0_0;
extern Il2CppType t3709_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6570_m33990_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3709_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33990_GM;
MethodInfo m33990_MI = 
{
	"CopyTo", NULL, &t6570_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6570_m33990_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33990_GM};
extern Il2CppType t1568_0_0_0;
static ParameterInfo t6570_m33991_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1568_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33991_GM;
MethodInfo m33991_MI = 
{
	"Remove", NULL, &t6570_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6570_m33991_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33991_GM};
static MethodInfo* t6570_MIs[] =
{
	&m33985_MI,
	&m33986_MI,
	&m33987_MI,
	&m33988_MI,
	&m33989_MI,
	&m33990_MI,
	&m33991_MI,
	NULL
};
extern TypeInfo t603_TI;
extern TypeInfo t6572_TI;
static TypeInfo* t6570_ITIs[] = 
{
	&t603_TI,
	&t6572_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6570_0_0_0;
extern Il2CppType t6570_1_0_0;
struct t6570;
extern Il2CppGenericClass t6570_GC;
TypeInfo t6570_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6570_MIs, t6570_PIs, NULL, NULL, NULL, NULL, NULL, &t6570_TI, t6570_ITIs, NULL, &EmptyCustomAttributesCache, &t6570_TI, &t6570_0_0_0, &t6570_1_0_0, NULL, &t6570_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Security.Principal.PrincipalPolicy>
extern Il2CppType t5019_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33992_GM;
MethodInfo m33992_MI = 
{
	"GetEnumerator", NULL, &t6572_TI, &t5019_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33992_GM};
static MethodInfo* t6572_MIs[] =
{
	&m33992_MI,
	NULL
};
static TypeInfo* t6572_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6572_0_0_0;
extern Il2CppType t6572_1_0_0;
struct t6572;
extern Il2CppGenericClass t6572_GC;
TypeInfo t6572_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6572_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6572_TI, t6572_ITIs, NULL, &EmptyCustomAttributesCache, &t6572_TI, &t6572_0_0_0, &t6572_1_0_0, NULL, &t6572_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6571_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Security.Principal.PrincipalPolicy>
extern MethodInfo m33993_MI;
extern MethodInfo m33994_MI;
static PropertyInfo t6571____Item_PropertyInfo = 
{
	&t6571_TI, "Item", &m33993_MI, &m33994_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6571_PIs[] =
{
	&t6571____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1568_0_0_0;
static ParameterInfo t6571_m33995_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1568_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33995_GM;
MethodInfo m33995_MI = 
{
	"IndexOf", NULL, &t6571_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6571_m33995_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33995_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1568_0_0_0;
static ParameterInfo t6571_m33996_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1568_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33996_GM;
MethodInfo m33996_MI = 
{
	"Insert", NULL, &t6571_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6571_m33996_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33996_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6571_m33997_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33997_GM;
MethodInfo m33997_MI = 
{
	"RemoveAt", NULL, &t6571_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6571_m33997_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33997_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6571_m33993_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1568_0_0_0;
extern void* RuntimeInvoker_t1568_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33993_GM;
MethodInfo m33993_MI = 
{
	"get_Item", NULL, &t6571_TI, &t1568_0_0_0, RuntimeInvoker_t1568_t44, t6571_m33993_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33993_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1568_0_0_0;
static ParameterInfo t6571_m33994_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1568_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33994_GM;
MethodInfo m33994_MI = 
{
	"set_Item", NULL, &t6571_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6571_m33994_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33994_GM};
static MethodInfo* t6571_MIs[] =
{
	&m33995_MI,
	&m33996_MI,
	&m33997_MI,
	&m33993_MI,
	&m33994_MI,
	NULL
};
static TypeInfo* t6571_ITIs[] = 
{
	&t603_TI,
	&t6570_TI,
	&t6572_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6571_0_0_0;
extern Il2CppType t6571_1_0_0;
struct t6571;
extern Il2CppGenericClass t6571_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6571_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6571_MIs, t6571_PIs, NULL, NULL, NULL, NULL, NULL, &t6571_TI, t6571_ITIs, NULL, &t1908__CustomAttributeCache, &t6571_TI, &t6571_0_0_0, &t6571_1_0_0, NULL, &t6571_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5021_TI;

#include "t615.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Security.SecuritySafeCriticalAttribute>
extern MethodInfo m33998_MI;
static PropertyInfo t5021____Current_PropertyInfo = 
{
	&t5021_TI, "Current", &m33998_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5021_PIs[] =
{
	&t5021____Current_PropertyInfo,
	NULL
};
extern Il2CppType t615_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33998_GM;
MethodInfo m33998_MI = 
{
	"get_Current", NULL, &t5021_TI, &t615_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33998_GM};
static MethodInfo* t5021_MIs[] =
{
	&m33998_MI,
	NULL
};
extern TypeInfo t136_TI;
extern TypeInfo t324_TI;
static TypeInfo* t5021_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5021_0_0_0;
extern Il2CppType t5021_1_0_0;
struct t5021;
extern Il2CppGenericClass t5021_GC;
TypeInfo t5021_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t5021_MIs, t5021_PIs, NULL, NULL, NULL, NULL, NULL, &t5021_TI, t5021_ITIs, NULL, &EmptyCustomAttributesCache, &t5021_TI, &t5021_0_0_0, &t5021_1_0_0, NULL, &t5021_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3476.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3476_TI;
#include "t3476MD.h"

#include "t29.h"
#include "t7.h"
#include "t914.h"
extern TypeInfo t615_TI;
extern TypeInfo t914_TI;
#include "t914MD.h"
#include "t20MD.h"
extern MethodInfo m19331_MI;
extern MethodInfo m3964_MI;
extern MethodInfo m3969_MI;
extern MethodInfo m26055_MI;
struct t20;
#include "t915.h"
struct t20;
 t29 * m19490_gshared (t20 * __this, int32_t p0, MethodInfo* method);
#define m19490(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
#define m26055(__this, p0, method) (t615 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Security.SecuritySafeCriticalAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3476_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3476_TI, offsetof(t3476, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3476_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3476_TI, offsetof(t3476, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3476_FIs[] =
{
	&t3476_f0_FieldInfo,
	&t3476_f1_FieldInfo,
	NULL
};
extern MethodInfo m19328_MI;
static PropertyInfo t3476____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3476_TI, "System.Collections.IEnumerator.Current", &m19328_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3476____Current_PropertyInfo = 
{
	&t3476_TI, "Current", &m19331_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3476_PIs[] =
{
	&t3476____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3476____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
static ParameterInfo t3476_m19327_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19327_GM;
MethodInfo m19327_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3476_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3476_m19327_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19327_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19328_GM;
MethodInfo m19328_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3476_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19328_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19329_GM;
MethodInfo m19329_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3476_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19329_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19330_GM;
MethodInfo m19330_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3476_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19330_GM};
extern Il2CppType t615_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19331_GM;
MethodInfo m19331_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3476_TI, &t615_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19331_GM};
static MethodInfo* t3476_MIs[] =
{
	&m19327_MI,
	&m19328_MI,
	&m19329_MI,
	&m19330_MI,
	&m19331_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
extern MethodInfo m19330_MI;
extern MethodInfo m19329_MI;
static MethodInfo* t3476_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19328_MI,
	&m19330_MI,
	&m19329_MI,
	&m19331_MI,
};
static TypeInfo* t3476_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t5021_TI,
};
static Il2CppInterfaceOffsetPair t3476_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t5021_TI, 7},
};
extern TypeInfo t615_TI;
static Il2CppRGCTXData t3476_RGCTXData[3] = 
{
	&m19331_MI/* Method Usage */,
	&t615_TI/* Class Usage */,
	&m26055_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3476_0_0_0;
extern Il2CppType t3476_1_0_0;
extern TypeInfo t110_TI;
extern Il2CppGenericClass t3476_GC;
extern TypeInfo t20_TI;
TypeInfo t3476_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3476_MIs, t3476_PIs, t3476_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3476_TI, t3476_ITIs, t3476_VT, &EmptyCustomAttributesCache, &t3476_TI, &t3476_0_0_0, &t3476_1_0_0, t3476_IOs, &t3476_GC, NULL, NULL, NULL, t3476_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3476)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6573_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Security.SecuritySafeCriticalAttribute>
extern MethodInfo m33999_MI;
static PropertyInfo t6573____Count_PropertyInfo = 
{
	&t6573_TI, "Count", &m33999_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m34000_MI;
static PropertyInfo t6573____IsReadOnly_PropertyInfo = 
{
	&t6573_TI, "IsReadOnly", &m34000_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6573_PIs[] =
{
	&t6573____Count_PropertyInfo,
	&t6573____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33999_GM;
MethodInfo m33999_MI = 
{
	"get_Count", NULL, &t6573_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33999_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34000_GM;
MethodInfo m34000_MI = 
{
	"get_IsReadOnly", NULL, &t6573_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34000_GM};
extern Il2CppType t615_0_0_0;
extern Il2CppType t615_0_0_0;
static ParameterInfo t6573_m34001_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t615_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34001_GM;
MethodInfo m34001_MI = 
{
	"Add", NULL, &t6573_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6573_m34001_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34001_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34002_GM;
MethodInfo m34002_MI = 
{
	"Clear", NULL, &t6573_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34002_GM};
extern Il2CppType t615_0_0_0;
static ParameterInfo t6573_m34003_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t615_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34003_GM;
MethodInfo m34003_MI = 
{
	"Contains", NULL, &t6573_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6573_m34003_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34003_GM};
extern Il2CppType t3710_0_0_0;
extern Il2CppType t3710_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6573_m34004_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3710_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34004_GM;
MethodInfo m34004_MI = 
{
	"CopyTo", NULL, &t6573_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6573_m34004_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34004_GM};
extern Il2CppType t615_0_0_0;
static ParameterInfo t6573_m34005_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t615_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34005_GM;
MethodInfo m34005_MI = 
{
	"Remove", NULL, &t6573_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6573_m34005_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34005_GM};
static MethodInfo* t6573_MIs[] =
{
	&m33999_MI,
	&m34000_MI,
	&m34001_MI,
	&m34002_MI,
	&m34003_MI,
	&m34004_MI,
	&m34005_MI,
	NULL
};
extern TypeInfo t6575_TI;
static TypeInfo* t6573_ITIs[] = 
{
	&t603_TI,
	&t6575_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6573_0_0_0;
extern Il2CppType t6573_1_0_0;
struct t6573;
extern Il2CppGenericClass t6573_GC;
TypeInfo t6573_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6573_MIs, t6573_PIs, NULL, NULL, NULL, NULL, NULL, &t6573_TI, t6573_ITIs, NULL, &EmptyCustomAttributesCache, &t6573_TI, &t6573_0_0_0, &t6573_1_0_0, NULL, &t6573_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Security.SecuritySafeCriticalAttribute>
extern Il2CppType t5021_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34006_GM;
MethodInfo m34006_MI = 
{
	"GetEnumerator", NULL, &t6575_TI, &t5021_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34006_GM};
static MethodInfo* t6575_MIs[] =
{
	&m34006_MI,
	NULL
};
static TypeInfo* t6575_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6575_0_0_0;
extern Il2CppType t6575_1_0_0;
struct t6575;
extern Il2CppGenericClass t6575_GC;
TypeInfo t6575_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6575_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6575_TI, t6575_ITIs, NULL, &EmptyCustomAttributesCache, &t6575_TI, &t6575_0_0_0, &t6575_1_0_0, NULL, &t6575_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6574_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Security.SecuritySafeCriticalAttribute>
extern MethodInfo m34007_MI;
extern MethodInfo m34008_MI;
static PropertyInfo t6574____Item_PropertyInfo = 
{
	&t6574_TI, "Item", &m34007_MI, &m34008_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6574_PIs[] =
{
	&t6574____Item_PropertyInfo,
	NULL
};
extern Il2CppType t615_0_0_0;
static ParameterInfo t6574_m34009_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t615_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34009_GM;
MethodInfo m34009_MI = 
{
	"IndexOf", NULL, &t6574_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6574_m34009_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34009_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t615_0_0_0;
static ParameterInfo t6574_m34010_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t615_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34010_GM;
MethodInfo m34010_MI = 
{
	"Insert", NULL, &t6574_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6574_m34010_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34010_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6574_m34011_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34011_GM;
MethodInfo m34011_MI = 
{
	"RemoveAt", NULL, &t6574_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6574_m34011_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34011_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6574_m34007_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t615_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34007_GM;
MethodInfo m34007_MI = 
{
	"get_Item", NULL, &t6574_TI, &t615_0_0_0, RuntimeInvoker_t29_t44, t6574_m34007_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34007_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t615_0_0_0;
static ParameterInfo t6574_m34008_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t615_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34008_GM;
MethodInfo m34008_MI = 
{
	"set_Item", NULL, &t6574_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6574_m34008_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34008_GM};
static MethodInfo* t6574_MIs[] =
{
	&m34009_MI,
	&m34010_MI,
	&m34011_MI,
	&m34007_MI,
	&m34008_MI,
	NULL
};
static TypeInfo* t6574_ITIs[] = 
{
	&t603_TI,
	&t6573_TI,
	&t6575_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6574_0_0_0;
extern Il2CppType t6574_1_0_0;
struct t6574;
extern Il2CppGenericClass t6574_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6574_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6574_MIs, t6574_PIs, NULL, NULL, NULL, NULL, NULL, &t6574_TI, t6574_ITIs, NULL, &t1908__CustomAttributeCache, &t6574_TI, &t6574_0_0_0, &t6574_1_0_0, NULL, &t6574_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5023_TI;

#include "t1572.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Security.SuppressUnmanagedCodeSecurityAttribute>
extern MethodInfo m34012_MI;
static PropertyInfo t5023____Current_PropertyInfo = 
{
	&t5023_TI, "Current", &m34012_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5023_PIs[] =
{
	&t5023____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1572_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34012_GM;
MethodInfo m34012_MI = 
{
	"get_Current", NULL, &t5023_TI, &t1572_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34012_GM};
static MethodInfo* t5023_MIs[] =
{
	&m34012_MI,
	NULL
};
static TypeInfo* t5023_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5023_0_0_0;
extern Il2CppType t5023_1_0_0;
struct t5023;
extern Il2CppGenericClass t5023_GC;
TypeInfo t5023_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t5023_MIs, t5023_PIs, NULL, NULL, NULL, NULL, NULL, &t5023_TI, t5023_ITIs, NULL, &EmptyCustomAttributesCache, &t5023_TI, &t5023_0_0_0, &t5023_1_0_0, NULL, &t5023_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3477.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3477_TI;
#include "t3477MD.h"

extern TypeInfo t1572_TI;
extern MethodInfo m19336_MI;
extern MethodInfo m26066_MI;
struct t20;
#define m26066(__this, p0, method) (t1572 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Security.SuppressUnmanagedCodeSecurityAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3477_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3477_TI, offsetof(t3477, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3477_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3477_TI, offsetof(t3477, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3477_FIs[] =
{
	&t3477_f0_FieldInfo,
	&t3477_f1_FieldInfo,
	NULL
};
extern MethodInfo m19333_MI;
static PropertyInfo t3477____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3477_TI, "System.Collections.IEnumerator.Current", &m19333_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3477____Current_PropertyInfo = 
{
	&t3477_TI, "Current", &m19336_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3477_PIs[] =
{
	&t3477____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3477____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3477_m19332_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19332_GM;
MethodInfo m19332_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3477_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3477_m19332_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19332_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19333_GM;
MethodInfo m19333_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3477_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19333_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19334_GM;
MethodInfo m19334_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3477_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19334_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19335_GM;
MethodInfo m19335_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3477_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19335_GM};
extern Il2CppType t1572_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19336_GM;
MethodInfo m19336_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3477_TI, &t1572_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19336_GM};
static MethodInfo* t3477_MIs[] =
{
	&m19332_MI,
	&m19333_MI,
	&m19334_MI,
	&m19335_MI,
	&m19336_MI,
	NULL
};
extern MethodInfo m19335_MI;
extern MethodInfo m19334_MI;
static MethodInfo* t3477_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19333_MI,
	&m19335_MI,
	&m19334_MI,
	&m19336_MI,
};
static TypeInfo* t3477_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t5023_TI,
};
static Il2CppInterfaceOffsetPair t3477_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t5023_TI, 7},
};
extern TypeInfo t1572_TI;
static Il2CppRGCTXData t3477_RGCTXData[3] = 
{
	&m19336_MI/* Method Usage */,
	&t1572_TI/* Class Usage */,
	&m26066_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3477_0_0_0;
extern Il2CppType t3477_1_0_0;
extern Il2CppGenericClass t3477_GC;
TypeInfo t3477_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3477_MIs, t3477_PIs, t3477_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3477_TI, t3477_ITIs, t3477_VT, &EmptyCustomAttributesCache, &t3477_TI, &t3477_0_0_0, &t3477_1_0_0, t3477_IOs, &t3477_GC, NULL, NULL, NULL, t3477_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3477)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6576_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Security.SuppressUnmanagedCodeSecurityAttribute>
extern MethodInfo m34013_MI;
static PropertyInfo t6576____Count_PropertyInfo = 
{
	&t6576_TI, "Count", &m34013_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m34014_MI;
static PropertyInfo t6576____IsReadOnly_PropertyInfo = 
{
	&t6576_TI, "IsReadOnly", &m34014_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6576_PIs[] =
{
	&t6576____Count_PropertyInfo,
	&t6576____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34013_GM;
MethodInfo m34013_MI = 
{
	"get_Count", NULL, &t6576_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34013_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34014_GM;
MethodInfo m34014_MI = 
{
	"get_IsReadOnly", NULL, &t6576_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34014_GM};
extern Il2CppType t1572_0_0_0;
extern Il2CppType t1572_0_0_0;
static ParameterInfo t6576_m34015_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1572_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34015_GM;
MethodInfo m34015_MI = 
{
	"Add", NULL, &t6576_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6576_m34015_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34015_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34016_GM;
MethodInfo m34016_MI = 
{
	"Clear", NULL, &t6576_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34016_GM};
extern Il2CppType t1572_0_0_0;
static ParameterInfo t6576_m34017_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1572_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34017_GM;
MethodInfo m34017_MI = 
{
	"Contains", NULL, &t6576_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6576_m34017_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34017_GM};
extern Il2CppType t3711_0_0_0;
extern Il2CppType t3711_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6576_m34018_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3711_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34018_GM;
MethodInfo m34018_MI = 
{
	"CopyTo", NULL, &t6576_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6576_m34018_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34018_GM};
extern Il2CppType t1572_0_0_0;
static ParameterInfo t6576_m34019_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1572_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34019_GM;
MethodInfo m34019_MI = 
{
	"Remove", NULL, &t6576_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6576_m34019_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34019_GM};
static MethodInfo* t6576_MIs[] =
{
	&m34013_MI,
	&m34014_MI,
	&m34015_MI,
	&m34016_MI,
	&m34017_MI,
	&m34018_MI,
	&m34019_MI,
	NULL
};
extern TypeInfo t6578_TI;
static TypeInfo* t6576_ITIs[] = 
{
	&t603_TI,
	&t6578_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6576_0_0_0;
extern Il2CppType t6576_1_0_0;
struct t6576;
extern Il2CppGenericClass t6576_GC;
TypeInfo t6576_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6576_MIs, t6576_PIs, NULL, NULL, NULL, NULL, NULL, &t6576_TI, t6576_ITIs, NULL, &EmptyCustomAttributesCache, &t6576_TI, &t6576_0_0_0, &t6576_1_0_0, NULL, &t6576_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Security.SuppressUnmanagedCodeSecurityAttribute>
extern Il2CppType t5023_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34020_GM;
MethodInfo m34020_MI = 
{
	"GetEnumerator", NULL, &t6578_TI, &t5023_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34020_GM};
static MethodInfo* t6578_MIs[] =
{
	&m34020_MI,
	NULL
};
static TypeInfo* t6578_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6578_0_0_0;
extern Il2CppType t6578_1_0_0;
struct t6578;
extern Il2CppGenericClass t6578_GC;
TypeInfo t6578_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6578_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6578_TI, t6578_ITIs, NULL, &EmptyCustomAttributesCache, &t6578_TI, &t6578_0_0_0, &t6578_1_0_0, NULL, &t6578_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6577_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Security.SuppressUnmanagedCodeSecurityAttribute>
extern MethodInfo m34021_MI;
extern MethodInfo m34022_MI;
static PropertyInfo t6577____Item_PropertyInfo = 
{
	&t6577_TI, "Item", &m34021_MI, &m34022_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6577_PIs[] =
{
	&t6577____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1572_0_0_0;
static ParameterInfo t6577_m34023_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1572_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34023_GM;
MethodInfo m34023_MI = 
{
	"IndexOf", NULL, &t6577_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6577_m34023_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34023_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1572_0_0_0;
static ParameterInfo t6577_m34024_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1572_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34024_GM;
MethodInfo m34024_MI = 
{
	"Insert", NULL, &t6577_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6577_m34024_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34024_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6577_m34025_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34025_GM;
MethodInfo m34025_MI = 
{
	"RemoveAt", NULL, &t6577_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6577_m34025_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34025_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6577_m34021_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1572_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34021_GM;
MethodInfo m34021_MI = 
{
	"get_Item", NULL, &t6577_TI, &t1572_0_0_0, RuntimeInvoker_t29_t44, t6577_m34021_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34021_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1572_0_0_0;
static ParameterInfo t6577_m34022_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1572_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34022_GM;
MethodInfo m34022_MI = 
{
	"set_Item", NULL, &t6577_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6577_m34022_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34022_GM};
static MethodInfo* t6577_MIs[] =
{
	&m34023_MI,
	&m34024_MI,
	&m34025_MI,
	&m34021_MI,
	&m34022_MI,
	NULL
};
static TypeInfo* t6577_ITIs[] = 
{
	&t603_TI,
	&t6576_TI,
	&t6578_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6577_0_0_0;
extern Il2CppType t6577_1_0_0;
struct t6577;
extern Il2CppGenericClass t6577_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6577_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6577_MIs, t6577_PIs, NULL, NULL, NULL, NULL, NULL, &t6577_TI, t6577_ITIs, NULL, &t1908__CustomAttributeCache, &t6577_TI, &t6577_0_0_0, &t6577_1_0_0, NULL, &t6577_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5025_TI;

#include "t1573.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Security.UnverifiableCodeAttribute>
extern MethodInfo m34026_MI;
static PropertyInfo t5025____Current_PropertyInfo = 
{
	&t5025_TI, "Current", &m34026_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5025_PIs[] =
{
	&t5025____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1573_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34026_GM;
MethodInfo m34026_MI = 
{
	"get_Current", NULL, &t5025_TI, &t1573_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34026_GM};
static MethodInfo* t5025_MIs[] =
{
	&m34026_MI,
	NULL
};
static TypeInfo* t5025_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5025_0_0_0;
extern Il2CppType t5025_1_0_0;
struct t5025;
extern Il2CppGenericClass t5025_GC;
TypeInfo t5025_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t5025_MIs, t5025_PIs, NULL, NULL, NULL, NULL, NULL, &t5025_TI, t5025_ITIs, NULL, &EmptyCustomAttributesCache, &t5025_TI, &t5025_0_0_0, &t5025_1_0_0, NULL, &t5025_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3478.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3478_TI;
#include "t3478MD.h"

extern TypeInfo t1573_TI;
extern MethodInfo m19341_MI;
extern MethodInfo m26077_MI;
struct t20;
#define m26077(__this, p0, method) (t1573 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Security.UnverifiableCodeAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3478_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3478_TI, offsetof(t3478, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3478_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3478_TI, offsetof(t3478, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3478_FIs[] =
{
	&t3478_f0_FieldInfo,
	&t3478_f1_FieldInfo,
	NULL
};
extern MethodInfo m19338_MI;
static PropertyInfo t3478____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3478_TI, "System.Collections.IEnumerator.Current", &m19338_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3478____Current_PropertyInfo = 
{
	&t3478_TI, "Current", &m19341_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3478_PIs[] =
{
	&t3478____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3478____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3478_m19337_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19337_GM;
MethodInfo m19337_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3478_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3478_m19337_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19337_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19338_GM;
MethodInfo m19338_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3478_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19338_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19339_GM;
MethodInfo m19339_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3478_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19339_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19340_GM;
MethodInfo m19340_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3478_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19340_GM};
extern Il2CppType t1573_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19341_GM;
MethodInfo m19341_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3478_TI, &t1573_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19341_GM};
static MethodInfo* t3478_MIs[] =
{
	&m19337_MI,
	&m19338_MI,
	&m19339_MI,
	&m19340_MI,
	&m19341_MI,
	NULL
};
extern MethodInfo m19340_MI;
extern MethodInfo m19339_MI;
static MethodInfo* t3478_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19338_MI,
	&m19340_MI,
	&m19339_MI,
	&m19341_MI,
};
static TypeInfo* t3478_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t5025_TI,
};
static Il2CppInterfaceOffsetPair t3478_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t5025_TI, 7},
};
extern TypeInfo t1573_TI;
static Il2CppRGCTXData t3478_RGCTXData[3] = 
{
	&m19341_MI/* Method Usage */,
	&t1573_TI/* Class Usage */,
	&m26077_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3478_0_0_0;
extern Il2CppType t3478_1_0_0;
extern Il2CppGenericClass t3478_GC;
TypeInfo t3478_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3478_MIs, t3478_PIs, t3478_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3478_TI, t3478_ITIs, t3478_VT, &EmptyCustomAttributesCache, &t3478_TI, &t3478_0_0_0, &t3478_1_0_0, t3478_IOs, &t3478_GC, NULL, NULL, NULL, t3478_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3478)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6579_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Security.UnverifiableCodeAttribute>
extern MethodInfo m34027_MI;
static PropertyInfo t6579____Count_PropertyInfo = 
{
	&t6579_TI, "Count", &m34027_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m34028_MI;
static PropertyInfo t6579____IsReadOnly_PropertyInfo = 
{
	&t6579_TI, "IsReadOnly", &m34028_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6579_PIs[] =
{
	&t6579____Count_PropertyInfo,
	&t6579____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34027_GM;
MethodInfo m34027_MI = 
{
	"get_Count", NULL, &t6579_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34027_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34028_GM;
MethodInfo m34028_MI = 
{
	"get_IsReadOnly", NULL, &t6579_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34028_GM};
extern Il2CppType t1573_0_0_0;
extern Il2CppType t1573_0_0_0;
static ParameterInfo t6579_m34029_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1573_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34029_GM;
MethodInfo m34029_MI = 
{
	"Add", NULL, &t6579_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6579_m34029_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34029_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34030_GM;
MethodInfo m34030_MI = 
{
	"Clear", NULL, &t6579_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34030_GM};
extern Il2CppType t1573_0_0_0;
static ParameterInfo t6579_m34031_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1573_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34031_GM;
MethodInfo m34031_MI = 
{
	"Contains", NULL, &t6579_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6579_m34031_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34031_GM};
extern Il2CppType t3712_0_0_0;
extern Il2CppType t3712_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6579_m34032_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3712_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34032_GM;
MethodInfo m34032_MI = 
{
	"CopyTo", NULL, &t6579_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6579_m34032_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34032_GM};
extern Il2CppType t1573_0_0_0;
static ParameterInfo t6579_m34033_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1573_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34033_GM;
MethodInfo m34033_MI = 
{
	"Remove", NULL, &t6579_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6579_m34033_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34033_GM};
static MethodInfo* t6579_MIs[] =
{
	&m34027_MI,
	&m34028_MI,
	&m34029_MI,
	&m34030_MI,
	&m34031_MI,
	&m34032_MI,
	&m34033_MI,
	NULL
};
extern TypeInfo t6581_TI;
static TypeInfo* t6579_ITIs[] = 
{
	&t603_TI,
	&t6581_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6579_0_0_0;
extern Il2CppType t6579_1_0_0;
struct t6579;
extern Il2CppGenericClass t6579_GC;
TypeInfo t6579_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6579_MIs, t6579_PIs, NULL, NULL, NULL, NULL, NULL, &t6579_TI, t6579_ITIs, NULL, &EmptyCustomAttributesCache, &t6579_TI, &t6579_0_0_0, &t6579_1_0_0, NULL, &t6579_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Security.UnverifiableCodeAttribute>
extern Il2CppType t5025_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34034_GM;
MethodInfo m34034_MI = 
{
	"GetEnumerator", NULL, &t6581_TI, &t5025_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34034_GM};
static MethodInfo* t6581_MIs[] =
{
	&m34034_MI,
	NULL
};
static TypeInfo* t6581_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6581_0_0_0;
extern Il2CppType t6581_1_0_0;
struct t6581;
extern Il2CppGenericClass t6581_GC;
TypeInfo t6581_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6581_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6581_TI, t6581_ITIs, NULL, &EmptyCustomAttributesCache, &t6581_TI, &t6581_0_0_0, &t6581_1_0_0, NULL, &t6581_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6580_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Security.UnverifiableCodeAttribute>
extern MethodInfo m34035_MI;
extern MethodInfo m34036_MI;
static PropertyInfo t6580____Item_PropertyInfo = 
{
	&t6580_TI, "Item", &m34035_MI, &m34036_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6580_PIs[] =
{
	&t6580____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1573_0_0_0;
static ParameterInfo t6580_m34037_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1573_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34037_GM;
MethodInfo m34037_MI = 
{
	"IndexOf", NULL, &t6580_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6580_m34037_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34037_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1573_0_0_0;
static ParameterInfo t6580_m34038_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1573_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34038_GM;
MethodInfo m34038_MI = 
{
	"Insert", NULL, &t6580_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6580_m34038_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34038_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6580_m34039_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34039_GM;
MethodInfo m34039_MI = 
{
	"RemoveAt", NULL, &t6580_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6580_m34039_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34039_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6580_m34035_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1573_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34035_GM;
MethodInfo m34035_MI = 
{
	"get_Item", NULL, &t6580_TI, &t1573_0_0_0, RuntimeInvoker_t29_t44, t6580_m34035_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34035_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1573_0_0_0;
static ParameterInfo t6580_m34036_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1573_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34036_GM;
MethodInfo m34036_MI = 
{
	"set_Item", NULL, &t6580_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6580_m34036_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34036_GM};
static MethodInfo* t6580_MIs[] =
{
	&m34037_MI,
	&m34038_MI,
	&m34039_MI,
	&m34035_MI,
	&m34036_MI,
	NULL
};
static TypeInfo* t6580_ITIs[] = 
{
	&t603_TI,
	&t6579_TI,
	&t6581_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6580_0_0_0;
extern Il2CppType t6580_1_0_0;
struct t6580;
extern Il2CppGenericClass t6580_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6580_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6580_MIs, t6580_PIs, NULL, NULL, NULL, NULL, NULL, &t6580_TI, t6580_ITIs, NULL, &t1908__CustomAttributeCache, &t6580_TI, &t6580_0_0_0, &t6580_1_0_0, NULL, &t6580_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5027_TI;

#include "t1600.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Threading.EventResetMode>
extern MethodInfo m34040_MI;
static PropertyInfo t5027____Current_PropertyInfo = 
{
	&t5027_TI, "Current", &m34040_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5027_PIs[] =
{
	&t5027____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1600_0_0_0;
extern void* RuntimeInvoker_t1600 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34040_GM;
MethodInfo m34040_MI = 
{
	"get_Current", NULL, &t5027_TI, &t1600_0_0_0, RuntimeInvoker_t1600, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34040_GM};
static MethodInfo* t5027_MIs[] =
{
	&m34040_MI,
	NULL
};
static TypeInfo* t5027_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5027_0_0_0;
extern Il2CppType t5027_1_0_0;
struct t5027;
extern Il2CppGenericClass t5027_GC;
TypeInfo t5027_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t5027_MIs, t5027_PIs, NULL, NULL, NULL, NULL, NULL, &t5027_TI, t5027_ITIs, NULL, &EmptyCustomAttributesCache, &t5027_TI, &t5027_0_0_0, &t5027_1_0_0, NULL, &t5027_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3479.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3479_TI;
#include "t3479MD.h"

extern TypeInfo t1600_TI;
extern MethodInfo m19346_MI;
extern MethodInfo m26088_MI;
struct t20;
 int32_t m26088 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m19342_MI;
 void m19342 (t3479 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19343_MI;
 t29 * m19343 (t3479 * __this, MethodInfo* method){
	{
		int32_t L_0 = m19346(__this, &m19346_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1600_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m19344_MI;
 void m19344 (t3479 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19345_MI;
 bool m19345 (t3479 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m19346 (t3479 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m26088(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m26088_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Threading.EventResetMode>
extern Il2CppType t20_0_0_1;
FieldInfo t3479_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3479_TI, offsetof(t3479, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3479_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3479_TI, offsetof(t3479, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3479_FIs[] =
{
	&t3479_f0_FieldInfo,
	&t3479_f1_FieldInfo,
	NULL
};
static PropertyInfo t3479____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3479_TI, "System.Collections.IEnumerator.Current", &m19343_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3479____Current_PropertyInfo = 
{
	&t3479_TI, "Current", &m19346_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3479_PIs[] =
{
	&t3479____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3479____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3479_m19342_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19342_GM;
MethodInfo m19342_MI = 
{
	".ctor", (methodPointerType)&m19342, &t3479_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3479_m19342_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19342_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19343_GM;
MethodInfo m19343_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m19343, &t3479_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19343_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19344_GM;
MethodInfo m19344_MI = 
{
	"Dispose", (methodPointerType)&m19344, &t3479_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19344_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19345_GM;
MethodInfo m19345_MI = 
{
	"MoveNext", (methodPointerType)&m19345, &t3479_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19345_GM};
extern Il2CppType t1600_0_0_0;
extern void* RuntimeInvoker_t1600 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19346_GM;
MethodInfo m19346_MI = 
{
	"get_Current", (methodPointerType)&m19346, &t3479_TI, &t1600_0_0_0, RuntimeInvoker_t1600, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19346_GM};
static MethodInfo* t3479_MIs[] =
{
	&m19342_MI,
	&m19343_MI,
	&m19344_MI,
	&m19345_MI,
	&m19346_MI,
	NULL
};
static MethodInfo* t3479_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19343_MI,
	&m19345_MI,
	&m19344_MI,
	&m19346_MI,
};
static TypeInfo* t3479_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t5027_TI,
};
static Il2CppInterfaceOffsetPair t3479_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t5027_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3479_0_0_0;
extern Il2CppType t3479_1_0_0;
extern Il2CppGenericClass t3479_GC;
TypeInfo t3479_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3479_MIs, t3479_PIs, t3479_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3479_TI, t3479_ITIs, t3479_VT, &EmptyCustomAttributesCache, &t3479_TI, &t3479_0_0_0, &t3479_1_0_0, t3479_IOs, &t3479_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3479)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6582_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Threading.EventResetMode>
extern MethodInfo m34041_MI;
static PropertyInfo t6582____Count_PropertyInfo = 
{
	&t6582_TI, "Count", &m34041_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m34042_MI;
static PropertyInfo t6582____IsReadOnly_PropertyInfo = 
{
	&t6582_TI, "IsReadOnly", &m34042_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6582_PIs[] =
{
	&t6582____Count_PropertyInfo,
	&t6582____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34041_GM;
MethodInfo m34041_MI = 
{
	"get_Count", NULL, &t6582_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34041_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34042_GM;
MethodInfo m34042_MI = 
{
	"get_IsReadOnly", NULL, &t6582_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34042_GM};
extern Il2CppType t1600_0_0_0;
extern Il2CppType t1600_0_0_0;
static ParameterInfo t6582_m34043_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1600_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34043_GM;
MethodInfo m34043_MI = 
{
	"Add", NULL, &t6582_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6582_m34043_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34043_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34044_GM;
MethodInfo m34044_MI = 
{
	"Clear", NULL, &t6582_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34044_GM};
extern Il2CppType t1600_0_0_0;
static ParameterInfo t6582_m34045_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1600_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34045_GM;
MethodInfo m34045_MI = 
{
	"Contains", NULL, &t6582_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6582_m34045_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34045_GM};
extern Il2CppType t3713_0_0_0;
extern Il2CppType t3713_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6582_m34046_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3713_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34046_GM;
MethodInfo m34046_MI = 
{
	"CopyTo", NULL, &t6582_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6582_m34046_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34046_GM};
extern Il2CppType t1600_0_0_0;
static ParameterInfo t6582_m34047_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1600_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34047_GM;
MethodInfo m34047_MI = 
{
	"Remove", NULL, &t6582_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6582_m34047_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34047_GM};
static MethodInfo* t6582_MIs[] =
{
	&m34041_MI,
	&m34042_MI,
	&m34043_MI,
	&m34044_MI,
	&m34045_MI,
	&m34046_MI,
	&m34047_MI,
	NULL
};
extern TypeInfo t6584_TI;
static TypeInfo* t6582_ITIs[] = 
{
	&t603_TI,
	&t6584_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6582_0_0_0;
extern Il2CppType t6582_1_0_0;
struct t6582;
extern Il2CppGenericClass t6582_GC;
TypeInfo t6582_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6582_MIs, t6582_PIs, NULL, NULL, NULL, NULL, NULL, &t6582_TI, t6582_ITIs, NULL, &EmptyCustomAttributesCache, &t6582_TI, &t6582_0_0_0, &t6582_1_0_0, NULL, &t6582_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Threading.EventResetMode>
extern Il2CppType t5027_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34048_GM;
MethodInfo m34048_MI = 
{
	"GetEnumerator", NULL, &t6584_TI, &t5027_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34048_GM};
static MethodInfo* t6584_MIs[] =
{
	&m34048_MI,
	NULL
};
static TypeInfo* t6584_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6584_0_0_0;
extern Il2CppType t6584_1_0_0;
struct t6584;
extern Il2CppGenericClass t6584_GC;
TypeInfo t6584_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6584_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6584_TI, t6584_ITIs, NULL, &EmptyCustomAttributesCache, &t6584_TI, &t6584_0_0_0, &t6584_1_0_0, NULL, &t6584_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6583_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Threading.EventResetMode>
extern MethodInfo m34049_MI;
extern MethodInfo m34050_MI;
static PropertyInfo t6583____Item_PropertyInfo = 
{
	&t6583_TI, "Item", &m34049_MI, &m34050_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6583_PIs[] =
{
	&t6583____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1600_0_0_0;
static ParameterInfo t6583_m34051_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1600_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34051_GM;
MethodInfo m34051_MI = 
{
	"IndexOf", NULL, &t6583_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6583_m34051_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34051_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1600_0_0_0;
static ParameterInfo t6583_m34052_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1600_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34052_GM;
MethodInfo m34052_MI = 
{
	"Insert", NULL, &t6583_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6583_m34052_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34052_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6583_m34053_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34053_GM;
MethodInfo m34053_MI = 
{
	"RemoveAt", NULL, &t6583_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6583_m34053_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34053_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6583_m34049_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1600_0_0_0;
extern void* RuntimeInvoker_t1600_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34049_GM;
MethodInfo m34049_MI = 
{
	"get_Item", NULL, &t6583_TI, &t1600_0_0_0, RuntimeInvoker_t1600_t44, t6583_m34049_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34049_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1600_0_0_0;
static ParameterInfo t6583_m34050_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1600_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34050_GM;
MethodInfo m34050_MI = 
{
	"set_Item", NULL, &t6583_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6583_m34050_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34050_GM};
static MethodInfo* t6583_MIs[] =
{
	&m34051_MI,
	&m34052_MI,
	&m34053_MI,
	&m34049_MI,
	&m34050_MI,
	NULL
};
static TypeInfo* t6583_ITIs[] = 
{
	&t603_TI,
	&t6582_TI,
	&t6584_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6583_0_0_0;
extern Il2CppType t6583_1_0_0;
struct t6583;
extern Il2CppGenericClass t6583_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6583_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6583_MIs, t6583_PIs, NULL, NULL, NULL, NULL, NULL, &t6583_TI, t6583_ITIs, NULL, &t1908__CustomAttributeCache, &t6583_TI, &t6583_0_0_0, &t6583_1_0_0, NULL, &t6583_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5029_TI;

#include "t1605.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Threading.ThreadState>
extern MethodInfo m34054_MI;
static PropertyInfo t5029____Current_PropertyInfo = 
{
	&t5029_TI, "Current", &m34054_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5029_PIs[] =
{
	&t5029____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1605_0_0_0;
extern void* RuntimeInvoker_t1605 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34054_GM;
MethodInfo m34054_MI = 
{
	"get_Current", NULL, &t5029_TI, &t1605_0_0_0, RuntimeInvoker_t1605, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34054_GM};
static MethodInfo* t5029_MIs[] =
{
	&m34054_MI,
	NULL
};
static TypeInfo* t5029_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5029_0_0_0;
extern Il2CppType t5029_1_0_0;
struct t5029;
extern Il2CppGenericClass t5029_GC;
TypeInfo t5029_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t5029_MIs, t5029_PIs, NULL, NULL, NULL, NULL, NULL, &t5029_TI, t5029_ITIs, NULL, &EmptyCustomAttributesCache, &t5029_TI, &t5029_0_0_0, &t5029_1_0_0, NULL, &t5029_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3480.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3480_TI;
#include "t3480MD.h"

extern TypeInfo t1605_TI;
extern MethodInfo m19351_MI;
extern MethodInfo m26099_MI;
struct t20;
 int32_t m26099 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m19347_MI;
 void m19347 (t3480 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19348_MI;
 t29 * m19348 (t3480 * __this, MethodInfo* method){
	{
		int32_t L_0 = m19351(__this, &m19351_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1605_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m19349_MI;
 void m19349 (t3480 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19350_MI;
 bool m19350 (t3480 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m19351 (t3480 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m26099(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m26099_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Threading.ThreadState>
extern Il2CppType t20_0_0_1;
FieldInfo t3480_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3480_TI, offsetof(t3480, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3480_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3480_TI, offsetof(t3480, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3480_FIs[] =
{
	&t3480_f0_FieldInfo,
	&t3480_f1_FieldInfo,
	NULL
};
static PropertyInfo t3480____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3480_TI, "System.Collections.IEnumerator.Current", &m19348_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3480____Current_PropertyInfo = 
{
	&t3480_TI, "Current", &m19351_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3480_PIs[] =
{
	&t3480____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3480____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3480_m19347_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19347_GM;
MethodInfo m19347_MI = 
{
	".ctor", (methodPointerType)&m19347, &t3480_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3480_m19347_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19347_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19348_GM;
MethodInfo m19348_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m19348, &t3480_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19348_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19349_GM;
MethodInfo m19349_MI = 
{
	"Dispose", (methodPointerType)&m19349, &t3480_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19349_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19350_GM;
MethodInfo m19350_MI = 
{
	"MoveNext", (methodPointerType)&m19350, &t3480_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19350_GM};
extern Il2CppType t1605_0_0_0;
extern void* RuntimeInvoker_t1605 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19351_GM;
MethodInfo m19351_MI = 
{
	"get_Current", (methodPointerType)&m19351, &t3480_TI, &t1605_0_0_0, RuntimeInvoker_t1605, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19351_GM};
static MethodInfo* t3480_MIs[] =
{
	&m19347_MI,
	&m19348_MI,
	&m19349_MI,
	&m19350_MI,
	&m19351_MI,
	NULL
};
static MethodInfo* t3480_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19348_MI,
	&m19350_MI,
	&m19349_MI,
	&m19351_MI,
};
static TypeInfo* t3480_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t5029_TI,
};
static Il2CppInterfaceOffsetPair t3480_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t5029_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3480_0_0_0;
extern Il2CppType t3480_1_0_0;
extern Il2CppGenericClass t3480_GC;
TypeInfo t3480_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3480_MIs, t3480_PIs, t3480_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3480_TI, t3480_ITIs, t3480_VT, &EmptyCustomAttributesCache, &t3480_TI, &t3480_0_0_0, &t3480_1_0_0, t3480_IOs, &t3480_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3480)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6585_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Threading.ThreadState>
extern MethodInfo m34055_MI;
static PropertyInfo t6585____Count_PropertyInfo = 
{
	&t6585_TI, "Count", &m34055_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m34056_MI;
static PropertyInfo t6585____IsReadOnly_PropertyInfo = 
{
	&t6585_TI, "IsReadOnly", &m34056_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6585_PIs[] =
{
	&t6585____Count_PropertyInfo,
	&t6585____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34055_GM;
MethodInfo m34055_MI = 
{
	"get_Count", NULL, &t6585_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34055_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34056_GM;
MethodInfo m34056_MI = 
{
	"get_IsReadOnly", NULL, &t6585_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34056_GM};
extern Il2CppType t1605_0_0_0;
extern Il2CppType t1605_0_0_0;
static ParameterInfo t6585_m34057_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1605_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34057_GM;
MethodInfo m34057_MI = 
{
	"Add", NULL, &t6585_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6585_m34057_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34057_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34058_GM;
MethodInfo m34058_MI = 
{
	"Clear", NULL, &t6585_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34058_GM};
extern Il2CppType t1605_0_0_0;
static ParameterInfo t6585_m34059_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1605_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34059_GM;
MethodInfo m34059_MI = 
{
	"Contains", NULL, &t6585_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6585_m34059_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34059_GM};
extern Il2CppType t3714_0_0_0;
extern Il2CppType t3714_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6585_m34060_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3714_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34060_GM;
MethodInfo m34060_MI = 
{
	"CopyTo", NULL, &t6585_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6585_m34060_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34060_GM};
extern Il2CppType t1605_0_0_0;
static ParameterInfo t6585_m34061_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1605_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34061_GM;
MethodInfo m34061_MI = 
{
	"Remove", NULL, &t6585_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6585_m34061_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34061_GM};
static MethodInfo* t6585_MIs[] =
{
	&m34055_MI,
	&m34056_MI,
	&m34057_MI,
	&m34058_MI,
	&m34059_MI,
	&m34060_MI,
	&m34061_MI,
	NULL
};
extern TypeInfo t6587_TI;
static TypeInfo* t6585_ITIs[] = 
{
	&t603_TI,
	&t6587_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6585_0_0_0;
extern Il2CppType t6585_1_0_0;
struct t6585;
extern Il2CppGenericClass t6585_GC;
TypeInfo t6585_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6585_MIs, t6585_PIs, NULL, NULL, NULL, NULL, NULL, &t6585_TI, t6585_ITIs, NULL, &EmptyCustomAttributesCache, &t6585_TI, &t6585_0_0_0, &t6585_1_0_0, NULL, &t6585_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Threading.ThreadState>
extern Il2CppType t5029_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34062_GM;
MethodInfo m34062_MI = 
{
	"GetEnumerator", NULL, &t6587_TI, &t5029_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34062_GM};
static MethodInfo* t6587_MIs[] =
{
	&m34062_MI,
	NULL
};
static TypeInfo* t6587_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6587_0_0_0;
extern Il2CppType t6587_1_0_0;
struct t6587;
extern Il2CppGenericClass t6587_GC;
TypeInfo t6587_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6587_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6587_TI, t6587_ITIs, NULL, &EmptyCustomAttributesCache, &t6587_TI, &t6587_0_0_0, &t6587_1_0_0, NULL, &t6587_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6586_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Threading.ThreadState>
extern MethodInfo m34063_MI;
extern MethodInfo m34064_MI;
static PropertyInfo t6586____Item_PropertyInfo = 
{
	&t6586_TI, "Item", &m34063_MI, &m34064_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6586_PIs[] =
{
	&t6586____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1605_0_0_0;
static ParameterInfo t6586_m34065_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1605_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34065_GM;
MethodInfo m34065_MI = 
{
	"IndexOf", NULL, &t6586_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6586_m34065_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34065_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1605_0_0_0;
static ParameterInfo t6586_m34066_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1605_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34066_GM;
MethodInfo m34066_MI = 
{
	"Insert", NULL, &t6586_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6586_m34066_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34066_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6586_m34067_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34067_GM;
MethodInfo m34067_MI = 
{
	"RemoveAt", NULL, &t6586_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6586_m34067_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34067_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6586_m34063_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1605_0_0_0;
extern void* RuntimeInvoker_t1605_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34063_GM;
MethodInfo m34063_MI = 
{
	"get_Item", NULL, &t6586_TI, &t1605_0_0_0, RuntimeInvoker_t1605_t44, t6586_m34063_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34063_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1605_0_0_0;
static ParameterInfo t6586_m34064_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1605_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34064_GM;
MethodInfo m34064_MI = 
{
	"set_Item", NULL, &t6586_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6586_m34064_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34064_GM};
static MethodInfo* t6586_MIs[] =
{
	&m34065_MI,
	&m34066_MI,
	&m34067_MI,
	&m34063_MI,
	&m34064_MI,
	NULL
};
static TypeInfo* t6586_ITIs[] = 
{
	&t603_TI,
	&t6585_TI,
	&t6587_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6586_0_0_0;
extern Il2CppType t6586_1_0_0;
struct t6586;
extern Il2CppGenericClass t6586_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6586_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6586_MIs, t6586_PIs, NULL, NULL, NULL, NULL, NULL, &t6586_TI, t6586_ITIs, NULL, &t1908__CustomAttributeCache, &t6586_TI, &t6586_0_0_0, &t6586_1_0_0, NULL, &t6586_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5031_TI;

#include "t1129.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.AttributeTargets>
extern MethodInfo m34068_MI;
static PropertyInfo t5031____Current_PropertyInfo = 
{
	&t5031_TI, "Current", &m34068_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5031_PIs[] =
{
	&t5031____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1129_0_0_0;
extern void* RuntimeInvoker_t1129 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34068_GM;
MethodInfo m34068_MI = 
{
	"get_Current", NULL, &t5031_TI, &t1129_0_0_0, RuntimeInvoker_t1129, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34068_GM};
static MethodInfo* t5031_MIs[] =
{
	&m34068_MI,
	NULL
};
static TypeInfo* t5031_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5031_0_0_0;
extern Il2CppType t5031_1_0_0;
struct t5031;
extern Il2CppGenericClass t5031_GC;
TypeInfo t5031_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t5031_MIs, t5031_PIs, NULL, NULL, NULL, NULL, NULL, &t5031_TI, t5031_ITIs, NULL, &EmptyCustomAttributesCache, &t5031_TI, &t5031_0_0_0, &t5031_1_0_0, NULL, &t5031_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3481.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3481_TI;
#include "t3481MD.h"

extern TypeInfo t1129_TI;
extern MethodInfo m19356_MI;
extern MethodInfo m26110_MI;
struct t20;
 int32_t m26110 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m19352_MI;
 void m19352 (t3481 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19353_MI;
 t29 * m19353 (t3481 * __this, MethodInfo* method){
	{
		int32_t L_0 = m19356(__this, &m19356_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1129_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m19354_MI;
 void m19354 (t3481 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19355_MI;
 bool m19355 (t3481 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m19356 (t3481 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m26110(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m26110_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.AttributeTargets>
extern Il2CppType t20_0_0_1;
FieldInfo t3481_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3481_TI, offsetof(t3481, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3481_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3481_TI, offsetof(t3481, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3481_FIs[] =
{
	&t3481_f0_FieldInfo,
	&t3481_f1_FieldInfo,
	NULL
};
static PropertyInfo t3481____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3481_TI, "System.Collections.IEnumerator.Current", &m19353_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3481____Current_PropertyInfo = 
{
	&t3481_TI, "Current", &m19356_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3481_PIs[] =
{
	&t3481____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3481____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3481_m19352_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19352_GM;
MethodInfo m19352_MI = 
{
	".ctor", (methodPointerType)&m19352, &t3481_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3481_m19352_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19352_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19353_GM;
MethodInfo m19353_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m19353, &t3481_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19353_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19354_GM;
MethodInfo m19354_MI = 
{
	"Dispose", (methodPointerType)&m19354, &t3481_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19354_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19355_GM;
MethodInfo m19355_MI = 
{
	"MoveNext", (methodPointerType)&m19355, &t3481_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19355_GM};
extern Il2CppType t1129_0_0_0;
extern void* RuntimeInvoker_t1129 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19356_GM;
MethodInfo m19356_MI = 
{
	"get_Current", (methodPointerType)&m19356, &t3481_TI, &t1129_0_0_0, RuntimeInvoker_t1129, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19356_GM};
static MethodInfo* t3481_MIs[] =
{
	&m19352_MI,
	&m19353_MI,
	&m19354_MI,
	&m19355_MI,
	&m19356_MI,
	NULL
};
static MethodInfo* t3481_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19353_MI,
	&m19355_MI,
	&m19354_MI,
	&m19356_MI,
};
static TypeInfo* t3481_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t5031_TI,
};
static Il2CppInterfaceOffsetPair t3481_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t5031_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3481_0_0_0;
extern Il2CppType t3481_1_0_0;
extern Il2CppGenericClass t3481_GC;
TypeInfo t3481_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3481_MIs, t3481_PIs, t3481_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3481_TI, t3481_ITIs, t3481_VT, &EmptyCustomAttributesCache, &t3481_TI, &t3481_0_0_0, &t3481_1_0_0, t3481_IOs, &t3481_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3481)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6588_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.AttributeTargets>
extern MethodInfo m34069_MI;
static PropertyInfo t6588____Count_PropertyInfo = 
{
	&t6588_TI, "Count", &m34069_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m34070_MI;
static PropertyInfo t6588____IsReadOnly_PropertyInfo = 
{
	&t6588_TI, "IsReadOnly", &m34070_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6588_PIs[] =
{
	&t6588____Count_PropertyInfo,
	&t6588____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34069_GM;
MethodInfo m34069_MI = 
{
	"get_Count", NULL, &t6588_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34069_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34070_GM;
MethodInfo m34070_MI = 
{
	"get_IsReadOnly", NULL, &t6588_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34070_GM};
extern Il2CppType t1129_0_0_0;
extern Il2CppType t1129_0_0_0;
static ParameterInfo t6588_m34071_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1129_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34071_GM;
MethodInfo m34071_MI = 
{
	"Add", NULL, &t6588_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6588_m34071_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34071_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34072_GM;
MethodInfo m34072_MI = 
{
	"Clear", NULL, &t6588_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34072_GM};
extern Il2CppType t1129_0_0_0;
static ParameterInfo t6588_m34073_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1129_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34073_GM;
MethodInfo m34073_MI = 
{
	"Contains", NULL, &t6588_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6588_m34073_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34073_GM};
extern Il2CppType t3715_0_0_0;
extern Il2CppType t3715_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6588_m34074_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3715_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34074_GM;
MethodInfo m34074_MI = 
{
	"CopyTo", NULL, &t6588_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6588_m34074_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34074_GM};
extern Il2CppType t1129_0_0_0;
static ParameterInfo t6588_m34075_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1129_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34075_GM;
MethodInfo m34075_MI = 
{
	"Remove", NULL, &t6588_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6588_m34075_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34075_GM};
static MethodInfo* t6588_MIs[] =
{
	&m34069_MI,
	&m34070_MI,
	&m34071_MI,
	&m34072_MI,
	&m34073_MI,
	&m34074_MI,
	&m34075_MI,
	NULL
};
extern TypeInfo t6590_TI;
static TypeInfo* t6588_ITIs[] = 
{
	&t603_TI,
	&t6590_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6588_0_0_0;
extern Il2CppType t6588_1_0_0;
struct t6588;
extern Il2CppGenericClass t6588_GC;
TypeInfo t6588_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6588_MIs, t6588_PIs, NULL, NULL, NULL, NULL, NULL, &t6588_TI, t6588_ITIs, NULL, &EmptyCustomAttributesCache, &t6588_TI, &t6588_0_0_0, &t6588_1_0_0, NULL, &t6588_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.AttributeTargets>
extern Il2CppType t5031_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34076_GM;
MethodInfo m34076_MI = 
{
	"GetEnumerator", NULL, &t6590_TI, &t5031_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34076_GM};
static MethodInfo* t6590_MIs[] =
{
	&m34076_MI,
	NULL
};
static TypeInfo* t6590_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6590_0_0_0;
extern Il2CppType t6590_1_0_0;
struct t6590;
extern Il2CppGenericClass t6590_GC;
TypeInfo t6590_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6590_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6590_TI, t6590_ITIs, NULL, &EmptyCustomAttributesCache, &t6590_TI, &t6590_0_0_0, &t6590_1_0_0, NULL, &t6590_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6589_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.AttributeTargets>
extern MethodInfo m34077_MI;
extern MethodInfo m34078_MI;
static PropertyInfo t6589____Item_PropertyInfo = 
{
	&t6589_TI, "Item", &m34077_MI, &m34078_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6589_PIs[] =
{
	&t6589____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1129_0_0_0;
static ParameterInfo t6589_m34079_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1129_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34079_GM;
MethodInfo m34079_MI = 
{
	"IndexOf", NULL, &t6589_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6589_m34079_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34079_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1129_0_0_0;
static ParameterInfo t6589_m34080_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1129_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34080_GM;
MethodInfo m34080_MI = 
{
	"Insert", NULL, &t6589_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6589_m34080_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34080_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6589_m34081_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34081_GM;
MethodInfo m34081_MI = 
{
	"RemoveAt", NULL, &t6589_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6589_m34081_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34081_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6589_m34077_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1129_0_0_0;
extern void* RuntimeInvoker_t1129_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34077_GM;
MethodInfo m34077_MI = 
{
	"get_Item", NULL, &t6589_TI, &t1129_0_0_0, RuntimeInvoker_t1129_t44, t6589_m34077_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34077_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1129_0_0_0;
static ParameterInfo t6589_m34078_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1129_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34078_GM;
MethodInfo m34078_MI = 
{
	"set_Item", NULL, &t6589_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6589_m34078_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34078_GM};
static MethodInfo* t6589_MIs[] =
{
	&m34079_MI,
	&m34080_MI,
	&m34081_MI,
	&m34077_MI,
	&m34078_MI,
	NULL
};
static TypeInfo* t6589_ITIs[] = 
{
	&t603_TI,
	&t6588_TI,
	&t6590_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6589_0_0_0;
extern Il2CppType t6589_1_0_0;
struct t6589;
extern Il2CppGenericClass t6589_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6589_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6589_MIs, t6589_PIs, NULL, NULL, NULL, NULL, NULL, &t6589_TI, t6589_ITIs, NULL, &t1908__CustomAttributeCache, &t6589_TI, &t6589_0_0_0, &t6589_1_0_0, NULL, &t6589_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2070.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2070_TI;
#include "t2070MD.h"

#include "t465.h"
extern TypeInfo t465_TI;
extern TypeInfo t2074_TI;
extern TypeInfo t44_TI;
#include "t3482MD.h"
extern MethodInfo m19358_MI;
extern MethodInfo m33648_MI;


extern MethodInfo m10255_MI;
 void m10255 (t2070 * __this, MethodInfo* method){
	{
		m19358(__this, &m19358_MI);
		return;
	}
}
extern MethodInfo m19357_MI;
 int32_t m19357 (t2070 * __this, t465  p0, t465  p1, MethodInfo* method){
	int32_t G_B4_0 = 0;
	{
		t465  L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t465_TI), &L_0);
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		t465  L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t465_TI), &L_2);
		if (L_3)
		{
			goto IL_0013;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0014;
	}

IL_0013:
	{
		G_B4_0 = (-1);
	}

IL_0014:
	{
		return G_B4_0;
	}

IL_0015:
	{
		t465  L_4 = p1;
		t29 * L_5 = Box(InitializedTypeInfo(&t465_TI), &L_4);
		if (L_5)
		{
			goto IL_001f;
		}
	}
	{
		return 1;
	}

IL_001f:
	{
		int32_t L_6 = (int32_t)InterfaceFuncInvoker1< int32_t, t465  >::Invoke(&m33648_MI, Box(InitializedTypeInfo(&t465_TI), &(*(&p0))), p1);
		return L_6;
	}
}
// Metadata Definition System.Collections.Generic.GenericComparer`1<System.DateTime>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10255_GM;
MethodInfo m10255_MI = 
{
	".ctor", (methodPointerType)&m10255, &t2070_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10255_GM};
extern Il2CppType t465_0_0_0;
extern Il2CppType t465_0_0_0;
extern Il2CppType t465_0_0_0;
static ParameterInfo t2070_m19357_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t465_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t465_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t465_t465 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19357_GM;
MethodInfo m19357_MI = 
{
	"Compare", (methodPointerType)&m19357, &t2070_TI, &t44_0_0_0, RuntimeInvoker_t44_t465_t465, t2070_m19357_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19357_GM};
static MethodInfo* t2070_MIs[] =
{
	&m10255_MI,
	&m19357_MI,
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m1332_MI;
extern MethodInfo m19360_MI;
static MethodInfo* t2070_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m19357_MI,
	&m19360_MI,
	&m19357_MI,
};
extern TypeInfo t6764_TI;
extern TypeInfo t726_TI;
static Il2CppInterfaceOffsetPair t2070_IOs[] = 
{
	{ &t6764_TI, 4},
	{ &t726_TI, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2070_0_0_0;
extern Il2CppType t2070_1_0_0;
extern TypeInfo t3482_TI;
struct t2070;
extern Il2CppGenericClass t2070_GC;
TypeInfo t2070_TI = 
{
	&g_mscorlib_dll_Image, NULL, "GenericComparer`1", "System.Collections.Generic", t2070_MIs, NULL, NULL, NULL, &t3482_TI, NULL, NULL, &t2070_TI, NULL, t2070_VT, &EmptyCustomAttributesCache, &t2070_TI, &t2070_0_0_0, &t2070_1_0_0, t2070_IOs, &t2070_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2070), 0, -1, 0, 0, -1, 1057024, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 7, 0, 2};
#include "t3482.h"
#ifndef _MSC_VER
#else
#endif

#include "t42.h"
#include "t43.h"
#include "t1247.h"
#include "t3483.h"
#include "t305.h"
extern TypeInfo t42_TI;
extern TypeInfo t40_TI;
extern TypeInfo t1247_TI;
extern TypeInfo t537_TI;
extern TypeInfo t3483_TI;
extern TypeInfo t305_TI;
#include "t29MD.h"
#include "t42MD.h"
#include "t931MD.h"
#include "t3483MD.h"
#include "t305MD.h"
extern Il2CppType t2074_0_0_0;
extern Il2CppType t1247_0_0_0;
extern MethodInfo m1331_MI;
extern MethodInfo m1554_MI;
extern MethodInfo m2981_MI;
extern MethodInfo m2979_MI;
extern MethodInfo m8828_MI;
extern MethodInfo m19362_MI;
extern MethodInfo m34082_MI;
extern MethodInfo m8852_MI;


 void m19358 (t3482 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m19359_MI;
 void m19359 (t29 * __this, MethodInfo* method){
	t3483 * L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer = (t3483 *)il2cpp_codegen_object_new(InitializedTypeInfo(&t3483_TI));
	m19362(L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer, &m19362_MI);
	((t3482_SFs*)InitializedTypeInfo(&t3482_TI)->static_fields)->f0 = L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer;
}
 int32_t m19360 (t3482 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	int32_t G_B4_0 = 0;
	{
		if (p0)
		{
			goto IL_000b;
		}
	}
	{
		if (p1)
		{
			goto IL_0009;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_000a;
	}

IL_0009:
	{
		G_B4_0 = (-1);
	}

IL_000a:
	{
		return G_B4_0;
	}

IL_000b:
	{
		if (p1)
		{
			goto IL_0010;
		}
	}
	{
		return 1;
	}

IL_0010:
	{
		if (!((t29 *)IsInst(p0, InitializedTypeInfo(&t465_TI))))
		{
			goto IL_0033;
		}
	}
	{
		if (!((t29 *)IsInst(p1, InitializedTypeInfo(&t465_TI))))
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker2< int32_t, t465 , t465  >::Invoke(&m34082_MI, __this, ((*(t465 *)((t465 *)UnBox (p0, InitializedTypeInfo(&t465_TI))))), ((*(t465 *)((t465 *)UnBox (p1, InitializedTypeInfo(&t465_TI))))));
		return L_0;
	}

IL_0033:
	{
		t305 * L_1 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m8852(L_1, &m8852_MI);
		il2cpp_codegen_raise_exception(L_1);
	}
}
extern MethodInfo m19361_MI;
 t3482 * m19361 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t3482_TI));
		return (((t3482_SFs*)InitializedTypeInfo(&t3482_TI)->static_fields)->f0);
	}
}
// Metadata Definition System.Collections.Generic.Comparer`1<System.DateTime>
extern Il2CppType t3482_0_0_49;
FieldInfo t3482_f0_FieldInfo = 
{
	"_default", &t3482_0_0_49, &t3482_TI, offsetof(t3482_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3482_FIs[] =
{
	&t3482_f0_FieldInfo,
	NULL
};
static PropertyInfo t3482____Default_PropertyInfo = 
{
	&t3482_TI, "Default", &m19361_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3482_PIs[] =
{
	&t3482____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19358_GM;
MethodInfo m19358_MI = 
{
	".ctor", (methodPointerType)&m19358, &t3482_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19358_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19359_GM;
MethodInfo m19359_MI = 
{
	".cctor", (methodPointerType)&m19359, &t3482_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19359_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3482_m19360_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19360_GM;
MethodInfo m19360_MI = 
{
	"System.Collections.IComparer.Compare", (methodPointerType)&m19360, &t3482_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t3482_m19360_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19360_GM};
extern Il2CppType t465_0_0_0;
extern Il2CppType t465_0_0_0;
static ParameterInfo t3482_m34082_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t465_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t465_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t465_t465 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34082_GM;
MethodInfo m34082_MI = 
{
	"Compare", NULL, &t3482_TI, &t44_0_0_0, RuntimeInvoker_t44_t465_t465, t3482_m34082_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34082_GM};
extern Il2CppType t3482_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19361_GM;
MethodInfo m19361_MI = 
{
	"get_Default", (methodPointerType)&m19361, &t3482_TI, &t3482_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19361_GM};
static MethodInfo* t3482_MIs[] =
{
	&m19358_MI,
	&m19359_MI,
	&m19360_MI,
	&m34082_MI,
	&m19361_MI,
	NULL
};
static MethodInfo* t3482_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m34082_MI,
	&m19360_MI,
	NULL,
};
static TypeInfo* t3482_ITIs[] = 
{
	&t6764_TI,
	&t726_TI,
};
static Il2CppInterfaceOffsetPair t3482_IOs[] = 
{
	{ &t6764_TI, 4},
	{ &t726_TI, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3482_0_0_0;
extern Il2CppType t3482_1_0_0;
extern TypeInfo t29_TI;
struct t3482;
extern Il2CppGenericClass t3482_GC;
TypeInfo t3482_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparer`1", "System.Collections.Generic", t3482_MIs, t3482_PIs, t3482_FIs, NULL, &t29_TI, NULL, NULL, &t3482_TI, t3482_ITIs, t3482_VT, &EmptyCustomAttributesCache, &t3482_TI, &t3482_0_0_0, &t3482_1_0_0, t3482_IOs, &t3482_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3482), 0, -1, sizeof(t3482_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 5, 1, 1, 0, 0, 7, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IComparer`1<System.DateTime>
extern Il2CppType t465_0_0_0;
extern Il2CppType t465_0_0_0;
static ParameterInfo t6764_m34083_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t465_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t465_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t465_t465 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34083_GM;
MethodInfo m34083_MI = 
{
	"Compare", NULL, &t6764_TI, &t44_0_0_0, RuntimeInvoker_t44_t465_t465, t6764_m34083_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34083_GM};
static MethodInfo* t6764_MIs[] =
{
	&m34083_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6764_0_0_0;
extern Il2CppType t6764_1_0_0;
struct t6764;
extern Il2CppGenericClass t6764_GC;
TypeInfo t6764_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparer`1", "System.Collections.Generic", t6764_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6764_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6764_TI, &t6764_0_0_0, &t6764_1_0_0, NULL, &t6764_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo t290_TI;
extern MethodInfo m9672_MI;
extern MethodInfo m1935_MI;


 void m19362 (t3483 * __this, MethodInfo* method){
	{
		m19358(__this, &m19358_MI);
		return;
	}
}
extern MethodInfo m19363_MI;
 int32_t m19363 (t3483 * __this, t465  p0, t465  p1, MethodInfo* method){
	int32_t G_B4_0 = 0;
	{
		t465  L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t465_TI), &L_0);
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		t465  L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t465_TI), &L_2);
		if (L_3)
		{
			goto IL_0013;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0014;
	}

IL_0013:
	{
		G_B4_0 = (-1);
	}

IL_0014:
	{
		return G_B4_0;
	}

IL_0015:
	{
		t465  L_4 = p1;
		t29 * L_5 = Box(InitializedTypeInfo(&t465_TI), &L_4);
		if (L_5)
		{
			goto IL_001f;
		}
	}
	{
		return 1;
	}

IL_001f:
	{
		t465  L_6 = p0;
		t29 * L_7 = Box(InitializedTypeInfo(&t465_TI), &L_6);
		if (!((t29*)IsInst(L_7, InitializedTypeInfo(&t2074_TI))))
		{
			goto IL_003e;
		}
	}
	{
		t465  L_8 = p0;
		t29 * L_9 = Box(InitializedTypeInfo(&t465_TI), &L_8);
		int32_t L_10 = (int32_t)InterfaceFuncInvoker1< int32_t, t465  >::Invoke(&m33648_MI, ((t29*)Castclass(L_9, InitializedTypeInfo(&t2074_TI))), p1);
		return L_10;
	}

IL_003e:
	{
		t465  L_11 = p0;
		t29 * L_12 = Box(InitializedTypeInfo(&t465_TI), &L_11);
		if (!((t29 *)IsInst(L_12, InitializedTypeInfo(&t290_TI))))
		{
			goto IL_0062;
		}
	}
	{
		t465  L_13 = p0;
		t29 * L_14 = Box(InitializedTypeInfo(&t465_TI), &L_13);
		t465  L_15 = p1;
		t29 * L_16 = Box(InitializedTypeInfo(&t465_TI), &L_15);
		int32_t L_17 = (int32_t)InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(&m9672_MI, ((t29 *)Castclass(L_14, InitializedTypeInfo(&t290_TI))), L_16);
		return L_17;
	}

IL_0062:
	{
		t305 * L_18 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_18, (t7*) &_stringLiteral1162, &m1935_MI);
		il2cpp_codegen_raise_exception(L_18);
	}
}
// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTime>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19362_GM;
MethodInfo m19362_MI = 
{
	".ctor", (methodPointerType)&m19362, &t3483_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19362_GM};
extern Il2CppType t465_0_0_0;
extern Il2CppType t465_0_0_0;
static ParameterInfo t3483_m19363_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t465_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t465_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t465_t465 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19363_GM;
MethodInfo m19363_MI = 
{
	"Compare", (methodPointerType)&m19363, &t3483_TI, &t44_0_0_0, RuntimeInvoker_t44_t465_t465, t3483_m19363_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19363_GM};
static MethodInfo* t3483_MIs[] =
{
	&m19362_MI,
	&m19363_MI,
	NULL
};
static MethodInfo* t3483_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m19363_MI,
	&m19360_MI,
	&m19363_MI,
};
static Il2CppInterfaceOffsetPair t3483_IOs[] = 
{
	{ &t6764_TI, 4},
	{ &t726_TI, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3483_0_0_0;
extern Il2CppType t3483_1_0_0;
struct t3483;
extern Il2CppGenericClass t3483_GC;
extern TypeInfo t1246_TI;
TypeInfo t3483_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t3483_MIs, NULL, NULL, NULL, &t3482_TI, NULL, &t1246_TI, &t3483_TI, NULL, t3483_VT, &EmptyCustomAttributesCache, &t3483_TI, &t3483_0_0_0, &t3483_1_0_0, t3483_IOs, &t3483_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3483), 0, -1, 0, 0, -1, 1048835, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 7, 0, 2};
#include "t2071.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2071_TI;
#include "t2071MD.h"

extern TypeInfo t2075_TI;
#include "t3484MD.h"
extern MethodInfo m19366_MI;
extern MethodInfo m33663_MI;


extern MethodInfo m10256_MI;
 void m10256 (t2071 * __this, MethodInfo* method){
	{
		m19366(__this, &m19366_MI);
		return;
	}
}
extern MethodInfo m19364_MI;
 int32_t m19364 (t2071 * __this, t465  p0, MethodInfo* method){
	{
		t465  L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t465_TI), &L_0);
		if (L_1)
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}

IL_000a:
	{
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1322_MI, Box(InitializedTypeInfo(&t465_TI), &(*(&p0))));
		return L_2;
	}
}
extern MethodInfo m19365_MI;
 bool m19365 (t2071 * __this, t465  p0, t465  p1, MethodInfo* method){
	{
		t465  L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t465_TI), &L_0);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		t465  L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t465_TI), &L_2);
		return ((((t29 *)L_3) == ((t29 *)NULL))? 1 : 0);
	}

IL_0012:
	{
		bool L_4 = (bool)InterfaceFuncInvoker1< bool, t465  >::Invoke(&m33663_MI, Box(InitializedTypeInfo(&t465_TI), &(*(&p0))), p1);
		return L_4;
	}
}
// Metadata Definition System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10256_GM;
MethodInfo m10256_MI = 
{
	".ctor", (methodPointerType)&m10256, &t2071_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10256_GM};
extern Il2CppType t465_0_0_0;
static ParameterInfo t2071_m19364_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t465_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t465 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19364_GM;
MethodInfo m19364_MI = 
{
	"GetHashCode", (methodPointerType)&m19364, &t2071_TI, &t44_0_0_0, RuntimeInvoker_t44_t465, t2071_m19364_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19364_GM};
extern Il2CppType t465_0_0_0;
extern Il2CppType t465_0_0_0;
static ParameterInfo t2071_m19365_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t465_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t465_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t465_t465 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19365_GM;
MethodInfo m19365_MI = 
{
	"Equals", (methodPointerType)&m19365, &t2071_TI, &t40_0_0_0, RuntimeInvoker_t40_t465_t465, t2071_m19365_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19365_GM};
static MethodInfo* t2071_MIs[] =
{
	&m10256_MI,
	&m19364_MI,
	&m19365_MI,
	NULL
};
extern MethodInfo m19369_MI;
extern MethodInfo m19368_MI;
static MethodInfo* t2071_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m19365_MI,
	&m19364_MI,
	&m19369_MI,
	&m19368_MI,
	&m19364_MI,
	&m19365_MI,
};
extern TypeInfo t6765_TI;
extern TypeInfo t734_TI;
static Il2CppInterfaceOffsetPair t2071_IOs[] = 
{
	{ &t6765_TI, 4},
	{ &t734_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2071_0_0_0;
extern Il2CppType t2071_1_0_0;
extern TypeInfo t3484_TI;
struct t2071;
extern Il2CppGenericClass t2071_GC;
TypeInfo t2071_TI = 
{
	&g_mscorlib_dll_Image, NULL, "GenericEqualityComparer`1", "System.Collections.Generic", t2071_MIs, NULL, NULL, NULL, &t3484_TI, NULL, NULL, &t2071_TI, NULL, t2071_VT, &EmptyCustomAttributesCache, &t2071_TI, &t2071_0_0_0, &t2071_1_0_0, t2071_IOs, &t2071_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2071), 0, -1, 0, 0, -1, 1057024, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#include "t3484.h"
#ifndef _MSC_VER
#else
#endif

#include "t1257.h"
#include "t3485.h"
extern TypeInfo t1257_TI;
extern TypeInfo t3485_TI;
#include "t3485MD.h"
extern Il2CppType t2075_0_0_0;
extern Il2CppType t1257_0_0_0;
extern MethodInfo m19371_MI;
extern MethodInfo m34084_MI;
extern MethodInfo m34085_MI;


 void m19366 (t3484 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m19367_MI;
 void m19367 (t29 * __this, MethodInfo* method){
	t3485 * L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer = (t3485 *)il2cpp_codegen_object_new(InitializedTypeInfo(&t3485_TI));
	m19371(L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer, &m19371_MI);
	((t3484_SFs*)InitializedTypeInfo(&t3484_TI)->static_fields)->f0 = L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer;
}
 int32_t m19368 (t3484 * __this, t29 * p0, MethodInfo* method){
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker1< int32_t, t465  >::Invoke(&m34084_MI, __this, ((*(t465 *)((t465 *)UnBox (p0, InitializedTypeInfo(&t465_TI))))));
		return L_0;
	}
}
 bool m19369 (t3484 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	{
		bool L_0 = (bool)VirtFuncInvoker2< bool, t465 , t465  >::Invoke(&m34085_MI, __this, ((*(t465 *)((t465 *)UnBox (p0, InitializedTypeInfo(&t465_TI))))), ((*(t465 *)((t465 *)UnBox (p1, InitializedTypeInfo(&t465_TI))))));
		return L_0;
	}
}
extern MethodInfo m19370_MI;
 t3484 * m19370 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t3484_TI));
		return (((t3484_SFs*)InitializedTypeInfo(&t3484_TI)->static_fields)->f0);
	}
}
// Metadata Definition System.Collections.Generic.EqualityComparer`1<System.DateTime>
extern Il2CppType t3484_0_0_49;
FieldInfo t3484_f0_FieldInfo = 
{
	"_default", &t3484_0_0_49, &t3484_TI, offsetof(t3484_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3484_FIs[] =
{
	&t3484_f0_FieldInfo,
	NULL
};
static PropertyInfo t3484____Default_PropertyInfo = 
{
	&t3484_TI, "Default", &m19370_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3484_PIs[] =
{
	&t3484____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19366_GM;
MethodInfo m19366_MI = 
{
	".ctor", (methodPointerType)&m19366, &t3484_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19366_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19367_GM;
MethodInfo m19367_MI = 
{
	".cctor", (methodPointerType)&m19367, &t3484_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19367_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3484_m19368_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19368_GM;
MethodInfo m19368_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m19368, &t3484_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3484_m19368_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19368_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3484_m19369_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19369_GM;
MethodInfo m19369_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m19369, &t3484_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t3484_m19369_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19369_GM};
extern Il2CppType t465_0_0_0;
static ParameterInfo t3484_m34084_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t465_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t465 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34084_GM;
MethodInfo m34084_MI = 
{
	"GetHashCode", NULL, &t3484_TI, &t44_0_0_0, RuntimeInvoker_t44_t465, t3484_m34084_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34084_GM};
extern Il2CppType t465_0_0_0;
extern Il2CppType t465_0_0_0;
static ParameterInfo t3484_m34085_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t465_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t465_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t465_t465 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34085_GM;
MethodInfo m34085_MI = 
{
	"Equals", NULL, &t3484_TI, &t40_0_0_0, RuntimeInvoker_t40_t465_t465, t3484_m34085_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34085_GM};
extern Il2CppType t3484_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19370_GM;
MethodInfo m19370_MI = 
{
	"get_Default", (methodPointerType)&m19370, &t3484_TI, &t3484_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19370_GM};
static MethodInfo* t3484_MIs[] =
{
	&m19366_MI,
	&m19367_MI,
	&m19368_MI,
	&m19369_MI,
	&m34084_MI,
	&m34085_MI,
	&m19370_MI,
	NULL
};
static MethodInfo* t3484_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m34085_MI,
	&m34084_MI,
	&m19369_MI,
	&m19368_MI,
	NULL,
	NULL,
};
static TypeInfo* t3484_ITIs[] = 
{
	&t6765_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t3484_IOs[] = 
{
	{ &t6765_TI, 4},
	{ &t734_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3484_0_0_0;
extern Il2CppType t3484_1_0_0;
struct t3484;
extern Il2CppGenericClass t3484_GC;
TypeInfo t3484_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t3484_MIs, t3484_PIs, t3484_FIs, NULL, &t29_TI, NULL, NULL, &t3484_TI, t3484_ITIs, t3484_VT, &EmptyCustomAttributesCache, &t3484_TI, &t3484_0_0_0, &t3484_1_0_0, t3484_IOs, &t3484_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3484), 0, -1, sizeof(t3484_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<System.DateTime>
extern Il2CppType t465_0_0_0;
extern Il2CppType t465_0_0_0;
static ParameterInfo t6765_m34086_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t465_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t465_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t465_t465 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34086_GM;
MethodInfo m34086_MI = 
{
	"Equals", NULL, &t6765_TI, &t40_0_0_0, RuntimeInvoker_t40_t465_t465, t6765_m34086_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34086_GM};
extern Il2CppType t465_0_0_0;
static ParameterInfo t6765_m34087_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t465_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t465 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34087_GM;
MethodInfo m34087_MI = 
{
	"GetHashCode", NULL, &t6765_TI, &t44_0_0_0, RuntimeInvoker_t44_t465, t6765_m34087_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34087_GM};
static MethodInfo* t6765_MIs[] =
{
	&m34086_MI,
	&m34087_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6765_0_0_0;
extern Il2CppType t6765_1_0_0;
struct t6765;
extern Il2CppGenericClass t6765_GC;
TypeInfo t6765_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t6765_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6765_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6765_TI, &t6765_0_0_0, &t6765_1_0_0, NULL, &t6765_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



 void m19371 (t3485 * __this, MethodInfo* method){
	{
		m19366(__this, &m19366_MI);
		return;
	}
}
extern MethodInfo m19372_MI;
 int32_t m19372 (t3485 * __this, t465  p0, MethodInfo* method){
	{
		t465  L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t465_TI), &L_0);
		if (L_1)
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}

IL_000a:
	{
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1322_MI, Box(InitializedTypeInfo(&t465_TI), &(*(&p0))));
		return L_2;
	}
}
extern MethodInfo m19373_MI;
 bool m19373 (t3485 * __this, t465  p0, t465  p1, MethodInfo* method){
	{
		t465  L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t465_TI), &L_0);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		t465  L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t465_TI), &L_2);
		return ((((t29 *)L_3) == ((t29 *)NULL))? 1 : 0);
	}

IL_0012:
	{
		t465  L_4 = p1;
		t29 * L_5 = Box(InitializedTypeInfo(&t465_TI), &L_4);
		bool L_6 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t465_TI), &(*(&p0))), L_5);
		return L_6;
	}
}
// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTime>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19371_GM;
MethodInfo m19371_MI = 
{
	".ctor", (methodPointerType)&m19371, &t3485_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19371_GM};
extern Il2CppType t465_0_0_0;
static ParameterInfo t3485_m19372_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t465_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t465 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19372_GM;
MethodInfo m19372_MI = 
{
	"GetHashCode", (methodPointerType)&m19372, &t3485_TI, &t44_0_0_0, RuntimeInvoker_t44_t465, t3485_m19372_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19372_GM};
extern Il2CppType t465_0_0_0;
extern Il2CppType t465_0_0_0;
static ParameterInfo t3485_m19373_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t465_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t465_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t465_t465 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19373_GM;
MethodInfo m19373_MI = 
{
	"Equals", (methodPointerType)&m19373, &t3485_TI, &t40_0_0_0, RuntimeInvoker_t40_t465_t465, t3485_m19373_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19373_GM};
static MethodInfo* t3485_MIs[] =
{
	&m19371_MI,
	&m19372_MI,
	&m19373_MI,
	NULL
};
static MethodInfo* t3485_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m19373_MI,
	&m19372_MI,
	&m19369_MI,
	&m19368_MI,
	&m19372_MI,
	&m19373_MI,
};
static Il2CppInterfaceOffsetPair t3485_IOs[] = 
{
	{ &t6765_TI, 4},
	{ &t734_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3485_0_0_0;
extern Il2CppType t3485_1_0_0;
struct t3485;
extern Il2CppGenericClass t3485_GC;
extern TypeInfo t1256_TI;
TypeInfo t3485_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t3485_MIs, NULL, NULL, NULL, &t3484_TI, NULL, &t1256_TI, &t3485_TI, NULL, t3485_VT, &EmptyCustomAttributesCache, &t3485_TI, &t3485_0_0_0, &t3485_1_0_0, t3485_IOs, &t3485_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3485), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5033_TI;

#include "t1627.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.DateTime/Which>
extern MethodInfo m34088_MI;
static PropertyInfo t5033____Current_PropertyInfo = 
{
	&t5033_TI, "Current", &m34088_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5033_PIs[] =
{
	&t5033____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1627_0_0_0;
extern void* RuntimeInvoker_t1627 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34088_GM;
MethodInfo m34088_MI = 
{
	"get_Current", NULL, &t5033_TI, &t1627_0_0_0, RuntimeInvoker_t1627, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34088_GM};
static MethodInfo* t5033_MIs[] =
{
	&m34088_MI,
	NULL
};
static TypeInfo* t5033_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5033_0_0_0;
extern Il2CppType t5033_1_0_0;
struct t5033;
extern Il2CppGenericClass t5033_GC;
TypeInfo t5033_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t5033_MIs, t5033_PIs, NULL, NULL, NULL, NULL, NULL, &t5033_TI, t5033_ITIs, NULL, &EmptyCustomAttributesCache, &t5033_TI, &t5033_0_0_0, &t5033_1_0_0, NULL, &t5033_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3486.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3486_TI;
#include "t3486MD.h"

extern TypeInfo t1627_TI;
extern MethodInfo m19378_MI;
extern MethodInfo m26121_MI;
struct t20;
 int32_t m26121 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m19374_MI;
 void m19374 (t3486 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19375_MI;
 t29 * m19375 (t3486 * __this, MethodInfo* method){
	{
		int32_t L_0 = m19378(__this, &m19378_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1627_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m19376_MI;
 void m19376 (t3486 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19377_MI;
 bool m19377 (t3486 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m19378 (t3486 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m26121(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m26121_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.DateTime/Which>
extern Il2CppType t20_0_0_1;
FieldInfo t3486_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3486_TI, offsetof(t3486, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3486_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3486_TI, offsetof(t3486, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3486_FIs[] =
{
	&t3486_f0_FieldInfo,
	&t3486_f1_FieldInfo,
	NULL
};
static PropertyInfo t3486____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3486_TI, "System.Collections.IEnumerator.Current", &m19375_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3486____Current_PropertyInfo = 
{
	&t3486_TI, "Current", &m19378_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3486_PIs[] =
{
	&t3486____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3486____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3486_m19374_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19374_GM;
MethodInfo m19374_MI = 
{
	".ctor", (methodPointerType)&m19374, &t3486_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3486_m19374_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19374_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19375_GM;
MethodInfo m19375_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m19375, &t3486_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19375_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19376_GM;
MethodInfo m19376_MI = 
{
	"Dispose", (methodPointerType)&m19376, &t3486_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19376_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19377_GM;
MethodInfo m19377_MI = 
{
	"MoveNext", (methodPointerType)&m19377, &t3486_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19377_GM};
extern Il2CppType t1627_0_0_0;
extern void* RuntimeInvoker_t1627 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19378_GM;
MethodInfo m19378_MI = 
{
	"get_Current", (methodPointerType)&m19378, &t3486_TI, &t1627_0_0_0, RuntimeInvoker_t1627, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19378_GM};
static MethodInfo* t3486_MIs[] =
{
	&m19374_MI,
	&m19375_MI,
	&m19376_MI,
	&m19377_MI,
	&m19378_MI,
	NULL
};
static MethodInfo* t3486_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19375_MI,
	&m19377_MI,
	&m19376_MI,
	&m19378_MI,
};
static TypeInfo* t3486_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t5033_TI,
};
static Il2CppInterfaceOffsetPair t3486_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t5033_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3486_0_0_0;
extern Il2CppType t3486_1_0_0;
extern Il2CppGenericClass t3486_GC;
TypeInfo t3486_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3486_MIs, t3486_PIs, t3486_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3486_TI, t3486_ITIs, t3486_VT, &EmptyCustomAttributesCache, &t3486_TI, &t3486_0_0_0, &t3486_1_0_0, t3486_IOs, &t3486_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3486)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6591_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.DateTime/Which>
extern MethodInfo m34089_MI;
static PropertyInfo t6591____Count_PropertyInfo = 
{
	&t6591_TI, "Count", &m34089_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m34090_MI;
static PropertyInfo t6591____IsReadOnly_PropertyInfo = 
{
	&t6591_TI, "IsReadOnly", &m34090_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6591_PIs[] =
{
	&t6591____Count_PropertyInfo,
	&t6591____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34089_GM;
MethodInfo m34089_MI = 
{
	"get_Count", NULL, &t6591_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34089_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34090_GM;
MethodInfo m34090_MI = 
{
	"get_IsReadOnly", NULL, &t6591_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34090_GM};
extern Il2CppType t1627_0_0_0;
extern Il2CppType t1627_0_0_0;
static ParameterInfo t6591_m34091_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1627_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34091_GM;
MethodInfo m34091_MI = 
{
	"Add", NULL, &t6591_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6591_m34091_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34091_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34092_GM;
MethodInfo m34092_MI = 
{
	"Clear", NULL, &t6591_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34092_GM};
extern Il2CppType t1627_0_0_0;
static ParameterInfo t6591_m34093_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1627_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34093_GM;
MethodInfo m34093_MI = 
{
	"Contains", NULL, &t6591_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6591_m34093_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34093_GM};
extern Il2CppType t3716_0_0_0;
extern Il2CppType t3716_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6591_m34094_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3716_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34094_GM;
MethodInfo m34094_MI = 
{
	"CopyTo", NULL, &t6591_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6591_m34094_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34094_GM};
extern Il2CppType t1627_0_0_0;
static ParameterInfo t6591_m34095_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1627_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34095_GM;
MethodInfo m34095_MI = 
{
	"Remove", NULL, &t6591_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6591_m34095_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34095_GM};
static MethodInfo* t6591_MIs[] =
{
	&m34089_MI,
	&m34090_MI,
	&m34091_MI,
	&m34092_MI,
	&m34093_MI,
	&m34094_MI,
	&m34095_MI,
	NULL
};
extern TypeInfo t6593_TI;
static TypeInfo* t6591_ITIs[] = 
{
	&t603_TI,
	&t6593_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6591_0_0_0;
extern Il2CppType t6591_1_0_0;
struct t6591;
extern Il2CppGenericClass t6591_GC;
TypeInfo t6591_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6591_MIs, t6591_PIs, NULL, NULL, NULL, NULL, NULL, &t6591_TI, t6591_ITIs, NULL, &EmptyCustomAttributesCache, &t6591_TI, &t6591_0_0_0, &t6591_1_0_0, NULL, &t6591_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.DateTime/Which>
extern Il2CppType t5033_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34096_GM;
MethodInfo m34096_MI = 
{
	"GetEnumerator", NULL, &t6593_TI, &t5033_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34096_GM};
static MethodInfo* t6593_MIs[] =
{
	&m34096_MI,
	NULL
};
static TypeInfo* t6593_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6593_0_0_0;
extern Il2CppType t6593_1_0_0;
struct t6593;
extern Il2CppGenericClass t6593_GC;
TypeInfo t6593_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6593_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6593_TI, t6593_ITIs, NULL, &EmptyCustomAttributesCache, &t6593_TI, &t6593_0_0_0, &t6593_1_0_0, NULL, &t6593_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6592_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.DateTime/Which>
extern MethodInfo m34097_MI;
extern MethodInfo m34098_MI;
static PropertyInfo t6592____Item_PropertyInfo = 
{
	&t6592_TI, "Item", &m34097_MI, &m34098_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6592_PIs[] =
{
	&t6592____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1627_0_0_0;
static ParameterInfo t6592_m34099_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1627_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34099_GM;
MethodInfo m34099_MI = 
{
	"IndexOf", NULL, &t6592_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6592_m34099_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34099_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1627_0_0_0;
static ParameterInfo t6592_m34100_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1627_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34100_GM;
MethodInfo m34100_MI = 
{
	"Insert", NULL, &t6592_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6592_m34100_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34100_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6592_m34101_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34101_GM;
MethodInfo m34101_MI = 
{
	"RemoveAt", NULL, &t6592_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6592_m34101_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34101_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6592_m34097_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1627_0_0_0;
extern void* RuntimeInvoker_t1627_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34097_GM;
MethodInfo m34097_MI = 
{
	"get_Item", NULL, &t6592_TI, &t1627_0_0_0, RuntimeInvoker_t1627_t44, t6592_m34097_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34097_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1627_0_0_0;
static ParameterInfo t6592_m34098_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1627_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34098_GM;
MethodInfo m34098_MI = 
{
	"set_Item", NULL, &t6592_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6592_m34098_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34098_GM};
static MethodInfo* t6592_MIs[] =
{
	&m34099_MI,
	&m34100_MI,
	&m34101_MI,
	&m34097_MI,
	&m34098_MI,
	NULL
};
static TypeInfo* t6592_ITIs[] = 
{
	&t603_TI,
	&t6591_TI,
	&t6593_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6592_0_0_0;
extern Il2CppType t6592_1_0_0;
struct t6592;
extern Il2CppGenericClass t6592_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6592_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6592_MIs, t6592_PIs, NULL, NULL, NULL, NULL, NULL, &t6592_TI, t6592_ITIs, NULL, &t1908__CustomAttributeCache, &t6592_TI, &t6592_0_0_0, &t6592_1_0_0, NULL, &t6592_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5035_TI;

#include "t1628.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.DateTimeKind>
extern MethodInfo m34102_MI;
static PropertyInfo t5035____Current_PropertyInfo = 
{
	&t5035_TI, "Current", &m34102_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5035_PIs[] =
{
	&t5035____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1628_0_0_0;
extern void* RuntimeInvoker_t1628 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34102_GM;
MethodInfo m34102_MI = 
{
	"get_Current", NULL, &t5035_TI, &t1628_0_0_0, RuntimeInvoker_t1628, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34102_GM};
static MethodInfo* t5035_MIs[] =
{
	&m34102_MI,
	NULL
};
static TypeInfo* t5035_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5035_0_0_0;
extern Il2CppType t5035_1_0_0;
struct t5035;
extern Il2CppGenericClass t5035_GC;
TypeInfo t5035_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t5035_MIs, t5035_PIs, NULL, NULL, NULL, NULL, NULL, &t5035_TI, t5035_ITIs, NULL, &EmptyCustomAttributesCache, &t5035_TI, &t5035_0_0_0, &t5035_1_0_0, NULL, &t5035_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3487.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3487_TI;
#include "t3487MD.h"

extern TypeInfo t1628_TI;
extern MethodInfo m19383_MI;
extern MethodInfo m26132_MI;
struct t20;
 int32_t m26132 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m19379_MI;
 void m19379 (t3487 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19380_MI;
 t29 * m19380 (t3487 * __this, MethodInfo* method){
	{
		int32_t L_0 = m19383(__this, &m19383_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1628_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m19381_MI;
 void m19381 (t3487 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19382_MI;
 bool m19382 (t3487 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m19383 (t3487 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m26132(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m26132_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.DateTimeKind>
extern Il2CppType t20_0_0_1;
FieldInfo t3487_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3487_TI, offsetof(t3487, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3487_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3487_TI, offsetof(t3487, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3487_FIs[] =
{
	&t3487_f0_FieldInfo,
	&t3487_f1_FieldInfo,
	NULL
};
static PropertyInfo t3487____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3487_TI, "System.Collections.IEnumerator.Current", &m19380_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3487____Current_PropertyInfo = 
{
	&t3487_TI, "Current", &m19383_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3487_PIs[] =
{
	&t3487____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3487____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3487_m19379_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19379_GM;
MethodInfo m19379_MI = 
{
	".ctor", (methodPointerType)&m19379, &t3487_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3487_m19379_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19379_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19380_GM;
MethodInfo m19380_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m19380, &t3487_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19380_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19381_GM;
MethodInfo m19381_MI = 
{
	"Dispose", (methodPointerType)&m19381, &t3487_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19381_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19382_GM;
MethodInfo m19382_MI = 
{
	"MoveNext", (methodPointerType)&m19382, &t3487_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19382_GM};
extern Il2CppType t1628_0_0_0;
extern void* RuntimeInvoker_t1628 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19383_GM;
MethodInfo m19383_MI = 
{
	"get_Current", (methodPointerType)&m19383, &t3487_TI, &t1628_0_0_0, RuntimeInvoker_t1628, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19383_GM};
static MethodInfo* t3487_MIs[] =
{
	&m19379_MI,
	&m19380_MI,
	&m19381_MI,
	&m19382_MI,
	&m19383_MI,
	NULL
};
static MethodInfo* t3487_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19380_MI,
	&m19382_MI,
	&m19381_MI,
	&m19383_MI,
};
static TypeInfo* t3487_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t5035_TI,
};
static Il2CppInterfaceOffsetPair t3487_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t5035_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3487_0_0_0;
extern Il2CppType t3487_1_0_0;
extern Il2CppGenericClass t3487_GC;
TypeInfo t3487_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3487_MIs, t3487_PIs, t3487_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3487_TI, t3487_ITIs, t3487_VT, &EmptyCustomAttributesCache, &t3487_TI, &t3487_0_0_0, &t3487_1_0_0, t3487_IOs, &t3487_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3487)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6594_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.DateTimeKind>
extern MethodInfo m34103_MI;
static PropertyInfo t6594____Count_PropertyInfo = 
{
	&t6594_TI, "Count", &m34103_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m34104_MI;
static PropertyInfo t6594____IsReadOnly_PropertyInfo = 
{
	&t6594_TI, "IsReadOnly", &m34104_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6594_PIs[] =
{
	&t6594____Count_PropertyInfo,
	&t6594____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34103_GM;
MethodInfo m34103_MI = 
{
	"get_Count", NULL, &t6594_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34103_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34104_GM;
MethodInfo m34104_MI = 
{
	"get_IsReadOnly", NULL, &t6594_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34104_GM};
extern Il2CppType t1628_0_0_0;
extern Il2CppType t1628_0_0_0;
static ParameterInfo t6594_m34105_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1628_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34105_GM;
MethodInfo m34105_MI = 
{
	"Add", NULL, &t6594_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6594_m34105_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34105_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34106_GM;
MethodInfo m34106_MI = 
{
	"Clear", NULL, &t6594_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34106_GM};
extern Il2CppType t1628_0_0_0;
static ParameterInfo t6594_m34107_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1628_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34107_GM;
MethodInfo m34107_MI = 
{
	"Contains", NULL, &t6594_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6594_m34107_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34107_GM};
extern Il2CppType t3717_0_0_0;
extern Il2CppType t3717_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6594_m34108_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3717_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34108_GM;
MethodInfo m34108_MI = 
{
	"CopyTo", NULL, &t6594_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6594_m34108_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34108_GM};
extern Il2CppType t1628_0_0_0;
static ParameterInfo t6594_m34109_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1628_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34109_GM;
MethodInfo m34109_MI = 
{
	"Remove", NULL, &t6594_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6594_m34109_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34109_GM};
static MethodInfo* t6594_MIs[] =
{
	&m34103_MI,
	&m34104_MI,
	&m34105_MI,
	&m34106_MI,
	&m34107_MI,
	&m34108_MI,
	&m34109_MI,
	NULL
};
extern TypeInfo t6596_TI;
static TypeInfo* t6594_ITIs[] = 
{
	&t603_TI,
	&t6596_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6594_0_0_0;
extern Il2CppType t6594_1_0_0;
struct t6594;
extern Il2CppGenericClass t6594_GC;
TypeInfo t6594_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6594_MIs, t6594_PIs, NULL, NULL, NULL, NULL, NULL, &t6594_TI, t6594_ITIs, NULL, &EmptyCustomAttributesCache, &t6594_TI, &t6594_0_0_0, &t6594_1_0_0, NULL, &t6594_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.DateTimeKind>
extern Il2CppType t5035_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34110_GM;
MethodInfo m34110_MI = 
{
	"GetEnumerator", NULL, &t6596_TI, &t5035_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34110_GM};
static MethodInfo* t6596_MIs[] =
{
	&m34110_MI,
	NULL
};
static TypeInfo* t6596_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6596_0_0_0;
extern Il2CppType t6596_1_0_0;
struct t6596;
extern Il2CppGenericClass t6596_GC;
TypeInfo t6596_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6596_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6596_TI, t6596_ITIs, NULL, &EmptyCustomAttributesCache, &t6596_TI, &t6596_0_0_0, &t6596_1_0_0, NULL, &t6596_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6595_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.DateTimeKind>
extern MethodInfo m34111_MI;
extern MethodInfo m34112_MI;
static PropertyInfo t6595____Item_PropertyInfo = 
{
	&t6595_TI, "Item", &m34111_MI, &m34112_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6595_PIs[] =
{
	&t6595____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1628_0_0_0;
static ParameterInfo t6595_m34113_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1628_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34113_GM;
MethodInfo m34113_MI = 
{
	"IndexOf", NULL, &t6595_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6595_m34113_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34113_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1628_0_0_0;
static ParameterInfo t6595_m34114_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1628_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34114_GM;
MethodInfo m34114_MI = 
{
	"Insert", NULL, &t6595_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6595_m34114_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34114_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6595_m34115_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34115_GM;
MethodInfo m34115_MI = 
{
	"RemoveAt", NULL, &t6595_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6595_m34115_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34115_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6595_m34111_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1628_0_0_0;
extern void* RuntimeInvoker_t1628_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34111_GM;
MethodInfo m34111_MI = 
{
	"get_Item", NULL, &t6595_TI, &t1628_0_0_0, RuntimeInvoker_t1628_t44, t6595_m34111_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34111_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1628_0_0_0;
static ParameterInfo t6595_m34112_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1628_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34112_GM;
MethodInfo m34112_MI = 
{
	"set_Item", NULL, &t6595_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6595_m34112_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34112_GM};
static MethodInfo* t6595_MIs[] =
{
	&m34113_MI,
	&m34114_MI,
	&m34115_MI,
	&m34111_MI,
	&m34112_MI,
	NULL
};
static TypeInfo* t6595_ITIs[] = 
{
	&t603_TI,
	&t6594_TI,
	&t6596_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6595_0_0_0;
extern Il2CppType t6595_1_0_0;
struct t6595;
extern Il2CppGenericClass t6595_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6595_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6595_MIs, t6595_PIs, NULL, NULL, NULL, NULL, NULL, &t6595_TI, t6595_ITIs, NULL, &t1908__CustomAttributeCache, &t6595_TI, &t6595_0_0_0, &t6595_1_0_0, NULL, &t6595_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2078_TI;

#include "t1629.h"


// Metadata Definition System.IComparable`1<System.DateTimeOffset>
extern Il2CppType t1629_0_0_0;
extern Il2CppType t1629_0_0_0;
static ParameterInfo t2078_m34116_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t1629_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t1629 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34116_GM;
MethodInfo m34116_MI = 
{
	"CompareTo", NULL, &t2078_TI, &t44_0_0_0, RuntimeInvoker_t44_t1629, t2078_m34116_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34116_GM};
static MethodInfo* t2078_MIs[] =
{
	&m34116_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2078_0_0_0;
extern Il2CppType t2078_1_0_0;
struct t2078;
extern Il2CppGenericClass t2078_GC;
TypeInfo t2078_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t2078_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2078_TI, NULL, NULL, &EmptyCustomAttributesCache, &t2078_TI, &t2078_0_0_0, &t2078_1_0_0, NULL, &t2078_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2079_TI;



// Metadata Definition System.IEquatable`1<System.DateTimeOffset>
extern Il2CppType t1629_0_0_0;
static ParameterInfo t2079_m34117_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t1629_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t1629 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34117_GM;
MethodInfo m34117_MI = 
{
	"Equals", NULL, &t2079_TI, &t40_0_0_0, RuntimeInvoker_t40_t1629, t2079_m34117_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34117_GM};
static MethodInfo* t2079_MIs[] =
{
	&m34117_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2079_0_0_0;
extern Il2CppType t2079_1_0_0;
struct t2079;
extern Il2CppGenericClass t2079_GC;
TypeInfo t2079_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t2079_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2079_TI, NULL, NULL, &EmptyCustomAttributesCache, &t2079_TI, &t2079_0_0_0, &t2079_1_0_0, NULL, &t2079_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t2076.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2076_TI;
#include "t2076MD.h"

extern TypeInfo t1629_TI;
#include "t3488MD.h"
extern MethodInfo m19385_MI;
extern MethodInfo m34116_MI;


extern MethodInfo m10258_MI;
 void m10258 (t2076 * __this, MethodInfo* method){
	{
		m19385(__this, &m19385_MI);
		return;
	}
}
extern MethodInfo m19384_MI;
 int32_t m19384 (t2076 * __this, t1629  p0, t1629  p1, MethodInfo* method){
	int32_t G_B4_0 = 0;
	{
		t1629  L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t1629_TI), &L_0);
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		t1629  L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t1629_TI), &L_2);
		if (L_3)
		{
			goto IL_0013;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0014;
	}

IL_0013:
	{
		G_B4_0 = (-1);
	}

IL_0014:
	{
		return G_B4_0;
	}

IL_0015:
	{
		t1629  L_4 = p1;
		t29 * L_5 = Box(InitializedTypeInfo(&t1629_TI), &L_4);
		if (L_5)
		{
			goto IL_001f;
		}
	}
	{
		return 1;
	}

IL_001f:
	{
		int32_t L_6 = (int32_t)InterfaceFuncInvoker1< int32_t, t1629  >::Invoke(&m34116_MI, Box(InitializedTypeInfo(&t1629_TI), &(*(&p0))), p1);
		return L_6;
	}
}
// Metadata Definition System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10258_GM;
MethodInfo m10258_MI = 
{
	".ctor", (methodPointerType)&m10258, &t2076_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10258_GM};
extern Il2CppType t1629_0_0_0;
extern Il2CppType t1629_0_0_0;
static ParameterInfo t2076_m19384_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t1629_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t1629_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t1629_t1629 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19384_GM;
MethodInfo m19384_MI = 
{
	"Compare", (methodPointerType)&m19384, &t2076_TI, &t44_0_0_0, RuntimeInvoker_t44_t1629_t1629, t2076_m19384_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19384_GM};
static MethodInfo* t2076_MIs[] =
{
	&m10258_MI,
	&m19384_MI,
	NULL
};
extern MethodInfo m19387_MI;
static MethodInfo* t2076_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m19384_MI,
	&m19387_MI,
	&m19384_MI,
};
extern TypeInfo t6766_TI;
static Il2CppInterfaceOffsetPair t2076_IOs[] = 
{
	{ &t6766_TI, 4},
	{ &t726_TI, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2076_0_0_0;
extern Il2CppType t2076_1_0_0;
extern TypeInfo t3488_TI;
struct t2076;
extern Il2CppGenericClass t2076_GC;
TypeInfo t2076_TI = 
{
	&g_mscorlib_dll_Image, NULL, "GenericComparer`1", "System.Collections.Generic", t2076_MIs, NULL, NULL, NULL, &t3488_TI, NULL, NULL, &t2076_TI, NULL, t2076_VT, &EmptyCustomAttributesCache, &t2076_TI, &t2076_0_0_0, &t2076_1_0_0, t2076_IOs, &t2076_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2076), 0, -1, 0, 0, -1, 1057024, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 7, 0, 2};
#include "t3488.h"
#ifndef _MSC_VER
#else
#endif

#include "t3489.h"
extern TypeInfo t3489_TI;
#include "t3489MD.h"
extern MethodInfo m19389_MI;
extern MethodInfo m34118_MI;


 void m19385 (t3488 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m19386_MI;
 void m19386 (t29 * __this, MethodInfo* method){
	t3489 * L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer = (t3489 *)il2cpp_codegen_object_new(InitializedTypeInfo(&t3489_TI));
	m19389(L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer, &m19389_MI);
	((t3488_SFs*)InitializedTypeInfo(&t3488_TI)->static_fields)->f0 = L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer;
}
 int32_t m19387 (t3488 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	int32_t G_B4_0 = 0;
	{
		if (p0)
		{
			goto IL_000b;
		}
	}
	{
		if (p1)
		{
			goto IL_0009;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_000a;
	}

IL_0009:
	{
		G_B4_0 = (-1);
	}

IL_000a:
	{
		return G_B4_0;
	}

IL_000b:
	{
		if (p1)
		{
			goto IL_0010;
		}
	}
	{
		return 1;
	}

IL_0010:
	{
		if (!((t29 *)IsInst(p0, InitializedTypeInfo(&t1629_TI))))
		{
			goto IL_0033;
		}
	}
	{
		if (!((t29 *)IsInst(p1, InitializedTypeInfo(&t1629_TI))))
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker2< int32_t, t1629 , t1629  >::Invoke(&m34118_MI, __this, ((*(t1629 *)((t1629 *)UnBox (p0, InitializedTypeInfo(&t1629_TI))))), ((*(t1629 *)((t1629 *)UnBox (p1, InitializedTypeInfo(&t1629_TI))))));
		return L_0;
	}

IL_0033:
	{
		t305 * L_1 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m8852(L_1, &m8852_MI);
		il2cpp_codegen_raise_exception(L_1);
	}
}
extern MethodInfo m19388_MI;
 t3488 * m19388 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t3488_TI));
		return (((t3488_SFs*)InitializedTypeInfo(&t3488_TI)->static_fields)->f0);
	}
}
// Metadata Definition System.Collections.Generic.Comparer`1<System.DateTimeOffset>
extern Il2CppType t3488_0_0_49;
FieldInfo t3488_f0_FieldInfo = 
{
	"_default", &t3488_0_0_49, &t3488_TI, offsetof(t3488_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3488_FIs[] =
{
	&t3488_f0_FieldInfo,
	NULL
};
static PropertyInfo t3488____Default_PropertyInfo = 
{
	&t3488_TI, "Default", &m19388_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3488_PIs[] =
{
	&t3488____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19385_GM;
MethodInfo m19385_MI = 
{
	".ctor", (methodPointerType)&m19385, &t3488_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19385_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19386_GM;
MethodInfo m19386_MI = 
{
	".cctor", (methodPointerType)&m19386, &t3488_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19386_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3488_m19387_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19387_GM;
MethodInfo m19387_MI = 
{
	"System.Collections.IComparer.Compare", (methodPointerType)&m19387, &t3488_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t3488_m19387_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19387_GM};
extern Il2CppType t1629_0_0_0;
extern Il2CppType t1629_0_0_0;
static ParameterInfo t3488_m34118_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t1629_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t1629_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t1629_t1629 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34118_GM;
MethodInfo m34118_MI = 
{
	"Compare", NULL, &t3488_TI, &t44_0_0_0, RuntimeInvoker_t44_t1629_t1629, t3488_m34118_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34118_GM};
extern Il2CppType t3488_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19388_GM;
MethodInfo m19388_MI = 
{
	"get_Default", (methodPointerType)&m19388, &t3488_TI, &t3488_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19388_GM};
static MethodInfo* t3488_MIs[] =
{
	&m19385_MI,
	&m19386_MI,
	&m19387_MI,
	&m34118_MI,
	&m19388_MI,
	NULL
};
static MethodInfo* t3488_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m34118_MI,
	&m19387_MI,
	NULL,
};
static TypeInfo* t3488_ITIs[] = 
{
	&t6766_TI,
	&t726_TI,
};
static Il2CppInterfaceOffsetPair t3488_IOs[] = 
{
	{ &t6766_TI, 4},
	{ &t726_TI, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3488_0_0_0;
extern Il2CppType t3488_1_0_0;
struct t3488;
extern Il2CppGenericClass t3488_GC;
TypeInfo t3488_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparer`1", "System.Collections.Generic", t3488_MIs, t3488_PIs, t3488_FIs, NULL, &t29_TI, NULL, NULL, &t3488_TI, t3488_ITIs, t3488_VT, &EmptyCustomAttributesCache, &t3488_TI, &t3488_0_0_0, &t3488_1_0_0, t3488_IOs, &t3488_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3488), 0, -1, sizeof(t3488_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 5, 1, 1, 0, 0, 7, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IComparer`1<System.DateTimeOffset>
extern Il2CppType t1629_0_0_0;
extern Il2CppType t1629_0_0_0;
static ParameterInfo t6766_m34119_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t1629_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t1629_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t1629_t1629 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34119_GM;
MethodInfo m34119_MI = 
{
	"Compare", NULL, &t6766_TI, &t44_0_0_0, RuntimeInvoker_t44_t1629_t1629, t6766_m34119_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34119_GM};
static MethodInfo* t6766_MIs[] =
{
	&m34119_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6766_0_0_0;
extern Il2CppType t6766_1_0_0;
struct t6766;
extern Il2CppGenericClass t6766_GC;
TypeInfo t6766_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparer`1", "System.Collections.Generic", t6766_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6766_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6766_TI, &t6766_0_0_0, &t6766_1_0_0, NULL, &t6766_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



 void m19389 (t3489 * __this, MethodInfo* method){
	{
		m19385(__this, &m19385_MI);
		return;
	}
}
extern MethodInfo m19390_MI;
 int32_t m19390 (t3489 * __this, t1629  p0, t1629  p1, MethodInfo* method){
	int32_t G_B4_0 = 0;
	{
		t1629  L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t1629_TI), &L_0);
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		t1629  L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t1629_TI), &L_2);
		if (L_3)
		{
			goto IL_0013;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0014;
	}

IL_0013:
	{
		G_B4_0 = (-1);
	}

IL_0014:
	{
		return G_B4_0;
	}

IL_0015:
	{
		t1629  L_4 = p1;
		t29 * L_5 = Box(InitializedTypeInfo(&t1629_TI), &L_4);
		if (L_5)
		{
			goto IL_001f;
		}
	}
	{
		return 1;
	}

IL_001f:
	{
		t1629  L_6 = p0;
		t29 * L_7 = Box(InitializedTypeInfo(&t1629_TI), &L_6);
		if (!((t29*)IsInst(L_7, InitializedTypeInfo(&t2078_TI))))
		{
			goto IL_003e;
		}
	}
	{
		t1629  L_8 = p0;
		t29 * L_9 = Box(InitializedTypeInfo(&t1629_TI), &L_8);
		int32_t L_10 = (int32_t)InterfaceFuncInvoker1< int32_t, t1629  >::Invoke(&m34116_MI, ((t29*)Castclass(L_9, InitializedTypeInfo(&t2078_TI))), p1);
		return L_10;
	}

IL_003e:
	{
		t1629  L_11 = p0;
		t29 * L_12 = Box(InitializedTypeInfo(&t1629_TI), &L_11);
		if (!((t29 *)IsInst(L_12, InitializedTypeInfo(&t290_TI))))
		{
			goto IL_0062;
		}
	}
	{
		t1629  L_13 = p0;
		t29 * L_14 = Box(InitializedTypeInfo(&t1629_TI), &L_13);
		t1629  L_15 = p1;
		t29 * L_16 = Box(InitializedTypeInfo(&t1629_TI), &L_15);
		int32_t L_17 = (int32_t)InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(&m9672_MI, ((t29 *)Castclass(L_14, InitializedTypeInfo(&t290_TI))), L_16);
		return L_17;
	}

IL_0062:
	{
		t305 * L_18 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_18, (t7*) &_stringLiteral1162, &m1935_MI);
		il2cpp_codegen_raise_exception(L_18);
	}
}
// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTimeOffset>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19389_GM;
MethodInfo m19389_MI = 
{
	".ctor", (methodPointerType)&m19389, &t3489_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19389_GM};
extern Il2CppType t1629_0_0_0;
extern Il2CppType t1629_0_0_0;
static ParameterInfo t3489_m19390_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t1629_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t1629_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t1629_t1629 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19390_GM;
MethodInfo m19390_MI = 
{
	"Compare", (methodPointerType)&m19390, &t3489_TI, &t44_0_0_0, RuntimeInvoker_t44_t1629_t1629, t3489_m19390_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19390_GM};
static MethodInfo* t3489_MIs[] =
{
	&m19389_MI,
	&m19390_MI,
	NULL
};
static MethodInfo* t3489_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m19390_MI,
	&m19387_MI,
	&m19390_MI,
};
static Il2CppInterfaceOffsetPair t3489_IOs[] = 
{
	{ &t6766_TI, 4},
	{ &t726_TI, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3489_0_0_0;
extern Il2CppType t3489_1_0_0;
struct t3489;
extern Il2CppGenericClass t3489_GC;
TypeInfo t3489_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t3489_MIs, NULL, NULL, NULL, &t3488_TI, NULL, &t1246_TI, &t3489_TI, NULL, t3489_VT, &EmptyCustomAttributesCache, &t3489_TI, &t3489_0_0_0, &t3489_1_0_0, t3489_IOs, &t3489_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3489), 0, -1, 0, 0, -1, 1048835, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 7, 0, 2};
#include "t2077.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2077_TI;
#include "t2077MD.h"

#include "t3490MD.h"
extern MethodInfo m19393_MI;
extern MethodInfo m34117_MI;


extern MethodInfo m10259_MI;
 void m10259 (t2077 * __this, MethodInfo* method){
	{
		m19393(__this, &m19393_MI);
		return;
	}
}
extern MethodInfo m19391_MI;
 int32_t m19391 (t2077 * __this, t1629  p0, MethodInfo* method){
	{
		t1629  L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t1629_TI), &L_0);
		if (L_1)
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}

IL_000a:
	{
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1322_MI, Box(InitializedTypeInfo(&t1629_TI), &(*(&p0))));
		return L_2;
	}
}
extern MethodInfo m19392_MI;
 bool m19392 (t2077 * __this, t1629  p0, t1629  p1, MethodInfo* method){
	{
		t1629  L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t1629_TI), &L_0);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		t1629  L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t1629_TI), &L_2);
		return ((((t29 *)L_3) == ((t29 *)NULL))? 1 : 0);
	}

IL_0012:
	{
		bool L_4 = (bool)InterfaceFuncInvoker1< bool, t1629  >::Invoke(&m34117_MI, Box(InitializedTypeInfo(&t1629_TI), &(*(&p0))), p1);
		return L_4;
	}
}
// Metadata Definition System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10259_GM;
MethodInfo m10259_MI = 
{
	".ctor", (methodPointerType)&m10259, &t2077_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10259_GM};
extern Il2CppType t1629_0_0_0;
static ParameterInfo t2077_m19391_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t1629_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t1629 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19391_GM;
MethodInfo m19391_MI = 
{
	"GetHashCode", (methodPointerType)&m19391, &t2077_TI, &t44_0_0_0, RuntimeInvoker_t44_t1629, t2077_m19391_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19391_GM};
extern Il2CppType t1629_0_0_0;
extern Il2CppType t1629_0_0_0;
static ParameterInfo t2077_m19392_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t1629_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t1629_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t1629_t1629 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19392_GM;
MethodInfo m19392_MI = 
{
	"Equals", (methodPointerType)&m19392, &t2077_TI, &t40_0_0_0, RuntimeInvoker_t40_t1629_t1629, t2077_m19392_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19392_GM};
static MethodInfo* t2077_MIs[] =
{
	&m10259_MI,
	&m19391_MI,
	&m19392_MI,
	NULL
};
extern MethodInfo m19396_MI;
extern MethodInfo m19395_MI;
static MethodInfo* t2077_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m19392_MI,
	&m19391_MI,
	&m19396_MI,
	&m19395_MI,
	&m19391_MI,
	&m19392_MI,
};
extern TypeInfo t6767_TI;
static Il2CppInterfaceOffsetPair t2077_IOs[] = 
{
	{ &t6767_TI, 4},
	{ &t734_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2077_0_0_0;
extern Il2CppType t2077_1_0_0;
extern TypeInfo t3490_TI;
struct t2077;
extern Il2CppGenericClass t2077_GC;
TypeInfo t2077_TI = 
{
	&g_mscorlib_dll_Image, NULL, "GenericEqualityComparer`1", "System.Collections.Generic", t2077_MIs, NULL, NULL, NULL, &t3490_TI, NULL, NULL, &t2077_TI, NULL, t2077_VT, &EmptyCustomAttributesCache, &t2077_TI, &t2077_0_0_0, &t2077_1_0_0, t2077_IOs, &t2077_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2077), 0, -1, 0, 0, -1, 1057024, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#include "t3490.h"
#ifndef _MSC_VER
#else
#endif

#include "t3491.h"
extern TypeInfo t3491_TI;
#include "t3491MD.h"
extern MethodInfo m19398_MI;
extern MethodInfo m34120_MI;
extern MethodInfo m34121_MI;


 void m19393 (t3490 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m19394_MI;
 void m19394 (t29 * __this, MethodInfo* method){
	t3491 * L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer = (t3491 *)il2cpp_codegen_object_new(InitializedTypeInfo(&t3491_TI));
	m19398(L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer, &m19398_MI);
	((t3490_SFs*)InitializedTypeInfo(&t3490_TI)->static_fields)->f0 = L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer;
}
 int32_t m19395 (t3490 * __this, t29 * p0, MethodInfo* method){
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker1< int32_t, t1629  >::Invoke(&m34120_MI, __this, ((*(t1629 *)((t1629 *)UnBox (p0, InitializedTypeInfo(&t1629_TI))))));
		return L_0;
	}
}
 bool m19396 (t3490 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	{
		bool L_0 = (bool)VirtFuncInvoker2< bool, t1629 , t1629  >::Invoke(&m34121_MI, __this, ((*(t1629 *)((t1629 *)UnBox (p0, InitializedTypeInfo(&t1629_TI))))), ((*(t1629 *)((t1629 *)UnBox (p1, InitializedTypeInfo(&t1629_TI))))));
		return L_0;
	}
}
extern MethodInfo m19397_MI;
 t3490 * m19397 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t3490_TI));
		return (((t3490_SFs*)InitializedTypeInfo(&t3490_TI)->static_fields)->f0);
	}
}
// Metadata Definition System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>
extern Il2CppType t3490_0_0_49;
FieldInfo t3490_f0_FieldInfo = 
{
	"_default", &t3490_0_0_49, &t3490_TI, offsetof(t3490_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3490_FIs[] =
{
	&t3490_f0_FieldInfo,
	NULL
};
static PropertyInfo t3490____Default_PropertyInfo = 
{
	&t3490_TI, "Default", &m19397_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3490_PIs[] =
{
	&t3490____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19393_GM;
MethodInfo m19393_MI = 
{
	".ctor", (methodPointerType)&m19393, &t3490_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19393_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19394_GM;
MethodInfo m19394_MI = 
{
	".cctor", (methodPointerType)&m19394, &t3490_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19394_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3490_m19395_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19395_GM;
MethodInfo m19395_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m19395, &t3490_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3490_m19395_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19395_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3490_m19396_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19396_GM;
MethodInfo m19396_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m19396, &t3490_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t3490_m19396_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19396_GM};
extern Il2CppType t1629_0_0_0;
static ParameterInfo t3490_m34120_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t1629_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t1629 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34120_GM;
MethodInfo m34120_MI = 
{
	"GetHashCode", NULL, &t3490_TI, &t44_0_0_0, RuntimeInvoker_t44_t1629, t3490_m34120_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34120_GM};
extern Il2CppType t1629_0_0_0;
extern Il2CppType t1629_0_0_0;
static ParameterInfo t3490_m34121_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t1629_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t1629_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t1629_t1629 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34121_GM;
MethodInfo m34121_MI = 
{
	"Equals", NULL, &t3490_TI, &t40_0_0_0, RuntimeInvoker_t40_t1629_t1629, t3490_m34121_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34121_GM};
extern Il2CppType t3490_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19397_GM;
MethodInfo m19397_MI = 
{
	"get_Default", (methodPointerType)&m19397, &t3490_TI, &t3490_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19397_GM};
static MethodInfo* t3490_MIs[] =
{
	&m19393_MI,
	&m19394_MI,
	&m19395_MI,
	&m19396_MI,
	&m34120_MI,
	&m34121_MI,
	&m19397_MI,
	NULL
};
static MethodInfo* t3490_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m34121_MI,
	&m34120_MI,
	&m19396_MI,
	&m19395_MI,
	NULL,
	NULL,
};
static TypeInfo* t3490_ITIs[] = 
{
	&t6767_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t3490_IOs[] = 
{
	{ &t6767_TI, 4},
	{ &t734_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3490_0_0_0;
extern Il2CppType t3490_1_0_0;
struct t3490;
extern Il2CppGenericClass t3490_GC;
TypeInfo t3490_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t3490_MIs, t3490_PIs, t3490_FIs, NULL, &t29_TI, NULL, NULL, &t3490_TI, t3490_ITIs, t3490_VT, &EmptyCustomAttributesCache, &t3490_TI, &t3490_0_0_0, &t3490_1_0_0, t3490_IOs, &t3490_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3490), 0, -1, sizeof(t3490_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<System.DateTimeOffset>
extern Il2CppType t1629_0_0_0;
extern Il2CppType t1629_0_0_0;
static ParameterInfo t6767_m34122_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t1629_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t1629_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t1629_t1629 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34122_GM;
MethodInfo m34122_MI = 
{
	"Equals", NULL, &t6767_TI, &t40_0_0_0, RuntimeInvoker_t40_t1629_t1629, t6767_m34122_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34122_GM};
extern Il2CppType t1629_0_0_0;
static ParameterInfo t6767_m34123_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t1629_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t1629 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34123_GM;
MethodInfo m34123_MI = 
{
	"GetHashCode", NULL, &t6767_TI, &t44_0_0_0, RuntimeInvoker_t44_t1629, t6767_m34123_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34123_GM};
static MethodInfo* t6767_MIs[] =
{
	&m34122_MI,
	&m34123_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6767_0_0_0;
extern Il2CppType t6767_1_0_0;
struct t6767;
extern Il2CppGenericClass t6767_GC;
TypeInfo t6767_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t6767_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6767_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6767_TI, &t6767_0_0_0, &t6767_1_0_0, NULL, &t6767_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



 void m19398 (t3491 * __this, MethodInfo* method){
	{
		m19393(__this, &m19393_MI);
		return;
	}
}
extern MethodInfo m19399_MI;
 int32_t m19399 (t3491 * __this, t1629  p0, MethodInfo* method){
	{
		t1629  L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t1629_TI), &L_0);
		if (L_1)
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}

IL_000a:
	{
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1322_MI, Box(InitializedTypeInfo(&t1629_TI), &(*(&p0))));
		return L_2;
	}
}
extern MethodInfo m19400_MI;
 bool m19400 (t3491 * __this, t1629  p0, t1629  p1, MethodInfo* method){
	{
		t1629  L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t1629_TI), &L_0);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		t1629  L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t1629_TI), &L_2);
		return ((((t29 *)L_3) == ((t29 *)NULL))? 1 : 0);
	}

IL_0012:
	{
		t1629  L_4 = p1;
		t29 * L_5 = Box(InitializedTypeInfo(&t1629_TI), &L_4);
		bool L_6 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1629_TI), &(*(&p0))), L_5);
		return L_6;
	}
}
// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTimeOffset>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19398_GM;
MethodInfo m19398_MI = 
{
	".ctor", (methodPointerType)&m19398, &t3491_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19398_GM};
extern Il2CppType t1629_0_0_0;
static ParameterInfo t3491_m19399_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t1629_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t1629 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19399_GM;
MethodInfo m19399_MI = 
{
	"GetHashCode", (methodPointerType)&m19399, &t3491_TI, &t44_0_0_0, RuntimeInvoker_t44_t1629, t3491_m19399_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19399_GM};
extern Il2CppType t1629_0_0_0;
extern Il2CppType t1629_0_0_0;
static ParameterInfo t3491_m19400_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t1629_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t1629_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t1629_t1629 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19400_GM;
MethodInfo m19400_MI = 
{
	"Equals", (methodPointerType)&m19400, &t3491_TI, &t40_0_0_0, RuntimeInvoker_t40_t1629_t1629, t3491_m19400_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19400_GM};
static MethodInfo* t3491_MIs[] =
{
	&m19398_MI,
	&m19399_MI,
	&m19400_MI,
	NULL
};
static MethodInfo* t3491_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m19400_MI,
	&m19399_MI,
	&m19396_MI,
	&m19395_MI,
	&m19399_MI,
	&m19400_MI,
};
static Il2CppInterfaceOffsetPair t3491_IOs[] = 
{
	{ &t6767_TI, 4},
	{ &t734_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3491_0_0_0;
extern Il2CppType t3491_1_0_0;
struct t3491;
extern Il2CppGenericClass t3491_GC;
TypeInfo t3491_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t3491_MIs, NULL, NULL, NULL, &t3490_TI, NULL, &t1256_TI, &t3491_TI, NULL, t3491_VT, &EmptyCustomAttributesCache, &t3491_TI, &t3491_0_0_0, &t3491_1_0_0, t3491_IOs, &t3491_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3491), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#include "t1631.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1631_TI;
#include "t1631MD.h"

#include "t816.h"
#include "t110.h"
extern TypeInfo t816_TI;
extern TypeInfo t7_TI;
#include "t110MD.h"
#include "t7MD.h"
extern MethodInfo m19402_MI;


extern MethodInfo m10260_MI;
 void m10260 (t1631 * __this, t816  p0, MethodInfo* method){
	{
		__this->f1 = 1;
		__this->f0 = p0;
		return;
	}
}
extern MethodInfo m10261_MI;
 bool m10261 (t1631 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->f1);
		return L_0;
	}
}
extern MethodInfo m10262_MI;
 t816  m10262 (t1631 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->f1);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1077, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t816  L_2 = (__this->f0);
		return L_2;
	}
}
extern MethodInfo m19401_MI;
 bool m19401 (t1631 * __this, t29 * p0, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000d;
		}
	}
	{
		bool L_0 = (__this->f1);
		return ((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}

IL_000d:
	{
		if (((t29 *)IsInst(p0, InitializedTypeInfo(&t1631_TI))))
		{
			goto IL_0017;
		}
	}
	{
		return 0;
	}

IL_0017:
	{
		void* L_1 = alloca(sizeof(t1631 ));
		UnBoxNullable(p0, InitializedTypeInfo(&t1631_TI), L_1);
		bool L_2 = m19402(__this, ((*(t1631 *)((t1631 *)L_1))), &m19402_MI);
		return L_2;
	}
}
 bool m19402 (t1631 * __this, t1631  p0, MethodInfo* method){
	{
		bool L_0 = ((&p0)->f1);
		bool L_1 = (__this->f1);
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0011;
		}
	}
	{
		return 0;
	}

IL_0011:
	{
		bool L_2 = (__this->f1);
		if (L_2)
		{
			goto IL_001b;
		}
	}
	{
		return 1;
	}

IL_001b:
	{
		t816 * L_3 = &((&p0)->f0);
		t816  L_4 = (__this->f0);
		t816  L_5 = L_4;
		t29 * L_6 = Box(InitializedTypeInfo(&t816_TI), &L_5);
		bool L_7 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1388_MI, Box(InitializedTypeInfo(&t816_TI), &(*L_3)), L_6);
		return L_7;
	}
}
extern MethodInfo m19403_MI;
 int32_t m19403 (t1631 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->f1);
		if (L_0)
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}

IL_000a:
	{
		t816 * L_1 = &(__this->f0);
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1389_MI, Box(InitializedTypeInfo(&t816_TI), &(*L_1)));
		return L_2;
	}
}
extern MethodInfo m19404_MI;
 t7* m19404 (t1631 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->f1);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		t816 * L_1 = &(__this->f0);
		t7* L_2 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1500_MI, Box(InitializedTypeInfo(&t816_TI), &(*L_1)));
		return L_2;
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		return (((t7_SFs*)(&t7_TI)->static_fields)->f2);
	}
}
// Metadata Definition System.Nullable`1<System.TimeSpan>
extern Il2CppType t816_0_0_3;
FieldInfo t1631_f0_FieldInfo = 
{
	"value", &t816_0_0_3, &t1631_TI, offsetof(t1631, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_3;
FieldInfo t1631_f1_FieldInfo = 
{
	"has_value", &t40_0_0_3, &t1631_TI, offsetof(t1631, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t1631_FIs[] =
{
	&t1631_f0_FieldInfo,
	&t1631_f1_FieldInfo,
	NULL
};
static PropertyInfo t1631____HasValue_PropertyInfo = 
{
	&t1631_TI, "HasValue", &m10261_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1631____Value_PropertyInfo = 
{
	&t1631_TI, "Value", &m10262_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1631_PIs[] =
{
	&t1631____HasValue_PropertyInfo,
	&t1631____Value_PropertyInfo,
	NULL
};
extern Il2CppType t816_0_0_0;
extern Il2CppType t816_0_0_0;
static ParameterInfo t1631_m10260_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t816 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10260_GM;
MethodInfo m10260_MI = 
{
	".ctor", (methodPointerType)&m10260, &t1631_TI, &t21_0_0_0, RuntimeInvoker_t21_t816, t1631_m10260_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10260_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10261_GM;
MethodInfo m10261_MI = 
{
	"get_HasValue", (methodPointerType)&m10261, &t1631_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10261_GM};
extern Il2CppType t816_0_0_0;
extern void* RuntimeInvoker_t816 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10262_GM;
MethodInfo m10262_MI = 
{
	"get_Value", (methodPointerType)&m10262, &t1631_TI, &t816_0_0_0, RuntimeInvoker_t816, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10262_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t1631_m19401_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19401_GM;
MethodInfo m19401_MI = 
{
	"Equals", (methodPointerType)&m19401, &t1631_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t1631_m19401_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19401_GM};
extern Il2CppType t1631_0_0_0;
extern Il2CppType t1631_0_0_0;
static ParameterInfo t1631_m19402_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t1631_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t1631 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19402_GM;
MethodInfo m19402_MI = 
{
	"Equals", (methodPointerType)&m19402, &t1631_TI, &t40_0_0_0, RuntimeInvoker_t40_t1631, t1631_m19402_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19402_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19403_GM;
MethodInfo m19403_MI = 
{
	"GetHashCode", (methodPointerType)&m19403, &t1631_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 198, 0, 2, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19403_GM};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19404_GM;
MethodInfo m19404_MI = 
{
	"ToString", (methodPointerType)&m19404, &t1631_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19404_GM};
static MethodInfo* t1631_MIs[] =
{
	&m10260_MI,
	&m10261_MI,
	&m10262_MI,
	&m19401_MI,
	&m19402_MI,
	&m19403_MI,
	&m19404_MI,
	NULL
};
static MethodInfo* t1631_VT[] =
{
	&m19401_MI,
	&m46_MI,
	&m19403_MI,
	&m19404_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1631_1_0_0;
extern Il2CppGenericClass t1631_GC;
TypeInfo t1631_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Nullable`1", "System", t1631_MIs, t1631_PIs, t1631_FIs, NULL, &t110_TI, NULL, NULL, &t1631_TI, NULL, t1631_VT, &EmptyCustomAttributesCache, &t1631_TI, &t1631_0_0_0, &t1631_1_0_0, NULL, &t1631_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1631)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057033, 0, true, false, false, false, true, false, false, false, false, false, false, false, 7, 2, 2, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5037_TI;

#include "t1292.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.DayOfWeek>
extern MethodInfo m34124_MI;
static PropertyInfo t5037____Current_PropertyInfo = 
{
	&t5037_TI, "Current", &m34124_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5037_PIs[] =
{
	&t5037____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1292_0_0_0;
extern void* RuntimeInvoker_t1292 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34124_GM;
MethodInfo m34124_MI = 
{
	"get_Current", NULL, &t5037_TI, &t1292_0_0_0, RuntimeInvoker_t1292, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34124_GM};
static MethodInfo* t5037_MIs[] =
{
	&m34124_MI,
	NULL
};
static TypeInfo* t5037_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5037_0_0_0;
extern Il2CppType t5037_1_0_0;
struct t5037;
extern Il2CppGenericClass t5037_GC;
TypeInfo t5037_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t5037_MIs, t5037_PIs, NULL, NULL, NULL, NULL, NULL, &t5037_TI, t5037_ITIs, NULL, &EmptyCustomAttributesCache, &t5037_TI, &t5037_0_0_0, &t5037_1_0_0, NULL, &t5037_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3492.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3492_TI;
#include "t3492MD.h"

extern TypeInfo t1292_TI;
extern MethodInfo m19409_MI;
extern MethodInfo m26143_MI;
struct t20;
 int32_t m26143 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m19405_MI;
 void m19405 (t3492 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19406_MI;
 t29 * m19406 (t3492 * __this, MethodInfo* method){
	{
		int32_t L_0 = m19409(__this, &m19409_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1292_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m19407_MI;
 void m19407 (t3492 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19408_MI;
 bool m19408 (t3492 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m19409 (t3492 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m26143(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m26143_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.DayOfWeek>
extern Il2CppType t20_0_0_1;
FieldInfo t3492_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3492_TI, offsetof(t3492, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3492_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3492_TI, offsetof(t3492, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3492_FIs[] =
{
	&t3492_f0_FieldInfo,
	&t3492_f1_FieldInfo,
	NULL
};
static PropertyInfo t3492____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3492_TI, "System.Collections.IEnumerator.Current", &m19406_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3492____Current_PropertyInfo = 
{
	&t3492_TI, "Current", &m19409_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3492_PIs[] =
{
	&t3492____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3492____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3492_m19405_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19405_GM;
MethodInfo m19405_MI = 
{
	".ctor", (methodPointerType)&m19405, &t3492_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3492_m19405_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19405_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19406_GM;
MethodInfo m19406_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m19406, &t3492_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19406_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19407_GM;
MethodInfo m19407_MI = 
{
	"Dispose", (methodPointerType)&m19407, &t3492_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19407_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19408_GM;
MethodInfo m19408_MI = 
{
	"MoveNext", (methodPointerType)&m19408, &t3492_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19408_GM};
extern Il2CppType t1292_0_0_0;
extern void* RuntimeInvoker_t1292 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19409_GM;
MethodInfo m19409_MI = 
{
	"get_Current", (methodPointerType)&m19409, &t3492_TI, &t1292_0_0_0, RuntimeInvoker_t1292, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19409_GM};
static MethodInfo* t3492_MIs[] =
{
	&m19405_MI,
	&m19406_MI,
	&m19407_MI,
	&m19408_MI,
	&m19409_MI,
	NULL
};
static MethodInfo* t3492_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19406_MI,
	&m19408_MI,
	&m19407_MI,
	&m19409_MI,
};
static TypeInfo* t3492_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t5037_TI,
};
static Il2CppInterfaceOffsetPair t3492_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t5037_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3492_0_0_0;
extern Il2CppType t3492_1_0_0;
extern Il2CppGenericClass t3492_GC;
TypeInfo t3492_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3492_MIs, t3492_PIs, t3492_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3492_TI, t3492_ITIs, t3492_VT, &EmptyCustomAttributesCache, &t3492_TI, &t3492_0_0_0, &t3492_1_0_0, t3492_IOs, &t3492_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3492)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6597_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.DayOfWeek>
extern MethodInfo m34125_MI;
static PropertyInfo t6597____Count_PropertyInfo = 
{
	&t6597_TI, "Count", &m34125_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m34126_MI;
static PropertyInfo t6597____IsReadOnly_PropertyInfo = 
{
	&t6597_TI, "IsReadOnly", &m34126_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6597_PIs[] =
{
	&t6597____Count_PropertyInfo,
	&t6597____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34125_GM;
MethodInfo m34125_MI = 
{
	"get_Count", NULL, &t6597_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34125_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34126_GM;
MethodInfo m34126_MI = 
{
	"get_IsReadOnly", NULL, &t6597_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34126_GM};
extern Il2CppType t1292_0_0_0;
extern Il2CppType t1292_0_0_0;
static ParameterInfo t6597_m34127_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1292_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34127_GM;
MethodInfo m34127_MI = 
{
	"Add", NULL, &t6597_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6597_m34127_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34127_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34128_GM;
MethodInfo m34128_MI = 
{
	"Clear", NULL, &t6597_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34128_GM};
extern Il2CppType t1292_0_0_0;
static ParameterInfo t6597_m34129_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1292_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34129_GM;
MethodInfo m34129_MI = 
{
	"Contains", NULL, &t6597_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6597_m34129_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34129_GM};
extern Il2CppType t3718_0_0_0;
extern Il2CppType t3718_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6597_m34130_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3718_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34130_GM;
MethodInfo m34130_MI = 
{
	"CopyTo", NULL, &t6597_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6597_m34130_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34130_GM};
extern Il2CppType t1292_0_0_0;
static ParameterInfo t6597_m34131_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1292_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34131_GM;
MethodInfo m34131_MI = 
{
	"Remove", NULL, &t6597_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6597_m34131_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34131_GM};
static MethodInfo* t6597_MIs[] =
{
	&m34125_MI,
	&m34126_MI,
	&m34127_MI,
	&m34128_MI,
	&m34129_MI,
	&m34130_MI,
	&m34131_MI,
	NULL
};
extern TypeInfo t6599_TI;
static TypeInfo* t6597_ITIs[] = 
{
	&t603_TI,
	&t6599_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6597_0_0_0;
extern Il2CppType t6597_1_0_0;
struct t6597;
extern Il2CppGenericClass t6597_GC;
TypeInfo t6597_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6597_MIs, t6597_PIs, NULL, NULL, NULL, NULL, NULL, &t6597_TI, t6597_ITIs, NULL, &EmptyCustomAttributesCache, &t6597_TI, &t6597_0_0_0, &t6597_1_0_0, NULL, &t6597_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.DayOfWeek>
extern Il2CppType t5037_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34132_GM;
MethodInfo m34132_MI = 
{
	"GetEnumerator", NULL, &t6599_TI, &t5037_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34132_GM};
static MethodInfo* t6599_MIs[] =
{
	&m34132_MI,
	NULL
};
static TypeInfo* t6599_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6599_0_0_0;
extern Il2CppType t6599_1_0_0;
struct t6599;
extern Il2CppGenericClass t6599_GC;
TypeInfo t6599_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6599_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6599_TI, t6599_ITIs, NULL, &EmptyCustomAttributesCache, &t6599_TI, &t6599_0_0_0, &t6599_1_0_0, NULL, &t6599_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6598_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.DayOfWeek>
extern MethodInfo m34133_MI;
extern MethodInfo m34134_MI;
static PropertyInfo t6598____Item_PropertyInfo = 
{
	&t6598_TI, "Item", &m34133_MI, &m34134_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6598_PIs[] =
{
	&t6598____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1292_0_0_0;
static ParameterInfo t6598_m34135_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1292_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34135_GM;
MethodInfo m34135_MI = 
{
	"IndexOf", NULL, &t6598_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6598_m34135_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34135_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1292_0_0_0;
static ParameterInfo t6598_m34136_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1292_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34136_GM;
MethodInfo m34136_MI = 
{
	"Insert", NULL, &t6598_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6598_m34136_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34136_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6598_m34137_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34137_GM;
MethodInfo m34137_MI = 
{
	"RemoveAt", NULL, &t6598_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6598_m34137_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34137_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6598_m34133_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1292_0_0_0;
extern void* RuntimeInvoker_t1292_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34133_GM;
MethodInfo m34133_MI = 
{
	"get_Item", NULL, &t6598_TI, &t1292_0_0_0, RuntimeInvoker_t1292_t44, t6598_m34133_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34133_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1292_0_0_0;
static ParameterInfo t6598_m34134_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1292_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34134_GM;
MethodInfo m34134_MI = 
{
	"set_Item", NULL, &t6598_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6598_m34134_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34134_GM};
static MethodInfo* t6598_MIs[] =
{
	&m34135_MI,
	&m34136_MI,
	&m34137_MI,
	&m34133_MI,
	&m34134_MI,
	NULL
};
static TypeInfo* t6598_ITIs[] = 
{
	&t603_TI,
	&t6597_TI,
	&t6599_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6598_0_0_0;
extern Il2CppType t6598_1_0_0;
struct t6598;
extern Il2CppGenericClass t6598_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6598_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6598_MIs, t6598_PIs, NULL, NULL, NULL, NULL, NULL, &t6598_TI, t6598_ITIs, NULL, &t1908__CustomAttributeCache, &t6598_TI, &t6598_0_0_0, &t6598_1_0_0, NULL, &t6598_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2080_TI;

#include "t297.h"


// Metadata Definition System.Collections.Generic.IComparer`1<System.SByte>
extern Il2CppType t297_0_0_0;
extern Il2CppType t297_0_0_0;
extern Il2CppType t297_0_0_0;
static ParameterInfo t2080_m34138_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t297_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t297_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t297_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34138_GM;
MethodInfo m34138_MI = 
{
	"Compare", NULL, &t2080_TI, &t44_0_0_0, RuntimeInvoker_t44_t297_t297, t2080_m34138_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34138_GM};
static MethodInfo* t2080_MIs[] =
{
	&m34138_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2080_0_0_0;
extern Il2CppType t2080_1_0_0;
struct t2080;
extern Il2CppGenericClass t2080_GC;
TypeInfo t2080_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparer`1", "System.Collections.Generic", t2080_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2080_TI, NULL, NULL, &EmptyCustomAttributesCache, &t2080_TI, &t2080_0_0_0, &t2080_1_0_0, NULL, &t2080_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2081_TI;

#include "t372.h"


// Metadata Definition System.Collections.Generic.IComparer`1<System.Int16>
extern Il2CppType t372_0_0_0;
extern Il2CppType t372_0_0_0;
extern Il2CppType t372_0_0_0;
static ParameterInfo t2081_m34139_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t372_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t372_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t372_t372 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34139_GM;
MethodInfo m34139_MI = 
{
	"Compare", NULL, &t2081_TI, &t44_0_0_0, RuntimeInvoker_t44_t372_t372, t2081_m34139_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34139_GM};
static MethodInfo* t2081_MIs[] =
{
	&m34139_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2081_0_0_0;
extern Il2CppType t2081_1_0_0;
struct t2081;
extern Il2CppGenericClass t2081_GC;
TypeInfo t2081_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparer`1", "System.Collections.Generic", t2081_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2081_TI, NULL, NULL, &EmptyCustomAttributesCache, &t2081_TI, &t2081_0_0_0, &t2081_1_0_0, NULL, &t2081_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2083_TI;

#include "t919.h"


// Metadata Definition System.Collections.Generic.IComparer`1<System.Int64>
extern Il2CppType t919_0_0_0;
extern Il2CppType t919_0_0_0;
extern Il2CppType t919_0_0_0;
static ParameterInfo t2083_m34140_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t919_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t919_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t919_t919 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34140_GM;
MethodInfo m34140_MI = 
{
	"Compare", NULL, &t2083_TI, &t44_0_0_0, RuntimeInvoker_t44_t919_t919, t2083_m34140_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34140_GM};
static MethodInfo* t2083_MIs[] =
{
	&m34140_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2083_0_0_0;
extern Il2CppType t2083_1_0_0;
struct t2083;
extern Il2CppGenericClass t2083_GC;
TypeInfo t2083_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparer`1", "System.Collections.Generic", t2083_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2083_TI, NULL, NULL, &EmptyCustomAttributesCache, &t2083_TI, &t2083_0_0_0, &t2083_1_0_0, NULL, &t2083_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5039_TI;

#include "t1112.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Environment/SpecialFolder>
extern MethodInfo m34141_MI;
static PropertyInfo t5039____Current_PropertyInfo = 
{
	&t5039_TI, "Current", &m34141_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5039_PIs[] =
{
	&t5039____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1112_0_0_0;
extern void* RuntimeInvoker_t1112 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34141_GM;
MethodInfo m34141_MI = 
{
	"get_Current", NULL, &t5039_TI, &t1112_0_0_0, RuntimeInvoker_t1112, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34141_GM};
static MethodInfo* t5039_MIs[] =
{
	&m34141_MI,
	NULL
};
static TypeInfo* t5039_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5039_0_0_0;
extern Il2CppType t5039_1_0_0;
struct t5039;
extern Il2CppGenericClass t5039_GC;
TypeInfo t5039_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t5039_MIs, t5039_PIs, NULL, NULL, NULL, NULL, NULL, &t5039_TI, t5039_ITIs, NULL, &EmptyCustomAttributesCache, &t5039_TI, &t5039_0_0_0, &t5039_1_0_0, NULL, &t5039_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3493.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3493_TI;
#include "t3493MD.h"

extern TypeInfo t1112_TI;
extern MethodInfo m19414_MI;
extern MethodInfo m26154_MI;
struct t20;
 int32_t m26154 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m19410_MI;
 void m19410 (t3493 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19411_MI;
 t29 * m19411 (t3493 * __this, MethodInfo* method){
	{
		int32_t L_0 = m19414(__this, &m19414_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1112_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m19412_MI;
 void m19412 (t3493 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19413_MI;
 bool m19413 (t3493 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m19414 (t3493 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m26154(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m26154_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Environment/SpecialFolder>
extern Il2CppType t20_0_0_1;
FieldInfo t3493_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3493_TI, offsetof(t3493, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3493_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3493_TI, offsetof(t3493, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3493_FIs[] =
{
	&t3493_f0_FieldInfo,
	&t3493_f1_FieldInfo,
	NULL
};
static PropertyInfo t3493____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3493_TI, "System.Collections.IEnumerator.Current", &m19411_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3493____Current_PropertyInfo = 
{
	&t3493_TI, "Current", &m19414_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3493_PIs[] =
{
	&t3493____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3493____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3493_m19410_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19410_GM;
MethodInfo m19410_MI = 
{
	".ctor", (methodPointerType)&m19410, &t3493_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3493_m19410_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19410_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19411_GM;
MethodInfo m19411_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m19411, &t3493_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19411_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19412_GM;
MethodInfo m19412_MI = 
{
	"Dispose", (methodPointerType)&m19412, &t3493_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19412_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19413_GM;
MethodInfo m19413_MI = 
{
	"MoveNext", (methodPointerType)&m19413, &t3493_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19413_GM};
extern Il2CppType t1112_0_0_0;
extern void* RuntimeInvoker_t1112 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19414_GM;
MethodInfo m19414_MI = 
{
	"get_Current", (methodPointerType)&m19414, &t3493_TI, &t1112_0_0_0, RuntimeInvoker_t1112, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19414_GM};
static MethodInfo* t3493_MIs[] =
{
	&m19410_MI,
	&m19411_MI,
	&m19412_MI,
	&m19413_MI,
	&m19414_MI,
	NULL
};
static MethodInfo* t3493_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19411_MI,
	&m19413_MI,
	&m19412_MI,
	&m19414_MI,
};
static TypeInfo* t3493_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t5039_TI,
};
static Il2CppInterfaceOffsetPair t3493_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t5039_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3493_0_0_0;
extern Il2CppType t3493_1_0_0;
extern Il2CppGenericClass t3493_GC;
TypeInfo t3493_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3493_MIs, t3493_PIs, t3493_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3493_TI, t3493_ITIs, t3493_VT, &EmptyCustomAttributesCache, &t3493_TI, &t3493_0_0_0, &t3493_1_0_0, t3493_IOs, &t3493_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3493)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6600_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Environment/SpecialFolder>
extern MethodInfo m34142_MI;
static PropertyInfo t6600____Count_PropertyInfo = 
{
	&t6600_TI, "Count", &m34142_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m34143_MI;
static PropertyInfo t6600____IsReadOnly_PropertyInfo = 
{
	&t6600_TI, "IsReadOnly", &m34143_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6600_PIs[] =
{
	&t6600____Count_PropertyInfo,
	&t6600____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34142_GM;
MethodInfo m34142_MI = 
{
	"get_Count", NULL, &t6600_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34142_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34143_GM;
MethodInfo m34143_MI = 
{
	"get_IsReadOnly", NULL, &t6600_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34143_GM};
extern Il2CppType t1112_0_0_0;
extern Il2CppType t1112_0_0_0;
static ParameterInfo t6600_m34144_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1112_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34144_GM;
MethodInfo m34144_MI = 
{
	"Add", NULL, &t6600_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6600_m34144_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34144_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34145_GM;
MethodInfo m34145_MI = 
{
	"Clear", NULL, &t6600_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34145_GM};
extern Il2CppType t1112_0_0_0;
static ParameterInfo t6600_m34146_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1112_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34146_GM;
MethodInfo m34146_MI = 
{
	"Contains", NULL, &t6600_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6600_m34146_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34146_GM};
extern Il2CppType t3719_0_0_0;
extern Il2CppType t3719_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6600_m34147_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3719_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34147_GM;
MethodInfo m34147_MI = 
{
	"CopyTo", NULL, &t6600_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6600_m34147_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34147_GM};
extern Il2CppType t1112_0_0_0;
static ParameterInfo t6600_m34148_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1112_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34148_GM;
MethodInfo m34148_MI = 
{
	"Remove", NULL, &t6600_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6600_m34148_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34148_GM};
static MethodInfo* t6600_MIs[] =
{
	&m34142_MI,
	&m34143_MI,
	&m34144_MI,
	&m34145_MI,
	&m34146_MI,
	&m34147_MI,
	&m34148_MI,
	NULL
};
extern TypeInfo t6602_TI;
static TypeInfo* t6600_ITIs[] = 
{
	&t603_TI,
	&t6602_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6600_0_0_0;
extern Il2CppType t6600_1_0_0;
struct t6600;
extern Il2CppGenericClass t6600_GC;
TypeInfo t6600_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6600_MIs, t6600_PIs, NULL, NULL, NULL, NULL, NULL, &t6600_TI, t6600_ITIs, NULL, &EmptyCustomAttributesCache, &t6600_TI, &t6600_0_0_0, &t6600_1_0_0, NULL, &t6600_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Environment/SpecialFolder>
extern Il2CppType t5039_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34149_GM;
MethodInfo m34149_MI = 
{
	"GetEnumerator", NULL, &t6602_TI, &t5039_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34149_GM};
static MethodInfo* t6602_MIs[] =
{
	&m34149_MI,
	NULL
};
static TypeInfo* t6602_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6602_0_0_0;
extern Il2CppType t6602_1_0_0;
struct t6602;
extern Il2CppGenericClass t6602_GC;
TypeInfo t6602_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6602_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6602_TI, t6602_ITIs, NULL, &EmptyCustomAttributesCache, &t6602_TI, &t6602_0_0_0, &t6602_1_0_0, NULL, &t6602_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6601_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Environment/SpecialFolder>
extern MethodInfo m34150_MI;
extern MethodInfo m34151_MI;
static PropertyInfo t6601____Item_PropertyInfo = 
{
	&t6601_TI, "Item", &m34150_MI, &m34151_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6601_PIs[] =
{
	&t6601____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1112_0_0_0;
static ParameterInfo t6601_m34152_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1112_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34152_GM;
MethodInfo m34152_MI = 
{
	"IndexOf", NULL, &t6601_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6601_m34152_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34152_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1112_0_0_0;
static ParameterInfo t6601_m34153_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1112_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34153_GM;
MethodInfo m34153_MI = 
{
	"Insert", NULL, &t6601_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6601_m34153_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34153_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6601_m34154_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34154_GM;
MethodInfo m34154_MI = 
{
	"RemoveAt", NULL, &t6601_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6601_m34154_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34154_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6601_m34150_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1112_0_0_0;
extern void* RuntimeInvoker_t1112_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34150_GM;
MethodInfo m34150_MI = 
{
	"get_Item", NULL, &t6601_TI, &t1112_0_0_0, RuntimeInvoker_t1112_t44, t6601_m34150_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34150_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1112_0_0_0;
static ParameterInfo t6601_m34151_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1112_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34151_GM;
MethodInfo m34151_MI = 
{
	"set_Item", NULL, &t6601_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6601_m34151_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34151_GM};
static MethodInfo* t6601_MIs[] =
{
	&m34152_MI,
	&m34153_MI,
	&m34154_MI,
	&m34150_MI,
	&m34151_MI,
	NULL
};
static TypeInfo* t6601_ITIs[] = 
{
	&t603_TI,
	&t6600_TI,
	&t6602_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6601_0_0_0;
extern Il2CppType t6601_1_0_0;
struct t6601;
extern Il2CppGenericClass t6601_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6601_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6601_MIs, t6601_PIs, NULL, NULL, NULL, NULL, NULL, &t6601_TI, t6601_ITIs, NULL, &t1908__CustomAttributeCache, &t6601_TI, &t6601_0_0_0, &t6601_1_0_0, NULL, &t6601_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2087_TI;

#include "t1648.h"


// Metadata Definition System.IComparable`1<System.Guid>
extern Il2CppType t1648_0_0_0;
extern Il2CppType t1648_0_0_0;
static ParameterInfo t2087_m34155_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t1648_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t1648 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34155_GM;
MethodInfo m34155_MI = 
{
	"CompareTo", NULL, &t2087_TI, &t44_0_0_0, RuntimeInvoker_t44_t1648, t2087_m34155_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34155_GM};
static MethodInfo* t2087_MIs[] =
{
	&m34155_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2087_0_0_0;
extern Il2CppType t2087_1_0_0;
struct t2087;
extern Il2CppGenericClass t2087_GC;
TypeInfo t2087_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t2087_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2087_TI, NULL, NULL, &EmptyCustomAttributesCache, &t2087_TI, &t2087_0_0_0, &t2087_1_0_0, NULL, &t2087_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2088_TI;



// Metadata Definition System.IEquatable`1<System.Guid>
extern Il2CppType t1648_0_0_0;
static ParameterInfo t2088_m34156_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t1648_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t1648 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34156_GM;
MethodInfo m34156_MI = 
{
	"Equals", NULL, &t2088_TI, &t40_0_0_0, RuntimeInvoker_t40_t1648, t2088_m34156_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34156_GM};
static MethodInfo* t2088_MIs[] =
{
	&m34156_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2088_0_0_0;
extern Il2CppType t2088_1_0_0;
struct t2088;
extern Il2CppGenericClass t2088_GC;
TypeInfo t2088_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t2088_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2088_TI, NULL, NULL, &EmptyCustomAttributesCache, &t2088_TI, &t2088_0_0_0, &t2088_1_0_0, NULL, &t2088_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t2085.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2085_TI;
#include "t2085MD.h"

extern TypeInfo t1648_TI;
#include "t3494MD.h"
extern MethodInfo m19416_MI;
extern MethodInfo m34155_MI;


extern MethodInfo m10263_MI;
 void m10263 (t2085 * __this, MethodInfo* method){
	{
		m19416(__this, &m19416_MI);
		return;
	}
}
extern MethodInfo m19415_MI;
 int32_t m19415 (t2085 * __this, t1648  p0, t1648  p1, MethodInfo* method){
	int32_t G_B4_0 = 0;
	{
		t1648  L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t1648_TI), &L_0);
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		t1648  L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t1648_TI), &L_2);
		if (L_3)
		{
			goto IL_0013;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0014;
	}

IL_0013:
	{
		G_B4_0 = (-1);
	}

IL_0014:
	{
		return G_B4_0;
	}

IL_0015:
	{
		t1648  L_4 = p1;
		t29 * L_5 = Box(InitializedTypeInfo(&t1648_TI), &L_4);
		if (L_5)
		{
			goto IL_001f;
		}
	}
	{
		return 1;
	}

IL_001f:
	{
		int32_t L_6 = (int32_t)InterfaceFuncInvoker1< int32_t, t1648  >::Invoke(&m34155_MI, Box(InitializedTypeInfo(&t1648_TI), &(*(&p0))), p1);
		return L_6;
	}
}
// Metadata Definition System.Collections.Generic.GenericComparer`1<System.Guid>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10263_GM;
MethodInfo m10263_MI = 
{
	".ctor", (methodPointerType)&m10263, &t2085_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10263_GM};
extern Il2CppType t1648_0_0_0;
extern Il2CppType t1648_0_0_0;
static ParameterInfo t2085_m19415_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t1648_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t1648_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t1648_t1648 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19415_GM;
MethodInfo m19415_MI = 
{
	"Compare", (methodPointerType)&m19415, &t2085_TI, &t44_0_0_0, RuntimeInvoker_t44_t1648_t1648, t2085_m19415_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19415_GM};
static MethodInfo* t2085_MIs[] =
{
	&m10263_MI,
	&m19415_MI,
	NULL
};
extern MethodInfo m19418_MI;
static MethodInfo* t2085_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m19415_MI,
	&m19418_MI,
	&m19415_MI,
};
extern TypeInfo t6768_TI;
static Il2CppInterfaceOffsetPair t2085_IOs[] = 
{
	{ &t6768_TI, 4},
	{ &t726_TI, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2085_0_0_0;
extern Il2CppType t2085_1_0_0;
extern TypeInfo t3494_TI;
struct t2085;
extern Il2CppGenericClass t2085_GC;
TypeInfo t2085_TI = 
{
	&g_mscorlib_dll_Image, NULL, "GenericComparer`1", "System.Collections.Generic", t2085_MIs, NULL, NULL, NULL, &t3494_TI, NULL, NULL, &t2085_TI, NULL, t2085_VT, &EmptyCustomAttributesCache, &t2085_TI, &t2085_0_0_0, &t2085_1_0_0, t2085_IOs, &t2085_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2085), 0, -1, 0, 0, -1, 1057024, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 7, 0, 2};
#include "t3494.h"
#ifndef _MSC_VER
#else
#endif

#include "t3495.h"
extern TypeInfo t3495_TI;
#include "t3495MD.h"
extern MethodInfo m19420_MI;
extern MethodInfo m34157_MI;


 void m19416 (t3494 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m19417_MI;
 void m19417 (t29 * __this, MethodInfo* method){
	t3495 * L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer = (t3495 *)il2cpp_codegen_object_new(InitializedTypeInfo(&t3495_TI));
	m19420(L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer, &m19420_MI);
	((t3494_SFs*)InitializedTypeInfo(&t3494_TI)->static_fields)->f0 = L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer;
}
 int32_t m19418 (t3494 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	int32_t G_B4_0 = 0;
	{
		if (p0)
		{
			goto IL_000b;
		}
	}
	{
		if (p1)
		{
			goto IL_0009;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_000a;
	}

IL_0009:
	{
		G_B4_0 = (-1);
	}

IL_000a:
	{
		return G_B4_0;
	}

IL_000b:
	{
		if (p1)
		{
			goto IL_0010;
		}
	}
	{
		return 1;
	}

IL_0010:
	{
		if (!((t29 *)IsInst(p0, InitializedTypeInfo(&t1648_TI))))
		{
			goto IL_0033;
		}
	}
	{
		if (!((t29 *)IsInst(p1, InitializedTypeInfo(&t1648_TI))))
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker2< int32_t, t1648 , t1648  >::Invoke(&m34157_MI, __this, ((*(t1648 *)((t1648 *)UnBox (p0, InitializedTypeInfo(&t1648_TI))))), ((*(t1648 *)((t1648 *)UnBox (p1, InitializedTypeInfo(&t1648_TI))))));
		return L_0;
	}

IL_0033:
	{
		t305 * L_1 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m8852(L_1, &m8852_MI);
		il2cpp_codegen_raise_exception(L_1);
	}
}
extern MethodInfo m19419_MI;
 t3494 * m19419 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t3494_TI));
		return (((t3494_SFs*)InitializedTypeInfo(&t3494_TI)->static_fields)->f0);
	}
}
// Metadata Definition System.Collections.Generic.Comparer`1<System.Guid>
extern Il2CppType t3494_0_0_49;
FieldInfo t3494_f0_FieldInfo = 
{
	"_default", &t3494_0_0_49, &t3494_TI, offsetof(t3494_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3494_FIs[] =
{
	&t3494_f0_FieldInfo,
	NULL
};
static PropertyInfo t3494____Default_PropertyInfo = 
{
	&t3494_TI, "Default", &m19419_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3494_PIs[] =
{
	&t3494____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19416_GM;
MethodInfo m19416_MI = 
{
	".ctor", (methodPointerType)&m19416, &t3494_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19416_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19417_GM;
MethodInfo m19417_MI = 
{
	".cctor", (methodPointerType)&m19417, &t3494_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19417_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3494_m19418_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19418_GM;
MethodInfo m19418_MI = 
{
	"System.Collections.IComparer.Compare", (methodPointerType)&m19418, &t3494_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t3494_m19418_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19418_GM};
extern Il2CppType t1648_0_0_0;
extern Il2CppType t1648_0_0_0;
static ParameterInfo t3494_m34157_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t1648_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t1648_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t1648_t1648 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34157_GM;
MethodInfo m34157_MI = 
{
	"Compare", NULL, &t3494_TI, &t44_0_0_0, RuntimeInvoker_t44_t1648_t1648, t3494_m34157_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34157_GM};
extern Il2CppType t3494_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19419_GM;
MethodInfo m19419_MI = 
{
	"get_Default", (methodPointerType)&m19419, &t3494_TI, &t3494_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19419_GM};
static MethodInfo* t3494_MIs[] =
{
	&m19416_MI,
	&m19417_MI,
	&m19418_MI,
	&m34157_MI,
	&m19419_MI,
	NULL
};
static MethodInfo* t3494_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m34157_MI,
	&m19418_MI,
	NULL,
};
static TypeInfo* t3494_ITIs[] = 
{
	&t6768_TI,
	&t726_TI,
};
static Il2CppInterfaceOffsetPair t3494_IOs[] = 
{
	{ &t6768_TI, 4},
	{ &t726_TI, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3494_0_0_0;
extern Il2CppType t3494_1_0_0;
struct t3494;
extern Il2CppGenericClass t3494_GC;
TypeInfo t3494_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparer`1", "System.Collections.Generic", t3494_MIs, t3494_PIs, t3494_FIs, NULL, &t29_TI, NULL, NULL, &t3494_TI, t3494_ITIs, t3494_VT, &EmptyCustomAttributesCache, &t3494_TI, &t3494_0_0_0, &t3494_1_0_0, t3494_IOs, &t3494_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3494), 0, -1, sizeof(t3494_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 5, 1, 1, 0, 0, 7, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IComparer`1<System.Guid>
extern Il2CppType t1648_0_0_0;
extern Il2CppType t1648_0_0_0;
static ParameterInfo t6768_m34158_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t1648_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t1648_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t1648_t1648 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34158_GM;
MethodInfo m34158_MI = 
{
	"Compare", NULL, &t6768_TI, &t44_0_0_0, RuntimeInvoker_t44_t1648_t1648, t6768_m34158_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34158_GM};
static MethodInfo* t6768_MIs[] =
{
	&m34158_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6768_0_0_0;
extern Il2CppType t6768_1_0_0;
struct t6768;
extern Il2CppGenericClass t6768_GC;
TypeInfo t6768_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparer`1", "System.Collections.Generic", t6768_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6768_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6768_TI, &t6768_0_0_0, &t6768_1_0_0, NULL, &t6768_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



 void m19420 (t3495 * __this, MethodInfo* method){
	{
		m19416(__this, &m19416_MI);
		return;
	}
}
extern MethodInfo m19421_MI;
 int32_t m19421 (t3495 * __this, t1648  p0, t1648  p1, MethodInfo* method){
	int32_t G_B4_0 = 0;
	{
		t1648  L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t1648_TI), &L_0);
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		t1648  L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t1648_TI), &L_2);
		if (L_3)
		{
			goto IL_0013;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0014;
	}

IL_0013:
	{
		G_B4_0 = (-1);
	}

IL_0014:
	{
		return G_B4_0;
	}

IL_0015:
	{
		t1648  L_4 = p1;
		t29 * L_5 = Box(InitializedTypeInfo(&t1648_TI), &L_4);
		if (L_5)
		{
			goto IL_001f;
		}
	}
	{
		return 1;
	}

IL_001f:
	{
		t1648  L_6 = p0;
		t29 * L_7 = Box(InitializedTypeInfo(&t1648_TI), &L_6);
		if (!((t29*)IsInst(L_7, InitializedTypeInfo(&t2087_TI))))
		{
			goto IL_003e;
		}
	}
	{
		t1648  L_8 = p0;
		t29 * L_9 = Box(InitializedTypeInfo(&t1648_TI), &L_8);
		int32_t L_10 = (int32_t)InterfaceFuncInvoker1< int32_t, t1648  >::Invoke(&m34155_MI, ((t29*)Castclass(L_9, InitializedTypeInfo(&t2087_TI))), p1);
		return L_10;
	}

IL_003e:
	{
		t1648  L_11 = p0;
		t29 * L_12 = Box(InitializedTypeInfo(&t1648_TI), &L_11);
		if (!((t29 *)IsInst(L_12, InitializedTypeInfo(&t290_TI))))
		{
			goto IL_0062;
		}
	}
	{
		t1648  L_13 = p0;
		t29 * L_14 = Box(InitializedTypeInfo(&t1648_TI), &L_13);
		t1648  L_15 = p1;
		t29 * L_16 = Box(InitializedTypeInfo(&t1648_TI), &L_15);
		int32_t L_17 = (int32_t)InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(&m9672_MI, ((t29 *)Castclass(L_14, InitializedTypeInfo(&t290_TI))), L_16);
		return L_17;
	}

IL_0062:
	{
		t305 * L_18 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_18, (t7*) &_stringLiteral1162, &m1935_MI);
		il2cpp_codegen_raise_exception(L_18);
	}
}
// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer<System.Guid>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19420_GM;
MethodInfo m19420_MI = 
{
	".ctor", (methodPointerType)&m19420, &t3495_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19420_GM};
extern Il2CppType t1648_0_0_0;
extern Il2CppType t1648_0_0_0;
static ParameterInfo t3495_m19421_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t1648_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t1648_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t1648_t1648 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19421_GM;
MethodInfo m19421_MI = 
{
	"Compare", (methodPointerType)&m19421, &t3495_TI, &t44_0_0_0, RuntimeInvoker_t44_t1648_t1648, t3495_m19421_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19421_GM};
static MethodInfo* t3495_MIs[] =
{
	&m19420_MI,
	&m19421_MI,
	NULL
};
static MethodInfo* t3495_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m19421_MI,
	&m19418_MI,
	&m19421_MI,
};
static Il2CppInterfaceOffsetPair t3495_IOs[] = 
{
	{ &t6768_TI, 4},
	{ &t726_TI, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3495_0_0_0;
extern Il2CppType t3495_1_0_0;
struct t3495;
extern Il2CppGenericClass t3495_GC;
TypeInfo t3495_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t3495_MIs, NULL, NULL, NULL, &t3494_TI, NULL, &t1246_TI, &t3495_TI, NULL, t3495_VT, &EmptyCustomAttributesCache, &t3495_TI, &t3495_0_0_0, &t3495_1_0_0, t3495_IOs, &t3495_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3495), 0, -1, 0, 0, -1, 1048835, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 7, 0, 2};
#include "t2086.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2086_TI;
#include "t2086MD.h"

#include "t3496MD.h"
extern MethodInfo m19424_MI;
extern MethodInfo m34156_MI;


extern MethodInfo m10264_MI;
 void m10264 (t2086 * __this, MethodInfo* method){
	{
		m19424(__this, &m19424_MI);
		return;
	}
}
extern MethodInfo m19422_MI;
 int32_t m19422 (t2086 * __this, t1648  p0, MethodInfo* method){
	{
		t1648  L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t1648_TI), &L_0);
		if (L_1)
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}

IL_000a:
	{
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1322_MI, Box(InitializedTypeInfo(&t1648_TI), &(*(&p0))));
		return L_2;
	}
}
extern MethodInfo m19423_MI;
 bool m19423 (t2086 * __this, t1648  p0, t1648  p1, MethodInfo* method){
	{
		t1648  L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t1648_TI), &L_0);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		t1648  L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t1648_TI), &L_2);
		return ((((t29 *)L_3) == ((t29 *)NULL))? 1 : 0);
	}

IL_0012:
	{
		bool L_4 = (bool)InterfaceFuncInvoker1< bool, t1648  >::Invoke(&m34156_MI, Box(InitializedTypeInfo(&t1648_TI), &(*(&p0))), p1);
		return L_4;
	}
}
// Metadata Definition System.Collections.Generic.GenericEqualityComparer`1<System.Guid>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10264_GM;
MethodInfo m10264_MI = 
{
	".ctor", (methodPointerType)&m10264, &t2086_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10264_GM};
extern Il2CppType t1648_0_0_0;
static ParameterInfo t2086_m19422_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t1648_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t1648 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19422_GM;
MethodInfo m19422_MI = 
{
	"GetHashCode", (methodPointerType)&m19422, &t2086_TI, &t44_0_0_0, RuntimeInvoker_t44_t1648, t2086_m19422_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19422_GM};
extern Il2CppType t1648_0_0_0;
extern Il2CppType t1648_0_0_0;
static ParameterInfo t2086_m19423_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t1648_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t1648_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t1648_t1648 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19423_GM;
MethodInfo m19423_MI = 
{
	"Equals", (methodPointerType)&m19423, &t2086_TI, &t40_0_0_0, RuntimeInvoker_t40_t1648_t1648, t2086_m19423_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19423_GM};
static MethodInfo* t2086_MIs[] =
{
	&m10264_MI,
	&m19422_MI,
	&m19423_MI,
	NULL
};
extern MethodInfo m19427_MI;
extern MethodInfo m19426_MI;
static MethodInfo* t2086_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m19423_MI,
	&m19422_MI,
	&m19427_MI,
	&m19426_MI,
	&m19422_MI,
	&m19423_MI,
};
extern TypeInfo t6769_TI;
static Il2CppInterfaceOffsetPair t2086_IOs[] = 
{
	{ &t6769_TI, 4},
	{ &t734_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2086_0_0_0;
extern Il2CppType t2086_1_0_0;
extern TypeInfo t3496_TI;
struct t2086;
extern Il2CppGenericClass t2086_GC;
TypeInfo t2086_TI = 
{
	&g_mscorlib_dll_Image, NULL, "GenericEqualityComparer`1", "System.Collections.Generic", t2086_MIs, NULL, NULL, NULL, &t3496_TI, NULL, NULL, &t2086_TI, NULL, t2086_VT, &EmptyCustomAttributesCache, &t2086_TI, &t2086_0_0_0, &t2086_1_0_0, t2086_IOs, &t2086_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2086), 0, -1, 0, 0, -1, 1057024, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#include "t3496.h"
#ifndef _MSC_VER
#else
#endif

#include "t3497.h"
extern TypeInfo t3497_TI;
#include "t3497MD.h"
extern MethodInfo m19429_MI;
extern MethodInfo m34159_MI;
extern MethodInfo m34160_MI;


 void m19424 (t3496 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m19425_MI;
 void m19425 (t29 * __this, MethodInfo* method){
	t3497 * L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer = (t3497 *)il2cpp_codegen_object_new(InitializedTypeInfo(&t3497_TI));
	m19429(L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer, &m19429_MI);
	((t3496_SFs*)InitializedTypeInfo(&t3496_TI)->static_fields)->f0 = L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer;
}
 int32_t m19426 (t3496 * __this, t29 * p0, MethodInfo* method){
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker1< int32_t, t1648  >::Invoke(&m34159_MI, __this, ((*(t1648 *)((t1648 *)UnBox (p0, InitializedTypeInfo(&t1648_TI))))));
		return L_0;
	}
}
 bool m19427 (t3496 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	{
		bool L_0 = (bool)VirtFuncInvoker2< bool, t1648 , t1648  >::Invoke(&m34160_MI, __this, ((*(t1648 *)((t1648 *)UnBox (p0, InitializedTypeInfo(&t1648_TI))))), ((*(t1648 *)((t1648 *)UnBox (p1, InitializedTypeInfo(&t1648_TI))))));
		return L_0;
	}
}
extern MethodInfo m19428_MI;
 t3496 * m19428 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t3496_TI));
		return (((t3496_SFs*)InitializedTypeInfo(&t3496_TI)->static_fields)->f0);
	}
}
// Metadata Definition System.Collections.Generic.EqualityComparer`1<System.Guid>
extern Il2CppType t3496_0_0_49;
FieldInfo t3496_f0_FieldInfo = 
{
	"_default", &t3496_0_0_49, &t3496_TI, offsetof(t3496_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3496_FIs[] =
{
	&t3496_f0_FieldInfo,
	NULL
};
static PropertyInfo t3496____Default_PropertyInfo = 
{
	&t3496_TI, "Default", &m19428_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3496_PIs[] =
{
	&t3496____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19424_GM;
MethodInfo m19424_MI = 
{
	".ctor", (methodPointerType)&m19424, &t3496_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19424_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19425_GM;
MethodInfo m19425_MI = 
{
	".cctor", (methodPointerType)&m19425, &t3496_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19425_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3496_m19426_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19426_GM;
MethodInfo m19426_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m19426, &t3496_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3496_m19426_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19426_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3496_m19427_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19427_GM;
MethodInfo m19427_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m19427, &t3496_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t3496_m19427_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19427_GM};
extern Il2CppType t1648_0_0_0;
static ParameterInfo t3496_m34159_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t1648_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t1648 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34159_GM;
MethodInfo m34159_MI = 
{
	"GetHashCode", NULL, &t3496_TI, &t44_0_0_0, RuntimeInvoker_t44_t1648, t3496_m34159_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34159_GM};
extern Il2CppType t1648_0_0_0;
extern Il2CppType t1648_0_0_0;
static ParameterInfo t3496_m34160_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t1648_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t1648_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t1648_t1648 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34160_GM;
MethodInfo m34160_MI = 
{
	"Equals", NULL, &t3496_TI, &t40_0_0_0, RuntimeInvoker_t40_t1648_t1648, t3496_m34160_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34160_GM};
extern Il2CppType t3496_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19428_GM;
MethodInfo m19428_MI = 
{
	"get_Default", (methodPointerType)&m19428, &t3496_TI, &t3496_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19428_GM};
static MethodInfo* t3496_MIs[] =
{
	&m19424_MI,
	&m19425_MI,
	&m19426_MI,
	&m19427_MI,
	&m34159_MI,
	&m34160_MI,
	&m19428_MI,
	NULL
};
static MethodInfo* t3496_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m34160_MI,
	&m34159_MI,
	&m19427_MI,
	&m19426_MI,
	NULL,
	NULL,
};
static TypeInfo* t3496_ITIs[] = 
{
	&t6769_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t3496_IOs[] = 
{
	{ &t6769_TI, 4},
	{ &t734_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3496_0_0_0;
extern Il2CppType t3496_1_0_0;
struct t3496;
extern Il2CppGenericClass t3496_GC;
TypeInfo t3496_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t3496_MIs, t3496_PIs, t3496_FIs, NULL, &t29_TI, NULL, NULL, &t3496_TI, t3496_ITIs, t3496_VT, &EmptyCustomAttributesCache, &t3496_TI, &t3496_0_0_0, &t3496_1_0_0, t3496_IOs, &t3496_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3496), 0, -1, sizeof(t3496_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<System.Guid>
extern Il2CppType t1648_0_0_0;
extern Il2CppType t1648_0_0_0;
static ParameterInfo t6769_m34161_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t1648_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t1648_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t1648_t1648 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34161_GM;
MethodInfo m34161_MI = 
{
	"Equals", NULL, &t6769_TI, &t40_0_0_0, RuntimeInvoker_t40_t1648_t1648, t6769_m34161_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34161_GM};
extern Il2CppType t1648_0_0_0;
static ParameterInfo t6769_m34162_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t1648_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t1648 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34162_GM;
MethodInfo m34162_MI = 
{
	"GetHashCode", NULL, &t6769_TI, &t44_0_0_0, RuntimeInvoker_t44_t1648, t6769_m34162_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34162_GM};
static MethodInfo* t6769_MIs[] =
{
	&m34161_MI,
	&m34162_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6769_0_0_0;
extern Il2CppType t6769_1_0_0;
struct t6769;
extern Il2CppGenericClass t6769_GC;
TypeInfo t6769_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t6769_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6769_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6769_TI, &t6769_0_0_0, &t6769_1_0_0, NULL, &t6769_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



 void m19429 (t3497 * __this, MethodInfo* method){
	{
		m19424(__this, &m19424_MI);
		return;
	}
}
extern MethodInfo m19430_MI;
 int32_t m19430 (t3497 * __this, t1648  p0, MethodInfo* method){
	{
		t1648  L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t1648_TI), &L_0);
		if (L_1)
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}

IL_000a:
	{
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1322_MI, Box(InitializedTypeInfo(&t1648_TI), &(*(&p0))));
		return L_2;
	}
}
extern MethodInfo m19431_MI;
 bool m19431 (t3497 * __this, t1648  p0, t1648  p1, MethodInfo* method){
	{
		t1648  L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t1648_TI), &L_0);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		t1648  L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t1648_TI), &L_2);
		return ((((t29 *)L_3) == ((t29 *)NULL))? 1 : 0);
	}

IL_0012:
	{
		t1648  L_4 = p1;
		t29 * L_5 = Box(InitializedTypeInfo(&t1648_TI), &L_4);
		bool L_6 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1648_TI), &(*(&p0))), L_5);
		return L_6;
	}
}
// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19429_GM;
MethodInfo m19429_MI = 
{
	".ctor", (methodPointerType)&m19429, &t3497_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19429_GM};
extern Il2CppType t1648_0_0_0;
static ParameterInfo t3497_m19430_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t1648_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t1648 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19430_GM;
MethodInfo m19430_MI = 
{
	"GetHashCode", (methodPointerType)&m19430, &t3497_TI, &t44_0_0_0, RuntimeInvoker_t44_t1648, t3497_m19430_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19430_GM};
extern Il2CppType t1648_0_0_0;
extern Il2CppType t1648_0_0_0;
static ParameterInfo t3497_m19431_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t1648_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t1648_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t1648_t1648 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19431_GM;
MethodInfo m19431_MI = 
{
	"Equals", (methodPointerType)&m19431, &t3497_TI, &t40_0_0_0, RuntimeInvoker_t40_t1648_t1648, t3497_m19431_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19431_GM};
static MethodInfo* t3497_MIs[] =
{
	&m19429_MI,
	&m19430_MI,
	&m19431_MI,
	NULL
};
static MethodInfo* t3497_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m19431_MI,
	&m19430_MI,
	&m19427_MI,
	&m19426_MI,
	&m19430_MI,
	&m19431_MI,
};
static Il2CppInterfaceOffsetPair t3497_IOs[] = 
{
	{ &t6769_TI, 4},
	{ &t734_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3497_0_0_0;
extern Il2CppType t3497_1_0_0;
struct t3497;
extern Il2CppGenericClass t3497_GC;
TypeInfo t3497_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t3497_MIs, NULL, NULL, NULL, &t3496_TI, NULL, &t1256_TI, &t3497_TI, NULL, t3497_VT, &EmptyCustomAttributesCache, &t3497_TI, &t3497_0_0_0, &t3497_1_0_0, t3497_IOs, &t3497_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3497), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5041_TI;

#include "t1620.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.LoaderOptimization>
extern MethodInfo m34163_MI;
static PropertyInfo t5041____Current_PropertyInfo = 
{
	&t5041_TI, "Current", &m34163_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5041_PIs[] =
{
	&t5041____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1620_0_0_0;
extern void* RuntimeInvoker_t1620 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34163_GM;
MethodInfo m34163_MI = 
{
	"get_Current", NULL, &t5041_TI, &t1620_0_0_0, RuntimeInvoker_t1620, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34163_GM};
static MethodInfo* t5041_MIs[] =
{
	&m34163_MI,
	NULL
};
static TypeInfo* t5041_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5041_0_0_0;
extern Il2CppType t5041_1_0_0;
struct t5041;
extern Il2CppGenericClass t5041_GC;
TypeInfo t5041_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t5041_MIs, t5041_PIs, NULL, NULL, NULL, NULL, NULL, &t5041_TI, t5041_ITIs, NULL, &EmptyCustomAttributesCache, &t5041_TI, &t5041_0_0_0, &t5041_1_0_0, NULL, &t5041_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3498.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3498_TI;
#include "t3498MD.h"

extern TypeInfo t1620_TI;
extern MethodInfo m19436_MI;
extern MethodInfo m26165_MI;
struct t20;
 int32_t m26165 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m19432_MI;
 void m19432 (t3498 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19433_MI;
 t29 * m19433 (t3498 * __this, MethodInfo* method){
	{
		int32_t L_0 = m19436(__this, &m19436_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1620_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m19434_MI;
 void m19434 (t3498 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19435_MI;
 bool m19435 (t3498 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m19436 (t3498 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m26165(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m26165_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.LoaderOptimization>
extern Il2CppType t20_0_0_1;
FieldInfo t3498_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3498_TI, offsetof(t3498, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3498_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3498_TI, offsetof(t3498, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3498_FIs[] =
{
	&t3498_f0_FieldInfo,
	&t3498_f1_FieldInfo,
	NULL
};
static PropertyInfo t3498____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3498_TI, "System.Collections.IEnumerator.Current", &m19433_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3498____Current_PropertyInfo = 
{
	&t3498_TI, "Current", &m19436_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3498_PIs[] =
{
	&t3498____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3498____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3498_m19432_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19432_GM;
MethodInfo m19432_MI = 
{
	".ctor", (methodPointerType)&m19432, &t3498_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3498_m19432_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19432_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19433_GM;
MethodInfo m19433_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m19433, &t3498_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19433_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19434_GM;
MethodInfo m19434_MI = 
{
	"Dispose", (methodPointerType)&m19434, &t3498_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19434_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19435_GM;
MethodInfo m19435_MI = 
{
	"MoveNext", (methodPointerType)&m19435, &t3498_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19435_GM};
extern Il2CppType t1620_0_0_0;
extern void* RuntimeInvoker_t1620 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19436_GM;
MethodInfo m19436_MI = 
{
	"get_Current", (methodPointerType)&m19436, &t3498_TI, &t1620_0_0_0, RuntimeInvoker_t1620, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19436_GM};
static MethodInfo* t3498_MIs[] =
{
	&m19432_MI,
	&m19433_MI,
	&m19434_MI,
	&m19435_MI,
	&m19436_MI,
	NULL
};
static MethodInfo* t3498_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19433_MI,
	&m19435_MI,
	&m19434_MI,
	&m19436_MI,
};
static TypeInfo* t3498_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t5041_TI,
};
static Il2CppInterfaceOffsetPair t3498_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t5041_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3498_0_0_0;
extern Il2CppType t3498_1_0_0;
extern Il2CppGenericClass t3498_GC;
TypeInfo t3498_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3498_MIs, t3498_PIs, t3498_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3498_TI, t3498_ITIs, t3498_VT, &EmptyCustomAttributesCache, &t3498_TI, &t3498_0_0_0, &t3498_1_0_0, t3498_IOs, &t3498_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3498)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6603_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.LoaderOptimization>
extern MethodInfo m34164_MI;
static PropertyInfo t6603____Count_PropertyInfo = 
{
	&t6603_TI, "Count", &m34164_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m34165_MI;
static PropertyInfo t6603____IsReadOnly_PropertyInfo = 
{
	&t6603_TI, "IsReadOnly", &m34165_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6603_PIs[] =
{
	&t6603____Count_PropertyInfo,
	&t6603____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34164_GM;
MethodInfo m34164_MI = 
{
	"get_Count", NULL, &t6603_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34164_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34165_GM;
MethodInfo m34165_MI = 
{
	"get_IsReadOnly", NULL, &t6603_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34165_GM};
extern Il2CppType t1620_0_0_0;
extern Il2CppType t1620_0_0_0;
static ParameterInfo t6603_m34166_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1620_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34166_GM;
MethodInfo m34166_MI = 
{
	"Add", NULL, &t6603_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6603_m34166_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34166_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34167_GM;
MethodInfo m34167_MI = 
{
	"Clear", NULL, &t6603_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34167_GM};
extern Il2CppType t1620_0_0_0;
static ParameterInfo t6603_m34168_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1620_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34168_GM;
MethodInfo m34168_MI = 
{
	"Contains", NULL, &t6603_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6603_m34168_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34168_GM};
extern Il2CppType t3720_0_0_0;
extern Il2CppType t3720_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6603_m34169_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3720_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34169_GM;
MethodInfo m34169_MI = 
{
	"CopyTo", NULL, &t6603_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6603_m34169_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34169_GM};
extern Il2CppType t1620_0_0_0;
static ParameterInfo t6603_m34170_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1620_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34170_GM;
MethodInfo m34170_MI = 
{
	"Remove", NULL, &t6603_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6603_m34170_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34170_GM};
static MethodInfo* t6603_MIs[] =
{
	&m34164_MI,
	&m34165_MI,
	&m34166_MI,
	&m34167_MI,
	&m34168_MI,
	&m34169_MI,
	&m34170_MI,
	NULL
};
extern TypeInfo t6605_TI;
static TypeInfo* t6603_ITIs[] = 
{
	&t603_TI,
	&t6605_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6603_0_0_0;
extern Il2CppType t6603_1_0_0;
struct t6603;
extern Il2CppGenericClass t6603_GC;
TypeInfo t6603_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6603_MIs, t6603_PIs, NULL, NULL, NULL, NULL, NULL, &t6603_TI, t6603_ITIs, NULL, &EmptyCustomAttributesCache, &t6603_TI, &t6603_0_0_0, &t6603_1_0_0, NULL, &t6603_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.LoaderOptimization>
extern Il2CppType t5041_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34171_GM;
MethodInfo m34171_MI = 
{
	"GetEnumerator", NULL, &t6605_TI, &t5041_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34171_GM};
static MethodInfo* t6605_MIs[] =
{
	&m34171_MI,
	NULL
};
static TypeInfo* t6605_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6605_0_0_0;
extern Il2CppType t6605_1_0_0;
struct t6605;
extern Il2CppGenericClass t6605_GC;
TypeInfo t6605_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6605_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6605_TI, t6605_ITIs, NULL, &EmptyCustomAttributesCache, &t6605_TI, &t6605_0_0_0, &t6605_1_0_0, NULL, &t6605_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6604_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.LoaderOptimization>
extern MethodInfo m34172_MI;
extern MethodInfo m34173_MI;
static PropertyInfo t6604____Item_PropertyInfo = 
{
	&t6604_TI, "Item", &m34172_MI, &m34173_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6604_PIs[] =
{
	&t6604____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1620_0_0_0;
static ParameterInfo t6604_m34174_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1620_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34174_GM;
MethodInfo m34174_MI = 
{
	"IndexOf", NULL, &t6604_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6604_m34174_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34174_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1620_0_0_0;
static ParameterInfo t6604_m34175_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1620_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34175_GM;
MethodInfo m34175_MI = 
{
	"Insert", NULL, &t6604_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6604_m34175_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34175_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6604_m34176_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34176_GM;
MethodInfo m34176_MI = 
{
	"RemoveAt", NULL, &t6604_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6604_m34176_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34176_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6604_m34172_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1620_0_0_0;
extern void* RuntimeInvoker_t1620_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34172_GM;
MethodInfo m34172_MI = 
{
	"get_Item", NULL, &t6604_TI, &t1620_0_0_0, RuntimeInvoker_t1620_t44, t6604_m34172_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34172_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1620_0_0_0;
static ParameterInfo t6604_m34173_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1620_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34173_GM;
MethodInfo m34173_MI = 
{
	"set_Item", NULL, &t6604_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6604_m34173_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34173_GM};
static MethodInfo* t6604_MIs[] =
{
	&m34174_MI,
	&m34175_MI,
	&m34176_MI,
	&m34172_MI,
	&m34173_MI,
	NULL
};
static TypeInfo* t6604_ITIs[] = 
{
	&t603_TI,
	&t6603_TI,
	&t6605_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6604_0_0_0;
extern Il2CppType t6604_1_0_0;
struct t6604;
extern Il2CppGenericClass t6604_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6604_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6604_MIs, t6604_PIs, NULL, NULL, NULL, NULL, NULL, &t6604_TI, t6604_ITIs, NULL, &t1908__CustomAttributeCache, &t6604_TI, &t6604_0_0_0, &t6604_1_0_0, NULL, &t6604_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5043_TI;

#include "t1662.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.NonSerializedAttribute>
extern MethodInfo m34177_MI;
static PropertyInfo t5043____Current_PropertyInfo = 
{
	&t5043_TI, "Current", &m34177_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5043_PIs[] =
{
	&t5043____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1662_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34177_GM;
MethodInfo m34177_MI = 
{
	"get_Current", NULL, &t5043_TI, &t1662_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34177_GM};
static MethodInfo* t5043_MIs[] =
{
	&m34177_MI,
	NULL
};
static TypeInfo* t5043_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5043_0_0_0;
extern Il2CppType t5043_1_0_0;
struct t5043;
extern Il2CppGenericClass t5043_GC;
TypeInfo t5043_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t5043_MIs, t5043_PIs, NULL, NULL, NULL, NULL, NULL, &t5043_TI, t5043_ITIs, NULL, &EmptyCustomAttributesCache, &t5043_TI, &t5043_0_0_0, &t5043_1_0_0, NULL, &t5043_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3499.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3499_TI;
#include "t3499MD.h"

extern TypeInfo t1662_TI;
extern MethodInfo m19441_MI;
extern MethodInfo m26176_MI;
struct t20;
#define m26176(__this, p0, method) (t1662 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.NonSerializedAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3499_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3499_TI, offsetof(t3499, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3499_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3499_TI, offsetof(t3499, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3499_FIs[] =
{
	&t3499_f0_FieldInfo,
	&t3499_f1_FieldInfo,
	NULL
};
extern MethodInfo m19438_MI;
static PropertyInfo t3499____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3499_TI, "System.Collections.IEnumerator.Current", &m19438_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3499____Current_PropertyInfo = 
{
	&t3499_TI, "Current", &m19441_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3499_PIs[] =
{
	&t3499____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3499____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3499_m19437_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19437_GM;
MethodInfo m19437_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3499_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3499_m19437_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19437_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19438_GM;
MethodInfo m19438_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3499_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19438_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19439_GM;
MethodInfo m19439_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3499_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19439_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19440_GM;
MethodInfo m19440_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3499_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19440_GM};
extern Il2CppType t1662_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19441_GM;
MethodInfo m19441_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3499_TI, &t1662_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19441_GM};
static MethodInfo* t3499_MIs[] =
{
	&m19437_MI,
	&m19438_MI,
	&m19439_MI,
	&m19440_MI,
	&m19441_MI,
	NULL
};
extern MethodInfo m19440_MI;
extern MethodInfo m19439_MI;
static MethodInfo* t3499_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19438_MI,
	&m19440_MI,
	&m19439_MI,
	&m19441_MI,
};
static TypeInfo* t3499_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t5043_TI,
};
static Il2CppInterfaceOffsetPair t3499_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t5043_TI, 7},
};
extern TypeInfo t1662_TI;
static Il2CppRGCTXData t3499_RGCTXData[3] = 
{
	&m19441_MI/* Method Usage */,
	&t1662_TI/* Class Usage */,
	&m26176_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3499_0_0_0;
extern Il2CppType t3499_1_0_0;
extern Il2CppGenericClass t3499_GC;
TypeInfo t3499_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3499_MIs, t3499_PIs, t3499_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3499_TI, t3499_ITIs, t3499_VT, &EmptyCustomAttributesCache, &t3499_TI, &t3499_0_0_0, &t3499_1_0_0, t3499_IOs, &t3499_GC, NULL, NULL, NULL, t3499_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3499)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6606_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.NonSerializedAttribute>
extern MethodInfo m34178_MI;
static PropertyInfo t6606____Count_PropertyInfo = 
{
	&t6606_TI, "Count", &m34178_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m34179_MI;
static PropertyInfo t6606____IsReadOnly_PropertyInfo = 
{
	&t6606_TI, "IsReadOnly", &m34179_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6606_PIs[] =
{
	&t6606____Count_PropertyInfo,
	&t6606____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34178_GM;
MethodInfo m34178_MI = 
{
	"get_Count", NULL, &t6606_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34178_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34179_GM;
MethodInfo m34179_MI = 
{
	"get_IsReadOnly", NULL, &t6606_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34179_GM};
extern Il2CppType t1662_0_0_0;
extern Il2CppType t1662_0_0_0;
static ParameterInfo t6606_m34180_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1662_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34180_GM;
MethodInfo m34180_MI = 
{
	"Add", NULL, &t6606_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6606_m34180_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34180_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34181_GM;
MethodInfo m34181_MI = 
{
	"Clear", NULL, &t6606_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34181_GM};
extern Il2CppType t1662_0_0_0;
static ParameterInfo t6606_m34182_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1662_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34182_GM;
MethodInfo m34182_MI = 
{
	"Contains", NULL, &t6606_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6606_m34182_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34182_GM};
extern Il2CppType t3721_0_0_0;
extern Il2CppType t3721_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6606_m34183_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3721_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34183_GM;
MethodInfo m34183_MI = 
{
	"CopyTo", NULL, &t6606_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6606_m34183_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34183_GM};
extern Il2CppType t1662_0_0_0;
static ParameterInfo t6606_m34184_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1662_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34184_GM;
MethodInfo m34184_MI = 
{
	"Remove", NULL, &t6606_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6606_m34184_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34184_GM};
static MethodInfo* t6606_MIs[] =
{
	&m34178_MI,
	&m34179_MI,
	&m34180_MI,
	&m34181_MI,
	&m34182_MI,
	&m34183_MI,
	&m34184_MI,
	NULL
};
extern TypeInfo t6608_TI;
static TypeInfo* t6606_ITIs[] = 
{
	&t603_TI,
	&t6608_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6606_0_0_0;
extern Il2CppType t6606_1_0_0;
struct t6606;
extern Il2CppGenericClass t6606_GC;
TypeInfo t6606_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6606_MIs, t6606_PIs, NULL, NULL, NULL, NULL, NULL, &t6606_TI, t6606_ITIs, NULL, &EmptyCustomAttributesCache, &t6606_TI, &t6606_0_0_0, &t6606_1_0_0, NULL, &t6606_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.NonSerializedAttribute>
extern Il2CppType t5043_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34185_GM;
MethodInfo m34185_MI = 
{
	"GetEnumerator", NULL, &t6608_TI, &t5043_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34185_GM};
static MethodInfo* t6608_MIs[] =
{
	&m34185_MI,
	NULL
};
static TypeInfo* t6608_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6608_0_0_0;
extern Il2CppType t6608_1_0_0;
struct t6608;
extern Il2CppGenericClass t6608_GC;
TypeInfo t6608_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6608_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6608_TI, t6608_ITIs, NULL, &EmptyCustomAttributesCache, &t6608_TI, &t6608_0_0_0, &t6608_1_0_0, NULL, &t6608_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6607_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.NonSerializedAttribute>
extern MethodInfo m34186_MI;
extern MethodInfo m34187_MI;
static PropertyInfo t6607____Item_PropertyInfo = 
{
	&t6607_TI, "Item", &m34186_MI, &m34187_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6607_PIs[] =
{
	&t6607____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1662_0_0_0;
static ParameterInfo t6607_m34188_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1662_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34188_GM;
MethodInfo m34188_MI = 
{
	"IndexOf", NULL, &t6607_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6607_m34188_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34188_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1662_0_0_0;
static ParameterInfo t6607_m34189_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1662_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34189_GM;
MethodInfo m34189_MI = 
{
	"Insert", NULL, &t6607_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6607_m34189_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34189_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6607_m34190_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34190_GM;
MethodInfo m34190_MI = 
{
	"RemoveAt", NULL, &t6607_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6607_m34190_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34190_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6607_m34186_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1662_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34186_GM;
MethodInfo m34186_MI = 
{
	"get_Item", NULL, &t6607_TI, &t1662_0_0_0, RuntimeInvoker_t29_t44, t6607_m34186_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34186_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1662_0_0_0;
static ParameterInfo t6607_m34187_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1662_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34187_GM;
MethodInfo m34187_MI = 
{
	"set_Item", NULL, &t6607_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6607_m34187_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34187_GM};
static MethodInfo* t6607_MIs[] =
{
	&m34188_MI,
	&m34189_MI,
	&m34190_MI,
	&m34186_MI,
	&m34187_MI,
	NULL
};
static TypeInfo* t6607_ITIs[] = 
{
	&t603_TI,
	&t6606_TI,
	&t6608_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6607_0_0_0;
extern Il2CppType t6607_1_0_0;
struct t6607;
extern Il2CppGenericClass t6607_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6607_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6607_MIs, t6607_PIs, NULL, NULL, NULL, NULL, NULL, &t6607_TI, t6607_ITIs, NULL, &t1908__CustomAttributeCache, &t6607_TI, &t6607_0_0_0, &t6607_1_0_0, NULL, &t6607_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5045_TI;

#include "t1644.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.PlatformID>
extern MethodInfo m34191_MI;
static PropertyInfo t5045____Current_PropertyInfo = 
{
	&t5045_TI, "Current", &m34191_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5045_PIs[] =
{
	&t5045____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1644_0_0_0;
extern void* RuntimeInvoker_t1644 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34191_GM;
MethodInfo m34191_MI = 
{
	"get_Current", NULL, &t5045_TI, &t1644_0_0_0, RuntimeInvoker_t1644, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34191_GM};
static MethodInfo* t5045_MIs[] =
{
	&m34191_MI,
	NULL
};
static TypeInfo* t5045_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5045_0_0_0;
extern Il2CppType t5045_1_0_0;
struct t5045;
extern Il2CppGenericClass t5045_GC;
TypeInfo t5045_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t5045_MIs, t5045_PIs, NULL, NULL, NULL, NULL, NULL, &t5045_TI, t5045_ITIs, NULL, &EmptyCustomAttributesCache, &t5045_TI, &t5045_0_0_0, &t5045_1_0_0, NULL, &t5045_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3500.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3500_TI;
#include "t3500MD.h"

extern TypeInfo t1644_TI;
extern MethodInfo m19446_MI;
extern MethodInfo m26187_MI;
struct t20;
 int32_t m26187 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m19442_MI;
 void m19442 (t3500 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19443_MI;
 t29 * m19443 (t3500 * __this, MethodInfo* method){
	{
		int32_t L_0 = m19446(__this, &m19446_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1644_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m19444_MI;
 void m19444 (t3500 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19445_MI;
 bool m19445 (t3500 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m19446 (t3500 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m26187(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m26187_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.PlatformID>
extern Il2CppType t20_0_0_1;
FieldInfo t3500_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3500_TI, offsetof(t3500, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3500_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3500_TI, offsetof(t3500, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3500_FIs[] =
{
	&t3500_f0_FieldInfo,
	&t3500_f1_FieldInfo,
	NULL
};
static PropertyInfo t3500____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3500_TI, "System.Collections.IEnumerator.Current", &m19443_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3500____Current_PropertyInfo = 
{
	&t3500_TI, "Current", &m19446_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3500_PIs[] =
{
	&t3500____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3500____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3500_m19442_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19442_GM;
MethodInfo m19442_MI = 
{
	".ctor", (methodPointerType)&m19442, &t3500_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3500_m19442_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19442_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19443_GM;
MethodInfo m19443_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m19443, &t3500_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19443_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19444_GM;
MethodInfo m19444_MI = 
{
	"Dispose", (methodPointerType)&m19444, &t3500_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19444_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19445_GM;
MethodInfo m19445_MI = 
{
	"MoveNext", (methodPointerType)&m19445, &t3500_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19445_GM};
extern Il2CppType t1644_0_0_0;
extern void* RuntimeInvoker_t1644 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19446_GM;
MethodInfo m19446_MI = 
{
	"get_Current", (methodPointerType)&m19446, &t3500_TI, &t1644_0_0_0, RuntimeInvoker_t1644, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19446_GM};
static MethodInfo* t3500_MIs[] =
{
	&m19442_MI,
	&m19443_MI,
	&m19444_MI,
	&m19445_MI,
	&m19446_MI,
	NULL
};
static MethodInfo* t3500_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19443_MI,
	&m19445_MI,
	&m19444_MI,
	&m19446_MI,
};
static TypeInfo* t3500_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t5045_TI,
};
static Il2CppInterfaceOffsetPair t3500_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t5045_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3500_0_0_0;
extern Il2CppType t3500_1_0_0;
extern Il2CppGenericClass t3500_GC;
TypeInfo t3500_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3500_MIs, t3500_PIs, t3500_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3500_TI, t3500_ITIs, t3500_VT, &EmptyCustomAttributesCache, &t3500_TI, &t3500_0_0_0, &t3500_1_0_0, t3500_IOs, &t3500_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3500)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6609_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.PlatformID>
extern MethodInfo m34192_MI;
static PropertyInfo t6609____Count_PropertyInfo = 
{
	&t6609_TI, "Count", &m34192_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m34193_MI;
static PropertyInfo t6609____IsReadOnly_PropertyInfo = 
{
	&t6609_TI, "IsReadOnly", &m34193_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6609_PIs[] =
{
	&t6609____Count_PropertyInfo,
	&t6609____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34192_GM;
MethodInfo m34192_MI = 
{
	"get_Count", NULL, &t6609_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34192_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34193_GM;
MethodInfo m34193_MI = 
{
	"get_IsReadOnly", NULL, &t6609_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34193_GM};
extern Il2CppType t1644_0_0_0;
extern Il2CppType t1644_0_0_0;
static ParameterInfo t6609_m34194_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1644_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34194_GM;
MethodInfo m34194_MI = 
{
	"Add", NULL, &t6609_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6609_m34194_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34194_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34195_GM;
MethodInfo m34195_MI = 
{
	"Clear", NULL, &t6609_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34195_GM};
extern Il2CppType t1644_0_0_0;
static ParameterInfo t6609_m34196_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1644_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34196_GM;
MethodInfo m34196_MI = 
{
	"Contains", NULL, &t6609_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6609_m34196_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34196_GM};
extern Il2CppType t3722_0_0_0;
extern Il2CppType t3722_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6609_m34197_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3722_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34197_GM;
MethodInfo m34197_MI = 
{
	"CopyTo", NULL, &t6609_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6609_m34197_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34197_GM};
extern Il2CppType t1644_0_0_0;
static ParameterInfo t6609_m34198_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1644_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34198_GM;
MethodInfo m34198_MI = 
{
	"Remove", NULL, &t6609_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6609_m34198_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34198_GM};
static MethodInfo* t6609_MIs[] =
{
	&m34192_MI,
	&m34193_MI,
	&m34194_MI,
	&m34195_MI,
	&m34196_MI,
	&m34197_MI,
	&m34198_MI,
	NULL
};
extern TypeInfo t6611_TI;
static TypeInfo* t6609_ITIs[] = 
{
	&t603_TI,
	&t6611_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6609_0_0_0;
extern Il2CppType t6609_1_0_0;
struct t6609;
extern Il2CppGenericClass t6609_GC;
TypeInfo t6609_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6609_MIs, t6609_PIs, NULL, NULL, NULL, NULL, NULL, &t6609_TI, t6609_ITIs, NULL, &EmptyCustomAttributesCache, &t6609_TI, &t6609_0_0_0, &t6609_1_0_0, NULL, &t6609_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.PlatformID>
extern Il2CppType t5045_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34199_GM;
MethodInfo m34199_MI = 
{
	"GetEnumerator", NULL, &t6611_TI, &t5045_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34199_GM};
static MethodInfo* t6611_MIs[] =
{
	&m34199_MI,
	NULL
};
static TypeInfo* t6611_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6611_0_0_0;
extern Il2CppType t6611_1_0_0;
struct t6611;
extern Il2CppGenericClass t6611_GC;
TypeInfo t6611_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6611_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6611_TI, t6611_ITIs, NULL, &EmptyCustomAttributesCache, &t6611_TI, &t6611_0_0_0, &t6611_1_0_0, NULL, &t6611_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6610_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.PlatformID>
extern MethodInfo m34200_MI;
extern MethodInfo m34201_MI;
static PropertyInfo t6610____Item_PropertyInfo = 
{
	&t6610_TI, "Item", &m34200_MI, &m34201_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6610_PIs[] =
{
	&t6610____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1644_0_0_0;
static ParameterInfo t6610_m34202_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1644_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34202_GM;
MethodInfo m34202_MI = 
{
	"IndexOf", NULL, &t6610_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6610_m34202_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34202_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1644_0_0_0;
static ParameterInfo t6610_m34203_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1644_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34203_GM;
MethodInfo m34203_MI = 
{
	"Insert", NULL, &t6610_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6610_m34203_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34203_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6610_m34204_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34204_GM;
MethodInfo m34204_MI = 
{
	"RemoveAt", NULL, &t6610_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6610_m34204_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34204_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6610_m34200_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1644_0_0_0;
extern void* RuntimeInvoker_t1644_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34200_GM;
MethodInfo m34200_MI = 
{
	"get_Item", NULL, &t6610_TI, &t1644_0_0_0, RuntimeInvoker_t1644_t44, t6610_m34200_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34200_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1644_0_0_0;
static ParameterInfo t6610_m34201_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1644_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34201_GM;
MethodInfo m34201_MI = 
{
	"set_Item", NULL, &t6610_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6610_m34201_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34201_GM};
static MethodInfo* t6610_MIs[] =
{
	&m34202_MI,
	&m34203_MI,
	&m34204_MI,
	&m34200_MI,
	&m34201_MI,
	NULL
};
static TypeInfo* t6610_ITIs[] = 
{
	&t603_TI,
	&t6609_TI,
	&t6611_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6610_0_0_0;
extern Il2CppType t6610_1_0_0;
struct t6610;
extern Il2CppGenericClass t6610_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6610_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6610_MIs, t6610_PIs, NULL, NULL, NULL, NULL, NULL, &t6610_TI, t6610_ITIs, NULL, &t1908__CustomAttributeCache, &t6610_TI, &t6610_0_0_0, &t6610_1_0_0, NULL, &t6610_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
