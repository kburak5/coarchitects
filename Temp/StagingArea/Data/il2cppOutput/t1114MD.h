﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1114;
struct t7;

 void m8280 (t1114 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1114 * m5201 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1114 * m8281 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8282 (t1114 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8283 (t1114 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8284 (t1114 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
