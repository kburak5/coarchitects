﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1670;
struct t7;

 void m9512 (t1670 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9513 (t1670 * __this, t7* p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9514 (t1670 * __this, t7* p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9515 (t1670 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
