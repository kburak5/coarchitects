﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t118;
struct t29;
struct t6;
struct t321;
struct t2344;
struct t733;
struct t2346;
struct t20;
struct t136;
struct t2347;
struct t722;
#include "t735.h"
#include "t322.h"
#include "t323.h"
#include "t725.h"

 void m1409 (t118 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12038 (t118 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12039 (t118 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12040 (t118 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12041 (t118 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12042 (t118 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12043 (t118 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12044 (t118 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12045 (t118 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12046 (t118 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12047 (t118 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12048 (t118 * __this, t322  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12049 (t118 * __this, t322  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12050 (t118 * __this, t2346* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12051 (t118 * __this, t322  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12052 (t118 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12053 (t118 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m12054 (t118 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12055 (t118 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m12056 (t118 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t6 * m12057 (t118 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12058 (t118 * __this, int32_t p0, t6 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12059 (t118 * __this, int32_t p0, t29* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12060 (t118 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12061 (t118 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t322  m12062 (t29 * __this, int32_t p0, t6 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t6 * m12063 (t29 * __this, int32_t p0, t6 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12064 (t118 * __this, t2346* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12065 (t118 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1411 (t118 * __this, int32_t p0, t6 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1429 (t118 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12066 (t118 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12067 (t118 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12068 (t118 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12069 (t118 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1412 (t118 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1410 (t118 * __this, int32_t p0, t6 ** p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t321 * m1424 (t118 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m12070 (t118 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t6 * m12071 (t118 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12072 (t118 * __this, t322  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t323  m1432 (t118 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t725  m12073 (t29 * __this, int32_t p0, t6 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
