﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1125;
struct t7;
struct t841;
struct t29;
struct t42;
struct t1094;

 void m6924 (t1125 * __this, int32_t p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6925 (t1125 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6926 (t1125 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6927 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6928 (t1125 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6929 (t1125 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6930 (t1125 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t841* m6931 (t1125 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6932 (t1125 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6933 (t1125 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6934 (t1125 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1125 * m6935 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1125 * m6936 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6937 (t1125 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6938 (t1125 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6939 (t1125 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6940 (t1125 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6941 (t1125 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6942 (t1125 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t841* m6943 (t1125 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6944 (t1125 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6945 (t1125 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6946 (t1125 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6947 (t1125 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6948 (t1125 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t841* m6949 (t1125 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6950 (t1125 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6951 (t1125 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6952 (t1125 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6953 (t1125 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6954 (t1125 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6955 (t1125 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m6956 (t1125 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m6957 (t1125 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1125 * m6958 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
