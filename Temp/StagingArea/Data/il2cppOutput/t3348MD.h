﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3348;
struct t29;
struct t20;
#include "t1311.h"

 void m18612 (t3348 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18613 (t3348 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18614 (t3348 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18615 (t3348 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18616 (t3348 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
