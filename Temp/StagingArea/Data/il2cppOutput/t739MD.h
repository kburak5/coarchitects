﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t739;
struct t7;
struct t42;
struct t29;

 void m3150 (t739 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3151 (t739 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3152 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3153 (t739 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3154 (t739 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3155 (t739 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
