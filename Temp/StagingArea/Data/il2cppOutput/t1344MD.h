﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1344;
struct t929;
struct t7;
struct t42;
struct t1142;
struct t660;
struct t631;
struct t537;
struct t634;
struct t1147;
struct t316;
struct t1143;
struct t1144;
struct t557;
struct t1145;
struct t1146;
struct t29;
struct t633;
struct t446;
struct t295;
#include "t43.h"
#include "t1148.h"
#include "t630.h"
#include "t1150.h"

 t929 * m7287 (t1344 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7288 (t1344 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7289 (t1344 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7290 (t1344 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7291 (t1344 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1142 * m7292 (t1344 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7293 (t1344 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7294 (t1344 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7295 (t1344 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t43  m7296 (t1344 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7297 (t1344 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7298 (t1344 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t660 * m7299 (t1344 * __this, int32_t p0, t631 * p1, int32_t p2, t537* p3, t634* p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1147* m7300 (t1344 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m7301 (t1344 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m7302 (t1344 * __this, t42 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7303 (t1344 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1143 * m7304 (t1344 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1144 * m7305 (t1344 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t537* m7306 (t1344 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t557 * m7307 (t1344 * __this, t7* p0, int32_t p1, t631 * p2, int32_t p3, t537* p4, t634* p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1145* m7308 (t1344 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1146 * m7309 (t1344 * __this, t7* p0, int32_t p1, t631 * p2, t42 * p3, t537* p4, t634* p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7310 (t1344 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7311 (t1344 * __this, t7* p0, int32_t p1, t631 * p2, t29 * p3, t316* p4, t634* p5, t633 * p6, t446* p7, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7312 (t1344 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7313 (t1344 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7314 (t1344 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7315 (t1344 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7316 (t1344 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7317 (t1344 * __this, t42 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t295 * m7318 (t1344 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
