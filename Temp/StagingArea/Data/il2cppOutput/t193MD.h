﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t193;
struct t29;
struct t7;
struct t66;
struct t67;
#include "t35.h"

 void m558 (t193 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m559 (t193 * __this, t7* p0, int32_t p1, uint16_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m560 (t193 * __this, t7* p0, int32_t p1, uint16_t p2, t67 * p3, t29 * p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m561 (t193 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
