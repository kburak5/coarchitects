﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t492;
struct t29;
#include "t492.h"
#include "t132.h"
#include "t23.h"

 void m2450 (t492 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2451 (t29 * __this, t492 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2452 (t29 * __this, t492 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2453 (t492 * __this, t132  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2454 (t29 * __this, t132  p0, t492 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2455 (t29 * __this, t132 * p0, t492 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2456 (t492 * __this, t23  p0, t132  p1, float p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2457 (t29 * __this, t23  p0, t132  p1, t492 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2458 (t29 * __this, t23 * p0, t132 * p1, t492 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m2459 (t492 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2460 (t492 * __this, int32_t p0, int32_t p1, float p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2461 (t492 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2462 (t492 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t492  m2463 (t29 * __this, t492  p0, float p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t492  m2464 (t29 * __this, float p0, t492  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t492  m2465 (t29 * __this, t492  p0, t492  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2466 (t29 * __this, t492  p0, t492  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2467 (t29 * __this, t492  p0, t492  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
