﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2906;
struct t29;
struct t20;
#include "t149.h"

 void m15896 (t2906 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15897 (t2906 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15898 (t2906 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15899 (t2906 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m15900 (t2906 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
