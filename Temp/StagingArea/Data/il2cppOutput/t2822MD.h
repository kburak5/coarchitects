﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2822;
struct t29;
struct t450;
struct t450_marshaled;
struct t20;

#include "t2111MD.h"
#define m15374(__this, p0, method) (void)m10288_gshared((t2111 *)__this, (t20 *)p0, method)
#define m15375(__this, method) (t29 *)m10290_gshared((t2111 *)__this, method)
#define m15376(__this, method) (void)m10292_gshared((t2111 *)__this, method)
#define m15377(__this, method) (bool)m10294_gshared((t2111 *)__this, method)
#define m15378(__this, method) (t450 *)m10296_gshared((t2111 *)__this, method)
