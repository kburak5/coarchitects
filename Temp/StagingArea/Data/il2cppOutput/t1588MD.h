﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1588;
struct t1587;

 void m8604 (t1588 * __this, t1587 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8605 (t1588 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8606 (t1588 * __this, uint16_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8607 (t1588 * __this, uint16_t p0, uint16_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8608 (t1588 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m8609 (t1588 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
