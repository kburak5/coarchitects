﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3253;
struct t29;
struct t20;
#include "t1022.h"

 void m18061 (t3253 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18062 (t3253 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18063 (t3253 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18064 (t3253 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18065 (t3253 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
