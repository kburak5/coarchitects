﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t3149_TI;


#include "t20.h"

// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Events.BaseInvokableCall>
extern Il2CppType t3147_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30275_GM;
MethodInfo m30275_MI = 
{
	"GetEnumerator", NULL, &t3149_TI, &t3147_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30275_GM};
static MethodInfo* t3149_MIs[] =
{
	&m30275_MI,
	NULL
};
extern TypeInfo t603_TI;
static TypeInfo* t3149_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3149_0_0_0;
extern Il2CppType t3149_1_0_0;
struct t3149;
extern Il2CppGenericClass t3149_GC;
TypeInfo t3149_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t3149_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t3149_TI, t3149_ITIs, NULL, &EmptyCustomAttributesCache, &t3149_TI, &t3149_0_0_0, &t3149_1_0_0, NULL, &t3149_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3147_TI;

#include "t556.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Events.BaseInvokableCall>
extern MethodInfo m30276_MI;
static PropertyInfo t3147____Current_PropertyInfo = 
{
	&t3147_TI, "Current", &m30276_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3147_PIs[] =
{
	&t3147____Current_PropertyInfo,
	NULL
};
extern Il2CppType t556_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30276_GM;
MethodInfo m30276_MI = 
{
	"get_Current", NULL, &t3147_TI, &t556_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30276_GM};
static MethodInfo* t3147_MIs[] =
{
	&m30276_MI,
	NULL
};
extern TypeInfo t136_TI;
extern TypeInfo t324_TI;
static TypeInfo* t3147_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3147_0_0_0;
extern Il2CppType t3147_1_0_0;
struct t3147;
extern Il2CppGenericClass t3147_GC;
TypeInfo t3147_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t3147_MIs, t3147_PIs, NULL, NULL, NULL, NULL, NULL, &t3147_TI, t3147_ITIs, NULL, &EmptyCustomAttributesCache, &t3147_TI, &t3147_0_0_0, &t3147_1_0_0, NULL, &t3147_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3153.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3153_TI;
#include "t3153MD.h"

#include "t29.h"
#include "t44.h"
#include "t7.h"
#include "t914.h"
#include "t21.h"
#include "t40.h"
extern TypeInfo t556_TI;
extern TypeInfo t914_TI;
#include "t914MD.h"
#include "t20MD.h"
extern MethodInfo m17481_MI;
extern MethodInfo m3964_MI;
extern MethodInfo m3969_MI;
extern MethodInfo m23077_MI;
struct t20;
#include "t915.h"
struct t20;
 t29 * m19490_gshared (t20 * __this, int32_t p0, MethodInfo* method);
#define m19490(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
#define m23077(__this, p0, method) (t556 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Events.BaseInvokableCall>
extern Il2CppType t20_0_0_1;
FieldInfo t3153_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3153_TI, offsetof(t3153, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3153_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3153_TI, offsetof(t3153, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3153_FIs[] =
{
	&t3153_f0_FieldInfo,
	&t3153_f1_FieldInfo,
	NULL
};
extern MethodInfo m17478_MI;
static PropertyInfo t3153____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3153_TI, "System.Collections.IEnumerator.Current", &m17478_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3153____Current_PropertyInfo = 
{
	&t3153_TI, "Current", &m17481_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3153_PIs[] =
{
	&t3153____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3153____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
static ParameterInfo t3153_m17477_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17477_GM;
MethodInfo m17477_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3153_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3153_m17477_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17477_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17478_GM;
MethodInfo m17478_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3153_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17478_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17479_GM;
MethodInfo m17479_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3153_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17479_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17480_GM;
MethodInfo m17480_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3153_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17480_GM};
extern Il2CppType t556_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17481_GM;
MethodInfo m17481_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3153_TI, &t556_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17481_GM};
static MethodInfo* t3153_MIs[] =
{
	&m17477_MI,
	&m17478_MI,
	&m17479_MI,
	&m17480_MI,
	&m17481_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
extern MethodInfo m17480_MI;
extern MethodInfo m17479_MI;
static MethodInfo* t3153_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17478_MI,
	&m17480_MI,
	&m17479_MI,
	&m17481_MI,
};
static TypeInfo* t3153_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3147_TI,
};
static Il2CppInterfaceOffsetPair t3153_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3147_TI, 7},
};
extern TypeInfo t556_TI;
static Il2CppRGCTXData t3153_RGCTXData[3] = 
{
	&m17481_MI/* Method Usage */,
	&t556_TI/* Class Usage */,
	&m23077_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3153_0_0_0;
extern Il2CppType t3153_1_0_0;
extern TypeInfo t110_TI;
extern Il2CppGenericClass t3153_GC;
extern TypeInfo t20_TI;
TypeInfo t3153_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3153_MIs, t3153_PIs, t3153_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3153_TI, t3153_ITIs, t3153_VT, &EmptyCustomAttributesCache, &t3153_TI, &t3153_0_0_0, &t3153_1_0_0, t3153_IOs, &t3153_GC, NULL, NULL, NULL, t3153_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3153)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3154_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Events.BaseInvokableCall>
extern MethodInfo m30282_MI;
extern MethodInfo m30283_MI;
static PropertyInfo t3154____Item_PropertyInfo = 
{
	&t3154_TI, "Item", &m30282_MI, &m30283_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3154_PIs[] =
{
	&t3154____Item_PropertyInfo,
	NULL
};
extern Il2CppType t556_0_0_0;
extern Il2CppType t556_0_0_0;
static ParameterInfo t3154_m30284_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30284_GM;
MethodInfo m30284_MI = 
{
	"IndexOf", NULL, &t3154_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3154_m30284_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30284_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t556_0_0_0;
static ParameterInfo t3154_m30285_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30285_GM;
MethodInfo m30285_MI = 
{
	"Insert", NULL, &t3154_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3154_m30285_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30285_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3154_m30286_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30286_GM;
MethodInfo m30286_MI = 
{
	"RemoveAt", NULL, &t3154_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t3154_m30286_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30286_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3154_m30282_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t556_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30282_GM;
MethodInfo m30282_MI = 
{
	"get_Item", NULL, &t3154_TI, &t556_0_0_0, RuntimeInvoker_t29_t44, t3154_m30282_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30282_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t556_0_0_0;
static ParameterInfo t3154_m30283_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30283_GM;
MethodInfo m30283_MI = 
{
	"set_Item", NULL, &t3154_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3154_m30283_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30283_GM};
static MethodInfo* t3154_MIs[] =
{
	&m30284_MI,
	&m30285_MI,
	&m30286_MI,
	&m30282_MI,
	&m30283_MI,
	NULL
};
extern TypeInfo t3148_TI;
static TypeInfo* t3154_ITIs[] = 
{
	&t603_TI,
	&t3148_TI,
	&t3149_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3154_0_0_0;
extern Il2CppType t3154_1_0_0;
struct t3154;
extern Il2CppGenericClass t3154_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t3154_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t3154_MIs, t3154_PIs, NULL, NULL, NULL, NULL, NULL, &t3154_TI, t3154_ITIs, NULL, &t1908__CustomAttributeCache, &t3154_TI, &t3154_0_0_0, &t3154_1_0_0, NULL, &t3154_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t3152.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3152_TI;
#include "t3152MD.h"

#include "t570.h"
#include "t42.h"
#include "t1101.h"
#include "UnityEngine_ArrayTypes.h"
extern TypeInfo t570_TI;
extern TypeInfo t42_TI;
extern TypeInfo t7_TI;
extern TypeInfo t1101_TI;
#include "t29MD.h"
#include "t42MD.h"
#include "t1101MD.h"
extern MethodInfo m17485_MI;
extern MethodInfo m3974_MI;
extern MethodInfo m1430_MI;
extern MethodInfo m2998_MI;
extern MethodInfo m5150_MI;


// Metadata Definition System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.BaseInvokableCall>
extern Il2CppType t570_0_0_1;
FieldInfo t3152_f0_FieldInfo = 
{
	"l", &t570_0_0_1, &t3152_TI, offsetof(t3152, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3152_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t3152_TI, offsetof(t3152, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3152_f2_FieldInfo = 
{
	"ver", &t44_0_0_1, &t3152_TI, offsetof(t3152, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t556_0_0_1;
FieldInfo t3152_f3_FieldInfo = 
{
	"current", &t556_0_0_1, &t3152_TI, offsetof(t3152, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3152_FIs[] =
{
	&t3152_f0_FieldInfo,
	&t3152_f1_FieldInfo,
	&t3152_f2_FieldInfo,
	&t3152_f3_FieldInfo,
	NULL
};
extern MethodInfo m17483_MI;
static PropertyInfo t3152____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3152_TI, "System.Collections.IEnumerator.Current", &m17483_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17487_MI;
static PropertyInfo t3152____Current_PropertyInfo = 
{
	&t3152_TI, "Current", &m17487_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3152_PIs[] =
{
	&t3152____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3152____Current_PropertyInfo,
	NULL
};
extern Il2CppType t570_0_0_0;
extern Il2CppType t570_0_0_0;
static ParameterInfo t3152_m17482_ParameterInfos[] = 
{
	{"l", 0, 134217728, &EmptyCustomAttributesCache, &t570_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17482_GM;
MethodInfo m17482_MI = 
{
	".ctor", (methodPointerType)&m10631_gshared, &t3152_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3152_m17482_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17482_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17483_GM;
MethodInfo m17483_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10632_gshared, &t3152_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17483_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17484_GM;
MethodInfo m17484_MI = 
{
	"Dispose", (methodPointerType)&m10633_gshared, &t3152_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17484_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17485_GM;
MethodInfo m17485_MI = 
{
	"VerifyState", (methodPointerType)&m10634_gshared, &t3152_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17485_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17486_GM;
MethodInfo m17486_MI = 
{
	"MoveNext", (methodPointerType)&m10635_gshared, &t3152_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17486_GM};
extern Il2CppType t556_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17487_GM;
MethodInfo m17487_MI = 
{
	"get_Current", (methodPointerType)&m10636_gshared, &t3152_TI, &t556_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17487_GM};
static MethodInfo* t3152_MIs[] =
{
	&m17482_MI,
	&m17483_MI,
	&m17484_MI,
	&m17485_MI,
	&m17486_MI,
	&m17487_MI,
	NULL
};
extern MethodInfo m17486_MI;
extern MethodInfo m17484_MI;
static MethodInfo* t3152_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17483_MI,
	&m17486_MI,
	&m17484_MI,
	&m17487_MI,
};
static TypeInfo* t3152_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3147_TI,
};
static Il2CppInterfaceOffsetPair t3152_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3147_TI, 7},
};
extern TypeInfo t556_TI;
extern TypeInfo t3152_TI;
static Il2CppRGCTXData t3152_RGCTXData[3] = 
{
	&m17485_MI/* Method Usage */,
	&t556_TI/* Class Usage */,
	&t3152_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3152_0_0_0;
extern Il2CppType t3152_1_0_0;
extern Il2CppGenericClass t3152_GC;
extern TypeInfo t1261_TI;
TypeInfo t3152_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t3152_MIs, t3152_PIs, t3152_FIs, NULL, &t110_TI, NULL, &t1261_TI, &t3152_TI, t3152_ITIs, t3152_VT, &EmptyCustomAttributesCache, &t3152_TI, &t3152_0_0_0, &t3152_1_0_0, t3152_IOs, &t3152_GC, NULL, NULL, NULL, t3152_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3152)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 4, 0, 0, 8, 3, 3};
#include "t3150.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3150_TI;
#include "t3150MD.h"

#include "t345.h"
#include "t338.h"
extern TypeInfo t44_TI;
extern TypeInfo t345_TI;
extern TypeInfo t338_TI;
extern TypeInfo t674_TI;
extern TypeInfo t21_TI;
extern TypeInfo t40_TI;
extern TypeInfo t3146_TI;
#include "t345MD.h"
#include "t338MD.h"
#include "t3155MD.h"
extern MethodInfo m17517_MI;
extern MethodInfo m1516_MI;
extern MethodInfo m30273_MI;
extern MethodInfo m1331_MI;
extern MethodInfo m2950_MI;
extern MethodInfo m3960_MI;
extern MethodInfo m4154_MI;
extern MethodInfo m17549_MI;
extern MethodInfo m30280_MI;
extern MethodInfo m30284_MI;
extern MethodInfo m30274_MI;
extern MethodInfo m30275_MI;


// Metadata Definition System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Events.BaseInvokableCall>
extern Il2CppType t3154_0_0_1;
FieldInfo t3150_f0_FieldInfo = 
{
	"list", &t3154_0_0_1, &t3150_TI, offsetof(t3150, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3150_FIs[] =
{
	&t3150_f0_FieldInfo,
	NULL
};
extern MethodInfo m17494_MI;
extern MethodInfo m17495_MI;
static PropertyInfo t3150____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo = 
{
	&t3150_TI, "System.Collections.Generic.IList<T>.Item", &m17494_MI, &m17495_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17496_MI;
static PropertyInfo t3150____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t3150_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m17496_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17506_MI;
static PropertyInfo t3150____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t3150_TI, "System.Collections.ICollection.IsSynchronized", &m17506_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17507_MI;
static PropertyInfo t3150____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t3150_TI, "System.Collections.ICollection.SyncRoot", &m17507_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17508_MI;
static PropertyInfo t3150____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t3150_TI, "System.Collections.IList.IsFixedSize", &m17508_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17509_MI;
static PropertyInfo t3150____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t3150_TI, "System.Collections.IList.IsReadOnly", &m17509_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17510_MI;
extern MethodInfo m17511_MI;
static PropertyInfo t3150____System_Collections_IList_Item_PropertyInfo = 
{
	&t3150_TI, "System.Collections.IList.Item", &m17510_MI, &m17511_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17516_MI;
static PropertyInfo t3150____Count_PropertyInfo = 
{
	&t3150_TI, "Count", &m17516_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3150____Item_PropertyInfo = 
{
	&t3150_TI, "Item", &m17517_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3150_PIs[] =
{
	&t3150____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo,
	&t3150____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t3150____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t3150____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t3150____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t3150____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t3150____System_Collections_IList_Item_PropertyInfo,
	&t3150____Count_PropertyInfo,
	&t3150____Item_PropertyInfo,
	NULL
};
extern Il2CppType t3154_0_0_0;
static ParameterInfo t3150_m17488_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t3154_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17488_GM;
MethodInfo m17488_MI = 
{
	".ctor", (methodPointerType)&m10637_gshared, &t3150_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3150_m17488_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17488_GM};
extern Il2CppType t556_0_0_0;
static ParameterInfo t3150_m17489_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17489_GM;
MethodInfo m17489_MI = 
{
	"System.Collections.Generic.ICollection<T>.Add", (methodPointerType)&m10638_gshared, &t3150_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3150_m17489_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17489_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17490_GM;
MethodInfo m17490_MI = 
{
	"System.Collections.Generic.ICollection<T>.Clear", (methodPointerType)&m10639_gshared, &t3150_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17490_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t556_0_0_0;
static ParameterInfo t3150_m17491_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17491_GM;
MethodInfo m17491_MI = 
{
	"System.Collections.Generic.IList<T>.Insert", (methodPointerType)&m10640_gshared, &t3150_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3150_m17491_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17491_GM};
extern Il2CppType t556_0_0_0;
static ParameterInfo t3150_m17492_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17492_GM;
MethodInfo m17492_MI = 
{
	"System.Collections.Generic.ICollection<T>.Remove", (methodPointerType)&m10641_gshared, &t3150_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3150_m17492_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17492_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3150_m17493_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17493_GM;
MethodInfo m17493_MI = 
{
	"System.Collections.Generic.IList<T>.RemoveAt", (methodPointerType)&m10642_gshared, &t3150_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t3150_m17493_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17493_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3150_m17494_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t556_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17494_GM;
MethodInfo m17494_MI = 
{
	"System.Collections.Generic.IList<T>.get_Item", (methodPointerType)&m10643_gshared, &t3150_TI, &t556_0_0_0, RuntimeInvoker_t29_t44, t3150_m17494_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17494_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t556_0_0_0;
static ParameterInfo t3150_m17495_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17495_GM;
MethodInfo m17495_MI = 
{
	"System.Collections.Generic.IList<T>.set_Item", (methodPointerType)&m10644_gshared, &t3150_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3150_m17495_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17495_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17496_GM;
MethodInfo m17496_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10645_gshared, &t3150_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17496_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3150_m17497_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17497_GM;
MethodInfo m17497_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10646_gshared, &t3150_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3150_m17497_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17497_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17498_GM;
MethodInfo m17498_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10647_gshared, &t3150_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17498_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3150_m17499_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17499_GM;
MethodInfo m17499_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10648_gshared, &t3150_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3150_m17499_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17499_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17500_GM;
MethodInfo m17500_MI = 
{
	"System.Collections.IList.Clear", (methodPointerType)&m10649_gshared, &t3150_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 14, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17500_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3150_m17501_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17501_GM;
MethodInfo m17501_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10650_gshared, &t3150_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3150_m17501_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17501_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3150_m17502_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17502_GM;
MethodInfo m17502_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10651_gshared, &t3150_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3150_m17502_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17502_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3150_m17503_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17503_GM;
MethodInfo m17503_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10652_gshared, &t3150_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3150_m17503_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17503_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3150_m17504_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17504_GM;
MethodInfo m17504_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10653_gshared, &t3150_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3150_m17504_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17504_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3150_m17505_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17505_GM;
MethodInfo m17505_MI = 
{
	"System.Collections.IList.RemoveAt", (methodPointerType)&m10654_gshared, &t3150_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t3150_m17505_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17505_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17506_GM;
MethodInfo m17506_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10655_gshared, &t3150_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17506_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17507_GM;
MethodInfo m17507_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10656_gshared, &t3150_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17507_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17508_GM;
MethodInfo m17508_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10657_gshared, &t3150_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17508_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17509_GM;
MethodInfo m17509_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10658_gshared, &t3150_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17509_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3150_m17510_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17510_GM;
MethodInfo m17510_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10659_gshared, &t3150_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t3150_m17510_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17510_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3150_m17511_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17511_GM;
MethodInfo m17511_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10660_gshared, &t3150_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3150_m17511_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17511_GM};
extern Il2CppType t556_0_0_0;
static ParameterInfo t3150_m17512_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17512_GM;
MethodInfo m17512_MI = 
{
	"Contains", (methodPointerType)&m10661_gshared, &t3150_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3150_m17512_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17512_GM};
extern Il2CppType t3146_0_0_0;
extern Il2CppType t3146_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3150_m17513_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3146_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17513_GM;
MethodInfo m17513_MI = 
{
	"CopyTo", (methodPointerType)&m10662_gshared, &t3150_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3150_m17513_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17513_GM};
extern Il2CppType t3147_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17514_GM;
MethodInfo m17514_MI = 
{
	"GetEnumerator", (methodPointerType)&m10663_gshared, &t3150_TI, &t3147_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17514_GM};
extern Il2CppType t556_0_0_0;
static ParameterInfo t3150_m17515_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17515_GM;
MethodInfo m17515_MI = 
{
	"IndexOf", (methodPointerType)&m10664_gshared, &t3150_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3150_m17515_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17515_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17516_GM;
MethodInfo m17516_MI = 
{
	"get_Count", (methodPointerType)&m10665_gshared, &t3150_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17516_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3150_m17517_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t556_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17517_GM;
MethodInfo m17517_MI = 
{
	"get_Item", (methodPointerType)&m10666_gshared, &t3150_TI, &t556_0_0_0, RuntimeInvoker_t29_t44, t3150_m17517_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 33, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17517_GM};
static MethodInfo* t3150_MIs[] =
{
	&m17488_MI,
	&m17489_MI,
	&m17490_MI,
	&m17491_MI,
	&m17492_MI,
	&m17493_MI,
	&m17494_MI,
	&m17495_MI,
	&m17496_MI,
	&m17497_MI,
	&m17498_MI,
	&m17499_MI,
	&m17500_MI,
	&m17501_MI,
	&m17502_MI,
	&m17503_MI,
	&m17504_MI,
	&m17505_MI,
	&m17506_MI,
	&m17507_MI,
	&m17508_MI,
	&m17509_MI,
	&m17510_MI,
	&m17511_MI,
	&m17512_MI,
	&m17513_MI,
	&m17514_MI,
	&m17515_MI,
	&m17516_MI,
	&m17517_MI,
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m1332_MI;
extern MethodInfo m17498_MI;
extern MethodInfo m17497_MI;
extern MethodInfo m17499_MI;
extern MethodInfo m17500_MI;
extern MethodInfo m17501_MI;
extern MethodInfo m17502_MI;
extern MethodInfo m17503_MI;
extern MethodInfo m17504_MI;
extern MethodInfo m17505_MI;
extern MethodInfo m17489_MI;
extern MethodInfo m17490_MI;
extern MethodInfo m17512_MI;
extern MethodInfo m17513_MI;
extern MethodInfo m17492_MI;
extern MethodInfo m17515_MI;
extern MethodInfo m17491_MI;
extern MethodInfo m17493_MI;
extern MethodInfo m17514_MI;
static MethodInfo* t3150_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m17498_MI,
	&m17516_MI,
	&m17506_MI,
	&m17507_MI,
	&m17497_MI,
	&m17508_MI,
	&m17509_MI,
	&m17510_MI,
	&m17511_MI,
	&m17499_MI,
	&m17500_MI,
	&m17501_MI,
	&m17502_MI,
	&m17503_MI,
	&m17504_MI,
	&m17505_MI,
	&m17516_MI,
	&m17496_MI,
	&m17489_MI,
	&m17490_MI,
	&m17512_MI,
	&m17513_MI,
	&m17492_MI,
	&m17515_MI,
	&m17491_MI,
	&m17493_MI,
	&m17494_MI,
	&m17495_MI,
	&m17514_MI,
	&m17517_MI,
};
extern TypeInfo t868_TI;
static TypeInfo* t3150_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t3148_TI,
	&t3154_TI,
	&t3149_TI,
};
static Il2CppInterfaceOffsetPair t3150_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t3148_TI, 20},
	{ &t3154_TI, 27},
	{ &t3149_TI, 32},
};
extern TypeInfo t556_TI;
static Il2CppRGCTXData t3150_RGCTXData[9] = 
{
	&m17517_MI/* Method Usage */,
	&m17549_MI/* Method Usage */,
	&t556_TI/* Class Usage */,
	&m30280_MI/* Method Usage */,
	&m30284_MI/* Method Usage */,
	&m30282_MI/* Method Usage */,
	&m30274_MI/* Method Usage */,
	&m30275_MI/* Method Usage */,
	&m30273_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3150_0_0_0;
extern Il2CppType t3150_1_0_0;
extern TypeInfo t29_TI;
struct t3150;
extern Il2CppGenericClass t3150_GC;
extern CustomAttributesCache t1263__CustomAttributeCache;
TypeInfo t3150_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ReadOnlyCollection`1", "System.Collections.ObjectModel", t3150_MIs, t3150_PIs, t3150_FIs, NULL, &t29_TI, NULL, NULL, &t3150_TI, t3150_ITIs, t3150_VT, &t1263__CustomAttributeCache, &t3150_TI, &t3150_0_0_0, &t3150_1_0_0, t3150_IOs, &t3150_GC, NULL, NULL, NULL, t3150_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3150), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 30, 9, 1, 0, 0, 34, 6, 6};
#include "t3155.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3155_TI;

#include "t43.h"
#include "t305.h"
extern TypeInfo t305_TI;
#include "t570MD.h"
#include "t305MD.h"
extern MethodInfo m30277_MI;
extern MethodInfo m17552_MI;
extern MethodInfo m17553_MI;
extern MethodInfo m17550_MI;
extern MethodInfo m17548_MI;
extern MethodInfo m2987_MI;
extern MethodInfo m9816_MI;
extern MethodInfo m17541_MI;
extern MethodInfo m17551_MI;
extern MethodInfo m17539_MI;
extern MethodInfo m17544_MI;
extern MethodInfo m17535_MI;
extern MethodInfo m30279_MI;
extern MethodInfo m30285_MI;
extern MethodInfo m30286_MI;
extern MethodInfo m1554_MI;
extern MethodInfo m6004_MI;
extern MethodInfo m1935_MI;
extern MethodInfo m9815_MI;
extern MethodInfo m9817_MI;


// Metadata Definition System.Collections.ObjectModel.Collection`1<UnityEngine.Events.BaseInvokableCall>
extern Il2CppType t3154_0_0_1;
FieldInfo t3155_f0_FieldInfo = 
{
	"list", &t3154_0_0_1, &t3155_TI, offsetof(t3155, f0), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t3155_f1_FieldInfo = 
{
	"syncRoot", &t29_0_0_1, &t3155_TI, offsetof(t3155, f1), &EmptyCustomAttributesCache};
static FieldInfo* t3155_FIs[] =
{
	&t3155_f0_FieldInfo,
	&t3155_f1_FieldInfo,
	NULL
};
extern MethodInfo m17519_MI;
static PropertyInfo t3155____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t3155_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m17519_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17527_MI;
static PropertyInfo t3155____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t3155_TI, "System.Collections.ICollection.IsSynchronized", &m17527_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17528_MI;
static PropertyInfo t3155____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t3155_TI, "System.Collections.ICollection.SyncRoot", &m17528_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17529_MI;
static PropertyInfo t3155____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t3155_TI, "System.Collections.IList.IsFixedSize", &m17529_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17530_MI;
static PropertyInfo t3155____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t3155_TI, "System.Collections.IList.IsReadOnly", &m17530_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17531_MI;
extern MethodInfo m17532_MI;
static PropertyInfo t3155____System_Collections_IList_Item_PropertyInfo = 
{
	&t3155_TI, "System.Collections.IList.Item", &m17531_MI, &m17532_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17545_MI;
static PropertyInfo t3155____Count_PropertyInfo = 
{
	&t3155_TI, "Count", &m17545_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m17546_MI;
extern MethodInfo m17547_MI;
static PropertyInfo t3155____Item_PropertyInfo = 
{
	&t3155_TI, "Item", &m17546_MI, &m17547_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3155_PIs[] =
{
	&t3155____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t3155____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t3155____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t3155____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t3155____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t3155____System_Collections_IList_Item_PropertyInfo,
	&t3155____Count_PropertyInfo,
	&t3155____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17518_GM;
MethodInfo m17518_MI = 
{
	".ctor", (methodPointerType)&m10667_gshared, &t3155_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17518_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17519_GM;
MethodInfo m17519_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10668_gshared, &t3155_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17519_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3155_m17520_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17520_GM;
MethodInfo m17520_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10669_gshared, &t3155_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3155_m17520_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17520_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17521_GM;
MethodInfo m17521_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10670_gshared, &t3155_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17521_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3155_m17522_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17522_GM;
MethodInfo m17522_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10671_gshared, &t3155_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3155_m17522_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17522_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3155_m17523_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17523_GM;
MethodInfo m17523_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10672_gshared, &t3155_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3155_m17523_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17523_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3155_m17524_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17524_GM;
MethodInfo m17524_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10673_gshared, &t3155_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3155_m17524_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17524_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3155_m17525_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17525_GM;
MethodInfo m17525_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10674_gshared, &t3155_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3155_m17525_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17525_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3155_m17526_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17526_GM;
MethodInfo m17526_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10675_gshared, &t3155_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3155_m17526_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17526_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17527_GM;
MethodInfo m17527_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10676_gshared, &t3155_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17527_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17528_GM;
MethodInfo m17528_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10677_gshared, &t3155_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17528_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17529_GM;
MethodInfo m17529_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10678_gshared, &t3155_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17529_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17530_GM;
MethodInfo m17530_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10679_gshared, &t3155_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17530_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3155_m17531_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17531_GM;
MethodInfo m17531_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10680_gshared, &t3155_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t3155_m17531_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17531_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3155_m17532_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17532_GM;
MethodInfo m17532_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10681_gshared, &t3155_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3155_m17532_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17532_GM};
extern Il2CppType t556_0_0_0;
static ParameterInfo t3155_m17533_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17533_GM;
MethodInfo m17533_MI = 
{
	"Add", (methodPointerType)&m10682_gshared, &t3155_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3155_m17533_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17533_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17534_GM;
MethodInfo m17534_MI = 
{
	"Clear", (methodPointerType)&m10683_gshared, &t3155_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17534_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17535_GM;
MethodInfo m17535_MI = 
{
	"ClearItems", (methodPointerType)&m10684_gshared, &t3155_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 452, 0, 33, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17535_GM};
extern Il2CppType t556_0_0_0;
static ParameterInfo t3155_m17536_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17536_GM;
MethodInfo m17536_MI = 
{
	"Contains", (methodPointerType)&m10685_gshared, &t3155_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3155_m17536_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17536_GM};
extern Il2CppType t3146_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3155_m17537_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3146_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17537_GM;
MethodInfo m17537_MI = 
{
	"CopyTo", (methodPointerType)&m10686_gshared, &t3155_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3155_m17537_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17537_GM};
extern Il2CppType t3147_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17538_GM;
MethodInfo m17538_MI = 
{
	"GetEnumerator", (methodPointerType)&m10687_gshared, &t3155_TI, &t3147_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17538_GM};
extern Il2CppType t556_0_0_0;
static ParameterInfo t3155_m17539_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17539_GM;
MethodInfo m17539_MI = 
{
	"IndexOf", (methodPointerType)&m10688_gshared, &t3155_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3155_m17539_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17539_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t556_0_0_0;
static ParameterInfo t3155_m17540_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17540_GM;
MethodInfo m17540_MI = 
{
	"Insert", (methodPointerType)&m10689_gshared, &t3155_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3155_m17540_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17540_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t556_0_0_0;
static ParameterInfo t3155_m17541_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17541_GM;
MethodInfo m17541_MI = 
{
	"InsertItem", (methodPointerType)&m10690_gshared, &t3155_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3155_m17541_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 34, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17541_GM};
extern Il2CppType t556_0_0_0;
static ParameterInfo t3155_m17542_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17542_GM;
MethodInfo m17542_MI = 
{
	"Remove", (methodPointerType)&m10691_gshared, &t3155_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3155_m17542_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17542_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3155_m17543_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17543_GM;
MethodInfo m17543_MI = 
{
	"RemoveAt", (methodPointerType)&m10692_gshared, &t3155_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t3155_m17543_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17543_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3155_m17544_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17544_GM;
MethodInfo m17544_MI = 
{
	"RemoveItem", (methodPointerType)&m10693_gshared, &t3155_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t3155_m17544_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 35, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17544_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17545_GM;
MethodInfo m17545_MI = 
{
	"get_Count", (methodPointerType)&m10694_gshared, &t3155_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17545_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3155_m17546_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t556_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17546_GM;
MethodInfo m17546_MI = 
{
	"get_Item", (methodPointerType)&m10695_gshared, &t3155_TI, &t556_0_0_0, RuntimeInvoker_t29_t44, t3155_m17546_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17546_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t556_0_0_0;
static ParameterInfo t3155_m17547_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17547_GM;
MethodInfo m17547_MI = 
{
	"set_Item", (methodPointerType)&m10696_gshared, &t3155_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3155_m17547_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17547_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t556_0_0_0;
static ParameterInfo t3155_m17548_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17548_GM;
MethodInfo m17548_MI = 
{
	"SetItem", (methodPointerType)&m10697_gshared, &t3155_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3155_m17548_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 36, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17548_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3155_m17549_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17549_GM;
MethodInfo m17549_MI = 
{
	"IsValidItem", (methodPointerType)&m10698_gshared, &t3155_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3155_m17549_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17549_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3155_m17550_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t556_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17550_GM;
MethodInfo m17550_MI = 
{
	"ConvertItem", (methodPointerType)&m10699_gshared, &t3155_TI, &t556_0_0_0, RuntimeInvoker_t29_t29, t3155_m17550_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17550_GM};
extern Il2CppType t3154_0_0_0;
static ParameterInfo t3155_m17551_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t3154_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17551_GM;
MethodInfo m17551_MI = 
{
	"CheckWritable", (methodPointerType)&m10700_gshared, &t3155_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3155_m17551_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17551_GM};
extern Il2CppType t3154_0_0_0;
static ParameterInfo t3155_m17552_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t3154_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17552_GM;
MethodInfo m17552_MI = 
{
	"IsSynchronized", (methodPointerType)&m10701_gshared, &t3155_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3155_m17552_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17552_GM};
extern Il2CppType t3154_0_0_0;
static ParameterInfo t3155_m17553_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t3154_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17553_GM;
MethodInfo m17553_MI = 
{
	"IsFixedSize", (methodPointerType)&m10702_gshared, &t3155_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3155_m17553_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17553_GM};
static MethodInfo* t3155_MIs[] =
{
	&m17518_MI,
	&m17519_MI,
	&m17520_MI,
	&m17521_MI,
	&m17522_MI,
	&m17523_MI,
	&m17524_MI,
	&m17525_MI,
	&m17526_MI,
	&m17527_MI,
	&m17528_MI,
	&m17529_MI,
	&m17530_MI,
	&m17531_MI,
	&m17532_MI,
	&m17533_MI,
	&m17534_MI,
	&m17535_MI,
	&m17536_MI,
	&m17537_MI,
	&m17538_MI,
	&m17539_MI,
	&m17540_MI,
	&m17541_MI,
	&m17542_MI,
	&m17543_MI,
	&m17544_MI,
	&m17545_MI,
	&m17546_MI,
	&m17547_MI,
	&m17548_MI,
	&m17549_MI,
	&m17550_MI,
	&m17551_MI,
	&m17552_MI,
	&m17553_MI,
	NULL
};
extern MethodInfo m17521_MI;
extern MethodInfo m17520_MI;
extern MethodInfo m17522_MI;
extern MethodInfo m17534_MI;
extern MethodInfo m17523_MI;
extern MethodInfo m17524_MI;
extern MethodInfo m17525_MI;
extern MethodInfo m17526_MI;
extern MethodInfo m17543_MI;
extern MethodInfo m17533_MI;
extern MethodInfo m17536_MI;
extern MethodInfo m17537_MI;
extern MethodInfo m17542_MI;
extern MethodInfo m17540_MI;
extern MethodInfo m17538_MI;
static MethodInfo* t3155_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m17521_MI,
	&m17545_MI,
	&m17527_MI,
	&m17528_MI,
	&m17520_MI,
	&m17529_MI,
	&m17530_MI,
	&m17531_MI,
	&m17532_MI,
	&m17522_MI,
	&m17534_MI,
	&m17523_MI,
	&m17524_MI,
	&m17525_MI,
	&m17526_MI,
	&m17543_MI,
	&m17545_MI,
	&m17519_MI,
	&m17533_MI,
	&m17534_MI,
	&m17536_MI,
	&m17537_MI,
	&m17542_MI,
	&m17539_MI,
	&m17540_MI,
	&m17543_MI,
	&m17546_MI,
	&m17547_MI,
	&m17538_MI,
	&m17535_MI,
	&m17541_MI,
	&m17544_MI,
	&m17548_MI,
};
static TypeInfo* t3155_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t3148_TI,
	&t3154_TI,
	&t3149_TI,
};
static Il2CppInterfaceOffsetPair t3155_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t3148_TI, 20},
	{ &t3154_TI, 27},
	{ &t3149_TI, 32},
};
extern TypeInfo t570_TI;
extern TypeInfo t556_TI;
static Il2CppRGCTXData t3155_RGCTXData[25] = 
{
	&t570_TI/* Class Usage */,
	&m2987_MI/* Method Usage */,
	&m30277_MI/* Method Usage */,
	&m30275_MI/* Method Usage */,
	&m30273_MI/* Method Usage */,
	&m17550_MI/* Method Usage */,
	&m17541_MI/* Method Usage */,
	&m17549_MI/* Method Usage */,
	&t556_TI/* Class Usage */,
	&m30280_MI/* Method Usage */,
	&m30284_MI/* Method Usage */,
	&m17551_MI/* Method Usage */,
	&m17539_MI/* Method Usage */,
	&m17544_MI/* Method Usage */,
	&m17552_MI/* Method Usage */,
	&m17553_MI/* Method Usage */,
	&m30282_MI/* Method Usage */,
	&m17548_MI/* Method Usage */,
	&m17535_MI/* Method Usage */,
	&m30279_MI/* Method Usage */,
	&m30274_MI/* Method Usage */,
	&m30285_MI/* Method Usage */,
	&m30286_MI/* Method Usage */,
	&m30283_MI/* Method Usage */,
	&t556_0_0_0/* Type Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3155_0_0_0;
extern Il2CppType t3155_1_0_0;
struct t3155;
extern Il2CppGenericClass t3155_GC;
extern CustomAttributesCache t1262__CustomAttributeCache;
TypeInfo t3155_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Collection`1", "System.Collections.ObjectModel", t3155_MIs, t3155_PIs, t3155_FIs, NULL, &t29_TI, NULL, NULL, &t3155_TI, t3155_ITIs, t3155_VT, &t1262__CustomAttributeCache, &t3155_TI, &t3155_0_0_0, &t3155_1_0_0, t3155_IOs, &t3155_GC, NULL, NULL, NULL, t3155_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3155), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 36, 8, 2, 0, 0, 37, 6, 6};
#include "t3156.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3156_TI;
#include "t3156MD.h"

#include "t1257.h"
#include "mscorlib_ArrayTypes.h"
#include "t3157.h"
extern TypeInfo t6753_TI;
extern TypeInfo t1257_TI;
extern TypeInfo t537_TI;
extern TypeInfo t3157_TI;
#include "t931MD.h"
#include "t3157MD.h"
extern Il2CppType t6753_0_0_0;
extern Il2CppType t1257_0_0_0;
extern MethodInfo m2981_MI;
extern MethodInfo m2979_MI;
extern MethodInfo m8828_MI;
extern MethodInfo m17559_MI;
extern MethodInfo m30287_MI;
extern MethodInfo m23089_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1<UnityEngine.Events.BaseInvokableCall>
extern Il2CppType t3156_0_0_49;
FieldInfo t3156_f0_FieldInfo = 
{
	"_default", &t3156_0_0_49, &t3156_TI, offsetof(t3156_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3156_FIs[] =
{
	&t3156_f0_FieldInfo,
	NULL
};
extern MethodInfo m17558_MI;
static PropertyInfo t3156____Default_PropertyInfo = 
{
	&t3156_TI, "Default", &m17558_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3156_PIs[] =
{
	&t3156____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17554_GM;
MethodInfo m17554_MI = 
{
	".ctor", (methodPointerType)&m10703_gshared, &t3156_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17554_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17555_GM;
MethodInfo m17555_MI = 
{
	".cctor", (methodPointerType)&m10704_gshared, &t3156_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17555_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3156_m17556_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17556_GM;
MethodInfo m17556_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m10705_gshared, &t3156_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3156_m17556_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17556_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3156_m17557_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17557_GM;
MethodInfo m17557_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m10706_gshared, &t3156_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t3156_m17557_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17557_GM};
extern Il2CppType t556_0_0_0;
static ParameterInfo t3156_m30287_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30287_GM;
MethodInfo m30287_MI = 
{
	"GetHashCode", NULL, &t3156_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3156_m30287_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30287_GM};
extern Il2CppType t556_0_0_0;
extern Il2CppType t556_0_0_0;
static ParameterInfo t3156_m23089_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m23089_GM;
MethodInfo m23089_MI = 
{
	"Equals", NULL, &t3156_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t3156_m23089_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m23089_GM};
extern Il2CppType t3156_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17558_GM;
MethodInfo m17558_MI = 
{
	"get_Default", (methodPointerType)&m10707_gshared, &t3156_TI, &t3156_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17558_GM};
static MethodInfo* t3156_MIs[] =
{
	&m17554_MI,
	&m17555_MI,
	&m17556_MI,
	&m17557_MI,
	&m30287_MI,
	&m23089_MI,
	&m17558_MI,
	NULL
};
extern MethodInfo m17557_MI;
extern MethodInfo m17556_MI;
static MethodInfo* t3156_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m23089_MI,
	&m30287_MI,
	&m17557_MI,
	&m17556_MI,
	NULL,
	NULL,
};
extern TypeInfo t6754_TI;
extern TypeInfo t734_TI;
static TypeInfo* t3156_ITIs[] = 
{
	&t6754_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t3156_IOs[] = 
{
	{ &t6754_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t3156_TI;
extern TypeInfo t3156_TI;
extern TypeInfo t3157_TI;
extern TypeInfo t556_TI;
static Il2CppRGCTXData t3156_RGCTXData[9] = 
{
	&t6753_0_0_0/* Type Usage */,
	&t556_0_0_0/* Type Usage */,
	&t3156_TI/* Class Usage */,
	&t3156_TI/* Static Usage */,
	&t3157_TI/* Class Usage */,
	&m17559_MI/* Method Usage */,
	&t556_TI/* Class Usage */,
	&m30287_MI/* Method Usage */,
	&m23089_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3156_0_0_0;
extern Il2CppType t3156_1_0_0;
struct t3156;
extern Il2CppGenericClass t3156_GC;
TypeInfo t3156_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t3156_MIs, t3156_PIs, t3156_FIs, NULL, &t29_TI, NULL, NULL, &t3156_TI, t3156_ITIs, t3156_VT, &EmptyCustomAttributesCache, &t3156_TI, &t3156_0_0_0, &t3156_1_0_0, t3156_IOs, &t3156_GC, NULL, NULL, NULL, t3156_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3156), 0, -1, sizeof(t3156_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<UnityEngine.Events.BaseInvokableCall>
extern Il2CppType t556_0_0_0;
extern Il2CppType t556_0_0_0;
static ParameterInfo t6754_m30288_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30288_GM;
MethodInfo m30288_MI = 
{
	"Equals", NULL, &t6754_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t6754_m30288_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30288_GM};
extern Il2CppType t556_0_0_0;
static ParameterInfo t6754_m30289_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30289_GM;
MethodInfo m30289_MI = 
{
	"GetHashCode", NULL, &t6754_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6754_m30289_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30289_GM};
static MethodInfo* t6754_MIs[] =
{
	&m30288_MI,
	&m30289_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6754_0_0_0;
extern Il2CppType t6754_1_0_0;
struct t6754;
extern Il2CppGenericClass t6754_GC;
TypeInfo t6754_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t6754_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6754_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6754_TI, &t6754_0_0_0, &t6754_1_0_0, NULL, &t6754_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IEquatable`1<UnityEngine.Events.BaseInvokableCall>
extern Il2CppType t556_0_0_0;
static ParameterInfo t6753_m30290_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30290_GM;
MethodInfo m30290_MI = 
{
	"Equals", NULL, &t6753_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6753_m30290_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30290_GM};
static MethodInfo* t6753_MIs[] =
{
	&m30290_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6753_1_0_0;
struct t6753;
extern Il2CppGenericClass t6753_GC;
TypeInfo t6753_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t6753_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6753_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6753_TI, &t6753_0_0_0, &t6753_1_0_0, NULL, &t6753_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m17554_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Events.BaseInvokableCall>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17559_GM;
MethodInfo m17559_MI = 
{
	".ctor", (methodPointerType)&m10738_gshared, &t3157_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17559_GM};
extern Il2CppType t556_0_0_0;
static ParameterInfo t3157_m17560_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17560_GM;
MethodInfo m17560_MI = 
{
	"GetHashCode", (methodPointerType)&m10739_gshared, &t3157_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3157_m17560_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17560_GM};
extern Il2CppType t556_0_0_0;
extern Il2CppType t556_0_0_0;
static ParameterInfo t3157_m17561_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17561_GM;
MethodInfo m17561_MI = 
{
	"Equals", (methodPointerType)&m10740_gshared, &t3157_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t3157_m17561_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17561_GM};
static MethodInfo* t3157_MIs[] =
{
	&m17559_MI,
	&m17560_MI,
	&m17561_MI,
	NULL
};
extern MethodInfo m17561_MI;
extern MethodInfo m17560_MI;
static MethodInfo* t3157_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m17561_MI,
	&m17560_MI,
	&m17557_MI,
	&m17556_MI,
	&m17560_MI,
	&m17561_MI,
};
static Il2CppInterfaceOffsetPair t3157_IOs[] = 
{
	{ &t6754_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t3156_TI;
extern TypeInfo t3156_TI;
extern TypeInfo t3157_TI;
extern TypeInfo t556_TI;
extern TypeInfo t556_TI;
static Il2CppRGCTXData t3157_RGCTXData[11] = 
{
	&t6753_0_0_0/* Type Usage */,
	&t556_0_0_0/* Type Usage */,
	&t3156_TI/* Class Usage */,
	&t3156_TI/* Static Usage */,
	&t3157_TI/* Class Usage */,
	&m17559_MI/* Method Usage */,
	&t556_TI/* Class Usage */,
	&m30287_MI/* Method Usage */,
	&m23089_MI/* Method Usage */,
	&m17554_MI/* Method Usage */,
	&t556_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3157_0_0_0;
extern Il2CppType t3157_1_0_0;
struct t3157;
extern Il2CppGenericClass t3157_GC;
extern TypeInfo t1256_TI;
TypeInfo t3157_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t3157_MIs, NULL, NULL, NULL, &t3156_TI, NULL, &t1256_TI, &t3157_TI, NULL, t3157_VT, &EmptyCustomAttributesCache, &t3157_TI, &t3157_0_0_0, &t3157_1_0_0, t3157_IOs, &t3157_GC, NULL, NULL, NULL, t3157_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3157), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#include "t662.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t662_TI;
#include "t662MD.h"

#include "t35.h"
#include "t67.h"


// Metadata Definition System.Predicate`1<UnityEngine.Events.BaseInvokableCall>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t662_m2992_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2992_GM;
MethodInfo m2992_MI = 
{
	".ctor", (methodPointerType)&m10741_gshared, &t662_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t662_m2992_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m2992_GM};
extern Il2CppType t556_0_0_0;
static ParameterInfo t662_m17562_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17562_GM;
MethodInfo m17562_MI = 
{
	"Invoke", (methodPointerType)&m10742_gshared, &t662_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t662_m17562_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17562_GM};
extern Il2CppType t556_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t662_m17563_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17563_GM;
MethodInfo m17563_MI = 
{
	"BeginInvoke", (methodPointerType)&m10743_gshared, &t662_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t662_m17563_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m17563_GM};
extern Il2CppType t66_0_0_0;
extern Il2CppType t66_0_0_0;
static ParameterInfo t662_m17564_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17564_GM;
MethodInfo m17564_MI = 
{
	"EndInvoke", (methodPointerType)&m10744_gshared, &t662_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t662_m17564_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17564_GM};
static MethodInfo* t662_MIs[] =
{
	&m2992_MI,
	&m17562_MI,
	&m17563_MI,
	&m17564_MI,
	NULL
};
extern MethodInfo m1699_MI;
extern MethodInfo m1700_MI;
extern MethodInfo m1701_MI;
extern MethodInfo m1702_MI;
extern MethodInfo m1703_MI;
extern MethodInfo m1704_MI;
extern MethodInfo m1705_MI;
extern MethodInfo m17562_MI;
extern MethodInfo m17563_MI;
extern MethodInfo m17564_MI;
static MethodInfo* t662_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m17562_MI,
	&m17563_MI,
	&m17564_MI,
};
extern TypeInfo t373_TI;
extern TypeInfo t374_TI;
static Il2CppInterfaceOffsetPair t662_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t662_0_0_0;
extern Il2CppType t662_1_0_0;
extern TypeInfo t195_TI;
struct t662;
extern Il2CppGenericClass t662_GC;
TypeInfo t662_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Predicate`1", "System", t662_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t662_TI, NULL, t662_VT, &EmptyCustomAttributesCache, &t662_TI, &t662_0_0_0, &t662_1_0_0, t662_IOs, &t662_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t662), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t3158.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3158_TI;
#include "t3158MD.h"

#include "t1247.h"
#include "t3159.h"
extern TypeInfo t4511_TI;
extern TypeInfo t1247_TI;
extern TypeInfo t3159_TI;
#include "t3159MD.h"
extern Il2CppType t4511_0_0_0;
extern Il2CppType t1247_0_0_0;
extern MethodInfo m17569_MI;
extern MethodInfo m30291_MI;
extern MethodInfo m8852_MI;


// Metadata Definition System.Collections.Generic.Comparer`1<UnityEngine.Events.BaseInvokableCall>
extern Il2CppType t3158_0_0_49;
FieldInfo t3158_f0_FieldInfo = 
{
	"_default", &t3158_0_0_49, &t3158_TI, offsetof(t3158_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3158_FIs[] =
{
	&t3158_f0_FieldInfo,
	NULL
};
extern MethodInfo m17568_MI;
static PropertyInfo t3158____Default_PropertyInfo = 
{
	&t3158_TI, "Default", &m17568_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3158_PIs[] =
{
	&t3158____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17565_GM;
MethodInfo m17565_MI = 
{
	".ctor", (methodPointerType)&m10745_gshared, &t3158_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17565_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17566_GM;
MethodInfo m17566_MI = 
{
	".cctor", (methodPointerType)&m10746_gshared, &t3158_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17566_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3158_m17567_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17567_GM;
MethodInfo m17567_MI = 
{
	"System.Collections.IComparer.Compare", (methodPointerType)&m10747_gshared, &t3158_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t3158_m17567_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17567_GM};
extern Il2CppType t556_0_0_0;
extern Il2CppType t556_0_0_0;
static ParameterInfo t3158_m30291_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30291_GM;
MethodInfo m30291_MI = 
{
	"Compare", NULL, &t3158_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t3158_m30291_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30291_GM};
extern Il2CppType t3158_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17568_GM;
MethodInfo m17568_MI = 
{
	"get_Default", (methodPointerType)&m10748_gshared, &t3158_TI, &t3158_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17568_GM};
static MethodInfo* t3158_MIs[] =
{
	&m17565_MI,
	&m17566_MI,
	&m17567_MI,
	&m30291_MI,
	&m17568_MI,
	NULL
};
extern MethodInfo m17567_MI;
static MethodInfo* t3158_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m30291_MI,
	&m17567_MI,
	NULL,
};
extern TypeInfo t4510_TI;
extern TypeInfo t726_TI;
static TypeInfo* t3158_ITIs[] = 
{
	&t4510_TI,
	&t726_TI,
};
static Il2CppInterfaceOffsetPair t3158_IOs[] = 
{
	{ &t4510_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t3158_TI;
extern TypeInfo t3158_TI;
extern TypeInfo t3159_TI;
extern TypeInfo t556_TI;
static Il2CppRGCTXData t3158_RGCTXData[8] = 
{
	&t4511_0_0_0/* Type Usage */,
	&t556_0_0_0/* Type Usage */,
	&t3158_TI/* Class Usage */,
	&t3158_TI/* Static Usage */,
	&t3159_TI/* Class Usage */,
	&m17569_MI/* Method Usage */,
	&t556_TI/* Class Usage */,
	&m30291_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3158_0_0_0;
extern Il2CppType t3158_1_0_0;
struct t3158;
extern Il2CppGenericClass t3158_GC;
TypeInfo t3158_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparer`1", "System.Collections.Generic", t3158_MIs, t3158_PIs, t3158_FIs, NULL, &t29_TI, NULL, NULL, &t3158_TI, t3158_ITIs, t3158_VT, &EmptyCustomAttributesCache, &t3158_TI, &t3158_0_0_0, &t3158_1_0_0, t3158_IOs, &t3158_GC, NULL, NULL, NULL, t3158_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3158), 0, -1, sizeof(t3158_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 5, 1, 1, 0, 0, 7, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IComparer`1<UnityEngine.Events.BaseInvokableCall>
extern Il2CppType t556_0_0_0;
extern Il2CppType t556_0_0_0;
static ParameterInfo t4510_m23097_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m23097_GM;
MethodInfo m23097_MI = 
{
	"Compare", NULL, &t4510_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t4510_m23097_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m23097_GM};
static MethodInfo* t4510_MIs[] =
{
	&m23097_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4510_0_0_0;
extern Il2CppType t4510_1_0_0;
struct t4510;
extern Il2CppGenericClass t4510_GC;
TypeInfo t4510_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparer`1", "System.Collections.Generic", t4510_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4510_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4510_TI, &t4510_0_0_0, &t4510_1_0_0, NULL, &t4510_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IComparable`1<UnityEngine.Events.BaseInvokableCall>
extern Il2CppType t556_0_0_0;
static ParameterInfo t4511_m23098_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m23098_GM;
MethodInfo m23098_MI = 
{
	"CompareTo", NULL, &t4511_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t4511_m23098_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m23098_GM};
static MethodInfo* t4511_MIs[] =
{
	&m23098_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4511_1_0_0;
struct t4511;
extern Il2CppGenericClass t4511_GC;
TypeInfo t4511_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t4511_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4511_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4511_TI, &t4511_0_0_0, &t4511_1_0_0, NULL, &t4511_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo t290_TI;
extern MethodInfo m17565_MI;
extern MethodInfo m23098_MI;
extern MethodInfo m9672_MI;


// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Events.BaseInvokableCall>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17569_GM;
MethodInfo m17569_MI = 
{
	".ctor", (methodPointerType)&m10749_gshared, &t3159_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17569_GM};
extern Il2CppType t556_0_0_0;
extern Il2CppType t556_0_0_0;
static ParameterInfo t3159_m17570_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17570_GM;
MethodInfo m17570_MI = 
{
	"Compare", (methodPointerType)&m10750_gshared, &t3159_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t3159_m17570_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17570_GM};
static MethodInfo* t3159_MIs[] =
{
	&m17569_MI,
	&m17570_MI,
	NULL
};
extern MethodInfo m17570_MI;
static MethodInfo* t3159_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m17570_MI,
	&m17567_MI,
	&m17570_MI,
};
static Il2CppInterfaceOffsetPair t3159_IOs[] = 
{
	{ &t4510_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t3158_TI;
extern TypeInfo t3158_TI;
extern TypeInfo t3159_TI;
extern TypeInfo t556_TI;
extern TypeInfo t556_TI;
extern TypeInfo t4511_TI;
static Il2CppRGCTXData t3159_RGCTXData[12] = 
{
	&t4511_0_0_0/* Type Usage */,
	&t556_0_0_0/* Type Usage */,
	&t3158_TI/* Class Usage */,
	&t3158_TI/* Static Usage */,
	&t3159_TI/* Class Usage */,
	&m17569_MI/* Method Usage */,
	&t556_TI/* Class Usage */,
	&m30291_MI/* Method Usage */,
	&m17565_MI/* Method Usage */,
	&t556_TI/* Class Usage */,
	&t4511_TI/* Class Usage */,
	&m23098_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3159_0_0_0;
extern Il2CppType t3159_1_0_0;
struct t3159;
extern Il2CppGenericClass t3159_GC;
extern TypeInfo t1246_TI;
TypeInfo t3159_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t3159_MIs, NULL, NULL, NULL, &t3158_TI, NULL, &t1246_TI, &t3159_TI, NULL, t3159_VT, &EmptyCustomAttributesCache, &t3159_TI, &t3159_0_0_0, &t3159_1_0_0, t3159_IOs, &t3159_GC, NULL, NULL, NULL, t3159_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3159), 0, -1, 0, 0, -1, 1048835, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 7, 0, 2};
#include "t3151.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3151_TI;
#include "t3151MD.h"



// Metadata Definition System.Comparison`1<UnityEngine.Events.BaseInvokableCall>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t3151_m17571_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17571_GM;
MethodInfo m17571_MI = 
{
	".ctor", (methodPointerType)&m10796_gshared, &t3151_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t3151_m17571_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17571_GM};
extern Il2CppType t556_0_0_0;
extern Il2CppType t556_0_0_0;
static ParameterInfo t3151_m17572_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17572_GM;
MethodInfo m17572_MI = 
{
	"Invoke", (methodPointerType)&m10797_gshared, &t3151_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t3151_m17572_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17572_GM};
extern Il2CppType t556_0_0_0;
extern Il2CppType t556_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3151_m17573_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t556_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17573_GM;
MethodInfo m17573_MI = 
{
	"BeginInvoke", (methodPointerType)&m10798_gshared, &t3151_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t3151_m17573_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m17573_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t3151_m17574_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17574_GM;
MethodInfo m17574_MI = 
{
	"EndInvoke", (methodPointerType)&m10799_gshared, &t3151_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3151_m17574_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17574_GM};
static MethodInfo* t3151_MIs[] =
{
	&m17571_MI,
	&m17572_MI,
	&m17573_MI,
	&m17574_MI,
	NULL
};
extern MethodInfo m17572_MI;
extern MethodInfo m17573_MI;
extern MethodInfo m17574_MI;
static MethodInfo* t3151_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m17572_MI,
	&m17573_MI,
	&m17574_MI,
};
static Il2CppInterfaceOffsetPair t3151_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3151_0_0_0;
extern Il2CppType t3151_1_0_0;
struct t3151;
extern Il2CppGenericClass t3151_GC;
TypeInfo t3151_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparison`1", "System", t3151_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t3151_TI, NULL, t3151_VT, &EmptyCustomAttributesCache, &t3151_TI, &t3151_0_0_0, &t3151_1_0_0, t3151_IOs, &t3151_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3151), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3196_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Boolean>
extern MethodInfo m30292_MI;
static PropertyInfo t3196____Current_PropertyInfo = 
{
	&t3196_TI, "Current", &m30292_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3196_PIs[] =
{
	&t3196____Current_PropertyInfo,
	NULL
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30292_GM;
MethodInfo m30292_MI = 
{
	"get_Current", NULL, &t3196_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30292_GM};
static MethodInfo* t3196_MIs[] =
{
	&m30292_MI,
	NULL
};
static TypeInfo* t3196_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3196_0_0_0;
extern Il2CppType t3196_1_0_0;
struct t3196;
extern Il2CppGenericClass t3196_GC;
TypeInfo t3196_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t3196_MIs, t3196_PIs, NULL, NULL, NULL, NULL, NULL, &t3196_TI, t3196_ITIs, NULL, &EmptyCustomAttributesCache, &t3196_TI, &t3196_0_0_0, &t3196_1_0_0, NULL, &t3196_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3160.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3160_TI;
#include "t3160MD.h"

extern MethodInfo m17579_MI;
extern MethodInfo m23103_MI;
struct t20;
 bool m23103 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17575_MI;
 void m17575 (t3160 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17576_MI;
 t29 * m17576 (t3160 * __this, MethodInfo* method){
	{
		bool L_0 = m17579(__this, &m17579_MI);
		bool L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t40_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17577_MI;
 void m17577 (t3160 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17578_MI;
 bool m17578 (t3160 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 bool m17579 (t3160 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		bool L_8 = m23103(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23103_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Boolean>
extern Il2CppType t20_0_0_1;
FieldInfo t3160_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3160_TI, offsetof(t3160, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3160_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3160_TI, offsetof(t3160, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3160_FIs[] =
{
	&t3160_f0_FieldInfo,
	&t3160_f1_FieldInfo,
	NULL
};
static PropertyInfo t3160____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3160_TI, "System.Collections.IEnumerator.Current", &m17576_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3160____Current_PropertyInfo = 
{
	&t3160_TI, "Current", &m17579_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3160_PIs[] =
{
	&t3160____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3160____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3160_m17575_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17575_GM;
MethodInfo m17575_MI = 
{
	".ctor", (methodPointerType)&m17575, &t3160_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3160_m17575_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17575_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17576_GM;
MethodInfo m17576_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17576, &t3160_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17576_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17577_GM;
MethodInfo m17577_MI = 
{
	"Dispose", (methodPointerType)&m17577, &t3160_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17577_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17578_GM;
MethodInfo m17578_MI = 
{
	"MoveNext", (methodPointerType)&m17578, &t3160_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17578_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17579_GM;
MethodInfo m17579_MI = 
{
	"get_Current", (methodPointerType)&m17579, &t3160_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17579_GM};
static MethodInfo* t3160_MIs[] =
{
	&m17575_MI,
	&m17576_MI,
	&m17577_MI,
	&m17578_MI,
	&m17579_MI,
	NULL
};
static MethodInfo* t3160_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17576_MI,
	&m17578_MI,
	&m17577_MI,
	&m17579_MI,
};
static TypeInfo* t3160_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3196_TI,
};
static Il2CppInterfaceOffsetPair t3160_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3196_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3160_0_0_0;
extern Il2CppType t3160_1_0_0;
extern Il2CppGenericClass t3160_GC;
TypeInfo t3160_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3160_MIs, t3160_PIs, t3160_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3160_TI, t3160_ITIs, t3160_VT, &EmptyCustomAttributesCache, &t3160_TI, &t3160_0_0_0, &t3160_1_0_0, t3160_IOs, &t3160_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3160)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5790_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Boolean>
extern MethodInfo m30293_MI;
static PropertyInfo t5790____Count_PropertyInfo = 
{
	&t5790_TI, "Count", &m30293_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30294_MI;
static PropertyInfo t5790____IsReadOnly_PropertyInfo = 
{
	&t5790_TI, "IsReadOnly", &m30294_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5790_PIs[] =
{
	&t5790____Count_PropertyInfo,
	&t5790____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30293_GM;
MethodInfo m30293_MI = 
{
	"get_Count", NULL, &t5790_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30293_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30294_GM;
MethodInfo m30294_MI = 
{
	"get_IsReadOnly", NULL, &t5790_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30294_GM};
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t5790_m30295_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30295_GM;
MethodInfo m30295_MI = 
{
	"Add", NULL, &t5790_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t5790_m30295_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30295_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30296_GM;
MethodInfo m30296_MI = 
{
	"Clear", NULL, &t5790_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30296_GM};
extern Il2CppType t40_0_0_0;
static ParameterInfo t5790_m30297_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30297_GM;
MethodInfo m30297_MI = 
{
	"Contains", NULL, &t5790_TI, &t40_0_0_0, RuntimeInvoker_t40_t297, t5790_m30297_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30297_GM};
extern Il2CppType t771_0_0_0;
extern Il2CppType t771_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5790_m30298_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t771_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30298_GM;
MethodInfo m30298_MI = 
{
	"CopyTo", NULL, &t5790_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5790_m30298_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30298_GM};
extern Il2CppType t40_0_0_0;
static ParameterInfo t5790_m30299_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30299_GM;
MethodInfo m30299_MI = 
{
	"Remove", NULL, &t5790_TI, &t40_0_0_0, RuntimeInvoker_t40_t297, t5790_m30299_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30299_GM};
static MethodInfo* t5790_MIs[] =
{
	&m30293_MI,
	&m30294_MI,
	&m30295_MI,
	&m30296_MI,
	&m30297_MI,
	&m30298_MI,
	&m30299_MI,
	NULL
};
extern TypeInfo t5792_TI;
static TypeInfo* t5790_ITIs[] = 
{
	&t603_TI,
	&t5792_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5790_0_0_0;
extern Il2CppType t5790_1_0_0;
struct t5790;
extern Il2CppGenericClass t5790_GC;
TypeInfo t5790_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5790_MIs, t5790_PIs, NULL, NULL, NULL, NULL, NULL, &t5790_TI, t5790_ITIs, NULL, &EmptyCustomAttributesCache, &t5790_TI, &t5790_0_0_0, &t5790_1_0_0, NULL, &t5790_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Boolean>
extern Il2CppType t3196_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30300_GM;
MethodInfo m30300_MI = 
{
	"GetEnumerator", NULL, &t5792_TI, &t3196_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30300_GM};
static MethodInfo* t5792_MIs[] =
{
	&m30300_MI,
	NULL
};
static TypeInfo* t5792_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5792_0_0_0;
extern Il2CppType t5792_1_0_0;
struct t5792;
extern Il2CppGenericClass t5792_GC;
TypeInfo t5792_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5792_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5792_TI, t5792_ITIs, NULL, &EmptyCustomAttributesCache, &t5792_TI, &t5792_0_0_0, &t5792_1_0_0, NULL, &t5792_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5791_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Boolean>
extern MethodInfo m30301_MI;
extern MethodInfo m30302_MI;
static PropertyInfo t5791____Item_PropertyInfo = 
{
	&t5791_TI, "Item", &m30301_MI, &m30302_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5791_PIs[] =
{
	&t5791____Item_PropertyInfo,
	NULL
};
extern Il2CppType t40_0_0_0;
static ParameterInfo t5791_m30303_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30303_GM;
MethodInfo m30303_MI = 
{
	"IndexOf", NULL, &t5791_TI, &t44_0_0_0, RuntimeInvoker_t44_t297, t5791_m30303_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30303_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t5791_m30304_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30304_GM;
MethodInfo m30304_MI = 
{
	"Insert", NULL, &t5791_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t297, t5791_m30304_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30304_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5791_m30305_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30305_GM;
MethodInfo m30305_MI = 
{
	"RemoveAt", NULL, &t5791_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5791_m30305_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30305_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5791_m30301_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30301_GM;
MethodInfo m30301_MI = 
{
	"get_Item", NULL, &t5791_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5791_m30301_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30301_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t5791_m30302_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30302_GM;
MethodInfo m30302_MI = 
{
	"set_Item", NULL, &t5791_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t297, t5791_m30302_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30302_GM};
static MethodInfo* t5791_MIs[] =
{
	&m30303_MI,
	&m30304_MI,
	&m30305_MI,
	&m30301_MI,
	&m30302_MI,
	NULL
};
static TypeInfo* t5791_ITIs[] = 
{
	&t603_TI,
	&t5790_TI,
	&t5792_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5791_0_0_0;
extern Il2CppType t5791_1_0_0;
struct t5791;
extern Il2CppGenericClass t5791_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5791_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5791_MIs, t5791_PIs, NULL, NULL, NULL, NULL, NULL, &t5791_TI, t5791_ITIs, NULL, &t1908__CustomAttributeCache, &t5791_TI, &t5791_0_0_0, &t5791_1_0_0, NULL, &t5791_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5793_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IComparable`1<System.Boolean>>
extern MethodInfo m30306_MI;
static PropertyInfo t5793____Count_PropertyInfo = 
{
	&t5793_TI, "Count", &m30306_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30307_MI;
static PropertyInfo t5793____IsReadOnly_PropertyInfo = 
{
	&t5793_TI, "IsReadOnly", &m30307_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5793_PIs[] =
{
	&t5793____Count_PropertyInfo,
	&t5793____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30306_GM;
MethodInfo m30306_MI = 
{
	"get_Count", NULL, &t5793_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30306_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30307_GM;
MethodInfo m30307_MI = 
{
	"get_IsReadOnly", NULL, &t5793_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30307_GM};
extern Il2CppType t1759_0_0_0;
extern Il2CppType t1759_0_0_0;
static ParameterInfo t5793_m30308_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1759_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30308_GM;
MethodInfo m30308_MI = 
{
	"Add", NULL, &t5793_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5793_m30308_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30308_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30309_GM;
MethodInfo m30309_MI = 
{
	"Clear", NULL, &t5793_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30309_GM};
extern Il2CppType t1759_0_0_0;
static ParameterInfo t5793_m30310_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1759_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30310_GM;
MethodInfo m30310_MI = 
{
	"Contains", NULL, &t5793_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5793_m30310_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30310_GM};
extern Il2CppType t3548_0_0_0;
extern Il2CppType t3548_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5793_m30311_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3548_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30311_GM;
MethodInfo m30311_MI = 
{
	"CopyTo", NULL, &t5793_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5793_m30311_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30311_GM};
extern Il2CppType t1759_0_0_0;
static ParameterInfo t5793_m30312_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1759_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30312_GM;
MethodInfo m30312_MI = 
{
	"Remove", NULL, &t5793_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5793_m30312_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30312_GM};
static MethodInfo* t5793_MIs[] =
{
	&m30306_MI,
	&m30307_MI,
	&m30308_MI,
	&m30309_MI,
	&m30310_MI,
	&m30311_MI,
	&m30312_MI,
	NULL
};
extern TypeInfo t5795_TI;
static TypeInfo* t5793_ITIs[] = 
{
	&t603_TI,
	&t5795_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5793_0_0_0;
extern Il2CppType t5793_1_0_0;
struct t5793;
extern Il2CppGenericClass t5793_GC;
TypeInfo t5793_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5793_MIs, t5793_PIs, NULL, NULL, NULL, NULL, NULL, &t5793_TI, t5793_ITIs, NULL, &EmptyCustomAttributesCache, &t5793_TI, &t5793_0_0_0, &t5793_1_0_0, NULL, &t5793_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.Boolean>>
extern Il2CppType t4513_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30313_GM;
MethodInfo m30313_MI = 
{
	"GetEnumerator", NULL, &t5795_TI, &t4513_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30313_GM};
static MethodInfo* t5795_MIs[] =
{
	&m30313_MI,
	NULL
};
static TypeInfo* t5795_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5795_0_0_0;
extern Il2CppType t5795_1_0_0;
struct t5795;
extern Il2CppGenericClass t5795_GC;
TypeInfo t5795_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5795_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5795_TI, t5795_ITIs, NULL, &EmptyCustomAttributesCache, &t5795_TI, &t5795_0_0_0, &t5795_1_0_0, NULL, &t5795_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4513_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.Boolean>>
extern MethodInfo m30314_MI;
static PropertyInfo t4513____Current_PropertyInfo = 
{
	&t4513_TI, "Current", &m30314_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4513_PIs[] =
{
	&t4513____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1759_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30314_GM;
MethodInfo m30314_MI = 
{
	"get_Current", NULL, &t4513_TI, &t1759_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30314_GM};
static MethodInfo* t4513_MIs[] =
{
	&m30314_MI,
	NULL
};
static TypeInfo* t4513_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4513_0_0_0;
extern Il2CppType t4513_1_0_0;
struct t4513;
extern Il2CppGenericClass t4513_GC;
TypeInfo t4513_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4513_MIs, t4513_PIs, NULL, NULL, NULL, NULL, NULL, &t4513_TI, t4513_ITIs, NULL, &EmptyCustomAttributesCache, &t4513_TI, &t4513_0_0_0, &t4513_1_0_0, NULL, &t4513_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1759_TI;



// Metadata Definition System.IComparable`1<System.Boolean>
extern Il2CppType t40_0_0_0;
static ParameterInfo t1759_m30315_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30315_GM;
MethodInfo m30315_MI = 
{
	"CompareTo", NULL, &t1759_TI, &t44_0_0_0, RuntimeInvoker_t44_t297, t1759_m30315_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30315_GM};
static MethodInfo* t1759_MIs[] =
{
	&m30315_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1759_1_0_0;
struct t1759;
extern Il2CppGenericClass t1759_GC;
TypeInfo t1759_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t1759_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1759_TI, NULL, NULL, &EmptyCustomAttributesCache, &t1759_TI, &t1759_0_0_0, &t1759_1_0_0, NULL, &t1759_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t3161.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3161_TI;
#include "t3161MD.h"

extern MethodInfo m17584_MI;
extern MethodInfo m23114_MI;
struct t20;
#define m23114(__this, p0, method) (t29*)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.IComparable`1<System.Boolean>>
extern Il2CppType t20_0_0_1;
FieldInfo t3161_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3161_TI, offsetof(t3161, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3161_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3161_TI, offsetof(t3161, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3161_FIs[] =
{
	&t3161_f0_FieldInfo,
	&t3161_f1_FieldInfo,
	NULL
};
extern MethodInfo m17581_MI;
static PropertyInfo t3161____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3161_TI, "System.Collections.IEnumerator.Current", &m17581_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3161____Current_PropertyInfo = 
{
	&t3161_TI, "Current", &m17584_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3161_PIs[] =
{
	&t3161____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3161____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3161_m17580_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17580_GM;
MethodInfo m17580_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3161_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3161_m17580_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17580_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17581_GM;
MethodInfo m17581_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3161_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17581_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17582_GM;
MethodInfo m17582_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3161_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17582_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17583_GM;
MethodInfo m17583_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3161_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17583_GM};
extern Il2CppType t1759_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17584_GM;
MethodInfo m17584_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3161_TI, &t1759_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17584_GM};
static MethodInfo* t3161_MIs[] =
{
	&m17580_MI,
	&m17581_MI,
	&m17582_MI,
	&m17583_MI,
	&m17584_MI,
	NULL
};
extern MethodInfo m17583_MI;
extern MethodInfo m17582_MI;
static MethodInfo* t3161_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17581_MI,
	&m17583_MI,
	&m17582_MI,
	&m17584_MI,
};
static TypeInfo* t3161_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4513_TI,
};
static Il2CppInterfaceOffsetPair t3161_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4513_TI, 7},
};
extern TypeInfo t1759_TI;
static Il2CppRGCTXData t3161_RGCTXData[3] = 
{
	&m17584_MI/* Method Usage */,
	&t1759_TI/* Class Usage */,
	&m23114_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3161_0_0_0;
extern Il2CppType t3161_1_0_0;
extern Il2CppGenericClass t3161_GC;
TypeInfo t3161_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3161_MIs, t3161_PIs, t3161_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3161_TI, t3161_ITIs, t3161_VT, &EmptyCustomAttributesCache, &t3161_TI, &t3161_0_0_0, &t3161_1_0_0, t3161_IOs, &t3161_GC, NULL, NULL, NULL, t3161_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3161)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5794_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IComparable`1<System.Boolean>>
extern MethodInfo m30316_MI;
extern MethodInfo m30317_MI;
static PropertyInfo t5794____Item_PropertyInfo = 
{
	&t5794_TI, "Item", &m30316_MI, &m30317_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5794_PIs[] =
{
	&t5794____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1759_0_0_0;
static ParameterInfo t5794_m30318_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1759_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30318_GM;
MethodInfo m30318_MI = 
{
	"IndexOf", NULL, &t5794_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5794_m30318_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30318_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1759_0_0_0;
static ParameterInfo t5794_m30319_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1759_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30319_GM;
MethodInfo m30319_MI = 
{
	"Insert", NULL, &t5794_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5794_m30319_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30319_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5794_m30320_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30320_GM;
MethodInfo m30320_MI = 
{
	"RemoveAt", NULL, &t5794_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5794_m30320_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30320_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5794_m30316_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1759_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30316_GM;
MethodInfo m30316_MI = 
{
	"get_Item", NULL, &t5794_TI, &t1759_0_0_0, RuntimeInvoker_t29_t44, t5794_m30316_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30316_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1759_0_0_0;
static ParameterInfo t5794_m30317_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1759_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30317_GM;
MethodInfo m30317_MI = 
{
	"set_Item", NULL, &t5794_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5794_m30317_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30317_GM};
static MethodInfo* t5794_MIs[] =
{
	&m30318_MI,
	&m30319_MI,
	&m30320_MI,
	&m30316_MI,
	&m30317_MI,
	NULL
};
static TypeInfo* t5794_ITIs[] = 
{
	&t603_TI,
	&t5793_TI,
	&t5795_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5794_0_0_0;
extern Il2CppType t5794_1_0_0;
struct t5794;
extern Il2CppGenericClass t5794_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5794_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5794_MIs, t5794_PIs, NULL, NULL, NULL, NULL, NULL, &t5794_TI, t5794_ITIs, NULL, &t1908__CustomAttributeCache, &t5794_TI, &t5794_0_0_0, &t5794_1_0_0, NULL, &t5794_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5796_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Boolean>>
extern MethodInfo m30321_MI;
static PropertyInfo t5796____Count_PropertyInfo = 
{
	&t5796_TI, "Count", &m30321_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30322_MI;
static PropertyInfo t5796____IsReadOnly_PropertyInfo = 
{
	&t5796_TI, "IsReadOnly", &m30322_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5796_PIs[] =
{
	&t5796____Count_PropertyInfo,
	&t5796____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30321_GM;
MethodInfo m30321_MI = 
{
	"get_Count", NULL, &t5796_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30321_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30322_GM;
MethodInfo m30322_MI = 
{
	"get_IsReadOnly", NULL, &t5796_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30322_GM};
extern Il2CppType t1760_0_0_0;
extern Il2CppType t1760_0_0_0;
static ParameterInfo t5796_m30323_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1760_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30323_GM;
MethodInfo m30323_MI = 
{
	"Add", NULL, &t5796_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5796_m30323_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30323_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30324_GM;
MethodInfo m30324_MI = 
{
	"Clear", NULL, &t5796_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30324_GM};
extern Il2CppType t1760_0_0_0;
static ParameterInfo t5796_m30325_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1760_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30325_GM;
MethodInfo m30325_MI = 
{
	"Contains", NULL, &t5796_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5796_m30325_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30325_GM};
extern Il2CppType t3549_0_0_0;
extern Il2CppType t3549_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5796_m30326_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3549_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30326_GM;
MethodInfo m30326_MI = 
{
	"CopyTo", NULL, &t5796_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5796_m30326_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30326_GM};
extern Il2CppType t1760_0_0_0;
static ParameterInfo t5796_m30327_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1760_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30327_GM;
MethodInfo m30327_MI = 
{
	"Remove", NULL, &t5796_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5796_m30327_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30327_GM};
static MethodInfo* t5796_MIs[] =
{
	&m30321_MI,
	&m30322_MI,
	&m30323_MI,
	&m30324_MI,
	&m30325_MI,
	&m30326_MI,
	&m30327_MI,
	NULL
};
extern TypeInfo t5798_TI;
static TypeInfo* t5796_ITIs[] = 
{
	&t603_TI,
	&t5798_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5796_0_0_0;
extern Il2CppType t5796_1_0_0;
struct t5796;
extern Il2CppGenericClass t5796_GC;
TypeInfo t5796_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5796_MIs, t5796_PIs, NULL, NULL, NULL, NULL, NULL, &t5796_TI, t5796_ITIs, NULL, &EmptyCustomAttributesCache, &t5796_TI, &t5796_0_0_0, &t5796_1_0_0, NULL, &t5796_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.Boolean>>
extern Il2CppType t4515_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30328_GM;
MethodInfo m30328_MI = 
{
	"GetEnumerator", NULL, &t5798_TI, &t4515_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30328_GM};
static MethodInfo* t5798_MIs[] =
{
	&m30328_MI,
	NULL
};
static TypeInfo* t5798_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5798_0_0_0;
extern Il2CppType t5798_1_0_0;
struct t5798;
extern Il2CppGenericClass t5798_GC;
TypeInfo t5798_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5798_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5798_TI, t5798_ITIs, NULL, &EmptyCustomAttributesCache, &t5798_TI, &t5798_0_0_0, &t5798_1_0_0, NULL, &t5798_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4515_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IEquatable`1<System.Boolean>>
extern MethodInfo m30329_MI;
static PropertyInfo t4515____Current_PropertyInfo = 
{
	&t4515_TI, "Current", &m30329_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4515_PIs[] =
{
	&t4515____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1760_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30329_GM;
MethodInfo m30329_MI = 
{
	"get_Current", NULL, &t4515_TI, &t1760_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30329_GM};
static MethodInfo* t4515_MIs[] =
{
	&m30329_MI,
	NULL
};
static TypeInfo* t4515_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4515_0_0_0;
extern Il2CppType t4515_1_0_0;
struct t4515;
extern Il2CppGenericClass t4515_GC;
TypeInfo t4515_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4515_MIs, t4515_PIs, NULL, NULL, NULL, NULL, NULL, &t4515_TI, t4515_ITIs, NULL, &EmptyCustomAttributesCache, &t4515_TI, &t4515_0_0_0, &t4515_1_0_0, NULL, &t4515_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1760_TI;



// Metadata Definition System.IEquatable`1<System.Boolean>
extern Il2CppType t40_0_0_0;
static ParameterInfo t1760_m30330_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t297 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30330_GM;
MethodInfo m30330_MI = 
{
	"Equals", NULL, &t1760_TI, &t40_0_0_0, RuntimeInvoker_t40_t297, t1760_m30330_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30330_GM};
static MethodInfo* t1760_MIs[] =
{
	&m30330_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1760_1_0_0;
struct t1760;
extern Il2CppGenericClass t1760_GC;
TypeInfo t1760_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t1760_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1760_TI, NULL, NULL, &EmptyCustomAttributesCache, &t1760_TI, &t1760_0_0_0, &t1760_1_0_0, NULL, &t1760_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t3162.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3162_TI;
#include "t3162MD.h"

extern MethodInfo m17589_MI;
extern MethodInfo m23125_MI;
struct t20;
#define m23125(__this, p0, method) (t29*)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.IEquatable`1<System.Boolean>>
extern Il2CppType t20_0_0_1;
FieldInfo t3162_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3162_TI, offsetof(t3162, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3162_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3162_TI, offsetof(t3162, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3162_FIs[] =
{
	&t3162_f0_FieldInfo,
	&t3162_f1_FieldInfo,
	NULL
};
extern MethodInfo m17586_MI;
static PropertyInfo t3162____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3162_TI, "System.Collections.IEnumerator.Current", &m17586_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3162____Current_PropertyInfo = 
{
	&t3162_TI, "Current", &m17589_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3162_PIs[] =
{
	&t3162____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3162____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3162_m17585_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17585_GM;
MethodInfo m17585_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3162_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3162_m17585_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17585_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17586_GM;
MethodInfo m17586_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3162_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17586_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17587_GM;
MethodInfo m17587_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3162_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17587_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17588_GM;
MethodInfo m17588_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3162_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17588_GM};
extern Il2CppType t1760_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17589_GM;
MethodInfo m17589_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3162_TI, &t1760_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17589_GM};
static MethodInfo* t3162_MIs[] =
{
	&m17585_MI,
	&m17586_MI,
	&m17587_MI,
	&m17588_MI,
	&m17589_MI,
	NULL
};
extern MethodInfo m17588_MI;
extern MethodInfo m17587_MI;
static MethodInfo* t3162_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17586_MI,
	&m17588_MI,
	&m17587_MI,
	&m17589_MI,
};
static TypeInfo* t3162_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4515_TI,
};
static Il2CppInterfaceOffsetPair t3162_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4515_TI, 7},
};
extern TypeInfo t1760_TI;
static Il2CppRGCTXData t3162_RGCTXData[3] = 
{
	&m17589_MI/* Method Usage */,
	&t1760_TI/* Class Usage */,
	&m23125_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3162_0_0_0;
extern Il2CppType t3162_1_0_0;
extern Il2CppGenericClass t3162_GC;
TypeInfo t3162_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3162_MIs, t3162_PIs, t3162_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3162_TI, t3162_ITIs, t3162_VT, &EmptyCustomAttributesCache, &t3162_TI, &t3162_0_0_0, &t3162_1_0_0, t3162_IOs, &t3162_GC, NULL, NULL, NULL, t3162_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3162)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5797_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IEquatable`1<System.Boolean>>
extern MethodInfo m30331_MI;
extern MethodInfo m30332_MI;
static PropertyInfo t5797____Item_PropertyInfo = 
{
	&t5797_TI, "Item", &m30331_MI, &m30332_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5797_PIs[] =
{
	&t5797____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1760_0_0_0;
static ParameterInfo t5797_m30333_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1760_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30333_GM;
MethodInfo m30333_MI = 
{
	"IndexOf", NULL, &t5797_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5797_m30333_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30333_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1760_0_0_0;
static ParameterInfo t5797_m30334_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1760_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30334_GM;
MethodInfo m30334_MI = 
{
	"Insert", NULL, &t5797_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5797_m30334_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30334_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5797_m30335_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30335_GM;
MethodInfo m30335_MI = 
{
	"RemoveAt", NULL, &t5797_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5797_m30335_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30335_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5797_m30331_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1760_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30331_GM;
MethodInfo m30331_MI = 
{
	"get_Item", NULL, &t5797_TI, &t1760_0_0_0, RuntimeInvoker_t29_t44, t5797_m30331_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30331_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1760_0_0_0;
static ParameterInfo t5797_m30332_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1760_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30332_GM;
MethodInfo m30332_MI = 
{
	"set_Item", NULL, &t5797_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5797_m30332_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30332_GM};
static MethodInfo* t5797_MIs[] =
{
	&m30333_MI,
	&m30334_MI,
	&m30335_MI,
	&m30331_MI,
	&m30332_MI,
	NULL
};
static TypeInfo* t5797_ITIs[] = 
{
	&t603_TI,
	&t5796_TI,
	&t5798_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5797_0_0_0;
extern Il2CppType t5797_1_0_0;
struct t5797;
extern Il2CppGenericClass t5797_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5797_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5797_MIs, t5797_PIs, NULL, NULL, NULL, NULL, NULL, &t5797_TI, t5797_ITIs, NULL, &t1908__CustomAttributeCache, &t5797_TI, &t5797_0_0_0, &t5797_1_0_0, NULL, &t5797_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t3163.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3163_TI;
#include "t3163MD.h"

#include "t557.h"
#include "t3123.h"
extern TypeInfo t316_TI;
extern TypeInfo t3123_TI;
#include "t566MD.h"
#include "t3123MD.h"
extern MethodInfo m2812_MI;
extern MethodInfo m2820_MI;
extern MethodInfo m17254_MI;


extern MethodInfo m17590_MI;
 void m17590_gshared (t3163 * __this, MethodInfo* method)
{
	{
		__this->f4 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), 2));
		m2812(__this, &m2812_MI);
		return;
	}
}
extern MethodInfo m17591_MI;
 t557 * m17591_gshared (t3163 * __this, t7* p0, t29 * p1, MethodInfo* method)
{
	{
		t537* L_0 = ((t537*)SZArrayNew(InitializedTypeInfo(&t537_TI), 2));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_1 = m1554(NULL, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), &m1554_MI);
		ArrayElementTypeCheck (L_0, L_1);
		*((t42 **)(t42 **)SZArrayLdElema(L_0, 0)) = (t42 *)L_1;
		t537* L_2 = L_0;
		t42 * L_3 = m1554(NULL, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), &m1554_MI);
		ArrayElementTypeCheck (L_2, L_3);
		*((t42 **)(t42 **)SZArrayLdElema(L_2, 1)) = (t42 *)L_3;
		t557 * L_4 = m2820(NULL, p1, p0, L_2, &m2820_MI);
		return L_4;
	}
}
extern MethodInfo m17592_MI;
 t556 * m17592_gshared (t3163 * __this, t29 * p0, t557 * p1, MethodInfo* method)
{
	{
		t3123 * L_0 = (t3123 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		(( void (*) (t3123 * __this, t29 * p0, t557 * p1, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_0, p0, p1, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_0;
	}
}
// Metadata Definition UnityEngine.Events.UnityEvent`2<System.Object,System.Object>
extern Il2CppType t316_0_0_33;
FieldInfo t3163_f4_FieldInfo = 
{
	"m_InvokeArray", &t316_0_0_33, &t3163_TI, offsetof(t3163, f4), &EmptyCustomAttributesCache};
static FieldInfo* t3163_FIs[] =
{
	&t3163_f4_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17590_GM;
MethodInfo m17590_MI = 
{
	".ctor", (methodPointerType)&m17590_gshared, &t3163_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17590_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t7_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3163_m17591_ParameterInfos[] = 
{
	{"name", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"targetObj", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t557_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17591_GM;
MethodInfo m17591_MI = 
{
	"FindMethod_Impl", (methodPointerType)&m17591_gshared, &t3163_TI, &t557_0_0_0, RuntimeInvoker_t29_t29_t29, t3163_m17591_ParameterInfos, &EmptyCustomAttributesCache, 196, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17591_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t3163_m17592_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t556_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17592_GM;
MethodInfo m17592_MI = 
{
	"GetDelegate", (methodPointerType)&m17592_gshared, &t3163_TI, &t556_0_0_0, RuntimeInvoker_t29_t29_t29, t3163_m17592_ParameterInfos, &EmptyCustomAttributesCache, 195, 0, 7, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17592_GM};
static MethodInfo* t3163_MIs[] =
{
	&m17590_MI,
	&m17591_MI,
	&m17592_MI,
	NULL
};
extern MethodInfo m1323_MI;
extern MethodInfo m1324_MI;
extern MethodInfo m1325_MI;
static MethodInfo* t3163_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1323_MI,
	&m1324_MI,
	&m1325_MI,
	&m17591_MI,
	&m17592_MI,
};
extern TypeInfo t301_TI;
static Il2CppInterfaceOffsetPair t3163_IOs[] = 
{
	{ &t301_TI, 4},
};
extern TypeInfo t3123_TI;
static Il2CppRGCTXData t3163_RGCTXData[4] = 
{
	&t29_0_0_0/* Type Usage */,
	&t29_0_0_0/* Type Usage */,
	&t3123_TI/* Class Usage */,
	&m17254_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3163_0_0_0;
extern Il2CppType t3163_1_0_0;
extern TypeInfo t566_TI;
struct t3163;
extern Il2CppGenericClass t3163_GC;
TypeInfo t3163_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityEvent`2", "UnityEngine.Events", t3163_MIs, NULL, t3163_FIs, NULL, &t566_TI, NULL, NULL, &t3163_TI, NULL, t3163_VT, &EmptyCustomAttributesCache, &t3163_TI, &t3163_0_0_0, &t3163_1_0_0, t3163_IOs, &t3163_GC, NULL, NULL, NULL, t3163_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3163), 0, -1, 0, 0, -1, 1056897, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 1, 0, 0, 8, 0, 1};
#include "t3164.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3164_TI;
#include "t3164MD.h"

#include "t3125.h"
extern TypeInfo t3125_TI;
#include "t3125MD.h"
extern MethodInfo m17261_MI;


extern MethodInfo m17593_MI;
 void m17593_gshared (t3164 * __this, MethodInfo* method)
{
	{
		__this->f4 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), 3));
		m2812(__this, &m2812_MI);
		return;
	}
}
extern MethodInfo m17594_MI;
 t557 * m17594_gshared (t3164 * __this, t7* p0, t29 * p1, MethodInfo* method)
{
	{
		t537* L_0 = ((t537*)SZArrayNew(InitializedTypeInfo(&t537_TI), 3));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_1 = m1554(NULL, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), &m1554_MI);
		ArrayElementTypeCheck (L_0, L_1);
		*((t42 **)(t42 **)SZArrayLdElema(L_0, 0)) = (t42 *)L_1;
		t537* L_2 = L_0;
		t42 * L_3 = m1554(NULL, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), &m1554_MI);
		ArrayElementTypeCheck (L_2, L_3);
		*((t42 **)(t42 **)SZArrayLdElema(L_2, 1)) = (t42 *)L_3;
		t537* L_4 = L_2;
		t42 * L_5 = m1554(NULL, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), &m1554_MI);
		ArrayElementTypeCheck (L_4, L_5);
		*((t42 **)(t42 **)SZArrayLdElema(L_4, 2)) = (t42 *)L_5;
		t557 * L_6 = m2820(NULL, p1, p0, L_4, &m2820_MI);
		return L_6;
	}
}
extern MethodInfo m17595_MI;
 t556 * m17595_gshared (t3164 * __this, t29 * p0, t557 * p1, MethodInfo* method)
{
	{
		t3125 * L_0 = (t3125 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		(( void (*) (t3125 * __this, t29 * p0, t557 * p1, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_0, p0, p1, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_0;
	}
}
// Metadata Definition UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>
extern Il2CppType t316_0_0_33;
FieldInfo t3164_f4_FieldInfo = 
{
	"m_InvokeArray", &t316_0_0_33, &t3164_TI, offsetof(t3164, f4), &EmptyCustomAttributesCache};
static FieldInfo* t3164_FIs[] =
{
	&t3164_f4_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17593_GM;
MethodInfo m17593_MI = 
{
	".ctor", (methodPointerType)&m17593_gshared, &t3164_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17593_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3164_m17594_ParameterInfos[] = 
{
	{"name", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"targetObj", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t557_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17594_GM;
MethodInfo m17594_MI = 
{
	"FindMethod_Impl", (methodPointerType)&m17594_gshared, &t3164_TI, &t557_0_0_0, RuntimeInvoker_t29_t29_t29, t3164_m17594_ParameterInfos, &EmptyCustomAttributesCache, 196, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17594_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t3164_m17595_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t556_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17595_GM;
MethodInfo m17595_MI = 
{
	"GetDelegate", (methodPointerType)&m17595_gshared, &t3164_TI, &t556_0_0_0, RuntimeInvoker_t29_t29_t29, t3164_m17595_ParameterInfos, &EmptyCustomAttributesCache, 195, 0, 7, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17595_GM};
static MethodInfo* t3164_MIs[] =
{
	&m17593_MI,
	&m17594_MI,
	&m17595_MI,
	NULL
};
static MethodInfo* t3164_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1323_MI,
	&m1324_MI,
	&m1325_MI,
	&m17594_MI,
	&m17595_MI,
};
static Il2CppInterfaceOffsetPair t3164_IOs[] = 
{
	{ &t301_TI, 4},
};
extern TypeInfo t3125_TI;
static Il2CppRGCTXData t3164_RGCTXData[5] = 
{
	&t29_0_0_0/* Type Usage */,
	&t29_0_0_0/* Type Usage */,
	&t29_0_0_0/* Type Usage */,
	&t3125_TI/* Class Usage */,
	&m17261_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3164_0_0_0;
extern Il2CppType t3164_1_0_0;
struct t3164;
extern Il2CppGenericClass t3164_GC;
TypeInfo t3164_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityEvent`3", "UnityEngine.Events", t3164_MIs, NULL, t3164_FIs, NULL, &t566_TI, NULL, NULL, &t3164_TI, NULL, t3164_VT, &EmptyCustomAttributesCache, &t3164_TI, &t3164_0_0_0, &t3164_1_0_0, t3164_IOs, &t3164_GC, NULL, NULL, NULL, t3164_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3164), 0, -1, 0, 0, -1, 1056897, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 1, 0, 0, 8, 0, 1};
#include "t3165.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3165_TI;
#include "t3165MD.h"

#include "t3127.h"
extern TypeInfo t3127_TI;
#include "t3127MD.h"
extern MethodInfo m17268_MI;


extern MethodInfo m17596_MI;
 void m17596_gshared (t3165 * __this, MethodInfo* method)
{
	{
		__this->f4 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), 4));
		m2812(__this, &m2812_MI);
		return;
	}
}
extern MethodInfo m17597_MI;
 t557 * m17597_gshared (t3165 * __this, t7* p0, t29 * p1, MethodInfo* method)
{
	{
		t537* L_0 = ((t537*)SZArrayNew(InitializedTypeInfo(&t537_TI), 4));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_1 = m1554(NULL, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), &m1554_MI);
		ArrayElementTypeCheck (L_0, L_1);
		*((t42 **)(t42 **)SZArrayLdElema(L_0, 0)) = (t42 *)L_1;
		t537* L_2 = L_0;
		t42 * L_3 = m1554(NULL, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), &m1554_MI);
		ArrayElementTypeCheck (L_2, L_3);
		*((t42 **)(t42 **)SZArrayLdElema(L_2, 1)) = (t42 *)L_3;
		t537* L_4 = L_2;
		t42 * L_5 = m1554(NULL, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), &m1554_MI);
		ArrayElementTypeCheck (L_4, L_5);
		*((t42 **)(t42 **)SZArrayLdElema(L_4, 2)) = (t42 *)L_5;
		t537* L_6 = L_4;
		t42 * L_7 = m1554(NULL, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)), &m1554_MI);
		ArrayElementTypeCheck (L_6, L_7);
		*((t42 **)(t42 **)SZArrayLdElema(L_6, 3)) = (t42 *)L_7;
		t557 * L_8 = m2820(NULL, p1, p0, L_6, &m2820_MI);
		return L_8;
	}
}
extern MethodInfo m17598_MI;
 t556 * m17598_gshared (t3165 * __this, t29 * p0, t557 * p1, MethodInfo* method)
{
	{
		t3127 * L_0 = (t3127 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		(( void (*) (t3127 * __this, t29 * p0, t557 * p1, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_0, p0, p1, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_0;
	}
}
// Metadata Definition UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>
extern Il2CppType t316_0_0_33;
FieldInfo t3165_f4_FieldInfo = 
{
	"m_InvokeArray", &t316_0_0_33, &t3165_TI, offsetof(t3165, f4), &EmptyCustomAttributesCache};
static FieldInfo* t3165_FIs[] =
{
	&t3165_f4_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17596_GM;
MethodInfo m17596_MI = 
{
	".ctor", (methodPointerType)&m17596_gshared, &t3165_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17596_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3165_m17597_ParameterInfos[] = 
{
	{"name", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"targetObj", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t557_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17597_GM;
MethodInfo m17597_MI = 
{
	"FindMethod_Impl", (methodPointerType)&m17597_gshared, &t3165_TI, &t557_0_0_0, RuntimeInvoker_t29_t29_t29, t3165_m17597_ParameterInfos, &EmptyCustomAttributesCache, 196, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17597_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t3165_m17598_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t556_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17598_GM;
MethodInfo m17598_MI = 
{
	"GetDelegate", (methodPointerType)&m17598_gshared, &t3165_TI, &t556_0_0_0, RuntimeInvoker_t29_t29_t29, t3165_m17598_ParameterInfos, &EmptyCustomAttributesCache, 195, 0, 7, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17598_GM};
static MethodInfo* t3165_MIs[] =
{
	&m17596_MI,
	&m17597_MI,
	&m17598_MI,
	NULL
};
static MethodInfo* t3165_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1323_MI,
	&m1324_MI,
	&m1325_MI,
	&m17597_MI,
	&m17598_MI,
};
static Il2CppInterfaceOffsetPair t3165_IOs[] = 
{
	{ &t301_TI, 4},
};
extern TypeInfo t3127_TI;
static Il2CppRGCTXData t3165_RGCTXData[6] = 
{
	&t29_0_0_0/* Type Usage */,
	&t29_0_0_0/* Type Usage */,
	&t29_0_0_0/* Type Usage */,
	&t29_0_0_0/* Type Usage */,
	&t3127_TI/* Class Usage */,
	&m17268_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3165_0_0_0;
extern Il2CppType t3165_1_0_0;
struct t3165;
extern Il2CppGenericClass t3165_GC;
TypeInfo t3165_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityEvent`4", "UnityEngine.Events", t3165_MIs, NULL, t3165_FIs, NULL, &t566_TI, NULL, NULL, &t3165_TI, NULL, t3165_VT, &EmptyCustomAttributesCache, &t3165_TI, &t3165_0_0_0, &t3165_1_0_0, t3165_IOs, &t3165_GC, NULL, NULL, NULL, t3165_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3165), 0, -1, 0, 0, -1, 1056897, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 1, 0, 0, 8, 0, 1};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4517_TI;

#include "t575.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UserAuthorizationDialog>
extern MethodInfo m30336_MI;
static PropertyInfo t4517____Current_PropertyInfo = 
{
	&t4517_TI, "Current", &m30336_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4517_PIs[] =
{
	&t4517____Current_PropertyInfo,
	NULL
};
extern Il2CppType t575_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30336_GM;
MethodInfo m30336_MI = 
{
	"get_Current", NULL, &t4517_TI, &t575_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30336_GM};
static MethodInfo* t4517_MIs[] =
{
	&m30336_MI,
	NULL
};
static TypeInfo* t4517_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4517_0_0_0;
extern Il2CppType t4517_1_0_0;
struct t4517;
extern Il2CppGenericClass t4517_GC;
TypeInfo t4517_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4517_MIs, t4517_PIs, NULL, NULL, NULL, NULL, NULL, &t4517_TI, t4517_ITIs, NULL, &EmptyCustomAttributesCache, &t4517_TI, &t4517_0_0_0, &t4517_1_0_0, NULL, &t4517_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3166.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3166_TI;
#include "t3166MD.h"

extern TypeInfo t575_TI;
extern MethodInfo m17603_MI;
extern MethodInfo m23136_MI;
struct t20;
#define m23136(__this, p0, method) (t575 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UserAuthorizationDialog>
extern Il2CppType t20_0_0_1;
FieldInfo t3166_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3166_TI, offsetof(t3166, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3166_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3166_TI, offsetof(t3166, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3166_FIs[] =
{
	&t3166_f0_FieldInfo,
	&t3166_f1_FieldInfo,
	NULL
};
extern MethodInfo m17600_MI;
static PropertyInfo t3166____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3166_TI, "System.Collections.IEnumerator.Current", &m17600_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3166____Current_PropertyInfo = 
{
	&t3166_TI, "Current", &m17603_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3166_PIs[] =
{
	&t3166____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3166____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3166_m17599_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17599_GM;
MethodInfo m17599_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3166_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3166_m17599_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17599_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17600_GM;
MethodInfo m17600_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3166_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17600_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17601_GM;
MethodInfo m17601_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3166_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17601_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17602_GM;
MethodInfo m17602_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3166_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17602_GM};
extern Il2CppType t575_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17603_GM;
MethodInfo m17603_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3166_TI, &t575_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17603_GM};
static MethodInfo* t3166_MIs[] =
{
	&m17599_MI,
	&m17600_MI,
	&m17601_MI,
	&m17602_MI,
	&m17603_MI,
	NULL
};
extern MethodInfo m17602_MI;
extern MethodInfo m17601_MI;
static MethodInfo* t3166_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17600_MI,
	&m17602_MI,
	&m17601_MI,
	&m17603_MI,
};
static TypeInfo* t3166_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4517_TI,
};
static Il2CppInterfaceOffsetPair t3166_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4517_TI, 7},
};
extern TypeInfo t575_TI;
static Il2CppRGCTXData t3166_RGCTXData[3] = 
{
	&m17603_MI/* Method Usage */,
	&t575_TI/* Class Usage */,
	&m23136_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3166_0_0_0;
extern Il2CppType t3166_1_0_0;
extern Il2CppGenericClass t3166_GC;
TypeInfo t3166_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3166_MIs, t3166_PIs, t3166_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3166_TI, t3166_ITIs, t3166_VT, &EmptyCustomAttributesCache, &t3166_TI, &t3166_0_0_0, &t3166_1_0_0, t3166_IOs, &t3166_GC, NULL, NULL, NULL, t3166_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3166)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5799_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UserAuthorizationDialog>
extern MethodInfo m30337_MI;
static PropertyInfo t5799____Count_PropertyInfo = 
{
	&t5799_TI, "Count", &m30337_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30338_MI;
static PropertyInfo t5799____IsReadOnly_PropertyInfo = 
{
	&t5799_TI, "IsReadOnly", &m30338_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5799_PIs[] =
{
	&t5799____Count_PropertyInfo,
	&t5799____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30337_GM;
MethodInfo m30337_MI = 
{
	"get_Count", NULL, &t5799_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30337_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30338_GM;
MethodInfo m30338_MI = 
{
	"get_IsReadOnly", NULL, &t5799_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30338_GM};
extern Il2CppType t575_0_0_0;
extern Il2CppType t575_0_0_0;
static ParameterInfo t5799_m30339_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t575_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30339_GM;
MethodInfo m30339_MI = 
{
	"Add", NULL, &t5799_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5799_m30339_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30339_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30340_GM;
MethodInfo m30340_MI = 
{
	"Clear", NULL, &t5799_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30340_GM};
extern Il2CppType t575_0_0_0;
static ParameterInfo t5799_m30341_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t575_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30341_GM;
MethodInfo m30341_MI = 
{
	"Contains", NULL, &t5799_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5799_m30341_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30341_GM};
extern Il2CppType t3792_0_0_0;
extern Il2CppType t3792_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5799_m30342_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3792_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30342_GM;
MethodInfo m30342_MI = 
{
	"CopyTo", NULL, &t5799_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5799_m30342_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30342_GM};
extern Il2CppType t575_0_0_0;
static ParameterInfo t5799_m30343_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t575_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30343_GM;
MethodInfo m30343_MI = 
{
	"Remove", NULL, &t5799_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5799_m30343_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30343_GM};
static MethodInfo* t5799_MIs[] =
{
	&m30337_MI,
	&m30338_MI,
	&m30339_MI,
	&m30340_MI,
	&m30341_MI,
	&m30342_MI,
	&m30343_MI,
	NULL
};
extern TypeInfo t5801_TI;
static TypeInfo* t5799_ITIs[] = 
{
	&t603_TI,
	&t5801_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5799_0_0_0;
extern Il2CppType t5799_1_0_0;
struct t5799;
extern Il2CppGenericClass t5799_GC;
TypeInfo t5799_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5799_MIs, t5799_PIs, NULL, NULL, NULL, NULL, NULL, &t5799_TI, t5799_ITIs, NULL, &EmptyCustomAttributesCache, &t5799_TI, &t5799_0_0_0, &t5799_1_0_0, NULL, &t5799_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UserAuthorizationDialog>
extern Il2CppType t4517_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30344_GM;
MethodInfo m30344_MI = 
{
	"GetEnumerator", NULL, &t5801_TI, &t4517_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30344_GM};
static MethodInfo* t5801_MIs[] =
{
	&m30344_MI,
	NULL
};
static TypeInfo* t5801_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5801_0_0_0;
extern Il2CppType t5801_1_0_0;
struct t5801;
extern Il2CppGenericClass t5801_GC;
TypeInfo t5801_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5801_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5801_TI, t5801_ITIs, NULL, &EmptyCustomAttributesCache, &t5801_TI, &t5801_0_0_0, &t5801_1_0_0, NULL, &t5801_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5800_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UserAuthorizationDialog>
extern MethodInfo m30345_MI;
extern MethodInfo m30346_MI;
static PropertyInfo t5800____Item_PropertyInfo = 
{
	&t5800_TI, "Item", &m30345_MI, &m30346_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5800_PIs[] =
{
	&t5800____Item_PropertyInfo,
	NULL
};
extern Il2CppType t575_0_0_0;
static ParameterInfo t5800_m30347_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t575_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30347_GM;
MethodInfo m30347_MI = 
{
	"IndexOf", NULL, &t5800_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5800_m30347_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30347_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t575_0_0_0;
static ParameterInfo t5800_m30348_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t575_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30348_GM;
MethodInfo m30348_MI = 
{
	"Insert", NULL, &t5800_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5800_m30348_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30348_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5800_m30349_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30349_GM;
MethodInfo m30349_MI = 
{
	"RemoveAt", NULL, &t5800_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5800_m30349_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30349_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5800_m30345_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t575_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30345_GM;
MethodInfo m30345_MI = 
{
	"get_Item", NULL, &t5800_TI, &t575_0_0_0, RuntimeInvoker_t29_t44, t5800_m30345_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30345_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t575_0_0_0;
static ParameterInfo t5800_m30346_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t575_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30346_GM;
MethodInfo m30346_MI = 
{
	"set_Item", NULL, &t5800_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5800_m30346_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30346_GM};
static MethodInfo* t5800_MIs[] =
{
	&m30347_MI,
	&m30348_MI,
	&m30349_MI,
	&m30345_MI,
	&m30346_MI,
	NULL
};
static TypeInfo* t5800_ITIs[] = 
{
	&t603_TI,
	&t5799_TI,
	&t5801_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5800_0_0_0;
extern Il2CppType t5800_1_0_0;
struct t5800;
extern Il2CppGenericClass t5800_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5800_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5800_MIs, t5800_PIs, NULL, NULL, NULL, NULL, NULL, &t5800_TI, t5800_ITIs, NULL, &t1908__CustomAttributeCache, &t5800_TI, &t5800_0_0_0, &t5800_1_0_0, NULL, &t5800_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t3167.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3167_TI;
#include "t3167MD.h"

#include "t41.h"
#include "t3168.h"
extern TypeInfo t3168_TI;
#include "t3168MD.h"
extern MethodInfo m17606_MI;
extern MethodInfo m17608_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UserAuthorizationDialog>
extern Il2CppType t316_0_0_33;
FieldInfo t3167_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t3167_TI, offsetof(t3167, f1), &EmptyCustomAttributesCache};
static FieldInfo* t3167_FIs[] =
{
	&t3167_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t575_0_0_0;
static ParameterInfo t3167_m17604_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t575_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17604_GM;
MethodInfo m17604_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t3167_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t3167_m17604_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m17604_GM};
extern Il2CppType t316_0_0_0;
extern Il2CppType t316_0_0_0;
static ParameterInfo t3167_m17605_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17605_GM;
MethodInfo m17605_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t3167_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3167_m17605_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17605_GM};
static MethodInfo* t3167_MIs[] =
{
	&m17604_MI,
	&m17605_MI,
	NULL
};
extern MethodInfo m17605_MI;
extern MethodInfo m17609_MI;
static MethodInfo* t3167_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m17605_MI,
	&m17609_MI,
};
extern Il2CppType t3169_0_0_0;
extern TypeInfo t3169_TI;
extern MethodInfo m23146_MI;
extern TypeInfo t575_TI;
extern MethodInfo m17611_MI;
extern TypeInfo t575_TI;
static Il2CppRGCTXData t3167_RGCTXData[8] = 
{
	&t3169_0_0_0/* Type Usage */,
	&t3169_TI/* Class Usage */,
	&m23146_MI/* Method Usage */,
	&t575_TI/* Class Usage */,
	&m17611_MI/* Method Usage */,
	&m17606_MI/* Method Usage */,
	&t575_TI/* Class Usage */,
	&m17608_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3167_0_0_0;
extern Il2CppType t3167_1_0_0;
struct t3167;
extern Il2CppGenericClass t3167_GC;
TypeInfo t3167_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t3167_MIs, NULL, t3167_FIs, NULL, &t3168_TI, NULL, NULL, &t3167_TI, NULL, t3167_VT, &EmptyCustomAttributesCache, &t3167_TI, &t3167_0_0_0, &t3167_1_0_0, NULL, &t3167_GC, NULL, NULL, NULL, t3167_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3167), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t3169.h"
#include "t353.h"
extern TypeInfo t3169_TI;
#include "t556MD.h"
#include "t353MD.h"
#include "t3169MD.h"
extern MethodInfo m2790_MI;
extern MethodInfo m2957_MI;
extern MethodInfo m1597_MI;
extern MethodInfo m2789_MI;
extern MethodInfo m2791_MI;
extern MethodInfo m2953_MI;
extern MethodInfo m2951_MI;
struct t556;
struct t556;
 void m19555_gshared (t29 * __this, t29 * p0, MethodInfo* method);
#define m19555(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)
#define m23146(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UserAuthorizationDialog>
extern Il2CppType t3169_0_0_1;
FieldInfo t3168_f0_FieldInfo = 
{
	"Delegate", &t3169_0_0_1, &t3168_TI, offsetof(t3168, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3168_FIs[] =
{
	&t3168_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t3168_m17606_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17606_GM;
MethodInfo m17606_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t3168_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t3168_m17606_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17606_GM};
extern Il2CppType t3169_0_0_0;
static ParameterInfo t3168_m17607_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t3169_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17607_GM;
MethodInfo m17607_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t3168_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3168_m17607_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17607_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t3168_m17608_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17608_GM;
MethodInfo m17608_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t3168_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3168_m17608_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17608_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t3168_m17609_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17609_GM;
MethodInfo m17609_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t3168_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t3168_m17609_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17609_GM};
static MethodInfo* t3168_MIs[] =
{
	&m17606_MI,
	&m17607_MI,
	&m17608_MI,
	&m17609_MI,
	NULL
};
static MethodInfo* t3168_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m17608_MI,
	&m17609_MI,
};
extern TypeInfo t3169_TI;
extern TypeInfo t575_TI;
static Il2CppRGCTXData t3168_RGCTXData[5] = 
{
	&t3169_0_0_0/* Type Usage */,
	&t3169_TI/* Class Usage */,
	&m23146_MI/* Method Usage */,
	&t575_TI/* Class Usage */,
	&m17611_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3168_0_0_0;
extern Il2CppType t3168_1_0_0;
struct t3168;
extern Il2CppGenericClass t3168_GC;
TypeInfo t3168_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t3168_MIs, NULL, t3168_FIs, NULL, &t556_TI, NULL, NULL, &t3168_TI, NULL, t3168_VT, &EmptyCustomAttributesCache, &t3168_TI, &t3168_0_0_0, &t3168_1_0_0, NULL, &t3168_GC, NULL, NULL, NULL, t3168_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3168), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UserAuthorizationDialog>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t3169_m17610_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17610_GM;
MethodInfo m17610_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t3169_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t3169_m17610_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17610_GM};
extern Il2CppType t575_0_0_0;
static ParameterInfo t3169_m17611_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t575_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17611_GM;
MethodInfo m17611_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t3169_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3169_m17611_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17611_GM};
extern Il2CppType t575_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3169_m17612_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t575_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17612_GM;
MethodInfo m17612_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t3169_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t3169_m17612_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m17612_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t3169_m17613_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17613_GM;
MethodInfo m17613_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t3169_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3169_m17613_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17613_GM};
static MethodInfo* t3169_MIs[] =
{
	&m17610_MI,
	&m17611_MI,
	&m17612_MI,
	&m17613_MI,
	NULL
};
extern MethodInfo m17612_MI;
extern MethodInfo m17613_MI;
static MethodInfo* t3169_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m17611_MI,
	&m17612_MI,
	&m17613_MI,
};
static Il2CppInterfaceOffsetPair t3169_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3169_1_0_0;
struct t3169;
extern Il2CppGenericClass t3169_GC;
TypeInfo t3169_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t3169_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t3169_TI, NULL, t3169_VT, &EmptyCustomAttributesCache, &t3169_TI, &t3169_0_0_0, &t3169_1_0_0, t3169_IOs, &t3169_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3169), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4519_TI;

#include "t576.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Internal.DefaultValueAttribute>
extern MethodInfo m30350_MI;
static PropertyInfo t4519____Current_PropertyInfo = 
{
	&t4519_TI, "Current", &m30350_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4519_PIs[] =
{
	&t4519____Current_PropertyInfo,
	NULL
};
extern Il2CppType t576_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30350_GM;
MethodInfo m30350_MI = 
{
	"get_Current", NULL, &t4519_TI, &t576_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30350_GM};
static MethodInfo* t4519_MIs[] =
{
	&m30350_MI,
	NULL
};
static TypeInfo* t4519_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4519_0_0_0;
extern Il2CppType t4519_1_0_0;
struct t4519;
extern Il2CppGenericClass t4519_GC;
TypeInfo t4519_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4519_MIs, t4519_PIs, NULL, NULL, NULL, NULL, NULL, &t4519_TI, t4519_ITIs, NULL, &EmptyCustomAttributesCache, &t4519_TI, &t4519_0_0_0, &t4519_1_0_0, NULL, &t4519_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3170.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3170_TI;
#include "t3170MD.h"

extern TypeInfo t576_TI;
extern MethodInfo m17618_MI;
extern MethodInfo m23148_MI;
struct t20;
#define m23148(__this, p0, method) (t576 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Internal.DefaultValueAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3170_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3170_TI, offsetof(t3170, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3170_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3170_TI, offsetof(t3170, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3170_FIs[] =
{
	&t3170_f0_FieldInfo,
	&t3170_f1_FieldInfo,
	NULL
};
extern MethodInfo m17615_MI;
static PropertyInfo t3170____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3170_TI, "System.Collections.IEnumerator.Current", &m17615_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3170____Current_PropertyInfo = 
{
	&t3170_TI, "Current", &m17618_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3170_PIs[] =
{
	&t3170____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3170____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3170_m17614_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17614_GM;
MethodInfo m17614_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3170_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3170_m17614_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17614_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17615_GM;
MethodInfo m17615_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3170_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17615_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17616_GM;
MethodInfo m17616_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3170_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17616_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17617_GM;
MethodInfo m17617_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3170_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17617_GM};
extern Il2CppType t576_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17618_GM;
MethodInfo m17618_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3170_TI, &t576_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17618_GM};
static MethodInfo* t3170_MIs[] =
{
	&m17614_MI,
	&m17615_MI,
	&m17616_MI,
	&m17617_MI,
	&m17618_MI,
	NULL
};
extern MethodInfo m17617_MI;
extern MethodInfo m17616_MI;
static MethodInfo* t3170_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17615_MI,
	&m17617_MI,
	&m17616_MI,
	&m17618_MI,
};
static TypeInfo* t3170_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4519_TI,
};
static Il2CppInterfaceOffsetPair t3170_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4519_TI, 7},
};
extern TypeInfo t576_TI;
static Il2CppRGCTXData t3170_RGCTXData[3] = 
{
	&m17618_MI/* Method Usage */,
	&t576_TI/* Class Usage */,
	&m23148_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3170_0_0_0;
extern Il2CppType t3170_1_0_0;
extern Il2CppGenericClass t3170_GC;
TypeInfo t3170_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3170_MIs, t3170_PIs, t3170_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3170_TI, t3170_ITIs, t3170_VT, &EmptyCustomAttributesCache, &t3170_TI, &t3170_0_0_0, &t3170_1_0_0, t3170_IOs, &t3170_GC, NULL, NULL, NULL, t3170_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3170)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5802_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Internal.DefaultValueAttribute>
extern MethodInfo m30351_MI;
static PropertyInfo t5802____Count_PropertyInfo = 
{
	&t5802_TI, "Count", &m30351_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30352_MI;
static PropertyInfo t5802____IsReadOnly_PropertyInfo = 
{
	&t5802_TI, "IsReadOnly", &m30352_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5802_PIs[] =
{
	&t5802____Count_PropertyInfo,
	&t5802____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30351_GM;
MethodInfo m30351_MI = 
{
	"get_Count", NULL, &t5802_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30351_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30352_GM;
MethodInfo m30352_MI = 
{
	"get_IsReadOnly", NULL, &t5802_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30352_GM};
extern Il2CppType t576_0_0_0;
extern Il2CppType t576_0_0_0;
static ParameterInfo t5802_m30353_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t576_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30353_GM;
MethodInfo m30353_MI = 
{
	"Add", NULL, &t5802_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5802_m30353_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30353_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30354_GM;
MethodInfo m30354_MI = 
{
	"Clear", NULL, &t5802_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30354_GM};
extern Il2CppType t576_0_0_0;
static ParameterInfo t5802_m30355_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t576_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30355_GM;
MethodInfo m30355_MI = 
{
	"Contains", NULL, &t5802_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5802_m30355_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30355_GM};
extern Il2CppType t3793_0_0_0;
extern Il2CppType t3793_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5802_m30356_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3793_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30356_GM;
MethodInfo m30356_MI = 
{
	"CopyTo", NULL, &t5802_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5802_m30356_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30356_GM};
extern Il2CppType t576_0_0_0;
static ParameterInfo t5802_m30357_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t576_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30357_GM;
MethodInfo m30357_MI = 
{
	"Remove", NULL, &t5802_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5802_m30357_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30357_GM};
static MethodInfo* t5802_MIs[] =
{
	&m30351_MI,
	&m30352_MI,
	&m30353_MI,
	&m30354_MI,
	&m30355_MI,
	&m30356_MI,
	&m30357_MI,
	NULL
};
extern TypeInfo t5804_TI;
static TypeInfo* t5802_ITIs[] = 
{
	&t603_TI,
	&t5804_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5802_0_0_0;
extern Il2CppType t5802_1_0_0;
struct t5802;
extern Il2CppGenericClass t5802_GC;
TypeInfo t5802_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5802_MIs, t5802_PIs, NULL, NULL, NULL, NULL, NULL, &t5802_TI, t5802_ITIs, NULL, &EmptyCustomAttributesCache, &t5802_TI, &t5802_0_0_0, &t5802_1_0_0, NULL, &t5802_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Internal.DefaultValueAttribute>
extern Il2CppType t4519_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30358_GM;
MethodInfo m30358_MI = 
{
	"GetEnumerator", NULL, &t5804_TI, &t4519_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30358_GM};
static MethodInfo* t5804_MIs[] =
{
	&m30358_MI,
	NULL
};
static TypeInfo* t5804_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5804_0_0_0;
extern Il2CppType t5804_1_0_0;
struct t5804;
extern Il2CppGenericClass t5804_GC;
TypeInfo t5804_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5804_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5804_TI, t5804_ITIs, NULL, &EmptyCustomAttributesCache, &t5804_TI, &t5804_0_0_0, &t5804_1_0_0, NULL, &t5804_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5803_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Internal.DefaultValueAttribute>
extern MethodInfo m30359_MI;
extern MethodInfo m30360_MI;
static PropertyInfo t5803____Item_PropertyInfo = 
{
	&t5803_TI, "Item", &m30359_MI, &m30360_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5803_PIs[] =
{
	&t5803____Item_PropertyInfo,
	NULL
};
extern Il2CppType t576_0_0_0;
static ParameterInfo t5803_m30361_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t576_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30361_GM;
MethodInfo m30361_MI = 
{
	"IndexOf", NULL, &t5803_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5803_m30361_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30361_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t576_0_0_0;
static ParameterInfo t5803_m30362_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t576_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30362_GM;
MethodInfo m30362_MI = 
{
	"Insert", NULL, &t5803_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5803_m30362_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30362_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5803_m30363_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30363_GM;
MethodInfo m30363_MI = 
{
	"RemoveAt", NULL, &t5803_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5803_m30363_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30363_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5803_m30359_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t576_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30359_GM;
MethodInfo m30359_MI = 
{
	"get_Item", NULL, &t5803_TI, &t576_0_0_0, RuntimeInvoker_t29_t44, t5803_m30359_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30359_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t576_0_0_0;
static ParameterInfo t5803_m30360_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t576_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30360_GM;
MethodInfo m30360_MI = 
{
	"set_Item", NULL, &t5803_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5803_m30360_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30360_GM};
static MethodInfo* t5803_MIs[] =
{
	&m30361_MI,
	&m30362_MI,
	&m30363_MI,
	&m30359_MI,
	&m30360_MI,
	NULL
};
static TypeInfo* t5803_ITIs[] = 
{
	&t603_TI,
	&t5802_TI,
	&t5804_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5803_0_0_0;
extern Il2CppType t5803_1_0_0;
struct t5803;
extern Il2CppGenericClass t5803_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5803_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5803_MIs, t5803_PIs, NULL, NULL, NULL, NULL, NULL, &t5803_TI, t5803_ITIs, NULL, &t1908__CustomAttributeCache, &t5803_TI, &t5803_0_0_0, &t5803_1_0_0, NULL, &t5803_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4521_TI;

#include "t577.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Internal.ExcludeFromDocsAttribute>
extern MethodInfo m30364_MI;
static PropertyInfo t4521____Current_PropertyInfo = 
{
	&t4521_TI, "Current", &m30364_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4521_PIs[] =
{
	&t4521____Current_PropertyInfo,
	NULL
};
extern Il2CppType t577_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30364_GM;
MethodInfo m30364_MI = 
{
	"get_Current", NULL, &t4521_TI, &t577_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30364_GM};
static MethodInfo* t4521_MIs[] =
{
	&m30364_MI,
	NULL
};
static TypeInfo* t4521_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4521_0_0_0;
extern Il2CppType t4521_1_0_0;
struct t4521;
extern Il2CppGenericClass t4521_GC;
TypeInfo t4521_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4521_MIs, t4521_PIs, NULL, NULL, NULL, NULL, NULL, &t4521_TI, t4521_ITIs, NULL, &EmptyCustomAttributesCache, &t4521_TI, &t4521_0_0_0, &t4521_1_0_0, NULL, &t4521_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3171.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3171_TI;
#include "t3171MD.h"

extern TypeInfo t577_TI;
extern MethodInfo m17623_MI;
extern MethodInfo m23159_MI;
struct t20;
#define m23159(__this, p0, method) (t577 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Internal.ExcludeFromDocsAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3171_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3171_TI, offsetof(t3171, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3171_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3171_TI, offsetof(t3171, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3171_FIs[] =
{
	&t3171_f0_FieldInfo,
	&t3171_f1_FieldInfo,
	NULL
};
extern MethodInfo m17620_MI;
static PropertyInfo t3171____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3171_TI, "System.Collections.IEnumerator.Current", &m17620_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3171____Current_PropertyInfo = 
{
	&t3171_TI, "Current", &m17623_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3171_PIs[] =
{
	&t3171____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3171____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3171_m17619_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17619_GM;
MethodInfo m17619_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3171_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3171_m17619_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17619_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17620_GM;
MethodInfo m17620_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3171_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17620_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17621_GM;
MethodInfo m17621_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3171_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17621_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17622_GM;
MethodInfo m17622_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3171_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17622_GM};
extern Il2CppType t577_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17623_GM;
MethodInfo m17623_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3171_TI, &t577_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17623_GM};
static MethodInfo* t3171_MIs[] =
{
	&m17619_MI,
	&m17620_MI,
	&m17621_MI,
	&m17622_MI,
	&m17623_MI,
	NULL
};
extern MethodInfo m17622_MI;
extern MethodInfo m17621_MI;
static MethodInfo* t3171_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17620_MI,
	&m17622_MI,
	&m17621_MI,
	&m17623_MI,
};
static TypeInfo* t3171_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4521_TI,
};
static Il2CppInterfaceOffsetPair t3171_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4521_TI, 7},
};
extern TypeInfo t577_TI;
static Il2CppRGCTXData t3171_RGCTXData[3] = 
{
	&m17623_MI/* Method Usage */,
	&t577_TI/* Class Usage */,
	&m23159_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3171_0_0_0;
extern Il2CppType t3171_1_0_0;
extern Il2CppGenericClass t3171_GC;
TypeInfo t3171_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3171_MIs, t3171_PIs, t3171_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3171_TI, t3171_ITIs, t3171_VT, &EmptyCustomAttributesCache, &t3171_TI, &t3171_0_0_0, &t3171_1_0_0, t3171_IOs, &t3171_GC, NULL, NULL, NULL, t3171_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3171)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5805_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Internal.ExcludeFromDocsAttribute>
extern MethodInfo m30365_MI;
static PropertyInfo t5805____Count_PropertyInfo = 
{
	&t5805_TI, "Count", &m30365_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30366_MI;
static PropertyInfo t5805____IsReadOnly_PropertyInfo = 
{
	&t5805_TI, "IsReadOnly", &m30366_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5805_PIs[] =
{
	&t5805____Count_PropertyInfo,
	&t5805____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30365_GM;
MethodInfo m30365_MI = 
{
	"get_Count", NULL, &t5805_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30365_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30366_GM;
MethodInfo m30366_MI = 
{
	"get_IsReadOnly", NULL, &t5805_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30366_GM};
extern Il2CppType t577_0_0_0;
extern Il2CppType t577_0_0_0;
static ParameterInfo t5805_m30367_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t577_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30367_GM;
MethodInfo m30367_MI = 
{
	"Add", NULL, &t5805_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5805_m30367_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30367_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30368_GM;
MethodInfo m30368_MI = 
{
	"Clear", NULL, &t5805_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30368_GM};
extern Il2CppType t577_0_0_0;
static ParameterInfo t5805_m30369_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t577_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30369_GM;
MethodInfo m30369_MI = 
{
	"Contains", NULL, &t5805_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5805_m30369_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30369_GM};
extern Il2CppType t3794_0_0_0;
extern Il2CppType t3794_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5805_m30370_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3794_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30370_GM;
MethodInfo m30370_MI = 
{
	"CopyTo", NULL, &t5805_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5805_m30370_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30370_GM};
extern Il2CppType t577_0_0_0;
static ParameterInfo t5805_m30371_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t577_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30371_GM;
MethodInfo m30371_MI = 
{
	"Remove", NULL, &t5805_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5805_m30371_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30371_GM};
static MethodInfo* t5805_MIs[] =
{
	&m30365_MI,
	&m30366_MI,
	&m30367_MI,
	&m30368_MI,
	&m30369_MI,
	&m30370_MI,
	&m30371_MI,
	NULL
};
extern TypeInfo t5807_TI;
static TypeInfo* t5805_ITIs[] = 
{
	&t603_TI,
	&t5807_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5805_0_0_0;
extern Il2CppType t5805_1_0_0;
struct t5805;
extern Il2CppGenericClass t5805_GC;
TypeInfo t5805_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5805_MIs, t5805_PIs, NULL, NULL, NULL, NULL, NULL, &t5805_TI, t5805_ITIs, NULL, &EmptyCustomAttributesCache, &t5805_TI, &t5805_0_0_0, &t5805_1_0_0, NULL, &t5805_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Internal.ExcludeFromDocsAttribute>
extern Il2CppType t4521_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30372_GM;
MethodInfo m30372_MI = 
{
	"GetEnumerator", NULL, &t5807_TI, &t4521_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30372_GM};
static MethodInfo* t5807_MIs[] =
{
	&m30372_MI,
	NULL
};
static TypeInfo* t5807_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5807_0_0_0;
extern Il2CppType t5807_1_0_0;
struct t5807;
extern Il2CppGenericClass t5807_GC;
TypeInfo t5807_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5807_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5807_TI, t5807_ITIs, NULL, &EmptyCustomAttributesCache, &t5807_TI, &t5807_0_0_0, &t5807_1_0_0, NULL, &t5807_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5806_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Internal.ExcludeFromDocsAttribute>
extern MethodInfo m30373_MI;
extern MethodInfo m30374_MI;
static PropertyInfo t5806____Item_PropertyInfo = 
{
	&t5806_TI, "Item", &m30373_MI, &m30374_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5806_PIs[] =
{
	&t5806____Item_PropertyInfo,
	NULL
};
extern Il2CppType t577_0_0_0;
static ParameterInfo t5806_m30375_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t577_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30375_GM;
MethodInfo m30375_MI = 
{
	"IndexOf", NULL, &t5806_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5806_m30375_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30375_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t577_0_0_0;
static ParameterInfo t5806_m30376_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t577_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30376_GM;
MethodInfo m30376_MI = 
{
	"Insert", NULL, &t5806_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5806_m30376_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30376_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5806_m30377_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30377_GM;
MethodInfo m30377_MI = 
{
	"RemoveAt", NULL, &t5806_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5806_m30377_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30377_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5806_m30373_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t577_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30373_GM;
MethodInfo m30373_MI = 
{
	"get_Item", NULL, &t5806_TI, &t577_0_0_0, RuntimeInvoker_t29_t44, t5806_m30373_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30373_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t577_0_0_0;
static ParameterInfo t5806_m30374_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t577_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30374_GM;
MethodInfo m30374_MI = 
{
	"set_Item", NULL, &t5806_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5806_m30374_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30374_GM};
static MethodInfo* t5806_MIs[] =
{
	&m30375_MI,
	&m30376_MI,
	&m30377_MI,
	&m30373_MI,
	&m30374_MI,
	NULL
};
static TypeInfo* t5806_ITIs[] = 
{
	&t603_TI,
	&t5805_TI,
	&t5807_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5806_0_0_0;
extern Il2CppType t5806_1_0_0;
struct t5806;
extern Il2CppGenericClass t5806_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5806_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5806_MIs, t5806_PIs, NULL, NULL, NULL, NULL, NULL, &t5806_TI, t5806_ITIs, NULL, &t1908__CustomAttributeCache, &t5806_TI, &t5806_0_0_0, &t5806_1_0_0, NULL, &t5806_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4523_TI;

#include "t299.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>
extern MethodInfo m30378_MI;
static PropertyInfo t4523____Current_PropertyInfo = 
{
	&t4523_TI, "Current", &m30378_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4523_PIs[] =
{
	&t4523____Current_PropertyInfo,
	NULL
};
extern Il2CppType t299_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30378_GM;
MethodInfo m30378_MI = 
{
	"get_Current", NULL, &t4523_TI, &t299_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30378_GM};
static MethodInfo* t4523_MIs[] =
{
	&m30378_MI,
	NULL
};
static TypeInfo* t4523_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4523_0_0_0;
extern Il2CppType t4523_1_0_0;
struct t4523;
extern Il2CppGenericClass t4523_GC;
TypeInfo t4523_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4523_MIs, t4523_PIs, NULL, NULL, NULL, NULL, NULL, &t4523_TI, t4523_ITIs, NULL, &EmptyCustomAttributesCache, &t4523_TI, &t4523_0_0_0, &t4523_1_0_0, NULL, &t4523_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3172.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3172_TI;
#include "t3172MD.h"

extern TypeInfo t299_TI;
extern MethodInfo m17628_MI;
extern MethodInfo m23170_MI;
struct t20;
#define m23170(__this, p0, method) (t299 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3172_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3172_TI, offsetof(t3172, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3172_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3172_TI, offsetof(t3172, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3172_FIs[] =
{
	&t3172_f0_FieldInfo,
	&t3172_f1_FieldInfo,
	NULL
};
extern MethodInfo m17625_MI;
static PropertyInfo t3172____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3172_TI, "System.Collections.IEnumerator.Current", &m17625_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3172____Current_PropertyInfo = 
{
	&t3172_TI, "Current", &m17628_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3172_PIs[] =
{
	&t3172____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3172____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3172_m17624_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17624_GM;
MethodInfo m17624_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3172_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3172_m17624_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17624_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17625_GM;
MethodInfo m17625_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3172_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17625_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17626_GM;
MethodInfo m17626_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3172_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17626_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17627_GM;
MethodInfo m17627_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3172_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17627_GM};
extern Il2CppType t299_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17628_GM;
MethodInfo m17628_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3172_TI, &t299_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17628_GM};
static MethodInfo* t3172_MIs[] =
{
	&m17624_MI,
	&m17625_MI,
	&m17626_MI,
	&m17627_MI,
	&m17628_MI,
	NULL
};
extern MethodInfo m17627_MI;
extern MethodInfo m17626_MI;
static MethodInfo* t3172_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17625_MI,
	&m17627_MI,
	&m17626_MI,
	&m17628_MI,
};
static TypeInfo* t3172_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4523_TI,
};
static Il2CppInterfaceOffsetPair t3172_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4523_TI, 7},
};
extern TypeInfo t299_TI;
static Il2CppRGCTXData t3172_RGCTXData[3] = 
{
	&m17628_MI/* Method Usage */,
	&t299_TI/* Class Usage */,
	&m23170_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3172_0_0_0;
extern Il2CppType t3172_1_0_0;
extern Il2CppGenericClass t3172_GC;
TypeInfo t3172_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3172_MIs, t3172_PIs, t3172_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3172_TI, t3172_ITIs, t3172_VT, &EmptyCustomAttributesCache, &t3172_TI, &t3172_0_0_0, &t3172_1_0_0, t3172_IOs, &t3172_GC, NULL, NULL, NULL, t3172_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3172)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5808_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>
extern MethodInfo m30379_MI;
static PropertyInfo t5808____Count_PropertyInfo = 
{
	&t5808_TI, "Count", &m30379_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30380_MI;
static PropertyInfo t5808____IsReadOnly_PropertyInfo = 
{
	&t5808_TI, "IsReadOnly", &m30380_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5808_PIs[] =
{
	&t5808____Count_PropertyInfo,
	&t5808____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30379_GM;
MethodInfo m30379_MI = 
{
	"get_Count", NULL, &t5808_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30379_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30380_GM;
MethodInfo m30380_MI = 
{
	"get_IsReadOnly", NULL, &t5808_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30380_GM};
extern Il2CppType t299_0_0_0;
extern Il2CppType t299_0_0_0;
static ParameterInfo t5808_m30381_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t299_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30381_GM;
MethodInfo m30381_MI = 
{
	"Add", NULL, &t5808_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5808_m30381_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30381_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30382_GM;
MethodInfo m30382_MI = 
{
	"Clear", NULL, &t5808_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30382_GM};
extern Il2CppType t299_0_0_0;
static ParameterInfo t5808_m30383_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t299_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30383_GM;
MethodInfo m30383_MI = 
{
	"Contains", NULL, &t5808_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5808_m30383_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30383_GM};
extern Il2CppType t3795_0_0_0;
extern Il2CppType t3795_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5808_m30384_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3795_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30384_GM;
MethodInfo m30384_MI = 
{
	"CopyTo", NULL, &t5808_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5808_m30384_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30384_GM};
extern Il2CppType t299_0_0_0;
static ParameterInfo t5808_m30385_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t299_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30385_GM;
MethodInfo m30385_MI = 
{
	"Remove", NULL, &t5808_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5808_m30385_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30385_GM};
static MethodInfo* t5808_MIs[] =
{
	&m30379_MI,
	&m30380_MI,
	&m30381_MI,
	&m30382_MI,
	&m30383_MI,
	&m30384_MI,
	&m30385_MI,
	NULL
};
extern TypeInfo t5810_TI;
static TypeInfo* t5808_ITIs[] = 
{
	&t603_TI,
	&t5810_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5808_0_0_0;
extern Il2CppType t5808_1_0_0;
struct t5808;
extern Il2CppGenericClass t5808_GC;
TypeInfo t5808_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5808_MIs, t5808_PIs, NULL, NULL, NULL, NULL, NULL, &t5808_TI, t5808_ITIs, NULL, &EmptyCustomAttributesCache, &t5808_TI, &t5808_0_0_0, &t5808_1_0_0, NULL, &t5808_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>
extern Il2CppType t4523_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30386_GM;
MethodInfo m30386_MI = 
{
	"GetEnumerator", NULL, &t5810_TI, &t4523_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30386_GM};
static MethodInfo* t5810_MIs[] =
{
	&m30386_MI,
	NULL
};
static TypeInfo* t5810_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5810_0_0_0;
extern Il2CppType t5810_1_0_0;
struct t5810;
extern Il2CppGenericClass t5810_GC;
TypeInfo t5810_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5810_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5810_TI, t5810_ITIs, NULL, &EmptyCustomAttributesCache, &t5810_TI, &t5810_0_0_0, &t5810_1_0_0, NULL, &t5810_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5809_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>
extern MethodInfo m30387_MI;
extern MethodInfo m30388_MI;
static PropertyInfo t5809____Item_PropertyInfo = 
{
	&t5809_TI, "Item", &m30387_MI, &m30388_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5809_PIs[] =
{
	&t5809____Item_PropertyInfo,
	NULL
};
extern Il2CppType t299_0_0_0;
static ParameterInfo t5809_m30389_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t299_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30389_GM;
MethodInfo m30389_MI = 
{
	"IndexOf", NULL, &t5809_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5809_m30389_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30389_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t299_0_0_0;
static ParameterInfo t5809_m30390_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t299_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30390_GM;
MethodInfo m30390_MI = 
{
	"Insert", NULL, &t5809_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5809_m30390_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30390_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5809_m30391_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30391_GM;
MethodInfo m30391_MI = 
{
	"RemoveAt", NULL, &t5809_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5809_m30391_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30391_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5809_m30387_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t299_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30387_GM;
MethodInfo m30387_MI = 
{
	"get_Item", NULL, &t5809_TI, &t299_0_0_0, RuntimeInvoker_t29_t44, t5809_m30387_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30387_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t299_0_0_0;
static ParameterInfo t5809_m30388_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t299_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30388_GM;
MethodInfo m30388_MI = 
{
	"set_Item", NULL, &t5809_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5809_m30388_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30388_GM};
static MethodInfo* t5809_MIs[] =
{
	&m30389_MI,
	&m30390_MI,
	&m30391_MI,
	&m30387_MI,
	&m30388_MI,
	NULL
};
static TypeInfo* t5809_ITIs[] = 
{
	&t603_TI,
	&t5808_TI,
	&t5810_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5809_0_0_0;
extern Il2CppType t5809_1_0_0;
struct t5809;
extern Il2CppGenericClass t5809_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5809_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5809_MIs, t5809_PIs, NULL, NULL, NULL, NULL, NULL, &t5809_TI, t5809_ITIs, NULL, &t1908__CustomAttributeCache, &t5809_TI, &t5809_0_0_0, &t5809_1_0_0, NULL, &t5809_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4525_TI;

#include "t578.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngineInternal.TypeInferenceRules>
extern MethodInfo m30392_MI;
static PropertyInfo t4525____Current_PropertyInfo = 
{
	&t4525_TI, "Current", &m30392_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4525_PIs[] =
{
	&t4525____Current_PropertyInfo,
	NULL
};
extern Il2CppType t578_0_0_0;
extern void* RuntimeInvoker_t578 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30392_GM;
MethodInfo m30392_MI = 
{
	"get_Current", NULL, &t4525_TI, &t578_0_0_0, RuntimeInvoker_t578, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30392_GM};
static MethodInfo* t4525_MIs[] =
{
	&m30392_MI,
	NULL
};
static TypeInfo* t4525_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4525_0_0_0;
extern Il2CppType t4525_1_0_0;
struct t4525;
extern Il2CppGenericClass t4525_GC;
TypeInfo t4525_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4525_MIs, t4525_PIs, NULL, NULL, NULL, NULL, NULL, &t4525_TI, t4525_ITIs, NULL, &EmptyCustomAttributesCache, &t4525_TI, &t4525_0_0_0, &t4525_1_0_0, NULL, &t4525_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3173.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3173_TI;
#include "t3173MD.h"

extern TypeInfo t578_TI;
extern MethodInfo m17633_MI;
extern MethodInfo m23181_MI;
struct t20;
 int32_t m23181 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17629_MI;
 void m17629 (t3173 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17630_MI;
 t29 * m17630 (t3173 * __this, MethodInfo* method){
	{
		int32_t L_0 = m17633(__this, &m17633_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t578_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17631_MI;
 void m17631 (t3173 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17632_MI;
 bool m17632 (t3173 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m17633 (t3173 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m23181(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23181_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRules>
extern Il2CppType t20_0_0_1;
FieldInfo t3173_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3173_TI, offsetof(t3173, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3173_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3173_TI, offsetof(t3173, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3173_FIs[] =
{
	&t3173_f0_FieldInfo,
	&t3173_f1_FieldInfo,
	NULL
};
static PropertyInfo t3173____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3173_TI, "System.Collections.IEnumerator.Current", &m17630_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3173____Current_PropertyInfo = 
{
	&t3173_TI, "Current", &m17633_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3173_PIs[] =
{
	&t3173____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3173____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3173_m17629_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17629_GM;
MethodInfo m17629_MI = 
{
	".ctor", (methodPointerType)&m17629, &t3173_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3173_m17629_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17629_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17630_GM;
MethodInfo m17630_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17630, &t3173_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17630_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17631_GM;
MethodInfo m17631_MI = 
{
	"Dispose", (methodPointerType)&m17631, &t3173_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17631_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17632_GM;
MethodInfo m17632_MI = 
{
	"MoveNext", (methodPointerType)&m17632, &t3173_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17632_GM};
extern Il2CppType t578_0_0_0;
extern void* RuntimeInvoker_t578 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17633_GM;
MethodInfo m17633_MI = 
{
	"get_Current", (methodPointerType)&m17633, &t3173_TI, &t578_0_0_0, RuntimeInvoker_t578, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17633_GM};
static MethodInfo* t3173_MIs[] =
{
	&m17629_MI,
	&m17630_MI,
	&m17631_MI,
	&m17632_MI,
	&m17633_MI,
	NULL
};
static MethodInfo* t3173_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17630_MI,
	&m17632_MI,
	&m17631_MI,
	&m17633_MI,
};
static TypeInfo* t3173_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4525_TI,
};
static Il2CppInterfaceOffsetPair t3173_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4525_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3173_0_0_0;
extern Il2CppType t3173_1_0_0;
extern Il2CppGenericClass t3173_GC;
TypeInfo t3173_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3173_MIs, t3173_PIs, t3173_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3173_TI, t3173_ITIs, t3173_VT, &EmptyCustomAttributesCache, &t3173_TI, &t3173_0_0_0, &t3173_1_0_0, t3173_IOs, &t3173_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3173)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5811_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRules>
extern MethodInfo m30393_MI;
static PropertyInfo t5811____Count_PropertyInfo = 
{
	&t5811_TI, "Count", &m30393_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30394_MI;
static PropertyInfo t5811____IsReadOnly_PropertyInfo = 
{
	&t5811_TI, "IsReadOnly", &m30394_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5811_PIs[] =
{
	&t5811____Count_PropertyInfo,
	&t5811____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30393_GM;
MethodInfo m30393_MI = 
{
	"get_Count", NULL, &t5811_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30393_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30394_GM;
MethodInfo m30394_MI = 
{
	"get_IsReadOnly", NULL, &t5811_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30394_GM};
extern Il2CppType t578_0_0_0;
extern Il2CppType t578_0_0_0;
static ParameterInfo t5811_m30395_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t578_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30395_GM;
MethodInfo m30395_MI = 
{
	"Add", NULL, &t5811_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5811_m30395_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30395_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30396_GM;
MethodInfo m30396_MI = 
{
	"Clear", NULL, &t5811_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30396_GM};
extern Il2CppType t578_0_0_0;
static ParameterInfo t5811_m30397_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t578_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30397_GM;
MethodInfo m30397_MI = 
{
	"Contains", NULL, &t5811_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5811_m30397_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30397_GM};
extern Il2CppType t3796_0_0_0;
extern Il2CppType t3796_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5811_m30398_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3796_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30398_GM;
MethodInfo m30398_MI = 
{
	"CopyTo", NULL, &t5811_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5811_m30398_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30398_GM};
extern Il2CppType t578_0_0_0;
static ParameterInfo t5811_m30399_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t578_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30399_GM;
MethodInfo m30399_MI = 
{
	"Remove", NULL, &t5811_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5811_m30399_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30399_GM};
static MethodInfo* t5811_MIs[] =
{
	&m30393_MI,
	&m30394_MI,
	&m30395_MI,
	&m30396_MI,
	&m30397_MI,
	&m30398_MI,
	&m30399_MI,
	NULL
};
extern TypeInfo t5813_TI;
static TypeInfo* t5811_ITIs[] = 
{
	&t603_TI,
	&t5813_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5811_0_0_0;
extern Il2CppType t5811_1_0_0;
struct t5811;
extern Il2CppGenericClass t5811_GC;
TypeInfo t5811_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5811_MIs, t5811_PIs, NULL, NULL, NULL, NULL, NULL, &t5811_TI, t5811_ITIs, NULL, &EmptyCustomAttributesCache, &t5811_TI, &t5811_0_0_0, &t5811_1_0_0, NULL, &t5811_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngineInternal.TypeInferenceRules>
extern Il2CppType t4525_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30400_GM;
MethodInfo m30400_MI = 
{
	"GetEnumerator", NULL, &t5813_TI, &t4525_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30400_GM};
static MethodInfo* t5813_MIs[] =
{
	&m30400_MI,
	NULL
};
static TypeInfo* t5813_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5813_0_0_0;
extern Il2CppType t5813_1_0_0;
struct t5813;
extern Il2CppGenericClass t5813_GC;
TypeInfo t5813_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5813_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5813_TI, t5813_ITIs, NULL, &EmptyCustomAttributesCache, &t5813_TI, &t5813_0_0_0, &t5813_1_0_0, NULL, &t5813_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5812_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRules>
extern MethodInfo m30401_MI;
extern MethodInfo m30402_MI;
static PropertyInfo t5812____Item_PropertyInfo = 
{
	&t5812_TI, "Item", &m30401_MI, &m30402_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5812_PIs[] =
{
	&t5812____Item_PropertyInfo,
	NULL
};
extern Il2CppType t578_0_0_0;
static ParameterInfo t5812_m30403_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t578_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30403_GM;
MethodInfo m30403_MI = 
{
	"IndexOf", NULL, &t5812_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5812_m30403_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30403_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t578_0_0_0;
static ParameterInfo t5812_m30404_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t578_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30404_GM;
MethodInfo m30404_MI = 
{
	"Insert", NULL, &t5812_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5812_m30404_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30404_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5812_m30405_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30405_GM;
MethodInfo m30405_MI = 
{
	"RemoveAt", NULL, &t5812_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5812_m30405_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30405_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5812_m30401_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t578_0_0_0;
extern void* RuntimeInvoker_t578_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30401_GM;
MethodInfo m30401_MI = 
{
	"get_Item", NULL, &t5812_TI, &t578_0_0_0, RuntimeInvoker_t578_t44, t5812_m30401_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30401_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t578_0_0_0;
static ParameterInfo t5812_m30402_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t578_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30402_GM;
MethodInfo m30402_MI = 
{
	"set_Item", NULL, &t5812_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5812_m30402_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30402_GM};
static MethodInfo* t5812_MIs[] =
{
	&m30403_MI,
	&m30404_MI,
	&m30405_MI,
	&m30401_MI,
	&m30402_MI,
	NULL
};
static TypeInfo* t5812_ITIs[] = 
{
	&t603_TI,
	&t5811_TI,
	&t5813_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5812_0_0_0;
extern Il2CppType t5812_1_0_0;
struct t5812;
extern Il2CppGenericClass t5812_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5812_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5812_MIs, t5812_PIs, NULL, NULL, NULL, NULL, NULL, &t5812_TI, t5812_ITIs, NULL, &t1908__CustomAttributeCache, &t5812_TI, &t5812_0_0_0, &t5812_1_0_0, NULL, &t5812_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4527_TI;

#include "t579.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngineInternal.TypeInferenceRuleAttribute>
extern MethodInfo m30406_MI;
static PropertyInfo t4527____Current_PropertyInfo = 
{
	&t4527_TI, "Current", &m30406_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4527_PIs[] =
{
	&t4527____Current_PropertyInfo,
	NULL
};
extern Il2CppType t579_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30406_GM;
MethodInfo m30406_MI = 
{
	"get_Current", NULL, &t4527_TI, &t579_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30406_GM};
static MethodInfo* t4527_MIs[] =
{
	&m30406_MI,
	NULL
};
static TypeInfo* t4527_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4527_0_0_0;
extern Il2CppType t4527_1_0_0;
struct t4527;
extern Il2CppGenericClass t4527_GC;
TypeInfo t4527_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4527_MIs, t4527_PIs, NULL, NULL, NULL, NULL, NULL, &t4527_TI, t4527_ITIs, NULL, &EmptyCustomAttributesCache, &t4527_TI, &t4527_0_0_0, &t4527_1_0_0, NULL, &t4527_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3174.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3174_TI;
#include "t3174MD.h"

extern TypeInfo t579_TI;
extern MethodInfo m17638_MI;
extern MethodInfo m23192_MI;
struct t20;
#define m23192(__this, p0, method) (t579 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRuleAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3174_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3174_TI, offsetof(t3174, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3174_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3174_TI, offsetof(t3174, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3174_FIs[] =
{
	&t3174_f0_FieldInfo,
	&t3174_f1_FieldInfo,
	NULL
};
extern MethodInfo m17635_MI;
static PropertyInfo t3174____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3174_TI, "System.Collections.IEnumerator.Current", &m17635_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3174____Current_PropertyInfo = 
{
	&t3174_TI, "Current", &m17638_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3174_PIs[] =
{
	&t3174____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3174____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3174_m17634_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17634_GM;
MethodInfo m17634_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3174_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3174_m17634_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17634_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17635_GM;
MethodInfo m17635_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3174_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17635_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17636_GM;
MethodInfo m17636_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3174_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17636_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17637_GM;
MethodInfo m17637_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3174_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17637_GM};
extern Il2CppType t579_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17638_GM;
MethodInfo m17638_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3174_TI, &t579_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17638_GM};
static MethodInfo* t3174_MIs[] =
{
	&m17634_MI,
	&m17635_MI,
	&m17636_MI,
	&m17637_MI,
	&m17638_MI,
	NULL
};
extern MethodInfo m17637_MI;
extern MethodInfo m17636_MI;
static MethodInfo* t3174_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17635_MI,
	&m17637_MI,
	&m17636_MI,
	&m17638_MI,
};
static TypeInfo* t3174_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4527_TI,
};
static Il2CppInterfaceOffsetPair t3174_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4527_TI, 7},
};
extern TypeInfo t579_TI;
static Il2CppRGCTXData t3174_RGCTXData[3] = 
{
	&m17638_MI/* Method Usage */,
	&t579_TI/* Class Usage */,
	&m23192_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3174_0_0_0;
extern Il2CppType t3174_1_0_0;
extern Il2CppGenericClass t3174_GC;
TypeInfo t3174_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3174_MIs, t3174_PIs, t3174_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3174_TI, t3174_ITIs, t3174_VT, &EmptyCustomAttributesCache, &t3174_TI, &t3174_0_0_0, &t3174_1_0_0, t3174_IOs, &t3174_GC, NULL, NULL, NULL, t3174_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3174)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5814_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRuleAttribute>
extern MethodInfo m30407_MI;
static PropertyInfo t5814____Count_PropertyInfo = 
{
	&t5814_TI, "Count", &m30407_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30408_MI;
static PropertyInfo t5814____IsReadOnly_PropertyInfo = 
{
	&t5814_TI, "IsReadOnly", &m30408_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5814_PIs[] =
{
	&t5814____Count_PropertyInfo,
	&t5814____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30407_GM;
MethodInfo m30407_MI = 
{
	"get_Count", NULL, &t5814_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30407_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30408_GM;
MethodInfo m30408_MI = 
{
	"get_IsReadOnly", NULL, &t5814_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30408_GM};
extern Il2CppType t579_0_0_0;
extern Il2CppType t579_0_0_0;
static ParameterInfo t5814_m30409_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t579_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30409_GM;
MethodInfo m30409_MI = 
{
	"Add", NULL, &t5814_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5814_m30409_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30409_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30410_GM;
MethodInfo m30410_MI = 
{
	"Clear", NULL, &t5814_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30410_GM};
extern Il2CppType t579_0_0_0;
static ParameterInfo t5814_m30411_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t579_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30411_GM;
MethodInfo m30411_MI = 
{
	"Contains", NULL, &t5814_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5814_m30411_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30411_GM};
extern Il2CppType t3797_0_0_0;
extern Il2CppType t3797_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5814_m30412_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3797_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30412_GM;
MethodInfo m30412_MI = 
{
	"CopyTo", NULL, &t5814_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5814_m30412_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30412_GM};
extern Il2CppType t579_0_0_0;
static ParameterInfo t5814_m30413_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t579_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30413_GM;
MethodInfo m30413_MI = 
{
	"Remove", NULL, &t5814_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5814_m30413_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30413_GM};
static MethodInfo* t5814_MIs[] =
{
	&m30407_MI,
	&m30408_MI,
	&m30409_MI,
	&m30410_MI,
	&m30411_MI,
	&m30412_MI,
	&m30413_MI,
	NULL
};
extern TypeInfo t5816_TI;
static TypeInfo* t5814_ITIs[] = 
{
	&t603_TI,
	&t5816_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5814_0_0_0;
extern Il2CppType t5814_1_0_0;
struct t5814;
extern Il2CppGenericClass t5814_GC;
TypeInfo t5814_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5814_MIs, t5814_PIs, NULL, NULL, NULL, NULL, NULL, &t5814_TI, t5814_ITIs, NULL, &EmptyCustomAttributesCache, &t5814_TI, &t5814_0_0_0, &t5814_1_0_0, NULL, &t5814_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngineInternal.TypeInferenceRuleAttribute>
extern Il2CppType t4527_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30414_GM;
MethodInfo m30414_MI = 
{
	"GetEnumerator", NULL, &t5816_TI, &t4527_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30414_GM};
static MethodInfo* t5816_MIs[] =
{
	&m30414_MI,
	NULL
};
static TypeInfo* t5816_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5816_0_0_0;
extern Il2CppType t5816_1_0_0;
struct t5816;
extern Il2CppGenericClass t5816_GC;
TypeInfo t5816_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5816_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5816_TI, t5816_ITIs, NULL, &EmptyCustomAttributesCache, &t5816_TI, &t5816_0_0_0, &t5816_1_0_0, NULL, &t5816_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5815_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRuleAttribute>
extern MethodInfo m30415_MI;
extern MethodInfo m30416_MI;
static PropertyInfo t5815____Item_PropertyInfo = 
{
	&t5815_TI, "Item", &m30415_MI, &m30416_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5815_PIs[] =
{
	&t5815____Item_PropertyInfo,
	NULL
};
extern Il2CppType t579_0_0_0;
static ParameterInfo t5815_m30417_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t579_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30417_GM;
MethodInfo m30417_MI = 
{
	"IndexOf", NULL, &t5815_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5815_m30417_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30417_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t579_0_0_0;
static ParameterInfo t5815_m30418_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t579_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30418_GM;
MethodInfo m30418_MI = 
{
	"Insert", NULL, &t5815_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5815_m30418_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30418_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5815_m30419_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30419_GM;
MethodInfo m30419_MI = 
{
	"RemoveAt", NULL, &t5815_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5815_m30419_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30419_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5815_m30415_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t579_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30415_GM;
MethodInfo m30415_MI = 
{
	"get_Item", NULL, &t5815_TI, &t579_0_0_0, RuntimeInvoker_t29_t44, t5815_m30415_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30415_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t579_0_0_0;
static ParameterInfo t5815_m30416_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t579_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30416_GM;
MethodInfo m30416_MI = 
{
	"set_Item", NULL, &t5815_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5815_m30416_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30416_GM};
static MethodInfo* t5815_MIs[] =
{
	&m30417_MI,
	&m30418_MI,
	&m30419_MI,
	&m30415_MI,
	&m30416_MI,
	NULL
};
static TypeInfo* t5815_ITIs[] = 
{
	&t603_TI,
	&t5814_TI,
	&t5816_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5815_0_0_0;
extern Il2CppType t5815_1_0_0;
struct t5815;
extern Il2CppGenericClass t5815_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5815_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5815_MIs, t5815_PIs, NULL, NULL, NULL, NULL, NULL, &t5815_TI, t5815_ITIs, NULL, &t1908__CustomAttributeCache, &t5815_TI, &t5815_0_0_0, &t5815_1_0_0, NULL, &t5815_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4529_TI;

#include "t686.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.CompilerServices.ExtensionAttribute>
extern MethodInfo m30420_MI;
static PropertyInfo t4529____Current_PropertyInfo = 
{
	&t4529_TI, "Current", &m30420_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4529_PIs[] =
{
	&t4529____Current_PropertyInfo,
	NULL
};
extern Il2CppType t686_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30420_GM;
MethodInfo m30420_MI = 
{
	"get_Current", NULL, &t4529_TI, &t686_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30420_GM};
static MethodInfo* t4529_MIs[] =
{
	&m30420_MI,
	NULL
};
static TypeInfo* t4529_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4529_0_0_0;
extern Il2CppType t4529_1_0_0;
struct t4529;
extern Il2CppGenericClass t4529_GC;
TypeInfo t4529_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4529_MIs, t4529_PIs, NULL, NULL, NULL, NULL, NULL, &t4529_TI, t4529_ITIs, NULL, &EmptyCustomAttributesCache, &t4529_TI, &t4529_0_0_0, &t4529_1_0_0, NULL, &t4529_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3175.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3175_TI;
#include "t3175MD.h"

extern TypeInfo t686_TI;
extern MethodInfo m17643_MI;
extern MethodInfo m23203_MI;
struct t20;
#define m23203(__this, p0, method) (t686 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.ExtensionAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3175_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3175_TI, offsetof(t3175, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3175_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3175_TI, offsetof(t3175, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3175_FIs[] =
{
	&t3175_f0_FieldInfo,
	&t3175_f1_FieldInfo,
	NULL
};
extern MethodInfo m17640_MI;
static PropertyInfo t3175____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3175_TI, "System.Collections.IEnumerator.Current", &m17640_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3175____Current_PropertyInfo = 
{
	&t3175_TI, "Current", &m17643_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3175_PIs[] =
{
	&t3175____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3175____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3175_m17639_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17639_GM;
MethodInfo m17639_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3175_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3175_m17639_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17639_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17640_GM;
MethodInfo m17640_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3175_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17640_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17641_GM;
MethodInfo m17641_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3175_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17641_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17642_GM;
MethodInfo m17642_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3175_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17642_GM};
extern Il2CppType t686_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17643_GM;
MethodInfo m17643_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3175_TI, &t686_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17643_GM};
static MethodInfo* t3175_MIs[] =
{
	&m17639_MI,
	&m17640_MI,
	&m17641_MI,
	&m17642_MI,
	&m17643_MI,
	NULL
};
extern MethodInfo m17642_MI;
extern MethodInfo m17641_MI;
static MethodInfo* t3175_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17640_MI,
	&m17642_MI,
	&m17641_MI,
	&m17643_MI,
};
static TypeInfo* t3175_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4529_TI,
};
static Il2CppInterfaceOffsetPair t3175_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4529_TI, 7},
};
extern TypeInfo t686_TI;
static Il2CppRGCTXData t3175_RGCTXData[3] = 
{
	&m17643_MI/* Method Usage */,
	&t686_TI/* Class Usage */,
	&m23203_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3175_0_0_0;
extern Il2CppType t3175_1_0_0;
extern Il2CppGenericClass t3175_GC;
TypeInfo t3175_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3175_MIs, t3175_PIs, t3175_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3175_TI, t3175_ITIs, t3175_VT, &EmptyCustomAttributesCache, &t3175_TI, &t3175_0_0_0, &t3175_1_0_0, t3175_IOs, &t3175_GC, NULL, NULL, NULL, t3175_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3175)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5817_TI;

#include "System.Core_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.ExtensionAttribute>
extern MethodInfo m30421_MI;
static PropertyInfo t5817____Count_PropertyInfo = 
{
	&t5817_TI, "Count", &m30421_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30422_MI;
static PropertyInfo t5817____IsReadOnly_PropertyInfo = 
{
	&t5817_TI, "IsReadOnly", &m30422_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5817_PIs[] =
{
	&t5817____Count_PropertyInfo,
	&t5817____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30421_GM;
MethodInfo m30421_MI = 
{
	"get_Count", NULL, &t5817_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30421_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30422_GM;
MethodInfo m30422_MI = 
{
	"get_IsReadOnly", NULL, &t5817_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30422_GM};
extern Il2CppType t686_0_0_0;
extern Il2CppType t686_0_0_0;
static ParameterInfo t5817_m30423_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t686_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30423_GM;
MethodInfo m30423_MI = 
{
	"Add", NULL, &t5817_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5817_m30423_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30423_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30424_GM;
MethodInfo m30424_MI = 
{
	"Clear", NULL, &t5817_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30424_GM};
extern Il2CppType t686_0_0_0;
static ParameterInfo t5817_m30425_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t686_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30425_GM;
MethodInfo m30425_MI = 
{
	"Contains", NULL, &t5817_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5817_m30425_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30425_GM};
extern Il2CppType t3895_0_0_0;
extern Il2CppType t3895_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5817_m30426_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3895_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30426_GM;
MethodInfo m30426_MI = 
{
	"CopyTo", NULL, &t5817_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5817_m30426_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30426_GM};
extern Il2CppType t686_0_0_0;
static ParameterInfo t5817_m30427_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t686_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30427_GM;
MethodInfo m30427_MI = 
{
	"Remove", NULL, &t5817_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5817_m30427_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30427_GM};
static MethodInfo* t5817_MIs[] =
{
	&m30421_MI,
	&m30422_MI,
	&m30423_MI,
	&m30424_MI,
	&m30425_MI,
	&m30426_MI,
	&m30427_MI,
	NULL
};
extern TypeInfo t5819_TI;
static TypeInfo* t5817_ITIs[] = 
{
	&t603_TI,
	&t5819_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5817_0_0_0;
extern Il2CppType t5817_1_0_0;
struct t5817;
extern Il2CppGenericClass t5817_GC;
TypeInfo t5817_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5817_MIs, t5817_PIs, NULL, NULL, NULL, NULL, NULL, &t5817_TI, t5817_ITIs, NULL, &EmptyCustomAttributesCache, &t5817_TI, &t5817_0_0_0, &t5817_1_0_0, NULL, &t5817_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.CompilerServices.ExtensionAttribute>
extern Il2CppType t4529_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30428_GM;
MethodInfo m30428_MI = 
{
	"GetEnumerator", NULL, &t5819_TI, &t4529_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30428_GM};
static MethodInfo* t5819_MIs[] =
{
	&m30428_MI,
	NULL
};
static TypeInfo* t5819_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5819_0_0_0;
extern Il2CppType t5819_1_0_0;
struct t5819;
extern Il2CppGenericClass t5819_GC;
TypeInfo t5819_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5819_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5819_TI, t5819_ITIs, NULL, &EmptyCustomAttributesCache, &t5819_TI, &t5819_0_0_0, &t5819_1_0_0, NULL, &t5819_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5818_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.CompilerServices.ExtensionAttribute>
extern MethodInfo m30429_MI;
extern MethodInfo m30430_MI;
static PropertyInfo t5818____Item_PropertyInfo = 
{
	&t5818_TI, "Item", &m30429_MI, &m30430_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5818_PIs[] =
{
	&t5818____Item_PropertyInfo,
	NULL
};
extern Il2CppType t686_0_0_0;
static ParameterInfo t5818_m30431_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t686_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30431_GM;
MethodInfo m30431_MI = 
{
	"IndexOf", NULL, &t5818_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5818_m30431_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30431_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t686_0_0_0;
static ParameterInfo t5818_m30432_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t686_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30432_GM;
MethodInfo m30432_MI = 
{
	"Insert", NULL, &t5818_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5818_m30432_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30432_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5818_m30433_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30433_GM;
MethodInfo m30433_MI = 
{
	"RemoveAt", NULL, &t5818_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5818_m30433_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30433_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5818_m30429_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t686_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30429_GM;
MethodInfo m30429_MI = 
{
	"get_Item", NULL, &t5818_TI, &t686_0_0_0, RuntimeInvoker_t29_t44, t5818_m30429_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30429_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t686_0_0_0;
static ParameterInfo t5818_m30430_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t686_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30430_GM;
MethodInfo m30430_MI = 
{
	"set_Item", NULL, &t5818_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5818_m30430_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30430_GM};
static MethodInfo* t5818_MIs[] =
{
	&m30431_MI,
	&m30432_MI,
	&m30433_MI,
	&m30429_MI,
	&m30430_MI,
	NULL
};
static TypeInfo* t5818_ITIs[] = 
{
	&t603_TI,
	&t5817_TI,
	&t5819_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5818_0_0_0;
extern Il2CppType t5818_1_0_0;
struct t5818;
extern Il2CppGenericClass t5818_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5818_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5818_MIs, t5818_PIs, NULL, NULL, NULL, NULL, NULL, &t5818_TI, t5818_ITIs, NULL, &t1908__CustomAttributeCache, &t5818_TI, &t5818_0_0_0, &t5818_1_0_0, NULL, &t5818_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t3176.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3176_TI;
#include "t3176MD.h"



extern MethodInfo m17644_MI;
 void m17644_gshared (t3176 * __this, t29 * p0, t35 p1, MethodInfo* method)
{
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m17645_MI;
 t29 * m17645_gshared (t3176 * __this, t29 * p0, MethodInfo* method)
{
	if(__this->f9 != NULL)
	{
		m17645((t3176 *)__this->f9,p0, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef t29 * (*FunctionPointerType) (t29 *, t29 * __this, t29 * p0, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef t29 * (*FunctionPointerType) (t29 * __this, t29 * p0, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef t29 * (*FunctionPointerType) (t29 * __this, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m17646_MI;
 t29 * m17646_gshared (t3176 * __this, t29 * p0, t67 * p1, t29 * p2, MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = p0;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p1, (Il2CppObject*)p2);
}
extern MethodInfo m17647_MI;
 t29 * m17647_gshared (t3176 * __this, t29 * p0, MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return (t29 *)__result;
}
// Metadata Definition System.Func`2<System.Object,System.Object>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t3176_m17644_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17644_GM;
MethodInfo m17644_MI = 
{
	".ctor", (methodPointerType)&m17644_gshared, &t3176_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t3176_m17644_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m17644_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3176_m17645_ParameterInfos[] = 
{
	{"arg1", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17645_GM;
MethodInfo m17645_MI = 
{
	"Invoke", (methodPointerType)&m17645_gshared, &t3176_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t3176_m17645_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17645_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3176_m17646_ParameterInfos[] = 
{
	{"arg1", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17646_GM;
MethodInfo m17646_MI = 
{
	"BeginInvoke", (methodPointerType)&m17646_gshared, &t3176_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t3176_m17646_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m17646_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t3176_m17647_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17647_GM;
MethodInfo m17647_MI = 
{
	"EndInvoke", (methodPointerType)&m17647_gshared, &t3176_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t3176_m17647_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17647_GM};
static MethodInfo* t3176_MIs[] =
{
	&m17644_MI,
	&m17645_MI,
	&m17646_MI,
	&m17647_MI,
	NULL
};
static MethodInfo* t3176_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m17645_MI,
	&m17646_MI,
	&m17647_MI,
};
static Il2CppInterfaceOffsetPair t3176_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType t3176_0_0_0;
extern Il2CppType t3176_1_0_0;
struct t3176;
extern Il2CppGenericClass t3176_GC;
TypeInfo t3176_TI = 
{
	&g_System_Core_dll_Image, NULL, "Func`2", "System", t3176_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t3176_TI, NULL, t3176_VT, &EmptyCustomAttributesCache, &t3176_TI, &t3176_0_0_0, &t3176_1_0_0, t3176_IOs, &t3176_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3176), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4531_TI;

#include "t715.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.MonoTODOAttribute>
extern MethodInfo m30434_MI;
static PropertyInfo t4531____Current_PropertyInfo = 
{
	&t4531_TI, "Current", &m30434_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4531_PIs[] =
{
	&t4531____Current_PropertyInfo,
	NULL
};
extern Il2CppType t715_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30434_GM;
MethodInfo m30434_MI = 
{
	"get_Current", NULL, &t4531_TI, &t715_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30434_GM};
static MethodInfo* t4531_MIs[] =
{
	&m30434_MI,
	NULL
};
static TypeInfo* t4531_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4531_0_0_0;
extern Il2CppType t4531_1_0_0;
struct t4531;
extern Il2CppGenericClass t4531_GC;
TypeInfo t4531_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4531_MIs, t4531_PIs, NULL, NULL, NULL, NULL, NULL, &t4531_TI, t4531_ITIs, NULL, &EmptyCustomAttributesCache, &t4531_TI, &t4531_0_0_0, &t4531_1_0_0, NULL, &t4531_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3177.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3177_TI;
#include "t3177MD.h"

extern TypeInfo t715_TI;
extern MethodInfo m17652_MI;
extern MethodInfo m23214_MI;
struct t20;
#define m23214(__this, p0, method) (t715 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.MonoTODOAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3177_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3177_TI, offsetof(t3177, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3177_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3177_TI, offsetof(t3177, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3177_FIs[] =
{
	&t3177_f0_FieldInfo,
	&t3177_f1_FieldInfo,
	NULL
};
extern MethodInfo m17649_MI;
static PropertyInfo t3177____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3177_TI, "System.Collections.IEnumerator.Current", &m17649_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3177____Current_PropertyInfo = 
{
	&t3177_TI, "Current", &m17652_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3177_PIs[] =
{
	&t3177____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3177____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3177_m17648_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17648_GM;
MethodInfo m17648_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3177_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3177_m17648_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17648_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17649_GM;
MethodInfo m17649_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3177_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17649_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17650_GM;
MethodInfo m17650_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3177_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17650_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17651_GM;
MethodInfo m17651_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3177_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17651_GM};
extern Il2CppType t715_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17652_GM;
MethodInfo m17652_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3177_TI, &t715_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17652_GM};
static MethodInfo* t3177_MIs[] =
{
	&m17648_MI,
	&m17649_MI,
	&m17650_MI,
	&m17651_MI,
	&m17652_MI,
	NULL
};
extern MethodInfo m17651_MI;
extern MethodInfo m17650_MI;
static MethodInfo* t3177_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17649_MI,
	&m17651_MI,
	&m17650_MI,
	&m17652_MI,
};
static TypeInfo* t3177_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4531_TI,
};
static Il2CppInterfaceOffsetPair t3177_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4531_TI, 7},
};
extern TypeInfo t715_TI;
static Il2CppRGCTXData t3177_RGCTXData[3] = 
{
	&m17652_MI/* Method Usage */,
	&t715_TI/* Class Usage */,
	&m23214_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3177_0_0_0;
extern Il2CppType t3177_1_0_0;
extern Il2CppGenericClass t3177_GC;
TypeInfo t3177_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3177_MIs, t3177_PIs, t3177_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3177_TI, t3177_ITIs, t3177_VT, &EmptyCustomAttributesCache, &t3177_TI, &t3177_0_0_0, &t3177_1_0_0, t3177_IOs, &t3177_GC, NULL, NULL, NULL, t3177_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3177)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5820_TI;

#include "System_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>
extern MethodInfo m30435_MI;
static PropertyInfo t5820____Count_PropertyInfo = 
{
	&t5820_TI, "Count", &m30435_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30436_MI;
static PropertyInfo t5820____IsReadOnly_PropertyInfo = 
{
	&t5820_TI, "IsReadOnly", &m30436_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5820_PIs[] =
{
	&t5820____Count_PropertyInfo,
	&t5820____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30435_GM;
MethodInfo m30435_MI = 
{
	"get_Count", NULL, &t5820_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30435_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30436_GM;
MethodInfo m30436_MI = 
{
	"get_IsReadOnly", NULL, &t5820_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30436_GM};
extern Il2CppType t715_0_0_0;
extern Il2CppType t715_0_0_0;
static ParameterInfo t5820_m30437_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t715_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30437_GM;
MethodInfo m30437_MI = 
{
	"Add", NULL, &t5820_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5820_m30437_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30437_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30438_GM;
MethodInfo m30438_MI = 
{
	"Clear", NULL, &t5820_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30438_GM};
extern Il2CppType t715_0_0_0;
static ParameterInfo t5820_m30439_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t715_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30439_GM;
MethodInfo m30439_MI = 
{
	"Contains", NULL, &t5820_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5820_m30439_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30439_GM};
extern Il2CppType t3896_0_0_0;
extern Il2CppType t3896_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5820_m30440_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3896_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30440_GM;
MethodInfo m30440_MI = 
{
	"CopyTo", NULL, &t5820_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5820_m30440_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30440_GM};
extern Il2CppType t715_0_0_0;
static ParameterInfo t5820_m30441_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t715_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30441_GM;
MethodInfo m30441_MI = 
{
	"Remove", NULL, &t5820_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5820_m30441_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30441_GM};
static MethodInfo* t5820_MIs[] =
{
	&m30435_MI,
	&m30436_MI,
	&m30437_MI,
	&m30438_MI,
	&m30439_MI,
	&m30440_MI,
	&m30441_MI,
	NULL
};
extern TypeInfo t5822_TI;
static TypeInfo* t5820_ITIs[] = 
{
	&t603_TI,
	&t5822_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5820_0_0_0;
extern Il2CppType t5820_1_0_0;
struct t5820;
extern Il2CppGenericClass t5820_GC;
TypeInfo t5820_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5820_MIs, t5820_PIs, NULL, NULL, NULL, NULL, NULL, &t5820_TI, t5820_ITIs, NULL, &EmptyCustomAttributesCache, &t5820_TI, &t5820_0_0_0, &t5820_1_0_0, NULL, &t5820_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.MonoTODOAttribute>
extern Il2CppType t4531_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30442_GM;
MethodInfo m30442_MI = 
{
	"GetEnumerator", NULL, &t5822_TI, &t4531_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30442_GM};
static MethodInfo* t5822_MIs[] =
{
	&m30442_MI,
	NULL
};
static TypeInfo* t5822_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5822_0_0_0;
extern Il2CppType t5822_1_0_0;
struct t5822;
extern Il2CppGenericClass t5822_GC;
TypeInfo t5822_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5822_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5822_TI, t5822_ITIs, NULL, &EmptyCustomAttributesCache, &t5822_TI, &t5822_0_0_0, &t5822_1_0_0, NULL, &t5822_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5821_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.MonoTODOAttribute>
extern MethodInfo m30443_MI;
extern MethodInfo m30444_MI;
static PropertyInfo t5821____Item_PropertyInfo = 
{
	&t5821_TI, "Item", &m30443_MI, &m30444_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5821_PIs[] =
{
	&t5821____Item_PropertyInfo,
	NULL
};
extern Il2CppType t715_0_0_0;
static ParameterInfo t5821_m30445_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t715_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30445_GM;
MethodInfo m30445_MI = 
{
	"IndexOf", NULL, &t5821_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5821_m30445_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30445_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t715_0_0_0;
static ParameterInfo t5821_m30446_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t715_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30446_GM;
MethodInfo m30446_MI = 
{
	"Insert", NULL, &t5821_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5821_m30446_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30446_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5821_m30447_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30447_GM;
MethodInfo m30447_MI = 
{
	"RemoveAt", NULL, &t5821_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5821_m30447_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30447_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5821_m30443_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t715_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30443_GM;
MethodInfo m30443_MI = 
{
	"get_Item", NULL, &t5821_TI, &t715_0_0_0, RuntimeInvoker_t29_t44, t5821_m30443_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30443_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t715_0_0_0;
static ParameterInfo t5821_m30444_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t715_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30444_GM;
MethodInfo m30444_MI = 
{
	"set_Item", NULL, &t5821_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5821_m30444_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30444_GM};
static MethodInfo* t5821_MIs[] =
{
	&m30445_MI,
	&m30446_MI,
	&m30447_MI,
	&m30443_MI,
	&m30444_MI,
	NULL
};
static TypeInfo* t5821_ITIs[] = 
{
	&t603_TI,
	&t5820_TI,
	&t5822_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5821_0_0_0;
extern Il2CppType t5821_1_0_0;
struct t5821;
extern Il2CppGenericClass t5821_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5821_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5821_MIs, t5821_PIs, NULL, NULL, NULL, NULL, NULL, &t5821_TI, t5821_ITIs, NULL, &t1908__CustomAttributeCache, &t5821_TI, &t5821_0_0_0, &t5821_1_0_0, NULL, &t5821_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4533_TI;

#include "t606.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>
extern MethodInfo m30448_MI;
static PropertyInfo t4533____Current_PropertyInfo = 
{
	&t4533_TI, "Current", &m30448_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4533_PIs[] =
{
	&t4533____Current_PropertyInfo,
	NULL
};
extern Il2CppType t606_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30448_GM;
MethodInfo m30448_MI = 
{
	"get_Current", NULL, &t4533_TI, &t606_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30448_GM};
static MethodInfo* t4533_MIs[] =
{
	&m30448_MI,
	NULL
};
static TypeInfo* t4533_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4533_0_0_0;
extern Il2CppType t4533_1_0_0;
struct t4533;
extern Il2CppGenericClass t4533_GC;
TypeInfo t4533_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4533_MIs, t4533_PIs, NULL, NULL, NULL, NULL, NULL, &t4533_TI, t4533_ITIs, NULL, &EmptyCustomAttributesCache, &t4533_TI, &t4533_0_0_0, &t4533_1_0_0, NULL, &t4533_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3178.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3178_TI;
#include "t3178MD.h"

extern TypeInfo t606_TI;
extern MethodInfo m17657_MI;
extern MethodInfo m23225_MI;
struct t20;
#define m23225(__this, p0, method) (t606 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3178_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3178_TI, offsetof(t3178, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3178_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3178_TI, offsetof(t3178, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3178_FIs[] =
{
	&t3178_f0_FieldInfo,
	&t3178_f1_FieldInfo,
	NULL
};
extern MethodInfo m17654_MI;
static PropertyInfo t3178____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3178_TI, "System.Collections.IEnumerator.Current", &m17654_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3178____Current_PropertyInfo = 
{
	&t3178_TI, "Current", &m17657_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3178_PIs[] =
{
	&t3178____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3178____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3178_m17653_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17653_GM;
MethodInfo m17653_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3178_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3178_m17653_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17653_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17654_GM;
MethodInfo m17654_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3178_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17654_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17655_GM;
MethodInfo m17655_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3178_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17655_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17656_GM;
MethodInfo m17656_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3178_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17656_GM};
extern Il2CppType t606_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17657_GM;
MethodInfo m17657_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3178_TI, &t606_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17657_GM};
static MethodInfo* t3178_MIs[] =
{
	&m17653_MI,
	&m17654_MI,
	&m17655_MI,
	&m17656_MI,
	&m17657_MI,
	NULL
};
extern MethodInfo m17656_MI;
extern MethodInfo m17655_MI;
static MethodInfo* t3178_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17654_MI,
	&m17656_MI,
	&m17655_MI,
	&m17657_MI,
};
static TypeInfo* t3178_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4533_TI,
};
static Il2CppInterfaceOffsetPair t3178_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4533_TI, 7},
};
extern TypeInfo t606_TI;
static Il2CppRGCTXData t3178_RGCTXData[3] = 
{
	&m17657_MI/* Method Usage */,
	&t606_TI/* Class Usage */,
	&m23225_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3178_0_0_0;
extern Il2CppType t3178_1_0_0;
extern Il2CppGenericClass t3178_GC;
TypeInfo t3178_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3178_MIs, t3178_PIs, t3178_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3178_TI, t3178_ITIs, t3178_VT, &EmptyCustomAttributesCache, &t3178_TI, &t3178_0_0_0, &t3178_1_0_0, t3178_IOs, &t3178_GC, NULL, NULL, NULL, t3178_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3178)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5823_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableAttribute>
extern MethodInfo m30449_MI;
static PropertyInfo t5823____Count_PropertyInfo = 
{
	&t5823_TI, "Count", &m30449_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30450_MI;
static PropertyInfo t5823____IsReadOnly_PropertyInfo = 
{
	&t5823_TI, "IsReadOnly", &m30450_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5823_PIs[] =
{
	&t5823____Count_PropertyInfo,
	&t5823____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30449_GM;
MethodInfo m30449_MI = 
{
	"get_Count", NULL, &t5823_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30449_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30450_GM;
MethodInfo m30450_MI = 
{
	"get_IsReadOnly", NULL, &t5823_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30450_GM};
extern Il2CppType t606_0_0_0;
extern Il2CppType t606_0_0_0;
static ParameterInfo t5823_m30451_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t606_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30451_GM;
MethodInfo m30451_MI = 
{
	"Add", NULL, &t5823_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5823_m30451_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30451_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30452_GM;
MethodInfo m30452_MI = 
{
	"Clear", NULL, &t5823_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30452_GM};
extern Il2CppType t606_0_0_0;
static ParameterInfo t5823_m30453_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t606_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30453_GM;
MethodInfo m30453_MI = 
{
	"Contains", NULL, &t5823_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5823_m30453_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30453_GM};
extern Il2CppType t3897_0_0_0;
extern Il2CppType t3897_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5823_m30454_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3897_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30454_GM;
MethodInfo m30454_MI = 
{
	"CopyTo", NULL, &t5823_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5823_m30454_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30454_GM};
extern Il2CppType t606_0_0_0;
static ParameterInfo t5823_m30455_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t606_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30455_GM;
MethodInfo m30455_MI = 
{
	"Remove", NULL, &t5823_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5823_m30455_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30455_GM};
static MethodInfo* t5823_MIs[] =
{
	&m30449_MI,
	&m30450_MI,
	&m30451_MI,
	&m30452_MI,
	&m30453_MI,
	&m30454_MI,
	&m30455_MI,
	NULL
};
extern TypeInfo t5825_TI;
static TypeInfo* t5823_ITIs[] = 
{
	&t603_TI,
	&t5825_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5823_0_0_0;
extern Il2CppType t5823_1_0_0;
struct t5823;
extern Il2CppGenericClass t5823_GC;
TypeInfo t5823_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5823_MIs, t5823_PIs, NULL, NULL, NULL, NULL, NULL, &t5823_TI, t5823_ITIs, NULL, &EmptyCustomAttributesCache, &t5823_TI, &t5823_0_0_0, &t5823_1_0_0, NULL, &t5823_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.ComponentModel.EditorBrowsableAttribute>
extern Il2CppType t4533_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30456_GM;
MethodInfo m30456_MI = 
{
	"GetEnumerator", NULL, &t5825_TI, &t4533_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30456_GM};
static MethodInfo* t5825_MIs[] =
{
	&m30456_MI,
	NULL
};
static TypeInfo* t5825_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5825_0_0_0;
extern Il2CppType t5825_1_0_0;
struct t5825;
extern Il2CppGenericClass t5825_GC;
TypeInfo t5825_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5825_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5825_TI, t5825_ITIs, NULL, &EmptyCustomAttributesCache, &t5825_TI, &t5825_0_0_0, &t5825_1_0_0, NULL, &t5825_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5824_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableAttribute>
extern MethodInfo m30457_MI;
extern MethodInfo m30458_MI;
static PropertyInfo t5824____Item_PropertyInfo = 
{
	&t5824_TI, "Item", &m30457_MI, &m30458_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5824_PIs[] =
{
	&t5824____Item_PropertyInfo,
	NULL
};
extern Il2CppType t606_0_0_0;
static ParameterInfo t5824_m30459_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t606_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30459_GM;
MethodInfo m30459_MI = 
{
	"IndexOf", NULL, &t5824_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5824_m30459_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30459_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t606_0_0_0;
static ParameterInfo t5824_m30460_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t606_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30460_GM;
MethodInfo m30460_MI = 
{
	"Insert", NULL, &t5824_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5824_m30460_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30460_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5824_m30461_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30461_GM;
MethodInfo m30461_MI = 
{
	"RemoveAt", NULL, &t5824_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5824_m30461_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30461_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5824_m30457_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t606_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30457_GM;
MethodInfo m30457_MI = 
{
	"get_Item", NULL, &t5824_TI, &t606_0_0_0, RuntimeInvoker_t29_t44, t5824_m30457_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30457_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t606_0_0_0;
static ParameterInfo t5824_m30458_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t606_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30458_GM;
MethodInfo m30458_MI = 
{
	"set_Item", NULL, &t5824_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5824_m30458_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30458_GM};
static MethodInfo* t5824_MIs[] =
{
	&m30459_MI,
	&m30460_MI,
	&m30461_MI,
	&m30457_MI,
	&m30458_MI,
	NULL
};
static TypeInfo* t5824_ITIs[] = 
{
	&t603_TI,
	&t5823_TI,
	&t5825_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5824_0_0_0;
extern Il2CppType t5824_1_0_0;
struct t5824;
extern Il2CppGenericClass t5824_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5824_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5824_MIs, t5824_PIs, NULL, NULL, NULL, NULL, NULL, &t5824_TI, t5824_ITIs, NULL, &t1908__CustomAttributeCache, &t5824_TI, &t5824_0_0_0, &t5824_1_0_0, NULL, &t5824_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4535_TI;

#include "t737.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.ComponentModel.EditorBrowsableState>
extern MethodInfo m30462_MI;
static PropertyInfo t4535____Current_PropertyInfo = 
{
	&t4535_TI, "Current", &m30462_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4535_PIs[] =
{
	&t4535____Current_PropertyInfo,
	NULL
};
extern Il2CppType t737_0_0_0;
extern void* RuntimeInvoker_t737 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30462_GM;
MethodInfo m30462_MI = 
{
	"get_Current", NULL, &t4535_TI, &t737_0_0_0, RuntimeInvoker_t737, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30462_GM};
static MethodInfo* t4535_MIs[] =
{
	&m30462_MI,
	NULL
};
static TypeInfo* t4535_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4535_0_0_0;
extern Il2CppType t4535_1_0_0;
struct t4535;
extern Il2CppGenericClass t4535_GC;
TypeInfo t4535_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4535_MIs, t4535_PIs, NULL, NULL, NULL, NULL, NULL, &t4535_TI, t4535_ITIs, NULL, &EmptyCustomAttributesCache, &t4535_TI, &t4535_0_0_0, &t4535_1_0_0, NULL, &t4535_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3179.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3179_TI;
#include "t3179MD.h"

extern TypeInfo t737_TI;
extern MethodInfo m17662_MI;
extern MethodInfo m23236_MI;
struct t20;
 int32_t m23236 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17658_MI;
 void m17658 (t3179 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17659_MI;
 t29 * m17659 (t3179 * __this, MethodInfo* method){
	{
		int32_t L_0 = m17662(__this, &m17662_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t737_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17660_MI;
 void m17660 (t3179 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17661_MI;
 bool m17661 (t3179 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m17662 (t3179 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m23236(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23236_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableState>
extern Il2CppType t20_0_0_1;
FieldInfo t3179_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3179_TI, offsetof(t3179, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3179_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3179_TI, offsetof(t3179, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3179_FIs[] =
{
	&t3179_f0_FieldInfo,
	&t3179_f1_FieldInfo,
	NULL
};
static PropertyInfo t3179____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3179_TI, "System.Collections.IEnumerator.Current", &m17659_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3179____Current_PropertyInfo = 
{
	&t3179_TI, "Current", &m17662_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3179_PIs[] =
{
	&t3179____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3179____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3179_m17658_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17658_GM;
MethodInfo m17658_MI = 
{
	".ctor", (methodPointerType)&m17658, &t3179_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3179_m17658_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17658_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17659_GM;
MethodInfo m17659_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17659, &t3179_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17659_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17660_GM;
MethodInfo m17660_MI = 
{
	"Dispose", (methodPointerType)&m17660, &t3179_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17660_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17661_GM;
MethodInfo m17661_MI = 
{
	"MoveNext", (methodPointerType)&m17661, &t3179_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17661_GM};
extern Il2CppType t737_0_0_0;
extern void* RuntimeInvoker_t737 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17662_GM;
MethodInfo m17662_MI = 
{
	"get_Current", (methodPointerType)&m17662, &t3179_TI, &t737_0_0_0, RuntimeInvoker_t737, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17662_GM};
static MethodInfo* t3179_MIs[] =
{
	&m17658_MI,
	&m17659_MI,
	&m17660_MI,
	&m17661_MI,
	&m17662_MI,
	NULL
};
static MethodInfo* t3179_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17659_MI,
	&m17661_MI,
	&m17660_MI,
	&m17662_MI,
};
static TypeInfo* t3179_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4535_TI,
};
static Il2CppInterfaceOffsetPair t3179_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4535_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3179_0_0_0;
extern Il2CppType t3179_1_0_0;
extern Il2CppGenericClass t3179_GC;
TypeInfo t3179_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3179_MIs, t3179_PIs, t3179_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3179_TI, t3179_ITIs, t3179_VT, &EmptyCustomAttributesCache, &t3179_TI, &t3179_0_0_0, &t3179_1_0_0, t3179_IOs, &t3179_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3179)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5826_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableState>
extern MethodInfo m30463_MI;
static PropertyInfo t5826____Count_PropertyInfo = 
{
	&t5826_TI, "Count", &m30463_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30464_MI;
static PropertyInfo t5826____IsReadOnly_PropertyInfo = 
{
	&t5826_TI, "IsReadOnly", &m30464_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5826_PIs[] =
{
	&t5826____Count_PropertyInfo,
	&t5826____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30463_GM;
MethodInfo m30463_MI = 
{
	"get_Count", NULL, &t5826_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30463_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30464_GM;
MethodInfo m30464_MI = 
{
	"get_IsReadOnly", NULL, &t5826_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30464_GM};
extern Il2CppType t737_0_0_0;
extern Il2CppType t737_0_0_0;
static ParameterInfo t5826_m30465_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t737_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30465_GM;
MethodInfo m30465_MI = 
{
	"Add", NULL, &t5826_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5826_m30465_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30465_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30466_GM;
MethodInfo m30466_MI = 
{
	"Clear", NULL, &t5826_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30466_GM};
extern Il2CppType t737_0_0_0;
static ParameterInfo t5826_m30467_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t737_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30467_GM;
MethodInfo m30467_MI = 
{
	"Contains", NULL, &t5826_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5826_m30467_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30467_GM};
extern Il2CppType t3898_0_0_0;
extern Il2CppType t3898_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5826_m30468_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3898_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30468_GM;
MethodInfo m30468_MI = 
{
	"CopyTo", NULL, &t5826_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5826_m30468_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30468_GM};
extern Il2CppType t737_0_0_0;
static ParameterInfo t5826_m30469_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t737_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30469_GM;
MethodInfo m30469_MI = 
{
	"Remove", NULL, &t5826_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5826_m30469_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30469_GM};
static MethodInfo* t5826_MIs[] =
{
	&m30463_MI,
	&m30464_MI,
	&m30465_MI,
	&m30466_MI,
	&m30467_MI,
	&m30468_MI,
	&m30469_MI,
	NULL
};
extern TypeInfo t5828_TI;
static TypeInfo* t5826_ITIs[] = 
{
	&t603_TI,
	&t5828_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5826_0_0_0;
extern Il2CppType t5826_1_0_0;
struct t5826;
extern Il2CppGenericClass t5826_GC;
TypeInfo t5826_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5826_MIs, t5826_PIs, NULL, NULL, NULL, NULL, NULL, &t5826_TI, t5826_ITIs, NULL, &EmptyCustomAttributesCache, &t5826_TI, &t5826_0_0_0, &t5826_1_0_0, NULL, &t5826_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.ComponentModel.EditorBrowsableState>
extern Il2CppType t4535_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30470_GM;
MethodInfo m30470_MI = 
{
	"GetEnumerator", NULL, &t5828_TI, &t4535_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30470_GM};
static MethodInfo* t5828_MIs[] =
{
	&m30470_MI,
	NULL
};
static TypeInfo* t5828_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5828_0_0_0;
extern Il2CppType t5828_1_0_0;
struct t5828;
extern Il2CppGenericClass t5828_GC;
TypeInfo t5828_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5828_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5828_TI, t5828_ITIs, NULL, &EmptyCustomAttributesCache, &t5828_TI, &t5828_0_0_0, &t5828_1_0_0, NULL, &t5828_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5827_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableState>
extern MethodInfo m30471_MI;
extern MethodInfo m30472_MI;
static PropertyInfo t5827____Item_PropertyInfo = 
{
	&t5827_TI, "Item", &m30471_MI, &m30472_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5827_PIs[] =
{
	&t5827____Item_PropertyInfo,
	NULL
};
extern Il2CppType t737_0_0_0;
static ParameterInfo t5827_m30473_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t737_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30473_GM;
MethodInfo m30473_MI = 
{
	"IndexOf", NULL, &t5827_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5827_m30473_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30473_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t737_0_0_0;
static ParameterInfo t5827_m30474_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t737_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30474_GM;
MethodInfo m30474_MI = 
{
	"Insert", NULL, &t5827_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5827_m30474_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30474_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5827_m30475_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30475_GM;
MethodInfo m30475_MI = 
{
	"RemoveAt", NULL, &t5827_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5827_m30475_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30475_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5827_m30471_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t737_0_0_0;
extern void* RuntimeInvoker_t737_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30471_GM;
MethodInfo m30471_MI = 
{
	"get_Item", NULL, &t5827_TI, &t737_0_0_0, RuntimeInvoker_t737_t44, t5827_m30471_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30471_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t737_0_0_0;
static ParameterInfo t5827_m30472_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t737_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30472_GM;
MethodInfo m30472_MI = 
{
	"set_Item", NULL, &t5827_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5827_m30472_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30472_GM};
static MethodInfo* t5827_MIs[] =
{
	&m30473_MI,
	&m30474_MI,
	&m30475_MI,
	&m30471_MI,
	&m30472_MI,
	NULL
};
static TypeInfo* t5827_ITIs[] = 
{
	&t603_TI,
	&t5826_TI,
	&t5828_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5827_0_0_0;
extern Il2CppType t5827_1_0_0;
struct t5827;
extern Il2CppGenericClass t5827_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5827_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5827_MIs, t5827_PIs, NULL, NULL, NULL, NULL, NULL, &t5827_TI, t5827_ITIs, NULL, &t1908__CustomAttributeCache, &t5827_TI, &t5827_0_0_0, &t5827_1_0_0, NULL, &t5827_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4537_TI;

#include "t739.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.ComponentModel.TypeConverterAttribute>
extern MethodInfo m30476_MI;
static PropertyInfo t4537____Current_PropertyInfo = 
{
	&t4537_TI, "Current", &m30476_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4537_PIs[] =
{
	&t4537____Current_PropertyInfo,
	NULL
};
extern Il2CppType t739_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30476_GM;
MethodInfo m30476_MI = 
{
	"get_Current", NULL, &t4537_TI, &t739_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30476_GM};
static MethodInfo* t4537_MIs[] =
{
	&m30476_MI,
	NULL
};
static TypeInfo* t4537_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4537_0_0_0;
extern Il2CppType t4537_1_0_0;
struct t4537;
extern Il2CppGenericClass t4537_GC;
TypeInfo t4537_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4537_MIs, t4537_PIs, NULL, NULL, NULL, NULL, NULL, &t4537_TI, t4537_ITIs, NULL, &EmptyCustomAttributesCache, &t4537_TI, &t4537_0_0_0, &t4537_1_0_0, NULL, &t4537_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3180.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3180_TI;
#include "t3180MD.h"

extern TypeInfo t739_TI;
extern MethodInfo m17667_MI;
extern MethodInfo m23247_MI;
struct t20;
#define m23247(__this, p0, method) (t739 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.ComponentModel.TypeConverterAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3180_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3180_TI, offsetof(t3180, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3180_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3180_TI, offsetof(t3180, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3180_FIs[] =
{
	&t3180_f0_FieldInfo,
	&t3180_f1_FieldInfo,
	NULL
};
extern MethodInfo m17664_MI;
static PropertyInfo t3180____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3180_TI, "System.Collections.IEnumerator.Current", &m17664_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3180____Current_PropertyInfo = 
{
	&t3180_TI, "Current", &m17667_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3180_PIs[] =
{
	&t3180____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3180____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3180_m17663_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17663_GM;
MethodInfo m17663_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3180_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3180_m17663_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17663_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17664_GM;
MethodInfo m17664_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3180_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17664_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17665_GM;
MethodInfo m17665_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3180_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17665_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17666_GM;
MethodInfo m17666_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3180_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17666_GM};
extern Il2CppType t739_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17667_GM;
MethodInfo m17667_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3180_TI, &t739_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17667_GM};
static MethodInfo* t3180_MIs[] =
{
	&m17663_MI,
	&m17664_MI,
	&m17665_MI,
	&m17666_MI,
	&m17667_MI,
	NULL
};
extern MethodInfo m17666_MI;
extern MethodInfo m17665_MI;
static MethodInfo* t3180_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17664_MI,
	&m17666_MI,
	&m17665_MI,
	&m17667_MI,
};
static TypeInfo* t3180_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4537_TI,
};
static Il2CppInterfaceOffsetPair t3180_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4537_TI, 7},
};
extern TypeInfo t739_TI;
static Il2CppRGCTXData t3180_RGCTXData[3] = 
{
	&m17667_MI/* Method Usage */,
	&t739_TI/* Class Usage */,
	&m23247_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3180_0_0_0;
extern Il2CppType t3180_1_0_0;
extern Il2CppGenericClass t3180_GC;
TypeInfo t3180_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3180_MIs, t3180_PIs, t3180_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3180_TI, t3180_ITIs, t3180_VT, &EmptyCustomAttributesCache, &t3180_TI, &t3180_0_0_0, &t3180_1_0_0, t3180_IOs, &t3180_GC, NULL, NULL, NULL, t3180_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3180)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5829_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.ComponentModel.TypeConverterAttribute>
extern MethodInfo m30477_MI;
static PropertyInfo t5829____Count_PropertyInfo = 
{
	&t5829_TI, "Count", &m30477_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30478_MI;
static PropertyInfo t5829____IsReadOnly_PropertyInfo = 
{
	&t5829_TI, "IsReadOnly", &m30478_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5829_PIs[] =
{
	&t5829____Count_PropertyInfo,
	&t5829____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30477_GM;
MethodInfo m30477_MI = 
{
	"get_Count", NULL, &t5829_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30477_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30478_GM;
MethodInfo m30478_MI = 
{
	"get_IsReadOnly", NULL, &t5829_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30478_GM};
extern Il2CppType t739_0_0_0;
extern Il2CppType t739_0_0_0;
static ParameterInfo t5829_m30479_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t739_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30479_GM;
MethodInfo m30479_MI = 
{
	"Add", NULL, &t5829_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5829_m30479_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30479_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30480_GM;
MethodInfo m30480_MI = 
{
	"Clear", NULL, &t5829_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30480_GM};
extern Il2CppType t739_0_0_0;
static ParameterInfo t5829_m30481_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t739_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30481_GM;
MethodInfo m30481_MI = 
{
	"Contains", NULL, &t5829_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5829_m30481_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30481_GM};
extern Il2CppType t3899_0_0_0;
extern Il2CppType t3899_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5829_m30482_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3899_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30482_GM;
MethodInfo m30482_MI = 
{
	"CopyTo", NULL, &t5829_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5829_m30482_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30482_GM};
extern Il2CppType t739_0_0_0;
static ParameterInfo t5829_m30483_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t739_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30483_GM;
MethodInfo m30483_MI = 
{
	"Remove", NULL, &t5829_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5829_m30483_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30483_GM};
static MethodInfo* t5829_MIs[] =
{
	&m30477_MI,
	&m30478_MI,
	&m30479_MI,
	&m30480_MI,
	&m30481_MI,
	&m30482_MI,
	&m30483_MI,
	NULL
};
extern TypeInfo t5831_TI;
static TypeInfo* t5829_ITIs[] = 
{
	&t603_TI,
	&t5831_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5829_0_0_0;
extern Il2CppType t5829_1_0_0;
struct t5829;
extern Il2CppGenericClass t5829_GC;
TypeInfo t5829_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5829_MIs, t5829_PIs, NULL, NULL, NULL, NULL, NULL, &t5829_TI, t5829_ITIs, NULL, &EmptyCustomAttributesCache, &t5829_TI, &t5829_0_0_0, &t5829_1_0_0, NULL, &t5829_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.ComponentModel.TypeConverterAttribute>
extern Il2CppType t4537_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30484_GM;
MethodInfo m30484_MI = 
{
	"GetEnumerator", NULL, &t5831_TI, &t4537_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30484_GM};
static MethodInfo* t5831_MIs[] =
{
	&m30484_MI,
	NULL
};
static TypeInfo* t5831_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5831_0_0_0;
extern Il2CppType t5831_1_0_0;
struct t5831;
extern Il2CppGenericClass t5831_GC;
TypeInfo t5831_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5831_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5831_TI, t5831_ITIs, NULL, &EmptyCustomAttributesCache, &t5831_TI, &t5831_0_0_0, &t5831_1_0_0, NULL, &t5831_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5830_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.ComponentModel.TypeConverterAttribute>
extern MethodInfo m30485_MI;
extern MethodInfo m30486_MI;
static PropertyInfo t5830____Item_PropertyInfo = 
{
	&t5830_TI, "Item", &m30485_MI, &m30486_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5830_PIs[] =
{
	&t5830____Item_PropertyInfo,
	NULL
};
extern Il2CppType t739_0_0_0;
static ParameterInfo t5830_m30487_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t739_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30487_GM;
MethodInfo m30487_MI = 
{
	"IndexOf", NULL, &t5830_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5830_m30487_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30487_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t739_0_0_0;
static ParameterInfo t5830_m30488_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t739_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30488_GM;
MethodInfo m30488_MI = 
{
	"Insert", NULL, &t5830_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5830_m30488_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30488_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5830_m30489_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30489_GM;
MethodInfo m30489_MI = 
{
	"RemoveAt", NULL, &t5830_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5830_m30489_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30489_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5830_m30485_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t739_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30485_GM;
MethodInfo m30485_MI = 
{
	"get_Item", NULL, &t5830_TI, &t739_0_0_0, RuntimeInvoker_t29_t44, t5830_m30485_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30485_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t739_0_0_0;
static ParameterInfo t5830_m30486_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t739_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30486_GM;
MethodInfo m30486_MI = 
{
	"set_Item", NULL, &t5830_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5830_m30486_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30486_GM};
static MethodInfo* t5830_MIs[] =
{
	&m30487_MI,
	&m30488_MI,
	&m30489_MI,
	&m30485_MI,
	&m30486_MI,
	NULL
};
static TypeInfo* t5830_ITIs[] = 
{
	&t603_TI,
	&t5829_TI,
	&t5831_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5830_0_0_0;
extern Il2CppType t5830_1_0_0;
struct t5830;
extern Il2CppGenericClass t5830_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5830_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5830_MIs, t5830_PIs, NULL, NULL, NULL, NULL, NULL, &t5830_TI, t5830_ITIs, NULL, &t1908__CustomAttributeCache, &t5830_TI, &t5830_0_0_0, &t5830_1_0_0, NULL, &t5830_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4539_TI;

#include "t740.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Net.Security.AuthenticationLevel>
extern MethodInfo m30490_MI;
static PropertyInfo t4539____Current_PropertyInfo = 
{
	&t4539_TI, "Current", &m30490_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4539_PIs[] =
{
	&t4539____Current_PropertyInfo,
	NULL
};
extern Il2CppType t740_0_0_0;
extern void* RuntimeInvoker_t740 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30490_GM;
MethodInfo m30490_MI = 
{
	"get_Current", NULL, &t4539_TI, &t740_0_0_0, RuntimeInvoker_t740, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30490_GM};
static MethodInfo* t4539_MIs[] =
{
	&m30490_MI,
	NULL
};
static TypeInfo* t4539_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4539_0_0_0;
extern Il2CppType t4539_1_0_0;
struct t4539;
extern Il2CppGenericClass t4539_GC;
TypeInfo t4539_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4539_MIs, t4539_PIs, NULL, NULL, NULL, NULL, NULL, &t4539_TI, t4539_ITIs, NULL, &EmptyCustomAttributesCache, &t4539_TI, &t4539_0_0_0, &t4539_1_0_0, NULL, &t4539_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3181.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3181_TI;
#include "t3181MD.h"

extern TypeInfo t740_TI;
extern MethodInfo m17672_MI;
extern MethodInfo m23258_MI;
struct t20;
 int32_t m23258 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17668_MI;
 void m17668 (t3181 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17669_MI;
 t29 * m17669 (t3181 * __this, MethodInfo* method){
	{
		int32_t L_0 = m17672(__this, &m17672_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t740_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17670_MI;
 void m17670 (t3181 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17671_MI;
 bool m17671 (t3181 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m17672 (t3181 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m23258(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23258_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Net.Security.AuthenticationLevel>
extern Il2CppType t20_0_0_1;
FieldInfo t3181_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3181_TI, offsetof(t3181, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3181_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3181_TI, offsetof(t3181, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3181_FIs[] =
{
	&t3181_f0_FieldInfo,
	&t3181_f1_FieldInfo,
	NULL
};
static PropertyInfo t3181____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3181_TI, "System.Collections.IEnumerator.Current", &m17669_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3181____Current_PropertyInfo = 
{
	&t3181_TI, "Current", &m17672_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3181_PIs[] =
{
	&t3181____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3181____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3181_m17668_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17668_GM;
MethodInfo m17668_MI = 
{
	".ctor", (methodPointerType)&m17668, &t3181_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3181_m17668_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17668_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17669_GM;
MethodInfo m17669_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17669, &t3181_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17669_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17670_GM;
MethodInfo m17670_MI = 
{
	"Dispose", (methodPointerType)&m17670, &t3181_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17670_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17671_GM;
MethodInfo m17671_MI = 
{
	"MoveNext", (methodPointerType)&m17671, &t3181_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17671_GM};
extern Il2CppType t740_0_0_0;
extern void* RuntimeInvoker_t740 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17672_GM;
MethodInfo m17672_MI = 
{
	"get_Current", (methodPointerType)&m17672, &t3181_TI, &t740_0_0_0, RuntimeInvoker_t740, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17672_GM};
static MethodInfo* t3181_MIs[] =
{
	&m17668_MI,
	&m17669_MI,
	&m17670_MI,
	&m17671_MI,
	&m17672_MI,
	NULL
};
static MethodInfo* t3181_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17669_MI,
	&m17671_MI,
	&m17670_MI,
	&m17672_MI,
};
static TypeInfo* t3181_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4539_TI,
};
static Il2CppInterfaceOffsetPair t3181_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4539_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3181_0_0_0;
extern Il2CppType t3181_1_0_0;
extern Il2CppGenericClass t3181_GC;
TypeInfo t3181_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3181_MIs, t3181_PIs, t3181_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3181_TI, t3181_ITIs, t3181_VT, &EmptyCustomAttributesCache, &t3181_TI, &t3181_0_0_0, &t3181_1_0_0, t3181_IOs, &t3181_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3181)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5832_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Net.Security.AuthenticationLevel>
extern MethodInfo m30491_MI;
static PropertyInfo t5832____Count_PropertyInfo = 
{
	&t5832_TI, "Count", &m30491_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m30492_MI;
static PropertyInfo t5832____IsReadOnly_PropertyInfo = 
{
	&t5832_TI, "IsReadOnly", &m30492_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5832_PIs[] =
{
	&t5832____Count_PropertyInfo,
	&t5832____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30491_GM;
MethodInfo m30491_MI = 
{
	"get_Count", NULL, &t5832_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30491_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30492_GM;
MethodInfo m30492_MI = 
{
	"get_IsReadOnly", NULL, &t5832_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30492_GM};
extern Il2CppType t740_0_0_0;
extern Il2CppType t740_0_0_0;
static ParameterInfo t5832_m30493_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t740_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30493_GM;
MethodInfo m30493_MI = 
{
	"Add", NULL, &t5832_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5832_m30493_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30493_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30494_GM;
MethodInfo m30494_MI = 
{
	"Clear", NULL, &t5832_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30494_GM};
extern Il2CppType t740_0_0_0;
static ParameterInfo t5832_m30495_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t740_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30495_GM;
MethodInfo m30495_MI = 
{
	"Contains", NULL, &t5832_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5832_m30495_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30495_GM};
extern Il2CppType t3900_0_0_0;
extern Il2CppType t3900_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5832_m30496_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3900_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30496_GM;
MethodInfo m30496_MI = 
{
	"CopyTo", NULL, &t5832_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5832_m30496_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m30496_GM};
extern Il2CppType t740_0_0_0;
static ParameterInfo t5832_m30497_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t740_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30497_GM;
MethodInfo m30497_MI = 
{
	"Remove", NULL, &t5832_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5832_m30497_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m30497_GM};
static MethodInfo* t5832_MIs[] =
{
	&m30491_MI,
	&m30492_MI,
	&m30493_MI,
	&m30494_MI,
	&m30495_MI,
	&m30496_MI,
	&m30497_MI,
	NULL
};
extern TypeInfo t5834_TI;
static TypeInfo* t5832_ITIs[] = 
{
	&t603_TI,
	&t5834_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5832_0_0_0;
extern Il2CppType t5832_1_0_0;
struct t5832;
extern Il2CppGenericClass t5832_GC;
TypeInfo t5832_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5832_MIs, t5832_PIs, NULL, NULL, NULL, NULL, NULL, &t5832_TI, t5832_ITIs, NULL, &EmptyCustomAttributesCache, &t5832_TI, &t5832_0_0_0, &t5832_1_0_0, NULL, &t5832_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Net.Security.AuthenticationLevel>
extern Il2CppType t4539_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m30498_GM;
MethodInfo m30498_MI = 
{
	"GetEnumerator", NULL, &t5834_TI, &t4539_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m30498_GM};
static MethodInfo* t5834_MIs[] =
{
	&m30498_MI,
	NULL
};
static TypeInfo* t5834_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5834_0_0_0;
extern Il2CppType t5834_1_0_0;
struct t5834;
extern Il2CppGenericClass t5834_GC;
TypeInfo t5834_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5834_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5834_TI, t5834_ITIs, NULL, &EmptyCustomAttributesCache, &t5834_TI, &t5834_0_0_0, &t5834_1_0_0, NULL, &t5834_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
