﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1266;
struct t29;
struct t731;
struct t674;
struct t20;
struct t136;
struct t726;
struct t316;
struct t42;

 void m6602 (t1266 * __this, t731 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m6603 (t1266 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6604 (t1266 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6605 (t1266 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6606 (t1266 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6607 (t1266 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6608 (t1266 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m6609 (t1266 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6610 (t1266 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6611 (t1266 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6612 (t1266 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6613 (t1266 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6614 (t1266 * __this, t29 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6615 (t1266 * __this, t29 * p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6616 (t1266 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6617 (t1266 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6618 (t1266 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6619 (t1266 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6620 (t1266 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6621 (t1266 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6622 (t1266 * __this, int32_t p0, t20 * p1, int32_t p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m6623 (t1266 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6624 (t1266 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6625 (t1266 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6626 (t1266 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m6627 (t1266 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t20 * m6628 (t1266 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
