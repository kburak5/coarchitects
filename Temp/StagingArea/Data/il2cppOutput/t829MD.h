﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t829;
struct t841;
struct t7;
struct t733;
struct t840;
struct t828;
struct t838;
struct t836;
struct t446;
struct t721;
#include "t842.h"
#include "t735.h"

 void m3551 (t829 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3552 (t829 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3553 (t829 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3554 (t829 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3555 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3556 (t829 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3557 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3558 (t829 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3559 (t829 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3560 (t29 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3561 (t829 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3562 (t829 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3563 (t829 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3564 (t829 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3565 (t829 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3566 (t829 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t828 * m3567 (t829 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t838 * m3568 (t829 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t838 * m3569 (t829 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3570 (t829 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3571 (t829 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3572 (t829 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t446* m3573 (t29 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t841* m3574 (t829 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
