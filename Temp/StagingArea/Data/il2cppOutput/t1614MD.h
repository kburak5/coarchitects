﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1614;
struct t29;
struct t1623;
struct t66;
struct t67;
#include "t35.h"

 void m9636 (t1614 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9637 (t1614 * __this, t29 * p0, t1623 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m9638 (t1614 * __this, t29 * p0, t1623 * p1, t67 * p2, t29 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9639 (t1614 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
