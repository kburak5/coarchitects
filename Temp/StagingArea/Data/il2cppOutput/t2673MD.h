﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2673;
struct t29;
struct t20;
#include "t231.h"

 void m14486 (t2673 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m14487 (t2673 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m14488 (t2673 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m14489 (t2673 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m14490 (t2673 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
