﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1377;
struct t42;
struct t7;
struct t636;
struct t557;
struct t638;
struct t29;
struct t316;
struct t295;
struct t631;
struct t633;
struct t1152;
struct t733;
struct t537;
#include "t1343.h"
#include "t1342.h"
#include "t1150.h"
#include "t630.h"
#include "t35.h"
#include "t735.h"

 void m7620 (t1377 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7621 (t29 * __this, t636 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1377 * m7622 (t29 * __this, t1377 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t557 * m7623 (t1377 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7624 (t1377 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t638* m7625 (t1377 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7626 (t1377 * __this, t29 * p0, t316* p1, t295 ** p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7627 (t1377 * __this, t29 * p0, int32_t p1, t631 * p2, t316* p3, t633 * p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1343  m7628 (t1377 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7629 (t1377 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7630 (t1377 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7631 (t1377 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7632 (t1377 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7633 (t1377 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7634 (t1377 * __this, t42 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m7635 (t1377 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m7636 (t1377 * __this, t42 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1152 * m7637 (t29 * __this, t35 p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m7638 (t1377 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7639 (t29 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7640 (t1377 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7641 (t1377 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t557 * m7642 (t1377 * __this, t537* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t557 * m7643 (t1377 * __this, t537* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t537* m7644 (t1377 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7645 (t1377 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7646 (t1377 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7647 (t1377 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
