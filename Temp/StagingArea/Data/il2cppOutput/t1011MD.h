﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1011;
struct t446;
struct t809;
struct t7;

 void m4588 (t1011 * __this, t809 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4589 (t1011 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t446* m4590 (t1011 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t446* m4591 (t1011 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4592 (t1011 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
