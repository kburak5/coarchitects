﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3036;
struct t29;
struct t20;
struct t136;
struct t530;
struct t3029;
struct t388;
#include "t381.h"

 void m16613 (t3036 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16614 (t3036 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16615 (t3036 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m16616 (t3036 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16617 (t3036 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16618 (t3036 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16619 (t3036 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16620 (t3036 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16621 (t3036 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16622 (t3036 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m16623 (t3036 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16624 (t3036 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16625 (t3036 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m16626 (t3036 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16627 (t3036 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16628 (t3036 * __this, t381  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16629 (t3036 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16630 (t3036 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16631 (t3036 * __this, t381  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16632 (t3036 * __this, t530* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m16633 (t3036 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16634 (t3036 * __this, t381  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16635 (t3036 * __this, int32_t p0, t381  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16636 (t3036 * __this, int32_t p0, t381  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16637 (t3036 * __this, t381  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16638 (t3036 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16639 (t3036 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16640 (t3036 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t381  m16641 (t3036 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16642 (t3036 * __this, int32_t p0, t381  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16643 (t3036 * __this, int32_t p0, t381  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16644 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t381  m16645 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16646 (t29 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16647 (t29 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16648 (t29 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
