﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t755;
struct t29;
struct t745;
struct t756;
struct t66;
struct t67;
#include "t35.h"
#include "t741.h"

 void m3933 (t755 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3934 (t755 * __this, t29 * p0, t745 * p1, t756 * p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3935 (t755 * __this, t29 * p0, t745 * p1, t756 * p2, int32_t p3, t67 * p4, t29 * p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3936 (t755 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
