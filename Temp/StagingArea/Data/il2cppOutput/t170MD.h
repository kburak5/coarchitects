﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t170;
struct t29;
struct t366;
struct t3;
struct t2560;
struct t2558;
struct t733;
struct t2561;
struct t20;
struct t136;
struct t2563;
struct t722;
#include "t735.h"
#include "t2562.h"
#include "t2564.h"
#include "t725.h"

#include "t2461MD.h"
#define m1633(__this, method) (void)m12834_gshared((t2461 *)__this, method)
#define m13716(__this, p0, method) (void)m12836_gshared((t2461 *)__this, (t29*)p0, method)
#define m13717(__this, p0, method) (void)m12838_gshared((t2461 *)__this, (int32_t)p0, method)
#define m13718(__this, p0, p1, method) (void)m12840_gshared((t2461 *)__this, (t733 *)p0, (t735 )p1, method)
#define m13719(__this, p0, method) (t29 *)m12842_gshared((t2461 *)__this, (t29 *)p0, method)
#define m13720(__this, p0, p1, method) (void)m12844_gshared((t2461 *)__this, (t29 *)p0, (t29 *)p1, method)
#define m13721(__this, p0, p1, method) (void)m12846_gshared((t2461 *)__this, (t29 *)p0, (t29 *)p1, method)
#define m13722(__this, p0, method) (void)m12848_gshared((t2461 *)__this, (t29 *)p0, method)
#define m13723(__this, method) (bool)m12850_gshared((t2461 *)__this, method)
#define m13724(__this, method) (t29 *)m12852_gshared((t2461 *)__this, method)
#define m13725(__this, method) (bool)m12854_gshared((t2461 *)__this, method)
 void m13726 (t170 * __this, t2562  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13727 (t170 * __this, t2562  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
#define m13728(__this, p0, p1, method) (void)m12858_gshared((t2461 *)__this, (t2462*)p0, (int32_t)p1, method)
 bool m13729 (t170 * __this, t2562  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
#define m13730(__this, p0, p1, method) (void)m12861_gshared((t2461 *)__this, (t20 *)p0, (int32_t)p1, method)
#define m13731(__this, method) (t29 *)m12863_gshared((t2461 *)__this, method)
#define m13732(__this, method) (t29*)m12865_gshared((t2461 *)__this, method)
#define m13733(__this, method) (t29 *)m12867_gshared((t2461 *)__this, method)
#define m13734(__this, method) (int32_t)m12869_gshared((t2461 *)__this, method)
#define m13735(__this, p0, method) (t366 *)m12871_gshared((t2461 *)__this, (t29 *)p0, method)
#define m13736(__this, p0, p1, method) (void)m12873_gshared((t2461 *)__this, (t29 *)p0, (t29 *)p1, method)
#define m13737(__this, p0, p1, method) (void)m12875_gshared((t2461 *)__this, (int32_t)p0, (t29*)p1, method)
#define m13738(__this, p0, method) (void)m12877_gshared((t2461 *)__this, (int32_t)p0, method)
#define m13739(__this, p0, p1, method) (void)m12879_gshared((t2461 *)__this, (t20 *)p0, (int32_t)p1, method)
 t2562  m13740 (t29 * __this, t3 * p0, t366 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
#define m13741(__this, p0, p1, method) (t366 *)m12882_gshared((t29 *)__this, (t29 *)p0, (t29 *)p1, method)
#define m13742(__this, p0, p1, method) (void)m12884_gshared((t2461 *)__this, (t2462*)p0, (int32_t)p1, method)
#define m13743(__this, method) (void)m12886_gshared((t2461 *)__this, method)
#define m1637(__this, p0, p1, method) (void)m12887_gshared((t2461 *)__this, (t29 *)p0, (t29 *)p1, method)
#define m13744(__this, method) (void)m12889_gshared((t2461 *)__this, method)
#define m13745(__this, p0, method) (bool)m12891_gshared((t2461 *)__this, (t29 *)p0, method)
#define m13746(__this, p0, method) (bool)m12893_gshared((t2461 *)__this, (t29 *)p0, method)
#define m13747(__this, p0, p1, method) (void)m12895_gshared((t2461 *)__this, (t733 *)p0, (t735 )p1, method)
#define m13748(__this, p0, method) (void)m12897_gshared((t2461 *)__this, (t29 *)p0, method)
#define m13749(__this, p0, method) (bool)m12899_gshared((t2461 *)__this, (t29 *)p0, method)
#define m1634(__this, p0, p1, method) (bool)m12900_gshared((t2461 *)__this, (t29 *)p0, (t29 **)p1, method)
#define m13750(__this, method) (t2560 *)m12902_gshared((t2461 *)__this, method)
#define m13751(__this, p0, method) (t3 *)m12904_gshared((t2461 *)__this, (t29 *)p0, method)
#define m13752(__this, p0, method) (t366 *)m12906_gshared((t2461 *)__this, (t29 *)p0, method)
 bool m13753 (t170 * __this, t2562  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2564  m13754 (t170 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
#define m13755(__this, p0, p1, method) (t725 )m12910_gshared((t29 *)__this, (t29 *)p0, (t29 *)p1, method)
