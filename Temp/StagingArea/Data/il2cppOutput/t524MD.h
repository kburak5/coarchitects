﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t524;
struct t524_marshaled;
struct t7;

 t7* m2666 (t524 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2667 (t524 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2668 (t524 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2669 (t524 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
void t524_marshal(const t524& unmarshaled, t524_marshaled& marshaled);
void t524_marshal_back(const t524_marshaled& marshaled, t524& unmarshaled);
void t524_marshal_cleanup(t524_marshaled& marshaled);
