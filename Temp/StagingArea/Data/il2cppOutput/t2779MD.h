﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2779;
struct t29;
struct t557;
struct t2780;
struct t316;

#include "t2119MD.h"
#define m15185(__this, p0, p1, method) (void)m10321_gshared((t2119 *)__this, (t29 *)p0, (t557 *)p1, method)
#define m15186(__this, p0, method) (void)m10322_gshared((t2119 *)__this, (t2120 *)p0, method)
#define m15187(__this, p0, method) (void)m10323_gshared((t2119 *)__this, (t316*)p0, method)
#define m15188(__this, p0, p1, method) (bool)m10324_gshared((t2119 *)__this, (t29 *)p0, (t557 *)p1, method)
