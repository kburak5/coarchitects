﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2518;
struct t29;
struct t20;
#include "t184.h"

 void m13296 (t2518 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m13297 (t2518 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13298 (t2518 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13299 (t2518 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t184  m13300 (t2518 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
