﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t4983_TI;


#include "t20.h"

// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IEquatable`1<System.TimeSpan>>
extern MethodInfo m33748_MI;
static PropertyInfo t4983____Current_PropertyInfo = 
{
	&t4983_TI, "Current", &m33748_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4983_PIs[] =
{
	&t4983____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2100_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33748_GM;
MethodInfo m33748_MI = 
{
	"get_Current", NULL, &t4983_TI, &t2100_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33748_GM};
static MethodInfo* t4983_MIs[] =
{
	&m33748_MI,
	NULL
};
extern TypeInfo t136_TI;
extern TypeInfo t324_TI;
static TypeInfo* t4983_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4983_0_0_0;
extern Il2CppType t4983_1_0_0;
struct t4983;
extern Il2CppGenericClass t4983_GC;
TypeInfo t4983_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4983_MIs, t4983_PIs, NULL, NULL, NULL, NULL, NULL, &t4983_TI, t4983_ITIs, NULL, &EmptyCustomAttributesCache, &t4983_TI, &t4983_0_0_0, &t4983_1_0_0, NULL, &t4983_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2100_TI;

#include "t40.h"
#include "t816.h"


// Metadata Definition System.IEquatable`1<System.TimeSpan>
extern Il2CppType t816_0_0_0;
extern Il2CppType t816_0_0_0;
static ParameterInfo t2100_m33749_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t816 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33749_GM;
MethodInfo m33749_MI = 
{
	"Equals", NULL, &t2100_TI, &t40_0_0_0, RuntimeInvoker_t40_t816, t2100_m33749_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33749_GM};
static MethodInfo* t2100_MIs[] =
{
	&m33749_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2100_0_0_0;
extern Il2CppType t2100_1_0_0;
struct t2100;
extern Il2CppGenericClass t2100_GC;
TypeInfo t2100_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t2100_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2100_TI, NULL, NULL, &EmptyCustomAttributesCache, &t2100_TI, &t2100_0_0_0, &t2100_1_0_0, NULL, &t2100_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t3445.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3445_TI;
#include "t3445MD.h"

#include "t29.h"
#include "t44.h"
#include "t7.h"
#include "t914.h"
#include "t21.h"
extern TypeInfo t914_TI;
#include "t914MD.h"
#include "t20MD.h"
extern MethodInfo m19099_MI;
extern MethodInfo m3964_MI;
extern MethodInfo m3969_MI;
extern MethodInfo m25842_MI;
struct t20;
#include "t915.h"
struct t20;
 t29 * m19490_gshared (t20 * __this, int32_t p0, MethodInfo* method);
#define m19490(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
#define m25842(__this, p0, method) (t29*)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.IEquatable`1<System.TimeSpan>>
extern Il2CppType t20_0_0_1;
FieldInfo t3445_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3445_TI, offsetof(t3445, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3445_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3445_TI, offsetof(t3445, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3445_FIs[] =
{
	&t3445_f0_FieldInfo,
	&t3445_f1_FieldInfo,
	NULL
};
extern MethodInfo m19096_MI;
static PropertyInfo t3445____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3445_TI, "System.Collections.IEnumerator.Current", &m19096_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3445____Current_PropertyInfo = 
{
	&t3445_TI, "Current", &m19099_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3445_PIs[] =
{
	&t3445____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3445____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
static ParameterInfo t3445_m19095_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19095_GM;
MethodInfo m19095_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3445_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3445_m19095_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19095_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19096_GM;
MethodInfo m19096_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3445_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19096_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19097_GM;
MethodInfo m19097_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3445_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19097_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19098_GM;
MethodInfo m19098_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3445_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19098_GM};
extern Il2CppType t2100_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19099_GM;
MethodInfo m19099_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3445_TI, &t2100_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19099_GM};
static MethodInfo* t3445_MIs[] =
{
	&m19095_MI,
	&m19096_MI,
	&m19097_MI,
	&m19098_MI,
	&m19099_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
extern MethodInfo m19098_MI;
extern MethodInfo m19097_MI;
static MethodInfo* t3445_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19096_MI,
	&m19098_MI,
	&m19097_MI,
	&m19099_MI,
};
static TypeInfo* t3445_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4983_TI,
};
static Il2CppInterfaceOffsetPair t3445_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4983_TI, 7},
};
extern TypeInfo t2100_TI;
static Il2CppRGCTXData t3445_RGCTXData[3] = 
{
	&m19099_MI/* Method Usage */,
	&t2100_TI/* Class Usage */,
	&m25842_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3445_0_0_0;
extern Il2CppType t3445_1_0_0;
extern TypeInfo t110_TI;
extern Il2CppGenericClass t3445_GC;
extern TypeInfo t20_TI;
TypeInfo t3445_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3445_MIs, t3445_PIs, t3445_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3445_TI, t3445_ITIs, t3445_VT, &EmptyCustomAttributesCache, &t3445_TI, &t3445_0_0_0, &t3445_1_0_0, t3445_IOs, &t3445_GC, NULL, NULL, NULL, t3445_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3445)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6523_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IEquatable`1<System.TimeSpan>>
extern MethodInfo m33750_MI;
extern MethodInfo m33751_MI;
static PropertyInfo t6523____Item_PropertyInfo = 
{
	&t6523_TI, "Item", &m33750_MI, &m33751_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6523_PIs[] =
{
	&t6523____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2100_0_0_0;
static ParameterInfo t6523_m33752_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2100_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33752_GM;
MethodInfo m33752_MI = 
{
	"IndexOf", NULL, &t6523_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6523_m33752_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33752_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2100_0_0_0;
static ParameterInfo t6523_m33753_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t2100_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33753_GM;
MethodInfo m33753_MI = 
{
	"Insert", NULL, &t6523_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6523_m33753_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33753_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6523_m33754_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33754_GM;
MethodInfo m33754_MI = 
{
	"RemoveAt", NULL, &t6523_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6523_m33754_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33754_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6523_m33750_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2100_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33750_GM;
MethodInfo m33750_MI = 
{
	"get_Item", NULL, &t6523_TI, &t2100_0_0_0, RuntimeInvoker_t29_t44, t6523_m33750_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33750_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2100_0_0_0;
static ParameterInfo t6523_m33751_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2100_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33751_GM;
MethodInfo m33751_MI = 
{
	"set_Item", NULL, &t6523_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6523_m33751_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33751_GM};
static MethodInfo* t6523_MIs[] =
{
	&m33752_MI,
	&m33753_MI,
	&m33754_MI,
	&m33750_MI,
	&m33751_MI,
	NULL
};
extern TypeInfo t603_TI;
extern TypeInfo t6522_TI;
extern TypeInfo t6524_TI;
static TypeInfo* t6523_ITIs[] = 
{
	&t603_TI,
	&t6522_TI,
	&t6524_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6523_0_0_0;
extern Il2CppType t6523_1_0_0;
struct t6523;
extern Il2CppGenericClass t6523_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6523_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6523_MIs, t6523_PIs, NULL, NULL, NULL, NULL, NULL, &t6523_TI, t6523_ITIs, NULL, &t1908__CustomAttributeCache, &t6523_TI, &t6523_0_0_0, &t6523_1_0_0, NULL, &t6523_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4985_TI;

#include "t1495.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Serialization.Formatters.FormatterAssemblyStyle>
extern MethodInfo m33755_MI;
static PropertyInfo t4985____Current_PropertyInfo = 
{
	&t4985_TI, "Current", &m33755_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4985_PIs[] =
{
	&t4985____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1495_0_0_0;
extern void* RuntimeInvoker_t1495 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33755_GM;
MethodInfo m33755_MI = 
{
	"get_Current", NULL, &t4985_TI, &t1495_0_0_0, RuntimeInvoker_t1495, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33755_GM};
static MethodInfo* t4985_MIs[] =
{
	&m33755_MI,
	NULL
};
static TypeInfo* t4985_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4985_0_0_0;
extern Il2CppType t4985_1_0_0;
struct t4985;
extern Il2CppGenericClass t4985_GC;
TypeInfo t4985_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4985_MIs, t4985_PIs, NULL, NULL, NULL, NULL, NULL, &t4985_TI, t4985_ITIs, NULL, &EmptyCustomAttributesCache, &t4985_TI, &t4985_0_0_0, &t4985_1_0_0, NULL, &t4985_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3446.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3446_TI;
#include "t3446MD.h"

extern TypeInfo t1495_TI;
extern MethodInfo m19104_MI;
extern MethodInfo m25853_MI;
struct t20;
 int32_t m25853 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m19100_MI;
 void m19100 (t3446 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19101_MI;
 t29 * m19101 (t3446 * __this, MethodInfo* method){
	{
		int32_t L_0 = m19104(__this, &m19104_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1495_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m19102_MI;
 void m19102 (t3446 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19103_MI;
 bool m19103 (t3446 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m19104 (t3446 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m25853(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25853_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.FormatterAssemblyStyle>
extern Il2CppType t20_0_0_1;
FieldInfo t3446_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3446_TI, offsetof(t3446, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3446_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3446_TI, offsetof(t3446, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3446_FIs[] =
{
	&t3446_f0_FieldInfo,
	&t3446_f1_FieldInfo,
	NULL
};
static PropertyInfo t3446____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3446_TI, "System.Collections.IEnumerator.Current", &m19101_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3446____Current_PropertyInfo = 
{
	&t3446_TI, "Current", &m19104_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3446_PIs[] =
{
	&t3446____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3446____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3446_m19100_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19100_GM;
MethodInfo m19100_MI = 
{
	".ctor", (methodPointerType)&m19100, &t3446_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3446_m19100_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19100_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19101_GM;
MethodInfo m19101_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m19101, &t3446_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19101_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19102_GM;
MethodInfo m19102_MI = 
{
	"Dispose", (methodPointerType)&m19102, &t3446_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19102_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19103_GM;
MethodInfo m19103_MI = 
{
	"MoveNext", (methodPointerType)&m19103, &t3446_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19103_GM};
extern Il2CppType t1495_0_0_0;
extern void* RuntimeInvoker_t1495 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19104_GM;
MethodInfo m19104_MI = 
{
	"get_Current", (methodPointerType)&m19104, &t3446_TI, &t1495_0_0_0, RuntimeInvoker_t1495, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19104_GM};
static MethodInfo* t3446_MIs[] =
{
	&m19100_MI,
	&m19101_MI,
	&m19102_MI,
	&m19103_MI,
	&m19104_MI,
	NULL
};
static MethodInfo* t3446_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19101_MI,
	&m19103_MI,
	&m19102_MI,
	&m19104_MI,
};
static TypeInfo* t3446_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4985_TI,
};
static Il2CppInterfaceOffsetPair t3446_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4985_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3446_0_0_0;
extern Il2CppType t3446_1_0_0;
extern Il2CppGenericClass t3446_GC;
TypeInfo t3446_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3446_MIs, t3446_PIs, t3446_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3446_TI, t3446_ITIs, t3446_VT, &EmptyCustomAttributesCache, &t3446_TI, &t3446_0_0_0, &t3446_1_0_0, t3446_IOs, &t3446_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3446)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6525_TI;

#include "mscorlib_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.FormatterAssemblyStyle>
extern MethodInfo m33756_MI;
static PropertyInfo t6525____Count_PropertyInfo = 
{
	&t6525_TI, "Count", &m33756_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33757_MI;
static PropertyInfo t6525____IsReadOnly_PropertyInfo = 
{
	&t6525_TI, "IsReadOnly", &m33757_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6525_PIs[] =
{
	&t6525____Count_PropertyInfo,
	&t6525____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33756_GM;
MethodInfo m33756_MI = 
{
	"get_Count", NULL, &t6525_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33756_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33757_GM;
MethodInfo m33757_MI = 
{
	"get_IsReadOnly", NULL, &t6525_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33757_GM};
extern Il2CppType t1495_0_0_0;
extern Il2CppType t1495_0_0_0;
static ParameterInfo t6525_m33758_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1495_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33758_GM;
MethodInfo m33758_MI = 
{
	"Add", NULL, &t6525_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6525_m33758_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33758_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33759_GM;
MethodInfo m33759_MI = 
{
	"Clear", NULL, &t6525_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33759_GM};
extern Il2CppType t1495_0_0_0;
static ParameterInfo t6525_m33760_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1495_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33760_GM;
MethodInfo m33760_MI = 
{
	"Contains", NULL, &t6525_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6525_m33760_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33760_GM};
extern Il2CppType t3694_0_0_0;
extern Il2CppType t3694_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6525_m33761_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3694_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33761_GM;
MethodInfo m33761_MI = 
{
	"CopyTo", NULL, &t6525_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6525_m33761_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33761_GM};
extern Il2CppType t1495_0_0_0;
static ParameterInfo t6525_m33762_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1495_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33762_GM;
MethodInfo m33762_MI = 
{
	"Remove", NULL, &t6525_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6525_m33762_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33762_GM};
static MethodInfo* t6525_MIs[] =
{
	&m33756_MI,
	&m33757_MI,
	&m33758_MI,
	&m33759_MI,
	&m33760_MI,
	&m33761_MI,
	&m33762_MI,
	NULL
};
extern TypeInfo t6527_TI;
static TypeInfo* t6525_ITIs[] = 
{
	&t603_TI,
	&t6527_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6525_0_0_0;
extern Il2CppType t6525_1_0_0;
struct t6525;
extern Il2CppGenericClass t6525_GC;
TypeInfo t6525_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6525_MIs, t6525_PIs, NULL, NULL, NULL, NULL, NULL, &t6525_TI, t6525_ITIs, NULL, &EmptyCustomAttributesCache, &t6525_TI, &t6525_0_0_0, &t6525_1_0_0, NULL, &t6525_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Serialization.Formatters.FormatterAssemblyStyle>
extern Il2CppType t4985_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33763_GM;
MethodInfo m33763_MI = 
{
	"GetEnumerator", NULL, &t6527_TI, &t4985_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33763_GM};
static MethodInfo* t6527_MIs[] =
{
	&m33763_MI,
	NULL
};
static TypeInfo* t6527_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6527_0_0_0;
extern Il2CppType t6527_1_0_0;
struct t6527;
extern Il2CppGenericClass t6527_GC;
TypeInfo t6527_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6527_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6527_TI, t6527_ITIs, NULL, &EmptyCustomAttributesCache, &t6527_TI, &t6527_0_0_0, &t6527_1_0_0, NULL, &t6527_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6526_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.FormatterAssemblyStyle>
extern MethodInfo m33764_MI;
extern MethodInfo m33765_MI;
static PropertyInfo t6526____Item_PropertyInfo = 
{
	&t6526_TI, "Item", &m33764_MI, &m33765_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6526_PIs[] =
{
	&t6526____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1495_0_0_0;
static ParameterInfo t6526_m33766_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1495_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33766_GM;
MethodInfo m33766_MI = 
{
	"IndexOf", NULL, &t6526_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6526_m33766_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33766_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1495_0_0_0;
static ParameterInfo t6526_m33767_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1495_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33767_GM;
MethodInfo m33767_MI = 
{
	"Insert", NULL, &t6526_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6526_m33767_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33767_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6526_m33768_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33768_GM;
MethodInfo m33768_MI = 
{
	"RemoveAt", NULL, &t6526_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6526_m33768_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33768_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6526_m33764_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1495_0_0_0;
extern void* RuntimeInvoker_t1495_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33764_GM;
MethodInfo m33764_MI = 
{
	"get_Item", NULL, &t6526_TI, &t1495_0_0_0, RuntimeInvoker_t1495_t44, t6526_m33764_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33764_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1495_0_0_0;
static ParameterInfo t6526_m33765_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1495_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33765_GM;
MethodInfo m33765_MI = 
{
	"set_Item", NULL, &t6526_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6526_m33765_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33765_GM};
static MethodInfo* t6526_MIs[] =
{
	&m33766_MI,
	&m33767_MI,
	&m33768_MI,
	&m33764_MI,
	&m33765_MI,
	NULL
};
static TypeInfo* t6526_ITIs[] = 
{
	&t603_TI,
	&t6525_TI,
	&t6527_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6526_0_0_0;
extern Il2CppType t6526_1_0_0;
struct t6526;
extern Il2CppGenericClass t6526_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6526_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6526_MIs, t6526_PIs, NULL, NULL, NULL, NULL, NULL, &t6526_TI, t6526_ITIs, NULL, &t1908__CustomAttributeCache, &t6526_TI, &t6526_0_0_0, &t6526_1_0_0, NULL, &t6526_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4987_TI;

#include "t1496.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Serialization.Formatters.FormatterTypeStyle>
extern MethodInfo m33769_MI;
static PropertyInfo t4987____Current_PropertyInfo = 
{
	&t4987_TI, "Current", &m33769_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4987_PIs[] =
{
	&t4987____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1496_0_0_0;
extern void* RuntimeInvoker_t1496 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33769_GM;
MethodInfo m33769_MI = 
{
	"get_Current", NULL, &t4987_TI, &t1496_0_0_0, RuntimeInvoker_t1496, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33769_GM};
static MethodInfo* t4987_MIs[] =
{
	&m33769_MI,
	NULL
};
static TypeInfo* t4987_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4987_0_0_0;
extern Il2CppType t4987_1_0_0;
struct t4987;
extern Il2CppGenericClass t4987_GC;
TypeInfo t4987_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4987_MIs, t4987_PIs, NULL, NULL, NULL, NULL, NULL, &t4987_TI, t4987_ITIs, NULL, &EmptyCustomAttributesCache, &t4987_TI, &t4987_0_0_0, &t4987_1_0_0, NULL, &t4987_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3447.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3447_TI;
#include "t3447MD.h"

extern TypeInfo t1496_TI;
extern MethodInfo m19109_MI;
extern MethodInfo m25864_MI;
struct t20;
 int32_t m25864 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m19105_MI;
 void m19105 (t3447 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19106_MI;
 t29 * m19106 (t3447 * __this, MethodInfo* method){
	{
		int32_t L_0 = m19109(__this, &m19109_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1496_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m19107_MI;
 void m19107 (t3447 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19108_MI;
 bool m19108 (t3447 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m19109 (t3447 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m25864(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25864_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.FormatterTypeStyle>
extern Il2CppType t20_0_0_1;
FieldInfo t3447_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3447_TI, offsetof(t3447, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3447_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3447_TI, offsetof(t3447, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3447_FIs[] =
{
	&t3447_f0_FieldInfo,
	&t3447_f1_FieldInfo,
	NULL
};
static PropertyInfo t3447____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3447_TI, "System.Collections.IEnumerator.Current", &m19106_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3447____Current_PropertyInfo = 
{
	&t3447_TI, "Current", &m19109_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3447_PIs[] =
{
	&t3447____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3447____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3447_m19105_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19105_GM;
MethodInfo m19105_MI = 
{
	".ctor", (methodPointerType)&m19105, &t3447_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3447_m19105_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19105_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19106_GM;
MethodInfo m19106_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m19106, &t3447_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19106_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19107_GM;
MethodInfo m19107_MI = 
{
	"Dispose", (methodPointerType)&m19107, &t3447_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19107_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19108_GM;
MethodInfo m19108_MI = 
{
	"MoveNext", (methodPointerType)&m19108, &t3447_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19108_GM};
extern Il2CppType t1496_0_0_0;
extern void* RuntimeInvoker_t1496 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19109_GM;
MethodInfo m19109_MI = 
{
	"get_Current", (methodPointerType)&m19109, &t3447_TI, &t1496_0_0_0, RuntimeInvoker_t1496, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19109_GM};
static MethodInfo* t3447_MIs[] =
{
	&m19105_MI,
	&m19106_MI,
	&m19107_MI,
	&m19108_MI,
	&m19109_MI,
	NULL
};
static MethodInfo* t3447_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19106_MI,
	&m19108_MI,
	&m19107_MI,
	&m19109_MI,
};
static TypeInfo* t3447_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4987_TI,
};
static Il2CppInterfaceOffsetPair t3447_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4987_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3447_0_0_0;
extern Il2CppType t3447_1_0_0;
extern Il2CppGenericClass t3447_GC;
TypeInfo t3447_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3447_MIs, t3447_PIs, t3447_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3447_TI, t3447_ITIs, t3447_VT, &EmptyCustomAttributesCache, &t3447_TI, &t3447_0_0_0, &t3447_1_0_0, t3447_IOs, &t3447_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3447)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6528_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.FormatterTypeStyle>
extern MethodInfo m33770_MI;
static PropertyInfo t6528____Count_PropertyInfo = 
{
	&t6528_TI, "Count", &m33770_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33771_MI;
static PropertyInfo t6528____IsReadOnly_PropertyInfo = 
{
	&t6528_TI, "IsReadOnly", &m33771_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6528_PIs[] =
{
	&t6528____Count_PropertyInfo,
	&t6528____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33770_GM;
MethodInfo m33770_MI = 
{
	"get_Count", NULL, &t6528_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33770_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33771_GM;
MethodInfo m33771_MI = 
{
	"get_IsReadOnly", NULL, &t6528_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33771_GM};
extern Il2CppType t1496_0_0_0;
extern Il2CppType t1496_0_0_0;
static ParameterInfo t6528_m33772_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1496_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33772_GM;
MethodInfo m33772_MI = 
{
	"Add", NULL, &t6528_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6528_m33772_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33772_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33773_GM;
MethodInfo m33773_MI = 
{
	"Clear", NULL, &t6528_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33773_GM};
extern Il2CppType t1496_0_0_0;
static ParameterInfo t6528_m33774_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1496_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33774_GM;
MethodInfo m33774_MI = 
{
	"Contains", NULL, &t6528_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6528_m33774_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33774_GM};
extern Il2CppType t3695_0_0_0;
extern Il2CppType t3695_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6528_m33775_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3695_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33775_GM;
MethodInfo m33775_MI = 
{
	"CopyTo", NULL, &t6528_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6528_m33775_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33775_GM};
extern Il2CppType t1496_0_0_0;
static ParameterInfo t6528_m33776_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1496_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33776_GM;
MethodInfo m33776_MI = 
{
	"Remove", NULL, &t6528_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6528_m33776_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33776_GM};
static MethodInfo* t6528_MIs[] =
{
	&m33770_MI,
	&m33771_MI,
	&m33772_MI,
	&m33773_MI,
	&m33774_MI,
	&m33775_MI,
	&m33776_MI,
	NULL
};
extern TypeInfo t6530_TI;
static TypeInfo* t6528_ITIs[] = 
{
	&t603_TI,
	&t6530_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6528_0_0_0;
extern Il2CppType t6528_1_0_0;
struct t6528;
extern Il2CppGenericClass t6528_GC;
TypeInfo t6528_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6528_MIs, t6528_PIs, NULL, NULL, NULL, NULL, NULL, &t6528_TI, t6528_ITIs, NULL, &EmptyCustomAttributesCache, &t6528_TI, &t6528_0_0_0, &t6528_1_0_0, NULL, &t6528_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Serialization.Formatters.FormatterTypeStyle>
extern Il2CppType t4987_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33777_GM;
MethodInfo m33777_MI = 
{
	"GetEnumerator", NULL, &t6530_TI, &t4987_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33777_GM};
static MethodInfo* t6530_MIs[] =
{
	&m33777_MI,
	NULL
};
static TypeInfo* t6530_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6530_0_0_0;
extern Il2CppType t6530_1_0_0;
struct t6530;
extern Il2CppGenericClass t6530_GC;
TypeInfo t6530_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6530_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6530_TI, t6530_ITIs, NULL, &EmptyCustomAttributesCache, &t6530_TI, &t6530_0_0_0, &t6530_1_0_0, NULL, &t6530_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6529_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.FormatterTypeStyle>
extern MethodInfo m33778_MI;
extern MethodInfo m33779_MI;
static PropertyInfo t6529____Item_PropertyInfo = 
{
	&t6529_TI, "Item", &m33778_MI, &m33779_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6529_PIs[] =
{
	&t6529____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1496_0_0_0;
static ParameterInfo t6529_m33780_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1496_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33780_GM;
MethodInfo m33780_MI = 
{
	"IndexOf", NULL, &t6529_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6529_m33780_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33780_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1496_0_0_0;
static ParameterInfo t6529_m33781_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1496_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33781_GM;
MethodInfo m33781_MI = 
{
	"Insert", NULL, &t6529_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6529_m33781_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33781_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6529_m33782_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33782_GM;
MethodInfo m33782_MI = 
{
	"RemoveAt", NULL, &t6529_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6529_m33782_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33782_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6529_m33778_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1496_0_0_0;
extern void* RuntimeInvoker_t1496_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33778_GM;
MethodInfo m33778_MI = 
{
	"get_Item", NULL, &t6529_TI, &t1496_0_0_0, RuntimeInvoker_t1496_t44, t6529_m33778_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33778_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1496_0_0_0;
static ParameterInfo t6529_m33779_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1496_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33779_GM;
MethodInfo m33779_MI = 
{
	"set_Item", NULL, &t6529_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6529_m33779_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33779_GM};
static MethodInfo* t6529_MIs[] =
{
	&m33780_MI,
	&m33781_MI,
	&m33782_MI,
	&m33778_MI,
	&m33779_MI,
	NULL
};
static TypeInfo* t6529_ITIs[] = 
{
	&t603_TI,
	&t6528_TI,
	&t6530_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6529_0_0_0;
extern Il2CppType t6529_1_0_0;
struct t6529;
extern Il2CppGenericClass t6529_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6529_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6529_MIs, t6529_PIs, NULL, NULL, NULL, NULL, NULL, &t6529_TI, t6529_ITIs, NULL, &t1908__CustomAttributeCache, &t6529_TI, &t6529_0_0_0, &t6529_1_0_0, NULL, &t6529_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4989_TI;

#include "t1497.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Serialization.Formatters.TypeFilterLevel>
extern MethodInfo m33783_MI;
static PropertyInfo t4989____Current_PropertyInfo = 
{
	&t4989_TI, "Current", &m33783_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4989_PIs[] =
{
	&t4989____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1497_0_0_0;
extern void* RuntimeInvoker_t1497 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33783_GM;
MethodInfo m33783_MI = 
{
	"get_Current", NULL, &t4989_TI, &t1497_0_0_0, RuntimeInvoker_t1497, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33783_GM};
static MethodInfo* t4989_MIs[] =
{
	&m33783_MI,
	NULL
};
static TypeInfo* t4989_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4989_0_0_0;
extern Il2CppType t4989_1_0_0;
struct t4989;
extern Il2CppGenericClass t4989_GC;
TypeInfo t4989_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4989_MIs, t4989_PIs, NULL, NULL, NULL, NULL, NULL, &t4989_TI, t4989_ITIs, NULL, &EmptyCustomAttributesCache, &t4989_TI, &t4989_0_0_0, &t4989_1_0_0, NULL, &t4989_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3448.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3448_TI;
#include "t3448MD.h"

extern TypeInfo t1497_TI;
extern MethodInfo m19114_MI;
extern MethodInfo m25875_MI;
struct t20;
 int32_t m25875 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m19110_MI;
 void m19110 (t3448 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19111_MI;
 t29 * m19111 (t3448 * __this, MethodInfo* method){
	{
		int32_t L_0 = m19114(__this, &m19114_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1497_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m19112_MI;
 void m19112 (t3448 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19113_MI;
 bool m19113 (t3448 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m19114 (t3448 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m25875(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25875_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.TypeFilterLevel>
extern Il2CppType t20_0_0_1;
FieldInfo t3448_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3448_TI, offsetof(t3448, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3448_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3448_TI, offsetof(t3448, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3448_FIs[] =
{
	&t3448_f0_FieldInfo,
	&t3448_f1_FieldInfo,
	NULL
};
static PropertyInfo t3448____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3448_TI, "System.Collections.IEnumerator.Current", &m19111_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3448____Current_PropertyInfo = 
{
	&t3448_TI, "Current", &m19114_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3448_PIs[] =
{
	&t3448____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3448____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3448_m19110_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19110_GM;
MethodInfo m19110_MI = 
{
	".ctor", (methodPointerType)&m19110, &t3448_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3448_m19110_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19110_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19111_GM;
MethodInfo m19111_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m19111, &t3448_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19111_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19112_GM;
MethodInfo m19112_MI = 
{
	"Dispose", (methodPointerType)&m19112, &t3448_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19112_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19113_GM;
MethodInfo m19113_MI = 
{
	"MoveNext", (methodPointerType)&m19113, &t3448_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19113_GM};
extern Il2CppType t1497_0_0_0;
extern void* RuntimeInvoker_t1497 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19114_GM;
MethodInfo m19114_MI = 
{
	"get_Current", (methodPointerType)&m19114, &t3448_TI, &t1497_0_0_0, RuntimeInvoker_t1497, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19114_GM};
static MethodInfo* t3448_MIs[] =
{
	&m19110_MI,
	&m19111_MI,
	&m19112_MI,
	&m19113_MI,
	&m19114_MI,
	NULL
};
static MethodInfo* t3448_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19111_MI,
	&m19113_MI,
	&m19112_MI,
	&m19114_MI,
};
static TypeInfo* t3448_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4989_TI,
};
static Il2CppInterfaceOffsetPair t3448_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4989_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3448_0_0_0;
extern Il2CppType t3448_1_0_0;
extern Il2CppGenericClass t3448_GC;
TypeInfo t3448_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3448_MIs, t3448_PIs, t3448_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3448_TI, t3448_ITIs, t3448_VT, &EmptyCustomAttributesCache, &t3448_TI, &t3448_0_0_0, &t3448_1_0_0, t3448_IOs, &t3448_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3448)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6531_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.TypeFilterLevel>
extern MethodInfo m33784_MI;
static PropertyInfo t6531____Count_PropertyInfo = 
{
	&t6531_TI, "Count", &m33784_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33785_MI;
static PropertyInfo t6531____IsReadOnly_PropertyInfo = 
{
	&t6531_TI, "IsReadOnly", &m33785_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6531_PIs[] =
{
	&t6531____Count_PropertyInfo,
	&t6531____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33784_GM;
MethodInfo m33784_MI = 
{
	"get_Count", NULL, &t6531_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33784_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33785_GM;
MethodInfo m33785_MI = 
{
	"get_IsReadOnly", NULL, &t6531_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33785_GM};
extern Il2CppType t1497_0_0_0;
extern Il2CppType t1497_0_0_0;
static ParameterInfo t6531_m33786_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1497_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33786_GM;
MethodInfo m33786_MI = 
{
	"Add", NULL, &t6531_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6531_m33786_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33786_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33787_GM;
MethodInfo m33787_MI = 
{
	"Clear", NULL, &t6531_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33787_GM};
extern Il2CppType t1497_0_0_0;
static ParameterInfo t6531_m33788_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1497_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33788_GM;
MethodInfo m33788_MI = 
{
	"Contains", NULL, &t6531_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6531_m33788_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33788_GM};
extern Il2CppType t3696_0_0_0;
extern Il2CppType t3696_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6531_m33789_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3696_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33789_GM;
MethodInfo m33789_MI = 
{
	"CopyTo", NULL, &t6531_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6531_m33789_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33789_GM};
extern Il2CppType t1497_0_0_0;
static ParameterInfo t6531_m33790_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1497_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33790_GM;
MethodInfo m33790_MI = 
{
	"Remove", NULL, &t6531_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6531_m33790_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33790_GM};
static MethodInfo* t6531_MIs[] =
{
	&m33784_MI,
	&m33785_MI,
	&m33786_MI,
	&m33787_MI,
	&m33788_MI,
	&m33789_MI,
	&m33790_MI,
	NULL
};
extern TypeInfo t6533_TI;
static TypeInfo* t6531_ITIs[] = 
{
	&t603_TI,
	&t6533_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6531_0_0_0;
extern Il2CppType t6531_1_0_0;
struct t6531;
extern Il2CppGenericClass t6531_GC;
TypeInfo t6531_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6531_MIs, t6531_PIs, NULL, NULL, NULL, NULL, NULL, &t6531_TI, t6531_ITIs, NULL, &EmptyCustomAttributesCache, &t6531_TI, &t6531_0_0_0, &t6531_1_0_0, NULL, &t6531_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Serialization.Formatters.TypeFilterLevel>
extern Il2CppType t4989_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33791_GM;
MethodInfo m33791_MI = 
{
	"GetEnumerator", NULL, &t6533_TI, &t4989_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33791_GM};
static MethodInfo* t6533_MIs[] =
{
	&m33791_MI,
	NULL
};
static TypeInfo* t6533_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6533_0_0_0;
extern Il2CppType t6533_1_0_0;
struct t6533;
extern Il2CppGenericClass t6533_GC;
TypeInfo t6533_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6533_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6533_TI, t6533_ITIs, NULL, &EmptyCustomAttributesCache, &t6533_TI, &t6533_0_0_0, &t6533_1_0_0, NULL, &t6533_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6532_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.TypeFilterLevel>
extern MethodInfo m33792_MI;
extern MethodInfo m33793_MI;
static PropertyInfo t6532____Item_PropertyInfo = 
{
	&t6532_TI, "Item", &m33792_MI, &m33793_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6532_PIs[] =
{
	&t6532____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1497_0_0_0;
static ParameterInfo t6532_m33794_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1497_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33794_GM;
MethodInfo m33794_MI = 
{
	"IndexOf", NULL, &t6532_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6532_m33794_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33794_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1497_0_0_0;
static ParameterInfo t6532_m33795_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1497_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33795_GM;
MethodInfo m33795_MI = 
{
	"Insert", NULL, &t6532_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6532_m33795_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33795_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6532_m33796_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33796_GM;
MethodInfo m33796_MI = 
{
	"RemoveAt", NULL, &t6532_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6532_m33796_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33796_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6532_m33792_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1497_0_0_0;
extern void* RuntimeInvoker_t1497_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33792_GM;
MethodInfo m33792_MI = 
{
	"get_Item", NULL, &t6532_TI, &t1497_0_0_0, RuntimeInvoker_t1497_t44, t6532_m33792_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33792_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1497_0_0_0;
static ParameterInfo t6532_m33793_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1497_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33793_GM;
MethodInfo m33793_MI = 
{
	"set_Item", NULL, &t6532_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6532_m33793_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33793_GM};
static MethodInfo* t6532_MIs[] =
{
	&m33794_MI,
	&m33795_MI,
	&m33796_MI,
	&m33792_MI,
	&m33793_MI,
	NULL
};
static TypeInfo* t6532_ITIs[] = 
{
	&t603_TI,
	&t6531_TI,
	&t6533_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6532_0_0_0;
extern Il2CppType t6532_1_0_0;
struct t6532;
extern Il2CppGenericClass t6532_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6532_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6532_MIs, t6532_PIs, NULL, NULL, NULL, NULL, NULL, &t6532_TI, t6532_ITIs, NULL, &t1908__CustomAttributeCache, &t6532_TI, &t6532_0_0_0, &t6532_1_0_0, NULL, &t6532_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4991_TI;

#include "t1513.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Serialization.ObjectRecordStatus>
extern MethodInfo m33797_MI;
static PropertyInfo t4991____Current_PropertyInfo = 
{
	&t4991_TI, "Current", &m33797_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4991_PIs[] =
{
	&t4991____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1513_0_0_0;
extern void* RuntimeInvoker_t1513 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33797_GM;
MethodInfo m33797_MI = 
{
	"get_Current", NULL, &t4991_TI, &t1513_0_0_0, RuntimeInvoker_t1513, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33797_GM};
static MethodInfo* t4991_MIs[] =
{
	&m33797_MI,
	NULL
};
static TypeInfo* t4991_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4991_0_0_0;
extern Il2CppType t4991_1_0_0;
struct t4991;
extern Il2CppGenericClass t4991_GC;
TypeInfo t4991_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4991_MIs, t4991_PIs, NULL, NULL, NULL, NULL, NULL, &t4991_TI, t4991_ITIs, NULL, &EmptyCustomAttributesCache, &t4991_TI, &t4991_0_0_0, &t4991_1_0_0, NULL, &t4991_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3449.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3449_TI;
#include "t3449MD.h"

extern TypeInfo t1513_TI;
extern MethodInfo m19119_MI;
extern MethodInfo m25886_MI;
struct t20;
 uint8_t m25886 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m19115_MI;
 void m19115 (t3449 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19116_MI;
 t29 * m19116 (t3449 * __this, MethodInfo* method){
	{
		uint8_t L_0 = m19119(__this, &m19119_MI);
		uint8_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1513_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m19117_MI;
 void m19117 (t3449 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19118_MI;
 bool m19118 (t3449 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 uint8_t m19119 (t3449 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		uint8_t L_8 = m25886(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25886_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Serialization.ObjectRecordStatus>
extern Il2CppType t20_0_0_1;
FieldInfo t3449_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3449_TI, offsetof(t3449, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3449_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3449_TI, offsetof(t3449, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3449_FIs[] =
{
	&t3449_f0_FieldInfo,
	&t3449_f1_FieldInfo,
	NULL
};
static PropertyInfo t3449____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3449_TI, "System.Collections.IEnumerator.Current", &m19116_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3449____Current_PropertyInfo = 
{
	&t3449_TI, "Current", &m19119_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3449_PIs[] =
{
	&t3449____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3449____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3449_m19115_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19115_GM;
MethodInfo m19115_MI = 
{
	".ctor", (methodPointerType)&m19115, &t3449_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3449_m19115_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19115_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19116_GM;
MethodInfo m19116_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m19116, &t3449_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19116_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19117_GM;
MethodInfo m19117_MI = 
{
	"Dispose", (methodPointerType)&m19117, &t3449_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19117_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19118_GM;
MethodInfo m19118_MI = 
{
	"MoveNext", (methodPointerType)&m19118, &t3449_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19118_GM};
extern Il2CppType t1513_0_0_0;
extern void* RuntimeInvoker_t1513 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19119_GM;
MethodInfo m19119_MI = 
{
	"get_Current", (methodPointerType)&m19119, &t3449_TI, &t1513_0_0_0, RuntimeInvoker_t1513, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19119_GM};
static MethodInfo* t3449_MIs[] =
{
	&m19115_MI,
	&m19116_MI,
	&m19117_MI,
	&m19118_MI,
	&m19119_MI,
	NULL
};
static MethodInfo* t3449_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19116_MI,
	&m19118_MI,
	&m19117_MI,
	&m19119_MI,
};
static TypeInfo* t3449_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4991_TI,
};
static Il2CppInterfaceOffsetPair t3449_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4991_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3449_0_0_0;
extern Il2CppType t3449_1_0_0;
extern Il2CppGenericClass t3449_GC;
TypeInfo t3449_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3449_MIs, t3449_PIs, t3449_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3449_TI, t3449_ITIs, t3449_VT, &EmptyCustomAttributesCache, &t3449_TI, &t3449_0_0_0, &t3449_1_0_0, t3449_IOs, &t3449_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3449)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6534_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Serialization.ObjectRecordStatus>
extern MethodInfo m33798_MI;
static PropertyInfo t6534____Count_PropertyInfo = 
{
	&t6534_TI, "Count", &m33798_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33799_MI;
static PropertyInfo t6534____IsReadOnly_PropertyInfo = 
{
	&t6534_TI, "IsReadOnly", &m33799_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6534_PIs[] =
{
	&t6534____Count_PropertyInfo,
	&t6534____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33798_GM;
MethodInfo m33798_MI = 
{
	"get_Count", NULL, &t6534_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33798_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33799_GM;
MethodInfo m33799_MI = 
{
	"get_IsReadOnly", NULL, &t6534_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33799_GM};
extern Il2CppType t1513_0_0_0;
extern Il2CppType t1513_0_0_0;
static ParameterInfo t6534_m33800_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1513_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33800_GM;
MethodInfo m33800_MI = 
{
	"Add", NULL, &t6534_TI, &t21_0_0_0, RuntimeInvoker_t21_t348, t6534_m33800_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33800_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33801_GM;
MethodInfo m33801_MI = 
{
	"Clear", NULL, &t6534_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33801_GM};
extern Il2CppType t1513_0_0_0;
static ParameterInfo t6534_m33802_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1513_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33802_GM;
MethodInfo m33802_MI = 
{
	"Contains", NULL, &t6534_TI, &t40_0_0_0, RuntimeInvoker_t40_t348, t6534_m33802_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33802_GM};
extern Il2CppType t3697_0_0_0;
extern Il2CppType t3697_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6534_m33803_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3697_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33803_GM;
MethodInfo m33803_MI = 
{
	"CopyTo", NULL, &t6534_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6534_m33803_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33803_GM};
extern Il2CppType t1513_0_0_0;
static ParameterInfo t6534_m33804_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1513_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33804_GM;
MethodInfo m33804_MI = 
{
	"Remove", NULL, &t6534_TI, &t40_0_0_0, RuntimeInvoker_t40_t348, t6534_m33804_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33804_GM};
static MethodInfo* t6534_MIs[] =
{
	&m33798_MI,
	&m33799_MI,
	&m33800_MI,
	&m33801_MI,
	&m33802_MI,
	&m33803_MI,
	&m33804_MI,
	NULL
};
extern TypeInfo t6536_TI;
static TypeInfo* t6534_ITIs[] = 
{
	&t603_TI,
	&t6536_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6534_0_0_0;
extern Il2CppType t6534_1_0_0;
struct t6534;
extern Il2CppGenericClass t6534_GC;
TypeInfo t6534_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6534_MIs, t6534_PIs, NULL, NULL, NULL, NULL, NULL, &t6534_TI, t6534_ITIs, NULL, &EmptyCustomAttributesCache, &t6534_TI, &t6534_0_0_0, &t6534_1_0_0, NULL, &t6534_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Serialization.ObjectRecordStatus>
extern Il2CppType t4991_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33805_GM;
MethodInfo m33805_MI = 
{
	"GetEnumerator", NULL, &t6536_TI, &t4991_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33805_GM};
static MethodInfo* t6536_MIs[] =
{
	&m33805_MI,
	NULL
};
static TypeInfo* t6536_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6536_0_0_0;
extern Il2CppType t6536_1_0_0;
struct t6536;
extern Il2CppGenericClass t6536_GC;
TypeInfo t6536_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6536_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6536_TI, t6536_ITIs, NULL, &EmptyCustomAttributesCache, &t6536_TI, &t6536_0_0_0, &t6536_1_0_0, NULL, &t6536_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6535_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Serialization.ObjectRecordStatus>
extern MethodInfo m33806_MI;
extern MethodInfo m33807_MI;
static PropertyInfo t6535____Item_PropertyInfo = 
{
	&t6535_TI, "Item", &m33806_MI, &m33807_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6535_PIs[] =
{
	&t6535____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1513_0_0_0;
static ParameterInfo t6535_m33808_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1513_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33808_GM;
MethodInfo m33808_MI = 
{
	"IndexOf", NULL, &t6535_TI, &t44_0_0_0, RuntimeInvoker_t44_t348, t6535_m33808_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33808_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1513_0_0_0;
static ParameterInfo t6535_m33809_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1513_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33809_GM;
MethodInfo m33809_MI = 
{
	"Insert", NULL, &t6535_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t348, t6535_m33809_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33809_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6535_m33810_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33810_GM;
MethodInfo m33810_MI = 
{
	"RemoveAt", NULL, &t6535_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6535_m33810_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33810_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6535_m33806_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1513_0_0_0;
extern void* RuntimeInvoker_t1513_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33806_GM;
MethodInfo m33806_MI = 
{
	"get_Item", NULL, &t6535_TI, &t1513_0_0_0, RuntimeInvoker_t1513_t44, t6535_m33806_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33806_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1513_0_0_0;
static ParameterInfo t6535_m33807_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1513_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33807_GM;
MethodInfo m33807_MI = 
{
	"set_Item", NULL, &t6535_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t348, t6535_m33807_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33807_GM};
static MethodInfo* t6535_MIs[] =
{
	&m33808_MI,
	&m33809_MI,
	&m33810_MI,
	&m33806_MI,
	&m33807_MI,
	NULL
};
static TypeInfo* t6535_ITIs[] = 
{
	&t603_TI,
	&t6534_TI,
	&t6536_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6535_0_0_0;
extern Il2CppType t6535_1_0_0;
struct t6535;
extern Il2CppGenericClass t6535_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6535_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6535_MIs, t6535_PIs, NULL, NULL, NULL, NULL, NULL, &t6535_TI, t6535_ITIs, NULL, &t1908__CustomAttributeCache, &t6535_TI, &t6535_0_0_0, &t6535_1_0_0, NULL, &t6535_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4993_TI;

#include "t1514.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Serialization.OnDeserializedAttribute>
extern MethodInfo m33811_MI;
static PropertyInfo t4993____Current_PropertyInfo = 
{
	&t4993_TI, "Current", &m33811_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4993_PIs[] =
{
	&t4993____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1514_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33811_GM;
MethodInfo m33811_MI = 
{
	"get_Current", NULL, &t4993_TI, &t1514_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33811_GM};
static MethodInfo* t4993_MIs[] =
{
	&m33811_MI,
	NULL
};
static TypeInfo* t4993_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4993_0_0_0;
extern Il2CppType t4993_1_0_0;
struct t4993;
extern Il2CppGenericClass t4993_GC;
TypeInfo t4993_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4993_MIs, t4993_PIs, NULL, NULL, NULL, NULL, NULL, &t4993_TI, t4993_ITIs, NULL, &EmptyCustomAttributesCache, &t4993_TI, &t4993_0_0_0, &t4993_1_0_0, NULL, &t4993_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3450.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3450_TI;
#include "t3450MD.h"

extern TypeInfo t1514_TI;
extern MethodInfo m19124_MI;
extern MethodInfo m25897_MI;
struct t20;
#define m25897(__this, p0, method) (t1514 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Serialization.OnDeserializedAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3450_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3450_TI, offsetof(t3450, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3450_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3450_TI, offsetof(t3450, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3450_FIs[] =
{
	&t3450_f0_FieldInfo,
	&t3450_f1_FieldInfo,
	NULL
};
extern MethodInfo m19121_MI;
static PropertyInfo t3450____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3450_TI, "System.Collections.IEnumerator.Current", &m19121_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3450____Current_PropertyInfo = 
{
	&t3450_TI, "Current", &m19124_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3450_PIs[] =
{
	&t3450____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3450____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3450_m19120_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19120_GM;
MethodInfo m19120_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3450_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3450_m19120_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19120_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19121_GM;
MethodInfo m19121_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3450_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19121_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19122_GM;
MethodInfo m19122_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3450_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19122_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19123_GM;
MethodInfo m19123_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3450_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19123_GM};
extern Il2CppType t1514_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19124_GM;
MethodInfo m19124_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3450_TI, &t1514_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19124_GM};
static MethodInfo* t3450_MIs[] =
{
	&m19120_MI,
	&m19121_MI,
	&m19122_MI,
	&m19123_MI,
	&m19124_MI,
	NULL
};
extern MethodInfo m19123_MI;
extern MethodInfo m19122_MI;
static MethodInfo* t3450_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19121_MI,
	&m19123_MI,
	&m19122_MI,
	&m19124_MI,
};
static TypeInfo* t3450_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4993_TI,
};
static Il2CppInterfaceOffsetPair t3450_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4993_TI, 7},
};
extern TypeInfo t1514_TI;
static Il2CppRGCTXData t3450_RGCTXData[3] = 
{
	&m19124_MI/* Method Usage */,
	&t1514_TI/* Class Usage */,
	&m25897_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3450_0_0_0;
extern Il2CppType t3450_1_0_0;
extern Il2CppGenericClass t3450_GC;
TypeInfo t3450_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3450_MIs, t3450_PIs, t3450_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3450_TI, t3450_ITIs, t3450_VT, &EmptyCustomAttributesCache, &t3450_TI, &t3450_0_0_0, &t3450_1_0_0, t3450_IOs, &t3450_GC, NULL, NULL, NULL, t3450_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3450)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6537_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Serialization.OnDeserializedAttribute>
extern MethodInfo m33812_MI;
static PropertyInfo t6537____Count_PropertyInfo = 
{
	&t6537_TI, "Count", &m33812_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33813_MI;
static PropertyInfo t6537____IsReadOnly_PropertyInfo = 
{
	&t6537_TI, "IsReadOnly", &m33813_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6537_PIs[] =
{
	&t6537____Count_PropertyInfo,
	&t6537____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33812_GM;
MethodInfo m33812_MI = 
{
	"get_Count", NULL, &t6537_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33812_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33813_GM;
MethodInfo m33813_MI = 
{
	"get_IsReadOnly", NULL, &t6537_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33813_GM};
extern Il2CppType t1514_0_0_0;
extern Il2CppType t1514_0_0_0;
static ParameterInfo t6537_m33814_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1514_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33814_GM;
MethodInfo m33814_MI = 
{
	"Add", NULL, &t6537_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6537_m33814_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33814_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33815_GM;
MethodInfo m33815_MI = 
{
	"Clear", NULL, &t6537_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33815_GM};
extern Il2CppType t1514_0_0_0;
static ParameterInfo t6537_m33816_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1514_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33816_GM;
MethodInfo m33816_MI = 
{
	"Contains", NULL, &t6537_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6537_m33816_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33816_GM};
extern Il2CppType t3698_0_0_0;
extern Il2CppType t3698_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6537_m33817_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3698_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33817_GM;
MethodInfo m33817_MI = 
{
	"CopyTo", NULL, &t6537_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6537_m33817_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33817_GM};
extern Il2CppType t1514_0_0_0;
static ParameterInfo t6537_m33818_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1514_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33818_GM;
MethodInfo m33818_MI = 
{
	"Remove", NULL, &t6537_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6537_m33818_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33818_GM};
static MethodInfo* t6537_MIs[] =
{
	&m33812_MI,
	&m33813_MI,
	&m33814_MI,
	&m33815_MI,
	&m33816_MI,
	&m33817_MI,
	&m33818_MI,
	NULL
};
extern TypeInfo t6539_TI;
static TypeInfo* t6537_ITIs[] = 
{
	&t603_TI,
	&t6539_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6537_0_0_0;
extern Il2CppType t6537_1_0_0;
struct t6537;
extern Il2CppGenericClass t6537_GC;
TypeInfo t6537_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6537_MIs, t6537_PIs, NULL, NULL, NULL, NULL, NULL, &t6537_TI, t6537_ITIs, NULL, &EmptyCustomAttributesCache, &t6537_TI, &t6537_0_0_0, &t6537_1_0_0, NULL, &t6537_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Serialization.OnDeserializedAttribute>
extern Il2CppType t4993_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33819_GM;
MethodInfo m33819_MI = 
{
	"GetEnumerator", NULL, &t6539_TI, &t4993_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33819_GM};
static MethodInfo* t6539_MIs[] =
{
	&m33819_MI,
	NULL
};
static TypeInfo* t6539_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6539_0_0_0;
extern Il2CppType t6539_1_0_0;
struct t6539;
extern Il2CppGenericClass t6539_GC;
TypeInfo t6539_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6539_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6539_TI, t6539_ITIs, NULL, &EmptyCustomAttributesCache, &t6539_TI, &t6539_0_0_0, &t6539_1_0_0, NULL, &t6539_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6538_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Serialization.OnDeserializedAttribute>
extern MethodInfo m33820_MI;
extern MethodInfo m33821_MI;
static PropertyInfo t6538____Item_PropertyInfo = 
{
	&t6538_TI, "Item", &m33820_MI, &m33821_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6538_PIs[] =
{
	&t6538____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1514_0_0_0;
static ParameterInfo t6538_m33822_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1514_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33822_GM;
MethodInfo m33822_MI = 
{
	"IndexOf", NULL, &t6538_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6538_m33822_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33822_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1514_0_0_0;
static ParameterInfo t6538_m33823_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1514_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33823_GM;
MethodInfo m33823_MI = 
{
	"Insert", NULL, &t6538_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6538_m33823_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33823_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6538_m33824_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33824_GM;
MethodInfo m33824_MI = 
{
	"RemoveAt", NULL, &t6538_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6538_m33824_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33824_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6538_m33820_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1514_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33820_GM;
MethodInfo m33820_MI = 
{
	"get_Item", NULL, &t6538_TI, &t1514_0_0_0, RuntimeInvoker_t29_t44, t6538_m33820_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33820_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1514_0_0_0;
static ParameterInfo t6538_m33821_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1514_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33821_GM;
MethodInfo m33821_MI = 
{
	"set_Item", NULL, &t6538_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6538_m33821_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33821_GM};
static MethodInfo* t6538_MIs[] =
{
	&m33822_MI,
	&m33823_MI,
	&m33824_MI,
	&m33820_MI,
	&m33821_MI,
	NULL
};
static TypeInfo* t6538_ITIs[] = 
{
	&t603_TI,
	&t6537_TI,
	&t6539_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6538_0_0_0;
extern Il2CppType t6538_1_0_0;
struct t6538;
extern Il2CppGenericClass t6538_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6538_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6538_MIs, t6538_PIs, NULL, NULL, NULL, NULL, NULL, &t6538_TI, t6538_ITIs, NULL, &t1908__CustomAttributeCache, &t6538_TI, &t6538_0_0_0, &t6538_1_0_0, NULL, &t6538_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4995_TI;

#include "t1515.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Serialization.OnDeserializingAttribute>
extern MethodInfo m33825_MI;
static PropertyInfo t4995____Current_PropertyInfo = 
{
	&t4995_TI, "Current", &m33825_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4995_PIs[] =
{
	&t4995____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1515_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33825_GM;
MethodInfo m33825_MI = 
{
	"get_Current", NULL, &t4995_TI, &t1515_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33825_GM};
static MethodInfo* t4995_MIs[] =
{
	&m33825_MI,
	NULL
};
static TypeInfo* t4995_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4995_0_0_0;
extern Il2CppType t4995_1_0_0;
struct t4995;
extern Il2CppGenericClass t4995_GC;
TypeInfo t4995_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4995_MIs, t4995_PIs, NULL, NULL, NULL, NULL, NULL, &t4995_TI, t4995_ITIs, NULL, &EmptyCustomAttributesCache, &t4995_TI, &t4995_0_0_0, &t4995_1_0_0, NULL, &t4995_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3451.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3451_TI;
#include "t3451MD.h"

extern TypeInfo t1515_TI;
extern MethodInfo m19129_MI;
extern MethodInfo m25908_MI;
struct t20;
#define m25908(__this, p0, method) (t1515 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Serialization.OnDeserializingAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3451_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3451_TI, offsetof(t3451, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3451_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3451_TI, offsetof(t3451, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3451_FIs[] =
{
	&t3451_f0_FieldInfo,
	&t3451_f1_FieldInfo,
	NULL
};
extern MethodInfo m19126_MI;
static PropertyInfo t3451____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3451_TI, "System.Collections.IEnumerator.Current", &m19126_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3451____Current_PropertyInfo = 
{
	&t3451_TI, "Current", &m19129_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3451_PIs[] =
{
	&t3451____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3451____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3451_m19125_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19125_GM;
MethodInfo m19125_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3451_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3451_m19125_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19125_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19126_GM;
MethodInfo m19126_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3451_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19126_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19127_GM;
MethodInfo m19127_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3451_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19127_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19128_GM;
MethodInfo m19128_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3451_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19128_GM};
extern Il2CppType t1515_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19129_GM;
MethodInfo m19129_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3451_TI, &t1515_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19129_GM};
static MethodInfo* t3451_MIs[] =
{
	&m19125_MI,
	&m19126_MI,
	&m19127_MI,
	&m19128_MI,
	&m19129_MI,
	NULL
};
extern MethodInfo m19128_MI;
extern MethodInfo m19127_MI;
static MethodInfo* t3451_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19126_MI,
	&m19128_MI,
	&m19127_MI,
	&m19129_MI,
};
static TypeInfo* t3451_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4995_TI,
};
static Il2CppInterfaceOffsetPair t3451_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4995_TI, 7},
};
extern TypeInfo t1515_TI;
static Il2CppRGCTXData t3451_RGCTXData[3] = 
{
	&m19129_MI/* Method Usage */,
	&t1515_TI/* Class Usage */,
	&m25908_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3451_0_0_0;
extern Il2CppType t3451_1_0_0;
extern Il2CppGenericClass t3451_GC;
TypeInfo t3451_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3451_MIs, t3451_PIs, t3451_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3451_TI, t3451_ITIs, t3451_VT, &EmptyCustomAttributesCache, &t3451_TI, &t3451_0_0_0, &t3451_1_0_0, t3451_IOs, &t3451_GC, NULL, NULL, NULL, t3451_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3451)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6540_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Serialization.OnDeserializingAttribute>
extern MethodInfo m33826_MI;
static PropertyInfo t6540____Count_PropertyInfo = 
{
	&t6540_TI, "Count", &m33826_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33827_MI;
static PropertyInfo t6540____IsReadOnly_PropertyInfo = 
{
	&t6540_TI, "IsReadOnly", &m33827_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6540_PIs[] =
{
	&t6540____Count_PropertyInfo,
	&t6540____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33826_GM;
MethodInfo m33826_MI = 
{
	"get_Count", NULL, &t6540_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33826_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33827_GM;
MethodInfo m33827_MI = 
{
	"get_IsReadOnly", NULL, &t6540_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33827_GM};
extern Il2CppType t1515_0_0_0;
extern Il2CppType t1515_0_0_0;
static ParameterInfo t6540_m33828_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1515_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33828_GM;
MethodInfo m33828_MI = 
{
	"Add", NULL, &t6540_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6540_m33828_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33828_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33829_GM;
MethodInfo m33829_MI = 
{
	"Clear", NULL, &t6540_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33829_GM};
extern Il2CppType t1515_0_0_0;
static ParameterInfo t6540_m33830_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1515_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33830_GM;
MethodInfo m33830_MI = 
{
	"Contains", NULL, &t6540_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6540_m33830_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33830_GM};
extern Il2CppType t3699_0_0_0;
extern Il2CppType t3699_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6540_m33831_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3699_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33831_GM;
MethodInfo m33831_MI = 
{
	"CopyTo", NULL, &t6540_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6540_m33831_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33831_GM};
extern Il2CppType t1515_0_0_0;
static ParameterInfo t6540_m33832_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1515_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33832_GM;
MethodInfo m33832_MI = 
{
	"Remove", NULL, &t6540_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6540_m33832_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33832_GM};
static MethodInfo* t6540_MIs[] =
{
	&m33826_MI,
	&m33827_MI,
	&m33828_MI,
	&m33829_MI,
	&m33830_MI,
	&m33831_MI,
	&m33832_MI,
	NULL
};
extern TypeInfo t6542_TI;
static TypeInfo* t6540_ITIs[] = 
{
	&t603_TI,
	&t6542_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6540_0_0_0;
extern Il2CppType t6540_1_0_0;
struct t6540;
extern Il2CppGenericClass t6540_GC;
TypeInfo t6540_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6540_MIs, t6540_PIs, NULL, NULL, NULL, NULL, NULL, &t6540_TI, t6540_ITIs, NULL, &EmptyCustomAttributesCache, &t6540_TI, &t6540_0_0_0, &t6540_1_0_0, NULL, &t6540_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Serialization.OnDeserializingAttribute>
extern Il2CppType t4995_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33833_GM;
MethodInfo m33833_MI = 
{
	"GetEnumerator", NULL, &t6542_TI, &t4995_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33833_GM};
static MethodInfo* t6542_MIs[] =
{
	&m33833_MI,
	NULL
};
static TypeInfo* t6542_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6542_0_0_0;
extern Il2CppType t6542_1_0_0;
struct t6542;
extern Il2CppGenericClass t6542_GC;
TypeInfo t6542_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6542_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6542_TI, t6542_ITIs, NULL, &EmptyCustomAttributesCache, &t6542_TI, &t6542_0_0_0, &t6542_1_0_0, NULL, &t6542_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6541_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Serialization.OnDeserializingAttribute>
extern MethodInfo m33834_MI;
extern MethodInfo m33835_MI;
static PropertyInfo t6541____Item_PropertyInfo = 
{
	&t6541_TI, "Item", &m33834_MI, &m33835_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6541_PIs[] =
{
	&t6541____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1515_0_0_0;
static ParameterInfo t6541_m33836_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1515_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33836_GM;
MethodInfo m33836_MI = 
{
	"IndexOf", NULL, &t6541_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6541_m33836_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33836_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1515_0_0_0;
static ParameterInfo t6541_m33837_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1515_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33837_GM;
MethodInfo m33837_MI = 
{
	"Insert", NULL, &t6541_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6541_m33837_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33837_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6541_m33838_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33838_GM;
MethodInfo m33838_MI = 
{
	"RemoveAt", NULL, &t6541_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6541_m33838_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33838_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6541_m33834_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1515_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33834_GM;
MethodInfo m33834_MI = 
{
	"get_Item", NULL, &t6541_TI, &t1515_0_0_0, RuntimeInvoker_t29_t44, t6541_m33834_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33834_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1515_0_0_0;
static ParameterInfo t6541_m33835_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1515_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33835_GM;
MethodInfo m33835_MI = 
{
	"set_Item", NULL, &t6541_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6541_m33835_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33835_GM};
static MethodInfo* t6541_MIs[] =
{
	&m33836_MI,
	&m33837_MI,
	&m33838_MI,
	&m33834_MI,
	&m33835_MI,
	NULL
};
static TypeInfo* t6541_ITIs[] = 
{
	&t603_TI,
	&t6540_TI,
	&t6542_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6541_0_0_0;
extern Il2CppType t6541_1_0_0;
struct t6541;
extern Il2CppGenericClass t6541_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6541_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6541_MIs, t6541_PIs, NULL, NULL, NULL, NULL, NULL, &t6541_TI, t6541_ITIs, NULL, &t1908__CustomAttributeCache, &t6541_TI, &t6541_0_0_0, &t6541_1_0_0, NULL, &t6541_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4997_TI;

#include "t1516.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Serialization.OnSerializedAttribute>
extern MethodInfo m33839_MI;
static PropertyInfo t4997____Current_PropertyInfo = 
{
	&t4997_TI, "Current", &m33839_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4997_PIs[] =
{
	&t4997____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1516_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33839_GM;
MethodInfo m33839_MI = 
{
	"get_Current", NULL, &t4997_TI, &t1516_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33839_GM};
static MethodInfo* t4997_MIs[] =
{
	&m33839_MI,
	NULL
};
static TypeInfo* t4997_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4997_0_0_0;
extern Il2CppType t4997_1_0_0;
struct t4997;
extern Il2CppGenericClass t4997_GC;
TypeInfo t4997_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4997_MIs, t4997_PIs, NULL, NULL, NULL, NULL, NULL, &t4997_TI, t4997_ITIs, NULL, &EmptyCustomAttributesCache, &t4997_TI, &t4997_0_0_0, &t4997_1_0_0, NULL, &t4997_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3452.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3452_TI;
#include "t3452MD.h"

extern TypeInfo t1516_TI;
extern MethodInfo m19134_MI;
extern MethodInfo m25919_MI;
struct t20;
#define m25919(__this, p0, method) (t1516 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Serialization.OnSerializedAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3452_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3452_TI, offsetof(t3452, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3452_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3452_TI, offsetof(t3452, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3452_FIs[] =
{
	&t3452_f0_FieldInfo,
	&t3452_f1_FieldInfo,
	NULL
};
extern MethodInfo m19131_MI;
static PropertyInfo t3452____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3452_TI, "System.Collections.IEnumerator.Current", &m19131_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3452____Current_PropertyInfo = 
{
	&t3452_TI, "Current", &m19134_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3452_PIs[] =
{
	&t3452____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3452____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3452_m19130_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19130_GM;
MethodInfo m19130_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3452_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3452_m19130_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19130_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19131_GM;
MethodInfo m19131_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3452_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19131_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19132_GM;
MethodInfo m19132_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3452_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19132_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19133_GM;
MethodInfo m19133_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3452_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19133_GM};
extern Il2CppType t1516_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19134_GM;
MethodInfo m19134_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3452_TI, &t1516_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19134_GM};
static MethodInfo* t3452_MIs[] =
{
	&m19130_MI,
	&m19131_MI,
	&m19132_MI,
	&m19133_MI,
	&m19134_MI,
	NULL
};
extern MethodInfo m19133_MI;
extern MethodInfo m19132_MI;
static MethodInfo* t3452_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19131_MI,
	&m19133_MI,
	&m19132_MI,
	&m19134_MI,
};
static TypeInfo* t3452_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4997_TI,
};
static Il2CppInterfaceOffsetPair t3452_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4997_TI, 7},
};
extern TypeInfo t1516_TI;
static Il2CppRGCTXData t3452_RGCTXData[3] = 
{
	&m19134_MI/* Method Usage */,
	&t1516_TI/* Class Usage */,
	&m25919_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3452_0_0_0;
extern Il2CppType t3452_1_0_0;
extern Il2CppGenericClass t3452_GC;
TypeInfo t3452_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3452_MIs, t3452_PIs, t3452_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3452_TI, t3452_ITIs, t3452_VT, &EmptyCustomAttributesCache, &t3452_TI, &t3452_0_0_0, &t3452_1_0_0, t3452_IOs, &t3452_GC, NULL, NULL, NULL, t3452_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3452)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6543_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Serialization.OnSerializedAttribute>
extern MethodInfo m33840_MI;
static PropertyInfo t6543____Count_PropertyInfo = 
{
	&t6543_TI, "Count", &m33840_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33841_MI;
static PropertyInfo t6543____IsReadOnly_PropertyInfo = 
{
	&t6543_TI, "IsReadOnly", &m33841_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6543_PIs[] =
{
	&t6543____Count_PropertyInfo,
	&t6543____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33840_GM;
MethodInfo m33840_MI = 
{
	"get_Count", NULL, &t6543_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33840_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33841_GM;
MethodInfo m33841_MI = 
{
	"get_IsReadOnly", NULL, &t6543_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33841_GM};
extern Il2CppType t1516_0_0_0;
extern Il2CppType t1516_0_0_0;
static ParameterInfo t6543_m33842_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1516_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33842_GM;
MethodInfo m33842_MI = 
{
	"Add", NULL, &t6543_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6543_m33842_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33842_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33843_GM;
MethodInfo m33843_MI = 
{
	"Clear", NULL, &t6543_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33843_GM};
extern Il2CppType t1516_0_0_0;
static ParameterInfo t6543_m33844_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1516_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33844_GM;
MethodInfo m33844_MI = 
{
	"Contains", NULL, &t6543_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6543_m33844_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33844_GM};
extern Il2CppType t3700_0_0_0;
extern Il2CppType t3700_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6543_m33845_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3700_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33845_GM;
MethodInfo m33845_MI = 
{
	"CopyTo", NULL, &t6543_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6543_m33845_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33845_GM};
extern Il2CppType t1516_0_0_0;
static ParameterInfo t6543_m33846_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1516_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33846_GM;
MethodInfo m33846_MI = 
{
	"Remove", NULL, &t6543_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6543_m33846_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33846_GM};
static MethodInfo* t6543_MIs[] =
{
	&m33840_MI,
	&m33841_MI,
	&m33842_MI,
	&m33843_MI,
	&m33844_MI,
	&m33845_MI,
	&m33846_MI,
	NULL
};
extern TypeInfo t6545_TI;
static TypeInfo* t6543_ITIs[] = 
{
	&t603_TI,
	&t6545_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6543_0_0_0;
extern Il2CppType t6543_1_0_0;
struct t6543;
extern Il2CppGenericClass t6543_GC;
TypeInfo t6543_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6543_MIs, t6543_PIs, NULL, NULL, NULL, NULL, NULL, &t6543_TI, t6543_ITIs, NULL, &EmptyCustomAttributesCache, &t6543_TI, &t6543_0_0_0, &t6543_1_0_0, NULL, &t6543_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Serialization.OnSerializedAttribute>
extern Il2CppType t4997_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33847_GM;
MethodInfo m33847_MI = 
{
	"GetEnumerator", NULL, &t6545_TI, &t4997_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33847_GM};
static MethodInfo* t6545_MIs[] =
{
	&m33847_MI,
	NULL
};
static TypeInfo* t6545_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6545_0_0_0;
extern Il2CppType t6545_1_0_0;
struct t6545;
extern Il2CppGenericClass t6545_GC;
TypeInfo t6545_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6545_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6545_TI, t6545_ITIs, NULL, &EmptyCustomAttributesCache, &t6545_TI, &t6545_0_0_0, &t6545_1_0_0, NULL, &t6545_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6544_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Serialization.OnSerializedAttribute>
extern MethodInfo m33848_MI;
extern MethodInfo m33849_MI;
static PropertyInfo t6544____Item_PropertyInfo = 
{
	&t6544_TI, "Item", &m33848_MI, &m33849_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6544_PIs[] =
{
	&t6544____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1516_0_0_0;
static ParameterInfo t6544_m33850_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1516_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33850_GM;
MethodInfo m33850_MI = 
{
	"IndexOf", NULL, &t6544_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6544_m33850_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33850_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1516_0_0_0;
static ParameterInfo t6544_m33851_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1516_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33851_GM;
MethodInfo m33851_MI = 
{
	"Insert", NULL, &t6544_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6544_m33851_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33851_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6544_m33852_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33852_GM;
MethodInfo m33852_MI = 
{
	"RemoveAt", NULL, &t6544_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6544_m33852_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33852_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6544_m33848_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1516_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33848_GM;
MethodInfo m33848_MI = 
{
	"get_Item", NULL, &t6544_TI, &t1516_0_0_0, RuntimeInvoker_t29_t44, t6544_m33848_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33848_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1516_0_0_0;
static ParameterInfo t6544_m33849_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1516_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33849_GM;
MethodInfo m33849_MI = 
{
	"set_Item", NULL, &t6544_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6544_m33849_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33849_GM};
static MethodInfo* t6544_MIs[] =
{
	&m33850_MI,
	&m33851_MI,
	&m33852_MI,
	&m33848_MI,
	&m33849_MI,
	NULL
};
static TypeInfo* t6544_ITIs[] = 
{
	&t603_TI,
	&t6543_TI,
	&t6545_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6544_0_0_0;
extern Il2CppType t6544_1_0_0;
struct t6544;
extern Il2CppGenericClass t6544_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6544_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6544_MIs, t6544_PIs, NULL, NULL, NULL, NULL, NULL, &t6544_TI, t6544_ITIs, NULL, &t1908__CustomAttributeCache, &t6544_TI, &t6544_0_0_0, &t6544_1_0_0, NULL, &t6544_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4999_TI;

#include "t1517.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Serialization.OnSerializingAttribute>
extern MethodInfo m33853_MI;
static PropertyInfo t4999____Current_PropertyInfo = 
{
	&t4999_TI, "Current", &m33853_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4999_PIs[] =
{
	&t4999____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1517_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33853_GM;
MethodInfo m33853_MI = 
{
	"get_Current", NULL, &t4999_TI, &t1517_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33853_GM};
static MethodInfo* t4999_MIs[] =
{
	&m33853_MI,
	NULL
};
static TypeInfo* t4999_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4999_0_0_0;
extern Il2CppType t4999_1_0_0;
struct t4999;
extern Il2CppGenericClass t4999_GC;
TypeInfo t4999_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4999_MIs, t4999_PIs, NULL, NULL, NULL, NULL, NULL, &t4999_TI, t4999_ITIs, NULL, &EmptyCustomAttributesCache, &t4999_TI, &t4999_0_0_0, &t4999_1_0_0, NULL, &t4999_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3453.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3453_TI;
#include "t3453MD.h"

extern TypeInfo t1517_TI;
extern MethodInfo m19139_MI;
extern MethodInfo m25930_MI;
struct t20;
#define m25930(__this, p0, method) (t1517 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Serialization.OnSerializingAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3453_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3453_TI, offsetof(t3453, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3453_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3453_TI, offsetof(t3453, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3453_FIs[] =
{
	&t3453_f0_FieldInfo,
	&t3453_f1_FieldInfo,
	NULL
};
extern MethodInfo m19136_MI;
static PropertyInfo t3453____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3453_TI, "System.Collections.IEnumerator.Current", &m19136_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3453____Current_PropertyInfo = 
{
	&t3453_TI, "Current", &m19139_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3453_PIs[] =
{
	&t3453____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3453____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3453_m19135_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19135_GM;
MethodInfo m19135_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3453_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3453_m19135_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19135_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19136_GM;
MethodInfo m19136_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3453_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19136_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19137_GM;
MethodInfo m19137_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3453_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19137_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19138_GM;
MethodInfo m19138_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3453_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19138_GM};
extern Il2CppType t1517_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19139_GM;
MethodInfo m19139_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3453_TI, &t1517_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19139_GM};
static MethodInfo* t3453_MIs[] =
{
	&m19135_MI,
	&m19136_MI,
	&m19137_MI,
	&m19138_MI,
	&m19139_MI,
	NULL
};
extern MethodInfo m19138_MI;
extern MethodInfo m19137_MI;
static MethodInfo* t3453_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19136_MI,
	&m19138_MI,
	&m19137_MI,
	&m19139_MI,
};
static TypeInfo* t3453_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4999_TI,
};
static Il2CppInterfaceOffsetPair t3453_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4999_TI, 7},
};
extern TypeInfo t1517_TI;
static Il2CppRGCTXData t3453_RGCTXData[3] = 
{
	&m19139_MI/* Method Usage */,
	&t1517_TI/* Class Usage */,
	&m25930_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3453_0_0_0;
extern Il2CppType t3453_1_0_0;
extern Il2CppGenericClass t3453_GC;
TypeInfo t3453_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3453_MIs, t3453_PIs, t3453_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3453_TI, t3453_ITIs, t3453_VT, &EmptyCustomAttributesCache, &t3453_TI, &t3453_0_0_0, &t3453_1_0_0, t3453_IOs, &t3453_GC, NULL, NULL, NULL, t3453_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3453)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6546_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Serialization.OnSerializingAttribute>
extern MethodInfo m33854_MI;
static PropertyInfo t6546____Count_PropertyInfo = 
{
	&t6546_TI, "Count", &m33854_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33855_MI;
static PropertyInfo t6546____IsReadOnly_PropertyInfo = 
{
	&t6546_TI, "IsReadOnly", &m33855_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6546_PIs[] =
{
	&t6546____Count_PropertyInfo,
	&t6546____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33854_GM;
MethodInfo m33854_MI = 
{
	"get_Count", NULL, &t6546_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33854_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33855_GM;
MethodInfo m33855_MI = 
{
	"get_IsReadOnly", NULL, &t6546_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33855_GM};
extern Il2CppType t1517_0_0_0;
extern Il2CppType t1517_0_0_0;
static ParameterInfo t6546_m33856_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1517_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33856_GM;
MethodInfo m33856_MI = 
{
	"Add", NULL, &t6546_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6546_m33856_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33856_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33857_GM;
MethodInfo m33857_MI = 
{
	"Clear", NULL, &t6546_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33857_GM};
extern Il2CppType t1517_0_0_0;
static ParameterInfo t6546_m33858_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1517_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33858_GM;
MethodInfo m33858_MI = 
{
	"Contains", NULL, &t6546_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6546_m33858_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33858_GM};
extern Il2CppType t3701_0_0_0;
extern Il2CppType t3701_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6546_m33859_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3701_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33859_GM;
MethodInfo m33859_MI = 
{
	"CopyTo", NULL, &t6546_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6546_m33859_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33859_GM};
extern Il2CppType t1517_0_0_0;
static ParameterInfo t6546_m33860_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1517_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33860_GM;
MethodInfo m33860_MI = 
{
	"Remove", NULL, &t6546_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6546_m33860_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33860_GM};
static MethodInfo* t6546_MIs[] =
{
	&m33854_MI,
	&m33855_MI,
	&m33856_MI,
	&m33857_MI,
	&m33858_MI,
	&m33859_MI,
	&m33860_MI,
	NULL
};
extern TypeInfo t6548_TI;
static TypeInfo* t6546_ITIs[] = 
{
	&t603_TI,
	&t6548_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6546_0_0_0;
extern Il2CppType t6546_1_0_0;
struct t6546;
extern Il2CppGenericClass t6546_GC;
TypeInfo t6546_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6546_MIs, t6546_PIs, NULL, NULL, NULL, NULL, NULL, &t6546_TI, t6546_ITIs, NULL, &EmptyCustomAttributesCache, &t6546_TI, &t6546_0_0_0, &t6546_1_0_0, NULL, &t6546_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Serialization.OnSerializingAttribute>
extern Il2CppType t4999_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33861_GM;
MethodInfo m33861_MI = 
{
	"GetEnumerator", NULL, &t6548_TI, &t4999_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33861_GM};
static MethodInfo* t6548_MIs[] =
{
	&m33861_MI,
	NULL
};
static TypeInfo* t6548_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6548_0_0_0;
extern Il2CppType t6548_1_0_0;
struct t6548;
extern Il2CppGenericClass t6548_GC;
TypeInfo t6548_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6548_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6548_TI, t6548_ITIs, NULL, &EmptyCustomAttributesCache, &t6548_TI, &t6548_0_0_0, &t6548_1_0_0, NULL, &t6548_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6547_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Serialization.OnSerializingAttribute>
extern MethodInfo m33862_MI;
extern MethodInfo m33863_MI;
static PropertyInfo t6547____Item_PropertyInfo = 
{
	&t6547_TI, "Item", &m33862_MI, &m33863_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6547_PIs[] =
{
	&t6547____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1517_0_0_0;
static ParameterInfo t6547_m33864_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1517_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33864_GM;
MethodInfo m33864_MI = 
{
	"IndexOf", NULL, &t6547_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6547_m33864_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33864_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1517_0_0_0;
static ParameterInfo t6547_m33865_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1517_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33865_GM;
MethodInfo m33865_MI = 
{
	"Insert", NULL, &t6547_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6547_m33865_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33865_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6547_m33866_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33866_GM;
MethodInfo m33866_MI = 
{
	"RemoveAt", NULL, &t6547_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6547_m33866_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33866_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6547_m33862_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1517_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33862_GM;
MethodInfo m33862_MI = 
{
	"get_Item", NULL, &t6547_TI, &t1517_0_0_0, RuntimeInvoker_t29_t44, t6547_m33862_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33862_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1517_0_0_0;
static ParameterInfo t6547_m33863_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1517_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33863_GM;
MethodInfo m33863_MI = 
{
	"set_Item", NULL, &t6547_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6547_m33863_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33863_GM};
static MethodInfo* t6547_MIs[] =
{
	&m33864_MI,
	&m33865_MI,
	&m33866_MI,
	&m33862_MI,
	&m33863_MI,
	NULL
};
static TypeInfo* t6547_ITIs[] = 
{
	&t603_TI,
	&t6546_TI,
	&t6548_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6547_0_0_0;
extern Il2CppType t6547_1_0_0;
struct t6547;
extern Il2CppGenericClass t6547_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6547_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6547_MIs, t6547_PIs, NULL, NULL, NULL, NULL, NULL, &t6547_TI, t6547_ITIs, NULL, &t1908__CustomAttributeCache, &t6547_TI, &t6547_0_0_0, &t6547_1_0_0, NULL, &t6547_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5001_TI;

#include "t1523.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Serialization.StreamingContextStates>
extern MethodInfo m33867_MI;
static PropertyInfo t5001____Current_PropertyInfo = 
{
	&t5001_TI, "Current", &m33867_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5001_PIs[] =
{
	&t5001____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1523_0_0_0;
extern void* RuntimeInvoker_t1523 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33867_GM;
MethodInfo m33867_MI = 
{
	"get_Current", NULL, &t5001_TI, &t1523_0_0_0, RuntimeInvoker_t1523, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33867_GM};
static MethodInfo* t5001_MIs[] =
{
	&m33867_MI,
	NULL
};
static TypeInfo* t5001_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5001_0_0_0;
extern Il2CppType t5001_1_0_0;
struct t5001;
extern Il2CppGenericClass t5001_GC;
TypeInfo t5001_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t5001_MIs, t5001_PIs, NULL, NULL, NULL, NULL, NULL, &t5001_TI, t5001_ITIs, NULL, &EmptyCustomAttributesCache, &t5001_TI, &t5001_0_0_0, &t5001_1_0_0, NULL, &t5001_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3454.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3454_TI;
#include "t3454MD.h"

extern TypeInfo t1523_TI;
extern MethodInfo m19144_MI;
extern MethodInfo m25941_MI;
struct t20;
 int32_t m25941 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m19140_MI;
 void m19140 (t3454 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19141_MI;
 t29 * m19141 (t3454 * __this, MethodInfo* method){
	{
		int32_t L_0 = m19144(__this, &m19144_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1523_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m19142_MI;
 void m19142 (t3454 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19143_MI;
 bool m19143 (t3454 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m19144 (t3454 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m25941(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25941_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Serialization.StreamingContextStates>
extern Il2CppType t20_0_0_1;
FieldInfo t3454_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3454_TI, offsetof(t3454, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3454_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3454_TI, offsetof(t3454, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3454_FIs[] =
{
	&t3454_f0_FieldInfo,
	&t3454_f1_FieldInfo,
	NULL
};
static PropertyInfo t3454____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3454_TI, "System.Collections.IEnumerator.Current", &m19141_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3454____Current_PropertyInfo = 
{
	&t3454_TI, "Current", &m19144_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3454_PIs[] =
{
	&t3454____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3454____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3454_m19140_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19140_GM;
MethodInfo m19140_MI = 
{
	".ctor", (methodPointerType)&m19140, &t3454_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3454_m19140_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19140_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19141_GM;
MethodInfo m19141_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m19141, &t3454_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19141_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19142_GM;
MethodInfo m19142_MI = 
{
	"Dispose", (methodPointerType)&m19142, &t3454_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19142_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19143_GM;
MethodInfo m19143_MI = 
{
	"MoveNext", (methodPointerType)&m19143, &t3454_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19143_GM};
extern Il2CppType t1523_0_0_0;
extern void* RuntimeInvoker_t1523 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19144_GM;
MethodInfo m19144_MI = 
{
	"get_Current", (methodPointerType)&m19144, &t3454_TI, &t1523_0_0_0, RuntimeInvoker_t1523, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19144_GM};
static MethodInfo* t3454_MIs[] =
{
	&m19140_MI,
	&m19141_MI,
	&m19142_MI,
	&m19143_MI,
	&m19144_MI,
	NULL
};
static MethodInfo* t3454_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19141_MI,
	&m19143_MI,
	&m19142_MI,
	&m19144_MI,
};
static TypeInfo* t3454_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t5001_TI,
};
static Il2CppInterfaceOffsetPair t3454_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t5001_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3454_0_0_0;
extern Il2CppType t3454_1_0_0;
extern Il2CppGenericClass t3454_GC;
TypeInfo t3454_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3454_MIs, t3454_PIs, t3454_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3454_TI, t3454_ITIs, t3454_VT, &EmptyCustomAttributesCache, &t3454_TI, &t3454_0_0_0, &t3454_1_0_0, t3454_IOs, &t3454_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3454)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6549_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Serialization.StreamingContextStates>
extern MethodInfo m33868_MI;
static PropertyInfo t6549____Count_PropertyInfo = 
{
	&t6549_TI, "Count", &m33868_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33869_MI;
static PropertyInfo t6549____IsReadOnly_PropertyInfo = 
{
	&t6549_TI, "IsReadOnly", &m33869_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6549_PIs[] =
{
	&t6549____Count_PropertyInfo,
	&t6549____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33868_GM;
MethodInfo m33868_MI = 
{
	"get_Count", NULL, &t6549_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33868_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33869_GM;
MethodInfo m33869_MI = 
{
	"get_IsReadOnly", NULL, &t6549_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33869_GM};
extern Il2CppType t1523_0_0_0;
extern Il2CppType t1523_0_0_0;
static ParameterInfo t6549_m33870_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1523_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33870_GM;
MethodInfo m33870_MI = 
{
	"Add", NULL, &t6549_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6549_m33870_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33870_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33871_GM;
MethodInfo m33871_MI = 
{
	"Clear", NULL, &t6549_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33871_GM};
extern Il2CppType t1523_0_0_0;
static ParameterInfo t6549_m33872_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1523_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33872_GM;
MethodInfo m33872_MI = 
{
	"Contains", NULL, &t6549_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6549_m33872_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33872_GM};
extern Il2CppType t3702_0_0_0;
extern Il2CppType t3702_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6549_m33873_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3702_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33873_GM;
MethodInfo m33873_MI = 
{
	"CopyTo", NULL, &t6549_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6549_m33873_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33873_GM};
extern Il2CppType t1523_0_0_0;
static ParameterInfo t6549_m33874_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1523_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33874_GM;
MethodInfo m33874_MI = 
{
	"Remove", NULL, &t6549_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6549_m33874_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33874_GM};
static MethodInfo* t6549_MIs[] =
{
	&m33868_MI,
	&m33869_MI,
	&m33870_MI,
	&m33871_MI,
	&m33872_MI,
	&m33873_MI,
	&m33874_MI,
	NULL
};
extern TypeInfo t6551_TI;
static TypeInfo* t6549_ITIs[] = 
{
	&t603_TI,
	&t6551_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6549_0_0_0;
extern Il2CppType t6549_1_0_0;
struct t6549;
extern Il2CppGenericClass t6549_GC;
TypeInfo t6549_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6549_MIs, t6549_PIs, NULL, NULL, NULL, NULL, NULL, &t6549_TI, t6549_ITIs, NULL, &EmptyCustomAttributesCache, &t6549_TI, &t6549_0_0_0, &t6549_1_0_0, NULL, &t6549_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Serialization.StreamingContextStates>
extern Il2CppType t5001_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33875_GM;
MethodInfo m33875_MI = 
{
	"GetEnumerator", NULL, &t6551_TI, &t5001_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33875_GM};
static MethodInfo* t6551_MIs[] =
{
	&m33875_MI,
	NULL
};
static TypeInfo* t6551_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6551_0_0_0;
extern Il2CppType t6551_1_0_0;
struct t6551;
extern Il2CppGenericClass t6551_GC;
TypeInfo t6551_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6551_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6551_TI, t6551_ITIs, NULL, &EmptyCustomAttributesCache, &t6551_TI, &t6551_0_0_0, &t6551_1_0_0, NULL, &t6551_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6550_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Serialization.StreamingContextStates>
extern MethodInfo m33876_MI;
extern MethodInfo m33877_MI;
static PropertyInfo t6550____Item_PropertyInfo = 
{
	&t6550_TI, "Item", &m33876_MI, &m33877_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6550_PIs[] =
{
	&t6550____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1523_0_0_0;
static ParameterInfo t6550_m33878_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1523_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33878_GM;
MethodInfo m33878_MI = 
{
	"IndexOf", NULL, &t6550_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6550_m33878_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33878_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1523_0_0_0;
static ParameterInfo t6550_m33879_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1523_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33879_GM;
MethodInfo m33879_MI = 
{
	"Insert", NULL, &t6550_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6550_m33879_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33879_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6550_m33880_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33880_GM;
MethodInfo m33880_MI = 
{
	"RemoveAt", NULL, &t6550_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6550_m33880_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33880_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6550_m33876_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1523_0_0_0;
extern void* RuntimeInvoker_t1523_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33876_GM;
MethodInfo m33876_MI = 
{
	"get_Item", NULL, &t6550_TI, &t1523_0_0_0, RuntimeInvoker_t1523_t44, t6550_m33876_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33876_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1523_0_0_0;
static ParameterInfo t6550_m33877_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1523_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33877_GM;
MethodInfo m33877_MI = 
{
	"set_Item", NULL, &t6550_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6550_m33877_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33877_GM};
static MethodInfo* t6550_MIs[] =
{
	&m33878_MI,
	&m33879_MI,
	&m33880_MI,
	&m33876_MI,
	&m33877_MI,
	NULL
};
static TypeInfo* t6550_ITIs[] = 
{
	&t603_TI,
	&t6549_TI,
	&t6551_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6550_0_0_0;
extern Il2CppType t6550_1_0_0;
struct t6550;
extern Il2CppGenericClass t6550_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6550_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6550_MIs, t6550_PIs, NULL, NULL, NULL, NULL, NULL, &t6550_TI, t6550_ITIs, NULL, &t1908__CustomAttributeCache, &t6550_TI, &t6550_0_0_0, &t6550_1_0_0, NULL, &t6550_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5003_TI;

#include "t795.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Security.Cryptography.X509Certificates.X509KeyStorageFlags>
extern MethodInfo m33881_MI;
static PropertyInfo t5003____Current_PropertyInfo = 
{
	&t5003_TI, "Current", &m33881_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5003_PIs[] =
{
	&t5003____Current_PropertyInfo,
	NULL
};
extern Il2CppType t795_0_0_0;
extern void* RuntimeInvoker_t795 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33881_GM;
MethodInfo m33881_MI = 
{
	"get_Current", NULL, &t5003_TI, &t795_0_0_0, RuntimeInvoker_t795, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33881_GM};
static MethodInfo* t5003_MIs[] =
{
	&m33881_MI,
	NULL
};
static TypeInfo* t5003_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5003_0_0_0;
extern Il2CppType t5003_1_0_0;
struct t5003;
extern Il2CppGenericClass t5003_GC;
TypeInfo t5003_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t5003_MIs, t5003_PIs, NULL, NULL, NULL, NULL, NULL, &t5003_TI, t5003_ITIs, NULL, &EmptyCustomAttributesCache, &t5003_TI, &t5003_0_0_0, &t5003_1_0_0, NULL, &t5003_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3455.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3455_TI;
#include "t3455MD.h"

extern TypeInfo t795_TI;
extern MethodInfo m19149_MI;
extern MethodInfo m25952_MI;
struct t20;
 int32_t m25952 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m19145_MI;
 void m19145 (t3455 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19146_MI;
 t29 * m19146 (t3455 * __this, MethodInfo* method){
	{
		int32_t L_0 = m19149(__this, &m19149_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t795_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m19147_MI;
 void m19147 (t3455 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19148_MI;
 bool m19148 (t3455 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m19149 (t3455 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m25952(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25952_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509KeyStorageFlags>
extern Il2CppType t20_0_0_1;
FieldInfo t3455_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3455_TI, offsetof(t3455, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3455_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3455_TI, offsetof(t3455, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3455_FIs[] =
{
	&t3455_f0_FieldInfo,
	&t3455_f1_FieldInfo,
	NULL
};
static PropertyInfo t3455____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3455_TI, "System.Collections.IEnumerator.Current", &m19146_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3455____Current_PropertyInfo = 
{
	&t3455_TI, "Current", &m19149_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3455_PIs[] =
{
	&t3455____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3455____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3455_m19145_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19145_GM;
MethodInfo m19145_MI = 
{
	".ctor", (methodPointerType)&m19145, &t3455_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3455_m19145_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19145_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19146_GM;
MethodInfo m19146_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m19146, &t3455_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19146_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19147_GM;
MethodInfo m19147_MI = 
{
	"Dispose", (methodPointerType)&m19147, &t3455_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19147_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19148_GM;
MethodInfo m19148_MI = 
{
	"MoveNext", (methodPointerType)&m19148, &t3455_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19148_GM};
extern Il2CppType t795_0_0_0;
extern void* RuntimeInvoker_t795 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19149_GM;
MethodInfo m19149_MI = 
{
	"get_Current", (methodPointerType)&m19149, &t3455_TI, &t795_0_0_0, RuntimeInvoker_t795, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19149_GM};
static MethodInfo* t3455_MIs[] =
{
	&m19145_MI,
	&m19146_MI,
	&m19147_MI,
	&m19148_MI,
	&m19149_MI,
	NULL
};
static MethodInfo* t3455_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19146_MI,
	&m19148_MI,
	&m19147_MI,
	&m19149_MI,
};
static TypeInfo* t3455_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t5003_TI,
};
static Il2CppInterfaceOffsetPair t3455_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t5003_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3455_0_0_0;
extern Il2CppType t3455_1_0_0;
extern Il2CppGenericClass t3455_GC;
TypeInfo t3455_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3455_MIs, t3455_PIs, t3455_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3455_TI, t3455_ITIs, t3455_VT, &EmptyCustomAttributesCache, &t3455_TI, &t3455_0_0_0, &t3455_1_0_0, t3455_IOs, &t3455_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3455)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6552_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Security.Cryptography.X509Certificates.X509KeyStorageFlags>
extern MethodInfo m33882_MI;
static PropertyInfo t6552____Count_PropertyInfo = 
{
	&t6552_TI, "Count", &m33882_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33883_MI;
static PropertyInfo t6552____IsReadOnly_PropertyInfo = 
{
	&t6552_TI, "IsReadOnly", &m33883_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6552_PIs[] =
{
	&t6552____Count_PropertyInfo,
	&t6552____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33882_GM;
MethodInfo m33882_MI = 
{
	"get_Count", NULL, &t6552_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33882_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33883_GM;
MethodInfo m33883_MI = 
{
	"get_IsReadOnly", NULL, &t6552_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33883_GM};
extern Il2CppType t795_0_0_0;
extern Il2CppType t795_0_0_0;
static ParameterInfo t6552_m33884_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t795_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33884_GM;
MethodInfo m33884_MI = 
{
	"Add", NULL, &t6552_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6552_m33884_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33884_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33885_GM;
MethodInfo m33885_MI = 
{
	"Clear", NULL, &t6552_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33885_GM};
extern Il2CppType t795_0_0_0;
static ParameterInfo t6552_m33886_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t795_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33886_GM;
MethodInfo m33886_MI = 
{
	"Contains", NULL, &t6552_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6552_m33886_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33886_GM};
extern Il2CppType t3703_0_0_0;
extern Il2CppType t3703_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6552_m33887_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3703_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33887_GM;
MethodInfo m33887_MI = 
{
	"CopyTo", NULL, &t6552_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6552_m33887_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33887_GM};
extern Il2CppType t795_0_0_0;
static ParameterInfo t6552_m33888_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t795_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33888_GM;
MethodInfo m33888_MI = 
{
	"Remove", NULL, &t6552_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6552_m33888_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33888_GM};
static MethodInfo* t6552_MIs[] =
{
	&m33882_MI,
	&m33883_MI,
	&m33884_MI,
	&m33885_MI,
	&m33886_MI,
	&m33887_MI,
	&m33888_MI,
	NULL
};
extern TypeInfo t6554_TI;
static TypeInfo* t6552_ITIs[] = 
{
	&t603_TI,
	&t6554_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6552_0_0_0;
extern Il2CppType t6552_1_0_0;
struct t6552;
extern Il2CppGenericClass t6552_GC;
TypeInfo t6552_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6552_MIs, t6552_PIs, NULL, NULL, NULL, NULL, NULL, &t6552_TI, t6552_ITIs, NULL, &EmptyCustomAttributesCache, &t6552_TI, &t6552_0_0_0, &t6552_1_0_0, NULL, &t6552_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Security.Cryptography.X509Certificates.X509KeyStorageFlags>
extern Il2CppType t5003_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33889_GM;
MethodInfo m33889_MI = 
{
	"GetEnumerator", NULL, &t6554_TI, &t5003_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33889_GM};
static MethodInfo* t6554_MIs[] =
{
	&m33889_MI,
	NULL
};
static TypeInfo* t6554_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6554_0_0_0;
extern Il2CppType t6554_1_0_0;
struct t6554;
extern Il2CppGenericClass t6554_GC;
TypeInfo t6554_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6554_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6554_TI, t6554_ITIs, NULL, &EmptyCustomAttributesCache, &t6554_TI, &t6554_0_0_0, &t6554_1_0_0, NULL, &t6554_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6553_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Security.Cryptography.X509Certificates.X509KeyStorageFlags>
extern MethodInfo m33890_MI;
extern MethodInfo m33891_MI;
static PropertyInfo t6553____Item_PropertyInfo = 
{
	&t6553_TI, "Item", &m33890_MI, &m33891_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6553_PIs[] =
{
	&t6553____Item_PropertyInfo,
	NULL
};
extern Il2CppType t795_0_0_0;
static ParameterInfo t6553_m33892_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t795_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33892_GM;
MethodInfo m33892_MI = 
{
	"IndexOf", NULL, &t6553_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6553_m33892_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33892_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t795_0_0_0;
static ParameterInfo t6553_m33893_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t795_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33893_GM;
MethodInfo m33893_MI = 
{
	"Insert", NULL, &t6553_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6553_m33893_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33893_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6553_m33894_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33894_GM;
MethodInfo m33894_MI = 
{
	"RemoveAt", NULL, &t6553_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6553_m33894_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33894_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6553_m33890_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t795_0_0_0;
extern void* RuntimeInvoker_t795_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33890_GM;
MethodInfo m33890_MI = 
{
	"get_Item", NULL, &t6553_TI, &t795_0_0_0, RuntimeInvoker_t795_t44, t6553_m33890_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33890_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t795_0_0_0;
static ParameterInfo t6553_m33891_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t795_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33891_GM;
MethodInfo m33891_MI = 
{
	"set_Item", NULL, &t6553_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6553_m33891_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33891_GM};
static MethodInfo* t6553_MIs[] =
{
	&m33892_MI,
	&m33893_MI,
	&m33894_MI,
	&m33890_MI,
	&m33891_MI,
	NULL
};
static TypeInfo* t6553_ITIs[] = 
{
	&t603_TI,
	&t6552_TI,
	&t6554_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6553_0_0_0;
extern Il2CppType t6553_1_0_0;
struct t6553;
extern Il2CppGenericClass t6553_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6553_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6553_MIs, t6553_PIs, NULL, NULL, NULL, NULL, NULL, &t6553_TI, t6553_ITIs, NULL, &t1908__CustomAttributeCache, &t6553_TI, &t6553_0_0_0, &t6553_1_0_0, NULL, &t6553_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5005_TI;

#include "t1023.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Security.Cryptography.CipherMode>
extern MethodInfo m33895_MI;
static PropertyInfo t5005____Current_PropertyInfo = 
{
	&t5005_TI, "Current", &m33895_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5005_PIs[] =
{
	&t5005____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1023_0_0_0;
extern void* RuntimeInvoker_t1023 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33895_GM;
MethodInfo m33895_MI = 
{
	"get_Current", NULL, &t5005_TI, &t1023_0_0_0, RuntimeInvoker_t1023, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33895_GM};
static MethodInfo* t5005_MIs[] =
{
	&m33895_MI,
	NULL
};
static TypeInfo* t5005_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5005_0_0_0;
extern Il2CppType t5005_1_0_0;
struct t5005;
extern Il2CppGenericClass t5005_GC;
TypeInfo t5005_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t5005_MIs, t5005_PIs, NULL, NULL, NULL, NULL, NULL, &t5005_TI, t5005_ITIs, NULL, &EmptyCustomAttributesCache, &t5005_TI, &t5005_0_0_0, &t5005_1_0_0, NULL, &t5005_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3456.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3456_TI;
#include "t3456MD.h"

extern TypeInfo t1023_TI;
extern MethodInfo m19154_MI;
extern MethodInfo m25963_MI;
struct t20;
 int32_t m25963 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m19150_MI;
 void m19150 (t3456 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19151_MI;
 t29 * m19151 (t3456 * __this, MethodInfo* method){
	{
		int32_t L_0 = m19154(__this, &m19154_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1023_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m19152_MI;
 void m19152 (t3456 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19153_MI;
 bool m19153 (t3456 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m19154 (t3456 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m25963(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25963_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Security.Cryptography.CipherMode>
extern Il2CppType t20_0_0_1;
FieldInfo t3456_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3456_TI, offsetof(t3456, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3456_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3456_TI, offsetof(t3456, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3456_FIs[] =
{
	&t3456_f0_FieldInfo,
	&t3456_f1_FieldInfo,
	NULL
};
static PropertyInfo t3456____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3456_TI, "System.Collections.IEnumerator.Current", &m19151_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3456____Current_PropertyInfo = 
{
	&t3456_TI, "Current", &m19154_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3456_PIs[] =
{
	&t3456____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3456____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3456_m19150_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19150_GM;
MethodInfo m19150_MI = 
{
	".ctor", (methodPointerType)&m19150, &t3456_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3456_m19150_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19150_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19151_GM;
MethodInfo m19151_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m19151, &t3456_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19151_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19152_GM;
MethodInfo m19152_MI = 
{
	"Dispose", (methodPointerType)&m19152, &t3456_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19152_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19153_GM;
MethodInfo m19153_MI = 
{
	"MoveNext", (methodPointerType)&m19153, &t3456_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19153_GM};
extern Il2CppType t1023_0_0_0;
extern void* RuntimeInvoker_t1023 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19154_GM;
MethodInfo m19154_MI = 
{
	"get_Current", (methodPointerType)&m19154, &t3456_TI, &t1023_0_0_0, RuntimeInvoker_t1023, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19154_GM};
static MethodInfo* t3456_MIs[] =
{
	&m19150_MI,
	&m19151_MI,
	&m19152_MI,
	&m19153_MI,
	&m19154_MI,
	NULL
};
static MethodInfo* t3456_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19151_MI,
	&m19153_MI,
	&m19152_MI,
	&m19154_MI,
};
static TypeInfo* t3456_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t5005_TI,
};
static Il2CppInterfaceOffsetPair t3456_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t5005_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3456_0_0_0;
extern Il2CppType t3456_1_0_0;
extern Il2CppGenericClass t3456_GC;
TypeInfo t3456_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3456_MIs, t3456_PIs, t3456_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3456_TI, t3456_ITIs, t3456_VT, &EmptyCustomAttributesCache, &t3456_TI, &t3456_0_0_0, &t3456_1_0_0, t3456_IOs, &t3456_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3456)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6555_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Security.Cryptography.CipherMode>
extern MethodInfo m33896_MI;
static PropertyInfo t6555____Count_PropertyInfo = 
{
	&t6555_TI, "Count", &m33896_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33897_MI;
static PropertyInfo t6555____IsReadOnly_PropertyInfo = 
{
	&t6555_TI, "IsReadOnly", &m33897_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6555_PIs[] =
{
	&t6555____Count_PropertyInfo,
	&t6555____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33896_GM;
MethodInfo m33896_MI = 
{
	"get_Count", NULL, &t6555_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33896_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33897_GM;
MethodInfo m33897_MI = 
{
	"get_IsReadOnly", NULL, &t6555_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33897_GM};
extern Il2CppType t1023_0_0_0;
extern Il2CppType t1023_0_0_0;
static ParameterInfo t6555_m33898_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1023_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33898_GM;
MethodInfo m33898_MI = 
{
	"Add", NULL, &t6555_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6555_m33898_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33898_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33899_GM;
MethodInfo m33899_MI = 
{
	"Clear", NULL, &t6555_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33899_GM};
extern Il2CppType t1023_0_0_0;
static ParameterInfo t6555_m33900_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1023_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33900_GM;
MethodInfo m33900_MI = 
{
	"Contains", NULL, &t6555_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6555_m33900_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33900_GM};
extern Il2CppType t3704_0_0_0;
extern Il2CppType t3704_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6555_m33901_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3704_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33901_GM;
MethodInfo m33901_MI = 
{
	"CopyTo", NULL, &t6555_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6555_m33901_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33901_GM};
extern Il2CppType t1023_0_0_0;
static ParameterInfo t6555_m33902_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1023_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33902_GM;
MethodInfo m33902_MI = 
{
	"Remove", NULL, &t6555_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6555_m33902_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33902_GM};
static MethodInfo* t6555_MIs[] =
{
	&m33896_MI,
	&m33897_MI,
	&m33898_MI,
	&m33899_MI,
	&m33900_MI,
	&m33901_MI,
	&m33902_MI,
	NULL
};
extern TypeInfo t6557_TI;
static TypeInfo* t6555_ITIs[] = 
{
	&t603_TI,
	&t6557_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6555_0_0_0;
extern Il2CppType t6555_1_0_0;
struct t6555;
extern Il2CppGenericClass t6555_GC;
TypeInfo t6555_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6555_MIs, t6555_PIs, NULL, NULL, NULL, NULL, NULL, &t6555_TI, t6555_ITIs, NULL, &EmptyCustomAttributesCache, &t6555_TI, &t6555_0_0_0, &t6555_1_0_0, NULL, &t6555_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Security.Cryptography.CipherMode>
extern Il2CppType t5005_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33903_GM;
MethodInfo m33903_MI = 
{
	"GetEnumerator", NULL, &t6557_TI, &t5005_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33903_GM};
static MethodInfo* t6557_MIs[] =
{
	&m33903_MI,
	NULL
};
static TypeInfo* t6557_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6557_0_0_0;
extern Il2CppType t6557_1_0_0;
struct t6557;
extern Il2CppGenericClass t6557_GC;
TypeInfo t6557_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6557_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6557_TI, t6557_ITIs, NULL, &EmptyCustomAttributesCache, &t6557_TI, &t6557_0_0_0, &t6557_1_0_0, NULL, &t6557_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6556_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Security.Cryptography.CipherMode>
extern MethodInfo m33904_MI;
extern MethodInfo m33905_MI;
static PropertyInfo t6556____Item_PropertyInfo = 
{
	&t6556_TI, "Item", &m33904_MI, &m33905_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6556_PIs[] =
{
	&t6556____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1023_0_0_0;
static ParameterInfo t6556_m33906_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1023_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33906_GM;
MethodInfo m33906_MI = 
{
	"IndexOf", NULL, &t6556_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6556_m33906_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33906_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1023_0_0_0;
static ParameterInfo t6556_m33907_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1023_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33907_GM;
MethodInfo m33907_MI = 
{
	"Insert", NULL, &t6556_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6556_m33907_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33907_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6556_m33908_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33908_GM;
MethodInfo m33908_MI = 
{
	"RemoveAt", NULL, &t6556_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6556_m33908_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33908_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6556_m33904_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1023_0_0_0;
extern void* RuntimeInvoker_t1023_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33904_GM;
MethodInfo m33904_MI = 
{
	"get_Item", NULL, &t6556_TI, &t1023_0_0_0, RuntimeInvoker_t1023_t44, t6556_m33904_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33904_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1023_0_0_0;
static ParameterInfo t6556_m33905_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1023_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33905_GM;
MethodInfo m33905_MI = 
{
	"set_Item", NULL, &t6556_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6556_m33905_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33905_GM};
static MethodInfo* t6556_MIs[] =
{
	&m33906_MI,
	&m33907_MI,
	&m33908_MI,
	&m33904_MI,
	&m33905_MI,
	NULL
};
static TypeInfo* t6556_ITIs[] = 
{
	&t603_TI,
	&t6555_TI,
	&t6557_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6556_0_0_0;
extern Il2CppType t6556_1_0_0;
struct t6556;
extern Il2CppGenericClass t6556_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6556_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6556_MIs, t6556_PIs, NULL, NULL, NULL, NULL, NULL, &t6556_TI, t6556_ITIs, NULL, &t1908__CustomAttributeCache, &t6556_TI, &t6556_0_0_0, &t6556_1_0_0, NULL, &t6556_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5007_TI;

#include "t1099.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Security.Cryptography.CspProviderFlags>
extern MethodInfo m33909_MI;
static PropertyInfo t5007____Current_PropertyInfo = 
{
	&t5007_TI, "Current", &m33909_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5007_PIs[] =
{
	&t5007____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1099_0_0_0;
extern void* RuntimeInvoker_t1099 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33909_GM;
MethodInfo m33909_MI = 
{
	"get_Current", NULL, &t5007_TI, &t1099_0_0_0, RuntimeInvoker_t1099, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33909_GM};
static MethodInfo* t5007_MIs[] =
{
	&m33909_MI,
	NULL
};
static TypeInfo* t5007_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5007_0_0_0;
extern Il2CppType t5007_1_0_0;
struct t5007;
extern Il2CppGenericClass t5007_GC;
TypeInfo t5007_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t5007_MIs, t5007_PIs, NULL, NULL, NULL, NULL, NULL, &t5007_TI, t5007_ITIs, NULL, &EmptyCustomAttributesCache, &t5007_TI, &t5007_0_0_0, &t5007_1_0_0, NULL, &t5007_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3457.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3457_TI;
#include "t3457MD.h"

extern TypeInfo t1099_TI;
extern MethodInfo m19159_MI;
extern MethodInfo m25974_MI;
struct t20;
 int32_t m25974 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m19155_MI;
 void m19155 (t3457 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19156_MI;
 t29 * m19156 (t3457 * __this, MethodInfo* method){
	{
		int32_t L_0 = m19159(__this, &m19159_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1099_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m19157_MI;
 void m19157 (t3457 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19158_MI;
 bool m19158 (t3457 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m19159 (t3457 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m25974(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25974_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Security.Cryptography.CspProviderFlags>
extern Il2CppType t20_0_0_1;
FieldInfo t3457_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3457_TI, offsetof(t3457, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3457_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3457_TI, offsetof(t3457, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3457_FIs[] =
{
	&t3457_f0_FieldInfo,
	&t3457_f1_FieldInfo,
	NULL
};
static PropertyInfo t3457____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3457_TI, "System.Collections.IEnumerator.Current", &m19156_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3457____Current_PropertyInfo = 
{
	&t3457_TI, "Current", &m19159_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3457_PIs[] =
{
	&t3457____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3457____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3457_m19155_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19155_GM;
MethodInfo m19155_MI = 
{
	".ctor", (methodPointerType)&m19155, &t3457_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3457_m19155_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19155_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19156_GM;
MethodInfo m19156_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m19156, &t3457_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19156_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19157_GM;
MethodInfo m19157_MI = 
{
	"Dispose", (methodPointerType)&m19157, &t3457_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19157_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19158_GM;
MethodInfo m19158_MI = 
{
	"MoveNext", (methodPointerType)&m19158, &t3457_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19158_GM};
extern Il2CppType t1099_0_0_0;
extern void* RuntimeInvoker_t1099 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19159_GM;
MethodInfo m19159_MI = 
{
	"get_Current", (methodPointerType)&m19159, &t3457_TI, &t1099_0_0_0, RuntimeInvoker_t1099, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19159_GM};
static MethodInfo* t3457_MIs[] =
{
	&m19155_MI,
	&m19156_MI,
	&m19157_MI,
	&m19158_MI,
	&m19159_MI,
	NULL
};
static MethodInfo* t3457_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19156_MI,
	&m19158_MI,
	&m19157_MI,
	&m19159_MI,
};
static TypeInfo* t3457_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t5007_TI,
};
static Il2CppInterfaceOffsetPair t3457_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t5007_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3457_0_0_0;
extern Il2CppType t3457_1_0_0;
extern Il2CppGenericClass t3457_GC;
TypeInfo t3457_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3457_MIs, t3457_PIs, t3457_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3457_TI, t3457_ITIs, t3457_VT, &EmptyCustomAttributesCache, &t3457_TI, &t3457_0_0_0, &t3457_1_0_0, t3457_IOs, &t3457_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3457)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6558_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Security.Cryptography.CspProviderFlags>
extern MethodInfo m33910_MI;
static PropertyInfo t6558____Count_PropertyInfo = 
{
	&t6558_TI, "Count", &m33910_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33911_MI;
static PropertyInfo t6558____IsReadOnly_PropertyInfo = 
{
	&t6558_TI, "IsReadOnly", &m33911_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6558_PIs[] =
{
	&t6558____Count_PropertyInfo,
	&t6558____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33910_GM;
MethodInfo m33910_MI = 
{
	"get_Count", NULL, &t6558_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33910_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33911_GM;
MethodInfo m33911_MI = 
{
	"get_IsReadOnly", NULL, &t6558_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33911_GM};
extern Il2CppType t1099_0_0_0;
extern Il2CppType t1099_0_0_0;
static ParameterInfo t6558_m33912_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1099_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33912_GM;
MethodInfo m33912_MI = 
{
	"Add", NULL, &t6558_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6558_m33912_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33912_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33913_GM;
MethodInfo m33913_MI = 
{
	"Clear", NULL, &t6558_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33913_GM};
extern Il2CppType t1099_0_0_0;
static ParameterInfo t6558_m33914_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1099_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33914_GM;
MethodInfo m33914_MI = 
{
	"Contains", NULL, &t6558_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6558_m33914_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33914_GM};
extern Il2CppType t3705_0_0_0;
extern Il2CppType t3705_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6558_m33915_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3705_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33915_GM;
MethodInfo m33915_MI = 
{
	"CopyTo", NULL, &t6558_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6558_m33915_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33915_GM};
extern Il2CppType t1099_0_0_0;
static ParameterInfo t6558_m33916_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1099_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33916_GM;
MethodInfo m33916_MI = 
{
	"Remove", NULL, &t6558_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6558_m33916_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33916_GM};
static MethodInfo* t6558_MIs[] =
{
	&m33910_MI,
	&m33911_MI,
	&m33912_MI,
	&m33913_MI,
	&m33914_MI,
	&m33915_MI,
	&m33916_MI,
	NULL
};
extern TypeInfo t6560_TI;
static TypeInfo* t6558_ITIs[] = 
{
	&t603_TI,
	&t6560_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6558_0_0_0;
extern Il2CppType t6558_1_0_0;
struct t6558;
extern Il2CppGenericClass t6558_GC;
TypeInfo t6558_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6558_MIs, t6558_PIs, NULL, NULL, NULL, NULL, NULL, &t6558_TI, t6558_ITIs, NULL, &EmptyCustomAttributesCache, &t6558_TI, &t6558_0_0_0, &t6558_1_0_0, NULL, &t6558_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Security.Cryptography.CspProviderFlags>
extern Il2CppType t5007_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33917_GM;
MethodInfo m33917_MI = 
{
	"GetEnumerator", NULL, &t6560_TI, &t5007_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33917_GM};
static MethodInfo* t6560_MIs[] =
{
	&m33917_MI,
	NULL
};
static TypeInfo* t6560_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6560_0_0_0;
extern Il2CppType t6560_1_0_0;
struct t6560;
extern Il2CppGenericClass t6560_GC;
TypeInfo t6560_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6560_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6560_TI, t6560_ITIs, NULL, &EmptyCustomAttributesCache, &t6560_TI, &t6560_0_0_0, &t6560_1_0_0, NULL, &t6560_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6559_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Security.Cryptography.CspProviderFlags>
extern MethodInfo m33918_MI;
extern MethodInfo m33919_MI;
static PropertyInfo t6559____Item_PropertyInfo = 
{
	&t6559_TI, "Item", &m33918_MI, &m33919_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6559_PIs[] =
{
	&t6559____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1099_0_0_0;
static ParameterInfo t6559_m33920_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1099_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33920_GM;
MethodInfo m33920_MI = 
{
	"IndexOf", NULL, &t6559_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6559_m33920_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33920_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1099_0_0_0;
static ParameterInfo t6559_m33921_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1099_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33921_GM;
MethodInfo m33921_MI = 
{
	"Insert", NULL, &t6559_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6559_m33921_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33921_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6559_m33922_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33922_GM;
MethodInfo m33922_MI = 
{
	"RemoveAt", NULL, &t6559_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6559_m33922_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33922_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6559_m33918_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1099_0_0_0;
extern void* RuntimeInvoker_t1099_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33918_GM;
MethodInfo m33918_MI = 
{
	"get_Item", NULL, &t6559_TI, &t1099_0_0_0, RuntimeInvoker_t1099_t44, t6559_m33918_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33918_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1099_0_0_0;
static ParameterInfo t6559_m33919_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1099_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33919_GM;
MethodInfo m33919_MI = 
{
	"set_Item", NULL, &t6559_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6559_m33919_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33919_GM};
static MethodInfo* t6559_MIs[] =
{
	&m33920_MI,
	&m33921_MI,
	&m33922_MI,
	&m33918_MI,
	&m33919_MI,
	NULL
};
static TypeInfo* t6559_ITIs[] = 
{
	&t603_TI,
	&t6558_TI,
	&t6560_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6559_0_0_0;
extern Il2CppType t6559_1_0_0;
struct t6559;
extern Il2CppGenericClass t6559_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6559_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6559_MIs, t6559_PIs, NULL, NULL, NULL, NULL, NULL, &t6559_TI, t6559_ITIs, NULL, &t1908__CustomAttributeCache, &t6559_TI, &t6559_0_0_0, &t6559_1_0_0, NULL, &t6559_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5009_TI;

#include "t1117.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Security.Cryptography.PaddingMode>
extern MethodInfo m33923_MI;
static PropertyInfo t5009____Current_PropertyInfo = 
{
	&t5009_TI, "Current", &m33923_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5009_PIs[] =
{
	&t5009____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1117_0_0_0;
extern void* RuntimeInvoker_t1117 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33923_GM;
MethodInfo m33923_MI = 
{
	"get_Current", NULL, &t5009_TI, &t1117_0_0_0, RuntimeInvoker_t1117, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33923_GM};
static MethodInfo* t5009_MIs[] =
{
	&m33923_MI,
	NULL
};
static TypeInfo* t5009_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5009_0_0_0;
extern Il2CppType t5009_1_0_0;
struct t5009;
extern Il2CppGenericClass t5009_GC;
TypeInfo t5009_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t5009_MIs, t5009_PIs, NULL, NULL, NULL, NULL, NULL, &t5009_TI, t5009_ITIs, NULL, &EmptyCustomAttributesCache, &t5009_TI, &t5009_0_0_0, &t5009_1_0_0, NULL, &t5009_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3458.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3458_TI;
#include "t3458MD.h"

extern TypeInfo t1117_TI;
extern MethodInfo m19164_MI;
extern MethodInfo m25985_MI;
struct t20;
 int32_t m25985 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m19160_MI;
 void m19160 (t3458 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19161_MI;
 t29 * m19161 (t3458 * __this, MethodInfo* method){
	{
		int32_t L_0 = m19164(__this, &m19164_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1117_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m19162_MI;
 void m19162 (t3458 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19163_MI;
 bool m19163 (t3458 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m19164 (t3458 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m25985(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25985_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Security.Cryptography.PaddingMode>
extern Il2CppType t20_0_0_1;
FieldInfo t3458_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3458_TI, offsetof(t3458, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3458_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3458_TI, offsetof(t3458, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3458_FIs[] =
{
	&t3458_f0_FieldInfo,
	&t3458_f1_FieldInfo,
	NULL
};
static PropertyInfo t3458____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3458_TI, "System.Collections.IEnumerator.Current", &m19161_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3458____Current_PropertyInfo = 
{
	&t3458_TI, "Current", &m19164_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3458_PIs[] =
{
	&t3458____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3458____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3458_m19160_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19160_GM;
MethodInfo m19160_MI = 
{
	".ctor", (methodPointerType)&m19160, &t3458_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3458_m19160_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19160_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19161_GM;
MethodInfo m19161_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m19161, &t3458_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19161_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19162_GM;
MethodInfo m19162_MI = 
{
	"Dispose", (methodPointerType)&m19162, &t3458_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19162_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19163_GM;
MethodInfo m19163_MI = 
{
	"MoveNext", (methodPointerType)&m19163, &t3458_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19163_GM};
extern Il2CppType t1117_0_0_0;
extern void* RuntimeInvoker_t1117 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19164_GM;
MethodInfo m19164_MI = 
{
	"get_Current", (methodPointerType)&m19164, &t3458_TI, &t1117_0_0_0, RuntimeInvoker_t1117, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19164_GM};
static MethodInfo* t3458_MIs[] =
{
	&m19160_MI,
	&m19161_MI,
	&m19162_MI,
	&m19163_MI,
	&m19164_MI,
	NULL
};
static MethodInfo* t3458_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19161_MI,
	&m19163_MI,
	&m19162_MI,
	&m19164_MI,
};
static TypeInfo* t3458_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t5009_TI,
};
static Il2CppInterfaceOffsetPair t3458_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t5009_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3458_0_0_0;
extern Il2CppType t3458_1_0_0;
extern Il2CppGenericClass t3458_GC;
TypeInfo t3458_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3458_MIs, t3458_PIs, t3458_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3458_TI, t3458_ITIs, t3458_VT, &EmptyCustomAttributesCache, &t3458_TI, &t3458_0_0_0, &t3458_1_0_0, t3458_IOs, &t3458_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3458)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6561_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Security.Cryptography.PaddingMode>
extern MethodInfo m33924_MI;
static PropertyInfo t6561____Count_PropertyInfo = 
{
	&t6561_TI, "Count", &m33924_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33925_MI;
static PropertyInfo t6561____IsReadOnly_PropertyInfo = 
{
	&t6561_TI, "IsReadOnly", &m33925_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6561_PIs[] =
{
	&t6561____Count_PropertyInfo,
	&t6561____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33924_GM;
MethodInfo m33924_MI = 
{
	"get_Count", NULL, &t6561_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33924_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33925_GM;
MethodInfo m33925_MI = 
{
	"get_IsReadOnly", NULL, &t6561_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33925_GM};
extern Il2CppType t1117_0_0_0;
extern Il2CppType t1117_0_0_0;
static ParameterInfo t6561_m33926_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1117_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33926_GM;
MethodInfo m33926_MI = 
{
	"Add", NULL, &t6561_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6561_m33926_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33926_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33927_GM;
MethodInfo m33927_MI = 
{
	"Clear", NULL, &t6561_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33927_GM};
extern Il2CppType t1117_0_0_0;
static ParameterInfo t6561_m33928_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1117_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33928_GM;
MethodInfo m33928_MI = 
{
	"Contains", NULL, &t6561_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6561_m33928_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33928_GM};
extern Il2CppType t3706_0_0_0;
extern Il2CppType t3706_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6561_m33929_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3706_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33929_GM;
MethodInfo m33929_MI = 
{
	"CopyTo", NULL, &t6561_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6561_m33929_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33929_GM};
extern Il2CppType t1117_0_0_0;
static ParameterInfo t6561_m33930_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1117_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33930_GM;
MethodInfo m33930_MI = 
{
	"Remove", NULL, &t6561_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6561_m33930_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33930_GM};
static MethodInfo* t6561_MIs[] =
{
	&m33924_MI,
	&m33925_MI,
	&m33926_MI,
	&m33927_MI,
	&m33928_MI,
	&m33929_MI,
	&m33930_MI,
	NULL
};
extern TypeInfo t6563_TI;
static TypeInfo* t6561_ITIs[] = 
{
	&t603_TI,
	&t6563_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6561_0_0_0;
extern Il2CppType t6561_1_0_0;
struct t6561;
extern Il2CppGenericClass t6561_GC;
TypeInfo t6561_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6561_MIs, t6561_PIs, NULL, NULL, NULL, NULL, NULL, &t6561_TI, t6561_ITIs, NULL, &EmptyCustomAttributesCache, &t6561_TI, &t6561_0_0_0, &t6561_1_0_0, NULL, &t6561_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Security.Cryptography.PaddingMode>
extern Il2CppType t5009_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33931_GM;
MethodInfo m33931_MI = 
{
	"GetEnumerator", NULL, &t6563_TI, &t5009_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33931_GM};
static MethodInfo* t6563_MIs[] =
{
	&m33931_MI,
	NULL
};
static TypeInfo* t6563_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6563_0_0_0;
extern Il2CppType t6563_1_0_0;
struct t6563;
extern Il2CppGenericClass t6563_GC;
TypeInfo t6563_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6563_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6563_TI, t6563_ITIs, NULL, &EmptyCustomAttributesCache, &t6563_TI, &t6563_0_0_0, &t6563_1_0_0, NULL, &t6563_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6562_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Security.Cryptography.PaddingMode>
extern MethodInfo m33932_MI;
extern MethodInfo m33933_MI;
static PropertyInfo t6562____Item_PropertyInfo = 
{
	&t6562_TI, "Item", &m33932_MI, &m33933_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6562_PIs[] =
{
	&t6562____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1117_0_0_0;
static ParameterInfo t6562_m33934_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1117_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33934_GM;
MethodInfo m33934_MI = 
{
	"IndexOf", NULL, &t6562_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6562_m33934_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33934_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1117_0_0_0;
static ParameterInfo t6562_m33935_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1117_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33935_GM;
MethodInfo m33935_MI = 
{
	"Insert", NULL, &t6562_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6562_m33935_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33935_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6562_m33936_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33936_GM;
MethodInfo m33936_MI = 
{
	"RemoveAt", NULL, &t6562_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6562_m33936_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33936_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6562_m33932_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1117_0_0_0;
extern void* RuntimeInvoker_t1117_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33932_GM;
MethodInfo m33932_MI = 
{
	"get_Item", NULL, &t6562_TI, &t1117_0_0_0, RuntimeInvoker_t1117_t44, t6562_m33932_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33932_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1117_0_0_0;
static ParameterInfo t6562_m33933_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1117_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33933_GM;
MethodInfo m33933_MI = 
{
	"set_Item", NULL, &t6562_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6562_m33933_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33933_GM};
static MethodInfo* t6562_MIs[] =
{
	&m33934_MI,
	&m33935_MI,
	&m33936_MI,
	&m33932_MI,
	&m33933_MI,
	NULL
};
static TypeInfo* t6562_ITIs[] = 
{
	&t603_TI,
	&t6561_TI,
	&t6563_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6562_0_0_0;
extern Il2CppType t6562_1_0_0;
struct t6562;
extern Il2CppGenericClass t6562_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6562_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6562_MIs, t6562_PIs, NULL, NULL, NULL, NULL, NULL, &t6562_TI, t6562_ITIs, NULL, &t1908__CustomAttributeCache, &t6562_TI, &t6562_0_0_0, &t6562_1_0_0, NULL, &t6562_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1564_TI;

#include "t1567.h"


// Metadata Definition System.Collections.Generic.IList`1<System.Security.Policy.StrongName>
extern MethodInfo m33937_MI;
extern MethodInfo m33938_MI;
static PropertyInfo t1564____Item_PropertyInfo = 
{
	&t1564_TI, "Item", &m33937_MI, &m33938_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1564_PIs[] =
{
	&t1564____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1567_0_0_0;
extern Il2CppType t1567_0_0_0;
static ParameterInfo t1564_m33939_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33939_GM;
MethodInfo m33939_MI = 
{
	"IndexOf", NULL, &t1564_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t1564_m33939_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33939_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1567_0_0_0;
static ParameterInfo t1564_m33940_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33940_GM;
MethodInfo m33940_MI = 
{
	"Insert", NULL, &t1564_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t1564_m33940_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33940_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t1564_m33941_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33941_GM;
MethodInfo m33941_MI = 
{
	"RemoveAt", NULL, &t1564_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t1564_m33941_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33941_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t1564_m33937_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1567_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33937_GM;
MethodInfo m33937_MI = 
{
	"get_Item", NULL, &t1564_TI, &t1567_0_0_0, RuntimeInvoker_t29_t44, t1564_m33937_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33937_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1567_0_0_0;
static ParameterInfo t1564_m33938_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33938_GM;
MethodInfo m33938_MI = 
{
	"set_Item", NULL, &t1564_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t1564_m33938_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33938_GM};
static MethodInfo* t1564_MIs[] =
{
	&m33939_MI,
	&m33940_MI,
	&m33941_MI,
	&m33937_MI,
	&m33938_MI,
	NULL
};
extern TypeInfo t3464_TI;
extern TypeInfo t3465_TI;
static TypeInfo* t1564_ITIs[] = 
{
	&t603_TI,
	&t3464_TI,
	&t3465_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1564_0_0_0;
extern Il2CppType t1564_1_0_0;
struct t1564;
extern Il2CppGenericClass t1564_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t1564_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t1564_MIs, t1564_PIs, NULL, NULL, NULL, NULL, NULL, &t1564_TI, t1564_ITIs, NULL, &t1908__CustomAttributeCache, &t1564_TI, &t1564_0_0_0, &t1564_1_0_0, NULL, &t1564_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.ICollection`1<System.Security.Policy.StrongName>
extern MethodInfo m33942_MI;
static PropertyInfo t3464____Count_PropertyInfo = 
{
	&t3464_TI, "Count", &m33942_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33943_MI;
static PropertyInfo t3464____IsReadOnly_PropertyInfo = 
{
	&t3464_TI, "IsReadOnly", &m33943_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3464_PIs[] =
{
	&t3464____Count_PropertyInfo,
	&t3464____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33942_GM;
MethodInfo m33942_MI = 
{
	"get_Count", NULL, &t3464_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33942_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33943_GM;
MethodInfo m33943_MI = 
{
	"get_IsReadOnly", NULL, &t3464_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33943_GM};
extern Il2CppType t1567_0_0_0;
static ParameterInfo t3464_m33944_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33944_GM;
MethodInfo m33944_MI = 
{
	"Add", NULL, &t3464_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3464_m33944_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33944_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33945_GM;
MethodInfo m33945_MI = 
{
	"Clear", NULL, &t3464_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33945_GM};
extern Il2CppType t1567_0_0_0;
static ParameterInfo t3464_m33946_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33946_GM;
MethodInfo m33946_MI = 
{
	"Contains", NULL, &t3464_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3464_m33946_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33946_GM};
extern Il2CppType t3462_0_0_0;
extern Il2CppType t3462_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3464_m33947_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3462_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33947_GM;
MethodInfo m33947_MI = 
{
	"CopyTo", NULL, &t3464_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3464_m33947_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33947_GM};
extern Il2CppType t1567_0_0_0;
static ParameterInfo t3464_m33948_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33948_GM;
MethodInfo m33948_MI = 
{
	"Remove", NULL, &t3464_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3464_m33948_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33948_GM};
static MethodInfo* t3464_MIs[] =
{
	&m33942_MI,
	&m33943_MI,
	&m33944_MI,
	&m33945_MI,
	&m33946_MI,
	&m33947_MI,
	&m33948_MI,
	NULL
};
static TypeInfo* t3464_ITIs[] = 
{
	&t603_TI,
	&t3465_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3464_0_0_0;
extern Il2CppType t3464_1_0_0;
struct t3464;
extern Il2CppGenericClass t3464_GC;
TypeInfo t3464_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t3464_MIs, t3464_PIs, NULL, NULL, NULL, NULL, NULL, &t3464_TI, t3464_ITIs, NULL, &EmptyCustomAttributesCache, &t3464_TI, &t3464_0_0_0, &t3464_1_0_0, NULL, &t3464_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Security.Policy.StrongName>
extern Il2CppType t3463_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33949_GM;
MethodInfo m33949_MI = 
{
	"GetEnumerator", NULL, &t3465_TI, &t3463_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33949_GM};
static MethodInfo* t3465_MIs[] =
{
	&m33949_MI,
	NULL
};
static TypeInfo* t3465_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3465_0_0_0;
extern Il2CppType t3465_1_0_0;
struct t3465;
extern Il2CppGenericClass t3465_GC;
TypeInfo t3465_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t3465_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t3465_TI, t3465_ITIs, NULL, &EmptyCustomAttributesCache, &t3465_TI, &t3465_0_0_0, &t3465_1_0_0, NULL, &t3465_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3463_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Security.Policy.StrongName>
extern MethodInfo m33950_MI;
static PropertyInfo t3463____Current_PropertyInfo = 
{
	&t3463_TI, "Current", &m33950_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3463_PIs[] =
{
	&t3463____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1567_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33950_GM;
MethodInfo m33950_MI = 
{
	"get_Current", NULL, &t3463_TI, &t1567_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33950_GM};
static MethodInfo* t3463_MIs[] =
{
	&m33950_MI,
	NULL
};
static TypeInfo* t3463_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3463_0_0_0;
extern Il2CppType t3463_1_0_0;
struct t3463;
extern Il2CppGenericClass t3463_GC;
TypeInfo t3463_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t3463_MIs, t3463_PIs, NULL, NULL, NULL, NULL, NULL, &t3463_TI, t3463_ITIs, NULL, &EmptyCustomAttributesCache, &t3463_TI, &t3463_0_0_0, &t3463_1_0_0, NULL, &t3463_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3459.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3459_TI;
#include "t3459MD.h"

extern TypeInfo t1567_TI;
extern MethodInfo m19169_MI;
extern MethodInfo m25996_MI;
struct t20;
#define m25996(__this, p0, method) (t1567 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Security.Policy.StrongName>
extern Il2CppType t20_0_0_1;
FieldInfo t3459_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3459_TI, offsetof(t3459, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3459_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3459_TI, offsetof(t3459, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3459_FIs[] =
{
	&t3459_f0_FieldInfo,
	&t3459_f1_FieldInfo,
	NULL
};
extern MethodInfo m19166_MI;
static PropertyInfo t3459____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3459_TI, "System.Collections.IEnumerator.Current", &m19166_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3459____Current_PropertyInfo = 
{
	&t3459_TI, "Current", &m19169_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3459_PIs[] =
{
	&t3459____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3459____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3459_m19165_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19165_GM;
MethodInfo m19165_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3459_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3459_m19165_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19165_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19166_GM;
MethodInfo m19166_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3459_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19166_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19167_GM;
MethodInfo m19167_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3459_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19167_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19168_GM;
MethodInfo m19168_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3459_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19168_GM};
extern Il2CppType t1567_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19169_GM;
MethodInfo m19169_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3459_TI, &t1567_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19169_GM};
static MethodInfo* t3459_MIs[] =
{
	&m19165_MI,
	&m19166_MI,
	&m19167_MI,
	&m19168_MI,
	&m19169_MI,
	NULL
};
extern MethodInfo m19168_MI;
extern MethodInfo m19167_MI;
static MethodInfo* t3459_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19166_MI,
	&m19168_MI,
	&m19167_MI,
	&m19169_MI,
};
static TypeInfo* t3459_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3463_TI,
};
static Il2CppInterfaceOffsetPair t3459_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3463_TI, 7},
};
extern TypeInfo t1567_TI;
static Il2CppRGCTXData t3459_RGCTXData[3] = 
{
	&m19169_MI/* Method Usage */,
	&t1567_TI/* Class Usage */,
	&m25996_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3459_0_0_0;
extern Il2CppType t3459_1_0_0;
extern Il2CppGenericClass t3459_GC;
TypeInfo t3459_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3459_MIs, t3459_PIs, t3459_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3459_TI, t3459_ITIs, t3459_VT, &EmptyCustomAttributesCache, &t3459_TI, &t3459_0_0_0, &t3459_1_0_0, t3459_IOs, &t3459_GC, NULL, NULL, NULL, t3459_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3459)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6564_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Security.Policy.IBuiltInEvidence>
extern MethodInfo m33951_MI;
static PropertyInfo t6564____Count_PropertyInfo = 
{
	&t6564_TI, "Count", &m33951_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33952_MI;
static PropertyInfo t6564____IsReadOnly_PropertyInfo = 
{
	&t6564_TI, "IsReadOnly", &m33952_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6564_PIs[] =
{
	&t6564____Count_PropertyInfo,
	&t6564____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33951_GM;
MethodInfo m33951_MI = 
{
	"get_Count", NULL, &t6564_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33951_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33952_GM;
MethodInfo m33952_MI = 
{
	"get_IsReadOnly", NULL, &t6564_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33952_GM};
extern Il2CppType t2063_0_0_0;
extern Il2CppType t2063_0_0_0;
static ParameterInfo t6564_m33953_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2063_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33953_GM;
MethodInfo m33953_MI = 
{
	"Add", NULL, &t6564_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6564_m33953_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33953_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33954_GM;
MethodInfo m33954_MI = 
{
	"Clear", NULL, &t6564_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33954_GM};
extern Il2CppType t2063_0_0_0;
static ParameterInfo t6564_m33955_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2063_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33955_GM;
MethodInfo m33955_MI = 
{
	"Contains", NULL, &t6564_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6564_m33955_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33955_GM};
extern Il2CppType t3707_0_0_0;
extern Il2CppType t3707_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6564_m33956_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3707_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33956_GM;
MethodInfo m33956_MI = 
{
	"CopyTo", NULL, &t6564_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6564_m33956_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33956_GM};
extern Il2CppType t2063_0_0_0;
static ParameterInfo t6564_m33957_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2063_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33957_GM;
MethodInfo m33957_MI = 
{
	"Remove", NULL, &t6564_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6564_m33957_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33957_GM};
static MethodInfo* t6564_MIs[] =
{
	&m33951_MI,
	&m33952_MI,
	&m33953_MI,
	&m33954_MI,
	&m33955_MI,
	&m33956_MI,
	&m33957_MI,
	NULL
};
extern TypeInfo t6566_TI;
static TypeInfo* t6564_ITIs[] = 
{
	&t603_TI,
	&t6566_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6564_0_0_0;
extern Il2CppType t6564_1_0_0;
struct t6564;
extern Il2CppGenericClass t6564_GC;
TypeInfo t6564_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6564_MIs, t6564_PIs, NULL, NULL, NULL, NULL, NULL, &t6564_TI, t6564_ITIs, NULL, &EmptyCustomAttributesCache, &t6564_TI, &t6564_0_0_0, &t6564_1_0_0, NULL, &t6564_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Security.Policy.IBuiltInEvidence>
extern Il2CppType t5012_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33958_GM;
MethodInfo m33958_MI = 
{
	"GetEnumerator", NULL, &t6566_TI, &t5012_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33958_GM};
static MethodInfo* t6566_MIs[] =
{
	&m33958_MI,
	NULL
};
static TypeInfo* t6566_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6566_0_0_0;
extern Il2CppType t6566_1_0_0;
struct t6566;
extern Il2CppGenericClass t6566_GC;
TypeInfo t6566_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6566_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6566_TI, t6566_ITIs, NULL, &EmptyCustomAttributesCache, &t6566_TI, &t6566_0_0_0, &t6566_1_0_0, NULL, &t6566_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5012_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Security.Policy.IBuiltInEvidence>
extern MethodInfo m33959_MI;
static PropertyInfo t5012____Current_PropertyInfo = 
{
	&t5012_TI, "Current", &m33959_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5012_PIs[] =
{
	&t5012____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2063_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33959_GM;
MethodInfo m33959_MI = 
{
	"get_Current", NULL, &t5012_TI, &t2063_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33959_GM};
static MethodInfo* t5012_MIs[] =
{
	&m33959_MI,
	NULL
};
static TypeInfo* t5012_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5012_0_0_0;
extern Il2CppType t5012_1_0_0;
struct t5012;
extern Il2CppGenericClass t5012_GC;
TypeInfo t5012_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t5012_MIs, t5012_PIs, NULL, NULL, NULL, NULL, NULL, &t5012_TI, t5012_ITIs, NULL, &EmptyCustomAttributesCache, &t5012_TI, &t5012_0_0_0, &t5012_1_0_0, NULL, &t5012_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3460.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3460_TI;
#include "t3460MD.h"

extern TypeInfo t2063_TI;
extern MethodInfo m19174_MI;
extern MethodInfo m26007_MI;
struct t20;
#define m26007(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Security.Policy.IBuiltInEvidence>
extern Il2CppType t20_0_0_1;
FieldInfo t3460_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3460_TI, offsetof(t3460, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3460_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3460_TI, offsetof(t3460, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3460_FIs[] =
{
	&t3460_f0_FieldInfo,
	&t3460_f1_FieldInfo,
	NULL
};
extern MethodInfo m19171_MI;
static PropertyInfo t3460____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3460_TI, "System.Collections.IEnumerator.Current", &m19171_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3460____Current_PropertyInfo = 
{
	&t3460_TI, "Current", &m19174_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3460_PIs[] =
{
	&t3460____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3460____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3460_m19170_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19170_GM;
MethodInfo m19170_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3460_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3460_m19170_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19170_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19171_GM;
MethodInfo m19171_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3460_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19171_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19172_GM;
MethodInfo m19172_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3460_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19172_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19173_GM;
MethodInfo m19173_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3460_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19173_GM};
extern Il2CppType t2063_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19174_GM;
MethodInfo m19174_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3460_TI, &t2063_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19174_GM};
static MethodInfo* t3460_MIs[] =
{
	&m19170_MI,
	&m19171_MI,
	&m19172_MI,
	&m19173_MI,
	&m19174_MI,
	NULL
};
extern MethodInfo m19173_MI;
extern MethodInfo m19172_MI;
static MethodInfo* t3460_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19171_MI,
	&m19173_MI,
	&m19172_MI,
	&m19174_MI,
};
static TypeInfo* t3460_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t5012_TI,
};
static Il2CppInterfaceOffsetPair t3460_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t5012_TI, 7},
};
extern TypeInfo t2063_TI;
static Il2CppRGCTXData t3460_RGCTXData[3] = 
{
	&m19174_MI/* Method Usage */,
	&t2063_TI/* Class Usage */,
	&m26007_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3460_0_0_0;
extern Il2CppType t3460_1_0_0;
extern Il2CppGenericClass t3460_GC;
TypeInfo t3460_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3460_MIs, t3460_PIs, t3460_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3460_TI, t3460_ITIs, t3460_VT, &EmptyCustomAttributesCache, &t3460_TI, &t3460_0_0_0, &t3460_1_0_0, t3460_IOs, &t3460_GC, NULL, NULL, NULL, t3460_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3460)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6565_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Security.Policy.IBuiltInEvidence>
extern MethodInfo m33960_MI;
extern MethodInfo m33961_MI;
static PropertyInfo t6565____Item_PropertyInfo = 
{
	&t6565_TI, "Item", &m33960_MI, &m33961_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6565_PIs[] =
{
	&t6565____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2063_0_0_0;
static ParameterInfo t6565_m33962_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2063_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33962_GM;
MethodInfo m33962_MI = 
{
	"IndexOf", NULL, &t6565_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6565_m33962_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33962_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2063_0_0_0;
static ParameterInfo t6565_m33963_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t2063_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33963_GM;
MethodInfo m33963_MI = 
{
	"Insert", NULL, &t6565_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6565_m33963_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33963_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6565_m33964_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33964_GM;
MethodInfo m33964_MI = 
{
	"RemoveAt", NULL, &t6565_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6565_m33964_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33964_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6565_m33960_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2063_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33960_GM;
MethodInfo m33960_MI = 
{
	"get_Item", NULL, &t6565_TI, &t2063_0_0_0, RuntimeInvoker_t29_t44, t6565_m33960_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33960_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2063_0_0_0;
static ParameterInfo t6565_m33961_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2063_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33961_GM;
MethodInfo m33961_MI = 
{
	"set_Item", NULL, &t6565_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6565_m33961_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33961_GM};
static MethodInfo* t6565_MIs[] =
{
	&m33962_MI,
	&m33963_MI,
	&m33964_MI,
	&m33960_MI,
	&m33961_MI,
	NULL
};
static TypeInfo* t6565_ITIs[] = 
{
	&t603_TI,
	&t6564_TI,
	&t6566_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6565_0_0_0;
extern Il2CppType t6565_1_0_0;
struct t6565;
extern Il2CppGenericClass t6565_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6565_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6565_MIs, t6565_PIs, NULL, NULL, NULL, NULL, NULL, &t6565_TI, t6565_ITIs, NULL, &t1908__CustomAttributeCache, &t6565_TI, &t6565_0_0_0, &t6565_1_0_0, NULL, &t6565_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6567_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Security.Policy.IIdentityPermissionFactory>
extern MethodInfo m33965_MI;
static PropertyInfo t6567____Count_PropertyInfo = 
{
	&t6567_TI, "Count", &m33965_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33966_MI;
static PropertyInfo t6567____IsReadOnly_PropertyInfo = 
{
	&t6567_TI, "IsReadOnly", &m33966_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6567_PIs[] =
{
	&t6567____Count_PropertyInfo,
	&t6567____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33965_GM;
MethodInfo m33965_MI = 
{
	"get_Count", NULL, &t6567_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33965_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33966_GM;
MethodInfo m33966_MI = 
{
	"get_IsReadOnly", NULL, &t6567_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33966_GM};
extern Il2CppType t2064_0_0_0;
extern Il2CppType t2064_0_0_0;
static ParameterInfo t6567_m33967_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2064_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33967_GM;
MethodInfo m33967_MI = 
{
	"Add", NULL, &t6567_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6567_m33967_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33967_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33968_GM;
MethodInfo m33968_MI = 
{
	"Clear", NULL, &t6567_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33968_GM};
extern Il2CppType t2064_0_0_0;
static ParameterInfo t6567_m33969_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2064_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33969_GM;
MethodInfo m33969_MI = 
{
	"Contains", NULL, &t6567_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6567_m33969_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33969_GM};
extern Il2CppType t3708_0_0_0;
extern Il2CppType t3708_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6567_m33970_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3708_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33970_GM;
MethodInfo m33970_MI = 
{
	"CopyTo", NULL, &t6567_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6567_m33970_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33970_GM};
extern Il2CppType t2064_0_0_0;
static ParameterInfo t6567_m33971_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2064_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33971_GM;
MethodInfo m33971_MI = 
{
	"Remove", NULL, &t6567_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6567_m33971_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33971_GM};
static MethodInfo* t6567_MIs[] =
{
	&m33965_MI,
	&m33966_MI,
	&m33967_MI,
	&m33968_MI,
	&m33969_MI,
	&m33970_MI,
	&m33971_MI,
	NULL
};
extern TypeInfo t6569_TI;
static TypeInfo* t6567_ITIs[] = 
{
	&t603_TI,
	&t6569_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6567_0_0_0;
extern Il2CppType t6567_1_0_0;
struct t6567;
extern Il2CppGenericClass t6567_GC;
TypeInfo t6567_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6567_MIs, t6567_PIs, NULL, NULL, NULL, NULL, NULL, &t6567_TI, t6567_ITIs, NULL, &EmptyCustomAttributesCache, &t6567_TI, &t6567_0_0_0, &t6567_1_0_0, NULL, &t6567_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Security.Policy.IIdentityPermissionFactory>
extern Il2CppType t5014_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33972_GM;
MethodInfo m33972_MI = 
{
	"GetEnumerator", NULL, &t6569_TI, &t5014_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33972_GM};
static MethodInfo* t6569_MIs[] =
{
	&m33972_MI,
	NULL
};
static TypeInfo* t6569_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6569_0_0_0;
extern Il2CppType t6569_1_0_0;
struct t6569;
extern Il2CppGenericClass t6569_GC;
TypeInfo t6569_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6569_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6569_TI, t6569_ITIs, NULL, &EmptyCustomAttributesCache, &t6569_TI, &t6569_0_0_0, &t6569_1_0_0, NULL, &t6569_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5014_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Security.Policy.IIdentityPermissionFactory>
extern MethodInfo m33973_MI;
static PropertyInfo t5014____Current_PropertyInfo = 
{
	&t5014_TI, "Current", &m33973_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5014_PIs[] =
{
	&t5014____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2064_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33973_GM;
MethodInfo m33973_MI = 
{
	"get_Current", NULL, &t5014_TI, &t2064_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33973_GM};
static MethodInfo* t5014_MIs[] =
{
	&m33973_MI,
	NULL
};
static TypeInfo* t5014_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5014_0_0_0;
extern Il2CppType t5014_1_0_0;
struct t5014;
extern Il2CppGenericClass t5014_GC;
TypeInfo t5014_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t5014_MIs, t5014_PIs, NULL, NULL, NULL, NULL, NULL, &t5014_TI, t5014_ITIs, NULL, &EmptyCustomAttributesCache, &t5014_TI, &t5014_0_0_0, &t5014_1_0_0, NULL, &t5014_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3461.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3461_TI;
#include "t3461MD.h"

extern TypeInfo t2064_TI;
extern MethodInfo m19179_MI;
extern MethodInfo m26018_MI;
struct t20;
#define m26018(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Security.Policy.IIdentityPermissionFactory>
extern Il2CppType t20_0_0_1;
FieldInfo t3461_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3461_TI, offsetof(t3461, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3461_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3461_TI, offsetof(t3461, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3461_FIs[] =
{
	&t3461_f0_FieldInfo,
	&t3461_f1_FieldInfo,
	NULL
};
extern MethodInfo m19176_MI;
static PropertyInfo t3461____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3461_TI, "System.Collections.IEnumerator.Current", &m19176_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3461____Current_PropertyInfo = 
{
	&t3461_TI, "Current", &m19179_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3461_PIs[] =
{
	&t3461____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3461____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3461_m19175_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19175_GM;
MethodInfo m19175_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3461_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3461_m19175_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19175_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19176_GM;
MethodInfo m19176_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3461_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19176_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19177_GM;
MethodInfo m19177_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3461_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19177_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19178_GM;
MethodInfo m19178_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3461_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19178_GM};
extern Il2CppType t2064_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19179_GM;
MethodInfo m19179_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3461_TI, &t2064_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19179_GM};
static MethodInfo* t3461_MIs[] =
{
	&m19175_MI,
	&m19176_MI,
	&m19177_MI,
	&m19178_MI,
	&m19179_MI,
	NULL
};
extern MethodInfo m19178_MI;
extern MethodInfo m19177_MI;
static MethodInfo* t3461_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19176_MI,
	&m19178_MI,
	&m19177_MI,
	&m19179_MI,
};
static TypeInfo* t3461_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t5014_TI,
};
static Il2CppInterfaceOffsetPair t3461_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t5014_TI, 7},
};
extern TypeInfo t2064_TI;
static Il2CppRGCTXData t3461_RGCTXData[3] = 
{
	&m19179_MI/* Method Usage */,
	&t2064_TI/* Class Usage */,
	&m26018_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3461_0_0_0;
extern Il2CppType t3461_1_0_0;
extern Il2CppGenericClass t3461_GC;
TypeInfo t3461_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3461_MIs, t3461_PIs, t3461_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3461_TI, t3461_ITIs, t3461_VT, &EmptyCustomAttributesCache, &t3461_TI, &t3461_0_0_0, &t3461_1_0_0, t3461_IOs, &t3461_GC, NULL, NULL, NULL, t3461_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3461)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6568_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Security.Policy.IIdentityPermissionFactory>
extern MethodInfo m33974_MI;
extern MethodInfo m33975_MI;
static PropertyInfo t6568____Item_PropertyInfo = 
{
	&t6568_TI, "Item", &m33974_MI, &m33975_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6568_PIs[] =
{
	&t6568____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2064_0_0_0;
static ParameterInfo t6568_m33976_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2064_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33976_GM;
MethodInfo m33976_MI = 
{
	"IndexOf", NULL, &t6568_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6568_m33976_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33976_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2064_0_0_0;
static ParameterInfo t6568_m33977_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t2064_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33977_GM;
MethodInfo m33977_MI = 
{
	"Insert", NULL, &t6568_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6568_m33977_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33977_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6568_m33978_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33978_GM;
MethodInfo m33978_MI = 
{
	"RemoveAt", NULL, &t6568_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6568_m33978_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33978_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6568_m33974_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2064_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33974_GM;
MethodInfo m33974_MI = 
{
	"get_Item", NULL, &t6568_TI, &t2064_0_0_0, RuntimeInvoker_t29_t44, t6568_m33974_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33974_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2064_0_0_0;
static ParameterInfo t6568_m33975_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2064_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33975_GM;
MethodInfo m33975_MI = 
{
	"set_Item", NULL, &t6568_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6568_m33975_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33975_GM};
static MethodInfo* t6568_MIs[] =
{
	&m33976_MI,
	&m33977_MI,
	&m33978_MI,
	&m33974_MI,
	&m33975_MI,
	NULL
};
static TypeInfo* t6568_ITIs[] = 
{
	&t603_TI,
	&t6567_TI,
	&t6569_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6568_0_0_0;
extern Il2CppType t6568_1_0_0;
struct t6568;
extern Il2CppGenericClass t6568_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6568_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6568_MIs, t6568_PIs, NULL, NULL, NULL, NULL, NULL, &t6568_TI, t6568_ITIs, NULL, &t1908__CustomAttributeCache, &t6568_TI, &t6568_0_0_0, &t6568_1_0_0, NULL, &t6568_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2061.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2061_TI;
#include "t2061MD.h"

#include "t305.h"
#include "t3469.h"
#include "t3466.h"
#include "t3467.h"
#include "t338.h"
#include "t3473.h"
#include "t3468.h"
extern TypeInfo t44_TI;
extern TypeInfo t21_TI;
extern TypeInfo t305_TI;
extern TypeInfo t585_TI;
extern TypeInfo t1649_TI;
extern TypeInfo t915_TI;
extern TypeInfo t3462_TI;
extern TypeInfo t3469_TI;
extern TypeInfo t40_TI;
extern TypeInfo t3466_TI;
extern TypeInfo t338_TI;
extern TypeInfo t3467_TI;
extern TypeInfo t3473_TI;
#include "t305MD.h"
#include "t915MD.h"
#include "t29MD.h"
#include "t602MD.h"
#include "t3466MD.h"
#include "t338MD.h"
#include "t3467MD.h"
#include "t3469MD.h"
#include "t3473MD.h"
extern MethodInfo m19226_MI;
extern MethodInfo m19227_MI;
extern MethodInfo m1935_MI;
extern MethodInfo m4198_MI;
extern MethodInfo m26029_MI;
extern MethodInfo m3975_MI;
extern MethodInfo m19212_MI;
extern MethodInfo m1331_MI;
extern MethodInfo m19209_MI;
extern MethodInfo m5952_MI;
extern MethodInfo m19197_MI;
extern MethodInfo m19204_MI;
extern MethodInfo m19210_MI;
extern MethodInfo m19213_MI;
extern MethodInfo m19215_MI;
extern MethodInfo m19198_MI;
extern MethodInfo m19223_MI;
extern MethodInfo m5139_MI;
extern MethodInfo m19224_MI;
extern MethodInfo m33947_MI;
extern MethodInfo m33949_MI;
extern MethodInfo m3972_MI;
extern MethodInfo m1428_MI;
extern MethodInfo m19214_MI;
extern MethodInfo m19199_MI;
extern MethodInfo m19200_MI;
extern MethodInfo m19234_MI;
extern MethodInfo m5112_MI;
extern MethodInfo m26031_MI;
extern MethodInfo m19207_MI;
extern MethodInfo m19208_MI;
extern MethodInfo m2950_MI;
extern MethodInfo m19309_MI;
extern MethodInfo m19228_MI;
extern MethodInfo m19211_MI;
extern MethodInfo m19217_MI;
extern MethodInfo m5171_MI;
extern MethodInfo m19315_MI;
extern MethodInfo m26033_MI;
extern MethodInfo m26041_MI;
extern MethodInfo m5951_MI;
struct t20;
struct t20;
 void m19788_gshared (t29 * __this, t316** p0, int32_t p1, MethodInfo* method);
#define m19788(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
#define m26029(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
struct t20;
#include "t3471.h"
struct t20;
#include "t2001.h"
 int32_t m10120_gshared (t29 * __this, t316* p0, t29 * p1, int32_t p2, int32_t p3, MethodInfo* method);
#define m10120(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
#define m26031(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
struct t20;
struct t20;
 void m19857_gshared (t29 * __this, t316* p0, int32_t p1, int32_t p2, t29* p3, MethodInfo* method);
#define m19857(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
#define m26033(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
struct t20;
#include "t295.h"
struct t20;
#include "t2181.h"
 void m19964_gshared (t29 * __this, t316* p0, int32_t p1, t2181 * p2, MethodInfo* method);
#define m19964(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)
#define m26041(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)


 t3469  m19209 (t2061 * __this, MethodInfo* method){
	{
		t3469  L_0 = {0};
		m19228(&L_0, __this, &m19228_MI);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.List`1<System.Security.Policy.StrongName>
extern Il2CppType t44_0_0_32849;
FieldInfo t2061_f0_FieldInfo = 
{
	"DefaultCapacity", &t44_0_0_32849, &t2061_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t3462_0_0_1;
FieldInfo t2061_f1_FieldInfo = 
{
	"_items", &t3462_0_0_1, &t2061_TI, offsetof(t2061, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2061_f2_FieldInfo = 
{
	"_size", &t44_0_0_1, &t2061_TI, offsetof(t2061, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2061_f3_FieldInfo = 
{
	"_version", &t44_0_0_1, &t2061_TI, offsetof(t2061, f3), &EmptyCustomAttributesCache};
extern Il2CppType t3462_0_0_49;
FieldInfo t2061_f4_FieldInfo = 
{
	"EmptyArray", &t3462_0_0_49, &t2061_TI, offsetof(t2061_SFs, f4), &EmptyCustomAttributesCache};
static FieldInfo* t2061_FIs[] =
{
	&t2061_f0_FieldInfo,
	&t2061_f1_FieldInfo,
	&t2061_f2_FieldInfo,
	&t2061_f3_FieldInfo,
	&t2061_f4_FieldInfo,
	NULL
};
static const int32_t t2061_f0_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t2061_f0_DefaultValue = 
{
	&t2061_f0_FieldInfo, { (char*)&t2061_f0_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t2061_FDVs[] = 
{
	&t2061_f0_DefaultValue,
	NULL
};
extern MethodInfo m19190_MI;
static PropertyInfo t2061____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2061_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m19190_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m19191_MI;
static PropertyInfo t2061____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2061_TI, "System.Collections.ICollection.IsSynchronized", &m19191_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m19192_MI;
static PropertyInfo t2061____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2061_TI, "System.Collections.ICollection.SyncRoot", &m19192_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m19193_MI;
static PropertyInfo t2061____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2061_TI, "System.Collections.IList.IsFixedSize", &m19193_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m19194_MI;
static PropertyInfo t2061____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2061_TI, "System.Collections.IList.IsReadOnly", &m19194_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m19195_MI;
extern MethodInfo m19196_MI;
static PropertyInfo t2061____System_Collections_IList_Item_PropertyInfo = 
{
	&t2061_TI, "System.Collections.IList.Item", &m19195_MI, &m19196_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2061____Capacity_PropertyInfo = 
{
	&t2061_TI, "Capacity", &m19223_MI, &m19224_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m19225_MI;
static PropertyInfo t2061____Count_PropertyInfo = 
{
	&t2061_TI, "Count", &m19225_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2061____Item_PropertyInfo = 
{
	&t2061_TI, "Item", &m19226_MI, &m19227_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2061_PIs[] =
{
	&t2061____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2061____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2061____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2061____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2061____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2061____System_Collections_IList_Item_PropertyInfo,
	&t2061____Capacity_PropertyInfo,
	&t2061____Count_PropertyInfo,
	&t2061____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19180_GM;
MethodInfo m19180_MI = 
{
	".ctor", (methodPointerType)&m10537_gshared, &t2061_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19180_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2061_m10242_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10242_GM;
MethodInfo m10242_MI = 
{
	".ctor", (methodPointerType)&m10539_gshared, &t2061_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2061_m10242_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10242_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19181_GM;
MethodInfo m19181_MI = 
{
	".cctor", (methodPointerType)&m10541_gshared, &t2061_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19181_GM};
extern Il2CppType t3463_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19182_GM;
MethodInfo m19182_MI = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator", (methodPointerType)&m10543_gshared, &t2061_TI, &t3463_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 27, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19182_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2061_m19183_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19183_GM;
MethodInfo m19183_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10545_gshared, &t2061_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2061_m19183_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19183_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19184_GM;
MethodInfo m19184_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10547_gshared, &t2061_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19184_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2061_m19185_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19185_GM;
MethodInfo m19185_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10549_gshared, &t2061_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2061_m19185_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19185_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2061_m19186_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19186_GM;
MethodInfo m19186_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10551_gshared, &t2061_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2061_m19186_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19186_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2061_m19187_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19187_GM;
MethodInfo m19187_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10553_gshared, &t2061_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2061_m19187_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19187_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2061_m19188_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19188_GM;
MethodInfo m19188_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10555_gshared, &t2061_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2061_m19188_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19188_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2061_m19189_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19189_GM;
MethodInfo m19189_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10557_gshared, &t2061_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2061_m19189_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19189_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19190_GM;
MethodInfo m19190_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10559_gshared, &t2061_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19190_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19191_GM;
MethodInfo m19191_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10561_gshared, &t2061_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19191_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19192_GM;
MethodInfo m19192_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10563_gshared, &t2061_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19192_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19193_GM;
MethodInfo m19193_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10565_gshared, &t2061_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19193_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19194_GM;
MethodInfo m19194_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10567_gshared, &t2061_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19194_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2061_m19195_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19195_GM;
MethodInfo m19195_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10569_gshared, &t2061_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2061_m19195_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19195_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2061_m19196_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19196_GM;
MethodInfo m19196_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10571_gshared, &t2061_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2061_m19196_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19196_GM};
extern Il2CppType t1567_0_0_0;
static ParameterInfo t2061_m19197_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19197_GM;
MethodInfo m19197_MI = 
{
	"Add", (methodPointerType)&m10573_gshared, &t2061_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2061_m19197_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19197_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2061_m19198_ParameterInfos[] = 
{
	{"newCount", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19198_GM;
MethodInfo m19198_MI = 
{
	"GrowIfNeeded", (methodPointerType)&m10575_gshared, &t2061_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2061_m19198_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19198_GM};
extern Il2CppType t3464_0_0_0;
static ParameterInfo t2061_m19199_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t3464_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19199_GM;
MethodInfo m19199_MI = 
{
	"AddCollection", (methodPointerType)&m10577_gshared, &t2061_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2061_m19199_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19199_GM};
extern Il2CppType t3465_0_0_0;
static ParameterInfo t2061_m19200_ParameterInfos[] = 
{
	{"enumerable", 0, 134217728, &EmptyCustomAttributesCache, &t3465_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19200_GM;
MethodInfo m19200_MI = 
{
	"AddEnumerable", (methodPointerType)&m10579_gshared, &t2061_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2061_m19200_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19200_GM};
extern Il2CppType t3465_0_0_0;
static ParameterInfo t2061_m19201_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t3465_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19201_GM;
MethodInfo m19201_MI = 
{
	"AddRange", (methodPointerType)&m10581_gshared, &t2061_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2061_m19201_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19201_GM};
extern Il2CppType t3466_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19202_GM;
MethodInfo m19202_MI = 
{
	"AsReadOnly", (methodPointerType)&m10583_gshared, &t2061_TI, &t3466_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19202_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19203_GM;
MethodInfo m19203_MI = 
{
	"Clear", (methodPointerType)&m10585_gshared, &t2061_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19203_GM};
extern Il2CppType t1567_0_0_0;
static ParameterInfo t2061_m19204_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19204_GM;
MethodInfo m19204_MI = 
{
	"Contains", (methodPointerType)&m10587_gshared, &t2061_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2061_m19204_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19204_GM};
extern Il2CppType t3462_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2061_m19205_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3462_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19205_GM;
MethodInfo m19205_MI = 
{
	"CopyTo", (methodPointerType)&m10589_gshared, &t2061_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2061_m19205_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19205_GM};
extern Il2CppType t3467_0_0_0;
extern Il2CppType t3467_0_0_0;
static ParameterInfo t2061_m19206_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t3467_0_0_0},
};
extern Il2CppType t1567_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19206_GM;
MethodInfo m19206_MI = 
{
	"Find", (methodPointerType)&m10591_gshared, &t2061_TI, &t1567_0_0_0, RuntimeInvoker_t29_t29, t2061_m19206_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19206_GM};
extern Il2CppType t3467_0_0_0;
static ParameterInfo t2061_m19207_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t3467_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19207_GM;
MethodInfo m19207_MI = 
{
	"CheckMatch", (methodPointerType)&m10593_gshared, &t2061_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2061_m19207_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19207_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t3467_0_0_0;
static ParameterInfo t2061_m19208_ParameterInfos[] = 
{
	{"startIndex", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"count", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"match", 2, 134217728, &EmptyCustomAttributesCache, &t3467_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19208_GM;
MethodInfo m19208_MI = 
{
	"GetIndex", (methodPointerType)&m10595_gshared, &t2061_TI, &t44_0_0_0, RuntimeInvoker_t44_t44_t44_t29, t2061_m19208_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m19208_GM};
extern Il2CppType t3469_0_0_0;
extern void* RuntimeInvoker_t3469 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19209_GM;
MethodInfo m19209_MI = 
{
	"GetEnumerator", (methodPointerType)&m19209, &t2061_TI, &t3469_0_0_0, RuntimeInvoker_t3469, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19209_GM};
extern Il2CppType t1567_0_0_0;
static ParameterInfo t2061_m19210_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19210_GM;
MethodInfo m19210_MI = 
{
	"IndexOf", (methodPointerType)&m10598_gshared, &t2061_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2061_m19210_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19210_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2061_m19211_ParameterInfos[] = 
{
	{"start", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"delta", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19211_GM;
MethodInfo m19211_MI = 
{
	"Shift", (methodPointerType)&m10600_gshared, &t2061_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t2061_m19211_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19211_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2061_m19212_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19212_GM;
MethodInfo m19212_MI = 
{
	"CheckIndex", (methodPointerType)&m10602_gshared, &t2061_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2061_m19212_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19212_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1567_0_0_0;
static ParameterInfo t2061_m19213_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19213_GM;
MethodInfo m19213_MI = 
{
	"Insert", (methodPointerType)&m10604_gshared, &t2061_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2061_m19213_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19213_GM};
extern Il2CppType t3465_0_0_0;
static ParameterInfo t2061_m19214_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t3465_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19214_GM;
MethodInfo m19214_MI = 
{
	"CheckCollection", (methodPointerType)&m10606_gshared, &t2061_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2061_m19214_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19214_GM};
extern Il2CppType t1567_0_0_0;
static ParameterInfo t2061_m19215_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19215_GM;
MethodInfo m19215_MI = 
{
	"Remove", (methodPointerType)&m10608_gshared, &t2061_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2061_m19215_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19215_GM};
extern Il2CppType t3467_0_0_0;
static ParameterInfo t2061_m19216_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t3467_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19216_GM;
MethodInfo m19216_MI = 
{
	"RemoveAll", (methodPointerType)&m10610_gshared, &t2061_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2061_m19216_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19216_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2061_m19217_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19217_GM;
MethodInfo m19217_MI = 
{
	"RemoveAt", (methodPointerType)&m10611_gshared, &t2061_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2061_m19217_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19217_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19218_GM;
MethodInfo m19218_MI = 
{
	"Reverse", (methodPointerType)&m10613_gshared, &t2061_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19218_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19219_GM;
MethodInfo m19219_MI = 
{
	"Sort", (methodPointerType)&m10615_gshared, &t2061_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19219_GM};
extern Il2CppType t3468_0_0_0;
extern Il2CppType t3468_0_0_0;
static ParameterInfo t2061_m19220_ParameterInfos[] = 
{
	{"comparison", 0, 134217728, &EmptyCustomAttributesCache, &t3468_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19220_GM;
MethodInfo m19220_MI = 
{
	"Sort", (methodPointerType)&m10617_gshared, &t2061_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2061_m19220_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19220_GM};
extern Il2CppType t3462_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19221_GM;
MethodInfo m19221_MI = 
{
	"ToArray", (methodPointerType)&m10619_gshared, &t2061_TI, &t3462_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19221_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19222_GM;
MethodInfo m19222_MI = 
{
	"TrimExcess", (methodPointerType)&m10621_gshared, &t2061_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19222_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19223_GM;
MethodInfo m19223_MI = 
{
	"get_Capacity", (methodPointerType)&m10623_gshared, &t2061_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19223_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2061_m19224_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19224_GM;
MethodInfo m19224_MI = 
{
	"set_Capacity", (methodPointerType)&m10625_gshared, &t2061_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2061_m19224_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19224_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19225_GM;
MethodInfo m19225_MI = 
{
	"get_Count", (methodPointerType)&m10626_gshared, &t2061_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19225_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2061_m19226_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1567_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19226_GM;
MethodInfo m19226_MI = 
{
	"get_Item", (methodPointerType)&m10627_gshared, &t2061_TI, &t1567_0_0_0, RuntimeInvoker_t29_t44, t2061_m19226_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19226_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1567_0_0_0;
static ParameterInfo t2061_m19227_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19227_GM;
MethodInfo m19227_MI = 
{
	"set_Item", (methodPointerType)&m10629_gshared, &t2061_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2061_m19227_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 32, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19227_GM};
static MethodInfo* t2061_MIs[] =
{
	&m19180_MI,
	&m10242_MI,
	&m19181_MI,
	&m19182_MI,
	&m19183_MI,
	&m19184_MI,
	&m19185_MI,
	&m19186_MI,
	&m19187_MI,
	&m19188_MI,
	&m19189_MI,
	&m19190_MI,
	&m19191_MI,
	&m19192_MI,
	&m19193_MI,
	&m19194_MI,
	&m19195_MI,
	&m19196_MI,
	&m19197_MI,
	&m19198_MI,
	&m19199_MI,
	&m19200_MI,
	&m19201_MI,
	&m19202_MI,
	&m19203_MI,
	&m19204_MI,
	&m19205_MI,
	&m19206_MI,
	&m19207_MI,
	&m19208_MI,
	&m19209_MI,
	&m19210_MI,
	&m19211_MI,
	&m19212_MI,
	&m19213_MI,
	&m19214_MI,
	&m19215_MI,
	&m19216_MI,
	&m19217_MI,
	&m19218_MI,
	&m19219_MI,
	&m19220_MI,
	&m19221_MI,
	&m19222_MI,
	&m19223_MI,
	&m19224_MI,
	&m19225_MI,
	&m19226_MI,
	&m19227_MI,
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m1332_MI;
extern MethodInfo m19184_MI;
extern MethodInfo m19183_MI;
extern MethodInfo m19185_MI;
extern MethodInfo m19203_MI;
extern MethodInfo m19186_MI;
extern MethodInfo m19187_MI;
extern MethodInfo m19188_MI;
extern MethodInfo m19189_MI;
extern MethodInfo m19205_MI;
extern MethodInfo m19182_MI;
static MethodInfo* t2061_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m19184_MI,
	&m19225_MI,
	&m19191_MI,
	&m19192_MI,
	&m19183_MI,
	&m19193_MI,
	&m19194_MI,
	&m19195_MI,
	&m19196_MI,
	&m19185_MI,
	&m19203_MI,
	&m19186_MI,
	&m19187_MI,
	&m19188_MI,
	&m19189_MI,
	&m19217_MI,
	&m19225_MI,
	&m19190_MI,
	&m19197_MI,
	&m19203_MI,
	&m19204_MI,
	&m19205_MI,
	&m19215_MI,
	&m19182_MI,
	&m19210_MI,
	&m19213_MI,
	&m19217_MI,
	&m19226_MI,
	&m19227_MI,
};
extern TypeInfo t674_TI;
extern TypeInfo t868_TI;
static TypeInfo* t2061_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t3464_TI,
	&t3465_TI,
	&t1564_TI,
};
static Il2CppInterfaceOffsetPair t2061_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t3464_TI, 20},
	{ &t3465_TI, 27},
	{ &t1564_TI, 28},
};
extern TypeInfo t2061_TI;
extern TypeInfo t3462_TI;
extern TypeInfo t3469_TI;
extern TypeInfo t1567_TI;
extern TypeInfo t3464_TI;
extern TypeInfo t3466_TI;
static Il2CppRGCTXData t2061_RGCTXData[37] = 
{
	&t2061_TI/* Static Usage */,
	&t3462_TI/* Array Usage */,
	&m19209_MI/* Method Usage */,
	&t3469_TI/* Class Usage */,
	&t1567_TI/* Class Usage */,
	&m19197_MI/* Method Usage */,
	&m19204_MI/* Method Usage */,
	&m19210_MI/* Method Usage */,
	&m19212_MI/* Method Usage */,
	&m19213_MI/* Method Usage */,
	&m19215_MI/* Method Usage */,
	&m19226_MI/* Method Usage */,
	&m19227_MI/* Method Usage */,
	&m19198_MI/* Method Usage */,
	&m19223_MI/* Method Usage */,
	&m19224_MI/* Method Usage */,
	&m33942_MI/* Method Usage */,
	&m33947_MI/* Method Usage */,
	&m33949_MI/* Method Usage */,
	&m33950_MI/* Method Usage */,
	&m19214_MI/* Method Usage */,
	&t3464_TI/* Class Usage */,
	&m19199_MI/* Method Usage */,
	&m19200_MI/* Method Usage */,
	&t3466_TI/* Class Usage */,
	&m19234_MI/* Method Usage */,
	&m26031_MI/* Method Usage */,
	&m19207_MI/* Method Usage */,
	&m19208_MI/* Method Usage */,
	&m19309_MI/* Method Usage */,
	&m19228_MI/* Method Usage */,
	&m19211_MI/* Method Usage */,
	&m19217_MI/* Method Usage */,
	&m19315_MI/* Method Usage */,
	&m26033_MI/* Method Usage */,
	&m26041_MI/* Method Usage */,
	&m26029_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2061_0_0_0;
extern Il2CppType t2061_1_0_0;
extern TypeInfo t29_TI;
struct t2061;
extern Il2CppGenericClass t2061_GC;
extern CustomAttributesCache t1261__CustomAttributeCache;
TypeInfo t2061_TI = 
{
	&g_mscorlib_dll_Image, NULL, "List`1", "System.Collections.Generic", t2061_MIs, t2061_PIs, t2061_FIs, NULL, &t29_TI, NULL, NULL, &t2061_TI, t2061_ITIs, t2061_VT, &t1261__CustomAttributeCache, &t2061_TI, &t2061_0_0_0, &t2061_1_0_0, t2061_IOs, &t2061_GC, NULL, t2061_FDVs, NULL, t2061_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2061), 0, -1, sizeof(t2061_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, true, false, false, 49, 9, 5, 0, 0, 33, 6, 6};
#ifndef _MSC_VER
#else
#endif

#include "t42.h"
#include "t1101.h"
extern TypeInfo t42_TI;
extern TypeInfo t7_TI;
extern TypeInfo t1101_TI;
#include "t42MD.h"
#include "t1101MD.h"
extern MethodInfo m19231_MI;
extern MethodInfo m3974_MI;
extern MethodInfo m1430_MI;
extern MethodInfo m2998_MI;
extern MethodInfo m5150_MI;


// Metadata Definition System.Collections.Generic.List`1/Enumerator<System.Security.Policy.StrongName>
extern Il2CppType t2061_0_0_1;
FieldInfo t3469_f0_FieldInfo = 
{
	"l", &t2061_0_0_1, &t3469_TI, offsetof(t3469, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3469_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t3469_TI, offsetof(t3469, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3469_f2_FieldInfo = 
{
	"ver", &t44_0_0_1, &t3469_TI, offsetof(t3469, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1567_0_0_1;
FieldInfo t3469_f3_FieldInfo = 
{
	"current", &t1567_0_0_1, &t3469_TI, offsetof(t3469, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3469_FIs[] =
{
	&t3469_f0_FieldInfo,
	&t3469_f1_FieldInfo,
	&t3469_f2_FieldInfo,
	&t3469_f3_FieldInfo,
	NULL
};
extern MethodInfo m19229_MI;
static PropertyInfo t3469____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3469_TI, "System.Collections.IEnumerator.Current", &m19229_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m19233_MI;
static PropertyInfo t3469____Current_PropertyInfo = 
{
	&t3469_TI, "Current", &m19233_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3469_PIs[] =
{
	&t3469____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3469____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2061_0_0_0;
static ParameterInfo t3469_m19228_ParameterInfos[] = 
{
	{"l", 0, 134217728, &EmptyCustomAttributesCache, &t2061_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19228_GM;
MethodInfo m19228_MI = 
{
	".ctor", (methodPointerType)&m10631_gshared, &t3469_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3469_m19228_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19228_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19229_GM;
MethodInfo m19229_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10632_gshared, &t3469_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19229_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19230_GM;
MethodInfo m19230_MI = 
{
	"Dispose", (methodPointerType)&m10633_gshared, &t3469_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19230_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19231_GM;
MethodInfo m19231_MI = 
{
	"VerifyState", (methodPointerType)&m10634_gshared, &t3469_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19231_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19232_GM;
MethodInfo m19232_MI = 
{
	"MoveNext", (methodPointerType)&m10635_gshared, &t3469_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19232_GM};
extern Il2CppType t1567_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19233_GM;
MethodInfo m19233_MI = 
{
	"get_Current", (methodPointerType)&m10636_gshared, &t3469_TI, &t1567_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19233_GM};
static MethodInfo* t3469_MIs[] =
{
	&m19228_MI,
	&m19229_MI,
	&m19230_MI,
	&m19231_MI,
	&m19232_MI,
	&m19233_MI,
	NULL
};
extern MethodInfo m19232_MI;
extern MethodInfo m19230_MI;
static MethodInfo* t3469_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19229_MI,
	&m19232_MI,
	&m19230_MI,
	&m19233_MI,
};
static TypeInfo* t3469_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3463_TI,
};
static Il2CppInterfaceOffsetPair t3469_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3463_TI, 7},
};
extern TypeInfo t1567_TI;
extern TypeInfo t3469_TI;
static Il2CppRGCTXData t3469_RGCTXData[3] = 
{
	&m19231_MI/* Method Usage */,
	&t1567_TI/* Class Usage */,
	&t3469_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3469_0_0_0;
extern Il2CppType t3469_1_0_0;
extern Il2CppGenericClass t3469_GC;
extern TypeInfo t1261_TI;
TypeInfo t3469_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t3469_MIs, t3469_PIs, t3469_FIs, NULL, &t110_TI, NULL, &t1261_TI, &t3469_TI, t3469_ITIs, t3469_VT, &EmptyCustomAttributesCache, &t3469_TI, &t3469_0_0_0, &t3469_1_0_0, t3469_IOs, &t3469_GC, NULL, NULL, NULL, t3469_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3469)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 4, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif

#include "t345.h"
extern TypeInfo t345_TI;
#include "t345MD.h"
#include "t3470MD.h"
extern MethodInfo m19263_MI;
extern MethodInfo m1516_MI;
extern MethodInfo m3960_MI;
extern MethodInfo m4154_MI;
extern MethodInfo m19295_MI;
extern MethodInfo m33946_MI;
extern MethodInfo m33939_MI;


// Metadata Definition System.Collections.ObjectModel.ReadOnlyCollection`1<System.Security.Policy.StrongName>
extern Il2CppType t1564_0_0_1;
FieldInfo t3466_f0_FieldInfo = 
{
	"list", &t1564_0_0_1, &t3466_TI, offsetof(t3466, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3466_FIs[] =
{
	&t3466_f0_FieldInfo,
	NULL
};
extern MethodInfo m19240_MI;
extern MethodInfo m19241_MI;
static PropertyInfo t3466____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo = 
{
	&t3466_TI, "System.Collections.Generic.IList<T>.Item", &m19240_MI, &m19241_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m19242_MI;
static PropertyInfo t3466____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t3466_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m19242_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m19252_MI;
static PropertyInfo t3466____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t3466_TI, "System.Collections.ICollection.IsSynchronized", &m19252_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m19253_MI;
static PropertyInfo t3466____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t3466_TI, "System.Collections.ICollection.SyncRoot", &m19253_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m19254_MI;
static PropertyInfo t3466____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t3466_TI, "System.Collections.IList.IsFixedSize", &m19254_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m19255_MI;
static PropertyInfo t3466____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t3466_TI, "System.Collections.IList.IsReadOnly", &m19255_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m19256_MI;
extern MethodInfo m19257_MI;
static PropertyInfo t3466____System_Collections_IList_Item_PropertyInfo = 
{
	&t3466_TI, "System.Collections.IList.Item", &m19256_MI, &m19257_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m19262_MI;
static PropertyInfo t3466____Count_PropertyInfo = 
{
	&t3466_TI, "Count", &m19262_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3466____Item_PropertyInfo = 
{
	&t3466_TI, "Item", &m19263_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3466_PIs[] =
{
	&t3466____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo,
	&t3466____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t3466____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t3466____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t3466____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t3466____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t3466____System_Collections_IList_Item_PropertyInfo,
	&t3466____Count_PropertyInfo,
	&t3466____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1564_0_0_0;
static ParameterInfo t3466_m19234_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t1564_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19234_GM;
MethodInfo m19234_MI = 
{
	".ctor", (methodPointerType)&m10637_gshared, &t3466_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3466_m19234_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19234_GM};
extern Il2CppType t1567_0_0_0;
static ParameterInfo t3466_m19235_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19235_GM;
MethodInfo m19235_MI = 
{
	"System.Collections.Generic.ICollection<T>.Add", (methodPointerType)&m10638_gshared, &t3466_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3466_m19235_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19235_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19236_GM;
MethodInfo m19236_MI = 
{
	"System.Collections.Generic.ICollection<T>.Clear", (methodPointerType)&m10639_gshared, &t3466_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19236_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1567_0_0_0;
static ParameterInfo t3466_m19237_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19237_GM;
MethodInfo m19237_MI = 
{
	"System.Collections.Generic.IList<T>.Insert", (methodPointerType)&m10640_gshared, &t3466_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3466_m19237_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19237_GM};
extern Il2CppType t1567_0_0_0;
static ParameterInfo t3466_m19238_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19238_GM;
MethodInfo m19238_MI = 
{
	"System.Collections.Generic.ICollection<T>.Remove", (methodPointerType)&m10641_gshared, &t3466_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3466_m19238_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19238_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3466_m19239_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19239_GM;
MethodInfo m19239_MI = 
{
	"System.Collections.Generic.IList<T>.RemoveAt", (methodPointerType)&m10642_gshared, &t3466_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t3466_m19239_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19239_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3466_m19240_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1567_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19240_GM;
MethodInfo m19240_MI = 
{
	"System.Collections.Generic.IList<T>.get_Item", (methodPointerType)&m10643_gshared, &t3466_TI, &t1567_0_0_0, RuntimeInvoker_t29_t44, t3466_m19240_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19240_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1567_0_0_0;
static ParameterInfo t3466_m19241_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19241_GM;
MethodInfo m19241_MI = 
{
	"System.Collections.Generic.IList<T>.set_Item", (methodPointerType)&m10644_gshared, &t3466_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3466_m19241_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19241_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19242_GM;
MethodInfo m19242_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10645_gshared, &t3466_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19242_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3466_m19243_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19243_GM;
MethodInfo m19243_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10646_gshared, &t3466_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3466_m19243_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19243_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19244_GM;
MethodInfo m19244_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10647_gshared, &t3466_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19244_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3466_m19245_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19245_GM;
MethodInfo m19245_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10648_gshared, &t3466_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3466_m19245_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19245_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19246_GM;
MethodInfo m19246_MI = 
{
	"System.Collections.IList.Clear", (methodPointerType)&m10649_gshared, &t3466_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 14, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19246_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3466_m19247_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19247_GM;
MethodInfo m19247_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10650_gshared, &t3466_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3466_m19247_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19247_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3466_m19248_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19248_GM;
MethodInfo m19248_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10651_gshared, &t3466_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3466_m19248_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19248_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3466_m19249_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19249_GM;
MethodInfo m19249_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10652_gshared, &t3466_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3466_m19249_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19249_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3466_m19250_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19250_GM;
MethodInfo m19250_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10653_gshared, &t3466_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3466_m19250_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19250_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3466_m19251_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19251_GM;
MethodInfo m19251_MI = 
{
	"System.Collections.IList.RemoveAt", (methodPointerType)&m10654_gshared, &t3466_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t3466_m19251_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19251_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19252_GM;
MethodInfo m19252_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10655_gshared, &t3466_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19252_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19253_GM;
MethodInfo m19253_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10656_gshared, &t3466_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19253_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19254_GM;
MethodInfo m19254_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10657_gshared, &t3466_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19254_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19255_GM;
MethodInfo m19255_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10658_gshared, &t3466_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19255_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3466_m19256_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19256_GM;
MethodInfo m19256_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10659_gshared, &t3466_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t3466_m19256_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19256_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3466_m19257_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19257_GM;
MethodInfo m19257_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10660_gshared, &t3466_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3466_m19257_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19257_GM};
extern Il2CppType t1567_0_0_0;
static ParameterInfo t3466_m19258_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19258_GM;
MethodInfo m19258_MI = 
{
	"Contains", (methodPointerType)&m10661_gshared, &t3466_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3466_m19258_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19258_GM};
extern Il2CppType t3462_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3466_m19259_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3462_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19259_GM;
MethodInfo m19259_MI = 
{
	"CopyTo", (methodPointerType)&m10662_gshared, &t3466_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3466_m19259_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19259_GM};
extern Il2CppType t3463_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19260_GM;
MethodInfo m19260_MI = 
{
	"GetEnumerator", (methodPointerType)&m10663_gshared, &t3466_TI, &t3463_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19260_GM};
extern Il2CppType t1567_0_0_0;
static ParameterInfo t3466_m19261_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19261_GM;
MethodInfo m19261_MI = 
{
	"IndexOf", (methodPointerType)&m10664_gshared, &t3466_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3466_m19261_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19261_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19262_GM;
MethodInfo m19262_MI = 
{
	"get_Count", (methodPointerType)&m10665_gshared, &t3466_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19262_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3466_m19263_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1567_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19263_GM;
MethodInfo m19263_MI = 
{
	"get_Item", (methodPointerType)&m10666_gshared, &t3466_TI, &t1567_0_0_0, RuntimeInvoker_t29_t44, t3466_m19263_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 33, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19263_GM};
static MethodInfo* t3466_MIs[] =
{
	&m19234_MI,
	&m19235_MI,
	&m19236_MI,
	&m19237_MI,
	&m19238_MI,
	&m19239_MI,
	&m19240_MI,
	&m19241_MI,
	&m19242_MI,
	&m19243_MI,
	&m19244_MI,
	&m19245_MI,
	&m19246_MI,
	&m19247_MI,
	&m19248_MI,
	&m19249_MI,
	&m19250_MI,
	&m19251_MI,
	&m19252_MI,
	&m19253_MI,
	&m19254_MI,
	&m19255_MI,
	&m19256_MI,
	&m19257_MI,
	&m19258_MI,
	&m19259_MI,
	&m19260_MI,
	&m19261_MI,
	&m19262_MI,
	&m19263_MI,
	NULL
};
extern MethodInfo m19244_MI;
extern MethodInfo m19243_MI;
extern MethodInfo m19245_MI;
extern MethodInfo m19246_MI;
extern MethodInfo m19247_MI;
extern MethodInfo m19248_MI;
extern MethodInfo m19249_MI;
extern MethodInfo m19250_MI;
extern MethodInfo m19251_MI;
extern MethodInfo m19235_MI;
extern MethodInfo m19236_MI;
extern MethodInfo m19258_MI;
extern MethodInfo m19259_MI;
extern MethodInfo m19238_MI;
extern MethodInfo m19261_MI;
extern MethodInfo m19237_MI;
extern MethodInfo m19239_MI;
extern MethodInfo m19260_MI;
static MethodInfo* t3466_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m19244_MI,
	&m19262_MI,
	&m19252_MI,
	&m19253_MI,
	&m19243_MI,
	&m19254_MI,
	&m19255_MI,
	&m19256_MI,
	&m19257_MI,
	&m19245_MI,
	&m19246_MI,
	&m19247_MI,
	&m19248_MI,
	&m19249_MI,
	&m19250_MI,
	&m19251_MI,
	&m19262_MI,
	&m19242_MI,
	&m19235_MI,
	&m19236_MI,
	&m19258_MI,
	&m19259_MI,
	&m19238_MI,
	&m19261_MI,
	&m19237_MI,
	&m19239_MI,
	&m19240_MI,
	&m19241_MI,
	&m19260_MI,
	&m19263_MI,
};
static TypeInfo* t3466_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t3464_TI,
	&t1564_TI,
	&t3465_TI,
};
static Il2CppInterfaceOffsetPair t3466_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t3464_TI, 20},
	{ &t1564_TI, 27},
	{ &t3465_TI, 32},
};
extern TypeInfo t1567_TI;
static Il2CppRGCTXData t3466_RGCTXData[9] = 
{
	&m19263_MI/* Method Usage */,
	&m19295_MI/* Method Usage */,
	&t1567_TI/* Class Usage */,
	&m33946_MI/* Method Usage */,
	&m33939_MI/* Method Usage */,
	&m33937_MI/* Method Usage */,
	&m33947_MI/* Method Usage */,
	&m33949_MI/* Method Usage */,
	&m33942_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3466_0_0_0;
extern Il2CppType t3466_1_0_0;
struct t3466;
extern Il2CppGenericClass t3466_GC;
extern CustomAttributesCache t1263__CustomAttributeCache;
TypeInfo t3466_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ReadOnlyCollection`1", "System.Collections.ObjectModel", t3466_MIs, t3466_PIs, t3466_FIs, NULL, &t29_TI, NULL, NULL, &t3466_TI, t3466_ITIs, t3466_VT, &t1263__CustomAttributeCache, &t3466_TI, &t3466_0_0_0, &t3466_1_0_0, t3466_IOs, &t3466_GC, NULL, NULL, NULL, t3466_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3466), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 30, 9, 1, 0, 0, 34, 6, 6};
#include "t3470.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3470_TI;

#include "t43.h"
extern MethodInfo m19298_MI;
extern MethodInfo m19299_MI;
extern MethodInfo m19296_MI;
extern MethodInfo m19294_MI;
extern MethodInfo m19180_MI;
extern MethodInfo m9816_MI;
extern MethodInfo m19287_MI;
extern MethodInfo m19297_MI;
extern MethodInfo m19285_MI;
extern MethodInfo m19290_MI;
extern MethodInfo m19281_MI;
extern MethodInfo m33945_MI;
extern MethodInfo m33940_MI;
extern MethodInfo m33941_MI;
extern MethodInfo m1554_MI;
extern MethodInfo m6004_MI;
extern MethodInfo m9815_MI;
extern MethodInfo m9817_MI;


// Metadata Definition System.Collections.ObjectModel.Collection`1<System.Security.Policy.StrongName>
extern Il2CppType t1564_0_0_1;
FieldInfo t3470_f0_FieldInfo = 
{
	"list", &t1564_0_0_1, &t3470_TI, offsetof(t3470, f0), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t3470_f1_FieldInfo = 
{
	"syncRoot", &t29_0_0_1, &t3470_TI, offsetof(t3470, f1), &EmptyCustomAttributesCache};
static FieldInfo* t3470_FIs[] =
{
	&t3470_f0_FieldInfo,
	&t3470_f1_FieldInfo,
	NULL
};
extern MethodInfo m19265_MI;
static PropertyInfo t3470____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t3470_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m19265_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m19273_MI;
static PropertyInfo t3470____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t3470_TI, "System.Collections.ICollection.IsSynchronized", &m19273_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m19274_MI;
static PropertyInfo t3470____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t3470_TI, "System.Collections.ICollection.SyncRoot", &m19274_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m19275_MI;
static PropertyInfo t3470____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t3470_TI, "System.Collections.IList.IsFixedSize", &m19275_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m19276_MI;
static PropertyInfo t3470____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t3470_TI, "System.Collections.IList.IsReadOnly", &m19276_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m19277_MI;
extern MethodInfo m19278_MI;
static PropertyInfo t3470____System_Collections_IList_Item_PropertyInfo = 
{
	&t3470_TI, "System.Collections.IList.Item", &m19277_MI, &m19278_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m19291_MI;
static PropertyInfo t3470____Count_PropertyInfo = 
{
	&t3470_TI, "Count", &m19291_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m19292_MI;
extern MethodInfo m19293_MI;
static PropertyInfo t3470____Item_PropertyInfo = 
{
	&t3470_TI, "Item", &m19292_MI, &m19293_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3470_PIs[] =
{
	&t3470____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t3470____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t3470____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t3470____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t3470____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t3470____System_Collections_IList_Item_PropertyInfo,
	&t3470____Count_PropertyInfo,
	&t3470____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19264_GM;
MethodInfo m19264_MI = 
{
	".ctor", (methodPointerType)&m10667_gshared, &t3470_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19264_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19265_GM;
MethodInfo m19265_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10668_gshared, &t3470_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19265_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3470_m19266_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19266_GM;
MethodInfo m19266_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10669_gshared, &t3470_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3470_m19266_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19266_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19267_GM;
MethodInfo m19267_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10670_gshared, &t3470_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19267_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3470_m19268_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19268_GM;
MethodInfo m19268_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10671_gshared, &t3470_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3470_m19268_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19268_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3470_m19269_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19269_GM;
MethodInfo m19269_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10672_gshared, &t3470_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3470_m19269_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19269_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3470_m19270_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19270_GM;
MethodInfo m19270_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10673_gshared, &t3470_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3470_m19270_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19270_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3470_m19271_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19271_GM;
MethodInfo m19271_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10674_gshared, &t3470_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3470_m19271_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19271_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3470_m19272_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19272_GM;
MethodInfo m19272_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10675_gshared, &t3470_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3470_m19272_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19272_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19273_GM;
MethodInfo m19273_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10676_gshared, &t3470_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19273_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19274_GM;
MethodInfo m19274_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10677_gshared, &t3470_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19274_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19275_GM;
MethodInfo m19275_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10678_gshared, &t3470_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19275_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19276_GM;
MethodInfo m19276_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10679_gshared, &t3470_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19276_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3470_m19277_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19277_GM;
MethodInfo m19277_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10680_gshared, &t3470_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t3470_m19277_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19277_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3470_m19278_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19278_GM;
MethodInfo m19278_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10681_gshared, &t3470_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3470_m19278_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19278_GM};
extern Il2CppType t1567_0_0_0;
static ParameterInfo t3470_m19279_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19279_GM;
MethodInfo m19279_MI = 
{
	"Add", (methodPointerType)&m10682_gshared, &t3470_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3470_m19279_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19279_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19280_GM;
MethodInfo m19280_MI = 
{
	"Clear", (methodPointerType)&m10683_gshared, &t3470_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19280_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19281_GM;
MethodInfo m19281_MI = 
{
	"ClearItems", (methodPointerType)&m10684_gshared, &t3470_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 452, 0, 33, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19281_GM};
extern Il2CppType t1567_0_0_0;
static ParameterInfo t3470_m19282_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19282_GM;
MethodInfo m19282_MI = 
{
	"Contains", (methodPointerType)&m10685_gshared, &t3470_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3470_m19282_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19282_GM};
extern Il2CppType t3462_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3470_m19283_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3462_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19283_GM;
MethodInfo m19283_MI = 
{
	"CopyTo", (methodPointerType)&m10686_gshared, &t3470_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3470_m19283_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19283_GM};
extern Il2CppType t3463_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19284_GM;
MethodInfo m19284_MI = 
{
	"GetEnumerator", (methodPointerType)&m10687_gshared, &t3470_TI, &t3463_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19284_GM};
extern Il2CppType t1567_0_0_0;
static ParameterInfo t3470_m19285_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19285_GM;
MethodInfo m19285_MI = 
{
	"IndexOf", (methodPointerType)&m10688_gshared, &t3470_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3470_m19285_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19285_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1567_0_0_0;
static ParameterInfo t3470_m19286_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19286_GM;
MethodInfo m19286_MI = 
{
	"Insert", (methodPointerType)&m10689_gshared, &t3470_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3470_m19286_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19286_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1567_0_0_0;
static ParameterInfo t3470_m19287_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19287_GM;
MethodInfo m19287_MI = 
{
	"InsertItem", (methodPointerType)&m10690_gshared, &t3470_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3470_m19287_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 34, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19287_GM};
extern Il2CppType t1567_0_0_0;
static ParameterInfo t3470_m19288_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19288_GM;
MethodInfo m19288_MI = 
{
	"Remove", (methodPointerType)&m10691_gshared, &t3470_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3470_m19288_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19288_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3470_m19289_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19289_GM;
MethodInfo m19289_MI = 
{
	"RemoveAt", (methodPointerType)&m10692_gshared, &t3470_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t3470_m19289_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19289_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3470_m19290_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19290_GM;
MethodInfo m19290_MI = 
{
	"RemoveItem", (methodPointerType)&m10693_gshared, &t3470_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t3470_m19290_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 35, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19290_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19291_GM;
MethodInfo m19291_MI = 
{
	"get_Count", (methodPointerType)&m10694_gshared, &t3470_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19291_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3470_m19292_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1567_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19292_GM;
MethodInfo m19292_MI = 
{
	"get_Item", (methodPointerType)&m10695_gshared, &t3470_TI, &t1567_0_0_0, RuntimeInvoker_t29_t44, t3470_m19292_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19292_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1567_0_0_0;
static ParameterInfo t3470_m19293_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19293_GM;
MethodInfo m19293_MI = 
{
	"set_Item", (methodPointerType)&m10696_gshared, &t3470_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3470_m19293_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19293_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1567_0_0_0;
static ParameterInfo t3470_m19294_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19294_GM;
MethodInfo m19294_MI = 
{
	"SetItem", (methodPointerType)&m10697_gshared, &t3470_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3470_m19294_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 36, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19294_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3470_m19295_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19295_GM;
MethodInfo m19295_MI = 
{
	"IsValidItem", (methodPointerType)&m10698_gshared, &t3470_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3470_m19295_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19295_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3470_m19296_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t1567_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19296_GM;
MethodInfo m19296_MI = 
{
	"ConvertItem", (methodPointerType)&m10699_gshared, &t3470_TI, &t1567_0_0_0, RuntimeInvoker_t29_t29, t3470_m19296_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19296_GM};
extern Il2CppType t1564_0_0_0;
static ParameterInfo t3470_m19297_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t1564_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19297_GM;
MethodInfo m19297_MI = 
{
	"CheckWritable", (methodPointerType)&m10700_gshared, &t3470_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3470_m19297_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19297_GM};
extern Il2CppType t1564_0_0_0;
static ParameterInfo t3470_m19298_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t1564_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19298_GM;
MethodInfo m19298_MI = 
{
	"IsSynchronized", (methodPointerType)&m10701_gshared, &t3470_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3470_m19298_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19298_GM};
extern Il2CppType t1564_0_0_0;
static ParameterInfo t3470_m19299_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t1564_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19299_GM;
MethodInfo m19299_MI = 
{
	"IsFixedSize", (methodPointerType)&m10702_gshared, &t3470_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3470_m19299_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19299_GM};
static MethodInfo* t3470_MIs[] =
{
	&m19264_MI,
	&m19265_MI,
	&m19266_MI,
	&m19267_MI,
	&m19268_MI,
	&m19269_MI,
	&m19270_MI,
	&m19271_MI,
	&m19272_MI,
	&m19273_MI,
	&m19274_MI,
	&m19275_MI,
	&m19276_MI,
	&m19277_MI,
	&m19278_MI,
	&m19279_MI,
	&m19280_MI,
	&m19281_MI,
	&m19282_MI,
	&m19283_MI,
	&m19284_MI,
	&m19285_MI,
	&m19286_MI,
	&m19287_MI,
	&m19288_MI,
	&m19289_MI,
	&m19290_MI,
	&m19291_MI,
	&m19292_MI,
	&m19293_MI,
	&m19294_MI,
	&m19295_MI,
	&m19296_MI,
	&m19297_MI,
	&m19298_MI,
	&m19299_MI,
	NULL
};
extern MethodInfo m19267_MI;
extern MethodInfo m19266_MI;
extern MethodInfo m19268_MI;
extern MethodInfo m19280_MI;
extern MethodInfo m19269_MI;
extern MethodInfo m19270_MI;
extern MethodInfo m19271_MI;
extern MethodInfo m19272_MI;
extern MethodInfo m19289_MI;
extern MethodInfo m19279_MI;
extern MethodInfo m19282_MI;
extern MethodInfo m19283_MI;
extern MethodInfo m19288_MI;
extern MethodInfo m19286_MI;
extern MethodInfo m19284_MI;
static MethodInfo* t3470_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m19267_MI,
	&m19291_MI,
	&m19273_MI,
	&m19274_MI,
	&m19266_MI,
	&m19275_MI,
	&m19276_MI,
	&m19277_MI,
	&m19278_MI,
	&m19268_MI,
	&m19280_MI,
	&m19269_MI,
	&m19270_MI,
	&m19271_MI,
	&m19272_MI,
	&m19289_MI,
	&m19291_MI,
	&m19265_MI,
	&m19279_MI,
	&m19280_MI,
	&m19282_MI,
	&m19283_MI,
	&m19288_MI,
	&m19285_MI,
	&m19286_MI,
	&m19289_MI,
	&m19292_MI,
	&m19293_MI,
	&m19284_MI,
	&m19281_MI,
	&m19287_MI,
	&m19290_MI,
	&m19294_MI,
};
static TypeInfo* t3470_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t3464_TI,
	&t1564_TI,
	&t3465_TI,
};
static Il2CppInterfaceOffsetPair t3470_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t3464_TI, 20},
	{ &t1564_TI, 27},
	{ &t3465_TI, 32},
};
extern TypeInfo t2061_TI;
extern TypeInfo t1567_TI;
static Il2CppRGCTXData t3470_RGCTXData[25] = 
{
	&t2061_TI/* Class Usage */,
	&m19180_MI/* Method Usage */,
	&m33943_MI/* Method Usage */,
	&m33949_MI/* Method Usage */,
	&m33942_MI/* Method Usage */,
	&m19296_MI/* Method Usage */,
	&m19287_MI/* Method Usage */,
	&m19295_MI/* Method Usage */,
	&t1567_TI/* Class Usage */,
	&m33946_MI/* Method Usage */,
	&m33939_MI/* Method Usage */,
	&m19297_MI/* Method Usage */,
	&m19285_MI/* Method Usage */,
	&m19290_MI/* Method Usage */,
	&m19298_MI/* Method Usage */,
	&m19299_MI/* Method Usage */,
	&m33937_MI/* Method Usage */,
	&m19294_MI/* Method Usage */,
	&m19281_MI/* Method Usage */,
	&m33945_MI/* Method Usage */,
	&m33947_MI/* Method Usage */,
	&m33940_MI/* Method Usage */,
	&m33941_MI/* Method Usage */,
	&m33938_MI/* Method Usage */,
	&t1567_0_0_0/* Type Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3470_0_0_0;
extern Il2CppType t3470_1_0_0;
struct t3470;
extern Il2CppGenericClass t3470_GC;
extern CustomAttributesCache t1262__CustomAttributeCache;
TypeInfo t3470_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Collection`1", "System.Collections.ObjectModel", t3470_MIs, t3470_PIs, t3470_FIs, NULL, &t29_TI, NULL, NULL, &t3470_TI, t3470_ITIs, t3470_VT, &t1262__CustomAttributeCache, &t3470_TI, &t3470_0_0_0, &t3470_1_0_0, t3470_IOs, &t3470_GC, NULL, NULL, NULL, t3470_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3470), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 36, 8, 2, 0, 0, 37, 6, 6};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3471_TI;
#include "t3471MD.h"

#include "t1257.h"
#include "t3472.h"
extern TypeInfo t6762_TI;
extern TypeInfo t1257_TI;
extern TypeInfo t537_TI;
extern TypeInfo t3472_TI;
#include "t931MD.h"
#include "t3472MD.h"
extern Il2CppType t6762_0_0_0;
extern Il2CppType t1257_0_0_0;
extern MethodInfo m2981_MI;
extern MethodInfo m2979_MI;
extern MethodInfo m8828_MI;
extern MethodInfo m19305_MI;
extern MethodInfo m33979_MI;
extern MethodInfo m26030_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1<System.Security.Policy.StrongName>
extern Il2CppType t3471_0_0_49;
FieldInfo t3471_f0_FieldInfo = 
{
	"_default", &t3471_0_0_49, &t3471_TI, offsetof(t3471_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3471_FIs[] =
{
	&t3471_f0_FieldInfo,
	NULL
};
extern MethodInfo m19304_MI;
static PropertyInfo t3471____Default_PropertyInfo = 
{
	&t3471_TI, "Default", &m19304_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3471_PIs[] =
{
	&t3471____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19300_GM;
MethodInfo m19300_MI = 
{
	".ctor", (methodPointerType)&m10703_gshared, &t3471_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19300_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19301_GM;
MethodInfo m19301_MI = 
{
	".cctor", (methodPointerType)&m10704_gshared, &t3471_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19301_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3471_m19302_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19302_GM;
MethodInfo m19302_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m10705_gshared, &t3471_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3471_m19302_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19302_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3471_m19303_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19303_GM;
MethodInfo m19303_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m10706_gshared, &t3471_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t3471_m19303_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19303_GM};
extern Il2CppType t1567_0_0_0;
static ParameterInfo t3471_m33979_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33979_GM;
MethodInfo m33979_MI = 
{
	"GetHashCode", NULL, &t3471_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3471_m33979_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33979_GM};
extern Il2CppType t1567_0_0_0;
extern Il2CppType t1567_0_0_0;
static ParameterInfo t3471_m26030_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26030_GM;
MethodInfo m26030_MI = 
{
	"Equals", NULL, &t3471_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t3471_m26030_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26030_GM};
extern Il2CppType t3471_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19304_GM;
MethodInfo m19304_MI = 
{
	"get_Default", (methodPointerType)&m10707_gshared, &t3471_TI, &t3471_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19304_GM};
static MethodInfo* t3471_MIs[] =
{
	&m19300_MI,
	&m19301_MI,
	&m19302_MI,
	&m19303_MI,
	&m33979_MI,
	&m26030_MI,
	&m19304_MI,
	NULL
};
extern MethodInfo m19303_MI;
extern MethodInfo m19302_MI;
static MethodInfo* t3471_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m26030_MI,
	&m33979_MI,
	&m19303_MI,
	&m19302_MI,
	NULL,
	NULL,
};
extern TypeInfo t6763_TI;
extern TypeInfo t734_TI;
static TypeInfo* t3471_ITIs[] = 
{
	&t6763_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t3471_IOs[] = 
{
	{ &t6763_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t3471_TI;
extern TypeInfo t3471_TI;
extern TypeInfo t3472_TI;
extern TypeInfo t1567_TI;
static Il2CppRGCTXData t3471_RGCTXData[9] = 
{
	&t6762_0_0_0/* Type Usage */,
	&t1567_0_0_0/* Type Usage */,
	&t3471_TI/* Class Usage */,
	&t3471_TI/* Static Usage */,
	&t3472_TI/* Class Usage */,
	&m19305_MI/* Method Usage */,
	&t1567_TI/* Class Usage */,
	&m33979_MI/* Method Usage */,
	&m26030_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3471_0_0_0;
extern Il2CppType t3471_1_0_0;
struct t3471;
extern Il2CppGenericClass t3471_GC;
TypeInfo t3471_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t3471_MIs, t3471_PIs, t3471_FIs, NULL, &t29_TI, NULL, NULL, &t3471_TI, t3471_ITIs, t3471_VT, &EmptyCustomAttributesCache, &t3471_TI, &t3471_0_0_0, &t3471_1_0_0, t3471_IOs, &t3471_GC, NULL, NULL, NULL, t3471_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3471), 0, -1, sizeof(t3471_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<System.Security.Policy.StrongName>
extern Il2CppType t1567_0_0_0;
extern Il2CppType t1567_0_0_0;
static ParameterInfo t6763_m33980_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33980_GM;
MethodInfo m33980_MI = 
{
	"Equals", NULL, &t6763_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t6763_m33980_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33980_GM};
extern Il2CppType t1567_0_0_0;
static ParameterInfo t6763_m33981_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33981_GM;
MethodInfo m33981_MI = 
{
	"GetHashCode", NULL, &t6763_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6763_m33981_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33981_GM};
static MethodInfo* t6763_MIs[] =
{
	&m33980_MI,
	&m33981_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6763_0_0_0;
extern Il2CppType t6763_1_0_0;
struct t6763;
extern Il2CppGenericClass t6763_GC;
TypeInfo t6763_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t6763_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6763_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6763_TI, &t6763_0_0_0, &t6763_1_0_0, NULL, &t6763_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IEquatable`1<System.Security.Policy.StrongName>
extern Il2CppType t1567_0_0_0;
static ParameterInfo t6762_m33982_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33982_GM;
MethodInfo m33982_MI = 
{
	"Equals", NULL, &t6762_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6762_m33982_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33982_GM};
static MethodInfo* t6762_MIs[] =
{
	&m33982_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6762_1_0_0;
struct t6762;
extern Il2CppGenericClass t6762_GC;
TypeInfo t6762_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t6762_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6762_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6762_TI, &t6762_0_0_0, &t6762_1_0_0, NULL, &t6762_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m19300_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Security.Policy.StrongName>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19305_GM;
MethodInfo m19305_MI = 
{
	".ctor", (methodPointerType)&m10738_gshared, &t3472_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19305_GM};
extern Il2CppType t1567_0_0_0;
static ParameterInfo t3472_m19306_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19306_GM;
MethodInfo m19306_MI = 
{
	"GetHashCode", (methodPointerType)&m10739_gshared, &t3472_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3472_m19306_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19306_GM};
extern Il2CppType t1567_0_0_0;
extern Il2CppType t1567_0_0_0;
static ParameterInfo t3472_m19307_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19307_GM;
MethodInfo m19307_MI = 
{
	"Equals", (methodPointerType)&m10740_gshared, &t3472_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t3472_m19307_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19307_GM};
static MethodInfo* t3472_MIs[] =
{
	&m19305_MI,
	&m19306_MI,
	&m19307_MI,
	NULL
};
extern MethodInfo m19307_MI;
extern MethodInfo m19306_MI;
static MethodInfo* t3472_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m19307_MI,
	&m19306_MI,
	&m19303_MI,
	&m19302_MI,
	&m19306_MI,
	&m19307_MI,
};
static Il2CppInterfaceOffsetPair t3472_IOs[] = 
{
	{ &t6763_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t3471_TI;
extern TypeInfo t3471_TI;
extern TypeInfo t3472_TI;
extern TypeInfo t1567_TI;
extern TypeInfo t1567_TI;
static Il2CppRGCTXData t3472_RGCTXData[11] = 
{
	&t6762_0_0_0/* Type Usage */,
	&t1567_0_0_0/* Type Usage */,
	&t3471_TI/* Class Usage */,
	&t3471_TI/* Static Usage */,
	&t3472_TI/* Class Usage */,
	&m19305_MI/* Method Usage */,
	&t1567_TI/* Class Usage */,
	&m33979_MI/* Method Usage */,
	&m26030_MI/* Method Usage */,
	&m19300_MI/* Method Usage */,
	&t1567_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3472_0_0_0;
extern Il2CppType t3472_1_0_0;
struct t3472;
extern Il2CppGenericClass t3472_GC;
extern TypeInfo t1256_TI;
TypeInfo t3472_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t3472_MIs, NULL, NULL, NULL, &t3471_TI, NULL, &t1256_TI, &t3472_TI, NULL, t3472_VT, &EmptyCustomAttributesCache, &t3472_TI, &t3472_0_0_0, &t3472_1_0_0, t3472_IOs, &t3472_GC, NULL, NULL, NULL, t3472_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3472), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#ifndef _MSC_VER
#else
#endif

#include "t35.h"
#include "t67.h"


// Metadata Definition System.Predicate`1<System.Security.Policy.StrongName>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t3467_m19308_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19308_GM;
MethodInfo m19308_MI = 
{
	".ctor", (methodPointerType)&m10741_gshared, &t3467_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t3467_m19308_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19308_GM};
extern Il2CppType t1567_0_0_0;
static ParameterInfo t3467_m19309_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19309_GM;
MethodInfo m19309_MI = 
{
	"Invoke", (methodPointerType)&m10742_gshared, &t3467_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3467_m19309_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19309_GM};
extern Il2CppType t1567_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3467_m19310_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19310_GM;
MethodInfo m19310_MI = 
{
	"BeginInvoke", (methodPointerType)&m10743_gshared, &t3467_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t3467_m19310_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m19310_GM};
extern Il2CppType t66_0_0_0;
extern Il2CppType t66_0_0_0;
static ParameterInfo t3467_m19311_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19311_GM;
MethodInfo m19311_MI = 
{
	"EndInvoke", (methodPointerType)&m10744_gshared, &t3467_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3467_m19311_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19311_GM};
static MethodInfo* t3467_MIs[] =
{
	&m19308_MI,
	&m19309_MI,
	&m19310_MI,
	&m19311_MI,
	NULL
};
extern MethodInfo m1699_MI;
extern MethodInfo m1700_MI;
extern MethodInfo m1701_MI;
extern MethodInfo m1702_MI;
extern MethodInfo m1703_MI;
extern MethodInfo m1704_MI;
extern MethodInfo m1705_MI;
extern MethodInfo m19310_MI;
extern MethodInfo m19311_MI;
static MethodInfo* t3467_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m19309_MI,
	&m19310_MI,
	&m19311_MI,
};
extern TypeInfo t373_TI;
extern TypeInfo t374_TI;
static Il2CppInterfaceOffsetPair t3467_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3467_1_0_0;
extern TypeInfo t195_TI;
struct t3467;
extern Il2CppGenericClass t3467_GC;
TypeInfo t3467_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Predicate`1", "System", t3467_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t3467_TI, NULL, t3467_VT, &EmptyCustomAttributesCache, &t3467_TI, &t3467_0_0_0, &t3467_1_0_0, t3467_IOs, &t3467_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3467), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif

#include "t1247.h"
#include "t3474.h"
extern TypeInfo t5017_TI;
extern TypeInfo t1247_TI;
extern TypeInfo t3474_TI;
#include "t3474MD.h"
extern Il2CppType t5017_0_0_0;
extern Il2CppType t1247_0_0_0;
extern MethodInfo m19316_MI;
extern MethodInfo m33983_MI;
extern MethodInfo m8852_MI;


// Metadata Definition System.Collections.Generic.Comparer`1<System.Security.Policy.StrongName>
extern Il2CppType t3473_0_0_49;
FieldInfo t3473_f0_FieldInfo = 
{
	"_default", &t3473_0_0_49, &t3473_TI, offsetof(t3473_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3473_FIs[] =
{
	&t3473_f0_FieldInfo,
	NULL
};
static PropertyInfo t3473____Default_PropertyInfo = 
{
	&t3473_TI, "Default", &m19315_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3473_PIs[] =
{
	&t3473____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19312_GM;
MethodInfo m19312_MI = 
{
	".ctor", (methodPointerType)&m10745_gshared, &t3473_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19312_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19313_GM;
MethodInfo m19313_MI = 
{
	".cctor", (methodPointerType)&m10746_gshared, &t3473_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19313_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3473_m19314_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19314_GM;
MethodInfo m19314_MI = 
{
	"System.Collections.IComparer.Compare", (methodPointerType)&m10747_gshared, &t3473_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t3473_m19314_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19314_GM};
extern Il2CppType t1567_0_0_0;
extern Il2CppType t1567_0_0_0;
static ParameterInfo t3473_m33983_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33983_GM;
MethodInfo m33983_MI = 
{
	"Compare", NULL, &t3473_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t3473_m33983_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33983_GM};
extern Il2CppType t3473_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19315_GM;
MethodInfo m19315_MI = 
{
	"get_Default", (methodPointerType)&m10748_gshared, &t3473_TI, &t3473_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19315_GM};
static MethodInfo* t3473_MIs[] =
{
	&m19312_MI,
	&m19313_MI,
	&m19314_MI,
	&m33983_MI,
	&m19315_MI,
	NULL
};
extern MethodInfo m19314_MI;
static MethodInfo* t3473_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m33983_MI,
	&m19314_MI,
	NULL,
};
extern TypeInfo t5016_TI;
extern TypeInfo t726_TI;
static TypeInfo* t3473_ITIs[] = 
{
	&t5016_TI,
	&t726_TI,
};
static Il2CppInterfaceOffsetPair t3473_IOs[] = 
{
	{ &t5016_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t3473_TI;
extern TypeInfo t3473_TI;
extern TypeInfo t3474_TI;
extern TypeInfo t1567_TI;
static Il2CppRGCTXData t3473_RGCTXData[8] = 
{
	&t5017_0_0_0/* Type Usage */,
	&t1567_0_0_0/* Type Usage */,
	&t3473_TI/* Class Usage */,
	&t3473_TI/* Static Usage */,
	&t3474_TI/* Class Usage */,
	&m19316_MI/* Method Usage */,
	&t1567_TI/* Class Usage */,
	&m33983_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3473_0_0_0;
extern Il2CppType t3473_1_0_0;
struct t3473;
extern Il2CppGenericClass t3473_GC;
TypeInfo t3473_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparer`1", "System.Collections.Generic", t3473_MIs, t3473_PIs, t3473_FIs, NULL, &t29_TI, NULL, NULL, &t3473_TI, t3473_ITIs, t3473_VT, &EmptyCustomAttributesCache, &t3473_TI, &t3473_0_0_0, &t3473_1_0_0, t3473_IOs, &t3473_GC, NULL, NULL, NULL, t3473_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3473), 0, -1, sizeof(t3473_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 5, 1, 1, 0, 0, 7, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IComparer`1<System.Security.Policy.StrongName>
extern Il2CppType t1567_0_0_0;
extern Il2CppType t1567_0_0_0;
static ParameterInfo t5016_m26038_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26038_GM;
MethodInfo m26038_MI = 
{
	"Compare", NULL, &t5016_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t5016_m26038_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26038_GM};
static MethodInfo* t5016_MIs[] =
{
	&m26038_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5016_0_0_0;
extern Il2CppType t5016_1_0_0;
struct t5016;
extern Il2CppGenericClass t5016_GC;
TypeInfo t5016_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparer`1", "System.Collections.Generic", t5016_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5016_TI, NULL, NULL, &EmptyCustomAttributesCache, &t5016_TI, &t5016_0_0_0, &t5016_1_0_0, NULL, &t5016_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IComparable`1<System.Security.Policy.StrongName>
extern Il2CppType t1567_0_0_0;
static ParameterInfo t5017_m26039_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26039_GM;
MethodInfo m26039_MI = 
{
	"CompareTo", NULL, &t5017_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5017_m26039_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26039_GM};
static MethodInfo* t5017_MIs[] =
{
	&m26039_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5017_1_0_0;
struct t5017;
extern Il2CppGenericClass t5017_GC;
TypeInfo t5017_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t5017_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5017_TI, NULL, NULL, &EmptyCustomAttributesCache, &t5017_TI, &t5017_0_0_0, &t5017_1_0_0, NULL, &t5017_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo t290_TI;
extern MethodInfo m19312_MI;
extern MethodInfo m26039_MI;
extern MethodInfo m9672_MI;


// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer<System.Security.Policy.StrongName>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19316_GM;
MethodInfo m19316_MI = 
{
	".ctor", (methodPointerType)&m10749_gshared, &t3474_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19316_GM};
extern Il2CppType t1567_0_0_0;
extern Il2CppType t1567_0_0_0;
static ParameterInfo t3474_m19317_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19317_GM;
MethodInfo m19317_MI = 
{
	"Compare", (methodPointerType)&m10750_gshared, &t3474_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t3474_m19317_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19317_GM};
static MethodInfo* t3474_MIs[] =
{
	&m19316_MI,
	&m19317_MI,
	NULL
};
extern MethodInfo m19317_MI;
static MethodInfo* t3474_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m19317_MI,
	&m19314_MI,
	&m19317_MI,
};
static Il2CppInterfaceOffsetPair t3474_IOs[] = 
{
	{ &t5016_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t3473_TI;
extern TypeInfo t3473_TI;
extern TypeInfo t3474_TI;
extern TypeInfo t1567_TI;
extern TypeInfo t1567_TI;
extern TypeInfo t5017_TI;
static Il2CppRGCTXData t3474_RGCTXData[12] = 
{
	&t5017_0_0_0/* Type Usage */,
	&t1567_0_0_0/* Type Usage */,
	&t3473_TI/* Class Usage */,
	&t3473_TI/* Static Usage */,
	&t3474_TI/* Class Usage */,
	&m19316_MI/* Method Usage */,
	&t1567_TI/* Class Usage */,
	&m33983_MI/* Method Usage */,
	&m19312_MI/* Method Usage */,
	&t1567_TI/* Class Usage */,
	&t5017_TI/* Class Usage */,
	&m26039_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3474_0_0_0;
extern Il2CppType t3474_1_0_0;
struct t3474;
extern Il2CppGenericClass t3474_GC;
extern TypeInfo t1246_TI;
TypeInfo t3474_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t3474_MIs, NULL, NULL, NULL, &t3473_TI, NULL, &t1246_TI, &t3474_TI, NULL, t3474_VT, &EmptyCustomAttributesCache, &t3474_TI, &t3474_0_0_0, &t3474_1_0_0, t3474_IOs, &t3474_GC, NULL, NULL, NULL, t3474_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3474), 0, -1, 0, 0, -1, 1048835, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 7, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3468_TI;
#include "t3468MD.h"



// Metadata Definition System.Comparison`1<System.Security.Policy.StrongName>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t3468_m19318_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19318_GM;
MethodInfo m19318_MI = 
{
	".ctor", (methodPointerType)&m10796_gshared, &t3468_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t3468_m19318_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19318_GM};
extern Il2CppType t1567_0_0_0;
extern Il2CppType t1567_0_0_0;
static ParameterInfo t3468_m19319_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19319_GM;
MethodInfo m19319_MI = 
{
	"Invoke", (methodPointerType)&m10797_gshared, &t3468_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t3468_m19319_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19319_GM};
extern Il2CppType t1567_0_0_0;
extern Il2CppType t1567_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3468_m19320_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19320_GM;
MethodInfo m19320_MI = 
{
	"BeginInvoke", (methodPointerType)&m10798_gshared, &t3468_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t3468_m19320_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m19320_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t3468_m19321_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19321_GM;
MethodInfo m19321_MI = 
{
	"EndInvoke", (methodPointerType)&m10799_gshared, &t3468_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3468_m19321_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19321_GM};
static MethodInfo* t3468_MIs[] =
{
	&m19318_MI,
	&m19319_MI,
	&m19320_MI,
	&m19321_MI,
	NULL
};
extern MethodInfo m19319_MI;
extern MethodInfo m19320_MI;
extern MethodInfo m19321_MI;
static MethodInfo* t3468_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m19319_MI,
	&m19320_MI,
	&m19321_MI,
};
static Il2CppInterfaceOffsetPair t3468_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3468_1_0_0;
struct t3468;
extern Il2CppGenericClass t3468_GC;
TypeInfo t3468_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparison`1", "System", t3468_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t3468_TI, NULL, t3468_VT, &EmptyCustomAttributesCache, &t3468_TI, &t3468_0_0_0, &t3468_1_0_0, t3468_IOs, &t3468_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3468), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5019_TI;

#include "t1568.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Security.Principal.PrincipalPolicy>
extern MethodInfo m33984_MI;
static PropertyInfo t5019____Current_PropertyInfo = 
{
	&t5019_TI, "Current", &m33984_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5019_PIs[] =
{
	&t5019____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1568_0_0_0;
extern void* RuntimeInvoker_t1568 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33984_GM;
MethodInfo m33984_MI = 
{
	"get_Current", NULL, &t5019_TI, &t1568_0_0_0, RuntimeInvoker_t1568, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33984_GM};
static MethodInfo* t5019_MIs[] =
{
	&m33984_MI,
	NULL
};
static TypeInfo* t5019_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5019_0_0_0;
extern Il2CppType t5019_1_0_0;
struct t5019;
extern Il2CppGenericClass t5019_GC;
TypeInfo t5019_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t5019_MIs, t5019_PIs, NULL, NULL, NULL, NULL, NULL, &t5019_TI, t5019_ITIs, NULL, &EmptyCustomAttributesCache, &t5019_TI, &t5019_0_0_0, &t5019_1_0_0, NULL, &t5019_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3475.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3475_TI;
#include "t3475MD.h"

extern TypeInfo t1568_TI;
extern MethodInfo m19326_MI;
extern MethodInfo m26044_MI;
struct t20;
 int32_t m26044 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m19322_MI;
 void m19322 (t3475 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19323_MI;
 t29 * m19323 (t3475 * __this, MethodInfo* method){
	{
		int32_t L_0 = m19326(__this, &m19326_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1568_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m19324_MI;
 void m19324 (t3475 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19325_MI;
 bool m19325 (t3475 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m19326 (t3475 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m26044(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m26044_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Security.Principal.PrincipalPolicy>
extern Il2CppType t20_0_0_1;
FieldInfo t3475_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3475_TI, offsetof(t3475, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3475_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3475_TI, offsetof(t3475, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3475_FIs[] =
{
	&t3475_f0_FieldInfo,
	&t3475_f1_FieldInfo,
	NULL
};
static PropertyInfo t3475____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3475_TI, "System.Collections.IEnumerator.Current", &m19323_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3475____Current_PropertyInfo = 
{
	&t3475_TI, "Current", &m19326_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3475_PIs[] =
{
	&t3475____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3475____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3475_m19322_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19322_GM;
MethodInfo m19322_MI = 
{
	".ctor", (methodPointerType)&m19322, &t3475_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3475_m19322_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19322_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19323_GM;
MethodInfo m19323_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m19323, &t3475_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19323_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19324_GM;
MethodInfo m19324_MI = 
{
	"Dispose", (methodPointerType)&m19324, &t3475_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19324_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19325_GM;
MethodInfo m19325_MI = 
{
	"MoveNext", (methodPointerType)&m19325, &t3475_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19325_GM};
extern Il2CppType t1568_0_0_0;
extern void* RuntimeInvoker_t1568 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19326_GM;
MethodInfo m19326_MI = 
{
	"get_Current", (methodPointerType)&m19326, &t3475_TI, &t1568_0_0_0, RuntimeInvoker_t1568, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19326_GM};
static MethodInfo* t3475_MIs[] =
{
	&m19322_MI,
	&m19323_MI,
	&m19324_MI,
	&m19325_MI,
	&m19326_MI,
	NULL
};
static MethodInfo* t3475_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19323_MI,
	&m19325_MI,
	&m19324_MI,
	&m19326_MI,
};
static TypeInfo* t3475_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t5019_TI,
};
static Il2CppInterfaceOffsetPair t3475_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t5019_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3475_0_0_0;
extern Il2CppType t3475_1_0_0;
extern Il2CppGenericClass t3475_GC;
TypeInfo t3475_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3475_MIs, t3475_PIs, t3475_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3475_TI, t3475_ITIs, t3475_VT, &EmptyCustomAttributesCache, &t3475_TI, &t3475_0_0_0, &t3475_1_0_0, t3475_IOs, &t3475_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3475)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
