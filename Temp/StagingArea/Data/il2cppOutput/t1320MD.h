﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1320;
struct t295;
struct t7;
struct t446;
struct t781;
#include "t35.h"
#include "t1321.h"
#include "t1306.h"
#include "t1319.h"
#include "t1318.h"
#include "t1311.h"
#include "t751.h"
#include "t1314.h"
#include "t1313.h"
#include "t1064.h"

 void m7090 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t295 * m7091 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t295 * m7092 (t29 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7093 (t29 * __this, t7* p0, int32_t* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t446* m7094 (t29 * __this, t7* p0, t7* p1, int32_t p2, int32_t p3, int32_t* p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7095 (t29 * __this, int32_t* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7096 (t29 * __this, t7* p0, int32_t* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7097 (t29 * __this, t7* p0, int32_t* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7098 (t29 * __this, t35 p0, int32_t* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7099 (t29 * __this, t7* p0, int32_t* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7100 (t29 * __this, t7* p0, int32_t* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7101 (t29 * __this, t7* p0, t1318 * p1, int32_t* p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t35 m7102 (t29 * __this, t7* p0, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t* p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7103 (t29 * __this, t35 p0, int32_t* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7104 (t29 * __this, t35 p0, t781* p1, int32_t p2, int32_t p3, int32_t* p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7105 (t29 * __this, t35 p0, t781* p1, int32_t p2, int32_t p3, int32_t* p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m7106 (t29 * __this, t35 p0, int64_t p1, int32_t p2, int32_t* p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m7107 (t29 * __this, t35 p0, int32_t* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7108 (t29 * __this, t35 p0, int64_t p1, int32_t* p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t35 m7109 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t35 m7110 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t35 m7111 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m7112 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m7113 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m7114 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m7115 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
