﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1518;
struct t29;
struct t66;
struct t67;
#include "t35.h"
#include "t735.h"

 void m8126 (t1518 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8127 (t1518 * __this, t735  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m8128 (t1518 * __this, t735  p0, t67 * p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8129 (t1518 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
