﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1061;
struct t781;
struct t783;

 void m4882 (t1061 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4883 (t1061 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4884 (t1061 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4885 (t1061 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4886 (t1061 * __this, t783 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4887 (t1061 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
