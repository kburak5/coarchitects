﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2463;
struct t29;
struct t66;
struct t67;
#include "t35.h"
#include "t725.h"

 void m12963 (t2463 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t725  m12964 (t2463 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12965 (t2463 * __this, t29 * p0, t29 * p1, t67 * p2, t29 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t725  m12966 (t2463 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
