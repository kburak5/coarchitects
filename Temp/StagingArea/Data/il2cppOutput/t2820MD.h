﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2820;
struct t29;
struct t20;
#include "t376.h"

 void m15364 (t2820 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15365 (t2820 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15366 (t2820 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15367 (t2820 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m15368 (t2820 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
