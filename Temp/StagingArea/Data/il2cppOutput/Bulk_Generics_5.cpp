﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include "t84.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t84_TI;
#include "t84MD.h"

#include "t21.h"
#include "t29.h"
#include "t35.h"
#include "t53.h"
#include "t67.h"

#include "t20.h"

// Metadata Definition UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISubmitHandler>
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t84_m1355_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1355_GM;
MethodInfo m1355_MI = 
{
	".ctor", (methodPointerType)&m10902_gshared, &t84_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t84_m1355_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1355_GM};
extern Il2CppType t102_0_0_0;
extern Il2CppType t102_0_0_0;
extern Il2CppType t53_0_0_0;
extern Il2CppType t53_0_0_0;
static ParameterInfo t84_m11835_ParameterInfos[] = 
{
	{"handler", 0, 134217728, &EmptyCustomAttributesCache, &t102_0_0_0},
	{"eventData", 1, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11835_GM;
MethodInfo m11835_MI = 
{
	"Invoke", (methodPointerType)&m10904_gshared, &t84_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t84_m11835_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11835_GM};
extern Il2CppType t102_0_0_0;
extern Il2CppType t53_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t84_m11836_ParameterInfos[] = 
{
	{"handler", 0, 134217728, &EmptyCustomAttributesCache, &t102_0_0_0},
	{"eventData", 1, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11836_GM;
MethodInfo m11836_MI = 
{
	"BeginInvoke", (methodPointerType)&m10906_gshared, &t84_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t84_m11836_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m11836_GM};
extern Il2CppType t66_0_0_0;
extern Il2CppType t66_0_0_0;
static ParameterInfo t84_m11837_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11837_GM;
MethodInfo m11837_MI = 
{
	"EndInvoke", (methodPointerType)&m10908_gshared, &t84_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t84_m11837_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11837_GM};
static MethodInfo* t84_MIs[] =
{
	&m1355_MI,
	&m11835_MI,
	&m11836_MI,
	&m11837_MI,
	NULL
};
extern MethodInfo m1699_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1700_MI;
extern MethodInfo m1332_MI;
extern MethodInfo m1701_MI;
extern MethodInfo m1702_MI;
extern MethodInfo m1703_MI;
extern MethodInfo m1704_MI;
extern MethodInfo m1705_MI;
extern MethodInfo m11835_MI;
extern MethodInfo m11836_MI;
extern MethodInfo m11837_MI;
static MethodInfo* t84_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m11835_MI,
	&m11836_MI,
	&m11837_MI,
};
extern TypeInfo t373_TI;
extern TypeInfo t374_TI;
static Il2CppInterfaceOffsetPair t84_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t84_0_0_0;
extern Il2CppType t84_1_0_0;
extern TypeInfo t195_TI;
struct t84;
extern Il2CppGenericClass t84_GC;
extern TypeInfo t68_TI;
TypeInfo t84_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "EventFunction`1", "", t84_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t68_TI, &t84_TI, NULL, t84_VT, &EmptyCustomAttributesCache, &t84_TI, &t84_0_0_0, &t84_1_0_0, t84_IOs, &t84_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t84), 0, -1, 0, 0, -1, 258, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t85.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t85_TI;
#include "t85MD.h"



// Metadata Definition UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ICancelHandler>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t85_m1356_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1356_GM;
MethodInfo m1356_MI = 
{
	".ctor", (methodPointerType)&m10902_gshared, &t85_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t85_m1356_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1356_GM};
extern Il2CppType t103_0_0_0;
extern Il2CppType t103_0_0_0;
extern Il2CppType t53_0_0_0;
static ParameterInfo t85_m11838_ParameterInfos[] = 
{
	{"handler", 0, 134217728, &EmptyCustomAttributesCache, &t103_0_0_0},
	{"eventData", 1, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11838_GM;
MethodInfo m11838_MI = 
{
	"Invoke", (methodPointerType)&m10904_gshared, &t85_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t85_m11838_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11838_GM};
extern Il2CppType t103_0_0_0;
extern Il2CppType t53_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t85_m11839_ParameterInfos[] = 
{
	{"handler", 0, 134217728, &EmptyCustomAttributesCache, &t103_0_0_0},
	{"eventData", 1, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11839_GM;
MethodInfo m11839_MI = 
{
	"BeginInvoke", (methodPointerType)&m10906_gshared, &t85_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t85_m11839_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m11839_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t85_m11840_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11840_GM;
MethodInfo m11840_MI = 
{
	"EndInvoke", (methodPointerType)&m10908_gshared, &t85_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t85_m11840_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11840_GM};
static MethodInfo* t85_MIs[] =
{
	&m1356_MI,
	&m11838_MI,
	&m11839_MI,
	&m11840_MI,
	NULL
};
extern MethodInfo m11838_MI;
extern MethodInfo m11839_MI;
extern MethodInfo m11840_MI;
static MethodInfo* t85_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m11838_MI,
	&m11839_MI,
	&m11840_MI,
};
static Il2CppInterfaceOffsetPair t85_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t85_0_0_0;
extern Il2CppType t85_1_0_0;
struct t85;
extern Il2CppGenericClass t85_GC;
TypeInfo t85_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "EventFunction`1", "", t85_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t68_TI, &t85_TI, NULL, t85_VT, &EmptyCustomAttributesCache, &t85_TI, &t85_0_0_0, &t85_1_0_0, t85_IOs, &t85_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t85), 0, -1, 0, 0, -1, 258, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t87.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t87_TI;
#include "t87MD.h"

#include "t40.h"
#include "t44.h"
#include "t25.h"
#include "t7.h"
#include "t305.h"
#include "UnityEngine_ArrayTypes.h"
#include "t915.h"
#include "t2323.h"
#include "t2320.h"
#include "t2321.h"
#include "t338.h"
#include "t2328.h"
#include "t2322.h"
extern TypeInfo t25_TI;
extern TypeInfo t44_TI;
extern TypeInfo t21_TI;
extern TypeInfo t305_TI;
extern TypeInfo t585_TI;
extern TypeInfo t1649_TI;
extern TypeInfo t915_TI;
extern TypeInfo t2317_TI;
extern TypeInfo t2323_TI;
extern TypeInfo t40_TI;
extern TypeInfo t304_TI;
extern TypeInfo t2319_TI;
extern TypeInfo t2318_TI;
extern TypeInfo t136_TI;
extern TypeInfo t324_TI;
extern TypeInfo t2320_TI;
extern TypeInfo t338_TI;
extern TypeInfo t2321_TI;
extern TypeInfo t2328_TI;
#include "t305MD.h"
#include "t915MD.h"
#include "t20MD.h"
#include "t29MD.h"
#include "t602MD.h"
#include "t2320MD.h"
#include "t338MD.h"
#include "t2321MD.h"
#include "t2323MD.h"
#include "t2328MD.h"
extern MethodInfo m11887_MI;
extern MethodInfo m11888_MI;
extern MethodInfo m1935_MI;
extern MethodInfo m4198_MI;
extern MethodInfo m20356_MI;
extern MethodInfo m3975_MI;
extern MethodInfo m11873_MI;
extern MethodInfo m1331_MI;
extern MethodInfo m11870_MI;
extern MethodInfo m5952_MI;
extern MethodInfo m11858_MI;
extern MethodInfo m11865_MI;
extern MethodInfo m11871_MI;
extern MethodInfo m11874_MI;
extern MethodInfo m11876_MI;
extern MethodInfo m11859_MI;
extern MethodInfo m11884_MI;
extern MethodInfo m5139_MI;
extern MethodInfo m11885_MI;
extern MethodInfo m27216_MI;
extern MethodInfo m27217_MI;
extern MethodInfo m27218_MI;
extern MethodInfo m27219_MI;
extern MethodInfo m3972_MI;
extern MethodInfo m1428_MI;
extern MethodInfo m11875_MI;
extern MethodInfo m11860_MI;
extern MethodInfo m11861_MI;
extern MethodInfo m11900_MI;
extern MethodInfo m5112_MI;
extern MethodInfo m20358_MI;
extern MethodInfo m11868_MI;
extern MethodInfo m11869_MI;
extern MethodInfo m2950_MI;
extern MethodInfo m11975_MI;
extern MethodInfo m11894_MI;
extern MethodInfo m11872_MI;
extern MethodInfo m11878_MI;
extern MethodInfo m5171_MI;
extern MethodInfo m11981_MI;
extern MethodInfo m20360_MI;
extern MethodInfo m20368_MI;
extern MethodInfo m5951_MI;
struct t20;
struct t20;
#include "mscorlib_ArrayTypes.h"
 void m19788_gshared (t29 * __this, t316** p0, int32_t p1, MethodInfo* method);
#define m19788(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
#define m20356(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
struct t20;
#include "t2326.h"
struct t20;
#include "t2001.h"
 int32_t m10120_gshared (t29 * __this, t316* p0, t29 * p1, int32_t p2, int32_t p3, MethodInfo* method);
#define m10120(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
#define m20358(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
struct t20;
struct t20;
 void m19857_gshared (t29 * __this, t316* p0, int32_t p1, int32_t p2, t29* p3, MethodInfo* method);
#define m19857(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
#define m20360(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
struct t20;
#include "t295.h"
#include "t914.h"
struct t20;
#include "t2181.h"
 void m19964_gshared (t29 * __this, t316* p0, int32_t p1, t2181 * p2, MethodInfo* method);
#define m19964(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)
#define m20368(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)


 t2323  m11870 (t87 * __this, MethodInfo* method){
	{
		t2323  L_0 = {0};
		m11894(&L_0, __this, &m11894_MI);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.List`1<UnityEngine.Transform>
extern Il2CppType t44_0_0_32849;
FieldInfo t87_f0_FieldInfo = 
{
	"DefaultCapacity", &t44_0_0_32849, &t87_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t2317_0_0_1;
FieldInfo t87_f1_FieldInfo = 
{
	"_items", &t2317_0_0_1, &t87_TI, offsetof(t87, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t87_f2_FieldInfo = 
{
	"_size", &t44_0_0_1, &t87_TI, offsetof(t87, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t87_f3_FieldInfo = 
{
	"_version", &t44_0_0_1, &t87_TI, offsetof(t87, f3), &EmptyCustomAttributesCache};
extern Il2CppType t2317_0_0_49;
FieldInfo t87_f4_FieldInfo = 
{
	"EmptyArray", &t2317_0_0_49, &t87_TI, offsetof(t87_SFs, f4), &EmptyCustomAttributesCache};
static FieldInfo* t87_FIs[] =
{
	&t87_f0_FieldInfo,
	&t87_f1_FieldInfo,
	&t87_f2_FieldInfo,
	&t87_f3_FieldInfo,
	&t87_f4_FieldInfo,
	NULL
};
static const int32_t t87_f0_DefaultValueData = 4;
extern Il2CppType t44_0_0_0;
static Il2CppFieldDefaultValueEntry t87_f0_DefaultValue = 
{
	&t87_f0_FieldInfo, { (char*)&t87_f0_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t87_FDVs[] = 
{
	&t87_f0_DefaultValue,
	NULL
};
extern MethodInfo m11851_MI;
static PropertyInfo t87____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t87_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m11851_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11852_MI;
static PropertyInfo t87____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t87_TI, "System.Collections.ICollection.IsSynchronized", &m11852_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11853_MI;
static PropertyInfo t87____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t87_TI, "System.Collections.ICollection.SyncRoot", &m11853_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11854_MI;
static PropertyInfo t87____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t87_TI, "System.Collections.IList.IsFixedSize", &m11854_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11855_MI;
static PropertyInfo t87____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t87_TI, "System.Collections.IList.IsReadOnly", &m11855_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11856_MI;
extern MethodInfo m11857_MI;
static PropertyInfo t87____System_Collections_IList_Item_PropertyInfo = 
{
	&t87_TI, "System.Collections.IList.Item", &m11856_MI, &m11857_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t87____Capacity_PropertyInfo = 
{
	&t87_TI, "Capacity", &m11884_MI, &m11885_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11886_MI;
static PropertyInfo t87____Count_PropertyInfo = 
{
	&t87_TI, "Count", &m11886_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t87____Item_PropertyInfo = 
{
	&t87_TI, "Item", &m11887_MI, &m11888_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t87_PIs[] =
{
	&t87____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t87____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t87____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t87____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t87____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t87____System_Collections_IList_Item_PropertyInfo,
	&t87____Capacity_PropertyInfo,
	&t87____Count_PropertyInfo,
	&t87____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11841_GM;
MethodInfo m11841_MI = 
{
	".ctor", (methodPointerType)&m10537_gshared, &t87_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11841_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t87_m1359_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1359_GM;
MethodInfo m1359_MI = 
{
	".ctor", (methodPointerType)&m10539_gshared, &t87_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t87_m1359_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1359_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11842_GM;
MethodInfo m11842_MI = 
{
	".cctor", (methodPointerType)&m10541_gshared, &t87_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11842_GM};
extern Il2CppType t2318_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11843_GM;
MethodInfo m11843_MI = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator", (methodPointerType)&m10543_gshared, &t87_TI, &t2318_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 27, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11843_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t87_m11844_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11844_GM;
MethodInfo m11844_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10545_gshared, &t87_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t87_m11844_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11844_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11845_GM;
MethodInfo m11845_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10547_gshared, &t87_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11845_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t87_m11846_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11846_GM;
MethodInfo m11846_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10549_gshared, &t87_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t87_m11846_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11846_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t87_m11847_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11847_GM;
MethodInfo m11847_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10551_gshared, &t87_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t87_m11847_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11847_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t87_m11848_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11848_GM;
MethodInfo m11848_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10553_gshared, &t87_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t87_m11848_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11848_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t87_m11849_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11849_GM;
MethodInfo m11849_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10555_gshared, &t87_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t87_m11849_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11849_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t87_m11850_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11850_GM;
MethodInfo m11850_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10557_gshared, &t87_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t87_m11850_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11850_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11851_GM;
MethodInfo m11851_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10559_gshared, &t87_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11851_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11852_GM;
MethodInfo m11852_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10561_gshared, &t87_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11852_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11853_GM;
MethodInfo m11853_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10563_gshared, &t87_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11853_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11854_GM;
MethodInfo m11854_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10565_gshared, &t87_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11854_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11855_GM;
MethodInfo m11855_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10567_gshared, &t87_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11855_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t87_m11856_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11856_GM;
MethodInfo m11856_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10569_gshared, &t87_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t87_m11856_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11856_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t87_m11857_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11857_GM;
MethodInfo m11857_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10571_gshared, &t87_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t87_m11857_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11857_GM};
extern Il2CppType t25_0_0_0;
extern Il2CppType t25_0_0_0;
static ParameterInfo t87_m11858_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11858_GM;
MethodInfo m11858_MI = 
{
	"Add", (methodPointerType)&m10573_gshared, &t87_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t87_m11858_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11858_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t87_m11859_ParameterInfos[] = 
{
	{"newCount", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11859_GM;
MethodInfo m11859_MI = 
{
	"GrowIfNeeded", (methodPointerType)&m10575_gshared, &t87_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t87_m11859_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11859_GM};
extern Il2CppType t304_0_0_0;
extern Il2CppType t304_0_0_0;
static ParameterInfo t87_m11860_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t304_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11860_GM;
MethodInfo m11860_MI = 
{
	"AddCollection", (methodPointerType)&m10577_gshared, &t87_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t87_m11860_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11860_GM};
extern Il2CppType t2319_0_0_0;
extern Il2CppType t2319_0_0_0;
static ParameterInfo t87_m11861_ParameterInfos[] = 
{
	{"enumerable", 0, 134217728, &EmptyCustomAttributesCache, &t2319_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11861_GM;
MethodInfo m11861_MI = 
{
	"AddEnumerable", (methodPointerType)&m10579_gshared, &t87_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t87_m11861_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11861_GM};
extern Il2CppType t2319_0_0_0;
static ParameterInfo t87_m11862_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2319_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11862_GM;
MethodInfo m11862_MI = 
{
	"AddRange", (methodPointerType)&m10581_gshared, &t87_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t87_m11862_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11862_GM};
extern Il2CppType t2320_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11863_GM;
MethodInfo m11863_MI = 
{
	"AsReadOnly", (methodPointerType)&m10583_gshared, &t87_TI, &t2320_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11863_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11864_GM;
MethodInfo m11864_MI = 
{
	"Clear", (methodPointerType)&m10585_gshared, &t87_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11864_GM};
extern Il2CppType t25_0_0_0;
static ParameterInfo t87_m11865_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11865_GM;
MethodInfo m11865_MI = 
{
	"Contains", (methodPointerType)&m10587_gshared, &t87_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t87_m11865_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11865_GM};
extern Il2CppType t2317_0_0_0;
extern Il2CppType t2317_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t87_m11866_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2317_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11866_GM;
MethodInfo m11866_MI = 
{
	"CopyTo", (methodPointerType)&m10589_gshared, &t87_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t87_m11866_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11866_GM};
extern Il2CppType t2321_0_0_0;
extern Il2CppType t2321_0_0_0;
static ParameterInfo t87_m11867_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2321_0_0_0},
};
extern Il2CppType t25_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11867_GM;
MethodInfo m11867_MI = 
{
	"Find", (methodPointerType)&m10591_gshared, &t87_TI, &t25_0_0_0, RuntimeInvoker_t29_t29, t87_m11867_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11867_GM};
extern Il2CppType t2321_0_0_0;
static ParameterInfo t87_m11868_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2321_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11868_GM;
MethodInfo m11868_MI = 
{
	"CheckMatch", (methodPointerType)&m10593_gshared, &t87_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t87_m11868_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11868_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2321_0_0_0;
static ParameterInfo t87_m11869_ParameterInfos[] = 
{
	{"startIndex", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"count", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"match", 2, 134217728, &EmptyCustomAttributesCache, &t2321_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11869_GM;
MethodInfo m11869_MI = 
{
	"GetIndex", (methodPointerType)&m10595_gshared, &t87_TI, &t44_0_0_0, RuntimeInvoker_t44_t44_t44_t29, t87_m11869_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m11869_GM};
extern Il2CppType t2323_0_0_0;
extern void* RuntimeInvoker_t2323 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11870_GM;
MethodInfo m11870_MI = 
{
	"GetEnumerator", (methodPointerType)&m11870, &t87_TI, &t2323_0_0_0, RuntimeInvoker_t2323, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11870_GM};
extern Il2CppType t25_0_0_0;
static ParameterInfo t87_m11871_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11871_GM;
MethodInfo m11871_MI = 
{
	"IndexOf", (methodPointerType)&m10598_gshared, &t87_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t87_m11871_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11871_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t87_m11872_ParameterInfos[] = 
{
	{"start", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"delta", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11872_GM;
MethodInfo m11872_MI = 
{
	"Shift", (methodPointerType)&m10600_gshared, &t87_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t87_m11872_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11872_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t87_m11873_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11873_GM;
MethodInfo m11873_MI = 
{
	"CheckIndex", (methodPointerType)&m10602_gshared, &t87_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t87_m11873_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11873_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t25_0_0_0;
static ParameterInfo t87_m11874_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11874_GM;
MethodInfo m11874_MI = 
{
	"Insert", (methodPointerType)&m10604_gshared, &t87_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t87_m11874_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11874_GM};
extern Il2CppType t2319_0_0_0;
static ParameterInfo t87_m11875_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2319_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11875_GM;
MethodInfo m11875_MI = 
{
	"CheckCollection", (methodPointerType)&m10606_gshared, &t87_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t87_m11875_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11875_GM};
extern Il2CppType t25_0_0_0;
static ParameterInfo t87_m11876_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11876_GM;
MethodInfo m11876_MI = 
{
	"Remove", (methodPointerType)&m10608_gshared, &t87_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t87_m11876_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11876_GM};
extern Il2CppType t2321_0_0_0;
static ParameterInfo t87_m11877_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2321_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11877_GM;
MethodInfo m11877_MI = 
{
	"RemoveAll", (methodPointerType)&m10610_gshared, &t87_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t87_m11877_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11877_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t87_m11878_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11878_GM;
MethodInfo m11878_MI = 
{
	"RemoveAt", (methodPointerType)&m10611_gshared, &t87_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t87_m11878_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11878_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11879_GM;
MethodInfo m11879_MI = 
{
	"Reverse", (methodPointerType)&m10613_gshared, &t87_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11879_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11880_GM;
MethodInfo m11880_MI = 
{
	"Sort", (methodPointerType)&m10615_gshared, &t87_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11880_GM};
extern Il2CppType t2322_0_0_0;
extern Il2CppType t2322_0_0_0;
static ParameterInfo t87_m11881_ParameterInfos[] = 
{
	{"comparison", 0, 134217728, &EmptyCustomAttributesCache, &t2322_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11881_GM;
MethodInfo m11881_MI = 
{
	"Sort", (methodPointerType)&m10617_gshared, &t87_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t87_m11881_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11881_GM};
extern Il2CppType t2317_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11882_GM;
MethodInfo m11882_MI = 
{
	"ToArray", (methodPointerType)&m10619_gshared, &t87_TI, &t2317_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11882_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11883_GM;
MethodInfo m11883_MI = 
{
	"TrimExcess", (methodPointerType)&m10621_gshared, &t87_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11883_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11884_GM;
MethodInfo m11884_MI = 
{
	"get_Capacity", (methodPointerType)&m10623_gshared, &t87_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11884_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t87_m11885_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11885_GM;
MethodInfo m11885_MI = 
{
	"set_Capacity", (methodPointerType)&m10625_gshared, &t87_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t87_m11885_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11885_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11886_GM;
MethodInfo m11886_MI = 
{
	"get_Count", (methodPointerType)&m10626_gshared, &t87_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11886_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t87_m11887_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t25_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11887_GM;
MethodInfo m11887_MI = 
{
	"get_Item", (methodPointerType)&m10627_gshared, &t87_TI, &t25_0_0_0, RuntimeInvoker_t29_t44, t87_m11887_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11887_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t25_0_0_0;
static ParameterInfo t87_m11888_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11888_GM;
MethodInfo m11888_MI = 
{
	"set_Item", (methodPointerType)&m10629_gshared, &t87_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t87_m11888_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 32, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11888_GM};
static MethodInfo* t87_MIs[] =
{
	&m11841_MI,
	&m1359_MI,
	&m11842_MI,
	&m11843_MI,
	&m11844_MI,
	&m11845_MI,
	&m11846_MI,
	&m11847_MI,
	&m11848_MI,
	&m11849_MI,
	&m11850_MI,
	&m11851_MI,
	&m11852_MI,
	&m11853_MI,
	&m11854_MI,
	&m11855_MI,
	&m11856_MI,
	&m11857_MI,
	&m11858_MI,
	&m11859_MI,
	&m11860_MI,
	&m11861_MI,
	&m11862_MI,
	&m11863_MI,
	&m11864_MI,
	&m11865_MI,
	&m11866_MI,
	&m11867_MI,
	&m11868_MI,
	&m11869_MI,
	&m11870_MI,
	&m11871_MI,
	&m11872_MI,
	&m11873_MI,
	&m11874_MI,
	&m11875_MI,
	&m11876_MI,
	&m11877_MI,
	&m11878_MI,
	&m11879_MI,
	&m11880_MI,
	&m11881_MI,
	&m11882_MI,
	&m11883_MI,
	&m11884_MI,
	&m11885_MI,
	&m11886_MI,
	&m11887_MI,
	&m11888_MI,
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m11845_MI;
extern MethodInfo m11844_MI;
extern MethodInfo m11846_MI;
extern MethodInfo m11864_MI;
extern MethodInfo m11847_MI;
extern MethodInfo m11848_MI;
extern MethodInfo m11849_MI;
extern MethodInfo m11850_MI;
extern MethodInfo m11866_MI;
extern MethodInfo m11843_MI;
static MethodInfo* t87_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m11845_MI,
	&m11886_MI,
	&m11852_MI,
	&m11853_MI,
	&m11844_MI,
	&m11854_MI,
	&m11855_MI,
	&m11856_MI,
	&m11857_MI,
	&m11846_MI,
	&m11864_MI,
	&m11847_MI,
	&m11848_MI,
	&m11849_MI,
	&m11850_MI,
	&m11878_MI,
	&m11886_MI,
	&m11851_MI,
	&m11858_MI,
	&m11864_MI,
	&m11865_MI,
	&m11866_MI,
	&m11876_MI,
	&m11843_MI,
	&m11871_MI,
	&m11874_MI,
	&m11878_MI,
	&m11887_MI,
	&m11888_MI,
};
extern TypeInfo t603_TI;
extern TypeInfo t674_TI;
extern TypeInfo t868_TI;
extern TypeInfo t104_TI;
static TypeInfo* t87_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t304_TI,
	&t2319_TI,
	&t104_TI,
};
static Il2CppInterfaceOffsetPair t87_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t304_TI, 20},
	{ &t2319_TI, 27},
	{ &t104_TI, 28},
};
extern TypeInfo t87_TI;
extern TypeInfo t2317_TI;
extern TypeInfo t2323_TI;
extern TypeInfo t25_TI;
extern TypeInfo t304_TI;
extern TypeInfo t2320_TI;
static Il2CppRGCTXData t87_RGCTXData[37] = 
{
	&t87_TI/* Static Usage */,
	&t2317_TI/* Array Usage */,
	&m11870_MI/* Method Usage */,
	&t2323_TI/* Class Usage */,
	&t25_TI/* Class Usage */,
	&m11858_MI/* Method Usage */,
	&m11865_MI/* Method Usage */,
	&m11871_MI/* Method Usage */,
	&m11873_MI/* Method Usage */,
	&m11874_MI/* Method Usage */,
	&m11876_MI/* Method Usage */,
	&m11887_MI/* Method Usage */,
	&m11888_MI/* Method Usage */,
	&m11859_MI/* Method Usage */,
	&m11884_MI/* Method Usage */,
	&m11885_MI/* Method Usage */,
	&m27216_MI/* Method Usage */,
	&m27217_MI/* Method Usage */,
	&m27218_MI/* Method Usage */,
	&m27219_MI/* Method Usage */,
	&m11875_MI/* Method Usage */,
	&t304_TI/* Class Usage */,
	&m11860_MI/* Method Usage */,
	&m11861_MI/* Method Usage */,
	&t2320_TI/* Class Usage */,
	&m11900_MI/* Method Usage */,
	&m20358_MI/* Method Usage */,
	&m11868_MI/* Method Usage */,
	&m11869_MI/* Method Usage */,
	&m11975_MI/* Method Usage */,
	&m11894_MI/* Method Usage */,
	&m11872_MI/* Method Usage */,
	&m11878_MI/* Method Usage */,
	&m11981_MI/* Method Usage */,
	&m20360_MI/* Method Usage */,
	&m20368_MI/* Method Usage */,
	&m20356_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t87_0_0_0;
extern Il2CppType t87_1_0_0;
extern TypeInfo t29_TI;
struct t87;
extern Il2CppGenericClass t87_GC;
extern CustomAttributesCache t1261__CustomAttributeCache;
TypeInfo t87_TI = 
{
	&g_mscorlib_dll_Image, NULL, "List`1", "System.Collections.Generic", t87_MIs, t87_PIs, t87_FIs, NULL, &t29_TI, NULL, NULL, &t87_TI, t87_ITIs, t87_VT, &t1261__CustomAttributeCache, &t87_TI, &t87_0_0_0, &t87_1_0_0, t87_IOs, &t87_GC, NULL, t87_FDVs, NULL, t87_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t87), 0, -1, sizeof(t87_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, true, false, false, 49, 9, 5, 0, 0, 33, 6, 6};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Transform>
static PropertyInfo t304____Count_PropertyInfo = 
{
	&t304_TI, "Count", &m27216_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27220_MI;
static PropertyInfo t304____IsReadOnly_PropertyInfo = 
{
	&t304_TI, "IsReadOnly", &m27220_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t304_PIs[] =
{
	&t304____Count_PropertyInfo,
	&t304____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27216_GM;
MethodInfo m27216_MI = 
{
	"get_Count", NULL, &t304_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27216_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27220_GM;
MethodInfo m27220_MI = 
{
	"get_IsReadOnly", NULL, &t304_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27220_GM};
extern Il2CppType t25_0_0_0;
static ParameterInfo t304_m1364_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1364_GM;
MethodInfo m1364_MI = 
{
	"Add", NULL, &t304_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t304_m1364_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1364_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1362_GM;
MethodInfo m1362_MI = 
{
	"Clear", NULL, &t304_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1362_GM};
extern Il2CppType t25_0_0_0;
static ParameterInfo t304_m27221_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27221_GM;
MethodInfo m27221_MI = 
{
	"Contains", NULL, &t304_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t304_m27221_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27221_GM};
extern Il2CppType t2317_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t304_m27217_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2317_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27217_GM;
MethodInfo m27217_MI = 
{
	"CopyTo", NULL, &t304_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t304_m27217_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27217_GM};
extern Il2CppType t25_0_0_0;
static ParameterInfo t304_m27222_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27222_GM;
MethodInfo m27222_MI = 
{
	"Remove", NULL, &t304_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t304_m27222_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27222_GM};
static MethodInfo* t304_MIs[] =
{
	&m27216_MI,
	&m27220_MI,
	&m1364_MI,
	&m1362_MI,
	&m27221_MI,
	&m27217_MI,
	&m27222_MI,
	NULL
};
static TypeInfo* t304_ITIs[] = 
{
	&t603_TI,
	&t2319_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t304_1_0_0;
struct t304;
extern Il2CppGenericClass t304_GC;
TypeInfo t304_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t304_MIs, t304_PIs, NULL, NULL, NULL, NULL, NULL, &t304_TI, t304_ITIs, NULL, &EmptyCustomAttributesCache, &t304_TI, &t304_0_0_0, &t304_1_0_0, NULL, &t304_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Transform>
extern Il2CppType t2318_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27218_GM;
MethodInfo m27218_MI = 
{
	"GetEnumerator", NULL, &t2319_TI, &t2318_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27218_GM};
static MethodInfo* t2319_MIs[] =
{
	&m27218_MI,
	NULL
};
static TypeInfo* t2319_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2319_1_0_0;
struct t2319;
extern Il2CppGenericClass t2319_GC;
TypeInfo t2319_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t2319_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2319_TI, t2319_ITIs, NULL, &EmptyCustomAttributesCache, &t2319_TI, &t2319_0_0_0, &t2319_1_0_0, NULL, &t2319_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Transform>
static PropertyInfo t2318____Current_PropertyInfo = 
{
	&t2318_TI, "Current", &m27219_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2318_PIs[] =
{
	&t2318____Current_PropertyInfo,
	NULL
};
extern Il2CppType t25_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27219_GM;
MethodInfo m27219_MI = 
{
	"get_Current", NULL, &t2318_TI, &t25_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27219_GM};
static MethodInfo* t2318_MIs[] =
{
	&m27219_MI,
	NULL
};
static TypeInfo* t2318_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2318_0_0_0;
extern Il2CppType t2318_1_0_0;
struct t2318;
extern Il2CppGenericClass t2318_GC;
TypeInfo t2318_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2318_MIs, t2318_PIs, NULL, NULL, NULL, NULL, NULL, &t2318_TI, t2318_ITIs, NULL, &EmptyCustomAttributesCache, &t2318_TI, &t2318_0_0_0, &t2318_1_0_0, NULL, &t2318_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2324.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2324_TI;
#include "t2324MD.h"

extern TypeInfo t914_TI;
#include "t914MD.h"
extern MethodInfo m11893_MI;
extern MethodInfo m3964_MI;
extern MethodInfo m3969_MI;
extern MethodInfo m20345_MI;
struct t20;
struct t20;
 t29 * m19490_gshared (t20 * __this, int32_t p0, MethodInfo* method);
#define m19490(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
#define m20345(__this, p0, method) (t25 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Transform>
extern Il2CppType t20_0_0_1;
FieldInfo t2324_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2324_TI, offsetof(t2324, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2324_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2324_TI, offsetof(t2324, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2324_FIs[] =
{
	&t2324_f0_FieldInfo,
	&t2324_f1_FieldInfo,
	NULL
};
extern MethodInfo m11890_MI;
static PropertyInfo t2324____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2324_TI, "System.Collections.IEnumerator.Current", &m11890_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2324____Current_PropertyInfo = 
{
	&t2324_TI, "Current", &m11893_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2324_PIs[] =
{
	&t2324____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2324____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2324_m11889_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11889_GM;
MethodInfo m11889_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2324_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2324_m11889_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11889_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11890_GM;
MethodInfo m11890_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2324_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11890_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11891_GM;
MethodInfo m11891_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2324_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11891_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11892_GM;
MethodInfo m11892_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2324_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11892_GM};
extern Il2CppType t25_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11893_GM;
MethodInfo m11893_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2324_TI, &t25_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11893_GM};
static MethodInfo* t2324_MIs[] =
{
	&m11889_MI,
	&m11890_MI,
	&m11891_MI,
	&m11892_MI,
	&m11893_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
extern MethodInfo m11892_MI;
extern MethodInfo m11891_MI;
static MethodInfo* t2324_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m11890_MI,
	&m11892_MI,
	&m11891_MI,
	&m11893_MI,
};
static TypeInfo* t2324_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2318_TI,
};
static Il2CppInterfaceOffsetPair t2324_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2318_TI, 7},
};
extern TypeInfo t25_TI;
static Il2CppRGCTXData t2324_RGCTXData[3] = 
{
	&m11893_MI/* Method Usage */,
	&t25_TI/* Class Usage */,
	&m20345_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2324_0_0_0;
extern Il2CppType t2324_1_0_0;
extern TypeInfo t110_TI;
extern Il2CppGenericClass t2324_GC;
extern TypeInfo t20_TI;
TypeInfo t2324_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2324_MIs, t2324_PIs, t2324_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2324_TI, t2324_ITIs, t2324_VT, &EmptyCustomAttributesCache, &t2324_TI, &t2324_0_0_0, &t2324_1_0_0, t2324_IOs, &t2324_GC, NULL, NULL, NULL, t2324_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2324)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Transform>
extern MethodInfo m27223_MI;
extern MethodInfo m27224_MI;
static PropertyInfo t104____Item_PropertyInfo = 
{
	&t104_TI, "Item", &m27223_MI, &m27224_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t104_PIs[] =
{
	&t104____Item_PropertyInfo,
	NULL
};
extern Il2CppType t25_0_0_0;
static ParameterInfo t104_m27225_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27225_GM;
MethodInfo m27225_MI = 
{
	"IndexOf", NULL, &t104_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t104_m27225_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27225_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t25_0_0_0;
static ParameterInfo t104_m27226_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27226_GM;
MethodInfo m27226_MI = 
{
	"Insert", NULL, &t104_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t104_m27226_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27226_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t104_m27227_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27227_GM;
MethodInfo m27227_MI = 
{
	"RemoveAt", NULL, &t104_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t104_m27227_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27227_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t104_m27223_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t25_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27223_GM;
MethodInfo m27223_MI = 
{
	"get_Item", NULL, &t104_TI, &t25_0_0_0, RuntimeInvoker_t29_t44, t104_m27223_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27223_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t25_0_0_0;
static ParameterInfo t104_m27224_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27224_GM;
MethodInfo m27224_MI = 
{
	"set_Item", NULL, &t104_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t104_m27224_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27224_GM};
static MethodInfo* t104_MIs[] =
{
	&m27225_MI,
	&m27226_MI,
	&m27227_MI,
	&m27223_MI,
	&m27224_MI,
	NULL
};
static TypeInfo* t104_ITIs[] = 
{
	&t603_TI,
	&t304_TI,
	&t2319_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t104_0_0_0;
extern Il2CppType t104_1_0_0;
struct t104;
extern Il2CppGenericClass t104_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t104_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t104_MIs, t104_PIs, NULL, NULL, NULL, NULL, NULL, &t104_TI, t104_ITIs, NULL, &t1908__CustomAttributeCache, &t104_TI, &t104_0_0_0, &t104_1_0_0, NULL, &t104_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif

#include "t42.h"
#include "t1101.h"
extern TypeInfo t42_TI;
extern TypeInfo t7_TI;
extern TypeInfo t1101_TI;
#include "t42MD.h"
#include "t1101MD.h"
extern MethodInfo m11897_MI;
extern MethodInfo m3974_MI;
extern MethodInfo m1430_MI;
extern MethodInfo m2998_MI;
extern MethodInfo m5150_MI;


// Metadata Definition System.Collections.Generic.List`1/Enumerator<UnityEngine.Transform>
extern Il2CppType t87_0_0_1;
FieldInfo t2323_f0_FieldInfo = 
{
	"l", &t87_0_0_1, &t2323_TI, offsetof(t2323, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2323_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t2323_TI, offsetof(t2323, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2323_f2_FieldInfo = 
{
	"ver", &t44_0_0_1, &t2323_TI, offsetof(t2323, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t25_0_0_1;
FieldInfo t2323_f3_FieldInfo = 
{
	"current", &t25_0_0_1, &t2323_TI, offsetof(t2323, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2323_FIs[] =
{
	&t2323_f0_FieldInfo,
	&t2323_f1_FieldInfo,
	&t2323_f2_FieldInfo,
	&t2323_f3_FieldInfo,
	NULL
};
extern MethodInfo m11895_MI;
static PropertyInfo t2323____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2323_TI, "System.Collections.IEnumerator.Current", &m11895_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11899_MI;
static PropertyInfo t2323____Current_PropertyInfo = 
{
	&t2323_TI, "Current", &m11899_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2323_PIs[] =
{
	&t2323____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2323____Current_PropertyInfo,
	NULL
};
extern Il2CppType t87_0_0_0;
static ParameterInfo t2323_m11894_ParameterInfos[] = 
{
	{"l", 0, 134217728, &EmptyCustomAttributesCache, &t87_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11894_GM;
MethodInfo m11894_MI = 
{
	".ctor", (methodPointerType)&m10631_gshared, &t2323_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2323_m11894_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11894_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11895_GM;
MethodInfo m11895_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10632_gshared, &t2323_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11895_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11896_GM;
MethodInfo m11896_MI = 
{
	"Dispose", (methodPointerType)&m10633_gshared, &t2323_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11896_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11897_GM;
MethodInfo m11897_MI = 
{
	"VerifyState", (methodPointerType)&m10634_gshared, &t2323_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11897_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11898_GM;
MethodInfo m11898_MI = 
{
	"MoveNext", (methodPointerType)&m10635_gshared, &t2323_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11898_GM};
extern Il2CppType t25_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11899_GM;
MethodInfo m11899_MI = 
{
	"get_Current", (methodPointerType)&m10636_gshared, &t2323_TI, &t25_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11899_GM};
static MethodInfo* t2323_MIs[] =
{
	&m11894_MI,
	&m11895_MI,
	&m11896_MI,
	&m11897_MI,
	&m11898_MI,
	&m11899_MI,
	NULL
};
extern MethodInfo m11898_MI;
extern MethodInfo m11896_MI;
static MethodInfo* t2323_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m11895_MI,
	&m11898_MI,
	&m11896_MI,
	&m11899_MI,
};
static TypeInfo* t2323_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2318_TI,
};
static Il2CppInterfaceOffsetPair t2323_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2318_TI, 7},
};
extern TypeInfo t25_TI;
extern TypeInfo t2323_TI;
static Il2CppRGCTXData t2323_RGCTXData[3] = 
{
	&m11897_MI/* Method Usage */,
	&t25_TI/* Class Usage */,
	&t2323_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2323_0_0_0;
extern Il2CppType t2323_1_0_0;
extern Il2CppGenericClass t2323_GC;
extern TypeInfo t1261_TI;
TypeInfo t2323_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t2323_MIs, t2323_PIs, t2323_FIs, NULL, &t110_TI, NULL, &t1261_TI, &t2323_TI, t2323_ITIs, t2323_VT, &EmptyCustomAttributesCache, &t2323_TI, &t2323_0_0_0, &t2323_1_0_0, t2323_IOs, &t2323_GC, NULL, NULL, NULL, t2323_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2323)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 4, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif

#include "t345.h"
extern TypeInfo t345_TI;
#include "t345MD.h"
#include "t2325MD.h"
extern MethodInfo m11929_MI;
extern MethodInfo m1516_MI;
extern MethodInfo m3960_MI;
extern MethodInfo m4154_MI;
extern MethodInfo m11961_MI;
extern MethodInfo m27221_MI;
extern MethodInfo m27225_MI;


// Metadata Definition System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Transform>
extern Il2CppType t104_0_0_1;
FieldInfo t2320_f0_FieldInfo = 
{
	"list", &t104_0_0_1, &t2320_TI, offsetof(t2320, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2320_FIs[] =
{
	&t2320_f0_FieldInfo,
	NULL
};
extern MethodInfo m11906_MI;
extern MethodInfo m11907_MI;
static PropertyInfo t2320____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo = 
{
	&t2320_TI, "System.Collections.Generic.IList<T>.Item", &m11906_MI, &m11907_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11908_MI;
static PropertyInfo t2320____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2320_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m11908_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11918_MI;
static PropertyInfo t2320____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2320_TI, "System.Collections.ICollection.IsSynchronized", &m11918_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11919_MI;
static PropertyInfo t2320____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2320_TI, "System.Collections.ICollection.SyncRoot", &m11919_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11920_MI;
static PropertyInfo t2320____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2320_TI, "System.Collections.IList.IsFixedSize", &m11920_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11921_MI;
static PropertyInfo t2320____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2320_TI, "System.Collections.IList.IsReadOnly", &m11921_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11922_MI;
extern MethodInfo m11923_MI;
static PropertyInfo t2320____System_Collections_IList_Item_PropertyInfo = 
{
	&t2320_TI, "System.Collections.IList.Item", &m11922_MI, &m11923_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11928_MI;
static PropertyInfo t2320____Count_PropertyInfo = 
{
	&t2320_TI, "Count", &m11928_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2320____Item_PropertyInfo = 
{
	&t2320_TI, "Item", &m11929_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2320_PIs[] =
{
	&t2320____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo,
	&t2320____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2320____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2320____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2320____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2320____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2320____System_Collections_IList_Item_PropertyInfo,
	&t2320____Count_PropertyInfo,
	&t2320____Item_PropertyInfo,
	NULL
};
extern Il2CppType t104_0_0_0;
static ParameterInfo t2320_m11900_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t104_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11900_GM;
MethodInfo m11900_MI = 
{
	".ctor", (methodPointerType)&m10637_gshared, &t2320_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2320_m11900_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11900_GM};
extern Il2CppType t25_0_0_0;
static ParameterInfo t2320_m11901_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11901_GM;
MethodInfo m11901_MI = 
{
	"System.Collections.Generic.ICollection<T>.Add", (methodPointerType)&m10638_gshared, &t2320_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2320_m11901_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11901_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11902_GM;
MethodInfo m11902_MI = 
{
	"System.Collections.Generic.ICollection<T>.Clear", (methodPointerType)&m10639_gshared, &t2320_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11902_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t25_0_0_0;
static ParameterInfo t2320_m11903_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11903_GM;
MethodInfo m11903_MI = 
{
	"System.Collections.Generic.IList<T>.Insert", (methodPointerType)&m10640_gshared, &t2320_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2320_m11903_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11903_GM};
extern Il2CppType t25_0_0_0;
static ParameterInfo t2320_m11904_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11904_GM;
MethodInfo m11904_MI = 
{
	"System.Collections.Generic.ICollection<T>.Remove", (methodPointerType)&m10641_gshared, &t2320_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2320_m11904_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11904_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2320_m11905_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11905_GM;
MethodInfo m11905_MI = 
{
	"System.Collections.Generic.IList<T>.RemoveAt", (methodPointerType)&m10642_gshared, &t2320_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2320_m11905_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11905_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2320_m11906_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t25_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11906_GM;
MethodInfo m11906_MI = 
{
	"System.Collections.Generic.IList<T>.get_Item", (methodPointerType)&m10643_gshared, &t2320_TI, &t25_0_0_0, RuntimeInvoker_t29_t44, t2320_m11906_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11906_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t25_0_0_0;
static ParameterInfo t2320_m11907_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11907_GM;
MethodInfo m11907_MI = 
{
	"System.Collections.Generic.IList<T>.set_Item", (methodPointerType)&m10644_gshared, &t2320_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2320_m11907_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11907_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11908_GM;
MethodInfo m11908_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10645_gshared, &t2320_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11908_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2320_m11909_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11909_GM;
MethodInfo m11909_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10646_gshared, &t2320_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2320_m11909_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11909_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11910_GM;
MethodInfo m11910_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10647_gshared, &t2320_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11910_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2320_m11911_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11911_GM;
MethodInfo m11911_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10648_gshared, &t2320_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2320_m11911_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11911_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11912_GM;
MethodInfo m11912_MI = 
{
	"System.Collections.IList.Clear", (methodPointerType)&m10649_gshared, &t2320_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 14, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11912_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2320_m11913_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11913_GM;
MethodInfo m11913_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10650_gshared, &t2320_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2320_m11913_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11913_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2320_m11914_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11914_GM;
MethodInfo m11914_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10651_gshared, &t2320_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2320_m11914_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11914_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2320_m11915_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11915_GM;
MethodInfo m11915_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10652_gshared, &t2320_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2320_m11915_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11915_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2320_m11916_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11916_GM;
MethodInfo m11916_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10653_gshared, &t2320_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2320_m11916_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11916_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2320_m11917_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11917_GM;
MethodInfo m11917_MI = 
{
	"System.Collections.IList.RemoveAt", (methodPointerType)&m10654_gshared, &t2320_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2320_m11917_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11917_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11918_GM;
MethodInfo m11918_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10655_gshared, &t2320_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11918_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11919_GM;
MethodInfo m11919_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10656_gshared, &t2320_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11919_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11920_GM;
MethodInfo m11920_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10657_gshared, &t2320_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11920_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11921_GM;
MethodInfo m11921_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10658_gshared, &t2320_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11921_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2320_m11922_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11922_GM;
MethodInfo m11922_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10659_gshared, &t2320_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2320_m11922_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11922_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2320_m11923_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11923_GM;
MethodInfo m11923_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10660_gshared, &t2320_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2320_m11923_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11923_GM};
extern Il2CppType t25_0_0_0;
static ParameterInfo t2320_m11924_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11924_GM;
MethodInfo m11924_MI = 
{
	"Contains", (methodPointerType)&m10661_gshared, &t2320_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2320_m11924_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11924_GM};
extern Il2CppType t2317_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2320_m11925_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2317_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11925_GM;
MethodInfo m11925_MI = 
{
	"CopyTo", (methodPointerType)&m10662_gshared, &t2320_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2320_m11925_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11925_GM};
extern Il2CppType t2318_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11926_GM;
MethodInfo m11926_MI = 
{
	"GetEnumerator", (methodPointerType)&m10663_gshared, &t2320_TI, &t2318_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11926_GM};
extern Il2CppType t25_0_0_0;
static ParameterInfo t2320_m11927_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11927_GM;
MethodInfo m11927_MI = 
{
	"IndexOf", (methodPointerType)&m10664_gshared, &t2320_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2320_m11927_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11927_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11928_GM;
MethodInfo m11928_MI = 
{
	"get_Count", (methodPointerType)&m10665_gshared, &t2320_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11928_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2320_m11929_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t25_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11929_GM;
MethodInfo m11929_MI = 
{
	"get_Item", (methodPointerType)&m10666_gshared, &t2320_TI, &t25_0_0_0, RuntimeInvoker_t29_t44, t2320_m11929_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 33, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11929_GM};
static MethodInfo* t2320_MIs[] =
{
	&m11900_MI,
	&m11901_MI,
	&m11902_MI,
	&m11903_MI,
	&m11904_MI,
	&m11905_MI,
	&m11906_MI,
	&m11907_MI,
	&m11908_MI,
	&m11909_MI,
	&m11910_MI,
	&m11911_MI,
	&m11912_MI,
	&m11913_MI,
	&m11914_MI,
	&m11915_MI,
	&m11916_MI,
	&m11917_MI,
	&m11918_MI,
	&m11919_MI,
	&m11920_MI,
	&m11921_MI,
	&m11922_MI,
	&m11923_MI,
	&m11924_MI,
	&m11925_MI,
	&m11926_MI,
	&m11927_MI,
	&m11928_MI,
	&m11929_MI,
	NULL
};
extern MethodInfo m11910_MI;
extern MethodInfo m11909_MI;
extern MethodInfo m11911_MI;
extern MethodInfo m11912_MI;
extern MethodInfo m11913_MI;
extern MethodInfo m11914_MI;
extern MethodInfo m11915_MI;
extern MethodInfo m11916_MI;
extern MethodInfo m11917_MI;
extern MethodInfo m11901_MI;
extern MethodInfo m11902_MI;
extern MethodInfo m11924_MI;
extern MethodInfo m11925_MI;
extern MethodInfo m11904_MI;
extern MethodInfo m11927_MI;
extern MethodInfo m11903_MI;
extern MethodInfo m11905_MI;
extern MethodInfo m11926_MI;
static MethodInfo* t2320_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m11910_MI,
	&m11928_MI,
	&m11918_MI,
	&m11919_MI,
	&m11909_MI,
	&m11920_MI,
	&m11921_MI,
	&m11922_MI,
	&m11923_MI,
	&m11911_MI,
	&m11912_MI,
	&m11913_MI,
	&m11914_MI,
	&m11915_MI,
	&m11916_MI,
	&m11917_MI,
	&m11928_MI,
	&m11908_MI,
	&m11901_MI,
	&m11902_MI,
	&m11924_MI,
	&m11925_MI,
	&m11904_MI,
	&m11927_MI,
	&m11903_MI,
	&m11905_MI,
	&m11906_MI,
	&m11907_MI,
	&m11926_MI,
	&m11929_MI,
};
static TypeInfo* t2320_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t304_TI,
	&t104_TI,
	&t2319_TI,
};
static Il2CppInterfaceOffsetPair t2320_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t304_TI, 20},
	{ &t104_TI, 27},
	{ &t2319_TI, 32},
};
extern TypeInfo t25_TI;
static Il2CppRGCTXData t2320_RGCTXData[9] = 
{
	&m11929_MI/* Method Usage */,
	&m11961_MI/* Method Usage */,
	&t25_TI/* Class Usage */,
	&m27221_MI/* Method Usage */,
	&m27225_MI/* Method Usage */,
	&m27223_MI/* Method Usage */,
	&m27217_MI/* Method Usage */,
	&m27218_MI/* Method Usage */,
	&m27216_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2320_0_0_0;
extern Il2CppType t2320_1_0_0;
struct t2320;
extern Il2CppGenericClass t2320_GC;
extern CustomAttributesCache t1263__CustomAttributeCache;
TypeInfo t2320_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ReadOnlyCollection`1", "System.Collections.ObjectModel", t2320_MIs, t2320_PIs, t2320_FIs, NULL, &t29_TI, NULL, NULL, &t2320_TI, t2320_ITIs, t2320_VT, &t1263__CustomAttributeCache, &t2320_TI, &t2320_0_0_0, &t2320_1_0_0, t2320_IOs, &t2320_GC, NULL, NULL, NULL, t2320_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2320), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 30, 9, 1, 0, 0, 34, 6, 6};
#include "t2325.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2325_TI;

#include "t43.h"
extern MethodInfo m11964_MI;
extern MethodInfo m11965_MI;
extern MethodInfo m11962_MI;
extern MethodInfo m11960_MI;
extern MethodInfo m11841_MI;
extern MethodInfo m9816_MI;
extern MethodInfo m11953_MI;
extern MethodInfo m11963_MI;
extern MethodInfo m11951_MI;
extern MethodInfo m11956_MI;
extern MethodInfo m11947_MI;
extern MethodInfo m1362_MI;
extern MethodInfo m27226_MI;
extern MethodInfo m27227_MI;
extern MethodInfo m1554_MI;
extern MethodInfo m6004_MI;
extern MethodInfo m9815_MI;
extern MethodInfo m9817_MI;


// Metadata Definition System.Collections.ObjectModel.Collection`1<UnityEngine.Transform>
extern Il2CppType t104_0_0_1;
FieldInfo t2325_f0_FieldInfo = 
{
	"list", &t104_0_0_1, &t2325_TI, offsetof(t2325, f0), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t2325_f1_FieldInfo = 
{
	"syncRoot", &t29_0_0_1, &t2325_TI, offsetof(t2325, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2325_FIs[] =
{
	&t2325_f0_FieldInfo,
	&t2325_f1_FieldInfo,
	NULL
};
extern MethodInfo m11931_MI;
static PropertyInfo t2325____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2325_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m11931_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11939_MI;
static PropertyInfo t2325____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2325_TI, "System.Collections.ICollection.IsSynchronized", &m11939_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11940_MI;
static PropertyInfo t2325____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2325_TI, "System.Collections.ICollection.SyncRoot", &m11940_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11941_MI;
static PropertyInfo t2325____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2325_TI, "System.Collections.IList.IsFixedSize", &m11941_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11942_MI;
static PropertyInfo t2325____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2325_TI, "System.Collections.IList.IsReadOnly", &m11942_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11943_MI;
extern MethodInfo m11944_MI;
static PropertyInfo t2325____System_Collections_IList_Item_PropertyInfo = 
{
	&t2325_TI, "System.Collections.IList.Item", &m11943_MI, &m11944_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11957_MI;
static PropertyInfo t2325____Count_PropertyInfo = 
{
	&t2325_TI, "Count", &m11957_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11958_MI;
extern MethodInfo m11959_MI;
static PropertyInfo t2325____Item_PropertyInfo = 
{
	&t2325_TI, "Item", &m11958_MI, &m11959_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2325_PIs[] =
{
	&t2325____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2325____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2325____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2325____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2325____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2325____System_Collections_IList_Item_PropertyInfo,
	&t2325____Count_PropertyInfo,
	&t2325____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11930_GM;
MethodInfo m11930_MI = 
{
	".ctor", (methodPointerType)&m10667_gshared, &t2325_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11930_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11931_GM;
MethodInfo m11931_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10668_gshared, &t2325_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11931_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2325_m11932_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11932_GM;
MethodInfo m11932_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10669_gshared, &t2325_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2325_m11932_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11932_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11933_GM;
MethodInfo m11933_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10670_gshared, &t2325_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11933_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2325_m11934_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11934_GM;
MethodInfo m11934_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10671_gshared, &t2325_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2325_m11934_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11934_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2325_m11935_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11935_GM;
MethodInfo m11935_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10672_gshared, &t2325_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2325_m11935_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11935_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2325_m11936_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11936_GM;
MethodInfo m11936_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10673_gshared, &t2325_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2325_m11936_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11936_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2325_m11937_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11937_GM;
MethodInfo m11937_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10674_gshared, &t2325_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2325_m11937_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11937_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2325_m11938_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11938_GM;
MethodInfo m11938_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10675_gshared, &t2325_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2325_m11938_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11938_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11939_GM;
MethodInfo m11939_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10676_gshared, &t2325_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11939_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11940_GM;
MethodInfo m11940_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10677_gshared, &t2325_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11940_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11941_GM;
MethodInfo m11941_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10678_gshared, &t2325_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11941_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11942_GM;
MethodInfo m11942_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10679_gshared, &t2325_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11942_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2325_m11943_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11943_GM;
MethodInfo m11943_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10680_gshared, &t2325_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2325_m11943_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11943_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2325_m11944_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11944_GM;
MethodInfo m11944_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10681_gshared, &t2325_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2325_m11944_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11944_GM};
extern Il2CppType t25_0_0_0;
static ParameterInfo t2325_m11945_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11945_GM;
MethodInfo m11945_MI = 
{
	"Add", (methodPointerType)&m10682_gshared, &t2325_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2325_m11945_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11945_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11946_GM;
MethodInfo m11946_MI = 
{
	"Clear", (methodPointerType)&m10683_gshared, &t2325_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11946_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11947_GM;
MethodInfo m11947_MI = 
{
	"ClearItems", (methodPointerType)&m10684_gshared, &t2325_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 452, 0, 33, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11947_GM};
extern Il2CppType t25_0_0_0;
static ParameterInfo t2325_m11948_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11948_GM;
MethodInfo m11948_MI = 
{
	"Contains", (methodPointerType)&m10685_gshared, &t2325_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2325_m11948_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11948_GM};
extern Il2CppType t2317_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2325_m11949_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2317_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11949_GM;
MethodInfo m11949_MI = 
{
	"CopyTo", (methodPointerType)&m10686_gshared, &t2325_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2325_m11949_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11949_GM};
extern Il2CppType t2318_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11950_GM;
MethodInfo m11950_MI = 
{
	"GetEnumerator", (methodPointerType)&m10687_gshared, &t2325_TI, &t2318_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11950_GM};
extern Il2CppType t25_0_0_0;
static ParameterInfo t2325_m11951_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11951_GM;
MethodInfo m11951_MI = 
{
	"IndexOf", (methodPointerType)&m10688_gshared, &t2325_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2325_m11951_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11951_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t25_0_0_0;
static ParameterInfo t2325_m11952_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11952_GM;
MethodInfo m11952_MI = 
{
	"Insert", (methodPointerType)&m10689_gshared, &t2325_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2325_m11952_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11952_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t25_0_0_0;
static ParameterInfo t2325_m11953_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11953_GM;
MethodInfo m11953_MI = 
{
	"InsertItem", (methodPointerType)&m10690_gshared, &t2325_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2325_m11953_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 34, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11953_GM};
extern Il2CppType t25_0_0_0;
static ParameterInfo t2325_m11954_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11954_GM;
MethodInfo m11954_MI = 
{
	"Remove", (methodPointerType)&m10691_gshared, &t2325_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2325_m11954_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11954_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2325_m11955_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11955_GM;
MethodInfo m11955_MI = 
{
	"RemoveAt", (methodPointerType)&m10692_gshared, &t2325_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2325_m11955_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11955_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2325_m11956_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11956_GM;
MethodInfo m11956_MI = 
{
	"RemoveItem", (methodPointerType)&m10693_gshared, &t2325_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2325_m11956_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 35, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11956_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11957_GM;
MethodInfo m11957_MI = 
{
	"get_Count", (methodPointerType)&m10694_gshared, &t2325_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11957_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2325_m11958_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t25_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11958_GM;
MethodInfo m11958_MI = 
{
	"get_Item", (methodPointerType)&m10695_gshared, &t2325_TI, &t25_0_0_0, RuntimeInvoker_t29_t44, t2325_m11958_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11958_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t25_0_0_0;
static ParameterInfo t2325_m11959_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11959_GM;
MethodInfo m11959_MI = 
{
	"set_Item", (methodPointerType)&m10696_gshared, &t2325_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2325_m11959_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11959_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t25_0_0_0;
static ParameterInfo t2325_m11960_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11960_GM;
MethodInfo m11960_MI = 
{
	"SetItem", (methodPointerType)&m10697_gshared, &t2325_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2325_m11960_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 36, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11960_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2325_m11961_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11961_GM;
MethodInfo m11961_MI = 
{
	"IsValidItem", (methodPointerType)&m10698_gshared, &t2325_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2325_m11961_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11961_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2325_m11962_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t25_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11962_GM;
MethodInfo m11962_MI = 
{
	"ConvertItem", (methodPointerType)&m10699_gshared, &t2325_TI, &t25_0_0_0, RuntimeInvoker_t29_t29, t2325_m11962_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11962_GM};
extern Il2CppType t104_0_0_0;
static ParameterInfo t2325_m11963_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t104_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11963_GM;
MethodInfo m11963_MI = 
{
	"CheckWritable", (methodPointerType)&m10700_gshared, &t2325_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2325_m11963_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11963_GM};
extern Il2CppType t104_0_0_0;
static ParameterInfo t2325_m11964_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t104_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11964_GM;
MethodInfo m11964_MI = 
{
	"IsSynchronized", (methodPointerType)&m10701_gshared, &t2325_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2325_m11964_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11964_GM};
extern Il2CppType t104_0_0_0;
static ParameterInfo t2325_m11965_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t104_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11965_GM;
MethodInfo m11965_MI = 
{
	"IsFixedSize", (methodPointerType)&m10702_gshared, &t2325_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2325_m11965_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11965_GM};
static MethodInfo* t2325_MIs[] =
{
	&m11930_MI,
	&m11931_MI,
	&m11932_MI,
	&m11933_MI,
	&m11934_MI,
	&m11935_MI,
	&m11936_MI,
	&m11937_MI,
	&m11938_MI,
	&m11939_MI,
	&m11940_MI,
	&m11941_MI,
	&m11942_MI,
	&m11943_MI,
	&m11944_MI,
	&m11945_MI,
	&m11946_MI,
	&m11947_MI,
	&m11948_MI,
	&m11949_MI,
	&m11950_MI,
	&m11951_MI,
	&m11952_MI,
	&m11953_MI,
	&m11954_MI,
	&m11955_MI,
	&m11956_MI,
	&m11957_MI,
	&m11958_MI,
	&m11959_MI,
	&m11960_MI,
	&m11961_MI,
	&m11962_MI,
	&m11963_MI,
	&m11964_MI,
	&m11965_MI,
	NULL
};
extern MethodInfo m11933_MI;
extern MethodInfo m11932_MI;
extern MethodInfo m11934_MI;
extern MethodInfo m11946_MI;
extern MethodInfo m11935_MI;
extern MethodInfo m11936_MI;
extern MethodInfo m11937_MI;
extern MethodInfo m11938_MI;
extern MethodInfo m11955_MI;
extern MethodInfo m11945_MI;
extern MethodInfo m11948_MI;
extern MethodInfo m11949_MI;
extern MethodInfo m11954_MI;
extern MethodInfo m11952_MI;
extern MethodInfo m11950_MI;
static MethodInfo* t2325_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m11933_MI,
	&m11957_MI,
	&m11939_MI,
	&m11940_MI,
	&m11932_MI,
	&m11941_MI,
	&m11942_MI,
	&m11943_MI,
	&m11944_MI,
	&m11934_MI,
	&m11946_MI,
	&m11935_MI,
	&m11936_MI,
	&m11937_MI,
	&m11938_MI,
	&m11955_MI,
	&m11957_MI,
	&m11931_MI,
	&m11945_MI,
	&m11946_MI,
	&m11948_MI,
	&m11949_MI,
	&m11954_MI,
	&m11951_MI,
	&m11952_MI,
	&m11955_MI,
	&m11958_MI,
	&m11959_MI,
	&m11950_MI,
	&m11947_MI,
	&m11953_MI,
	&m11956_MI,
	&m11960_MI,
};
static TypeInfo* t2325_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t304_TI,
	&t104_TI,
	&t2319_TI,
};
static Il2CppInterfaceOffsetPair t2325_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t304_TI, 20},
	{ &t104_TI, 27},
	{ &t2319_TI, 32},
};
extern TypeInfo t87_TI;
extern TypeInfo t25_TI;
static Il2CppRGCTXData t2325_RGCTXData[25] = 
{
	&t87_TI/* Class Usage */,
	&m11841_MI/* Method Usage */,
	&m27220_MI/* Method Usage */,
	&m27218_MI/* Method Usage */,
	&m27216_MI/* Method Usage */,
	&m11962_MI/* Method Usage */,
	&m11953_MI/* Method Usage */,
	&m11961_MI/* Method Usage */,
	&t25_TI/* Class Usage */,
	&m27221_MI/* Method Usage */,
	&m27225_MI/* Method Usage */,
	&m11963_MI/* Method Usage */,
	&m11951_MI/* Method Usage */,
	&m11956_MI/* Method Usage */,
	&m11964_MI/* Method Usage */,
	&m11965_MI/* Method Usage */,
	&m27223_MI/* Method Usage */,
	&m11960_MI/* Method Usage */,
	&m11947_MI/* Method Usage */,
	&m1362_MI/* Method Usage */,
	&m27217_MI/* Method Usage */,
	&m27226_MI/* Method Usage */,
	&m27227_MI/* Method Usage */,
	&m27224_MI/* Method Usage */,
	&t25_0_0_0/* Type Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2325_0_0_0;
extern Il2CppType t2325_1_0_0;
struct t2325;
extern Il2CppGenericClass t2325_GC;
extern CustomAttributesCache t1262__CustomAttributeCache;
TypeInfo t2325_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Collection`1", "System.Collections.ObjectModel", t2325_MIs, t2325_PIs, t2325_FIs, NULL, &t29_TI, NULL, NULL, &t2325_TI, t2325_ITIs, t2325_VT, &t1262__CustomAttributeCache, &t2325_TI, &t2325_0_0_0, &t2325_1_0_0, t2325_IOs, &t2325_GC, NULL, NULL, NULL, t2325_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2325), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 36, 8, 2, 0, 0, 37, 6, 6};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2326_TI;
#include "t2326MD.h"

#include "t1257.h"
#include "t2327.h"
extern TypeInfo t6640_TI;
extern TypeInfo t1257_TI;
extern TypeInfo t537_TI;
extern TypeInfo t2327_TI;
#include "t931MD.h"
#include "t2327MD.h"
extern Il2CppType t6640_0_0_0;
extern Il2CppType t1257_0_0_0;
extern MethodInfo m2981_MI;
extern MethodInfo m2979_MI;
extern MethodInfo m8828_MI;
extern MethodInfo m11971_MI;
extern MethodInfo m27228_MI;
extern MethodInfo m20357_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1<UnityEngine.Transform>
extern Il2CppType t2326_0_0_49;
FieldInfo t2326_f0_FieldInfo = 
{
	"_default", &t2326_0_0_49, &t2326_TI, offsetof(t2326_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2326_FIs[] =
{
	&t2326_f0_FieldInfo,
	NULL
};
extern MethodInfo m11970_MI;
static PropertyInfo t2326____Default_PropertyInfo = 
{
	&t2326_TI, "Default", &m11970_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2326_PIs[] =
{
	&t2326____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11966_GM;
MethodInfo m11966_MI = 
{
	".ctor", (methodPointerType)&m10703_gshared, &t2326_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11966_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11967_GM;
MethodInfo m11967_MI = 
{
	".cctor", (methodPointerType)&m10704_gshared, &t2326_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11967_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2326_m11968_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11968_GM;
MethodInfo m11968_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m10705_gshared, &t2326_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2326_m11968_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11968_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2326_m11969_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11969_GM;
MethodInfo m11969_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m10706_gshared, &t2326_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2326_m11969_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11969_GM};
extern Il2CppType t25_0_0_0;
static ParameterInfo t2326_m27228_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27228_GM;
MethodInfo m27228_MI = 
{
	"GetHashCode", NULL, &t2326_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2326_m27228_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27228_GM};
extern Il2CppType t25_0_0_0;
extern Il2CppType t25_0_0_0;
static ParameterInfo t2326_m20357_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m20357_GM;
MethodInfo m20357_MI = 
{
	"Equals", NULL, &t2326_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2326_m20357_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m20357_GM};
extern Il2CppType t2326_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11970_GM;
MethodInfo m11970_MI = 
{
	"get_Default", (methodPointerType)&m10707_gshared, &t2326_TI, &t2326_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11970_GM};
static MethodInfo* t2326_MIs[] =
{
	&m11966_MI,
	&m11967_MI,
	&m11968_MI,
	&m11969_MI,
	&m27228_MI,
	&m20357_MI,
	&m11970_MI,
	NULL
};
extern MethodInfo m11969_MI;
extern MethodInfo m11968_MI;
static MethodInfo* t2326_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m20357_MI,
	&m27228_MI,
	&m11969_MI,
	&m11968_MI,
	NULL,
	NULL,
};
extern TypeInfo t6641_TI;
extern TypeInfo t734_TI;
static TypeInfo* t2326_ITIs[] = 
{
	&t6641_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t2326_IOs[] = 
{
	{ &t6641_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2326_TI;
extern TypeInfo t2326_TI;
extern TypeInfo t2327_TI;
extern TypeInfo t25_TI;
static Il2CppRGCTXData t2326_RGCTXData[9] = 
{
	&t6640_0_0_0/* Type Usage */,
	&t25_0_0_0/* Type Usage */,
	&t2326_TI/* Class Usage */,
	&t2326_TI/* Static Usage */,
	&t2327_TI/* Class Usage */,
	&m11971_MI/* Method Usage */,
	&t25_TI/* Class Usage */,
	&m27228_MI/* Method Usage */,
	&m20357_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2326_0_0_0;
extern Il2CppType t2326_1_0_0;
struct t2326;
extern Il2CppGenericClass t2326_GC;
TypeInfo t2326_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t2326_MIs, t2326_PIs, t2326_FIs, NULL, &t29_TI, NULL, NULL, &t2326_TI, t2326_ITIs, t2326_VT, &EmptyCustomAttributesCache, &t2326_TI, &t2326_0_0_0, &t2326_1_0_0, t2326_IOs, &t2326_GC, NULL, NULL, NULL, t2326_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2326), 0, -1, sizeof(t2326_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<UnityEngine.Transform>
extern Il2CppType t25_0_0_0;
extern Il2CppType t25_0_0_0;
static ParameterInfo t6641_m27229_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27229_GM;
MethodInfo m27229_MI = 
{
	"Equals", NULL, &t6641_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t6641_m27229_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27229_GM};
extern Il2CppType t25_0_0_0;
static ParameterInfo t6641_m27230_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27230_GM;
MethodInfo m27230_MI = 
{
	"GetHashCode", NULL, &t6641_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6641_m27230_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27230_GM};
static MethodInfo* t6641_MIs[] =
{
	&m27229_MI,
	&m27230_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6641_0_0_0;
extern Il2CppType t6641_1_0_0;
struct t6641;
extern Il2CppGenericClass t6641_GC;
TypeInfo t6641_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t6641_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6641_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6641_TI, &t6641_0_0_0, &t6641_1_0_0, NULL, &t6641_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IEquatable`1<UnityEngine.Transform>
extern Il2CppType t25_0_0_0;
static ParameterInfo t6640_m27231_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27231_GM;
MethodInfo m27231_MI = 
{
	"Equals", NULL, &t6640_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6640_m27231_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27231_GM};
static MethodInfo* t6640_MIs[] =
{
	&m27231_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6640_1_0_0;
struct t6640;
extern Il2CppGenericClass t6640_GC;
TypeInfo t6640_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t6640_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6640_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6640_TI, &t6640_0_0_0, &t6640_1_0_0, NULL, &t6640_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m11966_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Transform>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11971_GM;
MethodInfo m11971_MI = 
{
	".ctor", (methodPointerType)&m10738_gshared, &t2327_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11971_GM};
extern Il2CppType t25_0_0_0;
static ParameterInfo t2327_m11972_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11972_GM;
MethodInfo m11972_MI = 
{
	"GetHashCode", (methodPointerType)&m10739_gshared, &t2327_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2327_m11972_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11972_GM};
extern Il2CppType t25_0_0_0;
extern Il2CppType t25_0_0_0;
static ParameterInfo t2327_m11973_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11973_GM;
MethodInfo m11973_MI = 
{
	"Equals", (methodPointerType)&m10740_gshared, &t2327_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2327_m11973_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11973_GM};
static MethodInfo* t2327_MIs[] =
{
	&m11971_MI,
	&m11972_MI,
	&m11973_MI,
	NULL
};
extern MethodInfo m11973_MI;
extern MethodInfo m11972_MI;
static MethodInfo* t2327_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m11973_MI,
	&m11972_MI,
	&m11969_MI,
	&m11968_MI,
	&m11972_MI,
	&m11973_MI,
};
static Il2CppInterfaceOffsetPair t2327_IOs[] = 
{
	{ &t6641_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2326_TI;
extern TypeInfo t2326_TI;
extern TypeInfo t2327_TI;
extern TypeInfo t25_TI;
extern TypeInfo t25_TI;
static Il2CppRGCTXData t2327_RGCTXData[11] = 
{
	&t6640_0_0_0/* Type Usage */,
	&t25_0_0_0/* Type Usage */,
	&t2326_TI/* Class Usage */,
	&t2326_TI/* Static Usage */,
	&t2327_TI/* Class Usage */,
	&m11971_MI/* Method Usage */,
	&t25_TI/* Class Usage */,
	&m27228_MI/* Method Usage */,
	&m20357_MI/* Method Usage */,
	&m11966_MI/* Method Usage */,
	&t25_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2327_0_0_0;
extern Il2CppType t2327_1_0_0;
struct t2327;
extern Il2CppGenericClass t2327_GC;
extern TypeInfo t1256_TI;
TypeInfo t2327_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2327_MIs, NULL, NULL, NULL, &t2326_TI, NULL, &t1256_TI, &t2327_TI, NULL, t2327_VT, &EmptyCustomAttributesCache, &t2327_TI, &t2327_0_0_0, &t2327_1_0_0, t2327_IOs, &t2327_GC, NULL, NULL, NULL, t2327_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2327), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Predicate`1<UnityEngine.Transform>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2321_m11974_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11974_GM;
MethodInfo m11974_MI = 
{
	".ctor", (methodPointerType)&m10741_gshared, &t2321_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2321_m11974_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11974_GM};
extern Il2CppType t25_0_0_0;
static ParameterInfo t2321_m11975_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11975_GM;
MethodInfo m11975_MI = 
{
	"Invoke", (methodPointerType)&m10742_gshared, &t2321_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2321_m11975_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11975_GM};
extern Il2CppType t25_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2321_m11976_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11976_GM;
MethodInfo m11976_MI = 
{
	"BeginInvoke", (methodPointerType)&m10743_gshared, &t2321_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2321_m11976_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m11976_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2321_m11977_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11977_GM;
MethodInfo m11977_MI = 
{
	"EndInvoke", (methodPointerType)&m10744_gshared, &t2321_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2321_m11977_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11977_GM};
static MethodInfo* t2321_MIs[] =
{
	&m11974_MI,
	&m11975_MI,
	&m11976_MI,
	&m11977_MI,
	NULL
};
extern MethodInfo m11976_MI;
extern MethodInfo m11977_MI;
static MethodInfo* t2321_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m11975_MI,
	&m11976_MI,
	&m11977_MI,
};
static Il2CppInterfaceOffsetPair t2321_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2321_1_0_0;
struct t2321;
extern Il2CppGenericClass t2321_GC;
TypeInfo t2321_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Predicate`1", "System", t2321_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2321_TI, NULL, t2321_VT, &EmptyCustomAttributesCache, &t2321_TI, &t2321_0_0_0, &t2321_1_0_0, t2321_IOs, &t2321_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2321), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif

#include "t1247.h"
#include "t2329.h"
extern TypeInfo t4082_TI;
extern TypeInfo t1247_TI;
extern TypeInfo t2329_TI;
#include "t2329MD.h"
extern Il2CppType t4082_0_0_0;
extern Il2CppType t1247_0_0_0;
extern MethodInfo m11982_MI;
extern MethodInfo m27232_MI;
extern MethodInfo m8852_MI;


// Metadata Definition System.Collections.Generic.Comparer`1<UnityEngine.Transform>
extern Il2CppType t2328_0_0_49;
FieldInfo t2328_f0_FieldInfo = 
{
	"_default", &t2328_0_0_49, &t2328_TI, offsetof(t2328_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2328_FIs[] =
{
	&t2328_f0_FieldInfo,
	NULL
};
static PropertyInfo t2328____Default_PropertyInfo = 
{
	&t2328_TI, "Default", &m11981_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2328_PIs[] =
{
	&t2328____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11978_GM;
MethodInfo m11978_MI = 
{
	".ctor", (methodPointerType)&m10745_gshared, &t2328_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11978_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11979_GM;
MethodInfo m11979_MI = 
{
	".cctor", (methodPointerType)&m10746_gshared, &t2328_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11979_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2328_m11980_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11980_GM;
MethodInfo m11980_MI = 
{
	"System.Collections.IComparer.Compare", (methodPointerType)&m10747_gshared, &t2328_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2328_m11980_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11980_GM};
extern Il2CppType t25_0_0_0;
extern Il2CppType t25_0_0_0;
static ParameterInfo t2328_m27232_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27232_GM;
MethodInfo m27232_MI = 
{
	"Compare", NULL, &t2328_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2328_m27232_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27232_GM};
extern Il2CppType t2328_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11981_GM;
MethodInfo m11981_MI = 
{
	"get_Default", (methodPointerType)&m10748_gshared, &t2328_TI, &t2328_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11981_GM};
static MethodInfo* t2328_MIs[] =
{
	&m11978_MI,
	&m11979_MI,
	&m11980_MI,
	&m27232_MI,
	&m11981_MI,
	NULL
};
extern MethodInfo m11980_MI;
static MethodInfo* t2328_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m27232_MI,
	&m11980_MI,
	NULL,
};
extern TypeInfo t4081_TI;
extern TypeInfo t726_TI;
static TypeInfo* t2328_ITIs[] = 
{
	&t4081_TI,
	&t726_TI,
};
static Il2CppInterfaceOffsetPair t2328_IOs[] = 
{
	{ &t4081_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t2328_TI;
extern TypeInfo t2328_TI;
extern TypeInfo t2329_TI;
extern TypeInfo t25_TI;
static Il2CppRGCTXData t2328_RGCTXData[8] = 
{
	&t4082_0_0_0/* Type Usage */,
	&t25_0_0_0/* Type Usage */,
	&t2328_TI/* Class Usage */,
	&t2328_TI/* Static Usage */,
	&t2329_TI/* Class Usage */,
	&m11982_MI/* Method Usage */,
	&t25_TI/* Class Usage */,
	&m27232_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2328_0_0_0;
extern Il2CppType t2328_1_0_0;
struct t2328;
extern Il2CppGenericClass t2328_GC;
TypeInfo t2328_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparer`1", "System.Collections.Generic", t2328_MIs, t2328_PIs, t2328_FIs, NULL, &t29_TI, NULL, NULL, &t2328_TI, t2328_ITIs, t2328_VT, &EmptyCustomAttributesCache, &t2328_TI, &t2328_0_0_0, &t2328_1_0_0, t2328_IOs, &t2328_GC, NULL, NULL, NULL, t2328_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2328), 0, -1, sizeof(t2328_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 5, 1, 1, 0, 0, 7, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IComparer`1<UnityEngine.Transform>
extern Il2CppType t25_0_0_0;
extern Il2CppType t25_0_0_0;
static ParameterInfo t4081_m20365_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m20365_GM;
MethodInfo m20365_MI = 
{
	"Compare", NULL, &t4081_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t4081_m20365_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m20365_GM};
static MethodInfo* t4081_MIs[] =
{
	&m20365_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4081_0_0_0;
extern Il2CppType t4081_1_0_0;
struct t4081;
extern Il2CppGenericClass t4081_GC;
TypeInfo t4081_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparer`1", "System.Collections.Generic", t4081_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4081_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4081_TI, &t4081_0_0_0, &t4081_1_0_0, NULL, &t4081_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IComparable`1<UnityEngine.Transform>
extern Il2CppType t25_0_0_0;
static ParameterInfo t4082_m20366_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m20366_GM;
MethodInfo m20366_MI = 
{
	"CompareTo", NULL, &t4082_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t4082_m20366_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m20366_GM};
static MethodInfo* t4082_MIs[] =
{
	&m20366_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4082_1_0_0;
struct t4082;
extern Il2CppGenericClass t4082_GC;
TypeInfo t4082_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t4082_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4082_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4082_TI, &t4082_0_0_0, &t4082_1_0_0, NULL, &t4082_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo t290_TI;
extern MethodInfo m11978_MI;
extern MethodInfo m20366_MI;
extern MethodInfo m9672_MI;


// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Transform>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11982_GM;
MethodInfo m11982_MI = 
{
	".ctor", (methodPointerType)&m10749_gshared, &t2329_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11982_GM};
extern Il2CppType t25_0_0_0;
extern Il2CppType t25_0_0_0;
static ParameterInfo t2329_m11983_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11983_GM;
MethodInfo m11983_MI = 
{
	"Compare", (methodPointerType)&m10750_gshared, &t2329_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2329_m11983_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11983_GM};
static MethodInfo* t2329_MIs[] =
{
	&m11982_MI,
	&m11983_MI,
	NULL
};
extern MethodInfo m11983_MI;
static MethodInfo* t2329_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m11983_MI,
	&m11980_MI,
	&m11983_MI,
};
static Il2CppInterfaceOffsetPair t2329_IOs[] = 
{
	{ &t4081_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t2328_TI;
extern TypeInfo t2328_TI;
extern TypeInfo t2329_TI;
extern TypeInfo t25_TI;
extern TypeInfo t25_TI;
extern TypeInfo t4082_TI;
static Il2CppRGCTXData t2329_RGCTXData[12] = 
{
	&t4082_0_0_0/* Type Usage */,
	&t25_0_0_0/* Type Usage */,
	&t2328_TI/* Class Usage */,
	&t2328_TI/* Static Usage */,
	&t2329_TI/* Class Usage */,
	&m11982_MI/* Method Usage */,
	&t25_TI/* Class Usage */,
	&m27232_MI/* Method Usage */,
	&m11978_MI/* Method Usage */,
	&t25_TI/* Class Usage */,
	&t4082_TI/* Class Usage */,
	&m20366_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2329_0_0_0;
extern Il2CppType t2329_1_0_0;
struct t2329;
extern Il2CppGenericClass t2329_GC;
extern TypeInfo t1246_TI;
TypeInfo t2329_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2329_MIs, NULL, NULL, NULL, &t2328_TI, NULL, &t1246_TI, &t2329_TI, NULL, t2329_VT, &EmptyCustomAttributesCache, &t2329_TI, &t2329_0_0_0, &t2329_1_0_0, t2329_IOs, &t2329_GC, NULL, NULL, NULL, t2329_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2329), 0, -1, 0, 0, -1, 1048835, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 7, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2322_TI;
#include "t2322MD.h"



// Metadata Definition System.Comparison`1<UnityEngine.Transform>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2322_m11984_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11984_GM;
MethodInfo m11984_MI = 
{
	".ctor", (methodPointerType)&m10796_gshared, &t2322_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2322_m11984_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11984_GM};
extern Il2CppType t25_0_0_0;
extern Il2CppType t25_0_0_0;
static ParameterInfo t2322_m11985_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11985_GM;
MethodInfo m11985_MI = 
{
	"Invoke", (methodPointerType)&m10797_gshared, &t2322_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2322_m11985_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11985_GM};
extern Il2CppType t25_0_0_0;
extern Il2CppType t25_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2322_m11986_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11986_GM;
MethodInfo m11986_MI = 
{
	"BeginInvoke", (methodPointerType)&m10798_gshared, &t2322_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t2322_m11986_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m11986_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2322_m11987_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11987_GM;
MethodInfo m11987_MI = 
{
	"EndInvoke", (methodPointerType)&m10799_gshared, &t2322_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2322_m11987_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11987_GM};
static MethodInfo* t2322_MIs[] =
{
	&m11984_MI,
	&m11985_MI,
	&m11986_MI,
	&m11987_MI,
	NULL
};
extern MethodInfo m11985_MI;
extern MethodInfo m11986_MI;
extern MethodInfo m11987_MI;
static MethodInfo* t2322_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m11985_MI,
	&m11986_MI,
	&m11987_MI,
};
static Il2CppInterfaceOffsetPair t2322_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2322_1_0_0;
struct t2322;
extern Il2CppGenericClass t2322_GC;
TypeInfo t2322_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparison`1", "System", t2322_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2322_TI, NULL, t2322_VT, &EmptyCustomAttributesCache, &t2322_TI, &t2322_0_0_0, &t2322_1_0_0, t2322_IOs, &t2322_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2322), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4084_TI;

#include "t106.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.MoveDirection>
extern MethodInfo m27233_MI;
static PropertyInfo t4084____Current_PropertyInfo = 
{
	&t4084_TI, "Current", &m27233_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4084_PIs[] =
{
	&t4084____Current_PropertyInfo,
	NULL
};
extern Il2CppType t106_0_0_0;
extern void* RuntimeInvoker_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27233_GM;
MethodInfo m27233_MI = 
{
	"get_Current", NULL, &t4084_TI, &t106_0_0_0, RuntimeInvoker_t106, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27233_GM};
static MethodInfo* t4084_MIs[] =
{
	&m27233_MI,
	NULL
};
static TypeInfo* t4084_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4084_0_0_0;
extern Il2CppType t4084_1_0_0;
struct t4084;
extern Il2CppGenericClass t4084_GC;
TypeInfo t4084_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4084_MIs, t4084_PIs, NULL, NULL, NULL, NULL, NULL, &t4084_TI, t4084_ITIs, NULL, &EmptyCustomAttributesCache, &t4084_TI, &t4084_0_0_0, &t4084_1_0_0, NULL, &t4084_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2330.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2330_TI;
#include "t2330MD.h"

extern TypeInfo t106_TI;
extern MethodInfo m11992_MI;
extern MethodInfo m20371_MI;
struct t20;
 int32_t m20371 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m11988_MI;
 void m11988 (t2330 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m11989_MI;
 t29 * m11989 (t2330 * __this, MethodInfo* method){
	{
		int32_t L_0 = m11992(__this, &m11992_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t106_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m11990_MI;
 void m11990 (t2330 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m11991_MI;
 bool m11991 (t2330 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m11992 (t2330 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m20371(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m20371_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.MoveDirection>
extern Il2CppType t20_0_0_1;
FieldInfo t2330_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2330_TI, offsetof(t2330, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2330_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2330_TI, offsetof(t2330, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2330_FIs[] =
{
	&t2330_f0_FieldInfo,
	&t2330_f1_FieldInfo,
	NULL
};
static PropertyInfo t2330____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2330_TI, "System.Collections.IEnumerator.Current", &m11989_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2330____Current_PropertyInfo = 
{
	&t2330_TI, "Current", &m11992_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2330_PIs[] =
{
	&t2330____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2330____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2330_m11988_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11988_GM;
MethodInfo m11988_MI = 
{
	".ctor", (methodPointerType)&m11988, &t2330_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2330_m11988_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11988_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11989_GM;
MethodInfo m11989_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m11989, &t2330_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11989_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11990_GM;
MethodInfo m11990_MI = 
{
	"Dispose", (methodPointerType)&m11990, &t2330_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11990_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11991_GM;
MethodInfo m11991_MI = 
{
	"MoveNext", (methodPointerType)&m11991, &t2330_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11991_GM};
extern Il2CppType t106_0_0_0;
extern void* RuntimeInvoker_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11992_GM;
MethodInfo m11992_MI = 
{
	"get_Current", (methodPointerType)&m11992, &t2330_TI, &t106_0_0_0, RuntimeInvoker_t106, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11992_GM};
static MethodInfo* t2330_MIs[] =
{
	&m11988_MI,
	&m11989_MI,
	&m11990_MI,
	&m11991_MI,
	&m11992_MI,
	NULL
};
static MethodInfo* t2330_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m11989_MI,
	&m11991_MI,
	&m11990_MI,
	&m11992_MI,
};
static TypeInfo* t2330_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4084_TI,
};
static Il2CppInterfaceOffsetPair t2330_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4084_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2330_0_0_0;
extern Il2CppType t2330_1_0_0;
extern Il2CppGenericClass t2330_GC;
TypeInfo t2330_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2330_MIs, t2330_PIs, t2330_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2330_TI, t2330_ITIs, t2330_VT, &EmptyCustomAttributesCache, &t2330_TI, &t2330_0_0_0, &t2330_1_0_0, t2330_IOs, &t2330_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2330)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5229_TI;

#include "UnityEngine.UI_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.MoveDirection>
extern MethodInfo m27234_MI;
static PropertyInfo t5229____Count_PropertyInfo = 
{
	&t5229_TI, "Count", &m27234_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27235_MI;
static PropertyInfo t5229____IsReadOnly_PropertyInfo = 
{
	&t5229_TI, "IsReadOnly", &m27235_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5229_PIs[] =
{
	&t5229____Count_PropertyInfo,
	&t5229____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27234_GM;
MethodInfo m27234_MI = 
{
	"get_Count", NULL, &t5229_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27234_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27235_GM;
MethodInfo m27235_MI = 
{
	"get_IsReadOnly", NULL, &t5229_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27235_GM};
extern Il2CppType t106_0_0_0;
extern Il2CppType t106_0_0_0;
static ParameterInfo t5229_m27236_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t106_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27236_GM;
MethodInfo m27236_MI = 
{
	"Add", NULL, &t5229_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5229_m27236_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27236_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27237_GM;
MethodInfo m27237_MI = 
{
	"Clear", NULL, &t5229_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27237_GM};
extern Il2CppType t106_0_0_0;
static ParameterInfo t5229_m27238_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t106_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27238_GM;
MethodInfo m27238_MI = 
{
	"Contains", NULL, &t5229_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5229_m27238_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27238_GM};
extern Il2CppType t3820_0_0_0;
extern Il2CppType t3820_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5229_m27239_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3820_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27239_GM;
MethodInfo m27239_MI = 
{
	"CopyTo", NULL, &t5229_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5229_m27239_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27239_GM};
extern Il2CppType t106_0_0_0;
static ParameterInfo t5229_m27240_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t106_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27240_GM;
MethodInfo m27240_MI = 
{
	"Remove", NULL, &t5229_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5229_m27240_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27240_GM};
static MethodInfo* t5229_MIs[] =
{
	&m27234_MI,
	&m27235_MI,
	&m27236_MI,
	&m27237_MI,
	&m27238_MI,
	&m27239_MI,
	&m27240_MI,
	NULL
};
extern TypeInfo t5231_TI;
static TypeInfo* t5229_ITIs[] = 
{
	&t603_TI,
	&t5231_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5229_0_0_0;
extern Il2CppType t5229_1_0_0;
struct t5229;
extern Il2CppGenericClass t5229_GC;
TypeInfo t5229_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5229_MIs, t5229_PIs, NULL, NULL, NULL, NULL, NULL, &t5229_TI, t5229_ITIs, NULL, &EmptyCustomAttributesCache, &t5229_TI, &t5229_0_0_0, &t5229_1_0_0, NULL, &t5229_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.MoveDirection>
extern Il2CppType t4084_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27241_GM;
MethodInfo m27241_MI = 
{
	"GetEnumerator", NULL, &t5231_TI, &t4084_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27241_GM};
static MethodInfo* t5231_MIs[] =
{
	&m27241_MI,
	NULL
};
static TypeInfo* t5231_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5231_0_0_0;
extern Il2CppType t5231_1_0_0;
struct t5231;
extern Il2CppGenericClass t5231_GC;
TypeInfo t5231_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5231_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5231_TI, t5231_ITIs, NULL, &EmptyCustomAttributesCache, &t5231_TI, &t5231_0_0_0, &t5231_1_0_0, NULL, &t5231_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5230_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.MoveDirection>
extern MethodInfo m27242_MI;
extern MethodInfo m27243_MI;
static PropertyInfo t5230____Item_PropertyInfo = 
{
	&t5230_TI, "Item", &m27242_MI, &m27243_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5230_PIs[] =
{
	&t5230____Item_PropertyInfo,
	NULL
};
extern Il2CppType t106_0_0_0;
static ParameterInfo t5230_m27244_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t106_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27244_GM;
MethodInfo m27244_MI = 
{
	"IndexOf", NULL, &t5230_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5230_m27244_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27244_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t106_0_0_0;
static ParameterInfo t5230_m27245_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t106_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27245_GM;
MethodInfo m27245_MI = 
{
	"Insert", NULL, &t5230_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5230_m27245_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27245_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5230_m27246_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27246_GM;
MethodInfo m27246_MI = 
{
	"RemoveAt", NULL, &t5230_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5230_m27246_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27246_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5230_m27242_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t106_0_0_0;
extern void* RuntimeInvoker_t106_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27242_GM;
MethodInfo m27242_MI = 
{
	"get_Item", NULL, &t5230_TI, &t106_0_0_0, RuntimeInvoker_t106_t44, t5230_m27242_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27242_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t106_0_0_0;
static ParameterInfo t5230_m27243_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t106_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27243_GM;
MethodInfo m27243_MI = 
{
	"set_Item", NULL, &t5230_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5230_m27243_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27243_GM};
static MethodInfo* t5230_MIs[] =
{
	&m27244_MI,
	&m27245_MI,
	&m27246_MI,
	&m27242_MI,
	&m27243_MI,
	NULL
};
static TypeInfo* t5230_ITIs[] = 
{
	&t603_TI,
	&t5229_TI,
	&t5231_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5230_0_0_0;
extern Il2CppType t5230_1_0_0;
struct t5230;
extern Il2CppGenericClass t5230_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5230_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5230_MIs, t5230_PIs, NULL, NULL, NULL, NULL, NULL, &t5230_TI, t5230_ITIs, NULL, &t1908__CustomAttributeCache, &t5230_TI, &t5230_0_0_0, &t5230_1_0_0, NULL, &t5230_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2331.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2331_TI;
#include "t2331MD.h"

#include "t41.h"
#include "t557.h"
#include "t55.h"
#include "t2332.h"
extern TypeInfo t316_TI;
extern TypeInfo t55_TI;
extern TypeInfo t2332_TI;
#include "t2332MD.h"
extern MethodInfo m11995_MI;
extern MethodInfo m11997_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.EventSystems.UIBehaviour>
extern Il2CppType t316_0_0_33;
FieldInfo t2331_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2331_TI, offsetof(t2331, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2331_FIs[] =
{
	&t2331_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t55_0_0_0;
extern Il2CppType t55_0_0_0;
static ParameterInfo t2331_m11993_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t55_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11993_GM;
MethodInfo m11993_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2331_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2331_m11993_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m11993_GM};
extern Il2CppType t316_0_0_0;
extern Il2CppType t316_0_0_0;
static ParameterInfo t2331_m11994_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11994_GM;
MethodInfo m11994_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2331_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2331_m11994_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11994_GM};
static MethodInfo* t2331_MIs[] =
{
	&m11993_MI,
	&m11994_MI,
	NULL
};
extern MethodInfo m11994_MI;
extern MethodInfo m11998_MI;
static MethodInfo* t2331_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m11994_MI,
	&m11998_MI,
};
extern Il2CppType t2333_0_0_0;
extern TypeInfo t2333_TI;
extern MethodInfo m20381_MI;
extern TypeInfo t55_TI;
extern MethodInfo m12000_MI;
extern TypeInfo t55_TI;
static Il2CppRGCTXData t2331_RGCTXData[8] = 
{
	&t2333_0_0_0/* Type Usage */,
	&t2333_TI/* Class Usage */,
	&m20381_MI/* Method Usage */,
	&t55_TI/* Class Usage */,
	&m12000_MI/* Method Usage */,
	&m11995_MI/* Method Usage */,
	&t55_TI/* Class Usage */,
	&m11997_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2331_0_0_0;
extern Il2CppType t2331_1_0_0;
struct t2331;
extern Il2CppGenericClass t2331_GC;
TypeInfo t2331_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2331_MIs, NULL, t2331_FIs, NULL, &t2332_TI, NULL, NULL, &t2331_TI, NULL, t2331_VT, &EmptyCustomAttributesCache, &t2331_TI, &t2331_0_0_0, &t2331_1_0_0, NULL, &t2331_GC, NULL, NULL, NULL, t2331_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2331), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2333.h"
#include "t353.h"
extern TypeInfo t2333_TI;
#include "t556MD.h"
#include "t353MD.h"
#include "t2333MD.h"
extern MethodInfo m2790_MI;
extern MethodInfo m2957_MI;
extern MethodInfo m1597_MI;
extern MethodInfo m2789_MI;
extern MethodInfo m2791_MI;
extern MethodInfo m2953_MI;
extern MethodInfo m2951_MI;
struct t556;
#include "t556.h"
struct t556;
 void m19555_gshared (t29 * __this, t29 * p0, MethodInfo* method);
#define m19555(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)
#define m20381(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.UIBehaviour>
extern Il2CppType t2333_0_0_1;
FieldInfo t2332_f0_FieldInfo = 
{
	"Delegate", &t2333_0_0_1, &t2332_TI, offsetof(t2332, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2332_FIs[] =
{
	&t2332_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2332_m11995_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11995_GM;
MethodInfo m11995_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2332_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2332_m11995_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11995_GM};
extern Il2CppType t2333_0_0_0;
static ParameterInfo t2332_m11996_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2333_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11996_GM;
MethodInfo m11996_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2332_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2332_m11996_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11996_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2332_m11997_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11997_GM;
MethodInfo m11997_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2332_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2332_m11997_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11997_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2332_m11998_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11998_GM;
MethodInfo m11998_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2332_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2332_m11998_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11998_GM};
static MethodInfo* t2332_MIs[] =
{
	&m11995_MI,
	&m11996_MI,
	&m11997_MI,
	&m11998_MI,
	NULL
};
static MethodInfo* t2332_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m11997_MI,
	&m11998_MI,
};
extern TypeInfo t2333_TI;
extern TypeInfo t55_TI;
static Il2CppRGCTXData t2332_RGCTXData[5] = 
{
	&t2333_0_0_0/* Type Usage */,
	&t2333_TI/* Class Usage */,
	&m20381_MI/* Method Usage */,
	&t55_TI/* Class Usage */,
	&m12000_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2332_0_0_0;
extern Il2CppType t2332_1_0_0;
extern TypeInfo t556_TI;
struct t2332;
extern Il2CppGenericClass t2332_GC;
TypeInfo t2332_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2332_MIs, NULL, t2332_FIs, NULL, &t556_TI, NULL, NULL, &t2332_TI, NULL, t2332_VT, &EmptyCustomAttributesCache, &t2332_TI, &t2332_0_0_0, &t2332_1_0_0, NULL, &t2332_GC, NULL, NULL, NULL, t2332_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2332), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.UIBehaviour>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2333_m11999_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11999_GM;
MethodInfo m11999_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2333_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2333_m11999_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11999_GM};
extern Il2CppType t55_0_0_0;
static ParameterInfo t2333_m12000_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t55_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12000_GM;
MethodInfo m12000_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2333_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2333_m12000_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12000_GM};
extern Il2CppType t55_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2333_m12001_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t55_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12001_GM;
MethodInfo m12001_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2333_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2333_m12001_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m12001_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2333_m12002_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12002_GM;
MethodInfo m12002_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2333_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2333_m12002_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12002_GM};
static MethodInfo* t2333_MIs[] =
{
	&m11999_MI,
	&m12000_MI,
	&m12001_MI,
	&m12002_MI,
	NULL
};
extern MethodInfo m12001_MI;
extern MethodInfo m12002_MI;
static MethodInfo* t2333_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m12000_MI,
	&m12001_MI,
	&m12002_MI,
};
static Il2CppInterfaceOffsetPair t2333_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2333_1_0_0;
struct t2333;
extern Il2CppGenericClass t2333_GC;
TypeInfo t2333_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2333_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2333_TI, NULL, t2333_VT, &EmptyCustomAttributesCache, &t2333_TI, &t2333_0_0_0, &t2333_1_0_0, t2333_IOs, &t2333_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2333), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4086_TI;

#include "t111.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.PointerEventData/InputButton>
extern MethodInfo m27247_MI;
static PropertyInfo t4086____Current_PropertyInfo = 
{
	&t4086_TI, "Current", &m27247_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4086_PIs[] =
{
	&t4086____Current_PropertyInfo,
	NULL
};
extern Il2CppType t111_0_0_0;
extern void* RuntimeInvoker_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27247_GM;
MethodInfo m27247_MI = 
{
	"get_Current", NULL, &t4086_TI, &t111_0_0_0, RuntimeInvoker_t111, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27247_GM};
static MethodInfo* t4086_MIs[] =
{
	&m27247_MI,
	NULL
};
static TypeInfo* t4086_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4086_0_0_0;
extern Il2CppType t4086_1_0_0;
struct t4086;
extern Il2CppGenericClass t4086_GC;
TypeInfo t4086_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4086_MIs, t4086_PIs, NULL, NULL, NULL, NULL, NULL, &t4086_TI, t4086_ITIs, NULL, &EmptyCustomAttributesCache, &t4086_TI, &t4086_0_0_0, &t4086_1_0_0, NULL, &t4086_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2334.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2334_TI;
#include "t2334MD.h"

extern TypeInfo t111_TI;
extern MethodInfo m12007_MI;
extern MethodInfo m20383_MI;
struct t20;
 int32_t m20383 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m12003_MI;
 void m12003 (t2334 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m12004_MI;
 t29 * m12004 (t2334 * __this, MethodInfo* method){
	{
		int32_t L_0 = m12007(__this, &m12007_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t111_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m12005_MI;
 void m12005 (t2334 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m12006_MI;
 bool m12006 (t2334 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m12007 (t2334 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m20383(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m20383_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.PointerEventData/InputButton>
extern Il2CppType t20_0_0_1;
FieldInfo t2334_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2334_TI, offsetof(t2334, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2334_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2334_TI, offsetof(t2334, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2334_FIs[] =
{
	&t2334_f0_FieldInfo,
	&t2334_f1_FieldInfo,
	NULL
};
static PropertyInfo t2334____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2334_TI, "System.Collections.IEnumerator.Current", &m12004_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2334____Current_PropertyInfo = 
{
	&t2334_TI, "Current", &m12007_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2334_PIs[] =
{
	&t2334____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2334____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2334_m12003_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12003_GM;
MethodInfo m12003_MI = 
{
	".ctor", (methodPointerType)&m12003, &t2334_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2334_m12003_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12003_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12004_GM;
MethodInfo m12004_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m12004, &t2334_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12004_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12005_GM;
MethodInfo m12005_MI = 
{
	"Dispose", (methodPointerType)&m12005, &t2334_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12005_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12006_GM;
MethodInfo m12006_MI = 
{
	"MoveNext", (methodPointerType)&m12006, &t2334_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12006_GM};
extern Il2CppType t111_0_0_0;
extern void* RuntimeInvoker_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12007_GM;
MethodInfo m12007_MI = 
{
	"get_Current", (methodPointerType)&m12007, &t2334_TI, &t111_0_0_0, RuntimeInvoker_t111, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12007_GM};
static MethodInfo* t2334_MIs[] =
{
	&m12003_MI,
	&m12004_MI,
	&m12005_MI,
	&m12006_MI,
	&m12007_MI,
	NULL
};
static MethodInfo* t2334_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m12004_MI,
	&m12006_MI,
	&m12005_MI,
	&m12007_MI,
};
static TypeInfo* t2334_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4086_TI,
};
static Il2CppInterfaceOffsetPair t2334_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4086_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2334_0_0_0;
extern Il2CppType t2334_1_0_0;
extern Il2CppGenericClass t2334_GC;
TypeInfo t2334_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2334_MIs, t2334_PIs, t2334_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2334_TI, t2334_ITIs, t2334_VT, &EmptyCustomAttributesCache, &t2334_TI, &t2334_0_0_0, &t2334_1_0_0, t2334_IOs, &t2334_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2334)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5232_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.PointerEventData/InputButton>
extern MethodInfo m27248_MI;
static PropertyInfo t5232____Count_PropertyInfo = 
{
	&t5232_TI, "Count", &m27248_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27249_MI;
static PropertyInfo t5232____IsReadOnly_PropertyInfo = 
{
	&t5232_TI, "IsReadOnly", &m27249_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5232_PIs[] =
{
	&t5232____Count_PropertyInfo,
	&t5232____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27248_GM;
MethodInfo m27248_MI = 
{
	"get_Count", NULL, &t5232_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27248_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27249_GM;
MethodInfo m27249_MI = 
{
	"get_IsReadOnly", NULL, &t5232_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27249_GM};
extern Il2CppType t111_0_0_0;
extern Il2CppType t111_0_0_0;
static ParameterInfo t5232_m27250_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t111_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27250_GM;
MethodInfo m27250_MI = 
{
	"Add", NULL, &t5232_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5232_m27250_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27250_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27251_GM;
MethodInfo m27251_MI = 
{
	"Clear", NULL, &t5232_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27251_GM};
extern Il2CppType t111_0_0_0;
static ParameterInfo t5232_m27252_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t111_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27252_GM;
MethodInfo m27252_MI = 
{
	"Contains", NULL, &t5232_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5232_m27252_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27252_GM};
extern Il2CppType t3821_0_0_0;
extern Il2CppType t3821_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5232_m27253_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3821_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27253_GM;
MethodInfo m27253_MI = 
{
	"CopyTo", NULL, &t5232_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5232_m27253_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27253_GM};
extern Il2CppType t111_0_0_0;
static ParameterInfo t5232_m27254_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t111_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27254_GM;
MethodInfo m27254_MI = 
{
	"Remove", NULL, &t5232_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5232_m27254_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27254_GM};
static MethodInfo* t5232_MIs[] =
{
	&m27248_MI,
	&m27249_MI,
	&m27250_MI,
	&m27251_MI,
	&m27252_MI,
	&m27253_MI,
	&m27254_MI,
	NULL
};
extern TypeInfo t5234_TI;
static TypeInfo* t5232_ITIs[] = 
{
	&t603_TI,
	&t5234_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5232_0_0_0;
extern Il2CppType t5232_1_0_0;
struct t5232;
extern Il2CppGenericClass t5232_GC;
TypeInfo t5232_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5232_MIs, t5232_PIs, NULL, NULL, NULL, NULL, NULL, &t5232_TI, t5232_ITIs, NULL, &EmptyCustomAttributesCache, &t5232_TI, &t5232_0_0_0, &t5232_1_0_0, NULL, &t5232_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.PointerEventData/InputButton>
extern Il2CppType t4086_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27255_GM;
MethodInfo m27255_MI = 
{
	"GetEnumerator", NULL, &t5234_TI, &t4086_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27255_GM};
static MethodInfo* t5234_MIs[] =
{
	&m27255_MI,
	NULL
};
static TypeInfo* t5234_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5234_0_0_0;
extern Il2CppType t5234_1_0_0;
struct t5234;
extern Il2CppGenericClass t5234_GC;
TypeInfo t5234_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5234_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5234_TI, t5234_ITIs, NULL, &EmptyCustomAttributesCache, &t5234_TI, &t5234_0_0_0, &t5234_1_0_0, NULL, &t5234_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5233_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.PointerEventData/InputButton>
extern MethodInfo m27256_MI;
extern MethodInfo m27257_MI;
static PropertyInfo t5233____Item_PropertyInfo = 
{
	&t5233_TI, "Item", &m27256_MI, &m27257_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5233_PIs[] =
{
	&t5233____Item_PropertyInfo,
	NULL
};
extern Il2CppType t111_0_0_0;
static ParameterInfo t5233_m27258_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t111_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27258_GM;
MethodInfo m27258_MI = 
{
	"IndexOf", NULL, &t5233_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5233_m27258_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27258_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t111_0_0_0;
static ParameterInfo t5233_m27259_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t111_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27259_GM;
MethodInfo m27259_MI = 
{
	"Insert", NULL, &t5233_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5233_m27259_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27259_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5233_m27260_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27260_GM;
MethodInfo m27260_MI = 
{
	"RemoveAt", NULL, &t5233_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5233_m27260_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27260_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5233_m27256_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t111_0_0_0;
extern void* RuntimeInvoker_t111_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27256_GM;
MethodInfo m27256_MI = 
{
	"get_Item", NULL, &t5233_TI, &t111_0_0_0, RuntimeInvoker_t111_t44, t5233_m27256_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27256_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t111_0_0_0;
static ParameterInfo t5233_m27257_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t111_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27257_GM;
MethodInfo m27257_MI = 
{
	"set_Item", NULL, &t5233_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5233_m27257_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27257_GM};
static MethodInfo* t5233_MIs[] =
{
	&m27258_MI,
	&m27259_MI,
	&m27260_MI,
	&m27256_MI,
	&m27257_MI,
	NULL
};
static TypeInfo* t5233_ITIs[] = 
{
	&t603_TI,
	&t5232_TI,
	&t5234_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5233_0_0_0;
extern Il2CppType t5233_1_0_0;
struct t5233;
extern Il2CppGenericClass t5233_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5233_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5233_MIs, t5233_PIs, NULL, NULL, NULL, NULL, NULL, &t5233_TI, t5233_ITIs, NULL, &t1908__CustomAttributeCache, &t5233_TI, &t5233_0_0_0, &t5233_1_0_0, NULL, &t5233_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4088_TI;

#include "t112.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.PointerEventData/FramePressState>
extern MethodInfo m27261_MI;
static PropertyInfo t4088____Current_PropertyInfo = 
{
	&t4088_TI, "Current", &m27261_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4088_PIs[] =
{
	&t4088____Current_PropertyInfo,
	NULL
};
extern Il2CppType t112_0_0_0;
extern void* RuntimeInvoker_t112 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27261_GM;
MethodInfo m27261_MI = 
{
	"get_Current", NULL, &t4088_TI, &t112_0_0_0, RuntimeInvoker_t112, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27261_GM};
static MethodInfo* t4088_MIs[] =
{
	&m27261_MI,
	NULL
};
static TypeInfo* t4088_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4088_0_0_0;
extern Il2CppType t4088_1_0_0;
struct t4088;
extern Il2CppGenericClass t4088_GC;
TypeInfo t4088_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4088_MIs, t4088_PIs, NULL, NULL, NULL, NULL, NULL, &t4088_TI, t4088_ITIs, NULL, &EmptyCustomAttributesCache, &t4088_TI, &t4088_0_0_0, &t4088_1_0_0, NULL, &t4088_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2335.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2335_TI;
#include "t2335MD.h"

extern TypeInfo t112_TI;
extern MethodInfo m12012_MI;
extern MethodInfo m20394_MI;
struct t20;
 int32_t m20394 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m12008_MI;
 void m12008 (t2335 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m12009_MI;
 t29 * m12009 (t2335 * __this, MethodInfo* method){
	{
		int32_t L_0 = m12012(__this, &m12012_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t112_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m12010_MI;
 void m12010 (t2335 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m12011_MI;
 bool m12011 (t2335 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m12012 (t2335 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m20394(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m20394_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.PointerEventData/FramePressState>
extern Il2CppType t20_0_0_1;
FieldInfo t2335_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2335_TI, offsetof(t2335, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2335_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2335_TI, offsetof(t2335, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2335_FIs[] =
{
	&t2335_f0_FieldInfo,
	&t2335_f1_FieldInfo,
	NULL
};
static PropertyInfo t2335____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2335_TI, "System.Collections.IEnumerator.Current", &m12009_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2335____Current_PropertyInfo = 
{
	&t2335_TI, "Current", &m12012_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2335_PIs[] =
{
	&t2335____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2335____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2335_m12008_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12008_GM;
MethodInfo m12008_MI = 
{
	".ctor", (methodPointerType)&m12008, &t2335_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2335_m12008_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12008_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12009_GM;
MethodInfo m12009_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m12009, &t2335_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12009_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12010_GM;
MethodInfo m12010_MI = 
{
	"Dispose", (methodPointerType)&m12010, &t2335_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12010_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12011_GM;
MethodInfo m12011_MI = 
{
	"MoveNext", (methodPointerType)&m12011, &t2335_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12011_GM};
extern Il2CppType t112_0_0_0;
extern void* RuntimeInvoker_t112 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12012_GM;
MethodInfo m12012_MI = 
{
	"get_Current", (methodPointerType)&m12012, &t2335_TI, &t112_0_0_0, RuntimeInvoker_t112, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12012_GM};
static MethodInfo* t2335_MIs[] =
{
	&m12008_MI,
	&m12009_MI,
	&m12010_MI,
	&m12011_MI,
	&m12012_MI,
	NULL
};
static MethodInfo* t2335_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m12009_MI,
	&m12011_MI,
	&m12010_MI,
	&m12012_MI,
};
static TypeInfo* t2335_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4088_TI,
};
static Il2CppInterfaceOffsetPair t2335_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4088_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2335_0_0_0;
extern Il2CppType t2335_1_0_0;
extern Il2CppGenericClass t2335_GC;
TypeInfo t2335_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2335_MIs, t2335_PIs, t2335_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2335_TI, t2335_ITIs, t2335_VT, &EmptyCustomAttributesCache, &t2335_TI, &t2335_0_0_0, &t2335_1_0_0, t2335_IOs, &t2335_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2335)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5235_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.PointerEventData/FramePressState>
extern MethodInfo m27262_MI;
static PropertyInfo t5235____Count_PropertyInfo = 
{
	&t5235_TI, "Count", &m27262_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27263_MI;
static PropertyInfo t5235____IsReadOnly_PropertyInfo = 
{
	&t5235_TI, "IsReadOnly", &m27263_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5235_PIs[] =
{
	&t5235____Count_PropertyInfo,
	&t5235____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27262_GM;
MethodInfo m27262_MI = 
{
	"get_Count", NULL, &t5235_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27262_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27263_GM;
MethodInfo m27263_MI = 
{
	"get_IsReadOnly", NULL, &t5235_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27263_GM};
extern Il2CppType t112_0_0_0;
extern Il2CppType t112_0_0_0;
static ParameterInfo t5235_m27264_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t112_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27264_GM;
MethodInfo m27264_MI = 
{
	"Add", NULL, &t5235_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5235_m27264_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27264_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27265_GM;
MethodInfo m27265_MI = 
{
	"Clear", NULL, &t5235_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27265_GM};
extern Il2CppType t112_0_0_0;
static ParameterInfo t5235_m27266_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t112_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27266_GM;
MethodInfo m27266_MI = 
{
	"Contains", NULL, &t5235_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5235_m27266_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27266_GM};
extern Il2CppType t3822_0_0_0;
extern Il2CppType t3822_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5235_m27267_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3822_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27267_GM;
MethodInfo m27267_MI = 
{
	"CopyTo", NULL, &t5235_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5235_m27267_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27267_GM};
extern Il2CppType t112_0_0_0;
static ParameterInfo t5235_m27268_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t112_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27268_GM;
MethodInfo m27268_MI = 
{
	"Remove", NULL, &t5235_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5235_m27268_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27268_GM};
static MethodInfo* t5235_MIs[] =
{
	&m27262_MI,
	&m27263_MI,
	&m27264_MI,
	&m27265_MI,
	&m27266_MI,
	&m27267_MI,
	&m27268_MI,
	NULL
};
extern TypeInfo t5237_TI;
static TypeInfo* t5235_ITIs[] = 
{
	&t603_TI,
	&t5237_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5235_0_0_0;
extern Il2CppType t5235_1_0_0;
struct t5235;
extern Il2CppGenericClass t5235_GC;
TypeInfo t5235_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5235_MIs, t5235_PIs, NULL, NULL, NULL, NULL, NULL, &t5235_TI, t5235_ITIs, NULL, &EmptyCustomAttributesCache, &t5235_TI, &t5235_0_0_0, &t5235_1_0_0, NULL, &t5235_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.PointerEventData/FramePressState>
extern Il2CppType t4088_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27269_GM;
MethodInfo m27269_MI = 
{
	"GetEnumerator", NULL, &t5237_TI, &t4088_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27269_GM};
static MethodInfo* t5237_MIs[] =
{
	&m27269_MI,
	NULL
};
static TypeInfo* t5237_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5237_0_0_0;
extern Il2CppType t5237_1_0_0;
struct t5237;
extern Il2CppGenericClass t5237_GC;
TypeInfo t5237_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5237_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5237_TI, t5237_ITIs, NULL, &EmptyCustomAttributesCache, &t5237_TI, &t5237_0_0_0, &t5237_1_0_0, NULL, &t5237_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5236_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.PointerEventData/FramePressState>
extern MethodInfo m27270_MI;
extern MethodInfo m27271_MI;
static PropertyInfo t5236____Item_PropertyInfo = 
{
	&t5236_TI, "Item", &m27270_MI, &m27271_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5236_PIs[] =
{
	&t5236____Item_PropertyInfo,
	NULL
};
extern Il2CppType t112_0_0_0;
static ParameterInfo t5236_m27272_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t112_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27272_GM;
MethodInfo m27272_MI = 
{
	"IndexOf", NULL, &t5236_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5236_m27272_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27272_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t112_0_0_0;
static ParameterInfo t5236_m27273_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t112_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27273_GM;
MethodInfo m27273_MI = 
{
	"Insert", NULL, &t5236_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5236_m27273_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27273_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5236_m27274_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27274_GM;
MethodInfo m27274_MI = 
{
	"RemoveAt", NULL, &t5236_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5236_m27274_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27274_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5236_m27270_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t112_0_0_0;
extern void* RuntimeInvoker_t112_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27270_GM;
MethodInfo m27270_MI = 
{
	"get_Item", NULL, &t5236_TI, &t112_0_0_0, RuntimeInvoker_t112_t44, t5236_m27270_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27270_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t112_0_0_0;
static ParameterInfo t5236_m27271_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t112_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27271_GM;
MethodInfo m27271_MI = 
{
	"set_Item", NULL, &t5236_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5236_m27271_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27271_GM};
static MethodInfo* t5236_MIs[] =
{
	&m27272_MI,
	&m27273_MI,
	&m27274_MI,
	&m27270_MI,
	&m27271_MI,
	NULL
};
static TypeInfo* t5236_ITIs[] = 
{
	&t603_TI,
	&t5235_TI,
	&t5237_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5236_0_0_0;
extern Il2CppType t5236_1_0_0;
struct t5236;
extern Il2CppGenericClass t5236_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5236_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5236_MIs, t5236_PIs, NULL, NULL, NULL, NULL, NULL, &t5236_TI, t5236_ITIs, NULL, &t1908__CustomAttributeCache, &t5236_TI, &t5236_0_0_0, &t5236_1_0_0, NULL, &t5236_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2336.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2336_TI;
#include "t2336MD.h"

#include "t52.h"
#include "t2337.h"
extern TypeInfo t52_TI;
extern TypeInfo t2337_TI;
#include "t2337MD.h"
extern MethodInfo m12015_MI;
extern MethodInfo m12017_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.EventSystems.BaseInputModule>
extern Il2CppType t316_0_0_33;
FieldInfo t2336_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2336_TI, offsetof(t2336, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2336_FIs[] =
{
	&t2336_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t52_0_0_0;
extern Il2CppType t52_0_0_0;
static ParameterInfo t2336_m12013_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12013_GM;
MethodInfo m12013_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2336_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2336_m12013_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m12013_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2336_m12014_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12014_GM;
MethodInfo m12014_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2336_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2336_m12014_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12014_GM};
static MethodInfo* t2336_MIs[] =
{
	&m12013_MI,
	&m12014_MI,
	NULL
};
extern MethodInfo m12014_MI;
extern MethodInfo m12018_MI;
static MethodInfo* t2336_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12014_MI,
	&m12018_MI,
};
extern Il2CppType t2338_0_0_0;
extern TypeInfo t2338_TI;
extern MethodInfo m20404_MI;
extern TypeInfo t52_TI;
extern MethodInfo m12020_MI;
extern TypeInfo t52_TI;
static Il2CppRGCTXData t2336_RGCTXData[8] = 
{
	&t2338_0_0_0/* Type Usage */,
	&t2338_TI/* Class Usage */,
	&m20404_MI/* Method Usage */,
	&t52_TI/* Class Usage */,
	&m12020_MI/* Method Usage */,
	&m12015_MI/* Method Usage */,
	&t52_TI/* Class Usage */,
	&m12017_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2336_0_0_0;
extern Il2CppType t2336_1_0_0;
struct t2336;
extern Il2CppGenericClass t2336_GC;
TypeInfo t2336_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2336_MIs, NULL, t2336_FIs, NULL, &t2337_TI, NULL, NULL, &t2336_TI, NULL, t2336_VT, &EmptyCustomAttributesCache, &t2336_TI, &t2336_0_0_0, &t2336_1_0_0, NULL, &t2336_GC, NULL, NULL, NULL, t2336_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2336), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2338.h"
extern TypeInfo t2338_TI;
#include "t2338MD.h"
struct t556;
#define m20404(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.BaseInputModule>
extern Il2CppType t2338_0_0_1;
FieldInfo t2337_f0_FieldInfo = 
{
	"Delegate", &t2338_0_0_1, &t2337_TI, offsetof(t2337, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2337_FIs[] =
{
	&t2337_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2337_m12015_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12015_GM;
MethodInfo m12015_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2337_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2337_m12015_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12015_GM};
extern Il2CppType t2338_0_0_0;
static ParameterInfo t2337_m12016_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2338_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12016_GM;
MethodInfo m12016_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2337_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2337_m12016_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12016_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2337_m12017_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12017_GM;
MethodInfo m12017_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2337_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2337_m12017_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12017_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2337_m12018_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12018_GM;
MethodInfo m12018_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2337_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2337_m12018_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12018_GM};
static MethodInfo* t2337_MIs[] =
{
	&m12015_MI,
	&m12016_MI,
	&m12017_MI,
	&m12018_MI,
	NULL
};
static MethodInfo* t2337_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12017_MI,
	&m12018_MI,
};
extern TypeInfo t2338_TI;
extern TypeInfo t52_TI;
static Il2CppRGCTXData t2337_RGCTXData[5] = 
{
	&t2338_0_0_0/* Type Usage */,
	&t2338_TI/* Class Usage */,
	&m20404_MI/* Method Usage */,
	&t52_TI/* Class Usage */,
	&m12020_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2337_0_0_0;
extern Il2CppType t2337_1_0_0;
struct t2337;
extern Il2CppGenericClass t2337_GC;
TypeInfo t2337_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2337_MIs, NULL, t2337_FIs, NULL, &t556_TI, NULL, NULL, &t2337_TI, NULL, t2337_VT, &EmptyCustomAttributesCache, &t2337_TI, &t2337_0_0_0, &t2337_1_0_0, NULL, &t2337_GC, NULL, NULL, NULL, t2337_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2337), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.BaseInputModule>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2338_m12019_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12019_GM;
MethodInfo m12019_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2338_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2338_m12019_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12019_GM};
extern Il2CppType t52_0_0_0;
static ParameterInfo t2338_m12020_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12020_GM;
MethodInfo m12020_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2338_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2338_m12020_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12020_GM};
extern Il2CppType t52_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2338_m12021_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12021_GM;
MethodInfo m12021_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2338_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2338_m12021_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m12021_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2338_m12022_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12022_GM;
MethodInfo m12022_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2338_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2338_m12022_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12022_GM};
static MethodInfo* t2338_MIs[] =
{
	&m12019_MI,
	&m12020_MI,
	&m12021_MI,
	&m12022_MI,
	NULL
};
extern MethodInfo m12021_MI;
extern MethodInfo m12022_MI;
static MethodInfo* t2338_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m12020_MI,
	&m12021_MI,
	&m12022_MI,
};
static Il2CppInterfaceOffsetPair t2338_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2338_1_0_0;
struct t2338;
extern Il2CppGenericClass t2338_GC;
TypeInfo t2338_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2338_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2338_TI, NULL, t2338_VT, &EmptyCustomAttributesCache, &t2338_TI, &t2338_0_0_0, &t2338_1_0_0, t2338_IOs, &t2338_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2338), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4090_TI;

#include "t117.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.PointerInputModule>
extern MethodInfo m27275_MI;
static PropertyInfo t4090____Current_PropertyInfo = 
{
	&t4090_TI, "Current", &m27275_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4090_PIs[] =
{
	&t4090____Current_PropertyInfo,
	NULL
};
extern Il2CppType t117_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27275_GM;
MethodInfo m27275_MI = 
{
	"get_Current", NULL, &t4090_TI, &t117_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27275_GM};
static MethodInfo* t4090_MIs[] =
{
	&m27275_MI,
	NULL
};
static TypeInfo* t4090_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4090_0_0_0;
extern Il2CppType t4090_1_0_0;
struct t4090;
extern Il2CppGenericClass t4090_GC;
TypeInfo t4090_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4090_MIs, t4090_PIs, NULL, NULL, NULL, NULL, NULL, &t4090_TI, t4090_ITIs, NULL, &EmptyCustomAttributesCache, &t4090_TI, &t4090_0_0_0, &t4090_1_0_0, NULL, &t4090_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2339.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2339_TI;
#include "t2339MD.h"

extern TypeInfo t117_TI;
extern MethodInfo m12027_MI;
extern MethodInfo m20410_MI;
struct t20;
#define m20410(__this, p0, method) (t117 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.PointerInputModule>
extern Il2CppType t20_0_0_1;
FieldInfo t2339_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2339_TI, offsetof(t2339, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2339_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2339_TI, offsetof(t2339, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2339_FIs[] =
{
	&t2339_f0_FieldInfo,
	&t2339_f1_FieldInfo,
	NULL
};
extern MethodInfo m12024_MI;
static PropertyInfo t2339____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2339_TI, "System.Collections.IEnumerator.Current", &m12024_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2339____Current_PropertyInfo = 
{
	&t2339_TI, "Current", &m12027_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2339_PIs[] =
{
	&t2339____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2339____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2339_m12023_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12023_GM;
MethodInfo m12023_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2339_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2339_m12023_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12023_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12024_GM;
MethodInfo m12024_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2339_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12024_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12025_GM;
MethodInfo m12025_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2339_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12025_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12026_GM;
MethodInfo m12026_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2339_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12026_GM};
extern Il2CppType t117_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12027_GM;
MethodInfo m12027_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2339_TI, &t117_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12027_GM};
static MethodInfo* t2339_MIs[] =
{
	&m12023_MI,
	&m12024_MI,
	&m12025_MI,
	&m12026_MI,
	&m12027_MI,
	NULL
};
extern MethodInfo m12026_MI;
extern MethodInfo m12025_MI;
static MethodInfo* t2339_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m12024_MI,
	&m12026_MI,
	&m12025_MI,
	&m12027_MI,
};
static TypeInfo* t2339_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4090_TI,
};
static Il2CppInterfaceOffsetPair t2339_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4090_TI, 7},
};
extern TypeInfo t117_TI;
static Il2CppRGCTXData t2339_RGCTXData[3] = 
{
	&m12027_MI/* Method Usage */,
	&t117_TI/* Class Usage */,
	&m20410_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2339_0_0_0;
extern Il2CppType t2339_1_0_0;
extern Il2CppGenericClass t2339_GC;
TypeInfo t2339_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2339_MIs, t2339_PIs, t2339_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2339_TI, t2339_ITIs, t2339_VT, &EmptyCustomAttributesCache, &t2339_TI, &t2339_0_0_0, &t2339_1_0_0, t2339_IOs, &t2339_GC, NULL, NULL, NULL, t2339_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2339)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5238_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.PointerInputModule>
extern MethodInfo m27276_MI;
static PropertyInfo t5238____Count_PropertyInfo = 
{
	&t5238_TI, "Count", &m27276_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27277_MI;
static PropertyInfo t5238____IsReadOnly_PropertyInfo = 
{
	&t5238_TI, "IsReadOnly", &m27277_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5238_PIs[] =
{
	&t5238____Count_PropertyInfo,
	&t5238____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27276_GM;
MethodInfo m27276_MI = 
{
	"get_Count", NULL, &t5238_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27276_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27277_GM;
MethodInfo m27277_MI = 
{
	"get_IsReadOnly", NULL, &t5238_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27277_GM};
extern Il2CppType t117_0_0_0;
extern Il2CppType t117_0_0_0;
static ParameterInfo t5238_m27278_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t117_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27278_GM;
MethodInfo m27278_MI = 
{
	"Add", NULL, &t5238_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5238_m27278_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27278_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27279_GM;
MethodInfo m27279_MI = 
{
	"Clear", NULL, &t5238_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27279_GM};
extern Il2CppType t117_0_0_0;
static ParameterInfo t5238_m27280_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t117_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27280_GM;
MethodInfo m27280_MI = 
{
	"Contains", NULL, &t5238_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5238_m27280_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27280_GM};
extern Il2CppType t3823_0_0_0;
extern Il2CppType t3823_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5238_m27281_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3823_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27281_GM;
MethodInfo m27281_MI = 
{
	"CopyTo", NULL, &t5238_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5238_m27281_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27281_GM};
extern Il2CppType t117_0_0_0;
static ParameterInfo t5238_m27282_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t117_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27282_GM;
MethodInfo m27282_MI = 
{
	"Remove", NULL, &t5238_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5238_m27282_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27282_GM};
static MethodInfo* t5238_MIs[] =
{
	&m27276_MI,
	&m27277_MI,
	&m27278_MI,
	&m27279_MI,
	&m27280_MI,
	&m27281_MI,
	&m27282_MI,
	NULL
};
extern TypeInfo t5240_TI;
static TypeInfo* t5238_ITIs[] = 
{
	&t603_TI,
	&t5240_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5238_0_0_0;
extern Il2CppType t5238_1_0_0;
struct t5238;
extern Il2CppGenericClass t5238_GC;
TypeInfo t5238_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5238_MIs, t5238_PIs, NULL, NULL, NULL, NULL, NULL, &t5238_TI, t5238_ITIs, NULL, &EmptyCustomAttributesCache, &t5238_TI, &t5238_0_0_0, &t5238_1_0_0, NULL, &t5238_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.PointerInputModule>
extern Il2CppType t4090_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27283_GM;
MethodInfo m27283_MI = 
{
	"GetEnumerator", NULL, &t5240_TI, &t4090_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27283_GM};
static MethodInfo* t5240_MIs[] =
{
	&m27283_MI,
	NULL
};
static TypeInfo* t5240_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5240_0_0_0;
extern Il2CppType t5240_1_0_0;
struct t5240;
extern Il2CppGenericClass t5240_GC;
TypeInfo t5240_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5240_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5240_TI, t5240_ITIs, NULL, &EmptyCustomAttributesCache, &t5240_TI, &t5240_0_0_0, &t5240_1_0_0, NULL, &t5240_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5239_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.PointerInputModule>
extern MethodInfo m27284_MI;
extern MethodInfo m27285_MI;
static PropertyInfo t5239____Item_PropertyInfo = 
{
	&t5239_TI, "Item", &m27284_MI, &m27285_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5239_PIs[] =
{
	&t5239____Item_PropertyInfo,
	NULL
};
extern Il2CppType t117_0_0_0;
static ParameterInfo t5239_m27286_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t117_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27286_GM;
MethodInfo m27286_MI = 
{
	"IndexOf", NULL, &t5239_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5239_m27286_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27286_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t117_0_0_0;
static ParameterInfo t5239_m27287_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t117_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27287_GM;
MethodInfo m27287_MI = 
{
	"Insert", NULL, &t5239_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5239_m27287_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27287_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5239_m27288_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27288_GM;
MethodInfo m27288_MI = 
{
	"RemoveAt", NULL, &t5239_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5239_m27288_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27288_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5239_m27284_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t117_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27284_GM;
MethodInfo m27284_MI = 
{
	"get_Item", NULL, &t5239_TI, &t117_0_0_0, RuntimeInvoker_t29_t44, t5239_m27284_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27284_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t117_0_0_0;
static ParameterInfo t5239_m27285_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t117_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27285_GM;
MethodInfo m27285_MI = 
{
	"set_Item", NULL, &t5239_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5239_m27285_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27285_GM};
static MethodInfo* t5239_MIs[] =
{
	&m27286_MI,
	&m27287_MI,
	&m27288_MI,
	&m27284_MI,
	&m27285_MI,
	NULL
};
static TypeInfo* t5239_ITIs[] = 
{
	&t603_TI,
	&t5238_TI,
	&t5240_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5239_0_0_0;
extern Il2CppType t5239_1_0_0;
struct t5239;
extern Il2CppGenericClass t5239_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5239_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5239_MIs, t5239_PIs, NULL, NULL, NULL, NULL, NULL, &t5239_TI, t5239_ITIs, NULL, &t1908__CustomAttributeCache, &t5239_TI, &t5239_0_0_0, &t5239_1_0_0, NULL, &t5239_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2340.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2340_TI;
#include "t2340MD.h"

#include "t2341.h"
extern TypeInfo t2341_TI;
#include "t2341MD.h"
extern MethodInfo m12030_MI;
extern MethodInfo m12032_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.EventSystems.PointerInputModule>
extern Il2CppType t316_0_0_33;
FieldInfo t2340_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2340_TI, offsetof(t2340, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2340_FIs[] =
{
	&t2340_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t117_0_0_0;
static ParameterInfo t2340_m12028_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t117_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12028_GM;
MethodInfo m12028_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2340_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2340_m12028_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m12028_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2340_m12029_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12029_GM;
MethodInfo m12029_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2340_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2340_m12029_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12029_GM};
static MethodInfo* t2340_MIs[] =
{
	&m12028_MI,
	&m12029_MI,
	NULL
};
extern MethodInfo m12029_MI;
extern MethodInfo m12033_MI;
static MethodInfo* t2340_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12029_MI,
	&m12033_MI,
};
extern Il2CppType t2342_0_0_0;
extern TypeInfo t2342_TI;
extern MethodInfo m20420_MI;
extern TypeInfo t117_TI;
extern MethodInfo m12035_MI;
extern TypeInfo t117_TI;
static Il2CppRGCTXData t2340_RGCTXData[8] = 
{
	&t2342_0_0_0/* Type Usage */,
	&t2342_TI/* Class Usage */,
	&m20420_MI/* Method Usage */,
	&t117_TI/* Class Usage */,
	&m12035_MI/* Method Usage */,
	&m12030_MI/* Method Usage */,
	&t117_TI/* Class Usage */,
	&m12032_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2340_0_0_0;
extern Il2CppType t2340_1_0_0;
struct t2340;
extern Il2CppGenericClass t2340_GC;
TypeInfo t2340_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2340_MIs, NULL, t2340_FIs, NULL, &t2341_TI, NULL, NULL, &t2340_TI, NULL, t2340_VT, &EmptyCustomAttributesCache, &t2340_TI, &t2340_0_0_0, &t2340_1_0_0, NULL, &t2340_GC, NULL, NULL, NULL, t2340_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2340), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2342.h"
extern TypeInfo t2342_TI;
#include "t2342MD.h"
struct t556;
#define m20420(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.PointerInputModule>
extern Il2CppType t2342_0_0_1;
FieldInfo t2341_f0_FieldInfo = 
{
	"Delegate", &t2342_0_0_1, &t2341_TI, offsetof(t2341, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2341_FIs[] =
{
	&t2341_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2341_m12030_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12030_GM;
MethodInfo m12030_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2341_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2341_m12030_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12030_GM};
extern Il2CppType t2342_0_0_0;
static ParameterInfo t2341_m12031_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2342_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12031_GM;
MethodInfo m12031_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2341_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2341_m12031_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12031_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2341_m12032_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12032_GM;
MethodInfo m12032_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2341_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2341_m12032_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12032_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2341_m12033_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12033_GM;
MethodInfo m12033_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2341_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2341_m12033_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12033_GM};
static MethodInfo* t2341_MIs[] =
{
	&m12030_MI,
	&m12031_MI,
	&m12032_MI,
	&m12033_MI,
	NULL
};
static MethodInfo* t2341_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12032_MI,
	&m12033_MI,
};
extern TypeInfo t2342_TI;
extern TypeInfo t117_TI;
static Il2CppRGCTXData t2341_RGCTXData[5] = 
{
	&t2342_0_0_0/* Type Usage */,
	&t2342_TI/* Class Usage */,
	&m20420_MI/* Method Usage */,
	&t117_TI/* Class Usage */,
	&m12035_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2341_0_0_0;
extern Il2CppType t2341_1_0_0;
struct t2341;
extern Il2CppGenericClass t2341_GC;
TypeInfo t2341_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2341_MIs, NULL, t2341_FIs, NULL, &t556_TI, NULL, NULL, &t2341_TI, NULL, t2341_VT, &EmptyCustomAttributesCache, &t2341_TI, &t2341_0_0_0, &t2341_1_0_0, NULL, &t2341_GC, NULL, NULL, NULL, t2341_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2341), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.PointerInputModule>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2342_m12034_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12034_GM;
MethodInfo m12034_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2342_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2342_m12034_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12034_GM};
extern Il2CppType t117_0_0_0;
static ParameterInfo t2342_m12035_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t117_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12035_GM;
MethodInfo m12035_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2342_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2342_m12035_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12035_GM};
extern Il2CppType t117_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2342_m12036_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t117_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12036_GM;
MethodInfo m12036_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2342_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2342_m12036_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m12036_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2342_m12037_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12037_GM;
MethodInfo m12037_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2342_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2342_m12037_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12037_GM};
static MethodInfo* t2342_MIs[] =
{
	&m12034_MI,
	&m12035_MI,
	&m12036_MI,
	&m12037_MI,
	NULL
};
extern MethodInfo m12036_MI;
extern MethodInfo m12037_MI;
static MethodInfo* t2342_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m12035_MI,
	&m12036_MI,
	&m12037_MI,
};
static Il2CppInterfaceOffsetPair t2342_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2342_1_0_0;
struct t2342;
extern Il2CppGenericClass t2342_GC;
TypeInfo t2342_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2342_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2342_TI, NULL, t2342_VT, &EmptyCustomAttributesCache, &t2342_TI, &t2342_0_0_0, &t2342_1_0_0, t2342_IOs, &t2342_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2342), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t118.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t118_TI;
#include "t118MD.h"

#include "t6.h"
#include "t1248.h"
#include "t1258.h"
#include "t321.h"
#include "t733.h"
#include "t735.h"
#include "t322.h"
#include "t725.h"
#include "t2345.h"
#include "t2359.h"
#include "t323.h"
#include "t2360.h"
#include "t2361.h"
#include "t2364.h"
extern TypeInfo t6_TI;
extern TypeInfo t2344_TI;
extern TypeInfo t1248_TI;
extern TypeInfo t1258_TI;
extern TypeInfo t321_TI;
extern TypeInfo t2346_TI;
extern TypeInfo t322_TI;
extern TypeInfo t3541_TI;
extern TypeInfo t725_TI;
extern TypeInfo t2345_TI;
extern TypeInfo t2359_TI;
extern TypeInfo t323_TI;
extern TypeInfo t2360_TI;
extern TypeInfo t2361_TI;
extern TypeInfo t841_TI;
extern TypeInfo t1965_TI;
extern TypeInfo t2343_TI;
extern TypeInfo t719_TI;
extern TypeInfo t2364_TI;
extern TypeInfo t6642_TI;
#include "t1258MD.h"
#include "t321MD.h"
#include "t322MD.h"
#include "t2345MD.h"
#include "t2359MD.h"
#include "t323MD.h"
#include "t2360MD.h"
#include "t2361MD.h"
#include "t719MD.h"
#include "t2364MD.h"
#include "t733MD.h"
#include "t7MD.h"
#include "t725MD.h"
extern Il2CppType t2344_0_0_0;
extern Il2CppType t2346_0_0_0;
extern Il2CppType t6_0_0_0;
extern MethodInfo m12066_MI;
extern MethodInfo m12070_MI;
extern MethodInfo m12057_MI;
extern MethodInfo m12071_MI;
extern MethodInfo m12058_MI;
extern MethodInfo m27289_MI;
extern MethodInfo m27290_MI;
extern MethodInfo m6569_MI;
extern MethodInfo m12065_MI;
extern MethodInfo m12117_MI;
extern MethodInfo m12059_MI;
extern MethodInfo m1411_MI;
extern MethodInfo m1412_MI;
extern MethodInfo m1435_MI;
extern MethodInfo m1434_MI;
extern MethodInfo m12072_MI;
extern MethodInfo m12064_MI;
extern MethodInfo m12061_MI;
extern MethodInfo m12073_MI;
extern MethodInfo m12147_MI;
extern MethodInfo m20523_MI;
extern MethodInfo m12062_MI;
extern MethodInfo m12156_MI;
extern MethodInfo m20525_MI;
extern MethodInfo m12133_MI;
extern MethodInfo m12160_MI;
extern MethodInfo m12170_MI;
extern MethodInfo m12060_MI;
extern MethodInfo m12056_MI;
extern MethodInfo m12074_MI;
extern MethodInfo m20526_MI;
extern MethodInfo m6736_MI;
extern MethodInfo m12181_MI;
extern MethodInfo m27291_MI;
extern MethodInfo m3984_MI;
extern MethodInfo m3997_MI;
extern MethodInfo m3996_MI;
extern MethodInfo m3985_MI;
extern MethodInfo m6035_MI;
extern MethodInfo m66_MI;
extern MethodInfo m3973_MI;
extern MethodInfo m1410_MI;
extern MethodInfo m27292_MI;
extern MethodInfo m3965_MI;
struct t118;
 void m20523 (t118 * __this, t3541* p0, int32_t p1, t2345 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t118;
 void m20525 (t118 * __this, t20 * p0, int32_t p1, t2359 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t118;
 void m20526 (t118 * __this, t2346* p0, int32_t p1, t2359 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m1409_MI;
 void m1409 (t118 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		m12059(__this, ((int32_t)10), (t29*)NULL, &m12059_MI);
		return;
	}
}
extern MethodInfo m12038_MI;
 void m12038 (t118 * __this, t29* p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		m12059(__this, ((int32_t)10), p0, &m12059_MI);
		return;
	}
}
extern MethodInfo m12039_MI;
 void m12039 (t118 * __this, int32_t p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		m12059(__this, p0, (t29*)NULL, &m12059_MI);
		return;
	}
}
extern MethodInfo m12040_MI;
 void m12040 (t118 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		__this->f13 = p0;
		return;
	}
}
extern MethodInfo m12041_MI;
 t29 * m12041 (t118 * __this, t29 * p0, MethodInfo* method){
	{
		if (!((t29 *)IsInst(p0, InitializedTypeInfo(&t44_TI))))
		{
			goto IL_0029;
		}
	}
	{
		bool L_0 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(&m12066_MI, __this, ((*(int32_t*)((int32_t*)UnBox (p0, InitializedTypeInfo(&t44_TI))))));
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		int32_t L_1 = m12070(__this, p0, &m12070_MI);
		t6 * L_2 = (t6 *)VirtFuncInvoker1< t6 *, int32_t >::Invoke(&m12057_MI, __this, L_1);
		t6 * L_3 = L_2;
		return ((t6 *)L_3);
	}

IL_0029:
	{
		return NULL;
	}
}
extern MethodInfo m12042_MI;
 void m12042 (t118 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	{
		int32_t L_0 = m12070(__this, p0, &m12070_MI);
		t6 * L_1 = m12071(__this, p1, &m12071_MI);
		VirtActionInvoker2< int32_t, t6 * >::Invoke(&m12058_MI, __this, L_0, L_1);
		return;
	}
}
extern MethodInfo m12043_MI;
 void m12043 (t118 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	{
		int32_t L_0 = m12070(__this, p0, &m12070_MI);
		t6 * L_1 = m12071(__this, p1, &m12071_MI);
		VirtActionInvoker2< int32_t, t6 * >::Invoke(&m1411_MI, __this, L_0, L_1);
		return;
	}
}
extern MethodInfo m12044_MI;
 void m12044 (t118 * __this, t29 * p0, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		if (!((t29 *)IsInst(p0, InitializedTypeInfo(&t44_TI))))
		{
			goto IL_0023;
		}
	}
	{
		VirtFuncInvoker1< bool, int32_t >::Invoke(&m1412_MI, __this, ((*(int32_t*)((int32_t*)UnBox (p0, InitializedTypeInfo(&t44_TI))))));
	}

IL_0023:
	{
		return;
	}
}
extern MethodInfo m12045_MI;
 bool m12045 (t118 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m12046_MI;
 t29 * m12046 (t118 * __this, MethodInfo* method){
	{
		return __this;
	}
}
extern MethodInfo m12047_MI;
 bool m12047 (t118 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m12048_MI;
 void m12048 (t118 * __this, t322  p0, MethodInfo* method){
	{
		int32_t L_0 = m1435((&p0), &m1435_MI);
		t6 * L_1 = m1434((&p0), &m1434_MI);
		VirtActionInvoker2< int32_t, t6 * >::Invoke(&m1411_MI, __this, L_0, L_1);
		return;
	}
}
extern MethodInfo m12049_MI;
 bool m12049 (t118 * __this, t322  p0, MethodInfo* method){
	{
		bool L_0 = m12072(__this, p0, &m12072_MI);
		return L_0;
	}
}
extern MethodInfo m12050_MI;
 void m12050 (t118 * __this, t2346* p0, int32_t p1, MethodInfo* method){
	{
		m12064(__this, p0, p1, &m12064_MI);
		return;
	}
}
extern MethodInfo m12051_MI;
 bool m12051 (t118 * __this, t322  p0, MethodInfo* method){
	{
		bool L_0 = m12072(__this, p0, &m12072_MI);
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return 0;
	}

IL_000b:
	{
		int32_t L_1 = m1435((&p0), &m1435_MI);
		bool L_2 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(&m1412_MI, __this, L_1);
		return L_2;
	}
}
extern MethodInfo m12052_MI;
 void m12052 (t118 * __this, t20 * p0, int32_t p1, MethodInfo* method){
	t2346* V_0 = {0};
	t3541* V_1 = {0};
	int32_t G_B5_0 = 0;
	t3541* G_B5_1 = {0};
	t118 * G_B5_2 = {0};
	int32_t G_B4_0 = 0;
	t3541* G_B4_1 = {0};
	t118 * G_B4_2 = {0};
	{
		V_0 = ((t2346*)IsInst(p0, InitializedTypeInfo(&t2346_TI)));
		if (!V_0)
		{
			goto IL_0013;
		}
	}
	{
		m12064(__this, V_0, p1, &m12064_MI);
		return;
	}

IL_0013:
	{
		m12061(__this, p0, p1, &m12061_MI);
		V_1 = ((t3541*)IsInst(p0, InitializedTypeInfo(&t3541_TI)));
		if (!V_1)
		{
			goto IL_004b;
		}
	}
	{
		G_B4_0 = p1;
		G_B4_1 = V_1;
		G_B4_2 = ((t118 *)(__this));
		if ((((t118_SFs*)InitializedTypeInfo(&t118_TI)->static_fields)->f15))
		{
			G_B5_0 = p1;
			G_B5_1 = V_1;
			G_B5_2 = ((t118 *)(__this));
			goto IL_0040;
		}
	}
	{
		t35 L_0 = { &m12073_MI };
		t2345 * L_1 = (t2345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2345_TI));
		m12147(L_1, NULL, L_0, &m12147_MI);
		((t118_SFs*)InitializedTypeInfo(&t118_TI)->static_fields)->f15 = L_1;
		G_B5_0 = G_B4_0;
		G_B5_1 = G_B4_1;
		G_B5_2 = ((t118 *)(G_B4_2));
	}

IL_0040:
	{
		m20523(G_B5_2, G_B5_1, G_B5_0, (((t118_SFs*)InitializedTypeInfo(&t118_TI)->static_fields)->f15), &m20523_MI);
		return;
	}

IL_004b:
	{
		t35 L_2 = { &m12062_MI };
		t2359 * L_3 = (t2359 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2359_TI));
		m12156(L_3, NULL, L_2, &m12156_MI);
		m20525(__this, p0, p1, L_3, &m20525_MI);
		return;
	}
}
extern MethodInfo m12053_MI;
 t29 * m12053 (t118 * __this, MethodInfo* method){
	{
		t323  L_0 = {0};
		m12133(&L_0, __this, &m12133_MI);
		t323  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t323_TI), &L_1);
		return (t29 *)L_2;
	}
}
extern MethodInfo m12054_MI;
 t29* m12054 (t118 * __this, MethodInfo* method){
	{
		t323  L_0 = {0};
		m12133(&L_0, __this, &m12133_MI);
		t323  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t323_TI), &L_1);
		return (t29*)L_2;
	}
}
extern MethodInfo m12055_MI;
 t29 * m12055 (t118 * __this, MethodInfo* method){
	{
		t2360 * L_0 = (t2360 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2360_TI));
		m12160(L_0, __this, &m12160_MI);
		return L_0;
	}
}
 int32_t m12056 (t118 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f10);
		return L_0;
	}
}
 t6 * m12057 (t118 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t44_TI), &L_0);
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_2 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_2, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0013:
	{
		t29* L_3 = (__this->f12);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(&m27289_MI, L_3, p0);
		V_0 = ((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		t841* L_5 = (__this->f4);
		t841* L_6 = (__this->f4);
		int32_t L_7 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_6)->max_length)))));
		V_1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_7))-1));
		goto IL_008f;
	}

IL_0042:
	{
		t1965* L_8 = (__this->f5);
		int32_t L_9 = (((t1248 *)(t1248 *)SZArrayLdElema(L_8, V_1))->f0);
		if ((((uint32_t)L_9) != ((uint32_t)V_0)))
		{
			goto IL_007d;
		}
	}
	{
		t29* L_10 = (__this->f12);
		t841* L_11 = (__this->f6);
		int32_t L_12 = V_1;
		bool L_13 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(&m27290_MI, L_10, (*(int32_t*)(int32_t*)SZArrayLdElema(L_11, L_12)), p0);
		if (!L_13)
		{
			goto IL_007d;
		}
	}
	{
		t2343* L_14 = (__this->f7);
		int32_t L_15 = V_1;
		return (*(t6 **)(t6 **)SZArrayLdElema(L_14, L_15));
	}

IL_007d:
	{
		t1965* L_16 = (__this->f5);
		int32_t L_17 = (((t1248 *)(t1248 *)SZArrayLdElema(L_16, V_1))->f1);
		V_1 = L_17;
	}

IL_008f:
	{
		if ((((uint32_t)V_1) != ((uint32_t)(-1))))
		{
			goto IL_0042;
		}
	}
	{
		t1258 * L_18 = (t1258 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1258_TI));
		m6569(L_18, &m6569_MI);
		il2cpp_codegen_raise_exception(L_18);
	}
}
 void m12058 (t118 * __this, int32_t p0, t6 * p1, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		int32_t L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t44_TI), &L_0);
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_2 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_2, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0013:
	{
		t29* L_3 = (__this->f12);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(&m27289_MI, L_3, p0);
		V_0 = ((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		t841* L_5 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_5)->max_length)))));
		t841* L_6 = (__this->f4);
		int32_t L_7 = V_1;
		V_2 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_6, L_7))-1));
		V_3 = (-1);
		if ((((int32_t)V_2) == ((int32_t)(-1))))
		{
			goto IL_0090;
		}
	}

IL_0048:
	{
		t1965* L_8 = (__this->f5);
		int32_t L_9 = (((t1248 *)(t1248 *)SZArrayLdElema(L_8, V_2))->f0);
		if ((((uint32_t)L_9) != ((uint32_t)V_0)))
		{
			goto IL_0078;
		}
	}
	{
		t29* L_10 = (__this->f12);
		t841* L_11 = (__this->f6);
		int32_t L_12 = V_2;
		bool L_13 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(&m27290_MI, L_10, (*(int32_t*)(int32_t*)SZArrayLdElema(L_11, L_12)), p0);
		if (!L_13)
		{
			goto IL_0078;
		}
	}
	{
		goto IL_0090;
	}

IL_0078:
	{
		V_3 = V_2;
		t1965* L_14 = (__this->f5);
		int32_t L_15 = (((t1248 *)(t1248 *)SZArrayLdElema(L_14, V_2))->f1);
		V_2 = L_15;
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0048;
		}
	}

IL_0090:
	{
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0148;
		}
	}
	{
		int32_t L_16 = (__this->f10);
		int32_t L_17 = ((int32_t)(L_16+1));
		V_4 = L_17;
		__this->f10 = L_17;
		int32_t L_18 = (__this->f11);
		if ((((int32_t)V_4) <= ((int32_t)L_18)))
		{
			goto IL_00c9;
		}
	}
	{
		m12065(__this, &m12065_MI);
		t841* L_19 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_19)->max_length)))));
	}

IL_00c9:
	{
		int32_t L_20 = (__this->f9);
		V_2 = L_20;
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_00ea;
		}
	}
	{
		int32_t L_21 = (__this->f8);
		int32_t L_22 = L_21;
		V_4 = L_22;
		__this->f8 = ((int32_t)(L_22+1));
		V_2 = V_4;
		goto IL_0101;
	}

IL_00ea:
	{
		t1965* L_23 = (__this->f5);
		int32_t L_24 = (((t1248 *)(t1248 *)SZArrayLdElema(L_23, V_2))->f1);
		__this->f9 = L_24;
	}

IL_0101:
	{
		t1965* L_25 = (__this->f5);
		t841* L_26 = (__this->f4);
		int32_t L_27 = V_1;
		((t1248 *)(t1248 *)SZArrayLdElema(L_25, V_2))->f1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_26, L_27))-1));
		t841* L_28 = (__this->f4);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_28, V_1)) = (int32_t)((int32_t)(V_2+1));
		t1965* L_29 = (__this->f5);
		((t1248 *)(t1248 *)SZArrayLdElema(L_29, V_2))->f0 = V_0;
		t841* L_30 = (__this->f6);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_30, V_2)) = (int32_t)p0;
		goto IL_0194;
	}

IL_0148:
	{
		if ((((int32_t)V_3) == ((int32_t)(-1))))
		{
			goto IL_0194;
		}
	}
	{
		t1965* L_31 = (__this->f5);
		t1965* L_32 = (__this->f5);
		int32_t L_33 = (((t1248 *)(t1248 *)SZArrayLdElema(L_32, V_2))->f1);
		((t1248 *)(t1248 *)SZArrayLdElema(L_31, V_3))->f1 = L_33;
		t1965* L_34 = (__this->f5);
		t841* L_35 = (__this->f4);
		int32_t L_36 = V_1;
		((t1248 *)(t1248 *)SZArrayLdElema(L_34, V_2))->f1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_35, L_36))-1));
		t841* L_37 = (__this->f4);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_37, V_1)) = (int32_t)((int32_t)(V_2+1));
	}

IL_0194:
	{
		t2343* L_38 = (__this->f7);
		*((t6 **)(t6 **)SZArrayLdElema(L_38, V_2)) = (t6 *)p1;
		int32_t L_39 = (__this->f14);
		__this->f14 = ((int32_t)(L_39+1));
		return;
	}
}
 void m12059 (t118 * __this, int32_t p0, t29* p1, MethodInfo* method){
	t29* V_0 = {0};
	t118 * G_B4_0 = {0};
	t118 * G_B3_0 = {0};
	t29* G_B5_0 = {0};
	t118 * G_B5_1 = {0};
	{
		if ((((int32_t)p0) >= ((int32_t)0)))
		{
			goto IL_000f;
		}
	}
	{
		t915 * L_0 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_0, (t7*) &_stringLiteral1163, &m3975_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000f:
	{
		G_B3_0 = ((t118 *)(__this));
		if (!p1)
		{
			G_B4_0 = ((t118 *)(__this));
			goto IL_0018;
		}
	}
	{
		V_0 = p1;
		G_B5_0 = V_0;
		G_B5_1 = ((t118 *)(G_B3_0));
		goto IL_001d;
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t2361_TI));
		t2361 * L_1 = m12170(NULL, &m12170_MI);
		G_B5_0 = ((t29*)(L_1));
		G_B5_1 = ((t118 *)(G_B4_0));
	}

IL_001d:
	{
		G_B5_1->f12 = G_B5_0;
		if (p0)
		{
			goto IL_002b;
		}
	}
	{
		p0 = ((int32_t)10);
	}

IL_002b:
	{
		p0 = ((int32_t)((((int32_t)((float)((float)(((float)p0))/(float)(0.9f)))))+1));
		m12060(__this, p0, &m12060_MI);
		__this->f14 = 0;
		return;
	}
}
 void m12060 (t118 * __this, int32_t p0, MethodInfo* method){
	{
		__this->f4 = ((t841*)SZArrayNew(InitializedTypeInfo(&t841_TI), p0));
		__this->f5 = ((t1965*)SZArrayNew(InitializedTypeInfo(&t1965_TI), p0));
		__this->f9 = (-1);
		__this->f6 = ((t841*)SZArrayNew(InitializedTypeInfo(&t841_TI), p0));
		__this->f7 = ((t2343*)SZArrayNew(InitializedTypeInfo(&t2343_TI), p0));
		__this->f8 = 0;
		t841* L_0 = (__this->f4);
		__this->f11 = (((int32_t)((float)((float)(((float)(((int32_t)(((t20 *)L_0)->max_length)))))*(float)(0.9f)))));
		int32_t L_1 = (__this->f11);
		if (L_1)
		{
			goto IL_006e;
		}
	}
	{
		t841* L_2 = (__this->f4);
		if ((((int32_t)(((int32_t)(((t20 *)L_2)->max_length)))) <= ((int32_t)0)))
		{
			goto IL_006e;
		}
	}
	{
		__this->f11 = 1;
	}

IL_006e:
	{
		return;
	}
}
 void m12061 (t118 * __this, t20 * p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_001d:
	{
		int32_t L_2 = m3969(p0, &m3969_MI);
		if ((((int32_t)p1) <= ((int32_t)L_2)))
		{
			goto IL_0031;
		}
	}
	{
		t305 * L_3 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_3, (t7*) &_stringLiteral1164, &m1935_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0031:
	{
		int32_t L_4 = m3969(p0, &m3969_MI);
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m12056_MI, __this);
		if ((((int32_t)((int32_t)(L_4-p1))) >= ((int32_t)L_5)))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_6 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_6, (t7*) &_stringLiteral1165, &m1935_MI);
		il2cpp_codegen_raise_exception(L_6);
	}

IL_004c:
	{
		return;
	}
}
 t322  m12062 (t29 * __this, int32_t p0, t6 * p1, MethodInfo* method){
	{
		t322  L_0 = {0};
		m12074(&L_0, p0, p1, &m12074_MI);
		return L_0;
	}
}
extern MethodInfo m12063_MI;
 t6 * m12063 (t29 * __this, int32_t p0, t6 * p1, MethodInfo* method){
	{
		return p1;
	}
}
 void m12064 (t118 * __this, t2346* p0, int32_t p1, MethodInfo* method){
	{
		m12061(__this, (t20 *)(t20 *)p0, p1, &m12061_MI);
		t35 L_0 = { &m12062_MI };
		t2359 * L_1 = (t2359 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2359_TI));
		m12156(L_1, NULL, L_0, &m12156_MI);
		m20526(__this, p0, p1, L_1, &m20526_MI);
		return;
	}
}
 void m12065 (t118 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	t841* V_1 = {0};
	t1965* V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	t841* V_7 = {0};
	t2343* V_8 = {0};
	int32_t V_9 = 0;
	{
		t841* L_0 = (__this->f4);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t719_TI));
		int32_t L_1 = m6736(NULL, ((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)(((t20 *)L_0)->max_length)))<<(int32_t)1))|(int32_t)1)), &m6736_MI);
		V_0 = L_1;
		V_1 = ((t841*)SZArrayNew(InitializedTypeInfo(&t841_TI), V_0));
		V_2 = ((t1965*)SZArrayNew(InitializedTypeInfo(&t1965_TI), V_0));
		V_3 = 0;
		goto IL_00ab;
	}

IL_0027:
	{
		t841* L_2 = (__this->f4);
		int32_t L_3 = V_3;
		V_4 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_2, L_3))-1));
		goto IL_00a2;
	}

IL_0035:
	{
		t29* L_4 = (__this->f12);
		t841* L_5 = (__this->f6);
		int32_t L_6 = V_4;
		int32_t L_7 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(&m27289_MI, L_4, (*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_6)));
		int32_t L_8 = ((int32_t)((int32_t)L_7|(int32_t)((int32_t)-2147483648)));
		V_9 = L_8;
		((t1248 *)(t1248 *)SZArrayLdElema(V_2, V_4))->f0 = L_8;
		V_5 = V_9;
		V_6 = ((int32_t)(((int32_t)((int32_t)V_5&(int32_t)((int32_t)2147483647)))%V_0));
		int32_t L_9 = V_6;
		((t1248 *)(t1248 *)SZArrayLdElema(V_2, V_4))->f1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(V_1, L_9))-1));
		*((int32_t*)(int32_t*)SZArrayLdElema(V_1, V_6)) = (int32_t)((int32_t)(V_4+1));
		t1965* L_10 = (__this->f5);
		int32_t L_11 = (((t1248 *)(t1248 *)SZArrayLdElema(L_10, V_4))->f1);
		V_4 = L_11;
	}

IL_00a2:
	{
		if ((((uint32_t)V_4) != ((uint32_t)(-1))))
		{
			goto IL_0035;
		}
	}
	{
		V_3 = ((int32_t)(V_3+1));
	}

IL_00ab:
	{
		t841* L_12 = (__this->f4);
		if ((((int32_t)V_3) < ((int32_t)(((int32_t)(((t20 *)L_12)->max_length))))))
		{
			goto IL_0027;
		}
	}
	{
		__this->f4 = V_1;
		__this->f5 = V_2;
		V_7 = ((t841*)SZArrayNew(InitializedTypeInfo(&t841_TI), V_0));
		V_8 = ((t2343*)SZArrayNew(InitializedTypeInfo(&t2343_TI), V_0));
		t841* L_13 = (__this->f6);
		int32_t L_14 = (__this->f8);
		m5952(NULL, (t20 *)(t20 *)L_13, 0, (t20 *)(t20 *)V_7, 0, L_14, &m5952_MI);
		t2343* L_15 = (__this->f7);
		int32_t L_16 = (__this->f8);
		m5952(NULL, (t20 *)(t20 *)L_15, 0, (t20 *)(t20 *)V_8, 0, L_16, &m5952_MI);
		__this->f6 = V_7;
		__this->f7 = V_8;
		__this->f11 = (((int32_t)((float)((float)(((float)V_0))*(float)(0.9f)))));
		return;
	}
}
 void m1411 (t118 * __this, int32_t p0, t6 * p1, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		int32_t L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t44_TI), &L_0);
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_2 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_2, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0013:
	{
		t29* L_3 = (__this->f12);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(&m27289_MI, L_3, p0);
		V_0 = ((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		t841* L_5 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_5)->max_length)))));
		t841* L_6 = (__this->f4);
		int32_t L_7 = V_1;
		V_2 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_6, L_7))-1));
		goto IL_008f;
	}

IL_0044:
	{
		t1965* L_8 = (__this->f5);
		int32_t L_9 = (((t1248 *)(t1248 *)SZArrayLdElema(L_8, V_2))->f0);
		if ((((uint32_t)L_9) != ((uint32_t)V_0)))
		{
			goto IL_007d;
		}
	}
	{
		t29* L_10 = (__this->f12);
		t841* L_11 = (__this->f6);
		int32_t L_12 = V_2;
		bool L_13 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(&m27290_MI, L_10, (*(int32_t*)(int32_t*)SZArrayLdElema(L_11, L_12)), p0);
		if (!L_13)
		{
			goto IL_007d;
		}
	}
	{
		t305 * L_14 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_14, (t7*) &_stringLiteral1167, &m1935_MI);
		il2cpp_codegen_raise_exception(L_14);
	}

IL_007d:
	{
		t1965* L_15 = (__this->f5);
		int32_t L_16 = (((t1248 *)(t1248 *)SZArrayLdElema(L_15, V_2))->f1);
		V_2 = L_16;
	}

IL_008f:
	{
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0044;
		}
	}
	{
		int32_t L_17 = (__this->f10);
		int32_t L_18 = ((int32_t)(L_17+1));
		V_3 = L_18;
		__this->f10 = L_18;
		int32_t L_19 = (__this->f11);
		if ((((int32_t)V_3) <= ((int32_t)L_19)))
		{
			goto IL_00c3;
		}
	}
	{
		m12065(__this, &m12065_MI);
		t841* L_20 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_20)->max_length)))));
	}

IL_00c3:
	{
		int32_t L_21 = (__this->f9);
		V_2 = L_21;
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_00e2;
		}
	}
	{
		int32_t L_22 = (__this->f8);
		int32_t L_23 = L_22;
		V_3 = L_23;
		__this->f8 = ((int32_t)(L_23+1));
		V_2 = V_3;
		goto IL_00f9;
	}

IL_00e2:
	{
		t1965* L_24 = (__this->f5);
		int32_t L_25 = (((t1248 *)(t1248 *)SZArrayLdElema(L_24, V_2))->f1);
		__this->f9 = L_25;
	}

IL_00f9:
	{
		t1965* L_26 = (__this->f5);
		((t1248 *)(t1248 *)SZArrayLdElema(L_26, V_2))->f0 = V_0;
		t1965* L_27 = (__this->f5);
		t841* L_28 = (__this->f4);
		int32_t L_29 = V_1;
		((t1248 *)(t1248 *)SZArrayLdElema(L_27, V_2))->f1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_28, L_29))-1));
		t841* L_30 = (__this->f4);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_30, V_1)) = (int32_t)((int32_t)(V_2+1));
		t841* L_31 = (__this->f6);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_31, V_2)) = (int32_t)p0;
		t2343* L_32 = (__this->f7);
		*((t6 **)(t6 **)SZArrayLdElema(L_32, V_2)) = (t6 *)p1;
		int32_t L_33 = (__this->f14);
		__this->f14 = ((int32_t)(L_33+1));
		return;
	}
}
extern MethodInfo m1429_MI;
 void m1429 (t118 * __this, MethodInfo* method){
	{
		__this->f10 = 0;
		t841* L_0 = (__this->f4);
		t841* L_1 = (__this->f4);
		m5112(NULL, (t20 *)(t20 *)L_0, 0, (((int32_t)(((t20 *)L_1)->max_length))), &m5112_MI);
		t841* L_2 = (__this->f6);
		t841* L_3 = (__this->f6);
		m5112(NULL, (t20 *)(t20 *)L_2, 0, (((int32_t)(((t20 *)L_3)->max_length))), &m5112_MI);
		t2343* L_4 = (__this->f7);
		t2343* L_5 = (__this->f7);
		m5112(NULL, (t20 *)(t20 *)L_4, 0, (((int32_t)(((t20 *)L_5)->max_length))), &m5112_MI);
		t1965* L_6 = (__this->f5);
		t1965* L_7 = (__this->f5);
		m5112(NULL, (t20 *)(t20 *)L_6, 0, (((int32_t)(((t20 *)L_7)->max_length))), &m5112_MI);
		__this->f9 = (-1);
		__this->f8 = 0;
		int32_t L_8 = (__this->f14);
		__this->f14 = ((int32_t)(L_8+1));
		return;
	}
}
 bool m12066 (t118 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t44_TI), &L_0);
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_2 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_2, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0013:
	{
		t29* L_3 = (__this->f12);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(&m27289_MI, L_3, p0);
		V_0 = ((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		t841* L_5 = (__this->f4);
		t841* L_6 = (__this->f4);
		int32_t L_7 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_6)->max_length)))));
		V_1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_7))-1));
		goto IL_0084;
	}

IL_0042:
	{
		t1965* L_8 = (__this->f5);
		int32_t L_9 = (((t1248 *)(t1248 *)SZArrayLdElema(L_8, V_1))->f0);
		if ((((uint32_t)L_9) != ((uint32_t)V_0)))
		{
			goto IL_0072;
		}
	}
	{
		t29* L_10 = (__this->f12);
		t841* L_11 = (__this->f6);
		int32_t L_12 = V_1;
		bool L_13 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(&m27290_MI, L_10, (*(int32_t*)(int32_t*)SZArrayLdElema(L_11, L_12)), p0);
		if (!L_13)
		{
			goto IL_0072;
		}
	}
	{
		return 1;
	}

IL_0072:
	{
		t1965* L_14 = (__this->f5);
		int32_t L_15 = (((t1248 *)(t1248 *)SZArrayLdElema(L_14, V_1))->f1);
		V_1 = L_15;
	}

IL_0084:
	{
		if ((((uint32_t)V_1) != ((uint32_t)(-1))))
		{
			goto IL_0042;
		}
	}
	{
		return 0;
	}
}
extern MethodInfo m12067_MI;
 bool m12067 (t118 * __this, t6 * p0, MethodInfo* method){
	t29* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t2364_TI));
		t2364 * L_0 = m12181(NULL, &m12181_MI);
		V_0 = L_0;
		V_1 = 0;
		goto IL_0048;
	}

IL_000a:
	{
		t841* L_1 = (__this->f4);
		int32_t L_2 = V_1;
		V_2 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_1, L_2))-1));
		goto IL_0040;
	}

IL_0017:
	{
		t2343* L_3 = (__this->f7);
		int32_t L_4 = V_2;
		bool L_5 = (bool)InterfaceFuncInvoker2< bool, t6 *, t6 * >::Invoke(&m27291_MI, V_0, (*(t6 **)(t6 **)SZArrayLdElema(L_3, L_4)), p0);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		return 1;
	}

IL_002e:
	{
		t1965* L_6 = (__this->f5);
		int32_t L_7 = (((t1248 *)(t1248 *)SZArrayLdElema(L_6, V_2))->f1);
		V_2 = L_7;
	}

IL_0040:
	{
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0017;
		}
	}
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0048:
	{
		t841* L_8 = (__this->f4);
		if ((((int32_t)V_1) < ((int32_t)(((int32_t)(((t20 *)L_8)->max_length))))))
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}
}
extern MethodInfo m12068_MI;
 void m12068 (t118 * __this, t733 * p0, t735  p1, MethodInfo* method){
	t2346* V_0 = {0};
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral206, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = (__this->f14);
		m3984(p0, (t7*) &_stringLiteral208, L_1, &m3984_MI);
		t29* L_2 = (__this->f12);
		m3997(p0, (t7*) &_stringLiteral210, L_2, &m3997_MI);
		V_0 = (t2346*)NULL;
		int32_t L_3 = (__this->f10);
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_4 = (__this->f10);
		V_0 = ((t2346*)SZArrayNew(InitializedTypeInfo(&t2346_TI), L_4));
		m12064(__this, V_0, 0, &m12064_MI);
	}

IL_004f:
	{
		t841* L_5 = (__this->f4);
		m3984(p0, (t7*) &_stringLiteral1168, (((int32_t)(((t20 *)L_5)->max_length))), &m3984_MI);
		m3997(p0, (t7*) &_stringLiteral1169, (t29 *)(t29 *)V_0, &m3997_MI);
		return;
	}
}
extern MethodInfo m12069_MI;
 void m12069 (t118 * __this, t29 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	t2346* V_1 = {0};
	int32_t V_2 = 0;
	{
		t733 * L_0 = (__this->f13);
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		return;
	}

IL_0009:
	{
		t733 * L_1 = (__this->f13);
		int32_t L_2 = m3996(L_1, (t7*) &_stringLiteral208, &m3996_MI);
		__this->f14 = L_2;
		t733 * L_3 = (__this->f13);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_4 = m1554(NULL, LoadTypeToken(&t2344_0_0_0), &m1554_MI);
		t29 * L_5 = m3985(L_3, (t7*) &_stringLiteral210, L_4, &m3985_MI);
		__this->f12 = ((t29*)Castclass(L_5, InitializedTypeInfo(&t2344_TI)));
		t733 * L_6 = (__this->f13);
		int32_t L_7 = m3996(L_6, (t7*) &_stringLiteral1168, &m3996_MI);
		V_0 = L_7;
		t733 * L_8 = (__this->f13);
		t42 * L_9 = m1554(NULL, LoadTypeToken(&t2346_0_0_0), &m1554_MI);
		t29 * L_10 = m3985(L_8, (t7*) &_stringLiteral1169, L_9, &m3985_MI);
		V_1 = ((t2346*)Castclass(L_10, InitializedTypeInfo(&t2346_TI)));
		if ((((int32_t)V_0) >= ((int32_t)((int32_t)10))))
		{
			goto IL_007d;
		}
	}
	{
		V_0 = ((int32_t)10);
	}

IL_007d:
	{
		m12060(__this, V_0, &m12060_MI);
		__this->f10 = 0;
		if (!V_1)
		{
			goto IL_00ba;
		}
	}
	{
		V_2 = 0;
		goto IL_00b4;
	}

IL_0092:
	{
		int32_t L_11 = m1435(((t322 *)(t322 *)SZArrayLdElema(V_1, V_2)), &m1435_MI);
		t6 * L_12 = m1434(((t322 *)(t322 *)SZArrayLdElema(V_1, V_2)), &m1434_MI);
		VirtActionInvoker2< int32_t, t6 * >::Invoke(&m1411_MI, __this, L_11, L_12);
		V_2 = ((int32_t)(V_2+1));
	}

IL_00b4:
	{
		if ((((int32_t)V_2) < ((int32_t)(((int32_t)(((t20 *)V_1)->max_length))))))
		{
			goto IL_0092;
		}
	}

IL_00ba:
	{
		int32_t L_13 = (__this->f14);
		__this->f14 = ((int32_t)(L_13+1));
		__this->f13 = (t733 *)NULL;
		return;
	}
}
 bool m1412 (t118 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	t6 * V_5 = {0};
	{
		int32_t L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t44_TI), &L_0);
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_2 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_2, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0013:
	{
		t29* L_3 = (__this->f12);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(&m27289_MI, L_3, p0);
		V_0 = ((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		t841* L_5 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_5)->max_length)))));
		t841* L_6 = (__this->f4);
		int32_t L_7 = V_1;
		V_2 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_6, L_7))-1));
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0048;
		}
	}
	{
		return 0;
	}

IL_0048:
	{
		V_3 = (-1);
	}

IL_004a:
	{
		t1965* L_8 = (__this->f5);
		int32_t L_9 = (((t1248 *)(t1248 *)SZArrayLdElema(L_8, V_2))->f0);
		if ((((uint32_t)L_9) != ((uint32_t)V_0)))
		{
			goto IL_007a;
		}
	}
	{
		t29* L_10 = (__this->f12);
		t841* L_11 = (__this->f6);
		int32_t L_12 = V_2;
		bool L_13 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(&m27290_MI, L_10, (*(int32_t*)(int32_t*)SZArrayLdElema(L_11, L_12)), p0);
		if (!L_13)
		{
			goto IL_007a;
		}
	}
	{
		goto IL_0092;
	}

IL_007a:
	{
		V_3 = V_2;
		t1965* L_14 = (__this->f5);
		int32_t L_15 = (((t1248 *)(t1248 *)SZArrayLdElema(L_14, V_2))->f1);
		V_2 = L_15;
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_004a;
		}
	}

IL_0092:
	{
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0098;
		}
	}
	{
		return 0;
	}

IL_0098:
	{
		int32_t L_16 = (__this->f10);
		__this->f10 = ((int32_t)(L_16-1));
		if ((((uint32_t)V_3) != ((uint32_t)(-1))))
		{
			goto IL_00c7;
		}
	}
	{
		t841* L_17 = (__this->f4);
		t1965* L_18 = (__this->f5);
		int32_t L_19 = (((t1248 *)(t1248 *)SZArrayLdElema(L_18, V_2))->f1);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_17, V_1)) = (int32_t)((int32_t)(L_19+1));
		goto IL_00e9;
	}

IL_00c7:
	{
		t1965* L_20 = (__this->f5);
		t1965* L_21 = (__this->f5);
		int32_t L_22 = (((t1248 *)(t1248 *)SZArrayLdElema(L_21, V_2))->f1);
		((t1248 *)(t1248 *)SZArrayLdElema(L_20, V_3))->f1 = L_22;
	}

IL_00e9:
	{
		t1965* L_23 = (__this->f5);
		int32_t L_24 = (__this->f9);
		((t1248 *)(t1248 *)SZArrayLdElema(L_23, V_2))->f1 = L_24;
		__this->f9 = V_2;
		t1965* L_25 = (__this->f5);
		((t1248 *)(t1248 *)SZArrayLdElema(L_25, V_2))->f0 = 0;
		t841* L_26 = (__this->f6);
		Initobj (&t44_TI, (&V_4));
		*((int32_t*)(int32_t*)SZArrayLdElema(L_26, V_2)) = (int32_t)V_4;
		t2343* L_27 = (__this->f7);
		Initobj (&t6_TI, (&V_5));
		*((t6 **)(t6 **)SZArrayLdElema(L_27, V_2)) = (t6 *)V_5;
		int32_t L_28 = (__this->f14);
		__this->f14 = ((int32_t)(L_28+1));
		return 1;
	}
}
 bool m1410 (t118 * __this, int32_t p0, t6 ** p1, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	t6 * V_2 = {0};
	{
		int32_t L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t44_TI), &L_0);
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_2 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_2, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0013:
	{
		t29* L_3 = (__this->f12);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(&m27289_MI, L_3, p0);
		V_0 = ((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		t841* L_5 = (__this->f4);
		t841* L_6 = (__this->f4);
		int32_t L_7 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_6)->max_length)))));
		V_1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_7))-1));
		goto IL_0096;
	}

IL_0042:
	{
		t1965* L_8 = (__this->f5);
		int32_t L_9 = (((t1248 *)(t1248 *)SZArrayLdElema(L_8, V_1))->f0);
		if ((((uint32_t)L_9) != ((uint32_t)V_0)))
		{
			goto IL_0084;
		}
	}
	{
		t29* L_10 = (__this->f12);
		t841* L_11 = (__this->f6);
		int32_t L_12 = V_1;
		bool L_13 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(&m27290_MI, L_10, (*(int32_t*)(int32_t*)SZArrayLdElema(L_11, L_12)), p0);
		if (!L_13)
		{
			goto IL_0084;
		}
	}
	{
		t2343* L_14 = (__this->f7);
		int32_t L_15 = V_1;
		*p1 = (*(t6 **)(t6 **)SZArrayLdElema(L_14, L_15));
		return 1;
	}

IL_0084:
	{
		t1965* L_16 = (__this->f5);
		int32_t L_17 = (((t1248 *)(t1248 *)SZArrayLdElema(L_16, V_1))->f1);
		V_1 = L_17;
	}

IL_0096:
	{
		if ((((uint32_t)V_1) != ((uint32_t)(-1))))
		{
			goto IL_0042;
		}
	}
	{
		Initobj (&t6_TI, (&V_2));
		*p1 = V_2;
		return 0;
	}
}
extern MethodInfo m1424_MI;
 t321 * m1424 (t118 * __this, MethodInfo* method){
	{
		t321 * L_0 = (t321 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t321_TI));
		m12117(L_0, __this, &m12117_MI);
		return L_0;
	}
}
 int32_t m12070 (t118 * __this, t29 * p0, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		if (((t29 *)IsInst(p0, InitializedTypeInfo(&t44_TI))))
		{
			goto IL_003a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_1 = m1554(NULL, LoadTypeToken(&t44_0_0_0), &m1554_MI);
		t7* L_2 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m6035_MI, L_1);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_3 = m66(NULL, (t7*) &_stringLiteral1170, L_2, &m66_MI);
		t305 * L_4 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m3973(L_4, L_3, (t7*) &_stringLiteral195, &m3973_MI);
		il2cpp_codegen_raise_exception(L_4);
	}

IL_003a:
	{
		return ((*(int32_t*)((int32_t*)UnBox (p0, InitializedTypeInfo(&t44_TI)))));
	}
}
 t6 * m12071 (t118 * __this, t29 * p0, MethodInfo* method){
	t6 * V_0 = {0};
	{
		if (p0)
		{
			goto IL_001e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_0 = m1554(NULL, LoadTypeToken(&t6_0_0_0), &m1554_MI);
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6004_MI, L_0);
		if (L_1)
		{
			goto IL_001e;
		}
	}
	{
		Initobj (&t6_TI, (&V_0));
		return V_0;
	}

IL_001e:
	{
		if (((t6 *)IsInst(p0, InitializedTypeInfo(&t6_TI))))
		{
			goto IL_004a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_2 = m1554(NULL, LoadTypeToken(&t6_0_0_0), &m1554_MI);
		t7* L_3 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m6035_MI, L_2);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_4 = m66(NULL, (t7*) &_stringLiteral1170, L_3, &m66_MI);
		t305 * L_5 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m3973(L_5, L_4, (t7*) &_stringLiteral401, &m3973_MI);
		il2cpp_codegen_raise_exception(L_5);
	}

IL_004a:
	{
		return ((t6 *)Castclass(p0, InitializedTypeInfo(&t6_TI)));
	}
}
 bool m12072 (t118 * __this, t322  p0, MethodInfo* method){
	t6 * V_0 = {0};
	{
		int32_t L_0 = m1435((&p0), &m1435_MI);
		bool L_1 = (bool)VirtFuncInvoker2< bool, int32_t, t6 ** >::Invoke(&m1410_MI, __this, L_0, (&V_0));
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		return 0;
	}

IL_0013:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t2364_TI));
		t2364 * L_2 = m12181(NULL, &m12181_MI);
		t6 * L_3 = m1434((&p0), &m1434_MI);
		bool L_4 = (bool)VirtFuncInvoker2< bool, t6 *, t6 * >::Invoke(&m27292_MI, L_2, L_3, V_0);
		return L_4;
	}
}
extern MethodInfo m1432_MI;
 t323  m1432 (t118 * __this, MethodInfo* method){
	{
		t323  L_0 = {0};
		m12133(&L_0, __this, &m12133_MI);
		return L_0;
	}
}
 t725  m12073 (t29 * __this, int32_t p0, t6 * p1, MethodInfo* method){
	{
		int32_t L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t44_TI), &L_0);
		t6 * L_2 = p1;
		t725  L_3 = {0};
		m3965(&L_3, L_1, ((t6 *)L_2), &m3965_MI);
		return L_3;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData>
extern Il2CppType t44_0_0_32849;
FieldInfo t118_f0_FieldInfo = 
{
	"INITIAL_SIZE", &t44_0_0_32849, &t118_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_32849;
FieldInfo t118_f1_FieldInfo = 
{
	"DEFAULT_LOAD_FACTOR", &t22_0_0_32849, &t118_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_32849;
FieldInfo t118_f2_FieldInfo = 
{
	"NO_SLOT", &t44_0_0_32849, &t118_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_32849;
FieldInfo t118_f3_FieldInfo = 
{
	"HASH_FLAG", &t44_0_0_32849, &t118_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t841_0_0_1;
FieldInfo t118_f4_FieldInfo = 
{
	"table", &t841_0_0_1, &t118_TI, offsetof(t118, f4), &EmptyCustomAttributesCache};
extern Il2CppType t1965_0_0_1;
FieldInfo t118_f5_FieldInfo = 
{
	"linkSlots", &t1965_0_0_1, &t118_TI, offsetof(t118, f5), &EmptyCustomAttributesCache};
extern Il2CppType t841_0_0_1;
FieldInfo t118_f6_FieldInfo = 
{
	"keySlots", &t841_0_0_1, &t118_TI, offsetof(t118, f6), &EmptyCustomAttributesCache};
extern Il2CppType t2343_0_0_1;
FieldInfo t118_f7_FieldInfo = 
{
	"valueSlots", &t2343_0_0_1, &t118_TI, offsetof(t118, f7), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t118_f8_FieldInfo = 
{
	"touchedSlots", &t44_0_0_1, &t118_TI, offsetof(t118, f8), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t118_f9_FieldInfo = 
{
	"emptySlot", &t44_0_0_1, &t118_TI, offsetof(t118, f9), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t118_f10_FieldInfo = 
{
	"count", &t44_0_0_1, &t118_TI, offsetof(t118, f10), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t118_f11_FieldInfo = 
{
	"threshold", &t44_0_0_1, &t118_TI, offsetof(t118, f11), &EmptyCustomAttributesCache};
extern Il2CppType t2344_0_0_1;
FieldInfo t118_f12_FieldInfo = 
{
	"hcp", &t2344_0_0_1, &t118_TI, offsetof(t118, f12), &EmptyCustomAttributesCache};
extern Il2CppType t733_0_0_1;
FieldInfo t118_f13_FieldInfo = 
{
	"serialization_info", &t733_0_0_1, &t118_TI, offsetof(t118, f13), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t118_f14_FieldInfo = 
{
	"generation", &t44_0_0_1, &t118_TI, offsetof(t118, f14), &EmptyCustomAttributesCache};
extern Il2CppType t2345_0_0_17;
extern CustomAttributesCache t1254__CustomAttributeCache_U3CU3Ef__am$cacheB;
FieldInfo t118_f15_FieldInfo = 
{
	"<>f__am$cacheB", &t2345_0_0_17, &t118_TI, offsetof(t118_SFs, f15), &t1254__CustomAttributeCache_U3CU3Ef__am$cacheB};
static FieldInfo* t118_FIs[] =
{
	&t118_f0_FieldInfo,
	&t118_f1_FieldInfo,
	&t118_f2_FieldInfo,
	&t118_f3_FieldInfo,
	&t118_f4_FieldInfo,
	&t118_f5_FieldInfo,
	&t118_f6_FieldInfo,
	&t118_f7_FieldInfo,
	&t118_f8_FieldInfo,
	&t118_f9_FieldInfo,
	&t118_f10_FieldInfo,
	&t118_f11_FieldInfo,
	&t118_f12_FieldInfo,
	&t118_f13_FieldInfo,
	&t118_f14_FieldInfo,
	&t118_f15_FieldInfo,
	NULL
};
static const int32_t t118_f0_DefaultValueData = 10;
static Il2CppFieldDefaultValueEntry t118_f0_DefaultValue = 
{
	&t118_f0_FieldInfo, { (char*)&t118_f0_DefaultValueData, &t44_0_0_0 }};
static const float t118_f1_DefaultValueData = 0.9f;
extern Il2CppType t22_0_0_0;
static Il2CppFieldDefaultValueEntry t118_f1_DefaultValue = 
{
	&t118_f1_FieldInfo, { (char*)&t118_f1_DefaultValueData, &t22_0_0_0 }};
static const int32_t t118_f2_DefaultValueData = -1;
static Il2CppFieldDefaultValueEntry t118_f2_DefaultValue = 
{
	&t118_f2_FieldInfo, { (char*)&t118_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t118_f3_DefaultValueData = -2147483648;
static Il2CppFieldDefaultValueEntry t118_f3_DefaultValue = 
{
	&t118_f3_FieldInfo, { (char*)&t118_f3_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t118_FDVs[] = 
{
	&t118_f0_DefaultValue,
	&t118_f1_DefaultValue,
	&t118_f2_DefaultValue,
	&t118_f3_DefaultValue,
	NULL
};
static PropertyInfo t118____System_Collections_IDictionary_Item_PropertyInfo = 
{
	&t118_TI, "System.Collections.IDictionary.Item", &m12041_MI, &m12042_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t118____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t118_TI, "System.Collections.ICollection.IsSynchronized", &m12045_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t118____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t118_TI, "System.Collections.ICollection.SyncRoot", &m12046_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t118____System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_IsReadOnly_PropertyInfo = 
{
	&t118_TI, "System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.IsReadOnly", &m12047_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t118____Count_PropertyInfo = 
{
	&t118_TI, "Count", &m12056_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t118____Item_PropertyInfo = 
{
	&t118_TI, "Item", &m12057_MI, &m12058_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t118____Values_PropertyInfo = 
{
	&t118_TI, "Values", &m1424_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t118_PIs[] =
{
	&t118____System_Collections_IDictionary_Item_PropertyInfo,
	&t118____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t118____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t118____System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_IsReadOnly_PropertyInfo,
	&t118____Count_PropertyInfo,
	&t118____Item_PropertyInfo,
	&t118____Values_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1409_GM;
MethodInfo m1409_MI = 
{
	".ctor", (methodPointerType)&m1409, &t118_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1409_GM};
extern Il2CppType t2344_0_0_0;
static ParameterInfo t118_m12038_ParameterInfos[] = 
{
	{"comparer", 0, 134217728, &EmptyCustomAttributesCache, &t2344_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12038_GM;
MethodInfo m12038_MI = 
{
	".ctor", (methodPointerType)&m12038, &t118_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t118_m12038_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12038_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t118_m12039_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12039_GM;
MethodInfo m12039_MI = 
{
	".ctor", (methodPointerType)&m12039, &t118_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t118_m12039_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12039_GM};
extern Il2CppType t733_0_0_0;
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t118_m12040_ParameterInfos[] = 
{
	{"info", 0, 134217728, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134217728, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12040_GM;
MethodInfo m12040_MI = 
{
	".ctor", (methodPointerType)&m12040, &t118_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t118_m12040_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12040_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t118_m12041_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12041_GM;
MethodInfo m12041_MI = 
{
	"System.Collections.IDictionary.get_Item", (methodPointerType)&m12041, &t118_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t118_m12041_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12041_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t118_m12042_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12042_GM;
MethodInfo m12042_MI = 
{
	"System.Collections.IDictionary.set_Item", (methodPointerType)&m12042, &t118_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t118_m12042_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 20, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12042_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t118_m12043_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12043_GM;
MethodInfo m12043_MI = 
{
	"System.Collections.IDictionary.Add", (methodPointerType)&m12043, &t118_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t118_m12043_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 21, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12043_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t118_m12044_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12044_GM;
MethodInfo m12044_MI = 
{
	"System.Collections.IDictionary.Remove", (methodPointerType)&m12044, &t118_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t118_m12044_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 23, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12044_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12045_GM;
MethodInfo m12045_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m12045, &t118_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12045_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12046_GM;
MethodInfo m12046_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m12046, &t118_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12046_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12047_GM;
MethodInfo m12047_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly", (methodPointerType)&m12047, &t118_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 11, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12047_GM};
extern Il2CppType t322_0_0_0;
extern Il2CppType t322_0_0_0;
static ParameterInfo t118_m12048_ParameterInfos[] = 
{
	{"keyValuePair", 0, 134217728, &EmptyCustomAttributesCache, &t322_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t322 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12048_GM;
MethodInfo m12048_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add", (methodPointerType)&m12048, &t118_TI, &t21_0_0_0, RuntimeInvoker_t21_t322, t118_m12048_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12048_GM};
extern Il2CppType t322_0_0_0;
static ParameterInfo t118_m12049_ParameterInfos[] = 
{
	{"keyValuePair", 0, 134217728, &EmptyCustomAttributesCache, &t322_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t322 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12049_GM;
MethodInfo m12049_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains", (methodPointerType)&m12049, &t118_TI, &t40_0_0_0, RuntimeInvoker_t40_t322, t118_m12049_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 14, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12049_GM};
extern Il2CppType t2346_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t118_m12050_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2346_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12050_GM;
MethodInfo m12050_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo", (methodPointerType)&m12050, &t118_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t118_m12050_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12050_GM};
extern Il2CppType t322_0_0_0;
static ParameterInfo t118_m12051_ParameterInfos[] = 
{
	{"keyValuePair", 0, 134217728, &EmptyCustomAttributesCache, &t322_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t322 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12051_GM;
MethodInfo m12051_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove", (methodPointerType)&m12051, &t118_TI, &t40_0_0_0, RuntimeInvoker_t40_t322, t118_m12051_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12051_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t118_m12052_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12052_GM;
MethodInfo m12052_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m12052, &t118_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t118_m12052_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12052_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12053_GM;
MethodInfo m12053_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m12053, &t118_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12053_GM};
extern Il2CppType t2347_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12054_GM;
MethodInfo m12054_MI = 
{
	"System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator", (methodPointerType)&m12054, &t118_TI, &t2347_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 17, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12054_GM};
extern Il2CppType t722_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12055_GM;
MethodInfo m12055_MI = 
{
	"System.Collections.IDictionary.GetEnumerator", (methodPointerType)&m12055, &t118_TI, &t722_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 22, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12055_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12056_GM;
MethodInfo m12056_MI = 
{
	"get_Count", (methodPointerType)&m12056, &t118_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12056_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t118_m12057_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t6_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12057_GM;
MethodInfo m12057_MI = 
{
	"get_Item", (methodPointerType)&m12057, &t118_TI, &t6_0_0_0, RuntimeInvoker_t29_t44, t118_m12057_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 25, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12057_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t6_0_0_0;
static ParameterInfo t118_m12058_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12058_GM;
MethodInfo m12058_MI = 
{
	"set_Item", (methodPointerType)&m12058, &t118_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t118_m12058_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 26, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12058_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2344_0_0_0;
static ParameterInfo t118_m12059_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"hcp", 1, 134217728, &EmptyCustomAttributesCache, &t2344_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12059_GM;
MethodInfo m12059_MI = 
{
	"Init", (methodPointerType)&m12059, &t118_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t118_m12059_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12059_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t118_m12060_ParameterInfos[] = 
{
	{"size", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12060_GM;
MethodInfo m12060_MI = 
{
	"InitArrays", (methodPointerType)&m12060, &t118_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t118_m12060_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12060_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t118_m12061_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12061_GM;
MethodInfo m12061_MI = 
{
	"CopyToCheck", (methodPointerType)&m12061, &t118_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t118_m12061_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12061_GM};
extern Il2CppType t1957_0_0_0;
extern Il2CppType t1957_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t6643_0_0_0;
extern Il2CppType t6643_0_0_0;
static ParameterInfo t118_m27293_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1957_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"transform", 2, 134217728, &EmptyCustomAttributesCache, &t6643_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericContainer m27293_IGC;
extern TypeInfo m27293_gp_TRet_0_TI;
Il2CppGenericParamFull m27293_gp_TRet_0_TI_GenericParamFull = { { &m27293_IGC, 0}, {NULL, "TRet", 0, 0, NULL} };
extern TypeInfo m27293_gp_TElem_1_TI;
Il2CppGenericParamFull m27293_gp_TElem_1_TI_GenericParamFull = { { &m27293_IGC, 1}, {NULL, "TElem", 0, 0, NULL} };
static Il2CppGenericParamFull* m27293_IGPA[2] = 
{
	&m27293_gp_TRet_0_TI_GenericParamFull,
	&m27293_gp_TElem_1_TI_GenericParamFull,
};
extern MethodInfo m27293_MI;
Il2CppGenericContainer m27293_IGC = { { NULL, NULL }, NULL, &m27293_MI, 2, 1, m27293_IGPA };
extern Il2CppGenericMethod m27294_GM;
extern Il2CppType m9954_gp_0_0_0_0;
extern Il2CppType m9954_gp_1_0_0_0;
static Il2CppRGCTXDefinition m27293_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &m27294_GM }/* Method Definition */,
	{ IL2CPP_RGCTX_DATA_CLASS, &m9954_gp_0_0_0_0 }/* Class Definition */,
	{ IL2CPP_RGCTX_DATA_CLASS, &m9954_gp_1_0_0_0 }/* Class Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppGenericMethod m27293_GM;
MethodInfo m27293_MI = 
{
	"Do_CopyTo", NULL, &t118_TI, &t21_0_0_0, NULL, t118_m27293_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, true, true, 0, m27293_RGCTXData, (methodPointerType)NULL, &m27293_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t6_0_0_0;
static ParameterInfo t118_m12062_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t322_0_0_0;
extern void* RuntimeInvoker_t322_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12062_GM;
MethodInfo m12062_MI = 
{
	"make_pair", (methodPointerType)&m12062, &t118_TI, &t322_0_0_0, RuntimeInvoker_t322_t44_t29, t118_m12062_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12062_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t6_0_0_0;
static ParameterInfo t118_m12063_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t6_0_0_0;
extern void* RuntimeInvoker_t29_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12063_GM;
MethodInfo m12063_MI = 
{
	"pick_value", (methodPointerType)&m12063, &t118_TI, &t6_0_0_0, RuntimeInvoker_t29_t44_t29, t118_m12063_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12063_GM};
extern Il2CppType t2346_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t118_m12064_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2346_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12064_GM;
MethodInfo m12064_MI = 
{
	"CopyTo", (methodPointerType)&m12064, &t118_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t118_m12064_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12064_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t6646_0_0_0;
extern Il2CppType t6646_0_0_0;
static ParameterInfo t118_m27295_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"transform", 2, 134217728, &EmptyCustomAttributesCache, &t6646_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericContainer m27295_IGC;
extern TypeInfo m27295_gp_TRet_0_TI;
Il2CppGenericParamFull m27295_gp_TRet_0_TI_GenericParamFull = { { &m27295_IGC, 0}, {NULL, "TRet", 0, 0, NULL} };
static Il2CppGenericParamFull* m27295_IGPA[1] = 
{
	&m27295_gp_TRet_0_TI_GenericParamFull,
};
extern MethodInfo m27295_MI;
Il2CppGenericContainer m27295_IGC = { { NULL, NULL }, NULL, &m27295_MI, 1, 1, m27295_IGPA };
extern Il2CppType m9959_gp_0_0_0_0;
extern Il2CppGenericMethod m27296_GM;
static Il2CppRGCTXDefinition m27295_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, &m9959_gp_0_0_0_0 }/* Type Definition */,
	{ IL2CPP_RGCTX_DATA_METHOD, &m27296_GM }/* Method Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppGenericMethod m27295_GM;
MethodInfo m27295_MI = 
{
	"Do_ICollectionCopyTo", NULL, &t118_TI, &t21_0_0_0, NULL, t118_m27295_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, true, true, 0, m27295_RGCTXData, (methodPointerType)NULL, &m27295_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12065_GM;
MethodInfo m12065_MI = 
{
	"Resize", (methodPointerType)&m12065, &t118_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12065_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t6_0_0_0;
static ParameterInfo t118_m1411_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1411_GM;
MethodInfo m1411_MI = 
{
	"Add", (methodPointerType)&m1411, &t118_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t118_m1411_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1411_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1429_GM;
MethodInfo m1429_MI = 
{
	"Clear", (methodPointerType)&m1429, &t118_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 13, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1429_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t118_m12066_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12066_GM;
MethodInfo m12066_MI = 
{
	"ContainsKey", (methodPointerType)&m12066, &t118_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t118_m12066_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12066_GM};
extern Il2CppType t6_0_0_0;
static ParameterInfo t118_m12067_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12067_GM;
MethodInfo m12067_MI = 
{
	"ContainsValue", (methodPointerType)&m12067, &t118_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t118_m12067_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12067_GM};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t118_m12068_ParameterInfos[] = 
{
	{"info", 0, 134217728, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134217728, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12068_GM;
MethodInfo m12068_MI = 
{
	"GetObjectData", (methodPointerType)&m12068, &t118_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t118_m12068_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12068_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t118_m12069_ParameterInfos[] = 
{
	{"sender", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12069_GM;
MethodInfo m12069_MI = 
{
	"OnDeserialization", (methodPointerType)&m12069, &t118_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t118_m12069_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12069_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t118_m1412_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1412_GM;
MethodInfo m1412_MI = 
{
	"Remove", (methodPointerType)&m1412, &t118_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t118_m1412_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1412_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t6_1_0_2;
extern Il2CppType t6_1_0_0;
static ParameterInfo t118_m1410_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t6_1_0_2},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44_t325 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1410_GM;
MethodInfo m1410_MI = 
{
	"TryGetValue", (methodPointerType)&m1410, &t118_TI, &t40_0_0_0, RuntimeInvoker_t40_t44_t325, t118_m1410_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1410_GM};
extern Il2CppType t321_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1424_GM;
MethodInfo m1424_MI = 
{
	"get_Values", (methodPointerType)&m1424, &t118_TI, &t321_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1424_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t118_m12070_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12070_GM;
MethodInfo m12070_MI = 
{
	"ToTKey", (methodPointerType)&m12070, &t118_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t118_m12070_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12070_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t118_m12071_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t6_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12071_GM;
MethodInfo m12071_MI = 
{
	"ToTValue", (methodPointerType)&m12071, &t118_TI, &t6_0_0_0, RuntimeInvoker_t29_t29, t118_m12071_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12071_GM};
extern Il2CppType t322_0_0_0;
static ParameterInfo t118_m12072_ParameterInfos[] = 
{
	{"pair", 0, 134217728, &EmptyCustomAttributesCache, &t322_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t322 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12072_GM;
MethodInfo m12072_MI = 
{
	"ContainsKeyValuePair", (methodPointerType)&m12072, &t118_TI, &t40_0_0_0, RuntimeInvoker_t40_t322, t118_m12072_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12072_GM};
extern Il2CppType t323_0_0_0;
extern void* RuntimeInvoker_t323 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1432_GM;
MethodInfo m1432_MI = 
{
	"GetEnumerator", (methodPointerType)&m1432, &t118_TI, &t323_0_0_0, RuntimeInvoker_t323, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1432_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t6_0_0_0;
static ParameterInfo t118_m12073_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725_t44_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1254__CustomAttributeCache_m9975;
extern Il2CppGenericMethod m12073_GM;
MethodInfo m12073_MI = 
{
	"<CopyTo>m__0", (methodPointerType)&m12073, &t118_TI, &t725_0_0_0, RuntimeInvoker_t725_t44_t29, t118_m12073_ParameterInfos, &t1254__CustomAttributeCache_m9975, 145, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12073_GM};
static MethodInfo* t118_MIs[] =
{
	&m1409_MI,
	&m12038_MI,
	&m12039_MI,
	&m12040_MI,
	&m12041_MI,
	&m12042_MI,
	&m12043_MI,
	&m12044_MI,
	&m12045_MI,
	&m12046_MI,
	&m12047_MI,
	&m12048_MI,
	&m12049_MI,
	&m12050_MI,
	&m12051_MI,
	&m12052_MI,
	&m12053_MI,
	&m12054_MI,
	&m12055_MI,
	&m12056_MI,
	&m12057_MI,
	&m12058_MI,
	&m12059_MI,
	&m12060_MI,
	&m12061_MI,
	&m27293_MI,
	&m12062_MI,
	&m12063_MI,
	&m12064_MI,
	&m27295_MI,
	&m12065_MI,
	&m1411_MI,
	&m1429_MI,
	&m12066_MI,
	&m12067_MI,
	&m12068_MI,
	&m12069_MI,
	&m1412_MI,
	&m1410_MI,
	&m1424_MI,
	&m12070_MI,
	&m12071_MI,
	&m12072_MI,
	&m1432_MI,
	&m12073_MI,
	NULL
};
static MethodInfo* t118_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12053_MI,
	&m12068_MI,
	&m12056_MI,
	&m12045_MI,
	&m12046_MI,
	&m12052_MI,
	&m12056_MI,
	&m12047_MI,
	&m12048_MI,
	&m1429_MI,
	&m12049_MI,
	&m12050_MI,
	&m12051_MI,
	&m12054_MI,
	&m1412_MI,
	&m12041_MI,
	&m12042_MI,
	&m12043_MI,
	&m12055_MI,
	&m12044_MI,
	&m12069_MI,
	&m12057_MI,
	&m12058_MI,
	&m1411_MI,
	&m12066_MI,
	&m12068_MI,
	&m12069_MI,
	&m1410_MI,
};
extern TypeInfo t5250_TI;
extern TypeInfo t5252_TI;
extern TypeInfo t6648_TI;
extern TypeInfo t721_TI;
extern TypeInfo t918_TI;
static TypeInfo* t118_ITIs[] = 
{
	&t603_TI,
	&t374_TI,
	&t674_TI,
	&t5250_TI,
	&t5252_TI,
	&t6648_TI,
	&t721_TI,
	&t918_TI,
};
static Il2CppInterfaceOffsetPair t118_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t374_TI, 5},
	{ &t674_TI, 6},
	{ &t5250_TI, 10},
	{ &t5252_TI, 17},
	{ &t6648_TI, 18},
	{ &t721_TI, 19},
	{ &t918_TI, 24},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t118_0_0_0;
extern Il2CppType t118_1_0_0;
struct t118;
extern Il2CppGenericClass t118_GC;
extern CustomAttributesCache t1254__CustomAttributeCache;
extern CustomAttributesCache t1254__CustomAttributeCache_U3CU3Ef__am$cacheB;
extern CustomAttributesCache t1254__CustomAttributeCache_m9975;
TypeInfo t118_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Dictionary`2", "System.Collections.Generic", t118_MIs, t118_PIs, t118_FIs, NULL, &t29_TI, NULL, NULL, &t118_TI, t118_ITIs, t118_VT, &t1254__CustomAttributeCache, &t118_TI, &t118_0_0_0, &t118_1_0_0, t118_IOs, &t118_GC, NULL, t118_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t118), 0, -1, sizeof(t118_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 45, 7, 16, 0, 0, 32, 8, 8};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>>
extern MethodInfo m27297_MI;
static PropertyInfo t5250____Count_PropertyInfo = 
{
	&t5250_TI, "Count", &m27297_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27298_MI;
static PropertyInfo t5250____IsReadOnly_PropertyInfo = 
{
	&t5250_TI, "IsReadOnly", &m27298_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5250_PIs[] =
{
	&t5250____Count_PropertyInfo,
	&t5250____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27297_GM;
MethodInfo m27297_MI = 
{
	"get_Count", NULL, &t5250_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27297_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27298_GM;
MethodInfo m27298_MI = 
{
	"get_IsReadOnly", NULL, &t5250_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27298_GM};
extern Il2CppType t322_0_0_0;
static ParameterInfo t5250_m27299_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t322_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t322 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27299_GM;
MethodInfo m27299_MI = 
{
	"Add", NULL, &t5250_TI, &t21_0_0_0, RuntimeInvoker_t21_t322, t5250_m27299_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27299_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27300_GM;
MethodInfo m27300_MI = 
{
	"Clear", NULL, &t5250_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27300_GM};
extern Il2CppType t322_0_0_0;
static ParameterInfo t5250_m27301_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t322_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t322 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27301_GM;
MethodInfo m27301_MI = 
{
	"Contains", NULL, &t5250_TI, &t40_0_0_0, RuntimeInvoker_t40_t322, t5250_m27301_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27301_GM};
extern Il2CppType t2346_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5250_m27302_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2346_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27302_GM;
MethodInfo m27302_MI = 
{
	"CopyTo", NULL, &t5250_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5250_m27302_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27302_GM};
extern Il2CppType t322_0_0_0;
static ParameterInfo t5250_m27303_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t322_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t322 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27303_GM;
MethodInfo m27303_MI = 
{
	"Remove", NULL, &t5250_TI, &t40_0_0_0, RuntimeInvoker_t40_t322, t5250_m27303_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27303_GM};
static MethodInfo* t5250_MIs[] =
{
	&m27297_MI,
	&m27298_MI,
	&m27299_MI,
	&m27300_MI,
	&m27301_MI,
	&m27302_MI,
	&m27303_MI,
	NULL
};
static TypeInfo* t5250_ITIs[] = 
{
	&t603_TI,
	&t5252_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5250_0_0_0;
extern Il2CppType t5250_1_0_0;
struct t5250;
extern Il2CppGenericClass t5250_GC;
TypeInfo t5250_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5250_MIs, t5250_PIs, NULL, NULL, NULL, NULL, NULL, &t5250_TI, t5250_ITIs, NULL, &EmptyCustomAttributesCache, &t5250_TI, &t5250_0_0_0, &t5250_1_0_0, NULL, &t5250_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>>
extern Il2CppType t2347_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27304_GM;
MethodInfo m27304_MI = 
{
	"GetEnumerator", NULL, &t5252_TI, &t2347_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27304_GM};
static MethodInfo* t5252_MIs[] =
{
	&m27304_MI,
	NULL
};
static TypeInfo* t5252_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5252_0_0_0;
extern Il2CppType t5252_1_0_0;
struct t5252;
extern Il2CppGenericClass t5252_GC;
TypeInfo t5252_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5252_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5252_TI, t5252_ITIs, NULL, &EmptyCustomAttributesCache, &t5252_TI, &t5252_0_0_0, &t5252_1_0_0, NULL, &t5252_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2347_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>>
extern MethodInfo m27305_MI;
static PropertyInfo t2347____Current_PropertyInfo = 
{
	&t2347_TI, "Current", &m27305_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2347_PIs[] =
{
	&t2347____Current_PropertyInfo,
	NULL
};
extern Il2CppType t322_0_0_0;
extern void* RuntimeInvoker_t322 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27305_GM;
MethodInfo m27305_MI = 
{
	"get_Current", NULL, &t2347_TI, &t322_0_0_0, RuntimeInvoker_t322, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27305_GM};
static MethodInfo* t2347_MIs[] =
{
	&m27305_MI,
	NULL
};
static TypeInfo* t2347_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2347_0_0_0;
extern Il2CppType t2347_1_0_0;
struct t2347;
extern Il2CppGenericClass t2347_GC;
TypeInfo t2347_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2347_MIs, t2347_PIs, NULL, NULL, NULL, NULL, NULL, &t2347_TI, t2347_ITIs, NULL, &EmptyCustomAttributesCache, &t2347_TI, &t2347_0_0_0, &t2347_1_0_0, NULL, &t2347_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo t446_TI;
extern MethodInfo m12075_MI;
extern MethodInfo m12076_MI;
extern MethodInfo m4008_MI;


 void m12074 (t322 * __this, int32_t p0, t6 * p1, MethodInfo* method){
	{
		m12075(__this, p0, &m12075_MI);
		m12076(__this, p1, &m12076_MI);
		return;
	}
}
 int32_t m1435 (t322 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f0);
		return L_0;
	}
}
 void m12075 (t322 * __this, int32_t p0, MethodInfo* method){
	{
		__this->f0 = p0;
		return;
	}
}
 t6 * m1434 (t322 * __this, MethodInfo* method){
	{
		t6 * L_0 = (__this->f1);
		return L_0;
	}
}
 void m12076 (t322 * __this, t6 * p0, MethodInfo* method){
	{
		__this->f1 = p0;
		return;
	}
}
extern MethodInfo m1462_MI;
 t7* m1462 (t322 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	t6 * V_1 = {0};
	int32_t G_B2_0 = 0;
	t446* G_B2_1 = {0};
	t446* G_B2_2 = {0};
	int32_t G_B1_0 = 0;
	t446* G_B1_1 = {0};
	t446* G_B1_2 = {0};
	t7* G_B3_0 = {0};
	int32_t G_B3_1 = 0;
	t446* G_B3_2 = {0};
	t446* G_B3_3 = {0};
	int32_t G_B5_0 = 0;
	t446* G_B5_1 = {0};
	t446* G_B5_2 = {0};
	int32_t G_B4_0 = 0;
	t446* G_B4_1 = {0};
	t446* G_B4_2 = {0};
	t7* G_B6_0 = {0};
	int32_t G_B6_1 = 0;
	t446* G_B6_2 = {0};
	t446* G_B6_3 = {0};
	{
		t446* L_0 = ((t446*)SZArrayNew(InitializedTypeInfo(&t446_TI), 5));
		ArrayElementTypeCheck (L_0, (t7*) &_stringLiteral175);
		*((t7**)(t7**)SZArrayLdElema(L_0, 0)) = (t7*)(t7*) &_stringLiteral175;
		t446* L_1 = L_0;
		int32_t L_2 = m1435(__this, &m1435_MI);
		int32_t L_3 = L_2;
		t29 * L_4 = Box(InitializedTypeInfo(&t44_TI), &L_3);
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_4)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0033;
		}
	}
	{
		int32_t L_5 = m1435(__this, &m1435_MI);
		V_0 = L_5;
		t7* L_6 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1332_MI, Box(InitializedTypeInfo(&t44_TI), &(*(&V_0))));
		G_B3_0 = L_6;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_0038;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		G_B3_0 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_0038:
	{
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		*((t7**)(t7**)SZArrayLdElema(G_B3_2, G_B3_1)) = (t7*)G_B3_0;
		t446* L_7 = G_B3_3;
		ArrayElementTypeCheck (L_7, (t7*) &_stringLiteral184);
		*((t7**)(t7**)SZArrayLdElema(L_7, 2)) = (t7*)(t7*) &_stringLiteral184;
		t446* L_8 = L_7;
		t6 * L_9 = m1434(__this, &m1434_MI);
		t6 * L_10 = L_9;
		G_B4_0 = 3;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
		if (!((t6 *)L_10))
		{
			G_B5_0 = 3;
			G_B5_1 = L_8;
			G_B5_2 = L_8;
			goto IL_0066;
		}
	}
	{
		t6 * L_11 = m1434(__this, &m1434_MI);
		V_1 = L_11;
		t7* L_12 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1332_MI, (*(&V_1)));
		G_B6_0 = L_12;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_006b;
	}

IL_0066:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		G_B6_0 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_006b:
	{
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		*((t7**)(t7**)SZArrayLdElema(G_B6_2, G_B6_1)) = (t7*)G_B6_0;
		t446* L_13 = G_B6_3;
		ArrayElementTypeCheck (L_13, (t7*) &_stringLiteral176);
		*((t7**)(t7**)SZArrayLdElema(L_13, 4)) = (t7*)(t7*) &_stringLiteral176;
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_14 = m4008(NULL, L_13, &m4008_MI);
		return L_14;
	}
}
// Metadata Definition System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>
extern Il2CppType t44_0_0_1;
FieldInfo t322_f0_FieldInfo = 
{
	"key", &t44_0_0_1, &t322_TI, offsetof(t322, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t6_0_0_1;
FieldInfo t322_f1_FieldInfo = 
{
	"value", &t6_0_0_1, &t322_TI, offsetof(t322, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t322_FIs[] =
{
	&t322_f0_FieldInfo,
	&t322_f1_FieldInfo,
	NULL
};
static PropertyInfo t322____Key_PropertyInfo = 
{
	&t322_TI, "Key", &m1435_MI, &m12075_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t322____Value_PropertyInfo = 
{
	&t322_TI, "Value", &m1434_MI, &m12076_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t322_PIs[] =
{
	&t322____Key_PropertyInfo,
	&t322____Value_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern Il2CppType t6_0_0_0;
static ParameterInfo t322_m12074_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12074_GM;
MethodInfo m12074_MI = 
{
	".ctor", (methodPointerType)&m12074, &t322_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t322_m12074_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12074_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1435_GM;
MethodInfo m1435_MI = 
{
	"get_Key", (methodPointerType)&m1435, &t322_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1435_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t322_m12075_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12075_GM;
MethodInfo m12075_MI = 
{
	"set_Key", (methodPointerType)&m12075, &t322_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t322_m12075_ParameterInfos, &EmptyCustomAttributesCache, 2177, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12075_GM};
extern Il2CppType t6_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1434_GM;
MethodInfo m1434_MI = 
{
	"get_Value", (methodPointerType)&m1434, &t322_TI, &t6_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1434_GM};
extern Il2CppType t6_0_0_0;
static ParameterInfo t322_m12076_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12076_GM;
MethodInfo m12076_MI = 
{
	"set_Value", (methodPointerType)&m12076, &t322_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t322_m12076_ParameterInfos, &EmptyCustomAttributesCache, 2177, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12076_GM};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1462_GM;
MethodInfo m1462_MI = 
{
	"ToString", (methodPointerType)&m1462, &t322_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1462_GM};
static MethodInfo* t322_MIs[] =
{
	&m12074_MI,
	&m1435_MI,
	&m12075_MI,
	&m1434_MI,
	&m12076_MI,
	&m1462_MI,
	NULL
};
static MethodInfo* t322_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1462_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t322_1_0_0;
extern Il2CppGenericClass t322_GC;
extern CustomAttributesCache t1259__CustomAttributeCache;
TypeInfo t322_TI = 
{
	&g_mscorlib_dll_Image, NULL, "KeyValuePair`2", "System.Collections.Generic", t322_MIs, t322_PIs, t322_FIs, NULL, &t110_TI, NULL, NULL, &t322_TI, NULL, t322_VT, &t1259__CustomAttributeCache, &t322_TI, &t322_0_0_0, &t322_1_0_0, NULL, &t322_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t322)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057033, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 2, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3276_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.String>
extern MethodInfo m27306_MI;
static PropertyInfo t3276____Current_PropertyInfo = 
{
	&t3276_TI, "Current", &m27306_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3276_PIs[] =
{
	&t3276____Current_PropertyInfo,
	NULL
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27306_GM;
MethodInfo m27306_MI = 
{
	"get_Current", NULL, &t3276_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27306_GM};
static MethodInfo* t3276_MIs[] =
{
	&m27306_MI,
	NULL
};
static TypeInfo* t3276_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3276_0_0_0;
extern Il2CppType t3276_1_0_0;
struct t3276;
extern Il2CppGenericClass t3276_GC;
TypeInfo t3276_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t3276_MIs, t3276_PIs, NULL, NULL, NULL, NULL, NULL, &t3276_TI, t3276_ITIs, NULL, &EmptyCustomAttributesCache, &t3276_TI, &t3276_0_0_0, &t3276_1_0_0, NULL, &t3276_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2348.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2348_TI;
#include "t2348MD.h"

extern MethodInfo m12081_MI;
extern MethodInfo m20422_MI;
struct t20;
#define m20422(__this, p0, method) (t7*)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.String>
extern Il2CppType t20_0_0_1;
FieldInfo t2348_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2348_TI, offsetof(t2348, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2348_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2348_TI, offsetof(t2348, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2348_FIs[] =
{
	&t2348_f0_FieldInfo,
	&t2348_f1_FieldInfo,
	NULL
};
extern MethodInfo m12078_MI;
static PropertyInfo t2348____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2348_TI, "System.Collections.IEnumerator.Current", &m12078_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2348____Current_PropertyInfo = 
{
	&t2348_TI, "Current", &m12081_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2348_PIs[] =
{
	&t2348____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2348____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2348_m12077_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12077_GM;
MethodInfo m12077_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2348_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2348_m12077_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12077_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12078_GM;
MethodInfo m12078_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2348_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12078_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12079_GM;
MethodInfo m12079_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2348_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12079_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12080_GM;
MethodInfo m12080_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2348_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12080_GM};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12081_GM;
MethodInfo m12081_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2348_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12081_GM};
static MethodInfo* t2348_MIs[] =
{
	&m12077_MI,
	&m12078_MI,
	&m12079_MI,
	&m12080_MI,
	&m12081_MI,
	NULL
};
extern MethodInfo m12080_MI;
extern MethodInfo m12079_MI;
static MethodInfo* t2348_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m12078_MI,
	&m12080_MI,
	&m12079_MI,
	&m12081_MI,
};
static TypeInfo* t2348_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3276_TI,
};
static Il2CppInterfaceOffsetPair t2348_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3276_TI, 7},
};
extern TypeInfo t7_TI;
static Il2CppRGCTXData t2348_RGCTXData[3] = 
{
	&m12081_MI/* Method Usage */,
	&t7_TI/* Class Usage */,
	&m20422_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2348_0_0_0;
extern Il2CppType t2348_1_0_0;
extern Il2CppGenericClass t2348_GC;
TypeInfo t2348_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2348_MIs, t2348_PIs, t2348_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2348_TI, t2348_ITIs, t2348_VT, &EmptyCustomAttributesCache, &t2348_TI, &t2348_0_0_0, &t2348_1_0_0, t2348_IOs, &t2348_GC, NULL, NULL, NULL, t2348_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2348)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3277_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.String>
extern MethodInfo m27307_MI;
static PropertyInfo t3277____Count_PropertyInfo = 
{
	&t3277_TI, "Count", &m27307_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27308_MI;
static PropertyInfo t3277____IsReadOnly_PropertyInfo = 
{
	&t3277_TI, "IsReadOnly", &m27308_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3277_PIs[] =
{
	&t3277____Count_PropertyInfo,
	&t3277____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27307_GM;
MethodInfo m27307_MI = 
{
	"get_Count", NULL, &t3277_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27307_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27308_GM;
MethodInfo m27308_MI = 
{
	"get_IsReadOnly", NULL, &t3277_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27308_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t3277_m27309_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27309_GM;
MethodInfo m27309_MI = 
{
	"Add", NULL, &t3277_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3277_m27309_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27309_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27310_GM;
MethodInfo m27310_MI = 
{
	"Clear", NULL, &t3277_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27310_GM};
extern Il2CppType t7_0_0_0;
static ParameterInfo t3277_m27311_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27311_GM;
MethodInfo m27311_MI = 
{
	"Contains", NULL, &t3277_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3277_m27311_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27311_GM};
extern Il2CppType t446_0_0_0;
extern Il2CppType t446_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3277_m27312_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t446_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27312_GM;
MethodInfo m27312_MI = 
{
	"CopyTo", NULL, &t3277_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3277_m27312_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27312_GM};
extern Il2CppType t7_0_0_0;
static ParameterInfo t3277_m27313_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27313_GM;
MethodInfo m27313_MI = 
{
	"Remove", NULL, &t3277_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t3277_m27313_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27313_GM};
static MethodInfo* t3277_MIs[] =
{
	&m27307_MI,
	&m27308_MI,
	&m27309_MI,
	&m27310_MI,
	&m27311_MI,
	&m27312_MI,
	&m27313_MI,
	NULL
};
extern TypeInfo t3278_TI;
static TypeInfo* t3277_ITIs[] = 
{
	&t603_TI,
	&t3278_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3277_0_0_0;
extern Il2CppType t3277_1_0_0;
struct t3277;
extern Il2CppGenericClass t3277_GC;
TypeInfo t3277_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t3277_MIs, t3277_PIs, NULL, NULL, NULL, NULL, NULL, &t3277_TI, t3277_ITIs, NULL, &EmptyCustomAttributesCache, &t3277_TI, &t3277_0_0_0, &t3277_1_0_0, NULL, &t3277_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.String>
extern Il2CppType t3276_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27314_GM;
MethodInfo m27314_MI = 
{
	"GetEnumerator", NULL, &t3278_TI, &t3276_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27314_GM};
static MethodInfo* t3278_MIs[] =
{
	&m27314_MI,
	NULL
};
static TypeInfo* t3278_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3278_0_0_0;
extern Il2CppType t3278_1_0_0;
struct t3278;
extern Il2CppGenericClass t3278_GC;
TypeInfo t3278_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t3278_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t3278_TI, t3278_ITIs, NULL, &EmptyCustomAttributesCache, &t3278_TI, &t3278_0_0_0, &t3278_1_0_0, NULL, &t3278_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3283_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.String>
extern MethodInfo m27315_MI;
extern MethodInfo m27316_MI;
static PropertyInfo t3283____Item_PropertyInfo = 
{
	&t3283_TI, "Item", &m27315_MI, &m27316_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3283_PIs[] =
{
	&t3283____Item_PropertyInfo,
	NULL
};
extern Il2CppType t7_0_0_0;
static ParameterInfo t3283_m27317_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27317_GM;
MethodInfo m27317_MI = 
{
	"IndexOf", NULL, &t3283_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3283_m27317_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27317_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t3283_m27318_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27318_GM;
MethodInfo m27318_MI = 
{
	"Insert", NULL, &t3283_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3283_m27318_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27318_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3283_m27319_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27319_GM;
MethodInfo m27319_MI = 
{
	"RemoveAt", NULL, &t3283_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t3283_m27319_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27319_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t3283_m27315_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27315_GM;
MethodInfo m27315_MI = 
{
	"get_Item", NULL, &t3283_TI, &t7_0_0_0, RuntimeInvoker_t29_t44, t3283_m27315_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27315_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t3283_m27316_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27316_GM;
MethodInfo m27316_MI = 
{
	"set_Item", NULL, &t3283_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t3283_m27316_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27316_GM};
static MethodInfo* t3283_MIs[] =
{
	&m27317_MI,
	&m27318_MI,
	&m27319_MI,
	&m27315_MI,
	&m27316_MI,
	NULL
};
static TypeInfo* t3283_ITIs[] = 
{
	&t603_TI,
	&t3277_TI,
	&t3278_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3283_0_0_0;
extern Il2CppType t3283_1_0_0;
struct t3283;
extern Il2CppGenericClass t3283_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t3283_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t3283_MIs, t3283_PIs, NULL, NULL, NULL, NULL, NULL, &t3283_TI, t3283_ITIs, NULL, &t1908__CustomAttributeCache, &t3283_TI, &t3283_0_0_0, &t3283_1_0_0, NULL, &t3283_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5241_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.ICloneable>
extern MethodInfo m27320_MI;
static PropertyInfo t5241____Count_PropertyInfo = 
{
	&t5241_TI, "Count", &m27320_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27321_MI;
static PropertyInfo t5241____IsReadOnly_PropertyInfo = 
{
	&t5241_TI, "IsReadOnly", &m27321_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5241_PIs[] =
{
	&t5241____Count_PropertyInfo,
	&t5241____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27320_GM;
MethodInfo m27320_MI = 
{
	"get_Count", NULL, &t5241_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27320_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27321_GM;
MethodInfo m27321_MI = 
{
	"get_IsReadOnly", NULL, &t5241_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27321_GM};
extern Il2CppType t373_0_0_0;
extern Il2CppType t373_0_0_0;
static ParameterInfo t5241_m27322_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t373_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27322_GM;
MethodInfo m27322_MI = 
{
	"Add", NULL, &t5241_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5241_m27322_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27322_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27323_GM;
MethodInfo m27323_MI = 
{
	"Clear", NULL, &t5241_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27323_GM};
extern Il2CppType t373_0_0_0;
static ParameterInfo t5241_m27324_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t373_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27324_GM;
MethodInfo m27324_MI = 
{
	"Contains", NULL, &t5241_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5241_m27324_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27324_GM};
extern Il2CppType t3538_0_0_0;
extern Il2CppType t3538_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5241_m27325_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3538_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27325_GM;
MethodInfo m27325_MI = 
{
	"CopyTo", NULL, &t5241_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5241_m27325_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27325_GM};
extern Il2CppType t373_0_0_0;
static ParameterInfo t5241_m27326_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t373_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27326_GM;
MethodInfo m27326_MI = 
{
	"Remove", NULL, &t5241_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5241_m27326_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27326_GM};
static MethodInfo* t5241_MIs[] =
{
	&m27320_MI,
	&m27321_MI,
	&m27322_MI,
	&m27323_MI,
	&m27324_MI,
	&m27325_MI,
	&m27326_MI,
	NULL
};
extern TypeInfo t5243_TI;
static TypeInfo* t5241_ITIs[] = 
{
	&t603_TI,
	&t5243_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5241_0_0_0;
extern Il2CppType t5241_1_0_0;
struct t5241;
extern Il2CppGenericClass t5241_GC;
TypeInfo t5241_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5241_MIs, t5241_PIs, NULL, NULL, NULL, NULL, NULL, &t5241_TI, t5241_ITIs, NULL, &EmptyCustomAttributesCache, &t5241_TI, &t5241_0_0_0, &t5241_1_0_0, NULL, &t5241_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.ICloneable>
extern Il2CppType t4092_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27327_GM;
MethodInfo m27327_MI = 
{
	"GetEnumerator", NULL, &t5243_TI, &t4092_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27327_GM};
static MethodInfo* t5243_MIs[] =
{
	&m27327_MI,
	NULL
};
static TypeInfo* t5243_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5243_0_0_0;
extern Il2CppType t5243_1_0_0;
struct t5243;
extern Il2CppGenericClass t5243_GC;
TypeInfo t5243_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5243_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5243_TI, t5243_ITIs, NULL, &EmptyCustomAttributesCache, &t5243_TI, &t5243_0_0_0, &t5243_1_0_0, NULL, &t5243_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4092_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.ICloneable>
extern MethodInfo m27328_MI;
static PropertyInfo t4092____Current_PropertyInfo = 
{
	&t4092_TI, "Current", &m27328_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4092_PIs[] =
{
	&t4092____Current_PropertyInfo,
	NULL
};
extern Il2CppType t373_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27328_GM;
MethodInfo m27328_MI = 
{
	"get_Current", NULL, &t4092_TI, &t373_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27328_GM};
static MethodInfo* t4092_MIs[] =
{
	&m27328_MI,
	NULL
};
static TypeInfo* t4092_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4092_0_0_0;
extern Il2CppType t4092_1_0_0;
struct t4092;
extern Il2CppGenericClass t4092_GC;
TypeInfo t4092_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4092_MIs, t4092_PIs, NULL, NULL, NULL, NULL, NULL, &t4092_TI, t4092_ITIs, NULL, &EmptyCustomAttributesCache, &t4092_TI, &t4092_0_0_0, &t4092_1_0_0, NULL, &t4092_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2349.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2349_TI;
#include "t2349MD.h"

extern MethodInfo m12086_MI;
extern MethodInfo m20433_MI;
struct t20;
#define m20433(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.ICloneable>
extern Il2CppType t20_0_0_1;
FieldInfo t2349_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2349_TI, offsetof(t2349, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2349_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2349_TI, offsetof(t2349, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2349_FIs[] =
{
	&t2349_f0_FieldInfo,
	&t2349_f1_FieldInfo,
	NULL
};
extern MethodInfo m12083_MI;
static PropertyInfo t2349____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2349_TI, "System.Collections.IEnumerator.Current", &m12083_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2349____Current_PropertyInfo = 
{
	&t2349_TI, "Current", &m12086_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2349_PIs[] =
{
	&t2349____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2349____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2349_m12082_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12082_GM;
MethodInfo m12082_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2349_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2349_m12082_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12082_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12083_GM;
MethodInfo m12083_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2349_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12083_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12084_GM;
MethodInfo m12084_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2349_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12084_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12085_GM;
MethodInfo m12085_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2349_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12085_GM};
extern Il2CppType t373_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12086_GM;
MethodInfo m12086_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2349_TI, &t373_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12086_GM};
static MethodInfo* t2349_MIs[] =
{
	&m12082_MI,
	&m12083_MI,
	&m12084_MI,
	&m12085_MI,
	&m12086_MI,
	NULL
};
extern MethodInfo m12085_MI;
extern MethodInfo m12084_MI;
static MethodInfo* t2349_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m12083_MI,
	&m12085_MI,
	&m12084_MI,
	&m12086_MI,
};
static TypeInfo* t2349_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4092_TI,
};
static Il2CppInterfaceOffsetPair t2349_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4092_TI, 7},
};
extern TypeInfo t373_TI;
static Il2CppRGCTXData t2349_RGCTXData[3] = 
{
	&m12086_MI/* Method Usage */,
	&t373_TI/* Class Usage */,
	&m20433_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2349_0_0_0;
extern Il2CppType t2349_1_0_0;
extern Il2CppGenericClass t2349_GC;
TypeInfo t2349_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2349_MIs, t2349_PIs, t2349_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2349_TI, t2349_ITIs, t2349_VT, &EmptyCustomAttributesCache, &t2349_TI, &t2349_0_0_0, &t2349_1_0_0, t2349_IOs, &t2349_GC, NULL, NULL, NULL, t2349_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2349)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5242_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.ICloneable>
extern MethodInfo m27329_MI;
extern MethodInfo m27330_MI;
static PropertyInfo t5242____Item_PropertyInfo = 
{
	&t5242_TI, "Item", &m27329_MI, &m27330_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5242_PIs[] =
{
	&t5242____Item_PropertyInfo,
	NULL
};
extern Il2CppType t373_0_0_0;
static ParameterInfo t5242_m27331_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t373_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27331_GM;
MethodInfo m27331_MI = 
{
	"IndexOf", NULL, &t5242_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5242_m27331_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27331_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t373_0_0_0;
static ParameterInfo t5242_m27332_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t373_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27332_GM;
MethodInfo m27332_MI = 
{
	"Insert", NULL, &t5242_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5242_m27332_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27332_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5242_m27333_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27333_GM;
MethodInfo m27333_MI = 
{
	"RemoveAt", NULL, &t5242_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5242_m27333_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27333_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5242_m27329_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t373_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27329_GM;
MethodInfo m27329_MI = 
{
	"get_Item", NULL, &t5242_TI, &t373_0_0_0, RuntimeInvoker_t29_t44, t5242_m27329_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27329_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t373_0_0_0;
static ParameterInfo t5242_m27330_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t373_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27330_GM;
MethodInfo m27330_MI = 
{
	"set_Item", NULL, &t5242_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5242_m27330_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27330_GM};
static MethodInfo* t5242_MIs[] =
{
	&m27331_MI,
	&m27332_MI,
	&m27333_MI,
	&m27329_MI,
	&m27330_MI,
	NULL
};
static TypeInfo* t5242_ITIs[] = 
{
	&t603_TI,
	&t5241_TI,
	&t5243_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5242_0_0_0;
extern Il2CppType t5242_1_0_0;
struct t5242;
extern Il2CppGenericClass t5242_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5242_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5242_MIs, t5242_PIs, NULL, NULL, NULL, NULL, NULL, &t5242_TI, t5242_ITIs, NULL, &t1908__CustomAttributeCache, &t5242_TI, &t5242_0_0_0, &t5242_1_0_0, NULL, &t5242_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5244_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IComparable`1<System.String>>
extern MethodInfo m27334_MI;
static PropertyInfo t5244____Count_PropertyInfo = 
{
	&t5244_TI, "Count", &m27334_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27335_MI;
static PropertyInfo t5244____IsReadOnly_PropertyInfo = 
{
	&t5244_TI, "IsReadOnly", &m27335_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5244_PIs[] =
{
	&t5244____Count_PropertyInfo,
	&t5244____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27334_GM;
MethodInfo m27334_MI = 
{
	"get_Count", NULL, &t5244_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27334_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27335_GM;
MethodInfo m27335_MI = 
{
	"get_IsReadOnly", NULL, &t5244_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27335_GM};
extern Il2CppType t1745_0_0_0;
extern Il2CppType t1745_0_0_0;
static ParameterInfo t5244_m27336_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1745_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27336_GM;
MethodInfo m27336_MI = 
{
	"Add", NULL, &t5244_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5244_m27336_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27336_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27337_GM;
MethodInfo m27337_MI = 
{
	"Clear", NULL, &t5244_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27337_GM};
extern Il2CppType t1745_0_0_0;
static ParameterInfo t5244_m27338_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1745_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27338_GM;
MethodInfo m27338_MI = 
{
	"Contains", NULL, &t5244_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5244_m27338_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27338_GM};
extern Il2CppType t3539_0_0_0;
extern Il2CppType t3539_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5244_m27339_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3539_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27339_GM;
MethodInfo m27339_MI = 
{
	"CopyTo", NULL, &t5244_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5244_m27339_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27339_GM};
extern Il2CppType t1745_0_0_0;
static ParameterInfo t5244_m27340_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1745_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27340_GM;
MethodInfo m27340_MI = 
{
	"Remove", NULL, &t5244_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5244_m27340_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27340_GM};
static MethodInfo* t5244_MIs[] =
{
	&m27334_MI,
	&m27335_MI,
	&m27336_MI,
	&m27337_MI,
	&m27338_MI,
	&m27339_MI,
	&m27340_MI,
	NULL
};
extern TypeInfo t5246_TI;
static TypeInfo* t5244_ITIs[] = 
{
	&t603_TI,
	&t5246_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5244_0_0_0;
extern Il2CppType t5244_1_0_0;
struct t5244;
extern Il2CppGenericClass t5244_GC;
TypeInfo t5244_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5244_MIs, t5244_PIs, NULL, NULL, NULL, NULL, NULL, &t5244_TI, t5244_ITIs, NULL, &EmptyCustomAttributesCache, &t5244_TI, &t5244_0_0_0, &t5244_1_0_0, NULL, &t5244_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.String>>
extern Il2CppType t4094_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27341_GM;
MethodInfo m27341_MI = 
{
	"GetEnumerator", NULL, &t5246_TI, &t4094_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27341_GM};
static MethodInfo* t5246_MIs[] =
{
	&m27341_MI,
	NULL
};
static TypeInfo* t5246_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5246_0_0_0;
extern Il2CppType t5246_1_0_0;
struct t5246;
extern Il2CppGenericClass t5246_GC;
TypeInfo t5246_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5246_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5246_TI, t5246_ITIs, NULL, &EmptyCustomAttributesCache, &t5246_TI, &t5246_0_0_0, &t5246_1_0_0, NULL, &t5246_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4094_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.String>>
extern MethodInfo m27342_MI;
static PropertyInfo t4094____Current_PropertyInfo = 
{
	&t4094_TI, "Current", &m27342_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4094_PIs[] =
{
	&t4094____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1745_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27342_GM;
MethodInfo m27342_MI = 
{
	"get_Current", NULL, &t4094_TI, &t1745_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27342_GM};
static MethodInfo* t4094_MIs[] =
{
	&m27342_MI,
	NULL
};
static TypeInfo* t4094_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4094_0_0_0;
extern Il2CppType t4094_1_0_0;
struct t4094;
extern Il2CppGenericClass t4094_GC;
TypeInfo t4094_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4094_MIs, t4094_PIs, NULL, NULL, NULL, NULL, NULL, &t4094_TI, t4094_ITIs, NULL, &EmptyCustomAttributesCache, &t4094_TI, &t4094_0_0_0, &t4094_1_0_0, NULL, &t4094_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1745_TI;



// Metadata Definition System.IComparable`1<System.String>
extern Il2CppType t7_0_0_0;
static ParameterInfo t1745_m24146_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m24146_GM;
MethodInfo m24146_MI = 
{
	"CompareTo", NULL, &t1745_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t1745_m24146_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m24146_GM};
static MethodInfo* t1745_MIs[] =
{
	&m24146_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1745_1_0_0;
struct t1745;
extern Il2CppGenericClass t1745_GC;
TypeInfo t1745_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t1745_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1745_TI, NULL, NULL, &EmptyCustomAttributesCache, &t1745_TI, &t1745_0_0_0, &t1745_1_0_0, NULL, &t1745_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t2350.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2350_TI;
#include "t2350MD.h"

extern MethodInfo m12091_MI;
extern MethodInfo m20444_MI;
struct t20;
#define m20444(__this, p0, method) (t29*)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.IComparable`1<System.String>>
extern Il2CppType t20_0_0_1;
FieldInfo t2350_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2350_TI, offsetof(t2350, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2350_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2350_TI, offsetof(t2350, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2350_FIs[] =
{
	&t2350_f0_FieldInfo,
	&t2350_f1_FieldInfo,
	NULL
};
extern MethodInfo m12088_MI;
static PropertyInfo t2350____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2350_TI, "System.Collections.IEnumerator.Current", &m12088_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2350____Current_PropertyInfo = 
{
	&t2350_TI, "Current", &m12091_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2350_PIs[] =
{
	&t2350____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2350____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2350_m12087_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12087_GM;
MethodInfo m12087_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2350_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2350_m12087_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12087_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12088_GM;
MethodInfo m12088_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2350_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12088_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12089_GM;
MethodInfo m12089_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2350_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12089_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12090_GM;
MethodInfo m12090_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2350_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12090_GM};
extern Il2CppType t1745_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12091_GM;
MethodInfo m12091_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2350_TI, &t1745_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12091_GM};
static MethodInfo* t2350_MIs[] =
{
	&m12087_MI,
	&m12088_MI,
	&m12089_MI,
	&m12090_MI,
	&m12091_MI,
	NULL
};
extern MethodInfo m12090_MI;
extern MethodInfo m12089_MI;
static MethodInfo* t2350_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m12088_MI,
	&m12090_MI,
	&m12089_MI,
	&m12091_MI,
};
static TypeInfo* t2350_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4094_TI,
};
static Il2CppInterfaceOffsetPair t2350_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4094_TI, 7},
};
extern TypeInfo t1745_TI;
static Il2CppRGCTXData t2350_RGCTXData[3] = 
{
	&m12091_MI/* Method Usage */,
	&t1745_TI/* Class Usage */,
	&m20444_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2350_0_0_0;
extern Il2CppType t2350_1_0_0;
extern Il2CppGenericClass t2350_GC;
TypeInfo t2350_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2350_MIs, t2350_PIs, t2350_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2350_TI, t2350_ITIs, t2350_VT, &EmptyCustomAttributesCache, &t2350_TI, &t2350_0_0_0, &t2350_1_0_0, t2350_IOs, &t2350_GC, NULL, NULL, NULL, t2350_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2350)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5245_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IComparable`1<System.String>>
extern MethodInfo m27343_MI;
extern MethodInfo m27344_MI;
static PropertyInfo t5245____Item_PropertyInfo = 
{
	&t5245_TI, "Item", &m27343_MI, &m27344_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5245_PIs[] =
{
	&t5245____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1745_0_0_0;
static ParameterInfo t5245_m27345_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1745_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27345_GM;
MethodInfo m27345_MI = 
{
	"IndexOf", NULL, &t5245_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5245_m27345_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27345_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1745_0_0_0;
static ParameterInfo t5245_m27346_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1745_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27346_GM;
MethodInfo m27346_MI = 
{
	"Insert", NULL, &t5245_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5245_m27346_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27346_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5245_m27347_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27347_GM;
MethodInfo m27347_MI = 
{
	"RemoveAt", NULL, &t5245_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5245_m27347_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27347_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5245_m27343_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1745_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27343_GM;
MethodInfo m27343_MI = 
{
	"get_Item", NULL, &t5245_TI, &t1745_0_0_0, RuntimeInvoker_t29_t44, t5245_m27343_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27343_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1745_0_0_0;
static ParameterInfo t5245_m27344_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1745_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27344_GM;
MethodInfo m27344_MI = 
{
	"set_Item", NULL, &t5245_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5245_m27344_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27344_GM};
static MethodInfo* t5245_MIs[] =
{
	&m27345_MI,
	&m27346_MI,
	&m27347_MI,
	&m27343_MI,
	&m27344_MI,
	NULL
};
static TypeInfo* t5245_ITIs[] = 
{
	&t603_TI,
	&t5244_TI,
	&t5246_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5245_0_0_0;
extern Il2CppType t5245_1_0_0;
struct t5245;
extern Il2CppGenericClass t5245_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5245_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5245_MIs, t5245_PIs, NULL, NULL, NULL, NULL, NULL, &t5245_TI, t5245_ITIs, NULL, &t1908__CustomAttributeCache, &t5245_TI, &t5245_0_0_0, &t5245_1_0_0, NULL, &t5245_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5247_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IEquatable`1<System.String>>
extern MethodInfo m27348_MI;
static PropertyInfo t5247____Count_PropertyInfo = 
{
	&t5247_TI, "Count", &m27348_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27349_MI;
static PropertyInfo t5247____IsReadOnly_PropertyInfo = 
{
	&t5247_TI, "IsReadOnly", &m27349_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5247_PIs[] =
{
	&t5247____Count_PropertyInfo,
	&t5247____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27348_GM;
MethodInfo m27348_MI = 
{
	"get_Count", NULL, &t5247_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27348_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27349_GM;
MethodInfo m27349_MI = 
{
	"get_IsReadOnly", NULL, &t5247_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27349_GM};
extern Il2CppType t1746_0_0_0;
extern Il2CppType t1746_0_0_0;
static ParameterInfo t5247_m27350_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1746_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27350_GM;
MethodInfo m27350_MI = 
{
	"Add", NULL, &t5247_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5247_m27350_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27350_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27351_GM;
MethodInfo m27351_MI = 
{
	"Clear", NULL, &t5247_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27351_GM};
extern Il2CppType t1746_0_0_0;
static ParameterInfo t5247_m27352_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1746_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27352_GM;
MethodInfo m27352_MI = 
{
	"Contains", NULL, &t5247_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5247_m27352_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27352_GM};
extern Il2CppType t3540_0_0_0;
extern Il2CppType t3540_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5247_m27353_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3540_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27353_GM;
MethodInfo m27353_MI = 
{
	"CopyTo", NULL, &t5247_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5247_m27353_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27353_GM};
extern Il2CppType t1746_0_0_0;
static ParameterInfo t5247_m27354_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1746_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27354_GM;
MethodInfo m27354_MI = 
{
	"Remove", NULL, &t5247_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5247_m27354_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27354_GM};
static MethodInfo* t5247_MIs[] =
{
	&m27348_MI,
	&m27349_MI,
	&m27350_MI,
	&m27351_MI,
	&m27352_MI,
	&m27353_MI,
	&m27354_MI,
	NULL
};
extern TypeInfo t5249_TI;
static TypeInfo* t5247_ITIs[] = 
{
	&t603_TI,
	&t5249_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5247_0_0_0;
extern Il2CppType t5247_1_0_0;
struct t5247;
extern Il2CppGenericClass t5247_GC;
TypeInfo t5247_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5247_MIs, t5247_PIs, NULL, NULL, NULL, NULL, NULL, &t5247_TI, t5247_ITIs, NULL, &EmptyCustomAttributesCache, &t5247_TI, &t5247_0_0_0, &t5247_1_0_0, NULL, &t5247_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.String>>
extern Il2CppType t4096_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27355_GM;
MethodInfo m27355_MI = 
{
	"GetEnumerator", NULL, &t5249_TI, &t4096_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27355_GM};
static MethodInfo* t5249_MIs[] =
{
	&m27355_MI,
	NULL
};
static TypeInfo* t5249_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5249_0_0_0;
extern Il2CppType t5249_1_0_0;
struct t5249;
extern Il2CppGenericClass t5249_GC;
TypeInfo t5249_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5249_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5249_TI, t5249_ITIs, NULL, &EmptyCustomAttributesCache, &t5249_TI, &t5249_0_0_0, &t5249_1_0_0, NULL, &t5249_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4096_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IEquatable`1<System.String>>
extern MethodInfo m27356_MI;
static PropertyInfo t4096____Current_PropertyInfo = 
{
	&t4096_TI, "Current", &m27356_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4096_PIs[] =
{
	&t4096____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1746_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27356_GM;
MethodInfo m27356_MI = 
{
	"get_Current", NULL, &t4096_TI, &t1746_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27356_GM};
static MethodInfo* t4096_MIs[] =
{
	&m27356_MI,
	NULL
};
static TypeInfo* t4096_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4096_0_0_0;
extern Il2CppType t4096_1_0_0;
struct t4096;
extern Il2CppGenericClass t4096_GC;
TypeInfo t4096_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4096_MIs, t4096_PIs, NULL, NULL, NULL, NULL, NULL, &t4096_TI, t4096_ITIs, NULL, &EmptyCustomAttributesCache, &t4096_TI, &t4096_0_0_0, &t4096_1_0_0, NULL, &t4096_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1746_TI;



// Metadata Definition System.IEquatable`1<System.String>
extern Il2CppType t7_0_0_0;
static ParameterInfo t1746_m27357_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27357_GM;
MethodInfo m27357_MI = 
{
	"Equals", NULL, &t1746_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t1746_m27357_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27357_GM};
static MethodInfo* t1746_MIs[] =
{
	&m27357_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1746_1_0_0;
struct t1746;
extern Il2CppGenericClass t1746_GC;
TypeInfo t1746_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t1746_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1746_TI, NULL, NULL, &EmptyCustomAttributesCache, &t1746_TI, &t1746_0_0_0, &t1746_1_0_0, NULL, &t1746_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t2351.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2351_TI;
#include "t2351MD.h"

extern MethodInfo m12096_MI;
extern MethodInfo m20455_MI;
struct t20;
#define m20455(__this, p0, method) (t29*)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.IEquatable`1<System.String>>
extern Il2CppType t20_0_0_1;
FieldInfo t2351_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2351_TI, offsetof(t2351, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2351_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2351_TI, offsetof(t2351, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2351_FIs[] =
{
	&t2351_f0_FieldInfo,
	&t2351_f1_FieldInfo,
	NULL
};
extern MethodInfo m12093_MI;
static PropertyInfo t2351____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2351_TI, "System.Collections.IEnumerator.Current", &m12093_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2351____Current_PropertyInfo = 
{
	&t2351_TI, "Current", &m12096_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2351_PIs[] =
{
	&t2351____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2351____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2351_m12092_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12092_GM;
MethodInfo m12092_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2351_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2351_m12092_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12092_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12093_GM;
MethodInfo m12093_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2351_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12093_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12094_GM;
MethodInfo m12094_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2351_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12094_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12095_GM;
MethodInfo m12095_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2351_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12095_GM};
extern Il2CppType t1746_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12096_GM;
MethodInfo m12096_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2351_TI, &t1746_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12096_GM};
static MethodInfo* t2351_MIs[] =
{
	&m12092_MI,
	&m12093_MI,
	&m12094_MI,
	&m12095_MI,
	&m12096_MI,
	NULL
};
extern MethodInfo m12095_MI;
extern MethodInfo m12094_MI;
static MethodInfo* t2351_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m12093_MI,
	&m12095_MI,
	&m12094_MI,
	&m12096_MI,
};
static TypeInfo* t2351_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4096_TI,
};
static Il2CppInterfaceOffsetPair t2351_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4096_TI, 7},
};
extern TypeInfo t1746_TI;
static Il2CppRGCTXData t2351_RGCTXData[3] = 
{
	&m12096_MI/* Method Usage */,
	&t1746_TI/* Class Usage */,
	&m20455_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2351_0_0_0;
extern Il2CppType t2351_1_0_0;
extern Il2CppGenericClass t2351_GC;
TypeInfo t2351_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2351_MIs, t2351_PIs, t2351_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2351_TI, t2351_ITIs, t2351_VT, &EmptyCustomAttributesCache, &t2351_TI, &t2351_0_0_0, &t2351_1_0_0, t2351_IOs, &t2351_GC, NULL, NULL, NULL, t2351_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2351)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5248_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IEquatable`1<System.String>>
extern MethodInfo m27358_MI;
extern MethodInfo m27359_MI;
static PropertyInfo t5248____Item_PropertyInfo = 
{
	&t5248_TI, "Item", &m27358_MI, &m27359_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5248_PIs[] =
{
	&t5248____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1746_0_0_0;
static ParameterInfo t5248_m27360_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1746_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27360_GM;
MethodInfo m27360_MI = 
{
	"IndexOf", NULL, &t5248_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5248_m27360_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27360_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1746_0_0_0;
static ParameterInfo t5248_m27361_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1746_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27361_GM;
MethodInfo m27361_MI = 
{
	"Insert", NULL, &t5248_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5248_m27361_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27361_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5248_m27362_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27362_GM;
MethodInfo m27362_MI = 
{
	"RemoveAt", NULL, &t5248_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5248_m27362_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27362_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5248_m27358_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1746_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27358_GM;
MethodInfo m27358_MI = 
{
	"get_Item", NULL, &t5248_TI, &t1746_0_0_0, RuntimeInvoker_t29_t44, t5248_m27358_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27358_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1746_0_0_0;
static ParameterInfo t5248_m27359_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1746_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27359_GM;
MethodInfo m27359_MI = 
{
	"set_Item", NULL, &t5248_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5248_m27359_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27359_GM};
static MethodInfo* t5248_MIs[] =
{
	&m27360_MI,
	&m27361_MI,
	&m27362_MI,
	&m27358_MI,
	&m27359_MI,
	NULL
};
static TypeInfo* t5248_ITIs[] = 
{
	&t603_TI,
	&t5247_TI,
	&t5249_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5248_0_0_0;
extern Il2CppType t5248_1_0_0;
struct t5248;
extern Il2CppGenericClass t5248_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5248_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5248_MIs, t5248_PIs, NULL, NULL, NULL, NULL, NULL, &t5248_TI, t5248_ITIs, NULL, &t1908__CustomAttributeCache, &t5248_TI, &t5248_0_0_0, &t5248_1_0_0, NULL, &t5248_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2352.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2352_TI;
#include "t2352MD.h"

extern MethodInfo m12101_MI;
extern MethodInfo m20466_MI;
struct t20;
 t322  m20466 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m12097_MI;
 void m12097 (t2352 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m12098_MI;
 t29 * m12098 (t2352 * __this, MethodInfo* method){
	{
		t322  L_0 = m12101(__this, &m12101_MI);
		t322  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t322_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m12099_MI;
 void m12099 (t2352 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m12100_MI;
 bool m12100 (t2352 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 t322  m12101 (t2352 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		t322  L_8 = m20466(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m20466_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>>
extern Il2CppType t20_0_0_1;
FieldInfo t2352_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2352_TI, offsetof(t2352, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2352_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2352_TI, offsetof(t2352, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2352_FIs[] =
{
	&t2352_f0_FieldInfo,
	&t2352_f1_FieldInfo,
	NULL
};
static PropertyInfo t2352____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2352_TI, "System.Collections.IEnumerator.Current", &m12098_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2352____Current_PropertyInfo = 
{
	&t2352_TI, "Current", &m12101_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2352_PIs[] =
{
	&t2352____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2352____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2352_m12097_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12097_GM;
MethodInfo m12097_MI = 
{
	".ctor", (methodPointerType)&m12097, &t2352_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2352_m12097_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12097_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12098_GM;
MethodInfo m12098_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m12098, &t2352_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12098_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12099_GM;
MethodInfo m12099_MI = 
{
	"Dispose", (methodPointerType)&m12099, &t2352_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12099_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12100_GM;
MethodInfo m12100_MI = 
{
	"MoveNext", (methodPointerType)&m12100, &t2352_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12100_GM};
extern Il2CppType t322_0_0_0;
extern void* RuntimeInvoker_t322 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12101_GM;
MethodInfo m12101_MI = 
{
	"get_Current", (methodPointerType)&m12101, &t2352_TI, &t322_0_0_0, RuntimeInvoker_t322, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12101_GM};
static MethodInfo* t2352_MIs[] =
{
	&m12097_MI,
	&m12098_MI,
	&m12099_MI,
	&m12100_MI,
	&m12101_MI,
	NULL
};
static MethodInfo* t2352_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m12098_MI,
	&m12100_MI,
	&m12099_MI,
	&m12101_MI,
};
static TypeInfo* t2352_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2347_TI,
};
static Il2CppInterfaceOffsetPair t2352_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2347_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2352_0_0_0;
extern Il2CppType t2352_1_0_0;
extern Il2CppGenericClass t2352_GC;
TypeInfo t2352_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2352_MIs, t2352_PIs, t2352_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2352_TI, t2352_ITIs, t2352_VT, &EmptyCustomAttributesCache, &t2352_TI, &t2352_0_0_0, &t2352_1_0_0, t2352_IOs, &t2352_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2352)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5251_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>>
extern MethodInfo m27363_MI;
extern MethodInfo m27364_MI;
static PropertyInfo t5251____Item_PropertyInfo = 
{
	&t5251_TI, "Item", &m27363_MI, &m27364_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5251_PIs[] =
{
	&t5251____Item_PropertyInfo,
	NULL
};
extern Il2CppType t322_0_0_0;
static ParameterInfo t5251_m27365_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t322_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t322 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27365_GM;
MethodInfo m27365_MI = 
{
	"IndexOf", NULL, &t5251_TI, &t44_0_0_0, RuntimeInvoker_t44_t322, t5251_m27365_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27365_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t322_0_0_0;
static ParameterInfo t5251_m27366_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t322_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t322 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27366_GM;
MethodInfo m27366_MI = 
{
	"Insert", NULL, &t5251_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t322, t5251_m27366_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27366_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5251_m27367_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27367_GM;
MethodInfo m27367_MI = 
{
	"RemoveAt", NULL, &t5251_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5251_m27367_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27367_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5251_m27363_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t322_0_0_0;
extern void* RuntimeInvoker_t322_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27363_GM;
MethodInfo m27363_MI = 
{
	"get_Item", NULL, &t5251_TI, &t322_0_0_0, RuntimeInvoker_t322_t44, t5251_m27363_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27363_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t322_0_0_0;
static ParameterInfo t5251_m27364_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t322_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t322 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27364_GM;
MethodInfo m27364_MI = 
{
	"set_Item", NULL, &t5251_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t322, t5251_m27364_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27364_GM};
static MethodInfo* t5251_MIs[] =
{
	&m27365_MI,
	&m27366_MI,
	&m27367_MI,
	&m27363_MI,
	&m27364_MI,
	NULL
};
static TypeInfo* t5251_ITIs[] = 
{
	&t603_TI,
	&t5250_TI,
	&t5252_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5251_0_0_0;
extern Il2CppType t5251_1_0_0;
struct t5251;
extern Il2CppGenericClass t5251_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5251_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5251_MIs, t5251_PIs, NULL, NULL, NULL, NULL, NULL, &t5251_TI, t5251_ITIs, NULL, &t1908__CustomAttributeCache, &t5251_TI, &t5251_0_0_0, &t5251_1_0_0, NULL, &t5251_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IDictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData>
extern Il2CppType t44_0_0_0;
static ParameterInfo t6648_m27368_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27368_GM;
MethodInfo m27368_MI = 
{
	"Remove", NULL, &t6648_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6648_m27368_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27368_GM};
static MethodInfo* t6648_MIs[] =
{
	&m27368_MI,
	NULL
};
static TypeInfo* t6648_ITIs[] = 
{
	&t603_TI,
	&t5250_TI,
	&t5252_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6648_0_0_0;
extern Il2CppType t6648_1_0_0;
struct t6648;
extern Il2CppGenericClass t6648_GC;
extern CustomAttributesCache t1975__CustomAttributeCache;
TypeInfo t6648_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IDictionary`2", "System.Collections.Generic", t6648_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6648_TI, t6648_ITIs, NULL, &t1975__CustomAttributeCache, &t6648_TI, &t6648_0_0_0, &t6648_1_0_0, NULL, &t6648_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<System.Int32>
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2344_m27290_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27290_GM;
MethodInfo m27290_MI = 
{
	"Equals", NULL, &t2344_TI, &t40_0_0_0, RuntimeInvoker_t40_t44_t44, t2344_m27290_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27290_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2344_m27289_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27289_GM;
MethodInfo m27289_MI = 
{
	"GetHashCode", NULL, &t2344_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t2344_m27289_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27289_GM};
static MethodInfo* t2344_MIs[] =
{
	&m27290_MI,
	&m27289_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2344_1_0_0;
struct t2344;
extern Il2CppGenericClass t2344_GC;
TypeInfo t2344_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t2344_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2344_TI, NULL, NULL, &EmptyCustomAttributesCache, &t2344_TI, &t2344_0_0_0, &t2344_1_0_0, NULL, &t2344_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4099_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Collections.Generic.Link>
extern MethodInfo m27369_MI;
static PropertyInfo t4099____Current_PropertyInfo = 
{
	&t4099_TI, "Current", &m27369_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4099_PIs[] =
{
	&t4099____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1248_0_0_0;
extern void* RuntimeInvoker_t1248 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27369_GM;
MethodInfo m27369_MI = 
{
	"get_Current", NULL, &t4099_TI, &t1248_0_0_0, RuntimeInvoker_t1248, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27369_GM};
static MethodInfo* t4099_MIs[] =
{
	&m27369_MI,
	NULL
};
static TypeInfo* t4099_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4099_0_0_0;
extern Il2CppType t4099_1_0_0;
struct t4099;
extern Il2CppGenericClass t4099_GC;
TypeInfo t4099_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4099_MIs, t4099_PIs, NULL, NULL, NULL, NULL, NULL, &t4099_TI, t4099_ITIs, NULL, &EmptyCustomAttributesCache, &t4099_TI, &t4099_0_0_0, &t4099_1_0_0, NULL, &t4099_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2353.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2353_TI;
#include "t2353MD.h"

extern MethodInfo m12106_MI;
extern MethodInfo m20477_MI;
struct t20;
 t1248  m20477 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m12102_MI;
 void m12102 (t2353 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m12103_MI;
 t29 * m12103 (t2353 * __this, MethodInfo* method){
	{
		t1248  L_0 = m12106(__this, &m12106_MI);
		t1248  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1248_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m12104_MI;
 void m12104 (t2353 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m12105_MI;
 bool m12105 (t2353 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 t1248  m12106 (t2353 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		t1248  L_8 = m20477(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m20477_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Collections.Generic.Link>
extern Il2CppType t20_0_0_1;
FieldInfo t2353_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2353_TI, offsetof(t2353, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2353_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2353_TI, offsetof(t2353, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2353_FIs[] =
{
	&t2353_f0_FieldInfo,
	&t2353_f1_FieldInfo,
	NULL
};
static PropertyInfo t2353____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2353_TI, "System.Collections.IEnumerator.Current", &m12103_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2353____Current_PropertyInfo = 
{
	&t2353_TI, "Current", &m12106_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2353_PIs[] =
{
	&t2353____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2353____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2353_m12102_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12102_GM;
MethodInfo m12102_MI = 
{
	".ctor", (methodPointerType)&m12102, &t2353_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2353_m12102_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12102_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12103_GM;
MethodInfo m12103_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m12103, &t2353_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12103_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12104_GM;
MethodInfo m12104_MI = 
{
	"Dispose", (methodPointerType)&m12104, &t2353_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12104_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12105_GM;
MethodInfo m12105_MI = 
{
	"MoveNext", (methodPointerType)&m12105, &t2353_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12105_GM};
extern Il2CppType t1248_0_0_0;
extern void* RuntimeInvoker_t1248 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12106_GM;
MethodInfo m12106_MI = 
{
	"get_Current", (methodPointerType)&m12106, &t2353_TI, &t1248_0_0_0, RuntimeInvoker_t1248, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12106_GM};
static MethodInfo* t2353_MIs[] =
{
	&m12102_MI,
	&m12103_MI,
	&m12104_MI,
	&m12105_MI,
	&m12106_MI,
	NULL
};
static MethodInfo* t2353_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m12103_MI,
	&m12105_MI,
	&m12104_MI,
	&m12106_MI,
};
static TypeInfo* t2353_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4099_TI,
};
static Il2CppInterfaceOffsetPair t2353_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4099_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2353_0_0_0;
extern Il2CppType t2353_1_0_0;
extern Il2CppGenericClass t2353_GC;
TypeInfo t2353_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2353_MIs, t2353_PIs, t2353_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2353_TI, t2353_ITIs, t2353_VT, &EmptyCustomAttributesCache, &t2353_TI, &t2353_0_0_0, &t2353_1_0_0, t2353_IOs, &t2353_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2353)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5253_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Collections.Generic.Link>
extern MethodInfo m27370_MI;
static PropertyInfo t5253____Count_PropertyInfo = 
{
	&t5253_TI, "Count", &m27370_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27371_MI;
static PropertyInfo t5253____IsReadOnly_PropertyInfo = 
{
	&t5253_TI, "IsReadOnly", &m27371_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5253_PIs[] =
{
	&t5253____Count_PropertyInfo,
	&t5253____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27370_GM;
MethodInfo m27370_MI = 
{
	"get_Count", NULL, &t5253_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27370_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27371_GM;
MethodInfo m27371_MI = 
{
	"get_IsReadOnly", NULL, &t5253_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27371_GM};
extern Il2CppType t1248_0_0_0;
extern Il2CppType t1248_0_0_0;
static ParameterInfo t5253_m27372_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1248_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t1248 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27372_GM;
MethodInfo m27372_MI = 
{
	"Add", NULL, &t5253_TI, &t21_0_0_0, RuntimeInvoker_t21_t1248, t5253_m27372_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27372_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27373_GM;
MethodInfo m27373_MI = 
{
	"Clear", NULL, &t5253_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27373_GM};
extern Il2CppType t1248_0_0_0;
static ParameterInfo t5253_m27374_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1248_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t1248 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27374_GM;
MethodInfo m27374_MI = 
{
	"Contains", NULL, &t5253_TI, &t40_0_0_0, RuntimeInvoker_t40_t1248, t5253_m27374_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27374_GM};
extern Il2CppType t1965_0_0_0;
extern Il2CppType t1965_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5253_m27375_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1965_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27375_GM;
MethodInfo m27375_MI = 
{
	"CopyTo", NULL, &t5253_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5253_m27375_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27375_GM};
extern Il2CppType t1248_0_0_0;
static ParameterInfo t5253_m27376_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1248_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t1248 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27376_GM;
MethodInfo m27376_MI = 
{
	"Remove", NULL, &t5253_TI, &t40_0_0_0, RuntimeInvoker_t40_t1248, t5253_m27376_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27376_GM};
static MethodInfo* t5253_MIs[] =
{
	&m27370_MI,
	&m27371_MI,
	&m27372_MI,
	&m27373_MI,
	&m27374_MI,
	&m27375_MI,
	&m27376_MI,
	NULL
};
extern TypeInfo t5255_TI;
static TypeInfo* t5253_ITIs[] = 
{
	&t603_TI,
	&t5255_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5253_0_0_0;
extern Il2CppType t5253_1_0_0;
struct t5253;
extern Il2CppGenericClass t5253_GC;
TypeInfo t5253_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5253_MIs, t5253_PIs, NULL, NULL, NULL, NULL, NULL, &t5253_TI, t5253_ITIs, NULL, &EmptyCustomAttributesCache, &t5253_TI, &t5253_0_0_0, &t5253_1_0_0, NULL, &t5253_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Collections.Generic.Link>
extern Il2CppType t4099_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27377_GM;
MethodInfo m27377_MI = 
{
	"GetEnumerator", NULL, &t5255_TI, &t4099_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27377_GM};
static MethodInfo* t5255_MIs[] =
{
	&m27377_MI,
	NULL
};
static TypeInfo* t5255_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5255_0_0_0;
extern Il2CppType t5255_1_0_0;
struct t5255;
extern Il2CppGenericClass t5255_GC;
TypeInfo t5255_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5255_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5255_TI, t5255_ITIs, NULL, &EmptyCustomAttributesCache, &t5255_TI, &t5255_0_0_0, &t5255_1_0_0, NULL, &t5255_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5254_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Collections.Generic.Link>
extern MethodInfo m27378_MI;
extern MethodInfo m27379_MI;
static PropertyInfo t5254____Item_PropertyInfo = 
{
	&t5254_TI, "Item", &m27378_MI, &m27379_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5254_PIs[] =
{
	&t5254____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1248_0_0_0;
static ParameterInfo t5254_m27380_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1248_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t1248 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27380_GM;
MethodInfo m27380_MI = 
{
	"IndexOf", NULL, &t5254_TI, &t44_0_0_0, RuntimeInvoker_t44_t1248, t5254_m27380_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27380_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1248_0_0_0;
static ParameterInfo t5254_m27381_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1248_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t1248 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27381_GM;
MethodInfo m27381_MI = 
{
	"Insert", NULL, &t5254_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t1248, t5254_m27381_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27381_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5254_m27382_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27382_GM;
MethodInfo m27382_MI = 
{
	"RemoveAt", NULL, &t5254_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5254_m27382_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27382_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5254_m27378_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1248_0_0_0;
extern void* RuntimeInvoker_t1248_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27378_GM;
MethodInfo m27378_MI = 
{
	"get_Item", NULL, &t5254_TI, &t1248_0_0_0, RuntimeInvoker_t1248_t44, t5254_m27378_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27378_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1248_0_0_0;
static ParameterInfo t5254_m27379_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1248_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t1248 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27379_GM;
MethodInfo m27379_MI = 
{
	"set_Item", NULL, &t5254_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t1248, t5254_m27379_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27379_GM};
static MethodInfo* t5254_MIs[] =
{
	&m27380_MI,
	&m27381_MI,
	&m27382_MI,
	&m27378_MI,
	&m27379_MI,
	NULL
};
static TypeInfo* t5254_ITIs[] = 
{
	&t603_TI,
	&t5253_TI,
	&t5255_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5254_0_0_0;
extern Il2CppType t5254_1_0_0;
struct t5254;
extern Il2CppGenericClass t5254_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5254_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5254_MIs, t5254_PIs, NULL, NULL, NULL, NULL, NULL, &t5254_TI, t5254_ITIs, NULL, &t1908__CustomAttributeCache, &t5254_TI, &t5254_0_0_0, &t5254_1_0_0, NULL, &t5254_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2356_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.PointerEventData>
extern MethodInfo m27383_MI;
static PropertyInfo t2356____Current_PropertyInfo = 
{
	&t2356_TI, "Current", &m27383_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2356_PIs[] =
{
	&t2356____Current_PropertyInfo,
	NULL
};
extern Il2CppType t6_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27383_GM;
MethodInfo m27383_MI = 
{
	"get_Current", NULL, &t2356_TI, &t6_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27383_GM};
static MethodInfo* t2356_MIs[] =
{
	&m27383_MI,
	NULL
};
static TypeInfo* t2356_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2356_0_0_0;
extern Il2CppType t2356_1_0_0;
struct t2356;
extern Il2CppGenericClass t2356_GC;
TypeInfo t2356_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2356_MIs, t2356_PIs, NULL, NULL, NULL, NULL, NULL, &t2356_TI, t2356_ITIs, NULL, &EmptyCustomAttributesCache, &t2356_TI, &t2356_0_0_0, &t2356_1_0_0, NULL, &t2356_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2354.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2354_TI;
#include "t2354MD.h"

extern MethodInfo m12111_MI;
extern MethodInfo m20488_MI;
struct t20;
#define m20488(__this, p0, method) (t6 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.PointerEventData>
extern Il2CppType t20_0_0_1;
FieldInfo t2354_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2354_TI, offsetof(t2354, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2354_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2354_TI, offsetof(t2354, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2354_FIs[] =
{
	&t2354_f0_FieldInfo,
	&t2354_f1_FieldInfo,
	NULL
};
extern MethodInfo m12108_MI;
static PropertyInfo t2354____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2354_TI, "System.Collections.IEnumerator.Current", &m12108_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2354____Current_PropertyInfo = 
{
	&t2354_TI, "Current", &m12111_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2354_PIs[] =
{
	&t2354____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2354____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2354_m12107_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12107_GM;
MethodInfo m12107_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2354_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2354_m12107_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12107_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12108_GM;
MethodInfo m12108_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2354_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12108_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12109_GM;
MethodInfo m12109_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2354_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12109_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12110_GM;
MethodInfo m12110_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2354_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12110_GM};
extern Il2CppType t6_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12111_GM;
MethodInfo m12111_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2354_TI, &t6_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12111_GM};
static MethodInfo* t2354_MIs[] =
{
	&m12107_MI,
	&m12108_MI,
	&m12109_MI,
	&m12110_MI,
	&m12111_MI,
	NULL
};
extern MethodInfo m12110_MI;
extern MethodInfo m12109_MI;
static MethodInfo* t2354_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m12108_MI,
	&m12110_MI,
	&m12109_MI,
	&m12111_MI,
};
static TypeInfo* t2354_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2356_TI,
};
static Il2CppInterfaceOffsetPair t2354_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2356_TI, 7},
};
extern TypeInfo t6_TI;
static Il2CppRGCTXData t2354_RGCTXData[3] = 
{
	&m12111_MI/* Method Usage */,
	&t6_TI/* Class Usage */,
	&m20488_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2354_0_0_0;
extern Il2CppType t2354_1_0_0;
extern Il2CppGenericClass t2354_GC;
TypeInfo t2354_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2354_MIs, t2354_PIs, t2354_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2354_TI, t2354_ITIs, t2354_VT, &EmptyCustomAttributesCache, &t2354_TI, &t2354_0_0_0, &t2354_1_0_0, t2354_IOs, &t2354_GC, NULL, NULL, NULL, t2354_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2354)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5256_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.PointerEventData>
extern MethodInfo m27384_MI;
static PropertyInfo t5256____Count_PropertyInfo = 
{
	&t5256_TI, "Count", &m27384_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27385_MI;
static PropertyInfo t5256____IsReadOnly_PropertyInfo = 
{
	&t5256_TI, "IsReadOnly", &m27385_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5256_PIs[] =
{
	&t5256____Count_PropertyInfo,
	&t5256____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27384_GM;
MethodInfo m27384_MI = 
{
	"get_Count", NULL, &t5256_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27384_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27385_GM;
MethodInfo m27385_MI = 
{
	"get_IsReadOnly", NULL, &t5256_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27385_GM};
extern Il2CppType t6_0_0_0;
static ParameterInfo t5256_m27386_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27386_GM;
MethodInfo m27386_MI = 
{
	"Add", NULL, &t5256_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5256_m27386_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27386_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27387_GM;
MethodInfo m27387_MI = 
{
	"Clear", NULL, &t5256_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27387_GM};
extern Il2CppType t6_0_0_0;
static ParameterInfo t5256_m27388_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27388_GM;
MethodInfo m27388_MI = 
{
	"Contains", NULL, &t5256_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5256_m27388_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27388_GM};
extern Il2CppType t2343_0_0_0;
extern Il2CppType t2343_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5256_m27389_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2343_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27389_GM;
MethodInfo m27389_MI = 
{
	"CopyTo", NULL, &t5256_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5256_m27389_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27389_GM};
extern Il2CppType t6_0_0_0;
static ParameterInfo t5256_m27390_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27390_GM;
MethodInfo m27390_MI = 
{
	"Remove", NULL, &t5256_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5256_m27390_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27390_GM};
static MethodInfo* t5256_MIs[] =
{
	&m27384_MI,
	&m27385_MI,
	&m27386_MI,
	&m27387_MI,
	&m27388_MI,
	&m27389_MI,
	&m27390_MI,
	NULL
};
extern TypeInfo t5258_TI;
static TypeInfo* t5256_ITIs[] = 
{
	&t603_TI,
	&t5258_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5256_0_0_0;
extern Il2CppType t5256_1_0_0;
struct t5256;
extern Il2CppGenericClass t5256_GC;
TypeInfo t5256_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5256_MIs, t5256_PIs, NULL, NULL, NULL, NULL, NULL, &t5256_TI, t5256_ITIs, NULL, &EmptyCustomAttributesCache, &t5256_TI, &t5256_0_0_0, &t5256_1_0_0, NULL, &t5256_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.PointerEventData>
extern Il2CppType t2356_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27391_GM;
MethodInfo m27391_MI = 
{
	"GetEnumerator", NULL, &t5258_TI, &t2356_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27391_GM};
static MethodInfo* t5258_MIs[] =
{
	&m27391_MI,
	NULL
};
static TypeInfo* t5258_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5258_0_0_0;
extern Il2CppType t5258_1_0_0;
struct t5258;
extern Il2CppGenericClass t5258_GC;
TypeInfo t5258_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5258_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5258_TI, t5258_ITIs, NULL, &EmptyCustomAttributesCache, &t5258_TI, &t5258_0_0_0, &t5258_1_0_0, NULL, &t5258_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5257_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.PointerEventData>
extern MethodInfo m27392_MI;
extern MethodInfo m27393_MI;
static PropertyInfo t5257____Item_PropertyInfo = 
{
	&t5257_TI, "Item", &m27392_MI, &m27393_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5257_PIs[] =
{
	&t5257____Item_PropertyInfo,
	NULL
};
extern Il2CppType t6_0_0_0;
static ParameterInfo t5257_m27394_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27394_GM;
MethodInfo m27394_MI = 
{
	"IndexOf", NULL, &t5257_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5257_m27394_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27394_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t6_0_0_0;
static ParameterInfo t5257_m27395_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27395_GM;
MethodInfo m27395_MI = 
{
	"Insert", NULL, &t5257_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5257_m27395_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27395_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5257_m27396_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27396_GM;
MethodInfo m27396_MI = 
{
	"RemoveAt", NULL, &t5257_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5257_m27396_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27396_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5257_m27392_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t6_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27392_GM;
MethodInfo m27392_MI = 
{
	"get_Item", NULL, &t5257_TI, &t6_0_0_0, RuntimeInvoker_t29_t44, t5257_m27392_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27392_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t6_0_0_0;
static ParameterInfo t5257_m27393_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27393_GM;
MethodInfo m27393_MI = 
{
	"set_Item", NULL, &t5257_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5257_m27393_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27393_GM};
static MethodInfo* t5257_MIs[] =
{
	&m27394_MI,
	&m27395_MI,
	&m27396_MI,
	&m27392_MI,
	&m27393_MI,
	NULL
};
static TypeInfo* t5257_ITIs[] = 
{
	&t603_TI,
	&t5256_TI,
	&t5258_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5257_0_0_0;
extern Il2CppType t5257_1_0_0;
struct t5257;
extern Il2CppGenericClass t5257_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5257_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5257_MIs, t5257_PIs, NULL, NULL, NULL, NULL, NULL, &t5257_TI, t5257_ITIs, NULL, &t1908__CustomAttributeCache, &t5257_TI, &t5257_0_0_0, &t5257_1_0_0, NULL, &t5257_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5259_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.BaseEventData>
extern MethodInfo m27397_MI;
static PropertyInfo t5259____Count_PropertyInfo = 
{
	&t5259_TI, "Count", &m27397_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27398_MI;
static PropertyInfo t5259____IsReadOnly_PropertyInfo = 
{
	&t5259_TI, "IsReadOnly", &m27398_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5259_PIs[] =
{
	&t5259____Count_PropertyInfo,
	&t5259____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27397_GM;
MethodInfo m27397_MI = 
{
	"get_Count", NULL, &t5259_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27397_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27398_GM;
MethodInfo m27398_MI = 
{
	"get_IsReadOnly", NULL, &t5259_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27398_GM};
extern Il2CppType t53_0_0_0;
static ParameterInfo t5259_m27399_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27399_GM;
MethodInfo m27399_MI = 
{
	"Add", NULL, &t5259_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5259_m27399_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27399_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27400_GM;
MethodInfo m27400_MI = 
{
	"Clear", NULL, &t5259_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27400_GM};
extern Il2CppType t53_0_0_0;
static ParameterInfo t5259_m27401_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27401_GM;
MethodInfo m27401_MI = 
{
	"Contains", NULL, &t5259_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5259_m27401_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27401_GM};
extern Il2CppType t3824_0_0_0;
extern Il2CppType t3824_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5259_m27402_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3824_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27402_GM;
MethodInfo m27402_MI = 
{
	"CopyTo", NULL, &t5259_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5259_m27402_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27402_GM};
extern Il2CppType t53_0_0_0;
static ParameterInfo t5259_m27403_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27403_GM;
MethodInfo m27403_MI = 
{
	"Remove", NULL, &t5259_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5259_m27403_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27403_GM};
static MethodInfo* t5259_MIs[] =
{
	&m27397_MI,
	&m27398_MI,
	&m27399_MI,
	&m27400_MI,
	&m27401_MI,
	&m27402_MI,
	&m27403_MI,
	NULL
};
extern TypeInfo t5261_TI;
static TypeInfo* t5259_ITIs[] = 
{
	&t603_TI,
	&t5261_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5259_0_0_0;
extern Il2CppType t5259_1_0_0;
struct t5259;
extern Il2CppGenericClass t5259_GC;
TypeInfo t5259_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5259_MIs, t5259_PIs, NULL, NULL, NULL, NULL, NULL, &t5259_TI, t5259_ITIs, NULL, &EmptyCustomAttributesCache, &t5259_TI, &t5259_0_0_0, &t5259_1_0_0, NULL, &t5259_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.BaseEventData>
extern Il2CppType t4101_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27404_GM;
MethodInfo m27404_MI = 
{
	"GetEnumerator", NULL, &t5261_TI, &t4101_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27404_GM};
static MethodInfo* t5261_MIs[] =
{
	&m27404_MI,
	NULL
};
static TypeInfo* t5261_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5261_0_0_0;
extern Il2CppType t5261_1_0_0;
struct t5261;
extern Il2CppGenericClass t5261_GC;
TypeInfo t5261_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5261_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5261_TI, t5261_ITIs, NULL, &EmptyCustomAttributesCache, &t5261_TI, &t5261_0_0_0, &t5261_1_0_0, NULL, &t5261_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4101_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.BaseEventData>
extern MethodInfo m27405_MI;
static PropertyInfo t4101____Current_PropertyInfo = 
{
	&t4101_TI, "Current", &m27405_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4101_PIs[] =
{
	&t4101____Current_PropertyInfo,
	NULL
};
extern Il2CppType t53_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27405_GM;
MethodInfo m27405_MI = 
{
	"get_Current", NULL, &t4101_TI, &t53_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27405_GM};
static MethodInfo* t4101_MIs[] =
{
	&m27405_MI,
	NULL
};
static TypeInfo* t4101_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4101_0_0_0;
extern Il2CppType t4101_1_0_0;
struct t4101;
extern Il2CppGenericClass t4101_GC;
TypeInfo t4101_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4101_MIs, t4101_PIs, NULL, NULL, NULL, NULL, NULL, &t4101_TI, t4101_ITIs, NULL, &EmptyCustomAttributesCache, &t4101_TI, &t4101_0_0_0, &t4101_1_0_0, NULL, &t4101_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2355.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2355_TI;
#include "t2355MD.h"

extern TypeInfo t53_TI;
extern MethodInfo m12116_MI;
extern MethodInfo m20499_MI;
struct t20;
#define m20499(__this, p0, method) (t53 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.BaseEventData>
extern Il2CppType t20_0_0_1;
FieldInfo t2355_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2355_TI, offsetof(t2355, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2355_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2355_TI, offsetof(t2355, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2355_FIs[] =
{
	&t2355_f0_FieldInfo,
	&t2355_f1_FieldInfo,
	NULL
};
extern MethodInfo m12113_MI;
static PropertyInfo t2355____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2355_TI, "System.Collections.IEnumerator.Current", &m12113_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2355____Current_PropertyInfo = 
{
	&t2355_TI, "Current", &m12116_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2355_PIs[] =
{
	&t2355____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2355____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2355_m12112_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12112_GM;
MethodInfo m12112_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2355_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2355_m12112_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12112_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12113_GM;
MethodInfo m12113_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2355_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12113_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12114_GM;
MethodInfo m12114_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2355_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12114_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12115_GM;
MethodInfo m12115_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2355_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12115_GM};
extern Il2CppType t53_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12116_GM;
MethodInfo m12116_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2355_TI, &t53_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12116_GM};
static MethodInfo* t2355_MIs[] =
{
	&m12112_MI,
	&m12113_MI,
	&m12114_MI,
	&m12115_MI,
	&m12116_MI,
	NULL
};
extern MethodInfo m12115_MI;
extern MethodInfo m12114_MI;
static MethodInfo* t2355_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m12113_MI,
	&m12115_MI,
	&m12114_MI,
	&m12116_MI,
};
static TypeInfo* t2355_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4101_TI,
};
static Il2CppInterfaceOffsetPair t2355_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4101_TI, 7},
};
extern TypeInfo t53_TI;
static Il2CppRGCTXData t2355_RGCTXData[3] = 
{
	&m12116_MI/* Method Usage */,
	&t53_TI/* Class Usage */,
	&m20499_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2355_0_0_0;
extern Il2CppType t2355_1_0_0;
extern Il2CppGenericClass t2355_GC;
TypeInfo t2355_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2355_MIs, t2355_PIs, t2355_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2355_TI, t2355_ITIs, t2355_VT, &EmptyCustomAttributesCache, &t2355_TI, &t2355_0_0_0, &t2355_1_0_0, t2355_IOs, &t2355_GC, NULL, NULL, NULL, t2355_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2355)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5260_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.BaseEventData>
extern MethodInfo m27406_MI;
extern MethodInfo m27407_MI;
static PropertyInfo t5260____Item_PropertyInfo = 
{
	&t5260_TI, "Item", &m27406_MI, &m27407_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5260_PIs[] =
{
	&t5260____Item_PropertyInfo,
	NULL
};
extern Il2CppType t53_0_0_0;
static ParameterInfo t5260_m27408_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27408_GM;
MethodInfo m27408_MI = 
{
	"IndexOf", NULL, &t5260_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5260_m27408_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27408_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t53_0_0_0;
static ParameterInfo t5260_m27409_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27409_GM;
MethodInfo m27409_MI = 
{
	"Insert", NULL, &t5260_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5260_m27409_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27409_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5260_m27410_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27410_GM;
MethodInfo m27410_MI = 
{
	"RemoveAt", NULL, &t5260_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5260_m27410_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27410_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5260_m27406_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t53_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27406_GM;
MethodInfo m27406_MI = 
{
	"get_Item", NULL, &t5260_TI, &t53_0_0_0, RuntimeInvoker_t29_t44, t5260_m27406_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27406_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t53_0_0_0;
static ParameterInfo t5260_m27407_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27407_GM;
MethodInfo m27407_MI = 
{
	"set_Item", NULL, &t5260_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5260_m27407_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27407_GM};
static MethodInfo* t5260_MIs[] =
{
	&m27408_MI,
	&m27409_MI,
	&m27410_MI,
	&m27406_MI,
	&m27407_MI,
	NULL
};
static TypeInfo* t5260_ITIs[] = 
{
	&t603_TI,
	&t5259_TI,
	&t5261_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5260_0_0_0;
extern Il2CppType t5260_1_0_0;
struct t5260;
extern Il2CppGenericClass t5260_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5260_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5260_MIs, t5260_PIs, NULL, NULL, NULL, NULL, NULL, &t5260_TI, t5260_ITIs, NULL, &t1908__CustomAttributeCache, &t5260_TI, &t5260_0_0_0, &t5260_1_0_0, NULL, &t5260_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif

#include "t320.h"
#include "t2357.h"
extern TypeInfo t320_TI;
extern TypeInfo t2357_TI;
#include "t2357MD.h"
#include "t320MD.h"
extern MethodInfo m3988_MI;
extern MethodInfo m1425_MI;
extern MethodInfo m12128_MI;
extern MethodInfo m12143_MI;
extern MethodInfo m20510_MI;
extern MethodInfo m20511_MI;
extern MethodInfo m12130_MI;
struct t118;
 void m20510 (t118 * __this, t20 * p0, int32_t p1, t2357 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t118;
 void m20511 (t118 * __this, t2343* p0, int32_t p1, t2357 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;


 void m12117 (t321 * __this, t118 * p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		if (p0)
		{
			goto IL_0014;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1173, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0014:
	{
		__this->f0 = p0;
		return;
	}
}
extern MethodInfo m12118_MI;
 void m12118 (t321 * __this, t6 * p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral1174, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m12119_MI;
 void m12119 (t321 * __this, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral1174, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m12120_MI;
 bool m12120 (t321 * __this, t6 * p0, MethodInfo* method){
	{
		t118 * L_0 = (__this->f0);
		bool L_1 = m12067(L_0, p0, &m12067_MI);
		return L_1;
	}
}
extern MethodInfo m12121_MI;
 bool m12121 (t321 * __this, t6 * p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral1174, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m12122_MI;
 t29* m12122 (t321 * __this, MethodInfo* method){
	{
		t320  L_0 = m1425(__this, &m1425_MI);
		t320  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t320_TI), &L_1);
		return (t29*)L_2;
	}
}
extern MethodInfo m12123_MI;
 void m12123 (t321 * __this, t20 * p0, int32_t p1, MethodInfo* method){
	t2343* V_0 = {0};
	{
		V_0 = ((t2343*)IsInst(p0, InitializedTypeInfo(&t2343_TI)));
		if (!V_0)
		{
			goto IL_0013;
		}
	}
	{
		VirtActionInvoker2< t2343*, int32_t >::Invoke(&m12128_MI, __this, V_0, p1);
		return;
	}

IL_0013:
	{
		t118 * L_0 = (__this->f0);
		m12061(L_0, p0, p1, &m12061_MI);
		t118 * L_1 = (__this->f0);
		t35 L_2 = { &m12063_MI };
		t2357 * L_3 = (t2357 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2357_TI));
		m12143(L_3, NULL, L_2, &m12143_MI);
		m20510(L_1, p0, p1, L_3, &m20510_MI);
		return;
	}
}
extern MethodInfo m12124_MI;
 t29 * m12124 (t321 * __this, MethodInfo* method){
	{
		t320  L_0 = m1425(__this, &m1425_MI);
		t320  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t320_TI), &L_1);
		return (t29 *)L_2;
	}
}
extern MethodInfo m12125_MI;
 bool m12125 (t321 * __this, MethodInfo* method){
	{
		return 1;
	}
}
extern MethodInfo m12126_MI;
 bool m12126 (t321 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m12127_MI;
 t29 * m12127 (t321 * __this, MethodInfo* method){
	{
		t118 * L_0 = (__this->f0);
		t29 * L_1 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m9816_MI, L_0);
		return L_1;
	}
}
 void m12128 (t321 * __this, t2343* p0, int32_t p1, MethodInfo* method){
	{
		t118 * L_0 = (__this->f0);
		m12061(L_0, (t20 *)(t20 *)p0, p1, &m12061_MI);
		t118 * L_1 = (__this->f0);
		t35 L_2 = { &m12063_MI };
		t2357 * L_3 = (t2357 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2357_TI));
		m12143(L_3, NULL, L_2, &m12143_MI);
		m20511(L_1, p0, p1, L_3, &m20511_MI);
		return;
	}
}
 t320  m1425 (t321 * __this, MethodInfo* method){
	{
		t118 * L_0 = (__this->f0);
		t320  L_1 = {0};
		m12130(&L_1, L_0, &m12130_MI);
		return L_1;
	}
}
extern MethodInfo m12129_MI;
 int32_t m12129 (t321 * __this, MethodInfo* method){
	{
		t118 * L_0 = (__this->f0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m12056_MI, L_0);
		return L_1;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,UnityEngine.EventSystems.PointerEventData>
extern Il2CppType t118_0_0_1;
FieldInfo t321_f0_FieldInfo = 
{
	"dictionary", &t118_0_0_1, &t321_TI, offsetof(t321, f0), &EmptyCustomAttributesCache};
static FieldInfo* t321_FIs[] =
{
	&t321_f0_FieldInfo,
	NULL
};
static PropertyInfo t321____System_Collections_Generic_ICollectionU3CTValueU3E_IsReadOnly_PropertyInfo = 
{
	&t321_TI, "System.Collections.Generic.ICollection<TValue>.IsReadOnly", &m12125_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t321____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t321_TI, "System.Collections.ICollection.IsSynchronized", &m12126_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t321____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t321_TI, "System.Collections.ICollection.SyncRoot", &m12127_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t321____Count_PropertyInfo = 
{
	&t321_TI, "Count", &m12129_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t321_PIs[] =
{
	&t321____System_Collections_Generic_ICollectionU3CTValueU3E_IsReadOnly_PropertyInfo,
	&t321____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t321____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t321____Count_PropertyInfo,
	NULL
};
extern Il2CppType t118_0_0_0;
static ParameterInfo t321_m12117_ParameterInfos[] = 
{
	{"dictionary", 0, 134217728, &EmptyCustomAttributesCache, &t118_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12117_GM;
MethodInfo m12117_MI = 
{
	".ctor", (methodPointerType)&m12117, &t321_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t321_m12117_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12117_GM};
extern Il2CppType t6_0_0_0;
static ParameterInfo t321_m12118_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12118_GM;
MethodInfo m12118_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Add", (methodPointerType)&m12118, &t321_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t321_m12118_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12118_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12119_GM;
MethodInfo m12119_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Clear", (methodPointerType)&m12119, &t321_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 12, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12119_GM};
extern Il2CppType t6_0_0_0;
static ParameterInfo t321_m12120_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12120_GM;
MethodInfo m12120_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Contains", (methodPointerType)&m12120, &t321_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t321_m12120_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12120_GM};
extern Il2CppType t6_0_0_0;
static ParameterInfo t321_m12121_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12121_GM;
MethodInfo m12121_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Remove", (methodPointerType)&m12121, &t321_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t321_m12121_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12121_GM};
extern Il2CppType t2356_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12122_GM;
MethodInfo m12122_MI = 
{
	"System.Collections.Generic.IEnumerable<TValue>.GetEnumerator", (methodPointerType)&m12122, &t321_TI, &t2356_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 16, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12122_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t321_m12123_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12123_GM;
MethodInfo m12123_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m12123, &t321_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t321_m12123_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12123_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12124_GM;
MethodInfo m12124_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m12124, &t321_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12124_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12125_GM;
MethodInfo m12125_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.get_IsReadOnly", (methodPointerType)&m12125, &t321_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12125_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12126_GM;
MethodInfo m12126_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m12126, &t321_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12126_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12127_GM;
MethodInfo m12127_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m12127, &t321_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12127_GM};
extern Il2CppType t2343_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t321_m12128_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2343_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12128_GM;
MethodInfo m12128_MI = 
{
	"CopyTo", (methodPointerType)&m12128, &t321_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t321_m12128_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 14, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12128_GM};
extern Il2CppType t320_0_0_0;
extern void* RuntimeInvoker_t320 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1425_GM;
MethodInfo m1425_MI = 
{
	"GetEnumerator", (methodPointerType)&m1425, &t321_TI, &t320_0_0_0, RuntimeInvoker_t320, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1425_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12129_GM;
MethodInfo m12129_MI = 
{
	"get_Count", (methodPointerType)&m12129, &t321_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12129_GM};
static MethodInfo* t321_MIs[] =
{
	&m12117_MI,
	&m12118_MI,
	&m12119_MI,
	&m12120_MI,
	&m12121_MI,
	&m12122_MI,
	&m12123_MI,
	&m12124_MI,
	&m12125_MI,
	&m12126_MI,
	&m12127_MI,
	&m12128_MI,
	&m1425_MI,
	&m12129_MI,
	NULL
};
static MethodInfo* t321_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12124_MI,
	&m12129_MI,
	&m12126_MI,
	&m12127_MI,
	&m12123_MI,
	&m12129_MI,
	&m12125_MI,
	&m12118_MI,
	&m12119_MI,
	&m12120_MI,
	&m12128_MI,
	&m12121_MI,
	&m12122_MI,
};
static TypeInfo* t321_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t5256_TI,
	&t5258_TI,
};
static Il2CppInterfaceOffsetPair t321_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t5256_TI, 9},
	{ &t5258_TI, 16},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t321_0_0_0;
extern Il2CppType t321_1_0_0;
struct t321;
extern Il2CppGenericClass t321_GC;
extern TypeInfo t1254_TI;
extern CustomAttributesCache t1252__CustomAttributeCache;
TypeInfo t321_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ValueCollection", "", t321_MIs, t321_PIs, t321_FIs, NULL, &t29_TI, NULL, &t1254_TI, &t321_TI, t321_ITIs, t321_VT, &t1252__CustomAttributeCache, &t321_TI, &t321_0_0_0, &t321_1_0_0, t321_IOs, &t321_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t321), 0, -1, 0, 0, -1, 1057026, 0, false, false, false, false, true, false, false, false, false, false, false, false, 14, 4, 1, 0, 0, 17, 4, 4};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m12139_MI;
extern MethodInfo m12142_MI;
extern MethodInfo m1436_MI;


 void m12130 (t320 * __this, t118 * p0, MethodInfo* method){
	{
		t323  L_0 = m1432(p0, &m1432_MI);
		__this->f0 = L_0;
		return;
	}
}
extern MethodInfo m12131_MI;
 t29 * m12131 (t320 * __this, MethodInfo* method){
	{
		t323 * L_0 = &(__this->f0);
		t6 * L_1 = m12139(L_0, &m12139_MI);
		t6 * L_2 = L_1;
		return ((t6 *)L_2);
	}
}
extern MethodInfo m12132_MI;
 void m12132 (t320 * __this, MethodInfo* method){
	{
		t323 * L_0 = &(__this->f0);
		m12142(L_0, &m12142_MI);
		return;
	}
}
extern MethodInfo m1427_MI;
 bool m1427 (t320 * __this, MethodInfo* method){
	{
		t323 * L_0 = &(__this->f0);
		bool L_1 = m1436(L_0, &m1436_MI);
		return L_1;
	}
}
extern MethodInfo m1426_MI;
 t6 * m1426 (t320 * __this, MethodInfo* method){
	{
		t323 * L_0 = &(__this->f0);
		t322 * L_1 = &(L_0->f3);
		t6 * L_2 = m1434(L_1, &m1434_MI);
		return L_2;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>
extern Il2CppType t323_0_0_1;
FieldInfo t320_f0_FieldInfo = 
{
	"host_enumerator", &t323_0_0_1, &t320_TI, offsetof(t320, f0) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t320_FIs[] =
{
	&t320_f0_FieldInfo,
	NULL
};
static PropertyInfo t320____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t320_TI, "System.Collections.IEnumerator.Current", &m12131_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t320____Current_PropertyInfo = 
{
	&t320_TI, "Current", &m1426_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t320_PIs[] =
{
	&t320____System_Collections_IEnumerator_Current_PropertyInfo,
	&t320____Current_PropertyInfo,
	NULL
};
extern Il2CppType t118_0_0_0;
static ParameterInfo t320_m12130_ParameterInfos[] = 
{
	{"host", 0, 134217728, &EmptyCustomAttributesCache, &t118_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12130_GM;
MethodInfo m12130_MI = 
{
	".ctor", (methodPointerType)&m12130, &t320_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t320_m12130_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12130_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12131_GM;
MethodInfo m12131_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m12131, &t320_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12131_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12132_GM;
MethodInfo m12132_MI = 
{
	"Dispose", (methodPointerType)&m12132, &t320_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12132_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1427_GM;
MethodInfo m1427_MI = 
{
	"MoveNext", (methodPointerType)&m1427, &t320_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1427_GM};
extern Il2CppType t6_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1426_GM;
MethodInfo m1426_MI = 
{
	"get_Current", (methodPointerType)&m1426, &t320_TI, &t6_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1426_GM};
static MethodInfo* t320_MIs[] =
{
	&m12130_MI,
	&m12131_MI,
	&m12132_MI,
	&m1427_MI,
	&m1426_MI,
	NULL
};
static MethodInfo* t320_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m12131_MI,
	&m1427_MI,
	&m12132_MI,
	&m1426_MI,
};
static TypeInfo* t320_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2356_TI,
};
static Il2CppInterfaceOffsetPair t320_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2356_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t320_0_0_0;
extern Il2CppType t320_1_0_0;
extern Il2CppGenericClass t320_GC;
extern TypeInfo t1252_TI;
TypeInfo t320_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t320_MIs, t320_PIs, t320_FIs, NULL, &t110_TI, NULL, &t1252_TI, &t320_TI, t320_ITIs, t320_VT, &EmptyCustomAttributesCache, &t320_TI, &t320_0_0_0, &t320_1_0_0, t320_IOs, &t320_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t320)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 1, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m12141_MI;
extern MethodInfo m12138_MI;
extern MethodInfo m12140_MI;


 void m12133 (t323 * __this, t118 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		int32_t L_0 = (p0->f14);
		__this->f2 = L_0;
		return;
	}
}
extern MethodInfo m12134_MI;
 t29 * m12134 (t323 * __this, MethodInfo* method){
	{
		m12141(__this, &m12141_MI);
		t322  L_0 = (__this->f3);
		t322  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t322_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m12135_MI;
 t725  m12135 (t323 * __this, MethodInfo* method){
	{
		m12141(__this, &m12141_MI);
		t322 * L_0 = &(__this->f3);
		int32_t L_1 = m1435(L_0, &m1435_MI);
		int32_t L_2 = L_1;
		t29 * L_3 = Box(InitializedTypeInfo(&t44_TI), &L_2);
		t322 * L_4 = &(__this->f3);
		t6 * L_5 = m1434(L_4, &m1434_MI);
		t6 * L_6 = L_5;
		t725  L_7 = {0};
		m3965(&L_7, L_3, ((t6 *)L_6), &m3965_MI);
		return L_7;
	}
}
extern MethodInfo m12136_MI;
 t29 * m12136 (t323 * __this, MethodInfo* method){
	{
		int32_t L_0 = m12138(__this, &m12138_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t44_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m12137_MI;
 t29 * m12137 (t323 * __this, MethodInfo* method){
	{
		t6 * L_0 = m12139(__this, &m12139_MI);
		t6 * L_1 = L_0;
		return ((t6 *)L_1);
	}
}
 bool m1436 (t323 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		m12140(__this, &m12140_MI);
		int32_t L_0 = (__this->f1);
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0011;
		}
	}
	{
		return 0;
	}

IL_0011:
	{
		goto IL_0072;
	}

IL_0013:
	{
		int32_t L_1 = (__this->f1);
		int32_t L_2 = L_1;
		V_1 = L_2;
		__this->f1 = ((int32_t)(L_2+1));
		V_0 = V_1;
		t118 * L_3 = (__this->f0);
		t1965* L_4 = (L_3->f5);
		int32_t L_5 = (((t1248 *)(t1248 *)SZArrayLdElema(L_4, V_0))->f0);
		if (!((int32_t)((int32_t)L_5&(int32_t)((int32_t)-2147483648))))
		{
			goto IL_0072;
		}
	}
	{
		t118 * L_6 = (__this->f0);
		t841* L_7 = (L_6->f6);
		int32_t L_8 = V_0;
		t118 * L_9 = (__this->f0);
		t2343* L_10 = (L_9->f7);
		int32_t L_11 = V_0;
		t322  L_12 = {0};
		m12074(&L_12, (*(int32_t*)(int32_t*)SZArrayLdElema(L_7, L_8)), (*(t6 **)(t6 **)SZArrayLdElema(L_10, L_11)), &m12074_MI);
		__this->f3 = L_12;
		return 1;
	}

IL_0072:
	{
		int32_t L_13 = (__this->f1);
		t118 * L_14 = (__this->f0);
		int32_t L_15 = (L_14->f8);
		if ((((int32_t)L_13) < ((int32_t)L_15)))
		{
			goto IL_0013;
		}
	}
	{
		__this->f1 = (-1);
		return 0;
	}
}
extern MethodInfo m1433_MI;
 t322  m1433 (t323 * __this, MethodInfo* method){
	{
		t322  L_0 = (__this->f3);
		return L_0;
	}
}
 int32_t m12138 (t323 * __this, MethodInfo* method){
	{
		m12141(__this, &m12141_MI);
		t322 * L_0 = &(__this->f3);
		int32_t L_1 = m1435(L_0, &m1435_MI);
		return L_1;
	}
}
 t6 * m12139 (t323 * __this, MethodInfo* method){
	{
		m12141(__this, &m12141_MI);
		t322 * L_0 = &(__this->f3);
		t6 * L_1 = m1434(L_0, &m1434_MI);
		return L_1;
	}
}
 void m12140 (t323 * __this, MethodInfo* method){
	{
		t118 * L_0 = (__this->f0);
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		t1101 * L_1 = (t1101 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1101_TI));
		m5150(L_1, (t7*)NULL, &m5150_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_000f:
	{
		t118 * L_2 = (__this->f0);
		int32_t L_3 = (L_2->f14);
		int32_t L_4 = (__this->f2);
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_002d;
		}
	}
	{
		t914 * L_5 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_5, (t7*) &_stringLiteral1171, &m3964_MI);
		il2cpp_codegen_raise_exception(L_5);
	}

IL_002d:
	{
		return;
	}
}
 void m12141 (t323 * __this, MethodInfo* method){
	{
		m12140(__this, &m12140_MI);
		int32_t L_0 = (__this->f1);
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001a;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1172, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_001a:
	{
		return;
	}
}
 void m12142 (t323 * __this, MethodInfo* method){
	{
		__this->f0 = (t118 *)NULL;
		return;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>
extern Il2CppType t118_0_0_1;
FieldInfo t323_f0_FieldInfo = 
{
	"dictionary", &t118_0_0_1, &t323_TI, offsetof(t323, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t323_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t323_TI, offsetof(t323, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t323_f2_FieldInfo = 
{
	"stamp", &t44_0_0_1, &t323_TI, offsetof(t323, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t322_0_0_3;
FieldInfo t323_f3_FieldInfo = 
{
	"current", &t322_0_0_3, &t323_TI, offsetof(t323, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t323_FIs[] =
{
	&t323_f0_FieldInfo,
	&t323_f1_FieldInfo,
	&t323_f2_FieldInfo,
	&t323_f3_FieldInfo,
	NULL
};
static PropertyInfo t323____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t323_TI, "System.Collections.IEnumerator.Current", &m12134_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t323____System_Collections_IDictionaryEnumerator_Entry_PropertyInfo = 
{
	&t323_TI, "System.Collections.IDictionaryEnumerator.Entry", &m12135_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t323____System_Collections_IDictionaryEnumerator_Key_PropertyInfo = 
{
	&t323_TI, "System.Collections.IDictionaryEnumerator.Key", &m12136_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t323____System_Collections_IDictionaryEnumerator_Value_PropertyInfo = 
{
	&t323_TI, "System.Collections.IDictionaryEnumerator.Value", &m12137_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t323____Current_PropertyInfo = 
{
	&t323_TI, "Current", &m1433_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t323____CurrentKey_PropertyInfo = 
{
	&t323_TI, "CurrentKey", &m12138_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t323____CurrentValue_PropertyInfo = 
{
	&t323_TI, "CurrentValue", &m12139_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t323_PIs[] =
{
	&t323____System_Collections_IEnumerator_Current_PropertyInfo,
	&t323____System_Collections_IDictionaryEnumerator_Entry_PropertyInfo,
	&t323____System_Collections_IDictionaryEnumerator_Key_PropertyInfo,
	&t323____System_Collections_IDictionaryEnumerator_Value_PropertyInfo,
	&t323____Current_PropertyInfo,
	&t323____CurrentKey_PropertyInfo,
	&t323____CurrentValue_PropertyInfo,
	NULL
};
extern Il2CppType t118_0_0_0;
static ParameterInfo t323_m12133_ParameterInfos[] = 
{
	{"dictionary", 0, 134217728, &EmptyCustomAttributesCache, &t118_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12133_GM;
MethodInfo m12133_MI = 
{
	".ctor", (methodPointerType)&m12133, &t323_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t323_m12133_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12133_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12134_GM;
MethodInfo m12134_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m12134, &t323_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12134_GM};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12135_GM;
MethodInfo m12135_MI = 
{
	"System.Collections.IDictionaryEnumerator.get_Entry", (methodPointerType)&m12135, &t323_TI, &t725_0_0_0, RuntimeInvoker_t725, NULL, &EmptyCustomAttributesCache, 2529, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12135_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12136_GM;
MethodInfo m12136_MI = 
{
	"System.Collections.IDictionaryEnumerator.get_Key", (methodPointerType)&m12136, &t323_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12136_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12137_GM;
MethodInfo m12137_MI = 
{
	"System.Collections.IDictionaryEnumerator.get_Value", (methodPointerType)&m12137, &t323_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12137_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1436_GM;
MethodInfo m1436_MI = 
{
	"MoveNext", (methodPointerType)&m1436, &t323_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1436_GM};
extern Il2CppType t322_0_0_0;
extern void* RuntimeInvoker_t322 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1433_GM;
MethodInfo m1433_MI = 
{
	"get_Current", (methodPointerType)&m1433, &t323_TI, &t322_0_0_0, RuntimeInvoker_t322, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1433_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12138_GM;
MethodInfo m12138_MI = 
{
	"get_CurrentKey", (methodPointerType)&m12138, &t323_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2179, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12138_GM};
extern Il2CppType t6_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12139_GM;
MethodInfo m12139_MI = 
{
	"get_CurrentValue", (methodPointerType)&m12139, &t323_TI, &t6_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2179, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12139_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12140_GM;
MethodInfo m12140_MI = 
{
	"VerifyState", (methodPointerType)&m12140, &t323_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12140_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12141_GM;
MethodInfo m12141_MI = 
{
	"VerifyCurrent", (methodPointerType)&m12141, &t323_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12141_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12142_GM;
MethodInfo m12142_MI = 
{
	"Dispose", (methodPointerType)&m12142, &t323_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12142_GM};
static MethodInfo* t323_MIs[] =
{
	&m12133_MI,
	&m12134_MI,
	&m12135_MI,
	&m12136_MI,
	&m12137_MI,
	&m1436_MI,
	&m1433_MI,
	&m12138_MI,
	&m12139_MI,
	&m12140_MI,
	&m12141_MI,
	&m12142_MI,
	NULL
};
static MethodInfo* t323_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m12134_MI,
	&m1436_MI,
	&m12142_MI,
	&m1433_MI,
	&m12135_MI,
	&m12136_MI,
	&m12137_MI,
};
extern TypeInfo t722_TI;
static TypeInfo* t323_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2347_TI,
	&t722_TI,
};
static Il2CppInterfaceOffsetPair t323_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2347_TI, 7},
	{ &t722_TI, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t323_0_0_0;
extern Il2CppType t323_1_0_0;
extern Il2CppGenericClass t323_GC;
TypeInfo t323_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t323_MIs, t323_PIs, t323_FIs, NULL, &t110_TI, NULL, &t1254_TI, &t323_TI, t323_ITIs, t323_VT, &EmptyCustomAttributesCache, &t323_TI, &t323_0_0_0, &t323_1_0_0, t323_IOs, &t323_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t323)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 12, 7, 4, 0, 0, 11, 4, 4};
#ifndef _MSC_VER
#else
#endif



 void m12143 (t2357 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m12144_MI;
 t6 * m12144 (t2357 * __this, int32_t p0, t6 * p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m12144((t2357 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef t6 * (*FunctionPointerType) (t29 *, t29 * __this, int32_t p0, t6 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	typedef t6 * (*FunctionPointerType) (t29 * __this, int32_t p0, t6 * p1, MethodInfo* method);
	return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
}
extern MethodInfo m12145_MI;
 t29 * m12145 (t2357 * __this, int32_t p0, t6 * p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = Box(InitializedTypeInfo(&t44_TI), &p0);
	__d_args[1] = p1;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m12146_MI;
 t6 * m12146 (t2357 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return (t6 *)__result;
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,UnityEngine.EventSystems.PointerEventData,UnityEngine.EventSystems.PointerEventData>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2357_m12143_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12143_GM;
MethodInfo m12143_MI = 
{
	".ctor", (methodPointerType)&m12143, &t2357_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2357_m12143_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12143_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t6_0_0_0;
static ParameterInfo t2357_m12144_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t6_0_0_0;
extern void* RuntimeInvoker_t29_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12144_GM;
MethodInfo m12144_MI = 
{
	"Invoke", (methodPointerType)&m12144, &t2357_TI, &t6_0_0_0, RuntimeInvoker_t29_t44_t29, t2357_m12144_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12144_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t6_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2357_m12145_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t6_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t44_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12145_GM;
MethodInfo m12145_MI = 
{
	"BeginInvoke", (methodPointerType)&m12145, &t2357_TI, &t66_0_0_0, RuntimeInvoker_t29_t44_t29_t29_t29, t2357_m12145_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m12145_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2357_m12146_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t6_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12146_GM;
MethodInfo m12146_MI = 
{
	"EndInvoke", (methodPointerType)&m12146, &t2357_TI, &t6_0_0_0, RuntimeInvoker_t29_t29, t2357_m12146_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12146_GM};
static MethodInfo* t2357_MIs[] =
{
	&m12143_MI,
	&m12144_MI,
	&m12145_MI,
	&m12146_MI,
	NULL
};
static MethodInfo* t2357_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m12144_MI,
	&m12145_MI,
	&m12146_MI,
};
static Il2CppInterfaceOffsetPair t2357_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2357_0_0_0;
extern Il2CppType t2357_1_0_0;
struct t2357;
extern Il2CppGenericClass t2357_GC;
TypeInfo t2357_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Transform`1", "", t2357_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1254_TI, &t2357_TI, NULL, t2357_VT, &EmptyCustomAttributesCache, &t2357_TI, &t2357_0_0_0, &t2357_1_0_0, t2357_IOs, &t2357_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2357), 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
