﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2257;
struct t29;
struct t56;
#include "t57.h"

 void m11308 (t2257 * __this, t56 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m11309 (t2257 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11310 (t2257 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11311 (t2257 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m11312 (t2257 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t57  m11313 (t2257 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
