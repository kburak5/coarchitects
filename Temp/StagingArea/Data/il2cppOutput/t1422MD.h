﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1422;
struct t7;
struct t29;
struct t1424;
struct t1425;

 t7* m7771 (t1422 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7772 (t1422 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7773 (t1422 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7774 (t1422 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7775 (t1422 * __this, t1425 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
