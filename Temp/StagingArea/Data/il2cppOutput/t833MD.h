﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t833;
struct t831;
struct t7;

 void m3517 (t833 * __this, t7* p0, int32_t p1, int32_t p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3518 (t833 * __this, t7* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3519 (t833 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3520 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t831 * m3521 (t833 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3522 (t833 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
