﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3388;
struct t29;
struct t20;
#include "t1149.h"

 void m18812 (t3388 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18813 (t3388 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18814 (t3388 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18815 (t3388 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18816 (t3388 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
