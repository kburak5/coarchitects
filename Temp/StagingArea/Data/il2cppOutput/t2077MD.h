﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2077;
#include "t1629.h"

 void m10259 (t2077 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m19391 (t2077 * __this, t1629  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19392 (t2077 * __this, t1629  p0, t1629  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
