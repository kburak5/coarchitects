﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t294;
struct t29;
struct t346;
struct t20;
struct t136;
struct t2182;
struct t2183;
struct t2184;
struct t316;
struct t2180;
struct t2181;
#include "t2185.h"

 void m10537_gshared (t294 * __this, MethodInfo* method);
#define m10537(__this, method) (void)m10537_gshared((t294 *)__this, method)
 void m10539_gshared (t294 * __this, int32_t p0, MethodInfo* method);
#define m10539(__this, p0, method) (void)m10539_gshared((t294 *)__this, (int32_t)p0, method)
 void m10541_gshared (t29 * __this, MethodInfo* method);
#define m10541(__this, method) (void)m10541_gshared((t29 *)__this, method)
 t29* m10543_gshared (t294 * __this, MethodInfo* method);
#define m10543(__this, method) (t29*)m10543_gshared((t294 *)__this, method)
 void m10545_gshared (t294 * __this, t20 * p0, int32_t p1, MethodInfo* method);
#define m10545(__this, p0, p1, method) (void)m10545_gshared((t294 *)__this, (t20 *)p0, (int32_t)p1, method)
 t29 * m10547_gshared (t294 * __this, MethodInfo* method);
#define m10547(__this, method) (t29 *)m10547_gshared((t294 *)__this, method)
 int32_t m10549_gshared (t294 * __this, t29 * p0, MethodInfo* method);
#define m10549(__this, p0, method) (int32_t)m10549_gshared((t294 *)__this, (t29 *)p0, method)
 bool m10551_gshared (t294 * __this, t29 * p0, MethodInfo* method);
#define m10551(__this, p0, method) (bool)m10551_gshared((t294 *)__this, (t29 *)p0, method)
 int32_t m10553_gshared (t294 * __this, t29 * p0, MethodInfo* method);
#define m10553(__this, p0, method) (int32_t)m10553_gshared((t294 *)__this, (t29 *)p0, method)
 void m10555_gshared (t294 * __this, int32_t p0, t29 * p1, MethodInfo* method);
#define m10555(__this, p0, p1, method) (void)m10555_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
 void m10557_gshared (t294 * __this, t29 * p0, MethodInfo* method);
#define m10557(__this, p0, method) (void)m10557_gshared((t294 *)__this, (t29 *)p0, method)
 bool m10559_gshared (t294 * __this, MethodInfo* method);
#define m10559(__this, method) (bool)m10559_gshared((t294 *)__this, method)
 bool m10561_gshared (t294 * __this, MethodInfo* method);
#define m10561(__this, method) (bool)m10561_gshared((t294 *)__this, method)
 t29 * m10563_gshared (t294 * __this, MethodInfo* method);
#define m10563(__this, method) (t29 *)m10563_gshared((t294 *)__this, method)
 bool m10565_gshared (t294 * __this, MethodInfo* method);
#define m10565(__this, method) (bool)m10565_gshared((t294 *)__this, method)
 bool m10567_gshared (t294 * __this, MethodInfo* method);
#define m10567(__this, method) (bool)m10567_gshared((t294 *)__this, method)
 t29 * m10569_gshared (t294 * __this, int32_t p0, MethodInfo* method);
#define m10569(__this, p0, method) (t29 *)m10569_gshared((t294 *)__this, (int32_t)p0, method)
 void m10571_gshared (t294 * __this, int32_t p0, t29 * p1, MethodInfo* method);
#define m10571(__this, p0, p1, method) (void)m10571_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
 void m10573_gshared (t294 * __this, t29 * p0, MethodInfo* method);
#define m10573(__this, p0, method) (void)m10573_gshared((t294 *)__this, (t29 *)p0, method)
 void m10575_gshared (t294 * __this, int32_t p0, MethodInfo* method);
#define m10575(__this, p0, method) (void)m10575_gshared((t294 *)__this, (int32_t)p0, method)
 void m10577_gshared (t294 * __this, t29* p0, MethodInfo* method);
#define m10577(__this, p0, method) (void)m10577_gshared((t294 *)__this, (t29*)p0, method)
 void m10579_gshared (t294 * __this, t29* p0, MethodInfo* method);
#define m10579(__this, p0, method) (void)m10579_gshared((t294 *)__this, (t29*)p0, method)
 void m10581_gshared (t294 * __this, t29* p0, MethodInfo* method);
#define m10581(__this, p0, method) (void)m10581_gshared((t294 *)__this, (t29*)p0, method)
 t2184 * m10583_gshared (t294 * __this, MethodInfo* method);
#define m10583(__this, method) (t2184 *)m10583_gshared((t294 *)__this, method)
 void m10585_gshared (t294 * __this, MethodInfo* method);
#define m10585(__this, method) (void)m10585_gshared((t294 *)__this, method)
 bool m10587_gshared (t294 * __this, t29 * p0, MethodInfo* method);
#define m10587(__this, p0, method) (bool)m10587_gshared((t294 *)__this, (t29 *)p0, method)
 void m10589_gshared (t294 * __this, t316* p0, int32_t p1, MethodInfo* method);
#define m10589(__this, p0, p1, method) (void)m10589_gshared((t294 *)__this, (t316*)p0, (int32_t)p1, method)
 t29 * m10591_gshared (t294 * __this, t2180 * p0, MethodInfo* method);
#define m10591(__this, p0, method) (t29 *)m10591_gshared((t294 *)__this, (t2180 *)p0, method)
 void m10593_gshared (t29 * __this, t2180 * p0, MethodInfo* method);
#define m10593(__this, p0, method) (void)m10593_gshared((t29 *)__this, (t2180 *)p0, method)
 int32_t m10595_gshared (t294 * __this, int32_t p0, int32_t p1, t2180 * p2, MethodInfo* method);
#define m10595(__this, p0, p1, p2, method) (int32_t)m10595_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, (t2180 *)p2, method)
 t2185  m10630 (t294 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m10598_gshared (t294 * __this, t29 * p0, MethodInfo* method);
#define m10598(__this, p0, method) (int32_t)m10598_gshared((t294 *)__this, (t29 *)p0, method)
 void m10600_gshared (t294 * __this, int32_t p0, int32_t p1, MethodInfo* method);
#define m10600(__this, p0, p1, method) (void)m10600_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, method)
 void m10602_gshared (t294 * __this, int32_t p0, MethodInfo* method);
#define m10602(__this, p0, method) (void)m10602_gshared((t294 *)__this, (int32_t)p0, method)
 void m10604_gshared (t294 * __this, int32_t p0, t29 * p1, MethodInfo* method);
#define m10604(__this, p0, p1, method) (void)m10604_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
 void m10606_gshared (t294 * __this, t29* p0, MethodInfo* method);
#define m10606(__this, p0, method) (void)m10606_gshared((t294 *)__this, (t29*)p0, method)
 bool m10608_gshared (t294 * __this, t29 * p0, MethodInfo* method);
#define m10608(__this, p0, method) (bool)m10608_gshared((t294 *)__this, (t29 *)p0, method)
 int32_t m10610_gshared (t294 * __this, t2180 * p0, MethodInfo* method);
#define m10610(__this, p0, method) (int32_t)m10610_gshared((t294 *)__this, (t2180 *)p0, method)
 void m10611_gshared (t294 * __this, int32_t p0, MethodInfo* method);
#define m10611(__this, p0, method) (void)m10611_gshared((t294 *)__this, (int32_t)p0, method)
 void m10613_gshared (t294 * __this, MethodInfo* method);
#define m10613(__this, method) (void)m10613_gshared((t294 *)__this, method)
 void m10615_gshared (t294 * __this, MethodInfo* method);
#define m10615(__this, method) (void)m10615_gshared((t294 *)__this, method)
 void m10617_gshared (t294 * __this, t2181 * p0, MethodInfo* method);
#define m10617(__this, p0, method) (void)m10617_gshared((t294 *)__this, (t2181 *)p0, method)
 t316* m10619_gshared (t294 * __this, MethodInfo* method);
#define m10619(__this, method) (t316*)m10619_gshared((t294 *)__this, method)
 void m10621_gshared (t294 * __this, MethodInfo* method);
#define m10621(__this, method) (void)m10621_gshared((t294 *)__this, method)
 int32_t m10623_gshared (t294 * __this, MethodInfo* method);
#define m10623(__this, method) (int32_t)m10623_gshared((t294 *)__this, method)
 void m10625_gshared (t294 * __this, int32_t p0, MethodInfo* method);
#define m10625(__this, p0, method) (void)m10625_gshared((t294 *)__this, (int32_t)p0, method)
 int32_t m10626_gshared (t294 * __this, MethodInfo* method);
#define m10626(__this, method) (int32_t)m10626_gshared((t294 *)__this, method)
 t29 * m10627_gshared (t294 * __this, int32_t p0, MethodInfo* method);
#define m10627(__this, p0, method) (t29 *)m10627_gshared((t294 *)__this, (int32_t)p0, method)
 void m10629_gshared (t294 * __this, int32_t p0, t29 * p1, MethodInfo* method);
#define m10629(__this, p0, p1, method) (void)m10629_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
