﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2422;
struct t29;
struct t2415;
#include "t725.h"
#include "t2420.h"

 void m12555 (t2422 * __this, t2415 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12556 (t2422 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t725  m12557 (t2422 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12558 (t2422 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12559 (t2422 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12560 (t2422 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2420  m12561 (t2422 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12562 (t2422 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m12563 (t2422 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12564 (t2422 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12565 (t2422 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12566 (t2422 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
