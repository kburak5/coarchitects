﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t133;
struct t29;
struct t66;
struct t67;
#include "t35.h"
#include "t132.h"

 void m1594 (t133 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12419 (t133 * __this, t132  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12420 (t133 * __this, t132  p0, t67 * p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12421 (t133 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
