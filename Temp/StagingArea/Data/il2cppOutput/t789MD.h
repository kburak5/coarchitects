﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t789;
struct t7;
struct t781;
struct t778;

 void m3420 (t789 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3421 (t789 * __this, t7* p0, t781* p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3422 (t789 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3423 (t789 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3424 (t789 * __this, t778 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3425 (t789 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
