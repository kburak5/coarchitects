﻿#pragma once
#include <stdint.h>
#include "t29.h"
#include "t35.h"
struct t518  : public t29
{
	t35 f0;
};
// Native definition for marshalling of: UnityEngine.AnimationCurve
struct t518_marshaled
{
	t35 f0;
};
