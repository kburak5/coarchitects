﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1407;
struct t29;
#include "t1408.h"
#include "t1407.h"

 void m7736 (t1407 * __this, t29 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7737 (t1407 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7738 (t1407 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1407  m7739 (t29 * __this, t29 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7740 (t1407 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7741 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7742 (t29 * __this, t29 * p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7743 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7744 (t1407 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7745 (t1407 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
