﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t538;
struct t29;
struct t7;
struct t42;

 void m2736 (t538 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m2737 (t29 * __this, t29 * p0, t7* p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m2738 (t29 * __this, t42 * p0, t7* p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
