﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2596;
struct t29;
struct t20;
#include "t175.h"

 void m13991 (t2596 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m13992 (t2596 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13993 (t2596 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13994 (t2596 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m13995 (t2596 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
