﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t631;
struct t636;
struct t1365;
struct t316;
struct t634;
struct t633;
struct t446;
struct t29;
struct t42;
struct t537;
struct t1146;
struct t1366;
struct t638;
#include "t630.h"

 void m7508 (t631 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7509 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t631 * m7510 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7511 (t29 * __this, t631 * p0, t316* p1, t638* p2, t633 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7512 (t29 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t636 * m7513 (t29 * __this, t1365* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
