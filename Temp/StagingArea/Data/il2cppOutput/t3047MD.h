﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3047;
struct t29;
struct t20;
struct t136;
struct t531;
struct t3041;
struct t387;
#include "t380.h"

 void m16755 (t3047 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16756 (t3047 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16757 (t3047 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m16758 (t3047 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16759 (t3047 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16760 (t3047 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16761 (t3047 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16762 (t3047 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16763 (t3047 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16764 (t3047 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m16765 (t3047 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16766 (t3047 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16767 (t3047 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m16768 (t3047 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16769 (t3047 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16770 (t3047 * __this, t380  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16771 (t3047 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16772 (t3047 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16773 (t3047 * __this, t380  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16774 (t3047 * __this, t531* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m16775 (t3047 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16776 (t3047 * __this, t380  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16777 (t3047 * __this, int32_t p0, t380  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16778 (t3047 * __this, int32_t p0, t380  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16779 (t3047 * __this, t380  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16780 (t3047 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16781 (t3047 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16782 (t3047 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t380  m16783 (t3047 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16784 (t3047 * __this, int32_t p0, t380  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16785 (t3047 * __this, int32_t p0, t380  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16786 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t380  m16787 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16788 (t29 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16789 (t29 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16790 (t29 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
