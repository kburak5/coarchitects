﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1435;
struct t7;
#include "t35.h"

 void m8778 (t1435 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t35 m8779 (t29 * __this, bool p0, t7* p1, bool* p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8780 (t29 * __this, t35 p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8781 (t1435 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
