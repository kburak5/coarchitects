﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t147;
struct t148;
#include "t149.h"
#include "t150.h"
#include "t151.h"
#include "t152.h"

 void m410 (t147 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m411 (t147 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m412 (t147 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t147 * m413 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t148 * m414 (t147 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m415 (t147 * __this, t148 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m416 (t147 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m417 (t147 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m418 (t147 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m419 (t147 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m420 (t147 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m421 (t147 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m422 (t147 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m423 (t147 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m424 (t147 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m425 (t147 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m426 (t147 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m427 (t147 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m428 (t147 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m429 (t147 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m430 (t147 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m431 (t147 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m432 (t147 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m433 (t147 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m434 (t147 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m435 (t147 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
