﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3340;
struct t29;
struct t20;
#include "t1120.h"

 void m18572 (t3340 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18573 (t3340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18574 (t3340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18575 (t3340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18576 (t3340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
