﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2927;
struct t29;
struct t20;
#include "t408.h"

 void m16041 (t2927 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m16042 (t2927 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16043 (t2927 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16044 (t2927 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16045 (t2927 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
