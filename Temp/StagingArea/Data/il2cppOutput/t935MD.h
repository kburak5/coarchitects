﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t935;
struct t1098;
struct t781;
struct t29;
struct t999;
#include "t936.h"

 void m8208 (t935 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4063 (t935 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8209 (t935 * __this, int32_t p0, t1098 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8210 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8211 (t935 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8212 (t935 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4049 (t935 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t936  m8213 (t935 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8214 (t935 * __this, t936  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m8215 (t935 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8216 (t935 * __this, t781* p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8217 (t935 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8218 (t935 * __this, t29 * p0, t999 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
