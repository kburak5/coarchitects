﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2361;
struct t29;

 void m12166 (t2361 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12167 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m12168 (t2361 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12169 (t2361 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2361 * m12170 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
