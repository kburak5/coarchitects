﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t87;
struct t29;
struct t25;
struct t2318;
struct t20;
struct t136;
struct t304;
struct t2319;
struct t2320;
struct t2317;
struct t2321;
struct t2322;
#include "t2323.h"

#include "t294MD.h"
#define m11841(__this, method) (void)m10537_gshared((t294 *)__this, method)
#define m1359(__this, p0, method) (void)m10539_gshared((t294 *)__this, (int32_t)p0, method)
#define m11842(__this, method) (void)m10541_gshared((t29 *)__this, method)
#define m11843(__this, method) (t29*)m10543_gshared((t294 *)__this, method)
#define m11844(__this, p0, p1, method) (void)m10545_gshared((t294 *)__this, (t20 *)p0, (int32_t)p1, method)
#define m11845(__this, method) (t29 *)m10547_gshared((t294 *)__this, method)
#define m11846(__this, p0, method) (int32_t)m10549_gshared((t294 *)__this, (t29 *)p0, method)
#define m11847(__this, p0, method) (bool)m10551_gshared((t294 *)__this, (t29 *)p0, method)
#define m11848(__this, p0, method) (int32_t)m10553_gshared((t294 *)__this, (t29 *)p0, method)
#define m11849(__this, p0, p1, method) (void)m10555_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m11850(__this, p0, method) (void)m10557_gshared((t294 *)__this, (t29 *)p0, method)
#define m11851(__this, method) (bool)m10559_gshared((t294 *)__this, method)
#define m11852(__this, method) (bool)m10561_gshared((t294 *)__this, method)
#define m11853(__this, method) (t29 *)m10563_gshared((t294 *)__this, method)
#define m11854(__this, method) (bool)m10565_gshared((t294 *)__this, method)
#define m11855(__this, method) (bool)m10567_gshared((t294 *)__this, method)
#define m11856(__this, p0, method) (t29 *)m10569_gshared((t294 *)__this, (int32_t)p0, method)
#define m11857(__this, p0, p1, method) (void)m10571_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m11858(__this, p0, method) (void)m10573_gshared((t294 *)__this, (t29 *)p0, method)
#define m11859(__this, p0, method) (void)m10575_gshared((t294 *)__this, (int32_t)p0, method)
#define m11860(__this, p0, method) (void)m10577_gshared((t294 *)__this, (t29*)p0, method)
#define m11861(__this, p0, method) (void)m10579_gshared((t294 *)__this, (t29*)p0, method)
#define m11862(__this, p0, method) (void)m10581_gshared((t294 *)__this, (t29*)p0, method)
#define m11863(__this, method) (t2320 *)m10583_gshared((t294 *)__this, method)
#define m11864(__this, method) (void)m10585_gshared((t294 *)__this, method)
#define m11865(__this, p0, method) (bool)m10587_gshared((t294 *)__this, (t29 *)p0, method)
#define m11866(__this, p0, p1, method) (void)m10589_gshared((t294 *)__this, (t316*)p0, (int32_t)p1, method)
#define m11867(__this, p0, method) (t25 *)m10591_gshared((t294 *)__this, (t2180 *)p0, method)
#define m11868(__this, p0, method) (void)m10593_gshared((t29 *)__this, (t2180 *)p0, method)
#define m11869(__this, p0, p1, p2, method) (int32_t)m10595_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, (t2180 *)p2, method)
 t2323  m11870 (t87 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
#define m11871(__this, p0, method) (int32_t)m10598_gshared((t294 *)__this, (t29 *)p0, method)
#define m11872(__this, p0, p1, method) (void)m10600_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, method)
#define m11873(__this, p0, method) (void)m10602_gshared((t294 *)__this, (int32_t)p0, method)
#define m11874(__this, p0, p1, method) (void)m10604_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m11875(__this, p0, method) (void)m10606_gshared((t294 *)__this, (t29*)p0, method)
#define m11876(__this, p0, method) (bool)m10608_gshared((t294 *)__this, (t29 *)p0, method)
#define m11877(__this, p0, method) (int32_t)m10610_gshared((t294 *)__this, (t2180 *)p0, method)
#define m11878(__this, p0, method) (void)m10611_gshared((t294 *)__this, (int32_t)p0, method)
#define m11879(__this, method) (void)m10613_gshared((t294 *)__this, method)
#define m11880(__this, method) (void)m10615_gshared((t294 *)__this, method)
#define m11881(__this, p0, method) (void)m10617_gshared((t294 *)__this, (t2181 *)p0, method)
#define m11882(__this, method) (t2317*)m10619_gshared((t294 *)__this, method)
#define m11883(__this, method) (void)m10621_gshared((t294 *)__this, method)
#define m11884(__this, method) (int32_t)m10623_gshared((t294 *)__this, method)
#define m11885(__this, p0, method) (void)m10625_gshared((t294 *)__this, (int32_t)p0, method)
#define m11886(__this, method) (int32_t)m10626_gshared((t294 *)__this, method)
#define m11887(__this, p0, method) (t25 *)m10627_gshared((t294 *)__this, (int32_t)p0, method)
#define m11888(__this, p0, p1, method) (void)m10629_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
