﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t880;
struct t877;

 void m3766 (t880 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t877 * m3767 (t880 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3768 (t880 * __this, int32_t* p0, int32_t* p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3769 (t880 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
