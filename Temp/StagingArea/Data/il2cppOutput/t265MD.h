﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t265;
struct t25;
struct t2;
struct t268;
struct t266;
struct t7;
struct t28;
#include "t140.h"
#include "t265.h"

 void m1165 (t265 * __this, t2 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1166 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1167 (t265 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1168 (t29 * __this, t2 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t25 * m1169 (t265 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1170 (t265 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1171 (t29 * __this, t268 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1172 (t265 * __this, t2 * p0, t266 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1173 (t265 * __this, t2 * p0, t266 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1174 (t29 * __this, t2 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1175 (t29 * __this, t2 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1176 (t29 * __this, t2 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1177 (t29 * __this, t2 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1178 (t265 * __this, t265  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1179 (t265 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m1180 (t265 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1181 (t29 * __this, t28 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1182 (t29 * __this, t28 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1183 (t29 * __this, t28 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1184 (t29 * __this, t28 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1185 (t29 * __this, t28 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
