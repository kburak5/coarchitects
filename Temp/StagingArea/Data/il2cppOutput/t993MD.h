﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t993;
struct t781;
struct t783;
struct t782;
#include "t936.h"

 void m4428 (t993 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4429 (t993 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4430 (t993 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4431 (t993 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4432 (t29 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4433 (t29 * __this, t781* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t783 * m4434 (t29 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t782 * m4435 (t29 * __this, t781* p0, t936  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
