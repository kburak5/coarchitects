﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1683;
struct t1683_marshaled;

void t1683_marshal(const t1683& unmarshaled, t1683_marshaled& marshaled);
void t1683_marshal_back(const t1683_marshaled& marshaled, t1683& unmarshaled);
void t1683_marshal_cleanup(t1683_marshaled& marshaled);
