﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2444;
struct t145;
struct t7;

 void m12774 (t2444 * __this, t29 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12775 (t2444 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12776 (t2444 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m12777 (t2444 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12778 (t2444 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m12779 (t2444 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
