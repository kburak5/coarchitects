﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t828;
struct t834;
struct t829;
struct t836;
struct t7;

 void m3531 (t828 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3532 (t828 * __this, t829 * p0, t29 * p1, t7* p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3533 (t828 * __this, t829 * p0, t29 * p1, t7* p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3534 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t828 * m3535 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t834 * m3536 (t828 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t828 * m3537 (t828 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t829 * m3538 (t828 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
