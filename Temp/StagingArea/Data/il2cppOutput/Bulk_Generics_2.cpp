﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t5139_TI;


#include "t20.h"

// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._MemberInfo>
extern Il2CppType t3999_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26700_GM;
MethodInfo m26700_MI = 
{
	"GetEnumerator", NULL, &t5139_TI, &t3999_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26700_GM};
static MethodInfo* t5139_MIs[] =
{
	&m26700_MI,
	NULL
};
extern TypeInfo t603_TI;
static TypeInfo* t5139_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5139_0_0_0;
extern Il2CppType t5139_1_0_0;
struct t5139;
extern Il2CppGenericClass t5139_GC;
TypeInfo t5139_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5139_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5139_TI, t5139_ITIs, NULL, &EmptyCustomAttributesCache, &t5139_TI, &t5139_0_0_0, &t5139_1_0_0, NULL, &t5139_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3999_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._MemberInfo>
extern MethodInfo m26701_MI;
static PropertyInfo t3999____Current_PropertyInfo = 
{
	&t3999_TI, "Current", &m26701_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3999_PIs[] =
{
	&t3999____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1915_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26701_GM;
MethodInfo m26701_MI = 
{
	"get_Current", NULL, &t3999_TI, &t1915_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26701_GM};
static MethodInfo* t3999_MIs[] =
{
	&m26701_MI,
	NULL
};
extern TypeInfo t136_TI;
extern TypeInfo t324_TI;
static TypeInfo* t3999_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3999_0_0_0;
extern Il2CppType t3999_1_0_0;
struct t3999;
extern Il2CppGenericClass t3999_GC;
TypeInfo t3999_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t3999_MIs, t3999_PIs, NULL, NULL, NULL, NULL, NULL, &t3999_TI, t3999_ITIs, NULL, &EmptyCustomAttributesCache, &t3999_TI, &t3999_0_0_0, &t3999_1_0_0, NULL, &t3999_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2193.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2193_TI;
#include "t2193MD.h"

#include "t29.h"
#include "t44.h"
#include "t7.h"
#include "t914.h"
#include "t21.h"
#include "t40.h"
extern TypeInfo t1915_TI;
extern TypeInfo t914_TI;
#include "t914MD.h"
#include "t20MD.h"
extern MethodInfo m10737_MI;
extern MethodInfo m3964_MI;
extern MethodInfo m3969_MI;
extern MethodInfo m19846_MI;
struct t20;
#include "t915.h"
struct t20;
 t29 * m19490_gshared (t20 * __this, int32_t p0, MethodInfo* method);
#define m19490(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
#define m19846(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MemberInfo>
extern Il2CppType t20_0_0_1;
FieldInfo t2193_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2193_TI, offsetof(t2193, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2193_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2193_TI, offsetof(t2193, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2193_FIs[] =
{
	&t2193_f0_FieldInfo,
	&t2193_f1_FieldInfo,
	NULL
};
extern MethodInfo m10734_MI;
static PropertyInfo t2193____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2193_TI, "System.Collections.IEnumerator.Current", &m10734_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2193____Current_PropertyInfo = 
{
	&t2193_TI, "Current", &m10737_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2193_PIs[] =
{
	&t2193____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2193____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
static ParameterInfo t2193_m10733_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10733_GM;
MethodInfo m10733_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2193_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2193_m10733_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10733_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10734_GM;
MethodInfo m10734_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2193_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10734_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10735_GM;
MethodInfo m10735_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2193_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10735_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10736_GM;
MethodInfo m10736_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2193_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10736_GM};
extern Il2CppType t1915_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10737_GM;
MethodInfo m10737_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2193_TI, &t1915_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10737_GM};
static MethodInfo* t2193_MIs[] =
{
	&m10733_MI,
	&m10734_MI,
	&m10735_MI,
	&m10736_MI,
	&m10737_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
extern MethodInfo m10736_MI;
extern MethodInfo m10735_MI;
static MethodInfo* t2193_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10734_MI,
	&m10736_MI,
	&m10735_MI,
	&m10737_MI,
};
static TypeInfo* t2193_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3999_TI,
};
static Il2CppInterfaceOffsetPair t2193_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3999_TI, 7},
};
extern TypeInfo t1915_TI;
static Il2CppRGCTXData t2193_RGCTXData[3] = 
{
	&m10737_MI/* Method Usage */,
	&t1915_TI/* Class Usage */,
	&m19846_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2193_0_0_0;
extern Il2CppType t2193_1_0_0;
extern TypeInfo t110_TI;
extern Il2CppGenericClass t2193_GC;
extern TypeInfo t20_TI;
TypeInfo t2193_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2193_MIs, t2193_PIs, t2193_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2193_TI, t2193_ITIs, t2193_VT, &EmptyCustomAttributesCache, &t2193_TI, &t2193_0_0_0, &t2193_1_0_0, t2193_IOs, &t2193_GC, NULL, NULL, NULL, t2193_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2193)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5138_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices._MemberInfo>
extern MethodInfo m26702_MI;
extern MethodInfo m26703_MI;
static PropertyInfo t5138____Item_PropertyInfo = 
{
	&t5138_TI, "Item", &m26702_MI, &m26703_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5138_PIs[] =
{
	&t5138____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1915_0_0_0;
extern Il2CppType t1915_0_0_0;
static ParameterInfo t5138_m26704_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1915_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26704_GM;
MethodInfo m26704_MI = 
{
	"IndexOf", NULL, &t5138_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5138_m26704_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26704_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1915_0_0_0;
static ParameterInfo t5138_m26705_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1915_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26705_GM;
MethodInfo m26705_MI = 
{
	"Insert", NULL, &t5138_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5138_m26705_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26705_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5138_m26706_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26706_GM;
MethodInfo m26706_MI = 
{
	"RemoveAt", NULL, &t5138_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5138_m26706_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26706_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5138_m26702_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1915_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26702_GM;
MethodInfo m26702_MI = 
{
	"get_Item", NULL, &t5138_TI, &t1915_0_0_0, RuntimeInvoker_t29_t44, t5138_m26702_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26702_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1915_0_0_0;
static ParameterInfo t5138_m26703_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1915_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26703_GM;
MethodInfo m26703_MI = 
{
	"set_Item", NULL, &t5138_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5138_m26703_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26703_GM};
static MethodInfo* t5138_MIs[] =
{
	&m26704_MI,
	&m26705_MI,
	&m26706_MI,
	&m26702_MI,
	&m26703_MI,
	NULL
};
extern TypeInfo t5137_TI;
static TypeInfo* t5138_ITIs[] = 
{
	&t603_TI,
	&t5137_TI,
	&t5139_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5138_0_0_0;
extern Il2CppType t5138_1_0_0;
struct t5138;
extern Il2CppGenericClass t5138_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5138_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5138_MIs, t5138_PIs, NULL, NULL, NULL, NULL, NULL, &t5138_TI, t5138_ITIs, NULL, &t1908__CustomAttributeCache, &t5138_TI, &t5138_0_0_0, &t5138_1_0_0, NULL, &t5138_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2194.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2194_TI;
#include "t2194MD.h"

extern TypeInfo t29_TI;
extern TypeInfo t44_TI;
extern TypeInfo t40_TI;
#include "t2001MD.h"
#include "t29MD.h"
extern MethodInfo m10703_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m1321_MI;


extern MethodInfo m10738_MI;
 void m10738_gshared (t2194 * __this, MethodInfo* method)
{
	{
		(( void (*) (t2001 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(__this, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return;
	}
}
extern MethodInfo m10739_MI;
 int32_t m10739_gshared (t2194 * __this, t29 * p0, MethodInfo* method)
{
	{
		t29 * L_0 = p0;
		if (((t29 *)L_0))
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}

IL_000a:
	{
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1322_MI, (*(&p0)));
		return L_1;
	}
}
extern MethodInfo m10740_MI;
 bool m10740_gshared (t2194 * __this, t29 * p0, t29 * p1, MethodInfo* method)
{
	{
		t29 * L_0 = p0;
		if (((t29 *)L_0))
		{
			goto IL_0012;
		}
	}
	{
		t29 * L_1 = p1;
		return ((((t29 *)((t29 *)L_1)) == ((t29 *)NULL))? 1 : 0);
	}

IL_0012:
	{
		t29 * L_2 = p1;
		bool L_3 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, (*(&p0)), ((t29 *)L_2));
		return L_3;
	}
}
// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Object>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10738_GM;
MethodInfo m10738_MI = 
{
	".ctor", (methodPointerType)&m10738_gshared, &t2194_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10738_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2194_m10739_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10739_GM;
MethodInfo m10739_MI = 
{
	"GetHashCode", (methodPointerType)&m10739_gshared, &t2194_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2194_m10739_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10739_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2194_m10740_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10740_GM;
MethodInfo m10740_MI = 
{
	"Equals", (methodPointerType)&m10740_gshared, &t2194_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2194_m10740_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10740_GM};
static MethodInfo* t2194_MIs[] =
{
	&m10738_MI,
	&m10739_MI,
	&m10740_MI,
	NULL
};
extern MethodInfo m1332_MI;
extern MethodInfo m10706_MI;
extern MethodInfo m10705_MI;
static MethodInfo* t2194_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m10740_MI,
	&m10739_MI,
	&m10706_MI,
	&m10705_MI,
	&m10739_MI,
	&m10740_MI,
};
extern TypeInfo t2416_TI;
extern TypeInfo t734_TI;
static Il2CppInterfaceOffsetPair t2194_IOs[] = 
{
	{ &t2416_TI, 4},
	{ &t734_TI, 6},
};
extern Il2CppType t6627_0_0_0;
extern TypeInfo t2001_TI;
extern TypeInfo t2001_TI;
extern TypeInfo t2194_TI;
extern TypeInfo t29_TI;
extern MethodInfo m26619_MI;
extern MethodInfo m19789_MI;
extern TypeInfo t29_TI;
static Il2CppRGCTXData t2194_RGCTXData[11] = 
{
	&t6627_0_0_0/* Type Usage */,
	&t29_0_0_0/* Type Usage */,
	&t2001_TI/* Class Usage */,
	&t2001_TI/* Static Usage */,
	&t2194_TI/* Class Usage */,
	&m10738_MI/* Method Usage */,
	&t29_TI/* Class Usage */,
	&m26619_MI/* Method Usage */,
	&m19789_MI/* Method Usage */,
	&m10703_MI/* Method Usage */,
	&t29_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2194_0_0_0;
extern Il2CppType t2194_1_0_0;
extern TypeInfo t2001_TI;
struct t2194;
extern Il2CppGenericClass t2194_GC;
extern TypeInfo t1256_TI;
TypeInfo t2194_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2194_MIs, NULL, NULL, NULL, &t2001_TI, NULL, &t1256_TI, &t2194_TI, NULL, t2194_VT, &EmptyCustomAttributesCache, &t2194_TI, &t2194_0_0_0, &t2194_1_0_0, t2194_IOs, &t2194_GC, NULL, NULL, NULL, t2194_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2194), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#include "t2180.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2180_TI;
#include "t2180MD.h"

#include "t35.h"
#include "t67.h"


extern MethodInfo m10741_MI;
 void m10741_gshared (t2180 * __this, t29 * p0, t35 p1, MethodInfo* method)
{
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m10742_MI;
 bool m10742_gshared (t2180 * __this, t29 * p0, MethodInfo* method)
{
	if(__this->f9 != NULL)
	{
		m10742((t2180 *)__this->f9,p0, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (t29 *, t29 * __this, t29 * p0, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (t29 * __this, t29 * p0, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef bool (*FunctionPointerType) (t29 * __this, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m10743_MI;
 t29 * m10743_gshared (t2180 * __this, t29 * p0, t67 * p1, t29 * p2, MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = p0;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p1, (Il2CppObject*)p2);
}
extern MethodInfo m10744_MI;
 bool m10744_gshared (t2180 * __this, t29 * p0, MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Predicate`1<System.Object>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2180_m10741_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10741_GM;
MethodInfo m10741_MI = 
{
	".ctor", (methodPointerType)&m10741_gshared, &t2180_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2180_m10741_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10741_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2180_m10742_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10742_GM;
MethodInfo m10742_MI = 
{
	"Invoke", (methodPointerType)&m10742_gshared, &t2180_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2180_m10742_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10742_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2180_m10743_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10743_GM;
MethodInfo m10743_MI = 
{
	"BeginInvoke", (methodPointerType)&m10743_gshared, &t2180_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2180_m10743_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m10743_GM};
extern Il2CppType t66_0_0_0;
extern Il2CppType t66_0_0_0;
static ParameterInfo t2180_m10744_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10744_GM;
MethodInfo m10744_MI = 
{
	"EndInvoke", (methodPointerType)&m10744_gshared, &t2180_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2180_m10744_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10744_GM};
static MethodInfo* t2180_MIs[] =
{
	&m10741_MI,
	&m10742_MI,
	&m10743_MI,
	&m10744_MI,
	NULL
};
extern MethodInfo m1699_MI;
extern MethodInfo m1700_MI;
extern MethodInfo m1701_MI;
extern MethodInfo m1702_MI;
extern MethodInfo m1703_MI;
extern MethodInfo m1704_MI;
extern MethodInfo m1705_MI;
static MethodInfo* t2180_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m10742_MI,
	&m10743_MI,
	&m10744_MI,
};
extern TypeInfo t373_TI;
extern TypeInfo t374_TI;
static Il2CppInterfaceOffsetPair t2180_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2180_0_0_0;
extern Il2CppType t2180_1_0_0;
extern TypeInfo t195_TI;
struct t2180;
extern Il2CppGenericClass t2180_GC;
TypeInfo t2180_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Predicate`1", "System", t2180_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2180_TI, NULL, t2180_VT, &EmptyCustomAttributesCache, &t2180_TI, &t2180_0_0_0, &t2180_1_0_0, t2180_IOs, &t2180_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2180), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t2195.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2195_TI;
#include "t2195MD.h"

#include "t42.h"
#include "t43.h"
#include "t1247.h"
#include "mscorlib_ArrayTypes.h"
#include "t2196.h"
#include "t305.h"
extern TypeInfo t4014_TI;
extern TypeInfo t42_TI;
extern TypeInfo t1247_TI;
extern TypeInfo t537_TI;
extern TypeInfo t2196_TI;
extern TypeInfo t305_TI;
#include "t42MD.h"
#include "t931MD.h"
#include "t2196MD.h"
#include "t305MD.h"
extern Il2CppType t4014_0_0_0;
extern Il2CppType t1247_0_0_0;
extern MethodInfo m1331_MI;
extern MethodInfo m1554_MI;
extern MethodInfo m2981_MI;
extern MethodInfo m2979_MI;
extern MethodInfo m8828_MI;
extern MethodInfo m10749_MI;
extern MethodInfo m26707_MI;
extern MethodInfo m8852_MI;


extern MethodInfo m10745_MI;
 void m10745_gshared (t2195 * __this, MethodInfo* method)
{
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m10746_MI;
 void m10746_gshared (t29 * __this, MethodInfo* method)
{
	t2196 * L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer = (t2196 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
	(( void (*) (t2196 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
	((t2195_SFs*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->f0 = L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer;
}
extern MethodInfo m10747_MI;
 int32_t m10747_gshared (t2195 * __this, t29 * p0, t29 * p1, MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	{
		if (p0)
		{
			goto IL_000b;
		}
	}
	{
		if (p1)
		{
			goto IL_0009;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_000a;
	}

IL_0009:
	{
		G_B4_0 = (-1);
	}

IL_000a:
	{
		return G_B4_0;
	}

IL_000b:
	{
		if (p1)
		{
			goto IL_0010;
		}
	}
	{
		return 1;
	}

IL_0010:
	{
		if (!((t29 *)IsInst(p0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))
		{
			goto IL_0033;
		}
	}
	{
		if (!((t29 *)IsInst(p1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker2< int32_t, t29 *, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), __this, ((t29 *)Castclass(p0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))), ((t29 *)Castclass(p1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))));
		return L_0;
	}

IL_0033:
	{
		t305 * L_1 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m8852(L_1, &m8852_MI);
		il2cpp_codegen_raise_exception(L_1);
	}
}
extern MethodInfo m10748_MI;
 t2195 * m10748_gshared (t29 * __this, MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return (((t2195_SFs*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->f0);
	}
}
// Metadata Definition System.Collections.Generic.Comparer`1<System.Object>
extern Il2CppType t2195_0_0_49;
FieldInfo t2195_f0_FieldInfo = 
{
	"_default", &t2195_0_0_49, &t2195_TI, offsetof(t2195_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2195_FIs[] =
{
	&t2195_f0_FieldInfo,
	NULL
};
static PropertyInfo t2195____Default_PropertyInfo = 
{
	&t2195_TI, "Default", &m10748_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2195_PIs[] =
{
	&t2195____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10745_GM;
MethodInfo m10745_MI = 
{
	".ctor", (methodPointerType)&m10745_gshared, &t2195_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10745_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10746_GM;
MethodInfo m10746_MI = 
{
	".cctor", (methodPointerType)&m10746_gshared, &t2195_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10746_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2195_m10747_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10747_GM;
MethodInfo m10747_MI = 
{
	"System.Collections.IComparer.Compare", (methodPointerType)&m10747_gshared, &t2195_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2195_m10747_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10747_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2195_m26707_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26707_GM;
MethodInfo m26707_MI = 
{
	"Compare", NULL, &t2195_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2195_m26707_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26707_GM};
extern Il2CppType t2195_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10748_GM;
MethodInfo m10748_MI = 
{
	"get_Default", (methodPointerType)&m10748_gshared, &t2195_TI, &t2195_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10748_GM};
static MethodInfo* t2195_MIs[] =
{
	&m10745_MI,
	&m10746_MI,
	&m10747_MI,
	&m26707_MI,
	&m10748_MI,
	NULL
};
static MethodInfo* t2195_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m26707_MI,
	&m10747_MI,
	NULL,
};
extern TypeInfo t4000_TI;
extern TypeInfo t726_TI;
static TypeInfo* t2195_ITIs[] = 
{
	&t4000_TI,
	&t726_TI,
};
static Il2CppInterfaceOffsetPair t2195_IOs[] = 
{
	{ &t4000_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t2195_TI;
extern TypeInfo t2195_TI;
extern TypeInfo t2196_TI;
extern TypeInfo t29_TI;
static Il2CppRGCTXData t2195_RGCTXData[8] = 
{
	&t4014_0_0_0/* Type Usage */,
	&t29_0_0_0/* Type Usage */,
	&t2195_TI/* Class Usage */,
	&t2195_TI/* Static Usage */,
	&t2196_TI/* Class Usage */,
	&m10749_MI/* Method Usage */,
	&t29_TI/* Class Usage */,
	&m26707_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2195_0_0_0;
extern Il2CppType t2195_1_0_0;
struct t2195;
extern Il2CppGenericClass t2195_GC;
TypeInfo t2195_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparer`1", "System.Collections.Generic", t2195_MIs, t2195_PIs, t2195_FIs, NULL, &t29_TI, NULL, NULL, &t2195_TI, t2195_ITIs, t2195_VT, &EmptyCustomAttributesCache, &t2195_TI, &t2195_0_0_0, &t2195_1_0_0, t2195_IOs, &t2195_GC, NULL, NULL, NULL, t2195_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2195), 0, -1, sizeof(t2195_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 5, 1, 1, 0, 0, 7, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IComparer`1<System.Object>
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t4000_m19961_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19961_GM;
MethodInfo m19961_MI = 
{
	"Compare", NULL, &t4000_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t4000_m19961_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19961_GM};
static MethodInfo* t4000_MIs[] =
{
	&m19961_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4000_0_0_0;
extern Il2CppType t4000_1_0_0;
struct t4000;
extern Il2CppGenericClass t4000_GC;
TypeInfo t4000_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparer`1", "System.Collections.Generic", t4000_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4000_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4000_TI, &t4000_0_0_0, &t4000_1_0_0, NULL, &t4000_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IComparable`1<System.Object>
extern Il2CppType t29_0_0_0;
static ParameterInfo t4014_m19962_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19962_GM;
MethodInfo m19962_MI = 
{
	"CompareTo", NULL, &t4014_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t4014_m19962_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19962_GM};
static MethodInfo* t4014_MIs[] =
{
	&m19962_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4014_1_0_0;
struct t4014;
extern Il2CppGenericClass t4014_GC;
TypeInfo t4014_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t4014_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4014_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4014_TI, &t4014_0_0_0, &t4014_1_0_0, NULL, &t4014_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo t290_TI;
extern MethodInfo m19962_MI;
extern MethodInfo m9672_MI;
extern MethodInfo m1935_MI;


 void m10749_gshared (t2196 * __this, MethodInfo* method)
{
	{
		(( void (*) (t2195 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(__this, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
extern MethodInfo m10750_MI;
 int32_t m10750_gshared (t2196 * __this, t29 * p0, t29 * p1, MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	{
		t29 * L_0 = p0;
		if (((t29 *)L_0))
		{
			goto IL_0015;
		}
	}
	{
		t29 * L_1 = p1;
		if (((t29 *)L_1))
		{
			goto IL_0013;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0014;
	}

IL_0013:
	{
		G_B4_0 = (-1);
	}

IL_0014:
	{
		return G_B4_0;
	}

IL_0015:
	{
		t29 * L_2 = p1;
		if (((t29 *)L_2))
		{
			goto IL_001f;
		}
	}
	{
		return 1;
	}

IL_001f:
	{
		t29 * L_3 = p0;
		if (!((t29*)IsInst(((t29 *)L_3), IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10))))
		{
			goto IL_003e;
		}
	}
	{
		t29 * L_4 = p0;
		int32_t L_5 = (int32_t)InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11), ((t29*)Castclass(((t29 *)L_4), IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10))), p1);
		return L_5;
	}

IL_003e:
	{
		t29 * L_6 = p0;
		if (!((t29 *)IsInst(((t29 *)L_6), InitializedTypeInfo(&t290_TI))))
		{
			goto IL_0062;
		}
	}
	{
		t29 * L_7 = p0;
		t29 * L_8 = p1;
		int32_t L_9 = (int32_t)InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(&m9672_MI, ((t29 *)Castclass(((t29 *)L_7), InitializedTypeInfo(&t290_TI))), ((t29 *)L_8));
		return L_9;
	}

IL_0062:
	{
		t305 * L_10 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_10, (t7*) &_stringLiteral1162, &m1935_MI);
		il2cpp_codegen_raise_exception(L_10);
	}
}
// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer<System.Object>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10749_GM;
MethodInfo m10749_MI = 
{
	".ctor", (methodPointerType)&m10749_gshared, &t2196_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10749_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2196_m10750_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10750_GM;
MethodInfo m10750_MI = 
{
	"Compare", (methodPointerType)&m10750_gshared, &t2196_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2196_m10750_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10750_GM};
static MethodInfo* t2196_MIs[] =
{
	&m10749_MI,
	&m10750_MI,
	NULL
};
static MethodInfo* t2196_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m10750_MI,
	&m10747_MI,
	&m10750_MI,
};
static Il2CppInterfaceOffsetPair t2196_IOs[] = 
{
	{ &t4000_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t2195_TI;
extern TypeInfo t2195_TI;
extern TypeInfo t2196_TI;
extern TypeInfo t29_TI;
extern TypeInfo t29_TI;
extern TypeInfo t4014_TI;
static Il2CppRGCTXData t2196_RGCTXData[12] = 
{
	&t4014_0_0_0/* Type Usage */,
	&t29_0_0_0/* Type Usage */,
	&t2195_TI/* Class Usage */,
	&t2195_TI/* Static Usage */,
	&t2196_TI/* Class Usage */,
	&m10749_MI/* Method Usage */,
	&t29_TI/* Class Usage */,
	&m26707_MI/* Method Usage */,
	&m10745_MI/* Method Usage */,
	&t29_TI/* Class Usage */,
	&t4014_TI/* Class Usage */,
	&m19962_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2196_0_0_0;
extern Il2CppType t2196_1_0_0;
struct t2196;
extern Il2CppGenericClass t2196_GC;
extern TypeInfo t1246_TI;
TypeInfo t2196_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2196_MIs, NULL, NULL, NULL, &t2195_TI, NULL, &t1246_TI, &t2196_TI, NULL, t2196_VT, &EmptyCustomAttributesCache, &t2196_TI, &t2196_0_0_0, &t2196_1_0_0, t2196_IOs, &t2196_GC, NULL, NULL, NULL, t2196_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2196), 0, -1, 0, 0, -1, 1048835, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 7, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2424_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Int32>
extern MethodInfo m26708_MI;
static PropertyInfo t2424____Current_PropertyInfo = 
{
	&t2424_TI, "Current", &m26708_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2424_PIs[] =
{
	&t2424____Current_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26708_GM;
MethodInfo m26708_MI = 
{
	"get_Current", NULL, &t2424_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26708_GM};
static MethodInfo* t2424_MIs[] =
{
	&m26708_MI,
	NULL
};
static TypeInfo* t2424_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2424_0_0_0;
extern Il2CppType t2424_1_0_0;
struct t2424;
extern Il2CppGenericClass t2424_GC;
TypeInfo t2424_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2424_MIs, t2424_PIs, NULL, NULL, NULL, NULL, NULL, &t2424_TI, t2424_ITIs, NULL, &EmptyCustomAttributesCache, &t2424_TI, &t2424_0_0_0, &t2424_1_0_0, NULL, &t2424_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2197.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2197_TI;
#include "t2197MD.h"

extern MethodInfo m10755_MI;
extern MethodInfo m19861_MI;
struct t20;
 int32_t m19861 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m10751_MI;
 void m10751 (t2197 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m10752_MI;
 t29 * m10752 (t2197 * __this, MethodInfo* method){
	{
		int32_t L_0 = m10755(__this, &m10755_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t44_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m10753_MI;
 void m10753 (t2197 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m10754_MI;
 bool m10754 (t2197 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m10755 (t2197 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m19861(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m19861_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Int32>
extern Il2CppType t20_0_0_1;
FieldInfo t2197_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2197_TI, offsetof(t2197, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2197_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2197_TI, offsetof(t2197, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2197_FIs[] =
{
	&t2197_f0_FieldInfo,
	&t2197_f1_FieldInfo,
	NULL
};
static PropertyInfo t2197____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2197_TI, "System.Collections.IEnumerator.Current", &m10752_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2197____Current_PropertyInfo = 
{
	&t2197_TI, "Current", &m10755_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2197_PIs[] =
{
	&t2197____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2197____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2197_m10751_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10751_GM;
MethodInfo m10751_MI = 
{
	".ctor", (methodPointerType)&m10751, &t2197_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2197_m10751_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10751_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10752_GM;
MethodInfo m10752_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10752, &t2197_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10752_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10753_GM;
MethodInfo m10753_MI = 
{
	"Dispose", (methodPointerType)&m10753, &t2197_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10753_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10754_GM;
MethodInfo m10754_MI = 
{
	"MoveNext", (methodPointerType)&m10754, &t2197_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10754_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10755_GM;
MethodInfo m10755_MI = 
{
	"get_Current", (methodPointerType)&m10755, &t2197_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10755_GM};
static MethodInfo* t2197_MIs[] =
{
	&m10751_MI,
	&m10752_MI,
	&m10753_MI,
	&m10754_MI,
	&m10755_MI,
	NULL
};
static MethodInfo* t2197_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10752_MI,
	&m10754_MI,
	&m10753_MI,
	&m10755_MI,
};
static TypeInfo* t2197_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2424_TI,
};
static Il2CppInterfaceOffsetPair t2197_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2424_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2197_0_0_0;
extern Il2CppType t2197_1_0_0;
extern Il2CppGenericClass t2197_GC;
TypeInfo t2197_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2197_MIs, t2197_PIs, t2197_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2197_TI, t2197_ITIs, t2197_VT, &EmptyCustomAttributesCache, &t2197_TI, &t2197_0_0_0, &t2197_1_0_0, t2197_IOs, &t2197_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2197)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5140_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Int32>
extern MethodInfo m26709_MI;
static PropertyInfo t5140____Count_PropertyInfo = 
{
	&t5140_TI, "Count", &m26709_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26710_MI;
static PropertyInfo t5140____IsReadOnly_PropertyInfo = 
{
	&t5140_TI, "IsReadOnly", &m26710_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5140_PIs[] =
{
	&t5140____Count_PropertyInfo,
	&t5140____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26709_GM;
MethodInfo m26709_MI = 
{
	"get_Count", NULL, &t5140_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26709_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26710_GM;
MethodInfo m26710_MI = 
{
	"get_IsReadOnly", NULL, &t5140_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26710_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5140_m26711_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26711_GM;
MethodInfo m26711_MI = 
{
	"Add", NULL, &t5140_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5140_m26711_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26711_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26712_GM;
MethodInfo m26712_MI = 
{
	"Clear", NULL, &t5140_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26712_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5140_m26713_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26713_GM;
MethodInfo m26713_MI = 
{
	"Contains", NULL, &t5140_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5140_m26713_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26713_GM};
extern Il2CppType t841_0_0_0;
extern Il2CppType t841_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5140_m26714_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t841_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26714_GM;
MethodInfo m26714_MI = 
{
	"CopyTo", NULL, &t5140_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5140_m26714_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26714_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5140_m26715_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26715_GM;
MethodInfo m26715_MI = 
{
	"Remove", NULL, &t5140_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5140_m26715_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26715_GM};
static MethodInfo* t5140_MIs[] =
{
	&m26709_MI,
	&m26710_MI,
	&m26711_MI,
	&m26712_MI,
	&m26713_MI,
	&m26714_MI,
	&m26715_MI,
	NULL
};
extern TypeInfo t5142_TI;
static TypeInfo* t5140_ITIs[] = 
{
	&t603_TI,
	&t5142_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5140_0_0_0;
extern Il2CppType t5140_1_0_0;
struct t5140;
extern Il2CppGenericClass t5140_GC;
TypeInfo t5140_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5140_MIs, t5140_PIs, NULL, NULL, NULL, NULL, NULL, &t5140_TI, t5140_ITIs, NULL, &EmptyCustomAttributesCache, &t5140_TI, &t5140_0_0_0, &t5140_1_0_0, NULL, &t5140_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Int32>
extern Il2CppType t2424_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26716_GM;
MethodInfo m26716_MI = 
{
	"GetEnumerator", NULL, &t5142_TI, &t2424_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26716_GM};
static MethodInfo* t5142_MIs[] =
{
	&m26716_MI,
	NULL
};
static TypeInfo* t5142_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5142_0_0_0;
extern Il2CppType t5142_1_0_0;
struct t5142;
extern Il2CppGenericClass t5142_GC;
TypeInfo t5142_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5142_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5142_TI, t5142_ITIs, NULL, &EmptyCustomAttributesCache, &t5142_TI, &t5142_0_0_0, &t5142_1_0_0, NULL, &t5142_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5141_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Int32>
extern MethodInfo m26717_MI;
extern MethodInfo m26718_MI;
static PropertyInfo t5141____Item_PropertyInfo = 
{
	&t5141_TI, "Item", &m26717_MI, &m26718_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5141_PIs[] =
{
	&t5141____Item_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5141_m26719_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26719_GM;
MethodInfo m26719_MI = 
{
	"IndexOf", NULL, &t5141_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5141_m26719_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26719_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5141_m26720_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26720_GM;
MethodInfo m26720_MI = 
{
	"Insert", NULL, &t5141_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5141_m26720_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26720_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5141_m26721_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26721_GM;
MethodInfo m26721_MI = 
{
	"RemoveAt", NULL, &t5141_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5141_m26721_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26721_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5141_m26717_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26717_GM;
MethodInfo m26717_MI = 
{
	"get_Item", NULL, &t5141_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5141_m26717_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26717_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5141_m26718_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26718_GM;
MethodInfo m26718_MI = 
{
	"set_Item", NULL, &t5141_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5141_m26718_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26718_GM};
static MethodInfo* t5141_MIs[] =
{
	&m26719_MI,
	&m26720_MI,
	&m26721_MI,
	&m26717_MI,
	&m26718_MI,
	NULL
};
static TypeInfo* t5141_ITIs[] = 
{
	&t603_TI,
	&t5140_TI,
	&t5142_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5141_0_0_0;
extern Il2CppType t5141_1_0_0;
struct t5141;
extern Il2CppGenericClass t5141_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5141_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5141_MIs, t5141_PIs, NULL, NULL, NULL, NULL, NULL, &t5141_TI, t5141_ITIs, NULL, &t1908__CustomAttributeCache, &t5141_TI, &t5141_0_0_0, &t5141_1_0_0, NULL, &t5141_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5143_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IComparable`1<System.Int32>>
extern MethodInfo m26722_MI;
static PropertyInfo t5143____Count_PropertyInfo = 
{
	&t5143_TI, "Count", &m26722_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26723_MI;
static PropertyInfo t5143____IsReadOnly_PropertyInfo = 
{
	&t5143_TI, "IsReadOnly", &m26723_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5143_PIs[] =
{
	&t5143____Count_PropertyInfo,
	&t5143____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26722_GM;
MethodInfo m26722_MI = 
{
	"get_Count", NULL, &t5143_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26722_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26723_GM;
MethodInfo m26723_MI = 
{
	"get_IsReadOnly", NULL, &t5143_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26723_GM};
extern Il2CppType t1706_0_0_0;
extern Il2CppType t1706_0_0_0;
static ParameterInfo t5143_m26724_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1706_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26724_GM;
MethodInfo m26724_MI = 
{
	"Add", NULL, &t5143_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5143_m26724_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26724_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26725_GM;
MethodInfo m26725_MI = 
{
	"Clear", NULL, &t5143_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26725_GM};
extern Il2CppType t1706_0_0_0;
static ParameterInfo t5143_m26726_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1706_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26726_GM;
MethodInfo m26726_MI = 
{
	"Contains", NULL, &t5143_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5143_m26726_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26726_GM};
extern Il2CppType t3529_0_0_0;
extern Il2CppType t3529_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5143_m26727_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3529_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26727_GM;
MethodInfo m26727_MI = 
{
	"CopyTo", NULL, &t5143_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5143_m26727_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26727_GM};
extern Il2CppType t1706_0_0_0;
static ParameterInfo t5143_m26728_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1706_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26728_GM;
MethodInfo m26728_MI = 
{
	"Remove", NULL, &t5143_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5143_m26728_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26728_GM};
static MethodInfo* t5143_MIs[] =
{
	&m26722_MI,
	&m26723_MI,
	&m26724_MI,
	&m26725_MI,
	&m26726_MI,
	&m26727_MI,
	&m26728_MI,
	NULL
};
extern TypeInfo t5145_TI;
static TypeInfo* t5143_ITIs[] = 
{
	&t603_TI,
	&t5145_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5143_0_0_0;
extern Il2CppType t5143_1_0_0;
struct t5143;
extern Il2CppGenericClass t5143_GC;
TypeInfo t5143_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5143_MIs, t5143_PIs, NULL, NULL, NULL, NULL, NULL, &t5143_TI, t5143_ITIs, NULL, &EmptyCustomAttributesCache, &t5143_TI, &t5143_0_0_0, &t5143_1_0_0, NULL, &t5143_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.Int32>>
extern Il2CppType t4002_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26729_GM;
MethodInfo m26729_MI = 
{
	"GetEnumerator", NULL, &t5145_TI, &t4002_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26729_GM};
static MethodInfo* t5145_MIs[] =
{
	&m26729_MI,
	NULL
};
static TypeInfo* t5145_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5145_0_0_0;
extern Il2CppType t5145_1_0_0;
struct t5145;
extern Il2CppGenericClass t5145_GC;
TypeInfo t5145_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5145_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5145_TI, t5145_ITIs, NULL, &EmptyCustomAttributesCache, &t5145_TI, &t5145_0_0_0, &t5145_1_0_0, NULL, &t5145_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4002_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.Int32>>
extern MethodInfo m26730_MI;
static PropertyInfo t4002____Current_PropertyInfo = 
{
	&t4002_TI, "Current", &m26730_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4002_PIs[] =
{
	&t4002____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1706_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26730_GM;
MethodInfo m26730_MI = 
{
	"get_Current", NULL, &t4002_TI, &t1706_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26730_GM};
static MethodInfo* t4002_MIs[] =
{
	&m26730_MI,
	NULL
};
static TypeInfo* t4002_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4002_0_0_0;
extern Il2CppType t4002_1_0_0;
struct t4002;
extern Il2CppGenericClass t4002_GC;
TypeInfo t4002_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4002_MIs, t4002_PIs, NULL, NULL, NULL, NULL, NULL, &t4002_TI, t4002_ITIs, NULL, &EmptyCustomAttributesCache, &t4002_TI, &t4002_0_0_0, &t4002_1_0_0, NULL, &t4002_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1706_TI;



// Metadata Definition System.IComparable`1<System.Int32>
extern Il2CppType t44_0_0_0;
static ParameterInfo t1706_m26731_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26731_GM;
MethodInfo m26731_MI = 
{
	"CompareTo", NULL, &t1706_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t1706_m26731_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26731_GM};
static MethodInfo* t1706_MIs[] =
{
	&m26731_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1706_1_0_0;
struct t1706;
extern Il2CppGenericClass t1706_GC;
TypeInfo t1706_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t1706_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1706_TI, NULL, NULL, &EmptyCustomAttributesCache, &t1706_TI, &t1706_0_0_0, &t1706_1_0_0, NULL, &t1706_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t2198.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2198_TI;
#include "t2198MD.h"

extern MethodInfo m10760_MI;
extern MethodInfo m19872_MI;
struct t20;
#define m19872(__this, p0, method) (t29*)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.IComparable`1<System.Int32>>
extern Il2CppType t20_0_0_1;
FieldInfo t2198_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2198_TI, offsetof(t2198, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2198_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2198_TI, offsetof(t2198, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2198_FIs[] =
{
	&t2198_f0_FieldInfo,
	&t2198_f1_FieldInfo,
	NULL
};
extern MethodInfo m10757_MI;
static PropertyInfo t2198____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2198_TI, "System.Collections.IEnumerator.Current", &m10757_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2198____Current_PropertyInfo = 
{
	&t2198_TI, "Current", &m10760_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2198_PIs[] =
{
	&t2198____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2198____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2198_m10756_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10756_GM;
MethodInfo m10756_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2198_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2198_m10756_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10756_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10757_GM;
MethodInfo m10757_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2198_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10757_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10758_GM;
MethodInfo m10758_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2198_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10758_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10759_GM;
MethodInfo m10759_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2198_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10759_GM};
extern Il2CppType t1706_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10760_GM;
MethodInfo m10760_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2198_TI, &t1706_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10760_GM};
static MethodInfo* t2198_MIs[] =
{
	&m10756_MI,
	&m10757_MI,
	&m10758_MI,
	&m10759_MI,
	&m10760_MI,
	NULL
};
extern MethodInfo m10759_MI;
extern MethodInfo m10758_MI;
static MethodInfo* t2198_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10757_MI,
	&m10759_MI,
	&m10758_MI,
	&m10760_MI,
};
static TypeInfo* t2198_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4002_TI,
};
static Il2CppInterfaceOffsetPair t2198_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4002_TI, 7},
};
extern TypeInfo t1706_TI;
static Il2CppRGCTXData t2198_RGCTXData[3] = 
{
	&m10760_MI/* Method Usage */,
	&t1706_TI/* Class Usage */,
	&m19872_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2198_0_0_0;
extern Il2CppType t2198_1_0_0;
extern Il2CppGenericClass t2198_GC;
TypeInfo t2198_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2198_MIs, t2198_PIs, t2198_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2198_TI, t2198_ITIs, t2198_VT, &EmptyCustomAttributesCache, &t2198_TI, &t2198_0_0_0, &t2198_1_0_0, t2198_IOs, &t2198_GC, NULL, NULL, NULL, t2198_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2198)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5144_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IComparable`1<System.Int32>>
extern MethodInfo m26732_MI;
extern MethodInfo m26733_MI;
static PropertyInfo t5144____Item_PropertyInfo = 
{
	&t5144_TI, "Item", &m26732_MI, &m26733_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5144_PIs[] =
{
	&t5144____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1706_0_0_0;
static ParameterInfo t5144_m26734_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1706_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26734_GM;
MethodInfo m26734_MI = 
{
	"IndexOf", NULL, &t5144_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5144_m26734_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26734_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1706_0_0_0;
static ParameterInfo t5144_m26735_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1706_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26735_GM;
MethodInfo m26735_MI = 
{
	"Insert", NULL, &t5144_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5144_m26735_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26735_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5144_m26736_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26736_GM;
MethodInfo m26736_MI = 
{
	"RemoveAt", NULL, &t5144_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5144_m26736_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26736_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5144_m26732_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1706_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26732_GM;
MethodInfo m26732_MI = 
{
	"get_Item", NULL, &t5144_TI, &t1706_0_0_0, RuntimeInvoker_t29_t44, t5144_m26732_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26732_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1706_0_0_0;
static ParameterInfo t5144_m26733_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1706_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26733_GM;
MethodInfo m26733_MI = 
{
	"set_Item", NULL, &t5144_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5144_m26733_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26733_GM};
static MethodInfo* t5144_MIs[] =
{
	&m26734_MI,
	&m26735_MI,
	&m26736_MI,
	&m26732_MI,
	&m26733_MI,
	NULL
};
static TypeInfo* t5144_ITIs[] = 
{
	&t603_TI,
	&t5143_TI,
	&t5145_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5144_0_0_0;
extern Il2CppType t5144_1_0_0;
struct t5144;
extern Il2CppGenericClass t5144_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5144_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5144_MIs, t5144_PIs, NULL, NULL, NULL, NULL, NULL, &t5144_TI, t5144_ITIs, NULL, &t1908__CustomAttributeCache, &t5144_TI, &t5144_0_0_0, &t5144_1_0_0, NULL, &t5144_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5146_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Int32>>
extern MethodInfo m26737_MI;
static PropertyInfo t5146____Count_PropertyInfo = 
{
	&t5146_TI, "Count", &m26737_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26738_MI;
static PropertyInfo t5146____IsReadOnly_PropertyInfo = 
{
	&t5146_TI, "IsReadOnly", &m26738_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5146_PIs[] =
{
	&t5146____Count_PropertyInfo,
	&t5146____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26737_GM;
MethodInfo m26737_MI = 
{
	"get_Count", NULL, &t5146_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26737_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26738_GM;
MethodInfo m26738_MI = 
{
	"get_IsReadOnly", NULL, &t5146_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26738_GM};
extern Il2CppType t1707_0_0_0;
extern Il2CppType t1707_0_0_0;
static ParameterInfo t5146_m26739_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1707_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26739_GM;
MethodInfo m26739_MI = 
{
	"Add", NULL, &t5146_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5146_m26739_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26739_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26740_GM;
MethodInfo m26740_MI = 
{
	"Clear", NULL, &t5146_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26740_GM};
extern Il2CppType t1707_0_0_0;
static ParameterInfo t5146_m26741_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1707_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26741_GM;
MethodInfo m26741_MI = 
{
	"Contains", NULL, &t5146_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5146_m26741_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26741_GM};
extern Il2CppType t3530_0_0_0;
extern Il2CppType t3530_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5146_m26742_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3530_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26742_GM;
MethodInfo m26742_MI = 
{
	"CopyTo", NULL, &t5146_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5146_m26742_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26742_GM};
extern Il2CppType t1707_0_0_0;
static ParameterInfo t5146_m26743_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1707_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26743_GM;
MethodInfo m26743_MI = 
{
	"Remove", NULL, &t5146_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5146_m26743_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26743_GM};
static MethodInfo* t5146_MIs[] =
{
	&m26737_MI,
	&m26738_MI,
	&m26739_MI,
	&m26740_MI,
	&m26741_MI,
	&m26742_MI,
	&m26743_MI,
	NULL
};
extern TypeInfo t5148_TI;
static TypeInfo* t5146_ITIs[] = 
{
	&t603_TI,
	&t5148_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5146_0_0_0;
extern Il2CppType t5146_1_0_0;
struct t5146;
extern Il2CppGenericClass t5146_GC;
TypeInfo t5146_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5146_MIs, t5146_PIs, NULL, NULL, NULL, NULL, NULL, &t5146_TI, t5146_ITIs, NULL, &EmptyCustomAttributesCache, &t5146_TI, &t5146_0_0_0, &t5146_1_0_0, NULL, &t5146_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.Int32>>
extern Il2CppType t4004_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26744_GM;
MethodInfo m26744_MI = 
{
	"GetEnumerator", NULL, &t5148_TI, &t4004_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26744_GM};
static MethodInfo* t5148_MIs[] =
{
	&m26744_MI,
	NULL
};
static TypeInfo* t5148_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5148_0_0_0;
extern Il2CppType t5148_1_0_0;
struct t5148;
extern Il2CppGenericClass t5148_GC;
TypeInfo t5148_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5148_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5148_TI, t5148_ITIs, NULL, &EmptyCustomAttributesCache, &t5148_TI, &t5148_0_0_0, &t5148_1_0_0, NULL, &t5148_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4004_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IEquatable`1<System.Int32>>
extern MethodInfo m26745_MI;
static PropertyInfo t4004____Current_PropertyInfo = 
{
	&t4004_TI, "Current", &m26745_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4004_PIs[] =
{
	&t4004____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1707_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26745_GM;
MethodInfo m26745_MI = 
{
	"get_Current", NULL, &t4004_TI, &t1707_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26745_GM};
static MethodInfo* t4004_MIs[] =
{
	&m26745_MI,
	NULL
};
static TypeInfo* t4004_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4004_0_0_0;
extern Il2CppType t4004_1_0_0;
struct t4004;
extern Il2CppGenericClass t4004_GC;
TypeInfo t4004_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4004_MIs, t4004_PIs, NULL, NULL, NULL, NULL, NULL, &t4004_TI, t4004_ITIs, NULL, &EmptyCustomAttributesCache, &t4004_TI, &t4004_0_0_0, &t4004_1_0_0, NULL, &t4004_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1707_TI;



// Metadata Definition System.IEquatable`1<System.Int32>
extern Il2CppType t44_0_0_0;
static ParameterInfo t1707_m26746_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26746_GM;
MethodInfo m26746_MI = 
{
	"Equals", NULL, &t1707_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t1707_m26746_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26746_GM};
static MethodInfo* t1707_MIs[] =
{
	&m26746_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1707_1_0_0;
struct t1707;
extern Il2CppGenericClass t1707_GC;
TypeInfo t1707_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t1707_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1707_TI, NULL, NULL, &EmptyCustomAttributesCache, &t1707_TI, &t1707_0_0_0, &t1707_1_0_0, NULL, &t1707_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t2199.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2199_TI;
#include "t2199MD.h"

extern MethodInfo m10765_MI;
extern MethodInfo m19883_MI;
struct t20;
#define m19883(__this, p0, method) (t29*)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.IEquatable`1<System.Int32>>
extern Il2CppType t20_0_0_1;
FieldInfo t2199_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2199_TI, offsetof(t2199, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2199_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2199_TI, offsetof(t2199, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2199_FIs[] =
{
	&t2199_f0_FieldInfo,
	&t2199_f1_FieldInfo,
	NULL
};
extern MethodInfo m10762_MI;
static PropertyInfo t2199____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2199_TI, "System.Collections.IEnumerator.Current", &m10762_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2199____Current_PropertyInfo = 
{
	&t2199_TI, "Current", &m10765_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2199_PIs[] =
{
	&t2199____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2199____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2199_m10761_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10761_GM;
MethodInfo m10761_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2199_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2199_m10761_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10761_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10762_GM;
MethodInfo m10762_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2199_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10762_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10763_GM;
MethodInfo m10763_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2199_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10763_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10764_GM;
MethodInfo m10764_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2199_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10764_GM};
extern Il2CppType t1707_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10765_GM;
MethodInfo m10765_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2199_TI, &t1707_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10765_GM};
static MethodInfo* t2199_MIs[] =
{
	&m10761_MI,
	&m10762_MI,
	&m10763_MI,
	&m10764_MI,
	&m10765_MI,
	NULL
};
extern MethodInfo m10764_MI;
extern MethodInfo m10763_MI;
static MethodInfo* t2199_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10762_MI,
	&m10764_MI,
	&m10763_MI,
	&m10765_MI,
};
static TypeInfo* t2199_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4004_TI,
};
static Il2CppInterfaceOffsetPair t2199_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4004_TI, 7},
};
extern TypeInfo t1707_TI;
static Il2CppRGCTXData t2199_RGCTXData[3] = 
{
	&m10765_MI/* Method Usage */,
	&t1707_TI/* Class Usage */,
	&m19883_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2199_0_0_0;
extern Il2CppType t2199_1_0_0;
extern Il2CppGenericClass t2199_GC;
TypeInfo t2199_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2199_MIs, t2199_PIs, t2199_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2199_TI, t2199_ITIs, t2199_VT, &EmptyCustomAttributesCache, &t2199_TI, &t2199_0_0_0, &t2199_1_0_0, t2199_IOs, &t2199_GC, NULL, NULL, NULL, t2199_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2199)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5147_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IEquatable`1<System.Int32>>
extern MethodInfo m26747_MI;
extern MethodInfo m26748_MI;
static PropertyInfo t5147____Item_PropertyInfo = 
{
	&t5147_TI, "Item", &m26747_MI, &m26748_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5147_PIs[] =
{
	&t5147____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1707_0_0_0;
static ParameterInfo t5147_m26749_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1707_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26749_GM;
MethodInfo m26749_MI = 
{
	"IndexOf", NULL, &t5147_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5147_m26749_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26749_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1707_0_0_0;
static ParameterInfo t5147_m26750_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1707_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26750_GM;
MethodInfo m26750_MI = 
{
	"Insert", NULL, &t5147_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5147_m26750_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26750_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5147_m26751_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26751_GM;
MethodInfo m26751_MI = 
{
	"RemoveAt", NULL, &t5147_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5147_m26751_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26751_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5147_m26747_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1707_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26747_GM;
MethodInfo m26747_MI = 
{
	"get_Item", NULL, &t5147_TI, &t1707_0_0_0, RuntimeInvoker_t29_t44, t5147_m26747_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26747_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1707_0_0_0;
static ParameterInfo t5147_m26748_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1707_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26748_GM;
MethodInfo m26748_MI = 
{
	"set_Item", NULL, &t5147_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5147_m26748_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26748_GM};
static MethodInfo* t5147_MIs[] =
{
	&m26749_MI,
	&m26750_MI,
	&m26751_MI,
	&m26747_MI,
	&m26748_MI,
	NULL
};
static TypeInfo* t5147_ITIs[] = 
{
	&t603_TI,
	&t5146_TI,
	&t5148_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5147_0_0_0;
extern Il2CppType t5147_1_0_0;
struct t5147;
extern Il2CppGenericClass t5147_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5147_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5147_MIs, t5147_PIs, NULL, NULL, NULL, NULL, NULL, &t5147_TI, t5147_ITIs, NULL, &t1908__CustomAttributeCache, &t5147_TI, &t5147_0_0_0, &t5147_1_0_0, NULL, &t5147_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4005_TI;

#include "t601.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Double>
extern MethodInfo m26752_MI;
static PropertyInfo t4005____Current_PropertyInfo = 
{
	&t4005_TI, "Current", &m26752_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4005_PIs[] =
{
	&t4005____Current_PropertyInfo,
	NULL
};
extern Il2CppType t601_0_0_0;
extern void* RuntimeInvoker_t601 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26752_GM;
MethodInfo m26752_MI = 
{
	"get_Current", NULL, &t4005_TI, &t601_0_0_0, RuntimeInvoker_t601, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26752_GM};
static MethodInfo* t4005_MIs[] =
{
	&m26752_MI,
	NULL
};
static TypeInfo* t4005_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4005_0_0_0;
extern Il2CppType t4005_1_0_0;
struct t4005;
extern Il2CppGenericClass t4005_GC;
TypeInfo t4005_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4005_MIs, t4005_PIs, NULL, NULL, NULL, NULL, NULL, &t4005_TI, t4005_ITIs, NULL, &EmptyCustomAttributesCache, &t4005_TI, &t4005_0_0_0, &t4005_1_0_0, NULL, &t4005_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2200.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2200_TI;
#include "t2200MD.h"

extern TypeInfo t601_TI;
extern MethodInfo m10770_MI;
extern MethodInfo m19894_MI;
struct t20;
 double m19894 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m10766_MI;
 void m10766 (t2200 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m10767_MI;
 t29 * m10767 (t2200 * __this, MethodInfo* method){
	{
		double L_0 = m10770(__this, &m10770_MI);
		double L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t601_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m10768_MI;
 void m10768 (t2200 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m10769_MI;
 bool m10769 (t2200 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 double m10770 (t2200 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		double L_8 = m19894(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m19894_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Double>
extern Il2CppType t20_0_0_1;
FieldInfo t2200_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2200_TI, offsetof(t2200, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2200_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2200_TI, offsetof(t2200, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2200_FIs[] =
{
	&t2200_f0_FieldInfo,
	&t2200_f1_FieldInfo,
	NULL
};
static PropertyInfo t2200____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2200_TI, "System.Collections.IEnumerator.Current", &m10767_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2200____Current_PropertyInfo = 
{
	&t2200_TI, "Current", &m10770_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2200_PIs[] =
{
	&t2200____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2200____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2200_m10766_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10766_GM;
MethodInfo m10766_MI = 
{
	".ctor", (methodPointerType)&m10766, &t2200_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2200_m10766_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10766_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10767_GM;
MethodInfo m10767_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10767, &t2200_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10767_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10768_GM;
MethodInfo m10768_MI = 
{
	"Dispose", (methodPointerType)&m10768, &t2200_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10768_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10769_GM;
MethodInfo m10769_MI = 
{
	"MoveNext", (methodPointerType)&m10769, &t2200_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10769_GM};
extern Il2CppType t601_0_0_0;
extern void* RuntimeInvoker_t601 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10770_GM;
MethodInfo m10770_MI = 
{
	"get_Current", (methodPointerType)&m10770, &t2200_TI, &t601_0_0_0, RuntimeInvoker_t601, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10770_GM};
static MethodInfo* t2200_MIs[] =
{
	&m10766_MI,
	&m10767_MI,
	&m10768_MI,
	&m10769_MI,
	&m10770_MI,
	NULL
};
static MethodInfo* t2200_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10767_MI,
	&m10769_MI,
	&m10768_MI,
	&m10770_MI,
};
static TypeInfo* t2200_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4005_TI,
};
static Il2CppInterfaceOffsetPair t2200_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4005_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2200_0_0_0;
extern Il2CppType t2200_1_0_0;
extern Il2CppGenericClass t2200_GC;
TypeInfo t2200_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2200_MIs, t2200_PIs, t2200_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2200_TI, t2200_ITIs, t2200_VT, &EmptyCustomAttributesCache, &t2200_TI, &t2200_0_0_0, &t2200_1_0_0, t2200_IOs, &t2200_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2200)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5149_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Double>
extern MethodInfo m26753_MI;
static PropertyInfo t5149____Count_PropertyInfo = 
{
	&t5149_TI, "Count", &m26753_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26754_MI;
static PropertyInfo t5149____IsReadOnly_PropertyInfo = 
{
	&t5149_TI, "IsReadOnly", &m26754_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5149_PIs[] =
{
	&t5149____Count_PropertyInfo,
	&t5149____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26753_GM;
MethodInfo m26753_MI = 
{
	"get_Count", NULL, &t5149_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26753_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26754_GM;
MethodInfo m26754_MI = 
{
	"get_IsReadOnly", NULL, &t5149_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26754_GM};
extern Il2CppType t601_0_0_0;
extern Il2CppType t601_0_0_0;
static ParameterInfo t5149_m26755_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t601_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t601 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26755_GM;
MethodInfo m26755_MI = 
{
	"Add", NULL, &t5149_TI, &t21_0_0_0, RuntimeInvoker_t21_t601, t5149_m26755_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26755_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26756_GM;
MethodInfo m26756_MI = 
{
	"Clear", NULL, &t5149_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26756_GM};
extern Il2CppType t601_0_0_0;
static ParameterInfo t5149_m26757_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t601_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t601 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26757_GM;
MethodInfo m26757_MI = 
{
	"Contains", NULL, &t5149_TI, &t40_0_0_0, RuntimeInvoker_t40_t601, t5149_m26757_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26757_GM};
extern Il2CppType t1140_0_0_0;
extern Il2CppType t1140_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5149_m26758_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1140_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26758_GM;
MethodInfo m26758_MI = 
{
	"CopyTo", NULL, &t5149_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5149_m26758_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26758_GM};
extern Il2CppType t601_0_0_0;
static ParameterInfo t5149_m26759_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t601_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t601 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26759_GM;
MethodInfo m26759_MI = 
{
	"Remove", NULL, &t5149_TI, &t40_0_0_0, RuntimeInvoker_t40_t601, t5149_m26759_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26759_GM};
static MethodInfo* t5149_MIs[] =
{
	&m26753_MI,
	&m26754_MI,
	&m26755_MI,
	&m26756_MI,
	&m26757_MI,
	&m26758_MI,
	&m26759_MI,
	NULL
};
extern TypeInfo t5151_TI;
static TypeInfo* t5149_ITIs[] = 
{
	&t603_TI,
	&t5151_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5149_0_0_0;
extern Il2CppType t5149_1_0_0;
struct t5149;
extern Il2CppGenericClass t5149_GC;
TypeInfo t5149_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5149_MIs, t5149_PIs, NULL, NULL, NULL, NULL, NULL, &t5149_TI, t5149_ITIs, NULL, &EmptyCustomAttributesCache, &t5149_TI, &t5149_0_0_0, &t5149_1_0_0, NULL, &t5149_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Double>
extern Il2CppType t4005_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26760_GM;
MethodInfo m26760_MI = 
{
	"GetEnumerator", NULL, &t5151_TI, &t4005_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26760_GM};
static MethodInfo* t5151_MIs[] =
{
	&m26760_MI,
	NULL
};
static TypeInfo* t5151_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5151_0_0_0;
extern Il2CppType t5151_1_0_0;
struct t5151;
extern Il2CppGenericClass t5151_GC;
TypeInfo t5151_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5151_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5151_TI, t5151_ITIs, NULL, &EmptyCustomAttributesCache, &t5151_TI, &t5151_0_0_0, &t5151_1_0_0, NULL, &t5151_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5150_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Double>
extern MethodInfo m26761_MI;
extern MethodInfo m26762_MI;
static PropertyInfo t5150____Item_PropertyInfo = 
{
	&t5150_TI, "Item", &m26761_MI, &m26762_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5150_PIs[] =
{
	&t5150____Item_PropertyInfo,
	NULL
};
extern Il2CppType t601_0_0_0;
static ParameterInfo t5150_m26763_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t601_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t601 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26763_GM;
MethodInfo m26763_MI = 
{
	"IndexOf", NULL, &t5150_TI, &t44_0_0_0, RuntimeInvoker_t44_t601, t5150_m26763_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26763_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t601_0_0_0;
static ParameterInfo t5150_m26764_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t601_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t601 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26764_GM;
MethodInfo m26764_MI = 
{
	"Insert", NULL, &t5150_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t601, t5150_m26764_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26764_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5150_m26765_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26765_GM;
MethodInfo m26765_MI = 
{
	"RemoveAt", NULL, &t5150_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5150_m26765_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26765_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5150_m26761_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t601_0_0_0;
extern void* RuntimeInvoker_t601_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26761_GM;
MethodInfo m26761_MI = 
{
	"get_Item", NULL, &t5150_TI, &t601_0_0_0, RuntimeInvoker_t601_t44, t5150_m26761_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26761_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t601_0_0_0;
static ParameterInfo t5150_m26762_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t601_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t601 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26762_GM;
MethodInfo m26762_MI = 
{
	"set_Item", NULL, &t5150_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t601, t5150_m26762_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26762_GM};
static MethodInfo* t5150_MIs[] =
{
	&m26763_MI,
	&m26764_MI,
	&m26765_MI,
	&m26761_MI,
	&m26762_MI,
	NULL
};
static TypeInfo* t5150_ITIs[] = 
{
	&t603_TI,
	&t5149_TI,
	&t5151_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5150_0_0_0;
extern Il2CppType t5150_1_0_0;
struct t5150;
extern Il2CppGenericClass t5150_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5150_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5150_MIs, t5150_PIs, NULL, NULL, NULL, NULL, NULL, &t5150_TI, t5150_ITIs, NULL, &t1908__CustomAttributeCache, &t5150_TI, &t5150_0_0_0, &t5150_1_0_0, NULL, &t5150_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5152_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IComparable`1<System.Double>>
extern MethodInfo m26766_MI;
static PropertyInfo t5152____Count_PropertyInfo = 
{
	&t5152_TI, "Count", &m26766_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26767_MI;
static PropertyInfo t5152____IsReadOnly_PropertyInfo = 
{
	&t5152_TI, "IsReadOnly", &m26767_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5152_PIs[] =
{
	&t5152____Count_PropertyInfo,
	&t5152____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26766_GM;
MethodInfo m26766_MI = 
{
	"get_Count", NULL, &t5152_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26766_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26767_GM;
MethodInfo m26767_MI = 
{
	"get_IsReadOnly", NULL, &t5152_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26767_GM};
extern Il2CppType t1754_0_0_0;
extern Il2CppType t1754_0_0_0;
static ParameterInfo t5152_m26768_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1754_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26768_GM;
MethodInfo m26768_MI = 
{
	"Add", NULL, &t5152_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5152_m26768_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26768_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26769_GM;
MethodInfo m26769_MI = 
{
	"Clear", NULL, &t5152_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26769_GM};
extern Il2CppType t1754_0_0_0;
static ParameterInfo t5152_m26770_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1754_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26770_GM;
MethodInfo m26770_MI = 
{
	"Contains", NULL, &t5152_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5152_m26770_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26770_GM};
extern Il2CppType t3531_0_0_0;
extern Il2CppType t3531_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5152_m26771_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3531_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26771_GM;
MethodInfo m26771_MI = 
{
	"CopyTo", NULL, &t5152_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5152_m26771_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26771_GM};
extern Il2CppType t1754_0_0_0;
static ParameterInfo t5152_m26772_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1754_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26772_GM;
MethodInfo m26772_MI = 
{
	"Remove", NULL, &t5152_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5152_m26772_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26772_GM};
static MethodInfo* t5152_MIs[] =
{
	&m26766_MI,
	&m26767_MI,
	&m26768_MI,
	&m26769_MI,
	&m26770_MI,
	&m26771_MI,
	&m26772_MI,
	NULL
};
extern TypeInfo t5154_TI;
static TypeInfo* t5152_ITIs[] = 
{
	&t603_TI,
	&t5154_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5152_0_0_0;
extern Il2CppType t5152_1_0_0;
struct t5152;
extern Il2CppGenericClass t5152_GC;
TypeInfo t5152_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5152_MIs, t5152_PIs, NULL, NULL, NULL, NULL, NULL, &t5152_TI, t5152_ITIs, NULL, &EmptyCustomAttributesCache, &t5152_TI, &t5152_0_0_0, &t5152_1_0_0, NULL, &t5152_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.Double>>
extern Il2CppType t4007_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26773_GM;
MethodInfo m26773_MI = 
{
	"GetEnumerator", NULL, &t5154_TI, &t4007_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26773_GM};
static MethodInfo* t5154_MIs[] =
{
	&m26773_MI,
	NULL
};
static TypeInfo* t5154_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5154_0_0_0;
extern Il2CppType t5154_1_0_0;
struct t5154;
extern Il2CppGenericClass t5154_GC;
TypeInfo t5154_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5154_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5154_TI, t5154_ITIs, NULL, &EmptyCustomAttributesCache, &t5154_TI, &t5154_0_0_0, &t5154_1_0_0, NULL, &t5154_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4007_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.Double>>
extern MethodInfo m26774_MI;
static PropertyInfo t4007____Current_PropertyInfo = 
{
	&t4007_TI, "Current", &m26774_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4007_PIs[] =
{
	&t4007____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1754_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26774_GM;
MethodInfo m26774_MI = 
{
	"get_Current", NULL, &t4007_TI, &t1754_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26774_GM};
static MethodInfo* t4007_MIs[] =
{
	&m26774_MI,
	NULL
};
static TypeInfo* t4007_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4007_0_0_0;
extern Il2CppType t4007_1_0_0;
struct t4007;
extern Il2CppGenericClass t4007_GC;
TypeInfo t4007_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4007_MIs, t4007_PIs, NULL, NULL, NULL, NULL, NULL, &t4007_TI, t4007_ITIs, NULL, &EmptyCustomAttributesCache, &t4007_TI, &t4007_0_0_0, &t4007_1_0_0, NULL, &t4007_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1754_TI;



// Metadata Definition System.IComparable`1<System.Double>
extern Il2CppType t601_0_0_0;
static ParameterInfo t1754_m26775_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t601_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t601 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26775_GM;
MethodInfo m26775_MI = 
{
	"CompareTo", NULL, &t1754_TI, &t44_0_0_0, RuntimeInvoker_t44_t601, t1754_m26775_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26775_GM};
static MethodInfo* t1754_MIs[] =
{
	&m26775_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1754_1_0_0;
struct t1754;
extern Il2CppGenericClass t1754_GC;
TypeInfo t1754_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t1754_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1754_TI, NULL, NULL, &EmptyCustomAttributesCache, &t1754_TI, &t1754_0_0_0, &t1754_1_0_0, NULL, &t1754_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t2201.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2201_TI;
#include "t2201MD.h"

extern MethodInfo m10775_MI;
extern MethodInfo m19905_MI;
struct t20;
#define m19905(__this, p0, method) (t29*)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.IComparable`1<System.Double>>
extern Il2CppType t20_0_0_1;
FieldInfo t2201_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2201_TI, offsetof(t2201, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2201_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2201_TI, offsetof(t2201, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2201_FIs[] =
{
	&t2201_f0_FieldInfo,
	&t2201_f1_FieldInfo,
	NULL
};
extern MethodInfo m10772_MI;
static PropertyInfo t2201____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2201_TI, "System.Collections.IEnumerator.Current", &m10772_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2201____Current_PropertyInfo = 
{
	&t2201_TI, "Current", &m10775_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2201_PIs[] =
{
	&t2201____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2201____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2201_m10771_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10771_GM;
MethodInfo m10771_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2201_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2201_m10771_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10771_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10772_GM;
MethodInfo m10772_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2201_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10772_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10773_GM;
MethodInfo m10773_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2201_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10773_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10774_GM;
MethodInfo m10774_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2201_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10774_GM};
extern Il2CppType t1754_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10775_GM;
MethodInfo m10775_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2201_TI, &t1754_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10775_GM};
static MethodInfo* t2201_MIs[] =
{
	&m10771_MI,
	&m10772_MI,
	&m10773_MI,
	&m10774_MI,
	&m10775_MI,
	NULL
};
extern MethodInfo m10774_MI;
extern MethodInfo m10773_MI;
static MethodInfo* t2201_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10772_MI,
	&m10774_MI,
	&m10773_MI,
	&m10775_MI,
};
static TypeInfo* t2201_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4007_TI,
};
static Il2CppInterfaceOffsetPair t2201_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4007_TI, 7},
};
extern TypeInfo t1754_TI;
static Il2CppRGCTXData t2201_RGCTXData[3] = 
{
	&m10775_MI/* Method Usage */,
	&t1754_TI/* Class Usage */,
	&m19905_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2201_0_0_0;
extern Il2CppType t2201_1_0_0;
extern Il2CppGenericClass t2201_GC;
TypeInfo t2201_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2201_MIs, t2201_PIs, t2201_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2201_TI, t2201_ITIs, t2201_VT, &EmptyCustomAttributesCache, &t2201_TI, &t2201_0_0_0, &t2201_1_0_0, t2201_IOs, &t2201_GC, NULL, NULL, NULL, t2201_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2201)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5153_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IComparable`1<System.Double>>
extern MethodInfo m26776_MI;
extern MethodInfo m26777_MI;
static PropertyInfo t5153____Item_PropertyInfo = 
{
	&t5153_TI, "Item", &m26776_MI, &m26777_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5153_PIs[] =
{
	&t5153____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1754_0_0_0;
static ParameterInfo t5153_m26778_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1754_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26778_GM;
MethodInfo m26778_MI = 
{
	"IndexOf", NULL, &t5153_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5153_m26778_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26778_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1754_0_0_0;
static ParameterInfo t5153_m26779_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1754_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26779_GM;
MethodInfo m26779_MI = 
{
	"Insert", NULL, &t5153_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5153_m26779_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26779_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5153_m26780_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26780_GM;
MethodInfo m26780_MI = 
{
	"RemoveAt", NULL, &t5153_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5153_m26780_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26780_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5153_m26776_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1754_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26776_GM;
MethodInfo m26776_MI = 
{
	"get_Item", NULL, &t5153_TI, &t1754_0_0_0, RuntimeInvoker_t29_t44, t5153_m26776_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26776_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1754_0_0_0;
static ParameterInfo t5153_m26777_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1754_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26777_GM;
MethodInfo m26777_MI = 
{
	"set_Item", NULL, &t5153_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5153_m26777_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26777_GM};
static MethodInfo* t5153_MIs[] =
{
	&m26778_MI,
	&m26779_MI,
	&m26780_MI,
	&m26776_MI,
	&m26777_MI,
	NULL
};
static TypeInfo* t5153_ITIs[] = 
{
	&t603_TI,
	&t5152_TI,
	&t5154_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5153_0_0_0;
extern Il2CppType t5153_1_0_0;
struct t5153;
extern Il2CppGenericClass t5153_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5153_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5153_MIs, t5153_PIs, NULL, NULL, NULL, NULL, NULL, &t5153_TI, t5153_ITIs, NULL, &t1908__CustomAttributeCache, &t5153_TI, &t5153_0_0_0, &t5153_1_0_0, NULL, &t5153_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5155_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Double>>
extern MethodInfo m26781_MI;
static PropertyInfo t5155____Count_PropertyInfo = 
{
	&t5155_TI, "Count", &m26781_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26782_MI;
static PropertyInfo t5155____IsReadOnly_PropertyInfo = 
{
	&t5155_TI, "IsReadOnly", &m26782_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5155_PIs[] =
{
	&t5155____Count_PropertyInfo,
	&t5155____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26781_GM;
MethodInfo m26781_MI = 
{
	"get_Count", NULL, &t5155_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26781_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26782_GM;
MethodInfo m26782_MI = 
{
	"get_IsReadOnly", NULL, &t5155_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26782_GM};
extern Il2CppType t1755_0_0_0;
extern Il2CppType t1755_0_0_0;
static ParameterInfo t5155_m26783_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1755_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26783_GM;
MethodInfo m26783_MI = 
{
	"Add", NULL, &t5155_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5155_m26783_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26783_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26784_GM;
MethodInfo m26784_MI = 
{
	"Clear", NULL, &t5155_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26784_GM};
extern Il2CppType t1755_0_0_0;
static ParameterInfo t5155_m26785_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1755_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26785_GM;
MethodInfo m26785_MI = 
{
	"Contains", NULL, &t5155_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5155_m26785_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26785_GM};
extern Il2CppType t3532_0_0_0;
extern Il2CppType t3532_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5155_m26786_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3532_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26786_GM;
MethodInfo m26786_MI = 
{
	"CopyTo", NULL, &t5155_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5155_m26786_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26786_GM};
extern Il2CppType t1755_0_0_0;
static ParameterInfo t5155_m26787_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1755_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26787_GM;
MethodInfo m26787_MI = 
{
	"Remove", NULL, &t5155_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5155_m26787_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26787_GM};
static MethodInfo* t5155_MIs[] =
{
	&m26781_MI,
	&m26782_MI,
	&m26783_MI,
	&m26784_MI,
	&m26785_MI,
	&m26786_MI,
	&m26787_MI,
	NULL
};
extern TypeInfo t5157_TI;
static TypeInfo* t5155_ITIs[] = 
{
	&t603_TI,
	&t5157_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5155_0_0_0;
extern Il2CppType t5155_1_0_0;
struct t5155;
extern Il2CppGenericClass t5155_GC;
TypeInfo t5155_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5155_MIs, t5155_PIs, NULL, NULL, NULL, NULL, NULL, &t5155_TI, t5155_ITIs, NULL, &EmptyCustomAttributesCache, &t5155_TI, &t5155_0_0_0, &t5155_1_0_0, NULL, &t5155_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.Double>>
extern Il2CppType t4009_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26788_GM;
MethodInfo m26788_MI = 
{
	"GetEnumerator", NULL, &t5157_TI, &t4009_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26788_GM};
static MethodInfo* t5157_MIs[] =
{
	&m26788_MI,
	NULL
};
static TypeInfo* t5157_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5157_0_0_0;
extern Il2CppType t5157_1_0_0;
struct t5157;
extern Il2CppGenericClass t5157_GC;
TypeInfo t5157_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5157_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5157_TI, t5157_ITIs, NULL, &EmptyCustomAttributesCache, &t5157_TI, &t5157_0_0_0, &t5157_1_0_0, NULL, &t5157_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4009_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IEquatable`1<System.Double>>
extern MethodInfo m26789_MI;
static PropertyInfo t4009____Current_PropertyInfo = 
{
	&t4009_TI, "Current", &m26789_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4009_PIs[] =
{
	&t4009____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1755_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26789_GM;
MethodInfo m26789_MI = 
{
	"get_Current", NULL, &t4009_TI, &t1755_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26789_GM};
static MethodInfo* t4009_MIs[] =
{
	&m26789_MI,
	NULL
};
static TypeInfo* t4009_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4009_0_0_0;
extern Il2CppType t4009_1_0_0;
struct t4009;
extern Il2CppGenericClass t4009_GC;
TypeInfo t4009_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4009_MIs, t4009_PIs, NULL, NULL, NULL, NULL, NULL, &t4009_TI, t4009_ITIs, NULL, &EmptyCustomAttributesCache, &t4009_TI, &t4009_0_0_0, &t4009_1_0_0, NULL, &t4009_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1755_TI;



// Metadata Definition System.IEquatable`1<System.Double>
extern Il2CppType t601_0_0_0;
static ParameterInfo t1755_m26790_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t601_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t601 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26790_GM;
MethodInfo m26790_MI = 
{
	"Equals", NULL, &t1755_TI, &t40_0_0_0, RuntimeInvoker_t40_t601, t1755_m26790_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26790_GM};
static MethodInfo* t1755_MIs[] =
{
	&m26790_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1755_1_0_0;
struct t1755;
extern Il2CppGenericClass t1755_GC;
TypeInfo t1755_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t1755_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1755_TI, NULL, NULL, &EmptyCustomAttributesCache, &t1755_TI, &t1755_0_0_0, &t1755_1_0_0, NULL, &t1755_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t2202.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2202_TI;
#include "t2202MD.h"

extern MethodInfo m10780_MI;
extern MethodInfo m19916_MI;
struct t20;
#define m19916(__this, p0, method) (t29*)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.IEquatable`1<System.Double>>
extern Il2CppType t20_0_0_1;
FieldInfo t2202_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2202_TI, offsetof(t2202, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2202_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2202_TI, offsetof(t2202, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2202_FIs[] =
{
	&t2202_f0_FieldInfo,
	&t2202_f1_FieldInfo,
	NULL
};
extern MethodInfo m10777_MI;
static PropertyInfo t2202____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2202_TI, "System.Collections.IEnumerator.Current", &m10777_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2202____Current_PropertyInfo = 
{
	&t2202_TI, "Current", &m10780_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2202_PIs[] =
{
	&t2202____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2202____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2202_m10776_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10776_GM;
MethodInfo m10776_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2202_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2202_m10776_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10776_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10777_GM;
MethodInfo m10777_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2202_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10777_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10778_GM;
MethodInfo m10778_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2202_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10778_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10779_GM;
MethodInfo m10779_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2202_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10779_GM};
extern Il2CppType t1755_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10780_GM;
MethodInfo m10780_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2202_TI, &t1755_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10780_GM};
static MethodInfo* t2202_MIs[] =
{
	&m10776_MI,
	&m10777_MI,
	&m10778_MI,
	&m10779_MI,
	&m10780_MI,
	NULL
};
extern MethodInfo m10779_MI;
extern MethodInfo m10778_MI;
static MethodInfo* t2202_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10777_MI,
	&m10779_MI,
	&m10778_MI,
	&m10780_MI,
};
static TypeInfo* t2202_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4009_TI,
};
static Il2CppInterfaceOffsetPair t2202_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4009_TI, 7},
};
extern TypeInfo t1755_TI;
static Il2CppRGCTXData t2202_RGCTXData[3] = 
{
	&m10780_MI/* Method Usage */,
	&t1755_TI/* Class Usage */,
	&m19916_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2202_0_0_0;
extern Il2CppType t2202_1_0_0;
extern Il2CppGenericClass t2202_GC;
TypeInfo t2202_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2202_MIs, t2202_PIs, t2202_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2202_TI, t2202_ITIs, t2202_VT, &EmptyCustomAttributesCache, &t2202_TI, &t2202_0_0_0, &t2202_1_0_0, t2202_IOs, &t2202_GC, NULL, NULL, NULL, t2202_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2202)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5156_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IEquatable`1<System.Double>>
extern MethodInfo m26791_MI;
extern MethodInfo m26792_MI;
static PropertyInfo t5156____Item_PropertyInfo = 
{
	&t5156_TI, "Item", &m26791_MI, &m26792_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5156_PIs[] =
{
	&t5156____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1755_0_0_0;
static ParameterInfo t5156_m26793_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1755_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26793_GM;
MethodInfo m26793_MI = 
{
	"IndexOf", NULL, &t5156_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5156_m26793_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26793_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1755_0_0_0;
static ParameterInfo t5156_m26794_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1755_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26794_GM;
MethodInfo m26794_MI = 
{
	"Insert", NULL, &t5156_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5156_m26794_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26794_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5156_m26795_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26795_GM;
MethodInfo m26795_MI = 
{
	"RemoveAt", NULL, &t5156_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5156_m26795_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26795_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5156_m26791_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1755_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26791_GM;
MethodInfo m26791_MI = 
{
	"get_Item", NULL, &t5156_TI, &t1755_0_0_0, RuntimeInvoker_t29_t44, t5156_m26791_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26791_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1755_0_0_0;
static ParameterInfo t5156_m26792_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1755_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26792_GM;
MethodInfo m26792_MI = 
{
	"set_Item", NULL, &t5156_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5156_m26792_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26792_GM};
static MethodInfo* t5156_MIs[] =
{
	&m26793_MI,
	&m26794_MI,
	&m26795_MI,
	&m26791_MI,
	&m26792_MI,
	NULL
};
static TypeInfo* t5156_ITIs[] = 
{
	&t603_TI,
	&t5155_TI,
	&t5157_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5156_0_0_0;
extern Il2CppType t5156_1_0_0;
struct t5156;
extern Il2CppGenericClass t5156_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5156_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5156_MIs, t5156_PIs, NULL, NULL, NULL, NULL, NULL, &t5156_TI, t5156_ITIs, NULL, &t1908__CustomAttributeCache, &t5156_TI, &t5156_0_0_0, &t5156_1_0_0, NULL, &t5156_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1130_TI;

#include "t194.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Char>
extern MethodInfo m26796_MI;
static PropertyInfo t1130____Current_PropertyInfo = 
{
	&t1130_TI, "Current", &m26796_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1130_PIs[] =
{
	&t1130____Current_PropertyInfo,
	NULL
};
extern Il2CppType t194_0_0_0;
extern void* RuntimeInvoker_t194 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26796_GM;
MethodInfo m26796_MI = 
{
	"get_Current", NULL, &t1130_TI, &t194_0_0_0, RuntimeInvoker_t194, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26796_GM};
static MethodInfo* t1130_MIs[] =
{
	&m26796_MI,
	NULL
};
static TypeInfo* t1130_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1130_0_0_0;
extern Il2CppType t1130_1_0_0;
struct t1130;
extern Il2CppGenericClass t1130_GC;
TypeInfo t1130_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t1130_MIs, t1130_PIs, NULL, NULL, NULL, NULL, NULL, &t1130_TI, t1130_ITIs, NULL, &EmptyCustomAttributesCache, &t1130_TI, &t1130_0_0_0, &t1130_1_0_0, NULL, &t1130_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2203.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2203_TI;
#include "t2203MD.h"

extern TypeInfo t194_TI;
extern MethodInfo m10785_MI;
extern MethodInfo m19927_MI;
struct t20;
 uint16_t m19927 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m10781_MI;
 void m10781 (t2203 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m10782_MI;
 t29 * m10782 (t2203 * __this, MethodInfo* method){
	{
		uint16_t L_0 = m10785(__this, &m10785_MI);
		uint16_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t194_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m10783_MI;
 void m10783 (t2203 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m10784_MI;
 bool m10784 (t2203 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 uint16_t m10785 (t2203 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		uint16_t L_8 = m19927(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m19927_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Char>
extern Il2CppType t20_0_0_1;
FieldInfo t2203_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2203_TI, offsetof(t2203, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2203_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2203_TI, offsetof(t2203, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2203_FIs[] =
{
	&t2203_f0_FieldInfo,
	&t2203_f1_FieldInfo,
	NULL
};
static PropertyInfo t2203____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2203_TI, "System.Collections.IEnumerator.Current", &m10782_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2203____Current_PropertyInfo = 
{
	&t2203_TI, "Current", &m10785_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2203_PIs[] =
{
	&t2203____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2203____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2203_m10781_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10781_GM;
MethodInfo m10781_MI = 
{
	".ctor", (methodPointerType)&m10781, &t2203_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2203_m10781_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10781_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10782_GM;
MethodInfo m10782_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10782, &t2203_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10782_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10783_GM;
MethodInfo m10783_MI = 
{
	"Dispose", (methodPointerType)&m10783, &t2203_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10783_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10784_GM;
MethodInfo m10784_MI = 
{
	"MoveNext", (methodPointerType)&m10784, &t2203_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10784_GM};
extern Il2CppType t194_0_0_0;
extern void* RuntimeInvoker_t194 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10785_GM;
MethodInfo m10785_MI = 
{
	"get_Current", (methodPointerType)&m10785, &t2203_TI, &t194_0_0_0, RuntimeInvoker_t194, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10785_GM};
static MethodInfo* t2203_MIs[] =
{
	&m10781_MI,
	&m10782_MI,
	&m10783_MI,
	&m10784_MI,
	&m10785_MI,
	NULL
};
static MethodInfo* t2203_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10782_MI,
	&m10784_MI,
	&m10783_MI,
	&m10785_MI,
};
static TypeInfo* t2203_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t1130_TI,
};
static Il2CppInterfaceOffsetPair t2203_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t1130_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2203_0_0_0;
extern Il2CppType t2203_1_0_0;
extern Il2CppGenericClass t2203_GC;
TypeInfo t2203_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2203_MIs, t2203_PIs, t2203_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2203_TI, t2203_ITIs, t2203_VT, &EmptyCustomAttributesCache, &t2203_TI, &t2203_0_0_0, &t2203_1_0_0, t2203_IOs, &t2203_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2203)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5158_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Char>
extern MethodInfo m26797_MI;
static PropertyInfo t5158____Count_PropertyInfo = 
{
	&t5158_TI, "Count", &m26797_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26798_MI;
static PropertyInfo t5158____IsReadOnly_PropertyInfo = 
{
	&t5158_TI, "IsReadOnly", &m26798_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5158_PIs[] =
{
	&t5158____Count_PropertyInfo,
	&t5158____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26797_GM;
MethodInfo m26797_MI = 
{
	"get_Count", NULL, &t5158_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26797_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26798_GM;
MethodInfo m26798_MI = 
{
	"get_IsReadOnly", NULL, &t5158_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26798_GM};
extern Il2CppType t194_0_0_0;
extern Il2CppType t194_0_0_0;
static ParameterInfo t5158_m26799_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t194_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t372 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26799_GM;
MethodInfo m26799_MI = 
{
	"Add", NULL, &t5158_TI, &t21_0_0_0, RuntimeInvoker_t21_t372, t5158_m26799_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26799_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26800_GM;
MethodInfo m26800_MI = 
{
	"Clear", NULL, &t5158_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26800_GM};
extern Il2CppType t194_0_0_0;
static ParameterInfo t5158_m26801_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t194_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t372 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26801_GM;
MethodInfo m26801_MI = 
{
	"Contains", NULL, &t5158_TI, &t40_0_0_0, RuntimeInvoker_t40_t372, t5158_m26801_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26801_GM};
extern Il2CppType t200_0_0_0;
extern Il2CppType t200_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5158_m26802_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t200_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26802_GM;
MethodInfo m26802_MI = 
{
	"CopyTo", NULL, &t5158_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5158_m26802_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26802_GM};
extern Il2CppType t194_0_0_0;
static ParameterInfo t5158_m26803_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t194_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t372 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26803_GM;
MethodInfo m26803_MI = 
{
	"Remove", NULL, &t5158_TI, &t40_0_0_0, RuntimeInvoker_t40_t372, t5158_m26803_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26803_GM};
static MethodInfo* t5158_MIs[] =
{
	&m26797_MI,
	&m26798_MI,
	&m26799_MI,
	&m26800_MI,
	&m26801_MI,
	&m26802_MI,
	&m26803_MI,
	NULL
};
extern TypeInfo t1747_TI;
static TypeInfo* t5158_ITIs[] = 
{
	&t603_TI,
	&t1747_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5158_0_0_0;
extern Il2CppType t5158_1_0_0;
struct t5158;
extern Il2CppGenericClass t5158_GC;
TypeInfo t5158_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5158_MIs, t5158_PIs, NULL, NULL, NULL, NULL, NULL, &t5158_TI, t5158_ITIs, NULL, &EmptyCustomAttributesCache, &t5158_TI, &t5158_0_0_0, &t5158_1_0_0, NULL, &t5158_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Char>
extern Il2CppType t1130_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26804_GM;
MethodInfo m26804_MI = 
{
	"GetEnumerator", NULL, &t1747_TI, &t1130_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26804_GM};
static MethodInfo* t1747_MIs[] =
{
	&m26804_MI,
	NULL
};
static TypeInfo* t1747_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1747_0_0_0;
extern Il2CppType t1747_1_0_0;
struct t1747;
extern Il2CppGenericClass t1747_GC;
TypeInfo t1747_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t1747_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1747_TI, t1747_ITIs, NULL, &EmptyCustomAttributesCache, &t1747_TI, &t1747_0_0_0, &t1747_1_0_0, NULL, &t1747_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5159_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Char>
extern MethodInfo m26805_MI;
extern MethodInfo m26806_MI;
static PropertyInfo t5159____Item_PropertyInfo = 
{
	&t5159_TI, "Item", &m26805_MI, &m26806_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5159_PIs[] =
{
	&t5159____Item_PropertyInfo,
	NULL
};
extern Il2CppType t194_0_0_0;
static ParameterInfo t5159_m26807_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t194_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t372 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26807_GM;
MethodInfo m26807_MI = 
{
	"IndexOf", NULL, &t5159_TI, &t44_0_0_0, RuntimeInvoker_t44_t372, t5159_m26807_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26807_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t194_0_0_0;
static ParameterInfo t5159_m26808_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t194_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t372 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26808_GM;
MethodInfo m26808_MI = 
{
	"Insert", NULL, &t5159_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t372, t5159_m26808_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26808_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5159_m26809_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26809_GM;
MethodInfo m26809_MI = 
{
	"RemoveAt", NULL, &t5159_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5159_m26809_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26809_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5159_m26805_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t194_0_0_0;
extern void* RuntimeInvoker_t194_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26805_GM;
MethodInfo m26805_MI = 
{
	"get_Item", NULL, &t5159_TI, &t194_0_0_0, RuntimeInvoker_t194_t44, t5159_m26805_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26805_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t194_0_0_0;
static ParameterInfo t5159_m26806_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t194_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t372 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26806_GM;
MethodInfo m26806_MI = 
{
	"set_Item", NULL, &t5159_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t372, t5159_m26806_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26806_GM};
static MethodInfo* t5159_MIs[] =
{
	&m26807_MI,
	&m26808_MI,
	&m26809_MI,
	&m26805_MI,
	&m26806_MI,
	NULL
};
static TypeInfo* t5159_ITIs[] = 
{
	&t603_TI,
	&t5158_TI,
	&t1747_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5159_0_0_0;
extern Il2CppType t5159_1_0_0;
struct t5159;
extern Il2CppGenericClass t5159_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5159_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5159_MIs, t5159_PIs, NULL, NULL, NULL, NULL, NULL, &t5159_TI, t5159_ITIs, NULL, &t1908__CustomAttributeCache, &t5159_TI, &t5159_0_0_0, &t5159_1_0_0, NULL, &t5159_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5160_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IComparable`1<System.Char>>
extern MethodInfo m26810_MI;
static PropertyInfo t5160____Count_PropertyInfo = 
{
	&t5160_TI, "Count", &m26810_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26811_MI;
static PropertyInfo t5160____IsReadOnly_PropertyInfo = 
{
	&t5160_TI, "IsReadOnly", &m26811_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5160_PIs[] =
{
	&t5160____Count_PropertyInfo,
	&t5160____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26810_GM;
MethodInfo m26810_MI = 
{
	"get_Count", NULL, &t5160_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26810_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26811_GM;
MethodInfo m26811_MI = 
{
	"get_IsReadOnly", NULL, &t5160_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26811_GM};
extern Il2CppType t1739_0_0_0;
extern Il2CppType t1739_0_0_0;
static ParameterInfo t5160_m26812_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1739_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26812_GM;
MethodInfo m26812_MI = 
{
	"Add", NULL, &t5160_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5160_m26812_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26812_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26813_GM;
MethodInfo m26813_MI = 
{
	"Clear", NULL, &t5160_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26813_GM};
extern Il2CppType t1739_0_0_0;
static ParameterInfo t5160_m26814_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1739_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26814_GM;
MethodInfo m26814_MI = 
{
	"Contains", NULL, &t5160_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5160_m26814_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26814_GM};
extern Il2CppType t3533_0_0_0;
extern Il2CppType t3533_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5160_m26815_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3533_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26815_GM;
MethodInfo m26815_MI = 
{
	"CopyTo", NULL, &t5160_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5160_m26815_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26815_GM};
extern Il2CppType t1739_0_0_0;
static ParameterInfo t5160_m26816_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1739_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26816_GM;
MethodInfo m26816_MI = 
{
	"Remove", NULL, &t5160_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5160_m26816_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26816_GM};
static MethodInfo* t5160_MIs[] =
{
	&m26810_MI,
	&m26811_MI,
	&m26812_MI,
	&m26813_MI,
	&m26814_MI,
	&m26815_MI,
	&m26816_MI,
	NULL
};
extern TypeInfo t5162_TI;
static TypeInfo* t5160_ITIs[] = 
{
	&t603_TI,
	&t5162_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5160_0_0_0;
extern Il2CppType t5160_1_0_0;
struct t5160;
extern Il2CppGenericClass t5160_GC;
TypeInfo t5160_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5160_MIs, t5160_PIs, NULL, NULL, NULL, NULL, NULL, &t5160_TI, t5160_ITIs, NULL, &EmptyCustomAttributesCache, &t5160_TI, &t5160_0_0_0, &t5160_1_0_0, NULL, &t5160_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.Char>>
extern Il2CppType t4011_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26817_GM;
MethodInfo m26817_MI = 
{
	"GetEnumerator", NULL, &t5162_TI, &t4011_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26817_GM};
static MethodInfo* t5162_MIs[] =
{
	&m26817_MI,
	NULL
};
static TypeInfo* t5162_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5162_0_0_0;
extern Il2CppType t5162_1_0_0;
struct t5162;
extern Il2CppGenericClass t5162_GC;
TypeInfo t5162_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5162_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5162_TI, t5162_ITIs, NULL, &EmptyCustomAttributesCache, &t5162_TI, &t5162_0_0_0, &t5162_1_0_0, NULL, &t5162_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4011_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.Char>>
extern MethodInfo m26818_MI;
static PropertyInfo t4011____Current_PropertyInfo = 
{
	&t4011_TI, "Current", &m26818_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4011_PIs[] =
{
	&t4011____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1739_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26818_GM;
MethodInfo m26818_MI = 
{
	"get_Current", NULL, &t4011_TI, &t1739_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26818_GM};
static MethodInfo* t4011_MIs[] =
{
	&m26818_MI,
	NULL
};
static TypeInfo* t4011_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4011_0_0_0;
extern Il2CppType t4011_1_0_0;
struct t4011;
extern Il2CppGenericClass t4011_GC;
TypeInfo t4011_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4011_MIs, t4011_PIs, NULL, NULL, NULL, NULL, NULL, &t4011_TI, t4011_ITIs, NULL, &EmptyCustomAttributesCache, &t4011_TI, &t4011_0_0_0, &t4011_1_0_0, NULL, &t4011_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1739_TI;



// Metadata Definition System.IComparable`1<System.Char>
extern Il2CppType t194_0_0_0;
static ParameterInfo t1739_m26819_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t194_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t372 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26819_GM;
MethodInfo m26819_MI = 
{
	"CompareTo", NULL, &t1739_TI, &t44_0_0_0, RuntimeInvoker_t44_t372, t1739_m26819_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26819_GM};
static MethodInfo* t1739_MIs[] =
{
	&m26819_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1739_1_0_0;
struct t1739;
extern Il2CppGenericClass t1739_GC;
TypeInfo t1739_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t1739_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1739_TI, NULL, NULL, &EmptyCustomAttributesCache, &t1739_TI, &t1739_0_0_0, &t1739_1_0_0, NULL, &t1739_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t2204.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2204_TI;
#include "t2204MD.h"

extern MethodInfo m10790_MI;
extern MethodInfo m19938_MI;
struct t20;
#define m19938(__this, p0, method) (t29*)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.IComparable`1<System.Char>>
extern Il2CppType t20_0_0_1;
FieldInfo t2204_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2204_TI, offsetof(t2204, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2204_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2204_TI, offsetof(t2204, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2204_FIs[] =
{
	&t2204_f0_FieldInfo,
	&t2204_f1_FieldInfo,
	NULL
};
extern MethodInfo m10787_MI;
static PropertyInfo t2204____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2204_TI, "System.Collections.IEnumerator.Current", &m10787_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2204____Current_PropertyInfo = 
{
	&t2204_TI, "Current", &m10790_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2204_PIs[] =
{
	&t2204____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2204____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2204_m10786_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10786_GM;
MethodInfo m10786_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2204_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2204_m10786_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10786_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10787_GM;
MethodInfo m10787_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2204_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10787_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10788_GM;
MethodInfo m10788_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2204_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10788_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10789_GM;
MethodInfo m10789_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2204_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10789_GM};
extern Il2CppType t1739_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10790_GM;
MethodInfo m10790_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2204_TI, &t1739_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10790_GM};
static MethodInfo* t2204_MIs[] =
{
	&m10786_MI,
	&m10787_MI,
	&m10788_MI,
	&m10789_MI,
	&m10790_MI,
	NULL
};
extern MethodInfo m10789_MI;
extern MethodInfo m10788_MI;
static MethodInfo* t2204_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10787_MI,
	&m10789_MI,
	&m10788_MI,
	&m10790_MI,
};
static TypeInfo* t2204_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4011_TI,
};
static Il2CppInterfaceOffsetPair t2204_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4011_TI, 7},
};
extern TypeInfo t1739_TI;
static Il2CppRGCTXData t2204_RGCTXData[3] = 
{
	&m10790_MI/* Method Usage */,
	&t1739_TI/* Class Usage */,
	&m19938_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2204_0_0_0;
extern Il2CppType t2204_1_0_0;
extern Il2CppGenericClass t2204_GC;
TypeInfo t2204_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2204_MIs, t2204_PIs, t2204_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2204_TI, t2204_ITIs, t2204_VT, &EmptyCustomAttributesCache, &t2204_TI, &t2204_0_0_0, &t2204_1_0_0, t2204_IOs, &t2204_GC, NULL, NULL, NULL, t2204_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2204)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5161_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IComparable`1<System.Char>>
extern MethodInfo m26820_MI;
extern MethodInfo m26821_MI;
static PropertyInfo t5161____Item_PropertyInfo = 
{
	&t5161_TI, "Item", &m26820_MI, &m26821_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5161_PIs[] =
{
	&t5161____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1739_0_0_0;
static ParameterInfo t5161_m26822_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1739_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26822_GM;
MethodInfo m26822_MI = 
{
	"IndexOf", NULL, &t5161_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5161_m26822_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26822_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1739_0_0_0;
static ParameterInfo t5161_m26823_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1739_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26823_GM;
MethodInfo m26823_MI = 
{
	"Insert", NULL, &t5161_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5161_m26823_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26823_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5161_m26824_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26824_GM;
MethodInfo m26824_MI = 
{
	"RemoveAt", NULL, &t5161_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5161_m26824_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26824_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5161_m26820_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1739_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26820_GM;
MethodInfo m26820_MI = 
{
	"get_Item", NULL, &t5161_TI, &t1739_0_0_0, RuntimeInvoker_t29_t44, t5161_m26820_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26820_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1739_0_0_0;
static ParameterInfo t5161_m26821_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1739_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26821_GM;
MethodInfo m26821_MI = 
{
	"set_Item", NULL, &t5161_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5161_m26821_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26821_GM};
static MethodInfo* t5161_MIs[] =
{
	&m26822_MI,
	&m26823_MI,
	&m26824_MI,
	&m26820_MI,
	&m26821_MI,
	NULL
};
static TypeInfo* t5161_ITIs[] = 
{
	&t603_TI,
	&t5160_TI,
	&t5162_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5161_0_0_0;
extern Il2CppType t5161_1_0_0;
struct t5161;
extern Il2CppGenericClass t5161_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5161_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5161_MIs, t5161_PIs, NULL, NULL, NULL, NULL, NULL, &t5161_TI, t5161_ITIs, NULL, &t1908__CustomAttributeCache, &t5161_TI, &t5161_0_0_0, &t5161_1_0_0, NULL, &t5161_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5163_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Char>>
extern MethodInfo m26825_MI;
static PropertyInfo t5163____Count_PropertyInfo = 
{
	&t5163_TI, "Count", &m26825_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26826_MI;
static PropertyInfo t5163____IsReadOnly_PropertyInfo = 
{
	&t5163_TI, "IsReadOnly", &m26826_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5163_PIs[] =
{
	&t5163____Count_PropertyInfo,
	&t5163____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26825_GM;
MethodInfo m26825_MI = 
{
	"get_Count", NULL, &t5163_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26825_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26826_GM;
MethodInfo m26826_MI = 
{
	"get_IsReadOnly", NULL, &t5163_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26826_GM};
extern Il2CppType t1740_0_0_0;
extern Il2CppType t1740_0_0_0;
static ParameterInfo t5163_m26827_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1740_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26827_GM;
MethodInfo m26827_MI = 
{
	"Add", NULL, &t5163_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5163_m26827_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26827_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26828_GM;
MethodInfo m26828_MI = 
{
	"Clear", NULL, &t5163_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26828_GM};
extern Il2CppType t1740_0_0_0;
static ParameterInfo t5163_m26829_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1740_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26829_GM;
MethodInfo m26829_MI = 
{
	"Contains", NULL, &t5163_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5163_m26829_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26829_GM};
extern Il2CppType t3534_0_0_0;
extern Il2CppType t3534_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5163_m26830_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3534_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26830_GM;
MethodInfo m26830_MI = 
{
	"CopyTo", NULL, &t5163_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5163_m26830_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26830_GM};
extern Il2CppType t1740_0_0_0;
static ParameterInfo t5163_m26831_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1740_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26831_GM;
MethodInfo m26831_MI = 
{
	"Remove", NULL, &t5163_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5163_m26831_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26831_GM};
static MethodInfo* t5163_MIs[] =
{
	&m26825_MI,
	&m26826_MI,
	&m26827_MI,
	&m26828_MI,
	&m26829_MI,
	&m26830_MI,
	&m26831_MI,
	NULL
};
extern TypeInfo t5165_TI;
static TypeInfo* t5163_ITIs[] = 
{
	&t603_TI,
	&t5165_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5163_0_0_0;
extern Il2CppType t5163_1_0_0;
struct t5163;
extern Il2CppGenericClass t5163_GC;
TypeInfo t5163_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5163_MIs, t5163_PIs, NULL, NULL, NULL, NULL, NULL, &t5163_TI, t5163_ITIs, NULL, &EmptyCustomAttributesCache, &t5163_TI, &t5163_0_0_0, &t5163_1_0_0, NULL, &t5163_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.Char>>
extern Il2CppType t4013_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26832_GM;
MethodInfo m26832_MI = 
{
	"GetEnumerator", NULL, &t5165_TI, &t4013_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26832_GM};
static MethodInfo* t5165_MIs[] =
{
	&m26832_MI,
	NULL
};
static TypeInfo* t5165_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5165_0_0_0;
extern Il2CppType t5165_1_0_0;
struct t5165;
extern Il2CppGenericClass t5165_GC;
TypeInfo t5165_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5165_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5165_TI, t5165_ITIs, NULL, &EmptyCustomAttributesCache, &t5165_TI, &t5165_0_0_0, &t5165_1_0_0, NULL, &t5165_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4013_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IEquatable`1<System.Char>>
extern MethodInfo m26833_MI;
static PropertyInfo t4013____Current_PropertyInfo = 
{
	&t4013_TI, "Current", &m26833_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4013_PIs[] =
{
	&t4013____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1740_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26833_GM;
MethodInfo m26833_MI = 
{
	"get_Current", NULL, &t4013_TI, &t1740_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26833_GM};
static MethodInfo* t4013_MIs[] =
{
	&m26833_MI,
	NULL
};
static TypeInfo* t4013_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4013_0_0_0;
extern Il2CppType t4013_1_0_0;
struct t4013;
extern Il2CppGenericClass t4013_GC;
TypeInfo t4013_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4013_MIs, t4013_PIs, NULL, NULL, NULL, NULL, NULL, &t4013_TI, t4013_ITIs, NULL, &EmptyCustomAttributesCache, &t4013_TI, &t4013_0_0_0, &t4013_1_0_0, NULL, &t4013_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1740_TI;



// Metadata Definition System.IEquatable`1<System.Char>
extern Il2CppType t194_0_0_0;
static ParameterInfo t1740_m26834_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t194_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t372 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26834_GM;
MethodInfo m26834_MI = 
{
	"Equals", NULL, &t1740_TI, &t40_0_0_0, RuntimeInvoker_t40_t372, t1740_m26834_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26834_GM};
static MethodInfo* t1740_MIs[] =
{
	&m26834_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1740_1_0_0;
struct t1740;
extern Il2CppGenericClass t1740_GC;
TypeInfo t1740_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t1740_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1740_TI, NULL, NULL, &EmptyCustomAttributesCache, &t1740_TI, &t1740_0_0_0, &t1740_1_0_0, NULL, &t1740_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t2205.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2205_TI;
#include "t2205MD.h"

extern MethodInfo m10795_MI;
extern MethodInfo m19949_MI;
struct t20;
#define m19949(__this, p0, method) (t29*)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.IEquatable`1<System.Char>>
extern Il2CppType t20_0_0_1;
FieldInfo t2205_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2205_TI, offsetof(t2205, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2205_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2205_TI, offsetof(t2205, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2205_FIs[] =
{
	&t2205_f0_FieldInfo,
	&t2205_f1_FieldInfo,
	NULL
};
extern MethodInfo m10792_MI;
static PropertyInfo t2205____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2205_TI, "System.Collections.IEnumerator.Current", &m10792_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2205____Current_PropertyInfo = 
{
	&t2205_TI, "Current", &m10795_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2205_PIs[] =
{
	&t2205____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2205____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2205_m10791_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10791_GM;
MethodInfo m10791_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2205_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2205_m10791_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10791_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10792_GM;
MethodInfo m10792_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2205_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10792_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10793_GM;
MethodInfo m10793_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2205_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10793_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10794_GM;
MethodInfo m10794_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2205_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10794_GM};
extern Il2CppType t1740_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10795_GM;
MethodInfo m10795_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2205_TI, &t1740_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10795_GM};
static MethodInfo* t2205_MIs[] =
{
	&m10791_MI,
	&m10792_MI,
	&m10793_MI,
	&m10794_MI,
	&m10795_MI,
	NULL
};
extern MethodInfo m10794_MI;
extern MethodInfo m10793_MI;
static MethodInfo* t2205_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10792_MI,
	&m10794_MI,
	&m10793_MI,
	&m10795_MI,
};
static TypeInfo* t2205_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4013_TI,
};
static Il2CppInterfaceOffsetPair t2205_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4013_TI, 7},
};
extern TypeInfo t1740_TI;
static Il2CppRGCTXData t2205_RGCTXData[3] = 
{
	&m10795_MI/* Method Usage */,
	&t1740_TI/* Class Usage */,
	&m19949_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2205_0_0_0;
extern Il2CppType t2205_1_0_0;
extern Il2CppGenericClass t2205_GC;
TypeInfo t2205_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2205_MIs, t2205_PIs, t2205_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2205_TI, t2205_ITIs, t2205_VT, &EmptyCustomAttributesCache, &t2205_TI, &t2205_0_0_0, &t2205_1_0_0, t2205_IOs, &t2205_GC, NULL, NULL, NULL, t2205_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2205)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5164_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IEquatable`1<System.Char>>
extern MethodInfo m26835_MI;
extern MethodInfo m26836_MI;
static PropertyInfo t5164____Item_PropertyInfo = 
{
	&t5164_TI, "Item", &m26835_MI, &m26836_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5164_PIs[] =
{
	&t5164____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1740_0_0_0;
static ParameterInfo t5164_m26837_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1740_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26837_GM;
MethodInfo m26837_MI = 
{
	"IndexOf", NULL, &t5164_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5164_m26837_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26837_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1740_0_0_0;
static ParameterInfo t5164_m26838_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1740_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26838_GM;
MethodInfo m26838_MI = 
{
	"Insert", NULL, &t5164_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5164_m26838_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26838_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5164_m26839_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26839_GM;
MethodInfo m26839_MI = 
{
	"RemoveAt", NULL, &t5164_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5164_m26839_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26839_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5164_m26835_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1740_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26835_GM;
MethodInfo m26835_MI = 
{
	"get_Item", NULL, &t5164_TI, &t1740_0_0_0, RuntimeInvoker_t29_t44, t5164_m26835_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26835_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1740_0_0_0;
static ParameterInfo t5164_m26836_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1740_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26836_GM;
MethodInfo m26836_MI = 
{
	"set_Item", NULL, &t5164_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5164_m26836_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26836_GM};
static MethodInfo* t5164_MIs[] =
{
	&m26837_MI,
	&m26838_MI,
	&m26839_MI,
	&m26835_MI,
	&m26836_MI,
	NULL
};
static TypeInfo* t5164_ITIs[] = 
{
	&t603_TI,
	&t5163_TI,
	&t5165_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5164_0_0_0;
extern Il2CppType t5164_1_0_0;
struct t5164;
extern Il2CppGenericClass t5164_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5164_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5164_MIs, t5164_PIs, NULL, NULL, NULL, NULL, NULL, &t5164_TI, t5164_ITIs, NULL, &t1908__CustomAttributeCache, &t5164_TI, &t5164_0_0_0, &t5164_1_0_0, NULL, &t5164_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2181.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2181_TI;
#include "t2181MD.h"



extern MethodInfo m10796_MI;
 void m10796_gshared (t2181 * __this, t29 * p0, t35 p1, MethodInfo* method)
{
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m10797_MI;
 int32_t m10797_gshared (t2181 * __this, t29 * p0, t29 * p1, MethodInfo* method)
{
	if(__this->f9 != NULL)
	{
		m10797((t2181 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (t29 *, t29 * __this, t29 * p0, t29 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (t29 * __this, t29 * p0, t29 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (t29 * __this, t29 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m10798_MI;
 t29 * m10798_gshared (t2181 * __this, t29 * p0, t29 * p1, t67 * p2, t29 * p3, MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = p1;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m10799_MI;
 int32_t m10799_gshared (t2181 * __this, t29 * p0, MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Comparison`1<System.Object>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2181_m10796_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10796_GM;
MethodInfo m10796_MI = 
{
	".ctor", (methodPointerType)&m10796_gshared, &t2181_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2181_m10796_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10796_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2181_m10797_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10797_GM;
MethodInfo m10797_MI = 
{
	"Invoke", (methodPointerType)&m10797_gshared, &t2181_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2181_m10797_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10797_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2181_m10798_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10798_GM;
MethodInfo m10798_MI = 
{
	"BeginInvoke", (methodPointerType)&m10798_gshared, &t2181_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t2181_m10798_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m10798_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2181_m10799_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10799_GM;
MethodInfo m10799_MI = 
{
	"EndInvoke", (methodPointerType)&m10799_gshared, &t2181_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2181_m10799_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10799_GM};
static MethodInfo* t2181_MIs[] =
{
	&m10796_MI,
	&m10797_MI,
	&m10798_MI,
	&m10799_MI,
	NULL
};
static MethodInfo* t2181_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m10797_MI,
	&m10798_MI,
	&m10799_MI,
};
static Il2CppInterfaceOffsetPair t2181_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2181_0_0_0;
extern Il2CppType t2181_1_0_0;
struct t2181;
extern Il2CppGenericClass t2181_GC;
TypeInfo t2181_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparison`1", "System", t2181_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2181_TI, NULL, t2181_VT, &EmptyCustomAttributesCache, &t2181_TI, &t2181_0_0_0, &t2181_1_0_0, t2181_IOs, &t2181_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2181), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2174_TI;

#include "t52.h"
#include "UnityEngine.UI_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.BaseInputModule>
extern MethodInfo m26615_MI;
static PropertyInfo t2174____Count_PropertyInfo = 
{
	&t2174_TI, "Count", &m26615_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26840_MI;
static PropertyInfo t2174____IsReadOnly_PropertyInfo = 
{
	&t2174_TI, "IsReadOnly", &m26840_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2174_PIs[] =
{
	&t2174____Count_PropertyInfo,
	&t2174____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26615_GM;
MethodInfo m26615_MI = 
{
	"get_Count", NULL, &t2174_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26615_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26840_GM;
MethodInfo m26840_MI = 
{
	"get_IsReadOnly", NULL, &t2174_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26840_GM};
extern Il2CppType t52_0_0_0;
extern Il2CppType t52_0_0_0;
static ParameterInfo t2174_m26841_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26841_GM;
MethodInfo m26841_MI = 
{
	"Add", NULL, &t2174_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2174_m26841_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26841_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26842_GM;
MethodInfo m26842_MI = 
{
	"Clear", NULL, &t2174_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26842_GM};
extern Il2CppType t52_0_0_0;
static ParameterInfo t2174_m26843_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26843_GM;
MethodInfo m26843_MI = 
{
	"Contains", NULL, &t2174_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2174_m26843_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26843_GM};
extern Il2CppType t2172_0_0_0;
extern Il2CppType t2172_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2174_m26616_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2172_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26616_GM;
MethodInfo m26616_MI = 
{
	"CopyTo", NULL, &t2174_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2174_m26616_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26616_GM};
extern Il2CppType t52_0_0_0;
static ParameterInfo t2174_m26844_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26844_GM;
MethodInfo m26844_MI = 
{
	"Remove", NULL, &t2174_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2174_m26844_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26844_GM};
static MethodInfo* t2174_MIs[] =
{
	&m26615_MI,
	&m26840_MI,
	&m26841_MI,
	&m26842_MI,
	&m26843_MI,
	&m26616_MI,
	&m26844_MI,
	NULL
};
extern TypeInfo t2175_TI;
static TypeInfo* t2174_ITIs[] = 
{
	&t603_TI,
	&t2175_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2174_0_0_0;
extern Il2CppType t2174_1_0_0;
struct t2174;
extern Il2CppGenericClass t2174_GC;
TypeInfo t2174_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t2174_MIs, t2174_PIs, NULL, NULL, NULL, NULL, NULL, &t2174_TI, t2174_ITIs, NULL, &EmptyCustomAttributesCache, &t2174_TI, &t2174_0_0_0, &t2174_1_0_0, NULL, &t2174_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.BaseInputModule>
extern Il2CppType t2173_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26617_GM;
MethodInfo m26617_MI = 
{
	"GetEnumerator", NULL, &t2175_TI, &t2173_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26617_GM};
static MethodInfo* t2175_MIs[] =
{
	&m26617_MI,
	NULL
};
static TypeInfo* t2175_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2175_0_0_0;
extern Il2CppType t2175_1_0_0;
struct t2175;
extern Il2CppGenericClass t2175_GC;
TypeInfo t2175_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t2175_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2175_TI, t2175_ITIs, NULL, &EmptyCustomAttributesCache, &t2175_TI, &t2175_0_0_0, &t2175_1_0_0, NULL, &t2175_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2173_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.BaseInputModule>
extern MethodInfo m26618_MI;
static PropertyInfo t2173____Current_PropertyInfo = 
{
	&t2173_TI, "Current", &m26618_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2173_PIs[] =
{
	&t2173____Current_PropertyInfo,
	NULL
};
extern Il2CppType t52_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26618_GM;
MethodInfo m26618_MI = 
{
	"get_Current", NULL, &t2173_TI, &t52_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26618_GM};
static MethodInfo* t2173_MIs[] =
{
	&m26618_MI,
	NULL
};
static TypeInfo* t2173_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2173_0_0_0;
extern Il2CppType t2173_1_0_0;
struct t2173;
extern Il2CppGenericClass t2173_GC;
TypeInfo t2173_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2173_MIs, t2173_PIs, NULL, NULL, NULL, NULL, NULL, &t2173_TI, t2173_ITIs, NULL, &EmptyCustomAttributesCache, &t2173_TI, &t2173_0_0_0, &t2173_1_0_0, NULL, &t2173_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2206.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2206_TI;
#include "t2206MD.h"

extern TypeInfo t52_TI;
extern MethodInfo m10804_MI;
extern MethodInfo m19967_MI;
struct t20;
#define m19967(__this, p0, method) (t52 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.BaseInputModule>
extern Il2CppType t20_0_0_1;
FieldInfo t2206_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2206_TI, offsetof(t2206, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2206_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2206_TI, offsetof(t2206, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2206_FIs[] =
{
	&t2206_f0_FieldInfo,
	&t2206_f1_FieldInfo,
	NULL
};
extern MethodInfo m10801_MI;
static PropertyInfo t2206____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2206_TI, "System.Collections.IEnumerator.Current", &m10801_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2206____Current_PropertyInfo = 
{
	&t2206_TI, "Current", &m10804_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2206_PIs[] =
{
	&t2206____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2206____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2206_m10800_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10800_GM;
MethodInfo m10800_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2206_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2206_m10800_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10800_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10801_GM;
MethodInfo m10801_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2206_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10801_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10802_GM;
MethodInfo m10802_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2206_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10802_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10803_GM;
MethodInfo m10803_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2206_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10803_GM};
extern Il2CppType t52_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10804_GM;
MethodInfo m10804_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2206_TI, &t52_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10804_GM};
static MethodInfo* t2206_MIs[] =
{
	&m10800_MI,
	&m10801_MI,
	&m10802_MI,
	&m10803_MI,
	&m10804_MI,
	NULL
};
extern MethodInfo m10803_MI;
extern MethodInfo m10802_MI;
static MethodInfo* t2206_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10801_MI,
	&m10803_MI,
	&m10802_MI,
	&m10804_MI,
};
static TypeInfo* t2206_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2173_TI,
};
static Il2CppInterfaceOffsetPair t2206_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2173_TI, 7},
};
extern TypeInfo t52_TI;
static Il2CppRGCTXData t2206_RGCTXData[3] = 
{
	&m10804_MI/* Method Usage */,
	&t52_TI/* Class Usage */,
	&m19967_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2206_0_0_0;
extern Il2CppType t2206_1_0_0;
extern Il2CppGenericClass t2206_GC;
TypeInfo t2206_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2206_MIs, t2206_PIs, t2206_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2206_TI, t2206_ITIs, t2206_VT, &EmptyCustomAttributesCache, &t2206_TI, &t2206_0_0_0, &t2206_1_0_0, t2206_IOs, &t2206_GC, NULL, NULL, NULL, t2206_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2206)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2207_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.BaseInputModule>
extern MethodInfo m26845_MI;
extern MethodInfo m26846_MI;
static PropertyInfo t2207____Item_PropertyInfo = 
{
	&t2207_TI, "Item", &m26845_MI, &m26846_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2207_PIs[] =
{
	&t2207____Item_PropertyInfo,
	NULL
};
extern Il2CppType t52_0_0_0;
static ParameterInfo t2207_m26847_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26847_GM;
MethodInfo m26847_MI = 
{
	"IndexOf", NULL, &t2207_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2207_m26847_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26847_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t52_0_0_0;
static ParameterInfo t2207_m26848_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26848_GM;
MethodInfo m26848_MI = 
{
	"Insert", NULL, &t2207_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2207_m26848_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26848_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2207_m26849_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26849_GM;
MethodInfo m26849_MI = 
{
	"RemoveAt", NULL, &t2207_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2207_m26849_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26849_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2207_m26845_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t52_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26845_GM;
MethodInfo m26845_MI = 
{
	"get_Item", NULL, &t2207_TI, &t52_0_0_0, RuntimeInvoker_t29_t44, t2207_m26845_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26845_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t52_0_0_0;
static ParameterInfo t2207_m26846_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26846_GM;
MethodInfo m26846_MI = 
{
	"set_Item", NULL, &t2207_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2207_m26846_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26846_GM};
static MethodInfo* t2207_MIs[] =
{
	&m26847_MI,
	&m26848_MI,
	&m26849_MI,
	&m26845_MI,
	&m26846_MI,
	NULL
};
static TypeInfo* t2207_ITIs[] = 
{
	&t603_TI,
	&t2174_TI,
	&t2175_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2207_0_0_0;
extern Il2CppType t2207_1_0_0;
struct t2207;
extern Il2CppGenericClass t2207_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t2207_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t2207_MIs, t2207_PIs, NULL, NULL, NULL, NULL, NULL, &t2207_TI, t2207_ITIs, NULL, &t1908__CustomAttributeCache, &t2207_TI, &t2207_0_0_0, &t2207_1_0_0, NULL, &t2207_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2179.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2179_TI;
#include "t2179MD.h"

#include "t51.h"
#include "t1101.h"
extern TypeInfo t51_TI;
extern TypeInfo t7_TI;
extern TypeInfo t1101_TI;
#include "t1101MD.h"
extern MethodInfo m10808_MI;
extern MethodInfo m3974_MI;
extern MethodInfo m1430_MI;
extern MethodInfo m2998_MI;
extern MethodInfo m5150_MI;


// Metadata Definition System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.BaseInputModule>
extern Il2CppType t51_0_0_1;
FieldInfo t2179_f0_FieldInfo = 
{
	"l", &t51_0_0_1, &t2179_TI, offsetof(t2179, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2179_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t2179_TI, offsetof(t2179, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2179_f2_FieldInfo = 
{
	"ver", &t44_0_0_1, &t2179_TI, offsetof(t2179, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t52_0_0_1;
FieldInfo t2179_f3_FieldInfo = 
{
	"current", &t52_0_0_1, &t2179_TI, offsetof(t2179, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2179_FIs[] =
{
	&t2179_f0_FieldInfo,
	&t2179_f1_FieldInfo,
	&t2179_f2_FieldInfo,
	&t2179_f3_FieldInfo,
	NULL
};
extern MethodInfo m10806_MI;
static PropertyInfo t2179____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2179_TI, "System.Collections.IEnumerator.Current", &m10806_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10810_MI;
static PropertyInfo t2179____Current_PropertyInfo = 
{
	&t2179_TI, "Current", &m10810_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2179_PIs[] =
{
	&t2179____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2179____Current_PropertyInfo,
	NULL
};
extern Il2CppType t51_0_0_0;
extern Il2CppType t51_0_0_0;
static ParameterInfo t2179_m10805_ParameterInfos[] = 
{
	{"l", 0, 134217728, &EmptyCustomAttributesCache, &t51_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10805_GM;
MethodInfo m10805_MI = 
{
	".ctor", (methodPointerType)&m10631_gshared, &t2179_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2179_m10805_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10805_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10806_GM;
MethodInfo m10806_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10632_gshared, &t2179_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10806_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10807_GM;
MethodInfo m10807_MI = 
{
	"Dispose", (methodPointerType)&m10633_gshared, &t2179_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10807_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10808_GM;
MethodInfo m10808_MI = 
{
	"VerifyState", (methodPointerType)&m10634_gshared, &t2179_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10808_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10809_GM;
MethodInfo m10809_MI = 
{
	"MoveNext", (methodPointerType)&m10635_gshared, &t2179_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10809_GM};
extern Il2CppType t52_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10810_GM;
MethodInfo m10810_MI = 
{
	"get_Current", (methodPointerType)&m10636_gshared, &t2179_TI, &t52_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10810_GM};
static MethodInfo* t2179_MIs[] =
{
	&m10805_MI,
	&m10806_MI,
	&m10807_MI,
	&m10808_MI,
	&m10809_MI,
	&m10810_MI,
	NULL
};
extern MethodInfo m10809_MI;
extern MethodInfo m10807_MI;
static MethodInfo* t2179_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10806_MI,
	&m10809_MI,
	&m10807_MI,
	&m10810_MI,
};
static TypeInfo* t2179_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2173_TI,
};
static Il2CppInterfaceOffsetPair t2179_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2173_TI, 7},
};
extern TypeInfo t52_TI;
extern TypeInfo t2179_TI;
static Il2CppRGCTXData t2179_RGCTXData[3] = 
{
	&m10808_MI/* Method Usage */,
	&t52_TI/* Class Usage */,
	&t2179_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2179_0_0_0;
extern Il2CppType t2179_1_0_0;
extern Il2CppGenericClass t2179_GC;
extern TypeInfo t1261_TI;
TypeInfo t2179_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t2179_MIs, t2179_PIs, t2179_FIs, NULL, &t110_TI, NULL, &t1261_TI, &t2179_TI, t2179_ITIs, t2179_VT, &EmptyCustomAttributesCache, &t2179_TI, &t2179_0_0_0, &t2179_1_0_0, t2179_IOs, &t2179_GC, NULL, NULL, NULL, t2179_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2179)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 4, 0, 0, 8, 3, 3};
#include "t2176.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2176_TI;
#include "t2176MD.h"

#include "t345.h"
#include "t338.h"
extern TypeInfo t345_TI;
extern TypeInfo t338_TI;
extern TypeInfo t674_TI;
extern TypeInfo t21_TI;
extern TypeInfo t2172_TI;
#include "t345MD.h"
#include "t338MD.h"
#include "t2208MD.h"
extern MethodInfo m10840_MI;
extern MethodInfo m1516_MI;
extern MethodInfo m2950_MI;
extern MethodInfo m3960_MI;
extern MethodInfo m4154_MI;
extern MethodInfo m10872_MI;
extern MethodInfo m26843_MI;
extern MethodInfo m26847_MI;
extern MethodInfo m26616_MI;
extern MethodInfo m26617_MI;


// Metadata Definition System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.BaseInputModule>
extern Il2CppType t2207_0_0_1;
FieldInfo t2176_f0_FieldInfo = 
{
	"list", &t2207_0_0_1, &t2176_TI, offsetof(t2176, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2176_FIs[] =
{
	&t2176_f0_FieldInfo,
	NULL
};
extern MethodInfo m10817_MI;
extern MethodInfo m10818_MI;
static PropertyInfo t2176____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo = 
{
	&t2176_TI, "System.Collections.Generic.IList<T>.Item", &m10817_MI, &m10818_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10819_MI;
static PropertyInfo t2176____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2176_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m10819_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10829_MI;
static PropertyInfo t2176____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2176_TI, "System.Collections.ICollection.IsSynchronized", &m10829_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10830_MI;
static PropertyInfo t2176____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2176_TI, "System.Collections.ICollection.SyncRoot", &m10830_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10831_MI;
static PropertyInfo t2176____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2176_TI, "System.Collections.IList.IsFixedSize", &m10831_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10832_MI;
static PropertyInfo t2176____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2176_TI, "System.Collections.IList.IsReadOnly", &m10832_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10833_MI;
extern MethodInfo m10834_MI;
static PropertyInfo t2176____System_Collections_IList_Item_PropertyInfo = 
{
	&t2176_TI, "System.Collections.IList.Item", &m10833_MI, &m10834_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10839_MI;
static PropertyInfo t2176____Count_PropertyInfo = 
{
	&t2176_TI, "Count", &m10839_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2176____Item_PropertyInfo = 
{
	&t2176_TI, "Item", &m10840_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2176_PIs[] =
{
	&t2176____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo,
	&t2176____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2176____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2176____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2176____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2176____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2176____System_Collections_IList_Item_PropertyInfo,
	&t2176____Count_PropertyInfo,
	&t2176____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2207_0_0_0;
static ParameterInfo t2176_m10811_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2207_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10811_GM;
MethodInfo m10811_MI = 
{
	".ctor", (methodPointerType)&m10637_gshared, &t2176_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2176_m10811_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10811_GM};
extern Il2CppType t52_0_0_0;
static ParameterInfo t2176_m10812_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10812_GM;
MethodInfo m10812_MI = 
{
	"System.Collections.Generic.ICollection<T>.Add", (methodPointerType)&m10638_gshared, &t2176_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2176_m10812_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10812_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10813_GM;
MethodInfo m10813_MI = 
{
	"System.Collections.Generic.ICollection<T>.Clear", (methodPointerType)&m10639_gshared, &t2176_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10813_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t52_0_0_0;
static ParameterInfo t2176_m10814_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10814_GM;
MethodInfo m10814_MI = 
{
	"System.Collections.Generic.IList<T>.Insert", (methodPointerType)&m10640_gshared, &t2176_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2176_m10814_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10814_GM};
extern Il2CppType t52_0_0_0;
static ParameterInfo t2176_m10815_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10815_GM;
MethodInfo m10815_MI = 
{
	"System.Collections.Generic.ICollection<T>.Remove", (methodPointerType)&m10641_gshared, &t2176_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2176_m10815_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10815_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2176_m10816_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10816_GM;
MethodInfo m10816_MI = 
{
	"System.Collections.Generic.IList<T>.RemoveAt", (methodPointerType)&m10642_gshared, &t2176_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2176_m10816_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10816_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2176_m10817_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t52_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10817_GM;
MethodInfo m10817_MI = 
{
	"System.Collections.Generic.IList<T>.get_Item", (methodPointerType)&m10643_gshared, &t2176_TI, &t52_0_0_0, RuntimeInvoker_t29_t44, t2176_m10817_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10817_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t52_0_0_0;
static ParameterInfo t2176_m10818_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10818_GM;
MethodInfo m10818_MI = 
{
	"System.Collections.Generic.IList<T>.set_Item", (methodPointerType)&m10644_gshared, &t2176_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2176_m10818_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10818_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10819_GM;
MethodInfo m10819_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10645_gshared, &t2176_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10819_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2176_m10820_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10820_GM;
MethodInfo m10820_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10646_gshared, &t2176_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2176_m10820_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10820_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10821_GM;
MethodInfo m10821_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10647_gshared, &t2176_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10821_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2176_m10822_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10822_GM;
MethodInfo m10822_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10648_gshared, &t2176_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2176_m10822_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10822_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10823_GM;
MethodInfo m10823_MI = 
{
	"System.Collections.IList.Clear", (methodPointerType)&m10649_gshared, &t2176_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 14, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10823_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2176_m10824_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10824_GM;
MethodInfo m10824_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10650_gshared, &t2176_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2176_m10824_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10824_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2176_m10825_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10825_GM;
MethodInfo m10825_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10651_gshared, &t2176_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2176_m10825_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10825_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2176_m10826_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10826_GM;
MethodInfo m10826_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10652_gshared, &t2176_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2176_m10826_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10826_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2176_m10827_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10827_GM;
MethodInfo m10827_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10653_gshared, &t2176_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2176_m10827_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10827_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2176_m10828_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10828_GM;
MethodInfo m10828_MI = 
{
	"System.Collections.IList.RemoveAt", (methodPointerType)&m10654_gshared, &t2176_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2176_m10828_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10828_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10829_GM;
MethodInfo m10829_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10655_gshared, &t2176_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10829_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10830_GM;
MethodInfo m10830_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10656_gshared, &t2176_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10830_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10831_GM;
MethodInfo m10831_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10657_gshared, &t2176_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10831_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10832_GM;
MethodInfo m10832_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10658_gshared, &t2176_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10832_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2176_m10833_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10833_GM;
MethodInfo m10833_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10659_gshared, &t2176_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2176_m10833_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10833_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2176_m10834_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10834_GM;
MethodInfo m10834_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10660_gshared, &t2176_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2176_m10834_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10834_GM};
extern Il2CppType t52_0_0_0;
static ParameterInfo t2176_m10835_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10835_GM;
MethodInfo m10835_MI = 
{
	"Contains", (methodPointerType)&m10661_gshared, &t2176_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2176_m10835_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10835_GM};
extern Il2CppType t2172_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2176_m10836_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2172_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10836_GM;
MethodInfo m10836_MI = 
{
	"CopyTo", (methodPointerType)&m10662_gshared, &t2176_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2176_m10836_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10836_GM};
extern Il2CppType t2173_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10837_GM;
MethodInfo m10837_MI = 
{
	"GetEnumerator", (methodPointerType)&m10663_gshared, &t2176_TI, &t2173_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10837_GM};
extern Il2CppType t52_0_0_0;
static ParameterInfo t2176_m10838_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10838_GM;
MethodInfo m10838_MI = 
{
	"IndexOf", (methodPointerType)&m10664_gshared, &t2176_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2176_m10838_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10838_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10839_GM;
MethodInfo m10839_MI = 
{
	"get_Count", (methodPointerType)&m10665_gshared, &t2176_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10839_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2176_m10840_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t52_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10840_GM;
MethodInfo m10840_MI = 
{
	"get_Item", (methodPointerType)&m10666_gshared, &t2176_TI, &t52_0_0_0, RuntimeInvoker_t29_t44, t2176_m10840_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 33, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10840_GM};
static MethodInfo* t2176_MIs[] =
{
	&m10811_MI,
	&m10812_MI,
	&m10813_MI,
	&m10814_MI,
	&m10815_MI,
	&m10816_MI,
	&m10817_MI,
	&m10818_MI,
	&m10819_MI,
	&m10820_MI,
	&m10821_MI,
	&m10822_MI,
	&m10823_MI,
	&m10824_MI,
	&m10825_MI,
	&m10826_MI,
	&m10827_MI,
	&m10828_MI,
	&m10829_MI,
	&m10830_MI,
	&m10831_MI,
	&m10832_MI,
	&m10833_MI,
	&m10834_MI,
	&m10835_MI,
	&m10836_MI,
	&m10837_MI,
	&m10838_MI,
	&m10839_MI,
	&m10840_MI,
	NULL
};
extern MethodInfo m10821_MI;
extern MethodInfo m10820_MI;
extern MethodInfo m10822_MI;
extern MethodInfo m10823_MI;
extern MethodInfo m10824_MI;
extern MethodInfo m10825_MI;
extern MethodInfo m10826_MI;
extern MethodInfo m10827_MI;
extern MethodInfo m10828_MI;
extern MethodInfo m10812_MI;
extern MethodInfo m10813_MI;
extern MethodInfo m10835_MI;
extern MethodInfo m10836_MI;
extern MethodInfo m10815_MI;
extern MethodInfo m10838_MI;
extern MethodInfo m10814_MI;
extern MethodInfo m10816_MI;
extern MethodInfo m10837_MI;
static MethodInfo* t2176_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m10821_MI,
	&m10839_MI,
	&m10829_MI,
	&m10830_MI,
	&m10820_MI,
	&m10831_MI,
	&m10832_MI,
	&m10833_MI,
	&m10834_MI,
	&m10822_MI,
	&m10823_MI,
	&m10824_MI,
	&m10825_MI,
	&m10826_MI,
	&m10827_MI,
	&m10828_MI,
	&m10839_MI,
	&m10819_MI,
	&m10812_MI,
	&m10813_MI,
	&m10835_MI,
	&m10836_MI,
	&m10815_MI,
	&m10838_MI,
	&m10814_MI,
	&m10816_MI,
	&m10817_MI,
	&m10818_MI,
	&m10837_MI,
	&m10840_MI,
};
extern TypeInfo t868_TI;
static TypeInfo* t2176_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2174_TI,
	&t2207_TI,
	&t2175_TI,
};
static Il2CppInterfaceOffsetPair t2176_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2174_TI, 20},
	{ &t2207_TI, 27},
	{ &t2175_TI, 32},
};
extern TypeInfo t52_TI;
static Il2CppRGCTXData t2176_RGCTXData[9] = 
{
	&m10840_MI/* Method Usage */,
	&m10872_MI/* Method Usage */,
	&t52_TI/* Class Usage */,
	&m26843_MI/* Method Usage */,
	&m26847_MI/* Method Usage */,
	&m26845_MI/* Method Usage */,
	&m26616_MI/* Method Usage */,
	&m26617_MI/* Method Usage */,
	&m26615_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2176_0_0_0;
extern Il2CppType t2176_1_0_0;
struct t2176;
extern Il2CppGenericClass t2176_GC;
extern CustomAttributesCache t1263__CustomAttributeCache;
TypeInfo t2176_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ReadOnlyCollection`1", "System.Collections.ObjectModel", t2176_MIs, t2176_PIs, t2176_FIs, NULL, &t29_TI, NULL, NULL, &t2176_TI, t2176_ITIs, t2176_VT, &t1263__CustomAttributeCache, &t2176_TI, &t2176_0_0_0, &t2176_1_0_0, t2176_IOs, &t2176_GC, NULL, NULL, NULL, t2176_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2176), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 30, 9, 1, 0, 0, 34, 6, 6};
#include "t2208.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2208_TI;

#include "t51MD.h"
extern MethodInfo m10875_MI;
extern MethodInfo m10876_MI;
extern MethodInfo m10873_MI;
extern MethodInfo m10871_MI;
extern MethodInfo m1288_MI;
extern MethodInfo m9816_MI;
extern MethodInfo m10864_MI;
extern MethodInfo m10874_MI;
extern MethodInfo m10862_MI;
extern MethodInfo m10867_MI;
extern MethodInfo m10858_MI;
extern MethodInfo m26842_MI;
extern MethodInfo m26848_MI;
extern MethodInfo m26849_MI;
extern MethodInfo m6004_MI;
extern MethodInfo m9815_MI;
extern MethodInfo m9817_MI;


// Metadata Definition System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseInputModule>
extern Il2CppType t2207_0_0_1;
FieldInfo t2208_f0_FieldInfo = 
{
	"list", &t2207_0_0_1, &t2208_TI, offsetof(t2208, f0), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t2208_f1_FieldInfo = 
{
	"syncRoot", &t29_0_0_1, &t2208_TI, offsetof(t2208, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2208_FIs[] =
{
	&t2208_f0_FieldInfo,
	&t2208_f1_FieldInfo,
	NULL
};
extern MethodInfo m10842_MI;
static PropertyInfo t2208____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2208_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m10842_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10850_MI;
static PropertyInfo t2208____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2208_TI, "System.Collections.ICollection.IsSynchronized", &m10850_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10851_MI;
static PropertyInfo t2208____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2208_TI, "System.Collections.ICollection.SyncRoot", &m10851_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10852_MI;
static PropertyInfo t2208____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2208_TI, "System.Collections.IList.IsFixedSize", &m10852_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10853_MI;
static PropertyInfo t2208____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2208_TI, "System.Collections.IList.IsReadOnly", &m10853_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10854_MI;
extern MethodInfo m10855_MI;
static PropertyInfo t2208____System_Collections_IList_Item_PropertyInfo = 
{
	&t2208_TI, "System.Collections.IList.Item", &m10854_MI, &m10855_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10868_MI;
static PropertyInfo t2208____Count_PropertyInfo = 
{
	&t2208_TI, "Count", &m10868_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10869_MI;
extern MethodInfo m10870_MI;
static PropertyInfo t2208____Item_PropertyInfo = 
{
	&t2208_TI, "Item", &m10869_MI, &m10870_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2208_PIs[] =
{
	&t2208____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2208____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2208____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2208____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2208____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2208____System_Collections_IList_Item_PropertyInfo,
	&t2208____Count_PropertyInfo,
	&t2208____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10841_GM;
MethodInfo m10841_MI = 
{
	".ctor", (methodPointerType)&m10667_gshared, &t2208_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10841_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10842_GM;
MethodInfo m10842_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10668_gshared, &t2208_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10842_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2208_m10843_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10843_GM;
MethodInfo m10843_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10669_gshared, &t2208_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2208_m10843_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10843_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10844_GM;
MethodInfo m10844_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10670_gshared, &t2208_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10844_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2208_m10845_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10845_GM;
MethodInfo m10845_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10671_gshared, &t2208_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2208_m10845_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10845_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2208_m10846_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10846_GM;
MethodInfo m10846_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10672_gshared, &t2208_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2208_m10846_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10846_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2208_m10847_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10847_GM;
MethodInfo m10847_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10673_gshared, &t2208_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2208_m10847_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10847_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2208_m10848_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10848_GM;
MethodInfo m10848_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10674_gshared, &t2208_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2208_m10848_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10848_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2208_m10849_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10849_GM;
MethodInfo m10849_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10675_gshared, &t2208_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2208_m10849_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10849_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10850_GM;
MethodInfo m10850_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10676_gshared, &t2208_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10850_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10851_GM;
MethodInfo m10851_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10677_gshared, &t2208_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10851_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10852_GM;
MethodInfo m10852_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10678_gshared, &t2208_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10852_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10853_GM;
MethodInfo m10853_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10679_gshared, &t2208_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10853_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2208_m10854_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10854_GM;
MethodInfo m10854_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10680_gshared, &t2208_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2208_m10854_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10854_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2208_m10855_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10855_GM;
MethodInfo m10855_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10681_gshared, &t2208_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2208_m10855_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10855_GM};
extern Il2CppType t52_0_0_0;
static ParameterInfo t2208_m10856_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10856_GM;
MethodInfo m10856_MI = 
{
	"Add", (methodPointerType)&m10682_gshared, &t2208_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2208_m10856_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10856_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10857_GM;
MethodInfo m10857_MI = 
{
	"Clear", (methodPointerType)&m10683_gshared, &t2208_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10857_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10858_GM;
MethodInfo m10858_MI = 
{
	"ClearItems", (methodPointerType)&m10684_gshared, &t2208_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 452, 0, 33, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10858_GM};
extern Il2CppType t52_0_0_0;
static ParameterInfo t2208_m10859_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10859_GM;
MethodInfo m10859_MI = 
{
	"Contains", (methodPointerType)&m10685_gshared, &t2208_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2208_m10859_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10859_GM};
extern Il2CppType t2172_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2208_m10860_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2172_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10860_GM;
MethodInfo m10860_MI = 
{
	"CopyTo", (methodPointerType)&m10686_gshared, &t2208_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2208_m10860_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10860_GM};
extern Il2CppType t2173_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10861_GM;
MethodInfo m10861_MI = 
{
	"GetEnumerator", (methodPointerType)&m10687_gshared, &t2208_TI, &t2173_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10861_GM};
extern Il2CppType t52_0_0_0;
static ParameterInfo t2208_m10862_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10862_GM;
MethodInfo m10862_MI = 
{
	"IndexOf", (methodPointerType)&m10688_gshared, &t2208_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2208_m10862_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10862_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t52_0_0_0;
static ParameterInfo t2208_m10863_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10863_GM;
MethodInfo m10863_MI = 
{
	"Insert", (methodPointerType)&m10689_gshared, &t2208_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2208_m10863_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10863_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t52_0_0_0;
static ParameterInfo t2208_m10864_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10864_GM;
MethodInfo m10864_MI = 
{
	"InsertItem", (methodPointerType)&m10690_gshared, &t2208_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2208_m10864_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 34, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10864_GM};
extern Il2CppType t52_0_0_0;
static ParameterInfo t2208_m10865_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10865_GM;
MethodInfo m10865_MI = 
{
	"Remove", (methodPointerType)&m10691_gshared, &t2208_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2208_m10865_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10865_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2208_m10866_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10866_GM;
MethodInfo m10866_MI = 
{
	"RemoveAt", (methodPointerType)&m10692_gshared, &t2208_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2208_m10866_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10866_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2208_m10867_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10867_GM;
MethodInfo m10867_MI = 
{
	"RemoveItem", (methodPointerType)&m10693_gshared, &t2208_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2208_m10867_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 35, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10867_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10868_GM;
MethodInfo m10868_MI = 
{
	"get_Count", (methodPointerType)&m10694_gshared, &t2208_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10868_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2208_m10869_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t52_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10869_GM;
MethodInfo m10869_MI = 
{
	"get_Item", (methodPointerType)&m10695_gshared, &t2208_TI, &t52_0_0_0, RuntimeInvoker_t29_t44, t2208_m10869_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10869_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t52_0_0_0;
static ParameterInfo t2208_m10870_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10870_GM;
MethodInfo m10870_MI = 
{
	"set_Item", (methodPointerType)&m10696_gshared, &t2208_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2208_m10870_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10870_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t52_0_0_0;
static ParameterInfo t2208_m10871_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10871_GM;
MethodInfo m10871_MI = 
{
	"SetItem", (methodPointerType)&m10697_gshared, &t2208_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2208_m10871_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 36, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10871_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2208_m10872_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10872_GM;
MethodInfo m10872_MI = 
{
	"IsValidItem", (methodPointerType)&m10698_gshared, &t2208_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2208_m10872_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10872_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2208_m10873_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t52_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10873_GM;
MethodInfo m10873_MI = 
{
	"ConvertItem", (methodPointerType)&m10699_gshared, &t2208_TI, &t52_0_0_0, RuntimeInvoker_t29_t29, t2208_m10873_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10873_GM};
extern Il2CppType t2207_0_0_0;
static ParameterInfo t2208_m10874_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2207_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10874_GM;
MethodInfo m10874_MI = 
{
	"CheckWritable", (methodPointerType)&m10700_gshared, &t2208_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2208_m10874_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10874_GM};
extern Il2CppType t2207_0_0_0;
static ParameterInfo t2208_m10875_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2207_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10875_GM;
MethodInfo m10875_MI = 
{
	"IsSynchronized", (methodPointerType)&m10701_gshared, &t2208_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2208_m10875_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10875_GM};
extern Il2CppType t2207_0_0_0;
static ParameterInfo t2208_m10876_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2207_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10876_GM;
MethodInfo m10876_MI = 
{
	"IsFixedSize", (methodPointerType)&m10702_gshared, &t2208_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2208_m10876_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10876_GM};
static MethodInfo* t2208_MIs[] =
{
	&m10841_MI,
	&m10842_MI,
	&m10843_MI,
	&m10844_MI,
	&m10845_MI,
	&m10846_MI,
	&m10847_MI,
	&m10848_MI,
	&m10849_MI,
	&m10850_MI,
	&m10851_MI,
	&m10852_MI,
	&m10853_MI,
	&m10854_MI,
	&m10855_MI,
	&m10856_MI,
	&m10857_MI,
	&m10858_MI,
	&m10859_MI,
	&m10860_MI,
	&m10861_MI,
	&m10862_MI,
	&m10863_MI,
	&m10864_MI,
	&m10865_MI,
	&m10866_MI,
	&m10867_MI,
	&m10868_MI,
	&m10869_MI,
	&m10870_MI,
	&m10871_MI,
	&m10872_MI,
	&m10873_MI,
	&m10874_MI,
	&m10875_MI,
	&m10876_MI,
	NULL
};
extern MethodInfo m10844_MI;
extern MethodInfo m10843_MI;
extern MethodInfo m10845_MI;
extern MethodInfo m10857_MI;
extern MethodInfo m10846_MI;
extern MethodInfo m10847_MI;
extern MethodInfo m10848_MI;
extern MethodInfo m10849_MI;
extern MethodInfo m10866_MI;
extern MethodInfo m10856_MI;
extern MethodInfo m10859_MI;
extern MethodInfo m10860_MI;
extern MethodInfo m10865_MI;
extern MethodInfo m10863_MI;
extern MethodInfo m10861_MI;
static MethodInfo* t2208_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m10844_MI,
	&m10868_MI,
	&m10850_MI,
	&m10851_MI,
	&m10843_MI,
	&m10852_MI,
	&m10853_MI,
	&m10854_MI,
	&m10855_MI,
	&m10845_MI,
	&m10857_MI,
	&m10846_MI,
	&m10847_MI,
	&m10848_MI,
	&m10849_MI,
	&m10866_MI,
	&m10868_MI,
	&m10842_MI,
	&m10856_MI,
	&m10857_MI,
	&m10859_MI,
	&m10860_MI,
	&m10865_MI,
	&m10862_MI,
	&m10863_MI,
	&m10866_MI,
	&m10869_MI,
	&m10870_MI,
	&m10861_MI,
	&m10858_MI,
	&m10864_MI,
	&m10867_MI,
	&m10871_MI,
};
static TypeInfo* t2208_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2174_TI,
	&t2207_TI,
	&t2175_TI,
};
static Il2CppInterfaceOffsetPair t2208_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2174_TI, 20},
	{ &t2207_TI, 27},
	{ &t2175_TI, 32},
};
extern TypeInfo t51_TI;
extern TypeInfo t52_TI;
static Il2CppRGCTXData t2208_RGCTXData[25] = 
{
	&t51_TI/* Class Usage */,
	&m1288_MI/* Method Usage */,
	&m26840_MI/* Method Usage */,
	&m26617_MI/* Method Usage */,
	&m26615_MI/* Method Usage */,
	&m10873_MI/* Method Usage */,
	&m10864_MI/* Method Usage */,
	&m10872_MI/* Method Usage */,
	&t52_TI/* Class Usage */,
	&m26843_MI/* Method Usage */,
	&m26847_MI/* Method Usage */,
	&m10874_MI/* Method Usage */,
	&m10862_MI/* Method Usage */,
	&m10867_MI/* Method Usage */,
	&m10875_MI/* Method Usage */,
	&m10876_MI/* Method Usage */,
	&m26845_MI/* Method Usage */,
	&m10871_MI/* Method Usage */,
	&m10858_MI/* Method Usage */,
	&m26842_MI/* Method Usage */,
	&m26616_MI/* Method Usage */,
	&m26848_MI/* Method Usage */,
	&m26849_MI/* Method Usage */,
	&m26846_MI/* Method Usage */,
	&t52_0_0_0/* Type Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2208_0_0_0;
extern Il2CppType t2208_1_0_0;
struct t2208;
extern Il2CppGenericClass t2208_GC;
extern CustomAttributesCache t1262__CustomAttributeCache;
TypeInfo t2208_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Collection`1", "System.Collections.ObjectModel", t2208_MIs, t2208_PIs, t2208_FIs, NULL, &t29_TI, NULL, NULL, &t2208_TI, t2208_ITIs, t2208_VT, &t1262__CustomAttributeCache, &t2208_TI, &t2208_0_0_0, &t2208_1_0_0, t2208_IOs, &t2208_GC, NULL, NULL, NULL, t2208_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2208), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 36, 8, 2, 0, 0, 37, 6, 6};
#include "t2209.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2209_TI;
#include "t2209MD.h"

#include "t1257.h"
#include "t2210.h"
extern TypeInfo t6628_TI;
extern TypeInfo t1257_TI;
extern TypeInfo t2210_TI;
#include "t2210MD.h"
extern Il2CppType t6628_0_0_0;
extern Il2CppType t1257_0_0_0;
extern MethodInfo m10882_MI;
extern MethodInfo m26850_MI;
extern MethodInfo m19979_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.BaseInputModule>
extern Il2CppType t2209_0_0_49;
FieldInfo t2209_f0_FieldInfo = 
{
	"_default", &t2209_0_0_49, &t2209_TI, offsetof(t2209_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2209_FIs[] =
{
	&t2209_f0_FieldInfo,
	NULL
};
extern MethodInfo m10881_MI;
static PropertyInfo t2209____Default_PropertyInfo = 
{
	&t2209_TI, "Default", &m10881_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2209_PIs[] =
{
	&t2209____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10877_GM;
MethodInfo m10877_MI = 
{
	".ctor", (methodPointerType)&m10703_gshared, &t2209_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10877_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10878_GM;
MethodInfo m10878_MI = 
{
	".cctor", (methodPointerType)&m10704_gshared, &t2209_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10878_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2209_m10879_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10879_GM;
MethodInfo m10879_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m10705_gshared, &t2209_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2209_m10879_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10879_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2209_m10880_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10880_GM;
MethodInfo m10880_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m10706_gshared, &t2209_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2209_m10880_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10880_GM};
extern Il2CppType t52_0_0_0;
static ParameterInfo t2209_m26850_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26850_GM;
MethodInfo m26850_MI = 
{
	"GetHashCode", NULL, &t2209_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2209_m26850_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26850_GM};
extern Il2CppType t52_0_0_0;
extern Il2CppType t52_0_0_0;
static ParameterInfo t2209_m19979_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19979_GM;
MethodInfo m19979_MI = 
{
	"Equals", NULL, &t2209_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2209_m19979_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19979_GM};
extern Il2CppType t2209_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10881_GM;
MethodInfo m10881_MI = 
{
	"get_Default", (methodPointerType)&m10707_gshared, &t2209_TI, &t2209_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10881_GM};
static MethodInfo* t2209_MIs[] =
{
	&m10877_MI,
	&m10878_MI,
	&m10879_MI,
	&m10880_MI,
	&m26850_MI,
	&m19979_MI,
	&m10881_MI,
	NULL
};
extern MethodInfo m10880_MI;
extern MethodInfo m10879_MI;
static MethodInfo* t2209_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m19979_MI,
	&m26850_MI,
	&m10880_MI,
	&m10879_MI,
	NULL,
	NULL,
};
extern TypeInfo t6629_TI;
static TypeInfo* t2209_ITIs[] = 
{
	&t6629_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t2209_IOs[] = 
{
	{ &t6629_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2209_TI;
extern TypeInfo t2209_TI;
extern TypeInfo t2210_TI;
extern TypeInfo t52_TI;
static Il2CppRGCTXData t2209_RGCTXData[9] = 
{
	&t6628_0_0_0/* Type Usage */,
	&t52_0_0_0/* Type Usage */,
	&t2209_TI/* Class Usage */,
	&t2209_TI/* Static Usage */,
	&t2210_TI/* Class Usage */,
	&m10882_MI/* Method Usage */,
	&t52_TI/* Class Usage */,
	&m26850_MI/* Method Usage */,
	&m19979_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2209_0_0_0;
extern Il2CppType t2209_1_0_0;
struct t2209;
extern Il2CppGenericClass t2209_GC;
TypeInfo t2209_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t2209_MIs, t2209_PIs, t2209_FIs, NULL, &t29_TI, NULL, NULL, &t2209_TI, t2209_ITIs, t2209_VT, &EmptyCustomAttributesCache, &t2209_TI, &t2209_0_0_0, &t2209_1_0_0, t2209_IOs, &t2209_GC, NULL, NULL, NULL, t2209_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2209), 0, -1, sizeof(t2209_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<UnityEngine.EventSystems.BaseInputModule>
extern Il2CppType t52_0_0_0;
extern Il2CppType t52_0_0_0;
static ParameterInfo t6629_m26851_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26851_GM;
MethodInfo m26851_MI = 
{
	"Equals", NULL, &t6629_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t6629_m26851_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26851_GM};
extern Il2CppType t52_0_0_0;
static ParameterInfo t6629_m26852_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26852_GM;
MethodInfo m26852_MI = 
{
	"GetHashCode", NULL, &t6629_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6629_m26852_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26852_GM};
static MethodInfo* t6629_MIs[] =
{
	&m26851_MI,
	&m26852_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6629_0_0_0;
extern Il2CppType t6629_1_0_0;
struct t6629;
extern Il2CppGenericClass t6629_GC;
TypeInfo t6629_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t6629_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6629_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6629_TI, &t6629_0_0_0, &t6629_1_0_0, NULL, &t6629_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IEquatable`1<UnityEngine.EventSystems.BaseInputModule>
extern Il2CppType t52_0_0_0;
static ParameterInfo t6628_m26853_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26853_GM;
MethodInfo m26853_MI = 
{
	"Equals", NULL, &t6628_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6628_m26853_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26853_GM};
static MethodInfo* t6628_MIs[] =
{
	&m26853_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6628_1_0_0;
struct t6628;
extern Il2CppGenericClass t6628_GC;
TypeInfo t6628_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t6628_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6628_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6628_TI, &t6628_0_0_0, &t6628_1_0_0, NULL, &t6628_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m10877_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.BaseInputModule>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10882_GM;
MethodInfo m10882_MI = 
{
	".ctor", (methodPointerType)&m10738_gshared, &t2210_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10882_GM};
extern Il2CppType t52_0_0_0;
static ParameterInfo t2210_m10883_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10883_GM;
MethodInfo m10883_MI = 
{
	"GetHashCode", (methodPointerType)&m10739_gshared, &t2210_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2210_m10883_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10883_GM};
extern Il2CppType t52_0_0_0;
extern Il2CppType t52_0_0_0;
static ParameterInfo t2210_m10884_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10884_GM;
MethodInfo m10884_MI = 
{
	"Equals", (methodPointerType)&m10740_gshared, &t2210_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2210_m10884_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10884_GM};
static MethodInfo* t2210_MIs[] =
{
	&m10882_MI,
	&m10883_MI,
	&m10884_MI,
	NULL
};
extern MethodInfo m10884_MI;
extern MethodInfo m10883_MI;
static MethodInfo* t2210_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m10884_MI,
	&m10883_MI,
	&m10880_MI,
	&m10879_MI,
	&m10883_MI,
	&m10884_MI,
};
static Il2CppInterfaceOffsetPair t2210_IOs[] = 
{
	{ &t6629_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2209_TI;
extern TypeInfo t2209_TI;
extern TypeInfo t2210_TI;
extern TypeInfo t52_TI;
extern TypeInfo t52_TI;
static Il2CppRGCTXData t2210_RGCTXData[11] = 
{
	&t6628_0_0_0/* Type Usage */,
	&t52_0_0_0/* Type Usage */,
	&t2209_TI/* Class Usage */,
	&t2209_TI/* Static Usage */,
	&t2210_TI/* Class Usage */,
	&m10882_MI/* Method Usage */,
	&t52_TI/* Class Usage */,
	&m26850_MI/* Method Usage */,
	&m19979_MI/* Method Usage */,
	&m10877_MI/* Method Usage */,
	&t52_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2210_0_0_0;
extern Il2CppType t2210_1_0_0;
struct t2210;
extern Il2CppGenericClass t2210_GC;
TypeInfo t2210_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2210_MIs, NULL, NULL, NULL, &t2209_TI, NULL, &t1256_TI, &t2210_TI, NULL, t2210_VT, &EmptyCustomAttributesCache, &t2210_TI, &t2210_0_0_0, &t2210_1_0_0, t2210_IOs, &t2210_GC, NULL, NULL, NULL, t2210_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2210), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#include "t2177.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2177_TI;
#include "t2177MD.h"



// Metadata Definition System.Predicate`1<UnityEngine.EventSystems.BaseInputModule>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2177_m10885_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10885_GM;
MethodInfo m10885_MI = 
{
	".ctor", (methodPointerType)&m10741_gshared, &t2177_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2177_m10885_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10885_GM};
extern Il2CppType t52_0_0_0;
static ParameterInfo t2177_m10886_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10886_GM;
MethodInfo m10886_MI = 
{
	"Invoke", (methodPointerType)&m10742_gshared, &t2177_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2177_m10886_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10886_GM};
extern Il2CppType t52_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2177_m10887_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10887_GM;
MethodInfo m10887_MI = 
{
	"BeginInvoke", (methodPointerType)&m10743_gshared, &t2177_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2177_m10887_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m10887_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2177_m10888_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10888_GM;
MethodInfo m10888_MI = 
{
	"EndInvoke", (methodPointerType)&m10744_gshared, &t2177_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2177_m10888_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10888_GM};
static MethodInfo* t2177_MIs[] =
{
	&m10885_MI,
	&m10886_MI,
	&m10887_MI,
	&m10888_MI,
	NULL
};
extern MethodInfo m10886_MI;
extern MethodInfo m10887_MI;
extern MethodInfo m10888_MI;
static MethodInfo* t2177_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m10886_MI,
	&m10887_MI,
	&m10888_MI,
};
static Il2CppInterfaceOffsetPair t2177_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2177_0_0_0;
extern Il2CppType t2177_1_0_0;
struct t2177;
extern Il2CppGenericClass t2177_GC;
TypeInfo t2177_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Predicate`1", "System", t2177_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2177_TI, NULL, t2177_VT, &EmptyCustomAttributesCache, &t2177_TI, &t2177_0_0_0, &t2177_1_0_0, t2177_IOs, &t2177_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2177), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t2211.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2211_TI;
#include "t2211MD.h"

#include "t2212.h"
extern TypeInfo t4018_TI;
extern TypeInfo t2212_TI;
#include "t2212MD.h"
extern Il2CppType t4018_0_0_0;
extern MethodInfo m10893_MI;
extern MethodInfo m26854_MI;


// Metadata Definition System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.BaseInputModule>
extern Il2CppType t2211_0_0_49;
FieldInfo t2211_f0_FieldInfo = 
{
	"_default", &t2211_0_0_49, &t2211_TI, offsetof(t2211_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2211_FIs[] =
{
	&t2211_f0_FieldInfo,
	NULL
};
extern MethodInfo m10892_MI;
static PropertyInfo t2211____Default_PropertyInfo = 
{
	&t2211_TI, "Default", &m10892_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2211_PIs[] =
{
	&t2211____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10889_GM;
MethodInfo m10889_MI = 
{
	".ctor", (methodPointerType)&m10745_gshared, &t2211_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10889_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10890_GM;
MethodInfo m10890_MI = 
{
	".cctor", (methodPointerType)&m10746_gshared, &t2211_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10890_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2211_m10891_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10891_GM;
MethodInfo m10891_MI = 
{
	"System.Collections.IComparer.Compare", (methodPointerType)&m10747_gshared, &t2211_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2211_m10891_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10891_GM};
extern Il2CppType t52_0_0_0;
extern Il2CppType t52_0_0_0;
static ParameterInfo t2211_m26854_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26854_GM;
MethodInfo m26854_MI = 
{
	"Compare", NULL, &t2211_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2211_m26854_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26854_GM};
extern Il2CppType t2211_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10892_GM;
MethodInfo m10892_MI = 
{
	"get_Default", (methodPointerType)&m10748_gshared, &t2211_TI, &t2211_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10892_GM};
static MethodInfo* t2211_MIs[] =
{
	&m10889_MI,
	&m10890_MI,
	&m10891_MI,
	&m26854_MI,
	&m10892_MI,
	NULL
};
extern MethodInfo m10891_MI;
static MethodInfo* t2211_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m26854_MI,
	&m10891_MI,
	NULL,
};
extern TypeInfo t4017_TI;
static TypeInfo* t2211_ITIs[] = 
{
	&t4017_TI,
	&t726_TI,
};
static Il2CppInterfaceOffsetPair t2211_IOs[] = 
{
	{ &t4017_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t2211_TI;
extern TypeInfo t2211_TI;
extern TypeInfo t2212_TI;
extern TypeInfo t52_TI;
static Il2CppRGCTXData t2211_RGCTXData[8] = 
{
	&t4018_0_0_0/* Type Usage */,
	&t52_0_0_0/* Type Usage */,
	&t2211_TI/* Class Usage */,
	&t2211_TI/* Static Usage */,
	&t2212_TI/* Class Usage */,
	&m10893_MI/* Method Usage */,
	&t52_TI/* Class Usage */,
	&m26854_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2211_0_0_0;
extern Il2CppType t2211_1_0_0;
struct t2211;
extern Il2CppGenericClass t2211_GC;
TypeInfo t2211_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparer`1", "System.Collections.Generic", t2211_MIs, t2211_PIs, t2211_FIs, NULL, &t29_TI, NULL, NULL, &t2211_TI, t2211_ITIs, t2211_VT, &EmptyCustomAttributesCache, &t2211_TI, &t2211_0_0_0, &t2211_1_0_0, t2211_IOs, &t2211_GC, NULL, NULL, NULL, t2211_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2211), 0, -1, sizeof(t2211_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 5, 1, 1, 0, 0, 7, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IComparer`1<UnityEngine.EventSystems.BaseInputModule>
extern Il2CppType t52_0_0_0;
extern Il2CppType t52_0_0_0;
static ParameterInfo t4017_m19987_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19987_GM;
MethodInfo m19987_MI = 
{
	"Compare", NULL, &t4017_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t4017_m19987_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19987_GM};
static MethodInfo* t4017_MIs[] =
{
	&m19987_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4017_0_0_0;
extern Il2CppType t4017_1_0_0;
struct t4017;
extern Il2CppGenericClass t4017_GC;
TypeInfo t4017_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparer`1", "System.Collections.Generic", t4017_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4017_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4017_TI, &t4017_0_0_0, &t4017_1_0_0, NULL, &t4017_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IComparable`1<UnityEngine.EventSystems.BaseInputModule>
extern Il2CppType t52_0_0_0;
static ParameterInfo t4018_m19988_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19988_GM;
MethodInfo m19988_MI = 
{
	"CompareTo", NULL, &t4018_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t4018_m19988_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19988_GM};
static MethodInfo* t4018_MIs[] =
{
	&m19988_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4018_1_0_0;
struct t4018;
extern Il2CppGenericClass t4018_GC;
TypeInfo t4018_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t4018_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4018_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4018_TI, &t4018_0_0_0, &t4018_1_0_0, NULL, &t4018_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m10889_MI;
extern MethodInfo m19988_MI;


// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.EventSystems.BaseInputModule>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10893_GM;
MethodInfo m10893_MI = 
{
	".ctor", (methodPointerType)&m10749_gshared, &t2212_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10893_GM};
extern Il2CppType t52_0_0_0;
extern Il2CppType t52_0_0_0;
static ParameterInfo t2212_m10894_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10894_GM;
MethodInfo m10894_MI = 
{
	"Compare", (methodPointerType)&m10750_gshared, &t2212_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2212_m10894_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10894_GM};
static MethodInfo* t2212_MIs[] =
{
	&m10893_MI,
	&m10894_MI,
	NULL
};
extern MethodInfo m10894_MI;
static MethodInfo* t2212_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m10894_MI,
	&m10891_MI,
	&m10894_MI,
};
static Il2CppInterfaceOffsetPair t2212_IOs[] = 
{
	{ &t4017_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t2211_TI;
extern TypeInfo t2211_TI;
extern TypeInfo t2212_TI;
extern TypeInfo t52_TI;
extern TypeInfo t52_TI;
extern TypeInfo t4018_TI;
static Il2CppRGCTXData t2212_RGCTXData[12] = 
{
	&t4018_0_0_0/* Type Usage */,
	&t52_0_0_0/* Type Usage */,
	&t2211_TI/* Class Usage */,
	&t2211_TI/* Static Usage */,
	&t2212_TI/* Class Usage */,
	&m10893_MI/* Method Usage */,
	&t52_TI/* Class Usage */,
	&m26854_MI/* Method Usage */,
	&m10889_MI/* Method Usage */,
	&t52_TI/* Class Usage */,
	&t4018_TI/* Class Usage */,
	&m19988_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2212_0_0_0;
extern Il2CppType t2212_1_0_0;
struct t2212;
extern Il2CppGenericClass t2212_GC;
TypeInfo t2212_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2212_MIs, NULL, NULL, NULL, &t2211_TI, NULL, &t1246_TI, &t2212_TI, NULL, t2212_VT, &EmptyCustomAttributesCache, &t2212_TI, &t2212_0_0_0, &t2212_1_0_0, t2212_IOs, &t2212_GC, NULL, NULL, NULL, t2212_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2212), 0, -1, 0, 0, -1, 1048835, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 7, 0, 2};
#include "t2178.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2178_TI;
#include "t2178MD.h"



// Metadata Definition System.Comparison`1<UnityEngine.EventSystems.BaseInputModule>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2178_m10895_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10895_GM;
MethodInfo m10895_MI = 
{
	".ctor", (methodPointerType)&m10796_gshared, &t2178_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2178_m10895_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10895_GM};
extern Il2CppType t52_0_0_0;
extern Il2CppType t52_0_0_0;
static ParameterInfo t2178_m10896_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10896_GM;
MethodInfo m10896_MI = 
{
	"Invoke", (methodPointerType)&m10797_gshared, &t2178_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2178_m10896_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10896_GM};
extern Il2CppType t52_0_0_0;
extern Il2CppType t52_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2178_m10897_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t52_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10897_GM;
MethodInfo m10897_MI = 
{
	"BeginInvoke", (methodPointerType)&m10798_gshared, &t2178_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t2178_m10897_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m10897_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2178_m10898_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10898_GM;
MethodInfo m10898_MI = 
{
	"EndInvoke", (methodPointerType)&m10799_gshared, &t2178_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2178_m10898_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10898_GM};
static MethodInfo* t2178_MIs[] =
{
	&m10895_MI,
	&m10896_MI,
	&m10897_MI,
	&m10898_MI,
	NULL
};
extern MethodInfo m10896_MI;
extern MethodInfo m10897_MI;
extern MethodInfo m10898_MI;
static MethodInfo* t2178_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m10896_MI,
	&m10897_MI,
	&m10898_MI,
};
static Il2CppInterfaceOffsetPair t2178_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2178_0_0_0;
extern Il2CppType t2178_1_0_0;
struct t2178;
extern Il2CppGenericClass t2178_GC;
TypeInfo t2178_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparison`1", "System", t2178_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2178_TI, NULL, t2178_VT, &EmptyCustomAttributesCache, &t2178_TI, &t2178_0_0_0, &t2178_1_0_0, t2178_IOs, &t2178_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2178), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t54.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t54_TI;
#include "t54MD.h"

#include "t57.h"


extern MethodInfo m1289_MI;
 void m1289 (t54 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m10899_MI;
 int32_t m10899 (t54 * __this, t57  p0, t57  p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m10899((t54 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (t29 *, t29 * __this, t57  p0, t57  p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	typedef int32_t (*FunctionPointerType) (t29 * __this, t57  p0, t57  p1, MethodInfo* method);
	return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
}
extern MethodInfo m10900_MI;
 t29 * m10900 (t54 * __this, t57  p0, t57  p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = Box(InitializedTypeInfo(&t57_TI), &p0);
	__d_args[1] = Box(InitializedTypeInfo(&t57_TI), &p1);
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m10901_MI;
 int32_t m10901 (t54 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Comparison`1<UnityEngine.EventSystems.RaycastResult>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t54_m1289_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1289_GM;
MethodInfo m1289_MI = 
{
	".ctor", (methodPointerType)&m1289, &t54_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t54_m1289_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1289_GM};
extern Il2CppType t57_0_0_0;
extern Il2CppType t57_0_0_0;
extern Il2CppType t57_0_0_0;
static ParameterInfo t54_m10899_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t57_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10899_GM;
MethodInfo m10899_MI = 
{
	"Invoke", (methodPointerType)&m10899, &t54_TI, &t44_0_0_0, RuntimeInvoker_t44_t57_t57, t54_m10899_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10899_GM};
extern Il2CppType t57_0_0_0;
extern Il2CppType t57_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t54_m10900_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t57_t57_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10900_GM;
MethodInfo m10900_MI = 
{
	"BeginInvoke", (methodPointerType)&m10900, &t54_TI, &t66_0_0_0, RuntimeInvoker_t29_t57_t57_t29_t29, t54_m10900_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m10900_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t54_m10901_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10901_GM;
MethodInfo m10901_MI = 
{
	"EndInvoke", (methodPointerType)&m10901, &t54_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t54_m10901_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10901_GM};
static MethodInfo* t54_MIs[] =
{
	&m1289_MI,
	&m10899_MI,
	&m10900_MI,
	&m10901_MI,
	NULL
};
static MethodInfo* t54_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m10899_MI,
	&m10900_MI,
	&m10901_MI,
};
static Il2CppInterfaceOffsetPair t54_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t54_0_0_0;
extern Il2CppType t54_1_0_0;
struct t54;
extern Il2CppGenericClass t54_GC;
TypeInfo t54_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparison`1", "System", t54_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t54_TI, NULL, t54_VT, &EmptyCustomAttributesCache, &t54_TI, &t54_0_0_0, &t54_1_0_0, t54_IOs, &t54_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t54), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t82.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t82_TI;
#include "t82MD.h"

#include "t53.h"


// Metadata Definition UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDeselectHandler>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t82_m1353_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1353_GM;
MethodInfo m1353_MI = 
{
	".ctor", (methodPointerType)&m10902_gshared, &t82_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t82_m1353_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1353_GM};
extern Il2CppType t100_0_0_0;
extern Il2CppType t100_0_0_0;
extern Il2CppType t53_0_0_0;
extern Il2CppType t53_0_0_0;
static ParameterInfo t82_m10903_ParameterInfos[] = 
{
	{"handler", 0, 134217728, &EmptyCustomAttributesCache, &t100_0_0_0},
	{"eventData", 1, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10903_GM;
MethodInfo m10903_MI = 
{
	"Invoke", (methodPointerType)&m10904_gshared, &t82_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t82_m10903_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10903_GM};
extern Il2CppType t100_0_0_0;
extern Il2CppType t53_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t82_m10905_ParameterInfos[] = 
{
	{"handler", 0, 134217728, &EmptyCustomAttributesCache, &t100_0_0_0},
	{"eventData", 1, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10905_GM;
MethodInfo m10905_MI = 
{
	"BeginInvoke", (methodPointerType)&m10906_gshared, &t82_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t82_m10905_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m10905_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t82_m10907_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10907_GM;
MethodInfo m10907_MI = 
{
	"EndInvoke", (methodPointerType)&m10908_gshared, &t82_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t82_m10907_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10907_GM};
static MethodInfo* t82_MIs[] =
{
	&m1353_MI,
	&m10903_MI,
	&m10905_MI,
	&m10907_MI,
	NULL
};
extern MethodInfo m10903_MI;
extern MethodInfo m10905_MI;
extern MethodInfo m10907_MI;
static MethodInfo* t82_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m10903_MI,
	&m10905_MI,
	&m10907_MI,
};
static Il2CppInterfaceOffsetPair t82_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t82_0_0_0;
extern Il2CppType t82_1_0_0;
struct t82;
extern Il2CppGenericClass t82_GC;
extern TypeInfo t68_TI;
TypeInfo t82_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "EventFunction`1", "", t82_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t68_TI, &t82_TI, NULL, t82_VT, &EmptyCustomAttributesCache, &t82_TI, &t82_0_0_0, &t82_1_0_0, t82_IOs, &t82_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t82), 0, -1, 0, 0, -1, 258, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t2213.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2213_TI;
#include "t2213MD.h"



extern MethodInfo m10902_MI;
 void m10902_gshared (t2213 * __this, t29 * p0, t35 p1, MethodInfo* method)
{
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m10904_MI;
 void m10904_gshared (t2213 * __this, t29 * p0, t53 * p1, MethodInfo* method)
{
	if(__this->f9 != NULL)
	{
		m10904((t2213 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (t29 *, t29 * __this, t29 * p0, t53 * p1, MethodInfo* method);
		((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (t29 * __this, t29 * p0, t53 * p1, MethodInfo* method);
		((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef void (*FunctionPointerType) (t29 * __this, t53 * p1, MethodInfo* method);
		((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m10906_MI;
 t29 * m10906_gshared (t2213 * __this, t29 * p0, t53 * p1, t67 * p2, t29 * p3, MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = p1;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m10908_MI;
 void m10908_gshared (t2213 * __this, t29 * p0, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
}
// Metadata Definition UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2213_m10902_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10902_GM;
MethodInfo m10902_MI = 
{
	".ctor", (methodPointerType)&m10902_gshared, &t2213_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2213_m10902_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10902_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t53_0_0_0;
static ParameterInfo t2213_m10904_ParameterInfos[] = 
{
	{"handler", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"eventData", 1, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10904_GM;
MethodInfo m10904_MI = 
{
	"Invoke", (methodPointerType)&m10904_gshared, &t2213_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2213_m10904_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10904_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t53_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2213_m10906_ParameterInfos[] = 
{
	{"handler", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"eventData", 1, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10906_GM;
MethodInfo m10906_MI = 
{
	"BeginInvoke", (methodPointerType)&m10906_gshared, &t2213_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t2213_m10906_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m10906_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2213_m10908_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10908_GM;
MethodInfo m10908_MI = 
{
	"EndInvoke", (methodPointerType)&m10908_gshared, &t2213_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2213_m10908_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10908_GM};
static MethodInfo* t2213_MIs[] =
{
	&m10902_MI,
	&m10904_MI,
	&m10906_MI,
	&m10908_MI,
	NULL
};
static MethodInfo* t2213_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m10904_MI,
	&m10906_MI,
	&m10908_MI,
};
static Il2CppInterfaceOffsetPair t2213_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t2213_0_0_0;
extern Il2CppType t2213_1_0_0;
struct t2213;
extern Il2CppGenericClass t2213_GC;
TypeInfo t2213_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "EventFunction`1", "", t2213_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t68_TI, &t2213_TI, NULL, t2213_VT, &EmptyCustomAttributesCache, &t2213_TI, &t2213_0_0_0, &t2213_1_0_0, t2213_IOs, &t2213_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2213), 0, -1, 0, 0, -1, 258, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t105.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t105_TI;
#include "t105MD.h"

#include "t2221.h"
#include "t2218.h"
#include "t2219.h"
#include "t2225.h"
#include "t2220.h"
extern TypeInfo t31_TI;
extern TypeInfo t585_TI;
extern TypeInfo t1649_TI;
extern TypeInfo t915_TI;
extern TypeInfo t2214_TI;
extern TypeInfo t2221_TI;
extern TypeInfo t2216_TI;
extern TypeInfo t2217_TI;
extern TypeInfo t2215_TI;
extern TypeInfo t2218_TI;
extern TypeInfo t2219_TI;
extern TypeInfo t2225_TI;
#include "t915MD.h"
#include "t602MD.h"
#include "t2218MD.h"
#include "t2219MD.h"
#include "t2221MD.h"
#include "t2225MD.h"
extern MethodInfo m10955_MI;
extern MethodInfo m10956_MI;
extern MethodInfo m4198_MI;
extern MethodInfo m19994_MI;
extern MethodInfo m3975_MI;
extern MethodInfo m10941_MI;
extern MethodInfo m10938_MI;
extern MethodInfo m5952_MI;
extern MethodInfo m10927_MI;
extern MethodInfo m10933_MI;
extern MethodInfo m10939_MI;
extern MethodInfo m10942_MI;
extern MethodInfo m10944_MI;
extern MethodInfo m10928_MI;
extern MethodInfo m10952_MI;
extern MethodInfo m5139_MI;
extern MethodInfo m10953_MI;
extern MethodInfo m26364_MI;
extern MethodInfo m26368_MI;
extern MethodInfo m26370_MI;
extern MethodInfo m26371_MI;
extern MethodInfo m3972_MI;
extern MethodInfo m1428_MI;
extern MethodInfo m10943_MI;
extern MethodInfo m10929_MI;
extern MethodInfo m10930_MI;
extern MethodInfo m10963_MI;
extern MethodInfo m5112_MI;
extern MethodInfo m19996_MI;
extern MethodInfo m10936_MI;
extern MethodInfo m10937_MI;
extern MethodInfo m11038_MI;
extern MethodInfo m10957_MI;
extern MethodInfo m10940_MI;
extern MethodInfo m10946_MI;
extern MethodInfo m5171_MI;
extern MethodInfo m11044_MI;
extern MethodInfo m19998_MI;
extern MethodInfo m20006_MI;
extern MethodInfo m5951_MI;
struct t20;
struct t20;
 void m19788_gshared (t29 * __this, t316** p0, int32_t p1, MethodInfo* method);
#define m19788(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
#define m19994(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
struct t20;
#include "t2223.h"
struct t20;
#include "t2001.h"
 int32_t m10120_gshared (t29 * __this, t316* p0, t29 * p1, int32_t p2, int32_t p3, MethodInfo* method);
#define m10120(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
#define m19996(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
struct t20;
struct t20;
 void m19857_gshared (t29 * __this, t316* p0, int32_t p1, int32_t p2, t29* p3, MethodInfo* method);
#define m19857(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
#define m19998(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
struct t20;
#include "t295.h"
struct t20;
 void m19964_gshared (t29 * __this, t316* p0, int32_t p1, t2181 * p2, MethodInfo* method);
#define m19964(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)
#define m20006(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)


 t2221  m10938 (t105 * __this, MethodInfo* method){
	{
		t2221  L_0 = {0};
		m10957(&L_0, __this, &m10957_MI);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>
extern Il2CppType t44_0_0_32849;
FieldInfo t105_f0_FieldInfo = 
{
	"DefaultCapacity", &t44_0_0_32849, &t105_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t2214_0_0_1;
FieldInfo t105_f1_FieldInfo = 
{
	"_items", &t2214_0_0_1, &t105_TI, offsetof(t105, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t105_f2_FieldInfo = 
{
	"_size", &t44_0_0_1, &t105_TI, offsetof(t105, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t105_f3_FieldInfo = 
{
	"_version", &t44_0_0_1, &t105_TI, offsetof(t105, f3), &EmptyCustomAttributesCache};
extern Il2CppType t2214_0_0_49;
FieldInfo t105_f4_FieldInfo = 
{
	"EmptyArray", &t2214_0_0_49, &t105_TI, offsetof(t105_SFs, f4), &EmptyCustomAttributesCache};
static FieldInfo* t105_FIs[] =
{
	&t105_f0_FieldInfo,
	&t105_f1_FieldInfo,
	&t105_f2_FieldInfo,
	&t105_f3_FieldInfo,
	&t105_f4_FieldInfo,
	NULL
};
static const int32_t t105_f0_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t105_f0_DefaultValue = 
{
	&t105_f0_FieldInfo, { (char*)&t105_f0_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t105_FDVs[] = 
{
	&t105_f0_DefaultValue,
	NULL
};
extern MethodInfo m10920_MI;
static PropertyInfo t105____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t105_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m10920_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10921_MI;
static PropertyInfo t105____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t105_TI, "System.Collections.ICollection.IsSynchronized", &m10921_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10922_MI;
static PropertyInfo t105____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t105_TI, "System.Collections.ICollection.SyncRoot", &m10922_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10923_MI;
static PropertyInfo t105____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t105_TI, "System.Collections.IList.IsFixedSize", &m10923_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10924_MI;
static PropertyInfo t105____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t105_TI, "System.Collections.IList.IsReadOnly", &m10924_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10925_MI;
extern MethodInfo m10926_MI;
static PropertyInfo t105____System_Collections_IList_Item_PropertyInfo = 
{
	&t105_TI, "System.Collections.IList.Item", &m10925_MI, &m10926_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t105____Capacity_PropertyInfo = 
{
	&t105_TI, "Capacity", &m10952_MI, &m10953_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10954_MI;
static PropertyInfo t105____Count_PropertyInfo = 
{
	&t105_TI, "Count", &m10954_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t105____Item_PropertyInfo = 
{
	&t105_TI, "Item", &m10955_MI, &m10956_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t105_PIs[] =
{
	&t105____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t105____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t105____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t105____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t105____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t105____System_Collections_IList_Item_PropertyInfo,
	&t105____Capacity_PropertyInfo,
	&t105____Count_PropertyInfo,
	&t105____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10909_GM;
MethodInfo m10909_MI = 
{
	".ctor", (methodPointerType)&m10537_gshared, &t105_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10909_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t105_m10910_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10910_GM;
MethodInfo m10910_MI = 
{
	".ctor", (methodPointerType)&m10539_gshared, &t105_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t105_m10910_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10910_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10911_GM;
MethodInfo m10911_MI = 
{
	".cctor", (methodPointerType)&m10541_gshared, &t105_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10911_GM};
extern Il2CppType t2215_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10912_GM;
MethodInfo m10912_MI = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator", (methodPointerType)&m10543_gshared, &t105_TI, &t2215_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 27, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10912_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t105_m10913_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10913_GM;
MethodInfo m10913_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10545_gshared, &t105_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t105_m10913_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10913_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10914_GM;
MethodInfo m10914_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10547_gshared, &t105_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10914_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t105_m10915_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10915_GM;
MethodInfo m10915_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10549_gshared, &t105_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t105_m10915_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10915_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t105_m10916_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10916_GM;
MethodInfo m10916_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10551_gshared, &t105_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t105_m10916_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10916_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t105_m10917_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10917_GM;
MethodInfo m10917_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10553_gshared, &t105_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t105_m10917_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10917_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t105_m10918_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10918_GM;
MethodInfo m10918_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10555_gshared, &t105_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t105_m10918_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10918_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t105_m10919_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10919_GM;
MethodInfo m10919_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10557_gshared, &t105_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t105_m10919_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10919_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10920_GM;
MethodInfo m10920_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10559_gshared, &t105_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10920_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10921_GM;
MethodInfo m10921_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10561_gshared, &t105_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10921_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10922_GM;
MethodInfo m10922_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10563_gshared, &t105_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10922_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10923_GM;
MethodInfo m10923_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10565_gshared, &t105_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10923_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10924_GM;
MethodInfo m10924_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10567_gshared, &t105_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10924_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t105_m10925_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10925_GM;
MethodInfo m10925_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10569_gshared, &t105_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t105_m10925_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10925_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t105_m10926_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10926_GM;
MethodInfo m10926_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10571_gshared, &t105_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t105_m10926_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10926_GM};
extern Il2CppType t31_0_0_0;
extern Il2CppType t31_0_0_0;
static ParameterInfo t105_m10927_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10927_GM;
MethodInfo m10927_MI = 
{
	"Add", (methodPointerType)&m10573_gshared, &t105_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t105_m10927_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10927_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t105_m10928_ParameterInfos[] = 
{
	{"newCount", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10928_GM;
MethodInfo m10928_MI = 
{
	"GrowIfNeeded", (methodPointerType)&m10575_gshared, &t105_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t105_m10928_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10928_GM};
extern Il2CppType t2216_0_0_0;
extern Il2CppType t2216_0_0_0;
static ParameterInfo t105_m10929_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2216_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10929_GM;
MethodInfo m10929_MI = 
{
	"AddCollection", (methodPointerType)&m10577_gshared, &t105_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t105_m10929_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10929_GM};
extern Il2CppType t2217_0_0_0;
extern Il2CppType t2217_0_0_0;
static ParameterInfo t105_m10930_ParameterInfos[] = 
{
	{"enumerable", 0, 134217728, &EmptyCustomAttributesCache, &t2217_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10930_GM;
MethodInfo m10930_MI = 
{
	"AddEnumerable", (methodPointerType)&m10579_gshared, &t105_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t105_m10930_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10930_GM};
extern Il2CppType t2217_0_0_0;
static ParameterInfo t105_m10931_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2217_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10931_GM;
MethodInfo m10931_MI = 
{
	"AddRange", (methodPointerType)&m10581_gshared, &t105_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t105_m10931_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10931_GM};
extern Il2CppType t2218_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10932_GM;
MethodInfo m10932_MI = 
{
	"AsReadOnly", (methodPointerType)&m10583_gshared, &t105_TI, &t2218_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10932_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1366_GM;
MethodInfo m1366_MI = 
{
	"Clear", (methodPointerType)&m10585_gshared, &t105_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1366_GM};
extern Il2CppType t31_0_0_0;
static ParameterInfo t105_m10933_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10933_GM;
MethodInfo m10933_MI = 
{
	"Contains", (methodPointerType)&m10587_gshared, &t105_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t105_m10933_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10933_GM};
extern Il2CppType t2214_0_0_0;
extern Il2CppType t2214_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t105_m10934_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2214_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10934_GM;
MethodInfo m10934_MI = 
{
	"CopyTo", (methodPointerType)&m10589_gshared, &t105_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t105_m10934_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10934_GM};
extern Il2CppType t2219_0_0_0;
extern Il2CppType t2219_0_0_0;
static ParameterInfo t105_m10935_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2219_0_0_0},
};
extern Il2CppType t31_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10935_GM;
MethodInfo m10935_MI = 
{
	"Find", (methodPointerType)&m10591_gshared, &t105_TI, &t31_0_0_0, RuntimeInvoker_t29_t29, t105_m10935_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10935_GM};
extern Il2CppType t2219_0_0_0;
static ParameterInfo t105_m10936_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2219_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10936_GM;
MethodInfo m10936_MI = 
{
	"CheckMatch", (methodPointerType)&m10593_gshared, &t105_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t105_m10936_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10936_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2219_0_0_0;
static ParameterInfo t105_m10937_ParameterInfos[] = 
{
	{"startIndex", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"count", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"match", 2, 134217728, &EmptyCustomAttributesCache, &t2219_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10937_GM;
MethodInfo m10937_MI = 
{
	"GetIndex", (methodPointerType)&m10595_gshared, &t105_TI, &t44_0_0_0, RuntimeInvoker_t44_t44_t44_t29, t105_m10937_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m10937_GM};
extern Il2CppType t2221_0_0_0;
extern void* RuntimeInvoker_t2221 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10938_GM;
MethodInfo m10938_MI = 
{
	"GetEnumerator", (methodPointerType)&m10938, &t105_TI, &t2221_0_0_0, RuntimeInvoker_t2221, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10938_GM};
extern Il2CppType t31_0_0_0;
static ParameterInfo t105_m10939_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10939_GM;
MethodInfo m10939_MI = 
{
	"IndexOf", (methodPointerType)&m10598_gshared, &t105_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t105_m10939_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10939_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t105_m10940_ParameterInfos[] = 
{
	{"start", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"delta", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10940_GM;
MethodInfo m10940_MI = 
{
	"Shift", (methodPointerType)&m10600_gshared, &t105_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t105_m10940_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10940_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t105_m10941_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10941_GM;
MethodInfo m10941_MI = 
{
	"CheckIndex", (methodPointerType)&m10602_gshared, &t105_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t105_m10941_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10941_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t31_0_0_0;
static ParameterInfo t105_m10942_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10942_GM;
MethodInfo m10942_MI = 
{
	"Insert", (methodPointerType)&m10604_gshared, &t105_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t105_m10942_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10942_GM};
extern Il2CppType t2217_0_0_0;
static ParameterInfo t105_m10943_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2217_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10943_GM;
MethodInfo m10943_MI = 
{
	"CheckCollection", (methodPointerType)&m10606_gshared, &t105_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t105_m10943_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10943_GM};
extern Il2CppType t31_0_0_0;
static ParameterInfo t105_m10944_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10944_GM;
MethodInfo m10944_MI = 
{
	"Remove", (methodPointerType)&m10608_gshared, &t105_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t105_m10944_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10944_GM};
extern Il2CppType t2219_0_0_0;
static ParameterInfo t105_m10945_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2219_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10945_GM;
MethodInfo m10945_MI = 
{
	"RemoveAll", (methodPointerType)&m10610_gshared, &t105_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t105_m10945_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10945_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t105_m10946_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10946_GM;
MethodInfo m10946_MI = 
{
	"RemoveAt", (methodPointerType)&m10611_gshared, &t105_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t105_m10946_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10946_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10947_GM;
MethodInfo m10947_MI = 
{
	"Reverse", (methodPointerType)&m10613_gshared, &t105_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10947_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10948_GM;
MethodInfo m10948_MI = 
{
	"Sort", (methodPointerType)&m10615_gshared, &t105_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10948_GM};
extern Il2CppType t2220_0_0_0;
extern Il2CppType t2220_0_0_0;
static ParameterInfo t105_m10949_ParameterInfos[] = 
{
	{"comparison", 0, 134217728, &EmptyCustomAttributesCache, &t2220_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10949_GM;
MethodInfo m10949_MI = 
{
	"Sort", (methodPointerType)&m10617_gshared, &t105_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t105_m10949_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10949_GM};
extern Il2CppType t2214_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10950_GM;
MethodInfo m10950_MI = 
{
	"ToArray", (methodPointerType)&m10619_gshared, &t105_TI, &t2214_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10950_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10951_GM;
MethodInfo m10951_MI = 
{
	"TrimExcess", (methodPointerType)&m10621_gshared, &t105_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10951_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10952_GM;
MethodInfo m10952_MI = 
{
	"get_Capacity", (methodPointerType)&m10623_gshared, &t105_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10952_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t105_m10953_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10953_GM;
MethodInfo m10953_MI = 
{
	"set_Capacity", (methodPointerType)&m10625_gshared, &t105_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t105_m10953_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10953_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10954_GM;
MethodInfo m10954_MI = 
{
	"get_Count", (methodPointerType)&m10626_gshared, &t105_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10954_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t105_m10955_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t31_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10955_GM;
MethodInfo m10955_MI = 
{
	"get_Item", (methodPointerType)&m10627_gshared, &t105_TI, &t31_0_0_0, RuntimeInvoker_t29_t44, t105_m10955_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10955_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t31_0_0_0;
static ParameterInfo t105_m10956_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10956_GM;
MethodInfo m10956_MI = 
{
	"set_Item", (methodPointerType)&m10629_gshared, &t105_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t105_m10956_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 32, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10956_GM};
static MethodInfo* t105_MIs[] =
{
	&m10909_MI,
	&m10910_MI,
	&m10911_MI,
	&m10912_MI,
	&m10913_MI,
	&m10914_MI,
	&m10915_MI,
	&m10916_MI,
	&m10917_MI,
	&m10918_MI,
	&m10919_MI,
	&m10920_MI,
	&m10921_MI,
	&m10922_MI,
	&m10923_MI,
	&m10924_MI,
	&m10925_MI,
	&m10926_MI,
	&m10927_MI,
	&m10928_MI,
	&m10929_MI,
	&m10930_MI,
	&m10931_MI,
	&m10932_MI,
	&m1366_MI,
	&m10933_MI,
	&m10934_MI,
	&m10935_MI,
	&m10936_MI,
	&m10937_MI,
	&m10938_MI,
	&m10939_MI,
	&m10940_MI,
	&m10941_MI,
	&m10942_MI,
	&m10943_MI,
	&m10944_MI,
	&m10945_MI,
	&m10946_MI,
	&m10947_MI,
	&m10948_MI,
	&m10949_MI,
	&m10950_MI,
	&m10951_MI,
	&m10952_MI,
	&m10953_MI,
	&m10954_MI,
	&m10955_MI,
	&m10956_MI,
	NULL
};
extern MethodInfo m10914_MI;
extern MethodInfo m10913_MI;
extern MethodInfo m10915_MI;
extern MethodInfo m1366_MI;
extern MethodInfo m10916_MI;
extern MethodInfo m10917_MI;
extern MethodInfo m10918_MI;
extern MethodInfo m10919_MI;
extern MethodInfo m10934_MI;
extern MethodInfo m10912_MI;
static MethodInfo* t105_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m10914_MI,
	&m10954_MI,
	&m10921_MI,
	&m10922_MI,
	&m10913_MI,
	&m10923_MI,
	&m10924_MI,
	&m10925_MI,
	&m10926_MI,
	&m10915_MI,
	&m1366_MI,
	&m10916_MI,
	&m10917_MI,
	&m10918_MI,
	&m10919_MI,
	&m10946_MI,
	&m10954_MI,
	&m10920_MI,
	&m10927_MI,
	&m1366_MI,
	&m10933_MI,
	&m10934_MI,
	&m10944_MI,
	&m10912_MI,
	&m10939_MI,
	&m10942_MI,
	&m10946_MI,
	&m10955_MI,
	&m10956_MI,
};
extern TypeInfo t312_TI;
static TypeInfo* t105_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2216_TI,
	&t2217_TI,
	&t312_TI,
};
static Il2CppInterfaceOffsetPair t105_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2216_TI, 20},
	{ &t2217_TI, 27},
	{ &t312_TI, 28},
};
extern TypeInfo t105_TI;
extern TypeInfo t2214_TI;
extern TypeInfo t2221_TI;
extern TypeInfo t31_TI;
extern TypeInfo t2216_TI;
extern TypeInfo t2218_TI;
static Il2CppRGCTXData t105_RGCTXData[37] = 
{
	&t105_TI/* Static Usage */,
	&t2214_TI/* Array Usage */,
	&m10938_MI/* Method Usage */,
	&t2221_TI/* Class Usage */,
	&t31_TI/* Class Usage */,
	&m10927_MI/* Method Usage */,
	&m10933_MI/* Method Usage */,
	&m10939_MI/* Method Usage */,
	&m10941_MI/* Method Usage */,
	&m10942_MI/* Method Usage */,
	&m10944_MI/* Method Usage */,
	&m10955_MI/* Method Usage */,
	&m10956_MI/* Method Usage */,
	&m10928_MI/* Method Usage */,
	&m10952_MI/* Method Usage */,
	&m10953_MI/* Method Usage */,
	&m26364_MI/* Method Usage */,
	&m26368_MI/* Method Usage */,
	&m26370_MI/* Method Usage */,
	&m26371_MI/* Method Usage */,
	&m10943_MI/* Method Usage */,
	&t2216_TI/* Class Usage */,
	&m10929_MI/* Method Usage */,
	&m10930_MI/* Method Usage */,
	&t2218_TI/* Class Usage */,
	&m10963_MI/* Method Usage */,
	&m19996_MI/* Method Usage */,
	&m10936_MI/* Method Usage */,
	&m10937_MI/* Method Usage */,
	&m11038_MI/* Method Usage */,
	&m10957_MI/* Method Usage */,
	&m10940_MI/* Method Usage */,
	&m10946_MI/* Method Usage */,
	&m11044_MI/* Method Usage */,
	&m19998_MI/* Method Usage */,
	&m20006_MI/* Method Usage */,
	&m19994_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t105_0_0_0;
extern Il2CppType t105_1_0_0;
struct t105;
extern Il2CppGenericClass t105_GC;
extern CustomAttributesCache t1261__CustomAttributeCache;
TypeInfo t105_TI = 
{
	&g_mscorlib_dll_Image, NULL, "List`1", "System.Collections.Generic", t105_MIs, t105_PIs, t105_FIs, NULL, &t29_TI, NULL, NULL, &t105_TI, t105_ITIs, t105_VT, &t1261__CustomAttributeCache, &t105_TI, &t105_0_0_0, &t105_1_0_0, t105_IOs, &t105_GC, NULL, t105_FDVs, NULL, t105_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t105), 0, -1, sizeof(t105_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, true, false, false, 49, 9, 5, 0, 0, 33, 6, 6};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m10960_MI;


// Metadata Definition System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.IEventSystemHandler>
extern Il2CppType t105_0_0_1;
FieldInfo t2221_f0_FieldInfo = 
{
	"l", &t105_0_0_1, &t2221_TI, offsetof(t2221, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2221_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t2221_TI, offsetof(t2221, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2221_f2_FieldInfo = 
{
	"ver", &t44_0_0_1, &t2221_TI, offsetof(t2221, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t31_0_0_1;
FieldInfo t2221_f3_FieldInfo = 
{
	"current", &t31_0_0_1, &t2221_TI, offsetof(t2221, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2221_FIs[] =
{
	&t2221_f0_FieldInfo,
	&t2221_f1_FieldInfo,
	&t2221_f2_FieldInfo,
	&t2221_f3_FieldInfo,
	NULL
};
extern MethodInfo m10958_MI;
static PropertyInfo t2221____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2221_TI, "System.Collections.IEnumerator.Current", &m10958_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10962_MI;
static PropertyInfo t2221____Current_PropertyInfo = 
{
	&t2221_TI, "Current", &m10962_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2221_PIs[] =
{
	&t2221____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2221____Current_PropertyInfo,
	NULL
};
extern Il2CppType t105_0_0_0;
static ParameterInfo t2221_m10957_ParameterInfos[] = 
{
	{"l", 0, 134217728, &EmptyCustomAttributesCache, &t105_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10957_GM;
MethodInfo m10957_MI = 
{
	".ctor", (methodPointerType)&m10631_gshared, &t2221_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2221_m10957_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10957_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10958_GM;
MethodInfo m10958_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10632_gshared, &t2221_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10958_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10959_GM;
MethodInfo m10959_MI = 
{
	"Dispose", (methodPointerType)&m10633_gshared, &t2221_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10959_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10960_GM;
MethodInfo m10960_MI = 
{
	"VerifyState", (methodPointerType)&m10634_gshared, &t2221_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10960_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10961_GM;
MethodInfo m10961_MI = 
{
	"MoveNext", (methodPointerType)&m10635_gshared, &t2221_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10961_GM};
extern Il2CppType t31_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10962_GM;
MethodInfo m10962_MI = 
{
	"get_Current", (methodPointerType)&m10636_gshared, &t2221_TI, &t31_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10962_GM};
static MethodInfo* t2221_MIs[] =
{
	&m10957_MI,
	&m10958_MI,
	&m10959_MI,
	&m10960_MI,
	&m10961_MI,
	&m10962_MI,
	NULL
};
extern MethodInfo m10961_MI;
extern MethodInfo m10959_MI;
static MethodInfo* t2221_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m10958_MI,
	&m10961_MI,
	&m10959_MI,
	&m10962_MI,
};
static TypeInfo* t2221_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2215_TI,
};
static Il2CppInterfaceOffsetPair t2221_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2215_TI, 7},
};
extern TypeInfo t31_TI;
extern TypeInfo t2221_TI;
static Il2CppRGCTXData t2221_RGCTXData[3] = 
{
	&m10960_MI/* Method Usage */,
	&t31_TI/* Class Usage */,
	&t2221_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2221_0_0_0;
extern Il2CppType t2221_1_0_0;
extern Il2CppGenericClass t2221_GC;
TypeInfo t2221_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t2221_MIs, t2221_PIs, t2221_FIs, NULL, &t110_TI, NULL, &t1261_TI, &t2221_TI, t2221_ITIs, t2221_VT, &EmptyCustomAttributesCache, &t2221_TI, &t2221_0_0_0, &t2221_1_0_0, t2221_IOs, &t2221_GC, NULL, NULL, NULL, t2221_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2221)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 4, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif

#include "t2222MD.h"
extern MethodInfo m10992_MI;
extern MethodInfo m26372_MI;
extern MethodInfo m11024_MI;
extern MethodInfo m26367_MI;
extern MethodInfo m26374_MI;


// Metadata Definition System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.IEventSystemHandler>
extern Il2CppType t312_0_0_1;
FieldInfo t2218_f0_FieldInfo = 
{
	"list", &t312_0_0_1, &t2218_TI, offsetof(t2218, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2218_FIs[] =
{
	&t2218_f0_FieldInfo,
	NULL
};
extern MethodInfo m10969_MI;
extern MethodInfo m10970_MI;
static PropertyInfo t2218____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo = 
{
	&t2218_TI, "System.Collections.Generic.IList<T>.Item", &m10969_MI, &m10970_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10971_MI;
static PropertyInfo t2218____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2218_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m10971_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10981_MI;
static PropertyInfo t2218____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2218_TI, "System.Collections.ICollection.IsSynchronized", &m10981_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10982_MI;
static PropertyInfo t2218____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2218_TI, "System.Collections.ICollection.SyncRoot", &m10982_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10983_MI;
static PropertyInfo t2218____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2218_TI, "System.Collections.IList.IsFixedSize", &m10983_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10984_MI;
static PropertyInfo t2218____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2218_TI, "System.Collections.IList.IsReadOnly", &m10984_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10985_MI;
extern MethodInfo m10986_MI;
static PropertyInfo t2218____System_Collections_IList_Item_PropertyInfo = 
{
	&t2218_TI, "System.Collections.IList.Item", &m10985_MI, &m10986_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10991_MI;
static PropertyInfo t2218____Count_PropertyInfo = 
{
	&t2218_TI, "Count", &m10991_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2218____Item_PropertyInfo = 
{
	&t2218_TI, "Item", &m10992_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2218_PIs[] =
{
	&t2218____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo,
	&t2218____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2218____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2218____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2218____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2218____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2218____System_Collections_IList_Item_PropertyInfo,
	&t2218____Count_PropertyInfo,
	&t2218____Item_PropertyInfo,
	NULL
};
extern Il2CppType t312_0_0_0;
extern Il2CppType t312_0_0_0;
static ParameterInfo t2218_m10963_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t312_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10963_GM;
MethodInfo m10963_MI = 
{
	".ctor", (methodPointerType)&m10637_gshared, &t2218_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2218_m10963_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10963_GM};
extern Il2CppType t31_0_0_0;
static ParameterInfo t2218_m10964_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10964_GM;
MethodInfo m10964_MI = 
{
	"System.Collections.Generic.ICollection<T>.Add", (methodPointerType)&m10638_gshared, &t2218_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2218_m10964_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10964_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10965_GM;
MethodInfo m10965_MI = 
{
	"System.Collections.Generic.ICollection<T>.Clear", (methodPointerType)&m10639_gshared, &t2218_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10965_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t31_0_0_0;
static ParameterInfo t2218_m10966_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10966_GM;
MethodInfo m10966_MI = 
{
	"System.Collections.Generic.IList<T>.Insert", (methodPointerType)&m10640_gshared, &t2218_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2218_m10966_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10966_GM};
extern Il2CppType t31_0_0_0;
static ParameterInfo t2218_m10967_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10967_GM;
MethodInfo m10967_MI = 
{
	"System.Collections.Generic.ICollection<T>.Remove", (methodPointerType)&m10641_gshared, &t2218_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2218_m10967_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10967_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2218_m10968_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10968_GM;
MethodInfo m10968_MI = 
{
	"System.Collections.Generic.IList<T>.RemoveAt", (methodPointerType)&m10642_gshared, &t2218_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2218_m10968_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10968_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2218_m10969_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t31_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10969_GM;
MethodInfo m10969_MI = 
{
	"System.Collections.Generic.IList<T>.get_Item", (methodPointerType)&m10643_gshared, &t2218_TI, &t31_0_0_0, RuntimeInvoker_t29_t44, t2218_m10969_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10969_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t31_0_0_0;
static ParameterInfo t2218_m10970_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10970_GM;
MethodInfo m10970_MI = 
{
	"System.Collections.Generic.IList<T>.set_Item", (methodPointerType)&m10644_gshared, &t2218_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2218_m10970_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10970_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10971_GM;
MethodInfo m10971_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10645_gshared, &t2218_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10971_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2218_m10972_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10972_GM;
MethodInfo m10972_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10646_gshared, &t2218_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2218_m10972_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10972_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10973_GM;
MethodInfo m10973_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10647_gshared, &t2218_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10973_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2218_m10974_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10974_GM;
MethodInfo m10974_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10648_gshared, &t2218_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2218_m10974_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10974_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10975_GM;
MethodInfo m10975_MI = 
{
	"System.Collections.IList.Clear", (methodPointerType)&m10649_gshared, &t2218_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 14, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10975_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2218_m10976_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10976_GM;
MethodInfo m10976_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10650_gshared, &t2218_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2218_m10976_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10976_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2218_m10977_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10977_GM;
MethodInfo m10977_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10651_gshared, &t2218_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2218_m10977_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10977_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2218_m10978_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10978_GM;
MethodInfo m10978_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10652_gshared, &t2218_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2218_m10978_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10978_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2218_m10979_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10979_GM;
MethodInfo m10979_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10653_gshared, &t2218_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2218_m10979_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10979_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2218_m10980_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10980_GM;
MethodInfo m10980_MI = 
{
	"System.Collections.IList.RemoveAt", (methodPointerType)&m10654_gshared, &t2218_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2218_m10980_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10980_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10981_GM;
MethodInfo m10981_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10655_gshared, &t2218_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10981_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10982_GM;
MethodInfo m10982_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10656_gshared, &t2218_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10982_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10983_GM;
MethodInfo m10983_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10657_gshared, &t2218_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10983_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10984_GM;
MethodInfo m10984_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10658_gshared, &t2218_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10984_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2218_m10985_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10985_GM;
MethodInfo m10985_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10659_gshared, &t2218_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2218_m10985_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10985_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2218_m10986_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10986_GM;
MethodInfo m10986_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10660_gshared, &t2218_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2218_m10986_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10986_GM};
extern Il2CppType t31_0_0_0;
static ParameterInfo t2218_m10987_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10987_GM;
MethodInfo m10987_MI = 
{
	"Contains", (methodPointerType)&m10661_gshared, &t2218_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2218_m10987_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10987_GM};
extern Il2CppType t2214_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2218_m10988_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2214_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10988_GM;
MethodInfo m10988_MI = 
{
	"CopyTo", (methodPointerType)&m10662_gshared, &t2218_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2218_m10988_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10988_GM};
extern Il2CppType t2215_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10989_GM;
MethodInfo m10989_MI = 
{
	"GetEnumerator", (methodPointerType)&m10663_gshared, &t2218_TI, &t2215_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10989_GM};
extern Il2CppType t31_0_0_0;
static ParameterInfo t2218_m10990_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10990_GM;
MethodInfo m10990_MI = 
{
	"IndexOf", (methodPointerType)&m10664_gshared, &t2218_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2218_m10990_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10990_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10991_GM;
MethodInfo m10991_MI = 
{
	"get_Count", (methodPointerType)&m10665_gshared, &t2218_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10991_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2218_m10992_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t31_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10992_GM;
MethodInfo m10992_MI = 
{
	"get_Item", (methodPointerType)&m10666_gshared, &t2218_TI, &t31_0_0_0, RuntimeInvoker_t29_t44, t2218_m10992_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 33, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10992_GM};
static MethodInfo* t2218_MIs[] =
{
	&m10963_MI,
	&m10964_MI,
	&m10965_MI,
	&m10966_MI,
	&m10967_MI,
	&m10968_MI,
	&m10969_MI,
	&m10970_MI,
	&m10971_MI,
	&m10972_MI,
	&m10973_MI,
	&m10974_MI,
	&m10975_MI,
	&m10976_MI,
	&m10977_MI,
	&m10978_MI,
	&m10979_MI,
	&m10980_MI,
	&m10981_MI,
	&m10982_MI,
	&m10983_MI,
	&m10984_MI,
	&m10985_MI,
	&m10986_MI,
	&m10987_MI,
	&m10988_MI,
	&m10989_MI,
	&m10990_MI,
	&m10991_MI,
	&m10992_MI,
	NULL
};
extern MethodInfo m10973_MI;
extern MethodInfo m10972_MI;
extern MethodInfo m10974_MI;
extern MethodInfo m10975_MI;
extern MethodInfo m10976_MI;
extern MethodInfo m10977_MI;
extern MethodInfo m10978_MI;
extern MethodInfo m10979_MI;
extern MethodInfo m10980_MI;
extern MethodInfo m10964_MI;
extern MethodInfo m10965_MI;
extern MethodInfo m10987_MI;
extern MethodInfo m10988_MI;
extern MethodInfo m10967_MI;
extern MethodInfo m10990_MI;
extern MethodInfo m10966_MI;
extern MethodInfo m10968_MI;
extern MethodInfo m10989_MI;
static MethodInfo* t2218_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m10973_MI,
	&m10991_MI,
	&m10981_MI,
	&m10982_MI,
	&m10972_MI,
	&m10983_MI,
	&m10984_MI,
	&m10985_MI,
	&m10986_MI,
	&m10974_MI,
	&m10975_MI,
	&m10976_MI,
	&m10977_MI,
	&m10978_MI,
	&m10979_MI,
	&m10980_MI,
	&m10991_MI,
	&m10971_MI,
	&m10964_MI,
	&m10965_MI,
	&m10987_MI,
	&m10988_MI,
	&m10967_MI,
	&m10990_MI,
	&m10966_MI,
	&m10968_MI,
	&m10969_MI,
	&m10970_MI,
	&m10989_MI,
	&m10992_MI,
};
static TypeInfo* t2218_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2216_TI,
	&t312_TI,
	&t2217_TI,
};
static Il2CppInterfaceOffsetPair t2218_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2216_TI, 20},
	{ &t312_TI, 27},
	{ &t2217_TI, 32},
};
extern TypeInfo t31_TI;
static Il2CppRGCTXData t2218_RGCTXData[9] = 
{
	&m10992_MI/* Method Usage */,
	&m11024_MI/* Method Usage */,
	&t31_TI/* Class Usage */,
	&m26367_MI/* Method Usage */,
	&m26374_MI/* Method Usage */,
	&m26372_MI/* Method Usage */,
	&m26368_MI/* Method Usage */,
	&m26370_MI/* Method Usage */,
	&m26364_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2218_0_0_0;
extern Il2CppType t2218_1_0_0;
struct t2218;
extern Il2CppGenericClass t2218_GC;
extern CustomAttributesCache t1263__CustomAttributeCache;
TypeInfo t2218_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ReadOnlyCollection`1", "System.Collections.ObjectModel", t2218_MIs, t2218_PIs, t2218_FIs, NULL, &t29_TI, NULL, NULL, &t2218_TI, t2218_ITIs, t2218_VT, &t1263__CustomAttributeCache, &t2218_TI, &t2218_0_0_0, &t2218_1_0_0, t2218_IOs, &t2218_GC, NULL, NULL, NULL, t2218_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2218), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 30, 9, 1, 0, 0, 34, 6, 6};
#include "t2222.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2222_TI;

extern MethodInfo m26365_MI;
extern MethodInfo m11027_MI;
extern MethodInfo m11028_MI;
extern MethodInfo m11025_MI;
extern MethodInfo m11023_MI;
extern MethodInfo m10909_MI;
extern MethodInfo m11016_MI;
extern MethodInfo m11026_MI;
extern MethodInfo m11014_MI;
extern MethodInfo m11019_MI;
extern MethodInfo m11010_MI;
extern MethodInfo m26366_MI;
extern MethodInfo m26375_MI;
extern MethodInfo m26376_MI;
extern MethodInfo m26373_MI;


// Metadata Definition System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.IEventSystemHandler>
extern Il2CppType t312_0_0_1;
FieldInfo t2222_f0_FieldInfo = 
{
	"list", &t312_0_0_1, &t2222_TI, offsetof(t2222, f0), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t2222_f1_FieldInfo = 
{
	"syncRoot", &t29_0_0_1, &t2222_TI, offsetof(t2222, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2222_FIs[] =
{
	&t2222_f0_FieldInfo,
	&t2222_f1_FieldInfo,
	NULL
};
extern MethodInfo m10994_MI;
static PropertyInfo t2222____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2222_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m10994_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11002_MI;
static PropertyInfo t2222____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2222_TI, "System.Collections.ICollection.IsSynchronized", &m11002_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11003_MI;
static PropertyInfo t2222____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2222_TI, "System.Collections.ICollection.SyncRoot", &m11003_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11004_MI;
static PropertyInfo t2222____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2222_TI, "System.Collections.IList.IsFixedSize", &m11004_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11005_MI;
static PropertyInfo t2222____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2222_TI, "System.Collections.IList.IsReadOnly", &m11005_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11006_MI;
extern MethodInfo m11007_MI;
static PropertyInfo t2222____System_Collections_IList_Item_PropertyInfo = 
{
	&t2222_TI, "System.Collections.IList.Item", &m11006_MI, &m11007_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11020_MI;
static PropertyInfo t2222____Count_PropertyInfo = 
{
	&t2222_TI, "Count", &m11020_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11021_MI;
extern MethodInfo m11022_MI;
static PropertyInfo t2222____Item_PropertyInfo = 
{
	&t2222_TI, "Item", &m11021_MI, &m11022_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2222_PIs[] =
{
	&t2222____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2222____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2222____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2222____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2222____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2222____System_Collections_IList_Item_PropertyInfo,
	&t2222____Count_PropertyInfo,
	&t2222____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10993_GM;
MethodInfo m10993_MI = 
{
	".ctor", (methodPointerType)&m10667_gshared, &t2222_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10993_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10994_GM;
MethodInfo m10994_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10668_gshared, &t2222_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10994_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2222_m10995_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10995_GM;
MethodInfo m10995_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10669_gshared, &t2222_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2222_m10995_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m10995_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10996_GM;
MethodInfo m10996_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10670_gshared, &t2222_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10996_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2222_m10997_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10997_GM;
MethodInfo m10997_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10671_gshared, &t2222_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2222_m10997_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10997_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2222_m10998_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10998_GM;
MethodInfo m10998_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10672_gshared, &t2222_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2222_m10998_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10998_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2222_m10999_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10999_GM;
MethodInfo m10999_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10673_gshared, &t2222_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2222_m10999_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m10999_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2222_m11000_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11000_GM;
MethodInfo m11000_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10674_gshared, &t2222_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2222_m11000_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11000_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2222_m11001_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11001_GM;
MethodInfo m11001_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10675_gshared, &t2222_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2222_m11001_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11001_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11002_GM;
MethodInfo m11002_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10676_gshared, &t2222_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11002_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11003_GM;
MethodInfo m11003_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10677_gshared, &t2222_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11003_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11004_GM;
MethodInfo m11004_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10678_gshared, &t2222_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11004_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11005_GM;
MethodInfo m11005_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10679_gshared, &t2222_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11005_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2222_m11006_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11006_GM;
MethodInfo m11006_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10680_gshared, &t2222_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2222_m11006_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11006_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2222_m11007_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11007_GM;
MethodInfo m11007_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10681_gshared, &t2222_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2222_m11007_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11007_GM};
extern Il2CppType t31_0_0_0;
static ParameterInfo t2222_m11008_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11008_GM;
MethodInfo m11008_MI = 
{
	"Add", (methodPointerType)&m10682_gshared, &t2222_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2222_m11008_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11008_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11009_GM;
MethodInfo m11009_MI = 
{
	"Clear", (methodPointerType)&m10683_gshared, &t2222_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11009_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11010_GM;
MethodInfo m11010_MI = 
{
	"ClearItems", (methodPointerType)&m10684_gshared, &t2222_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 452, 0, 33, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11010_GM};
extern Il2CppType t31_0_0_0;
static ParameterInfo t2222_m11011_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11011_GM;
MethodInfo m11011_MI = 
{
	"Contains", (methodPointerType)&m10685_gshared, &t2222_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2222_m11011_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11011_GM};
extern Il2CppType t2214_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2222_m11012_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2214_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11012_GM;
MethodInfo m11012_MI = 
{
	"CopyTo", (methodPointerType)&m10686_gshared, &t2222_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2222_m11012_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11012_GM};
extern Il2CppType t2215_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11013_GM;
MethodInfo m11013_MI = 
{
	"GetEnumerator", (methodPointerType)&m10687_gshared, &t2222_TI, &t2215_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11013_GM};
extern Il2CppType t31_0_0_0;
static ParameterInfo t2222_m11014_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11014_GM;
MethodInfo m11014_MI = 
{
	"IndexOf", (methodPointerType)&m10688_gshared, &t2222_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2222_m11014_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11014_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t31_0_0_0;
static ParameterInfo t2222_m11015_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11015_GM;
MethodInfo m11015_MI = 
{
	"Insert", (methodPointerType)&m10689_gshared, &t2222_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2222_m11015_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11015_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t31_0_0_0;
static ParameterInfo t2222_m11016_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11016_GM;
MethodInfo m11016_MI = 
{
	"InsertItem", (methodPointerType)&m10690_gshared, &t2222_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2222_m11016_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 34, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11016_GM};
extern Il2CppType t31_0_0_0;
static ParameterInfo t2222_m11017_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11017_GM;
MethodInfo m11017_MI = 
{
	"Remove", (methodPointerType)&m10691_gshared, &t2222_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2222_m11017_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11017_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2222_m11018_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11018_GM;
MethodInfo m11018_MI = 
{
	"RemoveAt", (methodPointerType)&m10692_gshared, &t2222_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2222_m11018_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11018_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2222_m11019_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11019_GM;
MethodInfo m11019_MI = 
{
	"RemoveItem", (methodPointerType)&m10693_gshared, &t2222_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2222_m11019_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 35, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11019_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11020_GM;
MethodInfo m11020_MI = 
{
	"get_Count", (methodPointerType)&m10694_gshared, &t2222_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11020_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2222_m11021_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t31_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11021_GM;
MethodInfo m11021_MI = 
{
	"get_Item", (methodPointerType)&m10695_gshared, &t2222_TI, &t31_0_0_0, RuntimeInvoker_t29_t44, t2222_m11021_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11021_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t31_0_0_0;
static ParameterInfo t2222_m11022_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11022_GM;
MethodInfo m11022_MI = 
{
	"set_Item", (methodPointerType)&m10696_gshared, &t2222_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2222_m11022_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11022_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t31_0_0_0;
static ParameterInfo t2222_m11023_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11023_GM;
MethodInfo m11023_MI = 
{
	"SetItem", (methodPointerType)&m10697_gshared, &t2222_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2222_m11023_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 36, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11023_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2222_m11024_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11024_GM;
MethodInfo m11024_MI = 
{
	"IsValidItem", (methodPointerType)&m10698_gshared, &t2222_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2222_m11024_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11024_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2222_m11025_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t31_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11025_GM;
MethodInfo m11025_MI = 
{
	"ConvertItem", (methodPointerType)&m10699_gshared, &t2222_TI, &t31_0_0_0, RuntimeInvoker_t29_t29, t2222_m11025_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11025_GM};
extern Il2CppType t312_0_0_0;
static ParameterInfo t2222_m11026_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t312_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11026_GM;
MethodInfo m11026_MI = 
{
	"CheckWritable", (methodPointerType)&m10700_gshared, &t2222_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2222_m11026_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11026_GM};
extern Il2CppType t312_0_0_0;
static ParameterInfo t2222_m11027_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t312_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11027_GM;
MethodInfo m11027_MI = 
{
	"IsSynchronized", (methodPointerType)&m10701_gshared, &t2222_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2222_m11027_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11027_GM};
extern Il2CppType t312_0_0_0;
static ParameterInfo t2222_m11028_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t312_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11028_GM;
MethodInfo m11028_MI = 
{
	"IsFixedSize", (methodPointerType)&m10702_gshared, &t2222_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2222_m11028_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11028_GM};
static MethodInfo* t2222_MIs[] =
{
	&m10993_MI,
	&m10994_MI,
	&m10995_MI,
	&m10996_MI,
	&m10997_MI,
	&m10998_MI,
	&m10999_MI,
	&m11000_MI,
	&m11001_MI,
	&m11002_MI,
	&m11003_MI,
	&m11004_MI,
	&m11005_MI,
	&m11006_MI,
	&m11007_MI,
	&m11008_MI,
	&m11009_MI,
	&m11010_MI,
	&m11011_MI,
	&m11012_MI,
	&m11013_MI,
	&m11014_MI,
	&m11015_MI,
	&m11016_MI,
	&m11017_MI,
	&m11018_MI,
	&m11019_MI,
	&m11020_MI,
	&m11021_MI,
	&m11022_MI,
	&m11023_MI,
	&m11024_MI,
	&m11025_MI,
	&m11026_MI,
	&m11027_MI,
	&m11028_MI,
	NULL
};
extern MethodInfo m10996_MI;
extern MethodInfo m10995_MI;
extern MethodInfo m10997_MI;
extern MethodInfo m11009_MI;
extern MethodInfo m10998_MI;
extern MethodInfo m10999_MI;
extern MethodInfo m11000_MI;
extern MethodInfo m11001_MI;
extern MethodInfo m11018_MI;
extern MethodInfo m11008_MI;
extern MethodInfo m11011_MI;
extern MethodInfo m11012_MI;
extern MethodInfo m11017_MI;
extern MethodInfo m11015_MI;
extern MethodInfo m11013_MI;
static MethodInfo* t2222_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m10996_MI,
	&m11020_MI,
	&m11002_MI,
	&m11003_MI,
	&m10995_MI,
	&m11004_MI,
	&m11005_MI,
	&m11006_MI,
	&m11007_MI,
	&m10997_MI,
	&m11009_MI,
	&m10998_MI,
	&m10999_MI,
	&m11000_MI,
	&m11001_MI,
	&m11018_MI,
	&m11020_MI,
	&m10994_MI,
	&m11008_MI,
	&m11009_MI,
	&m11011_MI,
	&m11012_MI,
	&m11017_MI,
	&m11014_MI,
	&m11015_MI,
	&m11018_MI,
	&m11021_MI,
	&m11022_MI,
	&m11013_MI,
	&m11010_MI,
	&m11016_MI,
	&m11019_MI,
	&m11023_MI,
};
static TypeInfo* t2222_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2216_TI,
	&t312_TI,
	&t2217_TI,
};
static Il2CppInterfaceOffsetPair t2222_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2216_TI, 20},
	{ &t312_TI, 27},
	{ &t2217_TI, 32},
};
extern TypeInfo t105_TI;
extern TypeInfo t31_TI;
static Il2CppRGCTXData t2222_RGCTXData[25] = 
{
	&t105_TI/* Class Usage */,
	&m10909_MI/* Method Usage */,
	&m26365_MI/* Method Usage */,
	&m26370_MI/* Method Usage */,
	&m26364_MI/* Method Usage */,
	&m11025_MI/* Method Usage */,
	&m11016_MI/* Method Usage */,
	&m11024_MI/* Method Usage */,
	&t31_TI/* Class Usage */,
	&m26367_MI/* Method Usage */,
	&m26374_MI/* Method Usage */,
	&m11026_MI/* Method Usage */,
	&m11014_MI/* Method Usage */,
	&m11019_MI/* Method Usage */,
	&m11027_MI/* Method Usage */,
	&m11028_MI/* Method Usage */,
	&m26372_MI/* Method Usage */,
	&m11023_MI/* Method Usage */,
	&m11010_MI/* Method Usage */,
	&m26366_MI/* Method Usage */,
	&m26368_MI/* Method Usage */,
	&m26375_MI/* Method Usage */,
	&m26376_MI/* Method Usage */,
	&m26373_MI/* Method Usage */,
	&t31_0_0_0/* Type Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2222_0_0_0;
extern Il2CppType t2222_1_0_0;
struct t2222;
extern Il2CppGenericClass t2222_GC;
extern CustomAttributesCache t1262__CustomAttributeCache;
TypeInfo t2222_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Collection`1", "System.Collections.ObjectModel", t2222_MIs, t2222_PIs, t2222_FIs, NULL, &t29_TI, NULL, NULL, &t2222_TI, t2222_ITIs, t2222_VT, &t1262__CustomAttributeCache, &t2222_TI, &t2222_0_0_0, &t2222_1_0_0, t2222_IOs, &t2222_GC, NULL, NULL, NULL, t2222_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2222), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 36, 8, 2, 0, 0, 37, 6, 6};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2223_TI;
#include "t2223MD.h"

#include "t2224.h"
extern TypeInfo t6630_TI;
extern TypeInfo t2224_TI;
#include "t2224MD.h"
extern Il2CppType t6630_0_0_0;
extern MethodInfo m11034_MI;
extern MethodInfo m26855_MI;
extern MethodInfo m19995_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.IEventSystemHandler>
extern Il2CppType t2223_0_0_49;
FieldInfo t2223_f0_FieldInfo = 
{
	"_default", &t2223_0_0_49, &t2223_TI, offsetof(t2223_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2223_FIs[] =
{
	&t2223_f0_FieldInfo,
	NULL
};
extern MethodInfo m11033_MI;
static PropertyInfo t2223____Default_PropertyInfo = 
{
	&t2223_TI, "Default", &m11033_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2223_PIs[] =
{
	&t2223____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11029_GM;
MethodInfo m11029_MI = 
{
	".ctor", (methodPointerType)&m10703_gshared, &t2223_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11029_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11030_GM;
MethodInfo m11030_MI = 
{
	".cctor", (methodPointerType)&m10704_gshared, &t2223_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11030_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2223_m11031_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11031_GM;
MethodInfo m11031_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m10705_gshared, &t2223_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2223_m11031_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11031_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2223_m11032_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11032_GM;
MethodInfo m11032_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m10706_gshared, &t2223_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2223_m11032_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11032_GM};
extern Il2CppType t31_0_0_0;
static ParameterInfo t2223_m26855_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26855_GM;
MethodInfo m26855_MI = 
{
	"GetHashCode", NULL, &t2223_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2223_m26855_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26855_GM};
extern Il2CppType t31_0_0_0;
extern Il2CppType t31_0_0_0;
static ParameterInfo t2223_m19995_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19995_GM;
MethodInfo m19995_MI = 
{
	"Equals", NULL, &t2223_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2223_m19995_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19995_GM};
extern Il2CppType t2223_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11033_GM;
MethodInfo m11033_MI = 
{
	"get_Default", (methodPointerType)&m10707_gshared, &t2223_TI, &t2223_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11033_GM};
static MethodInfo* t2223_MIs[] =
{
	&m11029_MI,
	&m11030_MI,
	&m11031_MI,
	&m11032_MI,
	&m26855_MI,
	&m19995_MI,
	&m11033_MI,
	NULL
};
extern MethodInfo m11032_MI;
extern MethodInfo m11031_MI;
static MethodInfo* t2223_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m19995_MI,
	&m26855_MI,
	&m11032_MI,
	&m11031_MI,
	NULL,
	NULL,
};
extern TypeInfo t6631_TI;
static TypeInfo* t2223_ITIs[] = 
{
	&t6631_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t2223_IOs[] = 
{
	{ &t6631_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2223_TI;
extern TypeInfo t2223_TI;
extern TypeInfo t2224_TI;
extern TypeInfo t31_TI;
static Il2CppRGCTXData t2223_RGCTXData[9] = 
{
	&t6630_0_0_0/* Type Usage */,
	&t31_0_0_0/* Type Usage */,
	&t2223_TI/* Class Usage */,
	&t2223_TI/* Static Usage */,
	&t2224_TI/* Class Usage */,
	&m11034_MI/* Method Usage */,
	&t31_TI/* Class Usage */,
	&m26855_MI/* Method Usage */,
	&m19995_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2223_0_0_0;
extern Il2CppType t2223_1_0_0;
struct t2223;
extern Il2CppGenericClass t2223_GC;
TypeInfo t2223_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t2223_MIs, t2223_PIs, t2223_FIs, NULL, &t29_TI, NULL, NULL, &t2223_TI, t2223_ITIs, t2223_VT, &EmptyCustomAttributesCache, &t2223_TI, &t2223_0_0_0, &t2223_1_0_0, t2223_IOs, &t2223_GC, NULL, NULL, NULL, t2223_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2223), 0, -1, sizeof(t2223_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<UnityEngine.EventSystems.IEventSystemHandler>
extern Il2CppType t31_0_0_0;
extern Il2CppType t31_0_0_0;
static ParameterInfo t6631_m26856_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26856_GM;
MethodInfo m26856_MI = 
{
	"Equals", NULL, &t6631_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t6631_m26856_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26856_GM};
extern Il2CppType t31_0_0_0;
static ParameterInfo t6631_m26857_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26857_GM;
MethodInfo m26857_MI = 
{
	"GetHashCode", NULL, &t6631_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6631_m26857_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26857_GM};
static MethodInfo* t6631_MIs[] =
{
	&m26856_MI,
	&m26857_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6631_0_0_0;
extern Il2CppType t6631_1_0_0;
struct t6631;
extern Il2CppGenericClass t6631_GC;
TypeInfo t6631_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t6631_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6631_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6631_TI, &t6631_0_0_0, &t6631_1_0_0, NULL, &t6631_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IEquatable`1<UnityEngine.EventSystems.IEventSystemHandler>
extern Il2CppType t31_0_0_0;
static ParameterInfo t6630_m26858_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26858_GM;
MethodInfo m26858_MI = 
{
	"Equals", NULL, &t6630_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6630_m26858_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26858_GM};
static MethodInfo* t6630_MIs[] =
{
	&m26858_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6630_1_0_0;
struct t6630;
extern Il2CppGenericClass t6630_GC;
TypeInfo t6630_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t6630_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6630_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6630_TI, &t6630_0_0_0, &t6630_1_0_0, NULL, &t6630_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m11029_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.IEventSystemHandler>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11034_GM;
MethodInfo m11034_MI = 
{
	".ctor", (methodPointerType)&m10738_gshared, &t2224_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11034_GM};
extern Il2CppType t31_0_0_0;
static ParameterInfo t2224_m11035_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11035_GM;
MethodInfo m11035_MI = 
{
	"GetHashCode", (methodPointerType)&m10739_gshared, &t2224_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2224_m11035_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11035_GM};
extern Il2CppType t31_0_0_0;
extern Il2CppType t31_0_0_0;
static ParameterInfo t2224_m11036_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11036_GM;
MethodInfo m11036_MI = 
{
	"Equals", (methodPointerType)&m10740_gshared, &t2224_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2224_m11036_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11036_GM};
static MethodInfo* t2224_MIs[] =
{
	&m11034_MI,
	&m11035_MI,
	&m11036_MI,
	NULL
};
extern MethodInfo m11036_MI;
extern MethodInfo m11035_MI;
static MethodInfo* t2224_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m11036_MI,
	&m11035_MI,
	&m11032_MI,
	&m11031_MI,
	&m11035_MI,
	&m11036_MI,
};
static Il2CppInterfaceOffsetPair t2224_IOs[] = 
{
	{ &t6631_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2223_TI;
extern TypeInfo t2223_TI;
extern TypeInfo t2224_TI;
extern TypeInfo t31_TI;
extern TypeInfo t31_TI;
static Il2CppRGCTXData t2224_RGCTXData[11] = 
{
	&t6630_0_0_0/* Type Usage */,
	&t31_0_0_0/* Type Usage */,
	&t2223_TI/* Class Usage */,
	&t2223_TI/* Static Usage */,
	&t2224_TI/* Class Usage */,
	&m11034_MI/* Method Usage */,
	&t31_TI/* Class Usage */,
	&m26855_MI/* Method Usage */,
	&m19995_MI/* Method Usage */,
	&m11029_MI/* Method Usage */,
	&t31_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2224_0_0_0;
extern Il2CppType t2224_1_0_0;
struct t2224;
extern Il2CppGenericClass t2224_GC;
TypeInfo t2224_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2224_MIs, NULL, NULL, NULL, &t2223_TI, NULL, &t1256_TI, &t2224_TI, NULL, t2224_VT, &EmptyCustomAttributesCache, &t2224_TI, &t2224_0_0_0, &t2224_1_0_0, t2224_IOs, &t2224_GC, NULL, NULL, NULL, t2224_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2224), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Predicate`1<UnityEngine.EventSystems.IEventSystemHandler>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2219_m11037_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11037_GM;
MethodInfo m11037_MI = 
{
	".ctor", (methodPointerType)&m10741_gshared, &t2219_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2219_m11037_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11037_GM};
extern Il2CppType t31_0_0_0;
static ParameterInfo t2219_m11038_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11038_GM;
MethodInfo m11038_MI = 
{
	"Invoke", (methodPointerType)&m10742_gshared, &t2219_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2219_m11038_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11038_GM};
extern Il2CppType t31_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2219_m11039_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11039_GM;
MethodInfo m11039_MI = 
{
	"BeginInvoke", (methodPointerType)&m10743_gshared, &t2219_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2219_m11039_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m11039_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2219_m11040_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11040_GM;
MethodInfo m11040_MI = 
{
	"EndInvoke", (methodPointerType)&m10744_gshared, &t2219_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2219_m11040_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11040_GM};
static MethodInfo* t2219_MIs[] =
{
	&m11037_MI,
	&m11038_MI,
	&m11039_MI,
	&m11040_MI,
	NULL
};
extern MethodInfo m11039_MI;
extern MethodInfo m11040_MI;
static MethodInfo* t2219_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m11038_MI,
	&m11039_MI,
	&m11040_MI,
};
static Il2CppInterfaceOffsetPair t2219_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2219_1_0_0;
struct t2219;
extern Il2CppGenericClass t2219_GC;
TypeInfo t2219_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Predicate`1", "System", t2219_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2219_TI, NULL, t2219_VT, &EmptyCustomAttributesCache, &t2219_TI, &t2219_0_0_0, &t2219_1_0_0, t2219_IOs, &t2219_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2219), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif

#include "t2226.h"
extern TypeInfo t4021_TI;
extern TypeInfo t2226_TI;
#include "t2226MD.h"
extern Il2CppType t4021_0_0_0;
extern MethodInfo m11045_MI;
extern MethodInfo m26859_MI;


// Metadata Definition System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.IEventSystemHandler>
extern Il2CppType t2225_0_0_49;
FieldInfo t2225_f0_FieldInfo = 
{
	"_default", &t2225_0_0_49, &t2225_TI, offsetof(t2225_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2225_FIs[] =
{
	&t2225_f0_FieldInfo,
	NULL
};
static PropertyInfo t2225____Default_PropertyInfo = 
{
	&t2225_TI, "Default", &m11044_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2225_PIs[] =
{
	&t2225____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11041_GM;
MethodInfo m11041_MI = 
{
	".ctor", (methodPointerType)&m10745_gshared, &t2225_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11041_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11042_GM;
MethodInfo m11042_MI = 
{
	".cctor", (methodPointerType)&m10746_gshared, &t2225_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11042_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2225_m11043_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11043_GM;
MethodInfo m11043_MI = 
{
	"System.Collections.IComparer.Compare", (methodPointerType)&m10747_gshared, &t2225_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2225_m11043_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11043_GM};
extern Il2CppType t31_0_0_0;
extern Il2CppType t31_0_0_0;
static ParameterInfo t2225_m26859_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26859_GM;
MethodInfo m26859_MI = 
{
	"Compare", NULL, &t2225_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2225_m26859_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26859_GM};
extern Il2CppType t2225_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11044_GM;
MethodInfo m11044_MI = 
{
	"get_Default", (methodPointerType)&m10748_gshared, &t2225_TI, &t2225_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11044_GM};
static MethodInfo* t2225_MIs[] =
{
	&m11041_MI,
	&m11042_MI,
	&m11043_MI,
	&m26859_MI,
	&m11044_MI,
	NULL
};
extern MethodInfo m11043_MI;
static MethodInfo* t2225_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m26859_MI,
	&m11043_MI,
	NULL,
};
extern TypeInfo t4020_TI;
static TypeInfo* t2225_ITIs[] = 
{
	&t4020_TI,
	&t726_TI,
};
static Il2CppInterfaceOffsetPair t2225_IOs[] = 
{
	{ &t4020_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t2225_TI;
extern TypeInfo t2225_TI;
extern TypeInfo t2226_TI;
extern TypeInfo t31_TI;
static Il2CppRGCTXData t2225_RGCTXData[8] = 
{
	&t4021_0_0_0/* Type Usage */,
	&t31_0_0_0/* Type Usage */,
	&t2225_TI/* Class Usage */,
	&t2225_TI/* Static Usage */,
	&t2226_TI/* Class Usage */,
	&m11045_MI/* Method Usage */,
	&t31_TI/* Class Usage */,
	&m26859_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2225_0_0_0;
extern Il2CppType t2225_1_0_0;
struct t2225;
extern Il2CppGenericClass t2225_GC;
TypeInfo t2225_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparer`1", "System.Collections.Generic", t2225_MIs, t2225_PIs, t2225_FIs, NULL, &t29_TI, NULL, NULL, &t2225_TI, t2225_ITIs, t2225_VT, &EmptyCustomAttributesCache, &t2225_TI, &t2225_0_0_0, &t2225_1_0_0, t2225_IOs, &t2225_GC, NULL, NULL, NULL, t2225_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2225), 0, -1, sizeof(t2225_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 5, 1, 1, 0, 0, 7, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IComparer`1<UnityEngine.EventSystems.IEventSystemHandler>
extern Il2CppType t31_0_0_0;
extern Il2CppType t31_0_0_0;
static ParameterInfo t4020_m20003_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m20003_GM;
MethodInfo m20003_MI = 
{
	"Compare", NULL, &t4020_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t4020_m20003_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m20003_GM};
static MethodInfo* t4020_MIs[] =
{
	&m20003_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4020_0_0_0;
extern Il2CppType t4020_1_0_0;
struct t4020;
extern Il2CppGenericClass t4020_GC;
TypeInfo t4020_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparer`1", "System.Collections.Generic", t4020_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4020_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4020_TI, &t4020_0_0_0, &t4020_1_0_0, NULL, &t4020_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IComparable`1<UnityEngine.EventSystems.IEventSystemHandler>
extern Il2CppType t31_0_0_0;
static ParameterInfo t4021_m20004_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m20004_GM;
MethodInfo m20004_MI = 
{
	"CompareTo", NULL, &t4021_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t4021_m20004_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m20004_GM};
static MethodInfo* t4021_MIs[] =
{
	&m20004_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4021_1_0_0;
struct t4021;
extern Il2CppGenericClass t4021_GC;
TypeInfo t4021_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t4021_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4021_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4021_TI, &t4021_0_0_0, &t4021_1_0_0, NULL, &t4021_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m11041_MI;
extern MethodInfo m20004_MI;


// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.EventSystems.IEventSystemHandler>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11045_GM;
MethodInfo m11045_MI = 
{
	".ctor", (methodPointerType)&m10749_gshared, &t2226_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11045_GM};
extern Il2CppType t31_0_0_0;
extern Il2CppType t31_0_0_0;
static ParameterInfo t2226_m11046_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11046_GM;
MethodInfo m11046_MI = 
{
	"Compare", (methodPointerType)&m10750_gshared, &t2226_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2226_m11046_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11046_GM};
static MethodInfo* t2226_MIs[] =
{
	&m11045_MI,
	&m11046_MI,
	NULL
};
extern MethodInfo m11046_MI;
static MethodInfo* t2226_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m11046_MI,
	&m11043_MI,
	&m11046_MI,
};
static Il2CppInterfaceOffsetPair t2226_IOs[] = 
{
	{ &t4020_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t2225_TI;
extern TypeInfo t2225_TI;
extern TypeInfo t2226_TI;
extern TypeInfo t31_TI;
extern TypeInfo t31_TI;
extern TypeInfo t4021_TI;
static Il2CppRGCTXData t2226_RGCTXData[12] = 
{
	&t4021_0_0_0/* Type Usage */,
	&t31_0_0_0/* Type Usage */,
	&t2225_TI/* Class Usage */,
	&t2225_TI/* Static Usage */,
	&t2226_TI/* Class Usage */,
	&m11045_MI/* Method Usage */,
	&t31_TI/* Class Usage */,
	&m26859_MI/* Method Usage */,
	&m11041_MI/* Method Usage */,
	&t31_TI/* Class Usage */,
	&t4021_TI/* Class Usage */,
	&m20004_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2226_0_0_0;
extern Il2CppType t2226_1_0_0;
struct t2226;
extern Il2CppGenericClass t2226_GC;
TypeInfo t2226_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2226_MIs, NULL, NULL, NULL, &t2225_TI, NULL, &t1246_TI, &t2226_TI, NULL, t2226_VT, &EmptyCustomAttributesCache, &t2226_TI, &t2226_0_0_0, &t2226_1_0_0, t2226_IOs, &t2226_GC, NULL, NULL, NULL, t2226_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2226), 0, -1, 0, 0, -1, 1048835, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 7, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2220_TI;
#include "t2220MD.h"



// Metadata Definition System.Comparison`1<UnityEngine.EventSystems.IEventSystemHandler>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2220_m11047_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11047_GM;
MethodInfo m11047_MI = 
{
	".ctor", (methodPointerType)&m10796_gshared, &t2220_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2220_m11047_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11047_GM};
extern Il2CppType t31_0_0_0;
extern Il2CppType t31_0_0_0;
static ParameterInfo t2220_m11048_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11048_GM;
MethodInfo m11048_MI = 
{
	"Invoke", (methodPointerType)&m10797_gshared, &t2220_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2220_m11048_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11048_GM};
extern Il2CppType t31_0_0_0;
extern Il2CppType t31_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2220_m11049_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t31_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11049_GM;
MethodInfo m11049_MI = 
{
	"BeginInvoke", (methodPointerType)&m10798_gshared, &t2220_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t2220_m11049_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m11049_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2220_m11050_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11050_GM;
MethodInfo m11050_MI = 
{
	"EndInvoke", (methodPointerType)&m10799_gshared, &t2220_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2220_m11050_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11050_GM};
static MethodInfo* t2220_MIs[] =
{
	&m11047_MI,
	&m11048_MI,
	&m11049_MI,
	&m11050_MI,
	NULL
};
extern MethodInfo m11048_MI;
extern MethodInfo m11049_MI;
extern MethodInfo m11050_MI;
static MethodInfo* t2220_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m11048_MI,
	&m11049_MI,
	&m11050_MI,
};
static Il2CppInterfaceOffsetPair t2220_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2220_1_0_0;
struct t2220;
extern Il2CppGenericClass t2220_GC;
TypeInfo t2220_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparison`1", "System", t2220_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2220_TI, NULL, t2220_VT, &EmptyCustomAttributesCache, &t2220_TI, &t2220_0_0_0, &t2220_1_0_0, t2220_IOs, &t2220_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2220), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t86.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t86_TI;
#include "t86MD.h"

#include "t2227.h"
#include "t88.h"
extern TypeInfo t2227_TI;
extern TypeInfo t88_TI;
#include "t2227MD.h"
#include "t88MD.h"
#include "t293MD.h"
extern MethodInfo m11052_MI;
extern MethodInfo m11058_MI;
extern MethodInfo m11089_MI;
extern MethodInfo m11080_MI;
extern MethodInfo m20055_MI;
extern MethodInfo m11054_MI;
extern MethodInfo m11087_MI;
extern MethodInfo m11116_MI;
extern MethodInfo m11086_MI;
extern MethodInfo m2868_MI;
extern MethodInfo m1296_MI;
extern MethodInfo m11088_MI;
struct t931;
#include "t931.h"
struct t931;
 t29 * m20008_gshared (t29 * __this, MethodInfo* method);
#define m20008(__this, method) (t29 *)m20008_gshared((t29 *)__this, method)
#define m20055(__this, method) (t105 *)m20008_gshared((t29 *)__this, method)


// Metadata Definition UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>
extern Il2CppType t2227_0_0_33;
FieldInfo t86_f0_FieldInfo = 
{
	"m_Stack", &t2227_0_0_33, &t86_TI, offsetof(t86, f0), &EmptyCustomAttributesCache};
extern Il2CppType t88_0_0_33;
FieldInfo t86_f1_FieldInfo = 
{
	"m_ActionOnGet", &t88_0_0_33, &t86_TI, offsetof(t86, f1), &EmptyCustomAttributesCache};
extern Il2CppType t88_0_0_33;
FieldInfo t86_f2_FieldInfo = 
{
	"m_ActionOnRelease", &t88_0_0_33, &t86_TI, offsetof(t86, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
extern CustomAttributesCache t282__CustomAttributeCache_U3CcountAllU3Ek__BackingField;
FieldInfo t86_f3_FieldInfo = 
{
	"<countAll>k__BackingField", &t44_0_0_1, &t86_TI, offsetof(t86, f3), &t282__CustomAttributeCache_U3CcountAllU3Ek__BackingField};
static FieldInfo* t86_FIs[] =
{
	&t86_f0_FieldInfo,
	&t86_f1_FieldInfo,
	&t86_f2_FieldInfo,
	&t86_f3_FieldInfo,
	NULL
};
static PropertyInfo t86____countAll_PropertyInfo = 
{
	&t86_TI, "countAll", &m11052_MI, &m11054_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11056_MI;
static PropertyInfo t86____countActive_PropertyInfo = 
{
	&t86_TI, "countActive", &m11056_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t86____countInactive_PropertyInfo = 
{
	&t86_TI, "countInactive", &m11058_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t86_PIs[] =
{
	&t86____countAll_PropertyInfo,
	&t86____countActive_PropertyInfo,
	&t86____countInactive_PropertyInfo,
	NULL
};
extern Il2CppType t88_0_0_0;
extern Il2CppType t88_0_0_0;
extern Il2CppType t88_0_0_0;
static ParameterInfo t86_m1358_ParameterInfos[] = 
{
	{"actionOnGet", 0, 134217728, &EmptyCustomAttributesCache, &t88_0_0_0},
	{"actionOnRelease", 1, 134217728, &EmptyCustomAttributesCache, &t88_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1358_GM;
MethodInfo m1358_MI = 
{
	".ctor", (methodPointerType)&m11051_gshared, &t86_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t86_m1358_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1358_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t282__CustomAttributeCache_m2037;
extern Il2CppGenericMethod m11052_GM;
MethodInfo m11052_MI = 
{
	"get_countAll", (methodPointerType)&m11053_gshared, &t86_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &t282__CustomAttributeCache_m2037, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11052_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t86_m11054_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t282__CustomAttributeCache_m2038;
extern Il2CppGenericMethod m11054_GM;
MethodInfo m11054_MI = 
{
	"set_countAll", (methodPointerType)&m11055_gshared, &t86_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t86_m11054_ParameterInfos, &t282__CustomAttributeCache_m2038, 2177, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11054_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11056_GM;
MethodInfo m11056_MI = 
{
	"get_countActive", (methodPointerType)&m11057_gshared, &t86_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11056_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11058_GM;
MethodInfo m11058_MI = 
{
	"get_countInactive", (methodPointerType)&m11059_gshared, &t86_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11058_GM};
extern Il2CppType t105_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11060_GM;
MethodInfo m11060_MI = 
{
	"Get", (methodPointerType)&m11061_gshared, &t86_TI, &t105_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11060_GM};
extern Il2CppType t105_0_0_0;
static ParameterInfo t86_m11062_ParameterInfos[] = 
{
	{"element", 0, 134217728, &EmptyCustomAttributesCache, &t105_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11062_GM;
MethodInfo m11062_MI = 
{
	"Release", (methodPointerType)&m11063_gshared, &t86_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t86_m11062_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11062_GM};
static MethodInfo* t86_MIs[] =
{
	&m1358_MI,
	&m11052_MI,
	&m11054_MI,
	&m11056_MI,
	&m11058_MI,
	&m11060_MI,
	&m11062_MI,
	NULL
};
static MethodInfo* t86_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern TypeInfo t2227_TI;
extern TypeInfo t105_TI;
static Il2CppRGCTXData t86_RGCTXData[12] = 
{
	&t2227_TI/* Class Usage */,
	&m11080_MI/* Method Usage */,
	&m11052_MI/* Method Usage */,
	&m11058_MI/* Method Usage */,
	&m11089_MI/* Method Usage */,
	&t105_TI/* Class Usage */,
	&m20055_MI/* Method Usage */,
	&m11054_MI/* Method Usage */,
	&m11087_MI/* Method Usage */,
	&m11116_MI/* Method Usage */,
	&m11086_MI/* Method Usage */,
	&m11088_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t86_0_0_0;
extern Il2CppType t86_1_0_0;
struct t86;
extern Il2CppGenericClass t86_GC;
extern CustomAttributesCache t282__CustomAttributeCache_U3CcountAllU3Ek__BackingField;
extern CustomAttributesCache t282__CustomAttributeCache_m2037;
extern CustomAttributesCache t282__CustomAttributeCache_m2038;
TypeInfo t86_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "ObjectPool`1", "UnityEngine.UI", t86_MIs, t86_PIs, t86_FIs, NULL, &t29_TI, NULL, NULL, &t86_TI, NULL, t86_VT, &EmptyCustomAttributesCache, &t86_TI, &t86_0_0_0, &t86_1_0_0, NULL, &t86_GC, NULL, NULL, NULL, t86_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t86), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 7, 3, 4, 0, 0, 4, 0, 0};
#include "t2228.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2228_TI;
#include "t2228MD.h"

#include "t2229.h"
#include "t2120.h"
extern TypeInfo t2229_TI;
extern TypeInfo t2120_TI;
#include "t2229MD.h"
#include "t2120MD.h"
extern MethodInfo m11053_MI;
extern MethodInfo m11059_MI;
extern MethodInfo m11073_MI;
extern MethodInfo m11064_MI;
extern MethodInfo m20008_MI;
extern MethodInfo m11055_MI;
extern MethodInfo m11071_MI;
extern MethodInfo m10326_MI;
extern MethodInfo m11070_MI;
extern MethodInfo m11072_MI;


extern MethodInfo m11051_MI;
 void m11051_gshared (t2228 * __this, t2120 * p0, t2120 * p1, MethodInfo* method)
{
	{
		t2229 * L_0 = (t2229 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		(( void (*) (t2229 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->f0 = L_0;
		m1331(__this, &m1331_MI);
		__this->f1 = p0;
		__this->f2 = p1;
		return;
	}
}
 int32_t m11053_gshared (t2228 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->f3);
		return L_0;
	}
}
 void m11055_gshared (t2228 * __this, int32_t p0, MethodInfo* method)
{
	{
		__this->f3 = p0;
		return;
	}
}
extern MethodInfo m11057_MI;
 int32_t m11057_gshared (t2228 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (( int32_t (*) (t2228 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(__this, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		int32_t L_1 = (( int32_t (*) (t2228 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(__this, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return ((int32_t)(L_0-L_1));
	}
}
 int32_t m11059_gshared (t2228 * __this, MethodInfo* method)
{
	{
		t2229 * L_0 = (__this->f0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), L_0);
		return L_1;
	}
}
extern MethodInfo m11061_MI;
 t29 * m11061_gshared (t2228 * __this, MethodInfo* method)
{
	t29 * V_0 = {0};
	t29 * V_1 = {0};
	t29 * G_B4_0 = {0};
	{
		t2229 * L_0 = (__this->f0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), L_0);
		if (L_1)
		{
			goto IL_0047;
		}
	}
	{
		Initobj (&t29_TI, (&V_1));
		t29 * L_2 = V_1;
		if (!((t29 *)L_2))
		{
			goto IL_002e;
		}
	}
	{
		Initobj (&t29_TI, (&V_1));
		G_B4_0 = V_1;
		goto IL_0033;
	}

IL_002e:
	{
		t29 * L_3 = (( t29 * (*) (t29 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		G_B4_0 = L_3;
	}

IL_0033:
	{
		V_0 = G_B4_0;
		int32_t L_4 = (( int32_t (*) (t2228 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(__this, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		(( void (*) (t2228 * __this, int32_t p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(__this, ((int32_t)(L_4+1)), IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		goto IL_0053;
	}

IL_0047:
	{
		t2229 * L_5 = (__this->f0);
		t29 * L_6 = (( t29 * (*) (t2229 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_5, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		V_0 = L_6;
	}

IL_0053:
	{
		t2120 * L_7 = (__this->f1);
		if (!L_7)
		{
			goto IL_006a;
		}
	}
	{
		t2120 * L_8 = (__this->f1);
		VirtActionInvoker1< t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), L_8, V_0);
	}

IL_006a:
	{
		return V_0;
	}
}
extern MethodInfo m11063_MI;
 void m11063_gshared (t2228 * __this, t29 * p0, MethodInfo* method)
{
	{
		t2229 * L_0 = (__this->f0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), L_0);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_003b;
		}
	}
	{
		t2229 * L_2 = (__this->f0);
		t29 * L_3 = (( t29 * (*) (t2229 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(L_2, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		t29 * L_4 = L_3;
		t29 * L_5 = p0;
		bool L_6 = m2868(NULL, ((t29 *)L_4), ((t29 *)L_5), &m2868_MI);
		if (!L_6)
		{
			goto IL_003b;
		}
	}
	{
		m1296(NULL, (t7*) &_stringLiteral68, &m1296_MI);
	}

IL_003b:
	{
		t2120 * L_7 = (__this->f2);
		if (!L_7)
		{
			goto IL_0052;
		}
	}
	{
		t2120 * L_8 = (__this->f2);
		VirtActionInvoker1< t29 * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), L_8, p0);
	}

IL_0052:
	{
		t2229 * L_9 = (__this->f0);
		(( void (*) (t2229 * __this, t29 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_9, p0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return;
	}
}
// Metadata Definition UnityEngine.UI.ObjectPool`1<System.Object>
extern Il2CppType t2229_0_0_33;
FieldInfo t2228_f0_FieldInfo = 
{
	"m_Stack", &t2229_0_0_33, &t2228_TI, offsetof(t2228, f0), &EmptyCustomAttributesCache};
extern Il2CppType t2120_0_0_33;
FieldInfo t2228_f1_FieldInfo = 
{
	"m_ActionOnGet", &t2120_0_0_33, &t2228_TI, offsetof(t2228, f1), &EmptyCustomAttributesCache};
extern Il2CppType t2120_0_0_33;
FieldInfo t2228_f2_FieldInfo = 
{
	"m_ActionOnRelease", &t2120_0_0_33, &t2228_TI, offsetof(t2228, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
extern CustomAttributesCache t282__CustomAttributeCache_U3CcountAllU3Ek__BackingField;
FieldInfo t2228_f3_FieldInfo = 
{
	"<countAll>k__BackingField", &t44_0_0_1, &t2228_TI, offsetof(t2228, f3), &t282__CustomAttributeCache_U3CcountAllU3Ek__BackingField};
static FieldInfo* t2228_FIs[] =
{
	&t2228_f0_FieldInfo,
	&t2228_f1_FieldInfo,
	&t2228_f2_FieldInfo,
	&t2228_f3_FieldInfo,
	NULL
};
static PropertyInfo t2228____countAll_PropertyInfo = 
{
	&t2228_TI, "countAll", &m11053_MI, &m11055_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2228____countActive_PropertyInfo = 
{
	&t2228_TI, "countActive", &m11057_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2228____countInactive_PropertyInfo = 
{
	&t2228_TI, "countInactive", &m11059_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2228_PIs[] =
{
	&t2228____countAll_PropertyInfo,
	&t2228____countActive_PropertyInfo,
	&t2228____countInactive_PropertyInfo,
	NULL
};
extern Il2CppType t2120_0_0_0;
extern Il2CppType t2120_0_0_0;
extern Il2CppType t2120_0_0_0;
static ParameterInfo t2228_m11051_ParameterInfos[] = 
{
	{"actionOnGet", 0, 134217728, &EmptyCustomAttributesCache, &t2120_0_0_0},
	{"actionOnRelease", 1, 134217728, &EmptyCustomAttributesCache, &t2120_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11051_GM;
MethodInfo m11051_MI = 
{
	".ctor", (methodPointerType)&m11051_gshared, &t2228_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2228_m11051_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11051_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t282__CustomAttributeCache_m2037;
extern Il2CppGenericMethod m11053_GM;
MethodInfo m11053_MI = 
{
	"get_countAll", (methodPointerType)&m11053_gshared, &t2228_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &t282__CustomAttributeCache_m2037, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11053_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2228_m11055_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t282__CustomAttributeCache_m2038;
extern Il2CppGenericMethod m11055_GM;
MethodInfo m11055_MI = 
{
	"set_countAll", (methodPointerType)&m11055_gshared, &t2228_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2228_m11055_ParameterInfos, &t282__CustomAttributeCache_m2038, 2177, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11055_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11057_GM;
MethodInfo m11057_MI = 
{
	"get_countActive", (methodPointerType)&m11057_gshared, &t2228_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11057_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11059_GM;
MethodInfo m11059_MI = 
{
	"get_countInactive", (methodPointerType)&m11059_gshared, &t2228_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11059_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11061_GM;
MethodInfo m11061_MI = 
{
	"Get", (methodPointerType)&m11061_gshared, &t2228_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11061_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2228_m11063_ParameterInfos[] = 
{
	{"element", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11063_GM;
MethodInfo m11063_MI = 
{
	"Release", (methodPointerType)&m11063_gshared, &t2228_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2228_m11063_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11063_GM};
static MethodInfo* t2228_MIs[] =
{
	&m11051_MI,
	&m11053_MI,
	&m11055_MI,
	&m11057_MI,
	&m11059_MI,
	&m11061_MI,
	&m11063_MI,
	NULL
};
static MethodInfo* t2228_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern TypeInfo t2229_TI;
extern TypeInfo t29_TI;
static Il2CppRGCTXData t2228_RGCTXData[12] = 
{
	&t2229_TI/* Class Usage */,
	&m11064_MI/* Method Usage */,
	&m11053_MI/* Method Usage */,
	&m11059_MI/* Method Usage */,
	&m11073_MI/* Method Usage */,
	&t29_TI/* Class Usage */,
	&m20008_MI/* Method Usage */,
	&m11055_MI/* Method Usage */,
	&m11071_MI/* Method Usage */,
	&m10326_MI/* Method Usage */,
	&m11070_MI/* Method Usage */,
	&m11072_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t2228_0_0_0;
extern Il2CppType t2228_1_0_0;
struct t2228;
extern Il2CppGenericClass t2228_GC;
extern CustomAttributesCache t282__CustomAttributeCache_U3CcountAllU3Ek__BackingField;
extern CustomAttributesCache t282__CustomAttributeCache_m2037;
extern CustomAttributesCache t282__CustomAttributeCache_m2038;
TypeInfo t2228_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "ObjectPool`1", "UnityEngine.UI", t2228_MIs, t2228_PIs, t2228_FIs, NULL, &t29_TI, NULL, NULL, &t2228_TI, NULL, t2228_VT, &EmptyCustomAttributesCache, &t2228_TI, &t2228_0_0_0, &t2228_1_0_0, NULL, &t2228_GC, NULL, NULL, NULL, t2228_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2228), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 7, 3, 4, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2230.h"
extern TypeInfo t1622_TI;
extern TypeInfo t2230_TI;
#include "t2230MD.h"
extern MethodInfo m4199_MI;
extern MethodInfo m11074_MI;
extern MethodInfo m19788_MI;
extern MethodInfo m11075_MI;


 void m11064_gshared (t2229 * __this, MethodInfo* method)
{
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m11065_MI;
 bool m11065_gshared (t2229 * __this, MethodInfo* method)
{
	{
		return 0;
	}
}
extern MethodInfo m11066_MI;
 t29 * m11066_gshared (t2229 * __this, MethodInfo* method)
{
	{
		return __this;
	}
}
extern MethodInfo m11067_MI;
 void m11067_gshared (t2229 * __this, t20 * p0, int32_t p1, MethodInfo* method)
{
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			t316* L_0 = (__this->f1);
			if (!L_0)
			{
				goto IL_0025;
			}
		}

IL_000b:
		{
			t316* L_1 = (__this->f1);
			VirtActionInvoker2< t20 *, int32_t >::Invoke(&m4199_MI, L_1, p0, p1);
			int32_t L_2 = (__this->f2);
			m5171(NULL, p0, p1, L_2, &m5171_MI);
		}

IL_0025:
		{
			// IL_0025: leave IL_0036
			goto IL_0036;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t1622_TI, e.ex->object.klass))
			goto IL_002a;
		throw e;
	}

IL_002a:
	{ // begin catch(System.ArrayTypeMismatchException)
		t305 * L_3 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m8852(L_3, &m8852_MI);
		il2cpp_codegen_raise_exception(L_3);
		// IL_0031: leave IL_0036
		goto IL_0036;
	} // end catch (depth: 1)

IL_0036:
	{
		return;
	}
}
extern MethodInfo m11068_MI;
 t29* m11068_gshared (t2229 * __this, MethodInfo* method)
{
	{
		t2230  L_0 = (( t2230  (*) (t2229 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(__this, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		t2230  L_1 = L_0;
		t29 * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return (t29*)L_2;
	}
}
extern MethodInfo m11069_MI;
 t29 * m11069_gshared (t2229 * __this, MethodInfo* method)
{
	{
		t2230  L_0 = (( t2230  (*) (t2229 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(__this, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		t2230  L_1 = L_0;
		t29 * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return (t29 *)L_2;
	}
}
 t29 * m11070_gshared (t2229 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->f2);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3974(L_1, &m3974_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0011:
	{
		t316* L_2 = (__this->f1);
		int32_t L_3 = (__this->f2);
		int32_t L_4 = ((int32_t)(L_3-1));
		return (*(t29 **)(t29 **)SZArrayLdElema(L_2, L_4));
	}
}
 t29 * m11071_gshared (t2229 * __this, MethodInfo* method)
{
	t29 * V_0 = {0};
	int32_t V_1 = 0;
	t29 * V_2 = {0};
	{
		int32_t L_0 = (__this->f2);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3974(L_1, &m3974_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0011:
	{
		int32_t L_2 = (__this->f3);
		__this->f3 = ((int32_t)(L_2+1));
		t316* L_3 = (__this->f1);
		int32_t L_4 = (__this->f2);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_1 = L_5;
		__this->f2 = L_5;
		int32_t L_6 = V_1;
		V_0 = (*(t29 **)(t29 **)SZArrayLdElema(L_3, L_6));
		t316* L_7 = (__this->f1);
		int32_t L_8 = (__this->f2);
		Initobj (&t29_TI, (&V_2));
		*((t29 **)(t29 **)SZArrayLdElema(L_7, L_8)) = (t29 *)V_2;
		return V_0;
	}
}
 void m11072_gshared (t2229 * __this, t29 * p0, MethodInfo* method)
{
	int32_t V_0 = 0;
	t316** G_B4_0 = {0};
	t316** G_B3_0 = {0};
	int32_t G_B5_0 = 0;
	t316** G_B5_1 = {0};
	{
		t316* L_0 = (__this->f1);
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (__this->f2);
		t316* L_2 = (__this->f1);
		if ((((uint32_t)L_1) != ((uint32_t)(((int32_t)(((t20 *)L_2)->max_length))))))
		{
			goto IL_0043;
		}
	}

IL_001e:
	{
		t316** L_3 = &(__this->f1);
		int32_t L_4 = (__this->f2);
		G_B3_0 = L_3;
		if (L_4)
		{
			G_B4_0 = L_3;
			goto IL_0036;
		}
	}
	{
		G_B5_0 = ((int32_t)16);
		G_B5_1 = G_B3_0;
		goto IL_003e;
	}

IL_0036:
	{
		int32_t L_5 = (__this->f2);
		G_B5_0 = ((int32_t)((int32_t)2*(int32_t)L_5));
		G_B5_1 = G_B4_0;
	}

IL_003e:
	{
		(( void (*) (t29 * __this, t316** p0, int32_t p1, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(NULL, G_B5_1, G_B5_0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}

IL_0043:
	{
		int32_t L_6 = (__this->f3);
		__this->f3 = ((int32_t)(L_6+1));
		t316* L_7 = (__this->f1);
		int32_t L_8 = (__this->f2);
		int32_t L_9 = L_8;
		V_0 = L_9;
		__this->f2 = ((int32_t)(L_9+1));
		*((t29 **)(t29 **)SZArrayLdElema(L_7, V_0)) = (t29 *)p0;
		return;
	}
}
 int32_t m11073_gshared (t2229 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->f2);
		return L_0;
	}
}
 t2230  m11074 (t2229 * __this, MethodInfo* method){
	{
		t2230  L_0 = {0};
		m11075(&L_0, __this, &m11075_MI);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.Stack`1<System.Object>
extern Il2CppType t44_0_0_32849;
FieldInfo t2229_f0_FieldInfo = 
{
	"INITIAL_SIZE", &t44_0_0_32849, &t2229_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t316_0_0_1;
FieldInfo t2229_f1_FieldInfo = 
{
	"_array", &t316_0_0_1, &t2229_TI, offsetof(t2229, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2229_f2_FieldInfo = 
{
	"_size", &t44_0_0_1, &t2229_TI, offsetof(t2229, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2229_f3_FieldInfo = 
{
	"_version", &t44_0_0_1, &t2229_TI, offsetof(t2229, f3), &EmptyCustomAttributesCache};
static FieldInfo* t2229_FIs[] =
{
	&t2229_f0_FieldInfo,
	&t2229_f1_FieldInfo,
	&t2229_f2_FieldInfo,
	&t2229_f3_FieldInfo,
	NULL
};
static const int32_t t2229_f0_DefaultValueData = 16;
static Il2CppFieldDefaultValueEntry t2229_f0_DefaultValue = 
{
	&t2229_f0_FieldInfo, { (char*)&t2229_f0_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t2229_FDVs[] = 
{
	&t2229_f0_DefaultValue,
	NULL
};
static PropertyInfo t2229____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2229_TI, "System.Collections.ICollection.IsSynchronized", &m11065_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2229____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2229_TI, "System.Collections.ICollection.SyncRoot", &m11066_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2229____Count_PropertyInfo = 
{
	&t2229_TI, "Count", &m11073_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2229_PIs[] =
{
	&t2229____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2229____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2229____Count_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11064_GM;
MethodInfo m11064_MI = 
{
	".ctor", (methodPointerType)&m11064_gshared, &t2229_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11064_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11065_GM;
MethodInfo m11065_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m11065_gshared, &t2229_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11065_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11066_GM;
MethodInfo m11066_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m11066_gshared, &t2229_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11066_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2229_m11067_ParameterInfos[] = 
{
	{"dest", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"idx", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11067_GM;
MethodInfo m11067_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m11067_gshared, &t2229_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2229_m11067_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11067_GM};
extern Il2CppType t346_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11068_GM;
MethodInfo m11068_MI = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator", (methodPointerType)&m11068_gshared, &t2229_TI, &t346_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11068_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11069_GM;
MethodInfo m11069_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m11069_gshared, &t2229_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11069_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11070_GM;
MethodInfo m11070_MI = 
{
	"Peek", (methodPointerType)&m11070_gshared, &t2229_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11070_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11071_GM;
MethodInfo m11071_MI = 
{
	"Pop", (methodPointerType)&m11071_gshared, &t2229_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11071_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2229_m11072_ParameterInfos[] = 
{
	{"t", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11072_GM;
MethodInfo m11072_MI = 
{
	"Push", (methodPointerType)&m11072_gshared, &t2229_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2229_m11072_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11072_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11073_GM;
MethodInfo m11073_MI = 
{
	"get_Count", (methodPointerType)&m11073_gshared, &t2229_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11073_GM};
extern Il2CppType t2230_0_0_0;
extern void* RuntimeInvoker_t2230 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11074_GM;
MethodInfo m11074_MI = 
{
	"GetEnumerator", (methodPointerType)&m11074, &t2229_TI, &t2230_0_0_0, RuntimeInvoker_t2230, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11074_GM};
static MethodInfo* t2229_MIs[] =
{
	&m11064_MI,
	&m11065_MI,
	&m11066_MI,
	&m11067_MI,
	&m11068_MI,
	&m11069_MI,
	&m11070_MI,
	&m11071_MI,
	&m11072_MI,
	&m11073_MI,
	&m11074_MI,
	NULL
};
static MethodInfo* t2229_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m11073_MI,
	&m11065_MI,
	&m11066_MI,
	&m11067_MI,
	&m11069_MI,
	&m11068_MI,
};
extern TypeInfo t2183_TI;
static TypeInfo* t2229_ITIs[] = 
{
	&t674_TI,
	&t603_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2229_IOs[] = 
{
	{ &t674_TI, 4},
	{ &t603_TI, 8},
	{ &t2183_TI, 9},
};
extern TypeInfo t2230_TI;
static Il2CppRGCTXData t2229_RGCTXData[4] = 
{
	&m11074_MI/* Method Usage */,
	&t2230_TI/* Class Usage */,
	&m19788_MI/* Method Usage */,
	&m11075_MI/* Method Usage */,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t2229_0_0_0;
extern Il2CppType t2229_1_0_0;
struct t2229;
extern Il2CppGenericClass t2229_GC;
extern CustomAttributesCache t717__CustomAttributeCache;
TypeInfo t2229_TI = 
{
	&g_System_dll_Image, NULL, "Stack`1", "System.Collections.Generic", t2229_MIs, t2229_PIs, t2229_FIs, NULL, &t29_TI, NULL, NULL, &t2229_TI, t2229_ITIs, t2229_VT, &t717__CustomAttributeCache, &t2229_TI, &t2229_0_0_0, &t2229_1_0_0, t2229_IOs, &t2229_GC, NULL, t2229_FDVs, NULL, t2229_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2229), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 11, 3, 4, 0, 0, 10, 3, 3};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
