﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3050;
struct t29;
#include "t380.h"

 void m16803 (t3050 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16804 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16805 (t3050 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t3050 * m16806 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
