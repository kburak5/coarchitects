﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3291;
struct t29;
struct t316;
struct t136;
struct t346;
struct t295;

 void m18326_gshared (t3291 * __this, t316* p0, MethodInfo* method);
#define m18326(__this, p0, method) (void)m18326_gshared((t3291 *)__this, (t316*)p0, method)
 t29 * m18327_gshared (t3291 * __this, MethodInfo* method);
#define m18327(__this, method) (t29 *)m18327_gshared((t3291 *)__this, method)
 t29 * m18328_gshared (t3291 * __this, int32_t p0, MethodInfo* method);
#define m18328(__this, p0, method) (t29 *)m18328_gshared((t3291 *)__this, (int32_t)p0, method)
 void m18329_gshared (t3291 * __this, int32_t p0, t29 * p1, MethodInfo* method);
#define m18329(__this, p0, p1, method) (void)m18329_gshared((t3291 *)__this, (int32_t)p0, (t29 *)p1, method)
 int32_t m18330_gshared (t3291 * __this, MethodInfo* method);
#define m18330(__this, method) (int32_t)m18330_gshared((t3291 *)__this, method)
 bool m18331_gshared (t3291 * __this, MethodInfo* method);
#define m18331(__this, method) (bool)m18331_gshared((t3291 *)__this, method)
 void m18332_gshared (t3291 * __this, t29 * p0, MethodInfo* method);
#define m18332(__this, p0, method) (void)m18332_gshared((t3291 *)__this, (t29 *)p0, method)
 void m18333_gshared (t3291 * __this, MethodInfo* method);
#define m18333(__this, method) (void)m18333_gshared((t3291 *)__this, method)
 bool m18334_gshared (t3291 * __this, t29 * p0, MethodInfo* method);
#define m18334(__this, p0, method) (bool)m18334_gshared((t3291 *)__this, (t29 *)p0, method)
 void m18335_gshared (t3291 * __this, t316* p0, int32_t p1, MethodInfo* method);
#define m18335(__this, p0, p1, method) (void)m18335_gshared((t3291 *)__this, (t316*)p0, (int32_t)p1, method)
 t29* m18336_gshared (t3291 * __this, MethodInfo* method);
#define m18336(__this, method) (t29*)m18336_gshared((t3291 *)__this, method)
 int32_t m18337_gshared (t3291 * __this, t29 * p0, MethodInfo* method);
#define m18337(__this, p0, method) (int32_t)m18337_gshared((t3291 *)__this, (t29 *)p0, method)
 void m18338_gshared (t3291 * __this, int32_t p0, t29 * p1, MethodInfo* method);
#define m18338(__this, p0, p1, method) (void)m18338_gshared((t3291 *)__this, (int32_t)p0, (t29 *)p1, method)
 bool m18339_gshared (t3291 * __this, t29 * p0, MethodInfo* method);
#define m18339(__this, p0, method) (bool)m18339_gshared((t3291 *)__this, (t29 *)p0, method)
 void m18340_gshared (t3291 * __this, int32_t p0, MethodInfo* method);
#define m18340(__this, p0, method) (void)m18340_gshared((t3291 *)__this, (int32_t)p0, method)
 t295 * m18341_gshared (t29 * __this, MethodInfo* method);
#define m18341(__this, method) (t295 *)m18341_gshared((t29 *)__this, method)
