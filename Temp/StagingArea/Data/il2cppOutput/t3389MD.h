﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3389;
struct t29;
struct t20;
#include "t1342.h"

 void m18817 (t3389 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18818 (t3389 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18819 (t3389 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18820 (t3389 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18821 (t3389 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
