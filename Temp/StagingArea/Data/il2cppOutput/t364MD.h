﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t364;
struct t29;
struct t155;
struct t2567;
struct t2565;
struct t733;
struct t2568;
struct t20;
struct t136;
struct t2570;
struct t722;
#include "t735.h"
#include "t2569.h"
#include "t2571.h"
#include "t725.h"

 void m13776 (t364 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13777 (t364 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13778 (t364 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13779 (t364 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m13780 (t364 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13781 (t364 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13782 (t364 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13783 (t364 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13784 (t364 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m13785 (t364 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13786 (t364 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13787 (t364 * __this, t2569  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13788 (t364 * __this, t2569  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13789 (t364 * __this, t2568* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13790 (t364 * __this, t2569  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13791 (t364 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m13792 (t364 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m13793 (t364 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m13794 (t364 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m13795 (t364 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m13796 (t364 * __this, t155 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13797 (t364 * __this, t155 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13798 (t364 * __this, int32_t p0, t29* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13799 (t364 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13800 (t364 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2569  m13801 (t29 * __this, t155 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m13802 (t29 * __this, t155 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13803 (t364 * __this, t2568* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13804 (t364 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13805 (t364 * __this, t155 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13806 (t364 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13807 (t364 * __this, t155 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13808 (t364 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13809 (t364 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13810 (t364 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13811 (t364 * __this, t155 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13812 (t364 * __this, t155 * p0, int32_t* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2567 * m13813 (t364 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t155 * m13814 (t364 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m13815 (t364 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13816 (t364 * __this, t2569  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2571  m13817 (t364 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t725  m13818 (t29 * __this, t155 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
