﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t914;
struct t7;
struct t295;
struct t733;
#include "t735.h"

 void m3974 (t914 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3964 (t914 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9281 (t914 * __this, t7* p0, t295 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9282 (t914 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
