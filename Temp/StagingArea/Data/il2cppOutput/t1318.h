﻿#pragma once
#include <stdint.h>
struct t7;
#include "t110.h"
#include "t1306.h"
struct t1318 
{
	t7* f0;
	int32_t f1;
	int64_t f2;
	int64_t f3;
	int64_t f4;
	int64_t f5;
};
// Native definition for marshalling of: System.IO.MonoIOStat
struct t1318_marshaled
{
	char* f0;
	int32_t f1;
	int64_t f2;
	int64_t f3;
	int64_t f4;
	int64_t f5;
};
