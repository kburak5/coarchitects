﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t838;
struct t828;
struct t29;
struct t674;
struct t20;
struct t136;

 void m3542 (t838 * __this, t828 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3543 (t838 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3544 (t838 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t828 * m3545 (t838 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3546 (t838 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3547 (t838 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3548 (t838 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3549 (t838 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3550 (t838 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
