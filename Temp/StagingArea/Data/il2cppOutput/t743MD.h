﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t743;
struct t744;
struct t745;
struct t746;

 void m3156 (t743 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3157 (t743 * __this, t744 * p0, t745 * p1, t746 * p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
