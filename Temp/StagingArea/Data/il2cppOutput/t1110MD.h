﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1110;
struct t7;
struct t1108;
struct t1310;
#include "t1311.h"

 void m7012 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7013 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1108 * m7014 (t29 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1108 * m5181 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1310 * m7015 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
