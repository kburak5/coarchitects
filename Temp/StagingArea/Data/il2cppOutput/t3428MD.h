﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3428;
struct t29;
struct t20;
#include "t1437.h"

 void m19010 (t3428 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m19011 (t3428 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m19012 (t3428 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19013 (t3428 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m19014 (t3428 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
