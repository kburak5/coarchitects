﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1633;
struct t733;
struct t353;
struct t29;
#include "t735.h"

 void m9210 (t1633 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9211 (t29 * __this, t353 * p0, t733 * p1, t735  p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9212 (t1633 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m9213 (t1633 * __this, t735  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
