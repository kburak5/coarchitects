﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2572;
struct t29;
struct t20;
#include "t2569.h"

 void m13825 (t2572 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m13826 (t2572 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13827 (t2572 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13828 (t2572 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2569  m13829 (t2572 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
