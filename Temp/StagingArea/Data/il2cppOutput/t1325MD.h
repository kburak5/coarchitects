﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1325;
struct t29;
struct t1050;
struct t295;

 void m7149 (t1325 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7150 (t1325 * __this, t295 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7151 (t1325 * __this, t295 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7152 (t1325 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1050 * m7153 (t1325 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7154 (t1325 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t295 * m7155 (t1325 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7156 (t1325 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7157 (t1325 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7158 (t1325 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
