﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3127;
struct t29;
struct t557;
struct t316;

 void m17268_gshared (t3127 * __this, t29 * p0, t557 * p1, MethodInfo* method);
#define m17268(__this, p0, p1, method) (void)m17268_gshared((t3127 *)__this, (t29 *)p0, (t557 *)p1, method)
 void m17269_gshared (t3127 * __this, t316* p0, MethodInfo* method);
#define m17269(__this, p0, method) (void)m17269_gshared((t3127 *)__this, (t316*)p0, method)
 bool m17270_gshared (t3127 * __this, t29 * p0, t557 * p1, MethodInfo* method);
#define m17270(__this, p0, p1, method) (bool)m17270_gshared((t3127 *)__this, (t29 *)p0, (t557 *)p1, method)
