﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t237;
struct t29;
struct t235;
struct t2676;
struct t20;
struct t136;
struct t2677;
struct t2678;
struct t2679;
struct t2675;
struct t2680;
struct t2681;
#include "t2682.h"

#include "t294MD.h"
#define m1902(__this, method) (void)m10537_gshared((t294 *)__this, method)
#define m14496(__this, p0, method) (void)m10539_gshared((t294 *)__this, (int32_t)p0, method)
#define m14497(__this, method) (void)m10541_gshared((t29 *)__this, method)
#define m14498(__this, method) (t29*)m10543_gshared((t294 *)__this, method)
#define m14499(__this, p0, p1, method) (void)m10545_gshared((t294 *)__this, (t20 *)p0, (int32_t)p1, method)
#define m14500(__this, method) (t29 *)m10547_gshared((t294 *)__this, method)
#define m14501(__this, p0, method) (int32_t)m10549_gshared((t294 *)__this, (t29 *)p0, method)
#define m14502(__this, p0, method) (bool)m10551_gshared((t294 *)__this, (t29 *)p0, method)
#define m14503(__this, p0, method) (int32_t)m10553_gshared((t294 *)__this, (t29 *)p0, method)
#define m14504(__this, p0, p1, method) (void)m10555_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m14505(__this, p0, method) (void)m10557_gshared((t294 *)__this, (t29 *)p0, method)
#define m14506(__this, method) (bool)m10559_gshared((t294 *)__this, method)
#define m14507(__this, method) (bool)m10561_gshared((t294 *)__this, method)
#define m14508(__this, method) (t29 *)m10563_gshared((t294 *)__this, method)
#define m14509(__this, method) (bool)m10565_gshared((t294 *)__this, method)
#define m14510(__this, method) (bool)m10567_gshared((t294 *)__this, method)
#define m14511(__this, p0, method) (t29 *)m10569_gshared((t294 *)__this, (int32_t)p0, method)
#define m14512(__this, p0, p1, method) (void)m10571_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m1910(__this, p0, method) (void)m10573_gshared((t294 *)__this, (t29 *)p0, method)
#define m14513(__this, p0, method) (void)m10575_gshared((t294 *)__this, (int32_t)p0, method)
#define m14514(__this, p0, method) (void)m10577_gshared((t294 *)__this, (t29*)p0, method)
#define m14515(__this, p0, method) (void)m10579_gshared((t294 *)__this, (t29*)p0, method)
#define m14516(__this, p0, method) (void)m10581_gshared((t294 *)__this, (t29*)p0, method)
#define m14517(__this, method) (t2679 *)m10583_gshared((t294 *)__this, method)
#define m14518(__this, method) (void)m10585_gshared((t294 *)__this, method)
#define m14519(__this, p0, method) (bool)m10587_gshared((t294 *)__this, (t29 *)p0, method)
#define m14520(__this, p0, p1, method) (void)m10589_gshared((t294 *)__this, (t316*)p0, (int32_t)p1, method)
#define m14521(__this, p0, method) (t235 *)m10591_gshared((t294 *)__this, (t2180 *)p0, method)
#define m14522(__this, p0, method) (void)m10593_gshared((t29 *)__this, (t2180 *)p0, method)
#define m14523(__this, p0, p1, p2, method) (int32_t)m10595_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, (t2180 *)p2, method)
 t2682  m14524 (t237 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
#define m14525(__this, p0, method) (int32_t)m10598_gshared((t294 *)__this, (t29 *)p0, method)
#define m14526(__this, p0, p1, method) (void)m10600_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, method)
#define m14527(__this, p0, method) (void)m10602_gshared((t294 *)__this, (int32_t)p0, method)
#define m14528(__this, p0, p1, method) (void)m10604_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m14529(__this, p0, method) (void)m10606_gshared((t294 *)__this, (t29*)p0, method)
#define m14530(__this, p0, method) (bool)m10608_gshared((t294 *)__this, (t29 *)p0, method)
#define m14531(__this, p0, method) (int32_t)m10610_gshared((t294 *)__this, (t2180 *)p0, method)
#define m1911(__this, p0, method) (void)m10611_gshared((t294 *)__this, (int32_t)p0, method)
#define m14532(__this, method) (void)m10613_gshared((t294 *)__this, method)
#define m14533(__this, method) (void)m10615_gshared((t294 *)__this, method)
#define m14534(__this, p0, method) (void)m10617_gshared((t294 *)__this, (t2181 *)p0, method)
#define m14535(__this, method) (t2675*)m10619_gshared((t294 *)__this, method)
#define m14536(__this, method) (void)m10621_gshared((t294 *)__this, method)
#define m14537(__this, method) (int32_t)m10623_gshared((t294 *)__this, method)
#define m14538(__this, p0, method) (void)m10625_gshared((t294 *)__this, (int32_t)p0, method)
#define m1906(__this, method) (int32_t)m10626_gshared((t294 *)__this, method)
#define m1905(__this, p0, method) (t235 *)m10627_gshared((t294 *)__this, (int32_t)p0, method)
#define m14539(__this, p0, p1, method) (void)m10629_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
