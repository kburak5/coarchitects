﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2597;
struct t29;
struct t20;
#include "t176.h"

 void m13996 (t2597 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m13997 (t2597 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13998 (t2597 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13999 (t2597 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m14000 (t2597 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
