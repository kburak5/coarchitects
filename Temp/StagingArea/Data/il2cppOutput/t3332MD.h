﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3332;
struct t29;
struct t20;
#include "t1284.h"

 void m18532 (t3332 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18533 (t3332 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18534 (t3332 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18535 (t3332 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18536 (t3332 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
