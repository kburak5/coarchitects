﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2229;
struct t29;
struct t20;
struct t346;
struct t136;
#include "t2230.h"

 void m11064_gshared (t2229 * __this, MethodInfo* method);
#define m11064(__this, method) (void)m11064_gshared((t2229 *)__this, method)
 bool m11065_gshared (t2229 * __this, MethodInfo* method);
#define m11065(__this, method) (bool)m11065_gshared((t2229 *)__this, method)
 t29 * m11066_gshared (t2229 * __this, MethodInfo* method);
#define m11066(__this, method) (t29 *)m11066_gshared((t2229 *)__this, method)
 void m11067_gshared (t2229 * __this, t20 * p0, int32_t p1, MethodInfo* method);
#define m11067(__this, p0, p1, method) (void)m11067_gshared((t2229 *)__this, (t20 *)p0, (int32_t)p1, method)
 t29* m11068_gshared (t2229 * __this, MethodInfo* method);
#define m11068(__this, method) (t29*)m11068_gshared((t2229 *)__this, method)
 t29 * m11069_gshared (t2229 * __this, MethodInfo* method);
#define m11069(__this, method) (t29 *)m11069_gshared((t2229 *)__this, method)
 t29 * m11070_gshared (t2229 * __this, MethodInfo* method);
#define m11070(__this, method) (t29 *)m11070_gshared((t2229 *)__this, method)
 t29 * m11071_gshared (t2229 * __this, MethodInfo* method);
#define m11071(__this, method) (t29 *)m11071_gshared((t2229 *)__this, method)
 void m11072_gshared (t2229 * __this, t29 * p0, MethodInfo* method);
#define m11072(__this, p0, method) (void)m11072_gshared((t2229 *)__this, (t29 *)p0, method)
 int32_t m11073_gshared (t2229 * __this, MethodInfo* method);
#define m11073(__this, method) (int32_t)m11073_gshared((t2229 *)__this, method)
 t2230  m11074 (t2229 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
