﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include "t577.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t577_TI;
#include "t577MD.h"

#include "t21.h"
#include "t490MD.h"
extern MethodInfo m2881_MI;

#include "t20.h"

extern MethodInfo m2830_MI;
 void m2830 (t577 * __this, MethodInfo* method){
	{
		m2881(__this, &m2881_MI);
		return;
	}
}
// Metadata Definition UnityEngine.Internal.ExcludeFromDocsAttribute
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2830_MI = 
{
	".ctor", (methodPointerType)&m2830, &t577_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 1172, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t577_MIs[] =
{
	&m2830_MI,
	NULL
};
extern MethodInfo m2882_MI;
extern MethodInfo m46_MI;
extern MethodInfo m2883_MI;
extern MethodInfo m1332_MI;
static MethodInfo* t577_VT[] =
{
	&m2882_MI,
	&m46_MI,
	&m2883_MI,
	&m1332_MI,
};
extern TypeInfo t604_TI;
static Il2CppInterfaceOffsetPair t577_IOs[] = 
{
	{ &t604_TI, 4},
};
extern TypeInfo t629_TI;
#include "t629.h"
#include "t629MD.h"
extern MethodInfo m2915_MI;
void t577_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t629 * tmp;
		tmp = (t629 *)il2cpp_codegen_object_new (&t629_TI);
		m2915(tmp, 64, &m2915_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t577__CustomAttributeCache = {
1,
NULL,
&t577_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t577_0_0_0;
extern Il2CppType t577_1_0_0;
extern TypeInfo t490_TI;
struct t577;
extern CustomAttributesCache t577__CustomAttributeCache;
TypeInfo t577_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "ExcludeFromDocsAttribute", "UnityEngine.Internal", t577_MIs, NULL, NULL, NULL, &t490_TI, NULL, NULL, &t577_TI, NULL, t577_VT, &t577__CustomAttributeCache, &t577_TI, &t577_0_0_0, &t577_1_0_0, t577_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t577), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 4, 0, 1};
#include "t299.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t299_TI;
#include "t299MD.h"

#include "t7.h"


extern MethodInfo m1318_MI;
 void m1318 (t299 * __this, t7* p0, MethodInfo* method){
	{
		m2881(__this, &m2881_MI);
		__this->f0 = p0;
		return;
	}
}
// Metadata Definition UnityEngine.Serialization.FormerlySerializedAsAttribute
extern Il2CppType t7_0_0_1;
FieldInfo t299_f0_FieldInfo = 
{
	"m_oldName", &t7_0_0_1, &t299_TI, offsetof(t299, f0), &EmptyCustomAttributesCache};
static FieldInfo* t299_FIs[] =
{
	&t299_f0_FieldInfo,
	NULL
};
extern Il2CppType t7_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t299_m1318_ParameterInfos[] = 
{
	{"oldName", 0, 134218924, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1318_MI = 
{
	".ctor", (methodPointerType)&m1318, &t299_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t299_m1318_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 1173, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t299_MIs[] =
{
	&m1318_MI,
	NULL
};
static MethodInfo* t299_VT[] =
{
	&m2882_MI,
	&m46_MI,
	&m2883_MI,
	&m1332_MI,
};
static Il2CppInterfaceOffsetPair t299_IOs[] = 
{
	{ &t604_TI, 4},
};
extern MethodInfo m2917_MI;
extern MethodInfo m2916_MI;
void t299_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t629 * tmp;
		tmp = (t629 *)il2cpp_codegen_object_new (&t629_TI);
		m2915(tmp, 256, &m2915_MI);
		m2917(tmp, true, &m2917_MI);
		m2916(tmp, false, &m2916_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t299__CustomAttributeCache = {
1,
NULL,
&t299_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t299_0_0_0;
extern Il2CppType t299_1_0_0;
struct t299;
extern CustomAttributesCache t299__CustomAttributeCache;
TypeInfo t299_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "FormerlySerializedAsAttribute", "UnityEngine.Serialization", t299_MIs, NULL, t299_FIs, NULL, &t490_TI, NULL, NULL, &t299_TI, NULL, t299_VT, &t299__CustomAttributeCache, &t299_TI, &t299_0_0_0, &t299_1_0_0, t299_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t299), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 1, 0, 0, 4, 0, 1};
#include "t578.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t578_TI;
#include "t578MD.h"



// Metadata Definition UnityEngineInternal.TypeInferenceRules
extern Il2CppType t44_0_0_1542;
FieldInfo t578_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t578_TI, offsetof(t578, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t578_0_0_32854;
FieldInfo t578_f2_FieldInfo = 
{
	"TypeReferencedByFirstArgument", &t578_0_0_32854, &t578_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t578_0_0_32854;
FieldInfo t578_f3_FieldInfo = 
{
	"TypeReferencedBySecondArgument", &t578_0_0_32854, &t578_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t578_0_0_32854;
FieldInfo t578_f4_FieldInfo = 
{
	"ArrayOfTypeReferencedByFirstArgument", &t578_0_0_32854, &t578_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t578_0_0_32854;
FieldInfo t578_f5_FieldInfo = 
{
	"TypeOfFirstArgument", &t578_0_0_32854, &t578_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t578_FIs[] =
{
	&t578_f1_FieldInfo,
	&t578_f2_FieldInfo,
	&t578_f3_FieldInfo,
	&t578_f4_FieldInfo,
	&t578_f5_FieldInfo,
	NULL
};
static const int32_t t578_f2_DefaultValueData = 0;
extern Il2CppType t44_0_0_0;
static Il2CppFieldDefaultValueEntry t578_f2_DefaultValue = 
{
	&t578_f2_FieldInfo, { (char*)&t578_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t578_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t578_f3_DefaultValue = 
{
	&t578_f3_FieldInfo, { (char*)&t578_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t578_f4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t578_f4_DefaultValue = 
{
	&t578_f4_FieldInfo, { (char*)&t578_f4_DefaultValueData, &t44_0_0_0 }};
static const int32_t t578_f5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry t578_f5_DefaultValue = 
{
	&t578_f5_FieldInfo, { (char*)&t578_f5_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t578_FDVs[] = 
{
	&t578_f2_DefaultValue,
	&t578_f3_DefaultValue,
	&t578_f4_DefaultValue,
	&t578_f5_DefaultValue,
	NULL
};
static MethodInfo* t578_MIs[] =
{
	NULL
};
extern MethodInfo m1248_MI;
extern MethodInfo m1249_MI;
extern MethodInfo m1250_MI;
extern MethodInfo m1251_MI;
extern MethodInfo m1252_MI;
extern MethodInfo m1253_MI;
extern MethodInfo m1254_MI;
extern MethodInfo m1255_MI;
extern MethodInfo m1256_MI;
extern MethodInfo m1257_MI;
extern MethodInfo m1258_MI;
extern MethodInfo m1259_MI;
extern MethodInfo m1260_MI;
extern MethodInfo m1261_MI;
extern MethodInfo m1262_MI;
extern MethodInfo m1263_MI;
extern MethodInfo m1264_MI;
extern MethodInfo m1265_MI;
extern MethodInfo m1266_MI;
extern MethodInfo m1267_MI;
extern MethodInfo m1268_MI;
extern MethodInfo m1269_MI;
static MethodInfo* t578_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
extern TypeInfo t288_TI;
extern TypeInfo t289_TI;
extern TypeInfo t290_TI;
static Il2CppInterfaceOffsetPair t578_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t578_0_0_0;
extern Il2CppType t578_1_0_0;
extern TypeInfo t49_TI;
#include "t44.h"
extern TypeInfo t44_TI;
TypeInfo t578_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "TypeInferenceRules", "UnityEngineInternal", t578_MIs, NULL, t578_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t578_VT, &EmptyCustomAttributesCache, &t44_TI, &t578_0_0_0, &t578_1_0_0, t578_IOs, NULL, NULL, t578_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t578)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 257, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 5, 0, 0, 23, 0, 3};
#include "t579.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t579_TI;
#include "t579MD.h"

#include "t49.h"
extern TypeInfo t7_TI;
#include "t49MD.h"
extern MethodInfo m2832_MI;


extern MethodInfo m2831_MI;
 void m2831 (t579 * __this, int32_t p0, MethodInfo* method){
	{
		int32_t L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t578_TI), &L_0);
		t7* L_2 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1250_MI, L_1);
		m2832(__this, L_2, &m2832_MI);
		return;
	}
}
 void m2832 (t579 * __this, t7* p0, MethodInfo* method){
	{
		m2881(__this, &m2881_MI);
		__this->f0 = p0;
		return;
	}
}
extern MethodInfo m2833_MI;
 t7* m2833 (t579 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f0);
		return L_0;
	}
}
// Metadata Definition UnityEngineInternal.TypeInferenceRuleAttribute
extern Il2CppType t7_0_0_33;
FieldInfo t579_f0_FieldInfo = 
{
	"_rule", &t7_0_0_33, &t579_TI, offsetof(t579, f0), &EmptyCustomAttributesCache};
static FieldInfo* t579_FIs[] =
{
	&t579_f0_FieldInfo,
	NULL
};
extern Il2CppType t578_0_0_0;
static ParameterInfo t579_m2831_ParameterInfos[] = 
{
	{"rule", 0, 134218925, &EmptyCustomAttributesCache, &t578_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m2831_MI = 
{
	".ctor", (methodPointerType)&m2831, &t579_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t579_m2831_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 1174, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t579_m2832_ParameterInfos[] = 
{
	{"rule", 0, 134218926, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2832_MI = 
{
	".ctor", (methodPointerType)&m2832, &t579_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t579_m2832_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 1175, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2833_MI = 
{
	"ToString", (methodPointerType)&m2833, &t579_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 3, 0, false, false, 1176, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t579_MIs[] =
{
	&m2831_MI,
	&m2832_MI,
	&m2833_MI,
	NULL
};
static MethodInfo* t579_VT[] =
{
	&m2882_MI,
	&m46_MI,
	&m2883_MI,
	&m2833_MI,
};
static Il2CppInterfaceOffsetPair t579_IOs[] = 
{
	{ &t604_TI, 4},
};
void t579_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t629 * tmp;
		tmp = (t629 *)il2cpp_codegen_object_new (&t629_TI);
		m2915(tmp, 64, &m2915_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t579__CustomAttributeCache = {
1,
NULL,
&t579_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t579_0_0_0;
extern Il2CppType t579_1_0_0;
struct t579;
extern CustomAttributesCache t579__CustomAttributeCache;
TypeInfo t579_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "TypeInferenceRuleAttribute", "UnityEngineInternal", t579_MIs, NULL, t579_FIs, NULL, &t490_TI, NULL, NULL, &t579_TI, NULL, t579_VT, &t579__CustomAttributeCache, &t579_TI, &t579_0_0_0, &t579_1_0_0, t579_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t579), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, false, false, false, false, false, false, false, false, 3, 0, 1, 0, 0, 4, 0, 1};
#include "t464.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t464_TI;
#include "t464MD.h"

#include "t580MD.h"
extern MethodInfo m3017_MI;


extern MethodInfo m2834_MI;
 void m2834 (t464 * __this, MethodInfo* method){
	{
		m3017(__this, &m3017_MI);
		return;
	}
}
// Metadata Definition UnityEngineInternal.GenericStack
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2834_MI = 
{
	".ctor", (methodPointerType)&m2834, &t464_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 1177, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t464_MIs[] =
{
	&m2834_MI,
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m3018_MI;
extern MethodInfo m3019_MI;
extern MethodInfo m3020_MI;
extern MethodInfo m3021_MI;
extern MethodInfo m3022_MI;
extern MethodInfo m2849_MI;
extern MethodInfo m3023_MI;
extern MethodInfo m3024_MI;
extern MethodInfo m2845_MI;
static MethodInfo* t464_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m3018_MI,
	&m3019_MI,
	&m3020_MI,
	&m3021_MI,
	&m3022_MI,
	&m3019_MI,
	&m3020_MI,
	&m3021_MI,
	&m2849_MI,
	&m3022_MI,
	&m3018_MI,
	&m3023_MI,
	&m3024_MI,
	&m2845_MI,
};
extern TypeInfo t603_TI;
extern TypeInfo t373_TI;
extern TypeInfo t674_TI;
static Il2CppInterfaceOffsetPair t464_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t464_0_0_0;
extern Il2CppType t464_1_0_0;
extern TypeInfo t580_TI;
struct t464;
TypeInfo t464_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "GenericStack", "UnityEngineInternal", t464_MIs, NULL, NULL, NULL, &t580_TI, NULL, NULL, &t464_TI, NULL, t464_VT, &EmptyCustomAttributesCache, &t464_TI, &t464_0_0_0, &t464_1_0_0, t464_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t464), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 18, 0, 3};
#include "t34.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t34_TI;
#include "t34MD.h"

#include "t29.h"
#include "t35.h"
#include "t67.h"


extern MethodInfo m52_MI;
 void m52 (t34 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m1564_MI;
 void m1564 (t34 * __this, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m1564((t34 *)__this->f9, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (t29 *, t29 * __this, MethodInfo* method);
		((FunctionPointerType)__this->f0)(NULL,__this->f2,(MethodInfo*)(__this->f3.f0));
	}
	typedef void (*FunctionPointerType) (t29 * __this, MethodInfo* method);
	((FunctionPointerType)__this->f0)(__this->f2,(MethodInfo*)(__this->f3.f0));
}
void pinvoke_delegate_wrapper_t34(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
extern MethodInfo m2835_MI;
 t29 * m2835 (t34 * __this, t67 * p0, t29 * p1, MethodInfo* method){
	void *__d_args[1] = {0};
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p0, (Il2CppObject*)p1);
}
extern MethodInfo m2836_MI;
 void m2836 (t34 * __this, t29 * p0, MethodInfo* method){
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
}
// Metadata Definition UnityEngine.Events.UnityAction
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t34_m52_ParameterInfos[] = 
{
	{"object", 0, 134218927, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134218928, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
MethodInfo m52_MI = 
{
	".ctor", (methodPointerType)&m52, &t34_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t34_m52_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, false, 1178, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1564_MI = 
{
	"Invoke", (methodPointerType)&m1564, &t34_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 454, 3, 10, 0, false, false, 1179, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t67_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t34_m2835_ParameterInfos[] = 
{
	{"callback", 0, 134218929, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 1, 134218930, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2835_MI = 
{
	"BeginInvoke", (methodPointerType)&m2835, &t34_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29, t34_m2835_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 2, false, false, 1180, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t66_0_0_0;
extern Il2CppType t66_0_0_0;
static ParameterInfo t34_m2836_ParameterInfos[] = 
{
	{"result", 0, 134218931, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2836_MI = 
{
	"EndInvoke", (methodPointerType)&m2836, &t34_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t34_m2836_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, false, 1181, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t34_MIs[] =
{
	&m52_MI,
	&m1564_MI,
	&m2835_MI,
	&m2836_MI,
	NULL
};
extern MethodInfo m1699_MI;
extern MethodInfo m1700_MI;
extern MethodInfo m1701_MI;
extern MethodInfo m1702_MI;
extern MethodInfo m1703_MI;
extern MethodInfo m1704_MI;
extern MethodInfo m1705_MI;
static MethodInfo* t34_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m1564_MI,
	&m2835_MI,
	&m2836_MI,
};
extern TypeInfo t374_TI;
static Il2CppInterfaceOffsetPair t34_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t34_0_0_0;
extern Il2CppType t34_1_0_0;
extern TypeInfo t195_TI;
struct t34;
TypeInfo t34_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction", "UnityEngine.Events", t34_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t34_TI, NULL, t34_VT, &EmptyCustomAttributesCache, &t34_TI, &t34_0_0_0, &t34_1_0_0, t34_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)pinvoke_delegate_wrapper_t34, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t34), 0, sizeof(methodPointerType), 0, 0, -1, 257, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t581.h"
extern Il2CppGenericContainer t581_IGC;
extern TypeInfo t581_gp_T0_0_TI;
Il2CppGenericParamFull t581_gp_T0_0_TI_GenericParamFull = { { &t581_IGC, 0}, {NULL, "T0", 0, 0, NULL} };
static Il2CppGenericParamFull* t581_IGPA[1] = 
{
	&t581_gp_T0_0_TI_GenericParamFull,
};
extern TypeInfo t581_TI;
Il2CppGenericContainer t581_IGC = { { NULL, NULL }, NULL, &t581_TI, 1, 0, t581_IGPA };
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t581_m3025_ParameterInfos[] = 
{
	{"object", 0, 134218932, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134218933, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m3025_MI = 
{
	".ctor", NULL, &t581_TI, &t21_0_0_0, NULL, t581_m3025_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, false, 1182, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t581_gp_0_0_0_0;
extern Il2CppType t581_gp_0_0_0_0;
static ParameterInfo t581_m3026_ParameterInfos[] = 
{
	{"arg0", 0, 134218934, &EmptyCustomAttributesCache, &t581_gp_0_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m3026_MI = 
{
	"Invoke", NULL, &t581_TI, &t21_0_0_0, NULL, t581_m3026_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, false, 1183, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t581_gp_0_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t581_m3027_ParameterInfos[] = 
{
	{"arg0", 0, 134218935, &EmptyCustomAttributesCache, &t581_gp_0_0_0_0},
	{"callback", 1, 134218936, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134218937, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
MethodInfo m3027_MI = 
{
	"BeginInvoke", NULL, &t581_TI, &t66_0_0_0, NULL, t581_m3027_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, false, 1184, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t66_0_0_0;
static ParameterInfo t581_m3028_ParameterInfos[] = 
{
	{"result", 0, 134218938, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m3028_MI = 
{
	"EndInvoke", NULL, &t581_TI, &t21_0_0_0, NULL, t581_m3028_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, false, 1185, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t581_MIs[] =
{
	&m3025_MI,
	&m3026_MI,
	&m3027_MI,
	&m3028_MI,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t581_0_0_0;
extern Il2CppType t581_1_0_0;
struct t581;
TypeInfo t581_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t581_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t581_TI, NULL, NULL, NULL, NULL, &t581_0_0_0, &t581_1_0_0, NULL, NULL, &t581_IGC, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 0, 0, 0};
#include "t582.h"
extern Il2CppGenericContainer t582_IGC;
extern TypeInfo t582_gp_T0_0_TI;
Il2CppGenericParamFull t582_gp_T0_0_TI_GenericParamFull = { { &t582_IGC, 0}, {NULL, "T0", 0, 0, NULL} };
extern TypeInfo t582_gp_T1_1_TI;
Il2CppGenericParamFull t582_gp_T1_1_TI_GenericParamFull = { { &t582_IGC, 1}, {NULL, "T1", 0, 0, NULL} };
static Il2CppGenericParamFull* t582_IGPA[2] = 
{
	&t582_gp_T0_0_TI_GenericParamFull,
	&t582_gp_T1_1_TI_GenericParamFull,
};
extern TypeInfo t582_TI;
Il2CppGenericContainer t582_IGC = { { NULL, NULL }, NULL, &t582_TI, 2, 0, t582_IGPA };
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t582_m3029_ParameterInfos[] = 
{
	{"object", 0, 134218939, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134218940, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m3029_MI = 
{
	".ctor", NULL, &t582_TI, &t21_0_0_0, NULL, t582_m3029_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, false, 1186, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t582_gp_0_0_0_0;
extern Il2CppType t582_gp_0_0_0_0;
extern Il2CppType t582_gp_1_0_0_0;
extern Il2CppType t582_gp_1_0_0_0;
static ParameterInfo t582_m3030_ParameterInfos[] = 
{
	{"arg0", 0, 134218941, &EmptyCustomAttributesCache, &t582_gp_0_0_0_0},
	{"arg1", 1, 134218942, &EmptyCustomAttributesCache, &t582_gp_1_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m3030_MI = 
{
	"Invoke", NULL, &t582_TI, &t21_0_0_0, NULL, t582_m3030_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, false, 1187, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t582_gp_0_0_0_0;
extern Il2CppType t582_gp_1_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t582_m3031_ParameterInfos[] = 
{
	{"arg0", 0, 134218943, &EmptyCustomAttributesCache, &t582_gp_0_0_0_0},
	{"arg1", 1, 134218944, &EmptyCustomAttributesCache, &t582_gp_1_0_0_0},
	{"callback", 2, 134218945, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134218946, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
MethodInfo m3031_MI = 
{
	"BeginInvoke", NULL, &t582_TI, &t66_0_0_0, NULL, t582_m3031_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, false, 1188, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t66_0_0_0;
static ParameterInfo t582_m3032_ParameterInfos[] = 
{
	{"result", 0, 134218947, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m3032_MI = 
{
	"EndInvoke", NULL, &t582_TI, &t21_0_0_0, NULL, t582_m3032_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, false, 1189, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t582_MIs[] =
{
	&m3029_MI,
	&m3030_MI,
	&m3031_MI,
	&m3032_MI,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t582_0_0_0;
extern Il2CppType t582_1_0_0;
struct t582;
TypeInfo t582_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`2", "UnityEngine.Events", t582_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t582_TI, NULL, NULL, NULL, NULL, &t582_0_0_0, &t582_1_0_0, NULL, NULL, &t582_IGC, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 0, 0, 0};
#include "t583.h"
extern Il2CppGenericContainer t583_IGC;
extern TypeInfo t583_gp_T0_0_TI;
Il2CppGenericParamFull t583_gp_T0_0_TI_GenericParamFull = { { &t583_IGC, 0}, {NULL, "T0", 0, 0, NULL} };
extern TypeInfo t583_gp_T1_1_TI;
Il2CppGenericParamFull t583_gp_T1_1_TI_GenericParamFull = { { &t583_IGC, 1}, {NULL, "T1", 0, 0, NULL} };
extern TypeInfo t583_gp_T2_2_TI;
Il2CppGenericParamFull t583_gp_T2_2_TI_GenericParamFull = { { &t583_IGC, 2}, {NULL, "T2", 0, 0, NULL} };
static Il2CppGenericParamFull* t583_IGPA[3] = 
{
	&t583_gp_T0_0_TI_GenericParamFull,
	&t583_gp_T1_1_TI_GenericParamFull,
	&t583_gp_T2_2_TI_GenericParamFull,
};
extern TypeInfo t583_TI;
Il2CppGenericContainer t583_IGC = { { NULL, NULL }, NULL, &t583_TI, 3, 0, t583_IGPA };
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t583_m3033_ParameterInfos[] = 
{
	{"object", 0, 134218948, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134218949, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m3033_MI = 
{
	".ctor", NULL, &t583_TI, &t21_0_0_0, NULL, t583_m3033_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, false, 1190, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t583_gp_0_0_0_0;
extern Il2CppType t583_gp_0_0_0_0;
extern Il2CppType t583_gp_1_0_0_0;
extern Il2CppType t583_gp_1_0_0_0;
extern Il2CppType t583_gp_2_0_0_0;
extern Il2CppType t583_gp_2_0_0_0;
static ParameterInfo t583_m3034_ParameterInfos[] = 
{
	{"arg0", 0, 134218950, &EmptyCustomAttributesCache, &t583_gp_0_0_0_0},
	{"arg1", 1, 134218951, &EmptyCustomAttributesCache, &t583_gp_1_0_0_0},
	{"arg2", 2, 134218952, &EmptyCustomAttributesCache, &t583_gp_2_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m3034_MI = 
{
	"Invoke", NULL, &t583_TI, &t21_0_0_0, NULL, t583_m3034_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 3, false, false, 1191, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t583_gp_0_0_0_0;
extern Il2CppType t583_gp_1_0_0_0;
extern Il2CppType t583_gp_2_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t583_m3035_ParameterInfos[] = 
{
	{"arg0", 0, 134218953, &EmptyCustomAttributesCache, &t583_gp_0_0_0_0},
	{"arg1", 1, 134218954, &EmptyCustomAttributesCache, &t583_gp_1_0_0_0},
	{"arg2", 2, 134218955, &EmptyCustomAttributesCache, &t583_gp_2_0_0_0},
	{"callback", 3, 134218956, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 4, 134218957, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
MethodInfo m3035_MI = 
{
	"BeginInvoke", NULL, &t583_TI, &t66_0_0_0, NULL, t583_m3035_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 5, false, false, 1192, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t66_0_0_0;
static ParameterInfo t583_m3036_ParameterInfos[] = 
{
	{"result", 0, 134218958, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m3036_MI = 
{
	"EndInvoke", NULL, &t583_TI, &t21_0_0_0, NULL, t583_m3036_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, false, 1193, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t583_MIs[] =
{
	&m3033_MI,
	&m3034_MI,
	&m3035_MI,
	&m3036_MI,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t583_0_0_0;
extern Il2CppType t583_1_0_0;
struct t583;
TypeInfo t583_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`3", "UnityEngine.Events", t583_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t583_TI, NULL, NULL, NULL, NULL, &t583_0_0_0, &t583_1_0_0, NULL, NULL, &t583_IGC, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 0, 0, 0};
#include "t584.h"
extern Il2CppGenericContainer t584_IGC;
extern TypeInfo t584_gp_T0_0_TI;
Il2CppGenericParamFull t584_gp_T0_0_TI_GenericParamFull = { { &t584_IGC, 0}, {NULL, "T0", 0, 0, NULL} };
extern TypeInfo t584_gp_T1_1_TI;
Il2CppGenericParamFull t584_gp_T1_1_TI_GenericParamFull = { { &t584_IGC, 1}, {NULL, "T1", 0, 0, NULL} };
extern TypeInfo t584_gp_T2_2_TI;
Il2CppGenericParamFull t584_gp_T2_2_TI_GenericParamFull = { { &t584_IGC, 2}, {NULL, "T2", 0, 0, NULL} };
extern TypeInfo t584_gp_T3_3_TI;
Il2CppGenericParamFull t584_gp_T3_3_TI_GenericParamFull = { { &t584_IGC, 3}, {NULL, "T3", 0, 0, NULL} };
static Il2CppGenericParamFull* t584_IGPA[4] = 
{
	&t584_gp_T0_0_TI_GenericParamFull,
	&t584_gp_T1_1_TI_GenericParamFull,
	&t584_gp_T2_2_TI_GenericParamFull,
	&t584_gp_T3_3_TI_GenericParamFull,
};
extern TypeInfo t584_TI;
Il2CppGenericContainer t584_IGC = { { NULL, NULL }, NULL, &t584_TI, 4, 0, t584_IGPA };
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t584_m3037_ParameterInfos[] = 
{
	{"object", 0, 134218959, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134218960, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m3037_MI = 
{
	".ctor", NULL, &t584_TI, &t21_0_0_0, NULL, t584_m3037_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, false, 1194, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t584_gp_0_0_0_0;
extern Il2CppType t584_gp_0_0_0_0;
extern Il2CppType t584_gp_1_0_0_0;
extern Il2CppType t584_gp_1_0_0_0;
extern Il2CppType t584_gp_2_0_0_0;
extern Il2CppType t584_gp_2_0_0_0;
extern Il2CppType t584_gp_3_0_0_0;
extern Il2CppType t584_gp_3_0_0_0;
static ParameterInfo t584_m3038_ParameterInfos[] = 
{
	{"arg0", 0, 134218961, &EmptyCustomAttributesCache, &t584_gp_0_0_0_0},
	{"arg1", 1, 134218962, &EmptyCustomAttributesCache, &t584_gp_1_0_0_0},
	{"arg2", 2, 134218963, &EmptyCustomAttributesCache, &t584_gp_2_0_0_0},
	{"arg3", 3, 134218964, &EmptyCustomAttributesCache, &t584_gp_3_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m3038_MI = 
{
	"Invoke", NULL, &t584_TI, &t21_0_0_0, NULL, t584_m3038_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 4, false, false, 1195, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t584_gp_0_0_0_0;
extern Il2CppType t584_gp_1_0_0_0;
extern Il2CppType t584_gp_2_0_0_0;
extern Il2CppType t584_gp_3_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t584_m3039_ParameterInfos[] = 
{
	{"arg0", 0, 134218965, &EmptyCustomAttributesCache, &t584_gp_0_0_0_0},
	{"arg1", 1, 134218966, &EmptyCustomAttributesCache, &t584_gp_1_0_0_0},
	{"arg2", 2, 134218967, &EmptyCustomAttributesCache, &t584_gp_2_0_0_0},
	{"arg3", 3, 134218968, &EmptyCustomAttributesCache, &t584_gp_3_0_0_0},
	{"callback", 4, 134218969, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 5, 134218970, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
MethodInfo m3039_MI = 
{
	"BeginInvoke", NULL, &t584_TI, &t66_0_0_0, NULL, t584_m3039_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 6, false, false, 1196, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t66_0_0_0;
static ParameterInfo t584_m3040_ParameterInfos[] = 
{
	{"result", 0, 134218971, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m3040_MI = 
{
	"EndInvoke", NULL, &t584_TI, &t21_0_0_0, NULL, t584_m3040_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, false, 1197, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t584_MIs[] =
{
	&m3037_MI,
	&m3038_MI,
	&m3039_MI,
	&m3040_MI,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t584_0_0_0;
extern Il2CppType t584_1_0_0;
struct t584;
TypeInfo t584_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`4", "UnityEngine.Events", t584_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t584_TI, NULL, NULL, NULL, NULL, &t584_0_0_0, &t584_1_0_0, NULL, NULL, &t584_IGC, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 0, 0, 0};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
