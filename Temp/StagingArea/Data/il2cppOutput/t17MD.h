﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t17;
struct t7;
struct t29;
#include "t17.h"
#include "t23.h"

 void m62 (t17 * __this, float p0, float p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1672 (t17 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1687 (t17 * __this, int32_t p0, float p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m1799 (t29 * __this, t17  p0, t17  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2317 (t17 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2318 (t17 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2319 (t17 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1396 (t17 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m2320 (t29 * __this, t17  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m1394 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m1667 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m1965 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m1670 (t29 * __this, t17  p0, t17  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m1416 (t29 * __this, t17  p0, t17  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m1668 (t29 * __this, t17  p0, float p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m1754 (t29 * __this, t17  p0, float p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2046 (t29 * __this, t17  p0, t17  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1792 (t29 * __this, t17  p0, t17  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m1419 (t29 * __this, t23  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m1463 (t29 * __this, t17  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
