﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3233;
struct t29;
struct t20;
#include "t862.h"

 void m17961 (t3233 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17962 (t3233 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17963 (t3233 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17964 (t3233 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17965 (t3233 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
