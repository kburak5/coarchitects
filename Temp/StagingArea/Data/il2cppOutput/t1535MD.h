﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1535;
struct t7;
struct t781;

 void m8261 (t1535 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8262 (t1535 * __this, t7* p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8263 (t1535 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8264 (t1535 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8265 (t1535 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8266 (t1535 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m8267 (t1535 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
