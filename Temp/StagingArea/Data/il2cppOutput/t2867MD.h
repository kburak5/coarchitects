﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2867;
struct t29;
struct t469;
struct t66;
struct t67;
#include "t35.h"
#include "t2859.h"

 void m15594 (t2867 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2859  m15595 (t2867 * __this, int32_t p0, t469 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15596 (t2867 * __this, int32_t p0, t469 * p1, t67 * p2, t29 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2859  m15597 (t2867 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
