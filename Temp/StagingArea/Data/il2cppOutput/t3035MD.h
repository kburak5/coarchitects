﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3035;
struct t29;
struct t528;
#include "t381.h"

 void m16577 (t3035 * __this, t528 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m16578 (t3035 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16579 (t3035 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16580 (t3035 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16581 (t3035 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t381  m16582 (t3035 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
