﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t919;
struct t1094;
struct t29;
struct t42;
struct t7;
struct t295;
#include "t465.h"
#include "t1126.h"
#include "t923.h"

 bool m5331 (int64_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m5332 (int64_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m5333 (int64_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m5334 (int64_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m5335 (int64_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m5336 (int64_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m5337 (int64_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5338 (int64_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m5339 (int64_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int8_t m5340 (int64_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m5341 (int64_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m5342 (int64_t* __this, t42 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m5343 (int64_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m5344 (int64_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m5345 (int64_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5346 (int64_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5347 (int64_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5348 (int64_t* __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5349 (int64_t* __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5350 (int64_t* __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5351 (t29 * __this, t7* p0, bool p1, int64_t* p2, t295 ** p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m5352 (t29 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5353 (t29 * __this, t7* p0, int32_t p1, t29 * p2, bool p3, int64_t* p4, t295 ** p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m5354 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m5355 (t29 * __this, t7* p0, int32_t p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5356 (t29 * __this, t7* p0, int64_t* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4005 (t29 * __this, t7* p0, int32_t p1, t29 * p2, int64_t* p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4007 (int64_t* __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5357 (int64_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5358 (int64_t* __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5359 (int64_t* __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
