﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1318;
struct t1318_marshaled;

void t1318_marshal(const t1318& unmarshaled, t1318_marshaled& marshaled);
void t1318_marshal_back(const t1318_marshaled& marshaled, t1318& unmarshaled);
void t1318_marshal_cleanup(t1318_marshaled& marshaled);
