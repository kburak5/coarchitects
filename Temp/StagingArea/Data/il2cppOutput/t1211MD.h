﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1211;
struct t781;
struct t783;
struct t976;
struct t955;

 void m6348 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6349 (t29 * __this, t781* p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6350 (t29 * __this, t781* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6351 (t29 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6352 (t29 * __this, t783 * p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6353 (t29 * __this, t783 * p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6354 (t29 * __this, t783 * p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6355 (t29 * __this, t783 * p0, t976 * p1, t781* p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6356 (t29 * __this, t783 * p0, t955 * p1, t781* p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6357 (t29 * __this, t783 * p0, t955 * p1, t781* p2, t781* p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6358 (t29 * __this, t783 * p0, t955 * p1, t781* p2, t781* p3, bool p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6359 (t29 * __this, t955 * p0, t781* p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
