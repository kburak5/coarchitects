﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3257;
struct t29;
struct t20;
#include "t1026.h"

 void m18081 (t3257 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18082 (t3257 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18083 (t3257 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18084 (t3257 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18085 (t3257 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
