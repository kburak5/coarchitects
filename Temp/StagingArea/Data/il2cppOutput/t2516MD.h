﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2516;
struct t29;
struct t66;
struct t67;
#include "t35.h"
#include "t184.h"

 void m13391 (t2516 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m13392 (t2516 * __this, t184  p0, t184  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m13393 (t2516 * __this, t184  p0, t184  p1, t67 * p2, t29 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m13394 (t2516 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
