﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t958;
struct t200;
struct t7;

 void m7228 (t958 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7229 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7230 (t958 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7231 (t958 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7232 (t958 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7233 (t958 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t958 * m7234 (t29 * __this, t958 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7235 (t958 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7236 (t958 * __this, t200* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7237 (t958 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7238 (t958 * __this, t200* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7239 (t958 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4275 (t958 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
