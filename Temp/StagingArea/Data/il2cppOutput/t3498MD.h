﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3498;
struct t29;
struct t20;
#include "t1620.h"

 void m19432 (t3498 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m19433 (t3498 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m19434 (t3498 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19435 (t3498 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m19436 (t3498 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
