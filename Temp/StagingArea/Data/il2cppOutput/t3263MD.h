﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3263;
struct t29;
struct t20;

 void m18111 (t3263 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18112 (t3263 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18113 (t3263 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18114 (t3263 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m18115 (t3263 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
