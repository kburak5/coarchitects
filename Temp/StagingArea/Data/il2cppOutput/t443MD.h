﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t443;
struct t41;
struct t41_marshaled;
struct t444;

 void m2060 (t443 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t41 * m2061 (t443 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t444* m2062 (t443 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
