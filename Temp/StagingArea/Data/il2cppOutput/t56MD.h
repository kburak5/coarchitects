﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t56;
struct t29;
struct t2252;
struct t20;
struct t136;
struct t2253;
struct t2254;
struct t2255;
struct t2251;
struct t2256;
struct t54;
#include "t57.h"
#include "t2257.h"

 void m1397 (t56 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11260 (t56 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11261 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m11262 (t56 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11263 (t56 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m11264 (t56 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m11265 (t56 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m11266 (t56 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m11267 (t56 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11268 (t56 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11269 (t56 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m11270 (t56 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m11271 (t56 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m11272 (t56 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m11273 (t56 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m11274 (t56 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m11275 (t56 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11276 (t56 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1478 (t56 * __this, t57  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11277 (t56 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11278 (t56 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11279 (t56 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11280 (t56 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2255 * m11281 (t56 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1305 (t56 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m11282 (t56 * __this, t57  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11283 (t56 * __this, t2251* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t57  m11284 (t56 * __this, t2256 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11285 (t29 * __this, t2256 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m11286 (t56 * __this, int32_t p0, int32_t p1, t2256 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2257  m11287 (t56 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m11288 (t56 * __this, t57  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11289 (t56 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11290 (t56 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11291 (t56 * __this, int32_t p0, t57  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11292 (t56 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m11293 (t56 * __this, t57  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m11294 (t56 * __this, t2256 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11295 (t56 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11296 (t56 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11297 (t56 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1309 (t56 * __this, t54 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2251* m11298 (t56 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11299 (t56 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m11300 (t56 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11301 (t56 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1400 (t56 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t57  m1399 (t56 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11302 (t56 * __this, int32_t p0, t57  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
