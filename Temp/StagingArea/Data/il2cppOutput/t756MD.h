﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t756;
struct t803;
struct t807;
struct t796;
struct t791;
struct t806;
struct t777;
struct t7;
struct t808;
struct t809;
struct t810;
#include "t811.h"

 void m3351 (t756 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3352 (t756 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3353 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t803 * m3354 (t756 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3355 (t756 * __this, t791 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3356 (t756 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t807 * m3357 (t756 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t807 * m3358 (t756 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t796 * m3359 (t756 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3360 (t756 * __this, t791 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t791 * m3361 (t756 * __this, t791 * p0, t796 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t791 * m3362 (t756 * __this, t791 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3363 (t756 * __this, t791 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3364 (t756 * __this, t791 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3365 (t756 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3366 (t756 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3367 (t756 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3368 (t756 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3369 (t756 * __this, t806 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3370 (t756 * __this, t791 * p0, t777 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3371 (t756 * __this, t791 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3372 (t756 * __this, t791 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3373 (t756 * __this, t808 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3374 (t756 * __this, t809 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3375 (t756 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3376 (t756 * __this, t791 * p0, int32_t p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3377 (t756 * __this, t791 * p0, t791 * p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t808 * m3378 (t756 * __this, t791 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3379 (t756 * __this, t808 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3380 (t756 * __this, t810 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
