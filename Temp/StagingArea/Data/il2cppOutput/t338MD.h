﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t338;
struct t7;
struct t733;
#include "t735.h"

 void m8858 (t338 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2950 (t338 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3966 (t338 * __this, t7* p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8859 (t338 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
