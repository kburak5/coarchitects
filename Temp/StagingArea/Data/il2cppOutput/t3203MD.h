﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3203;

 void m17817 (t3203 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17818 (t3203 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17819 (t3203 * __this, bool p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
