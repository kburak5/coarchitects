﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2448;
struct t29;
struct t365;

 void m12799 (t2448 * __this, t365 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12800 (t2448 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12801 (t2448 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12802 (t2448 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m12803 (t2448 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
