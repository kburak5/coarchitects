﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t515;
struct t7;

 bool m2645 (t515 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2646 (t515 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2647 (t515 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2648 (t515 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m2649 (t515 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m2650 (t515 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2651 (t515 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2652 (t515 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2653 (t515 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
