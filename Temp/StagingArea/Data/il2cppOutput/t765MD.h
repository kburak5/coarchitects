﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t765;
struct t764;
struct t7;
struct t29;

 void m3200 (t765 * __this, t764* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3201 (t765 * __this, t764* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3202 (t765 * __this, t764* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3203 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t765 * m3204 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3205 (t29 * __this, t764* p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3206 (t29 * __this, t7* p0, int32_t* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3207 (t29 * __this, t7* p0, t765 ** p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t764* m3208 (t765 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m3209 (t765 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3210 (t765 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3211 (t29 * __this, t765 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m3212 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3213 (t765 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3214 (t765 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3215 (t765 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3216 (t765 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3217 (t765 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3218 (t765 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3219 (t765 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3220 (t29 * __this, int32_t p0, int32_t p1, int32_t p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
