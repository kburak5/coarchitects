﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1281;
struct t29;
struct t926;
#include "t725.h"
#include "t1280.h"

 void m6737 (t1281 * __this, t926 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6738 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6739 (t1281 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6740 (t1281 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t725  m6741 (t1281 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m6742 (t1281 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m6743 (t1281 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m6744 (t1281 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
