﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3;
struct t24;
struct t347;
struct t156;
#include "t361.h"

 void m1525 (t29 * __this, t347 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2716 (t29 * __this, t347 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1605 (t3 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1947 (t3 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t24 * m1609 (t3 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1917 (t3 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1951 (t3 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1650 (t3 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1952 (t3 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1588 (t3 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1607 (t3 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1606 (t3 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1625 (t3 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t156 * m1551 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t156 * m1913 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2717 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1846 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
