﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t451;
struct t29;
#include "t451.h"

 float m2080 (t451 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2081 (t451 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m2082 (t451 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2083 (t451 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m2084 (t451 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2085 (t451 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m2086 (t451 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2087 (t451 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2088 (t451 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2089 (t451 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2090 (t451 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2091 (t451 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2092 (t451 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2093 (t451 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2094 (t451 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2095 (t451 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2096 (t451 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2097 (t451 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2098 (t29 * __this, t451  p0, t451  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2099 (t29 * __this, t451  p0, t451  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
