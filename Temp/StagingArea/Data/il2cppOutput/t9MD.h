﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t9;
struct t33;
struct t6;
struct t53;
struct t136;

 void m373 (t9 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t33 * m51 (t9 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m374 (t9 * __this, t33 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m375 (t9 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m376 (t9 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m377 (t9 * __this, t53 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m378 (t9 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
