﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2606;
struct t29;
struct t20;
#include "t381.h"

 void m14036 (t2606 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m14037 (t2606 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m14038 (t2606 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m14039 (t2606 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t381  m14040 (t2606 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
