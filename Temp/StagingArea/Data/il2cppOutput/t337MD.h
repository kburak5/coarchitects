﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t337;
struct t339;
#include "t23.h"
#include "t127.h"
#include "t329.h"

 bool m2596 (t29 * __this, t23  p0, t23  p1, t127 * p2, float p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2597 (t29 * __this, t23 * p0, t23 * p1, t127 * p2, float p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2598 (t29 * __this, t23  p0, t23  p1, t127 * p2, float p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1614 (t29 * __this, t329  p0, t127 * p1, float p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t339* m1483 (t29 * __this, t329  p0, float p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t339* m2599 (t29 * __this, t23  p0, t23  p1, float p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t339* m2600 (t29 * __this, t23 * p0, t23 * p1, float p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
