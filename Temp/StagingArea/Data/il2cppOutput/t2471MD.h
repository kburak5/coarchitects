﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2471;
struct t29;
struct t66;
struct t67;
#include "t35.h"
#include "t2465.h"

 void m12967 (t2471 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2465  m12968 (t2471 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12969 (t2471 * __this, t29 * p0, t29 * p1, t67 * p2, t29 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2465  m12970 (t2471 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
