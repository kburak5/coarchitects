﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1508;
struct t1507;
struct t1504;

 void m8102 (t1508 * __this, t1507 * p0, t1507 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8103 (t1508 * __this, t1504 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
