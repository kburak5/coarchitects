﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1069;
struct t1020;
struct t783;
struct t781;

 void m5030 (t1069 * __this, t1020 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5031 (t1069 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5032 (t1069 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5033 (t1069 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t783 * m5034 (t1069 * __this, t783 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m5035 (t1069 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
