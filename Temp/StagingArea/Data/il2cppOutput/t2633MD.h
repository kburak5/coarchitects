﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2633;
struct t29;
struct t557;
struct t2632;
struct t316;

 void m14159 (t2633 * __this, t29 * p0, t557 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m14160 (t2633 * __this, t2632 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m14161 (t2633 * __this, t316* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m14162 (t2633 * __this, t29 * p0, t557 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
