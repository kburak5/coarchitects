﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t227;
struct t29;
struct t352;
struct t2653;
struct t20;
struct t136;
struct t2654;
struct t2655;
struct t2656;
struct t2652;
struct t2657;
struct t2658;
#include "t2659.h"

#include "t294MD.h"
#define m1873(__this, method) (void)m10537_gshared((t294 *)__this, method)
#define m14316(__this, p0, method) (void)m10539_gshared((t294 *)__this, (int32_t)p0, method)
#define m14317(__this, method) (void)m10541_gshared((t29 *)__this, method)
#define m14318(__this, method) (t29*)m10543_gshared((t294 *)__this, method)
#define m14319(__this, p0, p1, method) (void)m10545_gshared((t294 *)__this, (t20 *)p0, (int32_t)p1, method)
#define m14320(__this, method) (t29 *)m10547_gshared((t294 *)__this, method)
#define m14321(__this, p0, method) (int32_t)m10549_gshared((t294 *)__this, (t29 *)p0, method)
#define m14322(__this, p0, method) (bool)m10551_gshared((t294 *)__this, (t29 *)p0, method)
#define m14323(__this, p0, method) (int32_t)m10553_gshared((t294 *)__this, (t29 *)p0, method)
#define m14324(__this, p0, p1, method) (void)m10555_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m14325(__this, p0, method) (void)m10557_gshared((t294 *)__this, (t29 *)p0, method)
#define m14326(__this, method) (bool)m10559_gshared((t294 *)__this, method)
#define m14327(__this, method) (bool)m10561_gshared((t294 *)__this, method)
#define m14328(__this, method) (t29 *)m10563_gshared((t294 *)__this, method)
#define m14329(__this, method) (bool)m10565_gshared((t294 *)__this, method)
#define m14330(__this, method) (bool)m10567_gshared((t294 *)__this, method)
#define m14331(__this, p0, method) (t29 *)m10569_gshared((t294 *)__this, (int32_t)p0, method)
#define m14332(__this, p0, p1, method) (void)m10571_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m14333(__this, p0, method) (void)m10573_gshared((t294 *)__this, (t29 *)p0, method)
#define m14334(__this, p0, method) (void)m10575_gshared((t294 *)__this, (int32_t)p0, method)
#define m14335(__this, p0, method) (void)m10577_gshared((t294 *)__this, (t29*)p0, method)
#define m14336(__this, p0, method) (void)m10579_gshared((t294 *)__this, (t29*)p0, method)
#define m14337(__this, p0, method) (void)m10581_gshared((t294 *)__this, (t29*)p0, method)
#define m14338(__this, method) (t2656 *)m10583_gshared((t294 *)__this, method)
#define m14339(__this, method) (void)m10585_gshared((t294 *)__this, method)
#define m14340(__this, p0, method) (bool)m10587_gshared((t294 *)__this, (t29 *)p0, method)
#define m14341(__this, p0, p1, method) (void)m10589_gshared((t294 *)__this, (t316*)p0, (int32_t)p1, method)
#define m14342(__this, p0, method) (t352 *)m10591_gshared((t294 *)__this, (t2180 *)p0, method)
#define m14343(__this, p0, method) (void)m10593_gshared((t29 *)__this, (t2180 *)p0, method)
#define m14344(__this, p0, p1, p2, method) (int32_t)m10595_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, (t2180 *)p2, method)
 t2659  m14345 (t227 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
#define m14346(__this, p0, method) (int32_t)m10598_gshared((t294 *)__this, (t29 *)p0, method)
#define m14347(__this, p0, p1, method) (void)m10600_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, method)
#define m14348(__this, p0, method) (void)m10602_gshared((t294 *)__this, (int32_t)p0, method)
#define m14349(__this, p0, p1, method) (void)m10604_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m14350(__this, p0, method) (void)m10606_gshared((t294 *)__this, (t29*)p0, method)
#define m14351(__this, p0, method) (bool)m10608_gshared((t294 *)__this, (t29 *)p0, method)
#define m14352(__this, p0, method) (int32_t)m10610_gshared((t294 *)__this, (t2180 *)p0, method)
#define m14353(__this, p0, method) (void)m10611_gshared((t294 *)__this, (int32_t)p0, method)
#define m14354(__this, method) (void)m10613_gshared((t294 *)__this, method)
#define m14355(__this, method) (void)m10615_gshared((t294 *)__this, method)
#define m14356(__this, p0, method) (void)m10617_gshared((t294 *)__this, (t2181 *)p0, method)
#define m14357(__this, method) (t2652*)m10619_gshared((t294 *)__this, method)
#define m14358(__this, method) (void)m10621_gshared((t294 *)__this, method)
#define m14359(__this, method) (int32_t)m10623_gshared((t294 *)__this, method)
#define m14360(__this, p0, method) (void)m10625_gshared((t294 *)__this, (int32_t)p0, method)
#define m1879(__this, method) (int32_t)m10626_gshared((t294 *)__this, method)
#define m1877(__this, p0, method) (t352 *)m10627_gshared((t294 *)__this, (int32_t)p0, method)
#define m14361(__this, p0, p1, method) (void)m10629_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
