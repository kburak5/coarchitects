﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2819;
struct t29;
struct t20;
#include "t445.h"

 void m15359 (t2819 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15360 (t2819 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15361 (t2819 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15362 (t2819 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m15363 (t2819 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
