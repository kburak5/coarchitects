﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t442;
struct t41;
struct t41_marshaled;
struct t7;
struct t42;
struct t444;

 t41 * m2063 (t442 * __this, t7* p0, t42 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t41 * m2064 (t442 * __this, t7* p0, t42 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t444* m2065 (t442 * __this, t7* p0, t42 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
