﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t53;
struct t52;
struct t16;
struct t50;

 void m186 (t53 * __this, t50 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m187 (t53 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m188 (t53 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m189 (t53 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t52 * m190 (t53 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t16 * m191 (t53 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m192 (t53 * __this, t16 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
