﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1383;
struct t42;
struct t7;
struct t1145;
struct t557;
struct t638;
struct t316;
struct t1385;
struct t29;
struct t631;
struct t633;
struct t537;
struct t733;
#include "t1382.h"
#include "t1384.h"
#include "t630.h"
#include "t735.h"

 void m7670 (t1383 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7671 (t1383 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7672 (t1383 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7673 (t1383 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7674 (t1383 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7675 (t1383 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7676 (t1383 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7677 (t1383 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7678 (t1383 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1145* m7679 (t1383 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t557 * m7680 (t1383 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t638* m7681 (t1383 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t557 * m7682 (t1383 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7683 (t1383 * __this, t42 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m7684 (t1383 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m7685 (t1383 * __this, t42 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1385 * m7686 (t29 * __this, t557 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7687 (t1383 * __this, t29 * p0, t316* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7688 (t1383 * __this, t29 * p0, int32_t p1, t631 * p2, t316* p3, t633 * p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7689 (t1383 * __this, t29 * p0, t29 * p1, int32_t p2, t631 * p3, t316* p4, t633 * p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7690 (t1383 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t537* m7691 (t1383 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t537* m7692 (t1383 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7693 (t1383 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
