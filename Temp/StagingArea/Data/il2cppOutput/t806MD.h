﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t806;
struct t791;
struct t804;
#include "t811.h"

 void m3381 (t806 * __this, t791 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t791 * m3382 (t806 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t804* m3383 (t806 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3384 (t806 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3385 (t806 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3386 (t806 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3387 (t806 * __this, t804* p0, int32_t* p1, int32_t p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3388 (t806 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
