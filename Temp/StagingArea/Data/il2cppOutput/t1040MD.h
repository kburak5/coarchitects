﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1040;
struct t945;
struct t783;
struct t781;
struct t1066;
struct t446;
#include "t934.h"

 void m4972 (t1040 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4973 (t1040 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4974 (t1040 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t945 * m4975 (t1040 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4976 (t1040 * __this, t945 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t783 * m4977 (t1040 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t934  m4978 (t1040 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4979 (t1040 * __this, t934  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4980 (t1040 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4981 (t1040 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4982 (t1040 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4983 (t1040 * __this, t1066* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4984 (t1040 * __this, t446* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4985 (t1040 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
