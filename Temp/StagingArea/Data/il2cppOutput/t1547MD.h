﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1547;
struct t781;

 void m8386 (t1547 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8387 (t1547 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8388 (t1547 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8389 (t1547 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m8390 (t1547 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8391 (t1547 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
