﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t984;
struct t983;
struct t781;
struct t793;

 void m4387 (t984 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4388 (t984 * __this, t793 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t983 * m4389 (t984 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4390 (t984 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
