﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t139;
struct t226;
struct t137;
struct t155;
struct t179;
struct t229;
struct t2;
struct t64;
struct t180;
struct t7;
struct t53;
struct t6;
#include "t210.h"
#include "t225.h"
#include "t146.h"
#include "t228.h"
#include "t207.h"
#include "t23.h"
#include "t17.h"
#include "t132.h"

 void m831 (t139 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m832 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t226 * m833 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t210  m834 (t139 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m835 (t139 * __this, t210  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m836 (t139 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m837 (t139 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t146  m838 (t139 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m839 (t139 * __this, t146  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t228  m840 (t139 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m841 (t139 * __this, t228  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t137 * m842 (t139 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m843 (t139 * __this, t137 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t155 * m844 (t139 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m845 (t139 * __this, t155 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m846 (t139 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m847 (t139 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m848 (t139 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m849 (t139 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m850 (t139 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m851 (t139 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m852 (t139 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m853 (t139 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t179 * m854 (t139 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m855 (t139 * __this, t179 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t229 * m856 (t139 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m857 (t139 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m858 (t139 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m859 (t139 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m860 (t139 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m861 (t139 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m862 (t139 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m863 (t139 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m864 (t139 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m865 (t139 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m866 (t139 * __this, int32_t p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t139 * m867 (t139 * __this, t23  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m868 (t29 * __this, t2 * p0, t17  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m869 (t139 * __this, t64 * p0, t139 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t139 * m870 (t139 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t139 * m871 (t139 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t139 * m872 (t139 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t139 * m873 (t139 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m874 (t139 * __this, t64 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m875 (t139 * __this, t132  p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m876 (t139 * __this, t180 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m877 (t139 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m878 (t139 * __this, t53 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m879 (t139 * __this, t53 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m880 (t139 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m881 (t139 * __this, t53 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m882 (t139 * __this, t53 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m883 (t139 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m884 (t139 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m885 (t139 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m886 (t139 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m887 (t139 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m888 (t139 * __this, t53 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m889 (t139 * __this, t53 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m890 (t139 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
