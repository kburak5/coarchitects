﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t13;
struct t7;

 void m22 (t13 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m23 (t13 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m24 (t13 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25 (t13 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
