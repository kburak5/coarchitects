﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1454;
struct t29;
struct t1446;
#include "t725.h"

 void m7883 (t1454 * __this, t1446 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7884 (t1454 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7885 (t1454 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t725  m7886 (t1454 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7887 (t1454 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7888 (t1454 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
