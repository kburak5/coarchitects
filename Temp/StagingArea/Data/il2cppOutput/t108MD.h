﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t108;
struct t29;
struct t109;
struct t2266;
struct t20;
struct t136;
struct t2267;
struct t2268;
struct t2269;
struct t2265;
struct t2270;
struct t2271;
#include "t2272.h"

#include "t294MD.h"
#define m1381(__this, method) (void)m10537_gshared((t294 *)__this, method)
#define m11398(__this, p0, method) (void)m10539_gshared((t294 *)__this, (int32_t)p0, method)
#define m11399(__this, method) (void)m10541_gshared((t29 *)__this, method)
#define m11400(__this, method) (t29*)m10543_gshared((t294 *)__this, method)
#define m11401(__this, p0, p1, method) (void)m10545_gshared((t294 *)__this, (t20 *)p0, (int32_t)p1, method)
#define m11402(__this, method) (t29 *)m10547_gshared((t294 *)__this, method)
#define m11403(__this, p0, method) (int32_t)m10549_gshared((t294 *)__this, (t29 *)p0, method)
#define m11404(__this, p0, method) (bool)m10551_gshared((t294 *)__this, (t29 *)p0, method)
#define m11405(__this, p0, method) (int32_t)m10553_gshared((t294 *)__this, (t29 *)p0, method)
#define m11406(__this, p0, p1, method) (void)m10555_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m11407(__this, p0, method) (void)m10557_gshared((t294 *)__this, (t29 *)p0, method)
#define m11408(__this, method) (bool)m10559_gshared((t294 *)__this, method)
#define m11409(__this, method) (bool)m10561_gshared((t294 *)__this, method)
#define m11410(__this, method) (t29 *)m10563_gshared((t294 *)__this, method)
#define m11411(__this, method) (bool)m10565_gshared((t294 *)__this, method)
#define m11412(__this, method) (bool)m10567_gshared((t294 *)__this, method)
#define m11413(__this, p0, method) (t29 *)m10569_gshared((t294 *)__this, (int32_t)p0, method)
#define m11414(__this, p0, p1, method) (void)m10571_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m1383(__this, p0, method) (void)m10573_gshared((t294 *)__this, (t29 *)p0, method)
#define m11415(__this, p0, method) (void)m10575_gshared((t294 *)__this, (int32_t)p0, method)
#define m11416(__this, p0, method) (void)m10577_gshared((t294 *)__this, (t29*)p0, method)
#define m11417(__this, p0, method) (void)m10579_gshared((t294 *)__this, (t29*)p0, method)
#define m11418(__this, p0, method) (void)m10581_gshared((t294 *)__this, (t29*)p0, method)
#define m11419(__this, method) (t2269 *)m10583_gshared((t294 *)__this, method)
#define m11420(__this, method) (void)m10585_gshared((t294 *)__this, method)
#define m1382(__this, p0, method) (bool)m10587_gshared((t294 *)__this, (t29 *)p0, method)
#define m11421(__this, p0, p1, method) (void)m10589_gshared((t294 *)__this, (t316*)p0, (int32_t)p1, method)
#define m11422(__this, p0, method) (t109 *)m10591_gshared((t294 *)__this, (t2180 *)p0, method)
#define m11423(__this, p0, method) (void)m10593_gshared((t29 *)__this, (t2180 *)p0, method)
#define m11424(__this, p0, p1, p2, method) (int32_t)m10595_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, (t2180 *)p2, method)
 t2272  m11425 (t108 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
#define m11426(__this, p0, method) (int32_t)m10598_gshared((t294 *)__this, (t29 *)p0, method)
#define m11427(__this, p0, p1, method) (void)m10600_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, method)
#define m11428(__this, p0, method) (void)m10602_gshared((t294 *)__this, (int32_t)p0, method)
#define m11429(__this, p0, p1, method) (void)m10604_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m11430(__this, p0, method) (void)m10606_gshared((t294 *)__this, (t29*)p0, method)
#define m1384(__this, p0, method) (bool)m10608_gshared((t294 *)__this, (t29 *)p0, method)
#define m11431(__this, p0, method) (int32_t)m10610_gshared((t294 *)__this, (t2180 *)p0, method)
#define m11432(__this, p0, method) (void)m10611_gshared((t294 *)__this, (int32_t)p0, method)
#define m11433(__this, method) (void)m10613_gshared((t294 *)__this, method)
#define m11434(__this, method) (void)m10615_gshared((t294 *)__this, method)
#define m11435(__this, p0, method) (void)m10617_gshared((t294 *)__this, (t2181 *)p0, method)
#define m11436(__this, method) (t2265*)m10619_gshared((t294 *)__this, method)
#define m11437(__this, method) (void)m10621_gshared((t294 *)__this, method)
#define m11438(__this, method) (int32_t)m10623_gshared((t294 *)__this, method)
#define m11439(__this, p0, method) (void)m10625_gshared((t294 *)__this, (int32_t)p0, method)
#define m1308(__this, method) (int32_t)m10626_gshared((t294 *)__this, method)
#define m1306(__this, p0, method) (t109 *)m10627_gshared((t294 *)__this, (int32_t)p0, method)
#define m11440(__this, p0, p1, method) (void)m10629_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
