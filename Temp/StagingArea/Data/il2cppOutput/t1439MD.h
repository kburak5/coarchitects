﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1439;
struct t29;
struct t1050;
struct t967;
struct t1441;
struct t1442;
struct t1443;

 void m7815 (t1439 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7816 (t1439 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1050 * m7817 (t1439 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7818 (t1439 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7819 (t1439 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7820 (t1439 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7821 (t1439 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7822 (t1439 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7823 (t1439 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7824 (t1439 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7825 (t1439 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7826 (t1439 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7827 (t1439 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7828 (t1439 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7829 (t1439 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1441 * m7830 (t1439 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7831 (t1439 * __this, t1441 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
