﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2001;
struct t29;

 void m10703_gshared (t2001 * __this, MethodInfo* method);
#define m10703(__this, method) (void)m10703_gshared((t2001 *)__this, method)
 void m10704_gshared (t29 * __this, MethodInfo* method);
#define m10704(__this, method) (void)m10704_gshared((t29 *)__this, method)
 int32_t m10705_gshared (t2001 * __this, t29 * p0, MethodInfo* method);
#define m10705(__this, p0, method) (int32_t)m10705_gshared((t2001 *)__this, (t29 *)p0, method)
 bool m10706_gshared (t2001 * __this, t29 * p0, t29 * p1, MethodInfo* method);
#define m10706(__this, p0, p1, method) (bool)m10706_gshared((t2001 *)__this, (t29 *)p0, (t29 *)p1, method)
 t2001 * m10707_gshared (t29 * __this, MethodInfo* method);
#define m10707(__this, method) (t2001 *)m10707_gshared((t29 *)__this, method)
