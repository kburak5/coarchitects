﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3384;
struct t29;
struct t20;
#include "t630.h"

 void m18792 (t3384 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18793 (t3384 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18794 (t3384 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18795 (t3384 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18796 (t3384 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
