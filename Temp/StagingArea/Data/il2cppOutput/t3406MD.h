﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3406;
struct t29;
struct t20;
#include "t1398.h"

 void m18900 (t3406 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18901 (t3406 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18902 (t3406 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18903 (t3406 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18904 (t3406 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
