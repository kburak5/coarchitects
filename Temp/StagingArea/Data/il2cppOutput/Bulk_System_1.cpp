﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include "t849.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t849_TI;
#include "t849MD.h"


#include "t20.h"

// Metadata Definition System.Text.RegularExpressions.Category
extern Il2CppType t626_0_0_1542;
FieldInfo t849_f1_FieldInfo = 
{
	"value__", &t626_0_0_1542, &t849_TI, offsetof(t849, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f2_FieldInfo = 
{
	"None", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f3_FieldInfo = 
{
	"Any", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f4_FieldInfo = 
{
	"AnySingleline", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f5_FieldInfo = 
{
	"Word", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f6_FieldInfo = 
{
	"Digit", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f7_FieldInfo = 
{
	"WhiteSpace", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f8_FieldInfo = 
{
	"EcmaAny", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f9_FieldInfo = 
{
	"EcmaAnySingleline", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f10_FieldInfo = 
{
	"EcmaWord", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f11_FieldInfo = 
{
	"EcmaDigit", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f12_FieldInfo = 
{
	"EcmaWhiteSpace", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f13_FieldInfo = 
{
	"UnicodeL", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f14_FieldInfo = 
{
	"UnicodeM", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f15_FieldInfo = 
{
	"UnicodeN", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f16_FieldInfo = 
{
	"UnicodeZ", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f17_FieldInfo = 
{
	"UnicodeP", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f18_FieldInfo = 
{
	"UnicodeS", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f19_FieldInfo = 
{
	"UnicodeC", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f20_FieldInfo = 
{
	"UnicodeLu", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f21_FieldInfo = 
{
	"UnicodeLl", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f22_FieldInfo = 
{
	"UnicodeLt", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f23_FieldInfo = 
{
	"UnicodeLm", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f24_FieldInfo = 
{
	"UnicodeLo", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f25_FieldInfo = 
{
	"UnicodeMn", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f26_FieldInfo = 
{
	"UnicodeMe", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f27_FieldInfo = 
{
	"UnicodeMc", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f28_FieldInfo = 
{
	"UnicodeNd", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f29_FieldInfo = 
{
	"UnicodeNl", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f30_FieldInfo = 
{
	"UnicodeNo", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f31_FieldInfo = 
{
	"UnicodeZs", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f32_FieldInfo = 
{
	"UnicodeZl", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f33_FieldInfo = 
{
	"UnicodeZp", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f34_FieldInfo = 
{
	"UnicodePd", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f35_FieldInfo = 
{
	"UnicodePs", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f36_FieldInfo = 
{
	"UnicodePi", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f37_FieldInfo = 
{
	"UnicodePe", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f38_FieldInfo = 
{
	"UnicodePf", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f39_FieldInfo = 
{
	"UnicodePc", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f40_FieldInfo = 
{
	"UnicodePo", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f41_FieldInfo = 
{
	"UnicodeSm", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f42_FieldInfo = 
{
	"UnicodeSc", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f43_FieldInfo = 
{
	"UnicodeSk", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f44_FieldInfo = 
{
	"UnicodeSo", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f45_FieldInfo = 
{
	"UnicodeCc", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f46_FieldInfo = 
{
	"UnicodeCf", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f47_FieldInfo = 
{
	"UnicodeCo", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f48_FieldInfo = 
{
	"UnicodeCs", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f49_FieldInfo = 
{
	"UnicodeCn", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f50_FieldInfo = 
{
	"UnicodeBasicLatin", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f51_FieldInfo = 
{
	"UnicodeLatin1Supplement", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f52_FieldInfo = 
{
	"UnicodeLatinExtendedA", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f53_FieldInfo = 
{
	"UnicodeLatinExtendedB", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f54_FieldInfo = 
{
	"UnicodeIPAExtensions", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f55_FieldInfo = 
{
	"UnicodeSpacingModifierLetters", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f56_FieldInfo = 
{
	"UnicodeCombiningDiacriticalMarks", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f57_FieldInfo = 
{
	"UnicodeGreek", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f58_FieldInfo = 
{
	"UnicodeCyrillic", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f59_FieldInfo = 
{
	"UnicodeArmenian", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f60_FieldInfo = 
{
	"UnicodeHebrew", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f61_FieldInfo = 
{
	"UnicodeArabic", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f62_FieldInfo = 
{
	"UnicodeSyriac", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f63_FieldInfo = 
{
	"UnicodeThaana", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f64_FieldInfo = 
{
	"UnicodeDevanagari", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f65_FieldInfo = 
{
	"UnicodeBengali", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f66_FieldInfo = 
{
	"UnicodeGurmukhi", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f67_FieldInfo = 
{
	"UnicodeGujarati", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f68_FieldInfo = 
{
	"UnicodeOriya", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f69_FieldInfo = 
{
	"UnicodeTamil", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f70_FieldInfo = 
{
	"UnicodeTelugu", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f71_FieldInfo = 
{
	"UnicodeKannada", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f72_FieldInfo = 
{
	"UnicodeMalayalam", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f73_FieldInfo = 
{
	"UnicodeSinhala", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f74_FieldInfo = 
{
	"UnicodeThai", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f75_FieldInfo = 
{
	"UnicodeLao", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f76_FieldInfo = 
{
	"UnicodeTibetan", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f77_FieldInfo = 
{
	"UnicodeMyanmar", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f78_FieldInfo = 
{
	"UnicodeGeorgian", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f79_FieldInfo = 
{
	"UnicodeHangulJamo", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f80_FieldInfo = 
{
	"UnicodeEthiopic", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f81_FieldInfo = 
{
	"UnicodeCherokee", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f82_FieldInfo = 
{
	"UnicodeUnifiedCanadianAboriginalSyllabics", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f83_FieldInfo = 
{
	"UnicodeOgham", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f84_FieldInfo = 
{
	"UnicodeRunic", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f85_FieldInfo = 
{
	"UnicodeKhmer", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f86_FieldInfo = 
{
	"UnicodeMongolian", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f87_FieldInfo = 
{
	"UnicodeLatinExtendedAdditional", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f88_FieldInfo = 
{
	"UnicodeGreekExtended", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f89_FieldInfo = 
{
	"UnicodeGeneralPunctuation", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f90_FieldInfo = 
{
	"UnicodeSuperscriptsandSubscripts", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f91_FieldInfo = 
{
	"UnicodeCurrencySymbols", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f92_FieldInfo = 
{
	"UnicodeCombiningMarksforSymbols", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f93_FieldInfo = 
{
	"UnicodeLetterlikeSymbols", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f94_FieldInfo = 
{
	"UnicodeNumberForms", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f95_FieldInfo = 
{
	"UnicodeArrows", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f96_FieldInfo = 
{
	"UnicodeMathematicalOperators", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f97_FieldInfo = 
{
	"UnicodeMiscellaneousTechnical", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f98_FieldInfo = 
{
	"UnicodeControlPictures", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f99_FieldInfo = 
{
	"UnicodeOpticalCharacterRecognition", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f100_FieldInfo = 
{
	"UnicodeEnclosedAlphanumerics", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f101_FieldInfo = 
{
	"UnicodeBoxDrawing", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f102_FieldInfo = 
{
	"UnicodeBlockElements", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f103_FieldInfo = 
{
	"UnicodeGeometricShapes", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f104_FieldInfo = 
{
	"UnicodeMiscellaneousSymbols", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f105_FieldInfo = 
{
	"UnicodeDingbats", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f106_FieldInfo = 
{
	"UnicodeBraillePatterns", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f107_FieldInfo = 
{
	"UnicodeCJKRadicalsSupplement", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f108_FieldInfo = 
{
	"UnicodeKangxiRadicals", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f109_FieldInfo = 
{
	"UnicodeIdeographicDescriptionCharacters", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f110_FieldInfo = 
{
	"UnicodeCJKSymbolsandPunctuation", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f111_FieldInfo = 
{
	"UnicodeHiragana", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f112_FieldInfo = 
{
	"UnicodeKatakana", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f113_FieldInfo = 
{
	"UnicodeBopomofo", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f114_FieldInfo = 
{
	"UnicodeHangulCompatibilityJamo", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f115_FieldInfo = 
{
	"UnicodeKanbun", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f116_FieldInfo = 
{
	"UnicodeBopomofoExtended", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f117_FieldInfo = 
{
	"UnicodeEnclosedCJKLettersandMonths", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f118_FieldInfo = 
{
	"UnicodeCJKCompatibility", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f119_FieldInfo = 
{
	"UnicodeCJKUnifiedIdeographsExtensionA", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f120_FieldInfo = 
{
	"UnicodeCJKUnifiedIdeographs", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f121_FieldInfo = 
{
	"UnicodeYiSyllables", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f122_FieldInfo = 
{
	"UnicodeYiRadicals", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f123_FieldInfo = 
{
	"UnicodeHangulSyllables", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f124_FieldInfo = 
{
	"UnicodeHighSurrogates", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f125_FieldInfo = 
{
	"UnicodeHighPrivateUseSurrogates", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f126_FieldInfo = 
{
	"UnicodeLowSurrogates", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f127_FieldInfo = 
{
	"UnicodePrivateUse", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f128_FieldInfo = 
{
	"UnicodeCJKCompatibilityIdeographs", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f129_FieldInfo = 
{
	"UnicodeAlphabeticPresentationForms", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f130_FieldInfo = 
{
	"UnicodeArabicPresentationFormsA", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f131_FieldInfo = 
{
	"UnicodeCombiningHalfMarks", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f132_FieldInfo = 
{
	"UnicodeCJKCompatibilityForms", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f133_FieldInfo = 
{
	"UnicodeSmallFormVariants", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f134_FieldInfo = 
{
	"UnicodeArabicPresentationFormsB", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f135_FieldInfo = 
{
	"UnicodeSpecials", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f136_FieldInfo = 
{
	"UnicodeHalfwidthandFullwidthForms", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f137_FieldInfo = 
{
	"UnicodeOldItalic", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f138_FieldInfo = 
{
	"UnicodeGothic", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f139_FieldInfo = 
{
	"UnicodeDeseret", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f140_FieldInfo = 
{
	"UnicodeByzantineMusicalSymbols", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f141_FieldInfo = 
{
	"UnicodeMusicalSymbols", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f142_FieldInfo = 
{
	"UnicodeMathematicalAlphanumericSymbols", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f143_FieldInfo = 
{
	"UnicodeCJKUnifiedIdeographsExtensionB", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f144_FieldInfo = 
{
	"UnicodeCJKCompatibilityIdeographsSupplement", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f145_FieldInfo = 
{
	"UnicodeTags", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t849_0_0_32854;
FieldInfo t849_f146_FieldInfo = 
{
	"LastValue", &t849_0_0_32854, &t849_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t849_FIs[] =
{
	&t849_f1_FieldInfo,
	&t849_f2_FieldInfo,
	&t849_f3_FieldInfo,
	&t849_f4_FieldInfo,
	&t849_f5_FieldInfo,
	&t849_f6_FieldInfo,
	&t849_f7_FieldInfo,
	&t849_f8_FieldInfo,
	&t849_f9_FieldInfo,
	&t849_f10_FieldInfo,
	&t849_f11_FieldInfo,
	&t849_f12_FieldInfo,
	&t849_f13_FieldInfo,
	&t849_f14_FieldInfo,
	&t849_f15_FieldInfo,
	&t849_f16_FieldInfo,
	&t849_f17_FieldInfo,
	&t849_f18_FieldInfo,
	&t849_f19_FieldInfo,
	&t849_f20_FieldInfo,
	&t849_f21_FieldInfo,
	&t849_f22_FieldInfo,
	&t849_f23_FieldInfo,
	&t849_f24_FieldInfo,
	&t849_f25_FieldInfo,
	&t849_f26_FieldInfo,
	&t849_f27_FieldInfo,
	&t849_f28_FieldInfo,
	&t849_f29_FieldInfo,
	&t849_f30_FieldInfo,
	&t849_f31_FieldInfo,
	&t849_f32_FieldInfo,
	&t849_f33_FieldInfo,
	&t849_f34_FieldInfo,
	&t849_f35_FieldInfo,
	&t849_f36_FieldInfo,
	&t849_f37_FieldInfo,
	&t849_f38_FieldInfo,
	&t849_f39_FieldInfo,
	&t849_f40_FieldInfo,
	&t849_f41_FieldInfo,
	&t849_f42_FieldInfo,
	&t849_f43_FieldInfo,
	&t849_f44_FieldInfo,
	&t849_f45_FieldInfo,
	&t849_f46_FieldInfo,
	&t849_f47_FieldInfo,
	&t849_f48_FieldInfo,
	&t849_f49_FieldInfo,
	&t849_f50_FieldInfo,
	&t849_f51_FieldInfo,
	&t849_f52_FieldInfo,
	&t849_f53_FieldInfo,
	&t849_f54_FieldInfo,
	&t849_f55_FieldInfo,
	&t849_f56_FieldInfo,
	&t849_f57_FieldInfo,
	&t849_f58_FieldInfo,
	&t849_f59_FieldInfo,
	&t849_f60_FieldInfo,
	&t849_f61_FieldInfo,
	&t849_f62_FieldInfo,
	&t849_f63_FieldInfo,
	&t849_f64_FieldInfo,
	&t849_f65_FieldInfo,
	&t849_f66_FieldInfo,
	&t849_f67_FieldInfo,
	&t849_f68_FieldInfo,
	&t849_f69_FieldInfo,
	&t849_f70_FieldInfo,
	&t849_f71_FieldInfo,
	&t849_f72_FieldInfo,
	&t849_f73_FieldInfo,
	&t849_f74_FieldInfo,
	&t849_f75_FieldInfo,
	&t849_f76_FieldInfo,
	&t849_f77_FieldInfo,
	&t849_f78_FieldInfo,
	&t849_f79_FieldInfo,
	&t849_f80_FieldInfo,
	&t849_f81_FieldInfo,
	&t849_f82_FieldInfo,
	&t849_f83_FieldInfo,
	&t849_f84_FieldInfo,
	&t849_f85_FieldInfo,
	&t849_f86_FieldInfo,
	&t849_f87_FieldInfo,
	&t849_f88_FieldInfo,
	&t849_f89_FieldInfo,
	&t849_f90_FieldInfo,
	&t849_f91_FieldInfo,
	&t849_f92_FieldInfo,
	&t849_f93_FieldInfo,
	&t849_f94_FieldInfo,
	&t849_f95_FieldInfo,
	&t849_f96_FieldInfo,
	&t849_f97_FieldInfo,
	&t849_f98_FieldInfo,
	&t849_f99_FieldInfo,
	&t849_f100_FieldInfo,
	&t849_f101_FieldInfo,
	&t849_f102_FieldInfo,
	&t849_f103_FieldInfo,
	&t849_f104_FieldInfo,
	&t849_f105_FieldInfo,
	&t849_f106_FieldInfo,
	&t849_f107_FieldInfo,
	&t849_f108_FieldInfo,
	&t849_f109_FieldInfo,
	&t849_f110_FieldInfo,
	&t849_f111_FieldInfo,
	&t849_f112_FieldInfo,
	&t849_f113_FieldInfo,
	&t849_f114_FieldInfo,
	&t849_f115_FieldInfo,
	&t849_f116_FieldInfo,
	&t849_f117_FieldInfo,
	&t849_f118_FieldInfo,
	&t849_f119_FieldInfo,
	&t849_f120_FieldInfo,
	&t849_f121_FieldInfo,
	&t849_f122_FieldInfo,
	&t849_f123_FieldInfo,
	&t849_f124_FieldInfo,
	&t849_f125_FieldInfo,
	&t849_f126_FieldInfo,
	&t849_f127_FieldInfo,
	&t849_f128_FieldInfo,
	&t849_f129_FieldInfo,
	&t849_f130_FieldInfo,
	&t849_f131_FieldInfo,
	&t849_f132_FieldInfo,
	&t849_f133_FieldInfo,
	&t849_f134_FieldInfo,
	&t849_f135_FieldInfo,
	&t849_f136_FieldInfo,
	&t849_f137_FieldInfo,
	&t849_f138_FieldInfo,
	&t849_f139_FieldInfo,
	&t849_f140_FieldInfo,
	&t849_f141_FieldInfo,
	&t849_f142_FieldInfo,
	&t849_f143_FieldInfo,
	&t849_f144_FieldInfo,
	&t849_f145_FieldInfo,
	&t849_f146_FieldInfo,
	NULL
};
static const uint16_t t849_f2_DefaultValueData = 0;
extern Il2CppType t626_0_0_0;
static Il2CppFieldDefaultValueEntry t849_f2_DefaultValue = 
{
	&t849_f2_FieldInfo, { (char*)&t849_f2_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t849_f3_DefaultValue = 
{
	&t849_f3_FieldInfo, { (char*)&t849_f3_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t849_f4_DefaultValue = 
{
	&t849_f4_FieldInfo, { (char*)&t849_f4_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry t849_f5_DefaultValue = 
{
	&t849_f5_FieldInfo, { (char*)&t849_f5_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f6_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t849_f6_DefaultValue = 
{
	&t849_f6_FieldInfo, { (char*)&t849_f6_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f7_DefaultValueData = 5;
static Il2CppFieldDefaultValueEntry t849_f7_DefaultValue = 
{
	&t849_f7_FieldInfo, { (char*)&t849_f7_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f8_DefaultValueData = 6;
static Il2CppFieldDefaultValueEntry t849_f8_DefaultValue = 
{
	&t849_f8_FieldInfo, { (char*)&t849_f8_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f9_DefaultValueData = 7;
static Il2CppFieldDefaultValueEntry t849_f9_DefaultValue = 
{
	&t849_f9_FieldInfo, { (char*)&t849_f9_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f10_DefaultValueData = 8;
static Il2CppFieldDefaultValueEntry t849_f10_DefaultValue = 
{
	&t849_f10_FieldInfo, { (char*)&t849_f10_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f11_DefaultValueData = 9;
static Il2CppFieldDefaultValueEntry t849_f11_DefaultValue = 
{
	&t849_f11_FieldInfo, { (char*)&t849_f11_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f12_DefaultValueData = 10;
static Il2CppFieldDefaultValueEntry t849_f12_DefaultValue = 
{
	&t849_f12_FieldInfo, { (char*)&t849_f12_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f13_DefaultValueData = 11;
static Il2CppFieldDefaultValueEntry t849_f13_DefaultValue = 
{
	&t849_f13_FieldInfo, { (char*)&t849_f13_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f14_DefaultValueData = 12;
static Il2CppFieldDefaultValueEntry t849_f14_DefaultValue = 
{
	&t849_f14_FieldInfo, { (char*)&t849_f14_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f15_DefaultValueData = 13;
static Il2CppFieldDefaultValueEntry t849_f15_DefaultValue = 
{
	&t849_f15_FieldInfo, { (char*)&t849_f15_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f16_DefaultValueData = 14;
static Il2CppFieldDefaultValueEntry t849_f16_DefaultValue = 
{
	&t849_f16_FieldInfo, { (char*)&t849_f16_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f17_DefaultValueData = 15;
static Il2CppFieldDefaultValueEntry t849_f17_DefaultValue = 
{
	&t849_f17_FieldInfo, { (char*)&t849_f17_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f18_DefaultValueData = 16;
static Il2CppFieldDefaultValueEntry t849_f18_DefaultValue = 
{
	&t849_f18_FieldInfo, { (char*)&t849_f18_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f19_DefaultValueData = 17;
static Il2CppFieldDefaultValueEntry t849_f19_DefaultValue = 
{
	&t849_f19_FieldInfo, { (char*)&t849_f19_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f20_DefaultValueData = 18;
static Il2CppFieldDefaultValueEntry t849_f20_DefaultValue = 
{
	&t849_f20_FieldInfo, { (char*)&t849_f20_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f21_DefaultValueData = 19;
static Il2CppFieldDefaultValueEntry t849_f21_DefaultValue = 
{
	&t849_f21_FieldInfo, { (char*)&t849_f21_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f22_DefaultValueData = 20;
static Il2CppFieldDefaultValueEntry t849_f22_DefaultValue = 
{
	&t849_f22_FieldInfo, { (char*)&t849_f22_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f23_DefaultValueData = 21;
static Il2CppFieldDefaultValueEntry t849_f23_DefaultValue = 
{
	&t849_f23_FieldInfo, { (char*)&t849_f23_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f24_DefaultValueData = 22;
static Il2CppFieldDefaultValueEntry t849_f24_DefaultValue = 
{
	&t849_f24_FieldInfo, { (char*)&t849_f24_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f25_DefaultValueData = 23;
static Il2CppFieldDefaultValueEntry t849_f25_DefaultValue = 
{
	&t849_f25_FieldInfo, { (char*)&t849_f25_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f26_DefaultValueData = 24;
static Il2CppFieldDefaultValueEntry t849_f26_DefaultValue = 
{
	&t849_f26_FieldInfo, { (char*)&t849_f26_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f27_DefaultValueData = 25;
static Il2CppFieldDefaultValueEntry t849_f27_DefaultValue = 
{
	&t849_f27_FieldInfo, { (char*)&t849_f27_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f28_DefaultValueData = 26;
static Il2CppFieldDefaultValueEntry t849_f28_DefaultValue = 
{
	&t849_f28_FieldInfo, { (char*)&t849_f28_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f29_DefaultValueData = 27;
static Il2CppFieldDefaultValueEntry t849_f29_DefaultValue = 
{
	&t849_f29_FieldInfo, { (char*)&t849_f29_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f30_DefaultValueData = 28;
static Il2CppFieldDefaultValueEntry t849_f30_DefaultValue = 
{
	&t849_f30_FieldInfo, { (char*)&t849_f30_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f31_DefaultValueData = 29;
static Il2CppFieldDefaultValueEntry t849_f31_DefaultValue = 
{
	&t849_f31_FieldInfo, { (char*)&t849_f31_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f32_DefaultValueData = 30;
static Il2CppFieldDefaultValueEntry t849_f32_DefaultValue = 
{
	&t849_f32_FieldInfo, { (char*)&t849_f32_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f33_DefaultValueData = 31;
static Il2CppFieldDefaultValueEntry t849_f33_DefaultValue = 
{
	&t849_f33_FieldInfo, { (char*)&t849_f33_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f34_DefaultValueData = 32;
static Il2CppFieldDefaultValueEntry t849_f34_DefaultValue = 
{
	&t849_f34_FieldInfo, { (char*)&t849_f34_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f35_DefaultValueData = 33;
static Il2CppFieldDefaultValueEntry t849_f35_DefaultValue = 
{
	&t849_f35_FieldInfo, { (char*)&t849_f35_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f36_DefaultValueData = 34;
static Il2CppFieldDefaultValueEntry t849_f36_DefaultValue = 
{
	&t849_f36_FieldInfo, { (char*)&t849_f36_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f37_DefaultValueData = 35;
static Il2CppFieldDefaultValueEntry t849_f37_DefaultValue = 
{
	&t849_f37_FieldInfo, { (char*)&t849_f37_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f38_DefaultValueData = 36;
static Il2CppFieldDefaultValueEntry t849_f38_DefaultValue = 
{
	&t849_f38_FieldInfo, { (char*)&t849_f38_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f39_DefaultValueData = 37;
static Il2CppFieldDefaultValueEntry t849_f39_DefaultValue = 
{
	&t849_f39_FieldInfo, { (char*)&t849_f39_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f40_DefaultValueData = 38;
static Il2CppFieldDefaultValueEntry t849_f40_DefaultValue = 
{
	&t849_f40_FieldInfo, { (char*)&t849_f40_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f41_DefaultValueData = 39;
static Il2CppFieldDefaultValueEntry t849_f41_DefaultValue = 
{
	&t849_f41_FieldInfo, { (char*)&t849_f41_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f42_DefaultValueData = 40;
static Il2CppFieldDefaultValueEntry t849_f42_DefaultValue = 
{
	&t849_f42_FieldInfo, { (char*)&t849_f42_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f43_DefaultValueData = 41;
static Il2CppFieldDefaultValueEntry t849_f43_DefaultValue = 
{
	&t849_f43_FieldInfo, { (char*)&t849_f43_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f44_DefaultValueData = 42;
static Il2CppFieldDefaultValueEntry t849_f44_DefaultValue = 
{
	&t849_f44_FieldInfo, { (char*)&t849_f44_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f45_DefaultValueData = 43;
static Il2CppFieldDefaultValueEntry t849_f45_DefaultValue = 
{
	&t849_f45_FieldInfo, { (char*)&t849_f45_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f46_DefaultValueData = 44;
static Il2CppFieldDefaultValueEntry t849_f46_DefaultValue = 
{
	&t849_f46_FieldInfo, { (char*)&t849_f46_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f47_DefaultValueData = 45;
static Il2CppFieldDefaultValueEntry t849_f47_DefaultValue = 
{
	&t849_f47_FieldInfo, { (char*)&t849_f47_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f48_DefaultValueData = 46;
static Il2CppFieldDefaultValueEntry t849_f48_DefaultValue = 
{
	&t849_f48_FieldInfo, { (char*)&t849_f48_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f49_DefaultValueData = 47;
static Il2CppFieldDefaultValueEntry t849_f49_DefaultValue = 
{
	&t849_f49_FieldInfo, { (char*)&t849_f49_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f50_DefaultValueData = 48;
static Il2CppFieldDefaultValueEntry t849_f50_DefaultValue = 
{
	&t849_f50_FieldInfo, { (char*)&t849_f50_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f51_DefaultValueData = 49;
static Il2CppFieldDefaultValueEntry t849_f51_DefaultValue = 
{
	&t849_f51_FieldInfo, { (char*)&t849_f51_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f52_DefaultValueData = 50;
static Il2CppFieldDefaultValueEntry t849_f52_DefaultValue = 
{
	&t849_f52_FieldInfo, { (char*)&t849_f52_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f53_DefaultValueData = 51;
static Il2CppFieldDefaultValueEntry t849_f53_DefaultValue = 
{
	&t849_f53_FieldInfo, { (char*)&t849_f53_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f54_DefaultValueData = 52;
static Il2CppFieldDefaultValueEntry t849_f54_DefaultValue = 
{
	&t849_f54_FieldInfo, { (char*)&t849_f54_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f55_DefaultValueData = 53;
static Il2CppFieldDefaultValueEntry t849_f55_DefaultValue = 
{
	&t849_f55_FieldInfo, { (char*)&t849_f55_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f56_DefaultValueData = 54;
static Il2CppFieldDefaultValueEntry t849_f56_DefaultValue = 
{
	&t849_f56_FieldInfo, { (char*)&t849_f56_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f57_DefaultValueData = 55;
static Il2CppFieldDefaultValueEntry t849_f57_DefaultValue = 
{
	&t849_f57_FieldInfo, { (char*)&t849_f57_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f58_DefaultValueData = 56;
static Il2CppFieldDefaultValueEntry t849_f58_DefaultValue = 
{
	&t849_f58_FieldInfo, { (char*)&t849_f58_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f59_DefaultValueData = 57;
static Il2CppFieldDefaultValueEntry t849_f59_DefaultValue = 
{
	&t849_f59_FieldInfo, { (char*)&t849_f59_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f60_DefaultValueData = 58;
static Il2CppFieldDefaultValueEntry t849_f60_DefaultValue = 
{
	&t849_f60_FieldInfo, { (char*)&t849_f60_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f61_DefaultValueData = 59;
static Il2CppFieldDefaultValueEntry t849_f61_DefaultValue = 
{
	&t849_f61_FieldInfo, { (char*)&t849_f61_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f62_DefaultValueData = 60;
static Il2CppFieldDefaultValueEntry t849_f62_DefaultValue = 
{
	&t849_f62_FieldInfo, { (char*)&t849_f62_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f63_DefaultValueData = 61;
static Il2CppFieldDefaultValueEntry t849_f63_DefaultValue = 
{
	&t849_f63_FieldInfo, { (char*)&t849_f63_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f64_DefaultValueData = 62;
static Il2CppFieldDefaultValueEntry t849_f64_DefaultValue = 
{
	&t849_f64_FieldInfo, { (char*)&t849_f64_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f65_DefaultValueData = 63;
static Il2CppFieldDefaultValueEntry t849_f65_DefaultValue = 
{
	&t849_f65_FieldInfo, { (char*)&t849_f65_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f66_DefaultValueData = 64;
static Il2CppFieldDefaultValueEntry t849_f66_DefaultValue = 
{
	&t849_f66_FieldInfo, { (char*)&t849_f66_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f67_DefaultValueData = 65;
static Il2CppFieldDefaultValueEntry t849_f67_DefaultValue = 
{
	&t849_f67_FieldInfo, { (char*)&t849_f67_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f68_DefaultValueData = 66;
static Il2CppFieldDefaultValueEntry t849_f68_DefaultValue = 
{
	&t849_f68_FieldInfo, { (char*)&t849_f68_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f69_DefaultValueData = 67;
static Il2CppFieldDefaultValueEntry t849_f69_DefaultValue = 
{
	&t849_f69_FieldInfo, { (char*)&t849_f69_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f70_DefaultValueData = 68;
static Il2CppFieldDefaultValueEntry t849_f70_DefaultValue = 
{
	&t849_f70_FieldInfo, { (char*)&t849_f70_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f71_DefaultValueData = 69;
static Il2CppFieldDefaultValueEntry t849_f71_DefaultValue = 
{
	&t849_f71_FieldInfo, { (char*)&t849_f71_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f72_DefaultValueData = 70;
static Il2CppFieldDefaultValueEntry t849_f72_DefaultValue = 
{
	&t849_f72_FieldInfo, { (char*)&t849_f72_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f73_DefaultValueData = 71;
static Il2CppFieldDefaultValueEntry t849_f73_DefaultValue = 
{
	&t849_f73_FieldInfo, { (char*)&t849_f73_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f74_DefaultValueData = 72;
static Il2CppFieldDefaultValueEntry t849_f74_DefaultValue = 
{
	&t849_f74_FieldInfo, { (char*)&t849_f74_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f75_DefaultValueData = 73;
static Il2CppFieldDefaultValueEntry t849_f75_DefaultValue = 
{
	&t849_f75_FieldInfo, { (char*)&t849_f75_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f76_DefaultValueData = 74;
static Il2CppFieldDefaultValueEntry t849_f76_DefaultValue = 
{
	&t849_f76_FieldInfo, { (char*)&t849_f76_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f77_DefaultValueData = 75;
static Il2CppFieldDefaultValueEntry t849_f77_DefaultValue = 
{
	&t849_f77_FieldInfo, { (char*)&t849_f77_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f78_DefaultValueData = 76;
static Il2CppFieldDefaultValueEntry t849_f78_DefaultValue = 
{
	&t849_f78_FieldInfo, { (char*)&t849_f78_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f79_DefaultValueData = 77;
static Il2CppFieldDefaultValueEntry t849_f79_DefaultValue = 
{
	&t849_f79_FieldInfo, { (char*)&t849_f79_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f80_DefaultValueData = 78;
static Il2CppFieldDefaultValueEntry t849_f80_DefaultValue = 
{
	&t849_f80_FieldInfo, { (char*)&t849_f80_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f81_DefaultValueData = 79;
static Il2CppFieldDefaultValueEntry t849_f81_DefaultValue = 
{
	&t849_f81_FieldInfo, { (char*)&t849_f81_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f82_DefaultValueData = 80;
static Il2CppFieldDefaultValueEntry t849_f82_DefaultValue = 
{
	&t849_f82_FieldInfo, { (char*)&t849_f82_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f83_DefaultValueData = 81;
static Il2CppFieldDefaultValueEntry t849_f83_DefaultValue = 
{
	&t849_f83_FieldInfo, { (char*)&t849_f83_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f84_DefaultValueData = 82;
static Il2CppFieldDefaultValueEntry t849_f84_DefaultValue = 
{
	&t849_f84_FieldInfo, { (char*)&t849_f84_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f85_DefaultValueData = 83;
static Il2CppFieldDefaultValueEntry t849_f85_DefaultValue = 
{
	&t849_f85_FieldInfo, { (char*)&t849_f85_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f86_DefaultValueData = 84;
static Il2CppFieldDefaultValueEntry t849_f86_DefaultValue = 
{
	&t849_f86_FieldInfo, { (char*)&t849_f86_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f87_DefaultValueData = 85;
static Il2CppFieldDefaultValueEntry t849_f87_DefaultValue = 
{
	&t849_f87_FieldInfo, { (char*)&t849_f87_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f88_DefaultValueData = 86;
static Il2CppFieldDefaultValueEntry t849_f88_DefaultValue = 
{
	&t849_f88_FieldInfo, { (char*)&t849_f88_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f89_DefaultValueData = 87;
static Il2CppFieldDefaultValueEntry t849_f89_DefaultValue = 
{
	&t849_f89_FieldInfo, { (char*)&t849_f89_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f90_DefaultValueData = 88;
static Il2CppFieldDefaultValueEntry t849_f90_DefaultValue = 
{
	&t849_f90_FieldInfo, { (char*)&t849_f90_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f91_DefaultValueData = 89;
static Il2CppFieldDefaultValueEntry t849_f91_DefaultValue = 
{
	&t849_f91_FieldInfo, { (char*)&t849_f91_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f92_DefaultValueData = 90;
static Il2CppFieldDefaultValueEntry t849_f92_DefaultValue = 
{
	&t849_f92_FieldInfo, { (char*)&t849_f92_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f93_DefaultValueData = 91;
static Il2CppFieldDefaultValueEntry t849_f93_DefaultValue = 
{
	&t849_f93_FieldInfo, { (char*)&t849_f93_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f94_DefaultValueData = 92;
static Il2CppFieldDefaultValueEntry t849_f94_DefaultValue = 
{
	&t849_f94_FieldInfo, { (char*)&t849_f94_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f95_DefaultValueData = 93;
static Il2CppFieldDefaultValueEntry t849_f95_DefaultValue = 
{
	&t849_f95_FieldInfo, { (char*)&t849_f95_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f96_DefaultValueData = 94;
static Il2CppFieldDefaultValueEntry t849_f96_DefaultValue = 
{
	&t849_f96_FieldInfo, { (char*)&t849_f96_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f97_DefaultValueData = 95;
static Il2CppFieldDefaultValueEntry t849_f97_DefaultValue = 
{
	&t849_f97_FieldInfo, { (char*)&t849_f97_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f98_DefaultValueData = 96;
static Il2CppFieldDefaultValueEntry t849_f98_DefaultValue = 
{
	&t849_f98_FieldInfo, { (char*)&t849_f98_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f99_DefaultValueData = 97;
static Il2CppFieldDefaultValueEntry t849_f99_DefaultValue = 
{
	&t849_f99_FieldInfo, { (char*)&t849_f99_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f100_DefaultValueData = 98;
static Il2CppFieldDefaultValueEntry t849_f100_DefaultValue = 
{
	&t849_f100_FieldInfo, { (char*)&t849_f100_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f101_DefaultValueData = 99;
static Il2CppFieldDefaultValueEntry t849_f101_DefaultValue = 
{
	&t849_f101_FieldInfo, { (char*)&t849_f101_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f102_DefaultValueData = 100;
static Il2CppFieldDefaultValueEntry t849_f102_DefaultValue = 
{
	&t849_f102_FieldInfo, { (char*)&t849_f102_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f103_DefaultValueData = 101;
static Il2CppFieldDefaultValueEntry t849_f103_DefaultValue = 
{
	&t849_f103_FieldInfo, { (char*)&t849_f103_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f104_DefaultValueData = 102;
static Il2CppFieldDefaultValueEntry t849_f104_DefaultValue = 
{
	&t849_f104_FieldInfo, { (char*)&t849_f104_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f105_DefaultValueData = 103;
static Il2CppFieldDefaultValueEntry t849_f105_DefaultValue = 
{
	&t849_f105_FieldInfo, { (char*)&t849_f105_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f106_DefaultValueData = 104;
static Il2CppFieldDefaultValueEntry t849_f106_DefaultValue = 
{
	&t849_f106_FieldInfo, { (char*)&t849_f106_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f107_DefaultValueData = 105;
static Il2CppFieldDefaultValueEntry t849_f107_DefaultValue = 
{
	&t849_f107_FieldInfo, { (char*)&t849_f107_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f108_DefaultValueData = 106;
static Il2CppFieldDefaultValueEntry t849_f108_DefaultValue = 
{
	&t849_f108_FieldInfo, { (char*)&t849_f108_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f109_DefaultValueData = 107;
static Il2CppFieldDefaultValueEntry t849_f109_DefaultValue = 
{
	&t849_f109_FieldInfo, { (char*)&t849_f109_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f110_DefaultValueData = 108;
static Il2CppFieldDefaultValueEntry t849_f110_DefaultValue = 
{
	&t849_f110_FieldInfo, { (char*)&t849_f110_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f111_DefaultValueData = 109;
static Il2CppFieldDefaultValueEntry t849_f111_DefaultValue = 
{
	&t849_f111_FieldInfo, { (char*)&t849_f111_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f112_DefaultValueData = 110;
static Il2CppFieldDefaultValueEntry t849_f112_DefaultValue = 
{
	&t849_f112_FieldInfo, { (char*)&t849_f112_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f113_DefaultValueData = 111;
static Il2CppFieldDefaultValueEntry t849_f113_DefaultValue = 
{
	&t849_f113_FieldInfo, { (char*)&t849_f113_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f114_DefaultValueData = 112;
static Il2CppFieldDefaultValueEntry t849_f114_DefaultValue = 
{
	&t849_f114_FieldInfo, { (char*)&t849_f114_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f115_DefaultValueData = 113;
static Il2CppFieldDefaultValueEntry t849_f115_DefaultValue = 
{
	&t849_f115_FieldInfo, { (char*)&t849_f115_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f116_DefaultValueData = 114;
static Il2CppFieldDefaultValueEntry t849_f116_DefaultValue = 
{
	&t849_f116_FieldInfo, { (char*)&t849_f116_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f117_DefaultValueData = 115;
static Il2CppFieldDefaultValueEntry t849_f117_DefaultValue = 
{
	&t849_f117_FieldInfo, { (char*)&t849_f117_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f118_DefaultValueData = 116;
static Il2CppFieldDefaultValueEntry t849_f118_DefaultValue = 
{
	&t849_f118_FieldInfo, { (char*)&t849_f118_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f119_DefaultValueData = 117;
static Il2CppFieldDefaultValueEntry t849_f119_DefaultValue = 
{
	&t849_f119_FieldInfo, { (char*)&t849_f119_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f120_DefaultValueData = 118;
static Il2CppFieldDefaultValueEntry t849_f120_DefaultValue = 
{
	&t849_f120_FieldInfo, { (char*)&t849_f120_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f121_DefaultValueData = 119;
static Il2CppFieldDefaultValueEntry t849_f121_DefaultValue = 
{
	&t849_f121_FieldInfo, { (char*)&t849_f121_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f122_DefaultValueData = 120;
static Il2CppFieldDefaultValueEntry t849_f122_DefaultValue = 
{
	&t849_f122_FieldInfo, { (char*)&t849_f122_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f123_DefaultValueData = 121;
static Il2CppFieldDefaultValueEntry t849_f123_DefaultValue = 
{
	&t849_f123_FieldInfo, { (char*)&t849_f123_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f124_DefaultValueData = 122;
static Il2CppFieldDefaultValueEntry t849_f124_DefaultValue = 
{
	&t849_f124_FieldInfo, { (char*)&t849_f124_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f125_DefaultValueData = 123;
static Il2CppFieldDefaultValueEntry t849_f125_DefaultValue = 
{
	&t849_f125_FieldInfo, { (char*)&t849_f125_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f126_DefaultValueData = 124;
static Il2CppFieldDefaultValueEntry t849_f126_DefaultValue = 
{
	&t849_f126_FieldInfo, { (char*)&t849_f126_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f127_DefaultValueData = 125;
static Il2CppFieldDefaultValueEntry t849_f127_DefaultValue = 
{
	&t849_f127_FieldInfo, { (char*)&t849_f127_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f128_DefaultValueData = 126;
static Il2CppFieldDefaultValueEntry t849_f128_DefaultValue = 
{
	&t849_f128_FieldInfo, { (char*)&t849_f128_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f129_DefaultValueData = 127;
static Il2CppFieldDefaultValueEntry t849_f129_DefaultValue = 
{
	&t849_f129_FieldInfo, { (char*)&t849_f129_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f130_DefaultValueData = 128;
static Il2CppFieldDefaultValueEntry t849_f130_DefaultValue = 
{
	&t849_f130_FieldInfo, { (char*)&t849_f130_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f131_DefaultValueData = 129;
static Il2CppFieldDefaultValueEntry t849_f131_DefaultValue = 
{
	&t849_f131_FieldInfo, { (char*)&t849_f131_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f132_DefaultValueData = 130;
static Il2CppFieldDefaultValueEntry t849_f132_DefaultValue = 
{
	&t849_f132_FieldInfo, { (char*)&t849_f132_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f133_DefaultValueData = 131;
static Il2CppFieldDefaultValueEntry t849_f133_DefaultValue = 
{
	&t849_f133_FieldInfo, { (char*)&t849_f133_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f134_DefaultValueData = 132;
static Il2CppFieldDefaultValueEntry t849_f134_DefaultValue = 
{
	&t849_f134_FieldInfo, { (char*)&t849_f134_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f135_DefaultValueData = 133;
static Il2CppFieldDefaultValueEntry t849_f135_DefaultValue = 
{
	&t849_f135_FieldInfo, { (char*)&t849_f135_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f136_DefaultValueData = 134;
static Il2CppFieldDefaultValueEntry t849_f136_DefaultValue = 
{
	&t849_f136_FieldInfo, { (char*)&t849_f136_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f137_DefaultValueData = 135;
static Il2CppFieldDefaultValueEntry t849_f137_DefaultValue = 
{
	&t849_f137_FieldInfo, { (char*)&t849_f137_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f138_DefaultValueData = 136;
static Il2CppFieldDefaultValueEntry t849_f138_DefaultValue = 
{
	&t849_f138_FieldInfo, { (char*)&t849_f138_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f139_DefaultValueData = 137;
static Il2CppFieldDefaultValueEntry t849_f139_DefaultValue = 
{
	&t849_f139_FieldInfo, { (char*)&t849_f139_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f140_DefaultValueData = 138;
static Il2CppFieldDefaultValueEntry t849_f140_DefaultValue = 
{
	&t849_f140_FieldInfo, { (char*)&t849_f140_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f141_DefaultValueData = 139;
static Il2CppFieldDefaultValueEntry t849_f141_DefaultValue = 
{
	&t849_f141_FieldInfo, { (char*)&t849_f141_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f142_DefaultValueData = 140;
static Il2CppFieldDefaultValueEntry t849_f142_DefaultValue = 
{
	&t849_f142_FieldInfo, { (char*)&t849_f142_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f143_DefaultValueData = 141;
static Il2CppFieldDefaultValueEntry t849_f143_DefaultValue = 
{
	&t849_f143_FieldInfo, { (char*)&t849_f143_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f144_DefaultValueData = 142;
static Il2CppFieldDefaultValueEntry t849_f144_DefaultValue = 
{
	&t849_f144_FieldInfo, { (char*)&t849_f144_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f145_DefaultValueData = 143;
static Il2CppFieldDefaultValueEntry t849_f145_DefaultValue = 
{
	&t849_f145_FieldInfo, { (char*)&t849_f145_DefaultValueData, &t626_0_0_0 }};
static const uint16_t t849_f146_DefaultValueData = 144;
static Il2CppFieldDefaultValueEntry t849_f146_DefaultValue = 
{
	&t849_f146_FieldInfo, { (char*)&t849_f146_DefaultValueData, &t626_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t849_FDVs[] = 
{
	&t849_f2_DefaultValue,
	&t849_f3_DefaultValue,
	&t849_f4_DefaultValue,
	&t849_f5_DefaultValue,
	&t849_f6_DefaultValue,
	&t849_f7_DefaultValue,
	&t849_f8_DefaultValue,
	&t849_f9_DefaultValue,
	&t849_f10_DefaultValue,
	&t849_f11_DefaultValue,
	&t849_f12_DefaultValue,
	&t849_f13_DefaultValue,
	&t849_f14_DefaultValue,
	&t849_f15_DefaultValue,
	&t849_f16_DefaultValue,
	&t849_f17_DefaultValue,
	&t849_f18_DefaultValue,
	&t849_f19_DefaultValue,
	&t849_f20_DefaultValue,
	&t849_f21_DefaultValue,
	&t849_f22_DefaultValue,
	&t849_f23_DefaultValue,
	&t849_f24_DefaultValue,
	&t849_f25_DefaultValue,
	&t849_f26_DefaultValue,
	&t849_f27_DefaultValue,
	&t849_f28_DefaultValue,
	&t849_f29_DefaultValue,
	&t849_f30_DefaultValue,
	&t849_f31_DefaultValue,
	&t849_f32_DefaultValue,
	&t849_f33_DefaultValue,
	&t849_f34_DefaultValue,
	&t849_f35_DefaultValue,
	&t849_f36_DefaultValue,
	&t849_f37_DefaultValue,
	&t849_f38_DefaultValue,
	&t849_f39_DefaultValue,
	&t849_f40_DefaultValue,
	&t849_f41_DefaultValue,
	&t849_f42_DefaultValue,
	&t849_f43_DefaultValue,
	&t849_f44_DefaultValue,
	&t849_f45_DefaultValue,
	&t849_f46_DefaultValue,
	&t849_f47_DefaultValue,
	&t849_f48_DefaultValue,
	&t849_f49_DefaultValue,
	&t849_f50_DefaultValue,
	&t849_f51_DefaultValue,
	&t849_f52_DefaultValue,
	&t849_f53_DefaultValue,
	&t849_f54_DefaultValue,
	&t849_f55_DefaultValue,
	&t849_f56_DefaultValue,
	&t849_f57_DefaultValue,
	&t849_f58_DefaultValue,
	&t849_f59_DefaultValue,
	&t849_f60_DefaultValue,
	&t849_f61_DefaultValue,
	&t849_f62_DefaultValue,
	&t849_f63_DefaultValue,
	&t849_f64_DefaultValue,
	&t849_f65_DefaultValue,
	&t849_f66_DefaultValue,
	&t849_f67_DefaultValue,
	&t849_f68_DefaultValue,
	&t849_f69_DefaultValue,
	&t849_f70_DefaultValue,
	&t849_f71_DefaultValue,
	&t849_f72_DefaultValue,
	&t849_f73_DefaultValue,
	&t849_f74_DefaultValue,
	&t849_f75_DefaultValue,
	&t849_f76_DefaultValue,
	&t849_f77_DefaultValue,
	&t849_f78_DefaultValue,
	&t849_f79_DefaultValue,
	&t849_f80_DefaultValue,
	&t849_f81_DefaultValue,
	&t849_f82_DefaultValue,
	&t849_f83_DefaultValue,
	&t849_f84_DefaultValue,
	&t849_f85_DefaultValue,
	&t849_f86_DefaultValue,
	&t849_f87_DefaultValue,
	&t849_f88_DefaultValue,
	&t849_f89_DefaultValue,
	&t849_f90_DefaultValue,
	&t849_f91_DefaultValue,
	&t849_f92_DefaultValue,
	&t849_f93_DefaultValue,
	&t849_f94_DefaultValue,
	&t849_f95_DefaultValue,
	&t849_f96_DefaultValue,
	&t849_f97_DefaultValue,
	&t849_f98_DefaultValue,
	&t849_f99_DefaultValue,
	&t849_f100_DefaultValue,
	&t849_f101_DefaultValue,
	&t849_f102_DefaultValue,
	&t849_f103_DefaultValue,
	&t849_f104_DefaultValue,
	&t849_f105_DefaultValue,
	&t849_f106_DefaultValue,
	&t849_f107_DefaultValue,
	&t849_f108_DefaultValue,
	&t849_f109_DefaultValue,
	&t849_f110_DefaultValue,
	&t849_f111_DefaultValue,
	&t849_f112_DefaultValue,
	&t849_f113_DefaultValue,
	&t849_f114_DefaultValue,
	&t849_f115_DefaultValue,
	&t849_f116_DefaultValue,
	&t849_f117_DefaultValue,
	&t849_f118_DefaultValue,
	&t849_f119_DefaultValue,
	&t849_f120_DefaultValue,
	&t849_f121_DefaultValue,
	&t849_f122_DefaultValue,
	&t849_f123_DefaultValue,
	&t849_f124_DefaultValue,
	&t849_f125_DefaultValue,
	&t849_f126_DefaultValue,
	&t849_f127_DefaultValue,
	&t849_f128_DefaultValue,
	&t849_f129_DefaultValue,
	&t849_f130_DefaultValue,
	&t849_f131_DefaultValue,
	&t849_f132_DefaultValue,
	&t849_f133_DefaultValue,
	&t849_f134_DefaultValue,
	&t849_f135_DefaultValue,
	&t849_f136_DefaultValue,
	&t849_f137_DefaultValue,
	&t849_f138_DefaultValue,
	&t849_f139_DefaultValue,
	&t849_f140_DefaultValue,
	&t849_f141_DefaultValue,
	&t849_f142_DefaultValue,
	&t849_f143_DefaultValue,
	&t849_f144_DefaultValue,
	&t849_f145_DefaultValue,
	&t849_f146_DefaultValue,
	NULL
};
static MethodInfo* t849_MIs[] =
{
	NULL
};
extern MethodInfo m1248_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1249_MI;
extern MethodInfo m1250_MI;
extern MethodInfo m1251_MI;
extern MethodInfo m1252_MI;
extern MethodInfo m1253_MI;
extern MethodInfo m1254_MI;
extern MethodInfo m1255_MI;
extern MethodInfo m1256_MI;
extern MethodInfo m1257_MI;
extern MethodInfo m1258_MI;
extern MethodInfo m1259_MI;
extern MethodInfo m1260_MI;
extern MethodInfo m1261_MI;
extern MethodInfo m1262_MI;
extern MethodInfo m1263_MI;
extern MethodInfo m1264_MI;
extern MethodInfo m1265_MI;
extern MethodInfo m1266_MI;
extern MethodInfo m1267_MI;
extern MethodInfo m1268_MI;
extern MethodInfo m1269_MI;
static MethodInfo* t849_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
extern TypeInfo t288_TI;
extern TypeInfo t289_TI;
extern TypeInfo t290_TI;
static Il2CppInterfaceOffsetPair t849_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t849_0_0_0;
extern Il2CppType t849_1_0_0;
extern TypeInfo t49_TI;
#include "t626.h"
extern TypeInfo t626_TI;
TypeInfo t849_TI = 
{
	&g_System_dll_Image, NULL, "Category", "System.Text.RegularExpressions", t849_MIs, NULL, t849_FIs, NULL, &t49_TI, NULL, NULL, &t626_TI, NULL, t849_VT, &EmptyCustomAttributesCache, &t626_TI, &t849_0_0_0, &t849_1_0_0, t849_IOs, NULL, NULL, t849_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t849)+ sizeof (Il2CppObject), sizeof (uint16_t), sizeof(uint16_t), 0, 0, -1, 256, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 146, 0, 0, 23, 0, 3};
#include "t850.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t850_TI;
#include "t850MD.h"

#include "t7.h"
#include "t40.h"
#include "t44.h"
#include "t42.h"
#include "t43.h"
#include "t29.h"
#include "t194.h"
#include "t851.h"
extern TypeInfo t42_TI;
extern TypeInfo t7_TI;
extern TypeInfo t305_TI;
extern TypeInfo t194_TI;
#include "t7MD.h"
#include "t42MD.h"
#include "t49MD.h"
#include "t194MD.h"
extern MethodInfo m2922_MI;
extern MethodInfo m1770_MI;
extern MethodInfo m1554_MI;
extern MethodInfo m66_MI;
extern MethodInfo m4220_MI;
extern MethodInfo m4221_MI;
extern MethodInfo m3589_MI;
extern MethodInfo m4222_MI;
extern MethodInfo m4223_MI;
extern MethodInfo m4224_MI;


extern MethodInfo m3587_MI;
 uint16_t m3587 (t29 * __this, t7* p0, MethodInfo* method){
	uint16_t V_0 = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			bool L_0 = m2922(p0, (t7*) &_stringLiteral480, &m2922_MI);
			if (!L_0)
			{
				goto IL_0019;
			}
		}

IL_0010:
		{
			t7* L_1 = m1770(p0, 2, &m1770_MI);
			p0 = L_1;
		}

IL_0019:
		{
			IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
			t42 * L_2 = m1554(NULL, LoadTypeToken(&t849_0_0_0), &m1554_MI);
			IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
			t7* L_3 = m66(NULL, (t7*) &_stringLiteral481, p0, &m66_MI);
			IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t49_TI));
			t29 * L_4 = m4220(NULL, L_2, L_3, 0, &m4220_MI);
			V_0 = ((*(uint16_t*)((uint16_t*)UnBox (L_4, InitializedTypeInfo(&t626_TI)))));
			// IL_003a: leave IL_0051
			goto IL_0051;
		}

IL_003f:
		{
			; // IL_003f: leave IL_0051
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t305_TI, e.ex->object.klass))
			goto IL_0044;
		throw e;
	}

IL_0044:
	{ // begin catch(System.ArgumentException)
		{
			V_0 = 0;
			// IL_0047: leave IL_0051
			goto IL_0051;
		}

IL_004c:
		{
			; // IL_004c: leave IL_0051
		}
	} // end catch (depth: 1)

IL_0051:
	{
		return V_0;
	}
}
extern MethodInfo m3588_MI;
 bool m3588 (t29 * __this, uint16_t p0, uint16_t p1, MethodInfo* method){
	uint16_t V_0 = {0};
	int32_t G_B8_0 = 0;
	int32_t G_B21_0 = 0;
	int32_t G_B25_0 = 0;
	int32_t G_B33_0 = 0;
	int32_t G_B70_0 = 0;
	int32_t G_B75_0 = 0;
	int32_t G_B80_0 = 0;
	int32_t G_B85_0 = 0;
	int32_t G_B94_0 = 0;
	int32_t G_B100_0 = 0;
	int32_t G_B107_0 = 0;
	int32_t G_B111_0 = 0;
	int32_t G_B115_0 = 0;
	int32_t G_B119_0 = 0;
	int32_t G_B123_0 = 0;
	int32_t G_B127_0 = 0;
	int32_t G_B131_0 = 0;
	int32_t G_B135_0 = 0;
	int32_t G_B139_0 = 0;
	int32_t G_B143_0 = 0;
	int32_t G_B147_0 = 0;
	int32_t G_B151_0 = 0;
	int32_t G_B155_0 = 0;
	int32_t G_B159_0 = 0;
	int32_t G_B163_0 = 0;
	int32_t G_B167_0 = 0;
	int32_t G_B171_0 = 0;
	int32_t G_B175_0 = 0;
	int32_t G_B179_0 = 0;
	int32_t G_B183_0 = 0;
	int32_t G_B187_0 = 0;
	int32_t G_B191_0 = 0;
	int32_t G_B195_0 = 0;
	int32_t G_B199_0 = 0;
	int32_t G_B203_0 = 0;
	int32_t G_B207_0 = 0;
	int32_t G_B211_0 = 0;
	int32_t G_B215_0 = 0;
	int32_t G_B219_0 = 0;
	int32_t G_B223_0 = 0;
	int32_t G_B227_0 = 0;
	int32_t G_B231_0 = 0;
	int32_t G_B235_0 = 0;
	int32_t G_B239_0 = 0;
	int32_t G_B243_0 = 0;
	int32_t G_B247_0 = 0;
	int32_t G_B251_0 = 0;
	int32_t G_B255_0 = 0;
	int32_t G_B259_0 = 0;
	int32_t G_B263_0 = 0;
	int32_t G_B267_0 = 0;
	int32_t G_B271_0 = 0;
	int32_t G_B275_0 = 0;
	int32_t G_B279_0 = 0;
	int32_t G_B283_0 = 0;
	int32_t G_B287_0 = 0;
	int32_t G_B291_0 = 0;
	int32_t G_B295_0 = 0;
	int32_t G_B299_0 = 0;
	int32_t G_B303_0 = 0;
	int32_t G_B307_0 = 0;
	int32_t G_B311_0 = 0;
	int32_t G_B315_0 = 0;
	int32_t G_B319_0 = 0;
	int32_t G_B323_0 = 0;
	int32_t G_B327_0 = 0;
	int32_t G_B331_0 = 0;
	int32_t G_B335_0 = 0;
	int32_t G_B339_0 = 0;
	int32_t G_B343_0 = 0;
	int32_t G_B347_0 = 0;
	int32_t G_B351_0 = 0;
	int32_t G_B355_0 = 0;
	int32_t G_B359_0 = 0;
	int32_t G_B363_0 = 0;
	int32_t G_B367_0 = 0;
	int32_t G_B371_0 = 0;
	int32_t G_B375_0 = 0;
	int32_t G_B379_0 = 0;
	int32_t G_B383_0 = 0;
	int32_t G_B387_0 = 0;
	int32_t G_B391_0 = 0;
	int32_t G_B395_0 = 0;
	int32_t G_B399_0 = 0;
	int32_t G_B403_0 = 0;
	int32_t G_B407_0 = 0;
	int32_t G_B411_0 = 0;
	int32_t G_B415_0 = 0;
	int32_t G_B419_0 = 0;
	int32_t G_B423_0 = 0;
	int32_t G_B427_0 = 0;
	int32_t G_B431_0 = 0;
	int32_t G_B435_0 = 0;
	int32_t G_B439_0 = 0;
	int32_t G_B443_0 = 0;
	int32_t G_B447_0 = 0;
	int32_t G_B451_0 = 0;
	int32_t G_B457_0 = 0;
	int32_t G_B459_0 = 0;
	{
		V_0 = p0;
		if (V_0 == 0)
		{
			goto IL_024d;
		}
		if (V_0 == 1)
		{
			goto IL_024f;
		}
		if (V_0 == 2)
		{
			goto IL_0258;
		}
		if (V_0 == 3)
		{
			goto IL_025a;
		}
		if (V_0 == 4)
		{
			goto IL_0271;
		}
		if (V_0 == 5)
		{
			goto IL_0278;
		}
		if (V_0 == 6)
		{
			goto IL_027f;
		}
		if (V_0 == 7)
		{
			goto IL_0288;
		}
		if (V_0 == 8)
		{
			goto IL_028a;
		}
		if (V_0 == 9)
		{
			goto IL_02c3;
		}
		if (V_0 == 10)
		{
			goto IL_02d7;
		}
		if (V_0 == 11)
		{
			goto IL_040d;
		}
		if (V_0 == 12)
		{
			goto IL_0448;
		}
		if (V_0 == 13)
		{
			goto IL_046b;
		}
		if (V_0 == 14)
		{
			goto IL_0490;
		}
		if (V_0 == 15)
		{
			goto IL_04b6;
		}
		if (V_0 == 16)
		{
			goto IL_0510;
		}
		if (V_0 == 17)
		{
			goto IL_0543;
		}
		if (V_0 == 18)
		{
			goto IL_0308;
		}
		if (V_0 == 19)
		{
			goto IL_0310;
		}
		if (V_0 == 20)
		{
			goto IL_0318;
		}
		if (V_0 == 21)
		{
			goto IL_0320;
		}
		if (V_0 == 22)
		{
			goto IL_0328;
		}
		if (V_0 == 23)
		{
			goto IL_0330;
		}
		if (V_0 == 24)
		{
			goto IL_0338;
		}
		if (V_0 == 25)
		{
			goto IL_0340;
		}
		if (V_0 == 26)
		{
			goto IL_0348;
		}
		if (V_0 == 27)
		{
			goto IL_0350;
		}
		if (V_0 == 28)
		{
			goto IL_0359;
		}
		if (V_0 == 29)
		{
			goto IL_0362;
		}
		if (V_0 == 30)
		{
			goto IL_036b;
		}
		if (V_0 == 31)
		{
			goto IL_0374;
		}
		if (V_0 == 32)
		{
			goto IL_037d;
		}
		if (V_0 == 33)
		{
			goto IL_0386;
		}
		if (V_0 == 34)
		{
			goto IL_038f;
		}
		if (V_0 == 35)
		{
			goto IL_0398;
		}
		if (V_0 == 36)
		{
			goto IL_03a1;
		}
		if (V_0 == 37)
		{
			goto IL_03aa;
		}
		if (V_0 == 38)
		{
			goto IL_03b3;
		}
		if (V_0 == 39)
		{
			goto IL_03bc;
		}
		if (V_0 == 40)
		{
			goto IL_03c5;
		}
		if (V_0 == 41)
		{
			goto IL_03ce;
		}
		if (V_0 == 42)
		{
			goto IL_03d7;
		}
		if (V_0 == 43)
		{
			goto IL_03e0;
		}
		if (V_0 == 44)
		{
			goto IL_03e9;
		}
		if (V_0 == 45)
		{
			goto IL_03f2;
		}
		if (V_0 == 46)
		{
			goto IL_03fb;
		}
		if (V_0 == 47)
		{
			goto IL_0404;
		}
		if (V_0 == 48)
		{
			goto IL_0583;
		}
		if (V_0 == 49)
		{
			goto IL_0596;
		}
		if (V_0 == 50)
		{
			goto IL_05b0;
		}
		if (V_0 == 51)
		{
			goto IL_05ca;
		}
		if (V_0 == 52)
		{
			goto IL_05e4;
		}
		if (V_0 == 53)
		{
			goto IL_05fe;
		}
		if (V_0 == 54)
		{
			goto IL_0618;
		}
		if (V_0 == 55)
		{
			goto IL_0632;
		}
		if (V_0 == 56)
		{
			goto IL_064c;
		}
		if (V_0 == 57)
		{
			goto IL_0666;
		}
		if (V_0 == 58)
		{
			goto IL_0680;
		}
		if (V_0 == 59)
		{
			goto IL_069a;
		}
		if (V_0 == 60)
		{
			goto IL_06b4;
		}
		if (V_0 == 61)
		{
			goto IL_06ce;
		}
		if (V_0 == 62)
		{
			goto IL_06e8;
		}
		if (V_0 == 63)
		{
			goto IL_0702;
		}
		if (V_0 == 64)
		{
			goto IL_071c;
		}
		if (V_0 == 65)
		{
			goto IL_0736;
		}
		if (V_0 == 66)
		{
			goto IL_0750;
		}
		if (V_0 == 67)
		{
			goto IL_076a;
		}
		if (V_0 == 68)
		{
			goto IL_0784;
		}
		if (V_0 == 69)
		{
			goto IL_079e;
		}
		if (V_0 == 70)
		{
			goto IL_07b8;
		}
		if (V_0 == 71)
		{
			goto IL_07d2;
		}
		if (V_0 == 72)
		{
			goto IL_07ec;
		}
		if (V_0 == 73)
		{
			goto IL_0806;
		}
		if (V_0 == 74)
		{
			goto IL_0820;
		}
		if (V_0 == 75)
		{
			goto IL_083a;
		}
		if (V_0 == 76)
		{
			goto IL_0854;
		}
		if (V_0 == 77)
		{
			goto IL_086e;
		}
		if (V_0 == 78)
		{
			goto IL_0888;
		}
		if (V_0 == 79)
		{
			goto IL_08a2;
		}
		if (V_0 == 80)
		{
			goto IL_08bc;
		}
		if (V_0 == 81)
		{
			goto IL_08d6;
		}
		if (V_0 == 82)
		{
			goto IL_08f0;
		}
		if (V_0 == 83)
		{
			goto IL_090a;
		}
		if (V_0 == 84)
		{
			goto IL_0924;
		}
		if (V_0 == 85)
		{
			goto IL_093e;
		}
		if (V_0 == 86)
		{
			goto IL_0958;
		}
		if (V_0 == 87)
		{
			goto IL_0972;
		}
		if (V_0 == 88)
		{
			goto IL_098c;
		}
		if (V_0 == 89)
		{
			goto IL_09a6;
		}
		if (V_0 == 90)
		{
			goto IL_09c0;
		}
		if (V_0 == 91)
		{
			goto IL_09da;
		}
		if (V_0 == 92)
		{
			goto IL_09f4;
		}
		if (V_0 == 93)
		{
			goto IL_0a0e;
		}
		if (V_0 == 94)
		{
			goto IL_0a28;
		}
		if (V_0 == 95)
		{
			goto IL_0a42;
		}
		if (V_0 == 96)
		{
			goto IL_0a5c;
		}
		if (V_0 == 97)
		{
			goto IL_0a76;
		}
		if (V_0 == 98)
		{
			goto IL_0a90;
		}
		if (V_0 == 99)
		{
			goto IL_0aaa;
		}
		if (V_0 == 100)
		{
			goto IL_0ac4;
		}
		if (V_0 == 101)
		{
			goto IL_0ade;
		}
		if (V_0 == 102)
		{
			goto IL_0af8;
		}
		if (V_0 == 103)
		{
			goto IL_0b12;
		}
		if (V_0 == 104)
		{
			goto IL_0b2c;
		}
		if (V_0 == 105)
		{
			goto IL_0b46;
		}
		if (V_0 == 106)
		{
			goto IL_0b60;
		}
		if (V_0 == 107)
		{
			goto IL_0b7a;
		}
		if (V_0 == 108)
		{
			goto IL_0b94;
		}
		if (V_0 == 109)
		{
			goto IL_0bae;
		}
		if (V_0 == 110)
		{
			goto IL_0bc8;
		}
		if (V_0 == 111)
		{
			goto IL_0be2;
		}
		if (V_0 == 112)
		{
			goto IL_0bfc;
		}
		if (V_0 == 113)
		{
			goto IL_0c16;
		}
		if (V_0 == 114)
		{
			goto IL_0c30;
		}
		if (V_0 == 115)
		{
			goto IL_0c4a;
		}
		if (V_0 == 116)
		{
			goto IL_0c64;
		}
		if (V_0 == 117)
		{
			goto IL_0c7e;
		}
		if (V_0 == 118)
		{
			goto IL_0c98;
		}
		if (V_0 == 119)
		{
			goto IL_0cb2;
		}
		if (V_0 == 120)
		{
			goto IL_0ccc;
		}
		if (V_0 == 121)
		{
			goto IL_0ce6;
		}
		if (V_0 == 122)
		{
			goto IL_0d00;
		}
		if (V_0 == 123)
		{
			goto IL_0d1a;
		}
		if (V_0 == 124)
		{
			goto IL_0d34;
		}
		if (V_0 == 125)
		{
			goto IL_0d4e;
		}
		if (V_0 == 126)
		{
			goto IL_0d68;
		}
		if (V_0 == 127)
		{
			goto IL_0d82;
		}
		if (V_0 == 128)
		{
			goto IL_0d9c;
		}
		if (V_0 == 129)
		{
			goto IL_0db6;
		}
		if (V_0 == 130)
		{
			goto IL_0dd0;
		}
		if (V_0 == 131)
		{
			goto IL_0dea;
		}
		if (V_0 == 132)
		{
			goto IL_0e04;
		}
		if (V_0 == 133)
		{
			goto IL_0e38;
		}
		if (V_0 == 134)
		{
			goto IL_0e1e;
		}
		if (V_0 == 135)
		{
			goto IL_0e6b;
		}
		if (V_0 == 136)
		{
			goto IL_0e6b;
		}
		if (V_0 == 137)
		{
			goto IL_0e6b;
		}
		if (V_0 == 138)
		{
			goto IL_0e6b;
		}
		if (V_0 == 139)
		{
			goto IL_0e6b;
		}
		if (V_0 == 140)
		{
			goto IL_0e6b;
		}
		if (V_0 == 141)
		{
			goto IL_0e6b;
		}
		if (V_0 == 142)
		{
			goto IL_0e6b;
		}
		if (V_0 == 143)
		{
			goto IL_0e6b;
		}
	}
	{
		goto IL_0e6d;
	}

IL_024d:
	{
		return 0;
	}

IL_024f:
	{
		return ((((int32_t)((((int32_t)p1) == ((int32_t)((int32_t)10)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}

IL_0258:
	{
		return 1;
	}

IL_025a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t194_TI));
		bool L_0 = m4221(NULL, p1, &m4221_MI);
		if (L_0)
		{
			goto IL_026f;
		}
	}
	{
		bool L_1 = m3589(NULL, ((int32_t)18), p1, &m3589_MI);
		G_B8_0 = ((int32_t)(L_1));
		goto IL_0270;
	}

IL_026f:
	{
		G_B8_0 = 1;
	}

IL_0270:
	{
		return G_B8_0;
	}

IL_0271:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t194_TI));
		bool L_2 = m4222(NULL, p1, &m4222_MI);
		return L_2;
	}

IL_0278:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t194_TI));
		bool L_3 = m4223(NULL, p1, &m4223_MI);
		return L_3;
	}

IL_027f:
	{
		return ((((int32_t)((((int32_t)p1) == ((int32_t)((int32_t)10)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}

IL_0288:
	{
		return 1;
	}

IL_028a:
	{
		if ((((int32_t)((int32_t)97)) > ((int32_t)p1)))
		{
			goto IL_029a;
		}
	}
	{
		if ((((int32_t)p1) <= ((int32_t)((int32_t)122))))
		{
			goto IL_02c1;
		}
	}

IL_029a:
	{
		if ((((int32_t)((int32_t)65)) > ((int32_t)p1)))
		{
			goto IL_02aa;
		}
	}
	{
		if ((((int32_t)p1) <= ((int32_t)((int32_t)90))))
		{
			goto IL_02c1;
		}
	}

IL_02aa:
	{
		if ((((int32_t)((int32_t)48)) > ((int32_t)p1)))
		{
			goto IL_02ba;
		}
	}
	{
		if ((((int32_t)p1) <= ((int32_t)((int32_t)57))))
		{
			goto IL_02c1;
		}
	}

IL_02ba:
	{
		G_B21_0 = ((((int32_t)((int32_t)95)) == ((int32_t)p1))? 1 : 0);
		goto IL_02c2;
	}

IL_02c1:
	{
		G_B21_0 = 1;
	}

IL_02c2:
	{
		return G_B21_0;
	}

IL_02c3:
	{
		if ((((int32_t)((int32_t)48)) > ((int32_t)p1)))
		{
			goto IL_02d5;
		}
	}
	{
		G_B25_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)57)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_02d6;
	}

IL_02d5:
	{
		G_B25_0 = 0;
	}

IL_02d6:
	{
		return G_B25_0;
	}

IL_02d7:
	{
		if ((((int32_t)p1) == ((int32_t)((int32_t)32))))
		{
			goto IL_0306;
		}
	}
	{
		if ((((int32_t)p1) == ((int32_t)((int32_t)12))))
		{
			goto IL_0306;
		}
	}
	{
		if ((((int32_t)p1) == ((int32_t)((int32_t)10))))
		{
			goto IL_0306;
		}
	}
	{
		if ((((int32_t)p1) == ((int32_t)((int32_t)13))))
		{
			goto IL_0306;
		}
	}
	{
		if ((((int32_t)p1) == ((int32_t)((int32_t)9))))
		{
			goto IL_0306;
		}
	}
	{
		G_B33_0 = ((((int32_t)p1) == ((int32_t)((int32_t)11)))? 1 : 0);
		goto IL_0307;
	}

IL_0306:
	{
		G_B33_0 = 1;
	}

IL_0307:
	{
		return G_B33_0;
	}

IL_0308:
	{
		bool L_4 = m3589(NULL, 0, p1, &m3589_MI);
		return L_4;
	}

IL_0310:
	{
		bool L_5 = m3589(NULL, 1, p1, &m3589_MI);
		return L_5;
	}

IL_0318:
	{
		bool L_6 = m3589(NULL, 2, p1, &m3589_MI);
		return L_6;
	}

IL_0320:
	{
		bool L_7 = m3589(NULL, 3, p1, &m3589_MI);
		return L_7;
	}

IL_0328:
	{
		bool L_8 = m3589(NULL, 4, p1, &m3589_MI);
		return L_8;
	}

IL_0330:
	{
		bool L_9 = m3589(NULL, 5, p1, &m3589_MI);
		return L_9;
	}

IL_0338:
	{
		bool L_10 = m3589(NULL, 7, p1, &m3589_MI);
		return L_10;
	}

IL_0340:
	{
		bool L_11 = m3589(NULL, 6, p1, &m3589_MI);
		return L_11;
	}

IL_0348:
	{
		bool L_12 = m3589(NULL, 8, p1, &m3589_MI);
		return L_12;
	}

IL_0350:
	{
		bool L_13 = m3589(NULL, ((int32_t)9), p1, &m3589_MI);
		return L_13;
	}

IL_0359:
	{
		bool L_14 = m3589(NULL, ((int32_t)10), p1, &m3589_MI);
		return L_14;
	}

IL_0362:
	{
		bool L_15 = m3589(NULL, ((int32_t)11), p1, &m3589_MI);
		return L_15;
	}

IL_036b:
	{
		bool L_16 = m3589(NULL, ((int32_t)12), p1, &m3589_MI);
		return L_16;
	}

IL_0374:
	{
		bool L_17 = m3589(NULL, ((int32_t)13), p1, &m3589_MI);
		return L_17;
	}

IL_037d:
	{
		bool L_18 = m3589(NULL, ((int32_t)19), p1, &m3589_MI);
		return L_18;
	}

IL_0386:
	{
		bool L_19 = m3589(NULL, ((int32_t)20), p1, &m3589_MI);
		return L_19;
	}

IL_038f:
	{
		bool L_20 = m3589(NULL, ((int32_t)22), p1, &m3589_MI);
		return L_20;
	}

IL_0398:
	{
		bool L_21 = m3589(NULL, ((int32_t)21), p1, &m3589_MI);
		return L_21;
	}

IL_03a1:
	{
		bool L_22 = m3589(NULL, ((int32_t)23), p1, &m3589_MI);
		return L_22;
	}

IL_03aa:
	{
		bool L_23 = m3589(NULL, ((int32_t)18), p1, &m3589_MI);
		return L_23;
	}

IL_03b3:
	{
		bool L_24 = m3589(NULL, ((int32_t)24), p1, &m3589_MI);
		return L_24;
	}

IL_03bc:
	{
		bool L_25 = m3589(NULL, ((int32_t)25), p1, &m3589_MI);
		return L_25;
	}

IL_03c5:
	{
		bool L_26 = m3589(NULL, ((int32_t)26), p1, &m3589_MI);
		return L_26;
	}

IL_03ce:
	{
		bool L_27 = m3589(NULL, ((int32_t)27), p1, &m3589_MI);
		return L_27;
	}

IL_03d7:
	{
		bool L_28 = m3589(NULL, ((int32_t)28), p1, &m3589_MI);
		return L_28;
	}

IL_03e0:
	{
		bool L_29 = m3589(NULL, ((int32_t)14), p1, &m3589_MI);
		return L_29;
	}

IL_03e9:
	{
		bool L_30 = m3589(NULL, ((int32_t)15), p1, &m3589_MI);
		return L_30;
	}

IL_03f2:
	{
		bool L_31 = m3589(NULL, ((int32_t)17), p1, &m3589_MI);
		return L_31;
	}

IL_03fb:
	{
		bool L_32 = m3589(NULL, ((int32_t)16), p1, &m3589_MI);
		return L_32;
	}

IL_0404:
	{
		bool L_33 = m3589(NULL, ((int32_t)29), p1, &m3589_MI);
		return L_33;
	}

IL_040d:
	{
		bool L_34 = m3589(NULL, 0, p1, &m3589_MI);
		if (L_34)
		{
			goto IL_0446;
		}
	}
	{
		bool L_35 = m3589(NULL, 1, p1, &m3589_MI);
		if (L_35)
		{
			goto IL_0446;
		}
	}
	{
		bool L_36 = m3589(NULL, 2, p1, &m3589_MI);
		if (L_36)
		{
			goto IL_0446;
		}
	}
	{
		bool L_37 = m3589(NULL, 3, p1, &m3589_MI);
		if (L_37)
		{
			goto IL_0446;
		}
	}
	{
		bool L_38 = m3589(NULL, 4, p1, &m3589_MI);
		G_B70_0 = ((int32_t)(L_38));
		goto IL_0447;
	}

IL_0446:
	{
		G_B70_0 = 1;
	}

IL_0447:
	{
		return G_B70_0;
	}

IL_0448:
	{
		bool L_39 = m3589(NULL, 5, p1, &m3589_MI);
		if (L_39)
		{
			goto IL_0469;
		}
	}
	{
		bool L_40 = m3589(NULL, 7, p1, &m3589_MI);
		if (L_40)
		{
			goto IL_0469;
		}
	}
	{
		bool L_41 = m3589(NULL, 6, p1, &m3589_MI);
		G_B75_0 = ((int32_t)(L_41));
		goto IL_046a;
	}

IL_0469:
	{
		G_B75_0 = 1;
	}

IL_046a:
	{
		return G_B75_0;
	}

IL_046b:
	{
		bool L_42 = m3589(NULL, 8, p1, &m3589_MI);
		if (L_42)
		{
			goto IL_048e;
		}
	}
	{
		bool L_43 = m3589(NULL, ((int32_t)9), p1, &m3589_MI);
		if (L_43)
		{
			goto IL_048e;
		}
	}
	{
		bool L_44 = m3589(NULL, ((int32_t)10), p1, &m3589_MI);
		G_B80_0 = ((int32_t)(L_44));
		goto IL_048f;
	}

IL_048e:
	{
		G_B80_0 = 1;
	}

IL_048f:
	{
		return G_B80_0;
	}

IL_0490:
	{
		bool L_45 = m3589(NULL, ((int32_t)11), p1, &m3589_MI);
		if (L_45)
		{
			goto IL_04b4;
		}
	}
	{
		bool L_46 = m3589(NULL, ((int32_t)12), p1, &m3589_MI);
		if (L_46)
		{
			goto IL_04b4;
		}
	}
	{
		bool L_47 = m3589(NULL, ((int32_t)13), p1, &m3589_MI);
		G_B85_0 = ((int32_t)(L_47));
		goto IL_04b5;
	}

IL_04b4:
	{
		G_B85_0 = 1;
	}

IL_04b5:
	{
		return G_B85_0;
	}

IL_04b6:
	{
		bool L_48 = m3589(NULL, ((int32_t)19), p1, &m3589_MI);
		if (L_48)
		{
			goto IL_050e;
		}
	}
	{
		bool L_49 = m3589(NULL, ((int32_t)20), p1, &m3589_MI);
		if (L_49)
		{
			goto IL_050e;
		}
	}
	{
		bool L_50 = m3589(NULL, ((int32_t)22), p1, &m3589_MI);
		if (L_50)
		{
			goto IL_050e;
		}
	}
	{
		bool L_51 = m3589(NULL, ((int32_t)21), p1, &m3589_MI);
		if (L_51)
		{
			goto IL_050e;
		}
	}
	{
		bool L_52 = m3589(NULL, ((int32_t)23), p1, &m3589_MI);
		if (L_52)
		{
			goto IL_050e;
		}
	}
	{
		bool L_53 = m3589(NULL, ((int32_t)18), p1, &m3589_MI);
		if (L_53)
		{
			goto IL_050e;
		}
	}
	{
		bool L_54 = m3589(NULL, ((int32_t)24), p1, &m3589_MI);
		G_B94_0 = ((int32_t)(L_54));
		goto IL_050f;
	}

IL_050e:
	{
		G_B94_0 = 1;
	}

IL_050f:
	{
		return G_B94_0;
	}

IL_0510:
	{
		bool L_55 = m3589(NULL, ((int32_t)25), p1, &m3589_MI);
		if (L_55)
		{
			goto IL_0541;
		}
	}
	{
		bool L_56 = m3589(NULL, ((int32_t)26), p1, &m3589_MI);
		if (L_56)
		{
			goto IL_0541;
		}
	}
	{
		bool L_57 = m3589(NULL, ((int32_t)27), p1, &m3589_MI);
		if (L_57)
		{
			goto IL_0541;
		}
	}
	{
		bool L_58 = m3589(NULL, ((int32_t)28), p1, &m3589_MI);
		G_B100_0 = ((int32_t)(L_58));
		goto IL_0542;
	}

IL_0541:
	{
		G_B100_0 = 1;
	}

IL_0542:
	{
		return G_B100_0;
	}

IL_0543:
	{
		bool L_59 = m3589(NULL, ((int32_t)14), p1, &m3589_MI);
		if (L_59)
		{
			goto IL_0581;
		}
	}
	{
		bool L_60 = m3589(NULL, ((int32_t)15), p1, &m3589_MI);
		if (L_60)
		{
			goto IL_0581;
		}
	}
	{
		bool L_61 = m3589(NULL, ((int32_t)17), p1, &m3589_MI);
		if (L_61)
		{
			goto IL_0581;
		}
	}
	{
		bool L_62 = m3589(NULL, ((int32_t)16), p1, &m3589_MI);
		if (L_62)
		{
			goto IL_0581;
		}
	}
	{
		bool L_63 = m3589(NULL, ((int32_t)29), p1, &m3589_MI);
		G_B107_0 = ((int32_t)(L_63));
		goto IL_0582;
	}

IL_0581:
	{
		G_B107_0 = 1;
	}

IL_0582:
	{
		return G_B107_0;
	}

IL_0583:
	{
		if ((((int32_t)0) > ((int32_t)p1)))
		{
			goto IL_0594;
		}
	}
	{
		G_B111_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)127)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0595;
	}

IL_0594:
	{
		G_B111_0 = 0;
	}

IL_0595:
	{
		return G_B111_0;
	}

IL_0596:
	{
		if ((((int32_t)((int32_t)128)) > ((int32_t)p1)))
		{
			goto IL_05ae;
		}
	}
	{
		G_B115_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)255)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_05af;
	}

IL_05ae:
	{
		G_B115_0 = 0;
	}

IL_05af:
	{
		return G_B115_0;
	}

IL_05b0:
	{
		if ((((int32_t)((int32_t)256)) > ((int32_t)p1)))
		{
			goto IL_05c8;
		}
	}
	{
		G_B119_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)383)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_05c9;
	}

IL_05c8:
	{
		G_B119_0 = 0;
	}

IL_05c9:
	{
		return G_B119_0;
	}

IL_05ca:
	{
		if ((((int32_t)((int32_t)384)) > ((int32_t)p1)))
		{
			goto IL_05e2;
		}
	}
	{
		G_B123_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)591)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_05e3;
	}

IL_05e2:
	{
		G_B123_0 = 0;
	}

IL_05e3:
	{
		return G_B123_0;
	}

IL_05e4:
	{
		if ((((int32_t)((int32_t)592)) > ((int32_t)p1)))
		{
			goto IL_05fc;
		}
	}
	{
		G_B127_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)687)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_05fd;
	}

IL_05fc:
	{
		G_B127_0 = 0;
	}

IL_05fd:
	{
		return G_B127_0;
	}

IL_05fe:
	{
		if ((((int32_t)((int32_t)688)) > ((int32_t)p1)))
		{
			goto IL_0616;
		}
	}
	{
		G_B131_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)767)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0617;
	}

IL_0616:
	{
		G_B131_0 = 0;
	}

IL_0617:
	{
		return G_B131_0;
	}

IL_0618:
	{
		if ((((int32_t)((int32_t)768)) > ((int32_t)p1)))
		{
			goto IL_0630;
		}
	}
	{
		G_B135_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)879)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0631;
	}

IL_0630:
	{
		G_B135_0 = 0;
	}

IL_0631:
	{
		return G_B135_0;
	}

IL_0632:
	{
		if ((((int32_t)((int32_t)880)) > ((int32_t)p1)))
		{
			goto IL_064a;
		}
	}
	{
		G_B139_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)1023)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_064b;
	}

IL_064a:
	{
		G_B139_0 = 0;
	}

IL_064b:
	{
		return G_B139_0;
	}

IL_064c:
	{
		if ((((int32_t)((int32_t)1024)) > ((int32_t)p1)))
		{
			goto IL_0664;
		}
	}
	{
		G_B143_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)1279)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0665;
	}

IL_0664:
	{
		G_B143_0 = 0;
	}

IL_0665:
	{
		return G_B143_0;
	}

IL_0666:
	{
		if ((((int32_t)((int32_t)1328)) > ((int32_t)p1)))
		{
			goto IL_067e;
		}
	}
	{
		G_B147_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)1423)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_067f;
	}

IL_067e:
	{
		G_B147_0 = 0;
	}

IL_067f:
	{
		return G_B147_0;
	}

IL_0680:
	{
		if ((((int32_t)((int32_t)1424)) > ((int32_t)p1)))
		{
			goto IL_0698;
		}
	}
	{
		G_B151_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)1535)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0699;
	}

IL_0698:
	{
		G_B151_0 = 0;
	}

IL_0699:
	{
		return G_B151_0;
	}

IL_069a:
	{
		if ((((int32_t)((int32_t)1536)) > ((int32_t)p1)))
		{
			goto IL_06b2;
		}
	}
	{
		G_B155_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)1791)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_06b3;
	}

IL_06b2:
	{
		G_B155_0 = 0;
	}

IL_06b3:
	{
		return G_B155_0;
	}

IL_06b4:
	{
		if ((((int32_t)((int32_t)1792)) > ((int32_t)p1)))
		{
			goto IL_06cc;
		}
	}
	{
		G_B159_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)1871)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_06cd;
	}

IL_06cc:
	{
		G_B159_0 = 0;
	}

IL_06cd:
	{
		return G_B159_0;
	}

IL_06ce:
	{
		if ((((int32_t)((int32_t)1920)) > ((int32_t)p1)))
		{
			goto IL_06e6;
		}
	}
	{
		G_B163_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)1983)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_06e7;
	}

IL_06e6:
	{
		G_B163_0 = 0;
	}

IL_06e7:
	{
		return G_B163_0;
	}

IL_06e8:
	{
		if ((((int32_t)((int32_t)2304)) > ((int32_t)p1)))
		{
			goto IL_0700;
		}
	}
	{
		G_B167_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)2431)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0701;
	}

IL_0700:
	{
		G_B167_0 = 0;
	}

IL_0701:
	{
		return G_B167_0;
	}

IL_0702:
	{
		if ((((int32_t)((int32_t)2432)) > ((int32_t)p1)))
		{
			goto IL_071a;
		}
	}
	{
		G_B171_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)2559)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_071b;
	}

IL_071a:
	{
		G_B171_0 = 0;
	}

IL_071b:
	{
		return G_B171_0;
	}

IL_071c:
	{
		if ((((int32_t)((int32_t)2560)) > ((int32_t)p1)))
		{
			goto IL_0734;
		}
	}
	{
		G_B175_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)2687)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0735;
	}

IL_0734:
	{
		G_B175_0 = 0;
	}

IL_0735:
	{
		return G_B175_0;
	}

IL_0736:
	{
		if ((((int32_t)((int32_t)2688)) > ((int32_t)p1)))
		{
			goto IL_074e;
		}
	}
	{
		G_B179_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)2815)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_074f;
	}

IL_074e:
	{
		G_B179_0 = 0;
	}

IL_074f:
	{
		return G_B179_0;
	}

IL_0750:
	{
		if ((((int32_t)((int32_t)2816)) > ((int32_t)p1)))
		{
			goto IL_0768;
		}
	}
	{
		G_B183_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)2943)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0769;
	}

IL_0768:
	{
		G_B183_0 = 0;
	}

IL_0769:
	{
		return G_B183_0;
	}

IL_076a:
	{
		if ((((int32_t)((int32_t)2944)) > ((int32_t)p1)))
		{
			goto IL_0782;
		}
	}
	{
		G_B187_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)3071)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0783;
	}

IL_0782:
	{
		G_B187_0 = 0;
	}

IL_0783:
	{
		return G_B187_0;
	}

IL_0784:
	{
		if ((((int32_t)((int32_t)3072)) > ((int32_t)p1)))
		{
			goto IL_079c;
		}
	}
	{
		G_B191_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)3199)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_079d;
	}

IL_079c:
	{
		G_B191_0 = 0;
	}

IL_079d:
	{
		return G_B191_0;
	}

IL_079e:
	{
		if ((((int32_t)((int32_t)3200)) > ((int32_t)p1)))
		{
			goto IL_07b6;
		}
	}
	{
		G_B195_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)3327)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_07b7;
	}

IL_07b6:
	{
		G_B195_0 = 0;
	}

IL_07b7:
	{
		return G_B195_0;
	}

IL_07b8:
	{
		if ((((int32_t)((int32_t)3328)) > ((int32_t)p1)))
		{
			goto IL_07d0;
		}
	}
	{
		G_B199_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)3455)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_07d1;
	}

IL_07d0:
	{
		G_B199_0 = 0;
	}

IL_07d1:
	{
		return G_B199_0;
	}

IL_07d2:
	{
		if ((((int32_t)((int32_t)3456)) > ((int32_t)p1)))
		{
			goto IL_07ea;
		}
	}
	{
		G_B203_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)3583)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_07eb;
	}

IL_07ea:
	{
		G_B203_0 = 0;
	}

IL_07eb:
	{
		return G_B203_0;
	}

IL_07ec:
	{
		if ((((int32_t)((int32_t)3584)) > ((int32_t)p1)))
		{
			goto IL_0804;
		}
	}
	{
		G_B207_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)3711)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0805;
	}

IL_0804:
	{
		G_B207_0 = 0;
	}

IL_0805:
	{
		return G_B207_0;
	}

IL_0806:
	{
		if ((((int32_t)((int32_t)3712)) > ((int32_t)p1)))
		{
			goto IL_081e;
		}
	}
	{
		G_B211_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)3839)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_081f;
	}

IL_081e:
	{
		G_B211_0 = 0;
	}

IL_081f:
	{
		return G_B211_0;
	}

IL_0820:
	{
		if ((((int32_t)((int32_t)3840)) > ((int32_t)p1)))
		{
			goto IL_0838;
		}
	}
	{
		G_B215_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)4095)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0839;
	}

IL_0838:
	{
		G_B215_0 = 0;
	}

IL_0839:
	{
		return G_B215_0;
	}

IL_083a:
	{
		if ((((int32_t)((int32_t)4096)) > ((int32_t)p1)))
		{
			goto IL_0852;
		}
	}
	{
		G_B219_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)4255)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0853;
	}

IL_0852:
	{
		G_B219_0 = 0;
	}

IL_0853:
	{
		return G_B219_0;
	}

IL_0854:
	{
		if ((((int32_t)((int32_t)4256)) > ((int32_t)p1)))
		{
			goto IL_086c;
		}
	}
	{
		G_B223_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)4351)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_086d;
	}

IL_086c:
	{
		G_B223_0 = 0;
	}

IL_086d:
	{
		return G_B223_0;
	}

IL_086e:
	{
		if ((((int32_t)((int32_t)4352)) > ((int32_t)p1)))
		{
			goto IL_0886;
		}
	}
	{
		G_B227_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)4607)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0887;
	}

IL_0886:
	{
		G_B227_0 = 0;
	}

IL_0887:
	{
		return G_B227_0;
	}

IL_0888:
	{
		if ((((int32_t)((int32_t)4608)) > ((int32_t)p1)))
		{
			goto IL_08a0;
		}
	}
	{
		G_B231_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)4991)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_08a1;
	}

IL_08a0:
	{
		G_B231_0 = 0;
	}

IL_08a1:
	{
		return G_B231_0;
	}

IL_08a2:
	{
		if ((((int32_t)((int32_t)5024)) > ((int32_t)p1)))
		{
			goto IL_08ba;
		}
	}
	{
		G_B235_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)5119)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_08bb;
	}

IL_08ba:
	{
		G_B235_0 = 0;
	}

IL_08bb:
	{
		return G_B235_0;
	}

IL_08bc:
	{
		if ((((int32_t)((int32_t)5120)) > ((int32_t)p1)))
		{
			goto IL_08d4;
		}
	}
	{
		G_B239_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)5759)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_08d5;
	}

IL_08d4:
	{
		G_B239_0 = 0;
	}

IL_08d5:
	{
		return G_B239_0;
	}

IL_08d6:
	{
		if ((((int32_t)((int32_t)5760)) > ((int32_t)p1)))
		{
			goto IL_08ee;
		}
	}
	{
		G_B243_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)5791)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_08ef;
	}

IL_08ee:
	{
		G_B243_0 = 0;
	}

IL_08ef:
	{
		return G_B243_0;
	}

IL_08f0:
	{
		if ((((int32_t)((int32_t)5792)) > ((int32_t)p1)))
		{
			goto IL_0908;
		}
	}
	{
		G_B247_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)5887)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0909;
	}

IL_0908:
	{
		G_B247_0 = 0;
	}

IL_0909:
	{
		return G_B247_0;
	}

IL_090a:
	{
		if ((((int32_t)((int32_t)6016)) > ((int32_t)p1)))
		{
			goto IL_0922;
		}
	}
	{
		G_B251_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)6143)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0923;
	}

IL_0922:
	{
		G_B251_0 = 0;
	}

IL_0923:
	{
		return G_B251_0;
	}

IL_0924:
	{
		if ((((int32_t)((int32_t)6144)) > ((int32_t)p1)))
		{
			goto IL_093c;
		}
	}
	{
		G_B255_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)6319)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_093d;
	}

IL_093c:
	{
		G_B255_0 = 0;
	}

IL_093d:
	{
		return G_B255_0;
	}

IL_093e:
	{
		if ((((int32_t)((int32_t)7680)) > ((int32_t)p1)))
		{
			goto IL_0956;
		}
	}
	{
		G_B259_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)7935)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0957;
	}

IL_0956:
	{
		G_B259_0 = 0;
	}

IL_0957:
	{
		return G_B259_0;
	}

IL_0958:
	{
		if ((((int32_t)((int32_t)7936)) > ((int32_t)p1)))
		{
			goto IL_0970;
		}
	}
	{
		G_B263_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)8191)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0971;
	}

IL_0970:
	{
		G_B263_0 = 0;
	}

IL_0971:
	{
		return G_B263_0;
	}

IL_0972:
	{
		if ((((int32_t)((int32_t)8192)) > ((int32_t)p1)))
		{
			goto IL_098a;
		}
	}
	{
		G_B267_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)8303)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_098b;
	}

IL_098a:
	{
		G_B267_0 = 0;
	}

IL_098b:
	{
		return G_B267_0;
	}

IL_098c:
	{
		if ((((int32_t)((int32_t)8304)) > ((int32_t)p1)))
		{
			goto IL_09a4;
		}
	}
	{
		G_B271_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)8351)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_09a5;
	}

IL_09a4:
	{
		G_B271_0 = 0;
	}

IL_09a5:
	{
		return G_B271_0;
	}

IL_09a6:
	{
		if ((((int32_t)((int32_t)8352)) > ((int32_t)p1)))
		{
			goto IL_09be;
		}
	}
	{
		G_B275_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)8399)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_09bf;
	}

IL_09be:
	{
		G_B275_0 = 0;
	}

IL_09bf:
	{
		return G_B275_0;
	}

IL_09c0:
	{
		if ((((int32_t)((int32_t)8400)) > ((int32_t)p1)))
		{
			goto IL_09d8;
		}
	}
	{
		G_B279_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)8447)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_09d9;
	}

IL_09d8:
	{
		G_B279_0 = 0;
	}

IL_09d9:
	{
		return G_B279_0;
	}

IL_09da:
	{
		if ((((int32_t)((int32_t)8448)) > ((int32_t)p1)))
		{
			goto IL_09f2;
		}
	}
	{
		G_B283_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)8527)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_09f3;
	}

IL_09f2:
	{
		G_B283_0 = 0;
	}

IL_09f3:
	{
		return G_B283_0;
	}

IL_09f4:
	{
		if ((((int32_t)((int32_t)8528)) > ((int32_t)p1)))
		{
			goto IL_0a0c;
		}
	}
	{
		G_B287_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)8591)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0a0d;
	}

IL_0a0c:
	{
		G_B287_0 = 0;
	}

IL_0a0d:
	{
		return G_B287_0;
	}

IL_0a0e:
	{
		if ((((int32_t)((int32_t)8592)) > ((int32_t)p1)))
		{
			goto IL_0a26;
		}
	}
	{
		G_B291_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)8703)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0a27;
	}

IL_0a26:
	{
		G_B291_0 = 0;
	}

IL_0a27:
	{
		return G_B291_0;
	}

IL_0a28:
	{
		if ((((int32_t)((int32_t)8704)) > ((int32_t)p1)))
		{
			goto IL_0a40;
		}
	}
	{
		G_B295_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)8959)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0a41;
	}

IL_0a40:
	{
		G_B295_0 = 0;
	}

IL_0a41:
	{
		return G_B295_0;
	}

IL_0a42:
	{
		if ((((int32_t)((int32_t)8960)) > ((int32_t)p1)))
		{
			goto IL_0a5a;
		}
	}
	{
		G_B299_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)9215)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0a5b;
	}

IL_0a5a:
	{
		G_B299_0 = 0;
	}

IL_0a5b:
	{
		return G_B299_0;
	}

IL_0a5c:
	{
		if ((((int32_t)((int32_t)9216)) > ((int32_t)p1)))
		{
			goto IL_0a74;
		}
	}
	{
		G_B303_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)9279)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0a75;
	}

IL_0a74:
	{
		G_B303_0 = 0;
	}

IL_0a75:
	{
		return G_B303_0;
	}

IL_0a76:
	{
		if ((((int32_t)((int32_t)9280)) > ((int32_t)p1)))
		{
			goto IL_0a8e;
		}
	}
	{
		G_B307_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)9311)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0a8f;
	}

IL_0a8e:
	{
		G_B307_0 = 0;
	}

IL_0a8f:
	{
		return G_B307_0;
	}

IL_0a90:
	{
		if ((((int32_t)((int32_t)9312)) > ((int32_t)p1)))
		{
			goto IL_0aa8;
		}
	}
	{
		G_B311_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)9471)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0aa9;
	}

IL_0aa8:
	{
		G_B311_0 = 0;
	}

IL_0aa9:
	{
		return G_B311_0;
	}

IL_0aaa:
	{
		if ((((int32_t)((int32_t)9472)) > ((int32_t)p1)))
		{
			goto IL_0ac2;
		}
	}
	{
		G_B315_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)9599)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0ac3;
	}

IL_0ac2:
	{
		G_B315_0 = 0;
	}

IL_0ac3:
	{
		return G_B315_0;
	}

IL_0ac4:
	{
		if ((((int32_t)((int32_t)9600)) > ((int32_t)p1)))
		{
			goto IL_0adc;
		}
	}
	{
		G_B319_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)9631)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0add;
	}

IL_0adc:
	{
		G_B319_0 = 0;
	}

IL_0add:
	{
		return G_B319_0;
	}

IL_0ade:
	{
		if ((((int32_t)((int32_t)9632)) > ((int32_t)p1)))
		{
			goto IL_0af6;
		}
	}
	{
		G_B323_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)9727)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0af7;
	}

IL_0af6:
	{
		G_B323_0 = 0;
	}

IL_0af7:
	{
		return G_B323_0;
	}

IL_0af8:
	{
		if ((((int32_t)((int32_t)9728)) > ((int32_t)p1)))
		{
			goto IL_0b10;
		}
	}
	{
		G_B327_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)9983)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0b11;
	}

IL_0b10:
	{
		G_B327_0 = 0;
	}

IL_0b11:
	{
		return G_B327_0;
	}

IL_0b12:
	{
		if ((((int32_t)((int32_t)9984)) > ((int32_t)p1)))
		{
			goto IL_0b2a;
		}
	}
	{
		G_B331_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)10175)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0b2b;
	}

IL_0b2a:
	{
		G_B331_0 = 0;
	}

IL_0b2b:
	{
		return G_B331_0;
	}

IL_0b2c:
	{
		if ((((int32_t)((int32_t)10240)) > ((int32_t)p1)))
		{
			goto IL_0b44;
		}
	}
	{
		G_B335_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)10495)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0b45;
	}

IL_0b44:
	{
		G_B335_0 = 0;
	}

IL_0b45:
	{
		return G_B335_0;
	}

IL_0b46:
	{
		if ((((int32_t)((int32_t)11904)) > ((int32_t)p1)))
		{
			goto IL_0b5e;
		}
	}
	{
		G_B339_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)12031)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0b5f;
	}

IL_0b5e:
	{
		G_B339_0 = 0;
	}

IL_0b5f:
	{
		return G_B339_0;
	}

IL_0b60:
	{
		if ((((int32_t)((int32_t)12032)) > ((int32_t)p1)))
		{
			goto IL_0b78;
		}
	}
	{
		G_B343_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)12255)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0b79;
	}

IL_0b78:
	{
		G_B343_0 = 0;
	}

IL_0b79:
	{
		return G_B343_0;
	}

IL_0b7a:
	{
		if ((((int32_t)((int32_t)12272)) > ((int32_t)p1)))
		{
			goto IL_0b92;
		}
	}
	{
		G_B347_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)12287)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0b93;
	}

IL_0b92:
	{
		G_B347_0 = 0;
	}

IL_0b93:
	{
		return G_B347_0;
	}

IL_0b94:
	{
		if ((((int32_t)((int32_t)12288)) > ((int32_t)p1)))
		{
			goto IL_0bac;
		}
	}
	{
		G_B351_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)12351)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0bad;
	}

IL_0bac:
	{
		G_B351_0 = 0;
	}

IL_0bad:
	{
		return G_B351_0;
	}

IL_0bae:
	{
		if ((((int32_t)((int32_t)12352)) > ((int32_t)p1)))
		{
			goto IL_0bc6;
		}
	}
	{
		G_B355_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)12447)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0bc7;
	}

IL_0bc6:
	{
		G_B355_0 = 0;
	}

IL_0bc7:
	{
		return G_B355_0;
	}

IL_0bc8:
	{
		if ((((int32_t)((int32_t)12448)) > ((int32_t)p1)))
		{
			goto IL_0be0;
		}
	}
	{
		G_B359_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)12543)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0be1;
	}

IL_0be0:
	{
		G_B359_0 = 0;
	}

IL_0be1:
	{
		return G_B359_0;
	}

IL_0be2:
	{
		if ((((int32_t)((int32_t)12544)) > ((int32_t)p1)))
		{
			goto IL_0bfa;
		}
	}
	{
		G_B363_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)12591)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0bfb;
	}

IL_0bfa:
	{
		G_B363_0 = 0;
	}

IL_0bfb:
	{
		return G_B363_0;
	}

IL_0bfc:
	{
		if ((((int32_t)((int32_t)12592)) > ((int32_t)p1)))
		{
			goto IL_0c14;
		}
	}
	{
		G_B367_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)12687)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0c15;
	}

IL_0c14:
	{
		G_B367_0 = 0;
	}

IL_0c15:
	{
		return G_B367_0;
	}

IL_0c16:
	{
		if ((((int32_t)((int32_t)12688)) > ((int32_t)p1)))
		{
			goto IL_0c2e;
		}
	}
	{
		G_B371_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)12703)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0c2f;
	}

IL_0c2e:
	{
		G_B371_0 = 0;
	}

IL_0c2f:
	{
		return G_B371_0;
	}

IL_0c30:
	{
		if ((((int32_t)((int32_t)12704)) > ((int32_t)p1)))
		{
			goto IL_0c48;
		}
	}
	{
		G_B375_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)12735)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0c49;
	}

IL_0c48:
	{
		G_B375_0 = 0;
	}

IL_0c49:
	{
		return G_B375_0;
	}

IL_0c4a:
	{
		if ((((int32_t)((int32_t)12800)) > ((int32_t)p1)))
		{
			goto IL_0c62;
		}
	}
	{
		G_B379_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)13055)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0c63;
	}

IL_0c62:
	{
		G_B379_0 = 0;
	}

IL_0c63:
	{
		return G_B379_0;
	}

IL_0c64:
	{
		if ((((int32_t)((int32_t)13056)) > ((int32_t)p1)))
		{
			goto IL_0c7c;
		}
	}
	{
		G_B383_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)13311)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0c7d;
	}

IL_0c7c:
	{
		G_B383_0 = 0;
	}

IL_0c7d:
	{
		return G_B383_0;
	}

IL_0c7e:
	{
		if ((((int32_t)((int32_t)13312)) > ((int32_t)p1)))
		{
			goto IL_0c96;
		}
	}
	{
		G_B387_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)19893)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0c97;
	}

IL_0c96:
	{
		G_B387_0 = 0;
	}

IL_0c97:
	{
		return G_B387_0;
	}

IL_0c98:
	{
		if ((((int32_t)((int32_t)19968)) > ((int32_t)p1)))
		{
			goto IL_0cb0;
		}
	}
	{
		G_B391_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)40959)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0cb1;
	}

IL_0cb0:
	{
		G_B391_0 = 0;
	}

IL_0cb1:
	{
		return G_B391_0;
	}

IL_0cb2:
	{
		if ((((int32_t)((int32_t)40960)) > ((int32_t)p1)))
		{
			goto IL_0cca;
		}
	}
	{
		G_B395_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)42127)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0ccb;
	}

IL_0cca:
	{
		G_B395_0 = 0;
	}

IL_0ccb:
	{
		return G_B395_0;
	}

IL_0ccc:
	{
		if ((((int32_t)((int32_t)42128)) > ((int32_t)p1)))
		{
			goto IL_0ce4;
		}
	}
	{
		G_B399_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)42191)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0ce5;
	}

IL_0ce4:
	{
		G_B399_0 = 0;
	}

IL_0ce5:
	{
		return G_B399_0;
	}

IL_0ce6:
	{
		if ((((int32_t)((int32_t)44032)) > ((int32_t)p1)))
		{
			goto IL_0cfe;
		}
	}
	{
		G_B403_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)55203)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0cff;
	}

IL_0cfe:
	{
		G_B403_0 = 0;
	}

IL_0cff:
	{
		return G_B403_0;
	}

IL_0d00:
	{
		if ((((int32_t)((int32_t)55296)) > ((int32_t)p1)))
		{
			goto IL_0d18;
		}
	}
	{
		G_B407_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)56191)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0d19;
	}

IL_0d18:
	{
		G_B407_0 = 0;
	}

IL_0d19:
	{
		return G_B407_0;
	}

IL_0d1a:
	{
		if ((((int32_t)((int32_t)56192)) > ((int32_t)p1)))
		{
			goto IL_0d32;
		}
	}
	{
		G_B411_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)56319)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0d33;
	}

IL_0d32:
	{
		G_B411_0 = 0;
	}

IL_0d33:
	{
		return G_B411_0;
	}

IL_0d34:
	{
		if ((((int32_t)((int32_t)56320)) > ((int32_t)p1)))
		{
			goto IL_0d4c;
		}
	}
	{
		G_B415_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)57343)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0d4d;
	}

IL_0d4c:
	{
		G_B415_0 = 0;
	}

IL_0d4d:
	{
		return G_B415_0;
	}

IL_0d4e:
	{
		if ((((int32_t)((int32_t)57344)) > ((int32_t)p1)))
		{
			goto IL_0d66;
		}
	}
	{
		G_B419_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)63743)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0d67;
	}

IL_0d66:
	{
		G_B419_0 = 0;
	}

IL_0d67:
	{
		return G_B419_0;
	}

IL_0d68:
	{
		if ((((int32_t)((int32_t)63744)) > ((int32_t)p1)))
		{
			goto IL_0d80;
		}
	}
	{
		G_B423_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)64255)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0d81;
	}

IL_0d80:
	{
		G_B423_0 = 0;
	}

IL_0d81:
	{
		return G_B423_0;
	}

IL_0d82:
	{
		if ((((int32_t)((int32_t)64256)) > ((int32_t)p1)))
		{
			goto IL_0d9a;
		}
	}
	{
		G_B427_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)64335)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0d9b;
	}

IL_0d9a:
	{
		G_B427_0 = 0;
	}

IL_0d9b:
	{
		return G_B427_0;
	}

IL_0d9c:
	{
		if ((((int32_t)((int32_t)64336)) > ((int32_t)p1)))
		{
			goto IL_0db4;
		}
	}
	{
		G_B431_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)65023)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0db5;
	}

IL_0db4:
	{
		G_B431_0 = 0;
	}

IL_0db5:
	{
		return G_B431_0;
	}

IL_0db6:
	{
		if ((((int32_t)((int32_t)65056)) > ((int32_t)p1)))
		{
			goto IL_0dce;
		}
	}
	{
		G_B435_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)65071)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0dcf;
	}

IL_0dce:
	{
		G_B435_0 = 0;
	}

IL_0dcf:
	{
		return G_B435_0;
	}

IL_0dd0:
	{
		if ((((int32_t)((int32_t)65072)) > ((int32_t)p1)))
		{
			goto IL_0de8;
		}
	}
	{
		G_B439_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)65103)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0de9;
	}

IL_0de8:
	{
		G_B439_0 = 0;
	}

IL_0de9:
	{
		return G_B439_0;
	}

IL_0dea:
	{
		if ((((int32_t)((int32_t)65104)) > ((int32_t)p1)))
		{
			goto IL_0e02;
		}
	}
	{
		G_B443_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)65135)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0e03;
	}

IL_0e02:
	{
		G_B443_0 = 0;
	}

IL_0e03:
	{
		return G_B443_0;
	}

IL_0e04:
	{
		if ((((int32_t)((int32_t)65136)) > ((int32_t)p1)))
		{
			goto IL_0e1c;
		}
	}
	{
		G_B447_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)65278)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0e1d;
	}

IL_0e1c:
	{
		G_B447_0 = 0;
	}

IL_0e1d:
	{
		return G_B447_0;
	}

IL_0e1e:
	{
		if ((((int32_t)((int32_t)65280)) > ((int32_t)p1)))
		{
			goto IL_0e36;
		}
	}
	{
		G_B451_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)65519)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0e37;
	}

IL_0e36:
	{
		G_B451_0 = 0;
	}

IL_0e37:
	{
		return G_B451_0;
	}

IL_0e38:
	{
		if ((((int32_t)((int32_t)65279)) > ((int32_t)p1)))
		{
			goto IL_0e4e;
		}
	}
	{
		if ((((int32_t)p1) <= ((int32_t)((int32_t)65279))))
		{
			goto IL_0e69;
		}
	}

IL_0e4e:
	{
		if ((((int32_t)((int32_t)65520)) > ((int32_t)p1)))
		{
			goto IL_0e66;
		}
	}
	{
		G_B457_0 = ((((int32_t)((((int32_t)p1) > ((int32_t)((int32_t)65533)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0e67;
	}

IL_0e66:
	{
		G_B457_0 = 0;
	}

IL_0e67:
	{
		G_B459_0 = G_B457_0;
		goto IL_0e6a;
	}

IL_0e69:
	{
		G_B459_0 = 1;
	}

IL_0e6a:
	{
		return G_B459_0;
	}

IL_0e6b:
	{
		return 0;
	}

IL_0e6d:
	{
		return 0;
	}
}
 bool m3589 (t29 * __this, int32_t p0, uint16_t p1, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t194_TI));
		int32_t L_0 = m4224(NULL, p1, &m4224_MI);
		if ((((uint32_t)L_0) != ((uint32_t)p0)))
		{
			goto IL_000e;
		}
	}
	{
		return 1;
	}

IL_000e:
	{
		return 0;
	}
}
// Metadata Definition System.Text.RegularExpressions.CategoryUtils
extern Il2CppType t7_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t850_m3587_ParameterInfos[] = 
{
	{"name", 0, 134218171, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t849_0_0_0;
extern void* RuntimeInvoker_t849_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3587_MI = 
{
	"CategoryFromName", (methodPointerType)&m3587, &t850_TI, &t849_0_0_0, RuntimeInvoker_t849_t29, t850_m3587_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 1, false, false, 549, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t849_0_0_0;
extern Il2CppType t194_0_0_0;
extern Il2CppType t194_0_0_0;
static ParameterInfo t850_m3588_ParameterInfos[] = 
{
	{"cat", 0, 134218172, &EmptyCustomAttributesCache, &t849_0_0_0},
	{"c", 1, 134218173, &EmptyCustomAttributesCache, &t194_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t626_t372 (MethodInfo* method, void* obj, void** args);
MethodInfo m3588_MI = 
{
	"IsCategory", (methodPointerType)&m3588, &t850_TI, &t40_0_0_0, RuntimeInvoker_t40_t626_t372, t850_m3588_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 2, false, false, 550, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t851_0_0_0;
extern Il2CppType t851_0_0_0;
extern Il2CppType t194_0_0_0;
static ParameterInfo t850_m3589_ParameterInfos[] = 
{
	{"uc", 0, 134218174, &EmptyCustomAttributesCache, &t851_0_0_0},
	{"c", 1, 134218175, &EmptyCustomAttributesCache, &t194_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44_t372 (MethodInfo* method, void* obj, void** args);
MethodInfo m3589_MI = 
{
	"IsCategory", (methodPointerType)&m3589, &t850_TI, &t40_0_0_0, RuntimeInvoker_t40_t44_t372, t850_m3589_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, false, 551, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t850_MIs[] =
{
	&m3587_MI,
	&m3588_MI,
	&m3589_MI,
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m1332_MI;
static MethodInfo* t850_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t850_0_0_0;
extern Il2CppType t850_1_0_0;
extern TypeInfo t29_TI;
struct t850;
TypeInfo t850_TI = 
{
	&g_System_dll_Image, NULL, "CategoryUtils", "System.Text.RegularExpressions", t850_MIs, NULL, NULL, NULL, &t29_TI, NULL, NULL, &t850_TI, NULL, t850_VT, &EmptyCustomAttributesCache, &t850_TI, &t850_0_0_0, &t850_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t850), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 4, 0, 0};
#include "t852.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t852_TI;
#include "t852MD.h"

#include "t21.h"
#include "t29MD.h"
extern MethodInfo m1331_MI;


extern MethodInfo m3590_MI;
 void m3590 (t852 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
// Metadata Definition System.Text.RegularExpressions.LinkRef
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3590_MI = 
{
	".ctor", (methodPointerType)&m3590, &t852_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 552, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t852_MIs[] =
{
	&m3590_MI,
	NULL
};
static MethodInfo* t852_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t852_0_0_0;
extern Il2CppType t852_1_0_0;
struct t852;
TypeInfo t852_TI = 
{
	&g_System_dll_Image, NULL, "LinkRef", "System.Text.RegularExpressions", t852_MIs, NULL, NULL, NULL, &t29_TI, NULL, NULL, &t852_TI, NULL, t852_VT, &EmptyCustomAttributesCache, &t852_TI, &t852_0_0_0, &t852_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t852), 0, -1, 0, 0, -1, 1048704, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t878_TI;

#include "t858.h"
#include "t845.h"


// Metadata Definition System.Text.RegularExpressions.ICompiler
extern Il2CppType t840_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m4208_MI = 
{
	"GetMachineFactory", NULL, &t878_TI, &t840_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, false, 553, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m4225_MI = 
{
	"EmitFalse", NULL, &t878_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 1, 0, false, false, 554, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m4226_MI = 
{
	"EmitTrue", NULL, &t878_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 2, 0, false, false, 555, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t194_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t878_m4227_ParameterInfos[] = 
{
	{"c", 0, 134218176, &EmptyCustomAttributesCache, &t194_0_0_0},
	{"negate", 1, 134218177, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"ignore", 2, 134218178, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"reverse", 3, 134218179, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t372_t297_t297_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m4227_MI = 
{
	"EmitCharacter", NULL, &t878_TI, &t21_0_0_0, RuntimeInvoker_t21_t372_t297_t297_t297, t878_m4227_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 3, 4, false, false, 556, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t849_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t878_m4228_ParameterInfos[] = 
{
	{"cat", 0, 134218180, &EmptyCustomAttributesCache, &t849_0_0_0},
	{"negate", 1, 134218181, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"reverse", 2, 134218182, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t626_t297_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m4228_MI = 
{
	"EmitCategory", NULL, &t878_TI, &t21_0_0_0, RuntimeInvoker_t21_t626_t297_t297, t878_m4228_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 3, false, false, 557, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t849_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t878_m4229_ParameterInfos[] = 
{
	{"cat", 0, 134218183, &EmptyCustomAttributesCache, &t849_0_0_0},
	{"negate", 1, 134218184, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"reverse", 2, 134218185, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t626_t297_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m4229_MI = 
{
	"EmitNotCategory", NULL, &t878_TI, &t21_0_0_0, RuntimeInvoker_t21_t626_t297_t297, t878_m4229_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 3, false, false, 558, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t194_0_0_0;
extern Il2CppType t194_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t878_m4230_ParameterInfos[] = 
{
	{"lo", 0, 134218186, &EmptyCustomAttributesCache, &t194_0_0_0},
	{"hi", 1, 134218187, &EmptyCustomAttributesCache, &t194_0_0_0},
	{"negate", 2, 134218188, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"ignore", 3, 134218189, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"reverse", 4, 134218190, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t372_t372_t297_t297_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m4230_MI = 
{
	"EmitRange", NULL, &t878_TI, &t21_0_0_0, RuntimeInvoker_t21_t372_t372_t297_t297_t297, t878_m4230_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 5, false, false, 559, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t194_0_0_0;
extern Il2CppType t858_0_0_0;
extern Il2CppType t858_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t878_m4231_ParameterInfos[] = 
{
	{"lo", 0, 134218191, &EmptyCustomAttributesCache, &t194_0_0_0},
	{"set", 1, 134218192, &EmptyCustomAttributesCache, &t858_0_0_0},
	{"negate", 2, 134218193, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"ignore", 3, 134218194, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"reverse", 4, 134218195, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t372_t29_t297_t297_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m4231_MI = 
{
	"EmitSet", NULL, &t878_TI, &t21_0_0_0, RuntimeInvoker_t21_t372_t29_t297_t297_t297, t878_m4231_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 7, 5, false, false, 560, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t878_m4232_ParameterInfos[] = 
{
	{"str", 0, 134218196, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"ignore", 1, 134218197, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"reverse", 2, 134218198, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t297_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m4232_MI = 
{
	"EmitString", NULL, &t878_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t297_t297, t878_m4232_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 3, false, false, 561, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t845_0_0_0;
extern Il2CppType t845_0_0_0;
static ParameterInfo t878_m4233_ParameterInfos[] = 
{
	{"pos", 0, 134218199, &EmptyCustomAttributesCache, &t845_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t626 (MethodInfo* method, void* obj, void** args);
MethodInfo m4233_MI = 
{
	"EmitPosition", NULL, &t878_TI, &t21_0_0_0, RuntimeInvoker_t21_t626, t878_m4233_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 1, false, false, 562, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t878_m4234_ParameterInfos[] = 
{
	{"gid", 0, 134218200, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m4234_MI = 
{
	"EmitOpen", NULL, &t878_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t878_m4234_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 10, 1, false, false, 563, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t878_m4235_ParameterInfos[] = 
{
	{"gid", 0, 134218201, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m4235_MI = 
{
	"EmitClose", NULL, &t878_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t878_m4235_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 11, 1, false, false, 564, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t852_0_0_0;
static ParameterInfo t878_m4236_ParameterInfos[] = 
{
	{"gid", 0, 134218202, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"balance", 1, 134218203, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"capture", 2, 134218204, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"tail", 3, 134218205, &EmptyCustomAttributesCache, &t852_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44_t297_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m4236_MI = 
{
	"EmitBalanceStart", NULL, &t878_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44_t297_t29, t878_m4236_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 12, 4, false, false, 565, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m4237_MI = 
{
	"EmitBalance", NULL, &t878_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 13, 0, false, false, 566, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t878_m4238_ParameterInfos[] = 
{
	{"gid", 0, 134218206, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"ignore", 1, 134218207, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"reverse", 2, 134218208, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t297_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m4238_MI = 
{
	"EmitReference", NULL, &t878_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t297_t297, t878_m4238_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 14, 3, false, false, 567, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t852_0_0_0;
static ParameterInfo t878_m4239_ParameterInfos[] = 
{
	{"gid", 0, 134218209, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"tail", 1, 134218210, &EmptyCustomAttributesCache, &t852_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m4239_MI = 
{
	"EmitIfDefined", NULL, &t878_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t878_m4239_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 15, 2, false, false, 568, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t852_0_0_0;
static ParameterInfo t878_m4240_ParameterInfos[] = 
{
	{"tail", 0, 134218211, &EmptyCustomAttributesCache, &t852_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m4240_MI = 
{
	"EmitSub", NULL, &t878_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t878_m4240_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 16, 1, false, false, 569, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t852_0_0_0;
extern Il2CppType t852_0_0_0;
static ParameterInfo t878_m4241_ParameterInfos[] = 
{
	{"yes", 0, 134218212, &EmptyCustomAttributesCache, &t852_0_0_0},
	{"tail", 1, 134218213, &EmptyCustomAttributesCache, &t852_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m4241_MI = 
{
	"EmitTest", NULL, &t878_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t878_m4241_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 17, 2, false, false, 570, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t852_0_0_0;
static ParameterInfo t878_m4242_ParameterInfos[] = 
{
	{"next", 0, 134218214, &EmptyCustomAttributesCache, &t852_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m4242_MI = 
{
	"EmitBranch", NULL, &t878_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t878_m4242_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 18, 1, false, false, 571, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t852_0_0_0;
static ParameterInfo t878_m4243_ParameterInfos[] = 
{
	{"target", 0, 134218215, &EmptyCustomAttributesCache, &t852_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m4243_MI = 
{
	"EmitJump", NULL, &t878_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t878_m4243_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 19, 1, false, false, 572, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t852_0_0_0;
static ParameterInfo t878_m4244_ParameterInfos[] = 
{
	{"min", 0, 134218216, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"max", 1, 134218217, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"lazy", 2, 134218218, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"until", 3, 134218219, &EmptyCustomAttributesCache, &t852_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44_t297_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m4244_MI = 
{
	"EmitRepeat", NULL, &t878_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44_t297_t29, t878_m4244_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 20, 4, false, false, 573, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t852_0_0_0;
static ParameterInfo t878_m4245_ParameterInfos[] = 
{
	{"repeat", 0, 134218220, &EmptyCustomAttributesCache, &t852_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m4245_MI = 
{
	"EmitUntil", NULL, &t878_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t878_m4245_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 21, 1, false, false, 574, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t852_0_0_0;
static ParameterInfo t878_m4246_ParameterInfos[] = 
{
	{"tail", 0, 134218221, &EmptyCustomAttributesCache, &t852_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m4246_MI = 
{
	"EmitIn", NULL, &t878_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t878_m4246_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 22, 1, false, false, 575, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t878_m4247_ParameterInfos[] = 
{
	{"count", 0, 134218222, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"min", 1, 134218223, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"max", 2, 134218224, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m4247_MI = 
{
	"EmitInfo", NULL, &t878_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44_t44, t878_m4247_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 23, 3, false, false, 576, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t852_0_0_0;
static ParameterInfo t878_m4248_ParameterInfos[] = 
{
	{"min", 0, 134218225, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"max", 1, 134218226, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"lazy", 2, 134218227, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"tail", 3, 134218228, &EmptyCustomAttributesCache, &t852_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44_t297_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m4248_MI = 
{
	"EmitFastRepeat", NULL, &t878_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44_t297_t29, t878_m4248_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 24, 4, false, false, 577, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t852_0_0_0;
static ParameterInfo t878_m4249_ParameterInfos[] = 
{
	{"reverse", 0, 134218229, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"offset", 1, 134218230, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"tail", 2, 134218231, &EmptyCustomAttributesCache, &t852_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m4249_MI = 
{
	"EmitAnchor", NULL, &t878_TI, &t21_0_0_0, RuntimeInvoker_t21_t297_t44_t29, t878_m4249_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 25, 3, false, false, 578, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m4250_MI = 
{
	"EmitBranchEnd", NULL, &t878_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 26, 0, false, false, 579, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m4251_MI = 
{
	"EmitAlternationEnd", NULL, &t878_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 27, 0, false, false, 580, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t852_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m4252_MI = 
{
	"NewLink", NULL, &t878_TI, &t852_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 28, 0, false, false, 581, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t852_0_0_0;
static ParameterInfo t878_m4253_ParameterInfos[] = 
{
	{"link", 0, 134218232, &EmptyCustomAttributesCache, &t852_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m4253_MI = 
{
	"ResolveLink", NULL, &t878_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t878_m4253_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 29, 1, false, false, 582, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t878_MIs[] =
{
	&m4208_MI,
	&m4225_MI,
	&m4226_MI,
	&m4227_MI,
	&m4228_MI,
	&m4229_MI,
	&m4230_MI,
	&m4231_MI,
	&m4232_MI,
	&m4233_MI,
	&m4234_MI,
	&m4235_MI,
	&m4236_MI,
	&m4237_MI,
	&m4238_MI,
	&m4239_MI,
	&m4240_MI,
	&m4241_MI,
	&m4242_MI,
	&m4243_MI,
	&m4244_MI,
	&m4245_MI,
	&m4246_MI,
	&m4247_MI,
	&m4248_MI,
	&m4249_MI,
	&m4250_MI,
	&m4251_MI,
	&m4252_MI,
	&m4253_MI,
	NULL
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t878_0_0_0;
extern Il2CppType t878_1_0_0;
struct t878;
TypeInfo t878_TI = 
{
	&g_System_dll_Image, NULL, "ICompiler", "System.Text.RegularExpressions", t878_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t878_TI, NULL, NULL, &EmptyCustomAttributesCache, &t878_TI, &t878_0_0_0, &t878_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 160, 0, false, true, false, false, false, false, false, false, false, false, false, false, 30, 0, 0, 0, 0, 0, 0, 0};
#include "t853.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t853_TI;
#include "t853MD.h"

#include "mscorlib_ArrayTypes.h"
#include "t863.h"
extern TypeInfo t863_TI;
#include "t863MD.h"
extern MethodInfo m3667_MI;


extern MethodInfo m3591_MI;
 void m3591 (t853 * __this, t764* p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		__this->f1 = p0;
		return;
	}
}
extern MethodInfo m3592_MI;
 t29 * m3592 (t853 * __this, MethodInfo* method){
	{
		t764* L_0 = (__this->f1);
		t863 * L_1 = (t863 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t863_TI));
		m3667(L_1, L_0, &m3667_MI);
		return L_1;
	}
}
extern MethodInfo m3593_MI;
 int32_t m3593 (t853 * __this, MethodInfo* method){
	{
		t764* L_0 = (__this->f1);
		int32_t L_1 = 1;
		return (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_0, L_1));
	}
}
extern MethodInfo m3594_MI;
 int32_t m3594 (t853 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f3);
		return L_0;
	}
}
extern MethodInfo m3595_MI;
 void m3595 (t853 * __this, int32_t p0, MethodInfo* method){
	{
		__this->f3 = p0;
		return;
	}
}
extern MethodInfo m3596_MI;
 t29 * m3596 (t853 * __this, MethodInfo* method){
	{
		t29 * L_0 = (__this->f0);
		return L_0;
	}
}
extern MethodInfo m3597_MI;
 void m3597 (t853 * __this, t29 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		return;
	}
}
extern MethodInfo m3598_MI;
 t446* m3598 (t853 * __this, MethodInfo* method){
	{
		t446* L_0 = (__this->f2);
		return L_0;
	}
}
extern MethodInfo m3599_MI;
 void m3599 (t853 * __this, t446* p0, MethodInfo* method){
	{
		__this->f2 = p0;
		return;
	}
}
// Metadata Definition System.Text.RegularExpressions.InterpreterFactory
extern Il2CppType t721_0_0_1;
FieldInfo t853_f0_FieldInfo = 
{
	"mapping", &t721_0_0_1, &t853_TI, offsetof(t853, f0), &EmptyCustomAttributesCache};
extern Il2CppType t764_0_0_1;
FieldInfo t853_f1_FieldInfo = 
{
	"pattern", &t764_0_0_1, &t853_TI, offsetof(t853, f1), &EmptyCustomAttributesCache};
extern Il2CppType t446_0_0_1;
FieldInfo t853_f2_FieldInfo = 
{
	"namesMapping", &t446_0_0_1, &t853_TI, offsetof(t853, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t853_f3_FieldInfo = 
{
	"gap", &t44_0_0_1, &t853_TI, offsetof(t853, f3), &EmptyCustomAttributesCache};
static FieldInfo* t853_FIs[] =
{
	&t853_f0_FieldInfo,
	&t853_f1_FieldInfo,
	&t853_f2_FieldInfo,
	&t853_f3_FieldInfo,
	NULL
};
static PropertyInfo t853____GroupCount_PropertyInfo = 
{
	&t853_TI, "GroupCount", &m3593_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t853____Gap_PropertyInfo = 
{
	&t853_TI, "Gap", &m3594_MI, &m3595_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t853____Mapping_PropertyInfo = 
{
	&t853_TI, "Mapping", &m3596_MI, &m3597_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t853____NamesMapping_PropertyInfo = 
{
	&t853_TI, "NamesMapping", &m3598_MI, &m3599_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t853_PIs[] =
{
	&t853____GroupCount_PropertyInfo,
	&t853____Gap_PropertyInfo,
	&t853____Mapping_PropertyInfo,
	&t853____NamesMapping_PropertyInfo,
	NULL
};
extern Il2CppType t764_0_0_0;
extern Il2CppType t764_0_0_0;
static ParameterInfo t853_m3591_ParameterInfos[] = 
{
	{"pattern", 0, 134218233, &EmptyCustomAttributesCache, &t764_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3591_MI = 
{
	".ctor", (methodPointerType)&m3591, &t853_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t853_m3591_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 583, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t836_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3592_MI = 
{
	"NewInstance", (methodPointerType)&m3592, &t853_TI, &t836_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 4, 0, false, false, 584, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3593_MI = 
{
	"get_GroupCount", (methodPointerType)&m3593, &t853_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, false, 585, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3594_MI = 
{
	"get_Gap", (methodPointerType)&m3594, &t853_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 8, 0, false, false, 586, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t853_m3595_ParameterInfos[] = 
{
	{"value", 0, 134218234, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3595_MI = 
{
	"set_Gap", (methodPointerType)&m3595, &t853_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t853_m3595_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 9, 1, false, false, 587, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t721_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3596_MI = 
{
	"get_Mapping", (methodPointerType)&m3596, &t853_TI, &t721_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 5, 0, false, false, 588, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t721_0_0_0;
extern Il2CppType t721_0_0_0;
static ParameterInfo t853_m3597_ParameterInfos[] = 
{
	{"value", 0, 134218235, &EmptyCustomAttributesCache, &t721_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3597_MI = 
{
	"set_Mapping", (methodPointerType)&m3597, &t853_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t853_m3597_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 6, 1, false, false, 589, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t446_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3598_MI = 
{
	"get_NamesMapping", (methodPointerType)&m3598, &t853_TI, &t446_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 10, 0, false, false, 590, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t446_0_0_0;
extern Il2CppType t446_0_0_0;
static ParameterInfo t853_m3599_ParameterInfos[] = 
{
	{"value", 0, 134218236, &EmptyCustomAttributesCache, &t446_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3599_MI = 
{
	"set_NamesMapping", (methodPointerType)&m3599, &t853_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t853_m3599_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 11, 1, false, false, 591, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t853_MIs[] =
{
	&m3591_MI,
	&m3592_MI,
	&m3593_MI,
	&m3594_MI,
	&m3595_MI,
	&m3596_MI,
	&m3597_MI,
	&m3598_MI,
	&m3599_MI,
	NULL
};
static MethodInfo* t853_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m3592_MI,
	&m3596_MI,
	&m3597_MI,
	&m3593_MI,
	&m3594_MI,
	&m3595_MI,
	&m3598_MI,
	&m3599_MI,
};
extern TypeInfo t840_TI;
static TypeInfo* t853_ITIs[] = 
{
	&t840_TI,
};
static Il2CppInterfaceOffsetPair t853_IOs[] = 
{
	{ &t840_TI, 4},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t853_0_0_0;
extern Il2CppType t853_1_0_0;
struct t853;
TypeInfo t853_TI = 
{
	&g_System_dll_Image, NULL, "InterpreterFactory", "System.Text.RegularExpressions", t853_MIs, t853_PIs, t853_FIs, NULL, &t29_TI, NULL, NULL, &t853_TI, t853_ITIs, t853_VT, &EmptyCustomAttributesCache, &t853_TI, &t853_0_0_0, &t853_1_0_0, t853_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t853), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 9, 4, 4, 0, 0, 12, 1, 1};
#include "t854.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t854_TI;
#include "t854MD.h"



// Metadata Definition System.Text.RegularExpressions.PatternCompiler/PatternLinkStack/Link
extern Il2CppType t44_0_0_6;
FieldInfo t854_f0_FieldInfo = 
{
	"base_addr", &t44_0_0_6, &t854_TI, offsetof(t854, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_6;
FieldInfo t854_f1_FieldInfo = 
{
	"offset_addr", &t44_0_0_6, &t854_TI, offsetof(t854, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t854_FIs[] =
{
	&t854_f0_FieldInfo,
	&t854_f1_FieldInfo,
	NULL
};
static MethodInfo* t854_MIs[] =
{
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
static MethodInfo* t854_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t854_0_0_0;
extern Il2CppType t854_1_0_0;
extern TypeInfo t110_TI;
extern TypeInfo t855_TI;
TypeInfo t854_TI = 
{
	&g_System_dll_Image, NULL, "Link", "", t854_MIs, NULL, t854_FIs, NULL, &t110_TI, NULL, &t855_TI, &t854_TI, NULL, t854_VT, &EmptyCustomAttributesCache, &t854_TI, &t854_0_0_0, &t854_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t854)+ sizeof (Il2CppObject), 0, sizeof(t854 ), 0, 0, -1, 1048843, 0, true, false, false, false, false, false, false, false, false, false, true, true, 0, 0, 2, 0, 0, 4, 0, 0};
#include "t855.h"
#ifndef _MSC_VER
#else
#endif
#include "t855MD.h"

#include "t856MD.h"
extern MethodInfo m3647_MI;


extern MethodInfo m3600_MI;
 void m3600 (t855 * __this, MethodInfo* method){
	{
		m3647(__this, &m3647_MI);
		return;
	}
}
extern MethodInfo m3601_MI;
 void m3601 (t855 * __this, int32_t p0, MethodInfo* method){
	{
		t854 * L_0 = &(__this->f1);
		L_0->f0 = p0;
		return;
	}
}
extern MethodInfo m3602_MI;
 int32_t m3602 (t855 * __this, MethodInfo* method){
	{
		t854 * L_0 = &(__this->f1);
		int32_t L_1 = (L_0->f1);
		return L_1;
	}
}
extern MethodInfo m3603_MI;
 void m3603 (t855 * __this, int32_t p0, MethodInfo* method){
	{
		t854 * L_0 = &(__this->f1);
		L_0->f1 = p0;
		return;
	}
}
extern MethodInfo m3604_MI;
 int32_t m3604 (t855 * __this, int32_t p0, MethodInfo* method){
	{
		t854 * L_0 = &(__this->f1);
		int32_t L_1 = (L_0->f0);
		return ((int32_t)(p0-L_1));
	}
}
extern MethodInfo m3605_MI;
 t29 * m3605 (t855 * __this, MethodInfo* method){
	{
		t854  L_0 = (__this->f1);
		t854  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t854_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m3606_MI;
 void m3606 (t855 * __this, t29 * p0, MethodInfo* method){
	{
		__this->f1 = ((*(t854 *)((t854 *)UnBox (p0, InitializedTypeInfo(&t854_TI)))));
		return;
	}
}
// Metadata Definition System.Text.RegularExpressions.PatternCompiler/PatternLinkStack
extern Il2CppType t854_0_0_1;
FieldInfo t855_f1_FieldInfo = 
{
	"link", &t854_0_0_1, &t855_TI, offsetof(t855, f1), &EmptyCustomAttributesCache};
static FieldInfo* t855_FIs[] =
{
	&t855_f1_FieldInfo,
	NULL
};
static PropertyInfo t855____BaseAddress_PropertyInfo = 
{
	&t855_TI, "BaseAddress", NULL, &m3601_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t855____OffsetAddress_PropertyInfo = 
{
	&t855_TI, "OffsetAddress", &m3602_MI, &m3603_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t855_PIs[] =
{
	&t855____BaseAddress_PropertyInfo,
	&t855____OffsetAddress_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3600_MI = 
{
	".ctor", (methodPointerType)&m3600, &t855_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 632, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t855_m3601_ParameterInfos[] = 
{
	{"value", 0, 134218307, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3601_MI = 
{
	"set_BaseAddress", (methodPointerType)&m3601, &t855_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t855_m3601_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 633, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3602_MI = 
{
	"get_OffsetAddress", (methodPointerType)&m3602, &t855_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 634, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t855_m3603_ParameterInfos[] = 
{
	{"value", 0, 134218308, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3603_MI = 
{
	"set_OffsetAddress", (methodPointerType)&m3603, &t855_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t855_m3603_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 635, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t855_m3604_ParameterInfos[] = 
{
	{"target_addr", 0, 134218309, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3604_MI = 
{
	"GetOffset", (methodPointerType)&m3604, &t855_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t855_m3604_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 636, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3605_MI = 
{
	"GetCurrent", (methodPointerType)&m3605, &t855_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 196, 0, 4, 0, false, false, 637, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t855_m3606_ParameterInfos[] = 
{
	{"l", 0, 134218310, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3606_MI = 
{
	"SetCurrent", (methodPointerType)&m3606, &t855_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t855_m3606_ParameterInfos, &EmptyCustomAttributesCache, 196, 0, 5, 1, false, false, 638, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t855_MIs[] =
{
	&m3600_MI,
	&m3601_MI,
	&m3602_MI,
	&m3603_MI,
	&m3604_MI,
	&m3605_MI,
	&m3606_MI,
	NULL
};
extern TypeInfo t854_TI;
static TypeInfo* t855_TI__nestedTypes[2] =
{
	&t854_TI,
	NULL
};
static MethodInfo* t855_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m3605_MI,
	&m3606_MI,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t855_0_0_0;
extern Il2CppType t855_1_0_0;
extern TypeInfo t856_TI;
struct t855;
extern TypeInfo t857_TI;
TypeInfo t855_TI = 
{
	&g_System_dll_Image, NULL, "PatternLinkStack", "", t855_MIs, t855_PIs, t855_FIs, NULL, &t856_TI, t855_TI__nestedTypes, &t857_TI, &t855_TI, NULL, t855_VT, &EmptyCustomAttributesCache, &t855_TI, &t855_0_0_0, &t855_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t855), 0, -1, 0, 0, -1, 1048579, 0, false, false, false, false, false, false, false, false, false, false, false, false, 7, 2, 1, 0, 1, 6, 0, 0};
#include "t857.h"
#ifndef _MSC_VER
#else
#endif
#include "t857MD.h"

#include "t731.h"
#include "t843.h"
#include "t844.h"
#include "t344.h"
extern TypeInfo t731_TI;
extern TypeInfo t44_TI;
extern TypeInfo t764_TI;
extern TypeInfo t21_TI;
extern TypeInfo t20_TI;
#include "t731MD.h"
#include "t858MD.h"
extern MethodInfo m3976_MI;
extern MethodInfo m3980_MI;
extern MethodInfo m4254_MI;
extern MethodInfo m3641_MI;
extern MethodInfo m3643_MI;
extern MethodInfo m3640_MI;
extern MethodInfo m3642_MI;
extern MethodInfo m1809_MI;
extern MethodInfo m4255_MI;
extern MethodInfo m4256_MI;
extern MethodInfo m1715_MI;
extern MethodInfo m4257_MI;
extern MethodInfo m1741_MI;
extern MethodInfo m3645_MI;
extern MethodInfo m3646_MI;
extern MethodInfo m3612_MI;
extern MethodInfo m3637_MI;
extern MethodInfo m3644_MI;
extern MethodInfo m4258_MI;
extern MethodInfo m3649_MI;
extern MethodInfo m3608_MI;
extern MethodInfo m3991_MI;
extern MethodInfo m3648_MI;


extern MethodInfo m3607_MI;
 void m3607 (t857 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t731_TI));
		t731 * L_0 = (t731 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t731_TI));
		m3980(L_0, &m3980_MI);
		__this->f0 = L_0;
		return;
	}
}
 uint16_t m3608 (t29 * __this, uint16_t p0, uint16_t p1, MethodInfo* method){
	{
		return (((uint16_t)((int32_t)((int32_t)p0|(int32_t)((int32_t)((int32_t)p1&(int32_t)((int32_t)65280)))))));
	}
}
extern MethodInfo m3609_MI;
 t29 * m3609 (t857 * __this, MethodInfo* method){
	t764* V_0 = {0};
	{
		t731 * L_0 = (__this->f0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m3976_MI, L_0);
		V_0 = ((t764*)SZArrayNew(InitializedTypeInfo(&t764_TI), L_1));
		t731 * L_2 = (__this->f0);
		VirtActionInvoker1< t20 * >::Invoke(&m4254_MI, L_2, (t20 *)(t20 *)V_0);
		t853 * L_3 = (t853 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t853_TI));
		m3591(L_3, V_0, &m3591_MI);
		return L_3;
	}
}
extern MethodInfo m3610_MI;
 void m3610 (t857 * __this, MethodInfo* method){
	{
		m3641(__this, 0, &m3641_MI);
		return;
	}
}
extern MethodInfo m3611_MI;
 void m3611 (t857 * __this, MethodInfo* method){
	{
		m3641(__this, 1, &m3641_MI);
		return;
	}
}
 void m3612 (t857 * __this, int32_t p0, MethodInfo* method){
	uint32_t V_0 = 0;
	{
		V_0 = p0;
		m3643(__this, (((uint16_t)((int32_t)((int32_t)V_0&(int32_t)((int32_t)65535))))), &m3643_MI);
		m3643(__this, (((uint16_t)((int32_t)((uint32_t)V_0>>((int32_t)16))))), &m3643_MI);
		return;
	}
}
extern MethodInfo m3613_MI;
 void m3613 (t857 * __this, uint16_t p0, bool p1, bool p2, bool p3, MethodInfo* method){
	{
		uint16_t L_0 = m3640(NULL, p1, p2, p3, 0, &m3640_MI);
		m3642(__this, 5, L_0, &m3642_MI);
		if (!p2)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t194_TI));
		uint16_t L_1 = m1809(NULL, p0, &m1809_MI);
		p0 = L_1;
	}

IL_001f:
	{
		m3643(__this, p0, &m3643_MI);
		return;
	}
}
extern MethodInfo m3614_MI;
 void m3614 (t857 * __this, uint16_t p0, bool p1, bool p2, MethodInfo* method){
	{
		uint16_t L_0 = m3640(NULL, p1, 0, p2, 0, &m3640_MI);
		m3642(__this, 6, L_0, &m3642_MI);
		m3643(__this, p0, &m3643_MI);
		return;
	}
}
extern MethodInfo m3615_MI;
 void m3615 (t857 * __this, uint16_t p0, bool p1, bool p2, MethodInfo* method){
	{
		uint16_t L_0 = m3640(NULL, p1, 0, p2, 0, &m3640_MI);
		m3642(__this, 7, L_0, &m3642_MI);
		m3643(__this, p0, &m3643_MI);
		return;
	}
}
extern MethodInfo m3616_MI;
 void m3616 (t857 * __this, uint16_t p0, uint16_t p1, bool p2, bool p3, bool p4, MethodInfo* method){
	{
		uint16_t L_0 = m3640(NULL, p2, p3, p4, 0, &m3640_MI);
		m3642(__this, 8, L_0, &m3642_MI);
		m3643(__this, p0, &m3643_MI);
		m3643(__this, p1, &m3643_MI);
		return;
	}
}
extern MethodInfo m3617_MI;
 void m3617 (t857 * __this, uint16_t p0, t858 * p1, bool p2, bool p3, bool p4, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	uint16_t V_2 = 0;
	int32_t V_3 = 0;
	{
		uint16_t L_0 = m3640(NULL, p2, p3, p4, 0, &m3640_MI);
		m3642(__this, ((int32_t)9), L_0, &m3642_MI);
		m3643(__this, p0, &m3643_MI);
		int32_t L_1 = m4255(p1, &m4255_MI);
		V_0 = ((int32_t)((int32_t)((int32_t)(L_1+((int32_t)15)))>>(int32_t)4));
		m3643(__this, (((uint16_t)V_0)), &m3643_MI);
		V_1 = 0;
		goto IL_007d;
	}

IL_0035:
	{
		V_2 = 0;
		V_3 = 0;
		goto IL_006e;
	}

IL_003e:
	{
		int32_t L_2 = m4255(p1, &m4255_MI);
		if ((((int32_t)V_1) < ((int32_t)L_2)))
		{
			goto IL_004f;
		}
	}
	{
		goto IL_0076;
	}

IL_004f:
	{
		int32_t L_3 = V_1;
		V_1 = ((int32_t)(L_3+1));
		bool L_4 = m4256(p1, L_3, &m4256_MI);
		if (!L_4)
		{
			goto IL_006a;
		}
	}
	{
		V_2 = (((uint16_t)((int32_t)((int32_t)V_2|(int32_t)(((uint16_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)V_3&(int32_t)((int32_t)31)))))))))));
	}

IL_006a:
	{
		V_3 = ((int32_t)(V_3+1));
	}

IL_006e:
	{
		if ((((int32_t)V_3) < ((int32_t)((int32_t)16))))
		{
			goto IL_003e;
		}
	}

IL_0076:
	{
		m3643(__this, V_2, &m3643_MI);
	}

IL_007d:
	{
		int32_t L_5 = V_0;
		V_0 = ((int32_t)(L_5-1));
		if (L_5)
		{
			goto IL_0035;
		}
	}
	{
		return;
	}
}
extern MethodInfo m3618_MI;
 void m3618 (t857 * __this, t7* p0, bool p1, bool p2, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		uint16_t L_0 = m3640(NULL, 0, p1, p2, 0, &m3640_MI);
		m3642(__this, 3, L_0, &m3642_MI);
		int32_t L_1 = m1715(p0, &m1715_MI);
		V_0 = L_1;
		m3643(__this, (((uint16_t)V_0)), &m3643_MI);
		if (!p1)
		{
			goto IL_002d;
		}
	}
	{
		t7* L_2 = m4257(p0, &m4257_MI);
		p0 = L_2;
	}

IL_002d:
	{
		V_1 = 0;
		goto IL_0045;
	}

IL_0034:
	{
		uint16_t L_3 = m1741(p0, V_1, &m1741_MI);
		m3643(__this, L_3, &m3643_MI);
		V_1 = ((int32_t)(V_1+1));
	}

IL_0045:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0034;
		}
	}
	{
		return;
	}
}
extern MethodInfo m3619_MI;
 void m3619 (t857 * __this, uint16_t p0, MethodInfo* method){
	{
		m3642(__this, 2, 0, &m3642_MI);
		m3643(__this, p0, &m3643_MI);
		return;
	}
}
extern MethodInfo m3620_MI;
 void m3620 (t857 * __this, int32_t p0, MethodInfo* method){
	{
		m3641(__this, ((int32_t)11), &m3641_MI);
		m3643(__this, (((uint16_t)p0)), &m3643_MI);
		return;
	}
}
extern MethodInfo m3621_MI;
 void m3621 (t857 * __this, int32_t p0, MethodInfo* method){
	{
		m3641(__this, ((int32_t)12), &m3641_MI);
		m3643(__this, (((uint16_t)p0)), &m3643_MI);
		return;
	}
}
extern MethodInfo m3622_MI;
 void m3622 (t857 * __this, int32_t p0, int32_t p1, bool p2, t852 * p3, MethodInfo* method){
	t857 * G_B2_0 = {0};
	t857 * G_B1_0 = {0};
	int32_t G_B3_0 = 0;
	t857 * G_B3_1 = {0};
	{
		m3645(__this, p3, &m3645_MI);
		m3641(__this, ((int32_t)14), &m3641_MI);
		m3643(__this, (((uint16_t)p0)), &m3643_MI);
		m3643(__this, (((uint16_t)p1)), &m3643_MI);
		G_B1_0 = __this;
		if (!p2)
		{
			G_B2_0 = __this;
			goto IL_002d;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		goto IL_002e;
	}

IL_002d:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_002e:
	{
		m3643(G_B3_1, (((uint16_t)G_B3_0)), &m3643_MI);
		m3646(__this, p3, &m3646_MI);
		return;
	}
}
extern MethodInfo m3623_MI;
 void m3623 (t857 * __this, MethodInfo* method){
	{
		m3641(__this, ((int32_t)13), &m3641_MI);
		return;
	}
}
extern MethodInfo m3624_MI;
 void m3624 (t857 * __this, int32_t p0, bool p1, bool p2, MethodInfo* method){
	{
		uint16_t L_0 = m3640(NULL, 0, p1, p2, 0, &m3640_MI);
		m3642(__this, 4, L_0, &m3642_MI);
		m3643(__this, (((uint16_t)p0)), &m3643_MI);
		return;
	}
}
extern MethodInfo m3625_MI;
 void m3625 (t857 * __this, int32_t p0, t852 * p1, MethodInfo* method){
	{
		m3645(__this, p1, &m3645_MI);
		m3641(__this, ((int32_t)15), &m3641_MI);
		m3646(__this, p1, &m3646_MI);
		m3643(__this, (((uint16_t)p0)), &m3643_MI);
		return;
	}
}
extern MethodInfo m3626_MI;
 void m3626 (t857 * __this, t852 * p0, MethodInfo* method){
	{
		m3645(__this, p0, &m3645_MI);
		m3641(__this, ((int32_t)16), &m3641_MI);
		m3646(__this, p0, &m3646_MI);
		return;
	}
}
extern MethodInfo m3627_MI;
 void m3627 (t857 * __this, t852 * p0, t852 * p1, MethodInfo* method){
	{
		m3645(__this, p0, &m3645_MI);
		m3645(__this, p1, &m3645_MI);
		m3641(__this, ((int32_t)17), &m3641_MI);
		m3646(__this, p0, &m3646_MI);
		m3646(__this, p1, &m3646_MI);
		return;
	}
}
extern MethodInfo m3628_MI;
 void m3628 (t857 * __this, t852 * p0, MethodInfo* method){
	{
		m3645(__this, p0, &m3645_MI);
		m3642(__this, ((int32_t)18), 0, &m3642_MI);
		m3646(__this, p0, &m3646_MI);
		return;
	}
}
extern MethodInfo m3629_MI;
 void m3629 (t857 * __this, t852 * p0, MethodInfo* method){
	{
		m3645(__this, p0, &m3645_MI);
		m3642(__this, ((int32_t)19), 0, &m3642_MI);
		m3646(__this, p0, &m3646_MI);
		return;
	}
}
extern MethodInfo m3630_MI;
 void m3630 (t857 * __this, int32_t p0, int32_t p1, bool p2, t852 * p3, MethodInfo* method){
	{
		m3645(__this, p3, &m3645_MI);
		uint16_t L_0 = m3640(NULL, 0, 0, 0, p2, &m3640_MI);
		m3642(__this, ((int32_t)20), L_0, &m3642_MI);
		m3646(__this, p3, &m3646_MI);
		m3612(__this, p0, &m3612_MI);
		m3612(__this, p1, &m3612_MI);
		return;
	}
}
extern MethodInfo m3631_MI;
 void m3631 (t857 * __this, t852 * p0, MethodInfo* method){
	{
		VirtActionInvoker1< t852 * >::Invoke(&m3637_MI, __this, p0);
		m3641(__this, ((int32_t)21), &m3641_MI);
		return;
	}
}
extern MethodInfo m3632_MI;
 void m3632 (t857 * __this, int32_t p0, int32_t p1, bool p2, t852 * p3, MethodInfo* method){
	{
		m3645(__this, p3, &m3645_MI);
		uint16_t L_0 = m3640(NULL, 0, 0, 0, p2, &m3640_MI);
		m3642(__this, ((int32_t)22), L_0, &m3642_MI);
		m3646(__this, p3, &m3646_MI);
		m3612(__this, p0, &m3612_MI);
		m3612(__this, p1, &m3612_MI);
		return;
	}
}
extern MethodInfo m3633_MI;
 void m3633 (t857 * __this, t852 * p0, MethodInfo* method){
	{
		m3645(__this, p0, &m3645_MI);
		m3641(__this, ((int32_t)10), &m3641_MI);
		m3646(__this, p0, &m3646_MI);
		return;
	}
}
extern MethodInfo m3634_MI;
 void m3634 (t857 * __this, bool p0, int32_t p1, t852 * p2, MethodInfo* method){
	{
		m3645(__this, p2, &m3645_MI);
		uint16_t L_0 = m3640(NULL, 0, 0, p0, 0, &m3640_MI);
		m3642(__this, ((int32_t)23), L_0, &m3642_MI);
		m3646(__this, p2, &m3646_MI);
		m3643(__this, (((uint16_t)p1)), &m3643_MI);
		return;
	}
}
extern MethodInfo m3635_MI;
 void m3635 (t857 * __this, int32_t p0, int32_t p1, int32_t p2, MethodInfo* method){
	{
		m3641(__this, ((int32_t)24), &m3641_MI);
		m3612(__this, p0, &m3612_MI);
		m3612(__this, p1, &m3612_MI);
		m3612(__this, p2, &m3612_MI);
		return;
	}
}
extern MethodInfo m3636_MI;
 t852 * m3636 (t857 * __this, MethodInfo* method){
	{
		t855 * L_0 = (t855 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t855_TI));
		m3600(L_0, &m3600_MI);
		return L_0;
	}
}
 void m3637 (t857 * __this, t852 * p0, MethodInfo* method){
	t855 * V_0 = {0};
	{
		V_0 = ((t855 *)Castclass(p0, InitializedTypeInfo(&t855_TI)));
		goto IL_002f;
	}

IL_000c:
	{
		t731 * L_0 = (__this->f0);
		int32_t L_1 = m3602(V_0, &m3602_MI);
		int32_t L_2 = m3644(__this, &m3644_MI);
		int32_t L_3 = m3604(V_0, L_2, &m3604_MI);
		uint16_t L_4 = (((uint16_t)L_3));
		t29 * L_5 = Box(InitializedTypeInfo(&t626_TI), &L_4);
		VirtActionInvoker2< int32_t, t29 * >::Invoke(&m4258_MI, L_0, L_1, L_5);
	}

IL_002f:
	{
		bool L_6 = m3649(V_0, &m3649_MI);
		if (L_6)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}
}
extern MethodInfo m3638_MI;
 void m3638 (t857 * __this, MethodInfo* method){
	{
		return;
	}
}
extern MethodInfo m3639_MI;
 void m3639 (t857 * __this, MethodInfo* method){
	{
		return;
	}
}
 uint16_t m3640 (t29 * __this, bool p0, bool p1, bool p2, bool p3, MethodInfo* method){
	uint16_t V_0 = {0};
	{
		V_0 = 0;
		if (!p0)
		{
			goto IL_0011;
		}
	}
	{
		V_0 = (((uint16_t)((int32_t)((int32_t)V_0|(int32_t)((int32_t)256)))));
	}

IL_0011:
	{
		if (!p1)
		{
			goto IL_0020;
		}
	}
	{
		V_0 = (((uint16_t)((int32_t)((int32_t)V_0|(int32_t)((int32_t)512)))));
	}

IL_0020:
	{
		if (!p2)
		{
			goto IL_002f;
		}
	}
	{
		V_0 = (((uint16_t)((int32_t)((int32_t)V_0|(int32_t)((int32_t)1024)))));
	}

IL_002f:
	{
		if (!p3)
		{
			goto IL_003e;
		}
	}
	{
		V_0 = (((uint16_t)((int32_t)((int32_t)V_0|(int32_t)((int32_t)2048)))));
	}

IL_003e:
	{
		return V_0;
	}
}
 void m3641 (t857 * __this, uint16_t p0, MethodInfo* method){
	{
		m3642(__this, p0, 0, &m3642_MI);
		return;
	}
}
 void m3642 (t857 * __this, uint16_t p0, uint16_t p1, MethodInfo* method){
	{
		uint16_t L_0 = m3608(NULL, p0, p1, &m3608_MI);
		m3643(__this, L_0, &m3643_MI);
		return;
	}
}
 void m3643 (t857 * __this, uint16_t p0, MethodInfo* method){
	{
		t731 * L_0 = (__this->f0);
		uint16_t L_1 = p0;
		t29 * L_2 = Box(InitializedTypeInfo(&t626_TI), &L_1);
		VirtFuncInvoker1< int32_t, t29 * >::Invoke(&m3991_MI, L_0, L_2);
		return;
	}
}
 int32_t m3644 (t857 * __this, MethodInfo* method){
	{
		t731 * L_0 = (__this->f0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m3976_MI, L_0);
		return L_1;
	}
}
 void m3645 (t857 * __this, t852 * p0, MethodInfo* method){
	t855 * V_0 = {0};
	{
		V_0 = ((t855 *)Castclass(p0, InitializedTypeInfo(&t855_TI)));
		int32_t L_0 = m3644(__this, &m3644_MI);
		m3601(V_0, L_0, &m3601_MI);
		return;
	}
}
 void m3646 (t857 * __this, t852 * p0, MethodInfo* method){
	t855 * V_0 = {0};
	{
		V_0 = ((t855 *)Castclass(p0, InitializedTypeInfo(&t855_TI)));
		int32_t L_0 = m3644(__this, &m3644_MI);
		m3603(V_0, L_0, &m3603_MI);
		m3643(__this, 0, &m3643_MI);
		m3648(V_0, &m3648_MI);
		return;
	}
}
// Metadata Definition System.Text.RegularExpressions.PatternCompiler
extern Il2CppType t731_0_0_1;
FieldInfo t857_f0_FieldInfo = 
{
	"pgm", &t731_0_0_1, &t857_TI, offsetof(t857, f0), &EmptyCustomAttributesCache};
static FieldInfo* t857_FIs[] =
{
	&t857_f0_FieldInfo,
	NULL
};
static PropertyInfo t857____CurrentAddress_PropertyInfo = 
{
	&t857_TI, "CurrentAddress", &m3644_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t857_PIs[] =
{
	&t857____CurrentAddress_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3607_MI = 
{
	".ctor", (methodPointerType)&m3607, &t857_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 592, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t843_0_0_0;
extern Il2CppType t843_0_0_0;
extern Il2CppType t844_0_0_0;
extern Il2CppType t844_0_0_0;
static ParameterInfo t857_m3608_ParameterInfos[] = 
{
	{"op", 0, 134218237, &EmptyCustomAttributesCache, &t843_0_0_0},
	{"flags", 1, 134218238, &EmptyCustomAttributesCache, &t844_0_0_0},
};
extern Il2CppType t626_0_0_0;
extern void* RuntimeInvoker_t626_t626_t626 (MethodInfo* method, void* obj, void** args);
MethodInfo m3608_MI = 
{
	"EncodeOp", (methodPointerType)&m3608, &t857_TI, &t626_0_0_0, RuntimeInvoker_t626_t626_t626, t857_m3608_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 2, false, false, 593, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t840_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3609_MI = 
{
	"GetMachineFactory", (methodPointerType)&m3609, &t857_TI, &t840_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 4, 0, false, false, 594, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3610_MI = 
{
	"EmitFalse", (methodPointerType)&m3610, &t857_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, false, 595, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3611_MI = 
{
	"EmitTrue", (methodPointerType)&m3611, &t857_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, false, 596, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t857_m3612_ParameterInfos[] = 
{
	{"count", 0, 134218239, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3612_MI = 
{
	"EmitCount", (methodPointerType)&m3612, &t857_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t857_m3612_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 597, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t194_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t857_m3613_ParameterInfos[] = 
{
	{"c", 0, 134218240, &EmptyCustomAttributesCache, &t194_0_0_0},
	{"negate", 1, 134218241, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"ignore", 2, 134218242, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"reverse", 3, 134218243, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t372_t297_t297_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3613_MI = 
{
	"EmitCharacter", (methodPointerType)&m3613, &t857_TI, &t21_0_0_0, RuntimeInvoker_t21_t372_t297_t297_t297, t857_m3613_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 7, 4, false, false, 598, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t849_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t857_m3614_ParameterInfos[] = 
{
	{"cat", 0, 134218244, &EmptyCustomAttributesCache, &t849_0_0_0},
	{"negate", 1, 134218245, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"reverse", 2, 134218246, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t626_t297_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3614_MI = 
{
	"EmitCategory", (methodPointerType)&m3614, &t857_TI, &t21_0_0_0, RuntimeInvoker_t21_t626_t297_t297, t857_m3614_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 8, 3, false, false, 599, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t849_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t857_m3615_ParameterInfos[] = 
{
	{"cat", 0, 134218247, &EmptyCustomAttributesCache, &t849_0_0_0},
	{"negate", 1, 134218248, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"reverse", 2, 134218249, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t626_t297_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3615_MI = 
{
	"EmitNotCategory", (methodPointerType)&m3615, &t857_TI, &t21_0_0_0, RuntimeInvoker_t21_t626_t297_t297, t857_m3615_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 9, 3, false, false, 600, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t194_0_0_0;
extern Il2CppType t194_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t857_m3616_ParameterInfos[] = 
{
	{"lo", 0, 134218250, &EmptyCustomAttributesCache, &t194_0_0_0},
	{"hi", 1, 134218251, &EmptyCustomAttributesCache, &t194_0_0_0},
	{"negate", 2, 134218252, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"ignore", 3, 134218253, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"reverse", 4, 134218254, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t372_t372_t297_t297_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3616_MI = 
{
	"EmitRange", (methodPointerType)&m3616, &t857_TI, &t21_0_0_0, RuntimeInvoker_t21_t372_t372_t297_t297_t297, t857_m3616_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 10, 5, false, false, 601, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t194_0_0_0;
extern Il2CppType t858_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t857_m3617_ParameterInfos[] = 
{
	{"lo", 0, 134218255, &EmptyCustomAttributesCache, &t194_0_0_0},
	{"set", 1, 134218256, &EmptyCustomAttributesCache, &t858_0_0_0},
	{"negate", 2, 134218257, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"ignore", 3, 134218258, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"reverse", 4, 134218259, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t372_t29_t297_t297_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3617_MI = 
{
	"EmitSet", (methodPointerType)&m3617, &t857_TI, &t21_0_0_0, RuntimeInvoker_t21_t372_t29_t297_t297_t297, t857_m3617_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 11, 5, false, false, 602, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t857_m3618_ParameterInfos[] = 
{
	{"str", 0, 134218260, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"ignore", 1, 134218261, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"reverse", 2, 134218262, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t297_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3618_MI = 
{
	"EmitString", (methodPointerType)&m3618, &t857_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t297_t297, t857_m3618_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 12, 3, false, false, 603, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t845_0_0_0;
static ParameterInfo t857_m3619_ParameterInfos[] = 
{
	{"pos", 0, 134218263, &EmptyCustomAttributesCache, &t845_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t626 (MethodInfo* method, void* obj, void** args);
MethodInfo m3619_MI = 
{
	"EmitPosition", (methodPointerType)&m3619, &t857_TI, &t21_0_0_0, RuntimeInvoker_t21_t626, t857_m3619_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 13, 1, false, false, 604, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t857_m3620_ParameterInfos[] = 
{
	{"gid", 0, 134218264, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3620_MI = 
{
	"EmitOpen", (methodPointerType)&m3620, &t857_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t857_m3620_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 14, 1, false, false, 605, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t857_m3621_ParameterInfos[] = 
{
	{"gid", 0, 134218265, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3621_MI = 
{
	"EmitClose", (methodPointerType)&m3621, &t857_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t857_m3621_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 15, 1, false, false, 606, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t852_0_0_0;
static ParameterInfo t857_m3622_ParameterInfos[] = 
{
	{"gid", 0, 134218266, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"balance", 1, 134218267, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"capture", 2, 134218268, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"tail", 3, 134218269, &EmptyCustomAttributesCache, &t852_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44_t297_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3622_MI = 
{
	"EmitBalanceStart", (methodPointerType)&m3622, &t857_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44_t297_t29, t857_m3622_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 16, 4, false, false, 607, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3623_MI = 
{
	"EmitBalance", (methodPointerType)&m3623, &t857_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 17, 0, false, false, 608, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t857_m3624_ParameterInfos[] = 
{
	{"gid", 0, 134218270, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"ignore", 1, 134218271, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"reverse", 2, 134218272, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t297_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3624_MI = 
{
	"EmitReference", (methodPointerType)&m3624, &t857_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t297_t297, t857_m3624_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 18, 3, false, false, 609, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t852_0_0_0;
static ParameterInfo t857_m3625_ParameterInfos[] = 
{
	{"gid", 0, 134218273, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"tail", 1, 134218274, &EmptyCustomAttributesCache, &t852_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3625_MI = 
{
	"EmitIfDefined", (methodPointerType)&m3625, &t857_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t857_m3625_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 19, 2, false, false, 610, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t852_0_0_0;
static ParameterInfo t857_m3626_ParameterInfos[] = 
{
	{"tail", 0, 134218275, &EmptyCustomAttributesCache, &t852_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3626_MI = 
{
	"EmitSub", (methodPointerType)&m3626, &t857_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t857_m3626_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 20, 1, false, false, 611, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t852_0_0_0;
extern Il2CppType t852_0_0_0;
static ParameterInfo t857_m3627_ParameterInfos[] = 
{
	{"yes", 0, 134218276, &EmptyCustomAttributesCache, &t852_0_0_0},
	{"tail", 1, 134218277, &EmptyCustomAttributesCache, &t852_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3627_MI = 
{
	"EmitTest", (methodPointerType)&m3627, &t857_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t857_m3627_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 21, 2, false, false, 612, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t852_0_0_0;
static ParameterInfo t857_m3628_ParameterInfos[] = 
{
	{"next", 0, 134218278, &EmptyCustomAttributesCache, &t852_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3628_MI = 
{
	"EmitBranch", (methodPointerType)&m3628, &t857_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t857_m3628_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, false, 613, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t852_0_0_0;
static ParameterInfo t857_m3629_ParameterInfos[] = 
{
	{"target", 0, 134218279, &EmptyCustomAttributesCache, &t852_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3629_MI = 
{
	"EmitJump", (methodPointerType)&m3629, &t857_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t857_m3629_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 23, 1, false, false, 614, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t852_0_0_0;
static ParameterInfo t857_m3630_ParameterInfos[] = 
{
	{"min", 0, 134218280, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"max", 1, 134218281, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"lazy", 2, 134218282, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"until", 3, 134218283, &EmptyCustomAttributesCache, &t852_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44_t297_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3630_MI = 
{
	"EmitRepeat", (methodPointerType)&m3630, &t857_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44_t297_t29, t857_m3630_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 4, false, false, 615, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t852_0_0_0;
static ParameterInfo t857_m3631_ParameterInfos[] = 
{
	{"repeat", 0, 134218284, &EmptyCustomAttributesCache, &t852_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3631_MI = 
{
	"EmitUntil", (methodPointerType)&m3631, &t857_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t857_m3631_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 1, false, false, 616, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t852_0_0_0;
static ParameterInfo t857_m3632_ParameterInfos[] = 
{
	{"min", 0, 134218285, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"max", 1, 134218286, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"lazy", 2, 134218287, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"tail", 3, 134218288, &EmptyCustomAttributesCache, &t852_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44_t297_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3632_MI = 
{
	"EmitFastRepeat", (methodPointerType)&m3632, &t857_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44_t297_t29, t857_m3632_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 4, false, false, 617, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t852_0_0_0;
static ParameterInfo t857_m3633_ParameterInfos[] = 
{
	{"tail", 0, 134218289, &EmptyCustomAttributesCache, &t852_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3633_MI = 
{
	"EmitIn", (methodPointerType)&m3633, &t857_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t857_m3633_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, false, 618, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t852_0_0_0;
static ParameterInfo t857_m3634_ParameterInfos[] = 
{
	{"reverse", 0, 134218290, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"offset", 1, 134218291, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"tail", 2, 134218292, &EmptyCustomAttributesCache, &t852_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3634_MI = 
{
	"EmitAnchor", (methodPointerType)&m3634, &t857_TI, &t21_0_0_0, RuntimeInvoker_t21_t297_t44_t29, t857_m3634_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 3, false, false, 619, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t857_m3635_ParameterInfos[] = 
{
	{"count", 0, 134218293, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"min", 1, 134218294, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"max", 2, 134218295, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3635_MI = 
{
	"EmitInfo", (methodPointerType)&m3635, &t857_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44_t44, t857_m3635_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 3, false, false, 620, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t852_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3636_MI = 
{
	"NewLink", (methodPointerType)&m3636, &t857_TI, &t852_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, false, 621, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t852_0_0_0;
static ParameterInfo t857_m3637_ParameterInfos[] = 
{
	{"lref", 0, 134218296, &EmptyCustomAttributesCache, &t852_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3637_MI = 
{
	"ResolveLink", (methodPointerType)&m3637, &t857_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t857_m3637_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 33, 1, false, false, 622, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3638_MI = 
{
	"EmitBranchEnd", (methodPointerType)&m3638, &t857_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 30, 0, false, false, 623, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3639_MI = 
{
	"EmitAlternationEnd", (methodPointerType)&m3639, &t857_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 31, 0, false, false, 624, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t857_m3640_ParameterInfos[] = 
{
	{"negate", 0, 134218297, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"ignore", 1, 134218298, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"reverse", 2, 134218299, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"lazy", 3, 134218300, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t844_0_0_0;
extern void* RuntimeInvoker_t844_t297_t297_t297_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3640_MI = 
{
	"MakeFlags", (methodPointerType)&m3640, &t857_TI, &t844_0_0_0, RuntimeInvoker_t844_t297_t297_t297_t297, t857_m3640_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 4, false, false, 625, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t843_0_0_0;
static ParameterInfo t857_m3641_ParameterInfos[] = 
{
	{"op", 0, 134218301, &EmptyCustomAttributesCache, &t843_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t626 (MethodInfo* method, void* obj, void** args);
MethodInfo m3641_MI = 
{
	"Emit", (methodPointerType)&m3641, &t857_TI, &t21_0_0_0, RuntimeInvoker_t21_t626, t857_m3641_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 626, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t843_0_0_0;
extern Il2CppType t844_0_0_0;
static ParameterInfo t857_m3642_ParameterInfos[] = 
{
	{"op", 0, 134218302, &EmptyCustomAttributesCache, &t843_0_0_0},
	{"flags", 1, 134218303, &EmptyCustomAttributesCache, &t844_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t626_t626 (MethodInfo* method, void* obj, void** args);
MethodInfo m3642_MI = 
{
	"Emit", (methodPointerType)&m3642, &t857_TI, &t21_0_0_0, RuntimeInvoker_t21_t626_t626, t857_m3642_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 627, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t626_0_0_0;
static ParameterInfo t857_m3643_ParameterInfos[] = 
{
	{"word", 0, 134218304, &EmptyCustomAttributesCache, &t626_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t372 (MethodInfo* method, void* obj, void** args);
MethodInfo m3643_MI = 
{
	"Emit", (methodPointerType)&m3643, &t857_TI, &t21_0_0_0, RuntimeInvoker_t21_t372, t857_m3643_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 628, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3644_MI = 
{
	"get_CurrentAddress", (methodPointerType)&m3644, &t857_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2177, 0, 255, 0, false, false, 629, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t852_0_0_0;
static ParameterInfo t857_m3645_ParameterInfos[] = 
{
	{"lref", 0, 134218305, &EmptyCustomAttributesCache, &t852_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3645_MI = 
{
	"BeginLink", (methodPointerType)&m3645, &t857_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t857_m3645_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 630, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t852_0_0_0;
static ParameterInfo t857_m3646_ParameterInfos[] = 
{
	{"lref", 0, 134218306, &EmptyCustomAttributesCache, &t852_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3646_MI = 
{
	"EmitLink", (methodPointerType)&m3646, &t857_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t857_m3646_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 631, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t857_MIs[] =
{
	&m3607_MI,
	&m3608_MI,
	&m3609_MI,
	&m3610_MI,
	&m3611_MI,
	&m3612_MI,
	&m3613_MI,
	&m3614_MI,
	&m3615_MI,
	&m3616_MI,
	&m3617_MI,
	&m3618_MI,
	&m3619_MI,
	&m3620_MI,
	&m3621_MI,
	&m3622_MI,
	&m3623_MI,
	&m3624_MI,
	&m3625_MI,
	&m3626_MI,
	&m3627_MI,
	&m3628_MI,
	&m3629_MI,
	&m3630_MI,
	&m3631_MI,
	&m3632_MI,
	&m3633_MI,
	&m3634_MI,
	&m3635_MI,
	&m3636_MI,
	&m3637_MI,
	&m3638_MI,
	&m3639_MI,
	&m3640_MI,
	&m3641_MI,
	&m3642_MI,
	&m3643_MI,
	&m3644_MI,
	&m3645_MI,
	&m3646_MI,
	NULL
};
extern TypeInfo t855_TI;
static TypeInfo* t857_TI__nestedTypes[2] =
{
	&t855_TI,
	NULL
};
static MethodInfo* t857_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m3609_MI,
	&m3610_MI,
	&m3611_MI,
	&m3613_MI,
	&m3614_MI,
	&m3615_MI,
	&m3616_MI,
	&m3617_MI,
	&m3618_MI,
	&m3619_MI,
	&m3620_MI,
	&m3621_MI,
	&m3622_MI,
	&m3623_MI,
	&m3624_MI,
	&m3625_MI,
	&m3626_MI,
	&m3627_MI,
	&m3628_MI,
	&m3629_MI,
	&m3630_MI,
	&m3631_MI,
	&m3633_MI,
	&m3635_MI,
	&m3632_MI,
	&m3634_MI,
	&m3638_MI,
	&m3639_MI,
	&m3636_MI,
	&m3637_MI,
};
static TypeInfo* t857_ITIs[] = 
{
	&t878_TI,
};
static Il2CppInterfaceOffsetPair t857_IOs[] = 
{
	{ &t878_TI, 4},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t857_0_0_0;
extern Il2CppType t857_1_0_0;
struct t857;
TypeInfo t857_TI = 
{
	&g_System_dll_Image, NULL, "PatternCompiler", "System.Text.RegularExpressions", t857_MIs, t857_PIs, t857_FIs, NULL, &t29_TI, t857_TI__nestedTypes, NULL, &t857_TI, t857_ITIs, t857_VT, &EmptyCustomAttributesCache, &t857_TI, &t857_0_0_0, &t857_1_0_0, t857_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t857), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 40, 1, 1, 0, 1, 34, 1, 1};
#include "t856.h"
#ifndef _MSC_VER
#else
#endif

#include "t580.h"
extern TypeInfo t580_TI;
#include "t580MD.h"
extern MethodInfo m3017_MI;
extern MethodInfo m4259_MI;
extern MethodInfo m2845_MI;
extern MethodInfo m3019_MI;
extern MethodInfo m3024_MI;
extern MethodInfo m4260_MI;


 void m3647 (t856 * __this, MethodInfo* method){
	{
		m3590(__this, &m3590_MI);
		t580 * L_0 = (t580 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t580_TI));
		m3017(L_0, &m3017_MI);
		__this->f0 = L_0;
		return;
	}
}
 void m3648 (t856 * __this, MethodInfo* method){
	{
		t580 * L_0 = (__this->f0);
		t29 * L_1 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m4259_MI, __this);
		VirtActionInvoker1< t29 * >::Invoke(&m2845_MI, L_0, L_1);
		return;
	}
}
 bool m3649 (t856 * __this, MethodInfo* method){
	{
		t580 * L_0 = (__this->f0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m3019_MI, L_0);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_0024;
		}
	}
	{
		t580 * L_2 = (__this->f0);
		t29 * L_3 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m3024_MI, L_2);
		VirtActionInvoker1< t29 * >::Invoke(&m4260_MI, __this, L_3);
		return 1;
	}

IL_0024:
	{
		return 0;
	}
}
// Metadata Definition System.Text.RegularExpressions.LinkStack
extern Il2CppType t580_0_0_1;
FieldInfo t856_f0_FieldInfo = 
{
	"stack", &t580_0_0_1, &t856_TI, offsetof(t856, f0), &EmptyCustomAttributesCache};
static FieldInfo* t856_FIs[] =
{
	&t856_f0_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3647_MI = 
{
	".ctor", (methodPointerType)&m3647, &t856_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 639, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3648_MI = 
{
	"Push", (methodPointerType)&m3648, &t856_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, false, 640, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m3649_MI = 
{
	"Pop", (methodPointerType)&m3649, &t856_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, false, 641, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m4259_MI = 
{
	"GetCurrent", NULL, &t856_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1476, 0, 4, 0, false, false, 642, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t856_m4260_ParameterInfos[] = 
{
	{"l", 0, 134218311, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m4260_MI = 
{
	"SetCurrent", NULL, &t856_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t856_m4260_ParameterInfos, &EmptyCustomAttributesCache, 1476, 0, 5, 1, false, false, 643, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t856_MIs[] =
{
	&m3647_MI,
	&m3648_MI,
	&m3649_MI,
	&m4259_MI,
	&m4260_MI,
	NULL
};
static MethodInfo* t856_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	NULL,
	NULL,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t856_0_0_0;
extern Il2CppType t856_1_0_0;
struct t856;
TypeInfo t856_TI = 
{
	&g_System_dll_Image, NULL, "LinkStack", "System.Text.RegularExpressions", t856_MIs, NULL, t856_FIs, NULL, &t852_TI, NULL, NULL, &t856_TI, NULL, t856_VT, &EmptyCustomAttributesCache, &t856_TI, &t856_0_0_0, &t856_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t856), 0, -1, 0, 0, -1, 1048704, 0, false, false, false, false, false, false, false, false, false, false, false, false, 5, 0, 1, 0, 0, 6, 0, 0};
#include "t859.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t859_TI;
#include "t859MD.h"



extern MethodInfo m3650_MI;
 bool m3650 (t859 * __this, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = (__this->f0);
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_1 = (__this->f1);
		G_B3_0 = ((((int32_t)((((int32_t)L_1) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_001b;
	}

IL_001a:
	{
		G_B3_0 = 0;
	}

IL_001b:
	{
		return G_B3_0;
	}
}
extern MethodInfo m3651_MI;
 int32_t m3651 (t859 * __this, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = (__this->f0);
		int32_t L_1 = (__this->f1);
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_2 = (__this->f0);
		G_B3_0 = L_2;
		goto IL_0022;
	}

IL_001c:
	{
		int32_t L_3 = (__this->f1);
		G_B3_0 = L_3;
	}

IL_0022:
	{
		return G_B3_0;
	}
}
extern MethodInfo m3652_MI;
 int32_t m3652 (t859 * __this, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = (__this->f0);
		int32_t L_1 = (__this->f1);
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_0023;
		}
	}
	{
		int32_t L_2 = (__this->f1);
		int32_t L_3 = (__this->f0);
		G_B3_0 = ((int32_t)(L_2-L_3));
		goto IL_0030;
	}

IL_0023:
	{
		int32_t L_4 = (__this->f0);
		int32_t L_5 = (__this->f1);
		G_B3_0 = ((int32_t)(L_4-L_5));
	}

IL_0030:
	{
		return G_B3_0;
	}
}
// Metadata Definition System.Text.RegularExpressions.Mark
extern Il2CppType t44_0_0_6;
FieldInfo t859_f0_FieldInfo = 
{
	"Start", &t44_0_0_6, &t859_TI, offsetof(t859, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_6;
FieldInfo t859_f1_FieldInfo = 
{
	"End", &t44_0_0_6, &t859_TI, offsetof(t859, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_6;
FieldInfo t859_f2_FieldInfo = 
{
	"Previous", &t44_0_0_6, &t859_TI, offsetof(t859, f2) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t859_FIs[] =
{
	&t859_f0_FieldInfo,
	&t859_f1_FieldInfo,
	&t859_f2_FieldInfo,
	NULL
};
static PropertyInfo t859____IsDefined_PropertyInfo = 
{
	&t859_TI, "IsDefined", &m3650_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t859____Index_PropertyInfo = 
{
	&t859_TI, "Index", &m3651_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t859____Length_PropertyInfo = 
{
	&t859_TI, "Length", &m3652_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t859_PIs[] =
{
	&t859____IsDefined_PropertyInfo,
	&t859____Index_PropertyInfo,
	&t859____Length_PropertyInfo,
	NULL
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m3650_MI = 
{
	"get_IsDefined", (methodPointerType)&m3650, &t859_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 644, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3651_MI = 
{
	"get_Index", (methodPointerType)&m3651, &t859_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 645, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3652_MI = 
{
	"get_Length", (methodPointerType)&m3652, &t859_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 646, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t859_MIs[] =
{
	&m3650_MI,
	&m3651_MI,
	&m3652_MI,
	NULL
};
static MethodInfo* t859_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t859_0_0_0;
extern Il2CppType t859_1_0_0;
TypeInfo t859_TI = 
{
	&g_System_dll_Image, NULL, "Mark", "System.Text.RegularExpressions", t859_MIs, t859_PIs, t859_FIs, NULL, &t110_TI, NULL, NULL, &t859_TI, NULL, t859_VT, &EmptyCustomAttributesCache, &t859_TI, &t859_0_0_0, &t859_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t859)+ sizeof (Il2CppObject), 0, sizeof(t859 ), 0, 0, -1, 1048840, 0, true, false, false, false, false, false, false, false, false, false, true, true, 3, 3, 3, 0, 0, 4, 0, 0};
#include "t860.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t860_TI;
#include "t860MD.h"

#include "t956.h"
extern TypeInfo t956_TI;
extern TypeInfo t841_TI;
#include "t956MD.h"
extern MethodInfo m4202_MI;


extern MethodInfo m3653_MI;
 int32_t m3653 (t860 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	{
		t841* L_0 = (__this->f0);
		int32_t L_1 = (__this->f1);
		int32_t L_2 = ((int32_t)(L_1-1));
		V_0 = L_2;
		__this->f1 = L_2;
		int32_t L_3 = V_0;
		return (*(int32_t*)(int32_t*)SZArrayLdElema(L_0, L_3));
	}
}
extern MethodInfo m3654_MI;
 void m3654 (t860 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	t841* V_1 = {0};
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		t841* L_0 = (__this->f0);
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		__this->f0 = ((t841*)SZArrayNew(InitializedTypeInfo(&t841_TI), 8));
		goto IL_006e;
	}

IL_001c:
	{
		int32_t L_1 = (__this->f1);
		t841* L_2 = (__this->f0);
		if ((((uint32_t)L_1) != ((uint32_t)(((int32_t)(((t20 *)L_2)->max_length))))))
		{
			goto IL_006e;
		}
	}
	{
		t841* L_3 = (__this->f0);
		V_0 = (((int32_t)(((t20 *)L_3)->max_length)));
		V_0 = ((int32_t)(V_0+((int32_t)((int32_t)V_0>>(int32_t)1))));
		V_1 = ((t841*)SZArrayNew(InitializedTypeInfo(&t841_TI), V_0));
		V_2 = 0;
		goto IL_005b;
	}

IL_004c:
	{
		t841* L_4 = (__this->f0);
		int32_t L_5 = V_2;
		*((int32_t*)(int32_t*)SZArrayLdElema(V_1, V_2)) = (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_4, L_5));
		V_2 = ((int32_t)(V_2+1));
	}

IL_005b:
	{
		int32_t L_6 = (__this->f1);
		if ((((int32_t)V_2) < ((int32_t)L_6)))
		{
			goto IL_004c;
		}
	}
	{
		__this->f0 = V_1;
	}

IL_006e:
	{
		t841* L_7 = (__this->f0);
		int32_t L_8 = (__this->f1);
		int32_t L_9 = L_8;
		V_3 = L_9;
		__this->f1 = ((int32_t)(L_9+1));
		*((int32_t*)(int32_t*)SZArrayLdElema(L_7, V_3)) = (int32_t)p0;
		return;
	}
}
extern MethodInfo m3655_MI;
 int32_t m3655 (t860 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		return L_0;
	}
}
extern MethodInfo m3656_MI;
 void m3656 (t860 * __this, int32_t p0, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((int32_t)p0) <= ((int32_t)L_0)))
		{
			goto IL_0017;
		}
	}
	{
		t956 * L_1 = (t956 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t956_TI));
		m4202(L_1, (t7*) &_stringLiteral482, &m4202_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0017:
	{
		__this->f1 = p0;
		return;
	}
}
// Conversion methods for marshalling of: System.Text.RegularExpressions.Interpreter/IntStack
void t860_marshal(const t860& unmarshaled, t860_marshaled& marshaled)
{
	marshaled.f0 = il2cpp_codegen_marshal_array<int32_t>((Il2CppCodeGenArray*)unmarshaled.f0);
	marshaled.f1 = unmarshaled.f1;
}
void t860_marshal_back(const t860_marshaled& marshaled, t860& unmarshaled)
{
	extern TypeInfo t44_TI;
	unmarshaled.f0 = (t841*)il2cpp_codegen_marshal_array_result(&t44_TI, marshaled.f0, 1);
	unmarshaled.f1 = marshaled.f1;
}
// Conversion method for clean up from marshalling of: System.Text.RegularExpressions.Interpreter/IntStack
void t860_marshal_cleanup(t860_marshaled& marshaled)
{
}
// Metadata Definition System.Text.RegularExpressions.Interpreter/IntStack
extern Il2CppType t841_0_0_1;
FieldInfo t860_f0_FieldInfo = 
{
	"values", &t841_0_0_1, &t860_TI, offsetof(t860, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t860_f1_FieldInfo = 
{
	"count", &t44_0_0_1, &t860_TI, offsetof(t860, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t860_FIs[] =
{
	&t860_f0_FieldInfo,
	&t860_f1_FieldInfo,
	NULL
};
static PropertyInfo t860____Count_PropertyInfo = 
{
	&t860_TI, "Count", &m3655_MI, &m3656_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t860_PIs[] =
{
	&t860____Count_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3653_MI = 
{
	"Pop", (methodPointerType)&m3653, &t860_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, false, 668, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t860_m3654_ParameterInfos[] = 
{
	{"value", 0, 134218349, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3654_MI = 
{
	"Push", (methodPointerType)&m3654, &t860_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t860_m3654_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 669, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3655_MI = 
{
	"get_Count", (methodPointerType)&m3655, &t860_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 670, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t860_m3656_ParameterInfos[] = 
{
	{"value", 0, 134218350, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3656_MI = 
{
	"set_Count", (methodPointerType)&m3656, &t860_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t860_m3656_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 671, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t860_MIs[] =
{
	&m3653_MI,
	&m3654_MI,
	&m3655_MI,
	&m3656_MI,
	NULL
};
static MethodInfo* t860_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t860_0_0_0;
extern Il2CppType t860_1_0_0;
TypeInfo t860_TI = 
{
	&g_System_dll_Image, NULL, "IntStack", "", t860_MIs, t860_PIs, t860_FIs, NULL, &t110_TI, NULL, &t863_TI, &t860_TI, NULL, t860_VT, &EmptyCustomAttributesCache, &t860_TI, &t860_0_0_0, &t860_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t860_marshal, (methodPointerType)t860_marshal_back, (methodPointerType)t860_marshal_cleanup, sizeof (t860)+ sizeof (Il2CppObject), 0, sizeof(t860_marshaled), 0, 0, -1, 1048843, 0, true, false, false, false, false, false, false, false, false, false, false, false, 4, 1, 2, 0, 0, 4, 0, 0};
#include "t861.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t861_TI;
#include "t861MD.h"



extern MethodInfo m3657_MI;
 void m3657 (t861 * __this, t861 * p0, int32_t p1, int32_t p2, bool p3, int32_t p4, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		__this->f5 = p0;
		__this->f1 = p1;
		__this->f2 = p2;
		__this->f3 = p3;
		__this->f4 = p4;
		__this->f0 = (-1);
		__this->f6 = 0;
		return;
	}
}
extern MethodInfo m3658_MI;
 int32_t m3658 (t861 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f6);
		return L_0;
	}
}
extern MethodInfo m3659_MI;
 void m3659 (t861 * __this, int32_t p0, MethodInfo* method){
	{
		__this->f6 = p0;
		return;
	}
}
extern MethodInfo m3660_MI;
 int32_t m3660 (t861 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f0);
		return L_0;
	}
}
extern MethodInfo m3661_MI;
 void m3661 (t861 * __this, int32_t p0, MethodInfo* method){
	{
		__this->f0 = p0;
		return;
	}
}
extern MethodInfo m3662_MI;
 bool m3662 (t861 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		int32_t L_1 = (__this->f6);
		return ((((int32_t)((((int32_t)L_0) > ((int32_t)L_1))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
extern MethodInfo m3663_MI;
 bool m3663 (t861 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f2);
		int32_t L_1 = (__this->f6);
		return ((((int32_t)((((int32_t)L_0) > ((int32_t)L_1))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
extern MethodInfo m3664_MI;
 bool m3664 (t861 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->f3);
		return L_0;
	}
}
extern MethodInfo m3665_MI;
 int32_t m3665 (t861 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f4);
		return L_0;
	}
}
extern MethodInfo m3666_MI;
 t861 * m3666 (t861 * __this, MethodInfo* method){
	{
		t861 * L_0 = (__this->f5);
		return L_0;
	}
}
// Metadata Definition System.Text.RegularExpressions.Interpreter/RepeatContext
extern Il2CppType t44_0_0_1;
FieldInfo t861_f0_FieldInfo = 
{
	"start", &t44_0_0_1, &t861_TI, offsetof(t861, f0), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t861_f1_FieldInfo = 
{
	"min", &t44_0_0_1, &t861_TI, offsetof(t861, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t861_f2_FieldInfo = 
{
	"max", &t44_0_0_1, &t861_TI, offsetof(t861, f2), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t861_f3_FieldInfo = 
{
	"lazy", &t40_0_0_1, &t861_TI, offsetof(t861, f3), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t861_f4_FieldInfo = 
{
	"expr_pc", &t44_0_0_1, &t861_TI, offsetof(t861, f4), &EmptyCustomAttributesCache};
extern Il2CppType t861_0_0_1;
FieldInfo t861_f5_FieldInfo = 
{
	"previous", &t861_0_0_1, &t861_TI, offsetof(t861, f5), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t861_f6_FieldInfo = 
{
	"count", &t44_0_0_1, &t861_TI, offsetof(t861, f6), &EmptyCustomAttributesCache};
static FieldInfo* t861_FIs[] =
{
	&t861_f0_FieldInfo,
	&t861_f1_FieldInfo,
	&t861_f2_FieldInfo,
	&t861_f3_FieldInfo,
	&t861_f4_FieldInfo,
	&t861_f5_FieldInfo,
	&t861_f6_FieldInfo,
	NULL
};
static PropertyInfo t861____Count_PropertyInfo = 
{
	&t861_TI, "Count", &m3658_MI, &m3659_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t861____Start_PropertyInfo = 
{
	&t861_TI, "Start", &m3660_MI, &m3661_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t861____IsMinimum_PropertyInfo = 
{
	&t861_TI, "IsMinimum", &m3662_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t861____IsMaximum_PropertyInfo = 
{
	&t861_TI, "IsMaximum", &m3663_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t861____IsLazy_PropertyInfo = 
{
	&t861_TI, "IsLazy", &m3664_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t861____Expression_PropertyInfo = 
{
	&t861_TI, "Expression", &m3665_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t861____Previous_PropertyInfo = 
{
	&t861_TI, "Previous", &m3666_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t861_PIs[] =
{
	&t861____Count_PropertyInfo,
	&t861____Start_PropertyInfo,
	&t861____IsMinimum_PropertyInfo,
	&t861____IsMaximum_PropertyInfo,
	&t861____IsLazy_PropertyInfo,
	&t861____Expression_PropertyInfo,
	&t861____Previous_PropertyInfo,
	NULL
};
extern Il2CppType t861_0_0_0;
extern Il2CppType t861_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t861_m3657_ParameterInfos[] = 
{
	{"previous", 0, 134218351, &EmptyCustomAttributesCache, &t861_0_0_0},
	{"min", 1, 134218352, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"max", 2, 134218353, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"lazy", 3, 134218354, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"expr_pc", 4, 134218355, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44_t44_t297_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3657_MI = 
{
	".ctor", (methodPointerType)&m3657, &t861_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44_t44_t297_t44, t861_m3657_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 5, false, false, 672, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3658_MI = 
{
	"get_Count", (methodPointerType)&m3658, &t861_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 673, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t861_m3659_ParameterInfos[] = 
{
	{"value", 0, 134218356, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3659_MI = 
{
	"set_Count", (methodPointerType)&m3659, &t861_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t861_m3659_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 674, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3660_MI = 
{
	"get_Start", (methodPointerType)&m3660, &t861_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 675, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t861_m3661_ParameterInfos[] = 
{
	{"value", 0, 134218357, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3661_MI = 
{
	"set_Start", (methodPointerType)&m3661, &t861_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t861_m3661_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 676, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m3662_MI = 
{
	"get_IsMinimum", (methodPointerType)&m3662, &t861_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 677, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m3663_MI = 
{
	"get_IsMaximum", (methodPointerType)&m3663, &t861_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 678, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m3664_MI = 
{
	"get_IsLazy", (methodPointerType)&m3664, &t861_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 679, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3665_MI = 
{
	"get_Expression", (methodPointerType)&m3665, &t861_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 680, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t861_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3666_MI = 
{
	"get_Previous", (methodPointerType)&m3666, &t861_TI, &t861_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 681, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t861_MIs[] =
{
	&m3657_MI,
	&m3658_MI,
	&m3659_MI,
	&m3660_MI,
	&m3661_MI,
	&m3662_MI,
	&m3663_MI,
	&m3664_MI,
	&m3665_MI,
	&m3666_MI,
	NULL
};
static MethodInfo* t861_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t861_1_0_0;
struct t861;
TypeInfo t861_TI = 
{
	&g_System_dll_Image, NULL, "RepeatContext", "", t861_MIs, t861_PIs, t861_FIs, NULL, &t29_TI, NULL, &t863_TI, &t861_TI, NULL, t861_VT, &EmptyCustomAttributesCache, &t861_TI, &t861_0_0_0, &t861_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t861), 0, -1, 0, 0, -1, 1048579, 0, false, false, false, false, false, false, false, false, false, false, false, false, 10, 7, 7, 0, 0, 4, 0, 0};
#include "t862.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t862_TI;
#include "t862MD.h"



// Metadata Definition System.Text.RegularExpressions.Interpreter/Mode
extern Il2CppType t44_0_0_1542;
FieldInfo t862_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t862_TI, offsetof(t862, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t862_0_0_32854;
FieldInfo t862_f2_FieldInfo = 
{
	"Search", &t862_0_0_32854, &t862_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t862_0_0_32854;
FieldInfo t862_f3_FieldInfo = 
{
	"Match", &t862_0_0_32854, &t862_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t862_0_0_32854;
FieldInfo t862_f4_FieldInfo = 
{
	"Count", &t862_0_0_32854, &t862_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t862_FIs[] =
{
	&t862_f1_FieldInfo,
	&t862_f2_FieldInfo,
	&t862_f3_FieldInfo,
	&t862_f4_FieldInfo,
	NULL
};
static const int32_t t862_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t862_f2_DefaultValue = 
{
	&t862_f2_FieldInfo, { (char*)&t862_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t862_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t862_f3_DefaultValue = 
{
	&t862_f3_FieldInfo, { (char*)&t862_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t862_f4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t862_f4_DefaultValue = 
{
	&t862_f4_FieldInfo, { (char*)&t862_f4_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t862_FDVs[] = 
{
	&t862_f2_DefaultValue,
	&t862_f3_DefaultValue,
	&t862_f4_DefaultValue,
	NULL
};
static MethodInfo* t862_MIs[] =
{
	NULL
};
static MethodInfo* t862_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t862_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t862_0_0_0;
extern Il2CppType t862_1_0_0;
TypeInfo t862_TI = 
{
	&g_System_dll_Image, NULL, "Mode", "", t862_MIs, NULL, t862_FIs, NULL, &t49_TI, NULL, &t863_TI, &t44_TI, NULL, t862_VT, &EmptyCustomAttributesCache, &t44_TI, &t862_0_0_0, &t862_1_0_0, t862_IOs, NULL, NULL, t862_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t862)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 259, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 4, 0, 0, 23, 0, 3};
#ifndef _MSC_VER
#else
#endif

#include "t864.h"
#include "t828.h"
#include "t829.h"
#include "System_ArrayTypes.h"
#include "t833.h"
#include "t830.h"
#include "t831.h"
#include "t827.h"
#include "t834.h"
extern TypeInfo t828_TI;
extern TypeInfo t864_TI;
extern TypeInfo t200_TI;
extern TypeInfo t865_TI;
extern TypeInfo t830_TI;
extern TypeInfo t827_TI;
extern TypeInfo t833_TI;
extern TypeInfo t834_TI;
#include "t827MD.h"
#include "t828MD.h"
#include "t864MD.h"
#include "t20MD.h"
#include "t830MD.h"
#include "t833MD.h"
#include "t831MD.h"
#include "t834MD.h"
extern MethodInfo m3501_MI;
extern MethodInfo m3668_MI;
extern MethodInfo m3671_MI;
extern MethodInfo m3687_MI;
extern MethodInfo m3535_MI;
extern MethodInfo m3682_MI;
extern MethodInfo m3673_MI;
extern MethodInfo m3676_MI;
extern MethodInfo m3751_MI;
extern MethodInfo m3754_MI;
extern MethodInfo m3753_MI;
extern MethodInfo m3674_MI;
extern MethodInfo m3683_MI;
extern MethodInfo m3672_MI;
extern MethodInfo m3677_MI;
extern MethodInfo m3678_MI;
extern MethodInfo m3679_MI;
extern MethodInfo m3680_MI;
extern MethodInfo m3681_MI;
extern MethodInfo m1807_MI;
extern MethodInfo m3670_MI;
extern MethodInfo m3675_MI;
extern MethodInfo m4261_MI;
extern MethodInfo m3684_MI;
extern MethodInfo m4199_MI;
extern MethodInfo m3504_MI;
extern MethodInfo m3521_MI;
extern MethodInfo m3513_MI;
extern MethodInfo m3685_MI;
extern MethodInfo m3532_MI;
extern MethodInfo m3533_MI;
extern MethodInfo m3686_MI;
extern MethodInfo m3517_MI;
extern MethodInfo m3536_MI;
extern MethodInfo m3527_MI;


 void m3667 (t863 * __this, t764* p0, MethodInfo* method){
	t860  V_0 = {0};
	{
		Initobj (&t860_TI, (&V_0));
		__this->f11 = V_0;
		m3501(__this, &m3501_MI);
		__this->f1 = p0;
		__this->f7 = (t864 *)NULL;
		int32_t L_0 = m3668(__this, 1, &m3668_MI);
		__this->f5 = ((int32_t)(L_0+1));
		int32_t L_1 = m3668(__this, 3, &m3668_MI);
		__this->f6 = L_1;
		__this->f2 = 7;
		int32_t L_2 = (__this->f5);
		__this->f16 = ((t841*)SZArrayNew(InitializedTypeInfo(&t841_TI), L_2));
		return;
	}
}
 int32_t m3668 (t863 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		t764* L_0 = (__this->f1);
		int32_t L_1 = ((int32_t)(p0+1));
		V_0 = (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_0, L_1));
		V_0 = ((int32_t)((int32_t)V_0<<(int32_t)((int32_t)16)));
		t764* L_2 = (__this->f1);
		int32_t L_3 = p0;
		V_0 = ((int32_t)(V_0+(*(uint16_t*)(uint16_t*)SZArrayLdElema(L_2, L_3))));
		return V_0;
	}
}
extern MethodInfo m3669_MI;
 t828 * m3669 (t863 * __this, t829 * p0, t7* p1, int32_t p2, int32_t p3, MethodInfo* method){
	{
		__this->f3 = p1;
		__this->f4 = p3;
		__this->f8 = p2;
		int32_t* L_0 = &(__this->f8);
		int32_t L_1 = (__this->f2);
		bool L_2 = m3671(__this, 1, L_0, L_1, &m3671_MI);
		if (!L_2)
		{
			goto IL_0036;
		}
	}
	{
		t828 * L_3 = m3687(__this, p0, &m3687_MI);
		return L_3;
	}

IL_0036:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t828_TI));
		t828 * L_4 = m3535(NULL, &m3535_MI);
		return L_4;
	}
}
 void m3670 (t863 * __this, MethodInfo* method){
	t861 * V_0 = {0};
	{
		m3682(__this, &m3682_MI);
		V_0 = (t861 *)NULL;
		__this->f9 = (t861 *)NULL;
		__this->f10 = V_0;
		return;
	}
}
 bool m3671 (t863 * __this, int32_t p0, int32_t* p1, int32_t p2, MethodInfo* method){
	int32_t V_0 = 0;
	uint16_t V_1 = 0;
	uint16_t V_2 = {0};
	uint16_t V_3 = {0};
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	bool V_6 = false;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	uint16_t V_10 = {0};
	bool V_11 = false;
	bool V_12 = false;
	t7* V_13 = {0};
	bool V_14 = false;
	bool V_15 = false;
	int32_t V_16 = 0;
	int32_t V_17 = 0;
	uint16_t V_18 = 0x0;
	bool V_19 = false;
	bool V_20 = false;
	int32_t V_21 = 0;
	int32_t V_22 = 0;
	int32_t V_23 = 0;
	int32_t V_24 = 0;
	int32_t V_25 = 0;
	int32_t V_26 = 0;
	int32_t V_27 = 0;
	int32_t V_28 = 0;
	int32_t V_29 = 0;
	int32_t V_30 = 0;
	uint16_t V_31 = {0};
	int32_t V_32 = 0;
	t861 * V_33 = {0};
	int32_t V_34 = 0;
	int32_t V_35 = 0;
	int32_t V_36 = 0;
	int32_t V_37 = 0;
	int32_t V_38 = 0;
	int32_t V_39 = 0;
	int32_t V_40 = 0;
	int32_t V_41 = 0;
	uint16_t V_42 = 0;
	int32_t V_43 = 0;
	int32_t V_44 = 0;
	int32_t V_45 = 0;
	uint16_t V_46 = {0};
	uint16_t V_47 = {0};
	int32_t V_48 = 0;
	int32_t V_49 = 0;
	int32_t V_50 = 0;
	int32_t V_51 = 0;
	uint16_t V_52 = {0};
	uint16_t V_53 = {0};
	int32_t V_54 = {0};
	int32_t G_B7_0 = 0;
	int32_t G_B29_0 = 0;
	int32_t G_B33_0 = 0;
	int32_t G_B48_0 = 0;
	int32_t G_B69_0 = 0;
	int32_t G_B96_0 = 0;
	int32_t G_B162_0 = 0;
	int32_t G_B162_1 = 0;
	t863 * G_B162_2 = {0};
	int32_t G_B161_0 = 0;
	int32_t G_B161_1 = 0;
	t863 * G_B161_2 = {0};
	int32_t G_B163_0 = 0;
	int32_t G_B163_1 = 0;
	int32_t G_B163_2 = 0;
	t863 * G_B163_3 = {0};
	{
		V_0 = (*((int32_t*)p1));
	}

IL_0003:
	{
		goto IL_0fee;
	}

IL_0008:
	{
		t764* L_0 = (__this->f1);
		int32_t L_1 = p2;
		V_1 = (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_0, L_1));
		V_2 = (((uint16_t)((int32_t)((int32_t)V_1&(int32_t)((int32_t)255)))));
		V_3 = (((uint16_t)((int32_t)((int32_t)V_1&(int32_t)((int32_t)65280)))));
		V_52 = V_2;
		if (V_52 == 0)
		{
			goto IL_04b8;
		}
		if (V_52 == 1)
		{
			goto IL_04bd;
		}
		if (V_52 == 2)
		{
			goto IL_04c2;
		}
		if (V_52 == 3)
		{
			goto IL_04e7;
		}
		if (V_52 == 4)
		{
			goto IL_05ab;
		}
		if (V_52 == 5)
		{
			goto IL_06ef;
		}
		if (V_52 == 6)
		{
			goto IL_06ef;
		}
		if (V_52 == 7)
		{
			goto IL_06ef;
		}
		if (V_52 == 8)
		{
			goto IL_06ef;
		}
		if (V_52 == 9)
		{
			goto IL_06ef;
		}
		if (V_52 == 10)
		{
			goto IL_070a;
		}
		if (V_52 == 11)
		{
			goto IL_073c;
		}
		if (V_52 == 12)
		{
			goto IL_0757;
		}
		if (V_52 == 13)
		{
			goto IL_07db;
		}
		if (V_52 == 14)
		{
			goto IL_0772;
		}
		if (V_52 == 15)
		{
			goto IL_07e0;
		}
		if (V_52 == 16)
		{
			goto IL_0817;
		}
		if (V_52 == 17)
		{
			goto IL_0840;
		}
		if (V_52 == 18)
		{
			goto IL_088a;
		}
		if (V_52 == 19)
		{
			goto IL_08db;
		}
		if (V_52 == 20)
		{
			goto IL_08ee;
		}
		if (V_52 == 21)
		{
			goto IL_0957;
		}
		if (V_52 == 22)
		{
			goto IL_0c6f;
		}
		if (V_52 == 23)
		{
			goto IL_0096;
		}
		if (V_52 == 24)
		{
			goto IL_0fe9;
		}
	}
	{
		goto IL_0fee;
	}

IL_0096:
	{
		t764* L_2 = (__this->f1);
		int32_t L_3 = ((int32_t)(p2+1));
		V_4 = (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_2, L_3));
		t764* L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(p2+2));
		V_5 = (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_4, L_5));
		V_6 = ((((int32_t)((((int32_t)(((uint16_t)((int32_t)((int32_t)V_3&(int32_t)((int32_t)1024)))))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		if (!V_6)
		{
			goto IL_00ce;
		}
	}
	{
		G_B7_0 = ((int32_t)(V_0-V_5));
		goto IL_00d2;
	}

IL_00ce:
	{
		G_B7_0 = ((int32_t)(V_0+V_5));
	}

IL_00d2:
	{
		V_7 = G_B7_0;
		int32_t L_6 = (__this->f4);
		int32_t L_7 = (__this->f6);
		V_8 = ((int32_t)(((int32_t)(L_6-L_7))+V_5));
		V_9 = 0;
		t764* L_8 = (__this->f1);
		int32_t L_9 = ((int32_t)(p2+3));
		V_10 = (((uint16_t)((int32_t)((int32_t)(*(uint16_t*)(uint16_t*)SZArrayLdElema(L_8, L_9))&(int32_t)((int32_t)255)))));
		if ((((uint32_t)V_10) != ((uint32_t)2)))
		{
			goto IL_0285;
		}
	}
	{
		if ((((uint32_t)V_4) != ((uint32_t)6)))
		{
			goto IL_0285;
		}
	}
	{
		t764* L_10 = (__this->f1);
		int32_t L_11 = ((int32_t)(p2+4));
		V_53 = (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_10, L_11));
		if (((uint16_t)(V_53-2)) == 0)
		{
			goto IL_0132;
		}
		if (((uint16_t)(V_53-2)) == 1)
		{
			goto IL_0165;
		}
		if (((uint16_t)(V_53-2)) == 2)
		{
			goto IL_0234;
		}
	}
	{
		goto IL_027b;
	}

IL_0132:
	{
		if (V_6)
		{
			goto IL_0140;
		}
	}
	{
		if (V_5)
		{
			goto IL_0160;
		}
	}

IL_0140:
	{
		if (!V_6)
		{
			goto IL_014a;
		}
	}
	{
		V_0 = V_5;
	}

IL_014a:
	{
		bool L_12 = m3673(__this, (&V_0), ((int32_t)(p2+V_4)), &m3673_MI);
		if (!L_12)
		{
			goto IL_0160;
		}
	}
	{
		goto IL_0ff3;
	}

IL_0160:
	{
		goto IL_0280;
	}

IL_0165:
	{
		if (V_7)
		{
			goto IL_018a;
		}
	}
	{
		V_0 = 0;
		bool L_13 = m3673(__this, (&V_0), ((int32_t)(p2+V_4)), &m3673_MI);
		if (!L_13)
		{
			goto IL_0184;
		}
	}
	{
		goto IL_0ff3;
	}

IL_0184:
	{
		V_7 = ((int32_t)(V_7+1));
	}

IL_018a:
	{
		goto IL_0210;
	}

IL_018f:
	{
		if (!V_7)
		{
			goto IL_01ac;
		}
	}
	{
		t7* L_14 = (__this->f3);
		uint16_t L_15 = m1741(L_14, ((int32_t)(V_7-1)), &m1741_MI);
		if ((((uint32_t)L_15) != ((uint32_t)((int32_t)10))))
		{
			goto IL_01f8;
		}
	}

IL_01ac:
	{
		if (!V_6)
		{
			goto IL_01ce;
		}
	}
	{
		if ((((uint32_t)V_7) != ((uint32_t)V_8)))
		{
			goto IL_01c3;
		}
	}
	{
		G_B29_0 = V_7;
		goto IL_01c8;
	}

IL_01c3:
	{
		G_B29_0 = ((int32_t)(V_7+V_5));
	}

IL_01c8:
	{
		V_0 = G_B29_0;
		goto IL_01e2;
	}

IL_01ce:
	{
		if (V_7)
		{
			goto IL_01dc;
		}
	}
	{
		G_B33_0 = V_7;
		goto IL_01e1;
	}

IL_01dc:
	{
		G_B33_0 = ((int32_t)(V_7-V_5));
	}

IL_01e1:
	{
		V_0 = G_B33_0;
	}

IL_01e2:
	{
		bool L_16 = m3673(__this, (&V_0), ((int32_t)(p2+V_4)), &m3673_MI);
		if (!L_16)
		{
			goto IL_01f8;
		}
	}
	{
		goto IL_0ff3;
	}

IL_01f8:
	{
		if (!V_6)
		{
			goto IL_020a;
		}
	}
	{
		V_7 = ((int32_t)(V_7-1));
		goto IL_0210;
	}

IL_020a:
	{
		V_7 = ((int32_t)(V_7+1));
	}

IL_0210:
	{
		if (!V_6)
		{
			goto IL_021f;
		}
	}
	{
		if ((((int32_t)V_7) >= ((int32_t)0)))
		{
			goto IL_018f;
		}
	}

IL_021f:
	{
		if (V_6)
		{
			goto IL_022f;
		}
	}
	{
		if ((((int32_t)V_7) <= ((int32_t)V_8)))
		{
			goto IL_018f;
		}
	}

IL_022f:
	{
		goto IL_0280;
	}

IL_0234:
	{
		int32_t L_17 = (__this->f8);
		if ((((uint32_t)V_7) != ((uint32_t)L_17)))
		{
			goto IL_0276;
		}
	}
	{
		if (!V_6)
		{
			goto IL_0256;
		}
	}
	{
		int32_t L_18 = (__this->f8);
		G_B48_0 = ((int32_t)(L_18+V_5));
		goto IL_025f;
	}

IL_0256:
	{
		int32_t L_19 = (__this->f8);
		G_B48_0 = ((int32_t)(L_19-V_5));
	}

IL_025f:
	{
		V_0 = G_B48_0;
		bool L_20 = m3673(__this, (&V_0), ((int32_t)(p2+V_4)), &m3673_MI);
		if (!L_20)
		{
			goto IL_0276;
		}
	}
	{
		goto IL_0ff3;
	}

IL_0276:
	{
		goto IL_0280;
	}

IL_027b:
	{
		goto IL_0280;
	}

IL_0280:
	{
		goto IL_04b3;
	}

IL_0285:
	{
		t864 * L_21 = (__this->f7);
		if (L_21)
		{
			goto IL_02ab;
		}
	}
	{
		if ((((uint32_t)V_10) != ((uint32_t)3)))
		{
			goto IL_03d2;
		}
	}
	{
		t764* L_22 = (__this->f1);
		int32_t L_23 = ((int32_t)(p2+4));
		if ((((uint32_t)V_4) != ((uint32_t)((int32_t)(6+(*(uint16_t*)(uint16_t*)SZArrayLdElema(L_22, L_23)))))))
		{
			goto IL_03d2;
		}
	}

IL_02ab:
	{
		t764* L_24 = (__this->f1);
		int32_t L_25 = ((int32_t)(p2+3));
		V_11 = ((((int32_t)((((int32_t)(((uint16_t)((int32_t)((int32_t)(*(uint16_t*)(uint16_t*)SZArrayLdElema(L_24, L_25))&(int32_t)((int32_t)1024)))))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		t864 * L_26 = (__this->f7);
		if (L_26)
		{
			goto IL_0304;
		}
	}
	{
		t764* L_27 = (__this->f1);
		int32_t L_28 = ((int32_t)(p2+3));
		V_12 = ((((int32_t)((((int32_t)(((uint16_t)((int32_t)((int32_t)(*(uint16_t*)(uint16_t*)SZArrayLdElema(L_27, L_28))&(int32_t)((int32_t)512)))))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		t7* L_29 = m3676(__this, ((int32_t)(p2+3)), &m3676_MI);
		V_13 = L_29;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t864_TI));
		t864 * L_30 = (t864 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t864_TI));
		m3751(L_30, V_13, V_12, V_11, &m3751_MI);
		__this->f7 = L_30;
	}

IL_0304:
	{
		goto IL_03ad;
	}

IL_0309:
	{
		if (!V_11)
		{
			goto IL_0344;
		}
	}
	{
		t864 * L_31 = (__this->f7);
		t7* L_32 = (__this->f3);
		int32_t L_33 = m3754(L_31, L_32, V_7, V_9, &m3754_MI);
		V_7 = L_33;
		if ((((int32_t)V_7) == ((int32_t)(-1))))
		{
			goto IL_033f;
		}
	}
	{
		t864 * L_34 = (__this->f7);
		int32_t L_35 = m3753(L_34, &m3753_MI);
		V_7 = ((int32_t)(V_7+L_35));
	}

IL_033f:
	{
		goto IL_035b;
	}

IL_0344:
	{
		t864 * L_36 = (__this->f7);
		t7* L_37 = (__this->f3);
		int32_t L_38 = m3754(L_36, L_37, V_7, V_8, &m3754_MI);
		V_7 = L_38;
	}

IL_035b:
	{
		if ((((int32_t)V_7) >= ((int32_t)0)))
		{
			goto IL_0368;
		}
	}
	{
		goto IL_03cd;
	}

IL_0368:
	{
		if (!V_11)
		{
			goto IL_0379;
		}
	}
	{
		G_B69_0 = ((int32_t)(V_7+V_5));
		goto IL_037e;
	}

IL_0379:
	{
		G_B69_0 = ((int32_t)(V_7-V_5));
	}

IL_037e:
	{
		V_0 = G_B69_0;
		bool L_39 = m3673(__this, (&V_0), ((int32_t)(p2+V_4)), &m3673_MI);
		if (!L_39)
		{
			goto IL_0395;
		}
	}
	{
		goto IL_0ff3;
	}

IL_0395:
	{
		if (!V_11)
		{
			goto IL_03a7;
		}
	}
	{
		V_7 = ((int32_t)(V_7-2));
		goto IL_03ad;
	}

IL_03a7:
	{
		V_7 = ((int32_t)(V_7+1));
	}

IL_03ad:
	{
		if (!V_6)
		{
			goto IL_03bd;
		}
	}
	{
		if ((((int32_t)V_7) >= ((int32_t)V_9)))
		{
			goto IL_0309;
		}
	}

IL_03bd:
	{
		if (V_6)
		{
			goto IL_03cd;
		}
	}
	{
		if ((((int32_t)V_7) <= ((int32_t)V_8)))
		{
			goto IL_0309;
		}
	}

IL_03cd:
	{
		goto IL_04b3;
	}

IL_03d2:
	{
		if ((((uint32_t)V_10) != ((uint32_t)1)))
		{
			goto IL_0435;
		}
	}
	{
		goto IL_0410;
	}

IL_03df:
	{
		V_0 = V_7;
		bool L_40 = m3673(__this, (&V_0), ((int32_t)(p2+V_4)), &m3673_MI);
		if (!L_40)
		{
			goto IL_03f8;
		}
	}
	{
		goto IL_0ff3;
	}

IL_03f8:
	{
		if (!V_6)
		{
			goto IL_040a;
		}
	}
	{
		V_7 = ((int32_t)(V_7-1));
		goto IL_0410;
	}

IL_040a:
	{
		V_7 = ((int32_t)(V_7+1));
	}

IL_0410:
	{
		if (!V_6)
		{
			goto IL_0420;
		}
	}
	{
		if ((((int32_t)V_7) >= ((int32_t)V_9)))
		{
			goto IL_03df;
		}
	}

IL_0420:
	{
		if (V_6)
		{
			goto IL_0430;
		}
	}
	{
		if ((((int32_t)V_7) <= ((int32_t)V_8)))
		{
			goto IL_03df;
		}
	}

IL_0430:
	{
		goto IL_04b3;
	}

IL_0435:
	{
		goto IL_0493;
	}

IL_043a:
	{
		V_0 = V_7;
		bool L_41 = m3671(__this, 1, (&V_0), ((int32_t)(p2+3)), &m3671_MI);
		if (!L_41)
		{
			goto IL_047b;
		}
	}
	{
		if (!V_6)
		{
			goto IL_045f;
		}
	}
	{
		G_B96_0 = ((int32_t)(V_7+V_5));
		goto IL_0464;
	}

IL_045f:
	{
		G_B96_0 = ((int32_t)(V_7-V_5));
	}

IL_0464:
	{
		V_0 = G_B96_0;
		bool L_42 = m3673(__this, (&V_0), ((int32_t)(p2+V_4)), &m3673_MI);
		if (!L_42)
		{
			goto IL_047b;
		}
	}
	{
		goto IL_0ff3;
	}

IL_047b:
	{
		if (!V_6)
		{
			goto IL_048d;
		}
	}
	{
		V_7 = ((int32_t)(V_7-1));
		goto IL_0493;
	}

IL_048d:
	{
		V_7 = ((int32_t)(V_7+1));
	}

IL_0493:
	{
		if (!V_6)
		{
			goto IL_04a3;
		}
	}
	{
		if ((((int32_t)V_7) >= ((int32_t)V_9)))
		{
			goto IL_043a;
		}
	}

IL_04a3:
	{
		if (V_6)
		{
			goto IL_04b3;
		}
	}
	{
		if ((((int32_t)V_7) <= ((int32_t)V_8)))
		{
			goto IL_043a;
		}
	}

IL_04b3:
	{
		goto IL_1067;
	}

IL_04b8:
	{
		goto IL_1067;
	}

IL_04bd:
	{
		goto IL_0ff3;
	}

IL_04c2:
	{
		t764* L_43 = (__this->f1);
		int32_t L_44 = ((int32_t)(p2+1));
		bool L_45 = m3674(__this, (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_43, L_44)), V_0, &m3674_MI);
		if (L_45)
		{
			goto IL_04dd;
		}
	}
	{
		goto IL_1067;
	}

IL_04dd:
	{
		p2 = ((int32_t)(p2+2));
		goto IL_0fee;
	}

IL_04e7:
	{
		V_14 = ((((int32_t)((((int32_t)(((uint16_t)((int32_t)((int32_t)V_3&(int32_t)((int32_t)1024)))))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		V_15 = ((((int32_t)((((int32_t)(((uint16_t)((int32_t)((int32_t)V_3&(int32_t)((int32_t)512)))))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		t764* L_46 = (__this->f1);
		int32_t L_47 = ((int32_t)(p2+1));
		V_16 = (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_46, L_47));
		if (!V_14)
		{
			goto IL_0530;
		}
	}
	{
		V_0 = ((int32_t)(V_0-V_16));
		if ((((int32_t)V_0) >= ((int32_t)0)))
		{
			goto IL_052b;
		}
	}
	{
		goto IL_1067;
	}

IL_052b:
	{
		goto IL_0544;
	}

IL_0530:
	{
		int32_t L_48 = (__this->f4);
		if ((((int32_t)((int32_t)(V_0+V_16))) <= ((int32_t)L_48)))
		{
			goto IL_0544;
		}
	}
	{
		goto IL_1067;
	}

IL_0544:
	{
		p2 = ((int32_t)(p2+2));
		V_17 = 0;
		goto IL_0591;
	}

IL_0551:
	{
		t7* L_49 = (__this->f3);
		uint16_t L_50 = m1741(L_49, ((int32_t)(V_0+V_17)), &m1741_MI);
		V_18 = L_50;
		if (!V_15)
		{
			goto IL_0572;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t194_TI));
		uint16_t L_51 = m1809(NULL, V_18, &m1809_MI);
		V_18 = L_51;
	}

IL_0572:
	{
		t764* L_52 = (__this->f1);
		int32_t L_53 = p2;
		p2 = ((int32_t)(L_53+1));
		int32_t L_54 = L_53;
		if ((((int32_t)V_18) == ((int32_t)(*(uint16_t*)(uint16_t*)SZArrayLdElema(L_52, L_54)))))
		{
			goto IL_058b;
		}
	}
	{
		goto IL_1067;
	}

IL_058b:
	{
		V_17 = ((int32_t)(V_17+1));
	}

IL_0591:
	{
		if ((((int32_t)V_17) < ((int32_t)V_16)))
		{
			goto IL_0551;
		}
	}
	{
		if (V_14)
		{
			goto IL_05a6;
		}
	}
	{
		V_0 = ((int32_t)(V_0+V_16));
	}

IL_05a6:
	{
		goto IL_0fee;
	}

IL_05ab:
	{
		V_19 = ((((int32_t)((((int32_t)(((uint16_t)((int32_t)((int32_t)V_3&(int32_t)((int32_t)1024)))))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		V_20 = ((((int32_t)((((int32_t)(((uint16_t)((int32_t)((int32_t)V_3&(int32_t)((int32_t)512)))))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		t764* L_55 = (__this->f1);
		int32_t L_56 = ((int32_t)(p2+1));
		int32_t L_57 = m3683(__this, (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_55, L_56)), &m3683_MI);
		V_21 = L_57;
		if ((((int32_t)V_21) >= ((int32_t)0)))
		{
			goto IL_05ea;
		}
	}
	{
		goto IL_1067;
	}

IL_05ea:
	{
		t865* L_58 = (__this->f13);
		int32_t L_59 = m3651(((t859 *)(t859 *)SZArrayLdElema(L_58, V_21)), &m3651_MI);
		V_22 = L_59;
		t865* L_60 = (__this->f13);
		int32_t L_61 = m3652(((t859 *)(t859 *)SZArrayLdElema(L_60, V_21)), &m3652_MI);
		V_23 = L_61;
		if (!V_19)
		{
			goto IL_062f;
		}
	}
	{
		V_0 = ((int32_t)(V_0-V_23));
		if ((((int32_t)V_0) >= ((int32_t)0)))
		{
			goto IL_062a;
		}
	}
	{
		goto IL_1067;
	}

IL_062a:
	{
		goto IL_0643;
	}

IL_062f:
	{
		int32_t L_62 = (__this->f4);
		if ((((int32_t)((int32_t)(V_0+V_23))) <= ((int32_t)L_62)))
		{
			goto IL_0643;
		}
	}
	{
		goto IL_1067;
	}

IL_0643:
	{
		p2 = ((int32_t)(p2+2));
		if (!V_20)
		{
			goto IL_069e;
		}
	}
	{
		V_24 = 0;
		goto IL_0690;
	}

IL_0657:
	{
		t7* L_63 = (__this->f3);
		uint16_t L_64 = m1741(L_63, ((int32_t)(V_0+V_24)), &m1741_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t194_TI));
		uint16_t L_65 = m1809(NULL, L_64, &m1809_MI);
		t7* L_66 = (__this->f3);
		uint16_t L_67 = m1741(L_66, ((int32_t)(V_22+V_24)), &m1741_MI);
		uint16_t L_68 = m1809(NULL, L_67, &m1809_MI);
		if ((((int32_t)L_65) == ((int32_t)L_68)))
		{
			goto IL_068a;
		}
	}
	{
		goto IL_1067;
	}

IL_068a:
	{
		V_24 = ((int32_t)(V_24+1));
	}

IL_0690:
	{
		if ((((int32_t)V_24) < ((int32_t)V_23)))
		{
			goto IL_0657;
		}
	}
	{
		goto IL_06de;
	}

IL_069e:
	{
		V_25 = 0;
		goto IL_06d5;
	}

IL_06a6:
	{
		t7* L_69 = (__this->f3);
		uint16_t L_70 = m1741(L_69, ((int32_t)(V_0+V_25)), &m1741_MI);
		t7* L_71 = (__this->f3);
		uint16_t L_72 = m1741(L_71, ((int32_t)(V_22+V_25)), &m1741_MI);
		if ((((int32_t)L_70) == ((int32_t)L_72)))
		{
			goto IL_06cf;
		}
	}
	{
		goto IL_1067;
	}

IL_06cf:
	{
		V_25 = ((int32_t)(V_25+1));
	}

IL_06d5:
	{
		if ((((int32_t)V_25) < ((int32_t)V_23)))
		{
			goto IL_06a6;
		}
	}

IL_06de:
	{
		if (V_19)
		{
			goto IL_06ea;
		}
	}
	{
		V_0 = ((int32_t)(V_0+V_23));
	}

IL_06ea:
	{
		goto IL_0fee;
	}

IL_06ef:
	{
		bool L_73 = m3672(__this, p0, (&V_0), (&p2), 0, &m3672_MI);
		if (L_73)
		{
			goto IL_0705;
		}
	}
	{
		goto IL_1067;
	}

IL_0705:
	{
		goto IL_0fee;
	}

IL_070a:
	{
		t764* L_74 = (__this->f1);
		int32_t L_75 = ((int32_t)(p2+1));
		V_26 = ((int32_t)(p2+(*(uint16_t*)(uint16_t*)SZArrayLdElema(L_74, L_75))));
		p2 = ((int32_t)(p2+2));
		bool L_76 = m3672(__this, p0, (&V_0), (&p2), 1, &m3672_MI);
		if (L_76)
		{
			goto IL_0733;
		}
	}
	{
		goto IL_1067;
	}

IL_0733:
	{
		p2 = V_26;
		goto IL_0fee;
	}

IL_073c:
	{
		t764* L_77 = (__this->f1);
		int32_t L_78 = ((int32_t)(p2+1));
		m3677(__this, (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_77, L_78)), V_0, &m3677_MI);
		p2 = ((int32_t)(p2+2));
		goto IL_0fee;
	}

IL_0757:
	{
		t764* L_79 = (__this->f1);
		int32_t L_80 = ((int32_t)(p2+1));
		m3678(__this, (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_79, L_80)), V_0, &m3678_MI);
		p2 = ((int32_t)(p2+2));
		goto IL_0fee;
	}

IL_0772:
	{
		V_27 = V_0;
		bool L_81 = m3671(__this, 1, (&V_0), ((int32_t)(p2+5)), &m3671_MI);
		if (L_81)
		{
			goto IL_078b;
		}
	}
	{
		goto IL_1067;
	}

IL_078b:
	{
		t764* L_82 = (__this->f1);
		int32_t L_83 = ((int32_t)(p2+1));
		t764* L_84 = (__this->f1);
		int32_t L_85 = ((int32_t)(p2+2));
		t764* L_86 = (__this->f1);
		int32_t L_87 = ((int32_t)(p2+3));
		G_B161_0 = ((int32_t)((*(uint16_t*)(uint16_t*)SZArrayLdElema(L_84, L_85))));
		G_B161_1 = ((int32_t)((*(uint16_t*)(uint16_t*)SZArrayLdElema(L_82, L_83))));
		G_B161_2 = __this;
		if ((((uint32_t)(*(uint16_t*)(uint16_t*)SZArrayLdElema(L_86, L_87))) != ((uint32_t)1)))
		{
			G_B162_0 = ((int32_t)((*(uint16_t*)(uint16_t*)SZArrayLdElema(L_84, L_85))));
			G_B162_1 = ((int32_t)((*(uint16_t*)(uint16_t*)SZArrayLdElema(L_82, L_83))));
			G_B162_2 = __this;
			goto IL_07b6;
		}
	}
	{
		G_B163_0 = 1;
		G_B163_1 = G_B161_0;
		G_B163_2 = G_B161_1;
		G_B163_3 = G_B161_2;
		goto IL_07b7;
	}

IL_07b6:
	{
		G_B163_0 = 0;
		G_B163_1 = G_B162_0;
		G_B163_2 = G_B162_1;
		G_B163_3 = G_B162_2;
	}

IL_07b7:
	{
		bool L_88 = m3679(G_B163_3, G_B163_2, G_B163_1, G_B163_0, V_27, &m3679_MI);
		if (L_88)
		{
			goto IL_07c8;
		}
	}
	{
		goto IL_1067;
	}

IL_07c8:
	{
		t764* L_89 = (__this->f1);
		int32_t L_90 = ((int32_t)(p2+4));
		p2 = ((int32_t)(p2+(*(uint16_t*)(uint16_t*)SZArrayLdElema(L_89, L_90))));
		goto IL_0fee;
	}

IL_07db:
	{
		goto IL_0ff3;
	}

IL_07e0:
	{
		t764* L_91 = (__this->f1);
		int32_t L_92 = ((int32_t)(p2+2));
		int32_t L_93 = m3683(__this, (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_91, L_92)), &m3683_MI);
		V_28 = L_93;
		if ((((int32_t)V_28) >= ((int32_t)0)))
		{
			goto IL_080d;
		}
	}
	{
		t764* L_94 = (__this->f1);
		int32_t L_95 = ((int32_t)(p2+1));
		p2 = ((int32_t)(p2+(*(uint16_t*)(uint16_t*)SZArrayLdElema(L_94, L_95))));
		goto IL_0812;
	}

IL_080d:
	{
		p2 = ((int32_t)(p2+3));
	}

IL_0812:
	{
		goto IL_0fee;
	}

IL_0817:
	{
		bool L_96 = m3671(__this, 1, (&V_0), ((int32_t)(p2+2)), &m3671_MI);
		if (L_96)
		{
			goto IL_082d;
		}
	}
	{
		goto IL_1067;
	}

IL_082d:
	{
		t764* L_97 = (__this->f1);
		int32_t L_98 = ((int32_t)(p2+1));
		p2 = ((int32_t)(p2+(*(uint16_t*)(uint16_t*)SZArrayLdElema(L_97, L_98))));
		goto IL_0fee;
	}

IL_0840:
	{
		int32_t L_99 = m3680(__this, &m3680_MI);
		V_29 = L_99;
		V_30 = V_0;
		bool L_100 = m3671(__this, 1, (&V_30), ((int32_t)(p2+3)), &m3671_MI);
		if (!L_100)
		{
			goto IL_086f;
		}
	}
	{
		t764* L_101 = (__this->f1);
		int32_t L_102 = ((int32_t)(p2+1));
		p2 = ((int32_t)(p2+(*(uint16_t*)(uint16_t*)SZArrayLdElema(L_101, L_102))));
		goto IL_0885;
	}

IL_086f:
	{
		m3681(__this, V_29, &m3681_MI);
		t764* L_103 = (__this->f1);
		int32_t L_104 = ((int32_t)(p2+2));
		p2 = ((int32_t)(p2+(*(uint16_t*)(uint16_t*)SZArrayLdElema(L_103, L_104))));
	}

IL_0885:
	{
		goto IL_0fee;
	}

IL_088a:
	{
		int32_t L_105 = m3680(__this, &m3680_MI);
		V_32 = L_105;
		bool L_106 = m3671(__this, 1, (&V_0), ((int32_t)(p2+2)), &m3671_MI);
		if (!L_106)
		{
			goto IL_08a8;
		}
	}
	{
		goto IL_0ff3;
	}

IL_08a8:
	{
		m3681(__this, V_32, &m3681_MI);
		t764* L_107 = (__this->f1);
		int32_t L_108 = ((int32_t)(p2+1));
		p2 = ((int32_t)(p2+(*(uint16_t*)(uint16_t*)SZArrayLdElema(L_107, L_108))));
		t764* L_109 = (__this->f1);
		int32_t L_110 = p2;
		V_31 = (((uint16_t)((int32_t)((int32_t)(*(uint16_t*)(uint16_t*)SZArrayLdElema(L_109, L_110))&(int32_t)((int32_t)255)))));
		if (V_31)
		{
			goto IL_088a;
		}
	}
	{
		goto IL_1067;
	}

IL_08db:
	{
		t764* L_111 = (__this->f1);
		int32_t L_112 = ((int32_t)(p2+1));
		p2 = ((int32_t)(p2+(*(uint16_t*)(uint16_t*)SZArrayLdElema(L_111, L_112))));
		goto IL_0fee;
	}

IL_08ee:
	{
		t861 * L_113 = (__this->f9);
		int32_t L_114 = m3668(__this, ((int32_t)(p2+2)), &m3668_MI);
		int32_t L_115 = m3668(__this, ((int32_t)(p2+4)), &m3668_MI);
		t861 * L_116 = (t861 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t861_TI));
		m3657(L_116, L_113, L_114, L_115, ((((int32_t)((((int32_t)(((uint16_t)((int32_t)((int32_t)V_3&(int32_t)((int32_t)2048)))))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0), ((int32_t)(p2+6)), &m3657_MI);
		__this->f9 = L_116;
		t764* L_117 = (__this->f1);
		int32_t L_118 = ((int32_t)(p2+1));
		bool L_119 = m3671(__this, 1, (&V_0), ((int32_t)(p2+(*(uint16_t*)(uint16_t*)SZArrayLdElema(L_117, L_118)))), &m3671_MI);
		if (!L_119)
		{
			goto IL_0941;
		}
	}
	{
		goto IL_0ff3;
	}

IL_0941:
	{
		t861 * L_120 = (__this->f9);
		t861 * L_121 = m3666(L_120, &m3666_MI);
		__this->f9 = L_121;
		goto IL_1067;
	}

IL_0957:
	{
		t861 * L_122 = (__this->f9);
		V_33 = L_122;
		t861 * L_123 = (__this->f12);
		if ((((t861 *)L_123) != ((t861 *)V_33)))
		{
			goto IL_0971;
		}
	}
	{
		goto IL_0ff3;
	}

IL_0971:
	{
		int32_t L_124 = m3660(V_33, &m3660_MI);
		V_34 = L_124;
		int32_t L_125 = m3658(V_33, &m3658_MI);
		V_35 = L_125;
		goto IL_09e5;
	}

IL_0988:
	{
		t861 * L_126 = V_33;
		int32_t L_127 = m3658(L_126, &m3658_MI);
		m3659(L_126, ((int32_t)(L_127+1)), &m3659_MI);
		m3661(V_33, V_0, &m3661_MI);
		__this->f12 = V_33;
		int32_t L_128 = m3665(V_33, &m3665_MI);
		bool L_129 = m3671(__this, 1, (&V_0), L_128, &m3671_MI);
		if (L_129)
		{
			goto IL_09d3;
		}
	}
	{
		m3661(V_33, V_34, &m3661_MI);
		m3659(V_33, V_35, &m3659_MI);
		goto IL_1067;
	}

IL_09d3:
	{
		t861 * L_130 = (__this->f12);
		if ((((t861 *)L_130) == ((t861 *)V_33)))
		{
			goto IL_09e5;
		}
	}
	{
		goto IL_0ff3;
	}

IL_09e5:
	{
		bool L_131 = m3662(V_33, &m3662_MI);
		if (!L_131)
		{
			goto IL_0988;
		}
	}
	{
		int32_t L_132 = m3660(V_33, &m3660_MI);
		if ((((uint32_t)V_0) != ((uint32_t)L_132)))
		{
			goto IL_0a35;
		}
	}
	{
		t861 * L_133 = m3666(V_33, &m3666_MI);
		__this->f9 = L_133;
		__this->f12 = (t861 *)NULL;
		bool L_134 = m3671(__this, 1, (&V_0), ((int32_t)(p2+1)), &m3671_MI);
		if (!L_134)
		{
			goto IL_0a28;
		}
	}
	{
		goto IL_0ff3;
	}

IL_0a28:
	{
		__this->f9 = V_33;
		goto IL_1067;
	}

IL_0a35:
	{
		bool L_135 = m3664(V_33, &m3664_MI);
		if (!L_135)
		{
			goto IL_0b0d;
		}
	}
	{
		goto IL_0b08;
	}

IL_0a46:
	{
		t861 * L_136 = m3666(V_33, &m3666_MI);
		__this->f9 = L_136;
		__this->f12 = (t861 *)NULL;
		int32_t L_137 = m3680(__this, &m3680_MI);
		V_36 = L_137;
		bool L_138 = m3671(__this, 1, (&V_0), ((int32_t)(p2+1)), &m3671_MI);
		if (!L_138)
		{
			goto IL_0a78;
		}
	}
	{
		goto IL_0ff3;
	}

IL_0a78:
	{
		m3681(__this, V_36, &m3681_MI);
		__this->f9 = V_33;
		bool L_139 = m3663(V_33, &m3663_MI);
		if (!L_139)
		{
			goto IL_0a99;
		}
	}
	{
		goto IL_1067;
	}

IL_0a99:
	{
		t861 * L_140 = V_33;
		int32_t L_141 = m3658(L_140, &m3658_MI);
		m3659(L_140, ((int32_t)(L_141+1)), &m3659_MI);
		m3661(V_33, V_0, &m3661_MI);
		__this->f12 = V_33;
		int32_t L_142 = m3665(V_33, &m3665_MI);
		bool L_143 = m3671(__this, 1, (&V_0), L_142, &m3671_MI);
		if (L_143)
		{
			goto IL_0ae4;
		}
	}
	{
		m3661(V_33, V_34, &m3661_MI);
		m3659(V_33, V_35, &m3659_MI);
		goto IL_1067;
	}

IL_0ae4:
	{
		t861 * L_144 = (__this->f12);
		if ((((t861 *)L_144) == ((t861 *)V_33)))
		{
			goto IL_0af6;
		}
	}
	{
		goto IL_0ff3;
	}

IL_0af6:
	{
		int32_t L_145 = m3660(V_33, &m3660_MI);
		if ((((uint32_t)V_0) != ((uint32_t)L_145)))
		{
			goto IL_0b08;
		}
	}
	{
		goto IL_1067;
	}

IL_0b08:
	{
		goto IL_0a46;
	}

IL_0b0d:
	{
		t860 * L_146 = &(__this->f11);
		int32_t L_147 = m3655(L_146, &m3655_MI);
		V_37 = L_147;
		goto IL_0bd7;
	}

IL_0b1f:
	{
		int32_t L_148 = m3680(__this, &m3680_MI);
		V_38 = L_148;
		V_39 = V_0;
		int32_t L_149 = m3660(V_33, &m3660_MI);
		V_40 = L_149;
		t861 * L_150 = V_33;
		int32_t L_151 = m3658(L_150, &m3658_MI);
		m3659(L_150, ((int32_t)(L_151+1)), &m3659_MI);
		m3661(V_33, V_0, &m3661_MI);
		__this->f12 = V_33;
		int32_t L_152 = m3665(V_33, &m3665_MI);
		bool L_153 = m3671(__this, 1, (&V_0), L_152, &m3671_MI);
		if (L_153)
		{
			goto IL_0b8c;
		}
	}
	{
		t861 * L_154 = V_33;
		int32_t L_155 = m3658(L_154, &m3658_MI);
		m3659(L_154, ((int32_t)(L_155-1)), &m3659_MI);
		m3661(V_33, V_40, &m3661_MI);
		m3681(__this, V_38, &m3681_MI);
		goto IL_0be3;
	}

IL_0b8c:
	{
		t861 * L_156 = (__this->f12);
		if ((((t861 *)L_156) == ((t861 *)V_33)))
		{
			goto IL_0bab;
		}
	}
	{
		t860 * L_157 = &(__this->f11);
		m3656(L_157, V_37, &m3656_MI);
		goto IL_0ff3;
	}

IL_0bab:
	{
		t860 * L_158 = &(__this->f11);
		m3654(L_158, V_38, &m3654_MI);
		t860 * L_159 = &(__this->f11);
		m3654(L_159, V_39, &m3654_MI);
		int32_t L_160 = m3660(V_33, &m3660_MI);
		if ((((uint32_t)V_0) != ((uint32_t)L_160)))
		{
			goto IL_0bd7;
		}
	}
	{
		goto IL_0be3;
	}

IL_0bd7:
	{
		bool L_161 = m3663(V_33, &m3663_MI);
		if (!L_161)
		{
			goto IL_0b1f;
		}
	}

IL_0be3:
	{
		t861 * L_162 = m3666(V_33, &m3666_MI);
		__this->f9 = L_162;
		goto IL_0c6a;
	}

IL_0bf5:
	{
		__this->f12 = (t861 *)NULL;
		bool L_163 = m3671(__this, 1, (&V_0), ((int32_t)(p2+1)), &m3671_MI);
		if (!L_163)
		{
			goto IL_0c1f;
		}
	}
	{
		t860 * L_164 = &(__this->f11);
		m3656(L_164, V_37, &m3656_MI);
		goto IL_0ff3;
	}

IL_0c1f:
	{
		t860 * L_165 = &(__this->f11);
		int32_t L_166 = m3655(L_165, &m3655_MI);
		if ((((uint32_t)L_166) != ((uint32_t)V_37)))
		{
			goto IL_0c3e;
		}
	}
	{
		__this->f9 = V_33;
		goto IL_1067;
	}

IL_0c3e:
	{
		t861 * L_167 = V_33;
		int32_t L_168 = m3658(L_167, &m3658_MI);
		m3659(L_167, ((int32_t)(L_168-1)), &m3659_MI);
		t860 * L_169 = &(__this->f11);
		int32_t L_170 = m3653(L_169, &m3653_MI);
		V_0 = L_170;
		t860 * L_171 = &(__this->f11);
		int32_t L_172 = m3653(L_171, &m3653_MI);
		m3681(__this, L_172, &m3681_MI);
	}

IL_0c6a:
	{
		goto IL_0bf5;
	}

IL_0c6f:
	{
		t861 * L_173 = (__this->f10);
		int32_t L_174 = m3668(__this, ((int32_t)(p2+2)), &m3668_MI);
		int32_t L_175 = m3668(__this, ((int32_t)(p2+4)), &m3668_MI);
		t861 * L_176 = (t861 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t861_TI));
		m3657(L_176, L_173, L_174, L_175, ((((int32_t)((((int32_t)(((uint16_t)((int32_t)((int32_t)V_3&(int32_t)((int32_t)2048)))))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0), ((int32_t)(p2+6)), &m3657_MI);
		__this->f10 = L_176;
		t861 * L_177 = (__this->f10);
		m3661(L_177, V_0, &m3661_MI);
		int32_t L_178 = m3680(__this, &m3680_MI);
		V_41 = L_178;
		t764* L_179 = (__this->f1);
		int32_t L_180 = ((int32_t)(p2+1));
		p2 = ((int32_t)(p2+(*(uint16_t*)(uint16_t*)SZArrayLdElema(L_179, L_180))));
		t764* L_181 = (__this->f1);
		int32_t L_182 = p2;
		V_42 = (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_181, L_182));
		V_43 = (-1);
		V_44 = (-1);
		V_45 = 0;
		V_46 = (((uint16_t)((int32_t)((int32_t)V_42&(int32_t)((int32_t)255)))));
		if ((((int32_t)V_46) == ((int32_t)5)))
		{
			goto IL_0cf3;
		}
	}
	{
		if ((((uint32_t)V_46) != ((uint32_t)3)))
		{
			goto IL_0d92;
		}
	}

IL_0cf3:
	{
		V_47 = (((uint16_t)((int32_t)((int32_t)V_42&(int32_t)((int32_t)65280)))));
		if (!(((uint16_t)((int32_t)((int32_t)V_47&(int32_t)((int32_t)256))))))
		{
			goto IL_0d11;
		}
	}
	{
		goto IL_0d92;
	}

IL_0d11:
	{
		if ((((uint32_t)V_46) != ((uint32_t)3)))
		{
			goto IL_0d4c;
		}
	}
	{
		V_48 = 0;
		if (!(((uint16_t)((int32_t)((int32_t)V_47&(int32_t)((int32_t)1024))))))
		{
			goto IL_0d38;
		}
	}
	{
		t764* L_183 = (__this->f1);
		int32_t L_184 = ((int32_t)(p2+1));
		V_48 = ((int32_t)((*(uint16_t*)(uint16_t*)SZArrayLdElema(L_183, L_184))-1));
	}

IL_0d38:
	{
		t764* L_185 = (__this->f1);
		int32_t L_186 = ((int32_t)(((int32_t)(p2+2))+V_48));
		V_43 = (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_185, L_186));
		goto IL_0d58;
	}

IL_0d4c:
	{
		t764* L_187 = (__this->f1);
		int32_t L_188 = ((int32_t)(p2+1));
		V_43 = (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_187, L_188));
	}

IL_0d58:
	{
		if (!(((uint16_t)((int32_t)((int32_t)V_47&(int32_t)((int32_t)512))))))
		{
			goto IL_0d75;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t194_TI));
		uint16_t L_189 = m1807(NULL, (((uint16_t)V_43)), &m1807_MI);
		V_44 = L_189;
		goto IL_0d79;
	}

IL_0d75:
	{
		V_44 = V_43;
	}

IL_0d79:
	{
		if (!(((uint16_t)((int32_t)((int32_t)V_47&(int32_t)((int32_t)1024))))))
		{
			goto IL_0d8f;
		}
	}
	{
		V_45 = (-1);
		goto IL_0d92;
	}

IL_0d8f:
	{
		V_45 = 0;
	}

IL_0d92:
	{
		t861 * L_190 = (__this->f10);
		bool L_191 = m3664(L_190, &m3664_MI);
		if (!L_191)
		{
			goto IL_0ebf;
		}
	}
	{
		t861 * L_192 = (__this->f10);
		bool L_193 = m3662(L_192, &m3662_MI);
		if (L_193)
		{
			goto IL_0de1;
		}
	}
	{
		t861 * L_194 = (__this->f10);
		int32_t L_195 = m3665(L_194, &m3665_MI);
		bool L_196 = m3671(__this, 2, (&V_0), L_195, &m3671_MI);
		if (L_196)
		{
			goto IL_0de1;
		}
	}
	{
		t861 * L_197 = (__this->f10);
		t861 * L_198 = m3666(L_197, &m3666_MI);
		__this->f10 = L_198;
		goto IL_1067;
	}

IL_0de1:
	{
		V_49 = ((int32_t)(V_0+V_45));
		if ((((int32_t)V_43) < ((int32_t)0)))
		{
			goto IL_0e2c;
		}
	}
	{
		if ((((int32_t)V_49) < ((int32_t)0)))
		{
			goto IL_0e47;
		}
	}
	{
		int32_t L_199 = (__this->f4);
		if ((((int32_t)V_49) >= ((int32_t)L_199)))
		{
			goto IL_0e47;
		}
	}
	{
		t7* L_200 = (__this->f3);
		uint16_t L_201 = m1741(L_200, V_49, &m1741_MI);
		if ((((int32_t)V_43) == ((int32_t)L_201)))
		{
			goto IL_0e2c;
		}
	}
	{
		t7* L_202 = (__this->f3);
		uint16_t L_203 = m1741(L_202, V_49, &m1741_MI);
		if ((((uint32_t)V_44) != ((uint32_t)L_203)))
		{
			goto IL_0e47;
		}
	}

IL_0e2c:
	{
		__this->f12 = (t861 *)NULL;
		bool L_204 = m3671(__this, 1, (&V_0), p2, &m3671_MI);
		if (!L_204)
		{
			goto IL_0e47;
		}
	}
	{
		goto IL_0ea9;
	}

IL_0e47:
	{
		t861 * L_205 = (__this->f10);
		bool L_206 = m3663(L_205, &m3663_MI);
		if (!L_206)
		{
			goto IL_0e6d;
		}
	}
	{
		t861 * L_207 = (__this->f10);
		t861 * L_208 = m3666(L_207, &m3666_MI);
		__this->f10 = L_208;
		goto IL_1067;
	}

IL_0e6d:
	{
		m3681(__this, V_41, &m3681_MI);
		t861 * L_209 = (__this->f10);
		int32_t L_210 = m3665(L_209, &m3665_MI);
		bool L_211 = m3671(__this, 2, (&V_0), L_210, &m3671_MI);
		if (L_211)
		{
			goto IL_0ea4;
		}
	}
	{
		t861 * L_212 = (__this->f10);
		t861 * L_213 = m3666(L_212, &m3666_MI);
		__this->f10 = L_213;
		goto IL_1067;
	}

IL_0ea4:
	{
		goto IL_0de1;
	}

IL_0ea9:
	{
		t861 * L_214 = (__this->f10);
		t861 * L_215 = m3666(L_214, &m3666_MI);
		__this->f10 = L_215;
		goto IL_0ff3;
	}

IL_0ebf:
	{
		t861 * L_216 = (__this->f10);
		int32_t L_217 = m3665(L_216, &m3665_MI);
		bool L_218 = m3671(__this, 2, (&V_0), L_217, &m3671_MI);
		if (L_218)
		{
			goto IL_0eee;
		}
	}
	{
		t861 * L_219 = (__this->f10);
		t861 * L_220 = m3666(L_219, &m3666_MI);
		__this->f10 = L_220;
		goto IL_1067;
	}

IL_0eee:
	{
		t861 * L_221 = (__this->f10);
		int32_t L_222 = m3658(L_221, &m3658_MI);
		if ((((int32_t)L_222) <= ((int32_t)0)))
		{
			goto IL_0f1f;
		}
	}
	{
		t861 * L_223 = (__this->f10);
		int32_t L_224 = m3660(L_223, &m3660_MI);
		t861 * L_225 = (__this->f10);
		int32_t L_226 = m3658(L_225, &m3658_MI);
		V_50 = ((int32_t)((int32_t)((int32_t)(V_0-L_224))/(int32_t)L_226));
		goto IL_0f22;
	}

IL_0f1f:
	{
		V_50 = 0;
	}

IL_0f22:
	{
		V_51 = ((int32_t)(V_0+V_45));
		if ((((int32_t)V_43) < ((int32_t)0)))
		{
			goto IL_0f6d;
		}
	}
	{
		if ((((int32_t)V_51) < ((int32_t)0)))
		{
			goto IL_0f88;
		}
	}
	{
		int32_t L_227 = (__this->f4);
		if ((((int32_t)V_51) >= ((int32_t)L_227)))
		{
			goto IL_0f88;
		}
	}
	{
		t7* L_228 = (__this->f3);
		uint16_t L_229 = m1741(L_228, V_51, &m1741_MI);
		if ((((int32_t)V_43) == ((int32_t)L_229)))
		{
			goto IL_0f6d;
		}
	}
	{
		t7* L_230 = (__this->f3);
		uint16_t L_231 = m1741(L_230, V_51, &m1741_MI);
		if ((((uint32_t)V_44) != ((uint32_t)L_231)))
		{
			goto IL_0f88;
		}
	}

IL_0f6d:
	{
		__this->f12 = (t861 *)NULL;
		bool L_232 = m3671(__this, 1, (&V_0), p2, &m3671_MI);
		if (!L_232)
		{
			goto IL_0f88;
		}
	}
	{
		goto IL_0fd3;
	}

IL_0f88:
	{
		t861 * L_233 = (__this->f10);
		t861 * L_234 = L_233;
		int32_t L_235 = m3658(L_234, &m3658_MI);
		m3659(L_234, ((int32_t)(L_235-1)), &m3659_MI);
		t861 * L_236 = (__this->f10);
		bool L_237 = m3662(L_236, &m3662_MI);
		if (L_237)
		{
			goto IL_0fc1;
		}
	}
	{
		t861 * L_238 = (__this->f10);
		t861 * L_239 = m3666(L_238, &m3666_MI);
		__this->f10 = L_239;
		goto IL_1067;
	}

IL_0fc1:
	{
		V_0 = ((int32_t)(V_0-V_50));
		m3681(__this, V_41, &m3681_MI);
		goto IL_0f22;
	}

IL_0fd3:
	{
		t861 * L_240 = (__this->f10);
		t861 * L_241 = m3666(L_240, &m3666_MI);
		__this->f10 = L_241;
		goto IL_0ff3;
	}

IL_0fe9:
	{
		goto IL_1067;
	}

IL_0fee:
	{
		goto IL_0008;
	}

IL_0ff3:
	{
		*((int32_t*)(p1)) = (int32_t)V_0;
		V_54 = p0;
		if ((((int32_t)V_54) == ((int32_t)1)))
		{
			goto IL_100e;
		}
	}
	{
		if ((((int32_t)V_54) == ((int32_t)2)))
		{
			goto IL_1010;
		}
	}
	{
		goto IL_1067;
	}

IL_100e:
	{
		return 1;
	}

IL_1010:
	{
		t861 * L_242 = (__this->f10);
		t861 * L_243 = L_242;
		int32_t L_244 = m3658(L_243, &m3658_MI);
		m3659(L_243, ((int32_t)(L_244+1)), &m3659_MI);
		t861 * L_245 = (__this->f10);
		bool L_246 = m3663(L_245, &m3663_MI);
		if (L_246)
		{
			goto IL_1053;
		}
	}
	{
		t861 * L_247 = (__this->f10);
		bool L_248 = m3664(L_247, &m3664_MI);
		if (!L_248)
		{
			goto IL_1055;
		}
	}
	{
		t861 * L_249 = (__this->f10);
		bool L_250 = m3662(L_249, &m3662_MI);
		if (!L_250)
		{
			goto IL_1055;
		}
	}

IL_1053:
	{
		return 1;
	}

IL_1055:
	{
		t861 * L_251 = (__this->f10);
		int32_t L_252 = m3665(L_251, &m3665_MI);
		p2 = L_252;
		goto IL_0003;
	}

IL_1067:
	{
		V_54 = p0;
		if ((((int32_t)V_54) == ((int32_t)1)))
		{
			goto IL_107f;
		}
	}
	{
		if ((((int32_t)V_54) == ((int32_t)2)))
		{
			goto IL_1081;
		}
	}
	{
		goto IL_10b2;
	}

IL_107f:
	{
		return 0;
	}

IL_1081:
	{
		t861 * L_253 = (__this->f10);
		bool L_254 = m3664(L_253, &m3664_MI);
		if (L_254)
		{
			goto IL_10a3;
		}
	}
	{
		t861 * L_255 = (__this->f10);
		bool L_256 = m3662(L_255, &m3662_MI);
		if (!L_256)
		{
			goto IL_10a3;
		}
	}
	{
		return 1;
	}

IL_10a3:
	{
		t861 * L_257 = (__this->f10);
		int32_t L_258 = m3660(L_257, &m3660_MI);
		*((int32_t*)(p1)) = (int32_t)L_258;
		return 0;
	}

IL_10b2:
	{
		return 0;
	}
}
 bool m3672 (t863 * __this, int32_t p0, int32_t* p1, int32_t* p2, bool p3, MethodInfo* method){
	bool V_0 = false;
	uint16_t V_1 = 0x0;
	bool V_2 = false;
	bool V_3 = false;
	uint16_t V_4 = 0;
	uint16_t V_5 = {0};
	uint16_t V_6 = {0};
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	uint16_t V_14 = {0};
	{
		V_0 = 0;
		V_1 = 0;
	}

IL_0004:
	{
		t764* L_0 = (__this->f1);
		int32_t L_1 = (*((int32_t*)p2));
		V_4 = (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_0, L_1));
		V_5 = (((uint16_t)((int32_t)((int32_t)V_4&(int32_t)((int32_t)255)))));
		V_6 = (((uint16_t)((int32_t)((int32_t)V_4&(int32_t)((int32_t)65280)))));
		*((int32_t*)(p2)) = (int32_t)((int32_t)((*((int32_t*)p2))+1));
		V_3 = ((((int32_t)((((int32_t)(((uint16_t)((int32_t)((int32_t)V_6&(int32_t)((int32_t)512)))))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		if (V_0)
		{
			goto IL_00aa;
		}
	}
	{
		if (!(((uint16_t)((int32_t)((int32_t)V_6&(int32_t)((int32_t)1024))))))
		{
			goto IL_0075;
		}
	}
	{
		if ((((int32_t)(*((int32_t*)p1))) > ((int32_t)0)))
		{
			goto IL_0059;
		}
	}
	{
		return 0;
	}

IL_0059:
	{
		t7* L_2 = (__this->f3);
		int32_t L_3 = ((int32_t)((*((int32_t*)p1))-1));
		V_13 = L_3;
		*((int32_t*)(p1)) = (int32_t)L_3;
		uint16_t L_4 = m1741(L_2, V_13, &m1741_MI);
		V_1 = L_4;
		goto IL_009b;
	}

IL_0075:
	{
		int32_t L_5 = (__this->f4);
		if ((((int32_t)(*((int32_t*)p1))) < ((int32_t)L_5)))
		{
			goto IL_0084;
		}
	}
	{
		return 0;
	}

IL_0084:
	{
		t7* L_6 = (__this->f3);
		int32_t L_7 = (*((int32_t*)p1));
		V_13 = L_7;
		*((int32_t*)(p1)) = (int32_t)((int32_t)(L_7+1));
		uint16_t L_8 = m1741(L_6, V_13, &m1741_MI);
		V_1 = L_8;
	}

IL_009b:
	{
		if (!V_3)
		{
			goto IL_00a8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t194_TI));
		uint16_t L_9 = m1809(NULL, V_1, &m1809_MI);
		V_1 = L_9;
	}

IL_00a8:
	{
		V_0 = 1;
	}

IL_00aa:
	{
		V_2 = ((((int32_t)((((int32_t)(((uint16_t)((int32_t)((int32_t)V_6&(int32_t)((int32_t)256)))))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		V_14 = V_5;
		if (V_14 == 0)
		{
			goto IL_00f4;
		}
		if (V_14 == 1)
		{
			goto IL_00f2;
		}
		if (V_14 == 2)
		{
			goto IL_0221;
		}
		if (V_14 == 3)
		{
			goto IL_0221;
		}
		if (V_14 == 4)
		{
			goto IL_0221;
		}
		if (V_14 == 5)
		{
			goto IL_00f6;
		}
		if (V_14 == 6)
		{
			goto IL_0118;
		}
		if (V_14 == 7)
		{
			goto IL_013f;
		}
		if (V_14 == 8)
		{
			goto IL_0166;
		}
		if (V_14 == 9)
		{
			goto IL_01a8;
		}
	}
	{
		goto IL_0221;
	}

IL_00f2:
	{
		return 1;
	}

IL_00f4:
	{
		return 0;
	}

IL_00f6:
	{
		t764* L_10 = (__this->f1);
		int32_t L_11 = (*((int32_t*)p2));
		V_13 = L_11;
		*((int32_t*)(p2)) = (int32_t)((int32_t)(L_11+1));
		int32_t L_12 = V_13;
		if ((((uint32_t)V_1) != ((uint32_t)(*(uint16_t*)(uint16_t*)SZArrayLdElema(L_10, L_12)))))
		{
			goto IL_0113;
		}
	}
	{
		return ((((int32_t)V_2) == ((int32_t)0))? 1 : 0);
	}

IL_0113:
	{
		goto IL_0221;
	}

IL_0118:
	{
		t764* L_13 = (__this->f1);
		int32_t L_14 = (*((int32_t*)p2));
		V_13 = L_14;
		*((int32_t*)(p2)) = (int32_t)((int32_t)(L_14+1));
		int32_t L_15 = V_13;
		bool L_16 = m3588(NULL, (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_13, L_15)), V_1, &m3588_MI);
		if (!L_16)
		{
			goto IL_013a;
		}
	}
	{
		return ((((int32_t)V_2) == ((int32_t)0))? 1 : 0);
	}

IL_013a:
	{
		goto IL_0221;
	}

IL_013f:
	{
		t764* L_17 = (__this->f1);
		int32_t L_18 = (*((int32_t*)p2));
		V_13 = L_18;
		*((int32_t*)(p2)) = (int32_t)((int32_t)(L_18+1));
		int32_t L_19 = V_13;
		bool L_20 = m3588(NULL, (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_17, L_19)), V_1, &m3588_MI);
		if (L_20)
		{
			goto IL_0161;
		}
	}
	{
		return ((((int32_t)V_2) == ((int32_t)0))? 1 : 0);
	}

IL_0161:
	{
		goto IL_0221;
	}

IL_0166:
	{
		t764* L_21 = (__this->f1);
		int32_t L_22 = (*((int32_t*)p2));
		V_13 = L_22;
		*((int32_t*)(p2)) = (int32_t)((int32_t)(L_22+1));
		int32_t L_23 = V_13;
		V_7 = (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_21, L_23));
		t764* L_24 = (__this->f1);
		int32_t L_25 = (*((int32_t*)p2));
		V_13 = L_25;
		*((int32_t*)(p2)) = (int32_t)((int32_t)(L_25+1));
		int32_t L_26 = V_13;
		V_8 = (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_24, L_26));
		if ((((int32_t)V_7) > ((int32_t)V_1)))
		{
			goto IL_01a3;
		}
	}
	{
		if ((((int32_t)V_1) > ((int32_t)V_8)))
		{
			goto IL_01a3;
		}
	}
	{
		return ((((int32_t)V_2) == ((int32_t)0))? 1 : 0);
	}

IL_01a3:
	{
		goto IL_0221;
	}

IL_01a8:
	{
		t764* L_27 = (__this->f1);
		int32_t L_28 = (*((int32_t*)p2));
		V_13 = L_28;
		*((int32_t*)(p2)) = (int32_t)((int32_t)(L_28+1));
		int32_t L_29 = V_13;
		V_9 = (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_27, L_29));
		t764* L_30 = (__this->f1);
		int32_t L_31 = (*((int32_t*)p2));
		V_13 = L_31;
		*((int32_t*)(p2)) = (int32_t)((int32_t)(L_31+1));
		int32_t L_32 = V_13;
		V_10 = (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_30, L_32));
		V_11 = (*((int32_t*)p2));
		*((int32_t*)(p2)) = (int32_t)((int32_t)((*((int32_t*)p2))+V_10));
		V_12 = ((uint16_t)(V_1-V_9));
		if ((((int32_t)V_12) < ((int32_t)0)))
		{
			goto IL_01f4;
		}
	}
	{
		if ((((int32_t)V_12) < ((int32_t)((int32_t)((int32_t)V_10<<(int32_t)4)))))
		{
			goto IL_01f9;
		}
	}

IL_01f4:
	{
		goto IL_0221;
	}

IL_01f9:
	{
		t764* L_33 = (__this->f1);
		int32_t L_34 = ((int32_t)(V_11+((int32_t)((int32_t)V_12>>(int32_t)4))));
		if (!((int32_t)((int32_t)(*(uint16_t*)(uint16_t*)SZArrayLdElema(L_33, L_34))&(int32_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)V_12&(int32_t)((int32_t)15)))&(int32_t)((int32_t)31))))))))
		{
			goto IL_021c;
		}
	}
	{
		return ((((int32_t)V_2) == ((int32_t)0))? 1 : 0);
	}

IL_021c:
	{
		goto IL_0221;
	}

IL_0221:
	{
		if (p3)
		{
			goto IL_0004;
		}
	}
	{
		return V_2;
	}
}
 bool m3673 (t863 * __this, int32_t* p0, int32_t p1, MethodInfo* method){
	int32_t V_0 = 0;
	{
		m3670(__this, &m3670_MI);
		V_0 = (*((int32_t*)p0));
		t865* L_0 = (__this->f13);
		t841* L_1 = (__this->f16);
		int32_t L_2 = 0;
		((t859 *)(t859 *)SZArrayLdElema(L_0, (*(int32_t*)(int32_t*)SZArrayLdElema(L_1, L_2))))->f0 = V_0;
		bool L_3 = m3671(__this, 1, (&V_0), p1, &m3671_MI);
		if (!L_3)
		{
			goto IL_004f;
		}
	}
	{
		t865* L_4 = (__this->f13);
		t841* L_5 = (__this->f16);
		int32_t L_6 = 0;
		((t859 *)(t859 *)SZArrayLdElema(L_4, (*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_6))))->f1 = V_0;
		*((int32_t*)(p0)) = (int32_t)V_0;
		return 1;
	}

IL_004f:
	{
		return 0;
	}
}
 bool m3674 (t863 * __this, uint16_t p0, int32_t p1, MethodInfo* method){
	uint16_t V_0 = {0};
	int32_t G_B6_0 = 0;
	int32_t G_B12_0 = 0;
	int32_t G_B14_0 = 0;
	int32_t G_B18_0 = 0;
	{
		V_0 = p0;
		if (((uint16_t)(V_0-1)) == 0)
		{
			goto IL_0033;
		}
		if (((uint16_t)(V_0-1)) == 1)
		{
			goto IL_0033;
		}
		if (((uint16_t)(V_0-1)) == 2)
		{
			goto IL_0038;
		}
		if (((uint16_t)(V_0-1)) == 3)
		{
			goto IL_0054;
		}
		if (((uint16_t)(V_0-1)) == 4)
		{
			goto IL_005e;
		}
		if (((uint16_t)(V_0-1)) == 5)
		{
			goto IL_00af;
		}
		if (((uint16_t)(V_0-1)) == 6)
		{
			goto IL_008f;
		}
		if (((uint16_t)(V_0-1)) == 7)
		{
			goto IL_00b9;
		}
		if (((uint16_t)(V_0-1)) == 8)
		{
			goto IL_012c;
		}
	}
	{
		goto IL_01a2;
	}

IL_0033:
	{
		return ((((int32_t)p1) == ((int32_t)0))? 1 : 0);
	}

IL_0038:
	{
		if (!p1)
		{
			goto IL_0052;
		}
	}
	{
		t7* L_0 = (__this->f3);
		uint16_t L_1 = m1741(L_0, ((int32_t)(p1-1)), &m1741_MI);
		G_B6_0 = ((((int32_t)L_1) == ((int32_t)((int32_t)10)))? 1 : 0);
		goto IL_0053;
	}

IL_0052:
	{
		G_B6_0 = 1;
	}

IL_0053:
	{
		return G_B6_0;
	}

IL_0054:
	{
		int32_t L_2 = (__this->f8);
		return ((((int32_t)p1) == ((int32_t)L_2))? 1 : 0);
	}

IL_005e:
	{
		int32_t L_3 = (__this->f4);
		if ((((int32_t)p1) == ((int32_t)L_3)))
		{
			goto IL_008d;
		}
	}
	{
		int32_t L_4 = (__this->f4);
		if ((((uint32_t)p1) != ((uint32_t)((int32_t)(L_4-1)))))
		{
			goto IL_008a;
		}
	}
	{
		t7* L_5 = (__this->f3);
		uint16_t L_6 = m1741(L_5, p1, &m1741_MI);
		G_B12_0 = ((((int32_t)L_6) == ((int32_t)((int32_t)10)))? 1 : 0);
		goto IL_008b;
	}

IL_008a:
	{
		G_B12_0 = 0;
	}

IL_008b:
	{
		G_B14_0 = G_B12_0;
		goto IL_008e;
	}

IL_008d:
	{
		G_B14_0 = 1;
	}

IL_008e:
	{
		return G_B14_0;
	}

IL_008f:
	{
		int32_t L_7 = (__this->f4);
		if ((((int32_t)p1) == ((int32_t)L_7)))
		{
			goto IL_00ad;
		}
	}
	{
		t7* L_8 = (__this->f3);
		uint16_t L_9 = m1741(L_8, p1, &m1741_MI);
		G_B18_0 = ((((int32_t)L_9) == ((int32_t)((int32_t)10)))? 1 : 0);
		goto IL_00ae;
	}

IL_00ad:
	{
		G_B18_0 = 1;
	}

IL_00ae:
	{
		return G_B18_0;
	}

IL_00af:
	{
		int32_t L_10 = (__this->f4);
		return ((((int32_t)p1) == ((int32_t)L_10))? 1 : 0);
	}

IL_00b9:
	{
		int32_t L_11 = (__this->f4);
		if (L_11)
		{
			goto IL_00c6;
		}
	}
	{
		return 0;
	}

IL_00c6:
	{
		if (p1)
		{
			goto IL_00df;
		}
	}
	{
		t7* L_12 = (__this->f3);
		uint16_t L_13 = m1741(L_12, p1, &m1741_MI);
		bool L_14 = m3675(__this, L_13, &m3675_MI);
		return L_14;
	}

IL_00df:
	{
		int32_t L_15 = (__this->f4);
		if ((((uint32_t)p1) != ((uint32_t)L_15)))
		{
			goto IL_0100;
		}
	}
	{
		t7* L_16 = (__this->f3);
		uint16_t L_17 = m1741(L_16, ((int32_t)(p1-1)), &m1741_MI);
		bool L_18 = m3675(__this, L_17, &m3675_MI);
		return L_18;
	}

IL_0100:
	{
		t7* L_19 = (__this->f3);
		uint16_t L_20 = m1741(L_19, p1, &m1741_MI);
		bool L_21 = m3675(__this, L_20, &m3675_MI);
		t7* L_22 = (__this->f3);
		uint16_t L_23 = m1741(L_22, ((int32_t)(p1-1)), &m1741_MI);
		bool L_24 = m3675(__this, L_23, &m3675_MI);
		return ((((int32_t)((((int32_t)L_21) == ((int32_t)L_24))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}

IL_012c:
	{
		int32_t L_25 = (__this->f4);
		if (L_25)
		{
			goto IL_0139;
		}
	}
	{
		return 0;
	}

IL_0139:
	{
		if (p1)
		{
			goto IL_0155;
		}
	}
	{
		t7* L_26 = (__this->f3);
		uint16_t L_27 = m1741(L_26, p1, &m1741_MI);
		bool L_28 = m3675(__this, L_27, &m3675_MI);
		return ((((int32_t)L_28) == ((int32_t)0))? 1 : 0);
	}

IL_0155:
	{
		int32_t L_29 = (__this->f4);
		if ((((uint32_t)p1) != ((uint32_t)L_29)))
		{
			goto IL_0179;
		}
	}
	{
		t7* L_30 = (__this->f3);
		uint16_t L_31 = m1741(L_30, ((int32_t)(p1-1)), &m1741_MI);
		bool L_32 = m3675(__this, L_31, &m3675_MI);
		return ((((int32_t)L_32) == ((int32_t)0))? 1 : 0);
	}

IL_0179:
	{
		t7* L_33 = (__this->f3);
		uint16_t L_34 = m1741(L_33, p1, &m1741_MI);
		bool L_35 = m3675(__this, L_34, &m3675_MI);
		t7* L_36 = (__this->f3);
		uint16_t L_37 = m1741(L_36, ((int32_t)(p1-1)), &m1741_MI);
		bool L_38 = m3675(__this, L_37, &m3675_MI);
		return ((((int32_t)L_35) == ((int32_t)L_38))? 1 : 0);
	}

IL_01a2:
	{
		return 0;
	}
}
 bool m3675 (t863 * __this, uint16_t p0, MethodInfo* method){
	{
		bool L_0 = m3588(NULL, 3, p0, &m3588_MI);
		return L_0;
	}
}
 t7* m3676 (t863 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	t200* V_2 = {0};
	int32_t V_3 = 0;
	{
		t764* L_0 = (__this->f1);
		int32_t L_1 = ((int32_t)(p0+1));
		V_0 = (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_0, L_1));
		V_1 = ((int32_t)(p0+2));
		V_2 = ((t200*)SZArrayNew(InitializedTypeInfo(&t200_TI), V_0));
		V_3 = 0;
		goto IL_0030;
	}

IL_001d:
	{
		t764* L_2 = (__this->f1);
		int32_t L_3 = V_1;
		V_1 = ((int32_t)(L_3+1));
		int32_t L_4 = L_3;
		*((uint16_t*)(uint16_t*)SZArrayLdElema(V_2, V_3)) = (uint16_t)(*(uint16_t*)(uint16_t*)SZArrayLdElema(L_2, L_4));
		V_3 = ((int32_t)(V_3+1));
	}

IL_0030:
	{
		if ((((int32_t)V_3) < ((int32_t)V_0)))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_5 = (t7*)il2cpp_codegen_object_new ((&t7_TI));
		L_5 = m4262(L_5, V_2, &m4261_MI);
		return L_5;
	}
}
 void m3677 (t863 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	int32_t V_0 = 0;
	{
		t841* L_0 = (__this->f16);
		int32_t L_1 = p0;
		V_0 = (*(int32_t*)(int32_t*)SZArrayLdElema(L_0, L_1));
		int32_t L_2 = (__this->f14);
		if ((((int32_t)V_0) < ((int32_t)L_2)))
		{
			goto IL_002b;
		}
	}
	{
		t865* L_3 = (__this->f13);
		bool L_4 = m3650(((t859 *)(t859 *)SZArrayLdElema(L_3, V_0)), &m3650_MI);
		if (!L_4)
		{
			goto IL_003c;
		}
	}

IL_002b:
	{
		int32_t L_5 = m3684(__this, V_0, &m3684_MI);
		V_0 = L_5;
		t841* L_6 = (__this->f16);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_6, p0)) = (int32_t)V_0;
	}

IL_003c:
	{
		t865* L_7 = (__this->f13);
		((t859 *)(t859 *)SZArrayLdElema(L_7, V_0))->f0 = p1;
		return;
	}
}
 void m3678 (t863 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	{
		t865* L_0 = (__this->f13);
		t841* L_1 = (__this->f16);
		int32_t L_2 = p0;
		((t859 *)(t859 *)SZArrayLdElema(L_0, (*(int32_t*)(int32_t*)SZArrayLdElema(L_1, L_2))))->f1 = p1;
		return;
	}
}
 bool m3679 (t863 * __this, int32_t p0, int32_t p1, bool p2, int32_t p3, MethodInfo* method){
	int32_t V_0 = 0;
	{
		t841* L_0 = (__this->f16);
		int32_t L_1 = p1;
		V_0 = (*(int32_t*)(int32_t*)SZArrayLdElema(L_0, L_1));
		if ((((int32_t)V_0) == ((int32_t)(-1))))
		{
			goto IL_0027;
		}
	}
	{
		t865* L_2 = (__this->f13);
		int32_t L_3 = m3651(((t859 *)(t859 *)SZArrayLdElema(L_2, V_0)), &m3651_MI);
		if ((((int32_t)L_3) >= ((int32_t)0)))
		{
			goto IL_0029;
		}
	}

IL_0027:
	{
		return 0;
	}

IL_0029:
	{
		if ((((int32_t)p0) <= ((int32_t)0)))
		{
			goto IL_0069;
		}
	}
	{
		if (!p2)
		{
			goto IL_0069;
		}
	}
	{
		t865* L_4 = (__this->f13);
		int32_t L_5 = m3651(((t859 *)(t859 *)SZArrayLdElema(L_4, V_0)), &m3651_MI);
		t865* L_6 = (__this->f13);
		int32_t L_7 = m3652(((t859 *)(t859 *)SZArrayLdElema(L_6, V_0)), &m3652_MI);
		m3677(__this, p0, ((int32_t)(L_5+L_7)), &m3677_MI);
		m3678(__this, p0, p3, &m3678_MI);
	}

IL_0069:
	{
		t841* L_8 = (__this->f16);
		t865* L_9 = (__this->f13);
		int32_t L_10 = (((t859 *)(t859 *)SZArrayLdElema(L_9, V_0))->f2);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_8, p1)) = (int32_t)L_10;
		return 1;
	}
}
 int32_t m3680 (t863 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f15);
		__this->f14 = L_0;
		int32_t L_1 = (__this->f14);
		return L_1;
	}
}
 void m3681 (t863 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		goto IL_003b;
	}

IL_0007:
	{
		t841* L_0 = (__this->f16);
		int32_t L_1 = V_0;
		V_1 = (*(int32_t*)(int32_t*)SZArrayLdElema(L_0, L_1));
		goto IL_0027;
	}

IL_0015:
	{
		t865* L_2 = (__this->f13);
		int32_t L_3 = (((t859 *)(t859 *)SZArrayLdElema(L_2, V_1))->f2);
		V_1 = L_3;
	}

IL_0027:
	{
		if ((((int32_t)p0) <= ((int32_t)V_1)))
		{
			goto IL_0015;
		}
	}
	{
		t841* L_4 = (__this->f16);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_4, V_0)) = (int32_t)V_1;
		V_0 = ((int32_t)(V_0+1));
	}

IL_003b:
	{
		t841* L_5 = (__this->f16);
		if ((((int32_t)V_0) < ((int32_t)(((int32_t)(((t20 *)L_5)->max_length))))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
 void m3682 (t863 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		t841* L_0 = (__this->f16);
		V_0 = (((int32_t)(((t20 *)L_0)->max_length)));
		t865* L_1 = (__this->f13);
		if (L_1)
		{
			goto IL_0023;
		}
	}
	{
		__this->f13 = ((t865*)SZArrayNew(InitializedTypeInfo(&t865_TI), ((int32_t)((int32_t)V_0*(int32_t)((int32_t)10)))));
	}

IL_0023:
	{
		V_1 = 0;
		goto IL_006d;
	}

IL_002a:
	{
		t841* L_2 = (__this->f16);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_2, V_1)) = (int32_t)V_1;
		t865* L_3 = (__this->f13);
		((t859 *)(t859 *)SZArrayLdElema(L_3, V_1))->f0 = (-1);
		t865* L_4 = (__this->f13);
		((t859 *)(t859 *)SZArrayLdElema(L_4, V_1))->f1 = (-1);
		t865* L_5 = (__this->f13);
		((t859 *)(t859 *)SZArrayLdElema(L_5, V_1))->f2 = (-1);
		V_1 = ((int32_t)(V_1+1));
	}

IL_006d:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_002a;
		}
	}
	{
		__this->f14 = 0;
		__this->f15 = V_0;
		return;
	}
}
 int32_t m3683 (t863 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		t841* L_0 = (__this->f16);
		int32_t L_1 = p0;
		V_0 = (*(int32_t*)(int32_t*)SZArrayLdElema(L_0, L_1));
		goto IL_0020;
	}

IL_000e:
	{
		t865* L_2 = (__this->f13);
		int32_t L_3 = (((t859 *)(t859 *)SZArrayLdElema(L_2, V_0))->f2);
		V_0 = L_3;
	}

IL_0020:
	{
		if ((((int32_t)V_0) < ((int32_t)0)))
		{
			goto IL_003d;
		}
	}
	{
		t865* L_4 = (__this->f13);
		bool L_5 = m3650(((t859 *)(t859 *)SZArrayLdElema(L_4, V_0)), &m3650_MI);
		if (!L_5)
		{
			goto IL_000e;
		}
	}

IL_003d:
	{
		return V_0;
	}
}
 int32_t m3684 (t863 * __this, int32_t p0, MethodInfo* method){
	t865* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = (__this->f15);
		t865* L_1 = (__this->f13);
		if ((((uint32_t)L_0) != ((uint32_t)(((int32_t)(((t20 *)L_1)->max_length))))))
		{
			goto IL_0037;
		}
	}
	{
		t865* L_2 = (__this->f13);
		V_0 = ((t865*)SZArrayNew(InitializedTypeInfo(&t865_TI), ((int32_t)((int32_t)(((int32_t)(((t20 *)L_2)->max_length)))*(int32_t)2))));
		t865* L_3 = (__this->f13);
		VirtActionInvoker2< t20 *, int32_t >::Invoke(&m4199_MI, L_3, (t20 *)(t20 *)V_0, 0);
		__this->f13 = V_0;
	}

IL_0037:
	{
		int32_t L_4 = (__this->f15);
		int32_t L_5 = L_4;
		V_2 = L_5;
		__this->f15 = ((int32_t)(L_5+1));
		V_1 = V_2;
		t865* L_6 = (__this->f13);
		t865* L_7 = (__this->f13);
		int32_t L_8 = (-1);
		V_2 = L_8;
		((t859 *)(t859 *)SZArrayLdElema(L_7, V_1))->f1 = L_8;
		((t859 *)(t859 *)SZArrayLdElema(L_6, V_1))->f0 = V_2;
		t865* L_9 = (__this->f13);
		((t859 *)(t859 *)SZArrayLdElema(L_9, V_1))->f2 = p0;
		return V_1;
	}
}
 void m3685 (t863 * __this, int32_t p0, int32_t* p1, int32_t* p2, MethodInfo* method){
	int32_t V_0 = 0;
	{
		*((int32_t*)(p1)) = (int32_t)(-1);
		*((int32_t*)(p2)) = (int32_t)0;
		t841* L_0 = (__this->f16);
		int32_t L_1 = p0;
		V_0 = (*(int32_t*)(int32_t*)SZArrayLdElema(L_0, L_1));
		goto IL_0052;
	}

IL_0014:
	{
		t865* L_2 = (__this->f13);
		bool L_3 = m3650(((t859 *)(t859 *)SZArrayLdElema(L_2, V_0)), &m3650_MI);
		if (L_3)
		{
			goto IL_002f;
		}
	}
	{
		goto IL_0040;
	}

IL_002f:
	{
		if ((((int32_t)(*((int32_t*)p1))) >= ((int32_t)0)))
		{
			goto IL_003a;
		}
	}
	{
		*((int32_t*)(p1)) = (int32_t)V_0;
	}

IL_003a:
	{
		*((int32_t*)(p2)) = (int32_t)((int32_t)((*((int32_t*)p2))+1));
	}

IL_0040:
	{
		t865* L_4 = (__this->f13);
		int32_t L_5 = (((t859 *)(t859 *)SZArrayLdElema(L_4, V_0))->f2);
		V_0 = L_5;
	}

IL_0052:
	{
		if ((((int32_t)V_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return;
	}
}
 void m3686 (t863 * __this, t833 * p0, int32_t p1, int32_t p2, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	t830 * V_2 = {0};
	{
		V_0 = 1;
		t865* L_0 = (__this->f13);
		int32_t L_1 = (((t859 *)(t859 *)SZArrayLdElema(L_0, p1))->f2);
		V_1 = L_1;
		goto IL_0089;
	}

IL_0019:
	{
		t865* L_2 = (__this->f13);
		bool L_3 = m3650(((t859 *)(t859 *)SZArrayLdElema(L_2, V_1)), &m3650_MI);
		if (L_3)
		{
			goto IL_0034;
		}
	}
	{
		goto IL_0077;
	}

IL_0034:
	{
		t7* L_4 = (__this->f3);
		t865* L_5 = (__this->f13);
		int32_t L_6 = m3651(((t859 *)(t859 *)SZArrayLdElema(L_5, V_1)), &m3651_MI);
		t865* L_7 = (__this->f13);
		int32_t L_8 = m3652(((t859 *)(t859 *)SZArrayLdElema(L_7, V_1)), &m3652_MI);
		t830 * L_9 = (t830 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t830_TI));
		m3504(L_9, L_4, L_6, L_8, &m3504_MI);
		V_2 = L_9;
		t831 * L_10 = m3521(p0, &m3521_MI);
		m3513(L_10, V_2, ((int32_t)(((int32_t)(p2-1))-V_0)), &m3513_MI);
		V_0 = ((int32_t)(V_0+1));
	}

IL_0077:
	{
		t865* L_11 = (__this->f13);
		int32_t L_12 = (((t859 *)(t859 *)SZArrayLdElema(L_11, V_1))->f2);
		V_1 = L_12;
	}

IL_0089:
	{
		if ((((int32_t)V_1) >= ((int32_t)0)))
		{
			goto IL_0019;
		}
	}
	{
		return;
	}
}
 t828 * m3687 (t863 * __this, t829 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	t833 * V_2 = {0};
	t828 * V_3 = {0};
	int32_t V_4 = 0;
	{
		m3685(__this, 0, (&V_1), (&V_0), &m3685_MI);
		bool L_0 = (__this->f0);
		if (L_0)
		{
			goto IL_004d;
		}
	}
	{
		t7* L_1 = (__this->f3);
		int32_t L_2 = (__this->f4);
		t865* L_3 = (__this->f13);
		int32_t L_4 = m3651(((t859 *)(t859 *)SZArrayLdElema(L_3, V_1)), &m3651_MI);
		t865* L_5 = (__this->f13);
		int32_t L_6 = m3652(((t859 *)(t859 *)SZArrayLdElema(L_5, V_1)), &m3652_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t828_TI));
		t828 * L_7 = (t828 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t828_TI));
		m3532(L_7, p0, __this, L_1, L_2, 0, L_4, L_6, &m3532_MI);
		return L_7;
	}

IL_004d:
	{
		t7* L_8 = (__this->f3);
		int32_t L_9 = (__this->f4);
		t841* L_10 = (__this->f16);
		t865* L_11 = (__this->f13);
		int32_t L_12 = m3651(((t859 *)(t859 *)SZArrayLdElema(L_11, V_1)), &m3651_MI);
		t865* L_13 = (__this->f13);
		int32_t L_14 = m3652(((t859 *)(t859 *)SZArrayLdElema(L_13, V_1)), &m3652_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t828_TI));
		t828 * L_15 = (t828 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t828_TI));
		m3533(L_15, p0, __this, L_8, L_9, (((int32_t)(((t20 *)L_10)->max_length))), L_12, L_14, V_0, &m3533_MI);
		V_3 = L_15;
		m3686(__this, V_3, V_1, V_0, &m3686_MI);
		V_4 = 1;
		goto IL_0107;
	}

IL_009d:
	{
		m3685(__this, V_4, (&V_1), (&V_0), &m3685_MI);
		if ((((int32_t)V_1) >= ((int32_t)0)))
		{
			goto IL_00bb;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t833_TI));
		V_2 = (((t833_SFs*)InitializedTypeInfo(&t833_TI)->static_fields)->f3);
		goto IL_00f3;
	}

IL_00bb:
	{
		t7* L_16 = (__this->f3);
		t865* L_17 = (__this->f13);
		int32_t L_18 = m3651(((t859 *)(t859 *)SZArrayLdElema(L_17, V_1)), &m3651_MI);
		t865* L_19 = (__this->f13);
		int32_t L_20 = m3652(((t859 *)(t859 *)SZArrayLdElema(L_19, V_1)), &m3652_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t833_TI));
		t833 * L_21 = (t833 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t833_TI));
		m3517(L_21, L_16, L_18, L_20, V_0, &m3517_MI);
		V_2 = L_21;
		m3686(__this, V_2, V_1, V_0, &m3686_MI);
	}

IL_00f3:
	{
		t834 * L_22 = (t834 *)VirtFuncInvoker0< t834 * >::Invoke(&m3536_MI, V_3);
		m3527(L_22, V_2, V_4, &m3527_MI);
		V_4 = ((int32_t)(V_4+1));
	}

IL_0107:
	{
		t841* L_23 = (__this->f16);
		if ((((int32_t)V_4) < ((int32_t)(((int32_t)(((t20 *)L_23)->max_length))))))
		{
			goto IL_009d;
		}
	}
	{
		return V_3;
	}
}
// Metadata Definition System.Text.RegularExpressions.Interpreter
extern Il2CppType t764_0_0_1;
FieldInfo t863_f1_FieldInfo = 
{
	"program", &t764_0_0_1, &t863_TI, offsetof(t863, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t863_f2_FieldInfo = 
{
	"program_start", &t44_0_0_1, &t863_TI, offsetof(t863, f2), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_1;
FieldInfo t863_f3_FieldInfo = 
{
	"text", &t7_0_0_1, &t863_TI, offsetof(t863, f3), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t863_f4_FieldInfo = 
{
	"text_end", &t44_0_0_1, &t863_TI, offsetof(t863, f4), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t863_f5_FieldInfo = 
{
	"group_count", &t44_0_0_1, &t863_TI, offsetof(t863, f5), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t863_f6_FieldInfo = 
{
	"match_min", &t44_0_0_1, &t863_TI, offsetof(t863, f6), &EmptyCustomAttributesCache};
extern Il2CppType t864_0_0_1;
FieldInfo t863_f7_FieldInfo = 
{
	"qs", &t864_0_0_1, &t863_TI, offsetof(t863, f7), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t863_f8_FieldInfo = 
{
	"scan_ptr", &t44_0_0_1, &t863_TI, offsetof(t863, f8), &EmptyCustomAttributesCache};
extern Il2CppType t861_0_0_1;
FieldInfo t863_f9_FieldInfo = 
{
	"repeat", &t861_0_0_1, &t863_TI, offsetof(t863, f9), &EmptyCustomAttributesCache};
extern Il2CppType t861_0_0_1;
FieldInfo t863_f10_FieldInfo = 
{
	"fast", &t861_0_0_1, &t863_TI, offsetof(t863, f10), &EmptyCustomAttributesCache};
extern Il2CppType t860_0_0_1;
FieldInfo t863_f11_FieldInfo = 
{
	"stack", &t860_0_0_1, &t863_TI, offsetof(t863, f11), &EmptyCustomAttributesCache};
extern Il2CppType t861_0_0_1;
FieldInfo t863_f12_FieldInfo = 
{
	"deep", &t861_0_0_1, &t863_TI, offsetof(t863, f12), &EmptyCustomAttributesCache};
extern Il2CppType t865_0_0_1;
FieldInfo t863_f13_FieldInfo = 
{
	"marks", &t865_0_0_1, &t863_TI, offsetof(t863, f13), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t863_f14_FieldInfo = 
{
	"mark_start", &t44_0_0_1, &t863_TI, offsetof(t863, f14), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t863_f15_FieldInfo = 
{
	"mark_end", &t44_0_0_1, &t863_TI, offsetof(t863, f15), &EmptyCustomAttributesCache};
extern Il2CppType t841_0_0_1;
FieldInfo t863_f16_FieldInfo = 
{
	"groups", &t841_0_0_1, &t863_TI, offsetof(t863, f16), &EmptyCustomAttributesCache};
static FieldInfo* t863_FIs[] =
{
	&t863_f1_FieldInfo,
	&t863_f2_FieldInfo,
	&t863_f3_FieldInfo,
	&t863_f4_FieldInfo,
	&t863_f5_FieldInfo,
	&t863_f6_FieldInfo,
	&t863_f7_FieldInfo,
	&t863_f8_FieldInfo,
	&t863_f9_FieldInfo,
	&t863_f10_FieldInfo,
	&t863_f11_FieldInfo,
	&t863_f12_FieldInfo,
	&t863_f13_FieldInfo,
	&t863_f14_FieldInfo,
	&t863_f15_FieldInfo,
	&t863_f16_FieldInfo,
	NULL
};
extern Il2CppType t764_0_0_0;
static ParameterInfo t863_m3667_ParameterInfos[] = 
{
	{"program", 0, 134218312, &EmptyCustomAttributesCache, &t764_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3667_MI = 
{
	".ctor", (methodPointerType)&m3667, &t863_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t863_m3667_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 647, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t863_m3668_ParameterInfos[] = 
{
	{"ptr", 0, 134218313, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3668_MI = 
{
	"ReadProgramCount", (methodPointerType)&m3668, &t863_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t863_m3668_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 648, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t829_0_0_0;
extern Il2CppType t829_0_0_0;
extern Il2CppType t7_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t863_m3669_ParameterInfos[] = 
{
	{"regex", 0, 134218314, &EmptyCustomAttributesCache, &t829_0_0_0},
	{"text", 1, 134218315, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"start", 2, 134218316, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"end", 3, 134218317, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t828_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3669_MI = 
{
	"Scan", (methodPointerType)&m3669, &t863_TI, &t828_0_0_0, RuntimeInvoker_t29_t29_t29_t44_t44, t863_m3669_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 4, false, false, 649, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3670_MI = 
{
	"Reset", (methodPointerType)&m3670, &t863_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 650, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t862_0_0_0;
extern Il2CppType t44_1_0_0;
extern Il2CppType t44_1_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t863_m3671_ParameterInfos[] = 
{
	{"mode", 0, 134218318, &EmptyCustomAttributesCache, &t862_0_0_0},
	{"ref_ptr", 1, 134218319, &EmptyCustomAttributesCache, &t44_1_0_0},
	{"pc", 2, 134218320, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44_t390_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3671_MI = 
{
	"Eval", (methodPointerType)&m3671, &t863_TI, &t40_0_0_0, RuntimeInvoker_t40_t44_t390_t44, t863_m3671_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, false, 651, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t862_0_0_0;
extern Il2CppType t44_1_0_0;
extern Il2CppType t44_1_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t863_m3672_ParameterInfos[] = 
{
	{"mode", 0, 134218321, &EmptyCustomAttributesCache, &t862_0_0_0},
	{"ptr", 1, 134218322, &EmptyCustomAttributesCache, &t44_1_0_0},
	{"pc", 2, 134218323, &EmptyCustomAttributesCache, &t44_1_0_0},
	{"multi", 3, 134218324, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44_t390_t390_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3672_MI = 
{
	"EvalChar", (methodPointerType)&m3672, &t863_TI, &t40_0_0_0, RuntimeInvoker_t40_t44_t390_t390_t297, t863_m3672_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 4, false, false, 652, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_1_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t863_m3673_ParameterInfos[] = 
{
	{"ref_ptr", 0, 134218325, &EmptyCustomAttributesCache, &t44_1_0_0},
	{"pc", 1, 134218326, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t390_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3673_MI = 
{
	"TryMatch", (methodPointerType)&m3673, &t863_TI, &t40_0_0_0, RuntimeInvoker_t40_t390_t44, t863_m3673_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 653, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t845_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t863_m3674_ParameterInfos[] = 
{
	{"pos", 0, 134218327, &EmptyCustomAttributesCache, &t845_0_0_0},
	{"ptr", 1, 134218328, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t626_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3674_MI = 
{
	"IsPosition", (methodPointerType)&m3674, &t863_TI, &t40_0_0_0, RuntimeInvoker_t40_t626_t44, t863_m3674_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 654, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t194_0_0_0;
static ParameterInfo t863_m3675_ParameterInfos[] = 
{
	{"c", 0, 134218329, &EmptyCustomAttributesCache, &t194_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t372 (MethodInfo* method, void* obj, void** args);
MethodInfo m3675_MI = 
{
	"IsWordChar", (methodPointerType)&m3675, &t863_TI, &t40_0_0_0, RuntimeInvoker_t40_t372, t863_m3675_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 655, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t863_m3676_ParameterInfos[] = 
{
	{"pc", 0, 134218330, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3676_MI = 
{
	"GetString", (methodPointerType)&m3676, &t863_TI, &t7_0_0_0, RuntimeInvoker_t29_t44, t863_m3676_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 656, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t863_m3677_ParameterInfos[] = 
{
	{"gid", 0, 134218331, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"ptr", 1, 134218332, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3677_MI = 
{
	"Open", (methodPointerType)&m3677, &t863_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t863_m3677_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 657, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t863_m3678_ParameterInfos[] = 
{
	{"gid", 0, 134218333, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"ptr", 1, 134218334, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3678_MI = 
{
	"Close", (methodPointerType)&m3678, &t863_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t863_m3678_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 658, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t863_m3679_ParameterInfos[] = 
{
	{"gid", 0, 134218335, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"balance_gid", 1, 134218336, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"capture", 2, 134218337, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"ptr", 3, 134218338, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44_t44_t297_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3679_MI = 
{
	"Balance", (methodPointerType)&m3679, &t863_TI, &t40_0_0_0, RuntimeInvoker_t40_t44_t44_t297_t44, t863_m3679_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 4, false, false, 659, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3680_MI = 
{
	"Checkpoint", (methodPointerType)&m3680, &t863_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 660, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t863_m3681_ParameterInfos[] = 
{
	{"cp", 0, 134218339, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3681_MI = 
{
	"Backtrack", (methodPointerType)&m3681, &t863_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t863_m3681_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 661, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3682_MI = 
{
	"ResetGroups", (methodPointerType)&m3682, &t863_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 662, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t863_m3683_ParameterInfos[] = 
{
	{"gid", 0, 134218340, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3683_MI = 
{
	"GetLastDefined", (methodPointerType)&m3683, &t863_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t863_m3683_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 663, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t863_m3684_ParameterInfos[] = 
{
	{"previous", 0, 134218341, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3684_MI = 
{
	"CreateMark", (methodPointerType)&m3684, &t863_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t863_m3684_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 664, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_1_0_2;
extern Il2CppType t44_1_0_2;
static ParameterInfo t863_m3685_ParameterInfos[] = 
{
	{"gid", 0, 134218342, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"first_mark_index", 1, 134218343, &EmptyCustomAttributesCache, &t44_1_0_2},
	{"n_caps", 2, 134218344, &EmptyCustomAttributesCache, &t44_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t390_t390 (MethodInfo* method, void* obj, void** args);
MethodInfo m3685_MI = 
{
	"GetGroupInfo", (methodPointerType)&m3685, &t863_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t390_t390, t863_m3685_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, false, 665, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t833_0_0_0;
extern Il2CppType t833_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t863_m3686_ParameterInfos[] = 
{
	{"g", 0, 134218345, &EmptyCustomAttributesCache, &t833_0_0_0},
	{"first_mark_index", 1, 134218346, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"n_caps", 2, 134218347, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3686_MI = 
{
	"PopulateGroup", (methodPointerType)&m3686, &t863_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44_t44, t863_m3686_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, false, 666, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t829_0_0_0;
static ParameterInfo t863_m3687_ParameterInfos[] = 
{
	{"regex", 0, 134218348, &EmptyCustomAttributesCache, &t829_0_0_0},
};
extern Il2CppType t828_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3687_MI = 
{
	"GenerateMatch", (methodPointerType)&m3687, &t863_TI, &t828_0_0_0, RuntimeInvoker_t29_t29, t863_m3687_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 667, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t863_MIs[] =
{
	&m3667_MI,
	&m3668_MI,
	&m3669_MI,
	&m3670_MI,
	&m3671_MI,
	&m3672_MI,
	&m3673_MI,
	&m3674_MI,
	&m3675_MI,
	&m3676_MI,
	&m3677_MI,
	&m3678_MI,
	&m3679_MI,
	&m3680_MI,
	&m3681_MI,
	&m3682_MI,
	&m3683_MI,
	&m3684_MI,
	&m3685_MI,
	&m3686_MI,
	&m3687_MI,
	NULL
};
extern TypeInfo t860_TI;
extern TypeInfo t861_TI;
extern TypeInfo t862_TI;
static TypeInfo* t863_TI__nestedTypes[4] =
{
	&t860_TI,
	&t861_TI,
	&t862_TI,
	NULL
};
static MethodInfo* t863_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m3669_MI,
	&m3669_MI,
};
extern TypeInfo t836_TI;
static Il2CppInterfaceOffsetPair t863_IOs[] = 
{
	{ &t836_TI, 4},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t863_0_0_0;
extern Il2CppType t863_1_0_0;
struct t863;
TypeInfo t863_TI = 
{
	&g_System_dll_Image, NULL, "Interpreter", "System.Text.RegularExpressions", t863_MIs, NULL, t863_FIs, NULL, &t827_TI, t863_TI__nestedTypes, NULL, &t863_TI, NULL, t863_VT, &EmptyCustomAttributesCache, &t863_TI, &t863_0_0_0, &t863_1_0_0, t863_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t863), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 21, 0, 16, 0, 3, 6, 0, 1};
#include "t866.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t866_TI;
#include "t866MD.h"

extern MethodInfo m3692_MI;
extern MethodInfo m3697_MI;


extern MethodInfo m3688_MI;
 void m3688 (t866 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	int32_t V_0 = 0;
	{
		if ((((int32_t)p0) <= ((int32_t)p1)))
		{
			goto IL_000f;
		}
	}
	{
		V_0 = p0;
		p0 = p1;
		p1 = V_0;
	}

IL_000f:
	{
		__this->f0 = p0;
		__this->f1 = p1;
		__this->f2 = 1;
		return;
	}
}
extern MethodInfo m3689_MI;
 t866  m3689 (t29 * __this, MethodInfo* method){
	t866  V_0 = {0};
	{
		(&V_0)->f0 = 0;
		int32_t L_0 = ((&V_0)->f0);
		(&V_0)->f1 = ((int32_t)(L_0-1));
		(&V_0)->f2 = 1;
		return V_0;
	}
}
extern MethodInfo m3690_MI;
 bool m3690 (t866 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->f2);
		return ((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
extern MethodInfo m3691_MI;
 bool m3691 (t866 * __this, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (__this->f2);
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		int32_t L_1 = (__this->f0);
		int32_t L_2 = (__this->f1);
		G_B3_0 = ((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 0;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
 bool m3692 (t866 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f0);
		int32_t L_1 = (__this->f1);
		return ((((int32_t)L_0) > ((int32_t)L_1))? 1 : 0);
	}
}
extern MethodInfo m3693_MI;
 int32_t m3693 (t866 * __this, MethodInfo* method){
	{
		bool L_0 = m3692(__this, &m3692_MI);
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = (__this->f1);
		int32_t L_2 = (__this->f0);
		return ((int32_t)(((int32_t)(L_1-L_2))+1));
	}
}
extern MethodInfo m3694_MI;
 bool m3694 (t866 * __this, t866  p0, MethodInfo* method){
	int32_t G_B6_0 = 0;
	{
		bool L_0 = m3692(__this, &m3692_MI);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		bool L_1 = m3692((&p0), &m3692_MI);
		if (!L_1)
		{
			goto IL_0019;
		}
	}

IL_0017:
	{
		return 1;
	}

IL_0019:
	{
		int32_t L_2 = (__this->f0);
		int32_t L_3 = ((&p0)->f1);
		if ((((int32_t)L_2) > ((int32_t)L_3)))
		{
			goto IL_003f;
		}
	}
	{
		int32_t L_4 = ((&p0)->f0);
		int32_t L_5 = (__this->f1);
		G_B6_0 = ((((int32_t)((((int32_t)L_4) > ((int32_t)L_5))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0040;
	}

IL_003f:
	{
		G_B6_0 = 0;
	}

IL_0040:
	{
		return ((((int32_t)G_B6_0) == ((int32_t)0))? 1 : 0);
	}
}
extern MethodInfo m3695_MI;
 bool m3695 (t866 * __this, t866  p0, MethodInfo* method){
	int32_t G_B6_0 = 0;
	{
		bool L_0 = m3692(__this, &m3692_MI);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		bool L_1 = m3692((&p0), &m3692_MI);
		if (!L_1)
		{
			goto IL_0019;
		}
	}

IL_0017:
	{
		return 0;
	}

IL_0019:
	{
		int32_t L_2 = (__this->f0);
		int32_t L_3 = ((&p0)->f1);
		if ((((int32_t)L_2) == ((int32_t)((int32_t)(L_3+1)))))
		{
			goto IL_0040;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((&p0)->f0);
		G_B6_0 = ((((int32_t)L_4) == ((int32_t)((int32_t)(L_5-1))))? 1 : 0);
		goto IL_0041;
	}

IL_0040:
	{
		G_B6_0 = 1;
	}

IL_0041:
	{
		return G_B6_0;
	}
}
extern MethodInfo m3696_MI;
 bool m3696 (t866 * __this, t866  p0, MethodInfo* method){
	int32_t G_B8_0 = 0;
	{
		bool L_0 = m3692(__this, &m3692_MI);
		if (L_0)
		{
			goto IL_0019;
		}
	}
	{
		bool L_1 = m3692((&p0), &m3692_MI);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		return 1;
	}

IL_0019:
	{
		bool L_2 = m3692(__this, &m3692_MI);
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		return 0;
	}

IL_0026:
	{
		int32_t L_3 = (__this->f0);
		int32_t L_4 = ((&p0)->f0);
		if ((((int32_t)L_3) > ((int32_t)L_4)))
		{
			goto IL_004c;
		}
	}
	{
		int32_t L_5 = ((&p0)->f1);
		int32_t L_6 = (__this->f1);
		G_B8_0 = ((((int32_t)((((int32_t)L_5) > ((int32_t)L_6))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_004d;
	}

IL_004c:
	{
		G_B8_0 = 0;
	}

IL_004d:
	{
		return G_B8_0;
	}
}
 bool m3697 (t866 * __this, int32_t p0, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = (__this->f0);
		if ((((int32_t)L_0) > ((int32_t)p0)))
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_1 = (__this->f1);
		G_B3_0 = ((((int32_t)((((int32_t)p0) > ((int32_t)L_1))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_001b;
	}

IL_001a:
	{
		G_B3_0 = 0;
	}

IL_001b:
	{
		return G_B3_0;
	}
}
extern MethodInfo m3698_MI;
 bool m3698 (t866 * __this, t866  p0, MethodInfo* method){
	int32_t G_B8_0 = 0;
	int32_t G_B10_0 = 0;
	{
		bool L_0 = m3692(__this, &m3692_MI);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		bool L_1 = m3692((&p0), &m3692_MI);
		if (!L_1)
		{
			goto IL_0019;
		}
	}

IL_0017:
	{
		return 0;
	}

IL_0019:
	{
		int32_t L_2 = ((&p0)->f0);
		bool L_3 = m3697(__this, L_2, &m3697_MI);
		if (!L_3)
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = ((&p0)->f1);
		bool L_5 = m3697(__this, L_4, &m3697_MI);
		if (!L_5)
		{
			goto IL_0064;
		}
	}

IL_003d:
	{
		int32_t L_6 = ((&p0)->f1);
		bool L_7 = m3697(__this, L_6, &m3697_MI);
		if (!L_7)
		{
			goto IL_0061;
		}
	}
	{
		int32_t L_8 = ((&p0)->f0);
		bool L_9 = m3697(__this, L_8, &m3697_MI);
		G_B8_0 = ((((int32_t)L_9) == ((int32_t)0))? 1 : 0);
		goto IL_0062;
	}

IL_0061:
	{
		G_B8_0 = 0;
	}

IL_0062:
	{
		G_B10_0 = G_B8_0;
		goto IL_0065;
	}

IL_0064:
	{
		G_B10_0 = 1;
	}

IL_0065:
	{
		return G_B10_0;
	}
}
extern MethodInfo m3699_MI;
 void m3699 (t866 * __this, t866  p0, MethodInfo* method){
	{
		bool L_0 = m3692((&p0), &m3692_MI);
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		bool L_1 = m3692(__this, &m3692_MI);
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		int32_t L_2 = ((&p0)->f0);
		__this->f0 = L_2;
		int32_t L_3 = ((&p0)->f1);
		__this->f1 = L_3;
	}

IL_0032:
	{
		int32_t L_4 = ((&p0)->f0);
		int32_t L_5 = (__this->f0);
		if ((((int32_t)L_4) >= ((int32_t)L_5)))
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_6 = ((&p0)->f0);
		__this->f0 = L_6;
	}

IL_0051:
	{
		int32_t L_7 = ((&p0)->f1);
		int32_t L_8 = (__this->f1);
		if ((((int32_t)L_7) <= ((int32_t)L_8)))
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_9 = ((&p0)->f1);
		__this->f1 = L_9;
	}

IL_0070:
	{
		return;
	}
}
extern MethodInfo m3700_MI;
 int32_t m3700 (t866 * __this, t29 * p0, MethodInfo* method){
	t866  V_0 = {0};
	{
		int32_t L_0 = (__this->f0);
		V_0 = ((*(t866 *)((t866 *)UnBox (p0, InitializedTypeInfo(&t866_TI)))));
		int32_t L_1 = ((&V_0)->f0);
		return ((int32_t)(L_0-L_1));
	}
}
// Conversion methods for marshalling of: System.Text.RegularExpressions.Interval
void t866_marshal(const t866& unmarshaled, t866_marshaled& marshaled)
{
	marshaled.f0 = unmarshaled.f0;
	marshaled.f1 = unmarshaled.f1;
	marshaled.f2 = unmarshaled.f2;
}
void t866_marshal_back(const t866_marshaled& marshaled, t866& unmarshaled)
{
	unmarshaled.f0 = marshaled.f0;
	unmarshaled.f1 = marshaled.f1;
	unmarshaled.f2 = marshaled.f2;
}
// Conversion method for clean up from marshalling of: System.Text.RegularExpressions.Interval
void t866_marshal_cleanup(t866_marshaled& marshaled)
{
}
// Metadata Definition System.Text.RegularExpressions.Interval
extern Il2CppType t44_0_0_6;
FieldInfo t866_f0_FieldInfo = 
{
	"low", &t44_0_0_6, &t866_TI, offsetof(t866, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_6;
FieldInfo t866_f1_FieldInfo = 
{
	"high", &t44_0_0_6, &t866_TI, offsetof(t866, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_6;
FieldInfo t866_f2_FieldInfo = 
{
	"contiguous", &t40_0_0_6, &t866_TI, offsetof(t866, f2) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t866_FIs[] =
{
	&t866_f0_FieldInfo,
	&t866_f1_FieldInfo,
	&t866_f2_FieldInfo,
	NULL
};
static PropertyInfo t866____Empty_PropertyInfo = 
{
	&t866_TI, "Empty", &m3689_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t866____IsDiscontiguous_PropertyInfo = 
{
	&t866_TI, "IsDiscontiguous", &m3690_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t866____IsSingleton_PropertyInfo = 
{
	&t866_TI, "IsSingleton", &m3691_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t866____IsEmpty_PropertyInfo = 
{
	&t866_TI, "IsEmpty", &m3692_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t866____Size_PropertyInfo = 
{
	&t866_TI, "Size", &m3693_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t866_PIs[] =
{
	&t866____Empty_PropertyInfo,
	&t866____IsDiscontiguous_PropertyInfo,
	&t866____IsSingleton_PropertyInfo,
	&t866____IsEmpty_PropertyInfo,
	&t866____Size_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t866_m3688_ParameterInfos[] = 
{
	{"low", 0, 134218358, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"high", 1, 134218359, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3688_MI = 
{
	".ctor", (methodPointerType)&m3688, &t866_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t866_m3688_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 682, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t866_0_0_0;
extern void* RuntimeInvoker_t866 (MethodInfo* method, void* obj, void** args);
MethodInfo m3689_MI = 
{
	"get_Empty", (methodPointerType)&m3689, &t866_TI, &t866_0_0_0, RuntimeInvoker_t866, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, false, 683, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m3690_MI = 
{
	"get_IsDiscontiguous", (methodPointerType)&m3690, &t866_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 684, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m3691_MI = 
{
	"get_IsSingleton", (methodPointerType)&m3691, &t866_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 685, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m3692_MI = 
{
	"get_IsEmpty", (methodPointerType)&m3692, &t866_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 686, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3693_MI = 
{
	"get_Size", (methodPointerType)&m3693, &t866_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 687, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t866_0_0_0;
extern Il2CppType t866_0_0_0;
static ParameterInfo t866_m3694_ParameterInfos[] = 
{
	{"i", 0, 134218360, &EmptyCustomAttributesCache, &t866_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t866 (MethodInfo* method, void* obj, void** args);
MethodInfo m3694_MI = 
{
	"IsDisjoint", (methodPointerType)&m3694, &t866_TI, &t40_0_0_0, RuntimeInvoker_t40_t866, t866_m3694_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 688, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t866_0_0_0;
static ParameterInfo t866_m3695_ParameterInfos[] = 
{
	{"i", 0, 134218361, &EmptyCustomAttributesCache, &t866_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t866 (MethodInfo* method, void* obj, void** args);
MethodInfo m3695_MI = 
{
	"IsAdjacent", (methodPointerType)&m3695, &t866_TI, &t40_0_0_0, RuntimeInvoker_t40_t866, t866_m3695_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 689, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t866_0_0_0;
static ParameterInfo t866_m3696_ParameterInfos[] = 
{
	{"i", 0, 134218362, &EmptyCustomAttributesCache, &t866_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t866 (MethodInfo* method, void* obj, void** args);
MethodInfo m3696_MI = 
{
	"Contains", (methodPointerType)&m3696, &t866_TI, &t40_0_0_0, RuntimeInvoker_t40_t866, t866_m3696_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 690, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t866_m3697_ParameterInfos[] = 
{
	{"i", 0, 134218363, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3697_MI = 
{
	"Contains", (methodPointerType)&m3697, &t866_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t866_m3697_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 691, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t866_0_0_0;
static ParameterInfo t866_m3698_ParameterInfos[] = 
{
	{"i", 0, 134218364, &EmptyCustomAttributesCache, &t866_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t866 (MethodInfo* method, void* obj, void** args);
MethodInfo m3698_MI = 
{
	"Intersects", (methodPointerType)&m3698, &t866_TI, &t40_0_0_0, RuntimeInvoker_t40_t866, t866_m3698_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 692, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t866_0_0_0;
static ParameterInfo t866_m3699_ParameterInfos[] = 
{
	{"i", 0, 134218365, &EmptyCustomAttributesCache, &t866_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t866 (MethodInfo* method, void* obj, void** args);
MethodInfo m3699_MI = 
{
	"Merge", (methodPointerType)&m3699, &t866_TI, &t21_0_0_0, RuntimeInvoker_t21_t866, t866_m3699_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 693, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t866_m3700_ParameterInfos[] = 
{
	{"o", 0, 134218366, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3700_MI = 
{
	"CompareTo", (methodPointerType)&m3700, &t866_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t866_m3700_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 4, 1, false, false, 694, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t866_MIs[] =
{
	&m3688_MI,
	&m3689_MI,
	&m3690_MI,
	&m3691_MI,
	&m3692_MI,
	&m3693_MI,
	&m3694_MI,
	&m3695_MI,
	&m3696_MI,
	&m3697_MI,
	&m3698_MI,
	&m3699_MI,
	&m3700_MI,
	NULL
};
static MethodInfo* t866_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m3700_MI,
};
static TypeInfo* t866_ITIs[] = 
{
	&t290_TI,
};
static Il2CppInterfaceOffsetPair t866_IOs[] = 
{
	{ &t290_TI, 4},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t866_1_0_0;
TypeInfo t866_TI = 
{
	&g_System_dll_Image, NULL, "Interval", "System.Text.RegularExpressions", t866_MIs, t866_PIs, t866_FIs, NULL, &t110_TI, NULL, NULL, &t866_TI, t866_ITIs, t866_VT, &EmptyCustomAttributesCache, &t866_TI, &t866_0_0_0, &t866_1_0_0, t866_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t866_marshal, (methodPointerType)t866_marshal_back, (methodPointerType)t866_marshal_cleanup, sizeof (t866)+ sizeof (Il2CppObject), 0, sizeof(t866_marshaled), 0, 0, -1, 1048840, 0, true, false, false, false, false, false, false, false, false, false, false, false, 13, 5, 3, 0, 0, 5, 1, 1};
#include "t867.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t867_TI;
#include "t867MD.h"

#include "t914.h"
extern TypeInfo t674_TI;
extern TypeInfo t914_TI;
extern TypeInfo t868_TI;
#include "t914MD.h"
extern MethodInfo m3953_MI;
extern MethodInfo m3974_MI;
extern MethodInfo m4263_MI;
extern MethodInfo m3704_MI;


extern MethodInfo m3701_MI;
 void m3701 (t867 * __this, t29 * p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		__this->f0 = p0;
		VirtActionInvoker0::Invoke(&m3704_MI, __this);
		return;
	}
}
extern MethodInfo m3702_MI;
 t29 * m3702 (t867 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		t29 * L_1 = (__this->f0);
		int32_t L_2 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(&m3953_MI, L_1);
		if ((((int32_t)L_0) < ((int32_t)L_2)))
		{
			goto IL_001c;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3974(L_3, &m3974_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_001c:
	{
		t29 * L_4 = (__this->f0);
		int32_t L_5 = (__this->f1);
		t29 * L_6 = (t29 *)InterfaceFuncInvoker1< t29 *, int32_t >::Invoke(&m4263_MI, L_4, L_5);
		return L_6;
	}
}
extern MethodInfo m3703_MI;
 bool m3703 (t867 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		t29 * L_1 = (__this->f0);
		int32_t L_2 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(&m3953_MI, L_1);
		if ((((int32_t)L_0) <= ((int32_t)L_2)))
		{
			goto IL_001c;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3974(L_3, &m3974_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_001c:
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4+1));
		V_0 = L_5;
		__this->f1 = L_5;
		t29 * L_6 = (__this->f0);
		int32_t L_7 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(&m3953_MI, L_6);
		return ((((int32_t)V_0) < ((int32_t)L_7))? 1 : 0);
	}
}
 void m3704 (t867 * __this, MethodInfo* method){
	{
		__this->f1 = (-1);
		return;
	}
}
// Metadata Definition System.Text.RegularExpressions.IntervalCollection/Enumerator
extern Il2CppType t868_0_0_1;
FieldInfo t867_f0_FieldInfo = 
{
	"list", &t868_0_0_1, &t867_TI, offsetof(t867, f0), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t867_f1_FieldInfo = 
{
	"ptr", &t44_0_0_1, &t867_TI, offsetof(t867, f1), &EmptyCustomAttributesCache};
static FieldInfo* t867_FIs[] =
{
	&t867_f0_FieldInfo,
	&t867_f1_FieldInfo,
	NULL
};
static PropertyInfo t867____Current_PropertyInfo = 
{
	&t867_TI, "Current", &m3702_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t867_PIs[] =
{
	&t867____Current_PropertyInfo,
	NULL
};
extern Il2CppType t868_0_0_0;
extern Il2CppType t868_0_0_0;
static ParameterInfo t867_m3701_ParameterInfos[] = 
{
	{"list", 0, 134218376, &EmptyCustomAttributesCache, &t868_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3701_MI = 
{
	".ctor", (methodPointerType)&m3701, &t867_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t867_m3701_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 706, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3702_MI = 
{
	"get_Current", (methodPointerType)&m3702, &t867_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 4, 0, false, false, 707, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m3703_MI = 
{
	"MoveNext", (methodPointerType)&m3703, &t867_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, false, 708, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3704_MI = 
{
	"Reset", (methodPointerType)&m3704, &t867_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, false, 709, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t867_MIs[] =
{
	&m3701_MI,
	&m3702_MI,
	&m3703_MI,
	&m3704_MI,
	NULL
};
static MethodInfo* t867_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m3702_MI,
	&m3703_MI,
	&m3704_MI,
};
extern TypeInfo t136_TI;
static TypeInfo* t867_ITIs[] = 
{
	&t136_TI,
};
static Il2CppInterfaceOffsetPair t867_IOs[] = 
{
	{ &t136_TI, 4},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t867_0_0_0;
extern Il2CppType t867_1_0_0;
struct t867;
extern TypeInfo t870_TI;
TypeInfo t867_TI = 
{
	&g_System_dll_Image, NULL, "Enumerator", "", t867_MIs, t867_PIs, t867_FIs, NULL, &t29_TI, NULL, &t870_TI, &t867_TI, t867_ITIs, t867_VT, &EmptyCustomAttributesCache, &t867_TI, &t867_0_0_0, &t867_1_0_0, t867_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t867), 0, -1, 0, 0, -1, 1048579, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 1, 2, 0, 0, 7, 1, 1};
#include "t869.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t869_TI;
#include "t869MD.h"

#include "t35.h"
#include "t601.h"
#include "t67.h"


extern MethodInfo m3705_MI;
 void m3705 (t869 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m3706_MI;
 double m3706 (t869 * __this, t866  p0, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m3706((t869 *)__this->f9,p0, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef double (*FunctionPointerType) (t29 *, t29 * __this, t866  p0, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0,(MethodInfo*)(__this->f3.f0));
	}
	typedef double (*FunctionPointerType) (t29 * __this, t866  p0, MethodInfo* method);
	return ((FunctionPointerType)__this->f0)(__this->f2,p0,(MethodInfo*)(__this->f3.f0));
}
double pinvoke_delegate_wrapper_t869(Il2CppObject* delegate, t866  p0)
{
	typedef double (STDCALL *native_function_ptr_type)(t866_marshaled);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter 'p0' to native representation
	t866_marshaled _p0_marshaled = { 0 };
	t866_marshal(p0, _p0_marshaled);

	// Native function invocation and marshaling of return value back from native representation
	double _return_value = _il2cpp_pinvoke_func(_p0_marshaled);

	// Marshaling cleanup of parameter 'p0' native representation
	t866_marshal_cleanup(_p0_marshaled);

	return _return_value;
}
extern MethodInfo m3707_MI;
 t29 * m3707 (t869 * __this, t866  p0, t67 * p1, t29 * p2, MethodInfo* method){
	void *__d_args[2] = {0};
	__d_args[0] = Box(InitializedTypeInfo(&t866_TI), &p0);
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p1, (Il2CppObject*)p2);
}
extern MethodInfo m3708_MI;
 double m3708 (t869 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(double*)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Text.RegularExpressions.IntervalCollection/CostDelegate
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t869_m3705_ParameterInfos[] = 
{
	{"object", 0, 134218377, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134218378, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
MethodInfo m3705_MI = 
{
	".ctor", (methodPointerType)&m3705, &t869_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t869_m3705_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, false, 710, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t866_0_0_0;
static ParameterInfo t869_m3706_ParameterInfos[] = 
{
	{"i", 0, 134218379, &EmptyCustomAttributesCache, &t866_0_0_0},
};
extern Il2CppType t601_0_0_0;
extern void* RuntimeInvoker_t601_t866 (MethodInfo* method, void* obj, void** args);
MethodInfo m3706_MI = 
{
	"Invoke", (methodPointerType)&m3706, &t869_TI, &t601_0_0_0, RuntimeInvoker_t601_t866, t869_m3706_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, false, 711, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t866_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t869_m3707_ParameterInfos[] = 
{
	{"i", 0, 134218380, &EmptyCustomAttributesCache, &t866_0_0_0},
	{"callback", 1, 134218381, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134218382, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t866_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3707_MI = 
{
	"BeginInvoke", (methodPointerType)&m3707, &t869_TI, &t66_0_0_0, RuntimeInvoker_t29_t866_t29_t29, t869_m3707_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, false, 712, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t66_0_0_0;
extern Il2CppType t66_0_0_0;
static ParameterInfo t869_m3708_ParameterInfos[] = 
{
	{"result", 0, 134218383, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t601_0_0_0;
extern void* RuntimeInvoker_t601_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3708_MI = 
{
	"EndInvoke", (methodPointerType)&m3708, &t869_TI, &t601_0_0_0, RuntimeInvoker_t601_t29, t869_m3708_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, false, 713, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t869_MIs[] =
{
	&m3705_MI,
	&m3706_MI,
	&m3707_MI,
	&m3708_MI,
	NULL
};
extern MethodInfo m1699_MI;
extern MethodInfo m1700_MI;
extern MethodInfo m1701_MI;
extern MethodInfo m1702_MI;
extern MethodInfo m1703_MI;
extern MethodInfo m1704_MI;
extern MethodInfo m1705_MI;
static MethodInfo* t869_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m3706_MI,
	&m3707_MI,
	&m3708_MI,
};
extern TypeInfo t373_TI;
extern TypeInfo t374_TI;
static Il2CppInterfaceOffsetPair t869_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t869_0_0_0;
extern Il2CppType t869_1_0_0;
extern TypeInfo t195_TI;
struct t869;
TypeInfo t869_TI = 
{
	&g_System_dll_Image, NULL, "CostDelegate", "", t869_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t870_TI, &t869_TI, NULL, t869_VT, &EmptyCustomAttributesCache, &t869_TI, &t869_0_0_0, &t869_1_0_0, t869_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)pinvoke_delegate_wrapper_t869, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t869), 0, sizeof(methodPointerType), 0, 0, -1, 258, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t870.h"
#ifndef _MSC_VER
#else
#endif
#include "t870MD.h"

extern TypeInfo t601_TI;
extern TypeInfo t40_TI;
extern TypeInfo t324_TI;
extern MethodInfo m3978_MI;
extern MethodInfo m4264_MI;
extern MethodInfo m4265_MI;
extern MethodInfo m3709_MI;
extern MethodInfo m3712_MI;
extern MethodInfo m3715_MI;
extern MethodInfo m3714_MI;
extern MethodInfo m3710_MI;
extern MethodInfo m3711_MI;
extern MethodInfo m3981_MI;
extern MethodInfo m3970_MI;
extern MethodInfo m3969_MI;
extern MethodInfo m3971_MI;
extern MethodInfo m3972_MI;
extern MethodInfo m1428_MI;


 void m3709 (t870 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t731_TI));
		t731 * L_0 = (t731 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t731_TI));
		m3980(L_0, &m3980_MI);
		__this->f0 = L_0;
		return;
	}
}
 t866  m3710 (t870 * __this, int32_t p0, MethodInfo* method){
	{
		t731 * L_0 = (__this->f0);
		t29 * L_1 = (t29 *)VirtFuncInvoker1< t29 *, int32_t >::Invoke(&m3978_MI, L_0, p0);
		return ((*(t866 *)((t866 *)UnBox (L_1, InitializedTypeInfo(&t866_TI)))));
	}
}
 void m3711 (t870 * __this, t866  p0, MethodInfo* method){
	{
		t731 * L_0 = (__this->f0);
		t866  L_1 = p0;
		t29 * L_2 = Box(InitializedTypeInfo(&t866_TI), &L_1);
		VirtFuncInvoker1< int32_t, t29 * >::Invoke(&m3991_MI, L_0, L_2);
		return;
	}
}
 void m3712 (t870 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	t866  V_1 = {0};
	t866  V_2 = {0};
	{
		t731 * L_0 = (__this->f0);
		VirtActionInvoker0::Invoke(&m4264_MI, L_0);
		V_0 = 0;
		goto IL_0083;
	}

IL_0012:
	{
		t731 * L_1 = (__this->f0);
		t29 * L_2 = (t29 *)VirtFuncInvoker1< t29 *, int32_t >::Invoke(&m3978_MI, L_1, V_0);
		V_1 = ((*(t866 *)((t866 *)UnBox (L_2, InitializedTypeInfo(&t866_TI)))));
		t731 * L_3 = (__this->f0);
		t29 * L_4 = (t29 *)VirtFuncInvoker1< t29 *, int32_t >::Invoke(&m3978_MI, L_3, ((int32_t)(V_0+1)));
		V_2 = ((*(t866 *)((t866 *)UnBox (L_4, InitializedTypeInfo(&t866_TI)))));
		bool L_5 = m3694((&V_1), V_2, &m3694_MI);
		if (!L_5)
		{
			goto IL_0052;
		}
	}
	{
		bool L_6 = m3695((&V_1), V_2, &m3695_MI);
		if (!L_6)
		{
			goto IL_007f;
		}
	}

IL_0052:
	{
		m3699((&V_1), V_2, &m3699_MI);
		t731 * L_7 = (__this->f0);
		t866  L_8 = V_1;
		t29 * L_9 = Box(InitializedTypeInfo(&t866_TI), &L_8);
		VirtActionInvoker2< int32_t, t29 * >::Invoke(&m4258_MI, L_7, V_0, L_9);
		t731 * L_10 = (__this->f0);
		VirtActionInvoker1< int32_t >::Invoke(&m4265_MI, L_10, ((int32_t)(V_0+1)));
		goto IL_0083;
	}

IL_007f:
	{
		V_0 = ((int32_t)(V_0+1));
	}

IL_0083:
	{
		t731 * L_11 = (__this->f0);
		int32_t L_12 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m3976_MI, L_11);
		if ((((int32_t)V_0) < ((int32_t)((int32_t)(L_12-1)))))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}
}
extern MethodInfo m3713_MI;
 t870 * m3713 (t870 * __this, t869 * p0, MethodInfo* method){
	t870 * V_0 = {0};
	{
		t870 * L_0 = (t870 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t870_TI));
		m3709(L_0, &m3709_MI);
		V_0 = L_0;
		m3712(__this, &m3712_MI);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m3715_MI, __this);
		m3714(__this, 0, ((int32_t)(L_1-1)), V_0, p0, &m3714_MI);
		t731 * L_2 = (V_0->f0);
		VirtActionInvoker0::Invoke(&m4264_MI, L_2);
		return V_0;
	}
}
 void m3714 (t870 * __this, int32_t p0, int32_t p1, t870 * p2, t869 * p3, MethodInfo* method){
	t866  V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	double V_3 = 0.0;
	int32_t V_4 = 0;
	double V_5 = 0.0;
	int32_t V_6 = 0;
	double V_7 = 0.0;
	int32_t V_8 = 0;
	t866  V_9 = {0};
	t866  V_10 = {0};
	t866  V_11 = {0};
	t866  V_12 = {0};
	{
		(&V_0)->f2 = 0;
		V_1 = (-1);
		V_2 = (-1);
		V_3 = (0.0);
		V_4 = p0;
		goto IL_00ae;
	}

IL_001e:
	{
		t866  L_0 = m3710(__this, V_4, &m3710_MI);
		V_9 = L_0;
		int32_t L_1 = ((&V_9)->f0);
		(&V_0)->f0 = L_1;
		V_5 = (0.0);
		V_6 = V_4;
		goto IL_00a0;
	}

IL_004a:
	{
		t866  L_2 = m3710(__this, V_6, &m3710_MI);
		V_10 = L_2;
		int32_t L_3 = ((&V_10)->f1);
		(&V_0)->f1 = L_3;
		t866  L_4 = m3710(__this, V_6, &m3710_MI);
		double L_5 = (double)VirtFuncInvoker1< double, t866  >::Invoke(&m3706_MI, p3, L_4);
		V_5 = ((double)(V_5+L_5));
		double L_6 = (double)VirtFuncInvoker1< double, t866  >::Invoke(&m3706_MI, p3, V_0);
		V_7 = L_6;
		if ((((double)V_7) >= ((double)V_5)))
		{
			goto IL_009a;
		}
	}
	{
		if ((((double)V_5) <= ((double)V_3)))
		{
			goto IL_009a;
		}
	}
	{
		V_1 = V_4;
		V_2 = V_6;
		V_3 = V_5;
	}

IL_009a:
	{
		V_6 = ((int32_t)(V_6+1));
	}

IL_00a0:
	{
		if ((((int32_t)V_6) <= ((int32_t)p1)))
		{
			goto IL_004a;
		}
	}
	{
		V_4 = ((int32_t)(V_4+1));
	}

IL_00ae:
	{
		if ((((int32_t)V_4) <= ((int32_t)p1)))
		{
			goto IL_001e;
		}
	}
	{
		if ((((int32_t)V_1) >= ((int32_t)0)))
		{
			goto IL_00e6;
		}
	}
	{
		V_8 = p0;
		goto IL_00d9;
	}

IL_00c5:
	{
		t866  L_7 = m3710(__this, V_8, &m3710_MI);
		m3711(p2, L_7, &m3711_MI);
		V_8 = ((int32_t)(V_8+1));
	}

IL_00d9:
	{
		if ((((int32_t)V_8) <= ((int32_t)p1)))
		{
			goto IL_00c5;
		}
	}
	{
		goto IL_0143;
	}

IL_00e6:
	{
		t866  L_8 = m3710(__this, V_1, &m3710_MI);
		V_11 = L_8;
		int32_t L_9 = ((&V_11)->f0);
		(&V_0)->f0 = L_9;
		t866  L_10 = m3710(__this, V_2, &m3710_MI);
		V_12 = L_10;
		int32_t L_11 = ((&V_12)->f1);
		(&V_0)->f1 = L_11;
		m3711(p2, V_0, &m3711_MI);
		if ((((int32_t)V_1) <= ((int32_t)p0)))
		{
			goto IL_012f;
		}
	}
	{
		m3714(__this, p0, ((int32_t)(V_1-1)), p2, p3, &m3714_MI);
	}

IL_012f:
	{
		if ((((int32_t)V_2) >= ((int32_t)p1)))
		{
			goto IL_0143;
		}
	}
	{
		m3714(__this, ((int32_t)(V_2+1)), p1, p2, p3, &m3714_MI);
	}

IL_0143:
	{
		return;
	}
}
 int32_t m3715 (t870 * __this, MethodInfo* method){
	{
		t731 * L_0 = (__this->f0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m3976_MI, L_0);
		return L_1;
	}
}
extern MethodInfo m3716_MI;
 bool m3716 (t870 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m3717_MI;
 t29 * m3717 (t870 * __this, MethodInfo* method){
	{
		t731 * L_0 = (__this->f0);
		return L_0;
	}
}
extern MethodInfo m3718_MI;
 void m3718 (t870 * __this, t20 * p0, int32_t p1, MethodInfo* method){
	t866  V_0 = {0};
	t29 * V_1 = {0};
	t29 * V_2 = {0};
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		t731 * L_0 = (__this->f0);
		t29 * L_1 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m3981_MI, L_0);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0040;
		}

IL_0011:
		{
			t29 * L_2 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m3970_MI, V_1);
			V_0 = ((*(t866 *)((t866 *)UnBox (L_2, InitializedTypeInfo(&t866_TI)))));
			int32_t L_3 = m3969(p0, &m3969_MI);
			if ((((int32_t)p1) <= ((int32_t)L_3)))
			{
				goto IL_002e;
			}
		}

IL_0029:
		{
			goto IL_004b;
		}

IL_002e:
		{
			t866  L_4 = V_0;
			t29 * L_5 = Box(InitializedTypeInfo(&t866_TI), &L_4);
			int32_t L_6 = p1;
			p1 = ((int32_t)(L_6+1));
			m3971(p0, L_5, L_6, &m3971_MI);
		}

IL_0040:
		{
			bool L_7 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m3972_MI, V_1);
			if (L_7)
			{
				goto IL_0011;
			}
		}

IL_004b:
		{
			// IL_004b: leave IL_0062
			leaveInstructions[0] = 0x62; // 1
			THROW_SENTINEL(IL_0062);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_0050;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_0050;
	}

IL_0050:
	{ // begin finally (depth: 1)
		{
			V_2 = ((t29 *)IsInst(V_1, InitializedTypeInfo(&t324_TI)));
			if (V_2)
			{
				goto IL_005b;
			}
		}

IL_005a:
		{
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x62:
					goto IL_0062;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}

IL_005b:
		{
			InterfaceActionInvoker0::Invoke(&m1428_MI, V_2);
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x62:
					goto IL_0062;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}
	} // end finally (depth: 1)

IL_0062:
	{
		return;
	}
}
extern MethodInfo m3719_MI;
 t29 * m3719 (t870 * __this, MethodInfo* method){
	{
		t731 * L_0 = (__this->f0);
		t867 * L_1 = (t867 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t867_TI));
		m3701(L_1, L_0, &m3701_MI);
		return L_1;
	}
}
// Metadata Definition System.Text.RegularExpressions.IntervalCollection
extern Il2CppType t731_0_0_1;
FieldInfo t870_f0_FieldInfo = 
{
	"intervals", &t731_0_0_1, &t870_TI, offsetof(t870, f0), &EmptyCustomAttributesCache};
static FieldInfo* t870_FIs[] =
{
	&t870_f0_FieldInfo,
	NULL
};
static PropertyInfo t870____Item_PropertyInfo = 
{
	&t870_TI, "Item", &m3710_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t870____Count_PropertyInfo = 
{
	&t870_TI, "Count", &m3715_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t870____IsSynchronized_PropertyInfo = 
{
	&t870_TI, "IsSynchronized", &m3716_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t870____SyncRoot_PropertyInfo = 
{
	&t870_TI, "SyncRoot", &m3717_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t870_PIs[] =
{
	&t870____Item_PropertyInfo,
	&t870____Count_PropertyInfo,
	&t870____IsSynchronized_PropertyInfo,
	&t870____SyncRoot_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3709_MI = 
{
	".ctor", (methodPointerType)&m3709, &t870_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 695, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t870_m3710_ParameterInfos[] = 
{
	{"i", 0, 134218367, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t866_0_0_0;
extern void* RuntimeInvoker_t866_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3710_MI = 
{
	"get_Item", (methodPointerType)&m3710, &t870_TI, &t866_0_0_0, RuntimeInvoker_t866_t44, t870_m3710_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 696, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t866_0_0_0;
static ParameterInfo t870_m3711_ParameterInfos[] = 
{
	{"i", 0, 134218368, &EmptyCustomAttributesCache, &t866_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t866 (MethodInfo* method, void* obj, void** args);
MethodInfo m3711_MI = 
{
	"Add", (methodPointerType)&m3711, &t870_TI, &t21_0_0_0, RuntimeInvoker_t21_t866, t870_m3711_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 697, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3712_MI = 
{
	"Normalize", (methodPointerType)&m3712, &t870_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, false, 698, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t869_0_0_0;
static ParameterInfo t870_m3713_ParameterInfos[] = 
{
	{"cost_del", 0, 134218369, &EmptyCustomAttributesCache, &t869_0_0_0},
};
extern Il2CppType t870_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3713_MI = 
{
	"GetMetaCollection", (methodPointerType)&m3713, &t870_TI, &t870_0_0_0, RuntimeInvoker_t29_t29, t870_m3713_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 699, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t870_0_0_0;
extern Il2CppType t870_0_0_0;
extern Il2CppType t869_0_0_0;
static ParameterInfo t870_m3714_ParameterInfos[] = 
{
	{"begin", 0, 134218370, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"end", 1, 134218371, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"meta", 2, 134218372, &EmptyCustomAttributesCache, &t870_0_0_0},
	{"cost_del", 3, 134218373, &EmptyCustomAttributesCache, &t869_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3714_MI = 
{
	"Optimize", (methodPointerType)&m3714, &t870_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44_t29_t29, t870_m3714_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 4, false, false, 700, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3715_MI = 
{
	"get_Count", (methodPointerType)&m3715, &t870_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 4, 0, false, false, 701, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m3716_MI = 
{
	"get_IsSynchronized", (methodPointerType)&m3716, &t870_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2534, 0, 5, 0, false, false, 702, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3717_MI = 
{
	"get_SyncRoot", (methodPointerType)&m3717, &t870_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 6, 0, false, false, 703, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t870_m3718_ParameterInfos[] = 
{
	{"array", 0, 134218374, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134218375, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3718_MI = 
{
	"CopyTo", (methodPointerType)&m3718, &t870_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t870_m3718_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 7, 2, false, false, 704, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3719_MI = 
{
	"GetEnumerator", (methodPointerType)&m3719, &t870_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 8, 0, false, false, 705, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t870_MIs[] =
{
	&m3709_MI,
	&m3710_MI,
	&m3711_MI,
	&m3712_MI,
	&m3713_MI,
	&m3714_MI,
	&m3715_MI,
	&m3716_MI,
	&m3717_MI,
	&m3718_MI,
	&m3719_MI,
	NULL
};
extern TypeInfo t867_TI;
extern TypeInfo t869_TI;
static TypeInfo* t870_TI__nestedTypes[3] =
{
	&t867_TI,
	&t869_TI,
	NULL
};
static MethodInfo* t870_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m3715_MI,
	&m3716_MI,
	&m3717_MI,
	&m3718_MI,
	&m3719_MI,
};
extern TypeInfo t603_TI;
static TypeInfo* t870_ITIs[] = 
{
	&t674_TI,
	&t603_TI,
};
static Il2CppInterfaceOffsetPair t870_IOs[] = 
{
	{ &t674_TI, 4},
	{ &t603_TI, 8},
};
extern TypeInfo t425_TI;
#include "t425.h"
#include "t425MD.h"
extern MethodInfo m2025_MI;
void t870_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t425 * tmp;
		tmp = (t425 *)il2cpp_codegen_object_new (&t425_TI);
		m2025(tmp, il2cpp_codegen_string_new_wrapper("Item"), &m2025_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t870__CustomAttributeCache = {
1,
NULL,
&t870_CustomAttributesCacheGenerator
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t870_1_0_0;
struct t870;
extern CustomAttributesCache t870__CustomAttributeCache;
TypeInfo t870_TI = 
{
	&g_System_dll_Image, NULL, "IntervalCollection", "System.Text.RegularExpressions", t870_MIs, t870_PIs, t870_FIs, NULL, &t29_TI, t870_TI__nestedTypes, NULL, &t870_TI, t870_ITIs, t870_VT, &t870__CustomAttributeCache, &t870_TI, &t870_0_0_0, &t870_1_0_0, t870_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t870), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 11, 4, 1, 0, 2, 9, 2, 2};
#include "t871.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t871_TI;
#include "t871MD.h"

#include "t719.h"
#include "t872.h"
#include "t842.h"
#include "t873.h"
#include "t874.h"
#include "t305.h"
#include "t881.h"
#include "t887.h"
#include "t875.h"
#include "t884.h"
#include "t888.h"
#include "t891.h"
#include "t886.h"
#include "t876.h"
#include "t882.h"
#include "t883.h"
#include "t885.h"
#include "t889.h"
#include "t890.h"
extern TypeInfo t719_TI;
extern TypeInfo t872_TI;
extern TypeInfo t595_TI;
extern TypeInfo t881_TI;
extern TypeInfo t873_TI;
extern TypeInfo t888_TI;
extern TypeInfo t891_TI;
extern TypeInfo t886_TI;
extern TypeInfo t887_TI;
extern TypeInfo t884_TI;
extern TypeInfo t883_TI;
extern TypeInfo t876_TI;
extern TypeInfo t882_TI;
extern TypeInfo t885_TI;
extern TypeInfo t316_TI;
extern TypeInfo t890_TI;
extern TypeInfo t889_TI;
extern TypeInfo t875_TI;
#include "t719MD.h"
#include "t44MD.h"
#include "t872MD.h"
#include "t881MD.h"
#include "t873MD.h"
#include "t888MD.h"
#include "t891MD.h"
#include "t886MD.h"
#include "t874MD.h"
#include "t887MD.h"
#include "t884MD.h"
#include "t883MD.h"
#include "t876MD.h"
#include "t882MD.h"
#include "t885MD.h"
#include "t890MD.h"
#include "t889MD.h"
#include "t305MD.h"
extern MethodInfo m4209_MI;
extern MethodInfo m3724_MI;
extern MethodInfo m3740_MI;
extern MethodInfo m2943_MI;
extern MethodInfo m3738_MI;
extern MethodInfo m1742_MI;
extern MethodInfo m4178_MI;
extern MethodInfo m4266_MI;
extern MethodInfo m3775_MI;
extern MethodInfo m3728_MI;
extern MethodInfo m3742_MI;
extern MethodInfo m3776_MI;
extern MethodInfo m3750_MI;
extern MethodInfo m3990_MI;
extern MethodInfo m3781_MI;
extern MethodInfo m3779_MI;
extern MethodInfo m4219_MI;
extern MethodInfo m3989_MI;
extern MethodInfo m3770_MI;
extern MethodInfo m3748_MI;
extern MethodInfo m3741_MI;
extern MethodInfo m3745_MI;
extern MethodInfo m3829_MI;
extern MethodInfo m3747_MI;
extern MethodInfo m3845_MI;
extern MethodInfo m3736_MI;
extern MethodInfo m3735_MI;
extern MethodInfo m3732_MI;
extern MethodInfo m3744_MI;
extern MethodInfo m3729_MI;
extern MethodInfo m3823_MI;
extern MethodInfo m3771_MI;
extern MethodInfo m3801_MI;
extern MethodInfo m3802_MI;
extern MethodInfo m3803_MI;
extern MethodInfo m3804_MI;
extern MethodInfo m3818_MI;
extern MethodInfo m3820_MI;
extern MethodInfo m3733_MI;
extern MethodInfo m3793_MI;
extern MethodInfo m1767_MI;
extern MethodInfo m3795_MI;
extern MethodInfo m1312_MI;
extern MethodInfo m3746_MI;
extern MethodInfo m3778_MI;
extern MethodInfo m3790_MI;
extern MethodInfo m3731_MI;
extern MethodInfo m3811_MI;
extern MethodInfo m3730_MI;
extern MethodInfo m3815_MI;
extern MethodInfo m3737_MI;
extern MethodInfo m3782_MI;
extern MethodInfo m3787_MI;
extern MethodInfo m3783_MI;
extern MethodInfo m3813_MI;
extern MethodInfo m3812_MI;
extern MethodInfo m3806_MI;
extern MethodInfo m3749_MI;
extern MethodInfo m3844_MI;
extern MethodInfo m3848_MI;
extern MethodInfo m3847_MI;
extern MethodInfo m3734_MI;
extern MethodInfo m1387_MI;
extern MethodInfo m3849_MI;
extern MethodInfo m3739_MI;
extern MethodInfo m3725_MI;
extern MethodInfo m1685_MI;
extern MethodInfo m3841_MI;
extern MethodInfo m3834_MI;
extern MethodInfo m1535_MI;
extern MethodInfo m3973_MI;
extern MethodInfo m3722_MI;
extern MethodInfo m3723_MI;
extern MethodInfo m3780_MI;
extern MethodInfo m3721_MI;
extern MethodInfo m4181_MI;
extern MethodInfo m3743_MI;
extern MethodInfo m4267_MI;
extern MethodInfo m4154_MI;
extern MethodInfo m3842_MI;
extern MethodInfo m3836_MI;
extern MethodInfo m3807_MI;
extern MethodInfo m3788_MI;
extern MethodInfo m2926_MI;


extern MethodInfo m3720_MI;
 void m3720 (t871 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t731_TI));
		t731 * L_0 = (t731 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t731_TI));
		m3980(L_0, &m3980_MI);
		__this->f2 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t719_TI));
		t719 * L_1 = (t719 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t719_TI));
		m4209(L_1, &m4209_MI);
		__this->f3 = L_1;
		return;
	}
}
 int32_t m3721 (t29 * __this, t7* p0, int32_t* p1, MethodInfo* method){
	{
		int32_t L_0 = m3724(NULL, p0, p1, ((int32_t)10), 1, ((int32_t)2147483647), &m3724_MI);
		return L_0;
	}
}
 int32_t m3722 (t29 * __this, t7* p0, int32_t* p1, MethodInfo* method){
	{
		int32_t L_0 = m3724(NULL, p0, p1, 8, 1, 3, &m3724_MI);
		return L_0;
	}
}
 int32_t m3723 (t29 * __this, t7* p0, int32_t* p1, int32_t p2, MethodInfo* method){
	{
		int32_t L_0 = m3724(NULL, p0, p1, ((int32_t)16), p2, p2, &m3724_MI);
		return L_0;
	}
}
 int32_t m3724 (t29 * __this, t7* p0, int32_t* p1, int32_t p2, int32_t p3, int32_t p4, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		V_0 = (*((int32_t*)p1));
		V_1 = 0;
		V_2 = 0;
		if ((((int32_t)p4) >= ((int32_t)p3)))
		{
			goto IL_0016;
		}
	}
	{
		p4 = ((int32_t)2147483647);
	}

IL_0016:
	{
		goto IL_0048;
	}

IL_001b:
	{
		int32_t L_0 = V_0;
		V_0 = ((int32_t)(L_0+1));
		uint16_t L_1 = m1741(p0, L_0, &m1741_MI);
		int32_t L_2 = m3740(NULL, L_1, p2, V_2, &m3740_MI);
		V_3 = L_2;
		if ((((int32_t)V_3) >= ((int32_t)0)))
		{
			goto IL_003e;
		}
	}
	{
		V_0 = ((int32_t)(V_0-1));
		goto IL_005c;
	}

IL_003e:
	{
		V_1 = ((int32_t)(((int32_t)((int32_t)V_1*(int32_t)p2))+V_3));
		V_2 = ((int32_t)(V_2+1));
	}

IL_0048:
	{
		if ((((int32_t)V_2) >= ((int32_t)p4)))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_3 = m1715(p0, &m1715_MI);
		if ((((int32_t)V_0) < ((int32_t)L_3)))
		{
			goto IL_001b;
		}
	}

IL_005c:
	{
		if ((((int32_t)V_2) >= ((int32_t)p3)))
		{
			goto IL_0065;
		}
	}
	{
		return (-1);
	}

IL_0065:
	{
		*((int32_t*)(p1)) = (int32_t)V_0;
		return V_1;
	}
}
 t7* m3725 (t29 * __this, t7* p0, int32_t* p1, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		uint16_t L_0 = m1741(p0, (*((int32_t*)p1)), &m1741_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t194_TI));
		bool L_1 = m4222(NULL, L_0, &m4222_MI);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		int32_t L_2 = m3724(NULL, p0, p1, ((int32_t)10), 1, 0, &m3724_MI);
		V_0 = L_2;
		if ((((int32_t)V_0) <= ((int32_t)0)))
		{
			goto IL_002d;
		}
	}
	{
		t7* L_3 = m2943((&V_0), &m2943_MI);
		return L_3;
	}

IL_002d:
	{
		return (t7*)NULL;
	}

IL_002f:
	{
		V_1 = (*((int32_t*)p1));
		goto IL_0054;
	}

IL_0037:
	{
		uint16_t L_4 = m1741(p0, (*((int32_t*)p1)), &m1741_MI);
		bool L_5 = m3738(NULL, L_4, &m3738_MI);
		if (L_5)
		{
			goto IL_004e;
		}
	}
	{
		goto IL_0059;
	}

IL_004e:
	{
		*((int32_t*)(p1)) = (int32_t)((int32_t)((*((int32_t*)p1))+1));
	}

IL_0054:
	{
		goto IL_0037;
	}

IL_0059:
	{
		if ((((int32_t)((int32_t)((*((int32_t*)p1))-V_1))) <= ((int32_t)0)))
		{
			goto IL_006f;
		}
	}
	{
		t7* L_6 = m1742(p0, V_1, ((int32_t)((*((int32_t*)p1))-V_1)), &m1742_MI);
		return L_6;
	}

IL_006f:
	{
		return (t7*)NULL;
	}
}
extern MethodInfo m3726_MI;
 t872 * m3726 (t871 * __this, t7* p0, int32_t p1, MethodInfo* method){
	t872 * V_0 = {0};
	t872 * V_1 = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		__this->f0 = p0;
		__this->f1 = 0;
		t731 * L_0 = (__this->f2);
		VirtActionInvoker0::Invoke(&m4178_MI, L_0);
		t719 * L_1 = (__this->f3);
		VirtActionInvoker0::Invoke(&m4266_MI, L_1);
		__this->f4 = 0;
	}

IL_002b:
	try
	{ // begin try (depth: 1)
		{
			t872 * L_2 = (t872 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t872_TI));
			m3775(L_2, &m3775_MI);
			V_0 = L_2;
			m3728(__this, V_0, p1, (t874 *)NULL, &m3728_MI);
			m3742(__this, &m3742_MI);
			int32_t L_3 = (__this->f4);
			m3776(V_0, L_3, &m3776_MI);
			V_1 = V_0;
			// IL_004e: leave IL_006a
			goto IL_006a;
		}

IL_0053:
		{
			; // IL_0053: leave IL_006a
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t595_TI, e.ex->object.klass))
			goto IL_0058;
		throw e;
	}

IL_0058:
	{ // begin catch(System.IndexOutOfRangeException)
		t305 * L_4 = m3750(__this, (t7*) &_stringLiteral483, &m3750_MI);
		il2cpp_codegen_raise_exception(L_4);
		// IL_0065: leave IL_006a
		goto IL_006a;
	} // end catch (depth: 1)

IL_006a:
	{
		return V_1;
	}
}
extern MethodInfo m3727_MI;
 int32_t m3727 (t871 * __this, t719 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	t881 * V_2 = {0};
	t7* V_3 = {0};
	int32_t V_4 = 0;
	t7* G_B4_0 = {0};
	{
		t731 * L_0 = (__this->f2);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m3976_MI, L_0);
		V_0 = L_1;
		int32_t L_2 = 0;
		t29 * L_3 = Box(InitializedTypeInfo(&t44_TI), &L_2);
		VirtActionInvoker2< t29 *, t29 * >::Invoke(&m3990_MI, p0, (t7*) &_stringLiteral484, L_3);
		V_1 = 0;
		goto IL_00a5;
	}

IL_0024:
	{
		t731 * L_4 = (__this->f2);
		t29 * L_5 = (t29 *)VirtFuncInvoker1< t29 *, int32_t >::Invoke(&m3978_MI, L_4, V_1);
		V_2 = ((t881 *)Castclass(L_5, InitializedTypeInfo(&t881_TI)));
		t7* L_6 = m3781(V_2, &m3781_MI);
		if (!L_6)
		{
			goto IL_004c;
		}
	}
	{
		t7* L_7 = m3781(V_2, &m3781_MI);
		G_B4_0 = L_7;
		goto IL_005b;
	}

IL_004c:
	{
		int32_t L_8 = m3779(V_2, &m3779_MI);
		V_4 = L_8;
		t7* L_9 = m2943((&V_4), &m2943_MI);
		G_B4_0 = L_9;
	}

IL_005b:
	{
		V_3 = G_B4_0;
		bool L_10 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m4219_MI, p0, V_3);
		if (!L_10)
		{
			goto IL_008f;
		}
	}
	{
		t29 * L_11 = (t29 *)VirtFuncInvoker1< t29 *, t29 * >::Invoke(&m3989_MI, p0, V_3);
		int32_t L_12 = m3779(V_2, &m3779_MI);
		if ((((int32_t)((*(int32_t*)((int32_t*)UnBox (L_11, InitializedTypeInfo(&t44_TI)))))) == ((int32_t)L_12)))
		{
			goto IL_008a;
		}
	}
	{
		t956 * L_13 = (t956 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t956_TI));
		m4202(L_13, (t7*) &_stringLiteral485, &m4202_MI);
		il2cpp_codegen_raise_exception(L_13);
	}

IL_008a:
	{
		goto IL_00a1;
	}

IL_008f:
	{
		int32_t L_14 = m3779(V_2, &m3779_MI);
		int32_t L_15 = L_14;
		t29 * L_16 = Box(InitializedTypeInfo(&t44_TI), &L_15);
		VirtActionInvoker2< t29 *, t29 * >::Invoke(&m3990_MI, p0, V_3, L_16);
	}

IL_00a1:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_00a5:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_17 = (__this->f5);
		return L_17;
	}
}
 void m3728 (t871 * __this, t873 * p0, int32_t p1, t874 * p2, MethodInfo* method){
	bool V_0 = false;
	t887 * V_1 = {0};
	t7* V_2 = {0};
	t873 * V_3 = {0};
	t875 * V_4 = {0};
	bool V_5 = false;
	uint16_t V_6 = 0x0;
	uint16_t V_7 = {0};
	uint16_t V_8 = {0};
	uint16_t V_9 = {0};
	int32_t V_10 = 0;
	bool V_11 = false;
	uint16_t V_12 = 0x0;
	int32_t V_13 = 0;
	int32_t V_14 = 0;
	bool V_15 = false;
	bool V_16 = false;
	int32_t V_17 = 0;
	t884 * V_18 = {0};
	int32_t V_19 = 0;
	uint16_t V_20 = 0x0;
	int32_t G_B11_0 = 0;
	int32_t G_B15_0 = 0;
	int32_t G_B19_0 = 0;
	{
		V_0 = ((((t872 *)((t872 *)IsInst(p0, InitializedTypeInfo(&t872_TI)))) > ((t29 *)NULL))? 1 : 0);
		V_1 = (t887 *)NULL;
		V_2 = (t7*)NULL;
		t873 * L_0 = (t873 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t873_TI));
		m3770(L_0, &m3770_MI);
		V_3 = L_0;
		V_4 = (t875 *)NULL;
		V_5 = 0;
	}

IL_001a:
	{
		bool L_1 = m3748(NULL, p1, &m3748_MI);
		m3741(__this, L_1, &m3741_MI);
		int32_t L_2 = (__this->f1);
		t7* L_3 = (__this->f0);
		int32_t L_4 = m1715(L_3, &m1715_MI);
		if ((((int32_t)L_2) < ((int32_t)L_4)))
		{
			goto IL_0041;
		}
	}
	{
		goto IL_0484;
	}

IL_0041:
	{
		t7* L_5 = (__this->f0);
		int32_t L_6 = (__this->f1);
		int32_t L_7 = L_6;
		V_19 = L_7;
		__this->f1 = ((int32_t)(L_7+1));
		uint16_t L_8 = m1741(L_5, V_19, &m1741_MI);
		V_6 = L_8;
		V_20 = V_6;
		if (((uint16_t)(V_20-((int32_t)36))) == 0)
		{
			goto IL_00ee;
		}
		if (((uint16_t)(V_20-((int32_t)36))) == 1)
		{
			goto IL_009b;
		}
		if (((uint16_t)(V_20-((int32_t)36))) == 2)
		{
			goto IL_009b;
		}
		if (((uint16_t)(V_20-((int32_t)36))) == 3)
		{
			goto IL_009b;
		}
		if (((uint16_t)(V_20-((int32_t)36))) == 4)
		{
			goto IL_0190;
		}
		if (((uint16_t)(V_20-((int32_t)36))) == 5)
		{
			goto IL_01da;
		}
		if (((uint16_t)(V_20-((int32_t)36))) == 6)
		{
			goto IL_025f;
		}
		if (((uint16_t)(V_20-((int32_t)36))) == 7)
		{
			goto IL_025f;
		}
		if (((uint16_t)(V_20-((int32_t)36))) == 8)
		{
			goto IL_009b;
		}
		if (((uint16_t)(V_20-((int32_t)36))) == 9)
		{
			goto IL_009b;
		}
		if (((uint16_t)(V_20-((int32_t)36))) == 10)
		{
			goto IL_0110;
		}
	}

IL_009b:
	{
		if (((uint16_t)(V_20-((int32_t)91))) == 0)
		{
			goto IL_0182;
		}
		if (((uint16_t)(V_20-((int32_t)91))) == 1)
		{
			goto IL_0133;
		}
		if (((uint16_t)(V_20-((int32_t)91))) == 2)
		{
			goto IL_00b5;
		}
		if (((uint16_t)(V_20-((int32_t)91))) == 3)
		{
			goto IL_00cc;
		}
	}

IL_00b5:
	{
		if ((((int32_t)V_20) == ((int32_t)((int32_t)63))))
		{
			goto IL_025f;
		}
	}
	{
		if ((((int32_t)V_20) == ((int32_t)((int32_t)124))))
		{
			goto IL_01e2;
		}
	}
	{
		goto IL_026b;
	}

IL_00cc:
	{
		bool L_9 = m3745(NULL, p1, &m3745_MI);
		if (!L_9)
		{
			goto IL_00dd;
		}
	}
	{
		G_B11_0 = 3;
		goto IL_00de;
	}

IL_00dd:
	{
		G_B11_0 = 1;
	}

IL_00de:
	{
		V_7 = G_B11_0;
		t888 * L_10 = (t888 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t888_TI));
		m3829(L_10, V_7, &m3829_MI);
		V_4 = L_10;
		goto IL_0270;
	}

IL_00ee:
	{
		bool L_11 = m3745(NULL, p1, &m3745_MI);
		if (!L_11)
		{
			goto IL_00ff;
		}
	}
	{
		G_B15_0 = 7;
		goto IL_0100;
	}

IL_00ff:
	{
		G_B15_0 = 5;
	}

IL_0100:
	{
		V_8 = G_B15_0;
		t888 * L_12 = (t888 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t888_TI));
		m3829(L_12, V_8, &m3829_MI);
		V_4 = L_12;
		goto IL_0270;
	}

IL_0110:
	{
		bool L_13 = m3747(NULL, p1, &m3747_MI);
		if (!L_13)
		{
			goto IL_0121;
		}
	}
	{
		G_B19_0 = 2;
		goto IL_0122;
	}

IL_0121:
	{
		G_B19_0 = 1;
	}

IL_0122:
	{
		V_9 = G_B19_0;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t891_TI));
		t891 * L_14 = (t891 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t891_TI));
		m3845(L_14, V_9, 0, &m3845_MI);
		V_4 = L_14;
		goto IL_0270;
	}

IL_0133:
	{
		int32_t L_15 = m3736(__this, &m3736_MI);
		V_10 = L_15;
		if ((((int32_t)V_10) < ((int32_t)0)))
		{
			goto IL_014d;
		}
	}
	{
		V_6 = (((uint16_t)V_10));
		goto IL_017d;
	}

IL_014d:
	{
		t875 * L_16 = m3735(__this, p1, &m3735_MI);
		V_4 = L_16;
		if (V_4)
		{
			goto IL_017d;
		}
	}
	{
		t7* L_17 = (__this->f0);
		int32_t L_18 = (__this->f1);
		int32_t L_19 = L_18;
		V_19 = L_19;
		__this->f1 = ((int32_t)(L_19+1));
		uint16_t L_20 = m1741(L_17, V_19, &m1741_MI);
		V_6 = L_20;
	}

IL_017d:
	{
		goto IL_0270;
	}

IL_0182:
	{
		t875 * L_21 = m3732(__this, p1, &m3732_MI);
		V_4 = L_21;
		goto IL_0270;
	}

IL_0190:
	{
		bool L_22 = m3744(NULL, p1, &m3744_MI);
		V_11 = L_22;
		t875 * L_23 = m3729(__this, (&p1), &m3729_MI);
		V_4 = L_23;
		if (V_4)
		{
			goto IL_01d5;
		}
	}
	{
		if (!V_2)
		{
			goto IL_01d0;
		}
	}
	{
		bool L_24 = m3744(NULL, p1, &m3744_MI);
		if ((((int32_t)L_24) == ((int32_t)V_11)))
		{
			goto IL_01d0;
		}
	}
	{
		bool L_25 = m3744(NULL, p1, &m3744_MI);
		t886 * L_26 = (t886 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t886_TI));
		m3823(L_26, V_2, L_25, &m3823_MI);
		m3771(V_3, L_26, &m3771_MI);
		V_2 = (t7*)NULL;
	}

IL_01d0:
	{
		goto IL_001a;
	}

IL_01d5:
	{
		goto IL_0270;
	}

IL_01da:
	{
		V_5 = 1;
		goto IL_0484;
	}

IL_01e2:
	{
		if (!V_2)
		{
			goto IL_01fc;
		}
	}
	{
		bool L_27 = m3744(NULL, p1, &m3744_MI);
		t886 * L_28 = (t886 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t886_TI));
		m3823(L_28, V_2, L_27, &m3823_MI);
		m3771(V_3, L_28, &m3771_MI);
		V_2 = (t7*)NULL;
	}

IL_01fc:
	{
		if (!p2)
		{
			goto IL_0241;
		}
	}
	{
		t875 * L_29 = m3801(p2, &m3801_MI);
		if (L_29)
		{
			goto IL_0219;
		}
	}
	{
		m3802(p2, V_3, &m3802_MI);
		goto IL_023c;
	}

IL_0219:
	{
		t875 * L_30 = m3803(p2, &m3803_MI);
		if (L_30)
		{
			goto IL_0230;
		}
	}
	{
		m3804(p2, V_3, &m3804_MI);
		goto IL_023c;
	}

IL_0230:
	{
		t305 * L_31 = m3750(__this, (t7*) &_stringLiteral486, &m3750_MI);
		il2cpp_codegen_raise_exception(L_31);
	}

IL_023c:
	{
		goto IL_0254;
	}

IL_0241:
	{
		if (V_1)
		{
			goto IL_024d;
		}
	}
	{
		t887 * L_32 = (t887 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t887_TI));
		m3818(L_32, &m3818_MI);
		V_1 = L_32;
	}

IL_024d:
	{
		m3820(V_1, V_3, &m3820_MI);
	}

IL_0254:
	{
		t873 * L_33 = (t873 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t873_TI));
		m3770(L_33, &m3770_MI);
		V_3 = L_33;
		goto IL_001a;
	}

IL_025f:
	{
		t305 * L_34 = m3750(__this, (t7*) &_stringLiteral487, &m3750_MI);
		il2cpp_codegen_raise_exception(L_34);
	}

IL_026b:
	{
		goto IL_0270;
	}

IL_0270:
	{
		bool L_35 = m3748(NULL, p1, &m3748_MI);
		m3741(__this, L_35, &m3741_MI);
		int32_t L_36 = (__this->f1);
		t7* L_37 = (__this->f0);
		int32_t L_38 = m1715(L_37, &m1715_MI);
		if ((((int32_t)L_36) >= ((int32_t)L_38)))
		{
			goto IL_0413;
		}
	}
	{
		t7* L_39 = (__this->f0);
		int32_t L_40 = (__this->f1);
		uint16_t L_41 = m1741(L_39, L_40, &m1741_MI);
		V_12 = L_41;
		V_13 = 0;
		V_14 = 0;
		V_15 = 0;
		V_16 = 0;
		if ((((int32_t)V_12) == ((int32_t)((int32_t)63))))
		{
			goto IL_02cc;
		}
	}
	{
		if ((((int32_t)V_12) == ((int32_t)((int32_t)42))))
		{
			goto IL_02cc;
		}
	}
	{
		if ((((uint32_t)V_12) != ((uint32_t)((int32_t)43))))
		{
			goto IL_032f;
		}
	}

IL_02cc:
	{
		int32_t L_42 = (__this->f1);
		__this->f1 = ((int32_t)(L_42+1));
		V_16 = 1;
		V_20 = V_12;
		if ((((int32_t)V_20) == ((int32_t)((int32_t)42))))
		{
			goto IL_030c;
		}
	}
	{
		if ((((int32_t)V_20) == ((int32_t)((int32_t)43))))
		{
			goto IL_031b;
		}
	}
	{
		if ((((int32_t)V_20) == ((int32_t)((int32_t)63))))
		{
			goto IL_0301;
		}
	}
	{
		goto IL_032a;
	}

IL_0301:
	{
		V_13 = 0;
		V_14 = 1;
		goto IL_032a;
	}

IL_030c:
	{
		V_13 = 0;
		V_14 = ((int32_t)2147483647);
		goto IL_032a;
	}

IL_031b:
	{
		V_13 = 1;
		V_14 = ((int32_t)2147483647);
		goto IL_032a;
	}

IL_032a:
	{
		goto IL_0382;
	}

IL_032f:
	{
		if ((((uint32_t)V_12) != ((uint32_t)((int32_t)123))))
		{
			goto IL_0382;
		}
	}
	{
		int32_t L_43 = (__this->f1);
		t7* L_44 = (__this->f0);
		int32_t L_45 = m1715(L_44, &m1715_MI);
		if ((((int32_t)((int32_t)(L_43+1))) >= ((int32_t)L_45)))
		{
			goto IL_0382;
		}
	}
	{
		int32_t L_46 = (__this->f1);
		V_17 = L_46;
		int32_t L_47 = (__this->f1);
		__this->f1 = ((int32_t)(L_47+1));
		bool L_48 = m3733(__this, (&V_13), (&V_14), p1, &m3733_MI);
		V_16 = L_48;
		if (V_16)
		{
			goto IL_0382;
		}
	}
	{
		__this->f1 = V_17;
	}

IL_0382:
	{
		if (!V_16)
		{
			goto IL_0413;
		}
	}
	{
		bool L_49 = m3748(NULL, p1, &m3748_MI);
		m3741(__this, L_49, &m3741_MI);
		int32_t L_50 = (__this->f1);
		t7* L_51 = (__this->f0);
		int32_t L_52 = m1715(L_51, &m1715_MI);
		if ((((int32_t)L_50) >= ((int32_t)L_52)))
		{
			goto IL_03d4;
		}
	}
	{
		t7* L_53 = (__this->f0);
		int32_t L_54 = (__this->f1);
		uint16_t L_55 = m1741(L_53, L_54, &m1741_MI);
		if ((((uint32_t)L_55) != ((uint32_t)((int32_t)63))))
		{
			goto IL_03d4;
		}
	}
	{
		int32_t L_56 = (__this->f1);
		__this->f1 = ((int32_t)(L_56+1));
		V_15 = 1;
	}

IL_03d4:
	{
		t884 * L_57 = (t884 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t884_TI));
		m3793(L_57, V_13, V_14, V_15, &m3793_MI);
		V_18 = L_57;
		if (V_4)
		{
			goto IL_0406;
		}
	}
	{
		t7* L_58 = m1767((&V_6), &m1767_MI);
		bool L_59 = m3744(NULL, p1, &m3744_MI);
		t886 * L_60 = (t886 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t886_TI));
		m3823(L_60, L_58, L_59, &m3823_MI);
		m3795(V_18, L_60, &m3795_MI);
		goto IL_040f;
	}

IL_0406:
	{
		m3795(V_18, V_4, &m3795_MI);
	}

IL_040f:
	{
		V_4 = V_18;
	}

IL_0413:
	{
		if (V_4)
		{
			goto IL_0439;
		}
	}
	{
		if (V_2)
		{
			goto IL_0426;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		V_2 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
	}

IL_0426:
	{
		uint16_t L_61 = V_6;
		t29 * L_62 = Box(InitializedTypeInfo(&t194_TI), &L_61);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_63 = m1312(NULL, V_2, L_62, &m1312_MI);
		V_2 = L_63;
		goto IL_045e;
	}

IL_0439:
	{
		if (!V_2)
		{
			goto IL_0453;
		}
	}
	{
		bool L_64 = m3744(NULL, p1, &m3744_MI);
		t886 * L_65 = (t886 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t886_TI));
		m3823(L_65, V_2, L_64, &m3823_MI);
		m3771(V_3, L_65, &m3771_MI);
		V_2 = (t7*)NULL;
	}

IL_0453:
	{
		m3771(V_3, V_4, &m3771_MI);
		V_4 = (t875 *)NULL;
	}

IL_045e:
	{
		if (!V_0)
		{
			goto IL_047f;
		}
	}
	{
		int32_t L_66 = (__this->f1);
		t7* L_67 = (__this->f0);
		int32_t L_68 = m1715(L_67, &m1715_MI);
		if ((((int32_t)L_66) < ((int32_t)L_68)))
		{
			goto IL_047f;
		}
	}
	{
		goto IL_0484;
	}

IL_047f:
	{
		goto IL_001a;
	}

IL_0484:
	{
		if (!V_0)
		{
			goto IL_049d;
		}
	}
	{
		if (!V_5)
		{
			goto IL_049d;
		}
	}
	{
		t305 * L_69 = m3750(__this, (t7*) &_stringLiteral488, &m3750_MI);
		il2cpp_codegen_raise_exception(L_69);
	}

IL_049d:
	{
		if (V_0)
		{
			goto IL_04b6;
		}
	}
	{
		if (V_5)
		{
			goto IL_04b6;
		}
	}
	{
		t305 * L_70 = m3750(__this, (t7*) &_stringLiteral489, &m3750_MI);
		il2cpp_codegen_raise_exception(L_70);
	}

IL_04b6:
	{
		if (!V_2)
		{
			goto IL_04ce;
		}
	}
	{
		bool L_71 = m3744(NULL, p1, &m3744_MI);
		t886 * L_72 = (t886 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t886_TI));
		m3823(L_72, V_2, L_71, &m3823_MI);
		m3771(V_3, L_72, &m3771_MI);
	}

IL_04ce:
	{
		if (!p2)
		{
			goto IL_04fe;
		}
	}
	{
		t875 * L_73 = m3801(p2, &m3801_MI);
		if (L_73)
		{
			goto IL_04eb;
		}
	}
	{
		m3802(p2, V_3, &m3802_MI);
		goto IL_04f2;
	}

IL_04eb:
	{
		m3804(p2, V_3, &m3804_MI);
	}

IL_04f2:
	{
		m3771(p0, p2, &m3771_MI);
		goto IL_051e;
	}

IL_04fe:
	{
		if (!V_1)
		{
			goto IL_0517;
		}
	}
	{
		m3820(V_1, V_3, &m3820_MI);
		m3771(p0, V_1, &m3771_MI);
		goto IL_051e;
	}

IL_0517:
	{
		m3771(p0, V_3, &m3771_MI);
	}

IL_051e:
	{
		return;
	}
}
 t875 * m3729 (t871 * __this, int32_t* p0, MethodInfo* method){
	t873 * V_0 = {0};
	t873 * V_1 = {0};
	t873 * V_2 = {0};
	int32_t V_3 = {0};
	t873 * V_4 = {0};
	t876 * V_5 = {0};
	t873 * V_6 = {0};
	uint16_t V_7 = 0x0;
	t7* V_8 = {0};
	t881 * V_9 = {0};
	t7* V_10 = {0};
	t882 * V_11 = {0};
	t874 * V_12 = {0};
	int32_t V_13 = 0;
	t7* V_14 = {0};
	t876 * V_15 = {0};
	t873 * V_16 = {0};
	t873 * V_17 = {0};
	uint16_t V_18 = 0x0;
	int32_t V_19 = 0;
	{
		t7* L_0 = (__this->f0);
		int32_t L_1 = (__this->f1);
		uint16_t L_2 = m1741(L_0, L_1, &m1741_MI);
		if ((((int32_t)L_2) == ((int32_t)((int32_t)63))))
		{
			goto IL_004e;
		}
	}
	{
		bool L_3 = m3746(NULL, (*((int32_t*)p0)), &m3746_MI);
		if (!L_3)
		{
			goto IL_002f;
		}
	}
	{
		t873 * L_4 = (t873 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t873_TI));
		m3770(L_4, &m3770_MI);
		V_0 = L_4;
		goto IL_0042;
	}

IL_002f:
	{
		t881 * L_5 = (t881 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t881_TI));
		m3778(L_5, &m3778_MI);
		V_0 = L_5;
		t731 * L_6 = (__this->f2);
		VirtFuncInvoker1< int32_t, t29 * >::Invoke(&m3991_MI, L_6, V_0);
	}

IL_0042:
	{
		m3728(__this, V_0, (*((int32_t*)p0)), (t874 *)NULL, &m3728_MI);
		return V_0;
	}

IL_004e:
	{
		int32_t L_7 = (__this->f1);
		__this->f1 = ((int32_t)(L_7+1));
		t7* L_8 = (__this->f0);
		int32_t L_9 = (__this->f1);
		uint16_t L_10 = m1741(L_8, L_9, &m1741_MI);
		V_18 = L_10;
		if (((uint16_t)(V_18-((int32_t)33))) == 0)
		{
			goto IL_01e5;
		}
		if (((uint16_t)(V_18-((int32_t)33))) == 1)
		{
			goto IL_0099;
		}
		if (((uint16_t)(V_18-((int32_t)33))) == 2)
		{
			goto IL_0482;
		}
		if (((uint16_t)(V_18-((int32_t)33))) == 3)
		{
			goto IL_0099;
		}
		if (((uint16_t)(V_18-((int32_t)33))) == 4)
		{
			goto IL_0099;
		}
		if (((uint16_t)(V_18-((int32_t)33))) == 5)
		{
			goto IL_0099;
		}
		if (((uint16_t)(V_18-((int32_t)33))) == 6)
		{
			goto IL_021c;
		}
		if (((uint16_t)(V_18-((int32_t)33))) == 7)
		{
			goto IL_0376;
		}
	}

IL_0099:
	{
		if (((uint16_t)(V_18-((int32_t)105))) == 0)
		{
			goto IL_0139;
		}
		if (((uint16_t)(V_18-((int32_t)105))) == 1)
		{
			goto IL_00bb;
		}
		if (((uint16_t)(V_18-((int32_t)105))) == 2)
		{
			goto IL_00bb;
		}
		if (((uint16_t)(V_18-((int32_t)105))) == 3)
		{
			goto IL_00bb;
		}
		if (((uint16_t)(V_18-((int32_t)105))) == 4)
		{
			goto IL_0139;
		}
		if (((uint16_t)(V_18-((int32_t)105))) == 5)
		{
			goto IL_0139;
		}
	}

IL_00bb:
	{
		if (((uint16_t)(V_18-((int32_t)58))) == 0)
		{
			goto IL_00f9;
		}
		if (((uint16_t)(V_18-((int32_t)58))) == 1)
		{
			goto IL_00d9;
		}
		if (((uint16_t)(V_18-((int32_t)58))) == 2)
		{
			goto IL_01e5;
		}
		if (((uint16_t)(V_18-((int32_t)58))) == 3)
		{
			goto IL_01e5;
		}
		if (((uint16_t)(V_18-((int32_t)58))) == 4)
		{
			goto IL_0119;
		}
	}

IL_00d9:
	{
		if ((((int32_t)V_18) == ((int32_t)((int32_t)45))))
		{
			goto IL_0139;
		}
	}
	{
		if ((((int32_t)V_18) == ((int32_t)((int32_t)115))))
		{
			goto IL_0139;
		}
	}
	{
		if ((((int32_t)V_18) == ((int32_t)((int32_t)120))))
		{
			goto IL_0139;
		}
	}
	{
		goto IL_04de;
	}

IL_00f9:
	{
		int32_t L_11 = (__this->f1);
		__this->f1 = ((int32_t)(L_11+1));
		t873 * L_12 = (t873 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t873_TI));
		m3770(L_12, &m3770_MI);
		V_1 = L_12;
		m3728(__this, V_1, (*((int32_t*)p0)), (t874 *)NULL, &m3728_MI);
		return V_1;
	}

IL_0119:
	{
		int32_t L_13 = (__this->f1);
		__this->f1 = ((int32_t)(L_13+1));
		t883 * L_14 = (t883 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t883_TI));
		m3790(L_14, &m3790_MI);
		V_2 = L_14;
		m3728(__this, V_2, (*((int32_t*)p0)), (t874 *)NULL, &m3728_MI);
		return V_2;
	}

IL_0139:
	{
		V_3 = (*((int32_t*)p0));
		m3731(__this, (&V_3), 0, &m3731_MI);
		t7* L_15 = (__this->f0);
		int32_t L_16 = (__this->f1);
		uint16_t L_17 = m1741(L_15, L_16, &m1741_MI);
		if ((((uint32_t)L_17) != ((uint32_t)((int32_t)45))))
		{
			goto IL_0174;
		}
	}
	{
		int32_t L_18 = (__this->f1);
		__this->f1 = ((int32_t)(L_18+1));
		m3731(__this, (&V_3), 1, &m3731_MI);
	}

IL_0174:
	{
		t7* L_19 = (__this->f0);
		int32_t L_20 = (__this->f1);
		uint16_t L_21 = m1741(L_19, L_20, &m1741_MI);
		if ((((uint32_t)L_21) != ((uint32_t)((int32_t)58))))
		{
			goto IL_01ae;
		}
	}
	{
		int32_t L_22 = (__this->f1);
		__this->f1 = ((int32_t)(L_22+1));
		t873 * L_23 = (t873 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t873_TI));
		m3770(L_23, &m3770_MI);
		V_4 = L_23;
		m3728(__this, V_4, V_3, (t874 *)NULL, &m3728_MI);
		return V_4;
	}

IL_01ae:
	{
		t7* L_24 = (__this->f0);
		int32_t L_25 = (__this->f1);
		uint16_t L_26 = m1741(L_24, L_25, &m1741_MI);
		if ((((uint32_t)L_26) != ((uint32_t)((int32_t)41))))
		{
			goto IL_01d9;
		}
	}
	{
		int32_t L_27 = (__this->f1);
		__this->f1 = ((int32_t)(L_27+1));
		*((int32_t*)(p0)) = (int32_t)V_3;
		return (t875 *)NULL;
	}

IL_01d9:
	{
		t305 * L_28 = m3750(__this, (t7*) &_stringLiteral490, &m3750_MI);
		il2cpp_codegen_raise_exception(L_28);
	}

IL_01e5:
	{
		t876 * L_29 = (t876 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t876_TI));
		m3811(L_29, &m3811_MI);
		V_5 = L_29;
		bool L_30 = m3730(__this, V_5, &m3730_MI);
		if (L_30)
		{
			goto IL_01fe;
		}
	}
	{
		goto IL_021c;
	}

IL_01fe:
	{
		t873 * L_31 = (t873 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t873_TI));
		m3770(L_31, &m3770_MI);
		V_6 = L_31;
		m3728(__this, V_6, (*((int32_t*)p0)), (t874 *)NULL, &m3728_MI);
		m3815(V_5, V_6, &m3815_MI);
		return V_5;
	}

IL_021c:
	{
		t7* L_32 = (__this->f0);
		int32_t L_33 = (__this->f1);
		uint16_t L_34 = m1741(L_32, L_33, &m1741_MI);
		if ((((uint32_t)L_34) != ((uint32_t)((int32_t)60))))
		{
			goto IL_023d;
		}
	}
	{
		V_7 = ((int32_t)62);
		goto IL_0241;
	}

IL_023d:
	{
		V_7 = ((int32_t)39);
	}

IL_0241:
	{
		int32_t L_35 = (__this->f1);
		__this->f1 = ((int32_t)(L_35+1));
		t7* L_36 = m3737(__this, &m3737_MI);
		V_8 = L_36;
		t7* L_37 = (__this->f0);
		int32_t L_38 = (__this->f1);
		uint16_t L_39 = m1741(L_37, L_38, &m1741_MI);
		if ((((uint32_t)L_39) != ((uint32_t)V_7)))
		{
			goto IL_02bc;
		}
	}
	{
		if (V_8)
		{
			goto IL_0282;
		}
	}
	{
		t305 * L_40 = m3750(__this, (t7*) &_stringLiteral491, &m3750_MI);
		il2cpp_codegen_raise_exception(L_40);
	}

IL_0282:
	{
		int32_t L_41 = (__this->f1);
		__this->f1 = ((int32_t)(L_41+1));
		t881 * L_42 = (t881 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t881_TI));
		m3778(L_42, &m3778_MI);
		V_9 = L_42;
		m3782(V_9, V_8, &m3782_MI);
		t731 * L_43 = (__this->f2);
		VirtFuncInvoker1< int32_t, t29 * >::Invoke(&m3991_MI, L_43, V_9);
		m3728(__this, V_9, (*((int32_t*)p0)), (t874 *)NULL, &m3728_MI);
		return V_9;
	}

IL_02bc:
	{
		t7* L_44 = (__this->f0);
		int32_t L_45 = (__this->f1);
		uint16_t L_46 = m1741(L_44, L_45, &m1741_MI);
		if ((((uint32_t)L_46) != ((uint32_t)((int32_t)45))))
		{
			goto IL_036a;
		}
	}
	{
		int32_t L_47 = (__this->f1);
		__this->f1 = ((int32_t)(L_47+1));
		t7* L_48 = m3737(__this, &m3737_MI);
		V_10 = L_48;
		if (!V_10)
		{
			goto IL_0309;
		}
	}
	{
		t7* L_49 = (__this->f0);
		int32_t L_50 = (__this->f1);
		uint16_t L_51 = m1741(L_49, L_50, &m1741_MI);
		if ((((int32_t)L_51) == ((int32_t)V_7)))
		{
			goto IL_0315;
		}
	}

IL_0309:
	{
		t305 * L_52 = m3750(__this, (t7*) &_stringLiteral492, &m3750_MI);
		il2cpp_codegen_raise_exception(L_52);
	}

IL_0315:
	{
		int32_t L_53 = (__this->f1);
		__this->f1 = ((int32_t)(L_53+1));
		t882 * L_54 = (t882 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t882_TI));
		m3787(L_54, &m3787_MI);
		V_11 = L_54;
		m3782(V_11, V_8, &m3782_MI);
		bool L_55 = m3783(V_11, &m3783_MI);
		if (!L_55)
		{
			goto IL_034d;
		}
	}
	{
		t731 * L_56 = (__this->f2);
		VirtFuncInvoker1< int32_t, t29 * >::Invoke(&m3991_MI, L_56, V_11);
	}

IL_034d:
	{
		t719 * L_57 = (__this->f3);
		VirtActionInvoker2< t29 *, t29 * >::Invoke(&m3990_MI, L_57, V_11, V_10);
		m3728(__this, V_11, (*((int32_t*)p0)), (t874 *)NULL, &m3728_MI);
		return V_11;
	}

IL_036a:
	{
		t305 * L_58 = m3750(__this, (t7*) &_stringLiteral491, &m3750_MI);
		il2cpp_codegen_raise_exception(L_58);
	}

IL_0376:
	{
		int32_t L_59 = (__this->f1);
		__this->f1 = ((int32_t)(L_59+1));
		int32_t L_60 = (__this->f1);
		V_13 = L_60;
		t7* L_61 = m3737(__this, &m3737_MI);
		V_14 = L_61;
		if (!V_14)
		{
			goto IL_03b3;
		}
	}
	{
		t7* L_62 = (__this->f0);
		int32_t L_63 = (__this->f1);
		uint16_t L_64 = m1741(L_62, L_63, &m1741_MI);
		if ((((int32_t)L_64) == ((int32_t)((int32_t)41))))
		{
			goto IL_043a;
		}
	}

IL_03b3:
	{
		__this->f1 = V_13;
		t876 * L_65 = (t876 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t876_TI));
		m3811(L_65, &m3811_MI);
		V_15 = L_65;
		t7* L_66 = (__this->f0);
		int32_t L_67 = (__this->f1);
		uint16_t L_68 = m1741(L_66, L_67, &m1741_MI);
		if ((((uint32_t)L_68) != ((uint32_t)((int32_t)63))))
		{
			goto IL_0406;
		}
	}
	{
		int32_t L_69 = (__this->f1);
		__this->f1 = ((int32_t)(L_69+1));
		bool L_70 = m3730(__this, V_15, &m3730_MI);
		if (L_70)
		{
			goto IL_0401;
		}
	}
	{
		t305 * L_71 = m3750(__this, (t7*) &_stringLiteral493, &m3750_MI);
		il2cpp_codegen_raise_exception(L_71);
	}

IL_0401:
	{
		goto IL_0416;
	}

IL_0406:
	{
		m3813(V_15, 0, &m3813_MI);
		m3812(V_15, 0, &m3812_MI);
	}

IL_0416:
	{
		t873 * L_72 = (t873 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t873_TI));
		m3770(L_72, &m3770_MI);
		V_16 = L_72;
		m3728(__this, V_16, (*((int32_t*)p0)), (t874 *)NULL, &m3728_MI);
		m3815(V_15, V_16, &m3815_MI);
		V_12 = V_15;
		goto IL_046c;
	}

IL_043a:
	{
		int32_t L_73 = (__this->f1);
		__this->f1 = ((int32_t)(L_73+1));
		bool L_74 = m3744(NULL, (*((int32_t*)p0)), &m3744_MI);
		t886 * L_75 = (t886 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t886_TI));
		m3823(L_75, V_14, L_74, &m3823_MI);
		t885 * L_76 = (t885 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t885_TI));
		m3806(L_76, L_75, &m3806_MI);
		V_12 = L_76;
		t719 * L_77 = (__this->f3);
		VirtActionInvoker2< t29 *, t29 * >::Invoke(&m3990_MI, L_77, V_12, V_14);
	}

IL_046c:
	{
		t873 * L_78 = (t873 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t873_TI));
		m3770(L_78, &m3770_MI);
		V_17 = L_78;
		m3728(__this, V_17, (*((int32_t*)p0)), V_12, &m3728_MI);
		return V_17;
	}

IL_0482:
	{
		int32_t L_79 = (__this->f1);
		__this->f1 = ((int32_t)(L_79+1));
		goto IL_04b7;
	}

IL_0495:
	{
		int32_t L_80 = (__this->f1);
		t7* L_81 = (__this->f0);
		int32_t L_82 = m1715(L_81, &m1715_MI);
		if ((((int32_t)L_80) < ((int32_t)L_82)))
		{
			goto IL_04b7;
		}
	}
	{
		t305 * L_83 = m3750(__this, (t7*) &_stringLiteral494, &m3750_MI);
		il2cpp_codegen_raise_exception(L_83);
	}

IL_04b7:
	{
		t7* L_84 = (__this->f0);
		int32_t L_85 = (__this->f1);
		int32_t L_86 = L_85;
		V_19 = L_86;
		__this->f1 = ((int32_t)(L_86+1));
		uint16_t L_87 = m1741(L_84, V_19, &m1741_MI);
		if ((((uint32_t)L_87) != ((uint32_t)((int32_t)41))))
		{
			goto IL_0495;
		}
	}
	{
		return (t875 *)NULL;
	}

IL_04de:
	{
		t305 * L_88 = m3750(__this, (t7*) &_stringLiteral495, &m3750_MI);
		il2cpp_codegen_raise_exception(L_88);
	}
}
 bool m3730 (t871 * __this, t876 * p0, MethodInfo* method){
	uint16_t V_0 = 0x0;
	{
		t7* L_0 = (__this->f0);
		int32_t L_1 = (__this->f1);
		uint16_t L_2 = m1741(L_0, L_1, &m1741_MI);
		if ((((uint32_t)L_2) != ((uint32_t)((int32_t)60))))
		{
			goto IL_0075;
		}
	}
	{
		t7* L_3 = (__this->f0);
		int32_t L_4 = (__this->f1);
		uint16_t L_5 = m1741(L_3, ((int32_t)(L_4+1)), &m1741_MI);
		V_0 = L_5;
		if ((((int32_t)V_0) == ((int32_t)((int32_t)33))))
		{
			goto IL_004d;
		}
	}
	{
		if ((((int32_t)V_0) == ((int32_t)((int32_t)61))))
		{
			goto IL_0041;
		}
	}
	{
		goto IL_0059;
	}

IL_0041:
	{
		m3813(p0, 0, &m3813_MI);
		goto IL_005b;
	}

IL_004d:
	{
		m3813(p0, 1, &m3813_MI);
		goto IL_005b;
	}

IL_0059:
	{
		return 0;
	}

IL_005b:
	{
		m3812(p0, 1, &m3812_MI);
		int32_t L_6 = (__this->f1);
		__this->f1 = ((int32_t)(L_6+2));
		goto IL_00cb;
	}

IL_0075:
	{
		t7* L_7 = (__this->f0);
		int32_t L_8 = (__this->f1);
		uint16_t L_9 = m1741(L_7, L_8, &m1741_MI);
		V_0 = L_9;
		if ((((int32_t)V_0) == ((int32_t)((int32_t)33))))
		{
			goto IL_00a8;
		}
	}
	{
		if ((((int32_t)V_0) == ((int32_t)((int32_t)61))))
		{
			goto IL_009c;
		}
	}
	{
		goto IL_00b4;
	}

IL_009c:
	{
		m3813(p0, 0, &m3813_MI);
		goto IL_00b6;
	}

IL_00a8:
	{
		m3813(p0, 1, &m3813_MI);
		goto IL_00b6;
	}

IL_00b4:
	{
		return 0;
	}

IL_00b6:
	{
		m3812(p0, 0, &m3812_MI);
		int32_t L_10 = (__this->f1);
		__this->f1 = ((int32_t)(L_10+1));
	}

IL_00cb:
	{
		return 1;
	}
}
 void m3731 (t871 * __this, int32_t* p0, bool p1, MethodInfo* method){
	uint16_t V_0 = 0x0;
	{
		goto IL_00ef;
	}

IL_0005:
	{
		t7* L_0 = (__this->f0);
		int32_t L_1 = (__this->f1);
		uint16_t L_2 = m1741(L_0, L_1, &m1741_MI);
		V_0 = L_2;
		if (((uint16_t)(V_0-((int32_t)105))) == 0)
		{
			goto IL_004d;
		}
		if (((uint16_t)(V_0-((int32_t)105))) == 1)
		{
			goto IL_0038;
		}
		if (((uint16_t)(V_0-((int32_t)105))) == 2)
		{
			goto IL_0038;
		}
		if (((uint16_t)(V_0-((int32_t)105))) == 3)
		{
			goto IL_0038;
		}
		if (((uint16_t)(V_0-((int32_t)105))) == 4)
		{
			goto IL_006a;
		}
		if (((uint16_t)(V_0-((int32_t)105))) == 5)
		{
			goto IL_0087;
		}
	}

IL_0038:
	{
		if ((((int32_t)V_0) == ((int32_t)((int32_t)115))))
		{
			goto IL_00a4;
		}
	}
	{
		if ((((int32_t)V_0) == ((int32_t)((int32_t)120))))
		{
			goto IL_00c2;
		}
	}
	{
		goto IL_00e0;
	}

IL_004d:
	{
		if (!p1)
		{
			goto IL_005f;
		}
	}
	{
		*((int32_t*)(p0)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)p0))&(int32_t)((int32_t)-2)));
		goto IL_0065;
	}

IL_005f:
	{
		*((int32_t*)(p0)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)p0))|(int32_t)1));
	}

IL_0065:
	{
		goto IL_00e1;
	}

IL_006a:
	{
		if (!p1)
		{
			goto IL_007c;
		}
	}
	{
		*((int32_t*)(p0)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)p0))&(int32_t)((int32_t)-3)));
		goto IL_0082;
	}

IL_007c:
	{
		*((int32_t*)(p0)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)p0))|(int32_t)2));
	}

IL_0082:
	{
		goto IL_00e1;
	}

IL_0087:
	{
		if (!p1)
		{
			goto IL_0099;
		}
	}
	{
		*((int32_t*)(p0)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)p0))&(int32_t)((int32_t)-5)));
		goto IL_009f;
	}

IL_0099:
	{
		*((int32_t*)(p0)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)p0))|(int32_t)4));
	}

IL_009f:
	{
		goto IL_00e1;
	}

IL_00a4:
	{
		if (!p1)
		{
			goto IL_00b6;
		}
	}
	{
		*((int32_t*)(p0)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)p0))&(int32_t)((int32_t)-17)));
		goto IL_00bd;
	}

IL_00b6:
	{
		*((int32_t*)(p0)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)p0))|(int32_t)((int32_t)16)));
	}

IL_00bd:
	{
		goto IL_00e1;
	}

IL_00c2:
	{
		if (!p1)
		{
			goto IL_00d4;
		}
	}
	{
		*((int32_t*)(p0)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)p0))&(int32_t)((int32_t)-33)));
		goto IL_00db;
	}

IL_00d4:
	{
		*((int32_t*)(p0)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)p0))|(int32_t)((int32_t)32)));
	}

IL_00db:
	{
		goto IL_00e1;
	}

IL_00e0:
	{
		return;
	}

IL_00e1:
	{
		int32_t L_3 = (__this->f1);
		__this->f1 = ((int32_t)(L_3+1));
	}

IL_00ef:
	{
		goto IL_0005;
	}
}
 t875 * m3732 (t871 * __this, int32_t p0, MethodInfo* method){
	bool V_0 = false;
	bool V_1 = false;
	t891 * V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	bool V_5 = false;
	bool V_6 = false;
	int32_t V_7 = 0;
	t891 * G_B24_0 = {0};
	t891 * G_B23_0 = {0};
	int32_t G_B25_0 = 0;
	t891 * G_B25_1 = {0};
	t891 * G_B28_0 = {0};
	t891 * G_B27_0 = {0};
	int32_t G_B29_0 = 0;
	t891 * G_B29_1 = {0};
	t891 * G_B32_0 = {0};
	t891 * G_B31_0 = {0};
	int32_t G_B33_0 = 0;
	t891 * G_B33_1 = {0};
	{
		V_0 = 0;
		t7* L_0 = (__this->f0);
		int32_t L_1 = (__this->f1);
		uint16_t L_2 = m1741(L_0, L_1, &m1741_MI);
		if ((((uint32_t)L_2) != ((uint32_t)((int32_t)94))))
		{
			goto IL_002a;
		}
	}
	{
		V_0 = 1;
		int32_t L_3 = (__this->f1);
		__this->f1 = ((int32_t)(L_3+1));
	}

IL_002a:
	{
		bool L_4 = m3749(NULL, p0, &m3749_MI);
		V_1 = L_4;
		bool L_5 = m3744(NULL, p0, &m3744_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t891_TI));
		t891 * L_6 = (t891 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t891_TI));
		m3844(L_6, V_0, L_5, &m3844_MI);
		V_2 = L_6;
		t7* L_7 = (__this->f0);
		int32_t L_8 = (__this->f1);
		uint16_t L_9 = m1741(L_7, L_8, &m1741_MI);
		if ((((uint32_t)L_9) != ((uint32_t)((int32_t)93))))
		{
			goto IL_006c;
		}
	}
	{
		m3848(V_2, ((int32_t)93), &m3848_MI);
		int32_t L_10 = (__this->f1);
		__this->f1 = ((int32_t)(L_10+1));
	}

IL_006c:
	{
		V_3 = (-1);
		V_4 = (-1);
		V_5 = 0;
		V_6 = 0;
		goto IL_027f;
	}

IL_007c:
	{
		t7* L_11 = (__this->f0);
		int32_t L_12 = (__this->f1);
		int32_t L_13 = L_12;
		V_7 = L_13;
		__this->f1 = ((int32_t)(L_13+1));
		uint16_t L_14 = m1741(L_11, V_7, &m1741_MI);
		V_3 = L_14;
		if ((((uint32_t)V_3) != ((uint32_t)((int32_t)93))))
		{
			goto IL_00ab;
		}
	}
	{
		V_6 = 1;
		goto IL_0295;
	}

IL_00ab:
	{
		if ((((uint32_t)V_3) != ((uint32_t)((int32_t)45))))
		{
			goto IL_00ca;
		}
	}
	{
		if ((((int32_t)V_4) < ((int32_t)0)))
		{
			goto IL_00ca;
		}
	}
	{
		if (V_5)
		{
			goto IL_00ca;
		}
	}
	{
		V_5 = 1;
		goto IL_027f;
	}

IL_00ca:
	{
		if ((((uint32_t)V_3) != ((uint32_t)((int32_t)92))))
		{
			goto IL_0212;
		}
	}
	{
		int32_t L_15 = m3736(__this, &m3736_MI);
		V_3 = L_15;
		if ((((int32_t)V_3) < ((int32_t)0)))
		{
			goto IL_00e5;
		}
	}
	{
		goto IL_0212;
	}

IL_00e5:
	{
		t7* L_16 = (__this->f0);
		int32_t L_17 = (__this->f1);
		int32_t L_18 = L_17;
		V_7 = L_18;
		__this->f1 = ((int32_t)(L_18+1));
		uint16_t L_19 = m1741(L_16, V_7, &m1741_MI);
		V_3 = L_19;
		V_7 = V_3;
		if (((int32_t)(V_7-((int32_t)80))) == 0)
		{
			goto IL_01d1;
		}
		if (((int32_t)(V_7-((int32_t)80))) == 1)
		{
			goto IL_0121;
		}
		if (((int32_t)(V_7-((int32_t)80))) == 2)
		{
			goto IL_0121;
		}
		if (((int32_t)(V_7-((int32_t)80))) == 3)
		{
			goto IL_01b3;
		}
	}

IL_0121:
	{
		if (((int32_t)(V_7-((int32_t)112))) == 0)
		{
			goto IL_01d1;
		}
		if (((int32_t)(V_7-((int32_t)112))) == 1)
		{
			goto IL_013b;
		}
		if (((int32_t)(V_7-((int32_t)112))) == 2)
		{
			goto IL_013b;
		}
		if (((int32_t)(V_7-((int32_t)112))) == 3)
		{
			goto IL_01b3;
		}
	}

IL_013b:
	{
		if (((int32_t)(V_7-((int32_t)98))) == 0)
		{
			goto IL_0171;
		}
		if (((int32_t)(V_7-((int32_t)98))) == 1)
		{
			goto IL_0151;
		}
		if (((int32_t)(V_7-((int32_t)98))) == 2)
		{
			goto IL_0178;
		}
	}

IL_0151:
	{
		if ((((int32_t)V_7) == ((int32_t)((int32_t)68))))
		{
			goto IL_0178;
		}
	}
	{
		if ((((int32_t)V_7) == ((int32_t)((int32_t)87))))
		{
			goto IL_0196;
		}
	}
	{
		if ((((int32_t)V_7) == ((int32_t)((int32_t)119))))
		{
			goto IL_0196;
		}
	}
	{
		goto IL_01e7;
	}

IL_0171:
	{
		V_3 = 8;
		goto IL_0212;
	}

IL_0178:
	{
		G_B23_0 = V_2;
		if (!V_1)
		{
			G_B24_0 = V_2;
			goto IL_0186;
		}
	}
	{
		G_B25_0 = ((int32_t)9);
		G_B25_1 = G_B23_0;
		goto IL_0187;
	}

IL_0186:
	{
		G_B25_0 = 4;
		G_B25_1 = G_B24_0;
	}

IL_0187:
	{
		m3847(G_B25_1, G_B25_0, ((((int32_t)V_3) == ((int32_t)((int32_t)68)))? 1 : 0), &m3847_MI);
		goto IL_01ec;
	}

IL_0196:
	{
		G_B27_0 = V_2;
		if (!V_1)
		{
			G_B28_0 = V_2;
			goto IL_01a3;
		}
	}
	{
		G_B29_0 = 8;
		G_B29_1 = G_B27_0;
		goto IL_01a4;
	}

IL_01a3:
	{
		G_B29_0 = 3;
		G_B29_1 = G_B28_0;
	}

IL_01a4:
	{
		m3847(G_B29_1, G_B29_0, ((((int32_t)V_3) == ((int32_t)((int32_t)87)))? 1 : 0), &m3847_MI);
		goto IL_01ec;
	}

IL_01b3:
	{
		G_B31_0 = V_2;
		if (!V_1)
		{
			G_B32_0 = V_2;
			goto IL_01c1;
		}
	}
	{
		G_B33_0 = ((int32_t)10);
		G_B33_1 = G_B31_0;
		goto IL_01c2;
	}

IL_01c1:
	{
		G_B33_0 = 5;
		G_B33_1 = G_B32_0;
	}

IL_01c2:
	{
		m3847(G_B33_1, G_B33_0, ((((int32_t)V_3) == ((int32_t)((int32_t)83)))? 1 : 0), &m3847_MI);
		goto IL_01ec;
	}

IL_01d1:
	{
		uint16_t L_20 = m3734(__this, &m3734_MI);
		m3847(V_2, L_20, ((((int32_t)V_3) == ((int32_t)((int32_t)80)))? 1 : 0), &m3847_MI);
		goto IL_01ec;
	}

IL_01e7:
	{
		goto IL_0212;
	}

IL_01ec:
	{
		if (!V_5)
		{
			goto IL_020a;
		}
	}
	{
		int32_t L_21 = V_3;
		t29 * L_22 = Box(InitializedTypeInfo(&t44_TI), &L_21);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_23 = m1312(NULL, (t7*) &_stringLiteral496, L_22, &m1312_MI);
		t305 * L_24 = m3750(__this, L_23, &m3750_MI);
		il2cpp_codegen_raise_exception(L_24);
	}

IL_020a:
	{
		V_4 = (-1);
		goto IL_027f;
	}

IL_0212:
	{
		if (!V_5)
		{
			goto IL_0274;
		}
	}
	{
		if ((((int32_t)V_3) >= ((int32_t)V_4)))
		{
			goto IL_025e;
		}
	}
	{
		t316* L_25 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), 5));
		ArrayElementTypeCheck (L_25, (t7*) &_stringLiteral175);
		*((t29 **)(t29 **)SZArrayLdElema(L_25, 0)) = (t29 *)(t7*) &_stringLiteral175;
		t316* L_26 = L_25;
		int32_t L_27 = V_4;
		t29 * L_28 = Box(InitializedTypeInfo(&t44_TI), &L_27);
		ArrayElementTypeCheck (L_26, L_28);
		*((t29 **)(t29 **)SZArrayLdElema(L_26, 1)) = (t29 *)L_28;
		t316* L_29 = L_26;
		ArrayElementTypeCheck (L_29, (t7*) &_stringLiteral83);
		*((t29 **)(t29 **)SZArrayLdElema(L_29, 2)) = (t29 *)(t7*) &_stringLiteral83;
		t316* L_30 = L_29;
		int32_t L_31 = V_3;
		t29 * L_32 = Box(InitializedTypeInfo(&t44_TI), &L_31);
		ArrayElementTypeCheck (L_30, L_32);
		*((t29 **)(t29 **)SZArrayLdElema(L_30, 3)) = (t29 *)L_32;
		t316* L_33 = L_30;
		ArrayElementTypeCheck (L_33, (t7*) &_stringLiteral497);
		*((t29 **)(t29 **)SZArrayLdElema(L_33, 4)) = (t29 *)(t7*) &_stringLiteral497;
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_34 = m1387(NULL, L_33, &m1387_MI);
		t305 * L_35 = m3750(__this, L_34, &m3750_MI);
		il2cpp_codegen_raise_exception(L_35);
	}

IL_025e:
	{
		m3849(V_2, (((uint16_t)V_4)), (((uint16_t)V_3)), &m3849_MI);
		V_4 = (-1);
		V_5 = 0;
		goto IL_027f;
	}

IL_0274:
	{
		m3848(V_2, (((uint16_t)V_3)), &m3848_MI);
		V_4 = V_3;
	}

IL_027f:
	{
		int32_t L_36 = (__this->f1);
		t7* L_37 = (__this->f0);
		int32_t L_38 = m1715(L_37, &m1715_MI);
		if ((((int32_t)L_36) < ((int32_t)L_38)))
		{
			goto IL_007c;
		}
	}

IL_0295:
	{
		if (V_6)
		{
			goto IL_02a8;
		}
	}
	{
		t305 * L_39 = m3750(__this, (t7*) &_stringLiteral498, &m3750_MI);
		il2cpp_codegen_raise_exception(L_39);
	}

IL_02a8:
	{
		if (!V_5)
		{
			goto IL_02b7;
		}
	}
	{
		m3848(V_2, ((int32_t)45), &m3848_MI);
	}

IL_02b7:
	{
		return V_2;
	}
}
 bool m3733 (t871 * __this, int32_t* p0, int32_t* p1, int32_t p2, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	uint16_t V_3 = 0x0;
	{
		int32_t L_0 = 0;
		V_2 = L_0;
		*((int32_t*)(p1)) = (int32_t)L_0;
		*((int32_t*)(p0)) = (int32_t)V_2;
		bool L_1 = m3748(NULL, p2, &m3748_MI);
		m3741(__this, L_1, &m3741_MI);
		t7* L_2 = (__this->f0);
		int32_t L_3 = (__this->f1);
		uint16_t L_4 = m1741(L_2, L_3, &m1741_MI);
		if ((((uint32_t)L_4) != ((uint32_t)((int32_t)44))))
		{
			goto IL_0033;
		}
	}
	{
		V_0 = (-1);
		goto IL_004a;
	}

IL_0033:
	{
		int32_t L_5 = m3739(__this, ((int32_t)10), 1, 0, &m3739_MI);
		V_0 = L_5;
		bool L_6 = m3748(NULL, p2, &m3748_MI);
		m3741(__this, L_6, &m3741_MI);
	}

IL_004a:
	{
		t7* L_7 = (__this->f0);
		int32_t L_8 = (__this->f1);
		int32_t L_9 = L_8;
		V_2 = L_9;
		__this->f1 = ((int32_t)(L_9+1));
		uint16_t L_10 = m1741(L_7, V_2, &m1741_MI);
		V_3 = L_10;
		if ((((int32_t)V_3) == ((int32_t)((int32_t)44))))
		{
			goto IL_0083;
		}
	}
	{
		if ((((int32_t)V_3) == ((int32_t)((int32_t)125))))
		{
			goto IL_007c;
		}
	}
	{
		goto IL_00d0;
	}

IL_007c:
	{
		V_1 = V_0;
		goto IL_00d2;
	}

IL_0083:
	{
		bool L_11 = m3748(NULL, p2, &m3748_MI);
		m3741(__this, L_11, &m3741_MI);
		int32_t L_12 = m3739(__this, ((int32_t)10), 1, 0, &m3739_MI);
		V_1 = L_12;
		bool L_13 = m3748(NULL, p2, &m3748_MI);
		m3741(__this, L_13, &m3741_MI);
		t7* L_14 = (__this->f0);
		int32_t L_15 = (__this->f1);
		int32_t L_16 = L_15;
		V_2 = L_16;
		__this->f1 = ((int32_t)(L_16+1));
		uint16_t L_17 = m1741(L_14, V_2, &m1741_MI);
		if ((((int32_t)L_17) == ((int32_t)((int32_t)125))))
		{
			goto IL_00cb;
		}
	}
	{
		return 0;
	}

IL_00cb:
	{
		goto IL_00d2;
	}

IL_00d0:
	{
		return 0;
	}

IL_00d2:
	{
		if ((((int32_t)V_0) > ((int32_t)((int32_t)2147483647))))
		{
			goto IL_00e8;
		}
	}
	{
		if ((((int32_t)V_1) <= ((int32_t)((int32_t)2147483647))))
		{
			goto IL_00f4;
		}
	}

IL_00e8:
	{
		t305 * L_18 = m3750(__this, (t7*) &_stringLiteral499, &m3750_MI);
		il2cpp_codegen_raise_exception(L_18);
	}

IL_00f4:
	{
		if ((((int32_t)V_1) < ((int32_t)0)))
		{
			goto IL_010e;
		}
	}
	{
		if ((((int32_t)V_1) >= ((int32_t)V_0)))
		{
			goto IL_010e;
		}
	}
	{
		t305 * L_19 = m3750(__this, (t7*) &_stringLiteral500, &m3750_MI);
		il2cpp_codegen_raise_exception(L_19);
	}

IL_010e:
	{
		*((int32_t*)(p0)) = (int32_t)V_0;
		if ((((int32_t)V_1) <= ((int32_t)0)))
		{
			goto IL_0120;
		}
	}
	{
		*((int32_t*)(p1)) = (int32_t)V_1;
		goto IL_0127;
	}

IL_0120:
	{
		*((int32_t*)(p1)) = (int32_t)((int32_t)2147483647);
	}

IL_0127:
	{
		return 1;
	}
}
 uint16_t m3734 (t871 * __this, MethodInfo* method){
	t7* V_0 = {0};
	uint16_t V_1 = {0};
	int32_t V_2 = 0;
	{
		t7* L_0 = (__this->f0);
		int32_t L_1 = (__this->f1);
		int32_t L_2 = L_1;
		V_2 = L_2;
		__this->f1 = ((int32_t)(L_2+1));
		uint16_t L_3 = m1741(L_0, V_2, &m1741_MI);
		if ((((int32_t)L_3) == ((int32_t)((int32_t)123))))
		{
			goto IL_002f;
		}
	}
	{
		t305 * L_4 = m3750(__this, (t7*) &_stringLiteral501, &m3750_MI);
		il2cpp_codegen_raise_exception(L_4);
	}

IL_002f:
	{
		t7* L_5 = (__this->f0);
		int32_t* L_6 = &(__this->f1);
		t7* L_7 = m3725(NULL, L_5, L_6, &m3725_MI);
		V_0 = L_7;
		if (V_0)
		{
			goto IL_0053;
		}
	}
	{
		t305 * L_8 = m3750(__this, (t7*) &_stringLiteral501, &m3750_MI);
		il2cpp_codegen_raise_exception(L_8);
	}

IL_0053:
	{
		uint16_t L_9 = m3587(NULL, V_0, &m3587_MI);
		V_1 = L_9;
		if (V_1)
		{
			goto IL_0077;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_10 = m1685(NULL, (t7*) &_stringLiteral502, V_0, (t7*) &_stringLiteral503, &m1685_MI);
		t305 * L_11 = m3750(__this, L_10, &m3750_MI);
		il2cpp_codegen_raise_exception(L_11);
	}

IL_0077:
	{
		t7* L_12 = (__this->f0);
		int32_t L_13 = (__this->f1);
		int32_t L_14 = L_13;
		V_2 = L_14;
		__this->f1 = ((int32_t)(L_14+1));
		uint16_t L_15 = m1741(L_12, V_2, &m1741_MI);
		if ((((int32_t)L_15) == ((int32_t)((int32_t)125))))
		{
			goto IL_00a6;
		}
	}
	{
		t305 * L_16 = m3750(__this, (t7*) &_stringLiteral501, &m3750_MI);
		il2cpp_codegen_raise_exception(L_16);
	}

IL_00a6:
	{
		return V_1;
	}
}
 t875 * m3735 (t871 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	bool V_1 = false;
	t875 * V_2 = {0};
	int32_t V_3 = 0;
	t889 * V_4 = {0};
	uint16_t V_5 = 0x0;
	t7* V_6 = {0};
	t889 * V_7 = {0};
	int32_t V_8 = 0;
	uint16_t V_9 = 0x0;
	int32_t G_B11_0 = 0;
	int32_t G_B15_0 = 0;
	int32_t G_B19_0 = 0;
	int32_t G_B24_0 = 0;
	int32_t G_B28_0 = 0;
	int32_t G_B32_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		V_0 = L_0;
		bool L_1 = m3749(NULL, p0, &m3749_MI);
		V_1 = L_1;
		V_2 = (t875 *)NULL;
		t7* L_2 = (__this->f0);
		int32_t L_3 = (__this->f1);
		int32_t L_4 = L_3;
		V_8 = L_4;
		__this->f1 = ((int32_t)(L_4+1));
		uint16_t L_5 = m1741(L_2, V_8, &m1741_MI);
		V_9 = L_5;
		if (((uint16_t)(V_9-((int32_t)49))) == 0)
		{
			goto IL_0229;
		}
		if (((uint16_t)(V_9-((int32_t)49))) == 1)
		{
			goto IL_0229;
		}
		if (((uint16_t)(V_9-((int32_t)49))) == 2)
		{
			goto IL_0229;
		}
		if (((uint16_t)(V_9-((int32_t)49))) == 3)
		{
			goto IL_0229;
		}
		if (((uint16_t)(V_9-((int32_t)49))) == 4)
		{
			goto IL_0229;
		}
		if (((uint16_t)(V_9-((int32_t)49))) == 5)
		{
			goto IL_0229;
		}
		if (((uint16_t)(V_9-((int32_t)49))) == 6)
		{
			goto IL_0229;
		}
		if (((uint16_t)(V_9-((int32_t)49))) == 7)
		{
			goto IL_0229;
		}
		if (((uint16_t)(V_9-((int32_t)49))) == 8)
		{
			goto IL_0229;
		}
		if (((uint16_t)(V_9-((int32_t)49))) == 9)
		{
			goto IL_0096;
		}
		if (((uint16_t)(V_9-((int32_t)49))) == 10)
		{
			goto IL_0096;
		}
		if (((uint16_t)(V_9-((int32_t)49))) == 11)
		{
			goto IL_0096;
		}
		if (((uint16_t)(V_9-((int32_t)49))) == 12)
		{
			goto IL_0096;
		}
		if (((uint16_t)(V_9-((int32_t)49))) == 13)
		{
			goto IL_0096;
		}
		if (((uint16_t)(V_9-((int32_t)49))) == 14)
		{
			goto IL_0096;
		}
		if (((uint16_t)(V_9-((int32_t)49))) == 15)
		{
			goto IL_0096;
		}
		if (((uint16_t)(V_9-((int32_t)49))) == 16)
		{
			goto IL_01e0;
		}
		if (((uint16_t)(V_9-((int32_t)49))) == 17)
		{
			goto IL_021c;
		}
		if (((uint16_t)(V_9-((int32_t)49))) == 18)
		{
			goto IL_0096;
		}
		if (((uint16_t)(V_9-((int32_t)49))) == 19)
		{
			goto IL_0181;
		}
		if (((uint16_t)(V_9-((int32_t)49))) == 20)
		{
			goto IL_0096;
		}
		if (((uint16_t)(V_9-((int32_t)49))) == 21)
		{
			goto IL_0096;
		}
		if (((uint16_t)(V_9-((int32_t)49))) == 22)
		{
			goto IL_0204;
		}
	}

IL_0096:
	{
		if (((uint16_t)(V_9-((int32_t)80))) == 0)
		{
			goto IL_01ce;
		}
		if (((uint16_t)(V_9-((int32_t)80))) == 1)
		{
			goto IL_00b0;
		}
		if (((uint16_t)(V_9-((int32_t)80))) == 2)
		{
			goto IL_00b0;
		}
		if (((uint16_t)(V_9-((int32_t)80))) == 3)
		{
			goto IL_01b4;
		}
	}

IL_00b0:
	{
		if (((uint16_t)(V_9-((int32_t)87))) == 0)
		{
			goto IL_019b;
		}
		if (((uint16_t)(V_9-((int32_t)87))) == 1)
		{
			goto IL_00ca;
		}
		if (((uint16_t)(V_9-((int32_t)87))) == 2)
		{
			goto IL_00ca;
		}
		if (((uint16_t)(V_9-((int32_t)87))) == 3)
		{
			goto IL_01ec;
		}
	}

IL_00ca:
	{
		if (((uint16_t)(V_9-((int32_t)112))) == 0)
		{
			goto IL_016f;
		}
		if (((uint16_t)(V_9-((int32_t)112))) == 1)
		{
			goto IL_00e4;
		}
		if (((uint16_t)(V_9-((int32_t)112))) == 2)
		{
			goto IL_00e4;
		}
		if (((uint16_t)(V_9-((int32_t)112))) == 3)
		{
			goto IL_0155;
		}
	}

IL_00e4:
	{
		if (((uint16_t)(V_9-((int32_t)119))) == 0)
		{
			goto IL_013c;
		}
		if (((uint16_t)(V_9-((int32_t)119))) == 1)
		{
			goto IL_00fe;
		}
		if (((uint16_t)(V_9-((int32_t)119))) == 2)
		{
			goto IL_00fe;
		}
		if (((uint16_t)(V_9-((int32_t)119))) == 3)
		{
			goto IL_01f8;
		}
	}

IL_00fe:
	{
		if (((uint16_t)(V_9-((int32_t)98))) == 0)
		{
			goto IL_0210;
		}
		if (((uint16_t)(V_9-((int32_t)98))) == 1)
		{
			goto IL_0114;
		}
		if (((uint16_t)(V_9-((int32_t)98))) == 2)
		{
			goto IL_0122;
		}
	}

IL_0114:
	{
		if ((((int32_t)V_9) == ((int32_t)((int32_t)107))))
		{
			goto IL_027c;
		}
	}
	{
		goto IL_0328;
	}

IL_0122:
	{
		if (!V_1)
		{
			goto IL_012f;
		}
	}
	{
		G_B11_0 = ((int32_t)9);
		goto IL_0130;
	}

IL_012f:
	{
		G_B11_0 = 4;
	}

IL_0130:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t891_TI));
		t891 * L_6 = (t891 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t891_TI));
		m3845(L_6, G_B11_0, 0, &m3845_MI);
		V_2 = L_6;
		goto IL_032f;
	}

IL_013c:
	{
		if (!V_1)
		{
			goto IL_0148;
		}
	}
	{
		G_B15_0 = 8;
		goto IL_0149;
	}

IL_0148:
	{
		G_B15_0 = 3;
	}

IL_0149:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t891_TI));
		t891 * L_7 = (t891 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t891_TI));
		m3845(L_7, G_B15_0, 0, &m3845_MI);
		V_2 = L_7;
		goto IL_032f;
	}

IL_0155:
	{
		if (!V_1)
		{
			goto IL_0162;
		}
	}
	{
		G_B19_0 = ((int32_t)10);
		goto IL_0163;
	}

IL_0162:
	{
		G_B19_0 = 5;
	}

IL_0163:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t891_TI));
		t891 * L_8 = (t891 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t891_TI));
		m3845(L_8, G_B19_0, 0, &m3845_MI);
		V_2 = L_8;
		goto IL_032f;
	}

IL_016f:
	{
		uint16_t L_9 = m3734(__this, &m3734_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t891_TI));
		t891 * L_10 = (t891 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t891_TI));
		m3845(L_10, L_9, 0, &m3845_MI);
		V_2 = L_10;
		goto IL_032f;
	}

IL_0181:
	{
		if (!V_1)
		{
			goto IL_018e;
		}
	}
	{
		G_B24_0 = ((int32_t)9);
		goto IL_018f;
	}

IL_018e:
	{
		G_B24_0 = 4;
	}

IL_018f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t891_TI));
		t891 * L_11 = (t891 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t891_TI));
		m3845(L_11, G_B24_0, 1, &m3845_MI);
		V_2 = L_11;
		goto IL_032f;
	}

IL_019b:
	{
		if (!V_1)
		{
			goto IL_01a7;
		}
	}
	{
		G_B28_0 = 8;
		goto IL_01a8;
	}

IL_01a7:
	{
		G_B28_0 = 3;
	}

IL_01a8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t891_TI));
		t891 * L_12 = (t891 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t891_TI));
		m3845(L_12, G_B28_0, 1, &m3845_MI);
		V_2 = L_12;
		goto IL_032f;
	}

IL_01b4:
	{
		if (!V_1)
		{
			goto IL_01c1;
		}
	}
	{
		G_B32_0 = ((int32_t)10);
		goto IL_01c2;
	}

IL_01c1:
	{
		G_B32_0 = 5;
	}

IL_01c2:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t891_TI));
		t891 * L_13 = (t891 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t891_TI));
		m3845(L_13, G_B32_0, 1, &m3845_MI);
		V_2 = L_13;
		goto IL_032f;
	}

IL_01ce:
	{
		uint16_t L_14 = m3734(__this, &m3734_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t891_TI));
		t891 * L_15 = (t891 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t891_TI));
		m3845(L_15, L_14, 1, &m3845_MI);
		V_2 = L_15;
		goto IL_032f;
	}

IL_01e0:
	{
		t888 * L_16 = (t888 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t888_TI));
		m3829(L_16, 2, &m3829_MI);
		V_2 = L_16;
		goto IL_032f;
	}

IL_01ec:
	{
		t888 * L_17 = (t888 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t888_TI));
		m3829(L_17, 5, &m3829_MI);
		V_2 = L_17;
		goto IL_032f;
	}

IL_01f8:
	{
		t888 * L_18 = (t888 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t888_TI));
		m3829(L_18, 6, &m3829_MI);
		V_2 = L_18;
		goto IL_032f;
	}

IL_0204:
	{
		t888 * L_19 = (t888 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t888_TI));
		m3829(L_19, 4, &m3829_MI);
		V_2 = L_19;
		goto IL_032f;
	}

IL_0210:
	{
		t888 * L_20 = (t888 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t888_TI));
		m3829(L_20, 8, &m3829_MI);
		V_2 = L_20;
		goto IL_032f;
	}

IL_021c:
	{
		t888 * L_21 = (t888 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t888_TI));
		m3829(L_21, ((int32_t)9), &m3829_MI);
		V_2 = L_21;
		goto IL_032f;
	}

IL_0229:
	{
		int32_t L_22 = (__this->f1);
		__this->f1 = ((int32_t)(L_22-1));
		int32_t L_23 = m3739(__this, ((int32_t)10), 1, 0, &m3739_MI);
		V_3 = L_23;
		if ((((int32_t)V_3) >= ((int32_t)0)))
		{
			goto IL_0252;
		}
	}
	{
		__this->f1 = V_0;
		return (t875 *)NULL;
	}

IL_0252:
	{
		bool L_24 = m3744(NULL, p0, &m3744_MI);
		t890 * L_25 = (t890 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t890_TI));
		m3841(L_25, L_24, V_1, &m3841_MI);
		V_4 = L_25;
		t719 * L_26 = (__this->f3);
		t7* L_27 = m2943((&V_3), &m2943_MI);
		VirtActionInvoker2< t29 *, t29 * >::Invoke(&m3990_MI, L_26, V_4, L_27);
		V_2 = V_4;
		goto IL_032f;
	}

IL_027c:
	{
		t7* L_28 = (__this->f0);
		int32_t L_29 = (__this->f1);
		int32_t L_30 = L_29;
		V_8 = L_30;
		__this->f1 = ((int32_t)(L_30+1));
		uint16_t L_31 = m1741(L_28, V_8, &m1741_MI);
		V_5 = L_31;
		if ((((uint32_t)V_5) != ((uint32_t)((int32_t)60))))
		{
			goto IL_02ae;
		}
	}
	{
		V_5 = ((int32_t)62);
		goto IL_02c3;
	}

IL_02ae:
	{
		if ((((int32_t)V_5) == ((int32_t)((int32_t)39))))
		{
			goto IL_02c3;
		}
	}
	{
		t305 * L_32 = m3750(__this, (t7*) &_stringLiteral504, &m3750_MI);
		il2cpp_codegen_raise_exception(L_32);
	}

IL_02c3:
	{
		t7* L_33 = m3737(__this, &m3737_MI);
		V_6 = L_33;
		if (!V_6)
		{
			goto IL_02ea;
		}
	}
	{
		t7* L_34 = (__this->f0);
		int32_t L_35 = (__this->f1);
		uint16_t L_36 = m1741(L_34, L_35, &m1741_MI);
		if ((((int32_t)L_36) == ((int32_t)V_5)))
		{
			goto IL_02f6;
		}
	}

IL_02ea:
	{
		t305 * L_37 = m3750(__this, (t7*) &_stringLiteral504, &m3750_MI);
		il2cpp_codegen_raise_exception(L_37);
	}

IL_02f6:
	{
		int32_t L_38 = (__this->f1);
		__this->f1 = ((int32_t)(L_38+1));
		bool L_39 = m3744(NULL, p0, &m3744_MI);
		t889 * L_40 = (t889 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t889_TI));
		m3834(L_40, L_39, &m3834_MI);
		V_7 = L_40;
		t719 * L_41 = (__this->f3);
		VirtActionInvoker2< t29 *, t29 * >::Invoke(&m3990_MI, L_41, V_7, V_6);
		V_2 = V_7;
		goto IL_032f;
	}

IL_0328:
	{
		V_2 = (t875 *)NULL;
		goto IL_032f;
	}

IL_032f:
	{
		if (V_2)
		{
			goto IL_033c;
		}
	}
	{
		__this->f1 = V_0;
	}

IL_033c:
	{
		return V_2;
	}
}
 int32_t m3736 (t871 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	uint16_t V_5 = 0x0;
	{
		int32_t L_0 = (__this->f1);
		V_0 = L_0;
		t7* L_1 = (__this->f0);
		int32_t L_2 = m1715(L_1, &m1715_MI);
		if ((((int32_t)V_0) < ((int32_t)L_2)))
		{
			goto IL_0034;
		}
	}
	{
		t7* L_3 = (__this->f0);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_4 = m1535(NULL, (t7*) &_stringLiteral505, L_3, &m1535_MI);
		t7* L_5 = (__this->f0);
		t305 * L_6 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m3973(L_6, L_4, L_5, &m3973_MI);
		il2cpp_codegen_raise_exception(L_6);
	}

IL_0034:
	{
		t7* L_7 = (__this->f0);
		int32_t L_8 = (__this->f1);
		int32_t L_9 = L_8;
		V_4 = L_9;
		__this->f1 = ((int32_t)(L_9+1));
		uint16_t L_10 = m1741(L_7, V_4, &m1741_MI);
		V_5 = L_10;
		if (((uint16_t)(V_5-((int32_t)110))) == 0)
		{
			goto IL_00d1;
		}
		if (((uint16_t)(V_5-((int32_t)110))) == 1)
		{
			goto IL_008a;
		}
		if (((uint16_t)(V_5-((int32_t)110))) == 2)
		{
			goto IL_008a;
		}
		if (((uint16_t)(V_5-((int32_t)110))) == 3)
		{
			goto IL_008a;
		}
		if (((uint16_t)(V_5-((int32_t)110))) == 4)
		{
			goto IL_00c8;
		}
		if (((uint16_t)(V_5-((int32_t)110))) == 5)
		{
			goto IL_008a;
		}
		if (((uint16_t)(V_5-((int32_t)110))) == 6)
		{
			goto IL_00c5;
		}
		if (((uint16_t)(V_5-((int32_t)110))) == 7)
		{
			goto IL_0140;
		}
		if (((uint16_t)(V_5-((int32_t)110))) == 8)
		{
			goto IL_00cb;
		}
		if (((uint16_t)(V_5-((int32_t)110))) == 9)
		{
			goto IL_008a;
		}
		if (((uint16_t)(V_5-((int32_t)110))) == 10)
		{
			goto IL_0118;
		}
	}

IL_008a:
	{
		if (((uint16_t)(V_5-((int32_t)97))) == 0)
		{
			goto IL_00c3;
		}
		if (((uint16_t)(V_5-((int32_t)97))) == 1)
		{
			goto IL_00ac;
		}
		if (((uint16_t)(V_5-((int32_t)97))) == 2)
		{
			goto IL_0168;
		}
		if (((uint16_t)(V_5-((int32_t)97))) == 3)
		{
			goto IL_00ac;
		}
		if (((uint16_t)(V_5-((int32_t)97))) == 4)
		{
			goto IL_00d4;
		}
		if (((uint16_t)(V_5-((int32_t)97))) == 5)
		{
			goto IL_00ce;
		}
	}

IL_00ac:
	{
		if ((((int32_t)V_5) == ((int32_t)((int32_t)48))))
		{
			goto IL_00da;
		}
	}
	{
		if ((((int32_t)V_5) == ((int32_t)((int32_t)92))))
		{
			goto IL_00d7;
		}
	}
	{
		goto IL_01a8;
	}

IL_00c3:
	{
		return 7;
	}

IL_00c5:
	{
		return ((int32_t)9);
	}

IL_00c8:
	{
		return ((int32_t)13);
	}

IL_00cb:
	{
		return ((int32_t)11);
	}

IL_00ce:
	{
		return ((int32_t)12);
	}

IL_00d1:
	{
		return ((int32_t)10);
	}

IL_00d4:
	{
		return ((int32_t)27);
	}

IL_00d7:
	{
		return ((int32_t)92);
	}

IL_00da:
	{
		int32_t L_11 = (__this->f1);
		__this->f1 = ((int32_t)(L_11-1));
		int32_t L_12 = (__this->f1);
		V_2 = L_12;
		t7* L_13 = (__this->f0);
		int32_t* L_14 = &(__this->f1);
		int32_t L_15 = m3722(NULL, L_13, L_14, &m3722_MI);
		V_3 = L_15;
		if ((((uint32_t)V_3) != ((uint32_t)(-1))))
		{
			goto IL_0116;
		}
	}
	{
		int32_t L_16 = (__this->f1);
		if ((((uint32_t)V_2) != ((uint32_t)L_16)))
		{
			goto IL_0116;
		}
	}
	{
		return 0;
	}

IL_0116:
	{
		return V_3;
	}

IL_0118:
	{
		t7* L_17 = (__this->f0);
		int32_t* L_18 = &(__this->f1);
		int32_t L_19 = m3723(NULL, L_17, L_18, 2, &m3723_MI);
		V_1 = L_19;
		if ((((int32_t)V_1) >= ((int32_t)0)))
		{
			goto IL_013e;
		}
	}
	{
		t305 * L_20 = m3750(__this, (t7*) &_stringLiteral506, &m3750_MI);
		il2cpp_codegen_raise_exception(L_20);
	}

IL_013e:
	{
		return V_1;
	}

IL_0140:
	{
		t7* L_21 = (__this->f0);
		int32_t* L_22 = &(__this->f1);
		int32_t L_23 = m3723(NULL, L_21, L_22, 4, &m3723_MI);
		V_1 = L_23;
		if ((((int32_t)V_1) >= ((int32_t)0)))
		{
			goto IL_0166;
		}
	}
	{
		t305 * L_24 = m3750(__this, (t7*) &_stringLiteral506, &m3750_MI);
		il2cpp_codegen_raise_exception(L_24);
	}

IL_0166:
	{
		return V_1;
	}

IL_0168:
	{
		t7* L_25 = (__this->f0);
		int32_t L_26 = (__this->f1);
		int32_t L_27 = L_26;
		V_4 = L_27;
		__this->f1 = ((int32_t)(L_27+1));
		uint16_t L_28 = m1741(L_25, V_4, &m1741_MI);
		V_1 = L_28;
		if ((((int32_t)V_1) < ((int32_t)((int32_t)64))))
		{
			goto IL_019c;
		}
	}
	{
		if ((((int32_t)V_1) > ((int32_t)((int32_t)95))))
		{
			goto IL_019c;
		}
	}
	{
		return ((int32_t)(V_1-((int32_t)64)));
	}

IL_019c:
	{
		t305 * L_29 = m3750(__this, (t7*) &_stringLiteral507, &m3750_MI);
		il2cpp_codegen_raise_exception(L_29);
	}

IL_01a8:
	{
		__this->f1 = V_0;
		return (-1);
	}
}
 t7* m3737 (t871 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f0);
		int32_t* L_1 = &(__this->f1);
		t7* L_2 = m3725(NULL, L_0, L_1, &m3725_MI);
		return L_2;
	}
}
 bool m3738 (t29 * __this, uint16_t p0, MethodInfo* method){
	int32_t V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t194_TI));
		int32_t L_0 = m4224(NULL, p0, &m4224_MI);
		V_0 = L_0;
		if ((((uint32_t)V_0) != ((uint32_t)3)))
		{
			goto IL_0010;
		}
	}
	{
		return 0;
	}

IL_0010:
	{
		if ((((uint32_t)V_0) != ((uint32_t)((int32_t)18))))
		{
			goto IL_001a;
		}
	}
	{
		return 1;
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t194_TI));
		bool L_1 = m4221(NULL, p0, &m4221_MI);
		return L_1;
	}
}
 int32_t m3739 (t871 * __this, int32_t p0, int32_t p1, int32_t p2, MethodInfo* method){
	{
		t7* L_0 = (__this->f0);
		int32_t* L_1 = &(__this->f1);
		int32_t L_2 = m3724(NULL, L_0, L_1, p0, p1, p2, &m3724_MI);
		return L_2;
	}
}
 int32_t m3740 (t29 * __this, uint16_t p0, int32_t p1, int32_t p2, MethodInfo* method){
	int32_t V_0 = 0;
	{
		V_0 = p1;
		if (((int32_t)(V_0-8)) == 0)
		{
			goto IL_0023;
		}
		if (((int32_t)(V_0-8)) == 1)
		{
			goto IL_0016;
		}
		if (((int32_t)(V_0-8)) == 2)
		{
			goto IL_003a;
		}
	}

IL_0016:
	{
		if ((((int32_t)V_0) == ((int32_t)((int32_t)16))))
		{
			goto IL_0051;
		}
	}
	{
		goto IL_0098;
	}

IL_0023:
	{
		if ((((int32_t)p0) < ((int32_t)((int32_t)48))))
		{
			goto IL_0038;
		}
	}
	{
		if ((((int32_t)p0) > ((int32_t)((int32_t)55))))
		{
			goto IL_0038;
		}
	}
	{
		return ((uint16_t)(p0-((int32_t)48)));
	}

IL_0038:
	{
		return (-1);
	}

IL_003a:
	{
		if ((((int32_t)p0) < ((int32_t)((int32_t)48))))
		{
			goto IL_004f;
		}
	}
	{
		if ((((int32_t)p0) > ((int32_t)((int32_t)57))))
		{
			goto IL_004f;
		}
	}
	{
		return ((uint16_t)(p0-((int32_t)48)));
	}

IL_004f:
	{
		return (-1);
	}

IL_0051:
	{
		if ((((int32_t)p0) < ((int32_t)((int32_t)48))))
		{
			goto IL_0066;
		}
	}
	{
		if ((((int32_t)p0) > ((int32_t)((int32_t)57))))
		{
			goto IL_0066;
		}
	}
	{
		return ((uint16_t)(p0-((int32_t)48)));
	}

IL_0066:
	{
		if ((((int32_t)p0) < ((int32_t)((int32_t)97))))
		{
			goto IL_007e;
		}
	}
	{
		if ((((int32_t)p0) > ((int32_t)((int32_t)102))))
		{
			goto IL_007e;
		}
	}
	{
		return ((int32_t)(((int32_t)(((int32_t)10)+p0))-((int32_t)97)));
	}

IL_007e:
	{
		if ((((int32_t)p0) < ((int32_t)((int32_t)65))))
		{
			goto IL_0096;
		}
	}
	{
		if ((((int32_t)p0) > ((int32_t)((int32_t)70))))
		{
			goto IL_0096;
		}
	}
	{
		return ((int32_t)(((int32_t)(((int32_t)10)+p0))-((int32_t)65)));
	}

IL_0096:
	{
		return (-1);
	}

IL_0098:
	{
		return (-1);
	}
}
 void m3741 (t871 * __this, bool p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		goto IL_0188;
	}

IL_0005:
	{
		t7* L_0 = (__this->f0);
		int32_t L_1 = (__this->f1);
		uint16_t L_2 = m1741(L_0, L_1, &m1741_MI);
		if ((((uint32_t)L_2) != ((uint32_t)((int32_t)40))))
		{
			goto IL_00bc;
		}
	}
	{
		int32_t L_3 = (__this->f1);
		t7* L_4 = (__this->f0);
		int32_t L_5 = m1715(L_4, &m1715_MI);
		if ((((int32_t)((int32_t)(L_3+3))) < ((int32_t)L_5)))
		{
			goto IL_0036;
		}
	}
	{
		return;
	}

IL_0036:
	{
		t7* L_6 = (__this->f0);
		int32_t L_7 = (__this->f1);
		uint16_t L_8 = m1741(L_6, ((int32_t)(L_7+1)), &m1741_MI);
		if ((((uint32_t)L_8) != ((uint32_t)((int32_t)63))))
		{
			goto IL_006a;
		}
	}
	{
		t7* L_9 = (__this->f0);
		int32_t L_10 = (__this->f1);
		uint16_t L_11 = m1741(L_9, ((int32_t)(L_10+2)), &m1741_MI);
		if ((((int32_t)L_11) == ((int32_t)((int32_t)35))))
		{
			goto IL_006b;
		}
	}

IL_006a:
	{
		return;
	}

IL_006b:
	{
		int32_t L_12 = (__this->f1);
		__this->f1 = ((int32_t)(L_12+3));
		goto IL_007e;
	}

IL_007e:
	{
		int32_t L_13 = (__this->f1);
		t7* L_14 = (__this->f0);
		int32_t L_15 = m1715(L_14, &m1715_MI);
		if ((((int32_t)L_13) >= ((int32_t)L_15)))
		{
			goto IL_00b7;
		}
	}
	{
		t7* L_16 = (__this->f0);
		int32_t L_17 = (__this->f1);
		int32_t L_18 = L_17;
		V_0 = L_18;
		__this->f1 = ((int32_t)(L_18+1));
		uint16_t L_19 = m1741(L_16, V_0, &m1741_MI);
		if ((((uint32_t)L_19) != ((uint32_t)((int32_t)41))))
		{
			goto IL_007e;
		}
	}

IL_00b7:
	{
		goto IL_0188;
	}

IL_00bc:
	{
		if (!p0)
		{
			goto IL_011d;
		}
	}
	{
		t7* L_20 = (__this->f0);
		int32_t L_21 = (__this->f1);
		uint16_t L_22 = m1741(L_20, L_21, &m1741_MI);
		if ((((uint32_t)L_22) != ((uint32_t)((int32_t)35))))
		{
			goto IL_011d;
		}
	}
	{
		goto IL_00df;
	}

IL_00df:
	{
		int32_t L_23 = (__this->f1);
		t7* L_24 = (__this->f0);
		int32_t L_25 = m1715(L_24, &m1715_MI);
		if ((((int32_t)L_23) >= ((int32_t)L_25)))
		{
			goto IL_0118;
		}
	}
	{
		t7* L_26 = (__this->f0);
		int32_t L_27 = (__this->f1);
		int32_t L_28 = L_27;
		V_0 = L_28;
		__this->f1 = ((int32_t)(L_28+1));
		uint16_t L_29 = m1741(L_26, V_0, &m1741_MI);
		if ((((uint32_t)L_29) != ((uint32_t)((int32_t)10))))
		{
			goto IL_00df;
		}
	}

IL_0118:
	{
		goto IL_0188;
	}

IL_011d:
	{
		if (!p0)
		{
			goto IL_0187;
		}
	}
	{
		t7* L_30 = (__this->f0);
		int32_t L_31 = (__this->f1);
		uint16_t L_32 = m1741(L_30, L_31, &m1741_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t194_TI));
		bool L_33 = m4223(NULL, L_32, &m4223_MI);
		if (!L_33)
		{
			goto IL_0187;
		}
	}
	{
		goto IL_0151;
	}

IL_0143:
	{
		int32_t L_34 = (__this->f1);
		__this->f1 = ((int32_t)(L_34+1));
	}

IL_0151:
	{
		int32_t L_35 = (__this->f1);
		t7* L_36 = (__this->f0);
		int32_t L_37 = m1715(L_36, &m1715_MI);
		if ((((int32_t)L_35) >= ((int32_t)L_37)))
		{
			goto IL_0182;
		}
	}
	{
		t7* L_38 = (__this->f0);
		int32_t L_39 = (__this->f1);
		uint16_t L_40 = m1741(L_38, L_39, &m1741_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t194_TI));
		bool L_41 = m4223(NULL, L_40, &m4223_MI);
		if (L_41)
		{
			goto IL_0143;
		}
	}

IL_0182:
	{
		goto IL_0188;
	}

IL_0187:
	{
		return;
	}

IL_0188:
	{
		int32_t L_42 = (__this->f1);
		t7* L_43 = (__this->f0);
		int32_t L_44 = m1715(L_43, &m1715_MI);
		if ((((int32_t)L_42) < ((int32_t)L_44)))
		{
			goto IL_0005;
		}
	}
	{
		return;
	}
}
 void m3742 (t871 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	t719 * V_1 = {0};
	t731 * V_2 = {0};
	t881 * V_3 = {0};
	t29 * V_4 = {0};
	t881 * V_5 = {0};
	t29 * V_6 = {0};
	t881 * V_7 = {0};
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	t7* V_10 = {0};
	t875 * V_11 = {0};
	t29 * V_12 = {0};
	t7* V_13 = {0};
	t890 * V_14 = {0};
	t881 * V_15 = {0};
	t29 * V_16 = {0};
	int32_t V_17 = 0;
	t29 * V_18 = {0};
	t29 * V_19 = {0};
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	t7* G_B50_0 = {0};
	t871 * G_B50_1 = {0};
	t7* G_B49_0 = {0};
	t871 * G_B49_1 = {0};
	t7* G_B51_0 = {0};
	t7* G_B51_1 = {0};
	t871 * G_B51_2 = {0};
	{
		V_0 = 1;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t719_TI));
		t719 * L_0 = (t719 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t719_TI));
		m4209(L_0, &m4209_MI);
		V_1 = L_0;
		V_2 = (t731 *)NULL;
		t731 * L_1 = (__this->f2);
		t29 * L_2 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m3981_MI, L_1);
		V_4 = L_2;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0060;
		}

IL_001c:
		{
			t29 * L_3 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m3970_MI, V_4);
			V_3 = ((t881 *)Castclass(L_3, InitializedTypeInfo(&t881_TI)));
			t7* L_4 = m3781(V_3, &m3781_MI);
			if (!L_4)
			{
				goto IL_0039;
			}
		}

IL_0034:
		{
			goto IL_0060;
		}

IL_0039:
		{
			t7* L_5 = m2943((&V_0), &m2943_MI);
			VirtActionInvoker2< t29 *, t29 * >::Invoke(&m3990_MI, V_1, L_5, V_3);
			int32_t L_6 = V_0;
			V_0 = ((int32_t)(L_6+1));
			m3780(V_3, L_6, &m3780_MI);
			int32_t L_7 = (__this->f4);
			__this->f4 = ((int32_t)(L_7+1));
		}

IL_0060:
		{
			bool L_8 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m3972_MI, V_4);
			if (L_8)
			{
				goto IL_001c;
			}
		}

IL_006c:
		{
			// IL_006c: leave IL_0087
			leaveInstructions[0] = 0x87; // 1
			THROW_SENTINEL(IL_0087);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_0071;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_0071;
	}

IL_0071:
	{ // begin finally (depth: 1)
		{
			V_16 = ((t29 *)IsInst(V_4, InitializedTypeInfo(&t324_TI)));
			if (V_16)
			{
				goto IL_007f;
			}
		}

IL_007e:
		{
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x87:
					goto IL_0087;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}

IL_007f:
		{
			InterfaceActionInvoker0::Invoke(&m1428_MI, V_16);
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x87:
					goto IL_0087;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}
	} // end finally (depth: 1)

IL_0087:
	{
		t731 * L_9 = (__this->f2);
		t29 * L_10 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m3981_MI, L_9);
		V_6 = L_10;
	}

IL_0094:
	try
	{ // begin try (depth: 1)
		{
			goto IL_020a;
		}

IL_0099:
		{
			t29 * L_11 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m3970_MI, V_6);
			V_5 = ((t881 *)Castclass(L_11, InitializedTypeInfo(&t881_TI)));
			t7* L_12 = m3781(V_5, &m3781_MI);
			if (L_12)
			{
				goto IL_00b8;
			}
		}

IL_00b3:
		{
			goto IL_020a;
		}

IL_00b8:
		{
			t7* L_13 = m3781(V_5, &m3781_MI);
			bool L_14 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m4219_MI, V_1, L_13);
			if (!L_14)
			{
				goto IL_011d;
			}
		}

IL_00ca:
		{
			t7* L_15 = m3781(V_5, &m3781_MI);
			t29 * L_16 = (t29 *)VirtFuncInvoker1< t29 *, t29 * >::Invoke(&m3989_MI, V_1, L_15);
			V_7 = ((t881 *)Castclass(L_16, InitializedTypeInfo(&t881_TI)));
			int32_t L_17 = m3779(V_7, &m3779_MI);
			m3780(V_5, L_17, &m3780_MI);
			int32_t L_18 = m3779(V_5, &m3779_MI);
			if ((((uint32_t)L_18) != ((uint32_t)V_0)))
			{
				goto IL_0102;
			}
		}

IL_00f9:
		{
			V_0 = ((int32_t)(V_0+1));
			goto IL_0118;
		}

IL_0102:
		{
			int32_t L_19 = m3779(V_5, &m3779_MI);
			if ((((int32_t)L_19) <= ((int32_t)V_0)))
			{
				goto IL_0118;
			}
		}

IL_010f:
		{
			VirtFuncInvoker1< int32_t, t29 * >::Invoke(&m3991_MI, V_2, V_5);
		}

IL_0118:
		{
			goto IL_020a;
		}

IL_011d:
		{
			t7* L_20 = m3781(V_5, &m3781_MI);
			uint16_t L_21 = m1741(L_20, 0, &m1741_MI);
			IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t194_TI));
			bool L_22 = m4222(NULL, L_21, &m4222_MI);
			if (!L_22)
			{
				goto IL_01ac;
			}
		}

IL_0134:
		{
			V_8 = 0;
			t7* L_23 = m3781(V_5, &m3781_MI);
			int32_t L_24 = m3721(NULL, L_23, (&V_8), &m3721_MI);
			V_9 = L_24;
			t7* L_25 = m3781(V_5, &m3781_MI);
			int32_t L_26 = m1715(L_25, &m1715_MI);
			if ((((uint32_t)V_8) != ((uint32_t)L_26)))
			{
				goto IL_01ac;
			}
		}

IL_015a:
		{
			m3780(V_5, V_9, &m3780_MI);
			t7* L_27 = m3781(V_5, &m3781_MI);
			VirtActionInvoker2< t29 *, t29 * >::Invoke(&m3990_MI, V_1, L_27, V_5);
			int32_t L_28 = (__this->f4);
			__this->f4 = ((int32_t)(L_28+1));
			if ((((uint32_t)V_9) != ((uint32_t)V_0)))
			{
				goto IL_0191;
			}
		}

IL_0188:
		{
			V_0 = ((int32_t)(V_0+1));
			goto IL_01a7;
		}

IL_0191:
		{
			if (V_2)
			{
				goto IL_019e;
			}
		}

IL_0197:
		{
			IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t731_TI));
			t731 * L_29 = (t731 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t731_TI));
			m4181(L_29, 4, &m4181_MI);
			V_2 = L_29;
		}

IL_019e:
		{
			VirtFuncInvoker1< int32_t, t29 * >::Invoke(&m3991_MI, V_2, V_5);
		}

IL_01a7:
		{
			goto IL_020a;
		}

IL_01ac:
		{
			t7* L_30 = m2943((&V_0), &m2943_MI);
			V_10 = L_30;
			goto IL_01ca;
		}

IL_01ba:
		{
			int32_t L_31 = ((int32_t)(V_0+1));
			V_0 = L_31;
			V_17 = L_31;
			t7* L_32 = m2943((&V_17), &m2943_MI);
			V_10 = L_32;
		}

IL_01ca:
		{
			bool L_33 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m4219_MI, V_1, V_10);
			if (L_33)
			{
				goto IL_01ba;
			}
		}

IL_01d7:
		{
			VirtActionInvoker2< t29 *, t29 * >::Invoke(&m3990_MI, V_1, V_10, V_5);
			t7* L_34 = m3781(V_5, &m3781_MI);
			VirtActionInvoker2< t29 *, t29 * >::Invoke(&m3990_MI, V_1, L_34, V_5);
			int32_t L_35 = V_0;
			V_0 = ((int32_t)(L_35+1));
			m3780(V_5, L_35, &m3780_MI);
			int32_t L_36 = (__this->f4);
			__this->f4 = ((int32_t)(L_36+1));
		}

IL_020a:
		{
			bool L_37 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m3972_MI, V_6);
			if (L_37)
			{
				goto IL_0099;
			}
		}

IL_0216:
		{
			// IL_0216: leave IL_0231
			leaveInstructions[0] = 0x231; // 1
			THROW_SENTINEL(IL_0231);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_021b;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_021b;
	}

IL_021b:
	{ // begin finally (depth: 1)
		{
			V_18 = ((t29 *)IsInst(V_6, InitializedTypeInfo(&t324_TI)));
			if (V_18)
			{
				goto IL_0229;
			}
		}

IL_0228:
		{
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x231:
					goto IL_0231;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}

IL_0229:
		{
			InterfaceActionInvoker0::Invoke(&m1428_MI, V_18);
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x231:
					goto IL_0231;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}
	} // end finally (depth: 1)

IL_0231:
	{
		__this->f5 = V_0;
		if (!V_2)
		{
			goto IL_0245;
		}
	}
	{
		m3743(__this, V_2, &m3743_MI);
	}

IL_0245:
	{
		t719 * L_38 = (__this->f3);
		t29 * L_39 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m4267_MI, L_38);
		t29 * L_40 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m4154_MI, L_39);
		V_12 = L_40;
	}

IL_0257:
	try
	{ // begin try (depth: 1)
		{
			goto IL_036d;
		}

IL_025c:
		{
			t29 * L_41 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m3970_MI, V_12);
			V_11 = ((t875 *)Castclass(L_41, InitializedTypeInfo(&t875_TI)));
			t719 * L_42 = (__this->f3);
			t29 * L_43 = (t29 *)VirtFuncInvoker1< t29 *, t29 * >::Invoke(&m3989_MI, L_42, V_11);
			V_13 = ((t7*)Castclass(L_43, (&t7_TI)));
			bool L_44 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m4219_MI, V_1, V_13);
			if (L_44)
			{
				goto IL_0306;
			}
		}

IL_028b:
		{
			if (!((t885 *)IsInst(V_11, InitializedTypeInfo(&t885_TI))))
			{
				goto IL_02ae;
			}
		}

IL_0297:
		{
			uint16_t L_45 = m1741(V_13, 0, &m1741_MI);
			IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t194_TI));
			bool L_46 = m4222(NULL, L_45, &m4222_MI);
			if (L_46)
			{
				goto IL_02ae;
			}
		}

IL_02a9:
		{
			goto IL_036d;
		}

IL_02ae:
		{
			V_14 = ((t890 *)IsInst(V_11, InitializedTypeInfo(&t890_TI)));
			if (!V_14)
			{
				goto IL_02d2;
			}
		}

IL_02be:
		{
			bool L_47 = m3842(V_14, V_13, V_1, &m3842_MI);
			if (!L_47)
			{
				goto IL_02d2;
			}
		}

IL_02cd:
		{
			goto IL_036d;
		}

IL_02d2:
		{
			uint16_t L_48 = m1741(V_13, 0, &m1741_MI);
			IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t194_TI));
			bool L_49 = m4222(NULL, L_48, &m4222_MI);
			G_B49_0 = (t7*) &_stringLiteral508;
			G_B49_1 = __this;
			if (!L_49)
			{
				G_B50_0 = (t7*) &_stringLiteral508;
				G_B50_1 = __this;
				goto IL_02f4;
			}
		}

IL_02ea:
		{
			G_B51_0 = (t7*) &_stringLiteral509;
			G_B51_1 = G_B49_0;
			G_B51_2 = G_B49_1;
			goto IL_02f9;
		}

IL_02f4:
		{
			G_B51_0 = (t7*) &_stringLiteral510;
			G_B51_1 = G_B50_0;
			G_B51_2 = G_B50_1;
		}

IL_02f9:
		{
			IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
			t7* L_50 = m1685(NULL, G_B51_1, G_B51_0, V_13, &m1685_MI);
			t305 * L_51 = m3750(G_B51_2, L_50, &m3750_MI);
			il2cpp_codegen_raise_exception(L_51);
		}

IL_0306:
		{
			t29 * L_52 = (t29 *)VirtFuncInvoker1< t29 *, t29 * >::Invoke(&m3989_MI, V_1, V_13);
			V_15 = ((t881 *)Castclass(L_52, InitializedTypeInfo(&t881_TI)));
			if (!((t889 *)IsInst(V_11, InitializedTypeInfo(&t889_TI))))
			{
				goto IL_0334;
			}
		}

IL_0321:
		{
			m3836(((t889 *)Castclass(V_11, InitializedTypeInfo(&t889_TI))), V_15, &m3836_MI);
			goto IL_036d;
		}

IL_0334:
		{
			if (!((t885 *)IsInst(V_11, InitializedTypeInfo(&t885_TI))))
			{
				goto IL_0353;
			}
		}

IL_0340:
		{
			m3807(((t885 *)Castclass(V_11, InitializedTypeInfo(&t885_TI))), V_15, &m3807_MI);
			goto IL_036d;
		}

IL_0353:
		{
			if (!((t882 *)IsInst(V_11, InitializedTypeInfo(&t882_TI))))
			{
				goto IL_036d;
			}
		}

IL_035f:
		{
			m3788(((t882 *)Castclass(V_11, InitializedTypeInfo(&t882_TI))), V_15, &m3788_MI);
		}

IL_036d:
		{
			bool L_53 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m3972_MI, V_12);
			if (L_53)
			{
				goto IL_025c;
			}
		}

IL_0379:
		{
			// IL_0379: leave IL_0394
			leaveInstructions[0] = 0x394; // 1
			THROW_SENTINEL(IL_0394);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_037e;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_037e;
	}

IL_037e:
	{ // begin finally (depth: 1)
		{
			V_19 = ((t29 *)IsInst(V_12, InitializedTypeInfo(&t324_TI)));
			if (V_19)
			{
				goto IL_038c;
			}
		}

IL_038b:
		{
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x394:
					goto IL_0394;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}

IL_038c:
		{
			InterfaceActionInvoker0::Invoke(&m1428_MI, V_19);
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x394:
					goto IL_0394;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}
	} // end finally (depth: 1)

IL_0394:
	{
		return;
	}
}
 void m3743 (t871 * __this, t731 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	t881 * V_3 = {0};
	int32_t V_4 = 0;
	t881 * V_5 = {0};
	{
		int32_t L_0 = (__this->f5);
		V_0 = L_0;
		V_1 = 0;
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m3976_MI, p0);
		V_2 = L_1;
		VirtActionInvoker0::Invoke(&m4264_MI, p0);
		goto IL_004d;
	}

IL_001b:
	{
		t29 * L_2 = (t29 *)VirtFuncInvoker1< t29 *, int32_t >::Invoke(&m3978_MI, p0, V_1);
		V_3 = ((t881 *)Castclass(L_2, InitializedTypeInfo(&t881_TI)));
		int32_t L_3 = m3779(V_3, &m3779_MI);
		if ((((int32_t)L_3) <= ((int32_t)V_0)))
		{
			goto IL_0039;
		}
	}
	{
		goto IL_0054;
	}

IL_0039:
	{
		int32_t L_4 = m3779(V_3, &m3779_MI);
		if ((((uint32_t)L_4) != ((uint32_t)V_0)))
		{
			goto IL_0049;
		}
	}
	{
		V_0 = ((int32_t)(V_0+1));
	}

IL_0049:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_004d:
	{
		if ((((int32_t)V_1) < ((int32_t)V_2)))
		{
			goto IL_001b;
		}
	}

IL_0054:
	{
		__this->f5 = V_0;
		V_4 = V_0;
		goto IL_00a7;
	}

IL_0063:
	{
		t29 * L_5 = (t29 *)VirtFuncInvoker1< t29 *, int32_t >::Invoke(&m3978_MI, p0, V_1);
		V_5 = ((t881 *)Castclass(L_5, InitializedTypeInfo(&t881_TI)));
		int32_t L_6 = m3779(V_5, &m3779_MI);
		if ((((uint32_t)L_6) != ((uint32_t)V_4)))
		{
			goto IL_008e;
		}
	}
	{
		m3780(V_5, ((int32_t)(V_0-1)), &m3780_MI);
		goto IL_00a3;
	}

IL_008e:
	{
		int32_t L_7 = m3779(V_5, &m3779_MI);
		V_4 = L_7;
		int32_t L_8 = V_0;
		V_0 = ((int32_t)(L_8+1));
		m3780(V_5, L_8, &m3780_MI);
	}

IL_00a3:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_00a7:
	{
		if ((((int32_t)V_1) < ((int32_t)V_2)))
		{
			goto IL_0063;
		}
	}
	{
		return;
	}
}
 bool m3744 (t29 * __this, int32_t p0, MethodInfo* method){
	{
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)p0&(int32_t)1))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
 bool m3745 (t29 * __this, int32_t p0, MethodInfo* method){
	{
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)p0&(int32_t)2))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
 bool m3746 (t29 * __this, int32_t p0, MethodInfo* method){
	{
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)p0&(int32_t)4))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
 bool m3747 (t29 * __this, int32_t p0, MethodInfo* method){
	{
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)p0&(int32_t)((int32_t)16)))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
 bool m3748 (t29 * __this, int32_t p0, MethodInfo* method){
	{
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)p0&(int32_t)((int32_t)32)))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
 bool m3749 (t29 * __this, int32_t p0, MethodInfo* method){
	{
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)p0&(int32_t)((int32_t)256)))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
 t305 * m3750 (t871 * __this, t7* p0, MethodInfo* method){
	{
		t7* L_0 = (__this->f0);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_1 = m2926(NULL, (t7*) &_stringLiteral511, L_0, (t7*) &_stringLiteral512, p0, &m2926_MI);
		p0 = L_1;
		t7* L_2 = (__this->f0);
		t305 * L_3 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m3973(L_3, p0, L_2, &m3973_MI);
		return L_3;
	}
}
// Metadata Definition System.Text.RegularExpressions.Syntax.Parser
extern Il2CppType t7_0_0_1;
FieldInfo t871_f0_FieldInfo = 
{
	"pattern", &t7_0_0_1, &t871_TI, offsetof(t871, f0), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t871_f1_FieldInfo = 
{
	"ptr", &t44_0_0_1, &t871_TI, offsetof(t871, f1), &EmptyCustomAttributesCache};
extern Il2CppType t731_0_0_1;
FieldInfo t871_f2_FieldInfo = 
{
	"caps", &t731_0_0_1, &t871_TI, offsetof(t871, f2), &EmptyCustomAttributesCache};
extern Il2CppType t719_0_0_1;
FieldInfo t871_f3_FieldInfo = 
{
	"refs", &t719_0_0_1, &t871_TI, offsetof(t871, f3), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t871_f4_FieldInfo = 
{
	"num_groups", &t44_0_0_1, &t871_TI, offsetof(t871, f4), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t871_f5_FieldInfo = 
{
	"gap", &t44_0_0_1, &t871_TI, offsetof(t871, f5), &EmptyCustomAttributesCache};
static FieldInfo* t871_FIs[] =
{
	&t871_f0_FieldInfo,
	&t871_f1_FieldInfo,
	&t871_f2_FieldInfo,
	&t871_f3_FieldInfo,
	&t871_f4_FieldInfo,
	&t871_f5_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3720_MI = 
{
	".ctor", (methodPointerType)&m3720, &t871_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 714, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t44_1_0_0;
static ParameterInfo t871_m3721_ParameterInfos[] = 
{
	{"str", 0, 134218384, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"ptr", 1, 134218385, &EmptyCustomAttributesCache, &t44_1_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t390 (MethodInfo* method, void* obj, void** args);
MethodInfo m3721_MI = 
{
	"ParseDecimal", (methodPointerType)&m3721, &t871_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t390, t871_m3721_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 2, false, false, 715, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t44_1_0_0;
static ParameterInfo t871_m3722_ParameterInfos[] = 
{
	{"str", 0, 134218386, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"ptr", 1, 134218387, &EmptyCustomAttributesCache, &t44_1_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t390 (MethodInfo* method, void* obj, void** args);
MethodInfo m3722_MI = 
{
	"ParseOctal", (methodPointerType)&m3722, &t871_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t390, t871_m3722_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 2, false, false, 716, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t44_1_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t871_m3723_ParameterInfos[] = 
{
	{"str", 0, 134218388, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"ptr", 1, 134218389, &EmptyCustomAttributesCache, &t44_1_0_0},
	{"digits", 2, 134218390, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t390_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3723_MI = 
{
	"ParseHex", (methodPointerType)&m3723, &t871_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t390_t44, t871_m3723_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 3, false, false, 717, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t44_1_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t871_m3724_ParameterInfos[] = 
{
	{"str", 0, 134218391, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"ptr", 1, 134218392, &EmptyCustomAttributesCache, &t44_1_0_0},
	{"b", 2, 134218393, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"min", 3, 134218394, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"max", 4, 134218395, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t390_t44_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3724_MI = 
{
	"ParseNumber", (methodPointerType)&m3724, &t871_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t390_t44_t44_t44, t871_m3724_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 5, false, false, 718, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t44_1_0_0;
static ParameterInfo t871_m3725_ParameterInfos[] = 
{
	{"str", 0, 134218396, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"ptr", 1, 134218397, &EmptyCustomAttributesCache, &t44_1_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29_t390 (MethodInfo* method, void* obj, void** args);
MethodInfo m3725_MI = 
{
	"ParseName", (methodPointerType)&m3725, &t871_TI, &t7_0_0_0, RuntimeInvoker_t29_t29_t390, t871_m3725_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 2, false, false, 719, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t842_0_0_0;
extern Il2CppType t842_0_0_0;
static ParameterInfo t871_m3726_ParameterInfos[] = 
{
	{"pattern", 0, 134218398, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"options", 1, 134218399, &EmptyCustomAttributesCache, &t842_0_0_0},
};
extern Il2CppType t872_0_0_0;
extern void* RuntimeInvoker_t29_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3726_MI = 
{
	"ParseRegularExpression", (methodPointerType)&m3726, &t871_TI, &t872_0_0_0, RuntimeInvoker_t29_t29_t44, t871_m3726_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 2, false, false, 720, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t719_0_0_0;
extern Il2CppType t719_0_0_0;
static ParameterInfo t871_m3727_ParameterInfos[] = 
{
	{"mapping", 0, 134218400, &EmptyCustomAttributesCache, &t719_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3727_MI = 
{
	"GetMapping", (methodPointerType)&m3727, &t871_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t871_m3727_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 721, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t873_0_0_0;
extern Il2CppType t873_0_0_0;
extern Il2CppType t842_0_0_0;
extern Il2CppType t874_0_0_0;
extern Il2CppType t874_0_0_0;
static ParameterInfo t871_m3728_ParameterInfos[] = 
{
	{"group", 0, 134218401, &EmptyCustomAttributesCache, &t873_0_0_0},
	{"options", 1, 134218402, &EmptyCustomAttributesCache, &t842_0_0_0},
	{"assertion", 2, 134218403, &EmptyCustomAttributesCache, &t874_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3728_MI = 
{
	"ParseGroup", (methodPointerType)&m3728, &t871_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44_t29, t871_m3728_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, false, 722, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t842_1_0_0;
extern Il2CppType t842_1_0_0;
static ParameterInfo t871_m3729_ParameterInfos[] = 
{
	{"options", 0, 134218404, &EmptyCustomAttributesCache, &t842_1_0_0},
};
extern Il2CppType t875_0_0_0;
extern void* RuntimeInvoker_t29_t957 (MethodInfo* method, void* obj, void** args);
MethodInfo m3729_MI = 
{
	"ParseGroupingConstruct", (methodPointerType)&m3729, &t871_TI, &t875_0_0_0, RuntimeInvoker_t29_t957, t871_m3729_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 723, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t876_0_0_0;
extern Il2CppType t876_0_0_0;
static ParameterInfo t871_m3730_ParameterInfos[] = 
{
	{"assertion", 0, 134218405, &EmptyCustomAttributesCache, &t876_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3730_MI = 
{
	"ParseAssertionType", (methodPointerType)&m3730, &t871_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t871_m3730_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 724, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t842_1_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t871_m3731_ParameterInfos[] = 
{
	{"options", 0, 134218406, &EmptyCustomAttributesCache, &t842_1_0_0},
	{"negate", 1, 134218407, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t957_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3731_MI = 
{
	"ParseOptions", (methodPointerType)&m3731, &t871_TI, &t21_0_0_0, RuntimeInvoker_t21_t957_t297, t871_m3731_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 725, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t842_0_0_0;
static ParameterInfo t871_m3732_ParameterInfos[] = 
{
	{"options", 0, 134218408, &EmptyCustomAttributesCache, &t842_0_0_0},
};
extern Il2CppType t875_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3732_MI = 
{
	"ParseCharacterClass", (methodPointerType)&m3732, &t871_TI, &t875_0_0_0, RuntimeInvoker_t29_t44, t871_m3732_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 726, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_1_0_2;
extern Il2CppType t44_1_0_2;
extern Il2CppType t842_0_0_0;
static ParameterInfo t871_m3733_ParameterInfos[] = 
{
	{"min", 0, 134218409, &EmptyCustomAttributesCache, &t44_1_0_2},
	{"max", 1, 134218410, &EmptyCustomAttributesCache, &t44_1_0_2},
	{"options", 2, 134218411, &EmptyCustomAttributesCache, &t842_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t390_t390_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3733_MI = 
{
	"ParseRepetitionBounds", (methodPointerType)&m3733, &t871_TI, &t40_0_0_0, RuntimeInvoker_t40_t390_t390_t44, t871_m3733_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, false, 727, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t849_0_0_0;
extern void* RuntimeInvoker_t849 (MethodInfo* method, void* obj, void** args);
MethodInfo m3734_MI = 
{
	"ParseUnicodeCategory", (methodPointerType)&m3734, &t871_TI, &t849_0_0_0, RuntimeInvoker_t849, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 728, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t842_0_0_0;
static ParameterInfo t871_m3735_ParameterInfos[] = 
{
	{"options", 0, 134218412, &EmptyCustomAttributesCache, &t842_0_0_0},
};
extern Il2CppType t875_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3735_MI = 
{
	"ParseSpecial", (methodPointerType)&m3735, &t871_TI, &t875_0_0_0, RuntimeInvoker_t29_t44, t871_m3735_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 729, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3736_MI = 
{
	"ParseEscape", (methodPointerType)&m3736, &t871_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 730, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3737_MI = 
{
	"ParseName", (methodPointerType)&m3737, &t871_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 731, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t194_0_0_0;
static ParameterInfo t871_m3738_ParameterInfos[] = 
{
	{"c", 0, 134218413, &EmptyCustomAttributesCache, &t194_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t372 (MethodInfo* method, void* obj, void** args);
MethodInfo m3738_MI = 
{
	"IsNameChar", (methodPointerType)&m3738, &t871_TI, &t40_0_0_0, RuntimeInvoker_t40_t372, t871_m3738_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 732, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t871_m3739_ParameterInfos[] = 
{
	{"b", 0, 134218414, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"min", 1, 134218415, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"max", 2, 134218416, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3739_MI = 
{
	"ParseNumber", (methodPointerType)&m3739, &t871_TI, &t44_0_0_0, RuntimeInvoker_t44_t44_t44_t44, t871_m3739_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, false, 733, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t194_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t871_m3740_ParameterInfos[] = 
{
	{"c", 0, 134218417, &EmptyCustomAttributesCache, &t194_0_0_0},
	{"b", 1, 134218418, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"n", 2, 134218419, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t372_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3740_MI = 
{
	"ParseDigit", (methodPointerType)&m3740, &t871_TI, &t44_0_0_0, RuntimeInvoker_t44_t372_t44_t44, t871_m3740_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 3, false, false, 734, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t871_m3741_ParameterInfos[] = 
{
	{"ignore", 0, 134218420, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3741_MI = 
{
	"ConsumeWhitespace", (methodPointerType)&m3741, &t871_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t871_m3741_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 735, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3742_MI = 
{
	"ResolveReferences", (methodPointerType)&m3742, &t871_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 736, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t731_0_0_0;
extern Il2CppType t731_0_0_0;
static ParameterInfo t871_m3743_ParameterInfos[] = 
{
	{"explicit_numeric_groups", 0, 134218421, &EmptyCustomAttributesCache, &t731_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3743_MI = 
{
	"HandleExplicitNumericGroups", (methodPointerType)&m3743, &t871_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t871_m3743_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 737, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t842_0_0_0;
static ParameterInfo t871_m3744_ParameterInfos[] = 
{
	{"options", 0, 134218422, &EmptyCustomAttributesCache, &t842_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3744_MI = 
{
	"IsIgnoreCase", (methodPointerType)&m3744, &t871_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t871_m3744_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 738, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t842_0_0_0;
static ParameterInfo t871_m3745_ParameterInfos[] = 
{
	{"options", 0, 134218423, &EmptyCustomAttributesCache, &t842_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3745_MI = 
{
	"IsMultiline", (methodPointerType)&m3745, &t871_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t871_m3745_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 739, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t842_0_0_0;
static ParameterInfo t871_m3746_ParameterInfos[] = 
{
	{"options", 0, 134218424, &EmptyCustomAttributesCache, &t842_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3746_MI = 
{
	"IsExplicitCapture", (methodPointerType)&m3746, &t871_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t871_m3746_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 740, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t842_0_0_0;
static ParameterInfo t871_m3747_ParameterInfos[] = 
{
	{"options", 0, 134218425, &EmptyCustomAttributesCache, &t842_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3747_MI = 
{
	"IsSingleline", (methodPointerType)&m3747, &t871_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t871_m3747_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 741, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t842_0_0_0;
static ParameterInfo t871_m3748_ParameterInfos[] = 
{
	{"options", 0, 134218426, &EmptyCustomAttributesCache, &t842_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3748_MI = 
{
	"IsIgnorePatternWhitespace", (methodPointerType)&m3748, &t871_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t871_m3748_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 742, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t842_0_0_0;
static ParameterInfo t871_m3749_ParameterInfos[] = 
{
	{"options", 0, 134218427, &EmptyCustomAttributesCache, &t842_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3749_MI = 
{
	"IsECMAScript", (methodPointerType)&m3749, &t871_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t871_m3749_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 743, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t871_m3750_ParameterInfos[] = 
{
	{"msg", 0, 134218428, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t305_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3750_MI = 
{
	"NewParseException", (methodPointerType)&m3750, &t871_TI, &t305_0_0_0, RuntimeInvoker_t29_t29, t871_m3750_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 744, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t871_MIs[] =
{
	&m3720_MI,
	&m3721_MI,
	&m3722_MI,
	&m3723_MI,
	&m3724_MI,
	&m3725_MI,
	&m3726_MI,
	&m3727_MI,
	&m3728_MI,
	&m3729_MI,
	&m3730_MI,
	&m3731_MI,
	&m3732_MI,
	&m3733_MI,
	&m3734_MI,
	&m3735_MI,
	&m3736_MI,
	&m3737_MI,
	&m3738_MI,
	&m3739_MI,
	&m3740_MI,
	&m3741_MI,
	&m3742_MI,
	&m3743_MI,
	&m3744_MI,
	&m3745_MI,
	&m3746_MI,
	&m3747_MI,
	&m3748_MI,
	&m3749_MI,
	&m3750_MI,
	NULL
};
static MethodInfo* t871_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t871_0_0_0;
extern Il2CppType t871_1_0_0;
struct t871;
TypeInfo t871_TI = 
{
	&g_System_dll_Image, NULL, "Parser", "System.Text.RegularExpressions.Syntax", t871_MIs, NULL, t871_FIs, NULL, &t29_TI, NULL, NULL, &t871_TI, NULL, t871_VT, &EmptyCustomAttributesCache, &t871_TI, &t871_0_0_0, &t871_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t871), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 31, 0, 6, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t348.h"
extern TypeInfo t781_TI;
extern TypeInfo t348_TI;
extern MethodInfo m3755_MI;
extern MethodInfo m3757_MI;
extern MethodInfo m3756_MI;
extern MethodInfo m4216_MI;


 void m3751 (t864 * __this, t7* p0, bool p1, bool p2, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		__this->f0 = p0;
		int32_t L_0 = m1715(p0, &m1715_MI);
		__this->f1 = L_0;
		__this->f2 = p1;
		__this->f3 = p2;
		if (!p1)
		{
			goto IL_0035;
		}
	}
	{
		t7* L_1 = m4257(p0, &m4257_MI);
		p0 = L_1;
	}

IL_0035:
	{
		int32_t L_2 = (__this->f1);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t864_TI));
		if ((((int32_t)L_2) <= ((int32_t)(((t864_SFs*)InitializedTypeInfo(&t864_TI)->static_fields)->f6))))
		{
			goto IL_004b;
		}
	}
	{
		m3755(__this, &m3755_MI);
	}

IL_004b:
	{
		return;
	}
}
extern MethodInfo m3752_MI;
 void m3752 (t29 * __this, MethodInfo* method){
	{
		((t864_SFs*)InitializedTypeInfo(&t864_TI)->static_fields)->f6 = 5;
		return;
	}
}
 int32_t m3753 (t864 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		return L_0;
	}
}
 int32_t m3754 (t864 * __this, t7* p0, int32_t p1, int32_t p2, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = p1;
		bool L_0 = (__this->f3);
		if (!L_0)
		{
			goto IL_0107;
		}
	}
	{
		if ((((int32_t)p1) >= ((int32_t)p2)))
		{
			goto IL_0016;
		}
	}
	{
		return (-1);
	}

IL_0016:
	{
		int32_t L_1 = m1715(p0, &m1715_MI);
		if ((((int32_t)V_0) <= ((int32_t)L_1)))
		{
			goto IL_0029;
		}
	}
	{
		int32_t L_2 = m1715(p0, &m1715_MI);
		V_0 = L_2;
	}

IL_0029:
	{
		int32_t L_3 = (__this->f1);
		if ((((uint32_t)L_3) != ((uint32_t)1)))
		{
			goto IL_0067;
		}
	}
	{
		goto IL_005a;
	}

IL_003a:
	{
		t7* L_4 = (__this->f0);
		uint16_t L_5 = m1741(L_4, 0, &m1741_MI);
		uint16_t L_6 = m1741(p0, V_0, &m1741_MI);
		uint16_t L_7 = m3757(__this, L_6, &m3757_MI);
		if ((((uint32_t)L_5) != ((uint32_t)L_7)))
		{
			goto IL_005a;
		}
	}
	{
		return V_0;
	}

IL_005a:
	{
		int32_t L_8 = ((int32_t)(V_0-1));
		V_0 = L_8;
		if ((((int32_t)L_8) >= ((int32_t)p2)))
		{
			goto IL_003a;
		}
	}
	{
		return (-1);
	}

IL_0067:
	{
		int32_t L_9 = (__this->f1);
		if ((((int32_t)p2) >= ((int32_t)L_9)))
		{
			goto IL_007d;
		}
	}
	{
		int32_t L_10 = (__this->f1);
		p2 = ((int32_t)(L_10-1));
	}

IL_007d:
	{
		V_0 = ((int32_t)(V_0-1));
		goto IL_00fb;
	}

IL_0086:
	{
		int32_t L_11 = (__this->f1);
		V_1 = ((int32_t)(L_11-1));
		goto IL_00aa;
	}

IL_0094:
	{
		int32_t L_12 = ((int32_t)(V_1-1));
		V_1 = L_12;
		if ((((int32_t)L_12) >= ((int32_t)0)))
		{
			goto IL_00aa;
		}
	}
	{
		int32_t L_13 = (__this->f1);
		return ((int32_t)(((int32_t)(V_0-L_13))+1));
	}

IL_00aa:
	{
		t7* L_14 = (__this->f0);
		uint16_t L_15 = m1741(L_14, V_1, &m1741_MI);
		int32_t L_16 = (__this->f1);
		uint16_t L_17 = m1741(p0, ((int32_t)(((int32_t)(((int32_t)(V_0-L_16))+1))+V_1)), &m1741_MI);
		uint16_t L_18 = m3757(__this, L_17, &m3757_MI);
		if ((((int32_t)L_15) == ((int32_t)L_18)))
		{
			goto IL_0094;
		}
	}
	{
		if ((((int32_t)V_0) <= ((int32_t)p2)))
		{
			goto IL_00f6;
		}
	}
	{
		int32_t L_19 = (__this->f1);
		uint16_t L_20 = m1741(p0, ((int32_t)(V_0-L_19)), &m1741_MI);
		int32_t L_21 = m3756(__this, L_20, &m3756_MI);
		V_0 = ((int32_t)(V_0-L_21));
		goto IL_00fb;
	}

IL_00f6:
	{
		goto IL_0102;
	}

IL_00fb:
	{
		if ((((int32_t)V_0) >= ((int32_t)p2)))
		{
			goto IL_0086;
		}
	}

IL_0102:
	{
		goto IL_01d6;
	}

IL_0107:
	{
		int32_t L_22 = (__this->f1);
		if ((((uint32_t)L_22) != ((uint32_t)1)))
		{
			goto IL_0145;
		}
	}
	{
		goto IL_013c;
	}

IL_0118:
	{
		t7* L_23 = (__this->f0);
		uint16_t L_24 = m1741(L_23, 0, &m1741_MI);
		uint16_t L_25 = m1741(p0, V_0, &m1741_MI);
		uint16_t L_26 = m3757(__this, L_25, &m3757_MI);
		if ((((uint32_t)L_24) != ((uint32_t)L_26)))
		{
			goto IL_0138;
		}
	}
	{
		return V_0;
	}

IL_0138:
	{
		V_0 = ((int32_t)(V_0+1));
	}

IL_013c:
	{
		if ((((int32_t)V_0) <= ((int32_t)p2)))
		{
			goto IL_0118;
		}
	}
	{
		return (-1);
	}

IL_0145:
	{
		int32_t L_27 = m1715(p0, &m1715_MI);
		int32_t L_28 = (__this->f1);
		if ((((int32_t)p2) <= ((int32_t)((int32_t)(L_27-L_28)))))
		{
			goto IL_0167;
		}
	}
	{
		int32_t L_29 = m1715(p0, &m1715_MI);
		int32_t L_30 = (__this->f1);
		p2 = ((int32_t)(L_29-L_30));
	}

IL_0167:
	{
		goto IL_01cf;
	}

IL_016c:
	{
		int32_t L_31 = (__this->f1);
		V_2 = ((int32_t)(L_31-1));
		goto IL_0187;
	}

IL_017a:
	{
		int32_t L_32 = ((int32_t)(V_2-1));
		V_2 = L_32;
		if ((((int32_t)L_32) >= ((int32_t)0)))
		{
			goto IL_0187;
		}
	}
	{
		return V_0;
	}

IL_0187:
	{
		t7* L_33 = (__this->f0);
		uint16_t L_34 = m1741(L_33, V_2, &m1741_MI);
		uint16_t L_35 = m1741(p0, ((int32_t)(V_0+V_2)), &m1741_MI);
		uint16_t L_36 = m3757(__this, L_35, &m3757_MI);
		if ((((int32_t)L_34) == ((int32_t)L_36)))
		{
			goto IL_017a;
		}
	}
	{
		if ((((int32_t)V_0) >= ((int32_t)p2)))
		{
			goto IL_01ca;
		}
	}
	{
		int32_t L_37 = (__this->f1);
		uint16_t L_38 = m1741(p0, ((int32_t)(V_0+L_37)), &m1741_MI);
		int32_t L_39 = m3756(__this, L_38, &m3756_MI);
		V_0 = ((int32_t)(V_0+L_39));
		goto IL_01cf;
	}

IL_01ca:
	{
		goto IL_01d6;
	}

IL_01cf:
	{
		if ((((int32_t)V_0) <= ((int32_t)p2)))
		{
			goto IL_016c;
		}
	}

IL_01d6:
	{
		return (-1);
	}
}
 void m3755 (t864 * __this, MethodInfo* method){
	bool V_0 = false;
	uint8_t V_1 = 0x0;
	int32_t V_2 = 0;
	uint16_t V_3 = 0x0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	uint16_t V_6 = 0x0;
	t7* G_B13_0 = {0};
	t7* G_B12_0 = {0};
	int32_t G_B14_0 = 0;
	t7* G_B14_1 = {0};
	{
		int32_t L_0 = (__this->f1);
		V_0 = ((((int32_t)L_0) > ((int32_t)((int32_t)254)))? 1 : 0);
		V_1 = 0;
		V_2 = 0;
		goto IL_0045;
	}

IL_0017:
	{
		t7* L_1 = (__this->f0);
		uint16_t L_2 = m1741(L_1, V_2, &m1741_MI);
		V_3 = L_2;
		if ((((int32_t)V_3) > ((int32_t)((int32_t)255))))
		{
			goto IL_003f;
		}
	}
	{
		if ((((int32_t)(((uint8_t)V_3))) <= ((int32_t)V_1)))
		{
			goto IL_003a;
		}
	}
	{
		V_1 = (((uint8_t)V_3));
	}

IL_003a:
	{
		goto IL_0041;
	}

IL_003f:
	{
		V_0 = 1;
	}

IL_0041:
	{
		V_2 = ((int32_t)(V_2+1));
	}

IL_0045:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)V_2) < ((int32_t)L_3)))
		{
			goto IL_0017;
		}
	}
	{
		__this->f4 = ((t781*)SZArrayNew(InitializedTypeInfo(&t781_TI), ((int32_t)(V_1+1))));
		if (!V_0)
		{
			goto IL_0070;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t719_TI));
		t719 * L_4 = (t719 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t719_TI));
		m4209(L_4, &m4209_MI);
		__this->f5 = L_4;
	}

IL_0070:
	{
		V_4 = 0;
		int32_t L_5 = (__this->f1);
		V_5 = L_5;
		goto IL_0102;
	}

IL_0080:
	{
		t7* L_6 = (__this->f0);
		bool L_7 = (__this->f3);
		G_B12_0 = L_6;
		if (L_7)
		{
			G_B13_0 = L_6;
			goto IL_0098;
		}
	}
	{
		G_B14_0 = V_4;
		G_B14_1 = G_B12_0;
		goto IL_009c;
	}

IL_0098:
	{
		G_B14_0 = ((int32_t)(V_5-1));
		G_B14_1 = G_B13_0;
	}

IL_009c:
	{
		uint16_t L_8 = m1741(G_B14_1, G_B14_0, &m1741_MI);
		V_6 = L_8;
		t781* L_9 = (__this->f4);
		if ((((int32_t)V_6) >= ((int32_t)(((int32_t)(((t20 *)L_9)->max_length))))))
		{
			goto IL_00dd;
		}
	}
	{
		if ((((int32_t)V_5) >= ((int32_t)((int32_t)255))))
		{
			goto IL_00cf;
		}
	}
	{
		t781* L_10 = (__this->f4);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_10, V_6)) = (uint8_t)(((uint8_t)V_5));
		goto IL_00f6;
	}

IL_00cf:
	{
		t781* L_11 = (__this->f4);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_11, V_6)) = (uint8_t)((int32_t)255);
	}

IL_00dd:
	{
		t719 * L_12 = (__this->f5);
		uint16_t L_13 = V_6;
		t29 * L_14 = Box(InitializedTypeInfo(&t194_TI), &L_13);
		int32_t L_15 = V_5;
		t29 * L_16 = Box(InitializedTypeInfo(&t44_TI), &L_15);
		VirtActionInvoker2< t29 *, t29 * >::Invoke(&m4216_MI, L_12, L_14, L_16);
	}

IL_00f6:
	{
		V_4 = ((int32_t)(V_4+1));
		V_5 = ((int32_t)(V_5-1));
	}

IL_0102:
	{
		int32_t L_17 = (__this->f1);
		if ((((int32_t)V_4) < ((int32_t)L_17)))
		{
			goto IL_0080;
		}
	}
	{
		return;
	}
}
 int32_t m3756 (t864 * __this, uint16_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	t29 * V_1 = {0};
	int32_t G_B15_0 = 0;
	{
		t781* L_0 = (__this->f4);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 1;
	}

IL_000d:
	{
		uint16_t L_1 = m3757(__this, p0, &m3757_MI);
		p0 = L_1;
		t781* L_2 = (__this->f4);
		if ((((int32_t)p0) >= ((int32_t)(((int32_t)(((t20 *)L_2)->max_length))))))
		{
			goto IL_004e;
		}
	}
	{
		t781* L_3 = (__this->f4);
		uint16_t L_4 = p0;
		V_0 = (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_3, L_4));
		if (V_0)
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_5 = (__this->f1);
		return ((int32_t)(L_5+1));
	}

IL_003c:
	{
		if ((((int32_t)V_0) == ((int32_t)((int32_t)255))))
		{
			goto IL_0049;
		}
	}
	{
		return V_0;
	}

IL_0049:
	{
		goto IL_0062;
	}

IL_004e:
	{
		if ((((int32_t)p0) >= ((int32_t)((int32_t)255))))
		{
			goto IL_0062;
		}
	}
	{
		int32_t L_6 = (__this->f1);
		return ((int32_t)(L_6+1));
	}

IL_0062:
	{
		t719 * L_7 = (__this->f5);
		if (L_7)
		{
			goto IL_0076;
		}
	}
	{
		int32_t L_8 = (__this->f1);
		return ((int32_t)(L_8+1));
	}

IL_0076:
	{
		t719 * L_9 = (__this->f5);
		uint16_t L_10 = p0;
		t29 * L_11 = Box(InitializedTypeInfo(&t194_TI), &L_10);
		t29 * L_12 = (t29 *)VirtFuncInvoker1< t29 *, t29 * >::Invoke(&m3989_MI, L_9, L_11);
		V_1 = L_12;
		if (!V_1)
		{
			goto IL_0099;
		}
	}
	{
		G_B15_0 = ((*(int32_t*)((int32_t*)UnBox (V_1, InitializedTypeInfo(&t44_TI)))));
		goto IL_00a1;
	}

IL_0099:
	{
		int32_t L_13 = (__this->f1);
		G_B15_0 = ((int32_t)(L_13+1));
	}

IL_00a1:
	{
		return G_B15_0;
	}
}
 uint16_t m3757 (t864 * __this, uint16_t p0, MethodInfo* method){
	uint16_t G_B3_0 = 0x0;
	{
		bool L_0 = (__this->f2);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = p0;
		goto IL_0017;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t194_TI));
		uint16_t L_1 = m1809(NULL, p0, &m1809_MI);
		G_B3_0 = L_1;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
// Metadata Definition System.Text.RegularExpressions.QuickSearch
extern Il2CppType t7_0_0_1;
FieldInfo t864_f0_FieldInfo = 
{
	"str", &t7_0_0_1, &t864_TI, offsetof(t864, f0), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t864_f1_FieldInfo = 
{
	"len", &t44_0_0_1, &t864_TI, offsetof(t864, f1), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t864_f2_FieldInfo = 
{
	"ignore", &t40_0_0_1, &t864_TI, offsetof(t864, f2), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t864_f3_FieldInfo = 
{
	"reverse", &t40_0_0_1, &t864_TI, offsetof(t864, f3), &EmptyCustomAttributesCache};
extern Il2CppType t781_0_0_1;
FieldInfo t864_f4_FieldInfo = 
{
	"shift", &t781_0_0_1, &t864_TI, offsetof(t864, f4), &EmptyCustomAttributesCache};
extern Il2CppType t719_0_0_1;
FieldInfo t864_f5_FieldInfo = 
{
	"shiftExtended", &t719_0_0_1, &t864_TI, offsetof(t864, f5), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_49;
FieldInfo t864_f6_FieldInfo = 
{
	"THRESHOLD", &t44_0_0_49, &t864_TI, offsetof(t864_SFs, f6), &EmptyCustomAttributesCache};
static FieldInfo* t864_FIs[] =
{
	&t864_f0_FieldInfo,
	&t864_f1_FieldInfo,
	&t864_f2_FieldInfo,
	&t864_f3_FieldInfo,
	&t864_f4_FieldInfo,
	&t864_f5_FieldInfo,
	&t864_f6_FieldInfo,
	NULL
};
static PropertyInfo t864____Length_PropertyInfo = 
{
	&t864_TI, "Length", &m3753_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t864_PIs[] =
{
	&t864____Length_PropertyInfo,
	NULL
};
extern Il2CppType t7_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t864_m3751_ParameterInfos[] = 
{
	{"str", 0, 134218429, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"ignore", 1, 134218430, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"reverse", 2, 134218431, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t297_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3751_MI = 
{
	".ctor", (methodPointerType)&m3751, &t864_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t297_t297, t864_m3751_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, false, 745, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3752_MI = 
{
	".cctor", (methodPointerType)&m3752, &t864_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 746, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3753_MI = 
{
	"get_Length", (methodPointerType)&m3753, &t864_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 747, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t864_m3754_ParameterInfos[] = 
{
	{"text", 0, 134218432, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"start", 1, 134218433, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"end", 2, 134218434, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3754_MI = 
{
	"Search", (methodPointerType)&m3754, &t864_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t44_t44, t864_m3754_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 3, false, false, 748, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3755_MI = 
{
	"SetupShiftTable", (methodPointerType)&m3755, &t864_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 749, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t194_0_0_0;
static ParameterInfo t864_m3756_ParameterInfos[] = 
{
	{"c", 0, 134218435, &EmptyCustomAttributesCache, &t194_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t372 (MethodInfo* method, void* obj, void** args);
MethodInfo m3756_MI = 
{
	"GetShiftDistance", (methodPointerType)&m3756, &t864_TI, &t44_0_0_0, RuntimeInvoker_t44_t372, t864_m3756_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 750, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t194_0_0_0;
static ParameterInfo t864_m3757_ParameterInfos[] = 
{
	{"c", 0, 134218436, &EmptyCustomAttributesCache, &t194_0_0_0},
};
extern Il2CppType t194_0_0_0;
extern void* RuntimeInvoker_t194_t372 (MethodInfo* method, void* obj, void** args);
MethodInfo m3757_MI = 
{
	"GetChar", (methodPointerType)&m3757, &t864_TI, &t194_0_0_0, RuntimeInvoker_t194_t372, t864_m3757_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 751, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t864_MIs[] =
{
	&m3751_MI,
	&m3752_MI,
	&m3753_MI,
	&m3754_MI,
	&m3755_MI,
	&m3756_MI,
	&m3757_MI,
	NULL
};
static MethodInfo* t864_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t864_0_0_0;
extern Il2CppType t864_1_0_0;
struct t864;
TypeInfo t864_TI = 
{
	&g_System_dll_Image, NULL, "QuickSearch", "System.Text.RegularExpressions", t864_MIs, t864_PIs, t864_FIs, NULL, &t29_TI, NULL, NULL, &t864_TI, NULL, t864_VT, &EmptyCustomAttributesCache, &t864_TI, &t864_0_0_0, &t864_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t864), 0, -1, sizeof(t864_SFs), 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, true, false, false, 7, 1, 7, 0, 0, 4, 0, 0};
#include "t877.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t877_TI;
#include "t877MD.h"

#include "t800MD.h"
extern MethodInfo m4268_MI;
extern MethodInfo m4269_MI;
extern MethodInfo m4155_MI;
extern MethodInfo m4270_MI;


extern MethodInfo m3758_MI;
 void m3758 (t877 * __this, MethodInfo* method){
	{
		m4155(__this, &m4155_MI);
		return;
	}
}
extern MethodInfo m3759_MI;
 void m3759 (t877 * __this, t875 * p0, MethodInfo* method){
	{
		t29 * L_0 = m4268(__this, &m4268_MI);
		InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(&m4270_MI, L_0, p0);
		return;
	}
}
extern MethodInfo m3760_MI;
 t875 * m3760 (t877 * __this, int32_t p0, MethodInfo* method){
	{
		t29 * L_0 = m4268(__this, &m4268_MI);
		t29 * L_1 = (t29 *)InterfaceFuncInvoker1< t29 *, int32_t >::Invoke(&m4263_MI, L_0, p0);
		return ((t875 *)Castclass(L_1, InitializedTypeInfo(&t875_TI)));
	}
}
extern MethodInfo m3761_MI;
 void m3761 (t877 * __this, int32_t p0, t875 * p1, MethodInfo* method){
	{
		t29 * L_0 = m4268(__this, &m4268_MI);
		InterfaceActionInvoker2< int32_t, t29 * >::Invoke(&m4269_MI, L_0, p0, p1);
		return;
	}
}
extern MethodInfo m3762_MI;
 void m3762 (t877 * __this, t29 * p0, MethodInfo* method){
	{
		return;
	}
}
// Metadata Definition System.Text.RegularExpressions.Syntax.ExpressionCollection
static PropertyInfo t877____Item_PropertyInfo = 
{
	&t877_TI, "Item", &m3760_MI, &m3761_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t877_PIs[] =
{
	&t877____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3758_MI = 
{
	".ctor", (methodPointerType)&m3758, &t877_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 752, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t875_0_0_0;
extern Il2CppType t875_0_0_0;
static ParameterInfo t877_m3759_ParameterInfos[] = 
{
	{"e", 0, 134218437, &EmptyCustomAttributesCache, &t875_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3759_MI = 
{
	"Add", (methodPointerType)&m3759, &t877_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t877_m3759_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 753, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t877_m3760_ParameterInfos[] = 
{
	{"i", 0, 134218438, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t875_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3760_MI = 
{
	"get_Item", (methodPointerType)&m3760, &t877_TI, &t875_0_0_0, RuntimeInvoker_t29_t44, t877_m3760_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 754, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t875_0_0_0;
static ParameterInfo t877_m3761_ParameterInfos[] = 
{
	{"i", 0, 134218439, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134218440, &EmptyCustomAttributesCache, &t875_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3761_MI = 
{
	"set_Item", (methodPointerType)&m3761, &t877_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t877_m3761_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 2, false, false, 755, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t877_m3762_ParameterInfos[] = 
{
	{"o", 0, 134218441, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3762_MI = 
{
	"OnValidate", (methodPointerType)&m3762, &t877_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t877_m3762_ParameterInfos, &EmptyCustomAttributesCache, 196, 0, 28, 1, false, false, 756, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t877_MIs[] =
{
	&m3758_MI,
	&m3759_MI,
	&m3760_MI,
	&m3761_MI,
	&m3762_MI,
	NULL
};
extern MethodInfo m4130_MI;
extern MethodInfo m4096_MI;
extern MethodInfo m4131_MI;
extern MethodInfo m4132_MI;
extern MethodInfo m4133_MI;
extern MethodInfo m4134_MI;
extern MethodInfo m4135_MI;
extern MethodInfo m4136_MI;
extern MethodInfo m4137_MI;
extern MethodInfo m4138_MI;
extern MethodInfo m4139_MI;
extern MethodInfo m4140_MI;
extern MethodInfo m4141_MI;
extern MethodInfo m4142_MI;
extern MethodInfo m4143_MI;
extern MethodInfo m4144_MI;
extern MethodInfo m4145_MI;
extern MethodInfo m4146_MI;
extern MethodInfo m4147_MI;
extern MethodInfo m4148_MI;
extern MethodInfo m4149_MI;
extern MethodInfo m4150_MI;
extern MethodInfo m4151_MI;
extern MethodInfo m4152_MI;
static MethodInfo* t877_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4130_MI,
	&m4096_MI,
	&m4131_MI,
	&m4132_MI,
	&m4133_MI,
	&m4134_MI,
	&m4135_MI,
	&m4136_MI,
	&m4137_MI,
	&m4138_MI,
	&m4139_MI,
	&m4140_MI,
	&m4141_MI,
	&m4142_MI,
	&m4143_MI,
	&m4144_MI,
	&m4145_MI,
	&m4146_MI,
	&m4147_MI,
	&m4148_MI,
	&m4149_MI,
	&m4150_MI,
	&m4151_MI,
	&m4152_MI,
	&m3762_MI,
};
static Il2CppInterfaceOffsetPair t877_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
};
void t877_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t425 * tmp;
		tmp = (t425 *)il2cpp_codegen_object_new (&t425_TI);
		m2025(tmp, il2cpp_codegen_string_new_wrapper("Item"), &m2025_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t877__CustomAttributeCache = {
1,
NULL,
&t877_CustomAttributesCacheGenerator
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t877_0_0_0;
extern Il2CppType t877_1_0_0;
extern TypeInfo t800_TI;
struct t877;
extern CustomAttributesCache t877__CustomAttributeCache;
TypeInfo t877_TI = 
{
	&g_System_dll_Image, NULL, "ExpressionCollection", "System.Text.RegularExpressions.Syntax", t877_MIs, t877_PIs, NULL, NULL, &t800_TI, NULL, NULL, &t877_TI, NULL, t877_VT, &t877__CustomAttributeCache, &t877_TI, &t877_0_0_0, &t877_1_0_0, t877_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t877), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 29, 0, 3};
#ifndef _MSC_VER
#else
#endif
#include "t875MD.h"

#include "t879.h"
extern TypeInfo t879_TI;
#include "t879MD.h"
extern MethodInfo m4271_MI;
extern MethodInfo m3764_MI;
extern MethodInfo m3854_MI;


extern MethodInfo m3763_MI;
 void m3763 (t875 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
 int32_t m3764 (t875 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		VirtActionInvoker2< int32_t*, int32_t* >::Invoke(&m4271_MI, __this, (&V_0), (&V_1));
		if ((((uint32_t)V_0) != ((uint32_t)V_1)))
		{
			goto IL_0013;
		}
	}
	{
		return V_0;
	}

IL_0013:
	{
		return (-1);
	}
}
extern MethodInfo m3765_MI;
 t879 * m3765 (t875 * __this, bool p0, MethodInfo* method){
	{
		int32_t L_0 = m3764(__this, &m3764_MI);
		t879 * L_1 = (t879 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t879_TI));
		m3854(L_1, __this, L_0, &m3854_MI);
		return L_1;
	}
}
// Metadata Definition System.Text.RegularExpressions.Syntax.Expression
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3763_MI = 
{
	".ctor", (methodPointerType)&m3763, &t875_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 757, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t878_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t875_m4272_ParameterInfos[] = 
{
	{"cmp", 0, 134218442, &EmptyCustomAttributesCache, &t878_0_0_0},
	{"reverse", 1, 134218443, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m4272_MI = 
{
	"Compile", NULL, &t875_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t297, t875_m4272_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 2, false, false, 758, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_1_0_2;
extern Il2CppType t44_1_0_2;
static ParameterInfo t875_m4271_ParameterInfos[] = 
{
	{"min", 0, 134218444, &EmptyCustomAttributesCache, &t44_1_0_2},
	{"max", 1, 134218445, &EmptyCustomAttributesCache, &t44_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t390_t390 (MethodInfo* method, void* obj, void** args);
MethodInfo m4271_MI = 
{
	"GetWidth", NULL, &t875_TI, &t21_0_0_0, RuntimeInvoker_t21_t390_t390, t875_m4271_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, false, 759, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3764_MI = 
{
	"GetFixedWidth", (methodPointerType)&m3764, &t875_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, false, 760, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t875_m3765_ParameterInfos[] = 
{
	{"reverse", 0, 134218446, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t879_0_0_0;
extern void* RuntimeInvoker_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3765_MI = 
{
	"GetAnchorInfo", (methodPointerType)&m3765, &t875_TI, &t879_0_0_0, RuntimeInvoker_t29_t297, t875_m3765_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 6, 1, false, false, 761, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m4273_MI = 
{
	"IsComplex", NULL, &t875_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 1478, 0, 7, 0, false, false, 762, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t875_MIs[] =
{
	&m3763_MI,
	&m4272_MI,
	&m4271_MI,
	&m3764_MI,
	&m3765_MI,
	&m4273_MI,
	NULL
};
static MethodInfo* t875_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	NULL,
	NULL,
	&m3765_MI,
	NULL,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t875_1_0_0;
struct t875;
TypeInfo t875_TI = 
{
	&g_System_dll_Image, NULL, "Expression", "System.Text.RegularExpressions.Syntax", t875_MIs, NULL, NULL, NULL, &t29_TI, NULL, NULL, &t875_TI, NULL, t875_VT, &EmptyCustomAttributesCache, &t875_TI, &t875_0_0_0, &t875_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t875), 0, -1, 0, 0, -1, 1048704, 0, false, false, false, false, false, false, false, false, false, false, false, false, 6, 0, 0, 0, 0, 8, 0, 0};
#include "t880.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t880_TI;
#include "t880MD.h"

#include "t800.h"
extern MethodInfo m3767_MI;
extern MethodInfo m4273_MI;


extern MethodInfo m3766_MI;
 void m3766 (t880 * __this, MethodInfo* method){
	{
		m3763(__this, &m3763_MI);
		t877 * L_0 = (t877 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t877_TI));
		m3758(L_0, &m3758_MI);
		__this->f0 = L_0;
		return;
	}
}
 t877 * m3767 (t880 * __this, MethodInfo* method){
	{
		t877 * L_0 = (__this->f0);
		return L_0;
	}
}
extern MethodInfo m3768_MI;
 void m3768 (t880 * __this, int32_t* p0, int32_t* p1, int32_t p2, MethodInfo* method){
	bool V_0 = false;
	int32_t V_1 = 0;
	t875 * V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		*((int32_t*)(p0)) = (int32_t)((int32_t)2147483647);
		*((int32_t*)(p1)) = (int32_t)0;
		V_0 = 1;
		V_1 = 0;
		goto IL_0053;
	}

IL_0013:
	{
		t877 * L_0 = m3767(__this, &m3767_MI);
		t875 * L_1 = m3760(L_0, V_1, &m3760_MI);
		V_2 = L_1;
		if (V_2)
		{
			goto IL_002b;
		}
	}
	{
		goto IL_004f;
	}

IL_002b:
	{
		V_0 = 0;
		VirtActionInvoker2< int32_t*, int32_t* >::Invoke(&m4271_MI, V_2, (&V_3), (&V_4));
		if ((((int32_t)V_3) >= ((int32_t)(*((int32_t*)p0)))))
		{
			goto IL_0042;
		}
	}
	{
		*((int32_t*)(p0)) = (int32_t)V_3;
	}

IL_0042:
	{
		if ((((int32_t)V_4) <= ((int32_t)(*((int32_t*)p1)))))
		{
			goto IL_004f;
		}
	}
	{
		*((int32_t*)(p1)) = (int32_t)V_4;
	}

IL_004f:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0053:
	{
		if ((((int32_t)V_1) < ((int32_t)p2)))
		{
			goto IL_0013;
		}
	}
	{
		if (!V_0)
		{
			goto IL_006a;
		}
	}
	{
		int32_t L_2 = 0;
		V_5 = L_2;
		*((int32_t*)(p1)) = (int32_t)L_2;
		*((int32_t*)(p0)) = (int32_t)V_5;
	}

IL_006a:
	{
		return;
	}
}
extern MethodInfo m3769_MI;
 bool m3769 (t880 * __this, MethodInfo* method){
	t875 * V_0 = {0};
	t29 * V_1 = {0};
	bool V_2 = false;
	t29 * V_3 = {0};
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		t877 * L_0 = m3767(__this, &m3767_MI);
		t29 * L_1 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m4130_MI, L_0);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002f;
		}

IL_0011:
		{
			t29 * L_2 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m3970_MI, V_1);
			V_0 = ((t875 *)Castclass(L_2, InitializedTypeInfo(&t875_TI)));
			bool L_3 = (bool)VirtFuncInvoker0< bool >::Invoke(&m4273_MI, V_0);
			if (!L_3)
			{
				goto IL_002f;
			}
		}

IL_0028:
		{
			V_2 = 1;
			// IL_002a: leave IL_005e
			leaveInstructions[0] = 0x5E; // 1
			THROW_SENTINEL(IL_005e);
			// finally target depth: 1
		}

IL_002f:
		{
			bool L_4 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m3972_MI, V_1);
			if (L_4)
			{
				goto IL_0011;
			}
		}

IL_003a:
		{
			// IL_003a: leave IL_0051
			leaveInstructions[0] = 0x51; // 1
			THROW_SENTINEL(IL_0051);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_003f;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_003f;
	}

IL_003f:
	{ // begin finally (depth: 1)
		{
			V_3 = ((t29 *)IsInst(V_1, InitializedTypeInfo(&t324_TI)));
			if (V_3)
			{
				goto IL_004a;
			}
		}

IL_0049:
		{
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x5E:
					goto IL_005e;
				case 0x51:
					goto IL_0051;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}

IL_004a:
		{
			InterfaceActionInvoker0::Invoke(&m1428_MI, V_3);
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x5E:
					goto IL_005e;
				case 0x51:
					goto IL_0051;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}
	} // end finally (depth: 1)

IL_0051:
	{
		int32_t L_5 = m3764(__this, &m3764_MI);
		return ((((int32_t)((((int32_t)L_5) > ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}

IL_005e:
	{
		return V_2;
	}
}
// Metadata Definition System.Text.RegularExpressions.Syntax.CompositeExpression
extern Il2CppType t877_0_0_1;
FieldInfo t880_f0_FieldInfo = 
{
	"expressions", &t877_0_0_1, &t880_TI, offsetof(t880, f0), &EmptyCustomAttributesCache};
static FieldInfo* t880_FIs[] =
{
	&t880_f0_FieldInfo,
	NULL
};
static PropertyInfo t880____Expressions_PropertyInfo = 
{
	&t880_TI, "Expressions", &m3767_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t880_PIs[] =
{
	&t880____Expressions_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3766_MI = 
{
	".ctor", (methodPointerType)&m3766, &t880_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 763, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t877_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3767_MI = 
{
	"get_Expressions", (methodPointerType)&m3767, &t880_TI, &t877_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2180, 0, 255, 0, false, false, 764, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_1_0_2;
extern Il2CppType t44_1_0_2;
extern Il2CppType t44_0_0_0;
static ParameterInfo t880_m3768_ParameterInfos[] = 
{
	{"min", 0, 134218447, &EmptyCustomAttributesCache, &t44_1_0_2},
	{"max", 1, 134218448, &EmptyCustomAttributesCache, &t44_1_0_2},
	{"count", 2, 134218449, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t390_t390_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3768_MI = 
{
	"GetWidth", (methodPointerType)&m3768, &t880_TI, &t21_0_0_0, RuntimeInvoker_t21_t390_t390_t44, t880_m3768_ParameterInfos, &EmptyCustomAttributesCache, 132, 0, 255, 3, false, false, 765, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m3769_MI = 
{
	"IsComplex", (methodPointerType)&m3769, &t880_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 198, 0, 7, 0, false, false, 766, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t880_MIs[] =
{
	&m3766_MI,
	&m3767_MI,
	&m3768_MI,
	&m3769_MI,
	NULL
};
static MethodInfo* t880_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	NULL,
	NULL,
	&m3765_MI,
	&m3769_MI,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t880_0_0_0;
extern Il2CppType t880_1_0_0;
struct t880;
TypeInfo t880_TI = 
{
	&g_System_dll_Image, NULL, "CompositeExpression", "System.Text.RegularExpressions.Syntax", t880_MIs, t880_PIs, t880_FIs, NULL, &t875_TI, NULL, NULL, &t880_TI, NULL, t880_VT, &EmptyCustomAttributesCache, &t880_TI, &t880_0_0_0, &t880_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t880), 0, -1, 0, 0, -1, 1048704, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 1, 1, 0, 0, 8, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t292.h"
#include "t958.h"
extern TypeInfo t292_TI;
extern TypeInfo t959_TI;
extern TypeInfo t958_TI;
#include "t292MD.h"
#include "t959MD.h"
#include "t958MD.h"
extern MethodInfo m4272_MI;
extern MethodInfo m3866_MI;
extern MethodInfo m3857_MI;
extern MethodInfo m3864_MI;
extern MethodInfo m3856_MI;
extern MethodInfo m3865_MI;
extern MethodInfo m3867_MI;
extern MethodInfo m3860_MI;
extern MethodInfo m3858_MI;
extern MethodInfo m3863_MI;
extern MethodInfo m1311_MI;
extern MethodInfo m3862_MI;
extern MethodInfo m2927_MI;
extern MethodInfo m4184_MI;
extern MethodInfo m1315_MI;
extern MethodInfo m3855_MI;
extern MethodInfo m4274_MI;
extern MethodInfo m4275_MI;


 void m3770 (t873 * __this, MethodInfo* method){
	{
		m3766(__this, &m3766_MI);
		return;
	}
}
 void m3771 (t873 * __this, t875 * p0, MethodInfo* method){
	{
		t877 * L_0 = m3767(__this, &m3767_MI);
		m3759(L_0, p0, &m3759_MI);
		return;
	}
}
extern MethodInfo m3772_MI;
 void m3772 (t873 * __this, t29 * p0, bool p1, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	t875 * V_2 = {0};
	{
		t877 * L_0 = m3767(__this, &m3767_MI);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m4096_MI, L_0);
		V_0 = L_1;
		V_1 = 0;
		goto IL_0048;
	}

IL_0013:
	{
		if (!p1)
		{
			goto IL_002f;
		}
	}
	{
		t877 * L_2 = m3767(__this, &m3767_MI);
		t875 * L_3 = m3760(L_2, ((int32_t)(((int32_t)(V_0-V_1))-1)), &m3760_MI);
		V_2 = L_3;
		goto IL_003c;
	}

IL_002f:
	{
		t877 * L_4 = m3767(__this, &m3767_MI);
		t875 * L_5 = m3760(L_4, V_1, &m3760_MI);
		V_2 = L_5;
	}

IL_003c:
	{
		VirtActionInvoker2< t29 *, bool >::Invoke(&m4272_MI, V_2, p0, p1);
		V_1 = ((int32_t)(V_1+1));
	}

IL_0048:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
extern MethodInfo m3773_MI;
 void m3773 (t873 * __this, int32_t* p0, int32_t* p1, MethodInfo* method){
	t875 * V_0 = {0};
	t29 * V_1 = {0};
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	t29 * V_4 = {0};
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		*((int32_t*)(p0)) = (int32_t)0;
		*((int32_t*)(p1)) = (int32_t)0;
		t877 * L_0 = m3767(__this, &m3767_MI);
		t29 * L_1 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m4130_MI, L_0);
		V_1 = L_1;
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			goto IL_005c;
		}

IL_0017:
		{
			t29 * L_2 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m3970_MI, V_1);
			V_0 = ((t875 *)Castclass(L_2, InitializedTypeInfo(&t875_TI)));
			VirtActionInvoker2< int32_t*, int32_t* >::Invoke(&m4271_MI, V_0, (&V_2), (&V_3));
			*((int32_t*)(p0)) = (int32_t)((int32_t)((*((int32_t*)p0))+V_2));
			if ((((int32_t)(*((int32_t*)p1))) == ((int32_t)((int32_t)2147483647))))
			{
				goto IL_004a;
			}
		}

IL_003f:
		{
			if ((((uint32_t)V_3) != ((uint32_t)((int32_t)2147483647))))
			{
				goto IL_0056;
			}
		}

IL_004a:
		{
			*((int32_t*)(p1)) = (int32_t)((int32_t)2147483647);
			goto IL_005c;
		}

IL_0056:
		{
			*((int32_t*)(p1)) = (int32_t)((int32_t)((*((int32_t*)p1))+V_3));
		}

IL_005c:
		{
			bool L_3 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m3972_MI, V_1);
			if (L_3)
			{
				goto IL_0017;
			}
		}

IL_0067:
		{
			// IL_0067: leave IL_0081
			leaveInstructions[0] = 0x81; // 1
			THROW_SENTINEL(IL_0081);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_006c;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_006c;
	}

IL_006c:
	{ // begin finally (depth: 1)
		{
			V_4 = ((t29 *)IsInst(V_1, InitializedTypeInfo(&t324_TI)));
			if (V_4)
			{
				goto IL_0079;
			}
		}

IL_0078:
		{
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x81:
					goto IL_0081;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}

IL_0079:
		{
			InterfaceActionInvoker0::Invoke(&m1428_MI, V_4);
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x81:
					goto IL_0081;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}
	} // end finally (depth: 1)

IL_0081:
	{
		return;
	}
}
extern MethodInfo m3774_MI;
 t879 * m3774 (t873 * __this, bool p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	t731 * V_2 = {0};
	t870 * V_3 = {0};
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	t875 * V_6 = {0};
	t879 * V_7 = {0};
	t866  V_8 = {0};
	t866  V_9 = {0};
	t29 * V_10 = {0};
	bool V_11 = false;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	t879 * V_14 = {0};
	t292 * V_15 = {0};
	int32_t V_16 = 0;
	t879 * V_17 = {0};
	t29 * V_18 = {0};
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		int32_t L_0 = m3764(__this, &m3764_MI);
		V_1 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t731_TI));
		t731 * L_1 = (t731 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t731_TI));
		m3980(L_1, &m3980_MI);
		V_2 = L_1;
		t870 * L_2 = (t870 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t870_TI));
		m3709(L_2, &m3709_MI);
		V_3 = L_2;
		V_0 = 0;
		t877 * L_3 = m3767(__this, &m3767_MI);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m4096_MI, L_3);
		V_4 = L_4;
		V_5 = 0;
		goto IL_00ca;
	}

IL_002a:
	{
		if (!p0)
		{
			goto IL_0049;
		}
	}
	{
		t877 * L_5 = m3767(__this, &m3767_MI);
		t875 * L_6 = m3760(L_5, ((int32_t)(((int32_t)(V_4-V_5))-1)), &m3760_MI);
		V_6 = L_6;
		goto IL_0058;
	}

IL_0049:
	{
		t877 * L_7 = m3767(__this, &m3767_MI);
		t875 * L_8 = m3760(L_7, V_5, &m3760_MI);
		V_6 = L_8;
	}

IL_0058:
	{
		t879 * L_9 = (t879 *)VirtFuncInvoker1< t879 *, bool >::Invoke(&m3765_MI, V_6, p0);
		V_7 = L_9;
		VirtFuncInvoker1< int32_t, t29 * >::Invoke(&m3991_MI, V_2, V_7);
		bool L_10 = m3866(V_7, &m3866_MI);
		if (!L_10)
		{
			goto IL_008f;
		}
	}
	{
		int32_t L_11 = m3857(V_7, &m3857_MI);
		uint16_t L_12 = m3864(V_7, &m3864_MI);
		t879 * L_13 = (t879 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t879_TI));
		m3856(L_13, __this, ((int32_t)(V_0+L_11)), V_1, L_12, &m3856_MI);
		return L_13;
	}

IL_008f:
	{
		bool L_14 = m3865(V_7, &m3865_MI);
		if (!L_14)
		{
			goto IL_00a9;
		}
	}
	{
		t866  L_15 = m3867(V_7, V_0, &m3867_MI);
		m3711(V_3, L_15, &m3711_MI);
	}

IL_00a9:
	{
		bool L_16 = m3860(V_7, &m3860_MI);
		if (!L_16)
		{
			goto IL_00ba;
		}
	}
	{
		goto IL_00d3;
	}

IL_00ba:
	{
		int32_t L_17 = m3858(V_7, &m3858_MI);
		V_0 = ((int32_t)(V_0+L_17));
		V_5 = ((int32_t)(V_5+1));
	}

IL_00ca:
	{
		if ((((int32_t)V_5) < ((int32_t)V_4)))
		{
			goto IL_002a;
		}
	}

IL_00d3:
	{
		m3712(V_3, &m3712_MI);
		t866  L_18 = m3689(NULL, &m3689_MI);
		V_8 = L_18;
		t29 * L_19 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m3719_MI, V_3);
		V_10 = L_19;
	}

IL_00e8:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0112;
		}

IL_00ed:
		{
			t29 * L_20 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m3970_MI, V_10);
			V_9 = ((*(t866 *)((t866 *)UnBox (L_20, InitializedTypeInfo(&t866_TI)))));
			int32_t L_21 = m3693((&V_9), &m3693_MI);
			int32_t L_22 = m3693((&V_8), &m3693_MI);
			if ((((int32_t)L_21) <= ((int32_t)L_22)))
			{
				goto IL_0112;
			}
		}

IL_010e:
		{
			V_8 = V_9;
		}

IL_0112:
		{
			bool L_23 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m3972_MI, V_10);
			if (L_23)
			{
				goto IL_00ed;
			}
		}

IL_011e:
		{
			// IL_011e: leave IL_0139
			leaveInstructions[0] = 0x139; // 1
			THROW_SENTINEL(IL_0139);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_0123;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_0123;
	}

IL_0123:
	{ // begin finally (depth: 1)
		{
			V_18 = ((t29 *)IsInst(V_10, InitializedTypeInfo(&t324_TI)));
			if (V_18)
			{
				goto IL_0131;
			}
		}

IL_0130:
		{
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x139:
					goto IL_0139;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}

IL_0131:
		{
			InterfaceActionInvoker0::Invoke(&m1428_MI, V_18);
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x139:
					goto IL_0139;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}
	} // end finally (depth: 1)

IL_0139:
	{
		bool L_24 = m3692((&V_8), &m3692_MI);
		if (!L_24)
		{
			goto IL_014d;
		}
	}
	{
		t879 * L_25 = (t879 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t879_TI));
		m3854(L_25, __this, V_1, &m3854_MI);
		return L_25;
	}

IL_014d:
	{
		V_11 = 0;
		V_12 = 0;
		V_0 = 0;
		V_13 = 0;
		goto IL_01c8;
	}

IL_015d:
	{
		t29 * L_26 = (t29 *)VirtFuncInvoker1< t29 *, int32_t >::Invoke(&m3978_MI, V_2, V_13);
		V_14 = ((t879 *)Castclass(L_26, InitializedTypeInfo(&t879_TI)));
		bool L_27 = m3865(V_14, &m3865_MI);
		if (!L_27)
		{
			goto IL_01a7;
		}
	}
	{
		t866  L_28 = m3867(V_14, V_0, &m3867_MI);
		bool L_29 = m3696((&V_8), L_28, &m3696_MI);
		if (!L_29)
		{
			goto IL_01a7;
		}
	}
	{
		bool L_30 = m3863(V_14, &m3863_MI);
		V_11 = ((int32_t)((int32_t)V_11|(int32_t)L_30));
		int32_t L_31 = V_12;
		V_12 = ((int32_t)(L_31+1));
		VirtActionInvoker2< int32_t, t29 * >::Invoke(&m4258_MI, V_2, L_31, V_14);
	}

IL_01a7:
	{
		bool L_32 = m3860(V_14, &m3860_MI);
		if (!L_32)
		{
			goto IL_01b8;
		}
	}
	{
		goto IL_01d5;
	}

IL_01b8:
	{
		int32_t L_33 = m3858(V_14, &m3858_MI);
		V_0 = ((int32_t)(V_0+L_33));
		V_13 = ((int32_t)(V_13+1));
	}

IL_01c8:
	{
		int32_t L_34 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m3976_MI, V_2);
		if ((((int32_t)V_13) < ((int32_t)L_34)))
		{
			goto IL_015d;
		}
	}

IL_01d5:
	{
		t292 * L_35 = (t292 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t292_TI));
		m1311(L_35, &m1311_MI);
		V_15 = L_35;
		V_16 = 0;
		goto IL_0227;
	}

IL_01e4:
	{
		if (!p0)
		{
			goto IL_0203;
		}
	}
	{
		t29 * L_36 = (t29 *)VirtFuncInvoker1< t29 *, int32_t >::Invoke(&m3978_MI, V_2, ((int32_t)(((int32_t)(V_12-V_16))-1)));
		V_17 = ((t879 *)Castclass(L_36, InitializedTypeInfo(&t879_TI)));
		goto IL_0212;
	}

IL_0203:
	{
		t29 * L_37 = (t29 *)VirtFuncInvoker1< t29 *, int32_t >::Invoke(&m3978_MI, V_2, V_16);
		V_17 = ((t879 *)Castclass(L_37, InitializedTypeInfo(&t879_TI)));
	}

IL_0212:
	{
		t7* L_38 = m3862(V_17, &m3862_MI);
		m2927(V_15, L_38, &m2927_MI);
		V_16 = ((int32_t)(V_16+1));
	}

IL_0227:
	{
		if ((((int32_t)V_16) < ((int32_t)V_12)))
		{
			goto IL_01e4;
		}
	}
	{
		int32_t L_39 = m4184(V_15, &m4184_MI);
		int32_t L_40 = m3693((&V_8), &m3693_MI);
		if ((((uint32_t)L_39) != ((uint32_t)L_40)))
		{
			goto IL_025b;
		}
	}
	{
		int32_t L_41 = ((&V_8)->f0);
		t7* L_42 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1315_MI, V_15);
		t879 * L_43 = (t879 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t879_TI));
		m3855(L_43, __this, L_41, V_1, L_42, V_11, &m3855_MI);
		return L_43;
	}

IL_025b:
	{
		int32_t L_44 = m4184(V_15, &m4184_MI);
		int32_t L_45 = m3693((&V_8), &m3693_MI);
		if ((((int32_t)L_44) <= ((int32_t)L_45)))
		{
			goto IL_0285;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t959_TI));
		t958 * L_46 = m4274(NULL, &m4274_MI);
		VirtActionInvoker1< t7* >::Invoke(&m4275_MI, L_46, (t7*) &_stringLiteral513);
		t879 * L_47 = (t879 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t879_TI));
		m3854(L_47, __this, V_1, &m3854_MI);
		return L_47;
	}

IL_0285:
	{
		t956 * L_48 = (t956 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t956_TI));
		m4202(L_48, (t7*) &_stringLiteral514, &m4202_MI);
		il2cpp_codegen_raise_exception(L_48);
	}
}
// Metadata Definition System.Text.RegularExpressions.Syntax.Group
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3770_MI = 
{
	".ctor", (methodPointerType)&m3770, &t873_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 767, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t875_0_0_0;
static ParameterInfo t873_m3771_ParameterInfos[] = 
{
	{"e", 0, 134218450, &EmptyCustomAttributesCache, &t875_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3771_MI = 
{
	"AppendExpression", (methodPointerType)&m3771, &t873_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t873_m3771_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 768, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t878_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t873_m3772_ParameterInfos[] = 
{
	{"cmp", 0, 134218451, &EmptyCustomAttributesCache, &t878_0_0_0},
	{"reverse", 1, 134218452, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3772_MI = 
{
	"Compile", (methodPointerType)&m3772, &t873_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t297, t873_m3772_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 2, false, false, 769, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_1_0_2;
extern Il2CppType t44_1_0_2;
static ParameterInfo t873_m3773_ParameterInfos[] = 
{
	{"min", 0, 134218453, &EmptyCustomAttributesCache, &t44_1_0_2},
	{"max", 1, 134218454, &EmptyCustomAttributesCache, &t44_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t390_t390 (MethodInfo* method, void* obj, void** args);
MethodInfo m3773_MI = 
{
	"GetWidth", (methodPointerType)&m3773, &t873_TI, &t21_0_0_0, RuntimeInvoker_t21_t390_t390, t873_m3773_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, false, 770, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t873_m3774_ParameterInfos[] = 
{
	{"reverse", 0, 134218455, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t879_0_0_0;
extern void* RuntimeInvoker_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3774_MI = 
{
	"GetAnchorInfo", (methodPointerType)&m3774, &t873_TI, &t879_0_0_0, RuntimeInvoker_t29_t297, t873_m3774_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 1, false, false, 771, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t873_MIs[] =
{
	&m3770_MI,
	&m3771_MI,
	&m3772_MI,
	&m3773_MI,
	&m3774_MI,
	NULL
};
static MethodInfo* t873_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m3772_MI,
	&m3773_MI,
	&m3774_MI,
	&m3769_MI,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t873_1_0_0;
struct t873;
TypeInfo t873_TI = 
{
	&g_System_dll_Image, NULL, "Group", "System.Text.RegularExpressions.Syntax", t873_MIs, NULL, NULL, NULL, &t880_TI, NULL, NULL, &t873_TI, NULL, t873_VT, &EmptyCustomAttributesCache, &t873_TI, &t873_0_0_0, &t873_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t873), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 5, 0, 0, 0, 0, 8, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo t845_TI;
extern MethodInfo m4247_MI;
extern MethodInfo m4252_MI;
extern MethodInfo m4249_MI;
extern MethodInfo m4233_MI;
extern MethodInfo m4232_MI;
extern MethodInfo m4226_MI;
extern MethodInfo m4253_MI;


 void m3775 (t872 * __this, MethodInfo* method){
	{
		m3770(__this, &m3770_MI);
		__this->f1 = 0;
		return;
	}
}
 void m3776 (t872 * __this, int32_t p0, MethodInfo* method){
	{
		__this->f1 = p0;
		return;
	}
}
extern MethodInfo m3777_MI;
 void m3777 (t872 * __this, t29 * p0, bool p1, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	t879 * V_2 = {0};
	t852 * V_3 = {0};
	{
		VirtActionInvoker2< int32_t*, int32_t* >::Invoke(&m3773_MI, __this, (&V_0), (&V_1));
		int32_t L_0 = (__this->f1);
		InterfaceActionInvoker3< int32_t, int32_t, int32_t >::Invoke(&m4247_MI, p0, L_0, V_0, V_1);
		t879 * L_1 = (t879 *)VirtFuncInvoker1< t879 *, bool >::Invoke(&m3774_MI, __this, p1);
		V_2 = L_1;
		t852 * L_2 = (t852 *)InterfaceFuncInvoker0< t852 * >::Invoke(&m4252_MI, p0);
		V_3 = L_2;
		int32_t L_3 = m3857(V_2, &m3857_MI);
		InterfaceActionInvoker3< bool, int32_t, t852 * >::Invoke(&m4249_MI, p0, p1, L_3, V_3);
		bool L_4 = m3866(V_2, &m3866_MI);
		if (!L_4)
		{
			goto IL_0051;
		}
	}
	{
		uint16_t L_5 = m3864(V_2, &m3864_MI);
		InterfaceActionInvoker1< uint16_t >::Invoke(&m4233_MI, p0, L_5);
		goto IL_006f;
	}

IL_0051:
	{
		bool L_6 = m3865(V_2, &m3865_MI);
		if (!L_6)
		{
			goto IL_006f;
		}
	}
	{
		t7* L_7 = m3862(V_2, &m3862_MI);
		bool L_8 = m3863(V_2, &m3863_MI);
		InterfaceActionInvoker3< t7*, bool, bool >::Invoke(&m4232_MI, p0, L_7, L_8, p1);
	}

IL_006f:
	{
		InterfaceActionInvoker0::Invoke(&m4226_MI, p0);
		InterfaceActionInvoker1< t852 * >::Invoke(&m4253_MI, p0, V_3);
		m3772(__this, p0, p1, &m3772_MI);
		InterfaceActionInvoker0::Invoke(&m4226_MI, p0);
		return;
	}
}
// Metadata Definition System.Text.RegularExpressions.Syntax.RegularExpression
extern Il2CppType t44_0_0_1;
FieldInfo t872_f1_FieldInfo = 
{
	"group_count", &t44_0_0_1, &t872_TI, offsetof(t872, f1), &EmptyCustomAttributesCache};
static FieldInfo* t872_FIs[] =
{
	&t872_f1_FieldInfo,
	NULL
};
static PropertyInfo t872____GroupCount_PropertyInfo = 
{
	&t872_TI, "GroupCount", NULL, &m3776_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t872_PIs[] =
{
	&t872____GroupCount_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3775_MI = 
{
	".ctor", (methodPointerType)&m3775, &t872_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 772, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t872_m3776_ParameterInfos[] = 
{
	{"value", 0, 134218456, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3776_MI = 
{
	"set_GroupCount", (methodPointerType)&m3776, &t872_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t872_m3776_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 773, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t878_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t872_m3777_ParameterInfos[] = 
{
	{"cmp", 0, 134218457, &EmptyCustomAttributesCache, &t878_0_0_0},
	{"reverse", 1, 134218458, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3777_MI = 
{
	"Compile", (methodPointerType)&m3777, &t872_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t297, t872_m3777_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 2, false, false, 774, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t872_MIs[] =
{
	&m3775_MI,
	&m3776_MI,
	&m3777_MI,
	NULL
};
static MethodInfo* t872_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m3777_MI,
	&m3773_MI,
	&m3774_MI,
	&m3769_MI,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t872_0_0_0;
extern Il2CppType t872_1_0_0;
struct t872;
TypeInfo t872_TI = 
{
	&g_System_dll_Image, NULL, "RegularExpression", "System.Text.RegularExpressions.Syntax", t872_MIs, t872_PIs, t872_FIs, NULL, &t873_TI, NULL, NULL, &t872_TI, NULL, t872_VT, &EmptyCustomAttributesCache, &t872_TI, &t872_0_0_0, &t872_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t872), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 3, 1, 1, 0, 0, 8, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m4234_MI;
extern MethodInfo m4235_MI;


 void m3778 (t881 * __this, MethodInfo* method){
	{
		m3770(__this, &m3770_MI);
		__this->f1 = 0;
		__this->f2 = (t7*)NULL;
		return;
	}
}
 int32_t m3779 (t881 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		return L_0;
	}
}
 void m3780 (t881 * __this, int32_t p0, MethodInfo* method){
	{
		__this->f1 = p0;
		return;
	}
}
 t7* m3781 (t881 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f2);
		return L_0;
	}
}
 void m3782 (t881 * __this, t7* p0, MethodInfo* method){
	{
		__this->f2 = p0;
		return;
	}
}
 bool m3783 (t881 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f2);
		return ((((int32_t)((((t7*)L_0) == ((t29 *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
extern MethodInfo m3784_MI;
 void m3784 (t881 * __this, t29 * p0, bool p1, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		InterfaceActionInvoker1< int32_t >::Invoke(&m4234_MI, p0, L_0);
		m3772(__this, p0, p1, &m3772_MI);
		int32_t L_1 = (__this->f1);
		InterfaceActionInvoker1< int32_t >::Invoke(&m4235_MI, p0, L_1);
		return;
	}
}
extern MethodInfo m3785_MI;
 bool m3785 (t881 * __this, MethodInfo* method){
	{
		return 1;
	}
}
extern MethodInfo m3786_MI;
 int32_t m3786 (t881 * __this, t29 * p0, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		int32_t L_1 = (((t881 *)Castclass(p0, InitializedTypeInfo(&t881_TI)))->f1);
		return ((int32_t)(L_0-L_1));
	}
}
// Metadata Definition System.Text.RegularExpressions.Syntax.CapturingGroup
extern Il2CppType t44_0_0_1;
FieldInfo t881_f1_FieldInfo = 
{
	"gid", &t44_0_0_1, &t881_TI, offsetof(t881, f1), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_1;
FieldInfo t881_f2_FieldInfo = 
{
	"name", &t7_0_0_1, &t881_TI, offsetof(t881, f2), &EmptyCustomAttributesCache};
static FieldInfo* t881_FIs[] =
{
	&t881_f1_FieldInfo,
	&t881_f2_FieldInfo,
	NULL
};
static PropertyInfo t881____Index_PropertyInfo = 
{
	&t881_TI, "Index", &m3779_MI, &m3780_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t881____Name_PropertyInfo = 
{
	&t881_TI, "Name", &m3781_MI, &m3782_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t881____IsNamed_PropertyInfo = 
{
	&t881_TI, "IsNamed", &m3783_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t881_PIs[] =
{
	&t881____Index_PropertyInfo,
	&t881____Name_PropertyInfo,
	&t881____IsNamed_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3778_MI = 
{
	".ctor", (methodPointerType)&m3778, &t881_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 775, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3779_MI = 
{
	"get_Index", (methodPointerType)&m3779, &t881_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 776, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t881_m3780_ParameterInfos[] = 
{
	{"value", 0, 134218459, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3780_MI = 
{
	"set_Index", (methodPointerType)&m3780, &t881_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t881_m3780_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 777, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3781_MI = 
{
	"get_Name", (methodPointerType)&m3781, &t881_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 778, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t881_m3782_ParameterInfos[] = 
{
	{"value", 0, 134218460, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3782_MI = 
{
	"set_Name", (methodPointerType)&m3782, &t881_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t881_m3782_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 779, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m3783_MI = 
{
	"get_IsNamed", (methodPointerType)&m3783, &t881_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 780, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t878_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t881_m3784_ParameterInfos[] = 
{
	{"cmp", 0, 134218461, &EmptyCustomAttributesCache, &t878_0_0_0},
	{"reverse", 1, 134218462, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3784_MI = 
{
	"Compile", (methodPointerType)&m3784, &t881_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t297, t881_m3784_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 2, false, false, 781, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m3785_MI = 
{
	"IsComplex", (methodPointerType)&m3785, &t881_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 198, 0, 7, 0, false, false, 782, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t881_m3786_ParameterInfos[] = 
{
	{"other", 0, 134218463, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3786_MI = 
{
	"CompareTo", (methodPointerType)&m3786, &t881_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t881_m3786_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 8, 1, false, false, 783, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t881_MIs[] =
{
	&m3778_MI,
	&m3779_MI,
	&m3780_MI,
	&m3781_MI,
	&m3782_MI,
	&m3783_MI,
	&m3784_MI,
	&m3785_MI,
	&m3786_MI,
	NULL
};
static MethodInfo* t881_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m3784_MI,
	&m3773_MI,
	&m3774_MI,
	&m3785_MI,
	&m3786_MI,
};
static TypeInfo* t881_ITIs[] = 
{
	&t290_TI,
};
static Il2CppInterfaceOffsetPair t881_IOs[] = 
{
	{ &t290_TI, 8},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t881_0_0_0;
extern Il2CppType t881_1_0_0;
struct t881;
TypeInfo t881_TI = 
{
	&g_System_dll_Image, NULL, "CapturingGroup", "System.Text.RegularExpressions.Syntax", t881_MIs, t881_PIs, t881_FIs, NULL, &t873_TI, NULL, NULL, &t881_TI, t881_ITIs, t881_VT, &EmptyCustomAttributesCache, &t881_TI, &t881_0_0_0, &t881_1_0_0, t881_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t881), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 9, 3, 2, 0, 0, 9, 1, 1};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m4236_MI;
extern MethodInfo m4237_MI;


 void m3787 (t882 * __this, MethodInfo* method){
	{
		m3778(__this, &m3778_MI);
		__this->f3 = (t881 *)NULL;
		return;
	}
}
 void m3788 (t882 * __this, t881 * p0, MethodInfo* method){
	{
		__this->f3 = p0;
		return;
	}
}
extern MethodInfo m3789_MI;
 void m3789 (t882 * __this, t29 * p0, bool p1, MethodInfo* method){
	t852 * V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	t875 * V_3 = {0};
	{
		t852 * L_0 = (t852 *)InterfaceFuncInvoker0< t852 * >::Invoke(&m4252_MI, p0);
		V_0 = L_0;
		int32_t L_1 = m3779(__this, &m3779_MI);
		t881 * L_2 = (__this->f3);
		int32_t L_3 = m3779(L_2, &m3779_MI);
		bool L_4 = m3783(__this, &m3783_MI);
		InterfaceActionInvoker4< int32_t, int32_t, bool, t852 * >::Invoke(&m4236_MI, p0, L_1, L_3, L_4, V_0);
		t877 * L_5 = m3767(__this, &m3767_MI);
		int32_t L_6 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m4096_MI, L_5);
		V_1 = L_6;
		V_2 = 0;
		goto IL_006d;
	}

IL_0038:
	{
		if (!p1)
		{
			goto IL_0054;
		}
	}
	{
		t877 * L_7 = m3767(__this, &m3767_MI);
		t875 * L_8 = m3760(L_7, ((int32_t)(((int32_t)(V_1-V_2))-1)), &m3760_MI);
		V_3 = L_8;
		goto IL_0061;
	}

IL_0054:
	{
		t877 * L_9 = m3767(__this, &m3767_MI);
		t875 * L_10 = m3760(L_9, V_2, &m3760_MI);
		V_3 = L_10;
	}

IL_0061:
	{
		VirtActionInvoker2< t29 *, bool >::Invoke(&m4272_MI, V_3, p0, p1);
		V_2 = ((int32_t)(V_2+1));
	}

IL_006d:
	{
		if ((((int32_t)V_2) < ((int32_t)V_1)))
		{
			goto IL_0038;
		}
	}
	{
		InterfaceActionInvoker0::Invoke(&m4237_MI, p0);
		InterfaceActionInvoker1< t852 * >::Invoke(&m4253_MI, p0, V_0);
		return;
	}
}
// Metadata Definition System.Text.RegularExpressions.Syntax.BalancingGroup
extern Il2CppType t881_0_0_1;
FieldInfo t882_f3_FieldInfo = 
{
	"balance", &t881_0_0_1, &t882_TI, offsetof(t882, f3), &EmptyCustomAttributesCache};
static FieldInfo* t882_FIs[] =
{
	&t882_f3_FieldInfo,
	NULL
};
static PropertyInfo t882____Balance_PropertyInfo = 
{
	&t882_TI, "Balance", NULL, &m3788_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t882_PIs[] =
{
	&t882____Balance_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3787_MI = 
{
	".ctor", (methodPointerType)&m3787, &t882_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 784, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t881_0_0_0;
static ParameterInfo t882_m3788_ParameterInfos[] = 
{
	{"value", 0, 134218464, &EmptyCustomAttributesCache, &t881_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3788_MI = 
{
	"set_Balance", (methodPointerType)&m3788, &t882_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t882_m3788_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 785, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t878_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t882_m3789_ParameterInfos[] = 
{
	{"cmp", 0, 134218465, &EmptyCustomAttributesCache, &t878_0_0_0},
	{"reverse", 1, 134218466, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3789_MI = 
{
	"Compile", (methodPointerType)&m3789, &t882_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t297, t882_m3789_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 2, false, false, 786, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t882_MIs[] =
{
	&m3787_MI,
	&m3788_MI,
	&m3789_MI,
	NULL
};
static MethodInfo* t882_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m3789_MI,
	&m3773_MI,
	&m3774_MI,
	&m3785_MI,
	&m3786_MI,
};
static Il2CppInterfaceOffsetPair t882_IOs[] = 
{
	{ &t290_TI, 8},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t882_0_0_0;
extern Il2CppType t882_1_0_0;
struct t882;
TypeInfo t882_TI = 
{
	&g_System_dll_Image, NULL, "BalancingGroup", "System.Text.RegularExpressions.Syntax", t882_MIs, t882_PIs, t882_FIs, NULL, &t881_TI, NULL, NULL, &t882_TI, NULL, t882_VT, &EmptyCustomAttributesCache, &t882_TI, &t882_0_0_0, &t882_1_0_0, t882_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t882), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 3, 1, 1, 0, 0, 9, 0, 1};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m4240_MI;


 void m3790 (t883 * __this, MethodInfo* method){
	{
		m3770(__this, &m3770_MI);
		return;
	}
}
extern MethodInfo m3791_MI;
 void m3791 (t883 * __this, t29 * p0, bool p1, MethodInfo* method){
	t852 * V_0 = {0};
	{
		t852 * L_0 = (t852 *)InterfaceFuncInvoker0< t852 * >::Invoke(&m4252_MI, p0);
		V_0 = L_0;
		InterfaceActionInvoker1< t852 * >::Invoke(&m4240_MI, p0, V_0);
		m3772(__this, p0, p1, &m3772_MI);
		InterfaceActionInvoker0::Invoke(&m4226_MI, p0);
		InterfaceActionInvoker1< t852 * >::Invoke(&m4253_MI, p0, V_0);
		return;
	}
}
extern MethodInfo m3792_MI;
 bool m3792 (t883 * __this, MethodInfo* method){
	{
		return 1;
	}
}
// Metadata Definition System.Text.RegularExpressions.Syntax.NonBacktrackingGroup
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3790_MI = 
{
	".ctor", (methodPointerType)&m3790, &t883_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 787, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t878_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t883_m3791_ParameterInfos[] = 
{
	{"cmp", 0, 134218467, &EmptyCustomAttributesCache, &t878_0_0_0},
	{"reverse", 1, 134218468, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3791_MI = 
{
	"Compile", (methodPointerType)&m3791, &t883_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t297, t883_m3791_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 2, false, false, 788, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m3792_MI = 
{
	"IsComplex", (methodPointerType)&m3792, &t883_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 198, 0, 7, 0, false, false, 789, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t883_MIs[] =
{
	&m3790_MI,
	&m3791_MI,
	&m3792_MI,
	NULL
};
static MethodInfo* t883_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m3791_MI,
	&m3773_MI,
	&m3774_MI,
	&m3792_MI,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t883_0_0_0;
extern Il2CppType t883_1_0_0;
struct t883;
TypeInfo t883_TI = 
{
	&g_System_dll_Image, NULL, "NonBacktrackingGroup", "System.Text.RegularExpressions.Syntax", t883_MIs, NULL, NULL, NULL, &t873_TI, NULL, NULL, &t883_TI, NULL, t883_VT, &EmptyCustomAttributesCache, &t883_TI, &t883_0_0_0, &t883_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t883), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 8, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m3794_MI;
extern MethodInfo m4244_MI;
extern MethodInfo m4245_MI;
extern MethodInfo m4248_MI;
extern MethodInfo m3796_MI;
extern MethodInfo m3861_MI;
extern MethodInfo m1431_MI;


 void m3793 (t884 * __this, int32_t p0, int32_t p1, bool p2, MethodInfo* method){
	{
		m3766(__this, &m3766_MI);
		t877 * L_0 = m3767(__this, &m3767_MI);
		m3759(L_0, (t875 *)NULL, &m3759_MI);
		__this->f1 = p0;
		__this->f2 = p1;
		__this->f3 = p2;
		return;
	}
}
 t875 * m3794 (t884 * __this, MethodInfo* method){
	{
		t877 * L_0 = m3767(__this, &m3767_MI);
		t875 * L_1 = m3760(L_0, 0, &m3760_MI);
		return L_1;
	}
}
 void m3795 (t884 * __this, t875 * p0, MethodInfo* method){
	{
		t877 * L_0 = m3767(__this, &m3767_MI);
		m3761(L_0, 0, p0, &m3761_MI);
		return;
	}
}
 int32_t m3796 (t884 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		return L_0;
	}
}
extern MethodInfo m3797_MI;
 void m3797 (t884 * __this, t29 * p0, bool p1, MethodInfo* method){
	t852 * V_0 = {0};
	t852 * V_1 = {0};
	{
		t875 * L_0 = m3794(__this, &m3794_MI);
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m4273_MI, L_0);
		if (!L_1)
		{
			goto IL_0049;
		}
	}
	{
		t852 * L_2 = (t852 *)InterfaceFuncInvoker0< t852 * >::Invoke(&m4252_MI, p0);
		V_0 = L_2;
		int32_t L_3 = (__this->f1);
		int32_t L_4 = (__this->f2);
		bool L_5 = (__this->f3);
		InterfaceActionInvoker4< int32_t, int32_t, bool, t852 * >::Invoke(&m4244_MI, p0, L_3, L_4, L_5, V_0);
		t875 * L_6 = m3794(__this, &m3794_MI);
		VirtActionInvoker2< t29 *, bool >::Invoke(&m4272_MI, L_6, p0, p1);
		InterfaceActionInvoker1< t852 * >::Invoke(&m4245_MI, p0, V_0);
		goto IL_0083;
	}

IL_0049:
	{
		t852 * L_7 = (t852 *)InterfaceFuncInvoker0< t852 * >::Invoke(&m4252_MI, p0);
		V_1 = L_7;
		int32_t L_8 = (__this->f1);
		int32_t L_9 = (__this->f2);
		bool L_10 = (__this->f3);
		InterfaceActionInvoker4< int32_t, int32_t, bool, t852 * >::Invoke(&m4248_MI, p0, L_8, L_9, L_10, V_1);
		t875 * L_11 = m3794(__this, &m3794_MI);
		VirtActionInvoker2< t29 *, bool >::Invoke(&m4272_MI, L_11, p0, p1);
		InterfaceActionInvoker0::Invoke(&m4226_MI, p0);
		InterfaceActionInvoker1< t852 * >::Invoke(&m4253_MI, p0, V_1);
	}

IL_0083:
	{
		return;
	}
}
extern MethodInfo m3798_MI;
 void m3798 (t884 * __this, int32_t* p0, int32_t* p1, MethodInfo* method){
	{
		t875 * L_0 = m3794(__this, &m3794_MI);
		VirtActionInvoker2< int32_t*, int32_t* >::Invoke(&m4271_MI, L_0, p0, p1);
		int32_t L_1 = (__this->f1);
		*((int32_t*)(p0)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)p0))*(int32_t)L_1));
		if ((((int32_t)(*((int32_t*)p1))) == ((int32_t)((int32_t)2147483647))))
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_2 = (__this->f2);
		if ((((uint32_t)L_2) != ((uint32_t)((int32_t)65535))))
		{
			goto IL_0040;
		}
	}

IL_0034:
	{
		*((int32_t*)(p1)) = (int32_t)((int32_t)2147483647);
		goto IL_004b;
	}

IL_0040:
	{
		int32_t L_3 = (__this->f2);
		*((int32_t*)(p1)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)p1))*(int32_t)L_3));
	}

IL_004b:
	{
		return;
	}
}
extern MethodInfo m3799_MI;
 t879 * m3799 (t884 * __this, bool p0, MethodInfo* method){
	int32_t V_0 = 0;
	t879 * V_1 = {0};
	t7* V_2 = {0};
	t292 * V_3 = {0};
	int32_t V_4 = 0;
	{
		int32_t L_0 = m3764(__this, &m3764_MI);
		V_0 = L_0;
		int32_t L_1 = m3796(__this, &m3796_MI);
		if (L_1)
		{
			goto IL_001a;
		}
	}
	{
		t879 * L_2 = (t879 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t879_TI));
		m3854(L_2, __this, V_0, &m3854_MI);
		return L_2;
	}

IL_001a:
	{
		t875 * L_3 = m3794(__this, &m3794_MI);
		t879 * L_4 = (t879 *)VirtFuncInvoker1< t879 *, bool >::Invoke(&m3765_MI, L_3, p0);
		V_1 = L_4;
		bool L_5 = m3866(V_1, &m3866_MI);
		if (!L_5)
		{
			goto IL_0046;
		}
	}
	{
		int32_t L_6 = m3857(V_1, &m3857_MI);
		uint16_t L_7 = m3864(V_1, &m3864_MI);
		t879 * L_8 = (t879 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t879_TI));
		m3856(L_8, __this, L_6, V_0, L_7, &m3856_MI);
		return L_8;
	}

IL_0046:
	{
		bool L_9 = m3865(V_1, &m3865_MI);
		if (!L_9)
		{
			goto IL_00bc;
		}
	}
	{
		bool L_10 = m3861(V_1, &m3861_MI);
		if (!L_10)
		{
			goto IL_00a2;
		}
	}
	{
		t7* L_11 = m3862(V_1, &m3862_MI);
		V_2 = L_11;
		t292 * L_12 = (t292 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t292_TI));
		m1431(L_12, V_2, &m1431_MI);
		V_3 = L_12;
		V_4 = 1;
		goto IL_0080;
	}

IL_0072:
	{
		m2927(V_3, V_2, &m2927_MI);
		V_4 = ((int32_t)(V_4+1));
	}

IL_0080:
	{
		int32_t L_13 = m3796(__this, &m3796_MI);
		if ((((int32_t)V_4) < ((int32_t)L_13)))
		{
			goto IL_0072;
		}
	}
	{
		t7* L_14 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1315_MI, V_3);
		bool L_15 = m3863(V_1, &m3863_MI);
		t879 * L_16 = (t879 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t879_TI));
		m3855(L_16, __this, 0, V_0, L_14, L_15, &m3855_MI);
		return L_16;
	}

IL_00a2:
	{
		int32_t L_17 = m3857(V_1, &m3857_MI);
		t7* L_18 = m3862(V_1, &m3862_MI);
		bool L_19 = m3863(V_1, &m3863_MI);
		t879 * L_20 = (t879 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t879_TI));
		m3855(L_20, __this, L_17, V_0, L_18, L_19, &m3855_MI);
		return L_20;
	}

IL_00bc:
	{
		t879 * L_21 = (t879 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t879_TI));
		m3854(L_21, __this, V_0, &m3854_MI);
		return L_21;
	}
}
// Metadata Definition System.Text.RegularExpressions.Syntax.Repetition
extern Il2CppType t44_0_0_1;
FieldInfo t884_f1_FieldInfo = 
{
	"min", &t44_0_0_1, &t884_TI, offsetof(t884, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t884_f2_FieldInfo = 
{
	"max", &t44_0_0_1, &t884_TI, offsetof(t884, f2), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t884_f3_FieldInfo = 
{
	"lazy", &t40_0_0_1, &t884_TI, offsetof(t884, f3), &EmptyCustomAttributesCache};
static FieldInfo* t884_FIs[] =
{
	&t884_f1_FieldInfo,
	&t884_f2_FieldInfo,
	&t884_f3_FieldInfo,
	NULL
};
static PropertyInfo t884____Expression_PropertyInfo = 
{
	&t884_TI, "Expression", &m3794_MI, &m3795_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t884____Minimum_PropertyInfo = 
{
	&t884_TI, "Minimum", &m3796_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t884_PIs[] =
{
	&t884____Expression_PropertyInfo,
	&t884____Minimum_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t884_m3793_ParameterInfos[] = 
{
	{"min", 0, 134218469, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"max", 1, 134218470, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"lazy", 2, 134218471, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3793_MI = 
{
	".ctor", (methodPointerType)&m3793, &t884_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44_t297, t884_m3793_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, false, 790, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t875_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3794_MI = 
{
	"get_Expression", (methodPointerType)&m3794, &t884_TI, &t875_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 791, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t875_0_0_0;
static ParameterInfo t884_m3795_ParameterInfos[] = 
{
	{"value", 0, 134218472, &EmptyCustomAttributesCache, &t875_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3795_MI = 
{
	"set_Expression", (methodPointerType)&m3795, &t884_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t884_m3795_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 792, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3796_MI = 
{
	"get_Minimum", (methodPointerType)&m3796, &t884_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 793, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t878_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t884_m3797_ParameterInfos[] = 
{
	{"cmp", 0, 134218473, &EmptyCustomAttributesCache, &t878_0_0_0},
	{"reverse", 1, 134218474, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3797_MI = 
{
	"Compile", (methodPointerType)&m3797, &t884_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t297, t884_m3797_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 2, false, false, 794, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_1_0_2;
extern Il2CppType t44_1_0_2;
static ParameterInfo t884_m3798_ParameterInfos[] = 
{
	{"min", 0, 134218475, &EmptyCustomAttributesCache, &t44_1_0_2},
	{"max", 1, 134218476, &EmptyCustomAttributesCache, &t44_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t390_t390 (MethodInfo* method, void* obj, void** args);
MethodInfo m3798_MI = 
{
	"GetWidth", (methodPointerType)&m3798, &t884_TI, &t21_0_0_0, RuntimeInvoker_t21_t390_t390, t884_m3798_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, false, 795, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t884_m3799_ParameterInfos[] = 
{
	{"reverse", 0, 134218477, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t879_0_0_0;
extern void* RuntimeInvoker_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3799_MI = 
{
	"GetAnchorInfo", (methodPointerType)&m3799, &t884_TI, &t879_0_0_0, RuntimeInvoker_t29_t297, t884_m3799_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 1, false, false, 796, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t884_MIs[] =
{
	&m3793_MI,
	&m3794_MI,
	&m3795_MI,
	&m3796_MI,
	&m3797_MI,
	&m3798_MI,
	&m3799_MI,
	NULL
};
static MethodInfo* t884_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m3797_MI,
	&m3798_MI,
	&m3799_MI,
	&m3769_MI,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t884_0_0_0;
extern Il2CppType t884_1_0_0;
struct t884;
TypeInfo t884_TI = 
{
	&g_System_dll_Image, NULL, "Repetition", "System.Text.RegularExpressions.Syntax", t884_MIs, t884_PIs, t884_FIs, NULL, &t880_TI, NULL, NULL, &t884_TI, NULL, t884_VT, &EmptyCustomAttributesCache, &t884_TI, &t884_0_0_0, &t884_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t884), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 7, 2, 3, 0, 0, 8, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t874_TI;



extern MethodInfo m3800_MI;
 void m3800 (t874 * __this, MethodInfo* method){
	{
		m3766(__this, &m3766_MI);
		t877 * L_0 = m3767(__this, &m3767_MI);
		m3759(L_0, (t875 *)NULL, &m3759_MI);
		t877 * L_1 = m3767(__this, &m3767_MI);
		m3759(L_1, (t875 *)NULL, &m3759_MI);
		return;
	}
}
 t875 * m3801 (t874 * __this, MethodInfo* method){
	{
		t877 * L_0 = m3767(__this, &m3767_MI);
		t875 * L_1 = m3760(L_0, 0, &m3760_MI);
		return L_1;
	}
}
 void m3802 (t874 * __this, t875 * p0, MethodInfo* method){
	{
		t877 * L_0 = m3767(__this, &m3767_MI);
		m3761(L_0, 0, p0, &m3761_MI);
		return;
	}
}
 t875 * m3803 (t874 * __this, MethodInfo* method){
	{
		t877 * L_0 = m3767(__this, &m3767_MI);
		t875 * L_1 = m3760(L_0, 1, &m3760_MI);
		return L_1;
	}
}
 void m3804 (t874 * __this, t875 * p0, MethodInfo* method){
	{
		t877 * L_0 = m3767(__this, &m3767_MI);
		m3761(L_0, 1, p0, &m3761_MI);
		return;
	}
}
extern MethodInfo m3805_MI;
 void m3805 (t874 * __this, int32_t* p0, int32_t* p1, MethodInfo* method){
	{
		m3768(__this, p0, p1, 2, &m3768_MI);
		t875 * L_0 = m3801(__this, &m3801_MI);
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		t875 * L_1 = m3803(__this, &m3803_MI);
		if (L_1)
		{
			goto IL_0022;
		}
	}

IL_001f:
	{
		*((int32_t*)(p0)) = (int32_t)0;
	}

IL_0022:
	{
		return;
	}
}
// Metadata Definition System.Text.RegularExpressions.Syntax.Assertion
static PropertyInfo t874____TrueExpression_PropertyInfo = 
{
	&t874_TI, "TrueExpression", &m3801_MI, &m3802_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t874____FalseExpression_PropertyInfo = 
{
	&t874_TI, "FalseExpression", &m3803_MI, &m3804_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t874_PIs[] =
{
	&t874____TrueExpression_PropertyInfo,
	&t874____FalseExpression_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3800_MI = 
{
	".ctor", (methodPointerType)&m3800, &t874_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 797, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t875_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3801_MI = 
{
	"get_TrueExpression", (methodPointerType)&m3801, &t874_TI, &t875_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 798, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t875_0_0_0;
static ParameterInfo t874_m3802_ParameterInfos[] = 
{
	{"value", 0, 134218478, &EmptyCustomAttributesCache, &t875_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3802_MI = 
{
	"set_TrueExpression", (methodPointerType)&m3802, &t874_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t874_m3802_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 799, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t875_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3803_MI = 
{
	"get_FalseExpression", (methodPointerType)&m3803, &t874_TI, &t875_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 800, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t875_0_0_0;
static ParameterInfo t874_m3804_ParameterInfos[] = 
{
	{"value", 0, 134218479, &EmptyCustomAttributesCache, &t875_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3804_MI = 
{
	"set_FalseExpression", (methodPointerType)&m3804, &t874_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t874_m3804_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 801, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_1_0_2;
extern Il2CppType t44_1_0_2;
static ParameterInfo t874_m3805_ParameterInfos[] = 
{
	{"min", 0, 134218480, &EmptyCustomAttributesCache, &t44_1_0_2},
	{"max", 1, 134218481, &EmptyCustomAttributesCache, &t44_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t390_t390 (MethodInfo* method, void* obj, void** args);
MethodInfo m3805_MI = 
{
	"GetWidth", (methodPointerType)&m3805, &t874_TI, &t21_0_0_0, RuntimeInvoker_t21_t390_t390, t874_m3805_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, false, 802, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t874_MIs[] =
{
	&m3800_MI,
	&m3801_MI,
	&m3802_MI,
	&m3803_MI,
	&m3804_MI,
	&m3805_MI,
	NULL
};
static MethodInfo* t874_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	NULL,
	&m3805_MI,
	&m3765_MI,
	&m3769_MI,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t874_1_0_0;
struct t874;
TypeInfo t874_TI = 
{
	&g_System_dll_Image, NULL, "Assertion", "System.Text.RegularExpressions.Syntax", t874_MIs, t874_PIs, NULL, NULL, &t880_TI, NULL, NULL, &t874_TI, NULL, t874_VT, &EmptyCustomAttributesCache, &t874_TI, &t874_0_0_0, &t874_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t874), 0, -1, 0, 0, -1, 1048704, 0, false, false, false, false, false, false, false, false, false, false, false, false, 6, 2, 0, 0, 0, 8, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m3810_MI;
extern MethodInfo m3816_MI;
extern MethodInfo m4239_MI;
extern MethodInfo m4243_MI;
extern MethodInfo m3817_MI;


 void m3806 (t885 * __this, t886 * p0, MethodInfo* method){
	{
		m3800(__this, &m3800_MI);
		__this->f3 = p0;
		return;
	}
}
 void m3807 (t885 * __this, t881 * p0, MethodInfo* method){
	{
		__this->f2 = p0;
		return;
	}
}
extern MethodInfo m3808_MI;
 void m3808 (t885 * __this, t29 * p0, bool p1, MethodInfo* method){
	int32_t V_0 = 0;
	t852 * V_1 = {0};
	t852 * V_2 = {0};
	{
		t881 * L_0 = (__this->f2);
		if (L_0)
		{
			goto IL_0019;
		}
	}
	{
		t876 * L_1 = m3810(__this, &m3810_MI);
		VirtActionInvoker2< t29 *, bool >::Invoke(&m3816_MI, L_1, p0, p1);
		return;
	}

IL_0019:
	{
		t881 * L_2 = (__this->f2);
		int32_t L_3 = m3779(L_2, &m3779_MI);
		V_0 = L_3;
		t852 * L_4 = (t852 *)InterfaceFuncInvoker0< t852 * >::Invoke(&m4252_MI, p0);
		V_1 = L_4;
		t875 * L_5 = m3803(__this, &m3803_MI);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		InterfaceActionInvoker2< int32_t, t852 * >::Invoke(&m4239_MI, p0, V_0, V_1);
		t875 * L_6 = m3801(__this, &m3801_MI);
		VirtActionInvoker2< t29 *, bool >::Invoke(&m4272_MI, L_6, p0, p1);
		goto IL_0088;
	}

IL_0051:
	{
		t852 * L_7 = (t852 *)InterfaceFuncInvoker0< t852 * >::Invoke(&m4252_MI, p0);
		V_2 = L_7;
		InterfaceActionInvoker2< int32_t, t852 * >::Invoke(&m4239_MI, p0, V_0, V_2);
		t875 * L_8 = m3801(__this, &m3801_MI);
		VirtActionInvoker2< t29 *, bool >::Invoke(&m4272_MI, L_8, p0, p1);
		InterfaceActionInvoker1< t852 * >::Invoke(&m4243_MI, p0, V_1);
		InterfaceActionInvoker1< t852 * >::Invoke(&m4253_MI, p0, V_2);
		t875 * L_9 = m3803(__this, &m3803_MI);
		VirtActionInvoker2< t29 *, bool >::Invoke(&m4272_MI, L_9, p0, p1);
	}

IL_0088:
	{
		InterfaceActionInvoker1< t852 * >::Invoke(&m4253_MI, p0, V_1);
		return;
	}
}
extern MethodInfo m3809_MI;
 bool m3809 (t885 * __this, MethodInfo* method){
	{
		t881 * L_0 = (__this->f2);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		t876 * L_1 = m3810(__this, &m3810_MI);
		bool L_2 = (bool)VirtFuncInvoker0< bool >::Invoke(&m3817_MI, L_1);
		return L_2;
	}

IL_0017:
	{
		t875 * L_3 = m3801(__this, &m3801_MI);
		if (!L_3)
		{
			goto IL_0034;
		}
	}
	{
		t875 * L_4 = m3801(__this, &m3801_MI);
		bool L_5 = (bool)VirtFuncInvoker0< bool >::Invoke(&m4273_MI, L_4);
		if (!L_5)
		{
			goto IL_0034;
		}
	}
	{
		return 1;
	}

IL_0034:
	{
		t875 * L_6 = m3803(__this, &m3803_MI);
		if (!L_6)
		{
			goto IL_0051;
		}
	}
	{
		t875 * L_7 = m3803(__this, &m3803_MI);
		bool L_8 = (bool)VirtFuncInvoker0< bool >::Invoke(&m4273_MI, L_7);
		if (!L_8)
		{
			goto IL_0051;
		}
	}
	{
		return 1;
	}

IL_0051:
	{
		int32_t L_9 = m3764(__this, &m3764_MI);
		return ((((int32_t)((((int32_t)L_9) > ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
 t876 * m3810 (t885 * __this, MethodInfo* method){
	{
		t876 * L_0 = (__this->f1);
		if (L_0)
		{
			goto IL_0049;
		}
	}
	{
		t876 * L_1 = (t876 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t876_TI));
		m3811(L_1, &m3811_MI);
		__this->f1 = L_1;
		t876 * L_2 = (__this->f1);
		t875 * L_3 = m3801(__this, &m3801_MI);
		m3802(L_2, L_3, &m3802_MI);
		t876 * L_4 = (__this->f1);
		t875 * L_5 = m3803(__this, &m3803_MI);
		m3804(L_4, L_5, &m3804_MI);
		t876 * L_6 = (__this->f1);
		t886 * L_7 = (__this->f3);
		m3815(L_6, L_7, &m3815_MI);
	}

IL_0049:
	{
		t876 * L_8 = (__this->f1);
		return L_8;
	}
}
// Metadata Definition System.Text.RegularExpressions.Syntax.CaptureAssertion
extern Il2CppType t876_0_0_1;
FieldInfo t885_f1_FieldInfo = 
{
	"alternate", &t876_0_0_1, &t885_TI, offsetof(t885, f1), &EmptyCustomAttributesCache};
extern Il2CppType t881_0_0_1;
FieldInfo t885_f2_FieldInfo = 
{
	"group", &t881_0_0_1, &t885_TI, offsetof(t885, f2), &EmptyCustomAttributesCache};
extern Il2CppType t886_0_0_1;
FieldInfo t885_f3_FieldInfo = 
{
	"literal", &t886_0_0_1, &t885_TI, offsetof(t885, f3), &EmptyCustomAttributesCache};
static FieldInfo* t885_FIs[] =
{
	&t885_f1_FieldInfo,
	&t885_f2_FieldInfo,
	&t885_f3_FieldInfo,
	NULL
};
static PropertyInfo t885____CapturingGroup_PropertyInfo = 
{
	&t885_TI, "CapturingGroup", NULL, &m3807_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t885____Alternate_PropertyInfo = 
{
	&t885_TI, "Alternate", &m3810_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t885_PIs[] =
{
	&t885____CapturingGroup_PropertyInfo,
	&t885____Alternate_PropertyInfo,
	NULL
};
extern Il2CppType t886_0_0_0;
extern Il2CppType t886_0_0_0;
static ParameterInfo t885_m3806_ParameterInfos[] = 
{
	{"l", 0, 134218482, &EmptyCustomAttributesCache, &t886_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3806_MI = 
{
	".ctor", (methodPointerType)&m3806, &t885_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t885_m3806_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 803, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t881_0_0_0;
static ParameterInfo t885_m3807_ParameterInfos[] = 
{
	{"value", 0, 134218483, &EmptyCustomAttributesCache, &t881_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3807_MI = 
{
	"set_CapturingGroup", (methodPointerType)&m3807, &t885_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t885_m3807_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 804, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t878_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t885_m3808_ParameterInfos[] = 
{
	{"cmp", 0, 134218484, &EmptyCustomAttributesCache, &t878_0_0_0},
	{"reverse", 1, 134218485, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3808_MI = 
{
	"Compile", (methodPointerType)&m3808, &t885_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t297, t885_m3808_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 2, false, false, 805, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m3809_MI = 
{
	"IsComplex", (methodPointerType)&m3809, &t885_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 198, 0, 7, 0, false, false, 806, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t876_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3810_MI = 
{
	"get_Alternate", (methodPointerType)&m3810, &t885_TI, &t876_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2177, 0, 255, 0, false, false, 807, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t885_MIs[] =
{
	&m3806_MI,
	&m3807_MI,
	&m3808_MI,
	&m3809_MI,
	&m3810_MI,
	NULL
};
static MethodInfo* t885_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m3808_MI,
	&m3805_MI,
	&m3765_MI,
	&m3809_MI,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t885_0_0_0;
extern Il2CppType t885_1_0_0;
struct t885;
TypeInfo t885_TI = 
{
	&g_System_dll_Image, NULL, "CaptureAssertion", "System.Text.RegularExpressions.Syntax", t885_MIs, t885_PIs, t885_FIs, NULL, &t874_TI, NULL, NULL, &t885_TI, NULL, t885_VT, &EmptyCustomAttributesCache, &t885_TI, &t885_0_0_0, &t885_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t885), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 5, 2, 3, 0, 0, 8, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m4241_MI;
extern MethodInfo m3814_MI;
extern MethodInfo m4225_MI;


 void m3811 (t876 * __this, MethodInfo* method){
	{
		m3800(__this, &m3800_MI);
		t877 * L_0 = m3767(__this, &m3767_MI);
		m3759(L_0, (t875 *)NULL, &m3759_MI);
		return;
	}
}
 void m3812 (t876 * __this, bool p0, MethodInfo* method){
	{
		__this->f1 = p0;
		return;
	}
}
 void m3813 (t876 * __this, bool p0, MethodInfo* method){
	{
		__this->f2 = p0;
		return;
	}
}
 t875 * m3814 (t876 * __this, MethodInfo* method){
	{
		t877 * L_0 = m3767(__this, &m3767_MI);
		t875 * L_1 = m3760(L_0, 2, &m3760_MI);
		return L_1;
	}
}
 void m3815 (t876 * __this, t875 * p0, MethodInfo* method){
	{
		t877 * L_0 = m3767(__this, &m3767_MI);
		m3761(L_0, 2, p0, &m3761_MI);
		return;
	}
}
 void m3816 (t876 * __this, t29 * p0, bool p1, MethodInfo* method){
	t852 * V_0 = {0};
	t852 * V_1 = {0};
	t852 * V_2 = {0};
	{
		t852 * L_0 = (t852 *)InterfaceFuncInvoker0< t852 * >::Invoke(&m4252_MI, p0);
		V_0 = L_0;
		t852 * L_1 = (t852 *)InterfaceFuncInvoker0< t852 * >::Invoke(&m4252_MI, p0);
		V_1 = L_1;
		bool L_2 = (__this->f2);
		if (L_2)
		{
			goto IL_0026;
		}
	}
	{
		InterfaceActionInvoker2< t852 *, t852 * >::Invoke(&m4241_MI, p0, V_0, V_1);
		goto IL_002e;
	}

IL_0026:
	{
		InterfaceActionInvoker2< t852 *, t852 * >::Invoke(&m4241_MI, p0, V_1, V_0);
	}

IL_002e:
	{
		t875 * L_3 = m3814(__this, &m3814_MI);
		bool L_4 = (__this->f1);
		VirtActionInvoker2< t29 *, bool >::Invoke(&m4272_MI, L_3, p0, L_4);
		InterfaceActionInvoker0::Invoke(&m4226_MI, p0);
		t875 * L_5 = m3801(__this, &m3801_MI);
		if (L_5)
		{
			goto IL_006a;
		}
	}
	{
		InterfaceActionInvoker1< t852 * >::Invoke(&m4253_MI, p0, V_1);
		InterfaceActionInvoker0::Invoke(&m4225_MI, p0);
		InterfaceActionInvoker1< t852 * >::Invoke(&m4253_MI, p0, V_0);
		goto IL_00be;
	}

IL_006a:
	{
		InterfaceActionInvoker1< t852 * >::Invoke(&m4253_MI, p0, V_0);
		t875 * L_6 = m3801(__this, &m3801_MI);
		VirtActionInvoker2< t29 *, bool >::Invoke(&m4272_MI, L_6, p0, p1);
		t875 * L_7 = m3803(__this, &m3803_MI);
		if (L_7)
		{
			goto IL_0095;
		}
	}
	{
		InterfaceActionInvoker1< t852 * >::Invoke(&m4253_MI, p0, V_1);
		goto IL_00be;
	}

IL_0095:
	{
		t852 * L_8 = (t852 *)InterfaceFuncInvoker0< t852 * >::Invoke(&m4252_MI, p0);
		V_2 = L_8;
		InterfaceActionInvoker1< t852 * >::Invoke(&m4243_MI, p0, V_2);
		InterfaceActionInvoker1< t852 * >::Invoke(&m4253_MI, p0, V_1);
		t875 * L_9 = m3803(__this, &m3803_MI);
		VirtActionInvoker2< t29 *, bool >::Invoke(&m4272_MI, L_9, p0, p1);
		InterfaceActionInvoker1< t852 * >::Invoke(&m4253_MI, p0, V_2);
	}

IL_00be:
	{
		return;
	}
}
 bool m3817 (t876 * __this, MethodInfo* method){
	{
		return 1;
	}
}
// Metadata Definition System.Text.RegularExpressions.Syntax.ExpressionAssertion
extern Il2CppType t40_0_0_1;
FieldInfo t876_f1_FieldInfo = 
{
	"reverse", &t40_0_0_1, &t876_TI, offsetof(t876, f1), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t876_f2_FieldInfo = 
{
	"negate", &t40_0_0_1, &t876_TI, offsetof(t876, f2), &EmptyCustomAttributesCache};
static FieldInfo* t876_FIs[] =
{
	&t876_f1_FieldInfo,
	&t876_f2_FieldInfo,
	NULL
};
static PropertyInfo t876____Reverse_PropertyInfo = 
{
	&t876_TI, "Reverse", NULL, &m3812_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t876____Negate_PropertyInfo = 
{
	&t876_TI, "Negate", NULL, &m3813_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t876____TestExpression_PropertyInfo = 
{
	&t876_TI, "TestExpression", &m3814_MI, &m3815_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t876_PIs[] =
{
	&t876____Reverse_PropertyInfo,
	&t876____Negate_PropertyInfo,
	&t876____TestExpression_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3811_MI = 
{
	".ctor", (methodPointerType)&m3811, &t876_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 808, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t876_m3812_ParameterInfos[] = 
{
	{"value", 0, 134218486, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3812_MI = 
{
	"set_Reverse", (methodPointerType)&m3812, &t876_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t876_m3812_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 809, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t876_m3813_ParameterInfos[] = 
{
	{"value", 0, 134218487, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3813_MI = 
{
	"set_Negate", (methodPointerType)&m3813, &t876_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t876_m3813_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 810, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t875_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3814_MI = 
{
	"get_TestExpression", (methodPointerType)&m3814, &t876_TI, &t875_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 811, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t875_0_0_0;
static ParameterInfo t876_m3815_ParameterInfos[] = 
{
	{"value", 0, 134218488, &EmptyCustomAttributesCache, &t875_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3815_MI = 
{
	"set_TestExpression", (methodPointerType)&m3815, &t876_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t876_m3815_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 812, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t878_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t876_m3816_ParameterInfos[] = 
{
	{"cmp", 0, 134218489, &EmptyCustomAttributesCache, &t878_0_0_0},
	{"reverse", 1, 134218490, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3816_MI = 
{
	"Compile", (methodPointerType)&m3816, &t876_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t297, t876_m3816_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 2, false, false, 813, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m3817_MI = 
{
	"IsComplex", (methodPointerType)&m3817, &t876_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 198, 0, 7, 0, false, false, 814, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t876_MIs[] =
{
	&m3811_MI,
	&m3812_MI,
	&m3813_MI,
	&m3814_MI,
	&m3815_MI,
	&m3816_MI,
	&m3817_MI,
	NULL
};
static MethodInfo* t876_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m3816_MI,
	&m3805_MI,
	&m3765_MI,
	&m3817_MI,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t876_1_0_0;
struct t876;
TypeInfo t876_TI = 
{
	&g_System_dll_Image, NULL, "ExpressionAssertion", "System.Text.RegularExpressions.Syntax", t876_MIs, t876_PIs, t876_FIs, NULL, &t874_TI, NULL, NULL, &t876_TI, NULL, t876_VT, &EmptyCustomAttributesCache, &t876_TI, &t876_0_0_0, &t876_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t876), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 7, 3, 2, 0, 0, 8, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m3819_MI;
extern MethodInfo m4242_MI;
extern MethodInfo m4250_MI;
extern MethodInfo m4251_MI;


 void m3818 (t887 * __this, MethodInfo* method){
	{
		m3766(__this, &m3766_MI);
		return;
	}
}
 t877 * m3819 (t887 * __this, MethodInfo* method){
	{
		t877 * L_0 = m3767(__this, &m3767_MI);
		return L_0;
	}
}
 void m3820 (t887 * __this, t875 * p0, MethodInfo* method){
	{
		t877 * L_0 = m3819(__this, &m3819_MI);
		m3759(L_0, p0, &m3759_MI);
		return;
	}
}
extern MethodInfo m3821_MI;
 void m3821 (t887 * __this, t29 * p0, bool p1, MethodInfo* method){
	t852 * V_0 = {0};
	t875 * V_1 = {0};
	t29 * V_2 = {0};
	t852 * V_3 = {0};
	t29 * V_4 = {0};
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		t852 * L_0 = (t852 *)InterfaceFuncInvoker0< t852 * >::Invoke(&m4252_MI, p0);
		V_0 = L_0;
		t877 * L_1 = m3819(__this, &m3819_MI);
		t29 * L_2 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m4130_MI, L_1);
		V_2 = L_2;
	}

IL_0013:
	try
	{ // begin try (depth: 1)
		{
			goto IL_004e;
		}

IL_0018:
		{
			t29 * L_3 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m3970_MI, V_2);
			V_1 = ((t875 *)Castclass(L_3, InitializedTypeInfo(&t875_TI)));
			t852 * L_4 = (t852 *)InterfaceFuncInvoker0< t852 * >::Invoke(&m4252_MI, p0);
			V_3 = L_4;
			InterfaceActionInvoker1< t852 * >::Invoke(&m4242_MI, p0, V_3);
			VirtActionInvoker2< t29 *, bool >::Invoke(&m4272_MI, V_1, p0, p1);
			InterfaceActionInvoker1< t852 * >::Invoke(&m4243_MI, p0, V_0);
			InterfaceActionInvoker1< t852 * >::Invoke(&m4253_MI, p0, V_3);
			InterfaceActionInvoker0::Invoke(&m4250_MI, p0);
		}

IL_004e:
		{
			bool L_5 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m3972_MI, V_2);
			if (L_5)
			{
				goto IL_0018;
			}
		}

IL_0059:
		{
			// IL_0059: leave IL_0073
			leaveInstructions[0] = 0x73; // 1
			THROW_SENTINEL(IL_0073);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_005e;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_005e;
	}

IL_005e:
	{ // begin finally (depth: 1)
		{
			V_4 = ((t29 *)IsInst(V_2, InitializedTypeInfo(&t324_TI)));
			if (V_4)
			{
				goto IL_006b;
			}
		}

IL_006a:
		{
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x73:
					goto IL_0073;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}

IL_006b:
		{
			InterfaceActionInvoker0::Invoke(&m1428_MI, V_4);
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x73:
					goto IL_0073;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}
	} // end finally (depth: 1)

IL_0073:
	{
		InterfaceActionInvoker0::Invoke(&m4225_MI, p0);
		InterfaceActionInvoker1< t852 * >::Invoke(&m4253_MI, p0, V_0);
		InterfaceActionInvoker0::Invoke(&m4251_MI, p0);
		return;
	}
}
extern MethodInfo m3822_MI;
 void m3822 (t887 * __this, int32_t* p0, int32_t* p1, MethodInfo* method){
	{
		t877 * L_0 = m3819(__this, &m3819_MI);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m4096_MI, L_0);
		m3768(__this, p0, p1, L_1, &m3768_MI);
		return;
	}
}
// Metadata Definition System.Text.RegularExpressions.Syntax.Alternation
static PropertyInfo t887____Alternatives_PropertyInfo = 
{
	&t887_TI, "Alternatives", &m3819_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t887_PIs[] =
{
	&t887____Alternatives_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3818_MI = 
{
	".ctor", (methodPointerType)&m3818, &t887_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 815, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t877_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3819_MI = 
{
	"get_Alternatives", (methodPointerType)&m3819, &t887_TI, &t877_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 816, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t875_0_0_0;
static ParameterInfo t887_m3820_ParameterInfos[] = 
{
	{"e", 0, 134218491, &EmptyCustomAttributesCache, &t875_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3820_MI = 
{
	"AddAlternative", (methodPointerType)&m3820, &t887_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t887_m3820_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 817, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t878_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t887_m3821_ParameterInfos[] = 
{
	{"cmp", 0, 134218492, &EmptyCustomAttributesCache, &t878_0_0_0},
	{"reverse", 1, 134218493, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3821_MI = 
{
	"Compile", (methodPointerType)&m3821, &t887_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t297, t887_m3821_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 2, false, false, 818, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_1_0_2;
extern Il2CppType t44_1_0_2;
static ParameterInfo t887_m3822_ParameterInfos[] = 
{
	{"min", 0, 134218494, &EmptyCustomAttributesCache, &t44_1_0_2},
	{"max", 1, 134218495, &EmptyCustomAttributesCache, &t44_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t390_t390 (MethodInfo* method, void* obj, void** args);
MethodInfo m3822_MI = 
{
	"GetWidth", (methodPointerType)&m3822, &t887_TI, &t21_0_0_0, RuntimeInvoker_t21_t390_t390, t887_m3822_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, false, 819, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t887_MIs[] =
{
	&m3818_MI,
	&m3819_MI,
	&m3820_MI,
	&m3821_MI,
	&m3822_MI,
	NULL
};
static MethodInfo* t887_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m3821_MI,
	&m3822_MI,
	&m3765_MI,
	&m3769_MI,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t887_0_0_0;
extern Il2CppType t887_1_0_0;
struct t887;
TypeInfo t887_TI = 
{
	&g_System_dll_Image, NULL, "Alternation", "System.Text.RegularExpressions.Syntax", t887_MIs, t887_PIs, NULL, NULL, &t880_TI, NULL, NULL, &t887_TI, NULL, t887_VT, &EmptyCustomAttributesCache, &t887_TI, &t887_0_0_0, &t887_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t887), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 8, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m4227_MI;
extern MethodInfo m3824_MI;


 void m3823 (t886 * __this, t7* p0, bool p1, MethodInfo* method){
	{
		m3763(__this, &m3763_MI);
		__this->f0 = p0;
		__this->f1 = p1;
		return;
	}
}
 void m3824 (t29 * __this, t7* p0, t29 * p1, bool p2, bool p3, MethodInfo* method){
	{
		int32_t L_0 = m1715(p0, &m1715_MI);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		int32_t L_1 = m1715(p0, &m1715_MI);
		if ((((uint32_t)L_1) != ((uint32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		uint16_t L_2 = m1741(p0, 0, &m1741_MI);
		InterfaceActionInvoker4< uint16_t, bool, bool, bool >::Invoke(&m4227_MI, p1, L_2, 0, p2, p3);
		goto IL_0036;
	}

IL_002d:
	{
		InterfaceActionInvoker3< t7*, bool, bool >::Invoke(&m4232_MI, p1, p0, p2, p3);
	}

IL_0036:
	{
		return;
	}
}
extern MethodInfo m3825_MI;
 void m3825 (t886 * __this, t29 * p0, bool p1, MethodInfo* method){
	{
		t7* L_0 = (__this->f0);
		bool L_1 = (__this->f1);
		m3824(NULL, L_0, p0, L_1, p1, &m3824_MI);
		return;
	}
}
extern MethodInfo m3826_MI;
 void m3826 (t886 * __this, int32_t* p0, int32_t* p1, MethodInfo* method){
	int32_t V_0 = 0;
	{
		t7* L_0 = (__this->f0);
		int32_t L_1 = m1715(L_0, &m1715_MI);
		int32_t L_2 = L_1;
		V_0 = L_2;
		*((int32_t*)(p1)) = (int32_t)L_2;
		*((int32_t*)(p0)) = (int32_t)V_0;
		return;
	}
}
extern MethodInfo m3827_MI;
 t879 * m3827 (t886 * __this, bool p0, MethodInfo* method){
	{
		t7* L_0 = (__this->f0);
		int32_t L_1 = m1715(L_0, &m1715_MI);
		t7* L_2 = (__this->f0);
		bool L_3 = (__this->f1);
		t879 * L_4 = (t879 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t879_TI));
		m3855(L_4, __this, 0, L_1, L_2, L_3, &m3855_MI);
		return L_4;
	}
}
extern MethodInfo m3828_MI;
 bool m3828 (t886 * __this, MethodInfo* method){
	{
		return 0;
	}
}
// Metadata Definition System.Text.RegularExpressions.Syntax.Literal
extern Il2CppType t7_0_0_1;
FieldInfo t886_f0_FieldInfo = 
{
	"str", &t7_0_0_1, &t886_TI, offsetof(t886, f0), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t886_f1_FieldInfo = 
{
	"ignore", &t40_0_0_1, &t886_TI, offsetof(t886, f1), &EmptyCustomAttributesCache};
static FieldInfo* t886_FIs[] =
{
	&t886_f0_FieldInfo,
	&t886_f1_FieldInfo,
	NULL
};
extern Il2CppType t7_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t886_m3823_ParameterInfos[] = 
{
	{"str", 0, 134218496, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"ignore", 1, 134218497, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3823_MI = 
{
	".ctor", (methodPointerType)&m3823, &t886_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t297, t886_m3823_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 820, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t878_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t886_m3824_ParameterInfos[] = 
{
	{"str", 0, 134218498, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"cmp", 1, 134218499, &EmptyCustomAttributesCache, &t878_0_0_0},
	{"ignore", 2, 134218500, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"reverse", 3, 134218501, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t297_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3824_MI = 
{
	"CompileLiteral", (methodPointerType)&m3824, &t886_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t297_t297, t886_m3824_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 4, false, false, 821, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t878_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t886_m3825_ParameterInfos[] = 
{
	{"cmp", 0, 134218502, &EmptyCustomAttributesCache, &t878_0_0_0},
	{"reverse", 1, 134218503, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3825_MI = 
{
	"Compile", (methodPointerType)&m3825, &t886_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t297, t886_m3825_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 2, false, false, 822, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_1_0_2;
extern Il2CppType t44_1_0_2;
static ParameterInfo t886_m3826_ParameterInfos[] = 
{
	{"min", 0, 134218504, &EmptyCustomAttributesCache, &t44_1_0_2},
	{"max", 1, 134218505, &EmptyCustomAttributesCache, &t44_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t390_t390 (MethodInfo* method, void* obj, void** args);
MethodInfo m3826_MI = 
{
	"GetWidth", (methodPointerType)&m3826, &t886_TI, &t21_0_0_0, RuntimeInvoker_t21_t390_t390, t886_m3826_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, false, 823, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t886_m3827_ParameterInfos[] = 
{
	{"reverse", 0, 134218506, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t879_0_0_0;
extern void* RuntimeInvoker_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3827_MI = 
{
	"GetAnchorInfo", (methodPointerType)&m3827, &t886_TI, &t879_0_0_0, RuntimeInvoker_t29_t297, t886_m3827_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 1, false, false, 824, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m3828_MI = 
{
	"IsComplex", (methodPointerType)&m3828, &t886_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 198, 0, 7, 0, false, false, 825, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t886_MIs[] =
{
	&m3823_MI,
	&m3824_MI,
	&m3825_MI,
	&m3826_MI,
	&m3827_MI,
	&m3828_MI,
	NULL
};
static MethodInfo* t886_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m3825_MI,
	&m3826_MI,
	&m3827_MI,
	&m3828_MI,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t886_1_0_0;
struct t886;
TypeInfo t886_TI = 
{
	&g_System_dll_Image, NULL, "Literal", "System.Text.RegularExpressions.Syntax", t886_MIs, NULL, t886_FIs, NULL, &t875_TI, NULL, NULL, &t886_TI, NULL, t886_VT, &EmptyCustomAttributesCache, &t886_TI, &t886_0_0_0, &t886_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t886), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 6, 0, 2, 0, 0, 8, 0, 0};
#ifndef _MSC_VER
#else
#endif



 void m3829 (t888 * __this, uint16_t p0, MethodInfo* method){
	{
		m3763(__this, &m3763_MI);
		__this->f0 = p0;
		return;
	}
}
extern MethodInfo m3830_MI;
 void m3830 (t888 * __this, t29 * p0, bool p1, MethodInfo* method){
	{
		uint16_t L_0 = (__this->f0);
		InterfaceActionInvoker1< uint16_t >::Invoke(&m4233_MI, p0, L_0);
		return;
	}
}
extern MethodInfo m3831_MI;
 void m3831 (t888 * __this, int32_t* p0, int32_t* p1, MethodInfo* method){
	int32_t V_0 = 0;
	{
		int32_t L_0 = 0;
		V_0 = L_0;
		*((int32_t*)(p1)) = (int32_t)L_0;
		*((int32_t*)(p0)) = (int32_t)V_0;
		return;
	}
}
extern MethodInfo m3832_MI;
 bool m3832 (t888 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m3833_MI;
 t879 * m3833 (t888 * __this, bool p0, MethodInfo* method){
	uint16_t V_0 = {0};
	{
		uint16_t L_0 = (__this->f0);
		V_0 = L_0;
		if (((uint16_t)(V_0-2)) == 0)
		{
			goto IL_0020;
		}
		if (((uint16_t)(V_0-2)) == 1)
		{
			goto IL_0020;
		}
		if (((uint16_t)(V_0-2)) == 2)
		{
			goto IL_0020;
		}
	}
	{
		goto IL_002f;
	}

IL_0020:
	{
		uint16_t L_1 = (__this->f0);
		t879 * L_2 = (t879 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t879_TI));
		m3856(L_2, __this, 0, 0, L_1, &m3856_MI);
		return L_2;
	}

IL_002f:
	{
		t879 * L_3 = (t879 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t879_TI));
		m3854(L_3, __this, 0, &m3854_MI);
		return L_3;
	}
}
// Metadata Definition System.Text.RegularExpressions.Syntax.PositionAssertion
extern Il2CppType t845_0_0_1;
FieldInfo t888_f0_FieldInfo = 
{
	"pos", &t845_0_0_1, &t888_TI, offsetof(t888, f0), &EmptyCustomAttributesCache};
static FieldInfo* t888_FIs[] =
{
	&t888_f0_FieldInfo,
	NULL
};
extern Il2CppType t845_0_0_0;
static ParameterInfo t888_m3829_ParameterInfos[] = 
{
	{"pos", 0, 134218507, &EmptyCustomAttributesCache, &t845_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t626 (MethodInfo* method, void* obj, void** args);
MethodInfo m3829_MI = 
{
	".ctor", (methodPointerType)&m3829, &t888_TI, &t21_0_0_0, RuntimeInvoker_t21_t626, t888_m3829_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 826, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t878_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t888_m3830_ParameterInfos[] = 
{
	{"cmp", 0, 134218508, &EmptyCustomAttributesCache, &t878_0_0_0},
	{"reverse", 1, 134218509, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3830_MI = 
{
	"Compile", (methodPointerType)&m3830, &t888_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t297, t888_m3830_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 2, false, false, 827, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_1_0_2;
extern Il2CppType t44_1_0_2;
static ParameterInfo t888_m3831_ParameterInfos[] = 
{
	{"min", 0, 134218510, &EmptyCustomAttributesCache, &t44_1_0_2},
	{"max", 1, 134218511, &EmptyCustomAttributesCache, &t44_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t390_t390 (MethodInfo* method, void* obj, void** args);
MethodInfo m3831_MI = 
{
	"GetWidth", (methodPointerType)&m3831, &t888_TI, &t21_0_0_0, RuntimeInvoker_t21_t390_t390, t888_m3831_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, false, 828, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m3832_MI = 
{
	"IsComplex", (methodPointerType)&m3832, &t888_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 198, 0, 7, 0, false, false, 829, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t888_m3833_ParameterInfos[] = 
{
	{"revers", 0, 134218512, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t879_0_0_0;
extern void* RuntimeInvoker_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3833_MI = 
{
	"GetAnchorInfo", (methodPointerType)&m3833, &t888_TI, &t879_0_0_0, RuntimeInvoker_t29_t297, t888_m3833_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 1, false, false, 830, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t888_MIs[] =
{
	&m3829_MI,
	&m3830_MI,
	&m3831_MI,
	&m3832_MI,
	&m3833_MI,
	NULL
};
static MethodInfo* t888_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m3830_MI,
	&m3831_MI,
	&m3833_MI,
	&m3832_MI,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t888_0_0_0;
extern Il2CppType t888_1_0_0;
struct t888;
TypeInfo t888_TI = 
{
	&g_System_dll_Image, NULL, "PositionAssertion", "System.Text.RegularExpressions.Syntax", t888_MIs, NULL, t888_FIs, NULL, &t875_TI, NULL, NULL, &t888_TI, NULL, t888_VT, &EmptyCustomAttributesCache, &t888_TI, &t888_0_0_0, &t888_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t888), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 5, 0, 1, 0, 0, 8, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m4238_MI;


 void m3834 (t889 * __this, bool p0, MethodInfo* method){
	{
		m3763(__this, &m3763_MI);
		__this->f1 = p0;
		return;
	}
}
extern MethodInfo m3835_MI;
 t881 * m3835 (t889 * __this, MethodInfo* method){
	{
		t881 * L_0 = (__this->f0);
		return L_0;
	}
}
 void m3836 (t889 * __this, t881 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		return;
	}
}
extern MethodInfo m3837_MI;
 bool m3837 (t889 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->f1);
		return L_0;
	}
}
extern MethodInfo m3838_MI;
 void m3838 (t889 * __this, t29 * p0, bool p1, MethodInfo* method){
	{
		t881 * L_0 = (__this->f0);
		int32_t L_1 = m3779(L_0, &m3779_MI);
		bool L_2 = (__this->f1);
		InterfaceActionInvoker3< int32_t, bool, bool >::Invoke(&m4238_MI, p0, L_1, L_2, p1);
		return;
	}
}
extern MethodInfo m3839_MI;
 void m3839 (t889 * __this, int32_t* p0, int32_t* p1, MethodInfo* method){
	{
		*((int32_t*)(p0)) = (int32_t)0;
		*((int32_t*)(p1)) = (int32_t)((int32_t)2147483647);
		return;
	}
}
extern MethodInfo m3840_MI;
 bool m3840 (t889 * __this, MethodInfo* method){
	{
		return 1;
	}
}
// Metadata Definition System.Text.RegularExpressions.Syntax.Reference
extern Il2CppType t881_0_0_1;
FieldInfo t889_f0_FieldInfo = 
{
	"group", &t881_0_0_1, &t889_TI, offsetof(t889, f0), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t889_f1_FieldInfo = 
{
	"ignore", &t40_0_0_1, &t889_TI, offsetof(t889, f1), &EmptyCustomAttributesCache};
static FieldInfo* t889_FIs[] =
{
	&t889_f0_FieldInfo,
	&t889_f1_FieldInfo,
	NULL
};
static PropertyInfo t889____CapturingGroup_PropertyInfo = 
{
	&t889_TI, "CapturingGroup", &m3835_MI, &m3836_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t889____IgnoreCase_PropertyInfo = 
{
	&t889_TI, "IgnoreCase", &m3837_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t889_PIs[] =
{
	&t889____CapturingGroup_PropertyInfo,
	&t889____IgnoreCase_PropertyInfo,
	NULL
};
extern Il2CppType t40_0_0_0;
static ParameterInfo t889_m3834_ParameterInfos[] = 
{
	{"ignore", 0, 134218513, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3834_MI = 
{
	".ctor", (methodPointerType)&m3834, &t889_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t889_m3834_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 831, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t881_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3835_MI = 
{
	"get_CapturingGroup", (methodPointerType)&m3835, &t889_TI, &t881_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 832, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t881_0_0_0;
static ParameterInfo t889_m3836_ParameterInfos[] = 
{
	{"value", 0, 134218514, &EmptyCustomAttributesCache, &t881_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3836_MI = 
{
	"set_CapturingGroup", (methodPointerType)&m3836, &t889_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t889_m3836_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 833, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m3837_MI = 
{
	"get_IgnoreCase", (methodPointerType)&m3837, &t889_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 834, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t878_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t889_m3838_ParameterInfos[] = 
{
	{"cmp", 0, 134218515, &EmptyCustomAttributesCache, &t878_0_0_0},
	{"reverse", 1, 134218516, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3838_MI = 
{
	"Compile", (methodPointerType)&m3838, &t889_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t297, t889_m3838_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 2, false, false, 835, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_1_0_2;
extern Il2CppType t44_1_0_2;
static ParameterInfo t889_m3839_ParameterInfos[] = 
{
	{"min", 0, 134218517, &EmptyCustomAttributesCache, &t44_1_0_2},
	{"max", 1, 134218518, &EmptyCustomAttributesCache, &t44_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t390_t390 (MethodInfo* method, void* obj, void** args);
MethodInfo m3839_MI = 
{
	"GetWidth", (methodPointerType)&m3839, &t889_TI, &t21_0_0_0, RuntimeInvoker_t21_t390_t390, t889_m3839_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, false, 836, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m3840_MI = 
{
	"IsComplex", (methodPointerType)&m3840, &t889_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 198, 0, 7, 0, false, false, 837, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t889_MIs[] =
{
	&m3834_MI,
	&m3835_MI,
	&m3836_MI,
	&m3837_MI,
	&m3838_MI,
	&m3839_MI,
	&m3840_MI,
	NULL
};
static MethodInfo* t889_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m3838_MI,
	&m3839_MI,
	&m3765_MI,
	&m3840_MI,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t889_0_0_0;
extern Il2CppType t889_1_0_0;
struct t889;
TypeInfo t889_TI = 
{
	&g_System_dll_Image, NULL, "Reference", "System.Text.RegularExpressions.Syntax", t889_MIs, t889_PIs, t889_FIs, NULL, &t875_TI, NULL, NULL, &t889_TI, NULL, t889_VT, &EmptyCustomAttributesCache, &t889_TI, &t889_0_0_0, &t889_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t889), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 7, 2, 2, 0, 0, 8, 0, 0};
#ifndef _MSC_VER
#else
#endif



 void m3841 (t890 * __this, bool p0, bool p1, MethodInfo* method){
	{
		m3834(__this, p0, &m3834_MI);
		__this->f3 = p1;
		return;
	}
}
 bool m3842 (t890 * __this, t7* p0, t719 * p1, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		bool L_0 = (__this->f3);
		if (!L_0)
		{
			goto IL_006c;
		}
	}
	{
		V_0 = 0;
		V_1 = 1;
		goto IL_002d;
	}

IL_0014:
	{
		t7* L_1 = m1742(p0, 0, V_1, &m1742_MI);
		t29 * L_2 = (t29 *)VirtFuncInvoker1< t29 *, t29 * >::Invoke(&m3989_MI, p1, L_1);
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		V_0 = V_1;
	}

IL_0029:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_002d:
	{
		int32_t L_3 = m1715(p0, &m1715_MI);
		if ((((int32_t)V_1) < ((int32_t)L_3)))
		{
			goto IL_0014;
		}
	}
	{
		if (!V_0)
		{
			goto IL_0067;
		}
	}
	{
		t7* L_4 = m1742(p0, 0, V_0, &m1742_MI);
		t29 * L_5 = (t29 *)VirtFuncInvoker1< t29 *, t29 * >::Invoke(&m3989_MI, p1, L_4);
		m3836(__this, ((t881 *)Castclass(L_5, InitializedTypeInfo(&t881_TI))), &m3836_MI);
		t7* L_6 = m1770(p0, V_0, &m1770_MI);
		__this->f2 = L_6;
		return 1;
	}

IL_0067:
	{
		goto IL_007a;
	}

IL_006c:
	{
		int32_t L_7 = m1715(p0, &m1715_MI);
		if ((((uint32_t)L_7) != ((uint32_t)1)))
		{
			goto IL_007a;
		}
	}
	{
		return 0;
	}

IL_007a:
	{
		V_2 = 0;
		int32_t L_8 = m3722(NULL, p0, (&V_2), &m3722_MI);
		V_3 = L_8;
		if ((((uint32_t)V_3) != ((uint32_t)(-1))))
		{
			goto IL_008e;
		}
	}
	{
		return 0;
	}

IL_008e:
	{
		if ((((int32_t)V_3) <= ((int32_t)((int32_t)255))))
		{
			goto IL_00ac;
		}
	}
	{
		bool L_9 = (__this->f3);
		if (!L_9)
		{
			goto IL_00ac;
		}
	}
	{
		V_3 = ((int32_t)((int32_t)V_3/(int32_t)8));
		V_2 = ((int32_t)(V_2-1));
	}

IL_00ac:
	{
		V_3 = ((int32_t)((int32_t)V_3&(int32_t)((int32_t)255)));
		uint16_t L_10 = (((uint16_t)V_3));
		t29 * L_11 = Box(InitializedTypeInfo(&t194_TI), &L_10);
		t7* L_12 = m1770(p0, V_2, &m1770_MI);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_13 = m1312(NULL, L_11, L_12, &m1312_MI);
		__this->f2 = L_13;
		return 1;
	}
}
extern MethodInfo m3843_MI;
 void m3843 (t890 * __this, t29 * p0, bool p1, MethodInfo* method){
	{
		t881 * L_0 = m3835(__this, &m3835_MI);
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		m3838(__this, p0, p1, &m3838_MI);
	}

IL_0013:
	{
		t7* L_1 = (__this->f2);
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		t7* L_2 = (__this->f2);
		bool L_3 = m3837(__this, &m3837_MI);
		m3824(NULL, L_2, p0, L_3, p1, &m3824_MI);
	}

IL_0031:
	{
		return;
	}
}
// Metadata Definition System.Text.RegularExpressions.Syntax.BackslashNumber
extern Il2CppType t7_0_0_1;
FieldInfo t890_f2_FieldInfo = 
{
	"literal", &t7_0_0_1, &t890_TI, offsetof(t890, f2), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t890_f3_FieldInfo = 
{
	"ecma", &t40_0_0_1, &t890_TI, offsetof(t890, f3), &EmptyCustomAttributesCache};
static FieldInfo* t890_FIs[] =
{
	&t890_f2_FieldInfo,
	&t890_f3_FieldInfo,
	NULL
};
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t890_m3841_ParameterInfos[] = 
{
	{"ignore", 0, 134218519, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"ecma", 1, 134218520, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3841_MI = 
{
	".ctor", (methodPointerType)&m3841, &t890_TI, &t21_0_0_0, RuntimeInvoker_t21_t297_t297, t890_m3841_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 838, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t719_0_0_0;
static ParameterInfo t890_m3842_ParameterInfos[] = 
{
	{"num_str", 0, 134218521, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"groups", 1, 134218522, &EmptyCustomAttributesCache, &t719_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3842_MI = 
{
	"ResolveReference", (methodPointerType)&m3842, &t890_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t890_m3842_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 2, false, false, 839, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t878_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t890_m3843_ParameterInfos[] = 
{
	{"cmp", 0, 134218523, &EmptyCustomAttributesCache, &t878_0_0_0},
	{"reverse", 1, 134218524, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3843_MI = 
{
	"Compile", (methodPointerType)&m3843, &t890_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t297, t890_m3843_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 2, false, false, 840, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t890_MIs[] =
{
	&m3841_MI,
	&m3842_MI,
	&m3843_MI,
	NULL
};
static MethodInfo* t890_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m3843_MI,
	&m3839_MI,
	&m3765_MI,
	&m3840_MI,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t890_0_0_0;
extern Il2CppType t890_1_0_0;
struct t890;
TypeInfo t890_TI = 
{
	&g_System_dll_Image, NULL, "BackslashNumber", "System.Text.RegularExpressions.Syntax", t890_MIs, NULL, t890_FIs, NULL, &t889_TI, NULL, NULL, &t890_TI, NULL, t890_VT, &EmptyCustomAttributesCache, &t890_TI, &t890_0_0_0, &t890_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t890), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 3, 0, 2, 0, 0, 8, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo t858_TI;
extern MethodInfo m4276_MI;
extern MethodInfo m4277_MI;
extern MethodInfo m3853_MI;
extern MethodInfo m4246_MI;
extern MethodInfo m4231_MI;
extern MethodInfo m4230_MI;
extern MethodInfo m4228_MI;
extern MethodInfo m4229_MI;


 void m3844 (t891 * __this, bool p0, bool p1, MethodInfo* method){
	int32_t V_0 = 0;
	{
		m3763(__this, &m3763_MI);
		__this->f1 = p0;
		__this->f2 = p1;
		t870 * L_0 = (t870 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t870_TI));
		m3709(L_0, &m3709_MI);
		__this->f5 = L_0;
		V_0 = ((int32_t)144);
		t858 * L_1 = (t858 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t858_TI));
		m4276(L_1, V_0, &m4276_MI);
		__this->f3 = L_1;
		t858 * L_2 = (t858 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t858_TI));
		m4276(L_2, V_0, &m4276_MI);
		__this->f4 = L_2;
		return;
	}
}
 void m3845 (t891 * __this, uint16_t p0, bool p1, MethodInfo* method){
	{
		m3844(__this, 0, 0, &m3844_MI);
		m3847(__this, p0, p1, &m3847_MI);
		return;
	}
}
extern MethodInfo m3846_MI;
 void m3846 (t29 * __this, MethodInfo* method){
	{
		t866  L_0 = {0};
		m3688(&L_0, ((int32_t)65), ((int32_t)90), &m3688_MI);
		((t891_SFs*)InitializedTypeInfo(&t891_TI)->static_fields)->f0 = L_0;
		return;
	}
}
 void m3847 (t891 * __this, uint16_t p0, bool p1, MethodInfo* method){
	int32_t V_0 = 0;
	{
		V_0 = p0;
		if (!p1)
		{
			goto IL_001a;
		}
	}
	{
		t858 * L_0 = (__this->f4);
		m4277(L_0, V_0, 1, &m4277_MI);
		goto IL_0027;
	}

IL_001a:
	{
		t858 * L_1 = (__this->f3);
		m4277(L_1, V_0, 1, &m4277_MI);
	}

IL_0027:
	{
		return;
	}
}
 void m3848 (t891 * __this, uint16_t p0, MethodInfo* method){
	{
		m3849(__this, p0, p0, &m3849_MI);
		return;
	}
}
 void m3849 (t891 * __this, uint16_t p0, uint16_t p1, MethodInfo* method){
	t866  V_0 = {0};
	t866  V_1 = {0};
	{
		m3688((&V_0), p0, p1, &m3688_MI);
		bool L_0 = (__this->f2);
		if (!L_0)
		{
			goto IL_00e2;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t891_TI));
		bool L_1 = m3698((&((t891_SFs*)InitializedTypeInfo(&t891_TI)->static_fields)->f0), V_0, &m3698_MI);
		if (!L_1)
		{
			goto IL_00b2;
		}
	}
	{
		int32_t L_2 = ((&V_0)->f0);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t891_TI));
		int32_t L_3 = ((&((t891_SFs*)InitializedTypeInfo(&t891_TI)->static_fields)->f0)->f0);
		if ((((int32_t)L_2) >= ((int32_t)L_3)))
		{
			goto IL_0070;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t891_TI));
		int32_t L_4 = ((&((t891_SFs*)InitializedTypeInfo(&t891_TI)->static_fields)->f0)->f0);
		int32_t L_5 = ((&V_0)->f1);
		m3688((&V_1), ((int32_t)(L_4+((int32_t)32))), ((int32_t)(L_5+((int32_t)32))), &m3688_MI);
		int32_t L_6 = ((&((t891_SFs*)InitializedTypeInfo(&t891_TI)->static_fields)->f0)->f0);
		(&V_0)->f1 = ((int32_t)(L_6-1));
		goto IL_00a1;
	}

IL_0070:
	{
		int32_t L_7 = ((&V_0)->f0);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t891_TI));
		int32_t L_8 = ((&((t891_SFs*)InitializedTypeInfo(&t891_TI)->static_fields)->f0)->f1);
		m3688((&V_1), ((int32_t)(L_7+((int32_t)32))), ((int32_t)(L_8+((int32_t)32))), &m3688_MI);
		int32_t L_9 = ((&((t891_SFs*)InitializedTypeInfo(&t891_TI)->static_fields)->f0)->f1);
		(&V_0)->f0 = ((int32_t)(L_9+1));
	}

IL_00a1:
	{
		t870 * L_10 = (__this->f5);
		m3711(L_10, V_1, &m3711_MI);
		goto IL_00e2;
	}

IL_00b2:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t891_TI));
		bool L_11 = m3696((&((t891_SFs*)InitializedTypeInfo(&t891_TI)->static_fields)->f0), V_0, &m3696_MI);
		if (!L_11)
		{
			goto IL_00e2;
		}
	}
	{
		t866 * L_12 = (&V_0);
		int32_t L_13 = (L_12->f1);
		L_12->f1 = ((int32_t)(L_13+((int32_t)32)));
		t866 * L_14 = (&V_0);
		int32_t L_15 = (L_14->f0);
		L_14->f0 = ((int32_t)(L_15+((int32_t)32)));
	}

IL_00e2:
	{
		t870 * L_16 = (__this->f5);
		m3711(L_16, V_0, &m3711_MI);
		return;
	}
}
extern MethodInfo m3850_MI;
 void m3850 (t891 * __this, t29 * p0, bool p1, MethodInfo* method){
	t870 * V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	t852 * V_3 = {0};
	t866  V_4 = {0};
	t29 * V_5 = {0};
	t858 * V_6 = {0};
	t866  V_7 = {0};
	t29 * V_8 = {0};
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	t29 * V_11 = {0};
	t29 * V_12 = {0};
	int32_t leaveInstructions[2] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		t870 * L_0 = (__this->f5);
		t35 L_1 = { &m3853_MI };
		t869 * L_2 = (t869 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t869_TI));
		m3705(L_2, NULL, L_1, &m3705_MI);
		t870 * L_3 = m3713(L_0, L_2, &m3713_MI);
		V_0 = L_3;
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m3715_MI, V_0);
		V_1 = L_4;
		V_2 = 0;
		goto IL_0050;
	}

IL_0026:
	{
		t858 * L_5 = (__this->f3);
		bool L_6 = m4256(L_5, V_2, &m4256_MI);
		if (L_6)
		{
			goto IL_0048;
		}
	}
	{
		t858 * L_7 = (__this->f4);
		bool L_8 = m4256(L_7, V_2, &m4256_MI);
		if (!L_8)
		{
			goto IL_004c;
		}
	}

IL_0048:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_004c:
	{
		V_2 = ((int32_t)(V_2+1));
	}

IL_0050:
	{
		t858 * L_9 = (__this->f3);
		int32_t L_10 = m4255(L_9, &m4255_MI);
		if ((((int32_t)V_2) < ((int32_t)L_10)))
		{
			goto IL_0026;
		}
	}
	{
		if (V_1)
		{
			goto IL_0068;
		}
	}
	{
		return;
	}

IL_0068:
	{
		t852 * L_11 = (t852 *)InterfaceFuncInvoker0< t852 * >::Invoke(&m4252_MI, p0);
		V_3 = L_11;
		if ((((int32_t)V_1) <= ((int32_t)1)))
		{
			goto IL_007d;
		}
	}
	{
		InterfaceActionInvoker1< t852 * >::Invoke(&m4246_MI, p0, V_3);
	}

IL_007d:
	{
		t29 * L_12 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m3719_MI, V_0);
		V_5 = L_12;
	}

IL_0085:
	try
	{ // begin try (depth: 1)
		{
			goto IL_01ac;
		}

IL_008a:
		{
			t29 * L_13 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m3970_MI, V_5);
			V_4 = ((*(t866 *)((t866 *)UnBox (L_13, InitializedTypeInfo(&t866_TI)))));
			bool L_14 = m3690((&V_4), &m3690_MI);
			if (!L_14)
			{
				goto IL_015d;
			}
		}

IL_00a4:
		{
			int32_t L_15 = m3693((&V_4), &m3693_MI);
			t858 * L_16 = (t858 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t858_TI));
			m4276(L_16, L_15, &m4276_MI);
			V_6 = L_16;
			t870 * L_17 = (__this->f5);
			t29 * L_18 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m3719_MI, L_17);
			V_8 = L_18;
		}

IL_00bf:
		try
		{ // begin try (depth: 2)
			{
				goto IL_0114;
			}

IL_00c4:
			{
				t29 * L_19 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m3970_MI, V_8);
				V_7 = ((*(t866 *)((t866 *)UnBox (L_19, InitializedTypeInfo(&t866_TI)))));
				bool L_20 = m3696((&V_4), V_7, &m3696_MI);
				if (!L_20)
				{
					goto IL_0114;
				}
			}

IL_00e0:
			{
				int32_t L_21 = ((&V_7)->f0);
				V_9 = L_21;
				goto IL_0106;
			}

IL_00ee:
			{
				int32_t L_22 = ((&V_4)->f0);
				m4277(V_6, ((int32_t)(V_9-L_22)), 1, &m4277_MI);
				V_9 = ((int32_t)(V_9+1));
			}

IL_0106:
			{
				int32_t L_23 = ((&V_7)->f1);
				if ((((int32_t)V_9) <= ((int32_t)L_23)))
				{
					goto IL_00ee;
				}
			}

IL_0114:
			{
				bool L_24 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m3972_MI, V_8);
				if (L_24)
				{
					goto IL_00c4;
				}
			}

IL_0120:
			{
				// IL_0120: leave IL_013b
				leaveInstructions[1] = 0x13B; // 2
				THROW_SENTINEL(IL_013b);
				// finally target depth: 2
			}
		} // end try (depth: 2)
		catch(Il2CppFinallySentinel& e)
		{
			goto IL_0125;
		}
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (t295 *)e.ex;
			goto IL_0125;
		}

IL_0125:
		{ // begin finally (depth: 2)
			{
				V_11 = ((t29 *)IsInst(V_8, InitializedTypeInfo(&t324_TI)));
				if (V_11)
				{
					goto IL_0133;
				}
			}

IL_0132:
			{
				// finally node depth: 2
				switch (leaveInstructions[1])
				{
					case 0x13B:
						goto IL_013b;
					default:
					{
						#if IL2CPP_DEBUG
						assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 2, __last_unhandled_exception has not been set");
						#endif
						t295 * _tmp_exception_local = __last_unhandled_exception;
						__last_unhandled_exception = 0;
						il2cpp_codegen_raise_exception(_tmp_exception_local);
					}
				}
			}

IL_0133:
			{
				InterfaceActionInvoker0::Invoke(&m1428_MI, V_11);
				// finally node depth: 2
				switch (leaveInstructions[1])
				{
					case 0x13B:
						goto IL_013b;
					default:
					{
						#if IL2CPP_DEBUG
						assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 2, __last_unhandled_exception has not been set");
						#endif
						t295 * _tmp_exception_local = __last_unhandled_exception;
						__last_unhandled_exception = 0;
						il2cpp_codegen_raise_exception(_tmp_exception_local);
					}
				}
			}
		} // end finally (depth: 2)

IL_013b:
		{
			int32_t L_25 = ((&V_4)->f0);
			bool L_26 = (__this->f1);
			bool L_27 = (__this->f2);
			InterfaceActionInvoker5< uint16_t, t858 *, bool, bool, bool >::Invoke(&m4231_MI, p0, (((uint16_t)L_25)), V_6, L_26, L_27, p1);
			goto IL_01ac;
		}

IL_015d:
		{
			bool L_28 = m3691((&V_4), &m3691_MI);
			if (!L_28)
			{
				goto IL_0189;
			}
		}

IL_0169:
		{
			int32_t L_29 = ((&V_4)->f0);
			bool L_30 = (__this->f1);
			bool L_31 = (__this->f2);
			InterfaceActionInvoker4< uint16_t, bool, bool, bool >::Invoke(&m4227_MI, p0, (((uint16_t)L_29)), L_30, L_31, p1);
			goto IL_01ac;
		}

IL_0189:
		{
			int32_t L_32 = ((&V_4)->f0);
			int32_t L_33 = ((&V_4)->f1);
			bool L_34 = (__this->f1);
			bool L_35 = (__this->f2);
			InterfaceActionInvoker5< uint16_t, uint16_t, bool, bool, bool >::Invoke(&m4230_MI, p0, (((uint16_t)L_32)), (((uint16_t)L_33)), L_34, L_35, p1);
		}

IL_01ac:
		{
			bool L_36 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m3972_MI, V_5);
			if (L_36)
			{
				goto IL_008a;
			}
		}

IL_01b8:
		{
			// IL_01b8: leave IL_01d3
			leaveInstructions[0] = 0x1D3; // 1
			THROW_SENTINEL(IL_01d3);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_01bd;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_01bd;
	}

IL_01bd:
	{ // begin finally (depth: 1)
		{
			V_12 = ((t29 *)IsInst(V_5, InitializedTypeInfo(&t324_TI)));
			if (V_12)
			{
				goto IL_01cb;
			}
		}

IL_01ca:
		{
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x1D3:
					goto IL_01d3;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}

IL_01cb:
		{
			InterfaceActionInvoker0::Invoke(&m1428_MI, V_12);
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x1D3:
					goto IL_01d3;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}
	} // end finally (depth: 1)

IL_01d3:
	{
		V_10 = 0;
		goto IL_024f;
	}

IL_01db:
	{
		t858 * L_37 = (__this->f3);
		bool L_38 = m4256(L_37, V_10, &m4256_MI);
		if (!L_38)
		{
			goto IL_0227;
		}
	}
	{
		t858 * L_39 = (__this->f4);
		bool L_40 = m4256(L_39, V_10, &m4256_MI);
		if (!L_40)
		{
			goto IL_0212;
		}
	}
	{
		bool L_41 = (__this->f1);
		InterfaceActionInvoker3< uint16_t, bool, bool >::Invoke(&m4228_MI, p0, 2, L_41, p1);
		goto IL_0222;
	}

IL_0212:
	{
		bool L_42 = (__this->f1);
		InterfaceActionInvoker3< uint16_t, bool, bool >::Invoke(&m4228_MI, p0, (((uint16_t)V_10)), L_42, p1);
	}

IL_0222:
	{
		goto IL_0249;
	}

IL_0227:
	{
		t858 * L_43 = (__this->f4);
		bool L_44 = m4256(L_43, V_10, &m4256_MI);
		if (!L_44)
		{
			goto IL_0249;
		}
	}
	{
		bool L_45 = (__this->f1);
		InterfaceActionInvoker3< uint16_t, bool, bool >::Invoke(&m4229_MI, p0, (((uint16_t)V_10)), L_45, p1);
	}

IL_0249:
	{
		V_10 = ((int32_t)(V_10+1));
	}

IL_024f:
	{
		t858 * L_46 = (__this->f3);
		int32_t L_47 = m4255(L_46, &m4255_MI);
		if ((((int32_t)V_10) < ((int32_t)L_47)))
		{
			goto IL_01db;
		}
	}
	{
		if ((((int32_t)V_1) <= ((int32_t)1)))
		{
			goto IL_028b;
		}
	}
	{
		bool L_48 = (__this->f1);
		if (!L_48)
		{
			goto IL_027e;
		}
	}
	{
		InterfaceActionInvoker0::Invoke(&m4226_MI, p0);
		goto IL_0284;
	}

IL_027e:
	{
		InterfaceActionInvoker0::Invoke(&m4225_MI, p0);
	}

IL_0284:
	{
		InterfaceActionInvoker1< t852 * >::Invoke(&m4253_MI, p0, V_3);
	}

IL_028b:
	{
		return;
	}
}
extern MethodInfo m3851_MI;
 void m3851 (t891 * __this, int32_t* p0, int32_t* p1, MethodInfo* method){
	int32_t V_0 = 0;
	{
		int32_t L_0 = 1;
		V_0 = L_0;
		*((int32_t*)(p1)) = (int32_t)L_0;
		*((int32_t*)(p0)) = (int32_t)V_0;
		return;
	}
}
extern MethodInfo m3852_MI;
 bool m3852 (t891 * __this, MethodInfo* method){
	{
		return 0;
	}
}
 double m3853 (t29 * __this, t866  p0, MethodInfo* method){
	{
		bool L_0 = m3690((&p0), &m3690_MI);
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = m3693((&p0), &m3693_MI);
		return (((double)((int32_t)(3+((int32_t)((int32_t)((int32_t)(L_1+((int32_t)15)))>>(int32_t)4))))));
	}

IL_001c:
	{
		bool L_2 = m3691((&p0), &m3691_MI);
		if (!L_2)
		{
			goto IL_0032;
		}
	}
	{
		return (2.0);
	}

IL_0032:
	{
		return (3.0);
	}
}
// Metadata Definition System.Text.RegularExpressions.Syntax.CharacterClass
extern Il2CppType t866_0_0_17;
FieldInfo t891_f0_FieldInfo = 
{
	"upper_case_characters", &t866_0_0_17, &t891_TI, offsetof(t891_SFs, f0), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t891_f1_FieldInfo = 
{
	"negate", &t40_0_0_1, &t891_TI, offsetof(t891, f1), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t891_f2_FieldInfo = 
{
	"ignore", &t40_0_0_1, &t891_TI, offsetof(t891, f2), &EmptyCustomAttributesCache};
extern Il2CppType t858_0_0_1;
FieldInfo t891_f3_FieldInfo = 
{
	"pos_cats", &t858_0_0_1, &t891_TI, offsetof(t891, f3), &EmptyCustomAttributesCache};
extern Il2CppType t858_0_0_1;
FieldInfo t891_f4_FieldInfo = 
{
	"neg_cats", &t858_0_0_1, &t891_TI, offsetof(t891, f4), &EmptyCustomAttributesCache};
extern Il2CppType t870_0_0_1;
FieldInfo t891_f5_FieldInfo = 
{
	"intervals", &t870_0_0_1, &t891_TI, offsetof(t891, f5), &EmptyCustomAttributesCache};
static FieldInfo* t891_FIs[] =
{
	&t891_f0_FieldInfo,
	&t891_f1_FieldInfo,
	&t891_f2_FieldInfo,
	&t891_f3_FieldInfo,
	&t891_f4_FieldInfo,
	&t891_f5_FieldInfo,
	NULL
};
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t891_m3844_ParameterInfos[] = 
{
	{"negate", 0, 134218525, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"ignore", 1, 134218526, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3844_MI = 
{
	".ctor", (methodPointerType)&m3844, &t891_TI, &t21_0_0_0, RuntimeInvoker_t21_t297_t297, t891_m3844_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 841, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t849_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t891_m3845_ParameterInfos[] = 
{
	{"cat", 0, 134218527, &EmptyCustomAttributesCache, &t849_0_0_0},
	{"negate", 1, 134218528, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t626_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3845_MI = 
{
	".ctor", (methodPointerType)&m3845, &t891_TI, &t21_0_0_0, RuntimeInvoker_t21_t626_t297, t891_m3845_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 842, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3846_MI = 
{
	".cctor", (methodPointerType)&m3846, &t891_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 843, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t849_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t891_m3847_ParameterInfos[] = 
{
	{"cat", 0, 134218529, &EmptyCustomAttributesCache, &t849_0_0_0},
	{"negate", 1, 134218530, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t626_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3847_MI = 
{
	"AddCategory", (methodPointerType)&m3847, &t891_TI, &t21_0_0_0, RuntimeInvoker_t21_t626_t297, t891_m3847_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 2, false, false, 844, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t194_0_0_0;
static ParameterInfo t891_m3848_ParameterInfos[] = 
{
	{"c", 0, 134218531, &EmptyCustomAttributesCache, &t194_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t372 (MethodInfo* method, void* obj, void** args);
MethodInfo m3848_MI = 
{
	"AddCharacter", (methodPointerType)&m3848, &t891_TI, &t21_0_0_0, RuntimeInvoker_t21_t372, t891_m3848_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 845, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t194_0_0_0;
extern Il2CppType t194_0_0_0;
static ParameterInfo t891_m3849_ParameterInfos[] = 
{
	{"lo", 0, 134218532, &EmptyCustomAttributesCache, &t194_0_0_0},
	{"hi", 1, 134218533, &EmptyCustomAttributesCache, &t194_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t372_t372 (MethodInfo* method, void* obj, void** args);
MethodInfo m3849_MI = 
{
	"AddRange", (methodPointerType)&m3849, &t891_TI, &t21_0_0_0, RuntimeInvoker_t21_t372_t372, t891_m3849_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 2, false, false, 846, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t878_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t891_m3850_ParameterInfos[] = 
{
	{"cmp", 0, 134218534, &EmptyCustomAttributesCache, &t878_0_0_0},
	{"reverse", 1, 134218535, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3850_MI = 
{
	"Compile", (methodPointerType)&m3850, &t891_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t297, t891_m3850_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 2, false, false, 847, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_1_0_2;
extern Il2CppType t44_1_0_2;
static ParameterInfo t891_m3851_ParameterInfos[] = 
{
	{"min", 0, 134218536, &EmptyCustomAttributesCache, &t44_1_0_2},
	{"max", 1, 134218537, &EmptyCustomAttributesCache, &t44_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t390_t390 (MethodInfo* method, void* obj, void** args);
MethodInfo m3851_MI = 
{
	"GetWidth", (methodPointerType)&m3851, &t891_TI, &t21_0_0_0, RuntimeInvoker_t21_t390_t390, t891_m3851_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, false, 848, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m3852_MI = 
{
	"IsComplex", (methodPointerType)&m3852, &t891_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 198, 0, 7, 0, false, false, 849, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t866_0_0_0;
static ParameterInfo t891_m3853_ParameterInfos[] = 
{
	{"i", 0, 134218538, &EmptyCustomAttributesCache, &t866_0_0_0},
};
extern Il2CppType t601_0_0_0;
extern void* RuntimeInvoker_t601_t866 (MethodInfo* method, void* obj, void** args);
MethodInfo m3853_MI = 
{
	"GetIntervalCost", (methodPointerType)&m3853, &t891_TI, &t601_0_0_0, RuntimeInvoker_t601_t866, t891_m3853_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 850, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t891_MIs[] =
{
	&m3844_MI,
	&m3845_MI,
	&m3846_MI,
	&m3847_MI,
	&m3848_MI,
	&m3849_MI,
	&m3850_MI,
	&m3851_MI,
	&m3852_MI,
	&m3853_MI,
	NULL
};
static MethodInfo* t891_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m3850_MI,
	&m3851_MI,
	&m3765_MI,
	&m3852_MI,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t891_0_0_0;
extern Il2CppType t891_1_0_0;
struct t891;
TypeInfo t891_TI = 
{
	&g_System_dll_Image, NULL, "CharacterClass", "System.Text.RegularExpressions.Syntax", t891_MIs, NULL, t891_FIs, NULL, &t875_TI, NULL, NULL, &t891_TI, NULL, t891_VT, &EmptyCustomAttributesCache, &t891_TI, &t891_0_0_0, &t891_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t891), 0, -1, sizeof(t891_SFs), 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, true, false, false, 10, 0, 6, 0, 0, 8, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m3859_MI;


 void m3854 (t879 * __this, t875 * p0, int32_t p1, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		__this->f0 = p0;
		__this->f2 = 0;
		__this->f4 = p1;
		__this->f3 = (t7*)NULL;
		__this->f5 = 0;
		__this->f1 = 0;
		return;
	}
}
 void m3855 (t879 * __this, t875 * p0, int32_t p1, int32_t p2, t7* p3, bool p4, MethodInfo* method){
	t879 * G_B2_0 = {0};
	t879 * G_B1_0 = {0};
	t7* G_B3_0 = {0};
	t879 * G_B3_1 = {0};
	{
		m1331(__this, &m1331_MI);
		__this->f0 = p0;
		__this->f2 = p1;
		__this->f4 = p2;
		G_B1_0 = __this;
		if (!p4)
		{
			G_B2_0 = __this;
			goto IL_002f;
		}
	}
	{
		t7* L_0 = m4257(p3, &m4257_MI);
		G_B3_0 = L_0;
		G_B3_1 = G_B1_0;
		goto IL_0031;
	}

IL_002f:
	{
		G_B3_0 = p3;
		G_B3_1 = G_B2_0;
	}

IL_0031:
	{
		G_B3_1->f3 = G_B3_0;
		__this->f5 = p4;
		__this->f1 = 0;
		return;
	}
}
 void m3856 (t879 * __this, t875 * p0, int32_t p1, int32_t p2, uint16_t p3, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		__this->f0 = p0;
		__this->f2 = p1;
		__this->f4 = p2;
		__this->f1 = p3;
		__this->f3 = (t7*)NULL;
		__this->f5 = 0;
		return;
	}
}
 int32_t m3857 (t879 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f2);
		return L_0;
	}
}
 int32_t m3858 (t879 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f4);
		return L_0;
	}
}
 int32_t m3859 (t879 * __this, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		t7* L_0 = (__this->f3);
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		t7* L_1 = (__this->f3);
		int32_t L_2 = m1715(L_1, &m1715_MI);
		G_B3_0 = L_2;
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 0;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
 bool m3860 (t879 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f4);
		return ((((int32_t)L_0) < ((int32_t)0))? 1 : 0);
	}
}
 bool m3861 (t879 * __this, MethodInfo* method){
	{
		int32_t L_0 = m3859(__this, &m3859_MI);
		int32_t L_1 = m3858(__this, &m3858_MI);
		return ((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0);
	}
}
 t7* m3862 (t879 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f3);
		return L_0;
	}
}
 bool m3863 (t879 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->f5);
		return L_0;
	}
}
 uint16_t m3864 (t879 * __this, MethodInfo* method){
	{
		uint16_t L_0 = (__this->f1);
		return L_0;
	}
}
 bool m3865 (t879 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f3);
		return ((((int32_t)((((t7*)L_0) == ((t29 *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
 bool m3866 (t879 * __this, MethodInfo* method){
	{
		uint16_t L_0 = (__this->f1);
		return ((((int32_t)((((int32_t)L_0) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
 t866  m3867 (t879 * __this, int32_t p0, MethodInfo* method){
	{
		bool L_0 = m3865(__this, &m3865_MI);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		t866  L_1 = m3689(NULL, &m3689_MI);
		return L_1;
	}

IL_0011:
	{
		int32_t L_2 = m3857(__this, &m3857_MI);
		int32_t L_3 = m3857(__this, &m3857_MI);
		int32_t L_4 = m3859(__this, &m3859_MI);
		t866  L_5 = {0};
		m3688(&L_5, ((int32_t)(p0+L_2)), ((int32_t)(((int32_t)(((int32_t)(p0+L_3))+L_4))-1)), &m3688_MI);
		return L_5;
	}
}
// Metadata Definition System.Text.RegularExpressions.Syntax.AnchorInfo
extern Il2CppType t875_0_0_1;
FieldInfo t879_f0_FieldInfo = 
{
	"expr", &t875_0_0_1, &t879_TI, offsetof(t879, f0), &EmptyCustomAttributesCache};
extern Il2CppType t845_0_0_1;
FieldInfo t879_f1_FieldInfo = 
{
	"pos", &t845_0_0_1, &t879_TI, offsetof(t879, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t879_f2_FieldInfo = 
{
	"offset", &t44_0_0_1, &t879_TI, offsetof(t879, f2), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_1;
FieldInfo t879_f3_FieldInfo = 
{
	"str", &t7_0_0_1, &t879_TI, offsetof(t879, f3), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t879_f4_FieldInfo = 
{
	"width", &t44_0_0_1, &t879_TI, offsetof(t879, f4), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t879_f5_FieldInfo = 
{
	"ignore", &t40_0_0_1, &t879_TI, offsetof(t879, f5), &EmptyCustomAttributesCache};
static FieldInfo* t879_FIs[] =
{
	&t879_f0_FieldInfo,
	&t879_f1_FieldInfo,
	&t879_f2_FieldInfo,
	&t879_f3_FieldInfo,
	&t879_f4_FieldInfo,
	&t879_f5_FieldInfo,
	NULL
};
static PropertyInfo t879____Offset_PropertyInfo = 
{
	&t879_TI, "Offset", &m3857_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t879____Width_PropertyInfo = 
{
	&t879_TI, "Width", &m3858_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t879____Length_PropertyInfo = 
{
	&t879_TI, "Length", &m3859_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t879____IsUnknownWidth_PropertyInfo = 
{
	&t879_TI, "IsUnknownWidth", &m3860_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t879____IsComplete_PropertyInfo = 
{
	&t879_TI, "IsComplete", &m3861_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t879____Substring_PropertyInfo = 
{
	&t879_TI, "Substring", &m3862_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t879____IgnoreCase_PropertyInfo = 
{
	&t879_TI, "IgnoreCase", &m3863_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t879____Position_PropertyInfo = 
{
	&t879_TI, "Position", &m3864_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t879____IsSubstring_PropertyInfo = 
{
	&t879_TI, "IsSubstring", &m3865_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t879____IsPosition_PropertyInfo = 
{
	&t879_TI, "IsPosition", &m3866_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t879_PIs[] =
{
	&t879____Offset_PropertyInfo,
	&t879____Width_PropertyInfo,
	&t879____Length_PropertyInfo,
	&t879____IsUnknownWidth_PropertyInfo,
	&t879____IsComplete_PropertyInfo,
	&t879____Substring_PropertyInfo,
	&t879____IgnoreCase_PropertyInfo,
	&t879____Position_PropertyInfo,
	&t879____IsSubstring_PropertyInfo,
	&t879____IsPosition_PropertyInfo,
	NULL
};
extern Il2CppType t875_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t879_m3854_ParameterInfos[] = 
{
	{"expr", 0, 134218539, &EmptyCustomAttributesCache, &t875_0_0_0},
	{"width", 1, 134218540, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3854_MI = 
{
	".ctor", (methodPointerType)&m3854, &t879_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t879_m3854_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 851, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t875_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t7_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t879_m3855_ParameterInfos[] = 
{
	{"expr", 0, 134218541, &EmptyCustomAttributesCache, &t875_0_0_0},
	{"offset", 1, 134218542, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"width", 2, 134218543, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"str", 3, 134218544, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"ignore", 4, 134218545, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44_t44_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3855_MI = 
{
	".ctor", (methodPointerType)&m3855, &t879_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44_t44_t29_t297, t879_m3855_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 5, false, false, 852, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t875_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t845_0_0_0;
static ParameterInfo t879_m3856_ParameterInfos[] = 
{
	{"expr", 0, 134218546, &EmptyCustomAttributesCache, &t875_0_0_0},
	{"offset", 1, 134218547, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"width", 2, 134218548, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"pos", 3, 134218549, &EmptyCustomAttributesCache, &t845_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44_t44_t626 (MethodInfo* method, void* obj, void** args);
MethodInfo m3856_MI = 
{
	".ctor", (methodPointerType)&m3856, &t879_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44_t44_t626, t879_m3856_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 4, false, false, 853, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3857_MI = 
{
	"get_Offset", (methodPointerType)&m3857, &t879_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 854, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3858_MI = 
{
	"get_Width", (methodPointerType)&m3858, &t879_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 855, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3859_MI = 
{
	"get_Length", (methodPointerType)&m3859, &t879_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 856, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m3860_MI = 
{
	"get_IsUnknownWidth", (methodPointerType)&m3860, &t879_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 857, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m3861_MI = 
{
	"get_IsComplete", (methodPointerType)&m3861, &t879_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 858, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3862_MI = 
{
	"get_Substring", (methodPointerType)&m3862, &t879_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 859, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m3863_MI = 
{
	"get_IgnoreCase", (methodPointerType)&m3863, &t879_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 860, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t845_0_0_0;
extern void* RuntimeInvoker_t845 (MethodInfo* method, void* obj, void** args);
MethodInfo m3864_MI = 
{
	"get_Position", (methodPointerType)&m3864, &t879_TI, &t845_0_0_0, RuntimeInvoker_t845, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 861, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m3865_MI = 
{
	"get_IsSubstring", (methodPointerType)&m3865, &t879_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 862, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m3866_MI = 
{
	"get_IsPosition", (methodPointerType)&m3866, &t879_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 863, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t879_m3867_ParameterInfos[] = 
{
	{"start", 0, 134218550, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t866_0_0_0;
extern void* RuntimeInvoker_t866_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3867_MI = 
{
	"GetInterval", (methodPointerType)&m3867, &t879_TI, &t866_0_0_0, RuntimeInvoker_t866_t44, t879_m3867_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 864, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t879_MIs[] =
{
	&m3854_MI,
	&m3855_MI,
	&m3856_MI,
	&m3857_MI,
	&m3858_MI,
	&m3859_MI,
	&m3860_MI,
	&m3861_MI,
	&m3862_MI,
	&m3863_MI,
	&m3864_MI,
	&m3865_MI,
	&m3866_MI,
	&m3867_MI,
	NULL
};
static MethodInfo* t879_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t879_0_0_0;
extern Il2CppType t879_1_0_0;
struct t879;
TypeInfo t879_TI = 
{
	&g_System_dll_Image, NULL, "AnchorInfo", "System.Text.RegularExpressions.Syntax", t879_MIs, t879_PIs, t879_FIs, NULL, &t29_TI, NULL, NULL, &t879_TI, NULL, t879_VT, &EmptyCustomAttributesCache, &t879_TI, &t879_0_0_0, &t879_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t879), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 14, 10, 6, 0, 0, 4, 0, 0};
#include "t892.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t892_TI;
#include "t892MD.h"

#include "t893.h"
extern TypeInfo t893_TI;
#include "t893MD.h"
extern MethodInfo m3923_MI;


extern MethodInfo m3868_MI;
 void m3868 (t892 * __this, MethodInfo* method){
	{
		m3923(__this, &m3923_MI);
		return;
	}
}
extern MethodInfo m3869_MI;
 void m3869 (t892 * __this, t7* p0, MethodInfo* method){
	{
		m3923(__this, &m3923_MI);
		__this->f2 = p0;
		return;
	}
}
// Metadata Definition System.DefaultUriParser
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3868_MI = 
{
	".ctor", (methodPointerType)&m3868, &t892_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 865, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t892_m3869_ParameterInfos[] = 
{
	{"scheme", 0, 134218551, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3869_MI = 
{
	".ctor", (methodPointerType)&m3869, &t892_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t892_m3869_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 866, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t892_MIs[] =
{
	&m3868_MI,
	&m3869_MI,
	NULL
};
extern MethodInfo m3925_MI;
extern MethodInfo m3926_MI;
static MethodInfo* t892_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m3925_MI,
	&m3926_MI,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t892_0_0_0;
extern Il2CppType t892_1_0_0;
struct t892;
TypeInfo t892_TI = 
{
	&g_System_dll_Image, NULL, "DefaultUriParser", "System", t892_MIs, NULL, NULL, NULL, &t893_TI, NULL, NULL, &t892_TI, NULL, t892_VT, &EmptyCustomAttributesCache, &t892_TI, &t892_0_0_0, &t892_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t892), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 6, 0, 0};
#include "t894.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t894_TI;
#include "t894MD.h"



// Metadata Definition System.GenericUriParser
static MethodInfo* t894_MIs[] =
{
	NULL
};
static MethodInfo* t894_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m3925_MI,
	&m3926_MI,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t894_0_0_0;
extern Il2CppType t894_1_0_0;
struct t894;
TypeInfo t894_TI = 
{
	&g_System_dll_Image, NULL, "GenericUriParser", "System", t894_MIs, NULL, NULL, NULL, &t893_TI, NULL, NULL, &t894_TI, NULL, t894_VT, &EmptyCustomAttributesCache, &t894_TI, &t894_0_0_0, &t894_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t894), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 6, 0, 0};
#include "t895.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t895_TI;
#include "t895MD.h"



extern MethodInfo m3870_MI;
 void m3870 (t895 * __this, t7* p0, t7* p1, int32_t p2, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = p1;
		__this->f2 = p2;
		return;
	}
}
// Conversion methods for marshalling of: System.Uri/UriScheme
void t895_marshal(const t895& unmarshaled, t895_marshaled& marshaled)
{
	marshaled.f0 = il2cpp_codegen_marshal_string(unmarshaled.f0);
	marshaled.f1 = il2cpp_codegen_marshal_string(unmarshaled.f1);
	marshaled.f2 = unmarshaled.f2;
}
void t895_marshal_back(const t895_marshaled& marshaled, t895& unmarshaled)
{
	unmarshaled.f0 = il2cpp_codegen_marshal_string_result(marshaled.f0);
	unmarshaled.f1 = il2cpp_codegen_marshal_string_result(marshaled.f1);
	unmarshaled.f2 = marshaled.f2;
}
// Conversion method for clean up from marshalling of: System.Uri/UriScheme
void t895_marshal_cleanup(t895_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.f0);
	marshaled.f0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.f1);
	marshaled.f1 = NULL;
}
// Metadata Definition System.Uri/UriScheme
extern Il2CppType t7_0_0_6;
FieldInfo t895_f0_FieldInfo = 
{
	"scheme", &t7_0_0_6, &t895_TI, offsetof(t895, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_6;
FieldInfo t895_f1_FieldInfo = 
{
	"delimiter", &t7_0_0_6, &t895_TI, offsetof(t895, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_6;
FieldInfo t895_f2_FieldInfo = 
{
	"defaultPort", &t44_0_0_6, &t895_TI, offsetof(t895, f2) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t895_FIs[] =
{
	&t895_f0_FieldInfo,
	&t895_f1_FieldInfo,
	&t895_f2_FieldInfo,
	NULL
};
extern Il2CppType t7_0_0_0;
extern Il2CppType t7_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t895_m3870_ParameterInfos[] = 
{
	{"s", 0, 134218600, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"d", 1, 134218601, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"p", 2, 134218602, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3870_MI = 
{
	".ctor", (methodPointerType)&m3870, &t895_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t44, t895_m3870_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, false, 915, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t895_MIs[] =
{
	&m3870_MI,
	NULL
};
static MethodInfo* t895_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t895_0_0_0;
extern Il2CppType t895_1_0_0;
extern TypeInfo t748_TI;
TypeInfo t895_TI = 
{
	&g_System_dll_Image, NULL, "UriScheme", "", t895_MIs, NULL, t895_FIs, NULL, &t110_TI, NULL, &t748_TI, &t895_TI, NULL, t895_VT, &EmptyCustomAttributesCache, &t895_TI, &t895_0_0_0, &t895_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t895_marshal, (methodPointerType)t895_marshal_back, (methodPointerType)t895_marshal_cleanup, sizeof (t895)+ sizeof (Il2CppObject), 0, sizeof(t895_marshaled), 0, 0, -1, 1048843, 0, true, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 3, 0, 0, 4, 0, 0};
#include "t748.h"
#ifndef _MSC_VER
#else
#endif
#include "t748MD.h"

#include "t898.h"
#include "t763.h"
#include "t765.h"
#include "t733.h"
#include "t735.h"
#include "t899.h"
#include "t900.h"
#include "t897.h"
#include "t633.h"
#include "t485.h"
#include "t915.h"
#include "t943.h"
#include "t338.h"
#include "t960.h"
#include "t923.h"
#include "t295.h"
extern TypeInfo t763_TI;
extern TypeInfo t765_TI;
extern TypeInfo t900_TI;
extern TypeInfo t896_TI;
extern TypeInfo t633_TI;
extern TypeInfo t485_TI;
extern TypeInfo t915_TI;
extern TypeInfo t943_TI;
extern TypeInfo t338_TI;
extern TypeInfo t960_TI;
extern TypeInfo t295_TI;
#include "t763MD.h"
#include "t765MD.h"
#include "t733MD.h"
#include "t900MD.h"
#include "t344MD.h"
#include "t633MD.h"
#include "t485MD.h"
#include "t915MD.h"
#include "t943MD.h"
#include "t338MD.h"
#include "t960MD.h"
#include "t714MD.h"
#include "t295MD.h"
extern MethodInfo m3917_MI;
extern MethodInfo m3892_MI;
extern MethodInfo m3882_MI;
extern MethodInfo m3913_MI;
extern MethodInfo m1295_MI;
extern MethodInfo m1713_MI;
extern MethodInfo m3878_MI;
extern MethodInfo m3879_MI;
extern MethodInfo m3188_MI;
extern MethodInfo m3197_MI;
extern MethodInfo m3207_MI;
extern MethodInfo m3211_MI;
extern MethodInfo m3932_MI;
extern MethodInfo m3873_MI;
extern MethodInfo m3994_MI;
extern MethodInfo m3901_MI;
extern MethodInfo m3920_MI;
extern MethodInfo m3876_MI;
extern MethodInfo m3997_MI;
extern MethodInfo m3885_MI;
extern MethodInfo m3886_MI;
extern MethodInfo m2928_MI;
extern MethodInfo m4278_MI;
extern MethodInfo m3888_MI;
extern MethodInfo m3871_MI;
extern MethodInfo m3890_MI;
extern MethodInfo m4009_MI;
extern MethodInfo m4279_MI;
extern MethodInfo m2843_MI;
extern MethodInfo m3914_MI;
extern MethodInfo m1761_MI;
extern MethodInfo m4077_MI;
extern MethodInfo m4038_MI;
extern MethodInfo m4039_MI;
extern MethodInfo m4040_MI;
extern MethodInfo m3909_MI;
extern MethodInfo m3910_MI;
extern MethodInfo m1935_MI;
extern MethodInfo m3975_MI;
extern MethodInfo m3895_MI;
extern MethodInfo m3903_MI;
extern MethodInfo m3902_MI;
extern MethodInfo m3897_MI;
extern MethodInfo m3900_MI;
extern MethodInfo m3896_MI;
extern MethodInfo m4091_MI;
extern MethodInfo m4280_MI;
extern MethodInfo m1810_MI;
extern MethodInfo m3894_MI;
extern MethodInfo m3907_MI;
extern MethodInfo m3899_MI;
extern MethodInfo m3911_MI;
extern MethodInfo m4281_MI;
extern MethodInfo m2932_MI;
extern MethodInfo m2950_MI;
extern MethodInfo m3908_MI;
extern MethodInfo m2925_MI;
extern MethodInfo m3906_MI;
extern MethodInfo m3904_MI;
extern MethodInfo m3905_MI;
extern MethodInfo m3887_MI;
extern MethodInfo m3068_MI;
extern MethodInfo m4282_MI;
extern MethodInfo m3881_MI;
extern MethodInfo m4283_MI;
extern MethodInfo m3915_MI;
extern MethodInfo m1740_MI;
extern MethodInfo m4284_MI;
extern MethodInfo m4010_MI;
extern MethodInfo m3884_MI;
extern MethodInfo m3217_MI;
extern MethodInfo m3916_MI;
extern MethodInfo m1684_MI;
extern MethodInfo m2933_MI;
extern MethodInfo m4285_MI;
extern MethodInfo m2930_MI;
extern MethodInfo m3893_MI;
extern MethodInfo m3928_MI;
extern MethodInfo m3912_MI;
extern MethodInfo m3883_MI;
extern MethodInfo m3964_MI;
extern MethodInfo m4286_MI;


 void m3871 (t748 * __this, t7* p0, MethodInfo* method){
	{
		m3873(__this, p0, 0, &m3873_MI);
		return;
	}
}
extern MethodInfo m3872_MI;
 void m3872 (t748 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		t7* L_0 = m3994(p0, (t7*) &_stringLiteral516, &m3994_MI);
		m3873(__this, L_0, 1, &m3873_MI);
		return;
	}
}
 void m3873 (t748 * __this, t7* p0, bool p1, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		__this->f2 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		__this->f3 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		__this->f4 = (-1);
		__this->f5 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		__this->f6 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		__this->f7 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		__this->f8 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		__this->f11 = 1;
		m1331(__this, &m1331_MI);
		__this->f12 = p1;
		__this->f1 = p0;
		m3901(__this, 1, &m3901_MI);
		bool L_0 = (__this->f11);
		if (L_0)
		{
			goto IL_0087;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_1 = m66(NULL, (t7*) &_stringLiteral517, p0, &m66_MI);
		t900 * L_2 = (t900 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t900_TI));
		m3920(L_2, L_1, &m3920_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0087:
	{
		return;
	}
}
extern MethodInfo m3874_MI;
 void m3874 (t29 * __this, MethodInfo* method){
	{
		((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f16 = (t7*) &_stringLiteral518;
		((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f17 = (t7*) &_stringLiteral280;
		((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f18 = (t7*) &_stringLiteral336;
		((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f19 = (t7*) &_stringLiteral338;
		((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f20 = (t7*) &_stringLiteral519;
		((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f21 = (t7*) &_stringLiteral278;
		((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f22 = (t7*) &_stringLiteral277;
		((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f23 = (t7*) &_stringLiteral520;
		((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f24 = (t7*) &_stringLiteral521;
		((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f25 = (t7*) &_stringLiteral522;
		((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f26 = (t7*) &_stringLiteral523;
		((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f27 = (t7*) &_stringLiteral524;
		t896* L_0 = ((t896*)SZArrayNew(InitializedTypeInfo(&t896_TI), 8));
		t895  L_1 = {0};
		m3870(&L_1, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f21), (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f17), ((int32_t)80), &m3870_MI);
		*((t895 *)(t895 *)SZArrayLdElema(L_0, 0)) = L_1;
		t896* L_2 = L_0;
		t895  L_3 = {0};
		m3870(&L_3, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f22), (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f17), ((int32_t)443), &m3870_MI);
		*((t895 *)(t895 *)SZArrayLdElema(L_2, 1)) = L_3;
		t896* L_4 = L_2;
		t895  L_5 = {0};
		m3870(&L_5, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f19), (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f17), ((int32_t)21), &m3870_MI);
		*((t895 *)(t895 *)SZArrayLdElema(L_4, 2)) = L_5;
		t896* L_6 = L_4;
		t895  L_7 = {0};
		m3870(&L_7, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f18), (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f17), (-1), &m3870_MI);
		*((t895 *)(t895 *)SZArrayLdElema(L_6, 3)) = L_7;
		t896* L_8 = L_6;
		t895  L_9 = {0};
		m3870(&L_9, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f23), (t7*) &_stringLiteral182, ((int32_t)25), &m3870_MI);
		*((t895 *)(t895 *)SZArrayLdElema(L_8, 4)) = L_9;
		t896* L_10 = L_8;
		t895  L_11 = {0};
		m3870(&L_11, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f24), (t7*) &_stringLiteral182, ((int32_t)119), &m3870_MI);
		*((t895 *)(t895 *)SZArrayLdElema(L_10, 5)) = L_11;
		t896* L_12 = L_10;
		t895  L_13 = {0};
		m3870(&L_13, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f25), (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f17), ((int32_t)119), &m3870_MI);
		*((t895 *)(t895 *)SZArrayLdElema(L_12, 6)) = L_13;
		t896* L_14 = L_12;
		t895  L_15 = {0};
		m3870(&L_15, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f20), (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f17), ((int32_t)70), &m3870_MI);
		*((t895 *)(t895 *)SZArrayLdElema(L_14, 7)) = L_15;
		((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f28 = L_14;
		return;
	}
}
extern MethodInfo m3875_MI;
 void m3875 (t748 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		t7* L_0 = m3876(__this, &m3876_MI);
		m3997(p0, (t7*) &_stringLiteral516, L_0, &m3997_MI);
		return;
	}
}
 t7* m3876 (t748 * __this, MethodInfo* method){
	{
		m3917(__this, &m3917_MI);
		t7* L_0 = (__this->f13);
		if (L_0)
		{
			goto IL_006e;
		}
	}
	{
		t7* L_1 = m3892(__this, 2, &m3892_MI);
		__this->f13 = L_1;
		t7* L_2 = (__this->f6);
		int32_t L_3 = m1715(L_2, &m1715_MI);
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_0046;
		}
	}
	{
		t7* L_4 = (__this->f13);
		t7* L_5 = (__this->f6);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_6 = m66(NULL, L_4, L_5, &m66_MI);
		__this->f13 = L_6;
	}

IL_0046:
	{
		t7* L_7 = (__this->f7);
		int32_t L_8 = m1715(L_7, &m1715_MI);
		if ((((int32_t)L_8) <= ((int32_t)0)))
		{
			goto IL_006e;
		}
	}
	{
		t7* L_9 = (__this->f13);
		t7* L_10 = (__this->f7);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_11 = m66(NULL, L_9, L_10, &m66_MI);
		__this->f13 = L_11;
	}

IL_006e:
	{
		t7* L_12 = (__this->f13);
		return L_12;
	}
}
extern MethodInfo m3877_MI;
 t7* m3877 (t748 * __this, MethodInfo* method){
	t7* G_B3_0 = {0};
	{
		m3917(__this, &m3917_MI);
		t7* L_0 = m3882(__this, &m3882_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		int32_t L_1 = m3913(NULL, L_0, &m3913_MI);
		int32_t L_2 = (__this->f4);
		if ((((uint32_t)L_1) != ((uint32_t)L_2)))
		{
			goto IL_0027;
		}
	}
	{
		t7* L_3 = (__this->f3);
		G_B3_0 = L_3;
		goto IL_0042;
	}

IL_0027:
	{
		t7* L_4 = (__this->f3);
		int32_t L_5 = (__this->f4);
		int32_t L_6 = L_5;
		t29 * L_7 = Box(InitializedTypeInfo(&t44_TI), &L_6);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_8 = m1295(NULL, L_4, (t7*) &_stringLiteral182, L_7, &m1295_MI);
		G_B3_0 = L_8;
	}

IL_0042:
	{
		return G_B3_0;
	}
}
 t7* m3878 (t748 * __this, MethodInfo* method){
	{
		m3917(__this, &m3917_MI);
		t7* L_0 = (__this->f3);
		return L_0;
	}
}
 bool m3879 (t748 * __this, MethodInfo* method){
	{
		m3917(__this, &m3917_MI);
		t7* L_0 = m3882(__this, &m3882_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_1 = m1713(NULL, L_0, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f18), &m1713_MI);
		return L_1;
	}
}
extern MethodInfo m3880_MI;
 bool m3880 (t748 * __this, MethodInfo* method){
	t763 * V_0 = {0};
	t765 * V_1 = {0};
	{
		m3917(__this, &m3917_MI);
		t7* L_0 = m3878(__this, &m3878_MI);
		int32_t L_1 = m1715(L_0, &m1715_MI);
		if (L_1)
		{
			goto IL_001d;
		}
	}
	{
		bool L_2 = m3879(__this, &m3879_MI);
		return L_2;
	}

IL_001d:
	{
		t7* L_3 = (__this->f3);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_4 = m1713(NULL, L_3, (t7*) &_stringLiteral334, &m1713_MI);
		if (L_4)
		{
			goto IL_0047;
		}
	}
	{
		t7* L_5 = (__this->f3);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_6 = m1713(NULL, L_5, (t7*) &_stringLiteral333, &m1713_MI);
		if (!L_6)
		{
			goto IL_0049;
		}
	}

IL_0047:
	{
		return 1;
	}

IL_0049:
	{
		t7* L_7 = (__this->f3);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t763_TI));
		bool L_8 = m3188(NULL, L_7, (&V_0), &m3188_MI);
		if (!L_8)
		{
			goto IL_006d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t763_TI));
		bool L_9 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m3197_MI, (((t763_SFs*)InitializedTypeInfo(&t763_TI)->static_fields)->f6), V_0);
		if (!L_9)
		{
			goto IL_006d;
		}
	}
	{
		return 1;
	}

IL_006d:
	{
		t7* L_10 = (__this->f3);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t765_TI));
		bool L_11 = m3207(NULL, L_10, (&V_1), &m3207_MI);
		if (!L_11)
		{
			goto IL_008c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t765_TI));
		bool L_12 = m3211(NULL, V_1, &m3211_MI);
		if (!L_12)
		{
			goto IL_008c;
		}
	}
	{
		return 1;
	}

IL_008c:
	{
		return 0;
	}
}
 bool m3881 (t748 * __this, MethodInfo* method){
	{
		m3917(__this, &m3917_MI);
		bool L_0 = (__this->f9);
		return L_0;
	}
}
 t7* m3882 (t748 * __this, MethodInfo* method){
	{
		m3917(__this, &m3917_MI);
		t7* L_0 = (__this->f2);
		return L_0;
	}
}
 bool m3883 (t748 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->f11);
		return L_0;
	}
}
 int32_t m3884 (t29 * __this, t7* p0, MethodInfo* method){
	t765 * V_0 = {0};
	{
		if (!p0)
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_0 = m1715(p0, &m1715_MI);
		if (L_0)
		{
			goto IL_0013;
		}
	}

IL_0011:
	{
		return (int32_t)(0);
	}

IL_0013:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		bool L_1 = m3885(NULL, p0, &m3885_MI);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		return (int32_t)(3);
	}

IL_0020:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		bool L_2 = m3886(NULL, p0, &m3886_MI);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		return (int32_t)(2);
	}

IL_002d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t765_TI));
		bool L_3 = m3207(NULL, p0, (&V_0), &m3207_MI);
		if (!L_3)
		{
			goto IL_003c;
		}
	}
	{
		return (int32_t)(4);
	}

IL_003c:
	{
		return (int32_t)(0);
	}
}
 bool m3885 (t29 * __this, t7* p0, MethodInfo* method){
	t446* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	uint32_t V_3 = 0;
	{
		t200* L_0 = ((t200*)SZArrayNew(InitializedTypeInfo(&t200_TI), 1));
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_0, 0)) = (uint16_t)((int32_t)46);
		t446* L_1 = m2928(p0, L_0, &m2928_MI);
		V_0 = L_1;
		if ((((int32_t)(((int32_t)(((t20 *)V_0)->max_length)))) == ((int32_t)4)))
		{
			goto IL_001d;
		}
	}
	{
		return 0;
	}

IL_001d:
	{
		V_1 = 0;
		goto IL_0057;
	}

IL_0024:
	{
		int32_t L_2 = V_1;
		int32_t L_3 = m1715((*(t7**)(t7**)SZArrayLdElema(V_0, L_2)), &m1715_MI);
		V_2 = L_3;
		if (V_2)
		{
			goto IL_0035;
		}
	}
	{
		return 0;
	}

IL_0035:
	{
		int32_t L_4 = V_1;
		bool L_5 = m4278(NULL, (*(t7**)(t7**)SZArrayLdElema(V_0, L_4)), (&V_3), &m4278_MI);
		if (L_5)
		{
			goto IL_0046;
		}
	}
	{
		return 0;
	}

IL_0046:
	{
		if ((((uint32_t)V_3) <= ((uint32_t)((int32_t)255))))
		{
			goto IL_0053;
		}
	}
	{
		return 0;
	}

IL_0053:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0057:
	{
		if ((((int32_t)V_1) < ((int32_t)4)))
		{
			goto IL_0024;
		}
	}
	{
		return 1;
	}
}
 bool m3886 (t29 * __this, t7* p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	uint16_t V_3 = 0x0;
	{
		int32_t L_0 = m1715(p0, &m1715_MI);
		V_0 = L_0;
		V_1 = 0;
		V_2 = 0;
		goto IL_006e;
	}

IL_0010:
	{
		uint16_t L_1 = m1741(p0, V_2, &m1741_MI);
		V_3 = L_1;
		if (V_1)
		{
			goto IL_0030;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t194_TI));
		bool L_2 = m4221(NULL, V_3, &m4221_MI);
		if (L_2)
		{
			goto IL_002b;
		}
	}
	{
		return 0;
	}

IL_002b:
	{
		goto IL_005c;
	}

IL_0030:
	{
		if ((((uint32_t)V_3) != ((uint32_t)((int32_t)46))))
		{
			goto IL_003f;
		}
	}
	{
		V_1 = 0;
		goto IL_005c;
	}

IL_003f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t194_TI));
		bool L_3 = m4221(NULL, V_3, &m4221_MI);
		if (L_3)
		{
			goto IL_005c;
		}
	}
	{
		if ((((int32_t)V_3) == ((int32_t)((int32_t)45))))
		{
			goto IL_005c;
		}
	}
	{
		if ((((int32_t)V_3) == ((int32_t)((int32_t)95))))
		{
			goto IL_005c;
		}
	}
	{
		return 0;
	}

IL_005c:
	{
		int32_t L_4 = ((int32_t)(V_1+1));
		V_1 = L_4;
		if ((((uint32_t)L_4) != ((uint32_t)((int32_t)64))))
		{
			goto IL_006a;
		}
	}
	{
		return 0;
	}

IL_006a:
	{
		V_2 = ((int32_t)(V_2+1));
	}

IL_006e:
	{
		if ((((int32_t)V_2) < ((int32_t)V_0)))
		{
			goto IL_0010;
		}
	}
	{
		return 1;
	}
}
 bool m3887 (t29 * __this, t7* p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	uint16_t V_2 = 0x0;
	{
		if (!p0)
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_0 = m1715(p0, &m1715_MI);
		if (L_0)
		{
			goto IL_0013;
		}
	}

IL_0011:
	{
		return 0;
	}

IL_0013:
	{
		uint16_t L_1 = m1741(p0, 0, &m1741_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		bool L_2 = m3888(NULL, L_1, &m3888_MI);
		if (L_2)
		{
			goto IL_0026;
		}
	}
	{
		return 0;
	}

IL_0026:
	{
		int32_t L_3 = m1715(p0, &m1715_MI);
		V_0 = L_3;
		V_1 = 1;
		goto IL_0070;
	}

IL_0034:
	{
		uint16_t L_4 = m1741(p0, V_1, &m1741_MI);
		V_2 = L_4;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t194_TI));
		bool L_5 = m4222(NULL, V_2, &m4222_MI);
		if (L_5)
		{
			goto IL_006c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		bool L_6 = m3888(NULL, V_2, &m3888_MI);
		if (L_6)
		{
			goto IL_006c;
		}
	}
	{
		if ((((int32_t)V_2) == ((int32_t)((int32_t)46))))
		{
			goto IL_006c;
		}
	}
	{
		if ((((int32_t)V_2) == ((int32_t)((int32_t)43))))
		{
			goto IL_006c;
		}
	}
	{
		if ((((int32_t)V_2) == ((int32_t)((int32_t)45))))
		{
			goto IL_006c;
		}
	}
	{
		return 0;
	}

IL_006c:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0070:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0034;
		}
	}
	{
		return 1;
	}
}
 bool m3888 (t29 * __this, uint16_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	int32_t G_B7_0 = 0;
	{
		V_0 = p0;
		if ((((int32_t)V_0) < ((int32_t)((int32_t)65))))
		{
			goto IL_0012;
		}
	}
	{
		if ((((int32_t)V_0) <= ((int32_t)((int32_t)90))))
		{
			goto IL_0027;
		}
	}

IL_0012:
	{
		if ((((int32_t)V_0) < ((int32_t)((int32_t)97))))
		{
			goto IL_0024;
		}
	}
	{
		G_B5_0 = ((((int32_t)((((int32_t)V_0) > ((int32_t)((int32_t)122)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0025;
	}

IL_0024:
	{
		G_B5_0 = 0;
	}

IL_0025:
	{
		G_B7_0 = G_B5_0;
		goto IL_0028;
	}

IL_0027:
	{
		G_B7_0 = 1;
	}

IL_0028:
	{
		return G_B7_0;
	}
}
extern MethodInfo m3889_MI;
 bool m3889 (t748 * __this, t29 * p0, MethodInfo* method){
	t748 * V_0 = {0};
	t7* V_1 = {0};
	{
		if (p0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		V_0 = ((t748 *)IsInst(p0, InitializedTypeInfo(&t748_TI)));
		if (V_0)
		{
			goto IL_002b;
		}
	}
	{
		V_1 = ((t7*)IsInst(p0, (&t7_TI)));
		if (V_1)
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}

IL_0024:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		t748 * L_0 = (t748 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t748_TI));
		m3871(L_0, V_1, &m3871_MI);
		V_0 = L_0;
	}

IL_002b:
	{
		bool L_1 = m3890(__this, V_0, &m3890_MI);
		return L_1;
	}
}
 bool m3890 (t748 * __this, t748 * p0, MethodInfo* method){
	t633 * V_0 = {0};
	int32_t G_B10_0 = 0;
	{
		bool L_0 = (__this->f11);
		bool L_1 = (p0->f11);
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0013;
		}
	}
	{
		return 0;
	}

IL_0013:
	{
		bool L_2 = (__this->f11);
		if (L_2)
		{
			goto IL_0030;
		}
	}
	{
		t7* L_3 = (__this->f1);
		t7* L_4 = (p0->f1);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_5 = m1713(NULL, L_3, L_4, &m1713_MI);
		return L_5;
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t633_TI));
		t633 * L_6 = m4009(NULL, &m4009_MI);
		V_0 = L_6;
		t7* L_7 = (__this->f2);
		t7* L_8 = m4279(L_7, V_0, &m4279_MI);
		t7* L_9 = (p0->f2);
		t7* L_10 = m4279(L_9, V_0, &m4279_MI);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_11 = m1713(NULL, L_8, L_10, &m1713_MI);
		if (!L_11)
		{
			goto IL_00b4;
		}
	}
	{
		t7* L_12 = (__this->f3);
		t7* L_13 = m4279(L_12, V_0, &m4279_MI);
		t7* L_14 = (p0->f3);
		t7* L_15 = m4279(L_14, V_0, &m4279_MI);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_16 = m1713(NULL, L_13, L_15, &m1713_MI);
		if (!L_16)
		{
			goto IL_00b4;
		}
	}
	{
		int32_t L_17 = (__this->f4);
		int32_t L_18 = (p0->f4);
		if ((((uint32_t)L_17) != ((uint32_t)L_18)))
		{
			goto IL_00b4;
		}
	}
	{
		t7* L_19 = (__this->f6);
		t7* L_20 = (p0->f6);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_21 = m1713(NULL, L_19, L_20, &m1713_MI);
		if (!L_21)
		{
			goto IL_00b4;
		}
	}
	{
		t7* L_22 = (__this->f5);
		t7* L_23 = (p0->f5);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_24 = m1713(NULL, L_22, L_23, &m1713_MI);
		G_B10_0 = ((int32_t)(L_24));
		goto IL_00b5;
	}

IL_00b4:
	{
		G_B10_0 = 0;
	}

IL_00b5:
	{
		return G_B10_0;
	}
}
extern MethodInfo m3891_MI;
 int32_t m3891 (t748 * __this, MethodInfo* method){
	t633 * V_0 = {0};
	{
		int32_t L_0 = (__this->f15);
		if (L_0)
		{
			goto IL_007a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t633_TI));
		t633 * L_1 = m4009(NULL, &m4009_MI);
		V_0 = L_1;
		bool L_2 = (__this->f11);
		if (!L_2)
		{
			goto IL_0069;
		}
	}
	{
		t7* L_3 = (__this->f2);
		t7* L_4 = m4279(L_3, V_0, &m4279_MI);
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m2843_MI, L_4);
		t7* L_6 = (__this->f3);
		t7* L_7 = m4279(L_6, V_0, &m4279_MI);
		int32_t L_8 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m2843_MI, L_7);
		int32_t L_9 = (__this->f4);
		t7* L_10 = (__this->f6);
		int32_t L_11 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m2843_MI, L_10);
		t7* L_12 = (__this->f5);
		int32_t L_13 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m2843_MI, L_12);
		__this->f15 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5^(int32_t)L_8))^(int32_t)L_9))^(int32_t)L_11))^(int32_t)L_13));
		goto IL_007a;
	}

IL_0069:
	{
		t7* L_14 = (__this->f1);
		int32_t L_15 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m2843_MI, L_14);
		__this->f15 = L_15;
	}

IL_007a:
	{
		int32_t L_16 = (__this->f15);
		return L_16;
	}
}
 t7* m3892 (t748 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	t292 * V_1 = {0};
	t292 * V_2 = {0};
	int32_t V_3 = {0};
	t7* V_4 = {0};
	t485 * V_5 = {0};
	int32_t V_6 = 0;
	{
		m3917(__this, &m3917_MI);
		V_3 = p0;
		if (V_3 == 0)
		{
			goto IL_001f;
		}
		if (V_3 == 1)
		{
			goto IL_0031;
		}
		if (V_3 == 2)
		{
			goto IL_0134;
		}
	}
	{
		goto IL_02ad;
	}

IL_001f:
	{
		t7* L_0 = (__this->f2);
		t7* L_1 = m3914(__this, &m3914_MI);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_2 = m66(NULL, L_0, L_1, &m66_MI);
		return L_2;
	}

IL_0031:
	{
		t7* L_3 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_4 = m1713(NULL, L_3, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f23), &m1713_MI);
		if (L_4)
		{
			goto IL_005b;
		}
	}
	{
		t7* L_5 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_6 = m1713(NULL, L_5, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f24), &m1713_MI);
		if (!L_6)
		{
			goto IL_0061;
		}
	}

IL_005b:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		return (((t7_SFs*)(&t7_TI)->static_fields)->f2);
	}

IL_0061:
	{
		t292 * L_7 = (t292 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t292_TI));
		m1311(L_7, &m1311_MI);
		V_1 = L_7;
		t7* L_8 = (__this->f2);
		m2927(V_1, L_8, &m2927_MI);
		t7* L_9 = m3914(__this, &m3914_MI);
		m2927(V_1, L_9, &m2927_MI);
		t7* L_10 = (__this->f5);
		int32_t L_11 = m1715(L_10, &m1715_MI);
		if ((((int32_t)L_11) <= ((int32_t)1)))
		{
			goto IL_00c3;
		}
	}
	{
		t7* L_12 = (__this->f5);
		uint16_t L_13 = m1741(L_12, 1, &m1741_MI);
		if ((((uint32_t)L_13) != ((uint32_t)((int32_t)58))))
		{
			goto IL_00c3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		t7* L_14 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_15 = m1713(NULL, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f18), L_14, &m1713_MI);
		if (!L_15)
		{
			goto IL_00c3;
		}
	}
	{
		m1761(V_1, ((int32_t)47), &m1761_MI);
	}

IL_00c3:
	{
		t7* L_16 = (__this->f8);
		int32_t L_17 = m1715(L_16, &m1715_MI);
		if ((((int32_t)L_17) <= ((int32_t)0)))
		{
			goto IL_00e8;
		}
	}
	{
		t7* L_18 = (__this->f8);
		t292 * L_19 = m2927(V_1, L_18, &m2927_MI);
		m1761(L_19, ((int32_t)64), &m1761_MI);
	}

IL_00e8:
	{
		t7* L_20 = (__this->f3);
		m2927(V_1, L_20, &m2927_MI);
		t7* L_21 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		int32_t L_22 = m3913(NULL, L_21, &m3913_MI);
		V_0 = L_22;
		int32_t L_23 = (__this->f4);
		if ((((int32_t)L_23) == ((int32_t)(-1))))
		{
			goto IL_012d;
		}
	}
	{
		int32_t L_24 = (__this->f4);
		if ((((int32_t)L_24) == ((int32_t)V_0)))
		{
			goto IL_012d;
		}
	}
	{
		t292 * L_25 = m1761(V_1, ((int32_t)58), &m1761_MI);
		int32_t L_26 = (__this->f4);
		m4077(L_25, L_26, &m4077_MI);
	}

IL_012d:
	{
		t7* L_27 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1315_MI, V_1);
		return L_27;
	}

IL_0134:
	{
		t292 * L_28 = (t292 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t292_TI));
		m1311(L_28, &m1311_MI);
		V_2 = L_28;
		t7* L_29 = (__this->f2);
		m2927(V_2, L_29, &m2927_MI);
		t7* L_30 = m3914(__this, &m3914_MI);
		m2927(V_2, L_30, &m2927_MI);
		t7* L_31 = (__this->f5);
		int32_t L_32 = m1715(L_31, &m1715_MI);
		if ((((int32_t)L_32) <= ((int32_t)1)))
		{
			goto IL_0196;
		}
	}
	{
		t7* L_33 = (__this->f5);
		uint16_t L_34 = m1741(L_33, 1, &m1741_MI);
		if ((((uint32_t)L_34) != ((uint32_t)((int32_t)58))))
		{
			goto IL_0196;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		t7* L_35 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_36 = m1713(NULL, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f18), L_35, &m1713_MI);
		if (!L_36)
		{
			goto IL_0196;
		}
	}
	{
		m1761(V_2, ((int32_t)47), &m1761_MI);
	}

IL_0196:
	{
		t7* L_37 = (__this->f8);
		int32_t L_38 = m1715(L_37, &m1715_MI);
		if ((((int32_t)L_38) <= ((int32_t)0)))
		{
			goto IL_01bb;
		}
	}
	{
		t7* L_39 = (__this->f8);
		t292 * L_40 = m2927(V_2, L_39, &m2927_MI);
		m1761(L_40, ((int32_t)64), &m1761_MI);
	}

IL_01bb:
	{
		t7* L_41 = (__this->f3);
		m2927(V_2, L_41, &m2927_MI);
		t7* L_42 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		int32_t L_43 = m3913(NULL, L_42, &m3913_MI);
		V_0 = L_43;
		int32_t L_44 = (__this->f4);
		if ((((int32_t)L_44) == ((int32_t)(-1))))
		{
			goto IL_0200;
		}
	}
	{
		int32_t L_45 = (__this->f4);
		if ((((int32_t)L_45) == ((int32_t)V_0)))
		{
			goto IL_0200;
		}
	}
	{
		t292 * L_46 = m1761(V_2, ((int32_t)58), &m1761_MI);
		int32_t L_47 = (__this->f4);
		m4077(L_46, L_47, &m4077_MI);
	}

IL_0200:
	{
		t7* L_48 = (__this->f5);
		int32_t L_49 = m1715(L_48, &m1715_MI);
		if ((((int32_t)L_49) <= ((int32_t)0)))
		{
			goto IL_02a6;
		}
	}
	{
		t7* L_50 = m3882(__this, &m3882_MI);
		V_4 = L_50;
		if (!V_4)
		{
			goto IL_0284;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		if ((((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f30))
		{
			goto IL_0253;
		}
	}
	{
		t485 * L_51 = (t485 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t485_TI));
		m4038(L_51, 2, &m4038_MI);
		V_5 = L_51;
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_5, (t7*) &_stringLiteral520, 0);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_5, (t7*) &_stringLiteral521, 0);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f30 = V_5;
	}

IL_0253:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		bool L_52 = (bool)VirtFuncInvoker2< bool, t7*, int32_t* >::Invoke(&m4040_MI, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f30), V_4, (&V_6));
		if (!L_52)
		{
			goto IL_0284;
		}
	}
	{
		if (!V_6)
		{
			goto IL_0272;
		}
	}
	{
		goto IL_0284;
	}

IL_0272:
	{
		t7* L_53 = (__this->f5);
		m2927(V_2, L_53, &m2927_MI);
		goto IL_02a6;
	}

IL_0284:
	{
		t7* L_54 = (__this->f5);
		t7* L_55 = m3882(__this, &m3882_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		bool L_56 = m3909(NULL, L_55, &m3909_MI);
		t7* L_57 = m3910(NULL, L_54, L_56, &m3910_MI);
		m2927(V_2, L_57, &m2927_MI);
		goto IL_02a6;
	}

IL_02a6:
	{
		t7* L_58 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1315_MI, V_2);
		return L_58;
	}

IL_02ad:
	{
		return (t7*)NULL;
	}
}
 int32_t m3893 (t29 * __this, uint16_t p0, MethodInfo* method){
	{
		if ((((int32_t)((int32_t)48)) > ((int32_t)p0)))
		{
			goto IL_0015;
		}
	}
	{
		if ((((int32_t)p0) > ((int32_t)((int32_t)57))))
		{
			goto IL_0015;
		}
	}
	{
		return ((uint16_t)(p0-((int32_t)48)));
	}

IL_0015:
	{
		if ((((int32_t)((int32_t)97)) > ((int32_t)p0)))
		{
			goto IL_002d;
		}
	}
	{
		if ((((int32_t)p0) > ((int32_t)((int32_t)102))))
		{
			goto IL_002d;
		}
	}
	{
		return ((int32_t)(((uint16_t)(p0-((int32_t)97)))+((int32_t)10)));
	}

IL_002d:
	{
		if ((((int32_t)((int32_t)65)) > ((int32_t)p0)))
		{
			goto IL_0045;
		}
	}
	{
		if ((((int32_t)p0) > ((int32_t)((int32_t)70))))
		{
			goto IL_0045;
		}
	}
	{
		return ((int32_t)(((uint16_t)(p0-((int32_t)65)))+((int32_t)10)));
	}

IL_0045:
	{
		t305 * L_0 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_0, (t7*) &_stringLiteral525, &m1935_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
 t7* m3894 (t29 * __this, uint16_t p0, MethodInfo* method){
	{
		if ((((int32_t)p0) <= ((int32_t)((int32_t)255))))
		{
			goto IL_0016;
		}
	}
	{
		t915 * L_0 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_0, (t7*) &_stringLiteral526, &m3975_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		uint16_t L_1 = m1741((((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f16), ((int32_t)((int32_t)((int32_t)((int32_t)p0&(int32_t)((int32_t)240)))>>(int32_t)4)), &m1741_MI);
		uint16_t L_2 = L_1;
		t29 * L_3 = Box(InitializedTypeInfo(&t194_TI), &L_2);
		uint16_t L_4 = m1741((((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f16), ((int32_t)((int32_t)p0&(int32_t)((int32_t)15))), &m1741_MI);
		uint16_t L_5 = L_4;
		t29 * L_6 = Box(InitializedTypeInfo(&t194_TI), &L_5);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_7 = m1295(NULL, (t7*) &_stringLiteral527, L_3, L_6, &m1295_MI);
		return L_7;
	}
}
 bool m3895 (t29 * __this, uint16_t p0, MethodInfo* method){
	int32_t G_B7_0 = 0;
	int32_t G_B9_0 = 0;
	{
		if ((((int32_t)((int32_t)48)) > ((int32_t)p0)))
		{
			goto IL_0010;
		}
	}
	{
		if ((((int32_t)p0) <= ((int32_t)((int32_t)57))))
		{
			goto IL_0035;
		}
	}

IL_0010:
	{
		if ((((int32_t)((int32_t)97)) > ((int32_t)p0)))
		{
			goto IL_0020;
		}
	}
	{
		if ((((int32_t)p0) <= ((int32_t)((int32_t)102))))
		{
			goto IL_0035;
		}
	}

IL_0020:
	{
		if ((((int32_t)((int32_t)65)) > ((int32_t)p0)))
		{
			goto IL_0032;
		}
	}
	{
		G_B7_0 = ((((int32_t)((((int32_t)p0) > ((int32_t)((int32_t)70)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0033;
	}

IL_0032:
	{
		G_B7_0 = 0;
	}

IL_0033:
	{
		G_B9_0 = G_B7_0;
		goto IL_0036;
	}

IL_0035:
	{
		G_B9_0 = 1;
	}

IL_0036:
	{
		return G_B9_0;
	}
}
 bool m3896 (t29 * __this, t7* p0, int32_t p1, MethodInfo* method){
	int32_t G_B6_0 = 0;
	{
		int32_t L_0 = m1715(p0, &m1715_MI);
		if ((((int32_t)((int32_t)(p1+3))) <= ((int32_t)L_0)))
		{
			goto IL_0010;
		}
	}
	{
		return 0;
	}

IL_0010:
	{
		int32_t L_1 = p1;
		p1 = ((int32_t)(L_1+1));
		uint16_t L_2 = m1741(p0, L_1, &m1741_MI);
		if ((((uint32_t)L_2) != ((uint32_t)((int32_t)37))))
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_3 = p1;
		p1 = ((int32_t)(L_3+1));
		uint16_t L_4 = m1741(p0, L_3, &m1741_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		bool L_5 = m3895(NULL, L_4, &m3895_MI);
		if (!L_5)
		{
			goto IL_0047;
		}
	}
	{
		uint16_t L_6 = m1741(p0, p1, &m1741_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		bool L_7 = m3895(NULL, L_6, &m3895_MI);
		G_B6_0 = ((int32_t)(L_7));
		goto IL_0048;
	}

IL_0047:
	{
		G_B6_0 = 0;
	}

IL_0048:
	{
		return G_B6_0;
	}
}
 void m3897 (t748 * __this, t7** p0, MethodInfo* method){
	t7* V_0 = {0};
	t7* G_B4_0 = {0};
	{
		t7* L_0 = (__this->f6);
		int32_t L_1 = m1715(L_0, &m1715_MI);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_005e;
		}
	}
	{
		t7* L_2 = (__this->f6);
		uint16_t L_3 = m1741(L_2, 0, &m1741_MI);
		if ((((uint32_t)L_3) != ((uint32_t)((int32_t)63))))
		{
			goto IL_0047;
		}
	}
	{
		uint16_t L_4 = ((int32_t)63);
		t29 * L_5 = Box(InitializedTypeInfo(&t194_TI), &L_4);
		t7* L_6 = (__this->f6);
		t7* L_7 = m1770(L_6, 1, &m1770_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		t7* L_8 = m3903(NULL, L_7, 0, &m3903_MI);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_9 = m1312(NULL, L_5, L_8, &m1312_MI);
		G_B4_0 = L_9;
		goto IL_0053;
	}

IL_0047:
	{
		t7* L_10 = (__this->f6);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		t7* L_11 = m3903(NULL, L_10, 0, &m3903_MI);
		G_B4_0 = L_11;
	}

IL_0053:
	{
		V_0 = G_B4_0;
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_12 = m66(NULL, (*((t7**)p0)), V_0, &m66_MI);
		*((t29 **)(p0)) = (t29 *)L_12;
	}

IL_005e:
	{
		t7* L_13 = (__this->f7);
		int32_t L_14 = m1715(L_13, &m1715_MI);
		if ((((int32_t)L_14) <= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		t7* L_15 = (__this->f7);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_16 = m66(NULL, (*((t7**)p0)), L_15, &m66_MI);
		*((t29 **)(p0)) = (t29 *)L_16;
	}

IL_007e:
	{
		return;
	}
}
extern MethodInfo m3898_MI;
 t7* m3898 (t748 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f14);
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		t7* L_1 = (__this->f14);
		return L_1;
	}

IL_0012:
	{
		bool L_2 = (__this->f11);
		if (!L_2)
		{
			goto IL_0035;
		}
	}
	{
		t7* L_3 = m3892(__this, 2, &m3892_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		t7* L_4 = m3903(NULL, L_3, 1, &m3903_MI);
		__this->f14 = L_4;
		goto IL_0047;
	}

IL_0035:
	{
		t7* L_5 = (__this->f5);
		t7* L_6 = (t7*)VirtFuncInvoker1< t7*, t7* >::Invoke(&m3902_MI, __this, L_5);
		__this->f14 = L_6;
	}

IL_0047:
	{
		t7** L_7 = &(__this->f14);
		m3897(__this, L_7, &m3897_MI);
		t7* L_8 = (__this->f14);
		return L_8;
	}
}
 t7* m3899 (t29 * __this, t7* p0, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		t7* L_0 = m3900(NULL, p0, 0, 1, 1, &m3900_MI);
		return L_0;
	}
}
 t7* m3900 (t29 * __this, t7* p0, bool p1, bool p2, bool p3, MethodInfo* method){
	t292 * V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	t781* V_3 = {0};
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	uint16_t V_6 = 0x0;
	{
		if (p0)
		{
			goto IL_000c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		return (((t7_SFs*)(&t7_TI)->static_fields)->f2);
	}

IL_000c:
	{
		t292 * L_0 = (t292 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t292_TI));
		m1311(L_0, &m1311_MI);
		V_0 = L_0;
		int32_t L_1 = m1715(p0, &m1715_MI);
		V_1 = L_1;
		V_2 = 0;
		goto IL_0105;
	}

IL_0020:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		bool L_2 = m3896(NULL, p0, V_2, &m3896_MI);
		if (!L_2)
		{
			goto IL_0044;
		}
	}
	{
		t7* L_3 = m1742(p0, V_2, 3, &m1742_MI);
		m2927(V_0, L_3, &m2927_MI);
		V_2 = ((int32_t)(V_2+2));
		goto IL_0101;
	}

IL_0044:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t943_TI));
		t943 * L_4 = m4091(NULL, &m4091_MI);
		t200* L_5 = ((t200*)SZArrayNew(InitializedTypeInfo(&t200_TI), 1));
		uint16_t L_6 = m1741(p0, V_2, &m1741_MI);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_5, 0)) = (uint16_t)L_6;
		t781* L_7 = (t781*)VirtFuncInvoker1< t781*, t200* >::Invoke(&m4280_MI, L_4, L_5);
		V_3 = L_7;
		V_4 = (((int32_t)(((t20 *)V_3)->max_length)));
		V_5 = 0;
		goto IL_00f8;
	}

IL_006c:
	{
		int32_t L_8 = V_5;
		V_6 = (((uint16_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(V_3, L_8))));
		if ((((int32_t)V_6) <= ((int32_t)((int32_t)32))))
		{
			goto IL_00d6;
		}
	}
	{
		if ((((int32_t)V_6) >= ((int32_t)((int32_t)127))))
		{
			goto IL_00d6;
		}
	}
	{
		int32_t L_9 = m1810((t7*) &_stringLiteral528, V_6, &m1810_MI);
		if ((((uint32_t)L_9) != ((uint32_t)(-1))))
		{
			goto IL_00d6;
		}
	}
	{
		if (!p2)
		{
			goto IL_00a6;
		}
	}
	{
		if ((((int32_t)V_6) == ((int32_t)((int32_t)35))))
		{
			goto IL_00d6;
		}
	}

IL_00a6:
	{
		if (!p3)
		{
			goto IL_00be;
		}
	}
	{
		if ((((int32_t)V_6) == ((int32_t)((int32_t)91))))
		{
			goto IL_00d6;
		}
	}
	{
		if ((((int32_t)V_6) == ((int32_t)((int32_t)93))))
		{
			goto IL_00d6;
		}
	}

IL_00be:
	{
		if (!p1)
		{
			goto IL_00e9;
		}
	}
	{
		int32_t L_10 = m1810((t7*) &_stringLiteral529, V_6, &m1810_MI);
		if ((((int32_t)L_10) == ((int32_t)(-1))))
		{
			goto IL_00e9;
		}
	}

IL_00d6:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		t7* L_11 = m3894(NULL, V_6, &m3894_MI);
		m2927(V_0, L_11, &m2927_MI);
		goto IL_00f2;
	}

IL_00e9:
	{
		m1761(V_0, V_6, &m1761_MI);
	}

IL_00f2:
	{
		V_5 = ((int32_t)(V_5+1));
	}

IL_00f8:
	{
		if ((((int32_t)V_5) < ((int32_t)V_4)))
		{
			goto IL_006c;
		}
	}

IL_0101:
	{
		V_2 = ((int32_t)(V_2+1));
	}

IL_0105:
	{
		if ((((int32_t)V_2) < ((int32_t)V_1)))
		{
			goto IL_0020;
		}
	}
	{
		t7* L_12 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1315_MI, V_0);
		return L_12;
	}
}
 void m3901 (t748 * __this, int32_t p0, MethodInfo* method){
	{
		t7* L_0 = (__this->f1);
		m3907(__this, p0, L_0, &m3907_MI);
		bool L_1 = (__this->f12);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		return;
	}

IL_0019:
	{
		t7* L_2 = (__this->f3);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		t7* L_3 = m3900(NULL, L_2, 0, 1, 0, &m3900_MI);
		__this->f3 = L_3;
		t7* L_4 = (__this->f3);
		int32_t L_5 = m1715(L_4, &m1715_MI);
		if ((((int32_t)L_5) <= ((int32_t)1)))
		{
			goto IL_0086;
		}
	}
	{
		t7* L_6 = (__this->f3);
		uint16_t L_7 = m1741(L_6, 0, &m1741_MI);
		if ((((int32_t)L_7) == ((int32_t)((int32_t)91))))
		{
			goto IL_0086;
		}
	}
	{
		t7* L_8 = (__this->f3);
		t7* L_9 = (__this->f3);
		int32_t L_10 = m1715(L_9, &m1715_MI);
		uint16_t L_11 = m1741(L_8, ((int32_t)(L_10-1)), &m1741_MI);
		if ((((int32_t)L_11) == ((int32_t)((int32_t)93))))
		{
			goto IL_0086;
		}
	}
	{
		t7* L_12 = (__this->f3);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t633_TI));
		t633 * L_13 = m4009(NULL, &m4009_MI);
		t7* L_14 = m4279(L_12, L_13, &m4279_MI);
		__this->f3 = L_14;
	}

IL_0086:
	{
		t7* L_15 = (__this->f5);
		int32_t L_16 = m1715(L_15, &m1715_MI);
		if ((((int32_t)L_16) <= ((int32_t)0)))
		{
			goto IL_00a8;
		}
	}
	{
		t7* L_17 = (__this->f5);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		t7* L_18 = m3899(NULL, L_17, &m3899_MI);
		__this->f5 = L_18;
	}

IL_00a8:
	{
		return;
	}
}
 t7* m3902 (t748 * __this, t7* p0, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		t7* L_0 = m3903(NULL, p0, 0, &m3903_MI);
		return L_0;
	}
}
 t7* m3903 (t29 * __this, t7* p0, bool p1, MethodInfo* method){
	t292 * V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	uint16_t V_3 = 0x0;
	uint16_t V_4 = 0x0;
	uint16_t V_5 = 0x0;
	{
		if (p0)
		{
			goto IL_000c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		return (((t7_SFs*)(&t7_TI)->static_fields)->f2);
	}

IL_000c:
	{
		t292 * L_0 = (t292 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t292_TI));
		m1311(L_0, &m1311_MI);
		V_0 = L_0;
		int32_t L_1 = m1715(p0, &m1715_MI);
		V_1 = L_1;
		V_2 = 0;
		goto IL_00ca;
	}

IL_0020:
	{
		uint16_t L_2 = m1741(p0, V_2, &m1741_MI);
		V_3 = L_2;
		if ((((uint32_t)V_3) != ((uint32_t)((int32_t)37))))
		{
			goto IL_00be;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		uint16_t L_3 = m3911(NULL, p0, (&V_2), (&V_4), &m3911_MI);
		V_5 = L_3;
		if (!p1)
		{
			goto IL_005c;
		}
	}
	{
		if ((((uint32_t)V_5) != ((uint32_t)((int32_t)35))))
		{
			goto IL_005c;
		}
	}
	{
		m2927(V_0, (t7*) &_stringLiteral530, &m2927_MI);
		goto IL_00b5;
	}

IL_005c:
	{
		if (!p1)
		{
			goto IL_007c;
		}
	}
	{
		if ((((uint32_t)V_5) != ((uint32_t)((int32_t)37))))
		{
			goto IL_007c;
		}
	}
	{
		m2927(V_0, (t7*) &_stringLiteral531, &m2927_MI);
		goto IL_00b5;
	}

IL_007c:
	{
		if (!p1)
		{
			goto IL_009c;
		}
	}
	{
		if ((((uint32_t)V_5) != ((uint32_t)((int32_t)63))))
		{
			goto IL_009c;
		}
	}
	{
		m2927(V_0, (t7*) &_stringLiteral532, &m2927_MI);
		goto IL_00b5;
	}

IL_009c:
	{
		m1761(V_0, V_5, &m1761_MI);
		if (!V_4)
		{
			goto IL_00b5;
		}
	}
	{
		m1761(V_0, V_4, &m1761_MI);
	}

IL_00b5:
	{
		V_2 = ((int32_t)(V_2-1));
		goto IL_00c6;
	}

IL_00be:
	{
		m1761(V_0, V_3, &m1761_MI);
	}

IL_00c6:
	{
		V_2 = ((int32_t)(V_2+1));
	}

IL_00ca:
	{
		if ((((int32_t)V_2) < ((int32_t)V_1)))
		{
			goto IL_0020;
		}
	}
	{
		t7* L_4 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1315_MI, V_0);
		return L_4;
	}
}
 void m3904 (t748 * __this, t7* p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		__this->f2 = (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f18);
		__this->f4 = (-1);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		__this->f7 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		__this->f6 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		__this->f9 = 1;
		t200* L_0 = ((t200*)SZArrayNew(InitializedTypeInfo(&t200_TI), 1));
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_0, 0)) = (uint16_t)((int32_t)92);
		t7* L_1 = m4281(p0, L_0, &m4281_MI);
		p0 = L_1;
		int32_t L_2 = m1810(p0, ((int32_t)92), &m1810_MI);
		V_0 = L_2;
		if ((((int32_t)V_0) <= ((int32_t)0)))
		{
			goto IL_0072;
		}
	}
	{
		t7* L_3 = m1770(p0, V_0, &m1770_MI);
		__this->f5 = L_3;
		t7* L_4 = m1742(p0, 0, V_0, &m1742_MI);
		__this->f3 = L_4;
		goto IL_0084;
	}

IL_0072:
	{
		__this->f3 = p0;
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		__this->f5 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
	}

IL_0084:
	{
		t7* L_5 = (__this->f5);
		t7* L_6 = m2932(L_5, (t7*) &_stringLiteral533, (t7*) &_stringLiteral534, &m2932_MI);
		__this->f5 = L_6;
		return;
	}
}
 t7* m3905 (t748 * __this, t7* p0, MethodInfo* method){
	{
		int32_t L_0 = m1715(p0, &m1715_MI);
		if ((((int32_t)L_0) <= ((int32_t)2)))
		{
			goto IL_002e;
		}
	}
	{
		uint16_t L_1 = m1741(p0, 2, &m1741_MI);
		if ((((int32_t)L_1) == ((int32_t)((int32_t)92))))
		{
			goto IL_002e;
		}
	}
	{
		uint16_t L_2 = m1741(p0, 2, &m1741_MI);
		if ((((int32_t)L_2) == ((int32_t)((int32_t)47))))
		{
			goto IL_002e;
		}
	}
	{
		return (t7*) &_stringLiteral535;
	}

IL_002e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		__this->f2 = (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f18);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		__this->f3 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		__this->f4 = (-1);
		t7* L_3 = m2932(p0, (t7*) &_stringLiteral533, (t7*) &_stringLiteral534, &m2932_MI);
		__this->f5 = L_3;
		__this->f7 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		__this->f6 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		return (t7*)NULL;
	}
}
 void m3906 (t748 * __this, t7* p0, MethodInfo* method){
	{
		__this->f0 = 1;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		__this->f2 = (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f18);
		__this->f4 = (-1);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		__this->f7 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		__this->f6 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		__this->f3 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		__this->f5 = (t7*)NULL;
		int32_t L_0 = m1715(p0, &m1715_MI);
		if ((((int32_t)L_0) < ((int32_t)2)))
		{
			goto IL_008f;
		}
	}
	{
		uint16_t L_1 = m1741(p0, 0, &m1741_MI);
		if ((((uint32_t)L_1) != ((uint32_t)((int32_t)47))))
		{
			goto IL_008f;
		}
	}
	{
		uint16_t L_2 = m1741(p0, 1, &m1741_MI);
		if ((((uint32_t)L_2) != ((uint32_t)((int32_t)47))))
		{
			goto IL_008f;
		}
	}
	{
		t200* L_3 = ((t200*)SZArrayNew(InitializedTypeInfo(&t200_TI), 1));
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_3, 0)) = (uint16_t)((int32_t)47);
		t7* L_4 = m4281(p0, L_3, &m4281_MI);
		p0 = L_4;
		uint16_t L_5 = ((int32_t)47);
		t29 * L_6 = Box(InitializedTypeInfo(&t194_TI), &L_5);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_7 = m1312(NULL, L_6, p0, &m1312_MI);
		__this->f5 = L_7;
	}

IL_008f:
	{
		t7* L_8 = (__this->f5);
		if (L_8)
		{
			goto IL_00a1;
		}
	}
	{
		__this->f5 = p0;
	}

IL_00a1:
	{
		return;
	}
}
 void m3907 (t748 * __this, int32_t p0, t7* p1, MethodInfo* method){
	t7* V_0 = {0};
	{
		if (p1)
		{
			goto IL_0011;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral536, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0011:
	{
		t7* L_1 = m3908(__this, p0, p1, &m3908_MI);
		V_0 = L_1;
		if (!V_0)
		{
			goto IL_0027;
		}
	}
	{
		t900 * L_2 = (t900 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t900_TI));
		m3920(L_2, V_0, &m3920_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0027:
	{
		return;
	}
}
 t7* m3908 (t748 * __this, int32_t p0, t7* p1, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	t7* V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	bool V_5 = false;
	bool V_6 = false;
	bool V_7 = false;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	t7* V_10 = {0};
	bool V_11 = false;
	t765 * V_12 = {0};
	t900 * V_13 = {0};
	int32_t G_B50_0 = 0;
	int32_t G_B55_0 = 0;
	int32_t G_B57_0 = 0;
	int32_t G_B139_0 = 0;
	{
		t7* L_0 = m2925(p1, &m2925_MI);
		p1 = L_0;
		int32_t L_1 = m1715(p1, &m1715_MI);
		V_0 = L_1;
		if (V_0)
		{
			goto IL_002b;
		}
	}
	{
		if ((((int32_t)p0) == ((int32_t)2)))
		{
			goto IL_0022;
		}
	}
	{
		if (p0)
		{
			goto IL_002b;
		}
	}

IL_0022:
	{
		__this->f11 = 0;
		return (t7*)NULL;
	}

IL_002b:
	{
		if ((((int32_t)V_0) > ((int32_t)1)))
		{
			goto IL_003f;
		}
	}
	{
		if ((((int32_t)p0) == ((int32_t)2)))
		{
			goto IL_003f;
		}
	}
	{
		return (t7*) &_stringLiteral537;
	}

IL_003f:
	{
		V_1 = 0;
		int32_t L_2 = m1810(p1, ((int32_t)58), &m1810_MI);
		V_1 = L_2;
		if (V_1)
		{
			goto IL_0056;
		}
	}
	{
		return (t7*) &_stringLiteral538;
	}

IL_0056:
	{
		if ((((int32_t)V_1) >= ((int32_t)0)))
		{
			goto IL_00d5;
		}
	}
	{
		uint16_t L_3 = m1741(p1, 0, &m1741_MI);
		if ((((uint32_t)L_3) != ((uint32_t)((int32_t)47))))
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t960_TI));
		if ((((uint32_t)(((t960_SFs*)InitializedTypeInfo(&t960_TI)->static_fields)->f2)) != ((uint32_t)((int32_t)47))))
		{
			goto IL_0091;
		}
	}
	{
		m3906(__this, p1, &m3906_MI);
		if ((((uint32_t)p0) != ((uint32_t)2)))
		{
			goto IL_008c;
		}
	}
	{
		__this->f11 = 0;
	}

IL_008c:
	{
		goto IL_00d3;
	}

IL_0091:
	{
		int32_t L_4 = m1715(p1, &m1715_MI);
		if ((((int32_t)L_4) < ((int32_t)2)))
		{
			goto IL_00c5;
		}
	}
	{
		uint16_t L_5 = m1741(p1, 0, &m1741_MI);
		if ((((uint32_t)L_5) != ((uint32_t)((int32_t)92))))
		{
			goto IL_00c5;
		}
	}
	{
		uint16_t L_6 = m1741(p1, 1, &m1741_MI);
		if ((((uint32_t)L_6) != ((uint32_t)((int32_t)92))))
		{
			goto IL_00c5;
		}
	}
	{
		m3904(__this, p1, &m3904_MI);
		goto IL_00d3;
	}

IL_00c5:
	{
		__this->f11 = 0;
		__this->f5 = p1;
	}

IL_00d3:
	{
		return (t7*)NULL;
	}

IL_00d5:
	{
		if ((((uint32_t)V_1) != ((uint32_t)1)))
		{
			goto IL_0105;
		}
	}
	{
		uint16_t L_7 = m1741(p1, 0, &m1741_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		bool L_8 = m3888(NULL, L_7, &m3888_MI);
		if (L_8)
		{
			goto IL_00f3;
		}
	}
	{
		return (t7*) &_stringLiteral539;
	}

IL_00f3:
	{
		t7* L_9 = m3905(__this, p1, &m3905_MI);
		V_2 = L_9;
		if (!V_2)
		{
			goto IL_0103;
		}
	}
	{
		return V_2;
	}

IL_0103:
	{
		return (t7*)NULL;
	}

IL_0105:
	{
		t7* L_10 = m1742(p1, 0, V_1, &m1742_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t633_TI));
		t633 * L_11 = m4009(NULL, &m4009_MI);
		t7* L_12 = m4279(L_10, L_11, &m4279_MI);
		__this->f2 = L_12;
		t7* L_13 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		bool L_14 = m3887(NULL, L_13, &m3887_MI);
		if (L_14)
		{
			goto IL_0138;
		}
	}
	{
		t7* L_15 = m3068(NULL, (t7*) &_stringLiteral540, &m3068_MI);
		return L_15;
	}

IL_0138:
	{
		V_3 = ((int32_t)(V_1+1));
		int32_t L_16 = m1715(p1, &m1715_MI);
		V_4 = L_16;
		int32_t L_17 = m4282(p1, ((int32_t)35), V_3, &m4282_MI);
		V_1 = L_17;
		bool L_18 = m3881(__this, &m3881_MI);
		if (L_18)
		{
			goto IL_019e;
		}
	}
	{
		if ((((int32_t)V_1) == ((int32_t)(-1))))
		{
			goto IL_019e;
		}
	}
	{
		bool L_19 = (__this->f12);
		if (!L_19)
		{
			goto IL_017d;
		}
	}
	{
		t7* L_20 = m1770(p1, V_1, &m1770_MI);
		__this->f7 = L_20;
		goto IL_019b;
	}

IL_017d:
	{
		t7* L_21 = m1770(p1, ((int32_t)(V_1+1)), &m1770_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		t7* L_22 = m3899(NULL, L_21, &m3899_MI);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_23 = m66(NULL, (t7*) &_stringLiteral541, L_22, &m66_MI);
		__this->f7 = L_23;
	}

IL_019b:
	{
		V_4 = V_1;
	}

IL_019e:
	{
		int32_t L_24 = m4283(p1, ((int32_t)63), V_3, ((int32_t)(V_4-V_3)), &m4283_MI);
		V_1 = L_24;
		if ((((int32_t)V_1) == ((int32_t)(-1))))
		{
			goto IL_01e3;
		}
	}
	{
		t7* L_25 = m1742(p1, V_1, ((int32_t)(V_4-V_1)), &m1742_MI);
		__this->f6 = L_25;
		V_4 = V_1;
		bool L_26 = (__this->f12);
		if (L_26)
		{
			goto IL_01e3;
		}
	}
	{
		t7* L_27 = (__this->f6);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		t7* L_28 = m3899(NULL, L_27, &m3899_MI);
		__this->f6 = L_28;
	}

IL_01e3:
	{
		t7* L_29 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		bool L_30 = m3915(NULL, L_29, &m3915_MI);
		if (!L_30)
		{
			goto IL_0255;
		}
	}
	{
		t7* L_31 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_32 = m1740(NULL, L_31, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f23), &m1740_MI);
		if (!L_32)
		{
			goto IL_0255;
		}
	}
	{
		t7* L_33 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_34 = m1740(NULL, L_33, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f24), &m1740_MI);
		if (!L_34)
		{
			goto IL_0255;
		}
	}
	{
		if ((((int32_t)((int32_t)(V_4-V_3))) < ((int32_t)2)))
		{
			goto IL_024f;
		}
	}
	{
		if ((((int32_t)((int32_t)(V_4-V_3))) < ((int32_t)2)))
		{
			goto IL_0255;
		}
	}
	{
		uint16_t L_35 = m1741(p1, V_3, &m1741_MI);
		if ((((uint32_t)L_35) != ((uint32_t)((int32_t)47))))
		{
			goto IL_0255;
		}
	}
	{
		uint16_t L_36 = m1741(p1, ((int32_t)(V_3+1)), &m1741_MI);
		if ((((int32_t)L_36) == ((int32_t)((int32_t)47))))
		{
			goto IL_0255;
		}
	}

IL_024f:
	{
		return (t7*) &_stringLiteral542;
	}

IL_0255:
	{
		if ((((int32_t)((int32_t)(V_4-V_3))) < ((int32_t)2)))
		{
			goto IL_027c;
		}
	}
	{
		uint16_t L_37 = m1741(p1, V_3, &m1741_MI);
		if ((((uint32_t)L_37) != ((uint32_t)((int32_t)47))))
		{
			goto IL_027c;
		}
	}
	{
		uint16_t L_38 = m1741(p1, ((int32_t)(V_3+1)), &m1741_MI);
		G_B50_0 = ((((int32_t)L_38) == ((int32_t)((int32_t)47)))? 1 : 0);
		goto IL_027d;
	}

IL_027c:
	{
		G_B50_0 = 0;
	}

IL_027d:
	{
		V_5 = G_B50_0;
		t7* L_39 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_40 = m1713(NULL, L_39, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f18), &m1713_MI);
		if (!L_40)
		{
			goto IL_02b7;
		}
	}
	{
		if (!V_5)
		{
			goto IL_02b7;
		}
	}
	{
		if ((((int32_t)((int32_t)(V_4-V_3))) == ((int32_t)2)))
		{
			goto IL_02b4;
		}
	}
	{
		uint16_t L_41 = m1741(p1, ((int32_t)(V_3+2)), &m1741_MI);
		G_B55_0 = ((((int32_t)L_41) == ((int32_t)((int32_t)47)))? 1 : 0);
		goto IL_02b5;
	}

IL_02b4:
	{
		G_B55_0 = 1;
	}

IL_02b5:
	{
		G_B57_0 = G_B55_0;
		goto IL_02b8;
	}

IL_02b7:
	{
		G_B57_0 = 0;
	}

IL_02b8:
	{
		V_6 = G_B57_0;
		V_7 = 0;
		if (!V_5)
		{
			goto IL_03a8;
		}
	}
	{
		if ((((uint32_t)p0) != ((uint32_t)2)))
		{
			goto IL_02d1;
		}
	}
	{
		return (t7*) &_stringLiteral543;
	}

IL_02d1:
	{
		t7* L_42 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_43 = m1740(NULL, L_42, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f23), &m1740_MI);
		if (!L_43)
		{
			goto IL_02ff;
		}
	}
	{
		t7* L_44 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_45 = m1740(NULL, L_44, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f24), &m1740_MI);
		if (!L_45)
		{
			goto IL_02ff;
		}
	}
	{
		V_3 = ((int32_t)(V_3+2));
	}

IL_02ff:
	{
		t7* L_46 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_47 = m1713(NULL, L_46, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f18), &m1713_MI);
		if (!L_47)
		{
			goto IL_0383;
		}
	}
	{
		V_8 = 2;
		V_9 = V_3;
		goto IL_033f;
	}

IL_031f:
	{
		uint16_t L_48 = m1741(p1, V_9, &m1741_MI);
		if ((((int32_t)L_48) == ((int32_t)((int32_t)47))))
		{
			goto IL_0333;
		}
	}
	{
		goto IL_0348;
	}

IL_0333:
	{
		V_8 = ((int32_t)(V_8+1));
		V_9 = ((int32_t)(V_9+1));
	}

IL_033f:
	{
		if ((((int32_t)V_9) < ((int32_t)V_4)))
		{
			goto IL_031f;
		}
	}

IL_0348:
	{
		if ((((int32_t)V_8) < ((int32_t)4)))
		{
			goto IL_0377;
		}
	}
	{
		V_6 = 0;
		goto IL_035c;
	}

IL_0358:
	{
		V_3 = ((int32_t)(V_3+1));
	}

IL_035c:
	{
		if ((((int32_t)V_3) >= ((int32_t)V_4)))
		{
			goto IL_0372;
		}
	}
	{
		uint16_t L_49 = m1741(p1, V_3, &m1741_MI);
		if ((((int32_t)L_49) == ((int32_t)((int32_t)47))))
		{
			goto IL_0358;
		}
	}

IL_0372:
	{
		goto IL_0383;
	}

IL_0377:
	{
		if ((((int32_t)V_8) < ((int32_t)3)))
		{
			goto IL_0383;
		}
	}
	{
		V_3 = ((int32_t)(V_3+1));
	}

IL_0383:
	{
		if ((((int32_t)((int32_t)(V_4-V_3))) <= ((int32_t)1)))
		{
			goto IL_03a3;
		}
	}
	{
		uint16_t L_50 = m1741(p1, ((int32_t)(V_3+1)), &m1741_MI);
		if ((((uint32_t)L_50) != ((uint32_t)((int32_t)58))))
		{
			goto IL_03a3;
		}
	}
	{
		V_6 = 0;
		V_7 = 1;
	}

IL_03a3:
	{
		goto IL_03d2;
	}

IL_03a8:
	{
		t7* L_51 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		bool L_52 = m3915(NULL, L_51, &m3915_MI);
		if (L_52)
		{
			goto IL_03d2;
		}
	}
	{
		t7* L_53 = m1742(p1, V_3, ((int32_t)(V_4-V_3)), &m1742_MI);
		__this->f5 = L_53;
		__this->f10 = 1;
		return (t7*)NULL;
	}

IL_03d2:
	{
		if (!V_6)
		{
			goto IL_03e0;
		}
	}
	{
		V_1 = (-1);
		goto IL_040a;
	}

IL_03e0:
	{
		int32_t L_54 = m4283(p1, ((int32_t)47), V_3, ((int32_t)(V_4-V_3)), &m4283_MI);
		V_1 = L_54;
		if ((((uint32_t)V_1) != ((uint32_t)(-1))))
		{
			goto IL_040a;
		}
	}
	{
		if (!V_7)
		{
			goto IL_040a;
		}
	}
	{
		int32_t L_55 = m4283(p1, ((int32_t)92), V_3, ((int32_t)(V_4-V_3)), &m4283_MI);
		V_1 = L_55;
	}

IL_040a:
	{
		if ((((uint32_t)V_1) != ((uint32_t)(-1))))
		{
			goto IL_044b;
		}
	}
	{
		t7* L_56 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_57 = m1740(NULL, L_56, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f23), &m1740_MI);
		if (!L_57)
		{
			goto IL_0446;
		}
	}
	{
		t7* L_58 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_59 = m1740(NULL, L_58, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f24), &m1740_MI);
		if (!L_59)
		{
			goto IL_0446;
		}
	}
	{
		__this->f5 = (t7*) &_stringLiteral534;
	}

IL_0446:
	{
		goto IL_045f;
	}

IL_044b:
	{
		t7* L_60 = m1742(p1, V_1, ((int32_t)(V_4-V_1)), &m1742_MI);
		__this->f5 = L_60;
		V_4 = V_1;
	}

IL_045f:
	{
		if (!V_6)
		{
			goto IL_046d;
		}
	}
	{
		V_1 = (-1);
		goto IL_047b;
	}

IL_046d:
	{
		int32_t L_61 = m4283(p1, ((int32_t)64), V_3, ((int32_t)(V_4-V_3)), &m4283_MI);
		V_1 = L_61;
	}

IL_047b:
	{
		if ((((int32_t)V_1) == ((int32_t)(-1))))
		{
			goto IL_0496;
		}
	}
	{
		t7* L_62 = m1742(p1, V_3, ((int32_t)(V_1-V_3)), &m1742_MI);
		__this->f8 = L_62;
		V_3 = ((int32_t)(V_1+1));
	}

IL_0496:
	{
		__this->f4 = (-1);
		if (!V_6)
		{
			goto IL_04ab;
		}
	}
	{
		V_1 = (-1);
		goto IL_04bc;
	}

IL_04ab:
	{
		int32_t L_63 = m4284(p1, ((int32_t)58), ((int32_t)(V_4-1)), ((int32_t)(V_4-V_3)), &m4284_MI);
		V_1 = L_63;
	}

IL_04bc:
	{
		if ((((int32_t)V_1) == ((int32_t)(-1))))
		{
			goto IL_0566;
		}
	}
	{
		if ((((int32_t)V_1) == ((int32_t)((int32_t)(V_4-1)))))
		{
			goto IL_0566;
		}
	}
	{
		t7* L_64 = m1742(p1, ((int32_t)(V_1+1)), ((int32_t)(V_4-((int32_t)(V_1+1)))), &m1742_MI);
		V_10 = L_64;
		int32_t L_65 = m1715(V_10, &m1715_MI);
		if ((((int32_t)L_65) <= ((int32_t)0)))
		{
			goto IL_0544;
		}
	}
	{
		int32_t L_66 = m1715(V_10, &m1715_MI);
		uint16_t L_67 = m1741(V_10, ((int32_t)(L_66-1)), &m1741_MI);
		if ((((int32_t)L_67) == ((int32_t)((int32_t)93))))
		{
			goto IL_0544;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t633_TI));
		t633 * L_68 = m4009(NULL, &m4009_MI);
		int32_t* L_69 = &(__this->f4);
		bool L_70 = m4010(NULL, V_10, 7, L_68, L_69, &m4010_MI);
		if (!L_70)
		{
			goto IL_0536;
		}
	}
	{
		int32_t L_71 = (__this->f4);
		if ((((int32_t)L_71) < ((int32_t)0)))
		{
			goto IL_0536;
		}
	}
	{
		int32_t L_72 = (__this->f4);
		if ((((int32_t)L_72) <= ((int32_t)((int32_t)65535))))
		{
			goto IL_053c;
		}
	}

IL_0536:
	{
		return (t7*) &_stringLiteral544;
	}

IL_053c:
	{
		V_4 = V_1;
		goto IL_0561;
	}

IL_0544:
	{
		int32_t L_73 = (__this->f4);
		if ((((uint32_t)L_73) != ((uint32_t)(-1))))
		{
			goto IL_0561;
		}
	}
	{
		t7* L_74 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		int32_t L_75 = m3913(NULL, L_74, &m3913_MI);
		__this->f4 = L_75;
	}

IL_0561:
	{
		goto IL_0583;
	}

IL_0566:
	{
		int32_t L_76 = (__this->f4);
		if ((((uint32_t)L_76) != ((uint32_t)(-1))))
		{
			goto IL_0583;
		}
	}
	{
		t7* L_77 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		int32_t L_78 = m3913(NULL, L_77, &m3913_MI);
		__this->f4 = L_78;
	}

IL_0583:
	{
		t7* L_79 = m1742(p1, V_3, ((int32_t)(V_4-V_3)), &m1742_MI);
		p1 = L_79;
		__this->f3 = p1;
		if (!V_6)
		{
			goto IL_05c7;
		}
	}
	{
		uint16_t L_80 = ((int32_t)47);
		t29 * L_81 = Box(InitializedTypeInfo(&t194_TI), &L_80);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_82 = m1312(NULL, L_81, p1, &m1312_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		t7* L_83 = m3910(NULL, L_82, 1, &m3910_MI);
		__this->f5 = L_83;
		__this->f3 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		goto IL_071c;
	}

IL_05c7:
	{
		t7* L_84 = (__this->f3);
		int32_t L_85 = m1715(L_84, &m1715_MI);
		if ((((uint32_t)L_85) != ((uint32_t)2)))
		{
			goto IL_0612;
		}
	}
	{
		t7* L_86 = (__this->f3);
		uint16_t L_87 = m1741(L_86, 1, &m1741_MI);
		if ((((uint32_t)L_87) != ((uint32_t)((int32_t)58))))
		{
			goto IL_0612;
		}
	}
	{
		t7* L_88 = (__this->f3);
		t7* L_89 = (__this->f5);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_90 = m66(NULL, L_88, L_89, &m66_MI);
		__this->f5 = L_90;
		__this->f3 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		goto IL_071c;
	}

IL_0612:
	{
		bool L_91 = (__this->f0);
		if (!L_91)
		{
			goto IL_063a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_92 = m66(NULL, (t7*) &_stringLiteral545, p1, &m66_MI);
		p1 = L_92;
		__this->f3 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		goto IL_071c;
	}

IL_063a:
	{
		t7* L_93 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_94 = m1713(NULL, L_93, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f18), &m1713_MI);
		if (!L_94)
		{
			goto IL_065b;
		}
	}
	{
		__this->f9 = 1;
		goto IL_071c;
	}

IL_065b:
	{
		t7* L_95 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_96 = m1713(NULL, L_95, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f24), &m1713_MI);
		if (!L_96)
		{
			goto IL_069d;
		}
	}
	{
		t7* L_97 = (__this->f3);
		int32_t L_98 = m1715(L_97, &m1715_MI);
		if ((((int32_t)L_98) <= ((int32_t)0)))
		{
			goto IL_0698;
		}
	}
	{
		t7* L_99 = (__this->f3);
		__this->f5 = L_99;
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		__this->f3 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
	}

IL_0698:
	{
		goto IL_071c;
	}

IL_069d:
	{
		t7* L_100 = (__this->f3);
		int32_t L_101 = m1715(L_100, &m1715_MI);
		if (L_101)
		{
			goto IL_071c;
		}
	}
	{
		t7* L_102 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_103 = m1713(NULL, L_102, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f21), &m1713_MI);
		if (L_103)
		{
			goto IL_0716;
		}
	}
	{
		t7* L_104 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_105 = m1713(NULL, L_104, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f20), &m1713_MI);
		if (L_105)
		{
			goto IL_0716;
		}
	}
	{
		t7* L_106 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_107 = m1713(NULL, L_106, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f25), &m1713_MI);
		if (L_107)
		{
			goto IL_0716;
		}
	}
	{
		t7* L_108 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_109 = m1713(NULL, L_108, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f22), &m1713_MI);
		if (L_109)
		{
			goto IL_0716;
		}
	}
	{
		t7* L_110 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_111 = m1713(NULL, L_110, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f19), &m1713_MI);
		if (!L_111)
		{
			goto IL_071c;
		}
	}

IL_0716:
	{
		return (t7*) &_stringLiteral546;
	}

IL_071c:
	{
		t7* L_112 = (__this->f3);
		int32_t L_113 = m1715(L_112, &m1715_MI);
		if ((((int32_t)L_113) <= ((int32_t)0)))
		{
			goto IL_073d;
		}
	}
	{
		t7* L_114 = (__this->f3);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		int32_t L_115 = m3884(NULL, L_114, &m3884_MI);
		G_B139_0 = ((((int32_t)L_115) == ((int32_t)0))? 1 : 0);
		goto IL_073e;
	}

IL_073d:
	{
		G_B139_0 = 0;
	}

IL_073e:
	{
		V_11 = G_B139_0;
		if (V_11)
		{
			goto IL_07c1;
		}
	}
	{
		t7* L_116 = (__this->f3);
		int32_t L_117 = m1715(L_116, &m1715_MI);
		if ((((int32_t)L_117) <= ((int32_t)1)))
		{
			goto IL_07c1;
		}
	}
	{
		t7* L_118 = (__this->f3);
		uint16_t L_119 = m1741(L_118, 0, &m1741_MI);
		if ((((uint32_t)L_119) != ((uint32_t)((int32_t)91))))
		{
			goto IL_07c1;
		}
	}
	{
		t7* L_120 = (__this->f3);
		t7* L_121 = (__this->f3);
		int32_t L_122 = m1715(L_121, &m1715_MI);
		uint16_t L_123 = m1741(L_120, ((int32_t)(L_122-1)), &m1741_MI);
		if ((((uint32_t)L_123) != ((uint32_t)((int32_t)93))))
		{
			goto IL_07c1;
		}
	}
	{
		t7* L_124 = (__this->f3);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t765_TI));
		bool L_125 = m3207(NULL, L_124, (&V_12), &m3207_MI);
		if (!L_125)
		{
			goto IL_07be;
		}
	}
	{
		t7* L_126 = m3217(V_12, 1, &m3217_MI);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_127 = m1685(NULL, (t7*) &_stringLiteral175, L_126, (t7*) &_stringLiteral176, &m1685_MI);
		__this->f3 = L_127;
		goto IL_07c1;
	}

IL_07be:
	{
		V_11 = 1;
	}

IL_07c1:
	{
		if (!V_11)
		{
			goto IL_07fe;
		}
	}
	{
		t893 * L_128 = m3916(__this, &m3916_MI);
		if (((t892 *)IsInst(L_128, InitializedTypeInfo(&t892_TI))))
		{
			goto IL_07e3;
		}
	}
	{
		t893 * L_129 = m3916(__this, &m3916_MI);
		if (L_129)
		{
			goto IL_07fe;
		}
	}

IL_07e3:
	{
		t7* L_130 = (__this->f3);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_131 = m1685(NULL, (t7*) &_stringLiteral547, L_130, (t7*) &_stringLiteral60, &m1685_MI);
		t7* L_132 = m3068(NULL, L_131, &m3068_MI);
		return L_132;
	}

IL_07fe:
	{
		V_13 = (t900 *)NULL;
		t893 * L_133 = m3916(__this, &m3916_MI);
		if (!L_133)
		{
			goto IL_081a;
		}
	}
	{
		t893 * L_134 = m3916(__this, &m3916_MI);
		VirtActionInvoker2< t748 *, t900 ** >::Invoke(&m3925_MI, L_134, __this, (&V_13));
	}

IL_081a:
	{
		if (!V_13)
		{
			goto IL_0829;
		}
	}
	{
		t7* L_135 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1684_MI, V_13);
		return L_135;
	}

IL_0829:
	{
		t7* L_136 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_137 = m1740(NULL, L_136, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f23), &m1740_MI);
		if (!L_137)
		{
			goto IL_0884;
		}
	}
	{
		t7* L_138 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_139 = m1740(NULL, L_138, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f24), &m1740_MI);
		if (!L_139)
		{
			goto IL_0884;
		}
	}
	{
		t7* L_140 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_141 = m1740(NULL, L_140, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f18), &m1740_MI);
		if (!L_141)
		{
			goto IL_0884;
		}
	}
	{
		t7* L_142 = (__this->f5);
		t7* L_143 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		bool L_144 = m3909(NULL, L_143, &m3909_MI);
		t7* L_145 = m3910(NULL, L_142, L_144, &m3910_MI);
		__this->f5 = L_145;
	}

IL_0884:
	{
		return (t7*)NULL;
	}
}
 bool m3909 (t29 * __this, t7* p0, MethodInfo* method){
	t7* V_0 = {0};
	t485 * V_1 = {0};
	int32_t V_2 = 0;
	{
		V_0 = p0;
		if (!V_0)
		{
			goto IL_007a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		if ((((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f31))
		{
			goto IL_005b;
		}
	}
	{
		t485 * L_0 = (t485 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t485_TI));
		m4038(L_0, 5, &m4038_MI);
		V_1 = L_0;
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral336, 0);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral278, 0);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral277, 0);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral523, 0);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral524, 0);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f31 = V_1;
	}

IL_005b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		bool L_1 = (bool)VirtFuncInvoker2< bool, t7*, int32_t* >::Invoke(&m4040_MI, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f31), V_0, (&V_2));
		if (!L_1)
		{
			goto IL_007a;
		}
	}
	{
		if (!V_2)
		{
			goto IL_0078;
		}
	}
	{
		goto IL_007a;
	}

IL_0078:
	{
		return 1;
	}

IL_007a:
	{
		return 0;
	}
}
 t7* m3910 (t29 * __this, t7* p0, bool p1, MethodInfo* method){
	t292 * V_0 = {0};
	int32_t V_1 = 0;
	uint16_t V_2 = 0x0;
	uint16_t V_3 = 0x0;
	uint16_t V_4 = 0x0;
	t731 * V_5 = {0};
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	t7* V_8 = {0};
	int32_t V_9 = 0;
	bool V_10 = false;
	t7* V_11 = {0};
	t29 * V_12 = {0};
	uint16_t V_13 = 0x0;
	t29 * V_14 = {0};
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_0 = m1713(NULL, p0, (t7*) &_stringLiteral534, &m1713_MI);
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		return p0;
	}

IL_0012:
	{
		t292 * L_1 = (t292 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t292_TI));
		m1311(L_1, &m1311_MI);
		V_0 = L_1;
		if (!p1)
		{
			goto IL_00f5;
		}
	}
	{
		V_1 = 0;
		goto IL_00dc;
	}

IL_0025:
	{
		uint16_t L_2 = m1741(p0, V_1, &m1741_MI);
		V_2 = L_2;
		V_13 = V_2;
		if ((((int32_t)V_13) == ((int32_t)((int32_t)37))))
		{
			goto IL_0055;
		}
	}
	{
		if ((((int32_t)V_13) == ((int32_t)((int32_t)92))))
		{
			goto IL_0047;
		}
	}
	{
		goto IL_00cb;
	}

IL_0047:
	{
		m1761(V_0, ((int32_t)47), &m1761_MI);
		goto IL_00d8;
	}

IL_0055:
	{
		int32_t L_3 = m1715(p0, &m1715_MI);
		if ((((int32_t)V_1) >= ((int32_t)((int32_t)(L_3-2)))))
		{
			goto IL_00be;
		}
	}
	{
		uint16_t L_4 = m1741(p0, ((int32_t)(V_1+1)), &m1741_MI);
		V_3 = L_4;
		uint16_t L_5 = m1741(p0, ((int32_t)(V_1+2)), &m1741_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t194_TI));
		uint16_t L_6 = m1807(NULL, L_5, &m1807_MI);
		V_4 = L_6;
		if ((((uint32_t)V_3) != ((uint32_t)((int32_t)50))))
		{
			goto IL_008e;
		}
	}
	{
		if ((((int32_t)V_4) == ((int32_t)((int32_t)70))))
		{
			goto IL_009f;
		}
	}

IL_008e:
	{
		if ((((uint32_t)V_3) != ((uint32_t)((int32_t)53))))
		{
			goto IL_00b1;
		}
	}
	{
		if ((((uint32_t)V_4) != ((uint32_t)((int32_t)67))))
		{
			goto IL_00b1;
		}
	}

IL_009f:
	{
		m1761(V_0, ((int32_t)47), &m1761_MI);
		V_1 = ((int32_t)(V_1+2));
		goto IL_00b9;
	}

IL_00b1:
	{
		m1761(V_0, V_2, &m1761_MI);
	}

IL_00b9:
	{
		goto IL_00c6;
	}

IL_00be:
	{
		m1761(V_0, V_2, &m1761_MI);
	}

IL_00c6:
	{
		goto IL_00d8;
	}

IL_00cb:
	{
		m1761(V_0, V_2, &m1761_MI);
		goto IL_00d8;
	}

IL_00d8:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_00dc:
	{
		int32_t L_7 = m1715(p0, &m1715_MI);
		if ((((int32_t)V_1) < ((int32_t)L_7)))
		{
			goto IL_0025;
		}
	}
	{
		t7* L_8 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1315_MI, V_0);
		p0 = L_8;
		goto IL_0101;
	}

IL_00f5:
	{
		t7* L_9 = m2933(p0, ((int32_t)92), ((int32_t)47), &m2933_MI);
		p0 = L_9;
	}

IL_0101:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t731_TI));
		t731 * L_10 = (t731 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t731_TI));
		m3980(L_10, &m3980_MI);
		V_5 = L_10;
		V_6 = 0;
		goto IL_01a3;
	}

IL_0110:
	{
		int32_t L_11 = m4282(p0, ((int32_t)47), V_6, &m4282_MI);
		V_7 = L_11;
		if ((((uint32_t)V_7) != ((uint32_t)(-1))))
		{
			goto IL_012c;
		}
	}
	{
		int32_t L_12 = m1715(p0, &m1715_MI);
		V_7 = L_12;
	}

IL_012c:
	{
		t7* L_13 = m1742(p0, V_6, ((int32_t)(V_7-V_6)), &m1742_MI);
		V_8 = L_13;
		V_6 = ((int32_t)(V_7+1));
		int32_t L_14 = m1715(V_8, &m1715_MI);
		if (!L_14)
		{
			goto IL_015e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_15 = m1713(NULL, V_8, (t7*) &_stringLiteral52, &m1713_MI);
		if (!L_15)
		{
			goto IL_0163;
		}
	}

IL_015e:
	{
		goto IL_01a3;
	}

IL_0163:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_16 = m1713(NULL, V_8, (t7*) &_stringLiteral548, &m1713_MI);
		if (!L_16)
		{
			goto IL_0199;
		}
	}
	{
		int32_t L_17 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m3976_MI, V_5);
		V_9 = L_17;
		if (V_9)
		{
			goto IL_0189;
		}
	}
	{
		goto IL_01a3;
	}

IL_0189:
	{
		VirtActionInvoker1< int32_t >::Invoke(&m4265_MI, V_5, ((int32_t)(V_9-1)));
		goto IL_01a3;
	}

IL_0199:
	{
		VirtFuncInvoker1< int32_t, t29 * >::Invoke(&m3991_MI, V_5, V_8);
	}

IL_01a3:
	{
		int32_t L_18 = m1715(p0, &m1715_MI);
		if ((((int32_t)V_6) < ((int32_t)L_18)))
		{
			goto IL_0110;
		}
	}
	{
		int32_t L_19 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m3976_MI, V_5);
		if (L_19)
		{
			goto IL_01c2;
		}
	}
	{
		return (t7*) &_stringLiteral534;
	}

IL_01c2:
	{
		m4285(V_0, 0, &m4285_MI);
		uint16_t L_20 = m1741(p0, 0, &m1741_MI);
		if ((((uint32_t)L_20) != ((uint32_t)((int32_t)47))))
		{
			goto IL_01e0;
		}
	}
	{
		m1761(V_0, ((int32_t)47), &m1761_MI);
	}

IL_01e0:
	{
		V_10 = 1;
		t29 * L_21 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m3981_MI, V_5);
		V_12 = L_21;
	}

IL_01ec:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0220;
		}

IL_01f1:
		{
			t29 * L_22 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m3970_MI, V_12);
			V_11 = ((t7*)Castclass(L_22, (&t7_TI)));
			if (!V_10)
			{
				goto IL_020e;
			}
		}

IL_0206:
		{
			V_10 = 0;
			goto IL_0217;
		}

IL_020e:
		{
			m1761(V_0, ((int32_t)47), &m1761_MI);
		}

IL_0217:
		{
			m2927(V_0, V_11, &m2927_MI);
		}

IL_0220:
		{
			bool L_23 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m3972_MI, V_12);
			if (L_23)
			{
				goto IL_01f1;
			}
		}

IL_022c:
		{
			// IL_022c: leave IL_0247
			leaveInstructions[0] = 0x247; // 1
			THROW_SENTINEL(IL_0247);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_0231;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_0231;
	}

IL_0231:
	{ // begin finally (depth: 1)
		{
			V_14 = ((t29 *)IsInst(V_12, InitializedTypeInfo(&t324_TI)));
			if (V_14)
			{
				goto IL_023f;
			}
		}

IL_023e:
		{
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x247:
					goto IL_0247;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}

IL_023f:
		{
			InterfaceActionInvoker0::Invoke(&m1428_MI, V_14);
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x247:
					goto IL_0247;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}
	} // end finally (depth: 1)

IL_0247:
	{
		bool L_24 = m2930(p0, (t7*) &_stringLiteral534, &m2930_MI);
		if (!L_24)
		{
			goto IL_0260;
		}
	}
	{
		m1761(V_0, ((int32_t)47), &m1761_MI);
	}

IL_0260:
	{
		t7* L_25 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1315_MI, V_0);
		return L_25;
	}
}
 uint16_t m3911 (t29 * __this, t7* p0, int32_t* p1, uint16_t* p2, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	t781* V_5 = {0};
	bool V_6 = false;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	uint8_t V_10 = 0x0;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	{
		*((int16_t*)(p2)) = (int16_t)0;
		if (p0)
		{
			goto IL_0014;
		}
	}
	{
		t305 * L_0 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_0, (t7*) &_stringLiteral473, &m1935_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0014:
	{
		if ((((int32_t)(*((int32_t*)p1))) < ((int32_t)0)))
		{
			goto IL_0029;
		}
	}
	{
		int32_t L_1 = m1715(p0, &m1715_MI);
		if ((((int32_t)(*((int32_t*)p1))) < ((int32_t)L_1)))
		{
			goto IL_0034;
		}
	}

IL_0029:
	{
		t915 * L_2 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_2, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0034:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		bool L_3 = m3896(NULL, p0, (*((int32_t*)p1)), &m3896_MI);
		if (L_3)
		{
			goto IL_0053;
		}
	}
	{
		int32_t L_4 = (*((int32_t*)p1));
		V_13 = L_4;
		*((int32_t*)(p1)) = (int32_t)((int32_t)(L_4+1));
		uint16_t L_5 = m1741(p0, V_13, &m1741_MI);
		return L_5;
	}

IL_0053:
	{
		int32_t L_6 = (*((int32_t*)p1));
		V_13 = L_6;
		*((int32_t*)(p1)) = (int32_t)((int32_t)(L_6+1));
		V_0 = V_13;
		int32_t L_7 = (*((int32_t*)p1));
		V_13 = L_7;
		*((int32_t*)(p1)) = (int32_t)((int32_t)(L_7+1));
		uint16_t L_8 = m1741(p0, V_13, &m1741_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		int32_t L_9 = m3893(NULL, L_8, &m3893_MI);
		V_1 = L_9;
		int32_t L_10 = (*((int32_t*)p1));
		V_13 = L_10;
		*((int32_t*)(p1)) = (int32_t)((int32_t)(L_10+1));
		uint16_t L_11 = m1741(p0, V_13, &m1741_MI);
		int32_t L_12 = m3893(NULL, L_11, &m3893_MI);
		V_2 = L_12;
		V_3 = V_1;
		V_4 = 0;
		goto IL_00a1;
	}

IL_0097:
	{
		V_4 = ((int32_t)(V_4+1));
		V_3 = ((int32_t)((int32_t)V_3<<(int32_t)1));
	}

IL_00a1:
	{
		if ((((int32_t)((int32_t)((int32_t)V_3&(int32_t)8))) == ((int32_t)8)))
		{
			goto IL_0097;
		}
	}
	{
		if ((((int32_t)V_4) > ((int32_t)1)))
		{
			goto IL_00b9;
		}
	}
	{
		return (((uint16_t)((int32_t)((int32_t)((int32_t)((int32_t)V_1<<(int32_t)4))|(int32_t)V_2))));
	}

IL_00b9:
	{
		V_5 = ((t781*)SZArrayNew(InitializedTypeInfo(&t781_TI), V_4));
		V_6 = 0;
		*((uint8_t*)(uint8_t*)SZArrayLdElema(V_5, 0)) = (uint8_t)(((uint8_t)((int32_t)((int32_t)((int32_t)((int32_t)V_1<<(int32_t)4))|(int32_t)V_2))));
		V_7 = 1;
		goto IL_014b;
	}

IL_00d7:
	{
		int32_t L_13 = (*((int32_t*)p1));
		V_13 = L_13;
		*((int32_t*)(p1)) = (int32_t)((int32_t)(L_13+1));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		bool L_14 = m3896(NULL, p0, V_13, &m3896_MI);
		if (L_14)
		{
			goto IL_00f5;
		}
	}
	{
		V_6 = 1;
		goto IL_0154;
	}

IL_00f5:
	{
		int32_t L_15 = (*((int32_t*)p1));
		V_13 = L_15;
		*((int32_t*)(p1)) = (int32_t)((int32_t)(L_15+1));
		uint16_t L_16 = m1741(p0, V_13, &m1741_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		int32_t L_17 = m3893(NULL, L_16, &m3893_MI);
		V_8 = L_17;
		if ((((int32_t)((int32_t)((int32_t)V_8&(int32_t)((int32_t)12)))) == ((int32_t)8)))
		{
			goto IL_0120;
		}
	}
	{
		V_6 = 1;
		goto IL_0154;
	}

IL_0120:
	{
		int32_t L_18 = (*((int32_t*)p1));
		V_13 = L_18;
		*((int32_t*)(p1)) = (int32_t)((int32_t)(L_18+1));
		uint16_t L_19 = m1741(p0, V_13, &m1741_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		int32_t L_20 = m3893(NULL, L_19, &m3893_MI);
		V_9 = L_20;
		*((uint8_t*)(uint8_t*)SZArrayLdElema(V_5, V_7)) = (uint8_t)(((uint8_t)((int32_t)((int32_t)((int32_t)((int32_t)V_8<<(int32_t)4))|(int32_t)V_9))));
		V_7 = ((int32_t)(V_7+1));
	}

IL_014b:
	{
		if ((((int32_t)V_7) < ((int32_t)V_4)))
		{
			goto IL_00d7;
		}
	}

IL_0154:
	{
		if (!V_6)
		{
			goto IL_0166;
		}
	}
	{
		*((int32_t*)(p1)) = (int32_t)((int32_t)(V_0+3));
		int32_t L_21 = 0;
		return (((uint16_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(V_5, L_21))));
	}

IL_0166:
	{
		V_10 = ((int32_t)255);
		V_10 = (((uint8_t)((int32_t)((int32_t)V_10>>(int32_t)((int32_t)((int32_t)((int32_t)(V_4+1))&(int32_t)((int32_t)31)))))));
		int32_t L_22 = 0;
		V_11 = ((int32_t)((int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(V_5, L_22))&(int32_t)V_10));
		V_12 = 1;
		goto IL_01a4;
	}

IL_018b:
	{
		V_11 = ((int32_t)((int32_t)V_11<<(int32_t)6));
		int32_t L_23 = V_12;
		V_11 = ((int32_t)((int32_t)V_11|(int32_t)((int32_t)((int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(V_5, L_23))&(int32_t)((int32_t)63)))));
		V_12 = ((int32_t)(V_12+1));
	}

IL_01a4:
	{
		if ((((int32_t)V_12) < ((int32_t)V_4)))
		{
			goto IL_018b;
		}
	}
	{
		if ((((int32_t)V_11) > ((int32_t)((int32_t)65535))))
		{
			goto IL_01bd;
		}
	}
	{
		return (((uint16_t)V_11));
	}

IL_01bd:
	{
		V_11 = ((int32_t)(V_11-((int32_t)65536)));
		*((int16_t*)(p2)) = (int16_t)(((uint16_t)((int32_t)((int32_t)((int32_t)((int32_t)V_11&(int32_t)((int32_t)1023)))|(int32_t)((int32_t)56320)))));
		return (((uint16_t)((int32_t)((int32_t)((int32_t)((int32_t)V_11>>(int32_t)((int32_t)10)))|(int32_t)((int32_t)55296)))));
	}
}
 t7* m3912 (t29 * __this, t7* p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0037;
	}

IL_0007:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		t7* L_0 = (((t895 *)(t895 *)SZArrayLdElema((((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f28), V_0))->f0);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_1 = m1713(NULL, L_0, p0, &m1713_MI);
		if (!L_1)
		{
			goto IL_0033;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		t7* L_2 = (((t895 *)(t895 *)SZArrayLdElema((((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f28), V_0))->f1);
		return L_2;
	}

IL_0033:
	{
		V_0 = ((int32_t)(V_0+1));
	}

IL_0037:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		if ((((int32_t)V_0) < ((int32_t)(((int32_t)(((t20 *)(((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f28))->max_length))))))
		{
			goto IL_0007;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		return (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f17);
	}
}
 int32_t m3913 (t29 * __this, t7* p0, MethodInfo* method){
	t893 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t893_TI));
		t893 * L_0 = m3932(NULL, p0, &m3932_MI);
		V_0 = L_0;
		if (V_0)
		{
			goto IL_000f;
		}
	}
	{
		return (-1);
	}

IL_000f:
	{
		int32_t L_1 = m3928(V_0, &m3928_MI);
		return L_1;
	}
}
 t7* m3914 (t748 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->f10);
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		return (t7*) &_stringLiteral182;
	}

IL_0011:
	{
		t7* L_1 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		t7* L_2 = m3912(NULL, L_1, &m3912_MI);
		return L_2;
	}
}
 bool m3915 (t29 * __this, t7* p0, MethodInfo* method){
	t7* V_0 = {0};
	t485 * V_1 = {0};
	int32_t V_2 = 0;
	{
		V_0 = p0;
		if (!V_0)
		{
			goto IL_00b7;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		if ((((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f32))
		{
			goto IL_0098;
		}
	}
	{
		t485 * L_0 = (t485 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t485_TI));
		m4038(L_0, ((int32_t)10), &m4038_MI);
		V_1 = L_0;
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral278, 0);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral277, 0);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral336, 0);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral338, 0);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral522, 0);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral519, 0);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral520, 0);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral521, 0);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral523, 0);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, V_1, (t7*) &_stringLiteral524, 0);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f32 = V_1;
	}

IL_0098:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		bool L_1 = (bool)VirtFuncInvoker2< bool, t7*, int32_t* >::Invoke(&m4040_MI, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f32), V_0, (&V_2));
		if (!L_1)
		{
			goto IL_00b7;
		}
	}
	{
		if (!V_2)
		{
			goto IL_00b5;
		}
	}
	{
		goto IL_00b7;
	}

IL_00b5:
	{
		return 1;
	}

IL_00b7:
	{
		return 0;
	}
}
 t893 * m3916 (t748 * __this, MethodInfo* method){
	{
		t893 * L_0 = (__this->f29);
		if (L_0)
		{
			goto IL_0037;
		}
	}
	{
		t7* L_1 = m3882(__this, &m3882_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t893_TI));
		t893 * L_2 = m3932(NULL, L_1, &m3932_MI);
		__this->f29 = L_2;
		t893 * L_3 = (__this->f29);
		if (L_3)
		{
			goto IL_0037;
		}
	}
	{
		t892 * L_4 = (t892 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t892_TI));
		m3869(L_4, (t7*) &_stringLiteral515, &m3869_MI);
		__this->f29 = L_4;
	}

IL_0037:
	{
		t893 * L_5 = (__this->f29);
		return L_5;
	}
}
 void m3917 (t748 * __this, MethodInfo* method){
	{
		bool L_0 = m3883(__this, &m3883_MI);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral549, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0016:
	{
		return;
	}
}
extern MethodInfo m3918_MI;
 bool m3918 (t29 * __this, t748 * p0, t748 * p1, MethodInfo* method){
	{
		bool L_0 = m4286(NULL, p0, p1, &m4286_MI);
		return L_0;
	}
}
// Metadata Definition System.Uri
extern Il2CppType t40_0_0_1;
FieldInfo t748_f0_FieldInfo = 
{
	"isUnixFilePath", &t40_0_0_1, &t748_TI, offsetof(t748, f0), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_1;
FieldInfo t748_f1_FieldInfo = 
{
	"source", &t7_0_0_1, &t748_TI, offsetof(t748, f1), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_1;
FieldInfo t748_f2_FieldInfo = 
{
	"scheme", &t7_0_0_1, &t748_TI, offsetof(t748, f2), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_1;
FieldInfo t748_f3_FieldInfo = 
{
	"host", &t7_0_0_1, &t748_TI, offsetof(t748, f3), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t748_f4_FieldInfo = 
{
	"port", &t44_0_0_1, &t748_TI, offsetof(t748, f4), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_1;
FieldInfo t748_f5_FieldInfo = 
{
	"path", &t7_0_0_1, &t748_TI, offsetof(t748, f5), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_1;
FieldInfo t748_f6_FieldInfo = 
{
	"query", &t7_0_0_1, &t748_TI, offsetof(t748, f6), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_1;
FieldInfo t748_f7_FieldInfo = 
{
	"fragment", &t7_0_0_1, &t748_TI, offsetof(t748, f7), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_1;
FieldInfo t748_f8_FieldInfo = 
{
	"userinfo", &t7_0_0_1, &t748_TI, offsetof(t748, f8), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t748_f9_FieldInfo = 
{
	"isUnc", &t40_0_0_1, &t748_TI, offsetof(t748, f9), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t748_f10_FieldInfo = 
{
	"isOpaquePart", &t40_0_0_1, &t748_TI, offsetof(t748, f10), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t748_f11_FieldInfo = 
{
	"isAbsoluteUri", &t40_0_0_1, &t748_TI, offsetof(t748, f11), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t748_f12_FieldInfo = 
{
	"userEscaped", &t40_0_0_1, &t748_TI, offsetof(t748, f12), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_1;
FieldInfo t748_f13_FieldInfo = 
{
	"cachedAbsoluteUri", &t7_0_0_1, &t748_TI, offsetof(t748, f13), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_1;
FieldInfo t748_f14_FieldInfo = 
{
	"cachedToString", &t7_0_0_1, &t748_TI, offsetof(t748, f14), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t748_f15_FieldInfo = 
{
	"cachedHashCode", &t44_0_0_1, &t748_TI, offsetof(t748, f15), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_49;
FieldInfo t748_f16_FieldInfo = 
{
	"hexUpperChars", &t7_0_0_49, &t748_TI, offsetof(t748_SFs, f16), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_54;
FieldInfo t748_f17_FieldInfo = 
{
	"SchemeDelimiter", &t7_0_0_54, &t748_TI, offsetof(t748_SFs, f17), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_54;
FieldInfo t748_f18_FieldInfo = 
{
	"UriSchemeFile", &t7_0_0_54, &t748_TI, offsetof(t748_SFs, f18), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_54;
FieldInfo t748_f19_FieldInfo = 
{
	"UriSchemeFtp", &t7_0_0_54, &t748_TI, offsetof(t748_SFs, f19), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_54;
FieldInfo t748_f20_FieldInfo = 
{
	"UriSchemeGopher", &t7_0_0_54, &t748_TI, offsetof(t748_SFs, f20), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_54;
FieldInfo t748_f21_FieldInfo = 
{
	"UriSchemeHttp", &t7_0_0_54, &t748_TI, offsetof(t748_SFs, f21), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_54;
FieldInfo t748_f22_FieldInfo = 
{
	"UriSchemeHttps", &t7_0_0_54, &t748_TI, offsetof(t748_SFs, f22), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_54;
FieldInfo t748_f23_FieldInfo = 
{
	"UriSchemeMailto", &t7_0_0_54, &t748_TI, offsetof(t748_SFs, f23), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_54;
FieldInfo t748_f24_FieldInfo = 
{
	"UriSchemeNews", &t7_0_0_54, &t748_TI, offsetof(t748_SFs, f24), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_54;
FieldInfo t748_f25_FieldInfo = 
{
	"UriSchemeNntp", &t7_0_0_54, &t748_TI, offsetof(t748_SFs, f25), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_54;
FieldInfo t748_f26_FieldInfo = 
{
	"UriSchemeNetPipe", &t7_0_0_54, &t748_TI, offsetof(t748_SFs, f26), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_54;
FieldInfo t748_f27_FieldInfo = 
{
	"UriSchemeNetTcp", &t7_0_0_54, &t748_TI, offsetof(t748_SFs, f27), &EmptyCustomAttributesCache};
extern Il2CppType t896_0_0_17;
FieldInfo t748_f28_FieldInfo = 
{
	"schemes", &t896_0_0_17, &t748_TI, offsetof(t748_SFs, f28), &EmptyCustomAttributesCache};
extern Il2CppType t893_0_0_129;
FieldInfo t748_f29_FieldInfo = 
{
	"parser", &t893_0_0_129, &t748_TI, offsetof(t748, f29), &EmptyCustomAttributesCache};
extern Il2CppType t485_0_0_17;
extern CustomAttributesCache t748__CustomAttributeCache_U3CU3Ef__switch$map14;
FieldInfo t748_f30_FieldInfo = 
{
	"<>f__switch$map14", &t485_0_0_17, &t748_TI, offsetof(t748_SFs, f30), &t748__CustomAttributeCache_U3CU3Ef__switch$map14};
extern Il2CppType t485_0_0_17;
extern CustomAttributesCache t748__CustomAttributeCache_U3CU3Ef__switch$map15;
FieldInfo t748_f31_FieldInfo = 
{
	"<>f__switch$map15", &t485_0_0_17, &t748_TI, offsetof(t748_SFs, f31), &t748__CustomAttributeCache_U3CU3Ef__switch$map15};
extern Il2CppType t485_0_0_17;
extern CustomAttributesCache t748__CustomAttributeCache_U3CU3Ef__switch$map16;
FieldInfo t748_f32_FieldInfo = 
{
	"<>f__switch$map16", &t485_0_0_17, &t748_TI, offsetof(t748_SFs, f32), &t748__CustomAttributeCache_U3CU3Ef__switch$map16};
static FieldInfo* t748_FIs[] =
{
	&t748_f0_FieldInfo,
	&t748_f1_FieldInfo,
	&t748_f2_FieldInfo,
	&t748_f3_FieldInfo,
	&t748_f4_FieldInfo,
	&t748_f5_FieldInfo,
	&t748_f6_FieldInfo,
	&t748_f7_FieldInfo,
	&t748_f8_FieldInfo,
	&t748_f9_FieldInfo,
	&t748_f10_FieldInfo,
	&t748_f11_FieldInfo,
	&t748_f12_FieldInfo,
	&t748_f13_FieldInfo,
	&t748_f14_FieldInfo,
	&t748_f15_FieldInfo,
	&t748_f16_FieldInfo,
	&t748_f17_FieldInfo,
	&t748_f18_FieldInfo,
	&t748_f19_FieldInfo,
	&t748_f20_FieldInfo,
	&t748_f21_FieldInfo,
	&t748_f22_FieldInfo,
	&t748_f23_FieldInfo,
	&t748_f24_FieldInfo,
	&t748_f25_FieldInfo,
	&t748_f26_FieldInfo,
	&t748_f27_FieldInfo,
	&t748_f28_FieldInfo,
	&t748_f29_FieldInfo,
	&t748_f30_FieldInfo,
	&t748_f31_FieldInfo,
	&t748_f32_FieldInfo,
	NULL
};
static PropertyInfo t748____AbsoluteUri_PropertyInfo = 
{
	&t748_TI, "AbsoluteUri", &m3876_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t748____Authority_PropertyInfo = 
{
	&t748_TI, "Authority", &m3877_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t748____Host_PropertyInfo = 
{
	&t748_TI, "Host", &m3878_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t748____IsFile_PropertyInfo = 
{
	&t748_TI, "IsFile", &m3879_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t748____IsLoopback_PropertyInfo = 
{
	&t748_TI, "IsLoopback", &m3880_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t748____IsUnc_PropertyInfo = 
{
	&t748_TI, "IsUnc", &m3881_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t748____Scheme_PropertyInfo = 
{
	&t748_TI, "Scheme", &m3882_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t748____IsAbsoluteUri_PropertyInfo = 
{
	&t748_TI, "IsAbsoluteUri", &m3883_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t748____Parser_PropertyInfo = 
{
	&t748_TI, "Parser", &m3916_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t748_PIs[] =
{
	&t748____AbsoluteUri_PropertyInfo,
	&t748____Authority_PropertyInfo,
	&t748____Host_PropertyInfo,
	&t748____IsFile_PropertyInfo,
	&t748____IsLoopback_PropertyInfo,
	&t748____IsUnc_PropertyInfo,
	&t748____Scheme_PropertyInfo,
	&t748____IsAbsoluteUri_PropertyInfo,
	&t748____Parser_PropertyInfo,
	NULL
};
extern Il2CppType t7_0_0_0;
static ParameterInfo t748_m3871_ParameterInfos[] = 
{
	{"uriString", 0, 134218552, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3871_MI = 
{
	".ctor", (methodPointerType)&m3871, &t748_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t748_m3871_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 867, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t748_m3872_ParameterInfos[] = 
{
	{"serializationInfo", 0, 134218553, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"streamingContext", 1, 134218554, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m3872_MI = 
{
	".ctor", (methodPointerType)&m3872, &t748_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t748_m3872_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 2, false, false, 868, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t748_m3873_ParameterInfos[] = 
{
	{"uriString", 0, 134218555, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"dontEscape", 1, 134218556, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t297 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t748__CustomAttributeCache_m3873;
MethodInfo m3873_MI = 
{
	".ctor", (methodPointerType)&m3873, &t748_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t297, t748_m3873_ParameterInfos, &t748__CustomAttributeCache_m3873, 6278, 0, 255, 2, false, false, 869, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3874_MI = 
{
	".cctor", (methodPointerType)&m3874, &t748_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 870, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t748_m3875_ParameterInfos[] = 
{
	{"info", 0, 134218557, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134218558, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m3875_MI = 
{
	"System.Runtime.Serialization.ISerializable.GetObjectData", (methodPointerType)&m3875, &t748_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t748_m3875_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 4, 2, false, false, 871, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3876_MI = 
{
	"get_AbsoluteUri", (methodPointerType)&m3876, &t748_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 872, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3877_MI = 
{
	"get_Authority", (methodPointerType)&m3877, &t748_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 873, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3878_MI = 
{
	"get_Host", (methodPointerType)&m3878, &t748_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 874, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m3879_MI = 
{
	"get_IsFile", (methodPointerType)&m3879, &t748_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 875, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m3880_MI = 
{
	"get_IsLoopback", (methodPointerType)&m3880, &t748_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 876, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m3881_MI = 
{
	"get_IsUnc", (methodPointerType)&m3881, &t748_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 877, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3882_MI = 
{
	"get_Scheme", (methodPointerType)&m3882, &t748_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 878, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m3883_MI = 
{
	"get_IsAbsoluteUri", (methodPointerType)&m3883, &t748_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 879, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t748_m3884_ParameterInfos[] = 
{
	{"name", 0, 134218559, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t897_0_0_0;
extern void* RuntimeInvoker_t897_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3884_MI = 
{
	"CheckHostName", (methodPointerType)&m3884, &t748_TI, &t897_0_0_0, RuntimeInvoker_t897_t29, t748_m3884_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 1, false, false, 880, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t748_m3885_ParameterInfos[] = 
{
	{"name", 0, 134218560, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3885_MI = 
{
	"IsIPv4Address", (methodPointerType)&m3885, &t748_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t748_m3885_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, false, 881, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t748_m3886_ParameterInfos[] = 
{
	{"name", 0, 134218561, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3886_MI = 
{
	"IsDomainAddress", (methodPointerType)&m3886, &t748_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t748_m3886_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, false, 882, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t748_m3887_ParameterInfos[] = 
{
	{"schemeName", 0, 134218562, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3887_MI = 
{
	"CheckSchemeName", (methodPointerType)&m3887, &t748_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t748_m3887_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 1, false, false, 883, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t194_0_0_0;
static ParameterInfo t748_m3888_ParameterInfos[] = 
{
	{"c", 0, 134218563, &EmptyCustomAttributesCache, &t194_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t372 (MethodInfo* method, void* obj, void** args);
MethodInfo m3888_MI = 
{
	"IsAlpha", (methodPointerType)&m3888, &t748_TI, &t40_0_0_0, RuntimeInvoker_t40_t372, t748_m3888_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 884, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t748_m3889_ParameterInfos[] = 
{
	{"comparant", 0, 134218564, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3889_MI = 
{
	"Equals", (methodPointerType)&m3889, &t748_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t748_m3889_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 0, 1, false, false, 885, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t748_0_0_0;
extern Il2CppType t748_0_0_0;
static ParameterInfo t748_m3890_ParameterInfos[] = 
{
	{"uri", 0, 134218565, &EmptyCustomAttributesCache, &t748_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3890_MI = 
{
	"InternalEquals", (methodPointerType)&m3890, &t748_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t748_m3890_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 886, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3891_MI = 
{
	"GetHashCode", (methodPointerType)&m3891, &t748_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 198, 0, 2, 0, false, false, 887, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t898_0_0_0;
extern Il2CppType t898_0_0_0;
static ParameterInfo t748_m3892_ParameterInfos[] = 
{
	{"part", 0, 134218566, &EmptyCustomAttributesCache, &t898_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3892_MI = 
{
	"GetLeftPart", (methodPointerType)&m3892, &t748_TI, &t7_0_0_0, RuntimeInvoker_t29_t44, t748_m3892_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 888, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t194_0_0_0;
static ParameterInfo t748_m3893_ParameterInfos[] = 
{
	{"digit", 0, 134218567, &EmptyCustomAttributesCache, &t194_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t372 (MethodInfo* method, void* obj, void** args);
MethodInfo m3893_MI = 
{
	"FromHex", (methodPointerType)&m3893, &t748_TI, &t44_0_0_0, RuntimeInvoker_t44_t372, t748_m3893_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 1, false, false, 889, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t194_0_0_0;
static ParameterInfo t748_m3894_ParameterInfos[] = 
{
	{"character", 0, 134218568, &EmptyCustomAttributesCache, &t194_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t372 (MethodInfo* method, void* obj, void** args);
MethodInfo m3894_MI = 
{
	"HexEscape", (methodPointerType)&m3894, &t748_TI, &t7_0_0_0, RuntimeInvoker_t29_t372, t748_m3894_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 1, false, false, 890, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t194_0_0_0;
static ParameterInfo t748_m3895_ParameterInfos[] = 
{
	{"digit", 0, 134218569, &EmptyCustomAttributesCache, &t194_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t372 (MethodInfo* method, void* obj, void** args);
MethodInfo m3895_MI = 
{
	"IsHexDigit", (methodPointerType)&m3895, &t748_TI, &t40_0_0_0, RuntimeInvoker_t40_t372, t748_m3895_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 1, false, false, 891, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t748_m3896_ParameterInfos[] = 
{
	{"pattern", 0, 134218570, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"index", 1, 134218571, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3896_MI = 
{
	"IsHexEncoding", (methodPointerType)&m3896, &t748_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t44, t748_m3896_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 2, false, false, 892, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_1_0_0;
extern Il2CppType t7_1_0_0;
static ParameterInfo t748_m3897_ParameterInfos[] = 
{
	{"result", 0, 134218572, &EmptyCustomAttributesCache, &t7_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t639 (MethodInfo* method, void* obj, void** args);
MethodInfo m3897_MI = 
{
	"AppendQueryAndFragment", (methodPointerType)&m3897, &t748_TI, &t21_0_0_0, RuntimeInvoker_t21_t639, t748_m3897_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 893, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3898_MI = 
{
	"ToString", (methodPointerType)&m3898, &t748_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 3, 0, false, false, 894, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t748_m3899_ParameterInfos[] = 
{
	{"str", 0, 134218573, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t748__CustomAttributeCache_m3899;
MethodInfo m3899_MI = 
{
	"EscapeString", (methodPointerType)&m3899, &t748_TI, &t7_0_0_0, RuntimeInvoker_t29_t29, t748_m3899_ParameterInfos, &t748__CustomAttributeCache_m3899, 148, 0, 255, 1, false, false, 895, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t748_m3900_ParameterInfos[] = 
{
	{"str", 0, 134218574, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"escapeReserved", 1, 134218575, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"escapeHex", 2, 134218576, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"escapeBrackets", 3, 134218577, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29_t297_t297_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3900_MI = 
{
	"EscapeString", (methodPointerType)&m3900, &t748_TI, &t7_0_0_0, RuntimeInvoker_t29_t29_t297_t297_t297, t748_m3900_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 4, false, false, 896, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t899_0_0_0;
extern Il2CppType t899_0_0_0;
static ParameterInfo t748_m3901_ParameterInfos[] = 
{
	{"kind", 0, 134218578, &EmptyCustomAttributesCache, &t899_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3901_MI = 
{
	"ParseUri", (methodPointerType)&m3901, &t748_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t748_m3901_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 897, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t748_m3902_ParameterInfos[] = 
{
	{"str", 0, 134218579, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t748__CustomAttributeCache_m3902;
MethodInfo m3902_MI = 
{
	"Unescape", (methodPointerType)&m3902, &t748_TI, &t7_0_0_0, RuntimeInvoker_t29_t29, t748_m3902_ParameterInfos, &t748__CustomAttributeCache_m3902, 452, 0, 5, 1, false, false, 898, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t748_m3903_ParameterInfos[] = 
{
	{"str", 0, 134218580, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"excludeSpecial", 1, 134218581, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3903_MI = 
{
	"Unescape", (methodPointerType)&m3903, &t748_TI, &t7_0_0_0, RuntimeInvoker_t29_t29_t297, t748_m3903_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 2, false, false, 899, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t748_m3904_ParameterInfos[] = 
{
	{"uriString", 0, 134218582, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3904_MI = 
{
	"ParseAsWindowsUNC", (methodPointerType)&m3904, &t748_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t748_m3904_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 900, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t748_m3905_ParameterInfos[] = 
{
	{"uriString", 0, 134218583, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3905_MI = 
{
	"ParseAsWindowsAbsoluteFilePath", (methodPointerType)&m3905, &t748_TI, &t7_0_0_0, RuntimeInvoker_t29_t29, t748_m3905_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 901, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t748_m3906_ParameterInfos[] = 
{
	{"uriString", 0, 134218584, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3906_MI = 
{
	"ParseAsUnixAbsoluteFilePath", (methodPointerType)&m3906, &t748_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t748_m3906_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 902, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t899_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t748_m3907_ParameterInfos[] = 
{
	{"kind", 0, 134218585, &EmptyCustomAttributesCache, &t899_0_0_0},
	{"uriString", 1, 134218586, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3907_MI = 
{
	"Parse", (methodPointerType)&m3907, &t748_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t748_m3907_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 903, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t899_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t748_m3908_ParameterInfos[] = 
{
	{"kind", 0, 134218587, &EmptyCustomAttributesCache, &t899_0_0_0},
	{"uriString", 1, 134218588, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3908_MI = 
{
	"ParseNoExceptions", (methodPointerType)&m3908, &t748_TI, &t7_0_0_0, RuntimeInvoker_t29_t44_t29, t748_m3908_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 904, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t748_m3909_ParameterInfos[] = 
{
	{"scheme", 0, 134218589, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3909_MI = 
{
	"CompactEscaped", (methodPointerType)&m3909, &t748_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t748_m3909_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 905, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t748_m3910_ParameterInfos[] = 
{
	{"path", 0, 134218590, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"compact_escaped", 1, 134218591, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m3910_MI = 
{
	"Reduce", (methodPointerType)&m3910, &t748_TI, &t7_0_0_0, RuntimeInvoker_t29_t29_t297, t748_m3910_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, false, 906, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t44_1_0_0;
extern Il2CppType t194_1_0_2;
extern Il2CppType t194_1_0_0;
static ParameterInfo t748_m3911_ParameterInfos[] = 
{
	{"pattern", 0, 134218592, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"index", 1, 134218593, &EmptyCustomAttributesCache, &t44_1_0_0},
	{"surrogate", 2, 134218594, &EmptyCustomAttributesCache, &t194_1_0_2},
};
extern Il2CppType t194_0_0_0;
extern void* RuntimeInvoker_t194_t29_t390_t961 (MethodInfo* method, void* obj, void** args);
MethodInfo m3911_MI = 
{
	"HexUnescapeMultiByte", (methodPointerType)&m3911, &t748_TI, &t194_0_0_0, RuntimeInvoker_t194_t29_t390_t961, t748_m3911_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 3, false, false, 907, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t748_m3912_ParameterInfos[] = 
{
	{"scheme", 0, 134218595, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3912_MI = 
{
	"GetSchemeDelimiter", (methodPointerType)&m3912, &t748_TI, &t7_0_0_0, RuntimeInvoker_t29_t29, t748_m3912_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, false, 908, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t748_m3913_ParameterInfos[] = 
{
	{"scheme", 0, 134218596, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3913_MI = 
{
	"GetDefaultPort", (methodPointerType)&m3913, &t748_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t748_m3913_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, false, 909, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3914_MI = 
{
	"GetOpaqueWiseSchemeDelimiter", (methodPointerType)&m3914, &t748_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 910, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t748_m3915_ParameterInfos[] = 
{
	{"scheme", 0, 134218597, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3915_MI = 
{
	"IsPredefinedScheme", (methodPointerType)&m3915, &t748_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t748_m3915_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 911, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t893_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3916_MI = 
{
	"get_Parser", (methodPointerType)&m3916, &t748_TI, &t893_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2177, 0, 255, 0, false, false, 912, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3917_MI = 
{
	"EnsureAbsoluteUri", (methodPointerType)&m3917, &t748_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 913, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t748_0_0_0;
extern Il2CppType t748_0_0_0;
static ParameterInfo t748_m3918_ParameterInfos[] = 
{
	{"u1", 0, 134218598, &EmptyCustomAttributesCache, &t748_0_0_0},
	{"u2", 1, 134218599, &EmptyCustomAttributesCache, &t748_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3918_MI = 
{
	"op_Equality", (methodPointerType)&m3918, &t748_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t748_m3918_ParameterInfos, &EmptyCustomAttributesCache, 2198, 0, 255, 2, false, false, 914, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t748_MIs[] =
{
	&m3871_MI,
	&m3872_MI,
	&m3873_MI,
	&m3874_MI,
	&m3875_MI,
	&m3876_MI,
	&m3877_MI,
	&m3878_MI,
	&m3879_MI,
	&m3880_MI,
	&m3881_MI,
	&m3882_MI,
	&m3883_MI,
	&m3884_MI,
	&m3885_MI,
	&m3886_MI,
	&m3887_MI,
	&m3888_MI,
	&m3889_MI,
	&m3890_MI,
	&m3891_MI,
	&m3892_MI,
	&m3893_MI,
	&m3894_MI,
	&m3895_MI,
	&m3896_MI,
	&m3897_MI,
	&m3898_MI,
	&m3899_MI,
	&m3900_MI,
	&m3901_MI,
	&m3902_MI,
	&m3903_MI,
	&m3904_MI,
	&m3905_MI,
	&m3906_MI,
	&m3907_MI,
	&m3908_MI,
	&m3909_MI,
	&m3910_MI,
	&m3911_MI,
	&m3912_MI,
	&m3913_MI,
	&m3914_MI,
	&m3915_MI,
	&m3916_MI,
	&m3917_MI,
	&m3918_MI,
	NULL
};
extern TypeInfo t895_TI;
static TypeInfo* t748_TI__nestedTypes[2] =
{
	&t895_TI,
	NULL
};
static MethodInfo* t748_VT[] =
{
	&m3889_MI,
	&m46_MI,
	&m3891_MI,
	&m3898_MI,
	&m3875_MI,
	&m3902_MI,
};
static TypeInfo* t748_ITIs[] = 
{
	&t374_TI,
};
static Il2CppInterfaceOffsetPair t748_IOs[] = 
{
	{ &t374_TI, 4},
};
extern TypeInfo t739_TI;
#include "t739.h"
#include "t739MD.h"
extern MethodInfo m3151_MI;
extern TypeInfo t902_TI;
#include "t902.h"
void t748_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t739 * tmp;
		tmp = (t739 *)il2cpp_codegen_object_new (&t739_TI);
		m3151(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&t902_TI)), &m3151_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo t38_TI;
#include "t38.h"
#include "t38MD.h"
extern MethodInfo m55_MI;
void t748_CustomAttributesCacheGenerator_U3CU3Ef__switch$map14(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t748_CustomAttributesCacheGenerator_U3CU3Ef__switch$map15(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t748_CustomAttributesCacheGenerator_U3CU3Ef__switch$map16(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo t327_TI;
#include "t327.h"
#include "t327MD.h"
extern MethodInfo m4287_MI;
void t748_CustomAttributesCacheGenerator_m3873(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t327 * tmp;
		tmp = (t327 *)il2cpp_codegen_object_new (&t327_TI);
		m4287(tmp, &m4287_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t748_CustomAttributesCacheGenerator_m3899(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t327 * tmp;
		tmp = (t327 *)il2cpp_codegen_object_new (&t327_TI);
		m4287(tmp, &m4287_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t748_CustomAttributesCacheGenerator_m3902(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t327 * tmp;
		tmp = (t327 *)il2cpp_codegen_object_new (&t327_TI);
		m4287(tmp, &m4287_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t748__CustomAttributeCache = {
1,
NULL,
&t748_CustomAttributesCacheGenerator
};
CustomAttributesCache t748__CustomAttributeCache_U3CU3Ef__switch$map14 = {
1,
NULL,
&t748_CustomAttributesCacheGenerator_U3CU3Ef__switch$map14
};
CustomAttributesCache t748__CustomAttributeCache_U3CU3Ef__switch$map15 = {
1,
NULL,
&t748_CustomAttributesCacheGenerator_U3CU3Ef__switch$map15
};
CustomAttributesCache t748__CustomAttributeCache_U3CU3Ef__switch$map16 = {
1,
NULL,
&t748_CustomAttributesCacheGenerator_U3CU3Ef__switch$map16
};
CustomAttributesCache t748__CustomAttributeCache_m3873 = {
1,
NULL,
&t748_CustomAttributesCacheGenerator_m3873
};
CustomAttributesCache t748__CustomAttributeCache_m3899 = {
1,
NULL,
&t748_CustomAttributesCacheGenerator_m3899
};
CustomAttributesCache t748__CustomAttributeCache_m3902 = {
1,
NULL,
&t748_CustomAttributesCacheGenerator_m3902
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t748_1_0_0;
struct t748;
extern CustomAttributesCache t748__CustomAttributeCache;
extern CustomAttributesCache t748__CustomAttributeCache_U3CU3Ef__switch$map14;
extern CustomAttributesCache t748__CustomAttributeCache_U3CU3Ef__switch$map15;
extern CustomAttributesCache t748__CustomAttributeCache_U3CU3Ef__switch$map16;
extern CustomAttributesCache t748__CustomAttributeCache_m3873;
extern CustomAttributesCache t748__CustomAttributeCache_m3899;
extern CustomAttributesCache t748__CustomAttributeCache_m3902;
TypeInfo t748_TI = 
{
	&g_System_dll_Image, NULL, "Uri", "System", t748_MIs, t748_PIs, t748_FIs, NULL, &t29_TI, t748_TI__nestedTypes, NULL, &t748_TI, t748_ITIs, t748_VT, &t748__CustomAttributeCache, &t748_TI, &t748_0_0_0, &t748_1_0_0, t748_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t748), 0, -1, sizeof(t748_SFs), 0, -1, 1056769, 0, false, false, false, false, false, false, false, false, false, true, false, false, 48, 9, 33, 0, 1, 6, 1, 1};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo t733_TI;
extern TypeInfo t735_TI;
#include "t901MD.h"
extern MethodInfo m4003_MI;
extern MethodInfo m4288_MI;
extern MethodInfo m2857_MI;


extern MethodInfo m3919_MI;
 void m3919 (t900 * __this, MethodInfo* method){
	{
		t7* L_0 = m3068(NULL, (t7*) &_stringLiteral550, &m3068_MI);
		m4003(__this, L_0, &m4003_MI);
		return;
	}
}
 void m3920 (t900 * __this, t7* p0, MethodInfo* method){
	{
		m4003(__this, p0, &m4003_MI);
		return;
	}
}
extern MethodInfo m3921_MI;
 void m3921 (t900 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m4288(__this, p0, p1, &m4288_MI);
		return;
	}
}
extern MethodInfo m3922_MI;
 void m3922 (t900 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m2857(__this, p0, p1, &m2857_MI);
		return;
	}
}
// Metadata Definition System.UriFormatException
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3919_MI = 
{
	".ctor", (methodPointerType)&m3919, &t900_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 916, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t900_m3920_ParameterInfos[] = 
{
	{"message", 0, 134218603, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3920_MI = 
{
	".ctor", (methodPointerType)&m3920, &t900_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t900_m3920_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 917, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t900_m3921_ParameterInfos[] = 
{
	{"info", 0, 134218604, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134218605, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m3921_MI = 
{
	".ctor", (methodPointerType)&m3921, &t900_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t900_m3921_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 2, false, false, 918, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t900_m3922_ParameterInfos[] = 
{
	{"info", 0, 134218606, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134218607, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m3922_MI = 
{
	"System.Runtime.Serialization.ISerializable.GetObjectData", (methodPointerType)&m3922, &t900_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t900_m3922_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 4, 2, false, false, 919, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t900_MIs[] =
{
	&m3919_MI,
	&m3920_MI,
	&m3921_MI,
	&m3922_MI,
	NULL
};
extern MethodInfo m2856_MI;
extern MethodInfo m2858_MI;
extern MethodInfo m2859_MI;
extern MethodInfo m2860_MI;
extern MethodInfo m2861_MI;
static MethodInfo* t900_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m2856_MI,
	&m3922_MI,
	&m2858_MI,
	&m1684_MI,
	&m2859_MI,
	&m2860_MI,
	&m2857_MI,
	&m2861_MI,
};
static TypeInfo* t900_ITIs[] = 
{
	&t374_TI,
};
extern TypeInfo t590_TI;
static Il2CppInterfaceOffsetPair t900_IOs[] = 
{
	{ &t374_TI, 4},
	{ &t590_TI, 5},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t900_0_0_0;
extern Il2CppType t900_1_0_0;
extern TypeInfo t901_TI;
struct t900;
TypeInfo t900_TI = 
{
	&g_System_dll_Image, NULL, "UriFormatException", "System", t900_MIs, NULL, NULL, NULL, &t901_TI, NULL, NULL, &t900_TI, t900_ITIs, t900_VT, &EmptyCustomAttributesCache, &t900_TI, &t900_0_0_0, &t900_1_0_0, t900_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t900), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 11, 1, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t897_TI;
#include "t897MD.h"



// Metadata Definition System.UriHostNameType
extern Il2CppType t44_0_0_1542;
FieldInfo t897_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t897_TI, offsetof(t897, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t897_0_0_32854;
FieldInfo t897_f2_FieldInfo = 
{
	"Unknown", &t897_0_0_32854, &t897_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t897_0_0_32854;
FieldInfo t897_f3_FieldInfo = 
{
	"Basic", &t897_0_0_32854, &t897_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t897_0_0_32854;
FieldInfo t897_f4_FieldInfo = 
{
	"Dns", &t897_0_0_32854, &t897_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t897_0_0_32854;
FieldInfo t897_f5_FieldInfo = 
{
	"IPv4", &t897_0_0_32854, &t897_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t897_0_0_32854;
FieldInfo t897_f6_FieldInfo = 
{
	"IPv6", &t897_0_0_32854, &t897_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t897_FIs[] =
{
	&t897_f1_FieldInfo,
	&t897_f2_FieldInfo,
	&t897_f3_FieldInfo,
	&t897_f4_FieldInfo,
	&t897_f5_FieldInfo,
	&t897_f6_FieldInfo,
	NULL
};
static const int32_t t897_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t897_f2_DefaultValue = 
{
	&t897_f2_FieldInfo, { (char*)&t897_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t897_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t897_f3_DefaultValue = 
{
	&t897_f3_FieldInfo, { (char*)&t897_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t897_f4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t897_f4_DefaultValue = 
{
	&t897_f4_FieldInfo, { (char*)&t897_f4_DefaultValueData, &t44_0_0_0 }};
static const int32_t t897_f5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry t897_f5_DefaultValue = 
{
	&t897_f5_FieldInfo, { (char*)&t897_f5_DefaultValueData, &t44_0_0_0 }};
static const int32_t t897_f6_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t897_f6_DefaultValue = 
{
	&t897_f6_FieldInfo, { (char*)&t897_f6_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t897_FDVs[] = 
{
	&t897_f2_DefaultValue,
	&t897_f3_DefaultValue,
	&t897_f4_DefaultValue,
	&t897_f5_DefaultValue,
	&t897_f6_DefaultValue,
	NULL
};
static MethodInfo* t897_MIs[] =
{
	NULL
};
static MethodInfo* t897_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t897_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t897_0_0_0;
extern Il2CppType t897_1_0_0;
TypeInfo t897_TI = 
{
	&g_System_dll_Image, NULL, "UriHostNameType", "System", t897_MIs, NULL, t897_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t897_VT, &EmptyCustomAttributesCache, &t44_TI, &t897_0_0_0, &t897_1_0_0, t897_IOs, NULL, NULL, t897_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t897)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 257, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 6, 0, 0, 23, 0, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t899_TI;
#include "t899MD.h"



// Metadata Definition System.UriKind
extern Il2CppType t44_0_0_1542;
FieldInfo t899_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t899_TI, offsetof(t899, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t899_0_0_32854;
FieldInfo t899_f2_FieldInfo = 
{
	"RelativeOrAbsolute", &t899_0_0_32854, &t899_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t899_0_0_32854;
FieldInfo t899_f3_FieldInfo = 
{
	"Absolute", &t899_0_0_32854, &t899_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t899_0_0_32854;
FieldInfo t899_f4_FieldInfo = 
{
	"Relative", &t899_0_0_32854, &t899_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t899_FIs[] =
{
	&t899_f1_FieldInfo,
	&t899_f2_FieldInfo,
	&t899_f3_FieldInfo,
	&t899_f4_FieldInfo,
	NULL
};
static const int32_t t899_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t899_f2_DefaultValue = 
{
	&t899_f2_FieldInfo, { (char*)&t899_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t899_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t899_f3_DefaultValue = 
{
	&t899_f3_FieldInfo, { (char*)&t899_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t899_f4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t899_f4_DefaultValue = 
{
	&t899_f4_FieldInfo, { (char*)&t899_f4_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t899_FDVs[] = 
{
	&t899_f2_DefaultValue,
	&t899_f3_DefaultValue,
	&t899_f4_DefaultValue,
	NULL
};
static MethodInfo* t899_MIs[] =
{
	NULL
};
static MethodInfo* t899_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t899_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t899_1_0_0;
TypeInfo t899_TI = 
{
	&g_System_dll_Image, NULL, "UriKind", "System", t899_MIs, NULL, t899_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t899_VT, &EmptyCustomAttributesCache, &t44_TI, &t899_0_0_0, &t899_1_0_0, t899_IOs, NULL, NULL, t899_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t899)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 257, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 4, 0, 0, 23, 0, 3};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo t829_TI;
#include "t829MD.h"
#include "t921MD.h"
extern MethodInfo m3552_MI;
extern MethodInfo m3931_MI;
extern MethodInfo m4000_MI;
extern MethodInfo m4001_MI;
extern MethodInfo m3927_MI;
extern MethodInfo m3929_MI;
extern MethodInfo m3930_MI;


 void m3923 (t893 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m3924_MI;
 void m3924 (t29 * __this, MethodInfo* method){
	{
		t29 * L_0 = (t29 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t29_TI));
		m1331(L_0, &m1331_MI);
		((t893_SFs*)InitializedTypeInfo(&t893_TI)->static_fields)->f0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t829_TI));
		t829 * L_1 = (t829 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t829_TI));
		m3552(L_1, (t7*) &_stringLiteral551, &m3552_MI);
		((t893_SFs*)InitializedTypeInfo(&t893_TI)->static_fields)->f4 = L_1;
		t829 * L_2 = (t829 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t829_TI));
		m3552(L_2, (t7*) &_stringLiteral552, &m3552_MI);
		((t893_SFs*)InitializedTypeInfo(&t893_TI)->static_fields)->f5 = L_2;
		return;
	}
}
 void m3925 (t893 * __this, t748 * p0, t900 ** p1, MethodInfo* method){
	{
		t7* L_0 = m3882(p0, &m3882_MI);
		t7* L_1 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_2 = m1740(NULL, L_0, L_1, &m1740_MI);
		if (!L_2)
		{
			goto IL_003c;
		}
	}
	{
		t7* L_3 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_4 = m1740(NULL, L_3, (t7*) &_stringLiteral515, &m1740_MI);
		if (!L_4)
		{
			goto IL_003c;
		}
	}
	{
		t900 * L_5 = (t900 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t900_TI));
		m3920(L_5, (t7*) &_stringLiteral553, &m3920_MI);
		*((t29 **)(p1)) = (t29 *)L_5;
		goto IL_003f;
	}

IL_003c:
	{
		*((t29 **)(p1)) = (t29 *)NULL;
	}

IL_003f:
	{
		return;
	}
}
 void m3926 (t893 * __this, t7* p0, int32_t p1, MethodInfo* method){
	{
		return;
	}
}
 void m3927 (t893 * __this, t7* p0, MethodInfo* method){
	{
		__this->f2 = p0;
		return;
	}
}
 int32_t m3928 (t893 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f3);
		return L_0;
	}
}
 void m3929 (t893 * __this, int32_t p0, MethodInfo* method){
	{
		__this->f3 = p0;
		return;
	}
}
 void m3930 (t29 * __this, MethodInfo* method){
	t719 * V_0 = {0};
	t29 * V_1 = {0};
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t893_TI));
		if (!(((t893_SFs*)InitializedTypeInfo(&t893_TI)->static_fields)->f1))
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t719_TI));
		t719 * L_0 = (t719 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t719_TI));
		m4209(L_0, &m4209_MI);
		V_0 = L_0;
		t892 * L_1 = (t892 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t892_TI));
		m3868(L_1, &m3868_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t748_TI));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t893_TI));
		m3931(NULL, V_0, L_1, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f18), (-1), &m3931_MI);
		t892 * L_2 = (t892 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t892_TI));
		m3868(L_2, &m3868_MI);
		m3931(NULL, V_0, L_2, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f19), ((int32_t)21), &m3931_MI);
		t892 * L_3 = (t892 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t892_TI));
		m3868(L_3, &m3868_MI);
		m3931(NULL, V_0, L_3, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f20), ((int32_t)70), &m3931_MI);
		t892 * L_4 = (t892 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t892_TI));
		m3868(L_4, &m3868_MI);
		m3931(NULL, V_0, L_4, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f21), ((int32_t)80), &m3931_MI);
		t892 * L_5 = (t892 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t892_TI));
		m3868(L_5, &m3868_MI);
		m3931(NULL, V_0, L_5, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f22), ((int32_t)443), &m3931_MI);
		t892 * L_6 = (t892 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t892_TI));
		m3868(L_6, &m3868_MI);
		m3931(NULL, V_0, L_6, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f23), ((int32_t)25), &m3931_MI);
		t892 * L_7 = (t892 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t892_TI));
		m3868(L_7, &m3868_MI);
		m3931(NULL, V_0, L_7, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f26), (-1), &m3931_MI);
		t892 * L_8 = (t892 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t892_TI));
		m3868(L_8, &m3868_MI);
		m3931(NULL, V_0, L_8, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f27), (-1), &m3931_MI);
		t892 * L_9 = (t892 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t892_TI));
		m3868(L_9, &m3868_MI);
		m3931(NULL, V_0, L_9, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f24), ((int32_t)119), &m3931_MI);
		t892 * L_10 = (t892 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t892_TI));
		m3868(L_10, &m3868_MI);
		m3931(NULL, V_0, L_10, (((t748_SFs*)InitializedTypeInfo(&t748_TI)->static_fields)->f25), ((int32_t)119), &m3931_MI);
		t892 * L_11 = (t892 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t892_TI));
		m3868(L_11, &m3868_MI);
		m3931(NULL, V_0, L_11, (t7*) &_stringLiteral554, ((int32_t)389), &m3931_MI);
		V_1 = (((t893_SFs*)InitializedTypeInfo(&t893_TI)->static_fields)->f0);
		m4000(NULL, V_1, &m4000_MI);
	}

IL_00e6:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t893_TI));
			if ((((t893_SFs*)InitializedTypeInfo(&t893_TI)->static_fields)->f1))
			{
				goto IL_00fb;
			}
		}

IL_00f0:
		{
			IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t893_TI));
			((t893_SFs*)InitializedTypeInfo(&t893_TI)->static_fields)->f1 = V_0;
			goto IL_00fd;
		}

IL_00fb:
		{
			V_0 = (t719 *)NULL;
		}

IL_00fd:
		{
			// IL_00fd: leave IL_0109
			leaveInstructions[0] = 0x109; // 1
			THROW_SENTINEL(IL_0109);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_0102;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_0102;
	}

IL_0102:
	{ // begin finally (depth: 1)
		m4001(NULL, V_1, &m4001_MI);
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0x109:
				goto IL_0109;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				t295 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_0109:
	{
		return;
	}
}
 void m3931 (t29 * __this, t719 * p0, t893 * p1, t7* p2, int32_t p3, MethodInfo* method){
	t892 * V_0 = {0};
	{
		m3927(p1, p2, &m3927_MI);
		m3929(p1, p3, &m3929_MI);
		if (!((t894 *)IsInst(p1, InitializedTypeInfo(&t894_TI))))
		{
			goto IL_0026;
		}
	}
	{
		VirtActionInvoker2< t29 *, t29 * >::Invoke(&m3990_MI, p0, p2, p1);
		goto IL_0042;
	}

IL_0026:
	{
		t892 * L_0 = (t892 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t892_TI));
		m3868(L_0, &m3868_MI);
		V_0 = L_0;
		m3927(V_0, p2, &m3927_MI);
		m3929(V_0, p3, &m3929_MI);
		VirtActionInvoker2< t29 *, t29 * >::Invoke(&m3990_MI, p0, p2, V_0);
	}

IL_0042:
	{
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m3926_MI, p1, p2, p3);
		return;
	}
}
 t893 * m3932 (t29 * __this, t7* p0, MethodInfo* method){
	t7* V_0 = {0};
	{
		if (p0)
		{
			goto IL_0008;
		}
	}
	{
		return (t893 *)NULL;
	}

IL_0008:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t893_TI));
		m3930(NULL, &m3930_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t633_TI));
		t633 * L_0 = m4009(NULL, &m4009_MI);
		t7* L_1 = m4279(p0, L_0, &m4279_MI);
		V_0 = L_1;
		t29 * L_2 = (t29 *)VirtFuncInvoker1< t29 *, t29 * >::Invoke(&m3989_MI, (((t893_SFs*)InitializedTypeInfo(&t893_TI)->static_fields)->f1), V_0);
		return ((t893 *)Castclass(L_2, InitializedTypeInfo(&t893_TI)));
	}
}
// Metadata Definition System.UriParser
extern Il2CppType t29_0_0_17;
FieldInfo t893_f0_FieldInfo = 
{
	"lock_object", &t29_0_0_17, &t893_TI, offsetof(t893_SFs, f0), &EmptyCustomAttributesCache};
extern Il2CppType t719_0_0_17;
FieldInfo t893_f1_FieldInfo = 
{
	"table", &t719_0_0_17, &t893_TI, offsetof(t893_SFs, f1), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_3;
FieldInfo t893_f2_FieldInfo = 
{
	"scheme_name", &t7_0_0_3, &t893_TI, offsetof(t893, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t893_f3_FieldInfo = 
{
	"default_port", &t44_0_0_1, &t893_TI, offsetof(t893, f3), &EmptyCustomAttributesCache};
extern Il2CppType t829_0_0_49;
FieldInfo t893_f4_FieldInfo = 
{
	"uri_regex", &t829_0_0_49, &t893_TI, offsetof(t893_SFs, f4), &EmptyCustomAttributesCache};
extern Il2CppType t829_0_0_49;
FieldInfo t893_f5_FieldInfo = 
{
	"auth_regex", &t829_0_0_49, &t893_TI, offsetof(t893_SFs, f5), &EmptyCustomAttributesCache};
static FieldInfo* t893_FIs[] =
{
	&t893_f0_FieldInfo,
	&t893_f1_FieldInfo,
	&t893_f2_FieldInfo,
	&t893_f3_FieldInfo,
	&t893_f4_FieldInfo,
	&t893_f5_FieldInfo,
	NULL
};
static PropertyInfo t893____SchemeName_PropertyInfo = 
{
	&t893_TI, "SchemeName", NULL, &m3927_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t893____DefaultPort_PropertyInfo = 
{
	&t893_TI, "DefaultPort", &m3928_MI, &m3929_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t893_PIs[] =
{
	&t893____SchemeName_PropertyInfo,
	&t893____DefaultPort_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3923_MI = 
{
	".ctor", (methodPointerType)&m3923, &t893_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 920, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3924_MI = 
{
	".cctor", (methodPointerType)&m3924, &t893_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 921, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t748_0_0_0;
extern Il2CppType t900_1_0_2;
static ParameterInfo t893_m3925_ParameterInfos[] = 
{
	{"uri", 0, 134218608, &EmptyCustomAttributesCache, &t748_0_0_0},
	{"parsingError", 1, 134218609, &EmptyCustomAttributesCache, &t900_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t962 (MethodInfo* method, void* obj, void** args);
MethodInfo m3925_MI = 
{
	"InitializeAndValidate", (methodPointerType)&m3925, &t893_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t962, t893_m3925_ParameterInfos, &EmptyCustomAttributesCache, 453, 0, 4, 2, false, false, 922, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t893_m3926_ParameterInfos[] = 
{
	{"schemeName", 0, 134218610, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"defaultPort", 1, 134218611, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t893__CustomAttributeCache_m3926;
MethodInfo m3926_MI = 
{
	"OnRegister", (methodPointerType)&m3926, &t893_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t893_m3926_ParameterInfos, &t893__CustomAttributeCache_m3926, 452, 0, 5, 2, false, false, 923, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t893_m3927_ParameterInfos[] = 
{
	{"value", 0, 134218612, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3927_MI = 
{
	"set_SchemeName", (methodPointerType)&m3927, &t893_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t893_m3927_ParameterInfos, &EmptyCustomAttributesCache, 2179, 0, 255, 1, false, false, 924, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3928_MI = 
{
	"get_DefaultPort", (methodPointerType)&m3928, &t893_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2179, 0, 255, 0, false, false, 925, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t893_m3929_ParameterInfos[] = 
{
	{"value", 0, 134218613, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3929_MI = 
{
	"set_DefaultPort", (methodPointerType)&m3929, &t893_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t893_m3929_ParameterInfos, &EmptyCustomAttributesCache, 2179, 0, 255, 1, false, false, 926, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3930_MI = 
{
	"CreateDefaults", (methodPointerType)&m3930, &t893_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 145, 0, 255, 0, false, false, 927, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t719_0_0_0;
extern Il2CppType t893_0_0_0;
extern Il2CppType t893_0_0_0;
extern Il2CppType t7_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t893_m3931_ParameterInfos[] = 
{
	{"table", 0, 134218614, &EmptyCustomAttributesCache, &t719_0_0_0},
	{"uriParser", 1, 134218615, &EmptyCustomAttributesCache, &t893_0_0_0},
	{"schemeName", 2, 134218616, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"defaultPort", 3, 134218617, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3931_MI = 
{
	"InternalRegister", (methodPointerType)&m3931, &t893_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29_t44, t893_m3931_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 4, false, false, 928, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t893_m3932_ParameterInfos[] = 
{
	{"schemeName", 0, 134218618, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t893_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3932_MI = 
{
	"GetParser", (methodPointerType)&m3932, &t893_TI, &t893_0_0_0, RuntimeInvoker_t29_t29, t893_m3932_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, false, 929, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t893_MIs[] =
{
	&m3923_MI,
	&m3924_MI,
	&m3925_MI,
	&m3926_MI,
	&m3927_MI,
	&m3928_MI,
	&m3929_MI,
	&m3930_MI,
	&m3931_MI,
	&m3932_MI,
	NULL
};
static MethodInfo* t893_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m3925_MI,
	&m3926_MI,
};
extern TypeInfo t715_TI;
#include "t715.h"
#include "t715MD.h"
extern MethodInfo m3070_MI;
void t893_CustomAttributesCacheGenerator_m3926(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t715 * tmp;
		tmp = (t715 *)il2cpp_codegen_object_new (&t715_TI);
		m3070(tmp, &m3070_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t893__CustomAttributeCache_m3926 = {
1,
NULL,
&t893_CustomAttributesCacheGenerator_m3926
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t893_1_0_0;
struct t893;
extern CustomAttributesCache t893__CustomAttributeCache_m3926;
TypeInfo t893_TI = 
{
	&g_System_dll_Image, NULL, "UriParser", "System", t893_MIs, t893_PIs, t893_FIs, NULL, &t29_TI, NULL, NULL, &t893_TI, NULL, t893_VT, &EmptyCustomAttributesCache, &t893_TI, &t893_0_0_0, &t893_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t893), 0, -1, sizeof(t893_SFs), 0, -1, 1048705, 0, false, false, false, false, false, false, false, false, false, true, false, false, 10, 2, 6, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t898_TI;
#include "t898MD.h"



// Metadata Definition System.UriPartial
extern Il2CppType t44_0_0_1542;
FieldInfo t898_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t898_TI, offsetof(t898, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t898_0_0_32854;
FieldInfo t898_f2_FieldInfo = 
{
	"Scheme", &t898_0_0_32854, &t898_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t898_0_0_32854;
FieldInfo t898_f3_FieldInfo = 
{
	"Authority", &t898_0_0_32854, &t898_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t898_0_0_32854;
FieldInfo t898_f4_FieldInfo = 
{
	"Path", &t898_0_0_32854, &t898_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t898_0_0_32854;
FieldInfo t898_f5_FieldInfo = 
{
	"Query", &t898_0_0_32854, &t898_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t898_FIs[] =
{
	&t898_f1_FieldInfo,
	&t898_f2_FieldInfo,
	&t898_f3_FieldInfo,
	&t898_f4_FieldInfo,
	&t898_f5_FieldInfo,
	NULL
};
static const int32_t t898_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t898_f2_DefaultValue = 
{
	&t898_f2_FieldInfo, { (char*)&t898_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t898_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t898_f3_DefaultValue = 
{
	&t898_f3_FieldInfo, { (char*)&t898_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t898_f4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t898_f4_DefaultValue = 
{
	&t898_f4_FieldInfo, { (char*)&t898_f4_DefaultValueData, &t44_0_0_0 }};
static const int32_t t898_f5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry t898_f5_DefaultValue = 
{
	&t898_f5_FieldInfo, { (char*)&t898_f5_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t898_FDVs[] = 
{
	&t898_f2_DefaultValue,
	&t898_f3_DefaultValue,
	&t898_f4_DefaultValue,
	&t898_f5_DefaultValue,
	NULL
};
static MethodInfo* t898_MIs[] =
{
	NULL
};
static MethodInfo* t898_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t898_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t898_1_0_0;
TypeInfo t898_TI = 
{
	&g_System_dll_Image, NULL, "UriPartial", "System", t898_MIs, NULL, t898_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t898_VT, &EmptyCustomAttributesCache, &t44_TI, &t898_0_0_0, &t898_1_0_0, t898_IOs, NULL, NULL, t898_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t898)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 257, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 5, 0, 0, 23, 0, 3};
#ifndef _MSC_VER
#else
#endif
#include "t902MD.h"



// Metadata Definition System.UriTypeConverter
static MethodInfo* t902_MIs[] =
{
	NULL
};
static MethodInfo* t902_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t902_0_0_0;
extern Il2CppType t902_1_0_0;
extern TypeInfo t738_TI;
struct t902;
TypeInfo t902_TI = 
{
	&g_System_dll_Image, NULL, "UriTypeConverter", "System", t902_MIs, NULL, NULL, NULL, &t738_TI, NULL, NULL, &t902_TI, NULL, t902_VT, &EmptyCustomAttributesCache, &t902_TI, &t902_0_0_0, &t902_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t902), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#include "t755.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t755_TI;
#include "t755MD.h"

#include "t745.h"
#include "t756.h"
#include "t741.h"


extern MethodInfo m3933_MI;
 void m3933 (t755 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m3934_MI;
 bool m3934 (t755 * __this, t29 * p0, t745 * p1, t756 * p2, int32_t p3, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m3934((t755 *)__this->f9,p0, p1, p2, p3, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (t29 *, t29 * __this, t29 * p0, t745 * p1, t756 * p2, int32_t p3, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1, p2, p3,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (t29 * __this, t29 * p0, t745 * p1, t756 * p2, int32_t p3, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1, p2, p3,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef bool (*FunctionPointerType) (t29 * __this, t745 * p1, t756 * p2, int32_t p3, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0, p1, p2, p3,(MethodInfo*)(__this->f3.f0));
	}
}
bool pinvoke_delegate_wrapper_t755(Il2CppObject* delegate, t29 * p0, t745 * p1, t756 * p2, int32_t p3)
{
	typedef int32_t (STDCALL *native_function_ptr_type)(t29 *, t745 *, t756 *, int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter 'p0' to native representation
	t29 * _p0_marshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));

	// Marshaling of parameter 'p1' to native representation
	t745 * _p1_marshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Security.Cryptography.X509Certificates.X509Certificate'."));

	// Marshaling of parameter 'p2' to native representation
	t756 * _p2_marshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Security.Cryptography.X509Certificates.X509Chain'."));

	// Marshaling of parameter 'p3' to native representation

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(_p0_marshaled, _p1_marshaled, _p2_marshaled, p3);

	// Marshaling cleanup of parameter 'p0' native representation

	// Marshaling cleanup of parameter 'p1' native representation

	// Marshaling cleanup of parameter 'p2' native representation

	// Marshaling cleanup of parameter 'p3' native representation

	return _return_value;
}
extern MethodInfo m3935_MI;
 t29 * m3935 (t755 * __this, t29 * p0, t745 * p1, t756 * p2, int32_t p3, t67 * p4, t29 * p5, MethodInfo* method){
	void *__d_args[5] = {0};
	__d_args[0] = p0;
	__d_args[1] = p1;
	__d_args[2] = p2;
	__d_args[3] = Box(InitializedTypeInfo(&t741_TI), &p3);
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p4, (Il2CppObject*)p5);
}
extern MethodInfo m3936_MI;
 bool m3936 (t755 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Net.Security.RemoteCertificateValidationCallback
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t755_m3933_ParameterInfos[] = 
{
	{"object", 0, 134218619, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134218620, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
MethodInfo m3933_MI = 
{
	".ctor", (methodPointerType)&m3933, &t755_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t755_m3933_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, false, 930, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t745_0_0_0;
extern Il2CppType t745_0_0_0;
extern Il2CppType t756_0_0_0;
extern Il2CppType t756_0_0_0;
extern Il2CppType t741_0_0_0;
extern Il2CppType t741_0_0_0;
static ParameterInfo t755_m3934_ParameterInfos[] = 
{
	{"sender", 0, 134218621, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"certificate", 1, 134218622, &EmptyCustomAttributesCache, &t745_0_0_0},
	{"chain", 2, 134218623, &EmptyCustomAttributesCache, &t756_0_0_0},
	{"sslPolicyErrors", 3, 134218624, &EmptyCustomAttributesCache, &t741_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3934_MI = 
{
	"Invoke", (methodPointerType)&m3934, &t755_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29_t29_t44, t755_m3934_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 4, false, false, 931, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t745_0_0_0;
extern Il2CppType t756_0_0_0;
extern Il2CppType t741_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t755_m3935_ParameterInfos[] = 
{
	{"sender", 0, 134218625, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"certificate", 1, 134218626, &EmptyCustomAttributesCache, &t745_0_0_0},
	{"chain", 2, 134218627, &EmptyCustomAttributesCache, &t756_0_0_0},
	{"sslPolicyErrors", 3, 134218628, &EmptyCustomAttributesCache, &t741_0_0_0},
	{"callback", 4, 134218629, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 5, 134218630, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3935_MI = 
{
	"BeginInvoke", (methodPointerType)&m3935, &t755_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t44_t29_t29, t755_m3935_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 6, false, false, 932, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t66_0_0_0;
static ParameterInfo t755_m3936_ParameterInfos[] = 
{
	{"result", 0, 134218631, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3936_MI = 
{
	"EndInvoke", (methodPointerType)&m3936, &t755_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t755_m3936_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, false, 933, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t755_MIs[] =
{
	&m3933_MI,
	&m3934_MI,
	&m3935_MI,
	&m3936_MI,
	NULL
};
static MethodInfo* t755_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m3934_MI,
	&m3935_MI,
	&m3936_MI,
};
static Il2CppInterfaceOffsetPair t755_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t755_0_0_0;
extern Il2CppType t755_1_0_0;
struct t755;
TypeInfo t755_TI = 
{
	&g_System_dll_Image, NULL, "RemoteCertificateValidationCallback", "System.Net.Security", t755_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t755_TI, NULL, t755_VT, &EmptyCustomAttributesCache, &t755_TI, &t755_0_0_0, &t755_1_0_0, t755_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)pinvoke_delegate_wrapper_t755, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t755), 0, sizeof(methodPointerType), 0, 0, -1, 257, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t903.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t903_TI;
#include "t903MD.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$128
void t903_marshal(const t903& unmarshaled, t903_marshaled& marshaled)
{
}
void t903_marshal_back(const t903_marshaled& marshaled, t903& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$128
void t903_marshal_cleanup(t903_marshaled& marshaled)
{
}
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$128
static MethodInfo* t903_MIs[] =
{
	NULL
};
static MethodInfo* t903_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t903_0_0_0;
extern Il2CppType t903_1_0_0;
extern TypeInfo t905_TI;
TypeInfo t903_TI = 
{
	&g_System_dll_Image, NULL, "$ArrayType$128", "", t903_MIs, NULL, NULL, NULL, &t110_TI, NULL, &t905_TI, &t903_TI, NULL, t903_VT, &EmptyCustomAttributesCache, &t903_TI, &t903_0_0_0, &t903_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t903_marshal, (methodPointerType)t903_marshal_back, (methodPointerType)t903_marshal_cleanup, sizeof (t903)+ sizeof (Il2CppObject), 0, sizeof(t903_marshaled), 0, 0, -1, 275, 0, true, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#include "t904.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t904_TI;
#include "t904MD.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$12
void t904_marshal(const t904& unmarshaled, t904_marshaled& marshaled)
{
}
void t904_marshal_back(const t904_marshaled& marshaled, t904& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$12
void t904_marshal_cleanup(t904_marshaled& marshaled)
{
}
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$12
static MethodInfo* t904_MIs[] =
{
	NULL
};
static MethodInfo* t904_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t904_0_0_0;
extern Il2CppType t904_1_0_0;
TypeInfo t904_TI = 
{
	&g_System_dll_Image, NULL, "$ArrayType$12", "", t904_MIs, NULL, NULL, NULL, &t110_TI, NULL, &t905_TI, &t904_TI, NULL, t904_VT, &EmptyCustomAttributesCache, &t904_TI, &t904_0_0_0, &t904_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t904_marshal, (methodPointerType)t904_marshal_back, (methodPointerType)t904_marshal_cleanup, sizeof (t904)+ sizeof (Il2CppObject), 0, sizeof(t904_marshaled), 0, 0, -1, 275, 0, true, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#include "t905.h"
#ifndef _MSC_VER
#else
#endif
#include "t905MD.h"



// Metadata Definition <PrivateImplementationDetails>
extern Il2CppType t903_0_0_275;
FieldInfo t905_f0_FieldInfo = 
{
	"$$field-2", &t903_0_0_275, &t905_TI, offsetof(t905_SFs, f0), &EmptyCustomAttributesCache};
extern Il2CppType t904_0_0_275;
FieldInfo t905_f1_FieldInfo = 
{
	"$$field-3", &t904_0_0_275, &t905_TI, offsetof(t905_SFs, f1), &EmptyCustomAttributesCache};
extern Il2CppType t904_0_0_275;
FieldInfo t905_f2_FieldInfo = 
{
	"$$field-4", &t904_0_0_275, &t905_TI, offsetof(t905_SFs, f2), &EmptyCustomAttributesCache};
static FieldInfo* t905_FIs[] =
{
	&t905_f0_FieldInfo,
	&t905_f1_FieldInfo,
	&t905_f2_FieldInfo,
	NULL
};
static const uint8_t t905_f0_DefaultValueData[] = { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x1, 0x1, 0x1, 0x1, 0x0, 0x0, 0x0, 0x1, 0x1, 0x0, 0x1, 0x1, 0x0, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x0, 0x0, 0x0, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x0, 0x1, 0x0, 0x0, 0x0 };
static Il2CppFieldDefaultValueEntry t905_f0_DefaultValue = 
{
	&t905_f0_FieldInfo, { (char*)t905_f0_DefaultValueData, &t903_0_0_0 }};
static const uint8_t t905_f1_DefaultValueData[] = { 0x2A, 0x86, 0x48, 0x86, 0xF7, 0xD, 0x1, 0x9, 0x1, 0x0, 0x0, 0x0 };
static Il2CppFieldDefaultValueEntry t905_f1_DefaultValue = 
{
	&t905_f1_FieldInfo, { (char*)t905_f1_DefaultValueData, &t904_0_0_0 }};
static const uint8_t t905_f2_DefaultValueData[] = { 0x2A, 0x86, 0x48, 0x86, 0xF7, 0xD, 0x1, 0x7, 0x2, 0x0, 0x0, 0x0 };
static Il2CppFieldDefaultValueEntry t905_f2_DefaultValue = 
{
	&t905_f2_FieldInfo, { (char*)t905_f2_DefaultValueData, &t904_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t905_FDVs[] = 
{
	&t905_f0_DefaultValue,
	&t905_f1_DefaultValue,
	&t905_f2_DefaultValue,
	NULL
};
static MethodInfo* t905_MIs[] =
{
	NULL
};
extern TypeInfo t903_TI;
extern TypeInfo t904_TI;
static TypeInfo* t905_TI__nestedTypes[3] =
{
	&t903_TI,
	&t904_TI,
	NULL
};
static MethodInfo* t905_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
void t905_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t905__CustomAttributeCache = {
1,
NULL,
&t905_CustomAttributesCacheGenerator
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t905_0_0_0;
extern Il2CppType t905_1_0_0;
struct t905;
extern CustomAttributesCache t905__CustomAttributeCache;
TypeInfo t905_TI = 
{
	&g_System_dll_Image, NULL, "<PrivateImplementationDetails>", "", t905_MIs, NULL, t905_FIs, NULL, &t29_TI, t905_TI__nestedTypes, NULL, &t905_TI, NULL, t905_VT, &t905__CustomAttributeCache, &t905_TI, &t905_0_0_0, &t905_1_0_0, NULL, NULL, NULL, t905_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t905), 0, -1, sizeof(t905_SFs), 0, -1, 0, 0, false, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 3, 0, 2, 4, 0, 0};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
