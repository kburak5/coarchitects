﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include "t1073.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t1073_TI;
#include "t1073MD.h"

#include "t21.h"
#include "t1020.h"
#include "mscorlib_ArrayTypes.h"
#include "t348.h"
#include "t1037.h"
#include "t1035.h"
#include "t1040.h"
#include "t945.h"
#include "t44.h"
#include "t780.h"
#include "t40.h"
#include "t1030.h"
#include "t1007.h"
#include "t1008.h"
#include "t1005.h"
#include "t809.h"
#include "t1010.h"
#include "t1022.h"
#include "t1042.h"
#include "t1019.h"
#include "t949.h"
#include "t7.h"
#include "t731.h"
#include "t29.h"
#include "t1009.h"
#include "t1016.h"
#include "t1057.h"
#include "t919.h"
#include "t745.h"
#include "t1002.h"
#include "t1003.h"
#include "t1031.h"
#include "t1065.h"
#include "t42.h"
#include "t43.h"
#include "t20.h"
#include "t1011.h"
#include "t1041.h"
#include "t829.h"
#include "t838.h"
#include "t828.h"
#include "t834.h"
#include "t833.h"
#include "t194.h"
#include "t633.h"
extern TypeInfo t1035_TI;
extern TypeInfo t21_TI;
extern TypeInfo t945_TI;
extern TypeInfo t780_TI;
extern TypeInfo t1030_TI;
extern TypeInfo t1008_TI;
extern TypeInfo t1005_TI;
extern TypeInfo t731_TI;
extern TypeInfo t40_TI;
extern TypeInfo t29_TI;
extern TypeInfo t1010_TI;
extern TypeInfo t1031_TI;
extern TypeInfo t1057_TI;
extern TypeInfo t919_TI;
extern TypeInfo t7_TI;
extern TypeInfo t1065_TI;
extern TypeInfo t781_TI;
extern TypeInfo t348_TI;
extern TypeInfo t745_TI;
extern TypeInfo t44_TI;
extern TypeInfo t1002_TI;
extern TypeInfo t42_TI;
extern TypeInfo t20_TI;
extern TypeInfo t841_TI;
extern TypeInfo t295_TI;
extern TypeInfo t1011_TI;
extern TypeInfo t829_TI;
extern TypeInfo t838_TI;
extern TypeInfo t828_TI;
extern TypeInfo t834_TI;
extern TypeInfo t633_TI;
#include "t1035MD.h"
#include "t1020MD.h"
#include "t1040MD.h"
#include "t945MD.h"
#include "t1036MD.h"
#include "t780MD.h"
#include "t1042MD.h"
#include "t1019MD.h"
#include "t949MD.h"
#include "t1008MD.h"
#include "t1005MD.h"
#include "t731MD.h"
#include "t1010MD.h"
#include "t1030MD.h"
#include "t1031MD.h"
#include "t1057MD.h"
#include "t7MD.h"
#include "t1065MD.h"
#include "t745MD.h"
#include "t1002MD.h"
#include "t42MD.h"
#include "t1041MD.h"
#include "t1011MD.h"
#include "t829MD.h"
#include "t838MD.h"
#include "t833MD.h"
#include "t828MD.h"
#include "t834MD.h"
#include "t830MD.h"
#include "t633MD.h"
extern Il2CppType t44_0_0_0;
extern MethodInfo m5014_MI;
extern MethodInfo m5019_MI;
extern MethodInfo m5015_MI;
extern MethodInfo m4717_MI;
extern MethodInfo m4976_MI;
extern MethodInfo m4985_MI;
extern MethodInfo m5052_MI;
extern MethodInfo m4511_MI;
extern MethodInfo m4998_MI;
extern MethodInfo m4999_MI;
extern MethodInfo m4102_MI;
extern MethodInfo m4514_MI;
extern MethodInfo m5054_MI;
extern MethodInfo m4087_MI;
extern MethodInfo m4761_MI;
extern MethodInfo m4831_MI;
extern MethodInfo m4624_MI;
extern MethodInfo m4161_MI;
extern MethodInfo m4162_MI;
extern MethodInfo m4579_MI;
extern MethodInfo m4570_MI;
extern MethodInfo m4582_MI;
extern MethodInfo m4573_MI;
extern MethodInfo m5210_MI;
extern MethodInfo m4584_MI;
extern MethodInfo m4586_MI;
extern MethodInfo m4677_MI;
extern MethodInfo m4868_MI;
extern MethodInfo m4872_MI;
extern MethodInfo m4838_MI;
extern MethodInfo m4839_MI;
extern MethodInfo m1535_MI;
extern MethodInfo m66_MI;
extern MethodInfo m4970_MI;
extern MethodInfo m4097_MI;
extern MethodInfo m4191_MI;
extern MethodInfo m5247_MI;
extern MethodInfo m3980_MI;
extern MethodInfo m5053_MI;
extern MethodInfo m3991_MI;
extern MethodInfo m5055_MI;
extern MethodInfo m4512_MI;
extern MethodInfo m4519_MI;
extern MethodInfo m4522_MI;
extern MethodInfo m4525_MI;
extern MethodInfo m4523_MI;
extern MethodInfo m1554_MI;
extern MethodInfo m4158_MI;
extern MethodInfo m4871_MI;
extern MethodInfo m4718_MI;
extern MethodInfo m4958_MI;
extern MethodInfo m4588_MI;
extern MethodInfo m4590_MI;
extern MethodInfo m5057_MI;
extern MethodInfo m4591_MI;
extern MethodInfo m1713_MI;
extern MethodInfo m4500_MI;
extern MethodInfo m5056_MI;
extern MethodInfo m3552_MI;
extern MethodInfo m3568_MI;
extern MethodInfo m3543_MI;
extern MethodInfo m3545_MI;
extern MethodInfo m3522_MI;
extern MethodInfo m3536_MI;
extern MethodInfo m3526_MI;
extern MethodInfo m3507_MI;
extern MethodInfo m2921_MI;
extern MethodInfo m1810_MI;
extern MethodInfo m4009_MI;
extern MethodInfo m4032_MI;
extern MethodInfo m1715_MI;
extern MethodInfo m1741_MI;
extern MethodInfo m4282_MI;
extern MethodInfo m1770_MI;
extern MethodInfo m5282_MI;
extern MethodInfo m1742_MI;


extern MethodInfo m5049_MI;
 void m5049 (t1073 * __this, t1020 * p0, t781* p1, MethodInfo* method){
	{
		m5014(__this, p0, ((int32_t)11), p1, &m5014_MI);
		return;
	}
}
extern MethodInfo m5050_MI;
 void m5050 (t1073 * __this, MethodInfo* method){
	{
		m5019(__this, &m5019_MI);
		t1020 * L_0 = m5015(__this, &m5015_MI);
		t1040 * L_1 = m4717(L_0, &m4717_MI);
		t945 * L_2 = (__this->f9);
		m4976(L_1, L_2, &m4976_MI);
		t1020 * L_3 = m5015(__this, &m5015_MI);
		t1040 * L_4 = m4717(L_3, &m4717_MI);
		m4985(L_4, &m4985_MI);
		return;
	}
}
extern MethodInfo m5051_MI;
 void m5051 (t1073 * __this, MethodInfo* method){
	{
		VirtActionInvoker0::Invoke(&m5052_MI, __this);
		return;
	}
}
 void m5052 (t1073 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	t781* V_3 = {0};
	t780 * V_4 = {0};
	{
		t945 * L_0 = (t945 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t945_TI));
		m4511(L_0, &m4511_MI);
		__this->f9 = L_0;
		V_0 = 0;
		int32_t L_1 = m4998(__this, &m4998_MI);
		V_1 = L_1;
		goto IL_004d;
	}

IL_0019:
	{
		int32_t L_2 = m4998(__this, &m4998_MI);
		V_2 = L_2;
		V_0 = ((int32_t)(V_0+3));
		if ((((int32_t)V_2) <= ((int32_t)0)))
		{
			goto IL_004d;
		}
	}
	{
		t781* L_3 = m4999(__this, V_2, &m4999_MI);
		V_3 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t780_TI));
		t780 * L_4 = (t780 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t780_TI));
		m4102(L_4, V_3, &m4102_MI);
		V_4 = L_4;
		t945 * L_5 = (__this->f9);
		m4514(L_5, V_4, &m4514_MI);
		V_0 = ((int32_t)(V_0+V_2));
	}

IL_004d:
	{
		if ((((int32_t)V_0) < ((int32_t)V_1)))
		{
			goto IL_0019;
		}
	}
	{
		t945 * L_6 = (__this->f9);
		m5054(__this, L_6, &m5054_MI);
		return;
	}
}
 bool m5053 (t1073 * __this, t780 * p0, MethodInfo* method){
	t1030 * V_0 = {0};
	int32_t V_1 = {0};
	t1008 * V_2 = {0};
	t1005 * V_3 = {0};
	t809 * V_4 = {0};
	t1010 * V_5 = {0};
	int32_t V_6 = {0};
	int32_t G_B19_0 = 0;
	int32_t G_B26_0 = 0;
	{
		t1020 * L_0 = m5015(__this, &m5015_MI);
		V_0 = ((t1030 *)Castclass(L_0, InitializedTypeInfo(&t1030_TI)));
		int32_t L_1 = m4087(p0, &m4087_MI);
		if ((((int32_t)L_1) >= ((int32_t)3)))
		{
			goto IL_001a;
		}
	}
	{
		return 1;
	}

IL_001a:
	{
		V_1 = 0;
		t1042 * L_2 = m4761(V_0, &m4761_MI);
		t1019 * L_3 = m4831(L_2, &m4831_MI);
		int32_t L_4 = m4624(L_3, &m4624_MI);
		V_6 = L_4;
		if (V_6 == 0)
		{
			goto IL_0061;
		}
		if (V_6 == 1)
		{
			goto IL_0068;
		}
		if (V_6 == 2)
		{
			goto IL_006a;
		}
		if (V_6 == 3)
		{
			goto IL_0059;
		}
		if (V_6 == 4)
		{
			goto IL_004e;
		}
	}
	{
		goto IL_006a;
	}

IL_004e:
	{
		V_1 = ((int32_t)128);
		goto IL_006a;
	}

IL_0059:
	{
		V_1 = ((int32_t)32);
		goto IL_006a;
	}

IL_0061:
	{
		V_1 = 8;
		goto IL_006a;
	}

IL_0068:
	{
		return 0;
	}

IL_006a:
	{
		V_2 = (t1008 *)NULL;
		V_3 = (t1005 *)NULL;
		t949 * L_5 = m4161(p0, &m4161_MI);
		t809 * L_6 = m4162(L_5, (t7*) &_stringLiteral400, &m4162_MI);
		V_4 = L_6;
		if (!V_4)
		{
			goto IL_008f;
		}
	}
	{
		t1008 * L_7 = (t1008 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1008_TI));
		m4579(L_7, V_4, &m4579_MI);
		V_2 = L_7;
	}

IL_008f:
	{
		t949 * L_8 = m4161(p0, &m4161_MI);
		t809 * L_9 = m4162(L_8, (t7*) &_stringLiteral407, &m4162_MI);
		V_4 = L_9;
		if (!V_4)
		{
			goto IL_00b0;
		}
	}
	{
		t1005 * L_10 = (t1005 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1005_TI));
		m4570(L_10, V_4, &m4570_MI);
		V_3 = L_10;
	}

IL_00b0:
	{
		if (!V_2)
		{
			goto IL_00f3;
		}
	}
	{
		if (!V_3)
		{
			goto IL_00f3;
		}
	}
	{
		bool L_11 = m4582(V_2, V_1, &m4582_MI);
		if (L_11)
		{
			goto IL_00ca;
		}
	}
	{
		return 0;
	}

IL_00ca:
	{
		t731 * L_12 = m4573(V_3, &m4573_MI);
		bool L_13 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m5210_MI, L_12, (t7*) &_stringLiteral411);
		if (L_13)
		{
			goto IL_00f1;
		}
	}
	{
		t731 * L_14 = m4573(V_3, &m4573_MI);
		bool L_15 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m5210_MI, L_14, (t7*) &_stringLiteral853);
		G_B19_0 = ((int32_t)(L_15));
		goto IL_00f2;
	}

IL_00f1:
	{
		G_B19_0 = 1;
	}

IL_00f2:
	{
		return G_B19_0;
	}

IL_00f3:
	{
		if (!V_2)
		{
			goto IL_0101;
		}
	}
	{
		bool L_16 = m4582(V_2, V_1, &m4582_MI);
		return L_16;
	}

IL_0101:
	{
		if (!V_3)
		{
			goto IL_0130;
		}
	}
	{
		t731 * L_17 = m4573(V_3, &m4573_MI);
		bool L_18 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m5210_MI, L_17, (t7*) &_stringLiteral411);
		if (L_18)
		{
			goto IL_012e;
		}
	}
	{
		t731 * L_19 = m4573(V_3, &m4573_MI);
		bool L_20 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m5210_MI, L_19, (t7*) &_stringLiteral853);
		G_B26_0 = ((int32_t)(L_20));
		goto IL_012f;
	}

IL_012e:
	{
		G_B26_0 = 1;
	}

IL_012f:
	{
		return G_B26_0;
	}

IL_0130:
	{
		t949 * L_21 = m4161(p0, &m4161_MI);
		t809 * L_22 = m4162(L_21, (t7*) &_stringLiteral438, &m4162_MI);
		V_4 = L_22;
		if (!V_4)
		{
			goto IL_015c;
		}
	}
	{
		t1010 * L_23 = (t1010 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1010_TI));
		m4584(L_23, V_4, &m4584_MI);
		V_5 = L_23;
		bool L_24 = m4586(V_5, ((int32_t)64), &m4586_MI);
		return L_24;
	}

IL_015c:
	{
		return 1;
	}
}
 void m5054 (t1073 * __this, t945 * p0, MethodInfo* method){
	t1030 * V_0 = {0};
	uint8_t V_1 = {0};
	t1057 * V_2 = {0};
	int64_t V_3 = 0;
	t7* V_4 = {0};
	t780 * V_5 = {0};
	t745 * V_6 = {0};
	t731 * V_7 = {0};
	t945 * V_8 = {0};
	t1002 * V_9 = {0};
	bool V_10 = false;
	t841* V_11 = {0};
	int64_t V_12 = 0;
	int32_t V_13 = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		t1020 * L_0 = m5015(__this, &m5015_MI);
		V_0 = ((t1030 *)Castclass(L_0, InitializedTypeInfo(&t1030_TI)));
		V_1 = ((int32_t)42);
		t1031 * L_1 = m4677(V_0, &m4677_MI);
		bool L_2 = (bool)VirtFuncInvoker0< bool >::Invoke(&m4868_MI, L_1);
		if (!L_2)
		{
			goto IL_00b4;
		}
	}
	{
		t1031 * L_3 = m4677(V_0, &m4677_MI);
		t1057 * L_4 = (t1057 *)VirtFuncInvoker1< t1057 *, t945 * >::Invoke(&m4872_MI, L_3, p0);
		V_2 = L_4;
		bool L_5 = m4838(V_2, &m4838_MI);
		if (!L_5)
		{
			goto IL_0038;
		}
	}
	{
		return;
	}

IL_0038:
	{
		int32_t L_6 = m4839(V_2, &m4839_MI);
		V_3 = (((int64_t)L_6));
		V_12 = V_3;
		if ((((int64_t)V_12) == ((int64_t)(((uint64_t)(((uint32_t)((int32_t)-2146762487))))))))
		{
			goto IL_007f;
		}
	}
	{
		if ((((int64_t)V_12) == ((int64_t)(((uint64_t)(((uint32_t)((int32_t)-2146762486))))))))
		{
			goto IL_0077;
		}
	}
	{
		if ((((int64_t)V_12) == ((int64_t)(((uint64_t)(((uint32_t)((int32_t)-2146762495))))))))
		{
			goto IL_006f;
		}
	}
	{
		goto IL_0087;
	}

IL_006f:
	{
		V_1 = ((int32_t)45);
		goto IL_008f;
	}

IL_0077:
	{
		V_1 = ((int32_t)48);
		goto IL_008f;
	}

IL_007f:
	{
		V_1 = ((int32_t)48);
		goto IL_008f;
	}

IL_0087:
	{
		V_1 = ((int32_t)46);
		goto IL_008f;
	}

IL_008f:
	{
		int64_t L_7 = V_3;
		t29 * L_8 = Box(InitializedTypeInfo(&t919_TI), &L_7);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_9 = m1535(NULL, (t7*) &_stringLiteral854, L_8, &m1535_MI);
		V_4 = L_9;
		t7* L_10 = m66(NULL, (t7*) &_stringLiteral855, V_4, &m66_MI);
		t1065 * L_11 = (t1065 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1065_TI));
		m4970(L_11, V_1, L_10, &m4970_MI);
		il2cpp_codegen_raise_exception(L_11);
	}

IL_00b4:
	{
		t780 * L_12 = m4097(p0, 0, &m4097_MI);
		V_5 = L_12;
		t781* L_13 = (t781*)VirtFuncInvoker0< t781* >::Invoke(&m4191_MI, V_5);
		t745 * L_14 = (t745 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t745_TI));
		m5247(L_14, L_13, &m5247_MI);
		V_6 = L_14;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t731_TI));
		t731 * L_15 = (t731 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t731_TI));
		m3980(L_15, &m3980_MI);
		V_7 = L_15;
		bool L_16 = m5053(__this, V_5, &m5053_MI);
		if (L_16)
		{
			goto IL_00f1;
		}
	}
	{
		int32_t L_17 = ((int32_t)-2146762490);
		t29 * L_18 = Box(InitializedTypeInfo(&t44_TI), &L_17);
		VirtFuncInvoker1< int32_t, t29 * >::Invoke(&m3991_MI, V_7, L_18);
	}

IL_00f1:
	{
		bool L_19 = m5055(__this, V_5, &m5055_MI);
		if (L_19)
		{
			goto IL_0110;
		}
	}
	{
		int32_t L_20 = ((int32_t)-2146762481);
		t29 * L_21 = Box(InitializedTypeInfo(&t44_TI), &L_20);
		VirtFuncInvoker1< int32_t, t29 * >::Invoke(&m3991_MI, V_7, L_21);
	}

IL_0110:
	{
		t945 * L_22 = (t945 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t945_TI));
		m4512(L_22, p0, &m4512_MI);
		V_8 = L_22;
		m4519(V_8, V_5, &m4519_MI);
		t1002 * L_23 = (t1002 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1002_TI));
		m4522(L_23, V_8, &m4522_MI);
		V_9 = L_23;
		V_10 = 0;
	}

IL_012d:
	try
	{ // begin try (depth: 1)
		bool L_24 = m4525(V_9, V_5, &m4525_MI);
		V_10 = L_24;
		// IL_0138: leave IL_0146
		goto IL_0146;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t295_TI, e.ex->object.klass))
			goto IL_013d;
		throw e;
	}

IL_013d:
	{ // begin catch(System.Exception)
		V_10 = 0;
		// IL_0141: leave IL_0146
		goto IL_0146;
	} // end catch (depth: 1)

IL_0146:
	{
		if (V_10)
		{
			goto IL_0243;
		}
	}
	{
		int32_t L_25 = m4523(V_9, &m4523_MI);
		V_13 = L_25;
		if ((((int32_t)V_13) == ((int32_t)1)))
		{
			goto IL_01d9;
		}
	}
	{
		if ((((int32_t)V_13) == ((int32_t)2)))
		{
			goto IL_01c2;
		}
	}
	{
		if ((((int32_t)V_13) == ((int32_t)8)))
		{
			goto IL_01ab;
		}
	}
	{
		if ((((int32_t)V_13) == ((int32_t)((int32_t)32))))
		{
			goto IL_020d;
		}
	}
	{
		if ((((int32_t)V_13) == ((int32_t)((int32_t)1024))))
		{
			goto IL_0194;
		}
	}
	{
		if ((((int32_t)V_13) == ((int32_t)((int32_t)65536))))
		{
			goto IL_01f3;
		}
	}
	{
		goto IL_0227;
	}

IL_0194:
	{
		int32_t L_26 = ((int32_t)-2146869223);
		t29 * L_27 = Box(InitializedTypeInfo(&t44_TI), &L_26);
		VirtFuncInvoker1< int32_t, t29 * >::Invoke(&m3991_MI, V_7, L_27);
		goto IL_0243;
	}

IL_01ab:
	{
		int32_t L_28 = ((int32_t)-2146869232);
		t29 * L_29 = Box(InitializedTypeInfo(&t44_TI), &L_28);
		VirtFuncInvoker1< int32_t, t29 * >::Invoke(&m3991_MI, V_7, L_29);
		goto IL_0243;
	}

IL_01c2:
	{
		int32_t L_30 = ((int32_t)-2146762494);
		t29 * L_31 = Box(InitializedTypeInfo(&t44_TI), &L_30);
		VirtFuncInvoker1< int32_t, t29 * >::Invoke(&m3991_MI, V_7, L_31);
		goto IL_0243;
	}

IL_01d9:
	{
		V_1 = ((int32_t)45);
		int32_t L_32 = ((int32_t)-2146762495);
		t29 * L_33 = Box(InitializedTypeInfo(&t44_TI), &L_32);
		VirtFuncInvoker1< int32_t, t29 * >::Invoke(&m3991_MI, V_7, L_33);
		goto IL_0243;
	}

IL_01f3:
	{
		V_1 = ((int32_t)48);
		int32_t L_34 = ((int32_t)-2146762486);
		t29 * L_35 = Box(InitializedTypeInfo(&t44_TI), &L_34);
		VirtFuncInvoker1< int32_t, t29 * >::Invoke(&m3991_MI, V_7, L_35);
		goto IL_0243;
	}

IL_020d:
	{
		V_1 = ((int32_t)48);
		int32_t L_36 = ((int32_t)-2146762487);
		t29 * L_37 = Box(InitializedTypeInfo(&t44_TI), &L_36);
		VirtFuncInvoker1< int32_t, t29 * >::Invoke(&m3991_MI, V_7, L_37);
		goto IL_0243;
	}

IL_0227:
	{
		V_1 = ((int32_t)46);
		int32_t L_38 = m4523(V_9, &m4523_MI);
		int32_t L_39 = L_38;
		t29 * L_40 = Box(InitializedTypeInfo(&t44_TI), &L_39);
		VirtFuncInvoker1< int32_t, t29 * >::Invoke(&m3991_MI, V_7, L_40);
		goto IL_0243;
	}

IL_0243:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_41 = m1554(NULL, LoadTypeToken(&t44_0_0_0), &m1554_MI);
		t20 * L_42 = (t20 *)VirtFuncInvoker1< t20 *, t42 * >::Invoke(&m4158_MI, V_7, L_41);
		V_11 = ((t841*)Castclass(L_42, InitializedTypeInfo(&t841_TI)));
		t1031 * L_43 = m4677(V_0, &m4677_MI);
		bool L_44 = (bool)VirtFuncInvoker2< bool, t745 *, t841* >::Invoke(&m4871_MI, L_43, V_6, V_11);
		if (L_44)
		{
			goto IL_027b;
		}
	}
	{
		t1065 * L_45 = (t1065 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1065_TI));
		m4970(L_45, V_1, (t7*) &_stringLiteral856, &m4970_MI);
		il2cpp_codegen_raise_exception(L_45);
	}

IL_027b:
	{
		return;
	}
}
 bool m5055 (t1073 * __this, t780 * p0, MethodInfo* method){
	t1030 * V_0 = {0};
	t7* V_1 = {0};
	t809 * V_2 = {0};
	t1011 * V_3 = {0};
	t7* V_4 = {0};
	t446* V_5 = {0};
	int32_t V_6 = 0;
	t7* V_7 = {0};
	t446* V_8 = {0};
	int32_t V_9 = 0;
	{
		t1020 * L_0 = m5015(__this, &m5015_MI);
		V_0 = ((t1030 *)Castclass(L_0, InitializedTypeInfo(&t1030_TI)));
		t1041 * L_1 = m4718(V_0, &m4718_MI);
		t7* L_2 = m4958(L_1, &m4958_MI);
		V_1 = L_2;
		t949 * L_3 = m4161(p0, &m4161_MI);
		t809 * L_4 = m4162(L_3, (t7*) &_stringLiteral437, &m4162_MI);
		V_2 = L_4;
		if (!V_2)
		{
			goto IL_00a4;
		}
	}
	{
		t1011 * L_5 = (t1011 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1011_TI));
		m4588(L_5, V_2, &m4588_MI);
		V_3 = L_5;
		t446* L_6 = m4590(V_3, &m4590_MI);
		V_5 = L_6;
		V_6 = 0;
		goto IL_0062;
	}

IL_0046:
	{
		int32_t L_7 = V_6;
		V_4 = (*(t7**)(t7**)SZArrayLdElema(V_5, L_7));
		bool L_8 = m5057(NULL, V_1, V_4, &m5057_MI);
		if (!L_8)
		{
			goto IL_005c;
		}
	}
	{
		return 1;
	}

IL_005c:
	{
		V_6 = ((int32_t)(V_6+1));
	}

IL_0062:
	{
		if ((((int32_t)V_6) < ((int32_t)(((int32_t)(((t20 *)V_5)->max_length))))))
		{
			goto IL_0046;
		}
	}
	{
		t446* L_9 = m4591(V_3, &m4591_MI);
		V_8 = L_9;
		V_9 = 0;
		goto IL_0099;
	}

IL_007d:
	{
		int32_t L_10 = V_9;
		V_7 = (*(t7**)(t7**)SZArrayLdElema(V_8, L_10));
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_11 = m1713(NULL, V_7, V_1, &m1713_MI);
		if (!L_11)
		{
			goto IL_0093;
		}
	}
	{
		return 1;
	}

IL_0093:
	{
		V_9 = ((int32_t)(V_9+1));
	}

IL_0099:
	{
		if ((((int32_t)V_9) < ((int32_t)(((int32_t)(((t20 *)V_8)->max_length))))))
		{
			goto IL_007d;
		}
	}

IL_00a4:
	{
		t7* L_12 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m4500_MI, p0);
		bool L_13 = m5056(__this, L_12, &m5056_MI);
		return L_13;
	}
}
 bool m5056 (t1073 * __this, t7* p0, MethodInfo* method){
	t1030 * V_0 = {0};
	t7* V_1 = {0};
	t829 * V_2 = {0};
	t838 * V_3 = {0};
	{
		t1020 * L_0 = m5015(__this, &m5015_MI);
		V_0 = ((t1030 *)Castclass(L_0, InitializedTypeInfo(&t1030_TI)));
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		V_1 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t829_TI));
		t829 * L_1 = (t829 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t829_TI));
		m3552(L_1, (t7*) &_stringLiteral857, &m3552_MI);
		V_2 = L_1;
		t838 * L_2 = m3568(V_2, p0, &m3568_MI);
		V_3 = L_2;
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m3543_MI, V_3);
		if ((((uint32_t)L_3) != ((uint32_t)1)))
		{
			goto IL_005f;
		}
	}
	{
		t828 * L_4 = (t828 *)VirtFuncInvoker1< t828 *, int32_t >::Invoke(&m3545_MI, V_3, 0);
		bool L_5 = m3522(L_4, &m3522_MI);
		if (!L_5)
		{
			goto IL_005f;
		}
	}
	{
		t828 * L_6 = (t828 *)VirtFuncInvoker1< t828 *, int32_t >::Invoke(&m3545_MI, V_3, 0);
		t834 * L_7 = (t834 *)VirtFuncInvoker0< t834 * >::Invoke(&m3536_MI, L_6);
		t833 * L_8 = m3526(L_7, 1, &m3526_MI);
		t7* L_9 = m3507(L_8, &m3507_MI);
		t7* L_10 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2921_MI, L_9);
		V_1 = L_10;
	}

IL_005f:
	{
		t1041 * L_11 = m4718(V_0, &m4718_MI);
		t7* L_12 = m4958(L_11, &m4958_MI);
		bool L_13 = m5057(NULL, L_12, V_1, &m5057_MI);
		return L_13;
	}
}
 bool m5057 (t29 * __this, t7* p0, t7* p1, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	t7* V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	t7* V_5 = {0};
	int32_t G_B15_0 = 0;
	{
		int32_t L_0 = m1810(p1, ((int32_t)42), &m1810_MI);
		V_0 = L_0;
		if ((((uint32_t)V_0) != ((uint32_t)(-1))))
		{
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t633_TI));
		t633 * L_1 = m4009(NULL, &m4009_MI);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		int32_t L_2 = m4032(NULL, p0, p1, 1, L_1, &m4032_MI);
		return ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}

IL_0021:
	{
		int32_t L_3 = m1715(p1, &m1715_MI);
		if ((((int32_t)V_0) == ((int32_t)((int32_t)(L_3-1)))))
		{
			goto IL_0041;
		}
	}
	{
		uint16_t L_4 = m1741(p1, ((int32_t)(V_0+1)), &m1741_MI);
		if ((((int32_t)L_4) == ((int32_t)((int32_t)46))))
		{
			goto IL_0041;
		}
	}
	{
		return 0;
	}

IL_0041:
	{
		int32_t L_5 = m4282(p1, ((int32_t)42), ((int32_t)(V_0+1)), &m4282_MI);
		V_1 = L_5;
		if ((((int32_t)V_1) == ((int32_t)(-1))))
		{
			goto IL_0056;
		}
	}
	{
		return 0;
	}

IL_0056:
	{
		t7* L_6 = m1770(p1, ((int32_t)(V_0+1)), &m1770_MI);
		V_2 = L_6;
		int32_t L_7 = m1715(p0, &m1715_MI);
		int32_t L_8 = m1715(V_2, &m1715_MI);
		V_3 = ((int32_t)(L_7-L_8));
		if ((((int32_t)V_3) > ((int32_t)0)))
		{
			goto IL_0077;
		}
	}
	{
		return 0;
	}

IL_0077:
	{
		int32_t L_9 = m1715(V_2, &m1715_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t633_TI));
		t633 * L_10 = m4009(NULL, &m4009_MI);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		int32_t L_11 = m5282(NULL, p0, V_3, V_2, 0, L_9, 1, L_10, &m5282_MI);
		if (!L_11)
		{
			goto IL_0093;
		}
	}
	{
		return 0;
	}

IL_0093:
	{
		if (V_0)
		{
			goto IL_00c3;
		}
	}
	{
		int32_t L_12 = m1810(p0, ((int32_t)46), &m1810_MI);
		V_4 = L_12;
		if ((((int32_t)V_4) == ((int32_t)(-1))))
		{
			goto IL_00c1;
		}
	}
	{
		int32_t L_13 = m1715(p0, &m1715_MI);
		int32_t L_14 = m1715(V_2, &m1715_MI);
		G_B15_0 = ((((int32_t)((((int32_t)V_4) < ((int32_t)((int32_t)(L_13-L_14))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_00c2;
	}

IL_00c1:
	{
		G_B15_0 = 1;
	}

IL_00c2:
	{
		return G_B15_0;
	}

IL_00c3:
	{
		t7* L_15 = m1742(p1, 0, V_0, &m1742_MI);
		V_5 = L_15;
		int32_t L_16 = m1715(V_5, &m1715_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t633_TI));
		t633 * L_17 = m4009(NULL, &m4009_MI);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		int32_t L_18 = m5282(NULL, p0, 0, V_5, 0, L_16, 1, L_17, &m5282_MI);
		return ((((int32_t)L_18) == ((int32_t)0))? 1 : 0);
	}
}
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate
extern Il2CppType t945_0_0_1;
FieldInfo t1073_f9_FieldInfo = 
{
	"certificates", &t945_0_0_1, &t1073_TI, offsetof(t1073, f9), &EmptyCustomAttributesCache};
static FieldInfo* t1073_FIs[] =
{
	&t1073_f9_FieldInfo,
	NULL
};
extern Il2CppType t1020_0_0_0;
extern Il2CppType t1020_0_0_0;
extern Il2CppType t781_0_0_0;
extern Il2CppType t781_0_0_0;
static ParameterInfo t1073_m5049_ParameterInfos[] = 
{
	{"context", 0, 134218531, &EmptyCustomAttributesCache, &t1020_0_0_0},
	{"buffer", 1, 134218532, &EmptyCustomAttributesCache, &t781_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m5049_MI = 
{
	".ctor", (methodPointerType)&m5049, &t1073_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t1073_m5049_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 834, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m5050_MI = 
{
	"Update", (methodPointerType)&m5050, &t1073_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 198, 0, 26, 0, false, false, 835, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m5051_MI = 
{
	"ProcessAsSsl3", (methodPointerType)&m5051, &t1073_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 25, 0, false, false, 836, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m5052_MI = 
{
	"ProcessAsTls1", (methodPointerType)&m5052, &t1073_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 24, 0, false, false, 837, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t780_0_0_0;
extern Il2CppType t780_0_0_0;
static ParameterInfo t1073_m5053_ParameterInfos[] = 
{
	{"cert", 0, 134218533, &EmptyCustomAttributesCache, &t780_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m5053_MI = 
{
	"checkCertificateUsage", (methodPointerType)&m5053, &t1073_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t1073_m5053_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 838, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t945_0_0_0;
extern Il2CppType t945_0_0_0;
static ParameterInfo t1073_m5054_ParameterInfos[] = 
{
	{"certificates", 0, 134218534, &EmptyCustomAttributesCache, &t945_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m5054_MI = 
{
	"validateCertificates", (methodPointerType)&m5054, &t1073_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1073_m5054_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 839, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t780_0_0_0;
static ParameterInfo t1073_m5055_ParameterInfos[] = 
{
	{"cert", 0, 134218535, &EmptyCustomAttributesCache, &t780_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m5055_MI = 
{
	"checkServerIdentity", (methodPointerType)&m5055, &t1073_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t1073_m5055_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 840, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t1073_m5056_ParameterInfos[] = 
{
	{"subjectName", 0, 134218536, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m5056_MI = 
{
	"checkDomainName", (methodPointerType)&m5056, &t1073_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t1073_m5056_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 841, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t1073_m5057_ParameterInfos[] = 
{
	{"hostname", 0, 134218537, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"pattern", 1, 134218538, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m5057_MI = 
{
	"Match", (methodPointerType)&m5057, &t1073_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t1073_m5057_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, false, 842, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1073_MIs[] =
{
	&m5049_MI,
	&m5050_MI,
	&m5051_MI,
	&m5052_MI,
	&m5053_MI,
	&m5054_MI,
	&m5055_MI,
	&m5056_MI,
	&m5057_MI,
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m1332_MI;
extern MethodInfo m5225_MI;
extern MethodInfo m4990_MI;
extern MethodInfo m4991_MI;
extern MethodInfo m4989_MI;
extern MethodInfo m4994_MI;
extern MethodInfo m4992_MI;
extern MethodInfo m4993_MI;
extern MethodInfo m5269_MI;
extern MethodInfo m5184_MI;
extern MethodInfo m5007_MI;
extern MethodInfo m5010_MI;
extern MethodInfo m5226_MI;
extern MethodInfo m5009_MI;
extern MethodInfo m5008_MI;
extern MethodInfo m5011_MI;
extern MethodInfo m5215_MI;
extern MethodInfo m5232_MI;
extern MethodInfo m5242_MI;
extern MethodInfo m5234_MI;
extern MethodInfo m5243_MI;
extern MethodInfo m5020_MI;
static MethodInfo* t1073_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m5225_MI,
	&m4990_MI,
	&m4991_MI,
	&m4989_MI,
	&m4994_MI,
	&m4992_MI,
	&m4993_MI,
	&m5269_MI,
	&m5184_MI,
	&m5007_MI,
	&m5010_MI,
	&m5226_MI,
	&m5009_MI,
	&m5008_MI,
	&m5011_MI,
	&m5215_MI,
	&m5232_MI,
	&m5242_MI,
	&m5234_MI,
	&m5243_MI,
	&m5052_MI,
	&m5051_MI,
	&m5050_MI,
	&m5020_MI,
};
extern TypeInfo t324_TI;
static Il2CppInterfaceOffsetPair t1073_IOs[] = 
{
	{ &t324_TI, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType t1073_0_0_0;
extern Il2CppType t1073_1_0_0;
struct t1073;
TypeInfo t1073_TI = 
{
	&g_Mono_Security_dll_Image, NULL, "TlsServerCertificate", "Mono.Security.Protocol.Tls.Handshake.Client", t1073_MIs, NULL, t1073_FIs, NULL, &t1035_TI, NULL, NULL, &t1073_TI, NULL, t1073_VT, &EmptyCustomAttributesCache, &t1073_TI, &t1073_0_0_0, &t1073_1_0_0, t1073_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1073), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 9, 0, 1, 0, 0, 28, 0, 1};
#include "t1074.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1074_TI;
#include "t1074MD.h"

#include "Mono.Security_ArrayTypes.h"
#include "t1067.h"
#include "t793.h"
#include "t372.h"
#include "t943.h"
extern TypeInfo t1066_TI;
extern TypeInfo t1067_TI;
extern TypeInfo t793_TI;
extern TypeInfo t446_TI;
extern TypeInfo t943_TI;
#include "t793MD.h"
#include "t943MD.h"
extern MethodInfo m4983_MI;
extern MethodInfo m4984_MI;
extern MethodInfo m4982_MI;
extern MethodInfo m5061_MI;
extern MethodInfo m4996_MI;
extern MethodInfo m4997_MI;
extern MethodInfo m4056_MI;
extern MethodInfo m4060_MI;
extern MethodInfo m4061_MI;
extern MethodInfo m4059_MI;
extern MethodInfo m4091_MI;
extern MethodInfo m4092_MI;


extern MethodInfo m5058_MI;
 void m5058 (t1074 * __this, t1020 * p0, t781* p1, MethodInfo* method){
	{
		m5014(__this, p0, ((int32_t)13), p1, &m5014_MI);
		return;
	}
}
extern MethodInfo m5059_MI;
 void m5059 (t1074 * __this, MethodInfo* method){
	{
		m5019(__this, &m5019_MI);
		t1020 * L_0 = m5015(__this, &m5015_MI);
		t1040 * L_1 = m4717(L_0, &m4717_MI);
		t1066* L_2 = (__this->f9);
		m4983(L_1, L_2, &m4983_MI);
		t1020 * L_3 = m5015(__this, &m5015_MI);
		t1040 * L_4 = m4717(L_3, &m4717_MI);
		t446* L_5 = (__this->f10);
		m4984(L_4, L_5, &m4984_MI);
		t1020 * L_6 = m5015(__this, &m5015_MI);
		t1040 * L_7 = m4717(L_6, &m4717_MI);
		m4982(L_7, 1, &m4982_MI);
		return;
	}
}
extern MethodInfo m5060_MI;
 void m5060 (t1074 * __this, MethodInfo* method){
	{
		VirtActionInvoker0::Invoke(&m5061_MI, __this);
		return;
	}
}
 void m5061 (t1074 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	t793 * V_2 = {0};
	int32_t V_3 = 0;
	t793 * V_4 = {0};
	{
		uint8_t L_0 = m4996(__this, &m4996_MI);
		V_0 = L_0;
		__this->f9 = ((t1066*)SZArrayNew(InitializedTypeInfo(&t1066_TI), V_0));
		V_1 = 0;
		goto IL_002c;
	}

IL_001a:
	{
		t1066* L_1 = (__this->f9);
		uint8_t L_2 = m4996(__this, &m4996_MI);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_1, V_1)) = (int32_t)L_2;
		V_1 = ((int32_t)(V_1+1));
	}

IL_002c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_001a;
		}
	}
	{
		int16_t L_3 = m4997(__this, &m4997_MI);
		if (!L_3)
		{
			goto IL_00aa;
		}
	}
	{
		int16_t L_4 = m4997(__this, &m4997_MI);
		t781* L_5 = m4999(__this, L_4, &m4999_MI);
		t793 * L_6 = (t793 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t793_TI));
		m4056(L_6, L_5, &m4056_MI);
		V_2 = L_6;
		int32_t L_7 = m4060(V_2, &m4060_MI);
		__this->f10 = ((t446*)SZArrayNew(InitializedTypeInfo(&t446_TI), L_7));
		V_3 = 0;
		goto IL_009e;
	}

IL_0068:
	{
		t793 * L_8 = m4061(V_2, V_3, &m4061_MI);
		t781* L_9 = m4059(L_8, &m4059_MI);
		t793 * L_10 = (t793 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t793_TI));
		m4056(L_10, L_9, &m4056_MI);
		V_4 = L_10;
		t446* L_11 = (__this->f10);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t943_TI));
		t943 * L_12 = m4091(NULL, &m4091_MI);
		t793 * L_13 = m4061(V_4, 1, &m4061_MI);
		t781* L_14 = m4059(L_13, &m4059_MI);
		t7* L_15 = (t7*)VirtFuncInvoker1< t7*, t781* >::Invoke(&m4092_MI, L_12, L_14);
		ArrayElementTypeCheck (L_11, L_15);
		*((t7**)(t7**)SZArrayLdElema(L_11, V_3)) = (t7*)L_15;
		V_3 = ((int32_t)(V_3+1));
	}

IL_009e:
	{
		int32_t L_16 = m4060(V_2, &m4060_MI);
		if ((((int32_t)V_3) < ((int32_t)L_16)))
		{
			goto IL_0068;
		}
	}

IL_00aa:
	{
		return;
	}
}
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest
extern Il2CppType t1066_0_0_1;
FieldInfo t1074_f9_FieldInfo = 
{
	"certificateTypes", &t1066_0_0_1, &t1074_TI, offsetof(t1074, f9), &EmptyCustomAttributesCache};
extern Il2CppType t446_0_0_1;
FieldInfo t1074_f10_FieldInfo = 
{
	"distinguisedNames", &t446_0_0_1, &t1074_TI, offsetof(t1074, f10), &EmptyCustomAttributesCache};
static FieldInfo* t1074_FIs[] =
{
	&t1074_f9_FieldInfo,
	&t1074_f10_FieldInfo,
	NULL
};
extern Il2CppType t1020_0_0_0;
extern Il2CppType t781_0_0_0;
static ParameterInfo t1074_m5058_ParameterInfos[] = 
{
	{"context", 0, 134218539, &EmptyCustomAttributesCache, &t1020_0_0_0},
	{"buffer", 1, 134218540, &EmptyCustomAttributesCache, &t781_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m5058_MI = 
{
	".ctor", (methodPointerType)&m5058, &t1074_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t1074_m5058_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 843, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m5059_MI = 
{
	"Update", (methodPointerType)&m5059, &t1074_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 198, 0, 26, 0, false, false, 844, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m5060_MI = 
{
	"ProcessAsSsl3", (methodPointerType)&m5060, &t1074_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 25, 0, false, false, 845, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m5061_MI = 
{
	"ProcessAsTls1", (methodPointerType)&m5061, &t1074_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 24, 0, false, false, 846, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1074_MIs[] =
{
	&m5058_MI,
	&m5059_MI,
	&m5060_MI,
	&m5061_MI,
	NULL
};
static MethodInfo* t1074_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m5225_MI,
	&m4990_MI,
	&m4991_MI,
	&m4989_MI,
	&m4994_MI,
	&m4992_MI,
	&m4993_MI,
	&m5269_MI,
	&m5184_MI,
	&m5007_MI,
	&m5010_MI,
	&m5226_MI,
	&m5009_MI,
	&m5008_MI,
	&m5011_MI,
	&m5215_MI,
	&m5232_MI,
	&m5242_MI,
	&m5234_MI,
	&m5243_MI,
	&m5061_MI,
	&m5060_MI,
	&m5059_MI,
	&m5020_MI,
};
static Il2CppInterfaceOffsetPair t1074_IOs[] = 
{
	{ &t324_TI, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType t1074_0_0_0;
extern Il2CppType t1074_1_0_0;
struct t1074;
TypeInfo t1074_TI = 
{
	&g_Mono_Security_dll_Image, NULL, "TlsServerCertificateRequest", "Mono.Security.Protocol.Tls.Handshake.Client", t1074_MIs, NULL, t1074_FIs, NULL, &t1035_TI, NULL, NULL, &t1074_TI, NULL, t1074_VT, &EmptyCustomAttributesCache, &t1074_TI, &t1074_0_0_0, &t1074_1_0_0, t1074_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1074), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 2, 0, 0, 28, 0, 1};
#include "t1075.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1075_TI;
#include "t1075MD.h"

#include "t1088.h"
#include "t1087.h"
#include "t927.h"
#include "t1044.h"
#include "t955.h"
#include "t1061.h"
#include "t1036.h"
#include "t1014.h"
extern TypeInfo t1088_TI;
extern TypeInfo t1061_TI;
extern TypeInfo t955_TI;
extern TypeInfo t1019_TI;
extern TypeInfo t1036_TI;
extern TypeInfo t1014_TI;
#include "t1088MD.h"
#include "t928MD.h"
#include "t1061MD.h"
#include "t955MD.h"
#include "t1014MD.h"
extern MethodInfo m4025_MI;
extern MethodInfo m4722_MI;
extern MethodInfo m4742_MI;
extern MethodInfo m4882_MI;
extern MethodInfo m4729_MI;
extern MethodInfo m5006_MI;
extern MethodInfo m5131_MI;
extern MethodInfo m5132_MI;
extern MethodInfo m5133_MI;
extern MethodInfo m5021_MI;
extern MethodInfo m4600_MI;
extern MethodInfo m5156_MI;
extern MethodInfo m4760_MI;
extern MethodInfo m4642_MI;
extern MethodInfo m4965_MI;
extern FieldInfo t1088_f14_FieldInfo;


extern MethodInfo m5062_MI;
 void m5062 (t1075 * __this, t1020 * p0, t781* p1, MethodInfo* method){
	{
		m5014(__this, p0, ((int32_t)20), p1, &m5014_MI);
		return;
	}
}
extern MethodInfo m5063_MI;
 void m5063 (t29 * __this, MethodInfo* method){
	{
		t781* L_0 = ((t781*)SZArrayNew(InitializedTypeInfo(&t781_TI), 4));
		m4025(NULL, (t20 *)(t20 *)L_0, LoadFieldToken(&t1088_f14_FieldInfo), &m4025_MI);
		((t1075_SFs*)InitializedTypeInfo(&t1075_TI)->static_fields)->f9 = L_0;
		return;
	}
}
extern MethodInfo m5064_MI;
 void m5064 (t1075 * __this, MethodInfo* method){
	{
		m5019(__this, &m5019_MI);
		t1020 * L_0 = m5015(__this, &m5015_MI);
		m4722(L_0, 2, &m4722_MI);
		return;
	}
}
extern MethodInfo m5065_MI;
 void m5065 (t1075 * __this, MethodInfo* method){
	t955 * V_0 = {0};
	t781* V_1 = {0};
	t781* V_2 = {0};
	t781* V_3 = {0};
	{
		t1020 * L_0 = m5015(__this, &m5015_MI);
		t781* L_1 = m4742(L_0, &m4742_MI);
		t1061 * L_2 = (t1061 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1061_TI));
		m4882(L_2, L_1, &m4882_MI);
		V_0 = L_2;
		t1020 * L_3 = m5015(__this, &m5015_MI);
		t1036 * L_4 = m4729(L_3, &m4729_MI);
		t781* L_5 = m5006(L_4, &m5006_MI);
		V_1 = L_5;
		VirtFuncInvoker5< int32_t, t781*, int32_t, int32_t, t781*, int32_t >::Invoke(&m5131_MI, V_0, V_1, 0, (((int32_t)(((t20 *)V_1)->max_length))), V_1, 0);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1075_TI));
		VirtFuncInvoker5< int32_t, t781*, int32_t, int32_t, t781*, int32_t >::Invoke(&m5131_MI, V_0, (((t1075_SFs*)InitializedTypeInfo(&t1075_TI)->static_fields)->f9), 0, (((int32_t)(((t20 *)(((t1075_SFs*)InitializedTypeInfo(&t1075_TI)->static_fields)->f9))->max_length))), (((t1075_SFs*)InitializedTypeInfo(&t1075_TI)->static_fields)->f9), 0);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1019_TI));
		VirtFuncInvoker3< t781*, t781*, int32_t, int32_t >::Invoke(&m5132_MI, V_0, (((t1019_SFs*)InitializedTypeInfo(&t1019_TI)->static_fields)->f0), 0, 0);
		int64_t L_6 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(&m4994_MI, __this);
		t781* L_7 = m4999(__this, (((int32_t)L_6)), &m4999_MI);
		V_2 = L_7;
		t781* L_8 = (t781*)VirtFuncInvoker0< t781* >::Invoke(&m5133_MI, V_0);
		V_3 = L_8;
		bool L_9 = m5021(NULL, V_3, V_2, &m5021_MI);
		if (L_9)
		{
			goto IL_0086;
		}
	}
	{
		t1065 * L_10 = (t1065 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1065_TI));
		m4970(L_10, ((int32_t)71), (t7*) &_stringLiteral858, &m4970_MI);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0086:
	{
		return;
	}
}
extern MethodInfo m5066_MI;
 void m5066 (t1075 * __this, MethodInfo* method){
	t781* V_0 = {0};
	t955 * V_1 = {0};
	t781* V_2 = {0};
	t781* V_3 = {0};
	t781* V_4 = {0};
	{
		int64_t L_0 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(&m4994_MI, __this);
		t781* L_1 = m4999(__this, (((int32_t)L_0)), &m4999_MI);
		V_0 = L_1;
		t1014 * L_2 = (t1014 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1014_TI));
		m4600(L_2, &m4600_MI);
		V_1 = L_2;
		t1020 * L_3 = m5015(__this, &m5015_MI);
		t1036 * L_4 = m4729(L_3, &m4729_MI);
		t781* L_5 = m5006(L_4, &m5006_MI);
		V_2 = L_5;
		t781* L_6 = m5156(V_1, V_2, 0, (((int32_t)(((t20 *)V_2)->max_length))), &m5156_MI);
		V_3 = L_6;
		t1020 * L_7 = m5015(__this, &m5015_MI);
		t1042 * L_8 = m4760(L_7, &m4760_MI);
		t1019 * L_9 = m4831(L_8, &m4831_MI);
		t1020 * L_10 = m5015(__this, &m5015_MI);
		t781* L_11 = m4742(L_10, &m4742_MI);
		t781* L_12 = m4642(L_9, L_11, (t7*) &_stringLiteral859, V_3, ((int32_t)12), &m4642_MI);
		V_4 = L_12;
		bool L_13 = m5021(NULL, V_4, V_0, &m5021_MI);
		if (L_13)
		{
			goto IL_0073;
		}
	}
	{
		t1065 * L_14 = (t1065 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1065_TI));
		m4965(L_14, (t7*) &_stringLiteral858, &m4965_MI);
		il2cpp_codegen_raise_exception(L_14);
	}

IL_0073:
	{
		return;
	}
}
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished
extern Il2CppType t781_0_0_17;
FieldInfo t1075_f9_FieldInfo = 
{
	"Ssl3Marker", &t781_0_0_17, &t1075_TI, offsetof(t1075_SFs, f9), &EmptyCustomAttributesCache};
static FieldInfo* t1075_FIs[] =
{
	&t1075_f9_FieldInfo,
	NULL
};
extern Il2CppType t1020_0_0_0;
extern Il2CppType t781_0_0_0;
static ParameterInfo t1075_m5062_ParameterInfos[] = 
{
	{"context", 0, 134218541, &EmptyCustomAttributesCache, &t1020_0_0_0},
	{"buffer", 1, 134218542, &EmptyCustomAttributesCache, &t781_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m5062_MI = 
{
	".ctor", (methodPointerType)&m5062, &t1075_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t1075_m5062_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 847, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m5063_MI = 
{
	".cctor", (methodPointerType)&m5063, &t1075_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 848, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m5064_MI = 
{
	"Update", (methodPointerType)&m5064, &t1075_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 198, 0, 26, 0, false, false, 849, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m5065_MI = 
{
	"ProcessAsSsl3", (methodPointerType)&m5065, &t1075_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 25, 0, false, false, 850, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m5066_MI = 
{
	"ProcessAsTls1", (methodPointerType)&m5066, &t1075_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 24, 0, false, false, 851, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1075_MIs[] =
{
	&m5062_MI,
	&m5063_MI,
	&m5064_MI,
	&m5065_MI,
	&m5066_MI,
	NULL
};
static MethodInfo* t1075_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m5225_MI,
	&m4990_MI,
	&m4991_MI,
	&m4989_MI,
	&m4994_MI,
	&m4992_MI,
	&m4993_MI,
	&m5269_MI,
	&m5184_MI,
	&m5007_MI,
	&m5010_MI,
	&m5226_MI,
	&m5009_MI,
	&m5008_MI,
	&m5011_MI,
	&m5215_MI,
	&m5232_MI,
	&m5242_MI,
	&m5234_MI,
	&m5243_MI,
	&m5066_MI,
	&m5065_MI,
	&m5064_MI,
	&m5020_MI,
};
static Il2CppInterfaceOffsetPair t1075_IOs[] = 
{
	{ &t324_TI, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType t1075_0_0_0;
extern Il2CppType t1075_1_0_0;
struct t1075;
TypeInfo t1075_TI = 
{
	&g_Mono_Security_dll_Image, NULL, "TlsServerFinished", "Mono.Security.Protocol.Tls.Handshake.Client", t1075_MIs, NULL, t1075_FIs, NULL, &t1035_TI, NULL, NULL, &t1075_TI, NULL, t1075_VT, &EmptyCustomAttributesCache, &t1075_TI, &t1075_0_0_0, &t1075_1_0_0, t1075_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1075), 0, -1, sizeof(t1075_SFs), 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, true, false, false, 5, 0, 1, 0, 0, 28, 0, 1};
#include "t1076.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1076_TI;
#include "t1076MD.h"

#include "t1043.h"
#include "t1025.h"
#include "t1026.h"
extern TypeInfo t1039_TI;
extern TypeInfo t1025_TI;
#include "t938MD.h"
#include "t1039MD.h"
#include "t1025MD.h"
#include "t1029MD.h"
extern MethodInfo m4714_MI;
extern MethodInfo m4737_MI;
extern MethodInfo m4832_MI;
extern MethodInfo m4716_MI;
extern MethodInfo m4708_MI;
extern MethodInfo m4734_MI;
extern MethodInfo m4736_MI;
extern MethodInfo m4055_MI;
extern MethodInfo m4739_MI;
extern MethodInfo m4741_MI;
extern MethodInfo m5070_MI;
extern MethodInfo m5071_MI;
extern MethodInfo m4699_MI;
extern MethodInfo m4713_MI;
extern MethodInfo m4706_MI;
extern MethodInfo m4727_MI;
extern MethodInfo m4668_MI;
extern MethodInfo m4661_MI;
extern MethodInfo m4758_MI;
extern MethodInfo m4711_MI;
extern MethodInfo m4710_MI;
extern MethodInfo m4666_MI;
extern MethodInfo m4728_MI;
extern MethodInfo m4673_MI;


extern MethodInfo m5067_MI;
 void m5067 (t1076 * __this, t1020 * p0, t781* p1, MethodInfo* method){
	{
		m5014(__this, p0, 2, p1, &m5014_MI);
		return;
	}
}
extern MethodInfo m5068_MI;
 void m5068 (t1076 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	t781* V_3 = {0};
	t781* V_4 = {0};
	{
		m5019(__this, &m5019_MI);
		t1020 * L_0 = m5015(__this, &m5015_MI);
		t781* L_1 = (__this->f11);
		m4714(L_0, L_1, &m4714_MI);
		t1020 * L_2 = m5015(__this, &m5015_MI);
		t781* L_3 = (__this->f10);
		m4737(L_2, L_3, &m4737_MI);
		t1020 * L_4 = m5015(__this, &m5015_MI);
		t1042 * L_5 = m4761(L_4, &m4761_MI);
		t1019 * L_6 = (__this->f12);
		m4832(L_5, L_6, &m4832_MI);
		t1020 * L_7 = m5015(__this, &m5015_MI);
		int32_t L_8 = (__this->f9);
		m4716(L_7, L_8, &m4716_MI);
		t1020 * L_9 = m5015(__this, &m5015_MI);
		m4708(L_9, 1, &m4708_MI);
		t1020 * L_10 = m5015(__this, &m5015_MI);
		t781* L_11 = m4734(L_10, &m4734_MI);
		V_0 = (((int32_t)(((t20 *)L_11)->max_length)));
		t1020 * L_12 = m5015(__this, &m5015_MI);
		t781* L_13 = m4736(L_12, &m4736_MI);
		V_1 = (((int32_t)(((t20 *)L_13)->max_length)));
		V_2 = ((int32_t)(V_0+V_1));
		V_3 = ((t781*)SZArrayNew(InitializedTypeInfo(&t781_TI), V_2));
		t1020 * L_14 = m5015(__this, &m5015_MI);
		t781* L_15 = m4734(L_14, &m4734_MI);
		m4055(NULL, (t20 *)(t20 *)L_15, 0, (t20 *)(t20 *)V_3, 0, V_0, &m4055_MI);
		t1020 * L_16 = m5015(__this, &m5015_MI);
		t781* L_17 = m4736(L_16, &m4736_MI);
		m4055(NULL, (t20 *)(t20 *)L_17, 0, (t20 *)(t20 *)V_3, V_0, V_1, &m4055_MI);
		t1020 * L_18 = m5015(__this, &m5015_MI);
		m4739(L_18, V_3, &m4739_MI);
		V_4 = ((t781*)SZArrayNew(InitializedTypeInfo(&t781_TI), V_2));
		t1020 * L_19 = m5015(__this, &m5015_MI);
		t781* L_20 = m4736(L_19, &m4736_MI);
		m4055(NULL, (t20 *)(t20 *)L_20, 0, (t20 *)(t20 *)V_4, 0, V_1, &m4055_MI);
		t1020 * L_21 = m5015(__this, &m5015_MI);
		t781* L_22 = m4734(L_21, &m4734_MI);
		m4055(NULL, (t20 *)(t20 *)L_22, 0, (t20 *)(t20 *)V_4, V_1, V_0, &m4055_MI);
		t1020 * L_23 = m5015(__this, &m5015_MI);
		m4741(L_23, V_4, &m4741_MI);
		return;
	}
}
extern MethodInfo m5069_MI;
 void m5069 (t1076 * __this, MethodInfo* method){
	{
		VirtActionInvoker0::Invoke(&m5070_MI, __this);
		return;
	}
}
 void m5070 (t1076 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int16_t V_1 = 0;
	{
		int16_t L_0 = m4997(__this, &m4997_MI);
		m5071(__this, L_0, &m5071_MI);
		t781* L_1 = m4999(__this, ((int32_t)32), &m4999_MI);
		__this->f10 = L_1;
		uint8_t L_2 = m4996(__this, &m4996_MI);
		V_0 = L_2;
		if ((((int32_t)V_0) <= ((int32_t)0)))
		{
			goto IL_0076;
		}
	}
	{
		t781* L_3 = m4999(__this, V_0, &m4999_MI);
		__this->f11 = L_3;
		t1020 * L_4 = m5015(__this, &m5015_MI);
		t1041 * L_5 = m4718(L_4, &m4718_MI);
		t7* L_6 = m4958(L_5, &m4958_MI);
		t781* L_7 = (__this->f11);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1039_TI));
		m4699(NULL, L_6, L_7, &m4699_MI);
		t1020 * L_8 = m5015(__this, &m5015_MI);
		t781* L_9 = (__this->f11);
		t1020 * L_10 = m5015(__this, &m5015_MI);
		t781* L_11 = m4713(L_10, &m4713_MI);
		bool L_12 = m5021(NULL, L_9, L_11, &m5021_MI);
		m4706(L_8, L_12, &m4706_MI);
		goto IL_0082;
	}

IL_0076:
	{
		t1020 * L_13 = m5015(__this, &m5015_MI);
		m4706(L_13, 0, &m4706_MI);
	}

IL_0082:
	{
		int16_t L_14 = m4997(__this, &m4997_MI);
		V_1 = L_14;
		t1020 * L_15 = m5015(__this, &m5015_MI);
		t1025 * L_16 = m4727(L_15, &m4727_MI);
		int32_t L_17 = m4668(L_16, V_1, &m4668_MI);
		if ((((uint32_t)L_17) != ((uint32_t)(-1))))
		{
			goto IL_00ad;
		}
	}
	{
		t1065 * L_18 = (t1065 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1065_TI));
		m4970(L_18, ((int32_t)71), (t7*) &_stringLiteral860, &m4970_MI);
		il2cpp_codegen_raise_exception(L_18);
	}

IL_00ad:
	{
		t1020 * L_19 = m5015(__this, &m5015_MI);
		t1025 * L_20 = m4727(L_19, &m4727_MI);
		t1019 * L_21 = m4661(L_20, V_1, &m4661_MI);
		__this->f12 = L_21;
		uint8_t L_22 = m4996(__this, &m4996_MI);
		__this->f9 = L_22;
		return;
	}
}
 void m5071 (t1076 * __this, int16_t p0, MethodInfo* method){
	int32_t V_0 = {0};
	{
		t1020 * L_0 = m5015(__this, &m5015_MI);
		int32_t L_1 = m4758(L_0, p0, &m4758_MI);
		V_0 = L_1;
		t1020 * L_2 = m5015(__this, &m5015_MI);
		int32_t L_3 = m4711(L_2, &m4711_MI);
		if ((((int32_t)((int32_t)((int32_t)V_0&(int32_t)L_3))) == ((int32_t)V_0)))
		{
			goto IL_003b;
		}
	}
	{
		t1020 * L_4 = m5015(__this, &m5015_MI);
		int32_t L_5 = m4711(L_4, &m4711_MI);
		if ((((uint32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)-1073741824)))) != ((uint32_t)((int32_t)-1073741824))))
		{
			goto IL_0079;
		}
	}

IL_003b:
	{
		t1020 * L_6 = m5015(__this, &m5015_MI);
		m4710(L_6, V_0, &m4710_MI);
		t1020 * L_7 = m5015(__this, &m5015_MI);
		t1025 * L_8 = m4727(L_7, &m4727_MI);
		VirtActionInvoker0::Invoke(&m4666_MI, L_8);
		t1020 * L_9 = m5015(__this, &m5015_MI);
		m4728(L_9, (t1025 *)NULL, &m4728_MI);
		t1020 * L_10 = m5015(__this, &m5015_MI);
		t1025 * L_11 = m4673(NULL, V_0, &m4673_MI);
		m4728(L_10, L_11, &m4728_MI);
		goto IL_0086;
	}

IL_0079:
	{
		t1065 * L_12 = (t1065 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1065_TI));
		m4970(L_12, ((int32_t)70), (t7*) &_stringLiteral805, &m4970_MI);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_0086:
	{
		return;
	}
}
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello
extern Il2CppType t1043_0_0_1;
FieldInfo t1076_f9_FieldInfo = 
{
	"compressionMethod", &t1043_0_0_1, &t1076_TI, offsetof(t1076, f9), &EmptyCustomAttributesCache};
extern Il2CppType t781_0_0_1;
FieldInfo t1076_f10_FieldInfo = 
{
	"random", &t781_0_0_1, &t1076_TI, offsetof(t1076, f10), &EmptyCustomAttributesCache};
extern Il2CppType t781_0_0_1;
FieldInfo t1076_f11_FieldInfo = 
{
	"sessionId", &t781_0_0_1, &t1076_TI, offsetof(t1076, f11), &EmptyCustomAttributesCache};
extern Il2CppType t1019_0_0_1;
FieldInfo t1076_f12_FieldInfo = 
{
	"cipherSuite", &t1019_0_0_1, &t1076_TI, offsetof(t1076, f12), &EmptyCustomAttributesCache};
static FieldInfo* t1076_FIs[] =
{
	&t1076_f9_FieldInfo,
	&t1076_f10_FieldInfo,
	&t1076_f11_FieldInfo,
	&t1076_f12_FieldInfo,
	NULL
};
extern Il2CppType t1020_0_0_0;
extern Il2CppType t781_0_0_0;
static ParameterInfo t1076_m5067_ParameterInfos[] = 
{
	{"context", 0, 134218543, &EmptyCustomAttributesCache, &t1020_0_0_0},
	{"buffer", 1, 134218544, &EmptyCustomAttributesCache, &t781_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m5067_MI = 
{
	".ctor", (methodPointerType)&m5067, &t1076_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t1076_m5067_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 852, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m5068_MI = 
{
	"Update", (methodPointerType)&m5068, &t1076_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 198, 0, 26, 0, false, false, 853, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m5069_MI = 
{
	"ProcessAsSsl3", (methodPointerType)&m5069, &t1076_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 25, 0, false, false, 854, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m5070_MI = 
{
	"ProcessAsTls1", (methodPointerType)&m5070, &t1076_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 24, 0, false, false, 855, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t372_0_0_0;
extern Il2CppType t372_0_0_0;
static ParameterInfo t1076_m5071_ParameterInfos[] = 
{
	{"protocol", 0, 134218545, &EmptyCustomAttributesCache, &t372_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t372 (MethodInfo* method, void* obj, void** args);
MethodInfo m5071_MI = 
{
	"processProtocol", (methodPointerType)&m5071, &t1076_TI, &t21_0_0_0, RuntimeInvoker_t21_t372, t1076_m5071_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 856, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1076_MIs[] =
{
	&m5067_MI,
	&m5068_MI,
	&m5069_MI,
	&m5070_MI,
	&m5071_MI,
	NULL
};
static MethodInfo* t1076_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m5225_MI,
	&m4990_MI,
	&m4991_MI,
	&m4989_MI,
	&m4994_MI,
	&m4992_MI,
	&m4993_MI,
	&m5269_MI,
	&m5184_MI,
	&m5007_MI,
	&m5010_MI,
	&m5226_MI,
	&m5009_MI,
	&m5008_MI,
	&m5011_MI,
	&m5215_MI,
	&m5232_MI,
	&m5242_MI,
	&m5234_MI,
	&m5243_MI,
	&m5070_MI,
	&m5069_MI,
	&m5068_MI,
	&m5020_MI,
};
static Il2CppInterfaceOffsetPair t1076_IOs[] = 
{
	{ &t324_TI, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType t1076_0_0_0;
extern Il2CppType t1076_1_0_0;
struct t1076;
TypeInfo t1076_TI = 
{
	&g_Mono_Security_dll_Image, NULL, "TlsServerHello", "Mono.Security.Protocol.Tls.Handshake.Client", t1076_MIs, NULL, t1076_FIs, NULL, &t1035_TI, NULL, NULL, &t1076_TI, NULL, t1076_VT, &EmptyCustomAttributesCache, &t1076_TI, &t1076_0_0_0, &t1076_1_0_0, t1076_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1076), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 5, 0, 4, 0, 0, 28, 0, 1};
#include "t1077.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1077_TI;
#include "t1077MD.h"



extern MethodInfo m5072_MI;
 void m5072 (t1077 * __this, t1020 * p0, t781* p1, MethodInfo* method){
	{
		m5014(__this, p0, ((int32_t)14), p1, &m5014_MI);
		return;
	}
}
extern MethodInfo m5073_MI;
 void m5073 (t1077 * __this, MethodInfo* method){
	{
		return;
	}
}
extern MethodInfo m5074_MI;
 void m5074 (t1077 * __this, MethodInfo* method){
	{
		return;
	}
}
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHelloDone
extern Il2CppType t1020_0_0_0;
extern Il2CppType t781_0_0_0;
static ParameterInfo t1077_m5072_ParameterInfos[] = 
{
	{"context", 0, 134218546, &EmptyCustomAttributesCache, &t1020_0_0_0},
	{"buffer", 1, 134218547, &EmptyCustomAttributesCache, &t781_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m5072_MI = 
{
	".ctor", (methodPointerType)&m5072, &t1077_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t1077_m5072_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 857, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m5073_MI = 
{
	"ProcessAsSsl3", (methodPointerType)&m5073, &t1077_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 25, 0, false, false, 858, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m5074_MI = 
{
	"ProcessAsTls1", (methodPointerType)&m5074, &t1077_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 24, 0, false, false, 859, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1077_MIs[] =
{
	&m5072_MI,
	&m5073_MI,
	&m5074_MI,
	NULL
};
static MethodInfo* t1077_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m5225_MI,
	&m4990_MI,
	&m4991_MI,
	&m4989_MI,
	&m4994_MI,
	&m4992_MI,
	&m4993_MI,
	&m5269_MI,
	&m5184_MI,
	&m5007_MI,
	&m5010_MI,
	&m5226_MI,
	&m5009_MI,
	&m5008_MI,
	&m5011_MI,
	&m5215_MI,
	&m5232_MI,
	&m5242_MI,
	&m5234_MI,
	&m5243_MI,
	&m5074_MI,
	&m5073_MI,
	&m5019_MI,
	&m5020_MI,
};
static Il2CppInterfaceOffsetPair t1077_IOs[] = 
{
	{ &t324_TI, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType t1077_0_0_0;
extern Il2CppType t1077_1_0_0;
struct t1077;
TypeInfo t1077_TI = 
{
	&g_Mono_Security_dll_Image, NULL, "TlsServerHelloDone", "Mono.Security.Protocol.Tls.Handshake.Client", t1077_MIs, NULL, NULL, NULL, &t1035_TI, NULL, NULL, &t1077_TI, NULL, t1077_VT, &EmptyCustomAttributesCache, &t1077_TI, &t1077_0_0_0, &t1077_1_0_0, t1077_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1077), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 28, 0, 1};
#include "t1078.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1078_TI;
#include "t1078MD.h"

#include "t934.h"
#include "t783.h"
extern TypeInfo t934_TI;
extern MethodInfo m5079_MI;
extern MethodInfo m4974_MI;
extern MethodInfo m4979_MI;
extern MethodInfo m4980_MI;
extern MethodInfo m5078_MI;
extern MethodInfo m4986_MI;
extern MethodInfo m4738_MI;
extern MethodInfo m5004_MI;
extern MethodInfo m4195_MI;
extern MethodInfo m5005_MI;
extern MethodInfo m4977_MI;
extern MethodInfo m4605_MI;


extern MethodInfo m5075_MI;
 void m5075 (t1078 * __this, t1020 * p0, t781* p1, MethodInfo* method){
	{
		m5014(__this, p0, ((int32_t)12), p1, &m5014_MI);
		m5079(__this, &m5079_MI);
		return;
	}
}
extern MethodInfo m5076_MI;
 void m5076 (t1078 * __this, MethodInfo* method){
	{
		m5019(__this, &m5019_MI);
		t1020 * L_0 = m5015(__this, &m5015_MI);
		t1040 * L_1 = m4717(L_0, &m4717_MI);
		m4974(L_1, 1, &m4974_MI);
		t1020 * L_2 = m5015(__this, &m5015_MI);
		t1040 * L_3 = m4717(L_2, &m4717_MI);
		t934  L_4 = (__this->f9);
		m4979(L_3, L_4, &m4979_MI);
		t1020 * L_5 = m5015(__this, &m5015_MI);
		t1040 * L_6 = m4717(L_5, &m4717_MI);
		t781* L_7 = (__this->f10);
		m4980(L_6, L_7, &m4980_MI);
		return;
	}
}
extern MethodInfo m5077_MI;
 void m5077 (t1078 * __this, MethodInfo* method){
	{
		VirtActionInvoker0::Invoke(&m5078_MI, __this);
		return;
	}
}
 void m5078 (t1078 * __this, MethodInfo* method){
	t934  V_0 = {0};
	{
		Initobj (&t934_TI, (&V_0));
		__this->f9 = V_0;
		t934 * L_0 = &(__this->f9);
		int16_t L_1 = m4997(__this, &m4997_MI);
		t781* L_2 = m4999(__this, L_1, &m4999_MI);
		L_0->f6 = L_2;
		t934 * L_3 = &(__this->f9);
		int16_t L_4 = m4997(__this, &m4997_MI);
		t781* L_5 = m4999(__this, L_4, &m4999_MI);
		L_3->f7 = L_5;
		int16_t L_6 = m4997(__this, &m4997_MI);
		t781* L_7 = m4999(__this, L_6, &m4999_MI);
		__this->f10 = L_7;
		return;
	}
}
 void m5079 (t1078 * __this, MethodInfo* method){
	t1014 * V_0 = {0};
	int32_t V_1 = 0;
	t1036 * V_2 = {0};
	bool V_3 = false;
	{
		t1014 * L_0 = (t1014 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1014_TI));
		m4600(L_0, &m4600_MI);
		V_0 = L_0;
		t934 * L_1 = &(__this->f9);
		t781* L_2 = (L_1->f6);
		t934 * L_3 = &(__this->f9);
		t781* L_4 = (L_3->f7);
		V_1 = ((int32_t)(((int32_t)((((int32_t)(((t20 *)L_2)->max_length)))+(((int32_t)(((t20 *)L_4)->max_length)))))+4));
		t1036 * L_5 = (t1036 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1036_TI));
		m4986(L_5, &m4986_MI);
		V_2 = L_5;
		t1020 * L_6 = m5015(__this, &m5015_MI);
		t781* L_7 = m4738(L_6, &m4738_MI);
		m5004(V_2, L_7, &m5004_MI);
		t781* L_8 = m5006(__this, &m5006_MI);
		VirtActionInvoker3< t781*, int32_t, int32_t >::Invoke(&m5011_MI, V_2, L_8, 0, V_1);
		t781* L_9 = m5006(V_2, &m5006_MI);
		m4195(V_0, L_9, &m4195_MI);
		m5005(V_2, &m5005_MI);
		t1020 * L_10 = m5015(__this, &m5015_MI);
		t1040 * L_11 = m4717(L_10, &m4717_MI);
		t783 * L_12 = m4977(L_11, &m4977_MI);
		t781* L_13 = (__this->f10);
		bool L_14 = m4605(V_0, L_12, L_13, &m4605_MI);
		V_3 = L_14;
		if (V_3)
		{
			goto IL_008c;
		}
	}
	{
		t1065 * L_15 = (t1065 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1065_TI));
		m4970(L_15, ((int32_t)50), (t7*) &_stringLiteral861, &m4970_MI);
		il2cpp_codegen_raise_exception(L_15);
	}

IL_008c:
	{
		return;
	}
}
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange
extern Il2CppType t934_0_0_1;
FieldInfo t1078_f9_FieldInfo = 
{
	"rsaParams", &t934_0_0_1, &t1078_TI, offsetof(t1078, f9), &EmptyCustomAttributesCache};
extern Il2CppType t781_0_0_1;
FieldInfo t1078_f10_FieldInfo = 
{
	"signedParams", &t781_0_0_1, &t1078_TI, offsetof(t1078, f10), &EmptyCustomAttributesCache};
static FieldInfo* t1078_FIs[] =
{
	&t1078_f9_FieldInfo,
	&t1078_f10_FieldInfo,
	NULL
};
extern Il2CppType t1020_0_0_0;
extern Il2CppType t781_0_0_0;
static ParameterInfo t1078_m5075_ParameterInfos[] = 
{
	{"context", 0, 134218548, &EmptyCustomAttributesCache, &t1020_0_0_0},
	{"buffer", 1, 134218549, &EmptyCustomAttributesCache, &t781_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m5075_MI = 
{
	".ctor", (methodPointerType)&m5075, &t1078_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t1078_m5075_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 860, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m5076_MI = 
{
	"Update", (methodPointerType)&m5076, &t1078_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 198, 0, 26, 0, false, false, 861, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m5077_MI = 
{
	"ProcessAsSsl3", (methodPointerType)&m5077, &t1078_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 25, 0, false, false, 862, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m5078_MI = 
{
	"ProcessAsTls1", (methodPointerType)&m5078, &t1078_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 24, 0, false, false, 863, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m5079_MI = 
{
	"verifySignature", (methodPointerType)&m5079, &t1078_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 864, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1078_MIs[] =
{
	&m5075_MI,
	&m5076_MI,
	&m5077_MI,
	&m5078_MI,
	&m5079_MI,
	NULL
};
static MethodInfo* t1078_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m5225_MI,
	&m4990_MI,
	&m4991_MI,
	&m4989_MI,
	&m4994_MI,
	&m4992_MI,
	&m4993_MI,
	&m5269_MI,
	&m5184_MI,
	&m5007_MI,
	&m5010_MI,
	&m5226_MI,
	&m5009_MI,
	&m5008_MI,
	&m5011_MI,
	&m5215_MI,
	&m5232_MI,
	&m5242_MI,
	&m5234_MI,
	&m5243_MI,
	&m5078_MI,
	&m5077_MI,
	&m5076_MI,
	&m5020_MI,
};
static Il2CppInterfaceOffsetPair t1078_IOs[] = 
{
	{ &t324_TI, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType t1078_0_0_0;
extern Il2CppType t1078_1_0_0;
struct t1078;
TypeInfo t1078_TI = 
{
	&g_Mono_Security_dll_Image, NULL, "TlsServerKeyExchange", "Mono.Security.Protocol.Tls.Handshake.Client", t1078_MIs, NULL, t1078_FIs, NULL, &t1035_TI, NULL, NULL, &t1078_TI, NULL, t1078_VT, &EmptyCustomAttributesCache, &t1078_TI, &t1078_0_0_0, &t1078_1_0_0, t1078_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1078), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 5, 0, 2, 0, 0, 28, 0, 1};
#include "t980.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t980_TI;
#include "t980MD.h"

#include "t35.h"
#include "t972.h"
#include "t977.h"
#include "t67.h"


extern MethodInfo m5080_MI;
 void m5080 (t980 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m5081_MI;
 bool m5081 (t980 * __this, t972 * p0, int32_t p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m5081((t980 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (t29 *, t29 * __this, t972 * p0, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (t29 * __this, t972 * p0, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef bool (*FunctionPointerType) (t29 * __this, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
bool pinvoke_delegate_wrapper_t980(Il2CppObject* delegate, t972 * p0, int32_t p1)
{
	typedef int32_t (STDCALL *native_function_ptr_type)(t972 *, int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter 'p0' to native representation
	t972 * _p0_marshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'Mono.Math.BigInteger'."));

	// Marshaling of parameter 'p1' to native representation

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(_p0_marshaled, p1);

	// Marshaling cleanup of parameter 'p0' native representation

	// Marshaling cleanup of parameter 'p1' native representation

	return _return_value;
}
extern MethodInfo m5082_MI;
 t29 * m5082 (t980 * __this, t972 * p0, int32_t p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = Box(InitializedTypeInfo(&t977_TI), &p1);
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m5083_MI;
 bool m5083 (t980 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition Mono.Math.Prime.PrimalityTest
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t980_m5080_ParameterInfos[] = 
{
	{"object", 0, 134218550, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134218551, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
MethodInfo m5080_MI = 
{
	".ctor", (methodPointerType)&m5080, &t980_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t980_m5080_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, false, 865, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t972_0_0_0;
extern Il2CppType t972_0_0_0;
extern Il2CppType t977_0_0_0;
extern Il2CppType t977_0_0_0;
static ParameterInfo t980_m5081_ParameterInfos[] = 
{
	{"bi", 0, 134218552, &EmptyCustomAttributesCache, &t972_0_0_0},
	{"confidence", 1, 134218553, &EmptyCustomAttributesCache, &t977_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m5081_MI = 
{
	"Invoke", (methodPointerType)&m5081, &t980_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t44, t980_m5081_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, false, 866, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t972_0_0_0;
extern Il2CppType t977_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t980_m5082_ParameterInfos[] = 
{
	{"bi", 0, 134218554, &EmptyCustomAttributesCache, &t972_0_0_0},
	{"confidence", 1, 134218555, &EmptyCustomAttributesCache, &t977_0_0_0},
	{"callback", 2, 134218556, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134218557, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m5082_MI = 
{
	"BeginInvoke", (methodPointerType)&m5082, &t980_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t44_t29_t29, t980_m5082_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, false, 867, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t66_0_0_0;
extern Il2CppType t66_0_0_0;
static ParameterInfo t980_m5083_ParameterInfos[] = 
{
	{"result", 0, 134218558, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m5083_MI = 
{
	"EndInvoke", (methodPointerType)&m5083, &t980_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t980_m5083_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, false, 868, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t980_MIs[] =
{
	&m5080_MI,
	&m5081_MI,
	&m5082_MI,
	&m5083_MI,
	NULL
};
extern MethodInfo m1699_MI;
extern MethodInfo m1700_MI;
extern MethodInfo m1701_MI;
extern MethodInfo m1702_MI;
extern MethodInfo m1703_MI;
extern MethodInfo m1704_MI;
extern MethodInfo m1705_MI;
static MethodInfo* t980_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m5081_MI,
	&m5082_MI,
	&m5083_MI,
};
extern TypeInfo t373_TI;
extern TypeInfo t374_TI;
static Il2CppInterfaceOffsetPair t980_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType t980_0_0_0;
extern Il2CppType t980_1_0_0;
extern TypeInfo t195_TI;
struct t980;
TypeInfo t980_TI = 
{
	&g_Mono_Security_dll_Image, NULL, "PrimalityTest", "Mono.Math.Prime", t980_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t980_TI, NULL, t980_VT, &EmptyCustomAttributesCache, &t980_TI, &t980_0_0_0, &t980_1_0_0, t980_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)pinvoke_delegate_wrapper_t980, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t980), 0, sizeof(methodPointerType), 0, 0, -1, 257, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t1058.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1058_TI;
#include "t1058MD.h"



extern MethodInfo m5084_MI;
 void m5084 (t1058 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m5085_MI;
 bool m5085 (t1058 * __this, t745 * p0, t841* p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m5085((t1058 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (t29 *, t29 * __this, t745 * p0, t841* p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (t29 * __this, t745 * p0, t841* p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef bool (*FunctionPointerType) (t29 * __this, t841* p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
bool pinvoke_delegate_wrapper_t1058(Il2CppObject* delegate, t745 * p0, t841* p1)
{
	typedef int32_t (STDCALL *native_function_ptr_type)(t745 *, int32_t*);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter 'p0' to native representation
	t745 * _p0_marshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Security.Cryptography.X509Certificates.X509Certificate'."));

	// Marshaling of parameter 'p1' to native representation
	int32_t* _p1_marshaled = { 0 };
	_p1_marshaled = il2cpp_codegen_marshal_array<int32_t>((Il2CppCodeGenArray*)p1);

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(_p0_marshaled, _p1_marshaled);

	// Marshaling cleanup of parameter 'p0' native representation

	// Marshaling cleanup of parameter 'p1' native representation

	return _return_value;
}
extern MethodInfo m5086_MI;
 t29 * m5086 (t1058 * __this, t745 * p0, t841* p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = p1;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m5087_MI;
 bool m5087 (t1058 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition Mono.Security.Protocol.Tls.CertificateValidationCallback
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t1058_m5084_ParameterInfos[] = 
{
	{"object", 0, 134218559, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134218560, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
MethodInfo m5084_MI = 
{
	".ctor", (methodPointerType)&m5084, &t1058_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t1058_m5084_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, false, 869, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t745_0_0_0;
extern Il2CppType t745_0_0_0;
extern Il2CppType t841_0_0_0;
extern Il2CppType t841_0_0_0;
static ParameterInfo t1058_m5085_ParameterInfos[] = 
{
	{"certificate", 0, 134218561, &EmptyCustomAttributesCache, &t745_0_0_0},
	{"certificateErrors", 1, 134218562, &EmptyCustomAttributesCache, &t841_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m5085_MI = 
{
	"Invoke", (methodPointerType)&m5085, &t1058_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t1058_m5085_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, false, 870, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t745_0_0_0;
extern Il2CppType t841_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1058_m5086_ParameterInfos[] = 
{
	{"certificate", 0, 134218563, &EmptyCustomAttributesCache, &t745_0_0_0},
	{"certificateErrors", 1, 134218564, &EmptyCustomAttributesCache, &t841_0_0_0},
	{"callback", 2, 134218565, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134218566, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m5086_MI = 
{
	"BeginInvoke", (methodPointerType)&m5086, &t1058_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t1058_m5086_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, false, 871, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t66_0_0_0;
static ParameterInfo t1058_m5087_ParameterInfos[] = 
{
	{"result", 0, 134218567, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m5087_MI = 
{
	"EndInvoke", (methodPointerType)&m5087, &t1058_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t1058_m5087_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, false, 872, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1058_MIs[] =
{
	&m5084_MI,
	&m5085_MI,
	&m5086_MI,
	&m5087_MI,
	NULL
};
static MethodInfo* t1058_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m5085_MI,
	&m5086_MI,
	&m5087_MI,
};
static Il2CppInterfaceOffsetPair t1058_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType t1058_0_0_0;
extern Il2CppType t1058_1_0_0;
struct t1058;
TypeInfo t1058_TI = 
{
	&g_Mono_Security_dll_Image, NULL, "CertificateValidationCallback", "Mono.Security.Protocol.Tls", t1058_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t1058_TI, NULL, t1058_VT, &EmptyCustomAttributesCache, &t1058_TI, &t1058_0_0_0, &t1058_1_0_0, t1058_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)pinvoke_delegate_wrapper_t1058, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1058), 0, sizeof(methodPointerType), 0, 0, -1, 257, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t1059.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1059_TI;
#include "t1059MD.h"



extern MethodInfo m5088_MI;
 void m5088 (t1059 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m5089_MI;
 t1057 * m5089 (t1059 * __this, t945 * p0, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m5089((t1059 *)__this->f9,p0, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef t1057 * (*FunctionPointerType) (t29 *, t29 * __this, t945 * p0, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef t1057 * (*FunctionPointerType) (t29 * __this, t945 * p0, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef t1057 * (*FunctionPointerType) (t29 * __this, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0,(MethodInfo*)(__this->f3.f0));
	}
}
t1057 * pinvoke_delegate_wrapper_t1059(Il2CppObject* delegate, t945 * p0)
{
	typedef t1057 * (STDCALL *native_function_ptr_type)(t945 *);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter 'p0' to native representation
	t945 * _p0_marshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'Mono.Security.X509.X509CertificateCollection'."));

	// Native function invocation and marshaling of return value back from native representation
	t1057 * _return_value = _il2cpp_pinvoke_func(_p0_marshaled);
	t1057 * __return_value_unmarshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'Mono.Security.Protocol.Tls.ValidationResult'."));

	// Marshaling cleanup of parameter 'p0' native representation

	return __return_value_unmarshaled;
}
extern MethodInfo m5090_MI;
 t29 * m5090 (t1059 * __this, t945 * p0, t67 * p1, t29 * p2, MethodInfo* method){
	void *__d_args[2] = {0};
	__d_args[0] = p0;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p1, (Il2CppObject*)p2);
}
extern MethodInfo m5091_MI;
 t1057 * m5091 (t1059 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return (t1057 *)__result;
}
// Metadata Definition Mono.Security.Protocol.Tls.CertificateValidationCallback2
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t1059_m5088_ParameterInfos[] = 
{
	{"object", 0, 134218568, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134218569, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
MethodInfo m5088_MI = 
{
	".ctor", (methodPointerType)&m5088, &t1059_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t1059_m5088_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, false, 873, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t945_0_0_0;
static ParameterInfo t1059_m5089_ParameterInfos[] = 
{
	{"collection", 0, 134218570, &EmptyCustomAttributesCache, &t945_0_0_0},
};
extern Il2CppType t1057_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m5089_MI = 
{
	"Invoke", (methodPointerType)&m5089, &t1059_TI, &t1057_0_0_0, RuntimeInvoker_t29_t29, t1059_m5089_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, false, 874, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t945_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1059_m5090_ParameterInfos[] = 
{
	{"collection", 0, 134218571, &EmptyCustomAttributesCache, &t945_0_0_0},
	{"callback", 1, 134218572, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134218573, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m5090_MI = 
{
	"BeginInvoke", (methodPointerType)&m5090, &t1059_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t1059_m5090_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, false, 875, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t66_0_0_0;
static ParameterInfo t1059_m5091_ParameterInfos[] = 
{
	{"result", 0, 134218574, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t1057_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m5091_MI = 
{
	"EndInvoke", (methodPointerType)&m5091, &t1059_TI, &t1057_0_0_0, RuntimeInvoker_t29_t29, t1059_m5091_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, false, 876, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1059_MIs[] =
{
	&m5088_MI,
	&m5089_MI,
	&m5090_MI,
	&m5091_MI,
	NULL
};
static MethodInfo* t1059_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m5089_MI,
	&m5090_MI,
	&m5091_MI,
};
static Il2CppInterfaceOffsetPair t1059_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType t1059_0_0_0;
extern Il2CppType t1059_1_0_0;
struct t1059;
TypeInfo t1059_TI = 
{
	&g_Mono_Security_dll_Image, NULL, "CertificateValidationCallback2", "Mono.Security.Protocol.Tls", t1059_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t1059_TI, NULL, t1059_VT, &EmptyCustomAttributesCache, &t1059_TI, &t1059_0_0_0, &t1059_1_0_0, t1059_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)pinvoke_delegate_wrapper_t1059, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1059), 0, sizeof(methodPointerType), 0, 0, -1, 257, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t1046.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1046_TI;
#include "t1046MD.h"

#include "t762.h"


extern MethodInfo m5092_MI;
 void m5092 (t1046 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m5093_MI;
 t745 * m5093 (t1046 * __this, t762 * p0, t745 * p1, t7* p2, t762 * p3, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m5093((t1046 *)__this->f9,p0, p1, p2, p3, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef t745 * (*FunctionPointerType) (t29 *, t29 * __this, t762 * p0, t745 * p1, t7* p2, t762 * p3, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1, p2, p3,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef t745 * (*FunctionPointerType) (t29 * __this, t762 * p0, t745 * p1, t7* p2, t762 * p3, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1, p2, p3,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef t745 * (*FunctionPointerType) (t29 * __this, t745 * p1, t7* p2, t762 * p3, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0, p1, p2, p3,(MethodInfo*)(__this->f3.f0));
	}
}
t745 * pinvoke_delegate_wrapper_t1046(Il2CppObject* delegate, t762 * p0, t745 * p1, t7* p2, t762 * p3)
{
	typedef t745 * (STDCALL *native_function_ptr_type)(t762 *, t745 *, char*, t762 *);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter 'p0' to native representation
	t762 * _p0_marshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Security.Cryptography.X509Certificates.X509CertificateCollection'."));

	// Marshaling of parameter 'p1' to native representation
	t745 * _p1_marshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Security.Cryptography.X509Certificates.X509Certificate'."));

	// Marshaling of parameter 'p2' to native representation
	char* _p2_marshaled = { 0 };
	_p2_marshaled = il2cpp_codegen_marshal_string(p2);

	// Marshaling of parameter 'p3' to native representation
	t762 * _p3_marshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Security.Cryptography.X509Certificates.X509CertificateCollection'."));

	// Native function invocation and marshaling of return value back from native representation
	t745 * _return_value = _il2cpp_pinvoke_func(_p0_marshaled, _p1_marshaled, _p2_marshaled, _p3_marshaled);
	t745 * __return_value_unmarshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Security.Cryptography.X509Certificates.X509Certificate'."));

	// Marshaling cleanup of parameter 'p0' native representation

	// Marshaling cleanup of parameter 'p1' native representation

	// Marshaling cleanup of parameter 'p2' native representation
	il2cpp_codegen_marshal_free(_p2_marshaled);
	_p2_marshaled = NULL;

	// Marshaling cleanup of parameter 'p3' native representation

	return __return_value_unmarshaled;
}
extern MethodInfo m5094_MI;
 t29 * m5094 (t1046 * __this, t762 * p0, t745 * p1, t7* p2, t762 * p3, t67 * p4, t29 * p5, MethodInfo* method){
	void *__d_args[5] = {0};
	__d_args[0] = p0;
	__d_args[1] = p1;
	__d_args[2] = p2;
	__d_args[3] = p3;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p4, (Il2CppObject*)p5);
}
extern MethodInfo m5095_MI;
 t745 * m5095 (t1046 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return (t745 *)__result;
}
// Metadata Definition Mono.Security.Protocol.Tls.CertificateSelectionCallback
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t1046_m5092_ParameterInfos[] = 
{
	{"object", 0, 134218575, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134218576, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
MethodInfo m5092_MI = 
{
	".ctor", (methodPointerType)&m5092, &t1046_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t1046_m5092_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, false, 877, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t762_0_0_0;
extern Il2CppType t762_0_0_0;
extern Il2CppType t745_0_0_0;
extern Il2CppType t7_0_0_0;
extern Il2CppType t762_0_0_0;
static ParameterInfo t1046_m5093_ParameterInfos[] = 
{
	{"clientCertificates", 0, 134218577, &EmptyCustomAttributesCache, &t762_0_0_0},
	{"serverCertificate", 1, 134218578, &EmptyCustomAttributesCache, &t745_0_0_0},
	{"targetHost", 2, 134218579, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"serverRequestedCertificates", 3, 134218580, &EmptyCustomAttributesCache, &t762_0_0_0},
};
extern Il2CppType t745_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m5093_MI = 
{
	"Invoke", (methodPointerType)&m5093, &t1046_TI, &t745_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t1046_m5093_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 4, false, false, 878, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t762_0_0_0;
extern Il2CppType t745_0_0_0;
extern Il2CppType t7_0_0_0;
extern Il2CppType t762_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1046_m5094_ParameterInfos[] = 
{
	{"clientCertificates", 0, 134218581, &EmptyCustomAttributesCache, &t762_0_0_0},
	{"serverCertificate", 1, 134218582, &EmptyCustomAttributesCache, &t745_0_0_0},
	{"targetHost", 2, 134218583, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"serverRequestedCertificates", 3, 134218584, &EmptyCustomAttributesCache, &t762_0_0_0},
	{"callback", 4, 134218585, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 5, 134218586, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m5094_MI = 
{
	"BeginInvoke", (methodPointerType)&m5094, &t1046_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29_t29_t29, t1046_m5094_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 6, false, false, 879, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t66_0_0_0;
static ParameterInfo t1046_m5095_ParameterInfos[] = 
{
	{"result", 0, 134218587, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t745_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m5095_MI = 
{
	"EndInvoke", (methodPointerType)&m5095, &t1046_TI, &t745_0_0_0, RuntimeInvoker_t29_t29, t1046_m5095_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, false, 880, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1046_MIs[] =
{
	&m5092_MI,
	&m5093_MI,
	&m5094_MI,
	&m5095_MI,
	NULL
};
static MethodInfo* t1046_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m5093_MI,
	&m5094_MI,
	&m5095_MI,
};
static Il2CppInterfaceOffsetPair t1046_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType t1046_0_0_0;
extern Il2CppType t1046_1_0_0;
struct t1046;
TypeInfo t1046_TI = 
{
	&g_Mono_Security_dll_Image, NULL, "CertificateSelectionCallback", "Mono.Security.Protocol.Tls", t1046_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t1046_TI, NULL, t1046_VT, &EmptyCustomAttributesCache, &t1046_TI, &t1046_0_0_0, &t1046_1_0_0, t1046_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)pinvoke_delegate_wrapper_t1046, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1046), 0, sizeof(methodPointerType), 0, 0, -1, 257, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t1047.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1047_TI;
#include "t1047MD.h"

#include "t777.h"


extern MethodInfo m5096_MI;
 void m5096 (t1047 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m5097_MI;
 t777 * m5097 (t1047 * __this, t745 * p0, t7* p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m5097((t1047 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef t777 * (*FunctionPointerType) (t29 *, t29 * __this, t745 * p0, t7* p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef t777 * (*FunctionPointerType) (t29 * __this, t745 * p0, t7* p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef t777 * (*FunctionPointerType) (t29 * __this, t7* p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
t777 * pinvoke_delegate_wrapper_t1047(Il2CppObject* delegate, t745 * p0, t7* p1)
{
	typedef t777 * (STDCALL *native_function_ptr_type)(t745 *, char*);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter 'p0' to native representation
	t745 * _p0_marshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Security.Cryptography.X509Certificates.X509Certificate'."));

	// Marshaling of parameter 'p1' to native representation
	char* _p1_marshaled = { 0 };
	_p1_marshaled = il2cpp_codegen_marshal_string(p1);

	// Native function invocation and marshaling of return value back from native representation
	t777 * _return_value = _il2cpp_pinvoke_func(_p0_marshaled, _p1_marshaled);
	t777 * __return_value_unmarshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Security.Cryptography.AsymmetricAlgorithm'."));

	// Marshaling cleanup of parameter 'p0' native representation

	// Marshaling cleanup of parameter 'p1' native representation
	il2cpp_codegen_marshal_free(_p1_marshaled);
	_p1_marshaled = NULL;

	return __return_value_unmarshaled;
}
extern MethodInfo m5098_MI;
 t29 * m5098 (t1047 * __this, t745 * p0, t7* p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = p1;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m5099_MI;
 t777 * m5099 (t1047 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return (t777 *)__result;
}
// Metadata Definition Mono.Security.Protocol.Tls.PrivateKeySelectionCallback
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t1047_m5096_ParameterInfos[] = 
{
	{"object", 0, 134218588, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134218589, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
MethodInfo m5096_MI = 
{
	".ctor", (methodPointerType)&m5096, &t1047_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t1047_m5096_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, false, 881, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t745_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t1047_m5097_ParameterInfos[] = 
{
	{"certificate", 0, 134218590, &EmptyCustomAttributesCache, &t745_0_0_0},
	{"targetHost", 1, 134218591, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t777_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m5097_MI = 
{
	"Invoke", (methodPointerType)&m5097, &t1047_TI, &t777_0_0_0, RuntimeInvoker_t29_t29_t29, t1047_m5097_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, false, 882, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t745_0_0_0;
extern Il2CppType t7_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1047_m5098_ParameterInfos[] = 
{
	{"certificate", 0, 134218592, &EmptyCustomAttributesCache, &t745_0_0_0},
	{"targetHost", 1, 134218593, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"callback", 2, 134218594, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134218595, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m5098_MI = 
{
	"BeginInvoke", (methodPointerType)&m5098, &t1047_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t1047_m5098_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, false, 883, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t66_0_0_0;
static ParameterInfo t1047_m5099_ParameterInfos[] = 
{
	{"result", 0, 134218596, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t777_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m5099_MI = 
{
	"EndInvoke", (methodPointerType)&m5099, &t1047_TI, &t777_0_0_0, RuntimeInvoker_t29_t29, t1047_m5099_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, false, 884, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1047_MIs[] =
{
	&m5096_MI,
	&m5097_MI,
	&m5098_MI,
	&m5099_MI,
	NULL
};
static MethodInfo* t1047_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m5097_MI,
	&m5098_MI,
	&m5099_MI,
};
static Il2CppInterfaceOffsetPair t1047_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType t1047_0_0_0;
extern Il2CppType t1047_1_0_0;
struct t1047;
TypeInfo t1047_TI = 
{
	&g_Mono_Security_dll_Image, NULL, "PrivateKeySelectionCallback", "Mono.Security.Protocol.Tls", t1047_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t1047_TI, NULL, t1047_VT, &EmptyCustomAttributesCache, &t1047_TI, &t1047_0_0_0, &t1047_1_0_0, t1047_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)pinvoke_delegate_wrapper_t1047, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1047), 0, sizeof(methodPointerType), 0, 0, -1, 257, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t1079.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1079_TI;
#include "t1079MD.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$3132
void t1079_marshal(const t1079& unmarshaled, t1079_marshaled& marshaled)
{
}
void t1079_marshal_back(const t1079_marshaled& marshaled, t1079& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$3132
void t1079_marshal_cleanup(t1079_marshaled& marshaled)
{
}
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$3132
static MethodInfo* t1079_MIs[] =
{
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
static MethodInfo* t1079_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType t1079_0_0_0;
extern Il2CppType t1079_1_0_0;
extern TypeInfo t110_TI;
TypeInfo t1079_TI = 
{
	&g_Mono_Security_dll_Image, NULL, "$ArrayType$3132", "", t1079_MIs, NULL, NULL, NULL, &t110_TI, NULL, &t1088_TI, &t1079_TI, NULL, t1079_VT, &EmptyCustomAttributesCache, &t1079_TI, &t1079_0_0_0, &t1079_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t1079_marshal, (methodPointerType)t1079_marshal_back, (methodPointerType)t1079_marshal_cleanup, sizeof (t1079)+ sizeof (Il2CppObject), 0, sizeof(t1079_marshaled), 0, 0, -1, 275, 0, true, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#include "t1080.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1080_TI;
#include "t1080MD.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$256
void t1080_marshal(const t1080& unmarshaled, t1080_marshaled& marshaled)
{
}
void t1080_marshal_back(const t1080_marshaled& marshaled, t1080& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$256
void t1080_marshal_cleanup(t1080_marshaled& marshaled)
{
}
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$256
static MethodInfo* t1080_MIs[] =
{
	NULL
};
static MethodInfo* t1080_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType t1080_0_0_0;
extern Il2CppType t1080_1_0_0;
TypeInfo t1080_TI = 
{
	&g_Mono_Security_dll_Image, NULL, "$ArrayType$256", "", t1080_MIs, NULL, NULL, NULL, &t110_TI, NULL, &t1088_TI, &t1080_TI, NULL, t1080_VT, &EmptyCustomAttributesCache, &t1080_TI, &t1080_0_0_0, &t1080_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t1080_marshal, (methodPointerType)t1080_marshal_back, (methodPointerType)t1080_marshal_cleanup, sizeof (t1080)+ sizeof (Il2CppObject), 0, sizeof(t1080_marshaled), 0, 0, -1, 275, 0, true, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#include "t1081.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1081_TI;
#include "t1081MD.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$20
void t1081_marshal(const t1081& unmarshaled, t1081_marshaled& marshaled)
{
}
void t1081_marshal_back(const t1081_marshaled& marshaled, t1081& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$20
void t1081_marshal_cleanup(t1081_marshaled& marshaled)
{
}
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$20
static MethodInfo* t1081_MIs[] =
{
	NULL
};
static MethodInfo* t1081_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType t1081_0_0_0;
extern Il2CppType t1081_1_0_0;
TypeInfo t1081_TI = 
{
	&g_Mono_Security_dll_Image, NULL, "$ArrayType$20", "", t1081_MIs, NULL, NULL, NULL, &t110_TI, NULL, &t1088_TI, &t1081_TI, NULL, t1081_VT, &EmptyCustomAttributesCache, &t1081_TI, &t1081_0_0_0, &t1081_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t1081_marshal, (methodPointerType)t1081_marshal_back, (methodPointerType)t1081_marshal_cleanup, sizeof (t1081)+ sizeof (Il2CppObject), 0, sizeof(t1081_marshaled), 0, 0, -1, 275, 0, true, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#include "t1082.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1082_TI;
#include "t1082MD.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$32
void t1082_marshal(const t1082& unmarshaled, t1082_marshaled& marshaled)
{
}
void t1082_marshal_back(const t1082_marshaled& marshaled, t1082& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$32
void t1082_marshal_cleanup(t1082_marshaled& marshaled)
{
}
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$32
static MethodInfo* t1082_MIs[] =
{
	NULL
};
static MethodInfo* t1082_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType t1082_0_0_0;
extern Il2CppType t1082_1_0_0;
TypeInfo t1082_TI = 
{
	&g_Mono_Security_dll_Image, NULL, "$ArrayType$32", "", t1082_MIs, NULL, NULL, NULL, &t110_TI, NULL, &t1088_TI, &t1082_TI, NULL, t1082_VT, &EmptyCustomAttributesCache, &t1082_TI, &t1082_0_0_0, &t1082_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t1082_marshal, (methodPointerType)t1082_marshal_back, (methodPointerType)t1082_marshal_cleanup, sizeof (t1082)+ sizeof (Il2CppObject), 0, sizeof(t1082_marshaled), 0, 0, -1, 275, 0, true, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#include "t1083.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1083_TI;
#include "t1083MD.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$48
void t1083_marshal(const t1083& unmarshaled, t1083_marshaled& marshaled)
{
}
void t1083_marshal_back(const t1083_marshaled& marshaled, t1083& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$48
void t1083_marshal_cleanup(t1083_marshaled& marshaled)
{
}
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$48
static MethodInfo* t1083_MIs[] =
{
	NULL
};
static MethodInfo* t1083_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType t1083_0_0_0;
extern Il2CppType t1083_1_0_0;
TypeInfo t1083_TI = 
{
	&g_Mono_Security_dll_Image, NULL, "$ArrayType$48", "", t1083_MIs, NULL, NULL, NULL, &t110_TI, NULL, &t1088_TI, &t1083_TI, NULL, t1083_VT, &EmptyCustomAttributesCache, &t1083_TI, &t1083_0_0_0, &t1083_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t1083_marshal, (methodPointerType)t1083_marshal_back, (methodPointerType)t1083_marshal_cleanup, sizeof (t1083)+ sizeof (Il2CppObject), 0, sizeof(t1083_marshaled), 0, 0, -1, 275, 0, true, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#include "t1084.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1084_TI;
#include "t1084MD.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$64
void t1084_marshal(const t1084& unmarshaled, t1084_marshaled& marshaled)
{
}
void t1084_marshal_back(const t1084_marshaled& marshaled, t1084& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$64
void t1084_marshal_cleanup(t1084_marshaled& marshaled)
{
}
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$64
static MethodInfo* t1084_MIs[] =
{
	NULL
};
static MethodInfo* t1084_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType t1084_0_0_0;
extern Il2CppType t1084_1_0_0;
TypeInfo t1084_TI = 
{
	&g_Mono_Security_dll_Image, NULL, "$ArrayType$64", "", t1084_MIs, NULL, NULL, NULL, &t110_TI, NULL, &t1088_TI, &t1084_TI, NULL, t1084_VT, &EmptyCustomAttributesCache, &t1084_TI, &t1084_0_0_0, &t1084_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t1084_marshal, (methodPointerType)t1084_marshal_back, (methodPointerType)t1084_marshal_cleanup, sizeof (t1084)+ sizeof (Il2CppObject), 0, sizeof(t1084_marshaled), 0, 0, -1, 275, 0, true, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#include "t1085.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1085_TI;
#include "t1085MD.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$12
void t1085_marshal(const t1085& unmarshaled, t1085_marshaled& marshaled)
{
}
void t1085_marshal_back(const t1085_marshaled& marshaled, t1085& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$12
void t1085_marshal_cleanup(t1085_marshaled& marshaled)
{
}
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$12
static MethodInfo* t1085_MIs[] =
{
	NULL
};
static MethodInfo* t1085_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType t1085_0_0_0;
extern Il2CppType t1085_1_0_0;
TypeInfo t1085_TI = 
{
	&g_Mono_Security_dll_Image, NULL, "$ArrayType$12", "", t1085_MIs, NULL, NULL, NULL, &t110_TI, NULL, &t1088_TI, &t1085_TI, NULL, t1085_VT, &EmptyCustomAttributesCache, &t1085_TI, &t1085_0_0_0, &t1085_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t1085_marshal, (methodPointerType)t1085_marshal_back, (methodPointerType)t1085_marshal_cleanup, sizeof (t1085)+ sizeof (Il2CppObject), 0, sizeof(t1085_marshaled), 0, 0, -1, 275, 0, true, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#include "t1086.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1086_TI;
#include "t1086MD.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$16
void t1086_marshal(const t1086& unmarshaled, t1086_marshaled& marshaled)
{
}
void t1086_marshal_back(const t1086_marshaled& marshaled, t1086& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$16
void t1086_marshal_cleanup(t1086_marshaled& marshaled)
{
}
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$16
static MethodInfo* t1086_MIs[] =
{
	NULL
};
static MethodInfo* t1086_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType t1086_0_0_0;
extern Il2CppType t1086_1_0_0;
TypeInfo t1086_TI = 
{
	&g_Mono_Security_dll_Image, NULL, "$ArrayType$16", "", t1086_MIs, NULL, NULL, NULL, &t110_TI, NULL, &t1088_TI, &t1086_TI, NULL, t1086_VT, &EmptyCustomAttributesCache, &t1086_TI, &t1086_0_0_0, &t1086_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t1086_marshal, (methodPointerType)t1086_marshal_back, (methodPointerType)t1086_marshal_cleanup, sizeof (t1086)+ sizeof (Il2CppObject), 0, sizeof(t1086_marshaled), 0, 0, -1, 275, 0, true, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1087_TI;
#include "t1087MD.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$4
void t1087_marshal(const t1087& unmarshaled, t1087_marshaled& marshaled)
{
}
void t1087_marshal_back(const t1087_marshaled& marshaled, t1087& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$4
void t1087_marshal_cleanup(t1087_marshaled& marshaled)
{
}
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$4
static MethodInfo* t1087_MIs[] =
{
	NULL
};
static MethodInfo* t1087_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType t1087_0_0_0;
extern Il2CppType t1087_1_0_0;
TypeInfo t1087_TI = 
{
	&g_Mono_Security_dll_Image, NULL, "$ArrayType$4", "", t1087_MIs, NULL, NULL, NULL, &t110_TI, NULL, &t1088_TI, &t1087_TI, NULL, t1087_VT, &EmptyCustomAttributesCache, &t1087_TI, &t1087_0_0_0, &t1087_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t1087_marshal, (methodPointerType)t1087_marshal_back, (methodPointerType)t1087_marshal_cleanup, sizeof (t1087)+ sizeof (Il2CppObject), 0, sizeof(t1087_marshaled), 0, 0, -1, 275, 0, true, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition <PrivateImplementationDetails>
extern Il2CppType t1079_0_0_275;
FieldInfo t1088_f0_FieldInfo = 
{
	"$$field-0", &t1079_0_0_275, &t1088_TI, offsetof(t1088_SFs, f0), &EmptyCustomAttributesCache};
extern Il2CppType t1080_0_0_275;
FieldInfo t1088_f1_FieldInfo = 
{
	"$$field-5", &t1080_0_0_275, &t1088_TI, offsetof(t1088_SFs, f1), &EmptyCustomAttributesCache};
extern Il2CppType t1081_0_0_275;
FieldInfo t1088_f2_FieldInfo = 
{
	"$$field-6", &t1081_0_0_275, &t1088_TI, offsetof(t1088_SFs, f2), &EmptyCustomAttributesCache};
extern Il2CppType t1082_0_0_275;
FieldInfo t1088_f3_FieldInfo = 
{
	"$$field-7", &t1082_0_0_275, &t1088_TI, offsetof(t1088_SFs, f3), &EmptyCustomAttributesCache};
extern Il2CppType t1083_0_0_275;
FieldInfo t1088_f4_FieldInfo = 
{
	"$$field-8", &t1083_0_0_275, &t1088_TI, offsetof(t1088_SFs, f4), &EmptyCustomAttributesCache};
extern Il2CppType t1084_0_0_275;
FieldInfo t1088_f5_FieldInfo = 
{
	"$$field-9", &t1084_0_0_275, &t1088_TI, offsetof(t1088_SFs, f5), &EmptyCustomAttributesCache};
extern Il2CppType t1084_0_0_275;
FieldInfo t1088_f6_FieldInfo = 
{
	"$$field-11", &t1084_0_0_275, &t1088_TI, offsetof(t1088_SFs, f6), &EmptyCustomAttributesCache};
extern Il2CppType t1084_0_0_275;
FieldInfo t1088_f7_FieldInfo = 
{
	"$$field-12", &t1084_0_0_275, &t1088_TI, offsetof(t1088_SFs, f7), &EmptyCustomAttributesCache};
extern Il2CppType t1084_0_0_275;
FieldInfo t1088_f8_FieldInfo = 
{
	"$$field-13", &t1084_0_0_275, &t1088_TI, offsetof(t1088_SFs, f8), &EmptyCustomAttributesCache};
extern Il2CppType t1085_0_0_275;
FieldInfo t1088_f9_FieldInfo = 
{
	"$$field-14", &t1085_0_0_275, &t1088_TI, offsetof(t1088_SFs, f9), &EmptyCustomAttributesCache};
extern Il2CppType t1085_0_0_275;
FieldInfo t1088_f10_FieldInfo = 
{
	"$$field-15", &t1085_0_0_275, &t1088_TI, offsetof(t1088_SFs, f10), &EmptyCustomAttributesCache};
extern Il2CppType t1085_0_0_275;
FieldInfo t1088_f11_FieldInfo = 
{
	"$$field-16", &t1085_0_0_275, &t1088_TI, offsetof(t1088_SFs, f11), &EmptyCustomAttributesCache};
extern Il2CppType t1086_0_0_275;
FieldInfo t1088_f12_FieldInfo = 
{
	"$$field-17", &t1086_0_0_275, &t1088_TI, offsetof(t1088_SFs, f12), &EmptyCustomAttributesCache};
extern Il2CppType t1087_0_0_275;
FieldInfo t1088_f13_FieldInfo = 
{
	"$$field-21", &t1087_0_0_275, &t1088_TI, offsetof(t1088_SFs, f13), &EmptyCustomAttributesCache};
extern Il2CppType t1087_0_0_275;
FieldInfo t1088_f14_FieldInfo = 
{
	"$$field-22", &t1087_0_0_275, &t1088_TI, offsetof(t1088_SFs, f14), &EmptyCustomAttributesCache};
static FieldInfo* t1088_FIs[] =
{
	&t1088_f0_FieldInfo,
	&t1088_f1_FieldInfo,
	&t1088_f2_FieldInfo,
	&t1088_f3_FieldInfo,
	&t1088_f4_FieldInfo,
	&t1088_f5_FieldInfo,
	&t1088_f6_FieldInfo,
	&t1088_f7_FieldInfo,
	&t1088_f8_FieldInfo,
	&t1088_f9_FieldInfo,
	&t1088_f10_FieldInfo,
	&t1088_f11_FieldInfo,
	&t1088_f12_FieldInfo,
	&t1088_f13_FieldInfo,
	&t1088_f14_FieldInfo,
	NULL
};
static const uint8_t t1088_f0_DefaultValueData[] = { 0x2, 0x0, 0x0, 0x0, 0x3, 0x0, 0x0, 0x0, 0x5, 0x0, 0x0, 0x0, 0x7, 0x0, 0x0, 0x0, 0xB, 0x0, 0x0, 0x0, 0xD, 0x0, 0x0, 0x0, 0x11, 0x0, 0x0, 0x0, 0x13, 0x0, 0x0, 0x0, 0x17, 0x0, 0x0, 0x0, 0x1D, 0x0, 0x0, 0x0, 0x1F, 0x0, 0x0, 0x0, 0x25, 0x0, 0x0, 0x0, 0x29, 0x0, 0x0, 0x0, 0x2B, 0x0, 0x0, 0x0, 0x2F, 0x0, 0x0, 0x0, 0x35, 0x0, 0x0, 0x0, 0x3B, 0x0, 0x0, 0x0, 0x3D, 0x0, 0x0, 0x0, 0x43, 0x0, 0x0, 0x0, 0x47, 0x0, 0x0, 0x0, 0x49, 0x0, 0x0, 0x0, 0x4F, 0x0, 0x0, 0x0, 0x53, 0x0, 0x0, 0x0, 0x59, 0x0, 0x0, 0x0, 0x61, 0x0, 0x0, 0x0, 0x65, 0x0, 0x0, 0x0, 0x67, 0x0, 0x0, 0x0, 0x6B, 0x0, 0x0, 0x0, 0x6D, 0x0, 0x0, 0x0, 0x71, 0x0, 0x0, 0x0, 0x7F, 0x0, 0x0, 0x0, 0x83, 0x0, 0x0, 0x0, 0x89, 0x0, 0x0, 0x0, 0x8B, 0x0, 0x0, 0x0, 0x95, 0x0, 0x0, 0x0, 0x97, 0x0, 0x0, 0x0, 0x9D, 0x0, 0x0, 0x0, 0xA3, 0x0, 0x0, 0x0, 0xA7, 0x0, 0x0, 0x0, 0xAD, 0x0, 0x0, 0x0, 0xB3, 0x0, 0x0, 0x0, 0xB5, 0x0, 0x0, 0x0, 0xBF, 0x0, 0x0, 0x0, 0xC1, 0x0, 0x0, 0x0, 0xC5, 0x0, 0x0, 0x0, 0xC7, 0x0, 0x0, 0x0, 0xD3, 0x0, 0x0, 0x0, 0xDF, 0x0, 0x0, 0x0, 0xE3, 0x0, 0x0, 0x0, 0xE5, 0x0, 0x0, 0x0, 0xE9, 0x0, 0x0, 0x0, 0xEF, 0x0, 0x0, 0x0, 0xF1, 0x0, 0x0, 0x0, 0xFB, 0x0, 0x0, 0x0, 0x1, 0x1, 0x0, 0x0, 0x7, 0x1, 0x0, 0x0, 0xD, 0x1, 0x0, 0x0, 0xF, 0x1, 0x0, 0x0, 0x15, 0x1, 0x0, 0x0, 0x19, 0x1, 0x0, 0x0, 0x1B, 0x1, 0x0, 0x0, 0x25, 0x1, 0x0, 0x0, 0x33, 0x1, 0x0, 0x0, 0x37, 0x1, 0x0, 0x0, 0x39, 0x1, 0x0, 0x0, 0x3D, 0x1, 0x0, 0x0, 0x4B, 0x1, 0x0, 0x0, 0x51, 0x1, 0x0, 0x0, 0x5B, 0x1, 0x0, 0x0, 0x5D, 0x1, 0x0, 0x0, 0x61, 0x1, 0x0, 0x0, 0x67, 0x1, 0x0, 0x0, 0x6F, 0x1, 0x0, 0x0, 0x75, 0x1, 0x0, 0x0, 0x7B, 0x1, 0x0, 0x0, 0x7F, 0x1, 0x0, 0x0, 0x85, 0x1, 0x0, 0x0, 0x8D, 0x1, 0x0, 0x0, 0x91, 0x1, 0x0, 0x0, 0x99, 0x1, 0x0, 0x0, 0xA3, 0x1, 0x0, 0x0, 0xA5, 0x1, 0x0, 0x0, 0xAF, 0x1, 0x0, 0x0, 0xB1, 0x1, 0x0, 0x0, 0xB7, 0x1, 0x0, 0x0, 0xBB, 0x1, 0x0, 0x0, 0xC1, 0x1, 0x0, 0x0, 0xC9, 0x1, 0x0, 0x0, 0xCD, 0x1, 0x0, 0x0, 0xCF, 0x1, 0x0, 0x0, 0xD3, 0x1, 0x0, 0x0, 0xDF, 0x1, 0x0, 0x0, 0xE7, 0x1, 0x0, 0x0, 0xEB, 0x1, 0x0, 0x0, 0xF3, 0x1, 0x0, 0x0, 0xF7, 0x1, 0x0, 0x0, 0xFD, 0x1, 0x0, 0x0, 0x9, 0x2, 0x0, 0x0, 0xB, 0x2, 0x0, 0x0, 0x1D, 0x2, 0x0, 0x0, 0x23, 0x2, 0x0, 0x0, 0x2D, 0x2, 0x0, 0x0, 0x33, 0x2, 0x0, 0x0, 0x39, 0x2, 0x0, 0x0, 0x3B, 0x2, 0x0, 0x0, 0x41, 0x2, 0x0, 0x0, 0x4B, 0x2, 0x0, 0x0, 0x51, 0x2, 0x0, 0x0, 0x57, 0x2, 0x0, 0x0, 0x59, 0x2, 0x0, 0x0, 0x5F, 0x2, 0x0, 0x0, 0x65, 0x2, 0x0, 0x0, 0x69, 0x2, 0x0, 0x0, 0x6B, 0x2, 0x0, 0x0, 0x77, 0x2, 0x0, 0x0, 0x81, 0x2, 0x0, 0x0, 0x83, 0x2, 0x0, 0x0, 0x87, 0x2, 0x0, 0x0, 0x8D, 0x2, 0x0, 0x0, 0x93, 0x2, 0x0, 0x0, 0x95, 0x2, 0x0, 0x0, 0xA1, 0x2, 0x0, 0x0, 0xA5, 0x2, 0x0, 0x0, 0xAB, 0x2, 0x0, 0x0, 0xB3, 0x2, 0x0, 0x0, 0xBD, 0x2, 0x0, 0x0, 0xC5, 0x2, 0x0, 0x0, 0xCF, 0x2, 0x0, 0x0, 0xD7, 0x2, 0x0, 0x0, 0xDD, 0x2, 0x0, 0x0, 0xE3, 0x2, 0x0, 0x0, 0xE7, 0x2, 0x0, 0x0, 0xEF, 0x2, 0x0, 0x0, 0xF5, 0x2, 0x0, 0x0, 0xF9, 0x2, 0x0, 0x0, 0x1, 0x3, 0x0, 0x0, 0x5, 0x3, 0x0, 0x0, 0x13, 0x3, 0x0, 0x0, 0x1D, 0x3, 0x0, 0x0, 0x29, 0x3, 0x0, 0x0, 0x2B, 0x3, 0x0, 0x0, 0x35, 0x3, 0x0, 0x0, 0x37, 0x3, 0x0, 0x0, 0x3B, 0x3, 0x0, 0x0, 0x3D, 0x3, 0x0, 0x0, 0x47, 0x3, 0x0, 0x0, 0x55, 0x3, 0x0, 0x0, 0x59, 0x3, 0x0, 0x0, 0x5B, 0x3, 0x0, 0x0, 0x5F, 0x3, 0x0, 0x0, 0x6D, 0x3, 0x0, 0x0, 0x71, 0x3, 0x0, 0x0, 0x73, 0x3, 0x0, 0x0, 0x77, 0x3, 0x0, 0x0, 0x8B, 0x3, 0x0, 0x0, 0x8F, 0x3, 0x0, 0x0, 0x97, 0x3, 0x0, 0x0, 0xA1, 0x3, 0x0, 0x0, 0xA9, 0x3, 0x0, 0x0, 0xAD, 0x3, 0x0, 0x0, 0xB3, 0x3, 0x0, 0x0, 0xB9, 0x3, 0x0, 0x0, 0xC7, 0x3, 0x0, 0x0, 0xCB, 0x3, 0x0, 0x0, 0xD1, 0x3, 0x0, 0x0, 0xD7, 0x3, 0x0, 0x0, 0xDF, 0x3, 0x0, 0x0, 0xE5, 0x3, 0x0, 0x0, 0xF1, 0x3, 0x0, 0x0, 0xF5, 0x3, 0x0, 0x0, 0xFB, 0x3, 0x0, 0x0, 0xFD, 0x3, 0x0, 0x0, 0x7, 0x4, 0x0, 0x0, 0x9, 0x4, 0x0, 0x0, 0xF, 0x4, 0x0, 0x0, 0x19, 0x4, 0x0, 0x0, 0x1B, 0x4, 0x0, 0x0, 0x25, 0x4, 0x0, 0x0, 0x27, 0x4, 0x0, 0x0, 0x2D, 0x4, 0x0, 0x0, 0x3F, 0x4, 0x0, 0x0, 0x43, 0x4, 0x0, 0x0, 0x45, 0x4, 0x0, 0x0, 0x49, 0x4, 0x0, 0x0, 0x4F, 0x4, 0x0, 0x0, 0x55, 0x4, 0x0, 0x0, 0x5D, 0x4, 0x0, 0x0, 0x63, 0x4, 0x0, 0x0, 0x69, 0x4, 0x0, 0x0, 0x7F, 0x4, 0x0, 0x0, 0x81, 0x4, 0x0, 0x0, 0x8B, 0x4, 0x0, 0x0, 0x93, 0x4, 0x0, 0x0, 0x9D, 0x4, 0x0, 0x0, 0xA3, 0x4, 0x0, 0x0, 0xA9, 0x4, 0x0, 0x0, 0xB1, 0x4, 0x0, 0x0, 0xBD, 0x4, 0x0, 0x0, 0xC1, 0x4, 0x0, 0x0, 0xC7, 0x4, 0x0, 0x0, 0xCD, 0x4, 0x0, 0x0, 0xCF, 0x4, 0x0, 0x0, 0xD5, 0x4, 0x0, 0x0, 0xE1, 0x4, 0x0, 0x0, 0xEB, 0x4, 0x0, 0x0, 0xFD, 0x4, 0x0, 0x0, 0xFF, 0x4, 0x0, 0x0, 0x3, 0x5, 0x0, 0x0, 0x9, 0x5, 0x0, 0x0, 0xB, 0x5, 0x0, 0x0, 0x11, 0x5, 0x0, 0x0, 0x15, 0x5, 0x0, 0x0, 0x17, 0x5, 0x0, 0x0, 0x1B, 0x5, 0x0, 0x0, 0x27, 0x5, 0x0, 0x0, 0x29, 0x5, 0x0, 0x0, 0x2F, 0x5, 0x0, 0x0, 0x51, 0x5, 0x0, 0x0, 0x57, 0x5, 0x0, 0x0, 0x5D, 0x5, 0x0, 0x0, 0x65, 0x5, 0x0, 0x0, 0x77, 0x5, 0x0, 0x0, 0x81, 0x5, 0x0, 0x0, 0x8F, 0x5, 0x0, 0x0, 0x93, 0x5, 0x0, 0x0, 0x95, 0x5, 0x0, 0x0, 0x99, 0x5, 0x0, 0x0, 0x9F, 0x5, 0x0, 0x0, 0xA7, 0x5, 0x0, 0x0, 0xAB, 0x5, 0x0, 0x0, 0xAD, 0x5, 0x0, 0x0, 0xB3, 0x5, 0x0, 0x0, 0xBF, 0x5, 0x0, 0x0, 0xC9, 0x5, 0x0, 0x0, 0xCB, 0x5, 0x0, 0x0, 0xCF, 0x5, 0x0, 0x0, 0xD1, 0x5, 0x0, 0x0, 0xD5, 0x5, 0x0, 0x0, 0xDB, 0x5, 0x0, 0x0, 0xE7, 0x5, 0x0, 0x0, 0xF3, 0x5, 0x0, 0x0, 0xFB, 0x5, 0x0, 0x0, 0x7, 0x6, 0x0, 0x0, 0xD, 0x6, 0x0, 0x0, 0x11, 0x6, 0x0, 0x0, 0x17, 0x6, 0x0, 0x0, 0x1F, 0x6, 0x0, 0x0, 0x23, 0x6, 0x0, 0x0, 0x2B, 0x6, 0x0, 0x0, 0x2F, 0x6, 0x0, 0x0, 0x3D, 0x6, 0x0, 0x0, 0x41, 0x6, 0x0, 0x0, 0x47, 0x6, 0x0, 0x0, 0x49, 0x6, 0x0, 0x0, 0x4D, 0x6, 0x0, 0x0, 0x53, 0x6, 0x0, 0x0, 0x55, 0x6, 0x0, 0x0, 0x5B, 0x6, 0x0, 0x0, 0x65, 0x6, 0x0, 0x0, 0x79, 0x6, 0x0, 0x0, 0x7F, 0x6, 0x0, 0x0, 0x83, 0x6, 0x0, 0x0, 0x85, 0x6, 0x0, 0x0, 0x9D, 0x6, 0x0, 0x0, 0xA1, 0x6, 0x0, 0x0, 0xA3, 0x6, 0x0, 0x0, 0xAD, 0x6, 0x0, 0x0, 0xB9, 0x6, 0x0, 0x0, 0xBB, 0x6, 0x0, 0x0, 0xC5, 0x6, 0x0, 0x0, 0xCD, 0x6, 0x0, 0x0, 0xD3, 0x6, 0x0, 0x0, 0xD9, 0x6, 0x0, 0x0, 0xDF, 0x6, 0x0, 0x0, 0xF1, 0x6, 0x0, 0x0, 0xF7, 0x6, 0x0, 0x0, 0xFB, 0x6, 0x0, 0x0, 0xFD, 0x6, 0x0, 0x0, 0x9, 0x7, 0x0, 0x0, 0x13, 0x7, 0x0, 0x0, 0x1F, 0x7, 0x0, 0x0, 0x27, 0x7, 0x0, 0x0, 0x37, 0x7, 0x0, 0x0, 0x45, 0x7, 0x0, 0x0, 0x4B, 0x7, 0x0, 0x0, 0x4F, 0x7, 0x0, 0x0, 0x51, 0x7, 0x0, 0x0, 0x55, 0x7, 0x0, 0x0, 0x57, 0x7, 0x0, 0x0, 0x61, 0x7, 0x0, 0x0, 0x6D, 0x7, 0x0, 0x0, 0x73, 0x7, 0x0, 0x0, 0x79, 0x7, 0x0, 0x0, 0x8B, 0x7, 0x0, 0x0, 0x8D, 0x7, 0x0, 0x0, 0x9D, 0x7, 0x0, 0x0, 0x9F, 0x7, 0x0, 0x0, 0xB5, 0x7, 0x0, 0x0, 0xBB, 0x7, 0x0, 0x0, 0xC3, 0x7, 0x0, 0x0, 0xC9, 0x7, 0x0, 0x0, 0xCD, 0x7, 0x0, 0x0, 0xCF, 0x7, 0x0, 0x0, 0xD3, 0x7, 0x0, 0x0, 0xDB, 0x7, 0x0, 0x0, 0xE1, 0x7, 0x0, 0x0, 0xEB, 0x7, 0x0, 0x0, 0xED, 0x7, 0x0, 0x0, 0xF7, 0x7, 0x0, 0x0, 0x5, 0x8, 0x0, 0x0, 0xF, 0x8, 0x0, 0x0, 0x15, 0x8, 0x0, 0x0, 0x21, 0x8, 0x0, 0x0, 0x23, 0x8, 0x0, 0x0, 0x27, 0x8, 0x0, 0x0, 0x29, 0x8, 0x0, 0x0, 0x33, 0x8, 0x0, 0x0, 0x3F, 0x8, 0x0, 0x0, 0x41, 0x8, 0x0, 0x0, 0x51, 0x8, 0x0, 0x0, 0x53, 0x8, 0x0, 0x0, 0x59, 0x8, 0x0, 0x0, 0x5D, 0x8, 0x0, 0x0, 0x5F, 0x8, 0x0, 0x0, 0x69, 0x8, 0x0, 0x0, 0x71, 0x8, 0x0, 0x0, 0x83, 0x8, 0x0, 0x0, 0x9B, 0x8, 0x0, 0x0, 0x9F, 0x8, 0x0, 0x0, 0xA5, 0x8, 0x0, 0x0, 0xAD, 0x8, 0x0, 0x0, 0xBD, 0x8, 0x0, 0x0, 0xBF, 0x8, 0x0, 0x0, 0xC3, 0x8, 0x0, 0x0, 0xCB, 0x8, 0x0, 0x0, 0xDB, 0x8, 0x0, 0x0, 0xDD, 0x8, 0x0, 0x0, 0xE1, 0x8, 0x0, 0x0, 0xE9, 0x8, 0x0, 0x0, 0xEF, 0x8, 0x0, 0x0, 0xF5, 0x8, 0x0, 0x0, 0xF9, 0x8, 0x0, 0x0, 0x5, 0x9, 0x0, 0x0, 0x7, 0x9, 0x0, 0x0, 0x1D, 0x9, 0x0, 0x0, 0x23, 0x9, 0x0, 0x0, 0x25, 0x9, 0x0, 0x0, 0x2B, 0x9, 0x0, 0x0, 0x2F, 0x9, 0x0, 0x0, 0x35, 0x9, 0x0, 0x0, 0x43, 0x9, 0x0, 0x0, 0x49, 0x9, 0x0, 0x0, 0x4D, 0x9, 0x0, 0x0, 0x4F, 0x9, 0x0, 0x0, 0x55, 0x9, 0x0, 0x0, 0x59, 0x9, 0x0, 0x0, 0x5F, 0x9, 0x0, 0x0, 0x6B, 0x9, 0x0, 0x0, 0x71, 0x9, 0x0, 0x0, 0x77, 0x9, 0x0, 0x0, 0x85, 0x9, 0x0, 0x0, 0x89, 0x9, 0x0, 0x0, 0x8F, 0x9, 0x0, 0x0, 0x9B, 0x9, 0x0, 0x0, 0xA3, 0x9, 0x0, 0x0, 0xA9, 0x9, 0x0, 0x0, 0xAD, 0x9, 0x0, 0x0, 0xC7, 0x9, 0x0, 0x0, 0xD9, 0x9, 0x0, 0x0, 0xE3, 0x9, 0x0, 0x0, 0xEB, 0x9, 0x0, 0x0, 0xEF, 0x9, 0x0, 0x0, 0xF5, 0x9, 0x0, 0x0, 0xF7, 0x9, 0x0, 0x0, 0xFD, 0x9, 0x0, 0x0, 0x13, 0xA, 0x0, 0x0, 0x1F, 0xA, 0x0, 0x0, 0x21, 0xA, 0x0, 0x0, 0x31, 0xA, 0x0, 0x0, 0x39, 0xA, 0x0, 0x0, 0x3D, 0xA, 0x0, 0x0, 0x49, 0xA, 0x0, 0x0, 0x57, 0xA, 0x0, 0x0, 0x61, 0xA, 0x0, 0x0, 0x63, 0xA, 0x0, 0x0, 0x67, 0xA, 0x0, 0x0, 0x6F, 0xA, 0x0, 0x0, 0x75, 0xA, 0x0, 0x0, 0x7B, 0xA, 0x0, 0x0, 0x7F, 0xA, 0x0, 0x0, 0x81, 0xA, 0x0, 0x0, 0x85, 0xA, 0x0, 0x0, 0x8B, 0xA, 0x0, 0x0, 0x93, 0xA, 0x0, 0x0, 0x97, 0xA, 0x0, 0x0, 0x99, 0xA, 0x0, 0x0, 0x9F, 0xA, 0x0, 0x0, 0xA9, 0xA, 0x0, 0x0, 0xAB, 0xA, 0x0, 0x0, 0xB5, 0xA, 0x0, 0x0, 0xBD, 0xA, 0x0, 0x0, 0xC1, 0xA, 0x0, 0x0, 0xCF, 0xA, 0x0, 0x0, 0xD9, 0xA, 0x0, 0x0, 0xE5, 0xA, 0x0, 0x0, 0xE7, 0xA, 0x0, 0x0, 0xED, 0xA, 0x0, 0x0, 0xF1, 0xA, 0x0, 0x0, 0xF3, 0xA, 0x0, 0x0, 0x3, 0xB, 0x0, 0x0, 0x11, 0xB, 0x0, 0x0, 0x15, 0xB, 0x0, 0x0, 0x1B, 0xB, 0x0, 0x0, 0x23, 0xB, 0x0, 0x0, 0x29, 0xB, 0x0, 0x0, 0x2D, 0xB, 0x0, 0x0, 0x3F, 0xB, 0x0, 0x0, 0x47, 0xB, 0x0, 0x0, 0x51, 0xB, 0x0, 0x0, 0x57, 0xB, 0x0, 0x0, 0x5D, 0xB, 0x0, 0x0, 0x65, 0xB, 0x0, 0x0, 0x6F, 0xB, 0x0, 0x0, 0x7B, 0xB, 0x0, 0x0, 0x89, 0xB, 0x0, 0x0, 0x8D, 0xB, 0x0, 0x0, 0x93, 0xB, 0x0, 0x0, 0x99, 0xB, 0x0, 0x0, 0x9B, 0xB, 0x0, 0x0, 0xB7, 0xB, 0x0, 0x0, 0xB9, 0xB, 0x0, 0x0, 0xC3, 0xB, 0x0, 0x0, 0xCB, 0xB, 0x0, 0x0, 0xCF, 0xB, 0x0, 0x0, 0xDD, 0xB, 0x0, 0x0, 0xE1, 0xB, 0x0, 0x0, 0xE9, 0xB, 0x0, 0x0, 0xF5, 0xB, 0x0, 0x0, 0xFB, 0xB, 0x0, 0x0, 0x7, 0xC, 0x0, 0x0, 0xB, 0xC, 0x0, 0x0, 0x11, 0xC, 0x0, 0x0, 0x25, 0xC, 0x0, 0x0, 0x2F, 0xC, 0x0, 0x0, 0x31, 0xC, 0x0, 0x0, 0x41, 0xC, 0x0, 0x0, 0x5B, 0xC, 0x0, 0x0, 0x5F, 0xC, 0x0, 0x0, 0x61, 0xC, 0x0, 0x0, 0x6D, 0xC, 0x0, 0x0, 0x73, 0xC, 0x0, 0x0, 0x77, 0xC, 0x0, 0x0, 0x83, 0xC, 0x0, 0x0, 0x89, 0xC, 0x0, 0x0, 0x91, 0xC, 0x0, 0x0, 0x95, 0xC, 0x0, 0x0, 0x9D, 0xC, 0x0, 0x0, 0xB3, 0xC, 0x0, 0x0, 0xB5, 0xC, 0x0, 0x0, 0xB9, 0xC, 0x0, 0x0, 0xBB, 0xC, 0x0, 0x0, 0xC7, 0xC, 0x0, 0x0, 0xE3, 0xC, 0x0, 0x0, 0xE5, 0xC, 0x0, 0x0, 0xEB, 0xC, 0x0, 0x0, 0xF1, 0xC, 0x0, 0x0, 0xF7, 0xC, 0x0, 0x0, 0xFB, 0xC, 0x0, 0x0, 0x1, 0xD, 0x0, 0x0, 0x3, 0xD, 0x0, 0x0, 0xF, 0xD, 0x0, 0x0, 0x13, 0xD, 0x0, 0x0, 0x1F, 0xD, 0x0, 0x0, 0x21, 0xD, 0x0, 0x0, 0x2B, 0xD, 0x0, 0x0, 0x2D, 0xD, 0x0, 0x0, 0x3D, 0xD, 0x0, 0x0, 0x3F, 0xD, 0x0, 0x0, 0x4F, 0xD, 0x0, 0x0, 0x55, 0xD, 0x0, 0x0, 0x69, 0xD, 0x0, 0x0, 0x79, 0xD, 0x0, 0x0, 0x81, 0xD, 0x0, 0x0, 0x85, 0xD, 0x0, 0x0, 0x87, 0xD, 0x0, 0x0, 0x8B, 0xD, 0x0, 0x0, 0x8D, 0xD, 0x0, 0x0, 0xA3, 0xD, 0x0, 0x0, 0xAB, 0xD, 0x0, 0x0, 0xB7, 0xD, 0x0, 0x0, 0xBD, 0xD, 0x0, 0x0, 0xC7, 0xD, 0x0, 0x0, 0xC9, 0xD, 0x0, 0x0, 0xCD, 0xD, 0x0, 0x0, 0xD3, 0xD, 0x0, 0x0, 0xD5, 0xD, 0x0, 0x0, 0xDB, 0xD, 0x0, 0x0, 0xE5, 0xD, 0x0, 0x0, 0xE7, 0xD, 0x0, 0x0, 0xF3, 0xD, 0x0, 0x0, 0xFD, 0xD, 0x0, 0x0, 0xFF, 0xD, 0x0, 0x0, 0x9, 0xE, 0x0, 0x0, 0x17, 0xE, 0x0, 0x0, 0x1D, 0xE, 0x0, 0x0, 0x21, 0xE, 0x0, 0x0, 0x27, 0xE, 0x0, 0x0, 0x2F, 0xE, 0x0, 0x0, 0x35, 0xE, 0x0, 0x0, 0x3B, 0xE, 0x0, 0x0, 0x4B, 0xE, 0x0, 0x0, 0x57, 0xE, 0x0, 0x0, 0x59, 0xE, 0x0, 0x0, 0x5D, 0xE, 0x0, 0x0, 0x6B, 0xE, 0x0, 0x0, 0x71, 0xE, 0x0, 0x0, 0x75, 0xE, 0x0, 0x0, 0x7D, 0xE, 0x0, 0x0, 0x87, 0xE, 0x0, 0x0, 0x8F, 0xE, 0x0, 0x0, 0x95, 0xE, 0x0, 0x0, 0x9B, 0xE, 0x0, 0x0, 0xB1, 0xE, 0x0, 0x0, 0xB7, 0xE, 0x0, 0x0, 0xB9, 0xE, 0x0, 0x0, 0xC3, 0xE, 0x0, 0x0, 0xD1, 0xE, 0x0, 0x0, 0xD5, 0xE, 0x0, 0x0, 0xDB, 0xE, 0x0, 0x0, 0xED, 0xE, 0x0, 0x0, 0xEF, 0xE, 0x0, 0x0, 0xF9, 0xE, 0x0, 0x0, 0x7, 0xF, 0x0, 0x0, 0xB, 0xF, 0x0, 0x0, 0xD, 0xF, 0x0, 0x0, 0x17, 0xF, 0x0, 0x0, 0x25, 0xF, 0x0, 0x0, 0x29, 0xF, 0x0, 0x0, 0x31, 0xF, 0x0, 0x0, 0x43, 0xF, 0x0, 0x0, 0x47, 0xF, 0x0, 0x0, 0x4D, 0xF, 0x0, 0x0, 0x4F, 0xF, 0x0, 0x0, 0x53, 0xF, 0x0, 0x0, 0x59, 0xF, 0x0, 0x0, 0x5B, 0xF, 0x0, 0x0, 0x67, 0xF, 0x0, 0x0, 0x6B, 0xF, 0x0, 0x0, 0x7F, 0xF, 0x0, 0x0, 0x95, 0xF, 0x0, 0x0, 0xA1, 0xF, 0x0, 0x0, 0xA3, 0xF, 0x0, 0x0, 0xA7, 0xF, 0x0, 0x0, 0xAD, 0xF, 0x0, 0x0, 0xB3, 0xF, 0x0, 0x0, 0xB5, 0xF, 0x0, 0x0, 0xBB, 0xF, 0x0, 0x0, 0xD1, 0xF, 0x0, 0x0, 0xD3, 0xF, 0x0, 0x0, 0xD9, 0xF, 0x0, 0x0, 0xE9, 0xF, 0x0, 0x0, 0xEF, 0xF, 0x0, 0x0, 0xFB, 0xF, 0x0, 0x0, 0xFD, 0xF, 0x0, 0x0, 0x3, 0x10, 0x0, 0x0, 0xF, 0x10, 0x0, 0x0, 0x1F, 0x10, 0x0, 0x0, 0x21, 0x10, 0x0, 0x0, 0x25, 0x10, 0x0, 0x0, 0x2B, 0x10, 0x0, 0x0, 0x39, 0x10, 0x0, 0x0, 0x3D, 0x10, 0x0, 0x0, 0x3F, 0x10, 0x0, 0x0, 0x51, 0x10, 0x0, 0x0, 0x69, 0x10, 0x0, 0x0, 0x73, 0x10, 0x0, 0x0, 0x79, 0x10, 0x0, 0x0, 0x7B, 0x10, 0x0, 0x0, 0x85, 0x10, 0x0, 0x0, 0x87, 0x10, 0x0, 0x0, 0x91, 0x10, 0x0, 0x0, 0x93, 0x10, 0x0, 0x0, 0x9D, 0x10, 0x0, 0x0, 0xA3, 0x10, 0x0, 0x0, 0xA5, 0x10, 0x0, 0x0, 0xAF, 0x10, 0x0, 0x0, 0xB1, 0x10, 0x0, 0x0, 0xBB, 0x10, 0x0, 0x0, 0xC1, 0x10, 0x0, 0x0, 0xC9, 0x10, 0x0, 0x0, 0xE7, 0x10, 0x0, 0x0, 0xF1, 0x10, 0x0, 0x0, 0xF3, 0x10, 0x0, 0x0, 0xFD, 0x10, 0x0, 0x0, 0x5, 0x11, 0x0, 0x0, 0xB, 0x11, 0x0, 0x0, 0x15, 0x11, 0x0, 0x0, 0x27, 0x11, 0x0, 0x0, 0x2D, 0x11, 0x0, 0x0, 0x39, 0x11, 0x0, 0x0, 0x45, 0x11, 0x0, 0x0, 0x47, 0x11, 0x0, 0x0, 0x59, 0x11, 0x0, 0x0, 0x5F, 0x11, 0x0, 0x0, 0x63, 0x11, 0x0, 0x0, 0x69, 0x11, 0x0, 0x0, 0x6F, 0x11, 0x0, 0x0, 0x81, 0x11, 0x0, 0x0, 0x83, 0x11, 0x0, 0x0, 0x8D, 0x11, 0x0, 0x0, 0x9B, 0x11, 0x0, 0x0, 0xA1, 0x11, 0x0, 0x0, 0xA5, 0x11, 0x0, 0x0, 0xA7, 0x11, 0x0, 0x0, 0xAB, 0x11, 0x0, 0x0, 0xC3, 0x11, 0x0, 0x0, 0xC5, 0x11, 0x0, 0x0, 0xD1, 0x11, 0x0, 0x0, 0xD7, 0x11, 0x0, 0x0, 0xE7, 0x11, 0x0, 0x0, 0xEF, 0x11, 0x0, 0x0, 0xF5, 0x11, 0x0, 0x0, 0xFB, 0x11, 0x0, 0x0, 0xD, 0x12, 0x0, 0x0, 0x1D, 0x12, 0x0, 0x0, 0x1F, 0x12, 0x0, 0x0, 0x23, 0x12, 0x0, 0x0, 0x29, 0x12, 0x0, 0x0, 0x2B, 0x12, 0x0, 0x0, 0x31, 0x12, 0x0, 0x0, 0x37, 0x12, 0x0, 0x0, 0x41, 0x12, 0x0, 0x0, 0x47, 0x12, 0x0, 0x0, 0x53, 0x12, 0x0, 0x0, 0x5F, 0x12, 0x0, 0x0, 0x71, 0x12, 0x0, 0x0, 0x73, 0x12, 0x0, 0x0, 0x79, 0x12, 0x0, 0x0, 0x7D, 0x12, 0x0, 0x0, 0x8F, 0x12, 0x0, 0x0, 0x97, 0x12, 0x0, 0x0, 0xAF, 0x12, 0x0, 0x0, 0xB3, 0x12, 0x0, 0x0, 0xB5, 0x12, 0x0, 0x0, 0xB9, 0x12, 0x0, 0x0, 0xBF, 0x12, 0x0, 0x0, 0xC1, 0x12, 0x0, 0x0, 0xCD, 0x12, 0x0, 0x0, 0xD1, 0x12, 0x0, 0x0, 0xDF, 0x12, 0x0, 0x0, 0xFD, 0x12, 0x0, 0x0, 0x7, 0x13, 0x0, 0x0, 0xD, 0x13, 0x0, 0x0, 0x19, 0x13, 0x0, 0x0, 0x27, 0x13, 0x0, 0x0, 0x2D, 0x13, 0x0, 0x0, 0x37, 0x13, 0x0, 0x0, 0x43, 0x13, 0x0, 0x0, 0x45, 0x13, 0x0, 0x0, 0x49, 0x13, 0x0, 0x0, 0x4F, 0x13, 0x0, 0x0, 0x57, 0x13, 0x0, 0x0, 0x5D, 0x13, 0x0, 0x0, 0x67, 0x13, 0x0, 0x0, 0x69, 0x13, 0x0, 0x0, 0x6D, 0x13, 0x0, 0x0, 0x7B, 0x13, 0x0, 0x0, 0x81, 0x13, 0x0, 0x0, 0x87, 0x13, 0x0, 0x0, 0x8B, 0x13, 0x0, 0x0, 0x91, 0x13, 0x0, 0x0, 0x93, 0x13, 0x0, 0x0, 0x9D, 0x13, 0x0, 0x0, 0x9F, 0x13, 0x0, 0x0, 0xAF, 0x13, 0x0, 0x0, 0xBB, 0x13, 0x0, 0x0, 0xC3, 0x13, 0x0, 0x0, 0xD5, 0x13, 0x0, 0x0, 0xD9, 0x13, 0x0, 0x0, 0xDF, 0x13, 0x0, 0x0, 0xEB, 0x13, 0x0, 0x0, 0xED, 0x13, 0x0, 0x0, 0xF3, 0x13, 0x0, 0x0, 0xF9, 0x13, 0x0, 0x0, 0xFF, 0x13, 0x0, 0x0, 0x1B, 0x14, 0x0, 0x0, 0x21, 0x14, 0x0, 0x0, 0x2F, 0x14, 0x0, 0x0, 0x33, 0x14, 0x0, 0x0, 0x3B, 0x14, 0x0, 0x0, 0x45, 0x14, 0x0, 0x0, 0x4D, 0x14, 0x0, 0x0, 0x59, 0x14, 0x0, 0x0, 0x6B, 0x14, 0x0, 0x0, 0x6F, 0x14, 0x0, 0x0, 0x71, 0x14, 0x0, 0x0, 0x75, 0x14, 0x0, 0x0, 0x8D, 0x14, 0x0, 0x0, 0x99, 0x14, 0x0, 0x0, 0x9F, 0x14, 0x0, 0x0, 0xA1, 0x14, 0x0, 0x0, 0xB1, 0x14, 0x0, 0x0, 0xB7, 0x14, 0x0, 0x0, 0xBD, 0x14, 0x0, 0x0, 0xCB, 0x14, 0x0, 0x0, 0xD5, 0x14, 0x0, 0x0, 0xE3, 0x14, 0x0, 0x0, 0xE7, 0x14, 0x0, 0x0, 0x5, 0x15, 0x0, 0x0, 0xB, 0x15, 0x0, 0x0, 0x11, 0x15, 0x0, 0x0, 0x17, 0x15, 0x0, 0x0, 0x1F, 0x15, 0x0, 0x0, 0x25, 0x15, 0x0, 0x0, 0x29, 0x15, 0x0, 0x0, 0x2B, 0x15, 0x0, 0x0, 0x37, 0x15, 0x0, 0x0, 0x3D, 0x15, 0x0, 0x0, 0x41, 0x15, 0x0, 0x0, 0x43, 0x15, 0x0, 0x0, 0x49, 0x15, 0x0, 0x0, 0x5F, 0x15, 0x0, 0x0, 0x65, 0x15, 0x0, 0x0, 0x67, 0x15, 0x0, 0x0, 0x6B, 0x15, 0x0, 0x0, 0x7D, 0x15, 0x0, 0x0, 0x7F, 0x15, 0x0, 0x0, 0x83, 0x15, 0x0, 0x0, 0x8F, 0x15, 0x0, 0x0, 0x91, 0x15, 0x0, 0x0, 0x97, 0x15, 0x0, 0x0, 0x9B, 0x15, 0x0, 0x0, 0xB5, 0x15, 0x0, 0x0, 0xBB, 0x15, 0x0, 0x0, 0xC1, 0x15, 0x0, 0x0, 0xC5, 0x15, 0x0, 0x0, 0xCD, 0x15, 0x0, 0x0, 0xD7, 0x15, 0x0, 0x0, 0xF7, 0x15, 0x0, 0x0, 0x7, 0x16, 0x0, 0x0, 0x9, 0x16, 0x0, 0x0, 0xF, 0x16, 0x0, 0x0, 0x13, 0x16, 0x0, 0x0, 0x15, 0x16, 0x0, 0x0, 0x19, 0x16, 0x0, 0x0, 0x1B, 0x16, 0x0, 0x0, 0x25, 0x16, 0x0, 0x0, 0x33, 0x16, 0x0, 0x0, 0x39, 0x16, 0x0, 0x0, 0x3D, 0x16, 0x0, 0x0, 0x45, 0x16, 0x0, 0x0, 0x4F, 0x16, 0x0, 0x0, 0x55, 0x16, 0x0, 0x0, 0x69, 0x16, 0x0, 0x0, 0x6D, 0x16, 0x0, 0x0, 0x6F, 0x16, 0x0, 0x0, 0x75, 0x16, 0x0, 0x0, 0x93, 0x16, 0x0, 0x0, 0x97, 0x16, 0x0, 0x0, 0x9F, 0x16, 0x0, 0x0, 0xA9, 0x16, 0x0, 0x0, 0xAF, 0x16, 0x0, 0x0, 0xB5, 0x16, 0x0, 0x0, 0xBD, 0x16, 0x0, 0x0, 0xC3, 0x16, 0x0, 0x0, 0xCF, 0x16, 0x0, 0x0, 0xD3, 0x16, 0x0, 0x0, 0xD9, 0x16, 0x0, 0x0, 0xDB, 0x16, 0x0, 0x0, 0xE1, 0x16, 0x0, 0x0, 0xE5, 0x16, 0x0, 0x0, 0xEB, 0x16, 0x0, 0x0, 0xED, 0x16, 0x0, 0x0, 0xF7, 0x16, 0x0, 0x0, 0xF9, 0x16, 0x0, 0x0, 0x9, 0x17, 0x0, 0x0, 0xF, 0x17, 0x0, 0x0, 0x23, 0x17, 0x0, 0x0, 0x27, 0x17, 0x0, 0x0, 0x33, 0x17, 0x0, 0x0, 0x41, 0x17, 0x0, 0x0, 0x5D, 0x17, 0x0, 0x0, 0x63, 0x17, 0x0, 0x0 };
static Il2CppFieldDefaultValueEntry t1088_f0_DefaultValue = 
{
	&t1088_f0_FieldInfo, { (char*)t1088_f0_DefaultValueData, &t1079_0_0_0 }};
static const uint8_t t1088_f1_DefaultValueData[] = { 0x29, 0x2E, 0x43, 0xC9, 0xA2, 0xD8, 0x7C, 0x1, 0x3D, 0x36, 0x54, 0xA1, 0xEC, 0xF0, 0x6, 0x13, 0x62, 0xA7, 0x5, 0xF3, 0xC0, 0xC7, 0x73, 0x8C, 0x98, 0x93, 0x2B, 0xD9, 0xBC, 0x4C, 0x82, 0xCA, 0x1E, 0x9B, 0x57, 0x3C, 0xFD, 0xD4, 0xE0, 0x16, 0x67, 0x42, 0x6F, 0x18, 0x8A, 0x17, 0xE5, 0x12, 0xBE, 0x4E, 0xC4, 0xD6, 0xDA, 0x9E, 0xDE, 0x49, 0xA0, 0xFB, 0xF5, 0x8E, 0xBB, 0x2F, 0xEE, 0x7A, 0xA9, 0x68, 0x79, 0x91, 0x15, 0xB2, 0x7, 0x3F, 0x94, 0xC2, 0x10, 0x89, 0xB, 0x22, 0x5F, 0x21, 0x80, 0x7F, 0x5D, 0x9A, 0x5A, 0x90, 0x32, 0x27, 0x35, 0x3E, 0xCC, 0xE7, 0xBF, 0xF7, 0x97, 0x3, 0xFF, 0x19, 0x30, 0xB3, 0x48, 0xA5, 0xB5, 0xD1, 0xD7, 0x5E, 0x92, 0x2A, 0xAC, 0x56, 0xAA, 0xC6, 0x4F, 0xB8, 0x38, 0xD2, 0x96, 0xA4, 0x7D, 0xB6, 0x76, 0xFC, 0x6B, 0xE2, 0x9C, 0x74, 0x4, 0xF1, 0x45, 0x9D, 0x70, 0x59, 0x64, 0x71, 0x87, 0x20, 0x86, 0x5B, 0xCF, 0x65, 0xE6, 0x2D, 0xA8, 0x2, 0x1B, 0x60, 0x25, 0xAD, 0xAE, 0xB0, 0xB9, 0xF6, 0x1C, 0x46, 0x61, 0x69, 0x34, 0x40, 0x7E, 0xF, 0x55, 0x47, 0xA3, 0x23, 0xDD, 0x51, 0xAF, 0x3A, 0xC3, 0x5C, 0xF9, 0xCE, 0xBA, 0xC5, 0xEA, 0x26, 0x2C, 0x53, 0xD, 0x6E, 0x85, 0x28, 0x84, 0x9, 0xD3, 0xDF, 0xCD, 0xF4, 0x41, 0x81, 0x4D, 0x52, 0x6A, 0xDC, 0x37, 0xC8, 0x6C, 0xC1, 0xAB, 0xFA, 0x24, 0xE1, 0x7B, 0x8, 0xC, 0xBD, 0xB1, 0x4A, 0x78, 0x88, 0x95, 0x8B, 0xE3, 0x63, 0xE8, 0x6D, 0xE9, 0xCB, 0xD5, 0xFE, 0x3B, 0x0, 0x1D, 0x39, 0xF2, 0xEF, 0xB7, 0xE, 0x66, 0x58, 0xD0, 0xE4, 0xA6, 0x77, 0x72, 0xF8, 0xEB, 0x75, 0x4B, 0xA, 0x31, 0x44, 0x50, 0xB4, 0x8F, 0xED, 0x1F, 0x1A, 0xDB, 0x99, 0x8D, 0x33, 0x9F, 0x11, 0x83, 0x14 };
static Il2CppFieldDefaultValueEntry t1088_f1_DefaultValue = 
{
	&t1088_f1_FieldInfo, { (char*)t1088_f1_DefaultValueData, &t1080_0_0_0 }};
static const uint8_t t1088_f2_DefaultValueData[] = { 0xDA, 0x39, 0xA3, 0xEE, 0x5E, 0x6B, 0x4B, 0xD, 0x32, 0x55, 0xBF, 0xEF, 0x95, 0x60, 0x18, 0x90, 0xAF, 0xD8, 0x7, 0x9 };
static Il2CppFieldDefaultValueEntry t1088_f2_DefaultValue = 
{
	&t1088_f2_FieldInfo, { (char*)t1088_f2_DefaultValueData, &t1081_0_0_0 }};
static const uint8_t t1088_f3_DefaultValueData[] = { 0xE3, 0xB0, 0xC4, 0x42, 0x98, 0xFC, 0x1C, 0x14, 0x9A, 0xFB, 0xF4, 0xC8, 0x99, 0x6F, 0xB9, 0x24, 0x27, 0xAE, 0x41, 0xE4, 0x64, 0x9B, 0x93, 0x4C, 0xA4, 0x95, 0x99, 0x1B, 0x78, 0x52, 0xB8, 0x55 };
static Il2CppFieldDefaultValueEntry t1088_f3_DefaultValue = 
{
	&t1088_f3_FieldInfo, { (char*)t1088_f3_DefaultValueData, &t1082_0_0_0 }};
static const uint8_t t1088_f4_DefaultValueData[] = { 0x38, 0xB0, 0x60, 0xA7, 0x51, 0xAC, 0x96, 0x38, 0x4C, 0xD9, 0x32, 0x7E, 0xB1, 0xB1, 0xE3, 0x6A, 0x21, 0xFD, 0xB7, 0x11, 0x14, 0xBE, 0x7, 0x43, 0x4C, 0xC, 0xC7, 0xBF, 0x63, 0xF6, 0xE1, 0xDA, 0x27, 0x4E, 0xDE, 0xBF, 0xE7, 0x6F, 0x65, 0xFB, 0xD5, 0x1A, 0xD2, 0xF1, 0x48, 0x98, 0xB9, 0x5B };
static Il2CppFieldDefaultValueEntry t1088_f4_DefaultValue = 
{
	&t1088_f4_FieldInfo, { (char*)t1088_f4_DefaultValueData, &t1083_0_0_0 }};
static const uint8_t t1088_f5_DefaultValueData[] = { 0xCF, 0x83, 0xE1, 0x35, 0x7E, 0xEF, 0xB8, 0xBD, 0xF1, 0x54, 0x28, 0x50, 0xD6, 0x6D, 0x80, 0x7, 0xD6, 0x20, 0xE4, 0x5, 0xB, 0x57, 0x15, 0xDC, 0x83, 0xF4, 0xA9, 0x21, 0xD3, 0x6C, 0xE9, 0xCE, 0x47, 0xD0, 0xD1, 0x3C, 0x5D, 0x85, 0xF2, 0xB0, 0xFF, 0x83, 0x18, 0xD2, 0x87, 0x7E, 0xEC, 0x2F, 0x63, 0xB9, 0x31, 0xBD, 0x47, 0x41, 0x7A, 0x81, 0xA5, 0x38, 0x32, 0x7A, 0xF9, 0x27, 0xDA, 0x3E };
static Il2CppFieldDefaultValueEntry t1088_f5_DefaultValue = 
{
	&t1088_f5_FieldInfo, { (char*)t1088_f5_DefaultValueData, &t1084_0_0_0 }};
static const uint8_t t1088_f6_DefaultValueData[] = { 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1 };
static Il2CppFieldDefaultValueEntry t1088_f6_DefaultValue = 
{
	&t1088_f6_FieldInfo, { (char*)t1088_f6_DefaultValueData, &t1084_0_0_0 }};
static const uint8_t t1088_f7_DefaultValueData[] = { 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2 };
static Il2CppFieldDefaultValueEntry t1088_f7_DefaultValue = 
{
	&t1088_f7_FieldInfo, { (char*)t1088_f7_DefaultValueData, &t1084_0_0_0 }};
static const uint8_t t1088_f8_DefaultValueData[] = { 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3 };
static Il2CppFieldDefaultValueEntry t1088_f8_DefaultValue = 
{
	&t1088_f8_FieldInfo, { (char*)t1088_f8_DefaultValueData, &t1084_0_0_0 }};
static const uint8_t t1088_f9_DefaultValueData[] = { 0x9, 0x92, 0x26, 0x89, 0x93, 0xF2, 0x2C, 0x64, 0x1, 0x19, 0x0, 0x0 };
static Il2CppFieldDefaultValueEntry t1088_f9_DefaultValue = 
{
	&t1088_f9_FieldInfo, { (char*)t1088_f9_DefaultValueData, &t1085_0_0_0 }};
static const uint8_t t1088_f10_DefaultValueData[] = { 0x9, 0x92, 0x26, 0x89, 0x93, 0xF2, 0x2C, 0x64, 0x1, 0x1, 0x0, 0x0 };
static Il2CppFieldDefaultValueEntry t1088_f10_DefaultValue = 
{
	&t1088_f10_FieldInfo, { (char*)t1088_f10_DefaultValueData, &t1085_0_0_0 }};
static const uint8_t t1088_f11_DefaultValueData[] = { 0x2A, 0x86, 0x48, 0x86, 0xF7, 0xD, 0x1, 0x9, 0x1, 0x0, 0x0, 0x0 };
static Il2CppFieldDefaultValueEntry t1088_f11_DefaultValue = 
{
	&t1088_f11_FieldInfo, { (char*)t1088_f11_DefaultValueData, &t1085_0_0_0 }};
static const uint8_t t1088_f12_DefaultValueData[] = { 0x2C, 0x0, 0x2B, 0x0, 0x22, 0x0, 0x5C, 0x0, 0x3C, 0x0, 0x3E, 0x0, 0x3B, 0x0, 0x0, 0x0 };
static Il2CppFieldDefaultValueEntry t1088_f12_DefaultValue = 
{
	&t1088_f12_FieldInfo, { (char*)t1088_f12_DefaultValueData, &t1086_0_0_0 }};
static const uint8_t t1088_f13_DefaultValueData[] = { 0x43, 0x4C, 0x4E, 0x54 };
static Il2CppFieldDefaultValueEntry t1088_f13_DefaultValue = 
{
	&t1088_f13_FieldInfo, { (char*)t1088_f13_DefaultValueData, &t1087_0_0_0 }};
static const uint8_t t1088_f14_DefaultValueData[] = { 0x53, 0x52, 0x56, 0x52 };
static Il2CppFieldDefaultValueEntry t1088_f14_DefaultValue = 
{
	&t1088_f14_FieldInfo, { (char*)t1088_f14_DefaultValueData, &t1087_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1088_FDVs[] = 
{
	&t1088_f0_DefaultValue,
	&t1088_f1_DefaultValue,
	&t1088_f2_DefaultValue,
	&t1088_f3_DefaultValue,
	&t1088_f4_DefaultValue,
	&t1088_f5_DefaultValue,
	&t1088_f6_DefaultValue,
	&t1088_f7_DefaultValue,
	&t1088_f8_DefaultValue,
	&t1088_f9_DefaultValue,
	&t1088_f10_DefaultValue,
	&t1088_f11_DefaultValue,
	&t1088_f12_DefaultValue,
	&t1088_f13_DefaultValue,
	&t1088_f14_DefaultValue,
	NULL
};
static MethodInfo* t1088_MIs[] =
{
	NULL
};
extern TypeInfo t1079_TI;
extern TypeInfo t1080_TI;
extern TypeInfo t1081_TI;
extern TypeInfo t1082_TI;
extern TypeInfo t1083_TI;
extern TypeInfo t1084_TI;
extern TypeInfo t1085_TI;
extern TypeInfo t1086_TI;
extern TypeInfo t1087_TI;
static TypeInfo* t1088_TI__nestedTypes[10] =
{
	&t1079_TI,
	&t1080_TI,
	&t1081_TI,
	&t1082_TI,
	&t1083_TI,
	&t1084_TI,
	&t1085_TI,
	&t1086_TI,
	&t1087_TI,
	NULL
};
static MethodInfo* t1088_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern TypeInfo t38_TI;
#include "t38.h"
#include "t38MD.h"
extern MethodInfo m55_MI;
void t1088_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1088__CustomAttributeCache = {
1,
NULL,
&t1088_CustomAttributesCacheGenerator
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType t1088_0_0_0;
extern Il2CppType t1088_1_0_0;
struct t1088;
extern CustomAttributesCache t1088__CustomAttributeCache;
TypeInfo t1088_TI = 
{
	&g_Mono_Security_dll_Image, NULL, "<PrivateImplementationDetails>", "", t1088_MIs, NULL, t1088_FIs, NULL, &t29_TI, t1088_TI__nestedTypes, NULL, &t1088_TI, NULL, t1088_VT, &t1088__CustomAttributeCache, &t1088_TI, &t1088_0_0_0, &t1088_1_0_0, NULL, NULL, NULL, t1088_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1088), 0, -1, sizeof(t1088_SFs), 0, -1, 0, 0, false, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 15, 0, 9, 4, 0, 0};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
