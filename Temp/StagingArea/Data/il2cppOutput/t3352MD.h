﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3352;
struct t29;
struct t20;
#include "t1321.h"

 void m18632 (t3352 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18633 (t3352 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18634 (t3352 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18635 (t3352 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18636 (t3352 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
