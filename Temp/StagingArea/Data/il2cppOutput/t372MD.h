﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t372;
struct t1094;
struct t29;
struct t42;
struct t7;
struct t295;
#include "t465.h"
#include "t1126.h"
#include "t923.h"

 bool m5469 (int16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m5470 (int16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m5471 (int16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m5472 (int16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m5473 (int16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m5474 (int16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m5475 (int16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5476 (int16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m5477 (int16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int8_t m5478 (int16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m5479 (int16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m5480 (int16_t* __this, t42 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m5481 (int16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m5482 (int16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m5483 (int16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5484 (int16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5485 (int16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5486 (int16_t* __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5487 (int16_t* __this, int16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5488 (int16_t* __this, int16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5489 (t29 * __this, t7* p0, bool p1, int16_t* p2, t295 ** p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m5490 (t29 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m5491 (t29 * __this, t7* p0, int32_t p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5492 (t29 * __this, t7* p0, int16_t* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5493 (int16_t* __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5494 (int16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5495 (int16_t* __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5496 (int16_t* __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
