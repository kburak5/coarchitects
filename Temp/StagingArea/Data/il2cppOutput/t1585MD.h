﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1585;

 void m8583 (t1585 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8584 (t1585 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8585 (t1585 * __this, uint16_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8586 (t1585 * __this, uint16_t p0, uint16_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m8587 (t1585 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
