﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2758;
struct t29;
struct t20;
#include "t255.h"

 void m15099 (t2758 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15100 (t2758 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15101 (t2758 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15102 (t2758 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m15103 (t2758 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
