﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2604;
struct t29;
struct t20;
#include "t185.h"

 void m14026 (t2604 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m14027 (t2604 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m14028 (t2604 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m14029 (t2604 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m14030 (t2604 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
