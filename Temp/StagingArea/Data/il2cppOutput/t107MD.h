﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t107;
struct t109;
struct t108;

 void m159 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m160 (t29 * __this, t109 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t108 * m161 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m162 (t29 * __this, t109 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
