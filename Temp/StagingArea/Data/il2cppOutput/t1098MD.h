﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1098;
struct t7;
#include "t1099.h"

 void m5140 (t1098 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8178 (t1098 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8179 (t1098 * __this, int32_t p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8180 (t1098 * __this, int32_t p0, t7* p1, t7* p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8181 (t1098 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5141 (t1098 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
