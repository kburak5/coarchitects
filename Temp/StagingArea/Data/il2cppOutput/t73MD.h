﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t73;
struct t29;
struct t91;
struct t53;
struct t66;
struct t67;
#include "t35.h"

#include "t2213MD.h"
#define m1344(__this, p0, p1, method) (void)m10902_gshared((t2213 *)__this, (t29 *)p0, (t35)p1, method)
#define m11808(__this, p0, p1, method) (void)m10904_gshared((t2213 *)__this, (t29 *)p0, (t53 *)p1, method)
#define m11809(__this, p0, p1, p2, p3, method) (t29 *)m10906_gshared((t2213 *)__this, (t29 *)p0, (t53 *)p1, (t67 *)p2, (t29 *)p3, method)
#define m11810(__this, p0, method) (void)m10908_gshared((t2213 *)__this, (t29 *)p0, method)
