﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t960;
struct t7;
struct t200;

 void m7116 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5187 (t29 * __this, t7* p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7117 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7118 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7119 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7120 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7121 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7122 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7123 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7124 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7125 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t200* m7126 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7127 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7128 (t29 * __this, t7* p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7129 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
