﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1331;
struct t7;
struct t200;

 void m7224 (t1331 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7225 (t1331 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7226 (t1331 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7227 (t1331 * __this, t200* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
