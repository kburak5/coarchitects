﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3455;
struct t29;
struct t20;
#include "t795.h"

 void m19145 (t3455 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m19146 (t3455 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m19147 (t3455 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19148 (t3455 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m19149 (t3455 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
