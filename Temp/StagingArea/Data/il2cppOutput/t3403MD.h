﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3403;
struct t29;
struct t20;
#include "t1394.h"

 void m18885 (t3403 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18886 (t3403 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18887 (t3403 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18888 (t3403 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18889 (t3403 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
