﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t154;
struct t29;
struct t350;
struct t148;
struct t2456;
struct t2454;
struct t733;
struct t2457;
struct t20;
struct t136;
struct t2459;
struct t722;
#include "t735.h"
#include "t2458.h"
#include "t2460.h"
#include "t725.h"

#include "t2461MD.h"
#define m1541(__this, method) (void)m12834_gshared((t2461 *)__this, method)
#define m12835(__this, p0, method) (void)m12836_gshared((t2461 *)__this, (t29*)p0, method)
#define m12837(__this, p0, method) (void)m12838_gshared((t2461 *)__this, (int32_t)p0, method)
#define m12839(__this, p0, p1, method) (void)m12840_gshared((t2461 *)__this, (t733 *)p0, (t735 )p1, method)
#define m12841(__this, p0, method) (t29 *)m12842_gshared((t2461 *)__this, (t29 *)p0, method)
#define m12843(__this, p0, p1, method) (void)m12844_gshared((t2461 *)__this, (t29 *)p0, (t29 *)p1, method)
#define m12845(__this, p0, p1, method) (void)m12846_gshared((t2461 *)__this, (t29 *)p0, (t29 *)p1, method)
#define m12847(__this, p0, method) (void)m12848_gshared((t2461 *)__this, (t29 *)p0, method)
#define m12849(__this, method) (bool)m12850_gshared((t2461 *)__this, method)
#define m12851(__this, method) (t29 *)m12852_gshared((t2461 *)__this, method)
#define m12853(__this, method) (bool)m12854_gshared((t2461 *)__this, method)
 void m12855 (t154 * __this, t2458  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12856 (t154 * __this, t2458  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
#define m12857(__this, p0, p1, method) (void)m12858_gshared((t2461 *)__this, (t2462*)p0, (int32_t)p1, method)
 bool m12859 (t154 * __this, t2458  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
#define m12860(__this, p0, p1, method) (void)m12861_gshared((t2461 *)__this, (t20 *)p0, (int32_t)p1, method)
#define m12862(__this, method) (t29 *)m12863_gshared((t2461 *)__this, method)
#define m12864(__this, method) (t29*)m12865_gshared((t2461 *)__this, method)
#define m12866(__this, method) (t29 *)m12867_gshared((t2461 *)__this, method)
#define m12868(__this, method) (int32_t)m12869_gshared((t2461 *)__this, method)
#define m12870(__this, p0, method) (t350 *)m12871_gshared((t2461 *)__this, (t29 *)p0, method)
#define m12872(__this, p0, p1, method) (void)m12873_gshared((t2461 *)__this, (t29 *)p0, (t29 *)p1, method)
#define m12874(__this, p0, p1, method) (void)m12875_gshared((t2461 *)__this, (int32_t)p0, (t29*)p1, method)
#define m12876(__this, p0, method) (void)m12877_gshared((t2461 *)__this, (int32_t)p0, method)
#define m12878(__this, p0, p1, method) (void)m12879_gshared((t2461 *)__this, (t20 *)p0, (int32_t)p1, method)
 t2458  m12880 (t29 * __this, t148 * p0, t350 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
#define m12881(__this, p0, p1, method) (t350 *)m12882_gshared((t29 *)__this, (t29 *)p0, (t29 *)p1, method)
#define m12883(__this, p0, p1, method) (void)m12884_gshared((t2461 *)__this, (t2462*)p0, (int32_t)p1, method)
#define m12885(__this, method) (void)m12886_gshared((t2461 *)__this, method)
#define m1544(__this, p0, p1, method) (void)m12887_gshared((t2461 *)__this, (t29 *)p0, (t29 *)p1, method)
#define m12888(__this, method) (void)m12889_gshared((t2461 *)__this, method)
#define m12890(__this, p0, method) (bool)m12891_gshared((t2461 *)__this, (t29 *)p0, method)
#define m12892(__this, p0, method) (bool)m12893_gshared((t2461 *)__this, (t29 *)p0, method)
#define m12894(__this, p0, p1, method) (void)m12895_gshared((t2461 *)__this, (t733 *)p0, (t735 )p1, method)
#define m12896(__this, p0, method) (void)m12897_gshared((t2461 *)__this, (t29 *)p0, method)
#define m12898(__this, p0, method) (bool)m12899_gshared((t2461 *)__this, (t29 *)p0, method)
#define m1542(__this, p0, p1, method) (bool)m12900_gshared((t2461 *)__this, (t29 *)p0, (t29 **)p1, method)
#define m12901(__this, method) (t2456 *)m12902_gshared((t2461 *)__this, method)
#define m12903(__this, p0, method) (t148 *)m12904_gshared((t2461 *)__this, (t29 *)p0, method)
#define m12905(__this, p0, method) (t350 *)m12906_gshared((t2461 *)__this, (t29 *)p0, method)
 bool m12907 (t154 * __this, t2458  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2460  m12908 (t154 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
#define m12909(__this, p0, p1, method) (t725 )m12910_gshared((t29 *)__this, (t29 *)p0, (t29 *)p1, method)
