﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t929;
struct t7;
struct t42;
struct t316;
struct t1142;
struct t1338;
struct t1337;
#include "t35.h"

 void m7459 (t929 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7460 (t929 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7461 (t929 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7462 (t929 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7463 (t929 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7464 (t929 * __this, t42 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m7465 (t929 * __this, t42 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t35 m7466 (t929 * __this, t7* p0, int32_t* p1, t1142 ** p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7467 (t929 * __this, t7* p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m4035 (t929 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7468 (t929 * __this, t1142 * p0, t7* p1, bool p2, bool p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7469 (t929 * __this, t7* p0, bool p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7470 (t29 * __this, t929 * p0, t1338 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1338 * m7471 (t929 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7472 (t929 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t929 * m7473 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1142 * m7474 (t929 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1337* m7475 (t929 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1337* m7476 (t929 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t929 * m7477 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
