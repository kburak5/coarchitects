﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t748;
struct t7;
struct t893;
struct t733;
struct t29;
#include "t735.h"
#include "t897.h"
#include "t898.h"
#include "t899.h"

 void m3871 (t748 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3872 (t748 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3873 (t748 * __this, t7* p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3874 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3875 (t748 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3876 (t748 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3877 (t748 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3878 (t748 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3879 (t748 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3880 (t748 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3881 (t748 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3882 (t748 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3883 (t748 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3884 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3885 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3886 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3887 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3888 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3889 (t748 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3890 (t748 * __this, t748 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3891 (t748 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3892 (t748 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3893 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3894 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3895 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3896 (t29 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3897 (t748 * __this, t7** p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3898 (t748 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3899 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3900 (t29 * __this, t7* p0, bool p1, bool p2, bool p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3901 (t748 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3902 (t748 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3903 (t29 * __this, t7* p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3904 (t748 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3905 (t748 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3906 (t748 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3907 (t748 * __this, int32_t p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3908 (t748 * __this, int32_t p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3909 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3910 (t29 * __this, t7* p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m3911 (t29 * __this, t7* p0, int32_t* p1, uint16_t* p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3912 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3913 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3914 (t748 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3915 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t893 * m3916 (t748 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3917 (t748 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3918 (t29 * __this, t748 * p0, t748 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
