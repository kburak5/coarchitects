﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t557;
struct t42;
struct t537;
#include "t1149.h"

 void m7554 (t557 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7555 (t557 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7556 (t557 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t557 * m7557 (t557 * __this, t537* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t537* m7558 (t557 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7559 (t557 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7560 (t557 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7561 (t557 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
