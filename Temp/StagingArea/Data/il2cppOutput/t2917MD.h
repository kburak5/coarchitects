﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2917;
struct t29;
struct t7;
struct t66;
struct t67;
#include "t35.h"
#include "t2911.h"

 void m15996 (t2917 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2911  m15997 (t2917 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15998 (t2917 * __this, t7* p0, int32_t p1, t67 * p2, t29 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2911  m15999 (t2917 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
