﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3493;
struct t29;
struct t20;
#include "t1112.h"

 void m19410 (t3493 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m19411 (t3493 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m19412 (t3493 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19413 (t3493 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m19414 (t3493 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
