﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2577;
struct t29;
struct t20;
#include "t2562.h"

 void m13879 (t2577 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m13880 (t2577 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13881 (t2577 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13882 (t2577 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2562  m13883 (t2577 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
