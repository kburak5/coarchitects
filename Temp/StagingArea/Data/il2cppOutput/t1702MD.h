﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1702;
struct t1702_marshaled;

void t1702_marshal(const t1702& unmarshaled, t1702_marshaled& marshaled);
void t1702_marshal_back(const t1702_marshaled& marshaled, t1702& unmarshaled);
void t1702_marshal_cleanup(t1702_marshaled& marshaled);
