﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1126;
struct t29;
struct t42;
struct t1094;
struct t841;
struct t7;
struct t1125;
#include "t465.h"
#include "t1126.h"
#include "t923.h"

 void m5696 (t1126 * __this, int32_t p0, int32_t p1, int32_t p2, bool p3, uint8_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5697 (t1126 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5698 (t1126 * __this, uint32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5699 (t1126 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5700 (t1126 * __this, uint64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5701 (t1126 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5702 (t1126 * __this, double p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5703 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m5704 (t1126 * __this, t42 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5705 (t1126 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m5706 (t1126 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m5707 (t1126 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m5708 (t1126 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m5709 (t1126 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m5710 (t1126 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m5711 (t1126 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5712 (t1126 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m5713 (t1126 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int8_t m5714 (t1126 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m5715 (t1126 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m5716 (t1126 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m5717 (t1126 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m5718 (t1126 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t841* m5719 (t29 * __this, t1126  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m5720 (t29 * __this, t1126  p0, t1126  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m5721 (t29 * __this, t1126  p0, t1126  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5722 (t1126 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m5723 (t29 * __this, t1126  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m5724 (t29 * __this, t1126  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5725 (t29 * __this, t1126  p0, t1126  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5726 (t1126 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5727 (t1126 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m5728 (t29 * __this, t1126  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m5729 (t29 * __this, t1126  p0, t1126  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m5730 (t29 * __this, t1126  p0, t1126  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5731 (t29 * __this, t1126  p0, t1126  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5732 (t1126 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5733 (t1126 * __this, t1126  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5734 (t1126 * __this, t1126  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m5735 (t29 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5736 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5737 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5738 (t29 * __this, t7* p0, int32_t p1, t1125 * p2, int32_t* p3, bool* p4, bool* p5, int32_t* p6, bool p7, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m5739 (t29 * __this, t7* p0, int32_t p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5740 (t29 * __this, t7* p0, int32_t p1, t29 * p2, t1126 * p3, bool p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5741 (t1126 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5742 (t1126 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5743 (t1126 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5744 (t29 * __this, t1126 * p0, uint64_t* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5745 (t29 * __this, t1126 * p0, int64_t* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5746 (t29 * __this, t1126 * p0, t1126 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5747 (t29 * __this, t1126 * p0, t7* p1, uint32_t p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5748 (t29 * __this, t1126 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m5749 (t29 * __this, t1126 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5750 (t29 * __this, t1126 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5751 (t29 * __this, t1126 * p0, t1126 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5752 (t29 * __this, t1126 * p0, t1126 * p1, t1126 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5753 (t29 * __this, t1126 * p0, t1126 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m5754 (t29 * __this, t1126  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m5755 (t29 * __this, t1126  p0, t1126  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m5756 (t29 * __this, t1126  p0, t1126  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m5757 (t29 * __this, t1126  p0, t1126  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m5758 (t29 * __this, t1126  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int8_t m5759 (t29 * __this, t1126  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m5760 (t29 * __this, t1126  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m5761 (t29 * __this, t1126  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5762 (t29 * __this, t1126  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m5763 (t29 * __this, t1126  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m5764 (t29 * __this, t1126  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m5765 (t29 * __this, t1126  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m5766 (t29 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m5767 (t29 * __this, int8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m5768 (t29 * __this, int16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m5769 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m5770 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m5771 (t29 * __this, uint32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m5772 (t29 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m5773 (t29 * __this, uint64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m5774 (t29 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m5775 (t29 * __this, double p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m5776 (t29 * __this, t1126  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m5777 (t29 * __this, t1126  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5778 (t29 * __this, t1126  p0, t1126  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5779 (t29 * __this, t1126  p0, t1126  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5780 (t29 * __this, t1126  p0, t1126  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5781 (t29 * __this, t1126  p0, t1126  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
