﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3242;
struct t29;
struct t20;
#include "t970.h"

 void m18006 (t3242 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18007 (t3242 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18008 (t3242 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18009 (t3242 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18010 (t3242 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
