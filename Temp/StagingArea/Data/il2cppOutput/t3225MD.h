﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3225;
struct t29;
struct t20;
#include "t842.h"

 void m17925 (t3225 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17926 (t3225 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17927 (t3225 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17928 (t3225 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17929 (t3225 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
