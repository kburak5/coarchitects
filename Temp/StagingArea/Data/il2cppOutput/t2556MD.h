﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2556;
struct t29;
struct t20;
#include "t165.h"

 void m13711 (t2556 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m13712 (t2556 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13713 (t2556 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13714 (t2556 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m13715 (t2556 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
