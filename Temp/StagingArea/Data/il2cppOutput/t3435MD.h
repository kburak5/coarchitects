﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3435;
struct t29;
struct t20;
#include "t1492.h"

 void m19045 (t3435 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m19046 (t3435 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m19047 (t3435 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19048 (t3435 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m19049 (t3435 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
