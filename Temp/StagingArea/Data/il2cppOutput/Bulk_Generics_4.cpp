﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t5191_TI;

#include "t44.h"
#include "t21.h"

#include "t20.h"

// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.IBeginDragHandler>
extern MethodInfo m27024_MI;
extern MethodInfo m27025_MI;
static PropertyInfo t5191____Item_PropertyInfo = 
{
	&t5191_TI, "Item", &m27024_MI, &m27025_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5191_PIs[] =
{
	&t5191____Item_PropertyInfo,
	NULL
};
extern Il2CppType t93_0_0_0;
extern Il2CppType t93_0_0_0;
static ParameterInfo t5191_m27026_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t93_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27026_GM;
MethodInfo m27026_MI = 
{
	"IndexOf", NULL, &t5191_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5191_m27026_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27026_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t93_0_0_0;
static ParameterInfo t5191_m27027_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t93_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27027_GM;
MethodInfo m27027_MI = 
{
	"Insert", NULL, &t5191_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5191_m27027_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27027_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5191_m27028_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27028_GM;
MethodInfo m27028_MI = 
{
	"RemoveAt", NULL, &t5191_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5191_m27028_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27028_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5191_m27024_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t93_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27024_GM;
MethodInfo m27024_MI = 
{
	"get_Item", NULL, &t5191_TI, &t93_0_0_0, RuntimeInvoker_t29_t44, t5191_m27024_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27024_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t93_0_0_0;
static ParameterInfo t5191_m27025_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t93_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27025_GM;
MethodInfo m27025_MI = 
{
	"set_Item", NULL, &t5191_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5191_m27025_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27025_GM};
static MethodInfo* t5191_MIs[] =
{
	&m27026_MI,
	&m27027_MI,
	&m27028_MI,
	&m27024_MI,
	&m27025_MI,
	NULL
};
extern TypeInfo t603_TI;
extern TypeInfo t5190_TI;
extern TypeInfo t5192_TI;
static TypeInfo* t5191_ITIs[] = 
{
	&t603_TI,
	&t5190_TI,
	&t5192_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5191_0_0_0;
extern Il2CppType t5191_1_0_0;
struct t5191;
extern Il2CppGenericClass t5191_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5191_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5191_MIs, t5191_PIs, NULL, NULL, NULL, NULL, NULL, &t5191_TI, t5191_ITIs, NULL, &t1908__CustomAttributeCache, &t5191_TI, &t5191_0_0_0, &t5191_1_0_0, NULL, &t5191_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5193_TI;

#include "t40.h"
#include "UnityEngine.UI_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>
extern MethodInfo m27029_MI;
static PropertyInfo t5193____Count_PropertyInfo = 
{
	&t5193_TI, "Count", &m27029_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27030_MI;
static PropertyInfo t5193____IsReadOnly_PropertyInfo = 
{
	&t5193_TI, "IsReadOnly", &m27030_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5193_PIs[] =
{
	&t5193____Count_PropertyInfo,
	&t5193____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27029_GM;
MethodInfo m27029_MI = 
{
	"get_Count", NULL, &t5193_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27029_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27030_GM;
MethodInfo m27030_MI = 
{
	"get_IsReadOnly", NULL, &t5193_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27030_GM};
extern Il2CppType t92_0_0_0;
extern Il2CppType t92_0_0_0;
static ParameterInfo t5193_m27031_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t92_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27031_GM;
MethodInfo m27031_MI = 
{
	"Add", NULL, &t5193_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5193_m27031_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27031_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27032_GM;
MethodInfo m27032_MI = 
{
	"Clear", NULL, &t5193_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27032_GM};
extern Il2CppType t92_0_0_0;
static ParameterInfo t5193_m27033_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t92_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27033_GM;
MethodInfo m27033_MI = 
{
	"Contains", NULL, &t5193_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5193_m27033_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27033_GM};
extern Il2CppType t3808_0_0_0;
extern Il2CppType t3808_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5193_m27034_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3808_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27034_GM;
MethodInfo m27034_MI = 
{
	"CopyTo", NULL, &t5193_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5193_m27034_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27034_GM};
extern Il2CppType t92_0_0_0;
static ParameterInfo t5193_m27035_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t92_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27035_GM;
MethodInfo m27035_MI = 
{
	"Remove", NULL, &t5193_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5193_m27035_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27035_GM};
static MethodInfo* t5193_MIs[] =
{
	&m27029_MI,
	&m27030_MI,
	&m27031_MI,
	&m27032_MI,
	&m27033_MI,
	&m27034_MI,
	&m27035_MI,
	NULL
};
extern TypeInfo t5195_TI;
static TypeInfo* t5193_ITIs[] = 
{
	&t603_TI,
	&t5195_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5193_0_0_0;
extern Il2CppType t5193_1_0_0;
struct t5193;
extern Il2CppGenericClass t5193_GC;
TypeInfo t5193_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5193_MIs, t5193_PIs, NULL, NULL, NULL, NULL, NULL, &t5193_TI, t5193_ITIs, NULL, &EmptyCustomAttributesCache, &t5193_TI, &t5193_0_0_0, &t5193_1_0_0, NULL, &t5193_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>
extern Il2CppType t4052_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27036_GM;
MethodInfo m27036_MI = 
{
	"GetEnumerator", NULL, &t5195_TI, &t4052_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27036_GM};
static MethodInfo* t5195_MIs[] =
{
	&m27036_MI,
	NULL
};
static TypeInfo* t5195_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5195_0_0_0;
extern Il2CppType t5195_1_0_0;
struct t5195;
extern Il2CppGenericClass t5195_GC;
TypeInfo t5195_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5195_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5195_TI, t5195_ITIs, NULL, &EmptyCustomAttributesCache, &t5195_TI, &t5195_0_0_0, &t5195_1_0_0, NULL, &t5195_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4052_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>
extern MethodInfo m27037_MI;
static PropertyInfo t4052____Current_PropertyInfo = 
{
	&t4052_TI, "Current", &m27037_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4052_PIs[] =
{
	&t4052____Current_PropertyInfo,
	NULL
};
extern Il2CppType t92_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27037_GM;
MethodInfo m27037_MI = 
{
	"get_Current", NULL, &t4052_TI, &t92_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27037_GM};
static MethodInfo* t4052_MIs[] =
{
	&m27037_MI,
	NULL
};
extern TypeInfo t136_TI;
extern TypeInfo t324_TI;
static TypeInfo* t4052_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4052_0_0_0;
extern Il2CppType t4052_1_0_0;
struct t4052;
extern Il2CppGenericClass t4052_GC;
TypeInfo t4052_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4052_MIs, t4052_PIs, NULL, NULL, NULL, NULL, NULL, &t4052_TI, t4052_ITIs, NULL, &EmptyCustomAttributesCache, &t4052_TI, &t4052_0_0_0, &t4052_1_0_0, NULL, &t4052_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2285.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2285_TI;
#include "t2285MD.h"

#include "t29.h"
#include "t7.h"
#include "t914.h"
extern TypeInfo t92_TI;
extern TypeInfo t914_TI;
#include "t914MD.h"
#include "t20MD.h"
extern MethodInfo m11569_MI;
extern MethodInfo m3964_MI;
extern MethodInfo m3969_MI;
extern MethodInfo m20185_MI;
struct t20;
#include "t915.h"
struct t20;
 t29 * m19490_gshared (t20 * __this, int32_t p0, MethodInfo* method);
#define m19490(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
#define m20185(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>
extern Il2CppType t20_0_0_1;
FieldInfo t2285_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2285_TI, offsetof(t2285, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2285_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2285_TI, offsetof(t2285, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2285_FIs[] =
{
	&t2285_f0_FieldInfo,
	&t2285_f1_FieldInfo,
	NULL
};
extern MethodInfo m11566_MI;
static PropertyInfo t2285____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2285_TI, "System.Collections.IEnumerator.Current", &m11566_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2285____Current_PropertyInfo = 
{
	&t2285_TI, "Current", &m11569_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2285_PIs[] =
{
	&t2285____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2285____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
static ParameterInfo t2285_m11565_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11565_GM;
MethodInfo m11565_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2285_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2285_m11565_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11565_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11566_GM;
MethodInfo m11566_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2285_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11566_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11567_GM;
MethodInfo m11567_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2285_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11567_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11568_GM;
MethodInfo m11568_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2285_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11568_GM};
extern Il2CppType t92_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11569_GM;
MethodInfo m11569_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2285_TI, &t92_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11569_GM};
static MethodInfo* t2285_MIs[] =
{
	&m11565_MI,
	&m11566_MI,
	&m11567_MI,
	&m11568_MI,
	&m11569_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
extern MethodInfo m11568_MI;
extern MethodInfo m11567_MI;
static MethodInfo* t2285_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m11566_MI,
	&m11568_MI,
	&m11567_MI,
	&m11569_MI,
};
static TypeInfo* t2285_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4052_TI,
};
static Il2CppInterfaceOffsetPair t2285_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4052_TI, 7},
};
extern TypeInfo t92_TI;
static Il2CppRGCTXData t2285_RGCTXData[3] = 
{
	&m11569_MI/* Method Usage */,
	&t92_TI/* Class Usage */,
	&m20185_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2285_0_0_0;
extern Il2CppType t2285_1_0_0;
extern TypeInfo t110_TI;
extern Il2CppGenericClass t2285_GC;
extern TypeInfo t20_TI;
TypeInfo t2285_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2285_MIs, t2285_PIs, t2285_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2285_TI, t2285_ITIs, t2285_VT, &EmptyCustomAttributesCache, &t2285_TI, &t2285_0_0_0, &t2285_1_0_0, t2285_IOs, &t2285_GC, NULL, NULL, NULL, t2285_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2285)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5194_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>
extern MethodInfo m27038_MI;
extern MethodInfo m27039_MI;
static PropertyInfo t5194____Item_PropertyInfo = 
{
	&t5194_TI, "Item", &m27038_MI, &m27039_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5194_PIs[] =
{
	&t5194____Item_PropertyInfo,
	NULL
};
extern Il2CppType t92_0_0_0;
static ParameterInfo t5194_m27040_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t92_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27040_GM;
MethodInfo m27040_MI = 
{
	"IndexOf", NULL, &t5194_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5194_m27040_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27040_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t92_0_0_0;
static ParameterInfo t5194_m27041_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t92_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27041_GM;
MethodInfo m27041_MI = 
{
	"Insert", NULL, &t5194_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5194_m27041_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27041_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5194_m27042_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27042_GM;
MethodInfo m27042_MI = 
{
	"RemoveAt", NULL, &t5194_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5194_m27042_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27042_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5194_m27038_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t92_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27038_GM;
MethodInfo m27038_MI = 
{
	"get_Item", NULL, &t5194_TI, &t92_0_0_0, RuntimeInvoker_t29_t44, t5194_m27038_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27038_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t92_0_0_0;
static ParameterInfo t5194_m27039_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t92_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27039_GM;
MethodInfo m27039_MI = 
{
	"set_Item", NULL, &t5194_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5194_m27039_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27039_GM};
static MethodInfo* t5194_MIs[] =
{
	&m27040_MI,
	&m27041_MI,
	&m27042_MI,
	&m27038_MI,
	&m27039_MI,
	NULL
};
static TypeInfo* t5194_ITIs[] = 
{
	&t603_TI,
	&t5193_TI,
	&t5195_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5194_0_0_0;
extern Il2CppType t5194_1_0_0;
struct t5194;
extern Il2CppGenericClass t5194_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5194_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5194_MIs, t5194_PIs, NULL, NULL, NULL, NULL, NULL, &t5194_TI, t5194_ITIs, NULL, &t1908__CustomAttributeCache, &t5194_TI, &t5194_0_0_0, &t5194_1_0_0, NULL, &t5194_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5196_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDragHandler>
extern MethodInfo m27043_MI;
static PropertyInfo t5196____Count_PropertyInfo = 
{
	&t5196_TI, "Count", &m27043_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27044_MI;
static PropertyInfo t5196____IsReadOnly_PropertyInfo = 
{
	&t5196_TI, "IsReadOnly", &m27044_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5196_PIs[] =
{
	&t5196____Count_PropertyInfo,
	&t5196____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27043_GM;
MethodInfo m27043_MI = 
{
	"get_Count", NULL, &t5196_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27043_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27044_GM;
MethodInfo m27044_MI = 
{
	"get_IsReadOnly", NULL, &t5196_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27044_GM};
extern Il2CppType t94_0_0_0;
extern Il2CppType t94_0_0_0;
static ParameterInfo t5196_m27045_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t94_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27045_GM;
MethodInfo m27045_MI = 
{
	"Add", NULL, &t5196_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5196_m27045_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27045_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27046_GM;
MethodInfo m27046_MI = 
{
	"Clear", NULL, &t5196_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27046_GM};
extern Il2CppType t94_0_0_0;
static ParameterInfo t5196_m27047_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t94_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27047_GM;
MethodInfo m27047_MI = 
{
	"Contains", NULL, &t5196_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5196_m27047_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27047_GM};
extern Il2CppType t3809_0_0_0;
extern Il2CppType t3809_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5196_m27048_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3809_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27048_GM;
MethodInfo m27048_MI = 
{
	"CopyTo", NULL, &t5196_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5196_m27048_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27048_GM};
extern Il2CppType t94_0_0_0;
static ParameterInfo t5196_m27049_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t94_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27049_GM;
MethodInfo m27049_MI = 
{
	"Remove", NULL, &t5196_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5196_m27049_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27049_GM};
static MethodInfo* t5196_MIs[] =
{
	&m27043_MI,
	&m27044_MI,
	&m27045_MI,
	&m27046_MI,
	&m27047_MI,
	&m27048_MI,
	&m27049_MI,
	NULL
};
extern TypeInfo t5198_TI;
static TypeInfo* t5196_ITIs[] = 
{
	&t603_TI,
	&t5198_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5196_0_0_0;
extern Il2CppType t5196_1_0_0;
struct t5196;
extern Il2CppGenericClass t5196_GC;
TypeInfo t5196_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5196_MIs, t5196_PIs, NULL, NULL, NULL, NULL, NULL, &t5196_TI, t5196_ITIs, NULL, &EmptyCustomAttributesCache, &t5196_TI, &t5196_0_0_0, &t5196_1_0_0, NULL, &t5196_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IDragHandler>
extern Il2CppType t4054_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27050_GM;
MethodInfo m27050_MI = 
{
	"GetEnumerator", NULL, &t5198_TI, &t4054_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27050_GM};
static MethodInfo* t5198_MIs[] =
{
	&m27050_MI,
	NULL
};
static TypeInfo* t5198_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5198_0_0_0;
extern Il2CppType t5198_1_0_0;
struct t5198;
extern Il2CppGenericClass t5198_GC;
TypeInfo t5198_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5198_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5198_TI, t5198_ITIs, NULL, &EmptyCustomAttributesCache, &t5198_TI, &t5198_0_0_0, &t5198_1_0_0, NULL, &t5198_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4054_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IDragHandler>
extern MethodInfo m27051_MI;
static PropertyInfo t4054____Current_PropertyInfo = 
{
	&t4054_TI, "Current", &m27051_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4054_PIs[] =
{
	&t4054____Current_PropertyInfo,
	NULL
};
extern Il2CppType t94_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27051_GM;
MethodInfo m27051_MI = 
{
	"get_Current", NULL, &t4054_TI, &t94_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27051_GM};
static MethodInfo* t4054_MIs[] =
{
	&m27051_MI,
	NULL
};
static TypeInfo* t4054_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4054_0_0_0;
extern Il2CppType t4054_1_0_0;
struct t4054;
extern Il2CppGenericClass t4054_GC;
TypeInfo t4054_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4054_MIs, t4054_PIs, NULL, NULL, NULL, NULL, NULL, &t4054_TI, t4054_ITIs, NULL, &EmptyCustomAttributesCache, &t4054_TI, &t4054_0_0_0, &t4054_1_0_0, NULL, &t4054_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2286.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2286_TI;
#include "t2286MD.h"

extern TypeInfo t94_TI;
extern MethodInfo m11574_MI;
extern MethodInfo m20196_MI;
struct t20;
#define m20196(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDragHandler>
extern Il2CppType t20_0_0_1;
FieldInfo t2286_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2286_TI, offsetof(t2286, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2286_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2286_TI, offsetof(t2286, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2286_FIs[] =
{
	&t2286_f0_FieldInfo,
	&t2286_f1_FieldInfo,
	NULL
};
extern MethodInfo m11571_MI;
static PropertyInfo t2286____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2286_TI, "System.Collections.IEnumerator.Current", &m11571_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2286____Current_PropertyInfo = 
{
	&t2286_TI, "Current", &m11574_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2286_PIs[] =
{
	&t2286____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2286____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2286_m11570_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11570_GM;
MethodInfo m11570_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2286_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2286_m11570_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11570_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11571_GM;
MethodInfo m11571_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2286_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11571_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11572_GM;
MethodInfo m11572_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2286_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11572_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11573_GM;
MethodInfo m11573_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2286_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11573_GM};
extern Il2CppType t94_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11574_GM;
MethodInfo m11574_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2286_TI, &t94_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11574_GM};
static MethodInfo* t2286_MIs[] =
{
	&m11570_MI,
	&m11571_MI,
	&m11572_MI,
	&m11573_MI,
	&m11574_MI,
	NULL
};
extern MethodInfo m11573_MI;
extern MethodInfo m11572_MI;
static MethodInfo* t2286_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m11571_MI,
	&m11573_MI,
	&m11572_MI,
	&m11574_MI,
};
static TypeInfo* t2286_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4054_TI,
};
static Il2CppInterfaceOffsetPair t2286_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4054_TI, 7},
};
extern TypeInfo t94_TI;
static Il2CppRGCTXData t2286_RGCTXData[3] = 
{
	&m11574_MI/* Method Usage */,
	&t94_TI/* Class Usage */,
	&m20196_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2286_0_0_0;
extern Il2CppType t2286_1_0_0;
extern Il2CppGenericClass t2286_GC;
TypeInfo t2286_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2286_MIs, t2286_PIs, t2286_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2286_TI, t2286_ITIs, t2286_VT, &EmptyCustomAttributesCache, &t2286_TI, &t2286_0_0_0, &t2286_1_0_0, t2286_IOs, &t2286_GC, NULL, NULL, NULL, t2286_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2286)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5197_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.IDragHandler>
extern MethodInfo m27052_MI;
extern MethodInfo m27053_MI;
static PropertyInfo t5197____Item_PropertyInfo = 
{
	&t5197_TI, "Item", &m27052_MI, &m27053_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5197_PIs[] =
{
	&t5197____Item_PropertyInfo,
	NULL
};
extern Il2CppType t94_0_0_0;
static ParameterInfo t5197_m27054_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t94_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27054_GM;
MethodInfo m27054_MI = 
{
	"IndexOf", NULL, &t5197_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5197_m27054_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27054_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t94_0_0_0;
static ParameterInfo t5197_m27055_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t94_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27055_GM;
MethodInfo m27055_MI = 
{
	"Insert", NULL, &t5197_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5197_m27055_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27055_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5197_m27056_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27056_GM;
MethodInfo m27056_MI = 
{
	"RemoveAt", NULL, &t5197_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5197_m27056_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27056_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5197_m27052_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t94_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27052_GM;
MethodInfo m27052_MI = 
{
	"get_Item", NULL, &t5197_TI, &t94_0_0_0, RuntimeInvoker_t29_t44, t5197_m27052_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27052_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t94_0_0_0;
static ParameterInfo t5197_m27053_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t94_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27053_GM;
MethodInfo m27053_MI = 
{
	"set_Item", NULL, &t5197_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5197_m27053_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27053_GM};
static MethodInfo* t5197_MIs[] =
{
	&m27054_MI,
	&m27055_MI,
	&m27056_MI,
	&m27052_MI,
	&m27053_MI,
	NULL
};
static TypeInfo* t5197_ITIs[] = 
{
	&t603_TI,
	&t5196_TI,
	&t5198_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5197_0_0_0;
extern Il2CppType t5197_1_0_0;
struct t5197;
extern Il2CppGenericClass t5197_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5197_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5197_MIs, t5197_PIs, NULL, NULL, NULL, NULL, NULL, &t5197_TI, t5197_ITIs, NULL, &t1908__CustomAttributeCache, &t5197_TI, &t5197_0_0_0, &t5197_1_0_0, NULL, &t5197_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5199_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IEndDragHandler>
extern MethodInfo m27057_MI;
static PropertyInfo t5199____Count_PropertyInfo = 
{
	&t5199_TI, "Count", &m27057_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27058_MI;
static PropertyInfo t5199____IsReadOnly_PropertyInfo = 
{
	&t5199_TI, "IsReadOnly", &m27058_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5199_PIs[] =
{
	&t5199____Count_PropertyInfo,
	&t5199____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27057_GM;
MethodInfo m27057_MI = 
{
	"get_Count", NULL, &t5199_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27057_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27058_GM;
MethodInfo m27058_MI = 
{
	"get_IsReadOnly", NULL, &t5199_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27058_GM};
extern Il2CppType t95_0_0_0;
extern Il2CppType t95_0_0_0;
static ParameterInfo t5199_m27059_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t95_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27059_GM;
MethodInfo m27059_MI = 
{
	"Add", NULL, &t5199_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5199_m27059_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27059_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27060_GM;
MethodInfo m27060_MI = 
{
	"Clear", NULL, &t5199_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27060_GM};
extern Il2CppType t95_0_0_0;
static ParameterInfo t5199_m27061_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t95_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27061_GM;
MethodInfo m27061_MI = 
{
	"Contains", NULL, &t5199_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5199_m27061_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27061_GM};
extern Il2CppType t3810_0_0_0;
extern Il2CppType t3810_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5199_m27062_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3810_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27062_GM;
MethodInfo m27062_MI = 
{
	"CopyTo", NULL, &t5199_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5199_m27062_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27062_GM};
extern Il2CppType t95_0_0_0;
static ParameterInfo t5199_m27063_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t95_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27063_GM;
MethodInfo m27063_MI = 
{
	"Remove", NULL, &t5199_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5199_m27063_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27063_GM};
static MethodInfo* t5199_MIs[] =
{
	&m27057_MI,
	&m27058_MI,
	&m27059_MI,
	&m27060_MI,
	&m27061_MI,
	&m27062_MI,
	&m27063_MI,
	NULL
};
extern TypeInfo t5201_TI;
static TypeInfo* t5199_ITIs[] = 
{
	&t603_TI,
	&t5201_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5199_0_0_0;
extern Il2CppType t5199_1_0_0;
struct t5199;
extern Il2CppGenericClass t5199_GC;
TypeInfo t5199_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5199_MIs, t5199_PIs, NULL, NULL, NULL, NULL, NULL, &t5199_TI, t5199_ITIs, NULL, &EmptyCustomAttributesCache, &t5199_TI, &t5199_0_0_0, &t5199_1_0_0, NULL, &t5199_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IEndDragHandler>
extern Il2CppType t4056_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27064_GM;
MethodInfo m27064_MI = 
{
	"GetEnumerator", NULL, &t5201_TI, &t4056_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27064_GM};
static MethodInfo* t5201_MIs[] =
{
	&m27064_MI,
	NULL
};
static TypeInfo* t5201_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5201_0_0_0;
extern Il2CppType t5201_1_0_0;
struct t5201;
extern Il2CppGenericClass t5201_GC;
TypeInfo t5201_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5201_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5201_TI, t5201_ITIs, NULL, &EmptyCustomAttributesCache, &t5201_TI, &t5201_0_0_0, &t5201_1_0_0, NULL, &t5201_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4056_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IEndDragHandler>
extern MethodInfo m27065_MI;
static PropertyInfo t4056____Current_PropertyInfo = 
{
	&t4056_TI, "Current", &m27065_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4056_PIs[] =
{
	&t4056____Current_PropertyInfo,
	NULL
};
extern Il2CppType t95_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27065_GM;
MethodInfo m27065_MI = 
{
	"get_Current", NULL, &t4056_TI, &t95_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27065_GM};
static MethodInfo* t4056_MIs[] =
{
	&m27065_MI,
	NULL
};
static TypeInfo* t4056_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4056_0_0_0;
extern Il2CppType t4056_1_0_0;
struct t4056;
extern Il2CppGenericClass t4056_GC;
TypeInfo t4056_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4056_MIs, t4056_PIs, NULL, NULL, NULL, NULL, NULL, &t4056_TI, t4056_ITIs, NULL, &EmptyCustomAttributesCache, &t4056_TI, &t4056_0_0_0, &t4056_1_0_0, NULL, &t4056_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2287.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2287_TI;
#include "t2287MD.h"

extern TypeInfo t95_TI;
extern MethodInfo m11579_MI;
extern MethodInfo m20207_MI;
struct t20;
#define m20207(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IEndDragHandler>
extern Il2CppType t20_0_0_1;
FieldInfo t2287_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2287_TI, offsetof(t2287, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2287_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2287_TI, offsetof(t2287, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2287_FIs[] =
{
	&t2287_f0_FieldInfo,
	&t2287_f1_FieldInfo,
	NULL
};
extern MethodInfo m11576_MI;
static PropertyInfo t2287____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2287_TI, "System.Collections.IEnumerator.Current", &m11576_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2287____Current_PropertyInfo = 
{
	&t2287_TI, "Current", &m11579_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2287_PIs[] =
{
	&t2287____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2287____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2287_m11575_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11575_GM;
MethodInfo m11575_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2287_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2287_m11575_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11575_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11576_GM;
MethodInfo m11576_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2287_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11576_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11577_GM;
MethodInfo m11577_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2287_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11577_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11578_GM;
MethodInfo m11578_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2287_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11578_GM};
extern Il2CppType t95_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11579_GM;
MethodInfo m11579_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2287_TI, &t95_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11579_GM};
static MethodInfo* t2287_MIs[] =
{
	&m11575_MI,
	&m11576_MI,
	&m11577_MI,
	&m11578_MI,
	&m11579_MI,
	NULL
};
extern MethodInfo m11578_MI;
extern MethodInfo m11577_MI;
static MethodInfo* t2287_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m11576_MI,
	&m11578_MI,
	&m11577_MI,
	&m11579_MI,
};
static TypeInfo* t2287_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4056_TI,
};
static Il2CppInterfaceOffsetPair t2287_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4056_TI, 7},
};
extern TypeInfo t95_TI;
static Il2CppRGCTXData t2287_RGCTXData[3] = 
{
	&m11579_MI/* Method Usage */,
	&t95_TI/* Class Usage */,
	&m20207_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2287_0_0_0;
extern Il2CppType t2287_1_0_0;
extern Il2CppGenericClass t2287_GC;
TypeInfo t2287_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2287_MIs, t2287_PIs, t2287_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2287_TI, t2287_ITIs, t2287_VT, &EmptyCustomAttributesCache, &t2287_TI, &t2287_0_0_0, &t2287_1_0_0, t2287_IOs, &t2287_GC, NULL, NULL, NULL, t2287_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2287)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5200_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.IEndDragHandler>
extern MethodInfo m27066_MI;
extern MethodInfo m27067_MI;
static PropertyInfo t5200____Item_PropertyInfo = 
{
	&t5200_TI, "Item", &m27066_MI, &m27067_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5200_PIs[] =
{
	&t5200____Item_PropertyInfo,
	NULL
};
extern Il2CppType t95_0_0_0;
static ParameterInfo t5200_m27068_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t95_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27068_GM;
MethodInfo m27068_MI = 
{
	"IndexOf", NULL, &t5200_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5200_m27068_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27068_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t95_0_0_0;
static ParameterInfo t5200_m27069_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t95_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27069_GM;
MethodInfo m27069_MI = 
{
	"Insert", NULL, &t5200_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5200_m27069_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27069_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5200_m27070_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27070_GM;
MethodInfo m27070_MI = 
{
	"RemoveAt", NULL, &t5200_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5200_m27070_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27070_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5200_m27066_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t95_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27066_GM;
MethodInfo m27066_MI = 
{
	"get_Item", NULL, &t5200_TI, &t95_0_0_0, RuntimeInvoker_t29_t44, t5200_m27066_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27066_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t95_0_0_0;
static ParameterInfo t5200_m27067_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t95_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27067_GM;
MethodInfo m27067_MI = 
{
	"set_Item", NULL, &t5200_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5200_m27067_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27067_GM};
static MethodInfo* t5200_MIs[] =
{
	&m27068_MI,
	&m27069_MI,
	&m27070_MI,
	&m27066_MI,
	&m27067_MI,
	NULL
};
static TypeInfo* t5200_ITIs[] = 
{
	&t603_TI,
	&t5199_TI,
	&t5201_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5200_0_0_0;
extern Il2CppType t5200_1_0_0;
struct t5200;
extern Il2CppGenericClass t5200_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5200_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5200_MIs, t5200_PIs, NULL, NULL, NULL, NULL, NULL, &t5200_TI, t5200_ITIs, NULL, &t1908__CustomAttributeCache, &t5200_TI, &t5200_0_0_0, &t5200_1_0_0, NULL, &t5200_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5202_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDropHandler>
extern MethodInfo m27071_MI;
static PropertyInfo t5202____Count_PropertyInfo = 
{
	&t5202_TI, "Count", &m27071_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27072_MI;
static PropertyInfo t5202____IsReadOnly_PropertyInfo = 
{
	&t5202_TI, "IsReadOnly", &m27072_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5202_PIs[] =
{
	&t5202____Count_PropertyInfo,
	&t5202____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27071_GM;
MethodInfo m27071_MI = 
{
	"get_Count", NULL, &t5202_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27071_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27072_GM;
MethodInfo m27072_MI = 
{
	"get_IsReadOnly", NULL, &t5202_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27072_GM};
extern Il2CppType t96_0_0_0;
extern Il2CppType t96_0_0_0;
static ParameterInfo t5202_m27073_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t96_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27073_GM;
MethodInfo m27073_MI = 
{
	"Add", NULL, &t5202_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5202_m27073_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27073_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27074_GM;
MethodInfo m27074_MI = 
{
	"Clear", NULL, &t5202_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27074_GM};
extern Il2CppType t96_0_0_0;
static ParameterInfo t5202_m27075_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t96_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27075_GM;
MethodInfo m27075_MI = 
{
	"Contains", NULL, &t5202_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5202_m27075_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27075_GM};
extern Il2CppType t3811_0_0_0;
extern Il2CppType t3811_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5202_m27076_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3811_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27076_GM;
MethodInfo m27076_MI = 
{
	"CopyTo", NULL, &t5202_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5202_m27076_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27076_GM};
extern Il2CppType t96_0_0_0;
static ParameterInfo t5202_m27077_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t96_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27077_GM;
MethodInfo m27077_MI = 
{
	"Remove", NULL, &t5202_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5202_m27077_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27077_GM};
static MethodInfo* t5202_MIs[] =
{
	&m27071_MI,
	&m27072_MI,
	&m27073_MI,
	&m27074_MI,
	&m27075_MI,
	&m27076_MI,
	&m27077_MI,
	NULL
};
extern TypeInfo t5204_TI;
static TypeInfo* t5202_ITIs[] = 
{
	&t603_TI,
	&t5204_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5202_0_0_0;
extern Il2CppType t5202_1_0_0;
struct t5202;
extern Il2CppGenericClass t5202_GC;
TypeInfo t5202_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5202_MIs, t5202_PIs, NULL, NULL, NULL, NULL, NULL, &t5202_TI, t5202_ITIs, NULL, &EmptyCustomAttributesCache, &t5202_TI, &t5202_0_0_0, &t5202_1_0_0, NULL, &t5202_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IDropHandler>
extern Il2CppType t4058_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27078_GM;
MethodInfo m27078_MI = 
{
	"GetEnumerator", NULL, &t5204_TI, &t4058_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27078_GM};
static MethodInfo* t5204_MIs[] =
{
	&m27078_MI,
	NULL
};
static TypeInfo* t5204_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5204_0_0_0;
extern Il2CppType t5204_1_0_0;
struct t5204;
extern Il2CppGenericClass t5204_GC;
TypeInfo t5204_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5204_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5204_TI, t5204_ITIs, NULL, &EmptyCustomAttributesCache, &t5204_TI, &t5204_0_0_0, &t5204_1_0_0, NULL, &t5204_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4058_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IDropHandler>
extern MethodInfo m27079_MI;
static PropertyInfo t4058____Current_PropertyInfo = 
{
	&t4058_TI, "Current", &m27079_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4058_PIs[] =
{
	&t4058____Current_PropertyInfo,
	NULL
};
extern Il2CppType t96_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27079_GM;
MethodInfo m27079_MI = 
{
	"get_Current", NULL, &t4058_TI, &t96_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27079_GM};
static MethodInfo* t4058_MIs[] =
{
	&m27079_MI,
	NULL
};
static TypeInfo* t4058_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4058_0_0_0;
extern Il2CppType t4058_1_0_0;
struct t4058;
extern Il2CppGenericClass t4058_GC;
TypeInfo t4058_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4058_MIs, t4058_PIs, NULL, NULL, NULL, NULL, NULL, &t4058_TI, t4058_ITIs, NULL, &EmptyCustomAttributesCache, &t4058_TI, &t4058_0_0_0, &t4058_1_0_0, NULL, &t4058_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2288.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2288_TI;
#include "t2288MD.h"

extern TypeInfo t96_TI;
extern MethodInfo m11584_MI;
extern MethodInfo m20218_MI;
struct t20;
#define m20218(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDropHandler>
extern Il2CppType t20_0_0_1;
FieldInfo t2288_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2288_TI, offsetof(t2288, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2288_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2288_TI, offsetof(t2288, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2288_FIs[] =
{
	&t2288_f0_FieldInfo,
	&t2288_f1_FieldInfo,
	NULL
};
extern MethodInfo m11581_MI;
static PropertyInfo t2288____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2288_TI, "System.Collections.IEnumerator.Current", &m11581_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2288____Current_PropertyInfo = 
{
	&t2288_TI, "Current", &m11584_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2288_PIs[] =
{
	&t2288____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2288____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2288_m11580_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11580_GM;
MethodInfo m11580_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2288_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2288_m11580_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11580_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11581_GM;
MethodInfo m11581_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2288_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11581_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11582_GM;
MethodInfo m11582_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2288_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11582_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11583_GM;
MethodInfo m11583_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2288_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11583_GM};
extern Il2CppType t96_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11584_GM;
MethodInfo m11584_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2288_TI, &t96_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11584_GM};
static MethodInfo* t2288_MIs[] =
{
	&m11580_MI,
	&m11581_MI,
	&m11582_MI,
	&m11583_MI,
	&m11584_MI,
	NULL
};
extern MethodInfo m11583_MI;
extern MethodInfo m11582_MI;
static MethodInfo* t2288_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m11581_MI,
	&m11583_MI,
	&m11582_MI,
	&m11584_MI,
};
static TypeInfo* t2288_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4058_TI,
};
static Il2CppInterfaceOffsetPair t2288_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4058_TI, 7},
};
extern TypeInfo t96_TI;
static Il2CppRGCTXData t2288_RGCTXData[3] = 
{
	&m11584_MI/* Method Usage */,
	&t96_TI/* Class Usage */,
	&m20218_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2288_0_0_0;
extern Il2CppType t2288_1_0_0;
extern Il2CppGenericClass t2288_GC;
TypeInfo t2288_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2288_MIs, t2288_PIs, t2288_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2288_TI, t2288_ITIs, t2288_VT, &EmptyCustomAttributesCache, &t2288_TI, &t2288_0_0_0, &t2288_1_0_0, t2288_IOs, &t2288_GC, NULL, NULL, NULL, t2288_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2288)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5203_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.IDropHandler>
extern MethodInfo m27080_MI;
extern MethodInfo m27081_MI;
static PropertyInfo t5203____Item_PropertyInfo = 
{
	&t5203_TI, "Item", &m27080_MI, &m27081_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5203_PIs[] =
{
	&t5203____Item_PropertyInfo,
	NULL
};
extern Il2CppType t96_0_0_0;
static ParameterInfo t5203_m27082_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t96_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27082_GM;
MethodInfo m27082_MI = 
{
	"IndexOf", NULL, &t5203_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5203_m27082_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27082_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t96_0_0_0;
static ParameterInfo t5203_m27083_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t96_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27083_GM;
MethodInfo m27083_MI = 
{
	"Insert", NULL, &t5203_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5203_m27083_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27083_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5203_m27084_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27084_GM;
MethodInfo m27084_MI = 
{
	"RemoveAt", NULL, &t5203_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5203_m27084_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27084_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5203_m27080_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t96_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27080_GM;
MethodInfo m27080_MI = 
{
	"get_Item", NULL, &t5203_TI, &t96_0_0_0, RuntimeInvoker_t29_t44, t5203_m27080_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27080_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t96_0_0_0;
static ParameterInfo t5203_m27081_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t96_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27081_GM;
MethodInfo m27081_MI = 
{
	"set_Item", NULL, &t5203_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5203_m27081_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27081_GM};
static MethodInfo* t5203_MIs[] =
{
	&m27082_MI,
	&m27083_MI,
	&m27084_MI,
	&m27080_MI,
	&m27081_MI,
	NULL
};
static TypeInfo* t5203_ITIs[] = 
{
	&t603_TI,
	&t5202_TI,
	&t5204_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5203_0_0_0;
extern Il2CppType t5203_1_0_0;
struct t5203;
extern Il2CppGenericClass t5203_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5203_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5203_MIs, t5203_PIs, NULL, NULL, NULL, NULL, NULL, &t5203_TI, t5203_ITIs, NULL, &t1908__CustomAttributeCache, &t5203_TI, &t5203_0_0_0, &t5203_1_0_0, NULL, &t5203_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5205_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IScrollHandler>
extern MethodInfo m27085_MI;
static PropertyInfo t5205____Count_PropertyInfo = 
{
	&t5205_TI, "Count", &m27085_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27086_MI;
static PropertyInfo t5205____IsReadOnly_PropertyInfo = 
{
	&t5205_TI, "IsReadOnly", &m27086_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5205_PIs[] =
{
	&t5205____Count_PropertyInfo,
	&t5205____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27085_GM;
MethodInfo m27085_MI = 
{
	"get_Count", NULL, &t5205_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27085_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27086_GM;
MethodInfo m27086_MI = 
{
	"get_IsReadOnly", NULL, &t5205_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27086_GM};
extern Il2CppType t97_0_0_0;
extern Il2CppType t97_0_0_0;
static ParameterInfo t5205_m27087_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t97_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27087_GM;
MethodInfo m27087_MI = 
{
	"Add", NULL, &t5205_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5205_m27087_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27087_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27088_GM;
MethodInfo m27088_MI = 
{
	"Clear", NULL, &t5205_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27088_GM};
extern Il2CppType t97_0_0_0;
static ParameterInfo t5205_m27089_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t97_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27089_GM;
MethodInfo m27089_MI = 
{
	"Contains", NULL, &t5205_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5205_m27089_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27089_GM};
extern Il2CppType t3812_0_0_0;
extern Il2CppType t3812_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5205_m27090_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3812_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27090_GM;
MethodInfo m27090_MI = 
{
	"CopyTo", NULL, &t5205_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5205_m27090_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27090_GM};
extern Il2CppType t97_0_0_0;
static ParameterInfo t5205_m27091_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t97_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27091_GM;
MethodInfo m27091_MI = 
{
	"Remove", NULL, &t5205_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5205_m27091_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27091_GM};
static MethodInfo* t5205_MIs[] =
{
	&m27085_MI,
	&m27086_MI,
	&m27087_MI,
	&m27088_MI,
	&m27089_MI,
	&m27090_MI,
	&m27091_MI,
	NULL
};
extern TypeInfo t5207_TI;
static TypeInfo* t5205_ITIs[] = 
{
	&t603_TI,
	&t5207_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5205_0_0_0;
extern Il2CppType t5205_1_0_0;
struct t5205;
extern Il2CppGenericClass t5205_GC;
TypeInfo t5205_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5205_MIs, t5205_PIs, NULL, NULL, NULL, NULL, NULL, &t5205_TI, t5205_ITIs, NULL, &EmptyCustomAttributesCache, &t5205_TI, &t5205_0_0_0, &t5205_1_0_0, NULL, &t5205_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IScrollHandler>
extern Il2CppType t4060_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27092_GM;
MethodInfo m27092_MI = 
{
	"GetEnumerator", NULL, &t5207_TI, &t4060_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27092_GM};
static MethodInfo* t5207_MIs[] =
{
	&m27092_MI,
	NULL
};
static TypeInfo* t5207_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5207_0_0_0;
extern Il2CppType t5207_1_0_0;
struct t5207;
extern Il2CppGenericClass t5207_GC;
TypeInfo t5207_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5207_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5207_TI, t5207_ITIs, NULL, &EmptyCustomAttributesCache, &t5207_TI, &t5207_0_0_0, &t5207_1_0_0, NULL, &t5207_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4060_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IScrollHandler>
extern MethodInfo m27093_MI;
static PropertyInfo t4060____Current_PropertyInfo = 
{
	&t4060_TI, "Current", &m27093_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4060_PIs[] =
{
	&t4060____Current_PropertyInfo,
	NULL
};
extern Il2CppType t97_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27093_GM;
MethodInfo m27093_MI = 
{
	"get_Current", NULL, &t4060_TI, &t97_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27093_GM};
static MethodInfo* t4060_MIs[] =
{
	&m27093_MI,
	NULL
};
static TypeInfo* t4060_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4060_0_0_0;
extern Il2CppType t4060_1_0_0;
struct t4060;
extern Il2CppGenericClass t4060_GC;
TypeInfo t4060_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4060_MIs, t4060_PIs, NULL, NULL, NULL, NULL, NULL, &t4060_TI, t4060_ITIs, NULL, &EmptyCustomAttributesCache, &t4060_TI, &t4060_0_0_0, &t4060_1_0_0, NULL, &t4060_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2289.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2289_TI;
#include "t2289MD.h"

extern TypeInfo t97_TI;
extern MethodInfo m11589_MI;
extern MethodInfo m20229_MI;
struct t20;
#define m20229(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IScrollHandler>
extern Il2CppType t20_0_0_1;
FieldInfo t2289_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2289_TI, offsetof(t2289, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2289_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2289_TI, offsetof(t2289, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2289_FIs[] =
{
	&t2289_f0_FieldInfo,
	&t2289_f1_FieldInfo,
	NULL
};
extern MethodInfo m11586_MI;
static PropertyInfo t2289____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2289_TI, "System.Collections.IEnumerator.Current", &m11586_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2289____Current_PropertyInfo = 
{
	&t2289_TI, "Current", &m11589_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2289_PIs[] =
{
	&t2289____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2289____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2289_m11585_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11585_GM;
MethodInfo m11585_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2289_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2289_m11585_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11585_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11586_GM;
MethodInfo m11586_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2289_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11586_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11587_GM;
MethodInfo m11587_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2289_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11587_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11588_GM;
MethodInfo m11588_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2289_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11588_GM};
extern Il2CppType t97_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11589_GM;
MethodInfo m11589_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2289_TI, &t97_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11589_GM};
static MethodInfo* t2289_MIs[] =
{
	&m11585_MI,
	&m11586_MI,
	&m11587_MI,
	&m11588_MI,
	&m11589_MI,
	NULL
};
extern MethodInfo m11588_MI;
extern MethodInfo m11587_MI;
static MethodInfo* t2289_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m11586_MI,
	&m11588_MI,
	&m11587_MI,
	&m11589_MI,
};
static TypeInfo* t2289_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4060_TI,
};
static Il2CppInterfaceOffsetPair t2289_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4060_TI, 7},
};
extern TypeInfo t97_TI;
static Il2CppRGCTXData t2289_RGCTXData[3] = 
{
	&m11589_MI/* Method Usage */,
	&t97_TI/* Class Usage */,
	&m20229_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2289_0_0_0;
extern Il2CppType t2289_1_0_0;
extern Il2CppGenericClass t2289_GC;
TypeInfo t2289_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2289_MIs, t2289_PIs, t2289_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2289_TI, t2289_ITIs, t2289_VT, &EmptyCustomAttributesCache, &t2289_TI, &t2289_0_0_0, &t2289_1_0_0, t2289_IOs, &t2289_GC, NULL, NULL, NULL, t2289_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2289)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5206_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.IScrollHandler>
extern MethodInfo m27094_MI;
extern MethodInfo m27095_MI;
static PropertyInfo t5206____Item_PropertyInfo = 
{
	&t5206_TI, "Item", &m27094_MI, &m27095_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5206_PIs[] =
{
	&t5206____Item_PropertyInfo,
	NULL
};
extern Il2CppType t97_0_0_0;
static ParameterInfo t5206_m27096_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t97_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27096_GM;
MethodInfo m27096_MI = 
{
	"IndexOf", NULL, &t5206_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5206_m27096_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27096_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t97_0_0_0;
static ParameterInfo t5206_m27097_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t97_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27097_GM;
MethodInfo m27097_MI = 
{
	"Insert", NULL, &t5206_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5206_m27097_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27097_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5206_m27098_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27098_GM;
MethodInfo m27098_MI = 
{
	"RemoveAt", NULL, &t5206_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5206_m27098_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27098_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5206_m27094_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t97_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27094_GM;
MethodInfo m27094_MI = 
{
	"get_Item", NULL, &t5206_TI, &t97_0_0_0, RuntimeInvoker_t29_t44, t5206_m27094_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27094_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t97_0_0_0;
static ParameterInfo t5206_m27095_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t97_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27095_GM;
MethodInfo m27095_MI = 
{
	"set_Item", NULL, &t5206_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5206_m27095_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27095_GM};
static MethodInfo* t5206_MIs[] =
{
	&m27096_MI,
	&m27097_MI,
	&m27098_MI,
	&m27094_MI,
	&m27095_MI,
	NULL
};
static TypeInfo* t5206_ITIs[] = 
{
	&t603_TI,
	&t5205_TI,
	&t5207_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5206_0_0_0;
extern Il2CppType t5206_1_0_0;
struct t5206;
extern Il2CppGenericClass t5206_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5206_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5206_MIs, t5206_PIs, NULL, NULL, NULL, NULL, NULL, &t5206_TI, t5206_ITIs, NULL, &t1908__CustomAttributeCache, &t5206_TI, &t5206_0_0_0, &t5206_1_0_0, NULL, &t5206_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5208_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IUpdateSelectedHandler>
extern MethodInfo m27099_MI;
static PropertyInfo t5208____Count_PropertyInfo = 
{
	&t5208_TI, "Count", &m27099_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27100_MI;
static PropertyInfo t5208____IsReadOnly_PropertyInfo = 
{
	&t5208_TI, "IsReadOnly", &m27100_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5208_PIs[] =
{
	&t5208____Count_PropertyInfo,
	&t5208____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27099_GM;
MethodInfo m27099_MI = 
{
	"get_Count", NULL, &t5208_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27099_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27100_GM;
MethodInfo m27100_MI = 
{
	"get_IsReadOnly", NULL, &t5208_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27100_GM};
extern Il2CppType t98_0_0_0;
extern Il2CppType t98_0_0_0;
static ParameterInfo t5208_m27101_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t98_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27101_GM;
MethodInfo m27101_MI = 
{
	"Add", NULL, &t5208_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5208_m27101_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27101_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27102_GM;
MethodInfo m27102_MI = 
{
	"Clear", NULL, &t5208_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27102_GM};
extern Il2CppType t98_0_0_0;
static ParameterInfo t5208_m27103_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t98_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27103_GM;
MethodInfo m27103_MI = 
{
	"Contains", NULL, &t5208_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5208_m27103_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27103_GM};
extern Il2CppType t3813_0_0_0;
extern Il2CppType t3813_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5208_m27104_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3813_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27104_GM;
MethodInfo m27104_MI = 
{
	"CopyTo", NULL, &t5208_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5208_m27104_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27104_GM};
extern Il2CppType t98_0_0_0;
static ParameterInfo t5208_m27105_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t98_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27105_GM;
MethodInfo m27105_MI = 
{
	"Remove", NULL, &t5208_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5208_m27105_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27105_GM};
static MethodInfo* t5208_MIs[] =
{
	&m27099_MI,
	&m27100_MI,
	&m27101_MI,
	&m27102_MI,
	&m27103_MI,
	&m27104_MI,
	&m27105_MI,
	NULL
};
extern TypeInfo t5210_TI;
static TypeInfo* t5208_ITIs[] = 
{
	&t603_TI,
	&t5210_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5208_0_0_0;
extern Il2CppType t5208_1_0_0;
struct t5208;
extern Il2CppGenericClass t5208_GC;
TypeInfo t5208_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5208_MIs, t5208_PIs, NULL, NULL, NULL, NULL, NULL, &t5208_TI, t5208_ITIs, NULL, &EmptyCustomAttributesCache, &t5208_TI, &t5208_0_0_0, &t5208_1_0_0, NULL, &t5208_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IUpdateSelectedHandler>
extern Il2CppType t4062_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27106_GM;
MethodInfo m27106_MI = 
{
	"GetEnumerator", NULL, &t5210_TI, &t4062_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27106_GM};
static MethodInfo* t5210_MIs[] =
{
	&m27106_MI,
	NULL
};
static TypeInfo* t5210_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5210_0_0_0;
extern Il2CppType t5210_1_0_0;
struct t5210;
extern Il2CppGenericClass t5210_GC;
TypeInfo t5210_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5210_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5210_TI, t5210_ITIs, NULL, &EmptyCustomAttributesCache, &t5210_TI, &t5210_0_0_0, &t5210_1_0_0, NULL, &t5210_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4062_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IUpdateSelectedHandler>
extern MethodInfo m27107_MI;
static PropertyInfo t4062____Current_PropertyInfo = 
{
	&t4062_TI, "Current", &m27107_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4062_PIs[] =
{
	&t4062____Current_PropertyInfo,
	NULL
};
extern Il2CppType t98_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27107_GM;
MethodInfo m27107_MI = 
{
	"get_Current", NULL, &t4062_TI, &t98_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27107_GM};
static MethodInfo* t4062_MIs[] =
{
	&m27107_MI,
	NULL
};
static TypeInfo* t4062_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4062_0_0_0;
extern Il2CppType t4062_1_0_0;
struct t4062;
extern Il2CppGenericClass t4062_GC;
TypeInfo t4062_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4062_MIs, t4062_PIs, NULL, NULL, NULL, NULL, NULL, &t4062_TI, t4062_ITIs, NULL, &EmptyCustomAttributesCache, &t4062_TI, &t4062_0_0_0, &t4062_1_0_0, NULL, &t4062_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2290.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2290_TI;
#include "t2290MD.h"

extern TypeInfo t98_TI;
extern MethodInfo m11594_MI;
extern MethodInfo m20240_MI;
struct t20;
#define m20240(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IUpdateSelectedHandler>
extern Il2CppType t20_0_0_1;
FieldInfo t2290_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2290_TI, offsetof(t2290, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2290_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2290_TI, offsetof(t2290, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2290_FIs[] =
{
	&t2290_f0_FieldInfo,
	&t2290_f1_FieldInfo,
	NULL
};
extern MethodInfo m11591_MI;
static PropertyInfo t2290____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2290_TI, "System.Collections.IEnumerator.Current", &m11591_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2290____Current_PropertyInfo = 
{
	&t2290_TI, "Current", &m11594_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2290_PIs[] =
{
	&t2290____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2290____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2290_m11590_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11590_GM;
MethodInfo m11590_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2290_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2290_m11590_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11590_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11591_GM;
MethodInfo m11591_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2290_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11591_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11592_GM;
MethodInfo m11592_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2290_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11592_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11593_GM;
MethodInfo m11593_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2290_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11593_GM};
extern Il2CppType t98_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11594_GM;
MethodInfo m11594_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2290_TI, &t98_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11594_GM};
static MethodInfo* t2290_MIs[] =
{
	&m11590_MI,
	&m11591_MI,
	&m11592_MI,
	&m11593_MI,
	&m11594_MI,
	NULL
};
extern MethodInfo m11593_MI;
extern MethodInfo m11592_MI;
static MethodInfo* t2290_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m11591_MI,
	&m11593_MI,
	&m11592_MI,
	&m11594_MI,
};
static TypeInfo* t2290_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4062_TI,
};
static Il2CppInterfaceOffsetPair t2290_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4062_TI, 7},
};
extern TypeInfo t98_TI;
static Il2CppRGCTXData t2290_RGCTXData[3] = 
{
	&m11594_MI/* Method Usage */,
	&t98_TI/* Class Usage */,
	&m20240_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2290_0_0_0;
extern Il2CppType t2290_1_0_0;
extern Il2CppGenericClass t2290_GC;
TypeInfo t2290_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2290_MIs, t2290_PIs, t2290_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2290_TI, t2290_ITIs, t2290_VT, &EmptyCustomAttributesCache, &t2290_TI, &t2290_0_0_0, &t2290_1_0_0, t2290_IOs, &t2290_GC, NULL, NULL, NULL, t2290_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2290)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5209_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.IUpdateSelectedHandler>
extern MethodInfo m27108_MI;
extern MethodInfo m27109_MI;
static PropertyInfo t5209____Item_PropertyInfo = 
{
	&t5209_TI, "Item", &m27108_MI, &m27109_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5209_PIs[] =
{
	&t5209____Item_PropertyInfo,
	NULL
};
extern Il2CppType t98_0_0_0;
static ParameterInfo t5209_m27110_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t98_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27110_GM;
MethodInfo m27110_MI = 
{
	"IndexOf", NULL, &t5209_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5209_m27110_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27110_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t98_0_0_0;
static ParameterInfo t5209_m27111_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t98_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27111_GM;
MethodInfo m27111_MI = 
{
	"Insert", NULL, &t5209_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5209_m27111_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27111_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5209_m27112_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27112_GM;
MethodInfo m27112_MI = 
{
	"RemoveAt", NULL, &t5209_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5209_m27112_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27112_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5209_m27108_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t98_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27108_GM;
MethodInfo m27108_MI = 
{
	"get_Item", NULL, &t5209_TI, &t98_0_0_0, RuntimeInvoker_t29_t44, t5209_m27108_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27108_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t98_0_0_0;
static ParameterInfo t5209_m27109_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t98_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27109_GM;
MethodInfo m27109_MI = 
{
	"set_Item", NULL, &t5209_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5209_m27109_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27109_GM};
static MethodInfo* t5209_MIs[] =
{
	&m27110_MI,
	&m27111_MI,
	&m27112_MI,
	&m27108_MI,
	&m27109_MI,
	NULL
};
static TypeInfo* t5209_ITIs[] = 
{
	&t603_TI,
	&t5208_TI,
	&t5210_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5209_0_0_0;
extern Il2CppType t5209_1_0_0;
struct t5209;
extern Il2CppGenericClass t5209_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5209_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5209_MIs, t5209_PIs, NULL, NULL, NULL, NULL, NULL, &t5209_TI, t5209_ITIs, NULL, &t1908__CustomAttributeCache, &t5209_TI, &t5209_0_0_0, &t5209_1_0_0, NULL, &t5209_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5211_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ISelectHandler>
extern MethodInfo m27113_MI;
static PropertyInfo t5211____Count_PropertyInfo = 
{
	&t5211_TI, "Count", &m27113_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27114_MI;
static PropertyInfo t5211____IsReadOnly_PropertyInfo = 
{
	&t5211_TI, "IsReadOnly", &m27114_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5211_PIs[] =
{
	&t5211____Count_PropertyInfo,
	&t5211____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27113_GM;
MethodInfo m27113_MI = 
{
	"get_Count", NULL, &t5211_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27113_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27114_GM;
MethodInfo m27114_MI = 
{
	"get_IsReadOnly", NULL, &t5211_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27114_GM};
extern Il2CppType t99_0_0_0;
extern Il2CppType t99_0_0_0;
static ParameterInfo t5211_m27115_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t99_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27115_GM;
MethodInfo m27115_MI = 
{
	"Add", NULL, &t5211_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5211_m27115_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27115_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27116_GM;
MethodInfo m27116_MI = 
{
	"Clear", NULL, &t5211_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27116_GM};
extern Il2CppType t99_0_0_0;
static ParameterInfo t5211_m27117_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t99_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27117_GM;
MethodInfo m27117_MI = 
{
	"Contains", NULL, &t5211_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5211_m27117_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27117_GM};
extern Il2CppType t3814_0_0_0;
extern Il2CppType t3814_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5211_m27118_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3814_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27118_GM;
MethodInfo m27118_MI = 
{
	"CopyTo", NULL, &t5211_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5211_m27118_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27118_GM};
extern Il2CppType t99_0_0_0;
static ParameterInfo t5211_m27119_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t99_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27119_GM;
MethodInfo m27119_MI = 
{
	"Remove", NULL, &t5211_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5211_m27119_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27119_GM};
static MethodInfo* t5211_MIs[] =
{
	&m27113_MI,
	&m27114_MI,
	&m27115_MI,
	&m27116_MI,
	&m27117_MI,
	&m27118_MI,
	&m27119_MI,
	NULL
};
extern TypeInfo t5213_TI;
static TypeInfo* t5211_ITIs[] = 
{
	&t603_TI,
	&t5213_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5211_0_0_0;
extern Il2CppType t5211_1_0_0;
struct t5211;
extern Il2CppGenericClass t5211_GC;
TypeInfo t5211_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5211_MIs, t5211_PIs, NULL, NULL, NULL, NULL, NULL, &t5211_TI, t5211_ITIs, NULL, &EmptyCustomAttributesCache, &t5211_TI, &t5211_0_0_0, &t5211_1_0_0, NULL, &t5211_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.ISelectHandler>
extern Il2CppType t4064_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27120_GM;
MethodInfo m27120_MI = 
{
	"GetEnumerator", NULL, &t5213_TI, &t4064_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27120_GM};
static MethodInfo* t5213_MIs[] =
{
	&m27120_MI,
	NULL
};
static TypeInfo* t5213_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5213_0_0_0;
extern Il2CppType t5213_1_0_0;
struct t5213;
extern Il2CppGenericClass t5213_GC;
TypeInfo t5213_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5213_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5213_TI, t5213_ITIs, NULL, &EmptyCustomAttributesCache, &t5213_TI, &t5213_0_0_0, &t5213_1_0_0, NULL, &t5213_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4064_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.ISelectHandler>
extern MethodInfo m27121_MI;
static PropertyInfo t4064____Current_PropertyInfo = 
{
	&t4064_TI, "Current", &m27121_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4064_PIs[] =
{
	&t4064____Current_PropertyInfo,
	NULL
};
extern Il2CppType t99_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27121_GM;
MethodInfo m27121_MI = 
{
	"get_Current", NULL, &t4064_TI, &t99_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27121_GM};
static MethodInfo* t4064_MIs[] =
{
	&m27121_MI,
	NULL
};
static TypeInfo* t4064_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4064_0_0_0;
extern Il2CppType t4064_1_0_0;
struct t4064;
extern Il2CppGenericClass t4064_GC;
TypeInfo t4064_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4064_MIs, t4064_PIs, NULL, NULL, NULL, NULL, NULL, &t4064_TI, t4064_ITIs, NULL, &EmptyCustomAttributesCache, &t4064_TI, &t4064_0_0_0, &t4064_1_0_0, NULL, &t4064_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2291.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2291_TI;
#include "t2291MD.h"

extern TypeInfo t99_TI;
extern MethodInfo m11599_MI;
extern MethodInfo m20251_MI;
struct t20;
#define m20251(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ISelectHandler>
extern Il2CppType t20_0_0_1;
FieldInfo t2291_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2291_TI, offsetof(t2291, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2291_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2291_TI, offsetof(t2291, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2291_FIs[] =
{
	&t2291_f0_FieldInfo,
	&t2291_f1_FieldInfo,
	NULL
};
extern MethodInfo m11596_MI;
static PropertyInfo t2291____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2291_TI, "System.Collections.IEnumerator.Current", &m11596_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2291____Current_PropertyInfo = 
{
	&t2291_TI, "Current", &m11599_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2291_PIs[] =
{
	&t2291____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2291____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2291_m11595_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11595_GM;
MethodInfo m11595_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2291_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2291_m11595_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11595_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11596_GM;
MethodInfo m11596_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2291_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11596_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11597_GM;
MethodInfo m11597_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2291_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11597_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11598_GM;
MethodInfo m11598_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2291_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11598_GM};
extern Il2CppType t99_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11599_GM;
MethodInfo m11599_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2291_TI, &t99_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11599_GM};
static MethodInfo* t2291_MIs[] =
{
	&m11595_MI,
	&m11596_MI,
	&m11597_MI,
	&m11598_MI,
	&m11599_MI,
	NULL
};
extern MethodInfo m11598_MI;
extern MethodInfo m11597_MI;
static MethodInfo* t2291_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m11596_MI,
	&m11598_MI,
	&m11597_MI,
	&m11599_MI,
};
static TypeInfo* t2291_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4064_TI,
};
static Il2CppInterfaceOffsetPair t2291_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4064_TI, 7},
};
extern TypeInfo t99_TI;
static Il2CppRGCTXData t2291_RGCTXData[3] = 
{
	&m11599_MI/* Method Usage */,
	&t99_TI/* Class Usage */,
	&m20251_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2291_0_0_0;
extern Il2CppType t2291_1_0_0;
extern Il2CppGenericClass t2291_GC;
TypeInfo t2291_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2291_MIs, t2291_PIs, t2291_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2291_TI, t2291_ITIs, t2291_VT, &EmptyCustomAttributesCache, &t2291_TI, &t2291_0_0_0, &t2291_1_0_0, t2291_IOs, &t2291_GC, NULL, NULL, NULL, t2291_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2291)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5212_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.ISelectHandler>
extern MethodInfo m27122_MI;
extern MethodInfo m27123_MI;
static PropertyInfo t5212____Item_PropertyInfo = 
{
	&t5212_TI, "Item", &m27122_MI, &m27123_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5212_PIs[] =
{
	&t5212____Item_PropertyInfo,
	NULL
};
extern Il2CppType t99_0_0_0;
static ParameterInfo t5212_m27124_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t99_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27124_GM;
MethodInfo m27124_MI = 
{
	"IndexOf", NULL, &t5212_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5212_m27124_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27124_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t99_0_0_0;
static ParameterInfo t5212_m27125_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t99_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27125_GM;
MethodInfo m27125_MI = 
{
	"Insert", NULL, &t5212_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5212_m27125_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27125_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5212_m27126_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27126_GM;
MethodInfo m27126_MI = 
{
	"RemoveAt", NULL, &t5212_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5212_m27126_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27126_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5212_m27122_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t99_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27122_GM;
MethodInfo m27122_MI = 
{
	"get_Item", NULL, &t5212_TI, &t99_0_0_0, RuntimeInvoker_t29_t44, t5212_m27122_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27122_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t99_0_0_0;
static ParameterInfo t5212_m27123_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t99_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27123_GM;
MethodInfo m27123_MI = 
{
	"set_Item", NULL, &t5212_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5212_m27123_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27123_GM};
static MethodInfo* t5212_MIs[] =
{
	&m27124_MI,
	&m27125_MI,
	&m27126_MI,
	&m27122_MI,
	&m27123_MI,
	NULL
};
static TypeInfo* t5212_ITIs[] = 
{
	&t603_TI,
	&t5211_TI,
	&t5213_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5212_0_0_0;
extern Il2CppType t5212_1_0_0;
struct t5212;
extern Il2CppGenericClass t5212_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5212_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5212_MIs, t5212_PIs, NULL, NULL, NULL, NULL, NULL, &t5212_TI, t5212_ITIs, NULL, &t1908__CustomAttributeCache, &t5212_TI, &t5212_0_0_0, &t5212_1_0_0, NULL, &t5212_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5214_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDeselectHandler>
extern MethodInfo m27127_MI;
static PropertyInfo t5214____Count_PropertyInfo = 
{
	&t5214_TI, "Count", &m27127_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27128_MI;
static PropertyInfo t5214____IsReadOnly_PropertyInfo = 
{
	&t5214_TI, "IsReadOnly", &m27128_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5214_PIs[] =
{
	&t5214____Count_PropertyInfo,
	&t5214____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27127_GM;
MethodInfo m27127_MI = 
{
	"get_Count", NULL, &t5214_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27127_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27128_GM;
MethodInfo m27128_MI = 
{
	"get_IsReadOnly", NULL, &t5214_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27128_GM};
extern Il2CppType t100_0_0_0;
extern Il2CppType t100_0_0_0;
static ParameterInfo t5214_m27129_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t100_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27129_GM;
MethodInfo m27129_MI = 
{
	"Add", NULL, &t5214_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5214_m27129_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27129_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27130_GM;
MethodInfo m27130_MI = 
{
	"Clear", NULL, &t5214_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27130_GM};
extern Il2CppType t100_0_0_0;
static ParameterInfo t5214_m27131_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t100_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27131_GM;
MethodInfo m27131_MI = 
{
	"Contains", NULL, &t5214_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5214_m27131_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27131_GM};
extern Il2CppType t3815_0_0_0;
extern Il2CppType t3815_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5214_m27132_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3815_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27132_GM;
MethodInfo m27132_MI = 
{
	"CopyTo", NULL, &t5214_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5214_m27132_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27132_GM};
extern Il2CppType t100_0_0_0;
static ParameterInfo t5214_m27133_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t100_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27133_GM;
MethodInfo m27133_MI = 
{
	"Remove", NULL, &t5214_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5214_m27133_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27133_GM};
static MethodInfo* t5214_MIs[] =
{
	&m27127_MI,
	&m27128_MI,
	&m27129_MI,
	&m27130_MI,
	&m27131_MI,
	&m27132_MI,
	&m27133_MI,
	NULL
};
extern TypeInfo t5216_TI;
static TypeInfo* t5214_ITIs[] = 
{
	&t603_TI,
	&t5216_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5214_0_0_0;
extern Il2CppType t5214_1_0_0;
struct t5214;
extern Il2CppGenericClass t5214_GC;
TypeInfo t5214_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5214_MIs, t5214_PIs, NULL, NULL, NULL, NULL, NULL, &t5214_TI, t5214_ITIs, NULL, &EmptyCustomAttributesCache, &t5214_TI, &t5214_0_0_0, &t5214_1_0_0, NULL, &t5214_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IDeselectHandler>
extern Il2CppType t4066_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27134_GM;
MethodInfo m27134_MI = 
{
	"GetEnumerator", NULL, &t5216_TI, &t4066_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27134_GM};
static MethodInfo* t5216_MIs[] =
{
	&m27134_MI,
	NULL
};
static TypeInfo* t5216_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5216_0_0_0;
extern Il2CppType t5216_1_0_0;
struct t5216;
extern Il2CppGenericClass t5216_GC;
TypeInfo t5216_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5216_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5216_TI, t5216_ITIs, NULL, &EmptyCustomAttributesCache, &t5216_TI, &t5216_0_0_0, &t5216_1_0_0, NULL, &t5216_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4066_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IDeselectHandler>
extern MethodInfo m27135_MI;
static PropertyInfo t4066____Current_PropertyInfo = 
{
	&t4066_TI, "Current", &m27135_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4066_PIs[] =
{
	&t4066____Current_PropertyInfo,
	NULL
};
extern Il2CppType t100_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27135_GM;
MethodInfo m27135_MI = 
{
	"get_Current", NULL, &t4066_TI, &t100_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27135_GM};
static MethodInfo* t4066_MIs[] =
{
	&m27135_MI,
	NULL
};
static TypeInfo* t4066_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4066_0_0_0;
extern Il2CppType t4066_1_0_0;
struct t4066;
extern Il2CppGenericClass t4066_GC;
TypeInfo t4066_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4066_MIs, t4066_PIs, NULL, NULL, NULL, NULL, NULL, &t4066_TI, t4066_ITIs, NULL, &EmptyCustomAttributesCache, &t4066_TI, &t4066_0_0_0, &t4066_1_0_0, NULL, &t4066_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2292.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2292_TI;
#include "t2292MD.h"

extern TypeInfo t100_TI;
extern MethodInfo m11604_MI;
extern MethodInfo m20262_MI;
struct t20;
#define m20262(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDeselectHandler>
extern Il2CppType t20_0_0_1;
FieldInfo t2292_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2292_TI, offsetof(t2292, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2292_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2292_TI, offsetof(t2292, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2292_FIs[] =
{
	&t2292_f0_FieldInfo,
	&t2292_f1_FieldInfo,
	NULL
};
extern MethodInfo m11601_MI;
static PropertyInfo t2292____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2292_TI, "System.Collections.IEnumerator.Current", &m11601_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2292____Current_PropertyInfo = 
{
	&t2292_TI, "Current", &m11604_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2292_PIs[] =
{
	&t2292____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2292____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2292_m11600_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11600_GM;
MethodInfo m11600_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2292_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2292_m11600_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11600_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11601_GM;
MethodInfo m11601_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2292_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11601_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11602_GM;
MethodInfo m11602_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2292_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11602_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11603_GM;
MethodInfo m11603_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2292_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11603_GM};
extern Il2CppType t100_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11604_GM;
MethodInfo m11604_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2292_TI, &t100_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11604_GM};
static MethodInfo* t2292_MIs[] =
{
	&m11600_MI,
	&m11601_MI,
	&m11602_MI,
	&m11603_MI,
	&m11604_MI,
	NULL
};
extern MethodInfo m11603_MI;
extern MethodInfo m11602_MI;
static MethodInfo* t2292_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m11601_MI,
	&m11603_MI,
	&m11602_MI,
	&m11604_MI,
};
static TypeInfo* t2292_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4066_TI,
};
static Il2CppInterfaceOffsetPair t2292_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4066_TI, 7},
};
extern TypeInfo t100_TI;
static Il2CppRGCTXData t2292_RGCTXData[3] = 
{
	&m11604_MI/* Method Usage */,
	&t100_TI/* Class Usage */,
	&m20262_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2292_0_0_0;
extern Il2CppType t2292_1_0_0;
extern Il2CppGenericClass t2292_GC;
TypeInfo t2292_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2292_MIs, t2292_PIs, t2292_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2292_TI, t2292_ITIs, t2292_VT, &EmptyCustomAttributesCache, &t2292_TI, &t2292_0_0_0, &t2292_1_0_0, t2292_IOs, &t2292_GC, NULL, NULL, NULL, t2292_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2292)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5215_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.IDeselectHandler>
extern MethodInfo m27136_MI;
extern MethodInfo m27137_MI;
static PropertyInfo t5215____Item_PropertyInfo = 
{
	&t5215_TI, "Item", &m27136_MI, &m27137_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5215_PIs[] =
{
	&t5215____Item_PropertyInfo,
	NULL
};
extern Il2CppType t100_0_0_0;
static ParameterInfo t5215_m27138_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t100_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27138_GM;
MethodInfo m27138_MI = 
{
	"IndexOf", NULL, &t5215_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5215_m27138_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27138_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t100_0_0_0;
static ParameterInfo t5215_m27139_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t100_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27139_GM;
MethodInfo m27139_MI = 
{
	"Insert", NULL, &t5215_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5215_m27139_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27139_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5215_m27140_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27140_GM;
MethodInfo m27140_MI = 
{
	"RemoveAt", NULL, &t5215_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5215_m27140_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27140_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5215_m27136_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t100_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27136_GM;
MethodInfo m27136_MI = 
{
	"get_Item", NULL, &t5215_TI, &t100_0_0_0, RuntimeInvoker_t29_t44, t5215_m27136_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27136_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t100_0_0_0;
static ParameterInfo t5215_m27137_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t100_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27137_GM;
MethodInfo m27137_MI = 
{
	"set_Item", NULL, &t5215_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5215_m27137_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27137_GM};
static MethodInfo* t5215_MIs[] =
{
	&m27138_MI,
	&m27139_MI,
	&m27140_MI,
	&m27136_MI,
	&m27137_MI,
	NULL
};
static TypeInfo* t5215_ITIs[] = 
{
	&t603_TI,
	&t5214_TI,
	&t5216_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5215_0_0_0;
extern Il2CppType t5215_1_0_0;
struct t5215;
extern Il2CppGenericClass t5215_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5215_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5215_MIs, t5215_PIs, NULL, NULL, NULL, NULL, NULL, &t5215_TI, t5215_ITIs, NULL, &t1908__CustomAttributeCache, &t5215_TI, &t5215_0_0_0, &t5215_1_0_0, NULL, &t5215_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5217_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IMoveHandler>
extern MethodInfo m27141_MI;
static PropertyInfo t5217____Count_PropertyInfo = 
{
	&t5217_TI, "Count", &m27141_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27142_MI;
static PropertyInfo t5217____IsReadOnly_PropertyInfo = 
{
	&t5217_TI, "IsReadOnly", &m27142_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5217_PIs[] =
{
	&t5217____Count_PropertyInfo,
	&t5217____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27141_GM;
MethodInfo m27141_MI = 
{
	"get_Count", NULL, &t5217_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27141_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27142_GM;
MethodInfo m27142_MI = 
{
	"get_IsReadOnly", NULL, &t5217_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27142_GM};
extern Il2CppType t101_0_0_0;
extern Il2CppType t101_0_0_0;
static ParameterInfo t5217_m27143_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t101_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27143_GM;
MethodInfo m27143_MI = 
{
	"Add", NULL, &t5217_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5217_m27143_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27143_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27144_GM;
MethodInfo m27144_MI = 
{
	"Clear", NULL, &t5217_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27144_GM};
extern Il2CppType t101_0_0_0;
static ParameterInfo t5217_m27145_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t101_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27145_GM;
MethodInfo m27145_MI = 
{
	"Contains", NULL, &t5217_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5217_m27145_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27145_GM};
extern Il2CppType t3816_0_0_0;
extern Il2CppType t3816_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5217_m27146_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3816_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27146_GM;
MethodInfo m27146_MI = 
{
	"CopyTo", NULL, &t5217_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5217_m27146_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27146_GM};
extern Il2CppType t101_0_0_0;
static ParameterInfo t5217_m27147_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t101_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27147_GM;
MethodInfo m27147_MI = 
{
	"Remove", NULL, &t5217_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5217_m27147_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27147_GM};
static MethodInfo* t5217_MIs[] =
{
	&m27141_MI,
	&m27142_MI,
	&m27143_MI,
	&m27144_MI,
	&m27145_MI,
	&m27146_MI,
	&m27147_MI,
	NULL
};
extern TypeInfo t5219_TI;
static TypeInfo* t5217_ITIs[] = 
{
	&t603_TI,
	&t5219_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5217_0_0_0;
extern Il2CppType t5217_1_0_0;
struct t5217;
extern Il2CppGenericClass t5217_GC;
TypeInfo t5217_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5217_MIs, t5217_PIs, NULL, NULL, NULL, NULL, NULL, &t5217_TI, t5217_ITIs, NULL, &EmptyCustomAttributesCache, &t5217_TI, &t5217_0_0_0, &t5217_1_0_0, NULL, &t5217_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IMoveHandler>
extern Il2CppType t4068_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27148_GM;
MethodInfo m27148_MI = 
{
	"GetEnumerator", NULL, &t5219_TI, &t4068_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27148_GM};
static MethodInfo* t5219_MIs[] =
{
	&m27148_MI,
	NULL
};
static TypeInfo* t5219_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5219_0_0_0;
extern Il2CppType t5219_1_0_0;
struct t5219;
extern Il2CppGenericClass t5219_GC;
TypeInfo t5219_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5219_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5219_TI, t5219_ITIs, NULL, &EmptyCustomAttributesCache, &t5219_TI, &t5219_0_0_0, &t5219_1_0_0, NULL, &t5219_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4068_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IMoveHandler>
extern MethodInfo m27149_MI;
static PropertyInfo t4068____Current_PropertyInfo = 
{
	&t4068_TI, "Current", &m27149_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4068_PIs[] =
{
	&t4068____Current_PropertyInfo,
	NULL
};
extern Il2CppType t101_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27149_GM;
MethodInfo m27149_MI = 
{
	"get_Current", NULL, &t4068_TI, &t101_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27149_GM};
static MethodInfo* t4068_MIs[] =
{
	&m27149_MI,
	NULL
};
static TypeInfo* t4068_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4068_0_0_0;
extern Il2CppType t4068_1_0_0;
struct t4068;
extern Il2CppGenericClass t4068_GC;
TypeInfo t4068_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4068_MIs, t4068_PIs, NULL, NULL, NULL, NULL, NULL, &t4068_TI, t4068_ITIs, NULL, &EmptyCustomAttributesCache, &t4068_TI, &t4068_0_0_0, &t4068_1_0_0, NULL, &t4068_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2293.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2293_TI;
#include "t2293MD.h"

extern TypeInfo t101_TI;
extern MethodInfo m11609_MI;
extern MethodInfo m20273_MI;
struct t20;
#define m20273(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IMoveHandler>
extern Il2CppType t20_0_0_1;
FieldInfo t2293_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2293_TI, offsetof(t2293, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2293_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2293_TI, offsetof(t2293, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2293_FIs[] =
{
	&t2293_f0_FieldInfo,
	&t2293_f1_FieldInfo,
	NULL
};
extern MethodInfo m11606_MI;
static PropertyInfo t2293____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2293_TI, "System.Collections.IEnumerator.Current", &m11606_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2293____Current_PropertyInfo = 
{
	&t2293_TI, "Current", &m11609_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2293_PIs[] =
{
	&t2293____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2293____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2293_m11605_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11605_GM;
MethodInfo m11605_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2293_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2293_m11605_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11605_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11606_GM;
MethodInfo m11606_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2293_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11606_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11607_GM;
MethodInfo m11607_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2293_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11607_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11608_GM;
MethodInfo m11608_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2293_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11608_GM};
extern Il2CppType t101_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11609_GM;
MethodInfo m11609_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2293_TI, &t101_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11609_GM};
static MethodInfo* t2293_MIs[] =
{
	&m11605_MI,
	&m11606_MI,
	&m11607_MI,
	&m11608_MI,
	&m11609_MI,
	NULL
};
extern MethodInfo m11608_MI;
extern MethodInfo m11607_MI;
static MethodInfo* t2293_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m11606_MI,
	&m11608_MI,
	&m11607_MI,
	&m11609_MI,
};
static TypeInfo* t2293_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4068_TI,
};
static Il2CppInterfaceOffsetPair t2293_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4068_TI, 7},
};
extern TypeInfo t101_TI;
static Il2CppRGCTXData t2293_RGCTXData[3] = 
{
	&m11609_MI/* Method Usage */,
	&t101_TI/* Class Usage */,
	&m20273_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2293_0_0_0;
extern Il2CppType t2293_1_0_0;
extern Il2CppGenericClass t2293_GC;
TypeInfo t2293_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2293_MIs, t2293_PIs, t2293_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2293_TI, t2293_ITIs, t2293_VT, &EmptyCustomAttributesCache, &t2293_TI, &t2293_0_0_0, &t2293_1_0_0, t2293_IOs, &t2293_GC, NULL, NULL, NULL, t2293_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2293)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5218_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.IMoveHandler>
extern MethodInfo m27150_MI;
extern MethodInfo m27151_MI;
static PropertyInfo t5218____Item_PropertyInfo = 
{
	&t5218_TI, "Item", &m27150_MI, &m27151_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5218_PIs[] =
{
	&t5218____Item_PropertyInfo,
	NULL
};
extern Il2CppType t101_0_0_0;
static ParameterInfo t5218_m27152_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t101_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27152_GM;
MethodInfo m27152_MI = 
{
	"IndexOf", NULL, &t5218_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5218_m27152_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27152_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t101_0_0_0;
static ParameterInfo t5218_m27153_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t101_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27153_GM;
MethodInfo m27153_MI = 
{
	"Insert", NULL, &t5218_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5218_m27153_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27153_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5218_m27154_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27154_GM;
MethodInfo m27154_MI = 
{
	"RemoveAt", NULL, &t5218_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5218_m27154_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27154_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5218_m27150_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t101_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27150_GM;
MethodInfo m27150_MI = 
{
	"get_Item", NULL, &t5218_TI, &t101_0_0_0, RuntimeInvoker_t29_t44, t5218_m27150_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27150_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t101_0_0_0;
static ParameterInfo t5218_m27151_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t101_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27151_GM;
MethodInfo m27151_MI = 
{
	"set_Item", NULL, &t5218_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5218_m27151_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27151_GM};
static MethodInfo* t5218_MIs[] =
{
	&m27152_MI,
	&m27153_MI,
	&m27154_MI,
	&m27150_MI,
	&m27151_MI,
	NULL
};
static TypeInfo* t5218_ITIs[] = 
{
	&t603_TI,
	&t5217_TI,
	&t5219_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5218_0_0_0;
extern Il2CppType t5218_1_0_0;
struct t5218;
extern Il2CppGenericClass t5218_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5218_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5218_MIs, t5218_PIs, NULL, NULL, NULL, NULL, NULL, &t5218_TI, t5218_ITIs, NULL, &t1908__CustomAttributeCache, &t5218_TI, &t5218_0_0_0, &t5218_1_0_0, NULL, &t5218_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5220_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ISubmitHandler>
extern MethodInfo m27155_MI;
static PropertyInfo t5220____Count_PropertyInfo = 
{
	&t5220_TI, "Count", &m27155_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27156_MI;
static PropertyInfo t5220____IsReadOnly_PropertyInfo = 
{
	&t5220_TI, "IsReadOnly", &m27156_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5220_PIs[] =
{
	&t5220____Count_PropertyInfo,
	&t5220____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27155_GM;
MethodInfo m27155_MI = 
{
	"get_Count", NULL, &t5220_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27155_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27156_GM;
MethodInfo m27156_MI = 
{
	"get_IsReadOnly", NULL, &t5220_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27156_GM};
extern Il2CppType t102_0_0_0;
extern Il2CppType t102_0_0_0;
static ParameterInfo t5220_m27157_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t102_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27157_GM;
MethodInfo m27157_MI = 
{
	"Add", NULL, &t5220_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5220_m27157_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27157_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27158_GM;
MethodInfo m27158_MI = 
{
	"Clear", NULL, &t5220_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27158_GM};
extern Il2CppType t102_0_0_0;
static ParameterInfo t5220_m27159_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t102_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27159_GM;
MethodInfo m27159_MI = 
{
	"Contains", NULL, &t5220_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5220_m27159_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27159_GM};
extern Il2CppType t3817_0_0_0;
extern Il2CppType t3817_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5220_m27160_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3817_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27160_GM;
MethodInfo m27160_MI = 
{
	"CopyTo", NULL, &t5220_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5220_m27160_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27160_GM};
extern Il2CppType t102_0_0_0;
static ParameterInfo t5220_m27161_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t102_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27161_GM;
MethodInfo m27161_MI = 
{
	"Remove", NULL, &t5220_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5220_m27161_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27161_GM};
static MethodInfo* t5220_MIs[] =
{
	&m27155_MI,
	&m27156_MI,
	&m27157_MI,
	&m27158_MI,
	&m27159_MI,
	&m27160_MI,
	&m27161_MI,
	NULL
};
extern TypeInfo t5222_TI;
static TypeInfo* t5220_ITIs[] = 
{
	&t603_TI,
	&t5222_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5220_0_0_0;
extern Il2CppType t5220_1_0_0;
struct t5220;
extern Il2CppGenericClass t5220_GC;
TypeInfo t5220_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5220_MIs, t5220_PIs, NULL, NULL, NULL, NULL, NULL, &t5220_TI, t5220_ITIs, NULL, &EmptyCustomAttributesCache, &t5220_TI, &t5220_0_0_0, &t5220_1_0_0, NULL, &t5220_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.ISubmitHandler>
extern Il2CppType t4070_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27162_GM;
MethodInfo m27162_MI = 
{
	"GetEnumerator", NULL, &t5222_TI, &t4070_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27162_GM};
static MethodInfo* t5222_MIs[] =
{
	&m27162_MI,
	NULL
};
static TypeInfo* t5222_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5222_0_0_0;
extern Il2CppType t5222_1_0_0;
struct t5222;
extern Il2CppGenericClass t5222_GC;
TypeInfo t5222_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5222_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5222_TI, t5222_ITIs, NULL, &EmptyCustomAttributesCache, &t5222_TI, &t5222_0_0_0, &t5222_1_0_0, NULL, &t5222_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4070_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.ISubmitHandler>
extern MethodInfo m27163_MI;
static PropertyInfo t4070____Current_PropertyInfo = 
{
	&t4070_TI, "Current", &m27163_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4070_PIs[] =
{
	&t4070____Current_PropertyInfo,
	NULL
};
extern Il2CppType t102_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27163_GM;
MethodInfo m27163_MI = 
{
	"get_Current", NULL, &t4070_TI, &t102_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27163_GM};
static MethodInfo* t4070_MIs[] =
{
	&m27163_MI,
	NULL
};
static TypeInfo* t4070_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4070_0_0_0;
extern Il2CppType t4070_1_0_0;
struct t4070;
extern Il2CppGenericClass t4070_GC;
TypeInfo t4070_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4070_MIs, t4070_PIs, NULL, NULL, NULL, NULL, NULL, &t4070_TI, t4070_ITIs, NULL, &EmptyCustomAttributesCache, &t4070_TI, &t4070_0_0_0, &t4070_1_0_0, NULL, &t4070_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2294.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2294_TI;
#include "t2294MD.h"

extern TypeInfo t102_TI;
extern MethodInfo m11614_MI;
extern MethodInfo m20284_MI;
struct t20;
#define m20284(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ISubmitHandler>
extern Il2CppType t20_0_0_1;
FieldInfo t2294_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2294_TI, offsetof(t2294, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2294_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2294_TI, offsetof(t2294, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2294_FIs[] =
{
	&t2294_f0_FieldInfo,
	&t2294_f1_FieldInfo,
	NULL
};
extern MethodInfo m11611_MI;
static PropertyInfo t2294____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2294_TI, "System.Collections.IEnumerator.Current", &m11611_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2294____Current_PropertyInfo = 
{
	&t2294_TI, "Current", &m11614_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2294_PIs[] =
{
	&t2294____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2294____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2294_m11610_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11610_GM;
MethodInfo m11610_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2294_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2294_m11610_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11610_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11611_GM;
MethodInfo m11611_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2294_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11611_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11612_GM;
MethodInfo m11612_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2294_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11612_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11613_GM;
MethodInfo m11613_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2294_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11613_GM};
extern Il2CppType t102_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11614_GM;
MethodInfo m11614_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2294_TI, &t102_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11614_GM};
static MethodInfo* t2294_MIs[] =
{
	&m11610_MI,
	&m11611_MI,
	&m11612_MI,
	&m11613_MI,
	&m11614_MI,
	NULL
};
extern MethodInfo m11613_MI;
extern MethodInfo m11612_MI;
static MethodInfo* t2294_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m11611_MI,
	&m11613_MI,
	&m11612_MI,
	&m11614_MI,
};
static TypeInfo* t2294_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4070_TI,
};
static Il2CppInterfaceOffsetPair t2294_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4070_TI, 7},
};
extern TypeInfo t102_TI;
static Il2CppRGCTXData t2294_RGCTXData[3] = 
{
	&m11614_MI/* Method Usage */,
	&t102_TI/* Class Usage */,
	&m20284_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2294_0_0_0;
extern Il2CppType t2294_1_0_0;
extern Il2CppGenericClass t2294_GC;
TypeInfo t2294_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2294_MIs, t2294_PIs, t2294_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2294_TI, t2294_ITIs, t2294_VT, &EmptyCustomAttributesCache, &t2294_TI, &t2294_0_0_0, &t2294_1_0_0, t2294_IOs, &t2294_GC, NULL, NULL, NULL, t2294_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2294)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5221_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.ISubmitHandler>
extern MethodInfo m27164_MI;
extern MethodInfo m27165_MI;
static PropertyInfo t5221____Item_PropertyInfo = 
{
	&t5221_TI, "Item", &m27164_MI, &m27165_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5221_PIs[] =
{
	&t5221____Item_PropertyInfo,
	NULL
};
extern Il2CppType t102_0_0_0;
static ParameterInfo t5221_m27166_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t102_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27166_GM;
MethodInfo m27166_MI = 
{
	"IndexOf", NULL, &t5221_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5221_m27166_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27166_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t102_0_0_0;
static ParameterInfo t5221_m27167_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t102_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27167_GM;
MethodInfo m27167_MI = 
{
	"Insert", NULL, &t5221_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5221_m27167_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27167_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5221_m27168_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27168_GM;
MethodInfo m27168_MI = 
{
	"RemoveAt", NULL, &t5221_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5221_m27168_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27168_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5221_m27164_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t102_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27164_GM;
MethodInfo m27164_MI = 
{
	"get_Item", NULL, &t5221_TI, &t102_0_0_0, RuntimeInvoker_t29_t44, t5221_m27164_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27164_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t102_0_0_0;
static ParameterInfo t5221_m27165_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t102_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27165_GM;
MethodInfo m27165_MI = 
{
	"set_Item", NULL, &t5221_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5221_m27165_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27165_GM};
static MethodInfo* t5221_MIs[] =
{
	&m27166_MI,
	&m27167_MI,
	&m27168_MI,
	&m27164_MI,
	&m27165_MI,
	NULL
};
static TypeInfo* t5221_ITIs[] = 
{
	&t603_TI,
	&t5220_TI,
	&t5222_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5221_0_0_0;
extern Il2CppType t5221_1_0_0;
struct t5221;
extern Il2CppGenericClass t5221_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5221_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5221_MIs, t5221_PIs, NULL, NULL, NULL, NULL, NULL, &t5221_TI, t5221_ITIs, NULL, &t1908__CustomAttributeCache, &t5221_TI, &t5221_0_0_0, &t5221_1_0_0, NULL, &t5221_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5223_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ICancelHandler>
extern MethodInfo m27169_MI;
static PropertyInfo t5223____Count_PropertyInfo = 
{
	&t5223_TI, "Count", &m27169_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27170_MI;
static PropertyInfo t5223____IsReadOnly_PropertyInfo = 
{
	&t5223_TI, "IsReadOnly", &m27170_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5223_PIs[] =
{
	&t5223____Count_PropertyInfo,
	&t5223____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27169_GM;
MethodInfo m27169_MI = 
{
	"get_Count", NULL, &t5223_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27169_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27170_GM;
MethodInfo m27170_MI = 
{
	"get_IsReadOnly", NULL, &t5223_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27170_GM};
extern Il2CppType t103_0_0_0;
extern Il2CppType t103_0_0_0;
static ParameterInfo t5223_m27171_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t103_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27171_GM;
MethodInfo m27171_MI = 
{
	"Add", NULL, &t5223_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5223_m27171_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27171_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27172_GM;
MethodInfo m27172_MI = 
{
	"Clear", NULL, &t5223_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27172_GM};
extern Il2CppType t103_0_0_0;
static ParameterInfo t5223_m27173_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t103_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27173_GM;
MethodInfo m27173_MI = 
{
	"Contains", NULL, &t5223_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5223_m27173_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27173_GM};
extern Il2CppType t3818_0_0_0;
extern Il2CppType t3818_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5223_m27174_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3818_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27174_GM;
MethodInfo m27174_MI = 
{
	"CopyTo", NULL, &t5223_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5223_m27174_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27174_GM};
extern Il2CppType t103_0_0_0;
static ParameterInfo t5223_m27175_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t103_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27175_GM;
MethodInfo m27175_MI = 
{
	"Remove", NULL, &t5223_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5223_m27175_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27175_GM};
static MethodInfo* t5223_MIs[] =
{
	&m27169_MI,
	&m27170_MI,
	&m27171_MI,
	&m27172_MI,
	&m27173_MI,
	&m27174_MI,
	&m27175_MI,
	NULL
};
extern TypeInfo t5225_TI;
static TypeInfo* t5223_ITIs[] = 
{
	&t603_TI,
	&t5225_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5223_0_0_0;
extern Il2CppType t5223_1_0_0;
struct t5223;
extern Il2CppGenericClass t5223_GC;
TypeInfo t5223_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5223_MIs, t5223_PIs, NULL, NULL, NULL, NULL, NULL, &t5223_TI, t5223_ITIs, NULL, &EmptyCustomAttributesCache, &t5223_TI, &t5223_0_0_0, &t5223_1_0_0, NULL, &t5223_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.ICancelHandler>
extern Il2CppType t4072_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27176_GM;
MethodInfo m27176_MI = 
{
	"GetEnumerator", NULL, &t5225_TI, &t4072_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27176_GM};
static MethodInfo* t5225_MIs[] =
{
	&m27176_MI,
	NULL
};
static TypeInfo* t5225_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5225_0_0_0;
extern Il2CppType t5225_1_0_0;
struct t5225;
extern Il2CppGenericClass t5225_GC;
TypeInfo t5225_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5225_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5225_TI, t5225_ITIs, NULL, &EmptyCustomAttributesCache, &t5225_TI, &t5225_0_0_0, &t5225_1_0_0, NULL, &t5225_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4072_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.ICancelHandler>
extern MethodInfo m27177_MI;
static PropertyInfo t4072____Current_PropertyInfo = 
{
	&t4072_TI, "Current", &m27177_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4072_PIs[] =
{
	&t4072____Current_PropertyInfo,
	NULL
};
extern Il2CppType t103_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27177_GM;
MethodInfo m27177_MI = 
{
	"get_Current", NULL, &t4072_TI, &t103_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27177_GM};
static MethodInfo* t4072_MIs[] =
{
	&m27177_MI,
	NULL
};
static TypeInfo* t4072_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4072_0_0_0;
extern Il2CppType t4072_1_0_0;
struct t4072;
extern Il2CppGenericClass t4072_GC;
TypeInfo t4072_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4072_MIs, t4072_PIs, NULL, NULL, NULL, NULL, NULL, &t4072_TI, t4072_ITIs, NULL, &EmptyCustomAttributesCache, &t4072_TI, &t4072_0_0_0, &t4072_1_0_0, NULL, &t4072_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2295.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2295_TI;
#include "t2295MD.h"

extern TypeInfo t103_TI;
extern MethodInfo m11619_MI;
extern MethodInfo m20295_MI;
struct t20;
#define m20295(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ICancelHandler>
extern Il2CppType t20_0_0_1;
FieldInfo t2295_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2295_TI, offsetof(t2295, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2295_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2295_TI, offsetof(t2295, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2295_FIs[] =
{
	&t2295_f0_FieldInfo,
	&t2295_f1_FieldInfo,
	NULL
};
extern MethodInfo m11616_MI;
static PropertyInfo t2295____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2295_TI, "System.Collections.IEnumerator.Current", &m11616_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2295____Current_PropertyInfo = 
{
	&t2295_TI, "Current", &m11619_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2295_PIs[] =
{
	&t2295____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2295____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2295_m11615_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11615_GM;
MethodInfo m11615_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2295_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2295_m11615_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11615_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11616_GM;
MethodInfo m11616_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2295_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11616_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11617_GM;
MethodInfo m11617_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2295_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11617_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11618_GM;
MethodInfo m11618_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2295_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11618_GM};
extern Il2CppType t103_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11619_GM;
MethodInfo m11619_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2295_TI, &t103_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11619_GM};
static MethodInfo* t2295_MIs[] =
{
	&m11615_MI,
	&m11616_MI,
	&m11617_MI,
	&m11618_MI,
	&m11619_MI,
	NULL
};
extern MethodInfo m11618_MI;
extern MethodInfo m11617_MI;
static MethodInfo* t2295_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m11616_MI,
	&m11618_MI,
	&m11617_MI,
	&m11619_MI,
};
static TypeInfo* t2295_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4072_TI,
};
static Il2CppInterfaceOffsetPair t2295_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4072_TI, 7},
};
extern TypeInfo t103_TI;
static Il2CppRGCTXData t2295_RGCTXData[3] = 
{
	&m11619_MI/* Method Usage */,
	&t103_TI/* Class Usage */,
	&m20295_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2295_0_0_0;
extern Il2CppType t2295_1_0_0;
extern Il2CppGenericClass t2295_GC;
TypeInfo t2295_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2295_MIs, t2295_PIs, t2295_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2295_TI, t2295_ITIs, t2295_VT, &EmptyCustomAttributesCache, &t2295_TI, &t2295_0_0_0, &t2295_1_0_0, t2295_IOs, &t2295_GC, NULL, NULL, NULL, t2295_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2295)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5224_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.ICancelHandler>
extern MethodInfo m27178_MI;
extern MethodInfo m27179_MI;
static PropertyInfo t5224____Item_PropertyInfo = 
{
	&t5224_TI, "Item", &m27178_MI, &m27179_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5224_PIs[] =
{
	&t5224____Item_PropertyInfo,
	NULL
};
extern Il2CppType t103_0_0_0;
static ParameterInfo t5224_m27180_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t103_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27180_GM;
MethodInfo m27180_MI = 
{
	"IndexOf", NULL, &t5224_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5224_m27180_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27180_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t103_0_0_0;
static ParameterInfo t5224_m27181_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t103_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27181_GM;
MethodInfo m27181_MI = 
{
	"Insert", NULL, &t5224_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5224_m27181_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27181_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5224_m27182_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27182_GM;
MethodInfo m27182_MI = 
{
	"RemoveAt", NULL, &t5224_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5224_m27182_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27182_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5224_m27178_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t103_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27178_GM;
MethodInfo m27178_MI = 
{
	"get_Item", NULL, &t5224_TI, &t103_0_0_0, RuntimeInvoker_t29_t44, t5224_m27178_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27178_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t103_0_0_0;
static ParameterInfo t5224_m27179_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t103_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27179_GM;
MethodInfo m27179_MI = 
{
	"set_Item", NULL, &t5224_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5224_m27179_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27179_GM};
static MethodInfo* t5224_MIs[] =
{
	&m27180_MI,
	&m27181_MI,
	&m27182_MI,
	&m27178_MI,
	&m27179_MI,
	NULL
};
static TypeInfo* t5224_ITIs[] = 
{
	&t603_TI,
	&t5223_TI,
	&t5225_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5224_0_0_0;
extern Il2CppType t5224_1_0_0;
struct t5224;
extern Il2CppGenericClass t5224_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5224_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5224_MIs, t5224_PIs, NULL, NULL, NULL, NULL, NULL, &t5224_TI, t5224_ITIs, NULL, &t1908__CustomAttributeCache, &t5224_TI, &t5224_0_0_0, &t5224_1_0_0, NULL, &t5224_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2296.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2296_TI;
#include "t2296MD.h"

#include "t41.h"
#include "t557.h"
#include "t62.h"
#include "mscorlib_ArrayTypes.h"
#include "t2297.h"
extern TypeInfo t316_TI;
extern TypeInfo t29_TI;
extern TypeInfo t62_TI;
extern TypeInfo t2297_TI;
extern TypeInfo t21_TI;
#include "t2297MD.h"
extern MethodInfo m11622_MI;
extern MethodInfo m11624_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.EventSystems.EventTrigger>
extern Il2CppType t316_0_0_33;
FieldInfo t2296_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2296_TI, offsetof(t2296, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2296_FIs[] =
{
	&t2296_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t62_0_0_0;
extern Il2CppType t62_0_0_0;
static ParameterInfo t2296_m11620_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t62_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11620_GM;
MethodInfo m11620_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2296_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2296_m11620_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m11620_GM};
extern Il2CppType t316_0_0_0;
extern Il2CppType t316_0_0_0;
static ParameterInfo t2296_m11621_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11621_GM;
MethodInfo m11621_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2296_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2296_m11621_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11621_GM};
static MethodInfo* t2296_MIs[] =
{
	&m11620_MI,
	&m11621_MI,
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m1332_MI;
extern MethodInfo m11621_MI;
extern MethodInfo m11625_MI;
static MethodInfo* t2296_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m11621_MI,
	&m11625_MI,
};
extern Il2CppType t2298_0_0_0;
extern TypeInfo t2298_TI;
extern MethodInfo m20305_MI;
extern TypeInfo t62_TI;
extern MethodInfo m11627_MI;
extern TypeInfo t62_TI;
static Il2CppRGCTXData t2296_RGCTXData[8] = 
{
	&t2298_0_0_0/* Type Usage */,
	&t2298_TI/* Class Usage */,
	&m20305_MI/* Method Usage */,
	&t62_TI/* Class Usage */,
	&m11627_MI/* Method Usage */,
	&m11622_MI/* Method Usage */,
	&t62_TI/* Class Usage */,
	&m11624_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2296_0_0_0;
extern Il2CppType t2296_1_0_0;
struct t2296;
extern Il2CppGenericClass t2296_GC;
TypeInfo t2296_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2296_MIs, NULL, t2296_FIs, NULL, &t2297_TI, NULL, NULL, &t2296_TI, NULL, t2296_VT, &EmptyCustomAttributesCache, &t2296_TI, &t2296_0_0_0, &t2296_1_0_0, NULL, &t2296_GC, NULL, NULL, NULL, t2296_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2296), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2298.h"
#include "t42.h"
#include "t43.h"
#include "t353.h"
#include "t305.h"
extern TypeInfo t2298_TI;
extern TypeInfo t42_TI;
extern TypeInfo t305_TI;
#include "t556MD.h"
#include "t42MD.h"
#include "t353MD.h"
#include "t305MD.h"
#include "t2298MD.h"
extern MethodInfo m2790_MI;
extern MethodInfo m1554_MI;
extern MethodInfo m2957_MI;
extern MethodInfo m1597_MI;
extern MethodInfo m2789_MI;
extern MethodInfo m1935_MI;
extern MethodInfo m2791_MI;
extern MethodInfo m2953_MI;
extern MethodInfo m2951_MI;
struct t556;
#include "t556.h"
struct t556;
 void m19555_gshared (t29 * __this, t29 * p0, MethodInfo* method);
#define m19555(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)
#define m20305(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.EventTrigger>
extern Il2CppType t2298_0_0_1;
FieldInfo t2297_f0_FieldInfo = 
{
	"Delegate", &t2298_0_0_1, &t2297_TI, offsetof(t2297, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2297_FIs[] =
{
	&t2297_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2297_m11622_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11622_GM;
MethodInfo m11622_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2297_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2297_m11622_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11622_GM};
extern Il2CppType t2298_0_0_0;
static ParameterInfo t2297_m11623_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2298_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11623_GM;
MethodInfo m11623_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2297_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2297_m11623_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11623_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2297_m11624_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11624_GM;
MethodInfo m11624_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2297_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2297_m11624_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11624_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2297_m11625_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11625_GM;
MethodInfo m11625_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2297_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2297_m11625_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11625_GM};
static MethodInfo* t2297_MIs[] =
{
	&m11622_MI,
	&m11623_MI,
	&m11624_MI,
	&m11625_MI,
	NULL
};
static MethodInfo* t2297_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m11624_MI,
	&m11625_MI,
};
extern TypeInfo t2298_TI;
extern TypeInfo t62_TI;
static Il2CppRGCTXData t2297_RGCTXData[5] = 
{
	&t2298_0_0_0/* Type Usage */,
	&t2298_TI/* Class Usage */,
	&m20305_MI/* Method Usage */,
	&t62_TI/* Class Usage */,
	&m11627_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2297_0_0_0;
extern Il2CppType t2297_1_0_0;
extern TypeInfo t556_TI;
struct t2297;
extern Il2CppGenericClass t2297_GC;
TypeInfo t2297_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2297_MIs, NULL, t2297_FIs, NULL, &t556_TI, NULL, NULL, &t2297_TI, NULL, t2297_VT, &EmptyCustomAttributesCache, &t2297_TI, &t2297_0_0_0, &t2297_1_0_0, NULL, &t2297_GC, NULL, NULL, NULL, t2297_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2297), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t35.h"
#include "t67.h"


// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.EventTrigger>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2298_m11626_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11626_GM;
MethodInfo m11626_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2298_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2298_m11626_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11626_GM};
extern Il2CppType t62_0_0_0;
static ParameterInfo t2298_m11627_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t62_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11627_GM;
MethodInfo m11627_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2298_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2298_m11627_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11627_GM};
extern Il2CppType t62_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2298_m11628_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t62_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11628_GM;
MethodInfo m11628_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2298_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2298_m11628_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m11628_GM};
extern Il2CppType t66_0_0_0;
extern Il2CppType t66_0_0_0;
static ParameterInfo t2298_m11629_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11629_GM;
MethodInfo m11629_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2298_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2298_m11629_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11629_GM};
static MethodInfo* t2298_MIs[] =
{
	&m11626_MI,
	&m11627_MI,
	&m11628_MI,
	&m11629_MI,
	NULL
};
extern MethodInfo m1699_MI;
extern MethodInfo m1700_MI;
extern MethodInfo m1701_MI;
extern MethodInfo m1702_MI;
extern MethodInfo m1703_MI;
extern MethodInfo m1704_MI;
extern MethodInfo m1705_MI;
extern MethodInfo m11628_MI;
extern MethodInfo m11629_MI;
static MethodInfo* t2298_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m11627_MI,
	&m11628_MI,
	&m11629_MI,
};
extern TypeInfo t373_TI;
extern TypeInfo t374_TI;
static Il2CppInterfaceOffsetPair t2298_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2298_1_0_0;
extern TypeInfo t195_TI;
struct t2298;
extern Il2CppGenericClass t2298_GC;
TypeInfo t2298_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2298_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2298_TI, NULL, t2298_VT, &EmptyCustomAttributesCache, &t2298_TI, &t2298_0_0_0, &t2298_1_0_0, t2298_IOs, &t2298_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2298), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t63.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t63_TI;
#include "t63MD.h"

#include "t60.h"
#include "t2306.h"
#include "t2303.h"
#include "t2304.h"
#include "t338.h"
#include "t2312.h"
#include "t2305.h"
extern TypeInfo t60_TI;
extern TypeInfo t44_TI;
extern TypeInfo t585_TI;
extern TypeInfo t1649_TI;
extern TypeInfo t915_TI;
extern TypeInfo t2299_TI;
extern TypeInfo t2306_TI;
extern TypeInfo t40_TI;
extern TypeInfo t2301_TI;
extern TypeInfo t2302_TI;
extern TypeInfo t2300_TI;
extern TypeInfo t2303_TI;
extern TypeInfo t338_TI;
extern TypeInfo t2304_TI;
extern TypeInfo t2312_TI;
#include "t915MD.h"
#include "t29MD.h"
#include "t602MD.h"
#include "t2303MD.h"
#include "t338MD.h"
#include "t2304MD.h"
#include "t2306MD.h"
#include "t2312MD.h"
extern MethodInfo m1334_MI;
extern MethodInfo m11676_MI;
extern MethodInfo m4198_MI;
extern MethodInfo m20318_MI;
extern MethodInfo m3975_MI;
extern MethodInfo m11663_MI;
extern MethodInfo m1331_MI;
extern MethodInfo m11660_MI;
extern MethodInfo m5952_MI;
extern MethodInfo m11648_MI;
extern MethodInfo m11655_MI;
extern MethodInfo m11661_MI;
extern MethodInfo m11664_MI;
extern MethodInfo m11666_MI;
extern MethodInfo m11649_MI;
extern MethodInfo m11674_MI;
extern MethodInfo m5139_MI;
extern MethodInfo m11675_MI;
extern MethodInfo m27183_MI;
extern MethodInfo m27184_MI;
extern MethodInfo m27185_MI;
extern MethodInfo m27186_MI;
extern MethodInfo m3972_MI;
extern MethodInfo m1428_MI;
extern MethodInfo m11665_MI;
extern MethodInfo m11650_MI;
extern MethodInfo m11651_MI;
extern MethodInfo m11688_MI;
extern MethodInfo m5112_MI;
extern MethodInfo m20320_MI;
extern MethodInfo m11658_MI;
extern MethodInfo m11659_MI;
extern MethodInfo m2950_MI;
extern MethodInfo m11763_MI;
extern MethodInfo m11682_MI;
extern MethodInfo m11662_MI;
extern MethodInfo m11668_MI;
extern MethodInfo m5171_MI;
extern MethodInfo m11769_MI;
extern MethodInfo m20322_MI;
extern MethodInfo m20330_MI;
extern MethodInfo m5951_MI;
struct t20;
struct t20;
 void m19788_gshared (t29 * __this, t316** p0, int32_t p1, MethodInfo* method);
#define m19788(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
#define m20318(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
struct t20;
#include "t2310.h"
struct t20;
#include "t2001.h"
 int32_t m10120_gshared (t29 * __this, t316* p0, t29 * p1, int32_t p2, int32_t p3, MethodInfo* method);
#define m10120(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
#define m20320(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
struct t20;
struct t20;
 void m19857_gshared (t29 * __this, t316* p0, int32_t p1, int32_t p2, t29* p3, MethodInfo* method);
#define m19857(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
#define m20322(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
struct t20;
#include "t295.h"
struct t20;
#include "t2181.h"
 void m19964_gshared (t29 * __this, t316* p0, int32_t p1, t2181 * p2, MethodInfo* method);
#define m19964(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)
#define m20330(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)


 t2306  m11660 (t63 * __this, MethodInfo* method){
	{
		t2306  L_0 = {0};
		m11682(&L_0, __this, &m11682_MI);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>
extern Il2CppType t44_0_0_32849;
FieldInfo t63_f0_FieldInfo = 
{
	"DefaultCapacity", &t44_0_0_32849, &t63_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t2299_0_0_1;
FieldInfo t63_f1_FieldInfo = 
{
	"_items", &t2299_0_0_1, &t63_TI, offsetof(t63, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t63_f2_FieldInfo = 
{
	"_size", &t44_0_0_1, &t63_TI, offsetof(t63, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t63_f3_FieldInfo = 
{
	"_version", &t44_0_0_1, &t63_TI, offsetof(t63, f3), &EmptyCustomAttributesCache};
extern Il2CppType t2299_0_0_49;
FieldInfo t63_f4_FieldInfo = 
{
	"EmptyArray", &t2299_0_0_49, &t63_TI, offsetof(t63_SFs, f4), &EmptyCustomAttributesCache};
static FieldInfo* t63_FIs[] =
{
	&t63_f0_FieldInfo,
	&t63_f1_FieldInfo,
	&t63_f2_FieldInfo,
	&t63_f3_FieldInfo,
	&t63_f4_FieldInfo,
	NULL
};
static const int32_t t63_f0_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t63_f0_DefaultValue = 
{
	&t63_f0_FieldInfo, { (char*)&t63_f0_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t63_FDVs[] = 
{
	&t63_f0_DefaultValue,
	NULL
};
extern MethodInfo m11641_MI;
static PropertyInfo t63____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t63_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m11641_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11642_MI;
static PropertyInfo t63____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t63_TI, "System.Collections.ICollection.IsSynchronized", &m11642_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11643_MI;
static PropertyInfo t63____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t63_TI, "System.Collections.ICollection.SyncRoot", &m11643_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11644_MI;
static PropertyInfo t63____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t63_TI, "System.Collections.IList.IsFixedSize", &m11644_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11645_MI;
static PropertyInfo t63____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t63_TI, "System.Collections.IList.IsReadOnly", &m11645_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11646_MI;
extern MethodInfo m11647_MI;
static PropertyInfo t63____System_Collections_IList_Item_PropertyInfo = 
{
	&t63_TI, "System.Collections.IList.Item", &m11646_MI, &m11647_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t63____Capacity_PropertyInfo = 
{
	&t63_TI, "Capacity", &m11674_MI, &m11675_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m1333_MI;
static PropertyInfo t63____Count_PropertyInfo = 
{
	&t63_TI, "Count", &m1333_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t63____Item_PropertyInfo = 
{
	&t63_TI, "Item", &m1334_MI, &m11676_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t63_PIs[] =
{
	&t63____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t63____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t63____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t63____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t63____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t63____System_Collections_IList_Item_PropertyInfo,
	&t63____Capacity_PropertyInfo,
	&t63____Count_PropertyInfo,
	&t63____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11630_GM;
MethodInfo m11630_MI = 
{
	".ctor", (methodPointerType)&m10537_gshared, &t63_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11630_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t63_m11631_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11631_GM;
MethodInfo m11631_MI = 
{
	".ctor", (methodPointerType)&m10539_gshared, &t63_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t63_m11631_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11631_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11632_GM;
MethodInfo m11632_MI = 
{
	".cctor", (methodPointerType)&m10541_gshared, &t63_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11632_GM};
extern Il2CppType t2300_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11633_GM;
MethodInfo m11633_MI = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator", (methodPointerType)&m10543_gshared, &t63_TI, &t2300_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 27, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11633_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t63_m11634_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11634_GM;
MethodInfo m11634_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10545_gshared, &t63_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t63_m11634_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11634_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11635_GM;
MethodInfo m11635_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10547_gshared, &t63_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11635_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t63_m11636_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11636_GM;
MethodInfo m11636_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10549_gshared, &t63_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t63_m11636_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11636_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t63_m11637_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11637_GM;
MethodInfo m11637_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10551_gshared, &t63_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t63_m11637_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11637_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t63_m11638_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11638_GM;
MethodInfo m11638_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10553_gshared, &t63_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t63_m11638_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11638_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t63_m11639_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11639_GM;
MethodInfo m11639_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10555_gshared, &t63_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t63_m11639_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11639_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t63_m11640_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11640_GM;
MethodInfo m11640_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10557_gshared, &t63_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t63_m11640_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11640_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11641_GM;
MethodInfo m11641_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10559_gshared, &t63_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11641_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11642_GM;
MethodInfo m11642_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10561_gshared, &t63_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11642_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11643_GM;
MethodInfo m11643_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10563_gshared, &t63_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11643_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11644_GM;
MethodInfo m11644_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10565_gshared, &t63_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11644_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11645_GM;
MethodInfo m11645_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10567_gshared, &t63_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11645_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t63_m11646_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11646_GM;
MethodInfo m11646_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10569_gshared, &t63_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t63_m11646_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11646_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t63_m11647_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11647_GM;
MethodInfo m11647_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10571_gshared, &t63_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t63_m11647_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11647_GM};
extern Il2CppType t60_0_0_0;
extern Il2CppType t60_0_0_0;
static ParameterInfo t63_m11648_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11648_GM;
MethodInfo m11648_MI = 
{
	"Add", (methodPointerType)&m10573_gshared, &t63_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t63_m11648_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11648_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t63_m11649_ParameterInfos[] = 
{
	{"newCount", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11649_GM;
MethodInfo m11649_MI = 
{
	"GrowIfNeeded", (methodPointerType)&m10575_gshared, &t63_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t63_m11649_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11649_GM};
extern Il2CppType t2301_0_0_0;
extern Il2CppType t2301_0_0_0;
static ParameterInfo t63_m11650_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2301_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11650_GM;
MethodInfo m11650_MI = 
{
	"AddCollection", (methodPointerType)&m10577_gshared, &t63_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t63_m11650_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11650_GM};
extern Il2CppType t2302_0_0_0;
extern Il2CppType t2302_0_0_0;
static ParameterInfo t63_m11651_ParameterInfos[] = 
{
	{"enumerable", 0, 134217728, &EmptyCustomAttributesCache, &t2302_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11651_GM;
MethodInfo m11651_MI = 
{
	"AddEnumerable", (methodPointerType)&m10579_gshared, &t63_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t63_m11651_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11651_GM};
extern Il2CppType t2302_0_0_0;
static ParameterInfo t63_m11652_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2302_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11652_GM;
MethodInfo m11652_MI = 
{
	"AddRange", (methodPointerType)&m10581_gshared, &t63_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t63_m11652_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11652_GM};
extern Il2CppType t2303_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11653_GM;
MethodInfo m11653_MI = 
{
	"AsReadOnly", (methodPointerType)&m10583_gshared, &t63_TI, &t2303_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11653_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11654_GM;
MethodInfo m11654_MI = 
{
	"Clear", (methodPointerType)&m10585_gshared, &t63_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11654_GM};
extern Il2CppType t60_0_0_0;
static ParameterInfo t63_m11655_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11655_GM;
MethodInfo m11655_MI = 
{
	"Contains", (methodPointerType)&m10587_gshared, &t63_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t63_m11655_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11655_GM};
extern Il2CppType t2299_0_0_0;
extern Il2CppType t2299_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t63_m11656_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2299_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11656_GM;
MethodInfo m11656_MI = 
{
	"CopyTo", (methodPointerType)&m10589_gshared, &t63_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t63_m11656_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11656_GM};
extern Il2CppType t2304_0_0_0;
extern Il2CppType t2304_0_0_0;
static ParameterInfo t63_m11657_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2304_0_0_0},
};
extern Il2CppType t60_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11657_GM;
MethodInfo m11657_MI = 
{
	"Find", (methodPointerType)&m10591_gshared, &t63_TI, &t60_0_0_0, RuntimeInvoker_t29_t29, t63_m11657_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11657_GM};
extern Il2CppType t2304_0_0_0;
static ParameterInfo t63_m11658_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2304_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11658_GM;
MethodInfo m11658_MI = 
{
	"CheckMatch", (methodPointerType)&m10593_gshared, &t63_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t63_m11658_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11658_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2304_0_0_0;
static ParameterInfo t63_m11659_ParameterInfos[] = 
{
	{"startIndex", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"count", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"match", 2, 134217728, &EmptyCustomAttributesCache, &t2304_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11659_GM;
MethodInfo m11659_MI = 
{
	"GetIndex", (methodPointerType)&m10595_gshared, &t63_TI, &t44_0_0_0, RuntimeInvoker_t44_t44_t44_t29, t63_m11659_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m11659_GM};
extern Il2CppType t2306_0_0_0;
extern void* RuntimeInvoker_t2306 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11660_GM;
MethodInfo m11660_MI = 
{
	"GetEnumerator", (methodPointerType)&m11660, &t63_TI, &t2306_0_0_0, RuntimeInvoker_t2306, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11660_GM};
extern Il2CppType t60_0_0_0;
static ParameterInfo t63_m11661_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11661_GM;
MethodInfo m11661_MI = 
{
	"IndexOf", (methodPointerType)&m10598_gshared, &t63_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t63_m11661_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11661_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t63_m11662_ParameterInfos[] = 
{
	{"start", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"delta", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11662_GM;
MethodInfo m11662_MI = 
{
	"Shift", (methodPointerType)&m10600_gshared, &t63_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t63_m11662_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11662_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t63_m11663_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11663_GM;
MethodInfo m11663_MI = 
{
	"CheckIndex", (methodPointerType)&m10602_gshared, &t63_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t63_m11663_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11663_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t60_0_0_0;
static ParameterInfo t63_m11664_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11664_GM;
MethodInfo m11664_MI = 
{
	"Insert", (methodPointerType)&m10604_gshared, &t63_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t63_m11664_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11664_GM};
extern Il2CppType t2302_0_0_0;
static ParameterInfo t63_m11665_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2302_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11665_GM;
MethodInfo m11665_MI = 
{
	"CheckCollection", (methodPointerType)&m10606_gshared, &t63_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t63_m11665_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11665_GM};
extern Il2CppType t60_0_0_0;
static ParameterInfo t63_m11666_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11666_GM;
MethodInfo m11666_MI = 
{
	"Remove", (methodPointerType)&m10608_gshared, &t63_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t63_m11666_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11666_GM};
extern Il2CppType t2304_0_0_0;
static ParameterInfo t63_m11667_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2304_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11667_GM;
MethodInfo m11667_MI = 
{
	"RemoveAll", (methodPointerType)&m10610_gshared, &t63_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t63_m11667_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11667_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t63_m11668_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11668_GM;
MethodInfo m11668_MI = 
{
	"RemoveAt", (methodPointerType)&m10611_gshared, &t63_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t63_m11668_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11668_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11669_GM;
MethodInfo m11669_MI = 
{
	"Reverse", (methodPointerType)&m10613_gshared, &t63_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11669_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11670_GM;
MethodInfo m11670_MI = 
{
	"Sort", (methodPointerType)&m10615_gshared, &t63_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11670_GM};
extern Il2CppType t2305_0_0_0;
extern Il2CppType t2305_0_0_0;
static ParameterInfo t63_m11671_ParameterInfos[] = 
{
	{"comparison", 0, 134217728, &EmptyCustomAttributesCache, &t2305_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11671_GM;
MethodInfo m11671_MI = 
{
	"Sort", (methodPointerType)&m10617_gshared, &t63_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t63_m11671_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11671_GM};
extern Il2CppType t2299_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11672_GM;
MethodInfo m11672_MI = 
{
	"ToArray", (methodPointerType)&m10619_gshared, &t63_TI, &t2299_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11672_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11673_GM;
MethodInfo m11673_MI = 
{
	"TrimExcess", (methodPointerType)&m10621_gshared, &t63_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11673_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11674_GM;
MethodInfo m11674_MI = 
{
	"get_Capacity", (methodPointerType)&m10623_gshared, &t63_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11674_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t63_m11675_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11675_GM;
MethodInfo m11675_MI = 
{
	"set_Capacity", (methodPointerType)&m10625_gshared, &t63_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t63_m11675_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11675_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1333_GM;
MethodInfo m1333_MI = 
{
	"get_Count", (methodPointerType)&m10626_gshared, &t63_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1333_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t63_m1334_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t60_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1334_GM;
MethodInfo m1334_MI = 
{
	"get_Item", (methodPointerType)&m10627_gshared, &t63_TI, &t60_0_0_0, RuntimeInvoker_t29_t44, t63_m1334_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1334_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t60_0_0_0;
static ParameterInfo t63_m11676_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11676_GM;
MethodInfo m11676_MI = 
{
	"set_Item", (methodPointerType)&m10629_gshared, &t63_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t63_m11676_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 32, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11676_GM};
static MethodInfo* t63_MIs[] =
{
	&m11630_MI,
	&m11631_MI,
	&m11632_MI,
	&m11633_MI,
	&m11634_MI,
	&m11635_MI,
	&m11636_MI,
	&m11637_MI,
	&m11638_MI,
	&m11639_MI,
	&m11640_MI,
	&m11641_MI,
	&m11642_MI,
	&m11643_MI,
	&m11644_MI,
	&m11645_MI,
	&m11646_MI,
	&m11647_MI,
	&m11648_MI,
	&m11649_MI,
	&m11650_MI,
	&m11651_MI,
	&m11652_MI,
	&m11653_MI,
	&m11654_MI,
	&m11655_MI,
	&m11656_MI,
	&m11657_MI,
	&m11658_MI,
	&m11659_MI,
	&m11660_MI,
	&m11661_MI,
	&m11662_MI,
	&m11663_MI,
	&m11664_MI,
	&m11665_MI,
	&m11666_MI,
	&m11667_MI,
	&m11668_MI,
	&m11669_MI,
	&m11670_MI,
	&m11671_MI,
	&m11672_MI,
	&m11673_MI,
	&m11674_MI,
	&m11675_MI,
	&m1333_MI,
	&m1334_MI,
	&m11676_MI,
	NULL
};
extern MethodInfo m11635_MI;
extern MethodInfo m11634_MI;
extern MethodInfo m11636_MI;
extern MethodInfo m11654_MI;
extern MethodInfo m11637_MI;
extern MethodInfo m11638_MI;
extern MethodInfo m11639_MI;
extern MethodInfo m11640_MI;
extern MethodInfo m11656_MI;
extern MethodInfo m11633_MI;
static MethodInfo* t63_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m11635_MI,
	&m1333_MI,
	&m11642_MI,
	&m11643_MI,
	&m11634_MI,
	&m11644_MI,
	&m11645_MI,
	&m11646_MI,
	&m11647_MI,
	&m11636_MI,
	&m11654_MI,
	&m11637_MI,
	&m11638_MI,
	&m11639_MI,
	&m11640_MI,
	&m11668_MI,
	&m1333_MI,
	&m11641_MI,
	&m11648_MI,
	&m11654_MI,
	&m11655_MI,
	&m11656_MI,
	&m11666_MI,
	&m11633_MI,
	&m11661_MI,
	&m11664_MI,
	&m11668_MI,
	&m1334_MI,
	&m11676_MI,
};
extern TypeInfo t674_TI;
extern TypeInfo t868_TI;
extern TypeInfo t2308_TI;
static TypeInfo* t63_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2301_TI,
	&t2302_TI,
	&t2308_TI,
};
static Il2CppInterfaceOffsetPair t63_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2301_TI, 20},
	{ &t2302_TI, 27},
	{ &t2308_TI, 28},
};
extern TypeInfo t63_TI;
extern TypeInfo t2299_TI;
extern TypeInfo t2306_TI;
extern TypeInfo t60_TI;
extern TypeInfo t2301_TI;
extern TypeInfo t2303_TI;
static Il2CppRGCTXData t63_RGCTXData[37] = 
{
	&t63_TI/* Static Usage */,
	&t2299_TI/* Array Usage */,
	&m11660_MI/* Method Usage */,
	&t2306_TI/* Class Usage */,
	&t60_TI/* Class Usage */,
	&m11648_MI/* Method Usage */,
	&m11655_MI/* Method Usage */,
	&m11661_MI/* Method Usage */,
	&m11663_MI/* Method Usage */,
	&m11664_MI/* Method Usage */,
	&m11666_MI/* Method Usage */,
	&m1334_MI/* Method Usage */,
	&m11676_MI/* Method Usage */,
	&m11649_MI/* Method Usage */,
	&m11674_MI/* Method Usage */,
	&m11675_MI/* Method Usage */,
	&m27183_MI/* Method Usage */,
	&m27184_MI/* Method Usage */,
	&m27185_MI/* Method Usage */,
	&m27186_MI/* Method Usage */,
	&m11665_MI/* Method Usage */,
	&t2301_TI/* Class Usage */,
	&m11650_MI/* Method Usage */,
	&m11651_MI/* Method Usage */,
	&t2303_TI/* Class Usage */,
	&m11688_MI/* Method Usage */,
	&m20320_MI/* Method Usage */,
	&m11658_MI/* Method Usage */,
	&m11659_MI/* Method Usage */,
	&m11763_MI/* Method Usage */,
	&m11682_MI/* Method Usage */,
	&m11662_MI/* Method Usage */,
	&m11668_MI/* Method Usage */,
	&m11769_MI/* Method Usage */,
	&m20322_MI/* Method Usage */,
	&m20330_MI/* Method Usage */,
	&m20318_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t63_0_0_0;
extern Il2CppType t63_1_0_0;
struct t63;
extern Il2CppGenericClass t63_GC;
extern CustomAttributesCache t1261__CustomAttributeCache;
TypeInfo t63_TI = 
{
	&g_mscorlib_dll_Image, NULL, "List`1", "System.Collections.Generic", t63_MIs, t63_PIs, t63_FIs, NULL, &t29_TI, NULL, NULL, &t63_TI, t63_ITIs, t63_VT, &t1261__CustomAttributeCache, &t63_TI, &t63_0_0_0, &t63_1_0_0, t63_IOs, &t63_GC, NULL, t63_FDVs, NULL, t63_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t63), 0, -1, sizeof(t63_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, true, false, false, 49, 9, 5, 0, 0, 33, 6, 6};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventTrigger/Entry>
static PropertyInfo t2301____Count_PropertyInfo = 
{
	&t2301_TI, "Count", &m27183_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27187_MI;
static PropertyInfo t2301____IsReadOnly_PropertyInfo = 
{
	&t2301_TI, "IsReadOnly", &m27187_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2301_PIs[] =
{
	&t2301____Count_PropertyInfo,
	&t2301____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27183_GM;
MethodInfo m27183_MI = 
{
	"get_Count", NULL, &t2301_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27183_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27187_GM;
MethodInfo m27187_MI = 
{
	"get_IsReadOnly", NULL, &t2301_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27187_GM};
extern Il2CppType t60_0_0_0;
static ParameterInfo t2301_m27188_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27188_GM;
MethodInfo m27188_MI = 
{
	"Add", NULL, &t2301_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2301_m27188_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27188_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27189_GM;
MethodInfo m27189_MI = 
{
	"Clear", NULL, &t2301_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27189_GM};
extern Il2CppType t60_0_0_0;
static ParameterInfo t2301_m27190_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27190_GM;
MethodInfo m27190_MI = 
{
	"Contains", NULL, &t2301_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2301_m27190_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27190_GM};
extern Il2CppType t2299_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2301_m27184_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2299_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27184_GM;
MethodInfo m27184_MI = 
{
	"CopyTo", NULL, &t2301_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2301_m27184_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27184_GM};
extern Il2CppType t60_0_0_0;
static ParameterInfo t2301_m27191_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27191_GM;
MethodInfo m27191_MI = 
{
	"Remove", NULL, &t2301_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2301_m27191_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27191_GM};
static MethodInfo* t2301_MIs[] =
{
	&m27183_MI,
	&m27187_MI,
	&m27188_MI,
	&m27189_MI,
	&m27190_MI,
	&m27184_MI,
	&m27191_MI,
	NULL
};
static TypeInfo* t2301_ITIs[] = 
{
	&t603_TI,
	&t2302_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2301_1_0_0;
struct t2301;
extern Il2CppGenericClass t2301_GC;
TypeInfo t2301_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t2301_MIs, t2301_PIs, NULL, NULL, NULL, NULL, NULL, &t2301_TI, t2301_ITIs, NULL, &EmptyCustomAttributesCache, &t2301_TI, &t2301_0_0_0, &t2301_1_0_0, NULL, &t2301_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.EventTrigger/Entry>
extern Il2CppType t2300_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27185_GM;
MethodInfo m27185_MI = 
{
	"GetEnumerator", NULL, &t2302_TI, &t2300_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27185_GM};
static MethodInfo* t2302_MIs[] =
{
	&m27185_MI,
	NULL
};
static TypeInfo* t2302_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2302_1_0_0;
struct t2302;
extern Il2CppGenericClass t2302_GC;
TypeInfo t2302_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t2302_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2302_TI, t2302_ITIs, NULL, &EmptyCustomAttributesCache, &t2302_TI, &t2302_0_0_0, &t2302_1_0_0, NULL, &t2302_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.EventTrigger/Entry>
static PropertyInfo t2300____Current_PropertyInfo = 
{
	&t2300_TI, "Current", &m27186_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2300_PIs[] =
{
	&t2300____Current_PropertyInfo,
	NULL
};
extern Il2CppType t60_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27186_GM;
MethodInfo m27186_MI = 
{
	"get_Current", NULL, &t2300_TI, &t60_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27186_GM};
static MethodInfo* t2300_MIs[] =
{
	&m27186_MI,
	NULL
};
static TypeInfo* t2300_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2300_0_0_0;
extern Il2CppType t2300_1_0_0;
struct t2300;
extern Il2CppGenericClass t2300_GC;
TypeInfo t2300_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2300_MIs, t2300_PIs, NULL, NULL, NULL, NULL, NULL, &t2300_TI, t2300_ITIs, NULL, &EmptyCustomAttributesCache, &t2300_TI, &t2300_0_0_0, &t2300_1_0_0, NULL, &t2300_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2307.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2307_TI;
#include "t2307MD.h"

extern MethodInfo m11681_MI;
extern MethodInfo m20307_MI;
struct t20;
#define m20307(__this, p0, method) (t60 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventTrigger/Entry>
extern Il2CppType t20_0_0_1;
FieldInfo t2307_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2307_TI, offsetof(t2307, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2307_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2307_TI, offsetof(t2307, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2307_FIs[] =
{
	&t2307_f0_FieldInfo,
	&t2307_f1_FieldInfo,
	NULL
};
extern MethodInfo m11678_MI;
static PropertyInfo t2307____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2307_TI, "System.Collections.IEnumerator.Current", &m11678_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2307____Current_PropertyInfo = 
{
	&t2307_TI, "Current", &m11681_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2307_PIs[] =
{
	&t2307____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2307____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2307_m11677_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11677_GM;
MethodInfo m11677_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2307_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2307_m11677_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11677_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11678_GM;
MethodInfo m11678_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2307_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11678_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11679_GM;
MethodInfo m11679_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2307_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11679_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11680_GM;
MethodInfo m11680_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2307_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11680_GM};
extern Il2CppType t60_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11681_GM;
MethodInfo m11681_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2307_TI, &t60_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11681_GM};
static MethodInfo* t2307_MIs[] =
{
	&m11677_MI,
	&m11678_MI,
	&m11679_MI,
	&m11680_MI,
	&m11681_MI,
	NULL
};
extern MethodInfo m11680_MI;
extern MethodInfo m11679_MI;
static MethodInfo* t2307_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m11678_MI,
	&m11680_MI,
	&m11679_MI,
	&m11681_MI,
};
static TypeInfo* t2307_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2300_TI,
};
static Il2CppInterfaceOffsetPair t2307_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2300_TI, 7},
};
extern TypeInfo t60_TI;
static Il2CppRGCTXData t2307_RGCTXData[3] = 
{
	&m11681_MI/* Method Usage */,
	&t60_TI/* Class Usage */,
	&m20307_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2307_0_0_0;
extern Il2CppType t2307_1_0_0;
extern Il2CppGenericClass t2307_GC;
TypeInfo t2307_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2307_MIs, t2307_PIs, t2307_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2307_TI, t2307_ITIs, t2307_VT, &EmptyCustomAttributesCache, &t2307_TI, &t2307_0_0_0, &t2307_1_0_0, t2307_IOs, &t2307_GC, NULL, NULL, NULL, t2307_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2307)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.EventTrigger/Entry>
extern MethodInfo m27192_MI;
extern MethodInfo m27193_MI;
static PropertyInfo t2308____Item_PropertyInfo = 
{
	&t2308_TI, "Item", &m27192_MI, &m27193_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2308_PIs[] =
{
	&t2308____Item_PropertyInfo,
	NULL
};
extern Il2CppType t60_0_0_0;
static ParameterInfo t2308_m27194_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27194_GM;
MethodInfo m27194_MI = 
{
	"IndexOf", NULL, &t2308_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2308_m27194_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27194_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t60_0_0_0;
static ParameterInfo t2308_m27195_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27195_GM;
MethodInfo m27195_MI = 
{
	"Insert", NULL, &t2308_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2308_m27195_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27195_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2308_m27196_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27196_GM;
MethodInfo m27196_MI = 
{
	"RemoveAt", NULL, &t2308_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2308_m27196_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27196_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2308_m27192_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t60_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27192_GM;
MethodInfo m27192_MI = 
{
	"get_Item", NULL, &t2308_TI, &t60_0_0_0, RuntimeInvoker_t29_t44, t2308_m27192_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27192_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t60_0_0_0;
static ParameterInfo t2308_m27193_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27193_GM;
MethodInfo m27193_MI = 
{
	"set_Item", NULL, &t2308_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2308_m27193_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27193_GM};
static MethodInfo* t2308_MIs[] =
{
	&m27194_MI,
	&m27195_MI,
	&m27196_MI,
	&m27192_MI,
	&m27193_MI,
	NULL
};
static TypeInfo* t2308_ITIs[] = 
{
	&t603_TI,
	&t2301_TI,
	&t2302_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2308_0_0_0;
extern Il2CppType t2308_1_0_0;
struct t2308;
extern Il2CppGenericClass t2308_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t2308_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t2308_MIs, t2308_PIs, NULL, NULL, NULL, NULL, NULL, &t2308_TI, t2308_ITIs, NULL, &t1908__CustomAttributeCache, &t2308_TI, &t2308_0_0_0, &t2308_1_0_0, NULL, &t2308_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif

#include "t1101.h"
extern TypeInfo t7_TI;
extern TypeInfo t1101_TI;
#include "t1101MD.h"
extern MethodInfo m11685_MI;
extern MethodInfo m3974_MI;
extern MethodInfo m1430_MI;
extern MethodInfo m2998_MI;
extern MethodInfo m5150_MI;


// Metadata Definition System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.EventTrigger/Entry>
extern Il2CppType t63_0_0_1;
FieldInfo t2306_f0_FieldInfo = 
{
	"l", &t63_0_0_1, &t2306_TI, offsetof(t2306, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2306_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t2306_TI, offsetof(t2306, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2306_f2_FieldInfo = 
{
	"ver", &t44_0_0_1, &t2306_TI, offsetof(t2306, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t60_0_0_1;
FieldInfo t2306_f3_FieldInfo = 
{
	"current", &t60_0_0_1, &t2306_TI, offsetof(t2306, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2306_FIs[] =
{
	&t2306_f0_FieldInfo,
	&t2306_f1_FieldInfo,
	&t2306_f2_FieldInfo,
	&t2306_f3_FieldInfo,
	NULL
};
extern MethodInfo m11683_MI;
static PropertyInfo t2306____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2306_TI, "System.Collections.IEnumerator.Current", &m11683_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11687_MI;
static PropertyInfo t2306____Current_PropertyInfo = 
{
	&t2306_TI, "Current", &m11687_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2306_PIs[] =
{
	&t2306____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2306____Current_PropertyInfo,
	NULL
};
extern Il2CppType t63_0_0_0;
static ParameterInfo t2306_m11682_ParameterInfos[] = 
{
	{"l", 0, 134217728, &EmptyCustomAttributesCache, &t63_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11682_GM;
MethodInfo m11682_MI = 
{
	".ctor", (methodPointerType)&m10631_gshared, &t2306_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2306_m11682_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11682_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11683_GM;
MethodInfo m11683_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10632_gshared, &t2306_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11683_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11684_GM;
MethodInfo m11684_MI = 
{
	"Dispose", (methodPointerType)&m10633_gshared, &t2306_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11684_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11685_GM;
MethodInfo m11685_MI = 
{
	"VerifyState", (methodPointerType)&m10634_gshared, &t2306_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11685_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11686_GM;
MethodInfo m11686_MI = 
{
	"MoveNext", (methodPointerType)&m10635_gshared, &t2306_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11686_GM};
extern Il2CppType t60_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11687_GM;
MethodInfo m11687_MI = 
{
	"get_Current", (methodPointerType)&m10636_gshared, &t2306_TI, &t60_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11687_GM};
static MethodInfo* t2306_MIs[] =
{
	&m11682_MI,
	&m11683_MI,
	&m11684_MI,
	&m11685_MI,
	&m11686_MI,
	&m11687_MI,
	NULL
};
extern MethodInfo m11686_MI;
extern MethodInfo m11684_MI;
static MethodInfo* t2306_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m11683_MI,
	&m11686_MI,
	&m11684_MI,
	&m11687_MI,
};
static TypeInfo* t2306_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2300_TI,
};
static Il2CppInterfaceOffsetPair t2306_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2300_TI, 7},
};
extern TypeInfo t60_TI;
extern TypeInfo t2306_TI;
static Il2CppRGCTXData t2306_RGCTXData[3] = 
{
	&m11685_MI/* Method Usage */,
	&t60_TI/* Class Usage */,
	&t2306_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2306_0_0_0;
extern Il2CppType t2306_1_0_0;
extern Il2CppGenericClass t2306_GC;
extern TypeInfo t1261_TI;
TypeInfo t2306_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t2306_MIs, t2306_PIs, t2306_FIs, NULL, &t110_TI, NULL, &t1261_TI, &t2306_TI, t2306_ITIs, t2306_VT, &EmptyCustomAttributesCache, &t2306_TI, &t2306_0_0_0, &t2306_1_0_0, t2306_IOs, &t2306_GC, NULL, NULL, NULL, t2306_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2306)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 4, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif

#include "t345.h"
extern TypeInfo t345_TI;
#include "t345MD.h"
#include "t2309MD.h"
extern MethodInfo m11717_MI;
extern MethodInfo m1516_MI;
extern MethodInfo m3960_MI;
extern MethodInfo m4154_MI;
extern MethodInfo m11749_MI;
extern MethodInfo m27190_MI;
extern MethodInfo m27194_MI;


// Metadata Definition System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.EventTrigger/Entry>
extern Il2CppType t2308_0_0_1;
FieldInfo t2303_f0_FieldInfo = 
{
	"list", &t2308_0_0_1, &t2303_TI, offsetof(t2303, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2303_FIs[] =
{
	&t2303_f0_FieldInfo,
	NULL
};
extern MethodInfo m11694_MI;
extern MethodInfo m11695_MI;
static PropertyInfo t2303____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo = 
{
	&t2303_TI, "System.Collections.Generic.IList<T>.Item", &m11694_MI, &m11695_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11696_MI;
static PropertyInfo t2303____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2303_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m11696_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11706_MI;
static PropertyInfo t2303____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2303_TI, "System.Collections.ICollection.IsSynchronized", &m11706_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11707_MI;
static PropertyInfo t2303____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2303_TI, "System.Collections.ICollection.SyncRoot", &m11707_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11708_MI;
static PropertyInfo t2303____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2303_TI, "System.Collections.IList.IsFixedSize", &m11708_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11709_MI;
static PropertyInfo t2303____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2303_TI, "System.Collections.IList.IsReadOnly", &m11709_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11710_MI;
extern MethodInfo m11711_MI;
static PropertyInfo t2303____System_Collections_IList_Item_PropertyInfo = 
{
	&t2303_TI, "System.Collections.IList.Item", &m11710_MI, &m11711_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11716_MI;
static PropertyInfo t2303____Count_PropertyInfo = 
{
	&t2303_TI, "Count", &m11716_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2303____Item_PropertyInfo = 
{
	&t2303_TI, "Item", &m11717_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2303_PIs[] =
{
	&t2303____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo,
	&t2303____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2303____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2303____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2303____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2303____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2303____System_Collections_IList_Item_PropertyInfo,
	&t2303____Count_PropertyInfo,
	&t2303____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2308_0_0_0;
static ParameterInfo t2303_m11688_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2308_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11688_GM;
MethodInfo m11688_MI = 
{
	".ctor", (methodPointerType)&m10637_gshared, &t2303_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2303_m11688_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11688_GM};
extern Il2CppType t60_0_0_0;
static ParameterInfo t2303_m11689_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11689_GM;
MethodInfo m11689_MI = 
{
	"System.Collections.Generic.ICollection<T>.Add", (methodPointerType)&m10638_gshared, &t2303_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2303_m11689_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11689_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11690_GM;
MethodInfo m11690_MI = 
{
	"System.Collections.Generic.ICollection<T>.Clear", (methodPointerType)&m10639_gshared, &t2303_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11690_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t60_0_0_0;
static ParameterInfo t2303_m11691_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11691_GM;
MethodInfo m11691_MI = 
{
	"System.Collections.Generic.IList<T>.Insert", (methodPointerType)&m10640_gshared, &t2303_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2303_m11691_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11691_GM};
extern Il2CppType t60_0_0_0;
static ParameterInfo t2303_m11692_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11692_GM;
MethodInfo m11692_MI = 
{
	"System.Collections.Generic.ICollection<T>.Remove", (methodPointerType)&m10641_gshared, &t2303_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2303_m11692_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11692_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2303_m11693_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11693_GM;
MethodInfo m11693_MI = 
{
	"System.Collections.Generic.IList<T>.RemoveAt", (methodPointerType)&m10642_gshared, &t2303_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2303_m11693_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11693_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2303_m11694_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t60_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11694_GM;
MethodInfo m11694_MI = 
{
	"System.Collections.Generic.IList<T>.get_Item", (methodPointerType)&m10643_gshared, &t2303_TI, &t60_0_0_0, RuntimeInvoker_t29_t44, t2303_m11694_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11694_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t60_0_0_0;
static ParameterInfo t2303_m11695_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11695_GM;
MethodInfo m11695_MI = 
{
	"System.Collections.Generic.IList<T>.set_Item", (methodPointerType)&m10644_gshared, &t2303_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2303_m11695_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11695_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11696_GM;
MethodInfo m11696_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10645_gshared, &t2303_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11696_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2303_m11697_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11697_GM;
MethodInfo m11697_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10646_gshared, &t2303_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2303_m11697_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11697_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11698_GM;
MethodInfo m11698_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10647_gshared, &t2303_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11698_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2303_m11699_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11699_GM;
MethodInfo m11699_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10648_gshared, &t2303_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2303_m11699_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11699_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11700_GM;
MethodInfo m11700_MI = 
{
	"System.Collections.IList.Clear", (methodPointerType)&m10649_gshared, &t2303_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 14, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11700_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2303_m11701_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11701_GM;
MethodInfo m11701_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10650_gshared, &t2303_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2303_m11701_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11701_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2303_m11702_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11702_GM;
MethodInfo m11702_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10651_gshared, &t2303_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2303_m11702_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11702_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2303_m11703_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11703_GM;
MethodInfo m11703_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10652_gshared, &t2303_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2303_m11703_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11703_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2303_m11704_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11704_GM;
MethodInfo m11704_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10653_gshared, &t2303_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2303_m11704_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11704_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2303_m11705_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11705_GM;
MethodInfo m11705_MI = 
{
	"System.Collections.IList.RemoveAt", (methodPointerType)&m10654_gshared, &t2303_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2303_m11705_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11705_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11706_GM;
MethodInfo m11706_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10655_gshared, &t2303_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11706_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11707_GM;
MethodInfo m11707_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10656_gshared, &t2303_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11707_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11708_GM;
MethodInfo m11708_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10657_gshared, &t2303_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11708_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11709_GM;
MethodInfo m11709_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10658_gshared, &t2303_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11709_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2303_m11710_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11710_GM;
MethodInfo m11710_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10659_gshared, &t2303_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2303_m11710_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11710_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2303_m11711_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11711_GM;
MethodInfo m11711_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10660_gshared, &t2303_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2303_m11711_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11711_GM};
extern Il2CppType t60_0_0_0;
static ParameterInfo t2303_m11712_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11712_GM;
MethodInfo m11712_MI = 
{
	"Contains", (methodPointerType)&m10661_gshared, &t2303_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2303_m11712_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11712_GM};
extern Il2CppType t2299_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2303_m11713_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2299_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11713_GM;
MethodInfo m11713_MI = 
{
	"CopyTo", (methodPointerType)&m10662_gshared, &t2303_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2303_m11713_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11713_GM};
extern Il2CppType t2300_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11714_GM;
MethodInfo m11714_MI = 
{
	"GetEnumerator", (methodPointerType)&m10663_gshared, &t2303_TI, &t2300_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11714_GM};
extern Il2CppType t60_0_0_0;
static ParameterInfo t2303_m11715_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11715_GM;
MethodInfo m11715_MI = 
{
	"IndexOf", (methodPointerType)&m10664_gshared, &t2303_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2303_m11715_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11715_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11716_GM;
MethodInfo m11716_MI = 
{
	"get_Count", (methodPointerType)&m10665_gshared, &t2303_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11716_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2303_m11717_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t60_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11717_GM;
MethodInfo m11717_MI = 
{
	"get_Item", (methodPointerType)&m10666_gshared, &t2303_TI, &t60_0_0_0, RuntimeInvoker_t29_t44, t2303_m11717_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 33, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11717_GM};
static MethodInfo* t2303_MIs[] =
{
	&m11688_MI,
	&m11689_MI,
	&m11690_MI,
	&m11691_MI,
	&m11692_MI,
	&m11693_MI,
	&m11694_MI,
	&m11695_MI,
	&m11696_MI,
	&m11697_MI,
	&m11698_MI,
	&m11699_MI,
	&m11700_MI,
	&m11701_MI,
	&m11702_MI,
	&m11703_MI,
	&m11704_MI,
	&m11705_MI,
	&m11706_MI,
	&m11707_MI,
	&m11708_MI,
	&m11709_MI,
	&m11710_MI,
	&m11711_MI,
	&m11712_MI,
	&m11713_MI,
	&m11714_MI,
	&m11715_MI,
	&m11716_MI,
	&m11717_MI,
	NULL
};
extern MethodInfo m11698_MI;
extern MethodInfo m11697_MI;
extern MethodInfo m11699_MI;
extern MethodInfo m11700_MI;
extern MethodInfo m11701_MI;
extern MethodInfo m11702_MI;
extern MethodInfo m11703_MI;
extern MethodInfo m11704_MI;
extern MethodInfo m11705_MI;
extern MethodInfo m11689_MI;
extern MethodInfo m11690_MI;
extern MethodInfo m11712_MI;
extern MethodInfo m11713_MI;
extern MethodInfo m11692_MI;
extern MethodInfo m11715_MI;
extern MethodInfo m11691_MI;
extern MethodInfo m11693_MI;
extern MethodInfo m11714_MI;
static MethodInfo* t2303_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m11698_MI,
	&m11716_MI,
	&m11706_MI,
	&m11707_MI,
	&m11697_MI,
	&m11708_MI,
	&m11709_MI,
	&m11710_MI,
	&m11711_MI,
	&m11699_MI,
	&m11700_MI,
	&m11701_MI,
	&m11702_MI,
	&m11703_MI,
	&m11704_MI,
	&m11705_MI,
	&m11716_MI,
	&m11696_MI,
	&m11689_MI,
	&m11690_MI,
	&m11712_MI,
	&m11713_MI,
	&m11692_MI,
	&m11715_MI,
	&m11691_MI,
	&m11693_MI,
	&m11694_MI,
	&m11695_MI,
	&m11714_MI,
	&m11717_MI,
};
static TypeInfo* t2303_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2301_TI,
	&t2308_TI,
	&t2302_TI,
};
static Il2CppInterfaceOffsetPair t2303_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2301_TI, 20},
	{ &t2308_TI, 27},
	{ &t2302_TI, 32},
};
extern TypeInfo t60_TI;
static Il2CppRGCTXData t2303_RGCTXData[9] = 
{
	&m11717_MI/* Method Usage */,
	&m11749_MI/* Method Usage */,
	&t60_TI/* Class Usage */,
	&m27190_MI/* Method Usage */,
	&m27194_MI/* Method Usage */,
	&m27192_MI/* Method Usage */,
	&m27184_MI/* Method Usage */,
	&m27185_MI/* Method Usage */,
	&m27183_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2303_0_0_0;
extern Il2CppType t2303_1_0_0;
struct t2303;
extern Il2CppGenericClass t2303_GC;
extern CustomAttributesCache t1263__CustomAttributeCache;
TypeInfo t2303_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ReadOnlyCollection`1", "System.Collections.ObjectModel", t2303_MIs, t2303_PIs, t2303_FIs, NULL, &t29_TI, NULL, NULL, &t2303_TI, t2303_ITIs, t2303_VT, &t1263__CustomAttributeCache, &t2303_TI, &t2303_0_0_0, &t2303_1_0_0, t2303_IOs, &t2303_GC, NULL, NULL, NULL, t2303_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2303), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 30, 9, 1, 0, 0, 34, 6, 6};
#include "t2309.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2309_TI;

extern MethodInfo m11752_MI;
extern MethodInfo m11753_MI;
extern MethodInfo m11750_MI;
extern MethodInfo m11748_MI;
extern MethodInfo m11630_MI;
extern MethodInfo m9816_MI;
extern MethodInfo m11741_MI;
extern MethodInfo m11751_MI;
extern MethodInfo m11739_MI;
extern MethodInfo m11744_MI;
extern MethodInfo m11735_MI;
extern MethodInfo m27189_MI;
extern MethodInfo m27195_MI;
extern MethodInfo m27196_MI;
extern MethodInfo m6004_MI;
extern MethodInfo m9815_MI;
extern MethodInfo m9817_MI;


// Metadata Definition System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.EventTrigger/Entry>
extern Il2CppType t2308_0_0_1;
FieldInfo t2309_f0_FieldInfo = 
{
	"list", &t2308_0_0_1, &t2309_TI, offsetof(t2309, f0), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t2309_f1_FieldInfo = 
{
	"syncRoot", &t29_0_0_1, &t2309_TI, offsetof(t2309, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2309_FIs[] =
{
	&t2309_f0_FieldInfo,
	&t2309_f1_FieldInfo,
	NULL
};
extern MethodInfo m11719_MI;
static PropertyInfo t2309____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2309_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m11719_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11727_MI;
static PropertyInfo t2309____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2309_TI, "System.Collections.ICollection.IsSynchronized", &m11727_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11728_MI;
static PropertyInfo t2309____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2309_TI, "System.Collections.ICollection.SyncRoot", &m11728_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11729_MI;
static PropertyInfo t2309____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2309_TI, "System.Collections.IList.IsFixedSize", &m11729_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11730_MI;
static PropertyInfo t2309____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2309_TI, "System.Collections.IList.IsReadOnly", &m11730_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11731_MI;
extern MethodInfo m11732_MI;
static PropertyInfo t2309____System_Collections_IList_Item_PropertyInfo = 
{
	&t2309_TI, "System.Collections.IList.Item", &m11731_MI, &m11732_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11745_MI;
static PropertyInfo t2309____Count_PropertyInfo = 
{
	&t2309_TI, "Count", &m11745_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11746_MI;
extern MethodInfo m11747_MI;
static PropertyInfo t2309____Item_PropertyInfo = 
{
	&t2309_TI, "Item", &m11746_MI, &m11747_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2309_PIs[] =
{
	&t2309____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2309____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2309____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2309____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2309____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2309____System_Collections_IList_Item_PropertyInfo,
	&t2309____Count_PropertyInfo,
	&t2309____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11718_GM;
MethodInfo m11718_MI = 
{
	".ctor", (methodPointerType)&m10667_gshared, &t2309_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11718_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11719_GM;
MethodInfo m11719_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10668_gshared, &t2309_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11719_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2309_m11720_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11720_GM;
MethodInfo m11720_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10669_gshared, &t2309_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2309_m11720_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11720_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11721_GM;
MethodInfo m11721_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10670_gshared, &t2309_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11721_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2309_m11722_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11722_GM;
MethodInfo m11722_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10671_gshared, &t2309_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2309_m11722_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11722_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2309_m11723_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11723_GM;
MethodInfo m11723_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10672_gshared, &t2309_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2309_m11723_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11723_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2309_m11724_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11724_GM;
MethodInfo m11724_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10673_gshared, &t2309_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2309_m11724_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11724_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2309_m11725_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11725_GM;
MethodInfo m11725_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10674_gshared, &t2309_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2309_m11725_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11725_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2309_m11726_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11726_GM;
MethodInfo m11726_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10675_gshared, &t2309_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2309_m11726_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11726_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11727_GM;
MethodInfo m11727_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10676_gshared, &t2309_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11727_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11728_GM;
MethodInfo m11728_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10677_gshared, &t2309_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11728_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11729_GM;
MethodInfo m11729_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10678_gshared, &t2309_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11729_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11730_GM;
MethodInfo m11730_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10679_gshared, &t2309_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11730_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2309_m11731_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11731_GM;
MethodInfo m11731_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10680_gshared, &t2309_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2309_m11731_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11731_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2309_m11732_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11732_GM;
MethodInfo m11732_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10681_gshared, &t2309_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2309_m11732_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11732_GM};
extern Il2CppType t60_0_0_0;
static ParameterInfo t2309_m11733_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11733_GM;
MethodInfo m11733_MI = 
{
	"Add", (methodPointerType)&m10682_gshared, &t2309_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2309_m11733_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11733_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11734_GM;
MethodInfo m11734_MI = 
{
	"Clear", (methodPointerType)&m10683_gshared, &t2309_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11734_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11735_GM;
MethodInfo m11735_MI = 
{
	"ClearItems", (methodPointerType)&m10684_gshared, &t2309_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 452, 0, 33, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11735_GM};
extern Il2CppType t60_0_0_0;
static ParameterInfo t2309_m11736_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11736_GM;
MethodInfo m11736_MI = 
{
	"Contains", (methodPointerType)&m10685_gshared, &t2309_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2309_m11736_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11736_GM};
extern Il2CppType t2299_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2309_m11737_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2299_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11737_GM;
MethodInfo m11737_MI = 
{
	"CopyTo", (methodPointerType)&m10686_gshared, &t2309_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2309_m11737_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11737_GM};
extern Il2CppType t2300_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11738_GM;
MethodInfo m11738_MI = 
{
	"GetEnumerator", (methodPointerType)&m10687_gshared, &t2309_TI, &t2300_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11738_GM};
extern Il2CppType t60_0_0_0;
static ParameterInfo t2309_m11739_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11739_GM;
MethodInfo m11739_MI = 
{
	"IndexOf", (methodPointerType)&m10688_gshared, &t2309_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2309_m11739_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11739_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t60_0_0_0;
static ParameterInfo t2309_m11740_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11740_GM;
MethodInfo m11740_MI = 
{
	"Insert", (methodPointerType)&m10689_gshared, &t2309_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2309_m11740_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11740_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t60_0_0_0;
static ParameterInfo t2309_m11741_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11741_GM;
MethodInfo m11741_MI = 
{
	"InsertItem", (methodPointerType)&m10690_gshared, &t2309_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2309_m11741_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 34, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11741_GM};
extern Il2CppType t60_0_0_0;
static ParameterInfo t2309_m11742_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11742_GM;
MethodInfo m11742_MI = 
{
	"Remove", (methodPointerType)&m10691_gshared, &t2309_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2309_m11742_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11742_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2309_m11743_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11743_GM;
MethodInfo m11743_MI = 
{
	"RemoveAt", (methodPointerType)&m10692_gshared, &t2309_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2309_m11743_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11743_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2309_m11744_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11744_GM;
MethodInfo m11744_MI = 
{
	"RemoveItem", (methodPointerType)&m10693_gshared, &t2309_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2309_m11744_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 35, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11744_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11745_GM;
MethodInfo m11745_MI = 
{
	"get_Count", (methodPointerType)&m10694_gshared, &t2309_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11745_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2309_m11746_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t60_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11746_GM;
MethodInfo m11746_MI = 
{
	"get_Item", (methodPointerType)&m10695_gshared, &t2309_TI, &t60_0_0_0, RuntimeInvoker_t29_t44, t2309_m11746_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11746_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t60_0_0_0;
static ParameterInfo t2309_m11747_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11747_GM;
MethodInfo m11747_MI = 
{
	"set_Item", (methodPointerType)&m10696_gshared, &t2309_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2309_m11747_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11747_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t60_0_0_0;
static ParameterInfo t2309_m11748_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11748_GM;
MethodInfo m11748_MI = 
{
	"SetItem", (methodPointerType)&m10697_gshared, &t2309_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2309_m11748_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 36, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11748_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2309_m11749_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11749_GM;
MethodInfo m11749_MI = 
{
	"IsValidItem", (methodPointerType)&m10698_gshared, &t2309_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2309_m11749_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11749_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2309_m11750_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t60_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11750_GM;
MethodInfo m11750_MI = 
{
	"ConvertItem", (methodPointerType)&m10699_gshared, &t2309_TI, &t60_0_0_0, RuntimeInvoker_t29_t29, t2309_m11750_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11750_GM};
extern Il2CppType t2308_0_0_0;
static ParameterInfo t2309_m11751_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2308_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11751_GM;
MethodInfo m11751_MI = 
{
	"CheckWritable", (methodPointerType)&m10700_gshared, &t2309_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2309_m11751_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11751_GM};
extern Il2CppType t2308_0_0_0;
static ParameterInfo t2309_m11752_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2308_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11752_GM;
MethodInfo m11752_MI = 
{
	"IsSynchronized", (methodPointerType)&m10701_gshared, &t2309_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2309_m11752_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11752_GM};
extern Il2CppType t2308_0_0_0;
static ParameterInfo t2309_m11753_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2308_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11753_GM;
MethodInfo m11753_MI = 
{
	"IsFixedSize", (methodPointerType)&m10702_gshared, &t2309_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2309_m11753_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11753_GM};
static MethodInfo* t2309_MIs[] =
{
	&m11718_MI,
	&m11719_MI,
	&m11720_MI,
	&m11721_MI,
	&m11722_MI,
	&m11723_MI,
	&m11724_MI,
	&m11725_MI,
	&m11726_MI,
	&m11727_MI,
	&m11728_MI,
	&m11729_MI,
	&m11730_MI,
	&m11731_MI,
	&m11732_MI,
	&m11733_MI,
	&m11734_MI,
	&m11735_MI,
	&m11736_MI,
	&m11737_MI,
	&m11738_MI,
	&m11739_MI,
	&m11740_MI,
	&m11741_MI,
	&m11742_MI,
	&m11743_MI,
	&m11744_MI,
	&m11745_MI,
	&m11746_MI,
	&m11747_MI,
	&m11748_MI,
	&m11749_MI,
	&m11750_MI,
	&m11751_MI,
	&m11752_MI,
	&m11753_MI,
	NULL
};
extern MethodInfo m11721_MI;
extern MethodInfo m11720_MI;
extern MethodInfo m11722_MI;
extern MethodInfo m11734_MI;
extern MethodInfo m11723_MI;
extern MethodInfo m11724_MI;
extern MethodInfo m11725_MI;
extern MethodInfo m11726_MI;
extern MethodInfo m11743_MI;
extern MethodInfo m11733_MI;
extern MethodInfo m11736_MI;
extern MethodInfo m11737_MI;
extern MethodInfo m11742_MI;
extern MethodInfo m11740_MI;
extern MethodInfo m11738_MI;
static MethodInfo* t2309_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m11721_MI,
	&m11745_MI,
	&m11727_MI,
	&m11728_MI,
	&m11720_MI,
	&m11729_MI,
	&m11730_MI,
	&m11731_MI,
	&m11732_MI,
	&m11722_MI,
	&m11734_MI,
	&m11723_MI,
	&m11724_MI,
	&m11725_MI,
	&m11726_MI,
	&m11743_MI,
	&m11745_MI,
	&m11719_MI,
	&m11733_MI,
	&m11734_MI,
	&m11736_MI,
	&m11737_MI,
	&m11742_MI,
	&m11739_MI,
	&m11740_MI,
	&m11743_MI,
	&m11746_MI,
	&m11747_MI,
	&m11738_MI,
	&m11735_MI,
	&m11741_MI,
	&m11744_MI,
	&m11748_MI,
};
static TypeInfo* t2309_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2301_TI,
	&t2308_TI,
	&t2302_TI,
};
static Il2CppInterfaceOffsetPair t2309_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2301_TI, 20},
	{ &t2308_TI, 27},
	{ &t2302_TI, 32},
};
extern TypeInfo t63_TI;
extern TypeInfo t60_TI;
static Il2CppRGCTXData t2309_RGCTXData[25] = 
{
	&t63_TI/* Class Usage */,
	&m11630_MI/* Method Usage */,
	&m27187_MI/* Method Usage */,
	&m27185_MI/* Method Usage */,
	&m27183_MI/* Method Usage */,
	&m11750_MI/* Method Usage */,
	&m11741_MI/* Method Usage */,
	&m11749_MI/* Method Usage */,
	&t60_TI/* Class Usage */,
	&m27190_MI/* Method Usage */,
	&m27194_MI/* Method Usage */,
	&m11751_MI/* Method Usage */,
	&m11739_MI/* Method Usage */,
	&m11744_MI/* Method Usage */,
	&m11752_MI/* Method Usage */,
	&m11753_MI/* Method Usage */,
	&m27192_MI/* Method Usage */,
	&m11748_MI/* Method Usage */,
	&m11735_MI/* Method Usage */,
	&m27189_MI/* Method Usage */,
	&m27184_MI/* Method Usage */,
	&m27195_MI/* Method Usage */,
	&m27196_MI/* Method Usage */,
	&m27193_MI/* Method Usage */,
	&t60_0_0_0/* Type Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2309_0_0_0;
extern Il2CppType t2309_1_0_0;
struct t2309;
extern Il2CppGenericClass t2309_GC;
extern CustomAttributesCache t1262__CustomAttributeCache;
TypeInfo t2309_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Collection`1", "System.Collections.ObjectModel", t2309_MIs, t2309_PIs, t2309_FIs, NULL, &t29_TI, NULL, NULL, &t2309_TI, t2309_ITIs, t2309_VT, &t1262__CustomAttributeCache, &t2309_TI, &t2309_0_0_0, &t2309_1_0_0, t2309_IOs, &t2309_GC, NULL, NULL, NULL, t2309_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2309), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 36, 8, 2, 0, 0, 37, 6, 6};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2310_TI;
#include "t2310MD.h"

#include "t1257.h"
#include "t2311.h"
extern TypeInfo t6638_TI;
extern TypeInfo t1257_TI;
extern TypeInfo t537_TI;
extern TypeInfo t2311_TI;
#include "t931MD.h"
#include "t2311MD.h"
extern Il2CppType t6638_0_0_0;
extern Il2CppType t1257_0_0_0;
extern MethodInfo m2981_MI;
extern MethodInfo m2979_MI;
extern MethodInfo m8828_MI;
extern MethodInfo m11759_MI;
extern MethodInfo m27197_MI;
extern MethodInfo m20319_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.EventTrigger/Entry>
extern Il2CppType t2310_0_0_49;
FieldInfo t2310_f0_FieldInfo = 
{
	"_default", &t2310_0_0_49, &t2310_TI, offsetof(t2310_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2310_FIs[] =
{
	&t2310_f0_FieldInfo,
	NULL
};
extern MethodInfo m11758_MI;
static PropertyInfo t2310____Default_PropertyInfo = 
{
	&t2310_TI, "Default", &m11758_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2310_PIs[] =
{
	&t2310____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11754_GM;
MethodInfo m11754_MI = 
{
	".ctor", (methodPointerType)&m10703_gshared, &t2310_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11754_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11755_GM;
MethodInfo m11755_MI = 
{
	".cctor", (methodPointerType)&m10704_gshared, &t2310_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11755_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2310_m11756_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11756_GM;
MethodInfo m11756_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m10705_gshared, &t2310_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2310_m11756_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11756_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2310_m11757_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11757_GM;
MethodInfo m11757_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m10706_gshared, &t2310_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2310_m11757_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11757_GM};
extern Il2CppType t60_0_0_0;
static ParameterInfo t2310_m27197_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27197_GM;
MethodInfo m27197_MI = 
{
	"GetHashCode", NULL, &t2310_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2310_m27197_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27197_GM};
extern Il2CppType t60_0_0_0;
extern Il2CppType t60_0_0_0;
static ParameterInfo t2310_m20319_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m20319_GM;
MethodInfo m20319_MI = 
{
	"Equals", NULL, &t2310_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2310_m20319_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m20319_GM};
extern Il2CppType t2310_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11758_GM;
MethodInfo m11758_MI = 
{
	"get_Default", (methodPointerType)&m10707_gshared, &t2310_TI, &t2310_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11758_GM};
static MethodInfo* t2310_MIs[] =
{
	&m11754_MI,
	&m11755_MI,
	&m11756_MI,
	&m11757_MI,
	&m27197_MI,
	&m20319_MI,
	&m11758_MI,
	NULL
};
extern MethodInfo m11757_MI;
extern MethodInfo m11756_MI;
static MethodInfo* t2310_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m20319_MI,
	&m27197_MI,
	&m11757_MI,
	&m11756_MI,
	NULL,
	NULL,
};
extern TypeInfo t6639_TI;
extern TypeInfo t734_TI;
static TypeInfo* t2310_ITIs[] = 
{
	&t6639_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t2310_IOs[] = 
{
	{ &t6639_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2310_TI;
extern TypeInfo t2310_TI;
extern TypeInfo t2311_TI;
extern TypeInfo t60_TI;
static Il2CppRGCTXData t2310_RGCTXData[9] = 
{
	&t6638_0_0_0/* Type Usage */,
	&t60_0_0_0/* Type Usage */,
	&t2310_TI/* Class Usage */,
	&t2310_TI/* Static Usage */,
	&t2311_TI/* Class Usage */,
	&m11759_MI/* Method Usage */,
	&t60_TI/* Class Usage */,
	&m27197_MI/* Method Usage */,
	&m20319_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2310_0_0_0;
extern Il2CppType t2310_1_0_0;
struct t2310;
extern Il2CppGenericClass t2310_GC;
TypeInfo t2310_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t2310_MIs, t2310_PIs, t2310_FIs, NULL, &t29_TI, NULL, NULL, &t2310_TI, t2310_ITIs, t2310_VT, &EmptyCustomAttributesCache, &t2310_TI, &t2310_0_0_0, &t2310_1_0_0, t2310_IOs, &t2310_GC, NULL, NULL, NULL, t2310_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2310), 0, -1, sizeof(t2310_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<UnityEngine.EventSystems.EventTrigger/Entry>
extern Il2CppType t60_0_0_0;
extern Il2CppType t60_0_0_0;
static ParameterInfo t6639_m27198_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27198_GM;
MethodInfo m27198_MI = 
{
	"Equals", NULL, &t6639_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t6639_m27198_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27198_GM};
extern Il2CppType t60_0_0_0;
static ParameterInfo t6639_m27199_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27199_GM;
MethodInfo m27199_MI = 
{
	"GetHashCode", NULL, &t6639_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6639_m27199_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27199_GM};
static MethodInfo* t6639_MIs[] =
{
	&m27198_MI,
	&m27199_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6639_0_0_0;
extern Il2CppType t6639_1_0_0;
struct t6639;
extern Il2CppGenericClass t6639_GC;
TypeInfo t6639_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t6639_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6639_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6639_TI, &t6639_0_0_0, &t6639_1_0_0, NULL, &t6639_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IEquatable`1<UnityEngine.EventSystems.EventTrigger/Entry>
extern Il2CppType t60_0_0_0;
static ParameterInfo t6638_m27200_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27200_GM;
MethodInfo m27200_MI = 
{
	"Equals", NULL, &t6638_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6638_m27200_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27200_GM};
static MethodInfo* t6638_MIs[] =
{
	&m27200_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6638_1_0_0;
struct t6638;
extern Il2CppGenericClass t6638_GC;
TypeInfo t6638_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t6638_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6638_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6638_TI, &t6638_0_0_0, &t6638_1_0_0, NULL, &t6638_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m11754_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.EventTrigger/Entry>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11759_GM;
MethodInfo m11759_MI = 
{
	".ctor", (methodPointerType)&m10738_gshared, &t2311_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11759_GM};
extern Il2CppType t60_0_0_0;
static ParameterInfo t2311_m11760_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11760_GM;
MethodInfo m11760_MI = 
{
	"GetHashCode", (methodPointerType)&m10739_gshared, &t2311_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2311_m11760_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11760_GM};
extern Il2CppType t60_0_0_0;
extern Il2CppType t60_0_0_0;
static ParameterInfo t2311_m11761_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11761_GM;
MethodInfo m11761_MI = 
{
	"Equals", (methodPointerType)&m10740_gshared, &t2311_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2311_m11761_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11761_GM};
static MethodInfo* t2311_MIs[] =
{
	&m11759_MI,
	&m11760_MI,
	&m11761_MI,
	NULL
};
extern MethodInfo m11761_MI;
extern MethodInfo m11760_MI;
static MethodInfo* t2311_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m11761_MI,
	&m11760_MI,
	&m11757_MI,
	&m11756_MI,
	&m11760_MI,
	&m11761_MI,
};
static Il2CppInterfaceOffsetPair t2311_IOs[] = 
{
	{ &t6639_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2310_TI;
extern TypeInfo t2310_TI;
extern TypeInfo t2311_TI;
extern TypeInfo t60_TI;
extern TypeInfo t60_TI;
static Il2CppRGCTXData t2311_RGCTXData[11] = 
{
	&t6638_0_0_0/* Type Usage */,
	&t60_0_0_0/* Type Usage */,
	&t2310_TI/* Class Usage */,
	&t2310_TI/* Static Usage */,
	&t2311_TI/* Class Usage */,
	&m11759_MI/* Method Usage */,
	&t60_TI/* Class Usage */,
	&m27197_MI/* Method Usage */,
	&m20319_MI/* Method Usage */,
	&m11754_MI/* Method Usage */,
	&t60_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2311_0_0_0;
extern Il2CppType t2311_1_0_0;
struct t2311;
extern Il2CppGenericClass t2311_GC;
extern TypeInfo t1256_TI;
TypeInfo t2311_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2311_MIs, NULL, NULL, NULL, &t2310_TI, NULL, &t1256_TI, &t2311_TI, NULL, t2311_VT, &EmptyCustomAttributesCache, &t2311_TI, &t2311_0_0_0, &t2311_1_0_0, t2311_IOs, &t2311_GC, NULL, NULL, NULL, t2311_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2311), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Predicate`1<UnityEngine.EventSystems.EventTrigger/Entry>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2304_m11762_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11762_GM;
MethodInfo m11762_MI = 
{
	".ctor", (methodPointerType)&m10741_gshared, &t2304_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2304_m11762_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11762_GM};
extern Il2CppType t60_0_0_0;
static ParameterInfo t2304_m11763_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11763_GM;
MethodInfo m11763_MI = 
{
	"Invoke", (methodPointerType)&m10742_gshared, &t2304_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2304_m11763_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11763_GM};
extern Il2CppType t60_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2304_m11764_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11764_GM;
MethodInfo m11764_MI = 
{
	"BeginInvoke", (methodPointerType)&m10743_gshared, &t2304_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2304_m11764_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m11764_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2304_m11765_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11765_GM;
MethodInfo m11765_MI = 
{
	"EndInvoke", (methodPointerType)&m10744_gshared, &t2304_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2304_m11765_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11765_GM};
static MethodInfo* t2304_MIs[] =
{
	&m11762_MI,
	&m11763_MI,
	&m11764_MI,
	&m11765_MI,
	NULL
};
extern MethodInfo m11764_MI;
extern MethodInfo m11765_MI;
static MethodInfo* t2304_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m11763_MI,
	&m11764_MI,
	&m11765_MI,
};
static Il2CppInterfaceOffsetPair t2304_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2304_1_0_0;
struct t2304;
extern Il2CppGenericClass t2304_GC;
TypeInfo t2304_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Predicate`1", "System", t2304_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2304_TI, NULL, t2304_VT, &EmptyCustomAttributesCache, &t2304_TI, &t2304_0_0_0, &t2304_1_0_0, t2304_IOs, &t2304_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2304), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif

#include "t1247.h"
#include "t2313.h"
extern TypeInfo t4076_TI;
extern TypeInfo t1247_TI;
extern TypeInfo t2313_TI;
#include "t2313MD.h"
extern Il2CppType t4076_0_0_0;
extern Il2CppType t1247_0_0_0;
extern MethodInfo m11770_MI;
extern MethodInfo m27201_MI;
extern MethodInfo m8852_MI;


// Metadata Definition System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.EventTrigger/Entry>
extern Il2CppType t2312_0_0_49;
FieldInfo t2312_f0_FieldInfo = 
{
	"_default", &t2312_0_0_49, &t2312_TI, offsetof(t2312_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2312_FIs[] =
{
	&t2312_f0_FieldInfo,
	NULL
};
static PropertyInfo t2312____Default_PropertyInfo = 
{
	&t2312_TI, "Default", &m11769_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2312_PIs[] =
{
	&t2312____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11766_GM;
MethodInfo m11766_MI = 
{
	".ctor", (methodPointerType)&m10745_gshared, &t2312_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11766_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11767_GM;
MethodInfo m11767_MI = 
{
	".cctor", (methodPointerType)&m10746_gshared, &t2312_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11767_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2312_m11768_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11768_GM;
MethodInfo m11768_MI = 
{
	"System.Collections.IComparer.Compare", (methodPointerType)&m10747_gshared, &t2312_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2312_m11768_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11768_GM};
extern Il2CppType t60_0_0_0;
extern Il2CppType t60_0_0_0;
static ParameterInfo t2312_m27201_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27201_GM;
MethodInfo m27201_MI = 
{
	"Compare", NULL, &t2312_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2312_m27201_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27201_GM};
extern Il2CppType t2312_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11769_GM;
MethodInfo m11769_MI = 
{
	"get_Default", (methodPointerType)&m10748_gshared, &t2312_TI, &t2312_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11769_GM};
static MethodInfo* t2312_MIs[] =
{
	&m11766_MI,
	&m11767_MI,
	&m11768_MI,
	&m27201_MI,
	&m11769_MI,
	NULL
};
extern MethodInfo m11768_MI;
static MethodInfo* t2312_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m27201_MI,
	&m11768_MI,
	NULL,
};
extern TypeInfo t4075_TI;
extern TypeInfo t726_TI;
static TypeInfo* t2312_ITIs[] = 
{
	&t4075_TI,
	&t726_TI,
};
static Il2CppInterfaceOffsetPair t2312_IOs[] = 
{
	{ &t4075_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t2312_TI;
extern TypeInfo t2312_TI;
extern TypeInfo t2313_TI;
extern TypeInfo t60_TI;
static Il2CppRGCTXData t2312_RGCTXData[8] = 
{
	&t4076_0_0_0/* Type Usage */,
	&t60_0_0_0/* Type Usage */,
	&t2312_TI/* Class Usage */,
	&t2312_TI/* Static Usage */,
	&t2313_TI/* Class Usage */,
	&m11770_MI/* Method Usage */,
	&t60_TI/* Class Usage */,
	&m27201_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2312_0_0_0;
extern Il2CppType t2312_1_0_0;
struct t2312;
extern Il2CppGenericClass t2312_GC;
TypeInfo t2312_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparer`1", "System.Collections.Generic", t2312_MIs, t2312_PIs, t2312_FIs, NULL, &t29_TI, NULL, NULL, &t2312_TI, t2312_ITIs, t2312_VT, &EmptyCustomAttributesCache, &t2312_TI, &t2312_0_0_0, &t2312_1_0_0, t2312_IOs, &t2312_GC, NULL, NULL, NULL, t2312_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2312), 0, -1, sizeof(t2312_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 5, 1, 1, 0, 0, 7, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IComparer`1<UnityEngine.EventSystems.EventTrigger/Entry>
extern Il2CppType t60_0_0_0;
extern Il2CppType t60_0_0_0;
static ParameterInfo t4075_m20327_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m20327_GM;
MethodInfo m20327_MI = 
{
	"Compare", NULL, &t4075_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t4075_m20327_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m20327_GM};
static MethodInfo* t4075_MIs[] =
{
	&m20327_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4075_0_0_0;
extern Il2CppType t4075_1_0_0;
struct t4075;
extern Il2CppGenericClass t4075_GC;
TypeInfo t4075_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparer`1", "System.Collections.Generic", t4075_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4075_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4075_TI, &t4075_0_0_0, &t4075_1_0_0, NULL, &t4075_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IComparable`1<UnityEngine.EventSystems.EventTrigger/Entry>
extern Il2CppType t60_0_0_0;
static ParameterInfo t4076_m20328_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m20328_GM;
MethodInfo m20328_MI = 
{
	"CompareTo", NULL, &t4076_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t4076_m20328_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m20328_GM};
static MethodInfo* t4076_MIs[] =
{
	&m20328_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4076_1_0_0;
struct t4076;
extern Il2CppGenericClass t4076_GC;
TypeInfo t4076_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t4076_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4076_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4076_TI, &t4076_0_0_0, &t4076_1_0_0, NULL, &t4076_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo t290_TI;
extern MethodInfo m11766_MI;
extern MethodInfo m20328_MI;
extern MethodInfo m9672_MI;


// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.EventSystems.EventTrigger/Entry>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11770_GM;
MethodInfo m11770_MI = 
{
	".ctor", (methodPointerType)&m10749_gshared, &t2313_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11770_GM};
extern Il2CppType t60_0_0_0;
extern Il2CppType t60_0_0_0;
static ParameterInfo t2313_m11771_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11771_GM;
MethodInfo m11771_MI = 
{
	"Compare", (methodPointerType)&m10750_gshared, &t2313_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2313_m11771_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11771_GM};
static MethodInfo* t2313_MIs[] =
{
	&m11770_MI,
	&m11771_MI,
	NULL
};
extern MethodInfo m11771_MI;
static MethodInfo* t2313_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m11771_MI,
	&m11768_MI,
	&m11771_MI,
};
static Il2CppInterfaceOffsetPair t2313_IOs[] = 
{
	{ &t4075_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t2312_TI;
extern TypeInfo t2312_TI;
extern TypeInfo t2313_TI;
extern TypeInfo t60_TI;
extern TypeInfo t60_TI;
extern TypeInfo t4076_TI;
static Il2CppRGCTXData t2313_RGCTXData[12] = 
{
	&t4076_0_0_0/* Type Usage */,
	&t60_0_0_0/* Type Usage */,
	&t2312_TI/* Class Usage */,
	&t2312_TI/* Static Usage */,
	&t2313_TI/* Class Usage */,
	&m11770_MI/* Method Usage */,
	&t60_TI/* Class Usage */,
	&m27201_MI/* Method Usage */,
	&m11766_MI/* Method Usage */,
	&t60_TI/* Class Usage */,
	&t4076_TI/* Class Usage */,
	&m20328_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2313_0_0_0;
extern Il2CppType t2313_1_0_0;
struct t2313;
extern Il2CppGenericClass t2313_GC;
extern TypeInfo t1246_TI;
TypeInfo t2313_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2313_MIs, NULL, NULL, NULL, &t2312_TI, NULL, &t1246_TI, &t2313_TI, NULL, t2313_VT, &EmptyCustomAttributesCache, &t2313_TI, &t2313_0_0_0, &t2313_1_0_0, t2313_IOs, &t2313_GC, NULL, NULL, NULL, t2313_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2313), 0, -1, 0, 0, -1, 1048835, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 7, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2305_TI;
#include "t2305MD.h"



// Metadata Definition System.Comparison`1<UnityEngine.EventSystems.EventTrigger/Entry>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2305_m11772_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11772_GM;
MethodInfo m11772_MI = 
{
	".ctor", (methodPointerType)&m10796_gshared, &t2305_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2305_m11772_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11772_GM};
extern Il2CppType t60_0_0_0;
extern Il2CppType t60_0_0_0;
static ParameterInfo t2305_m11773_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11773_GM;
MethodInfo m11773_MI = 
{
	"Invoke", (methodPointerType)&m10797_gshared, &t2305_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2305_m11773_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11773_GM};
extern Il2CppType t60_0_0_0;
extern Il2CppType t60_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2305_m11774_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t60_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11774_GM;
MethodInfo m11774_MI = 
{
	"BeginInvoke", (methodPointerType)&m10798_gshared, &t2305_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t2305_m11774_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m11774_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2305_m11775_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11775_GM;
MethodInfo m11775_MI = 
{
	"EndInvoke", (methodPointerType)&m10799_gshared, &t2305_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2305_m11775_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11775_GM};
static MethodInfo* t2305_MIs[] =
{
	&m11772_MI,
	&m11773_MI,
	&m11774_MI,
	&m11775_MI,
	NULL
};
extern MethodInfo m11773_MI;
extern MethodInfo m11774_MI;
extern MethodInfo m11775_MI;
static MethodInfo* t2305_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m11773_MI,
	&m11774_MI,
	&m11775_MI,
};
static Il2CppInterfaceOffsetPair t2305_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2305_1_0_0;
struct t2305;
extern Il2CppGenericClass t2305_GC;
TypeInfo t2305_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparison`1", "System", t2305_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2305_TI, NULL, t2305_VT, &EmptyCustomAttributesCache, &t2305_TI, &t2305_0_0_0, &t2305_1_0_0, t2305_IOs, &t2305_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2305), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t59.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t59_TI;
#include "t59MD.h"

#include "t2314.h"
#include "t53.h"
#include "t302.h"
extern TypeInfo t53_TI;
extern TypeInfo t302_TI;
#include "t566MD.h"
#include "t302MD.h"
extern Il2CppType t53_0_0_0;
extern MethodInfo m2812_MI;
extern MethodInfo m1328_MI;
extern MethodInfo m2817_MI;
extern MethodInfo m2818_MI;
extern MethodInfo m2820_MI;
extern MethodInfo m1329_MI;
extern MethodInfo m1330_MI;
extern MethodInfo m2819_MI;


// Metadata Definition UnityEngine.Events.UnityEvent`1<UnityEngine.EventSystems.BaseEventData>
extern Il2CppType t316_0_0_33;
FieldInfo t59_f4_FieldInfo = 
{
	"m_InvokeArray", &t316_0_0_33, &t59_TI, offsetof(t59, f4), &EmptyCustomAttributesCache};
static FieldInfo* t59_FIs[] =
{
	&t59_f4_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1320_GM;
MethodInfo m1320_MI = 
{
	".ctor", (methodPointerType)&m11776_gshared, &t59_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1320_GM};
extern Il2CppType t2314_0_0_0;
extern Il2CppType t2314_0_0_0;
static ParameterInfo t59_m11777_ParameterInfos[] = 
{
	{"call", 0, 134217728, &EmptyCustomAttributesCache, &t2314_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11777_GM;
MethodInfo m11777_MI = 
{
	"AddListener", (methodPointerType)&m11778_gshared, &t59_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t59_m11777_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11777_GM};
extern Il2CppType t2314_0_0_0;
static ParameterInfo t59_m11779_ParameterInfos[] = 
{
	{"call", 0, 134217728, &EmptyCustomAttributesCache, &t2314_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11779_GM;
MethodInfo m11779_MI = 
{
	"RemoveListener", (methodPointerType)&m11780_gshared, &t59_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t59_m11779_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11779_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t7_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t59_m1326_ParameterInfos[] = 
{
	{"name", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"targetObj", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t557_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1326_GM;
MethodInfo m1326_MI = 
{
	"FindMethod_Impl", (methodPointerType)&m11781_gshared, &t59_TI, &t557_0_0_0, RuntimeInvoker_t29_t29_t29, t59_m1326_ParameterInfos, &EmptyCustomAttributesCache, 196, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1326_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t59_m1327_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t556_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1327_GM;
MethodInfo m1327_MI = 
{
	"GetDelegate", (methodPointerType)&m11782_gshared, &t59_TI, &t556_0_0_0, RuntimeInvoker_t29_t29_t29, t59_m1327_ParameterInfos, &EmptyCustomAttributesCache, 195, 0, 7, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1327_GM};
extern Il2CppType t2314_0_0_0;
static ParameterInfo t59_m1328_ParameterInfos[] = 
{
	{"action", 0, 134217728, &EmptyCustomAttributesCache, &t2314_0_0_0},
};
extern Il2CppType t556_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1328_GM;
MethodInfo m1328_MI = 
{
	"GetDelegate", (methodPointerType)&m11783_gshared, &t59_TI, &t556_0_0_0, RuntimeInvoker_t29_t29, t59_m1328_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1328_GM};
extern Il2CppType t53_0_0_0;
static ParameterInfo t59_m1335_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1335_GM;
MethodInfo m1335_MI = 
{
	"Invoke", (methodPointerType)&m11784_gshared, &t59_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t59_m1335_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1335_GM};
static MethodInfo* t59_MIs[] =
{
	&m1320_MI,
	&m11777_MI,
	&m11779_MI,
	&m1326_MI,
	&m1327_MI,
	&m1328_MI,
	&m1335_MI,
	NULL
};
extern MethodInfo m1323_MI;
extern MethodInfo m1324_MI;
extern MethodInfo m1325_MI;
extern MethodInfo m1326_MI;
extern MethodInfo m1327_MI;
static MethodInfo* t59_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1323_MI,
	&m1324_MI,
	&m1325_MI,
	&m1326_MI,
	&m1327_MI,
};
extern TypeInfo t301_TI;
static Il2CppInterfaceOffsetPair t59_IOs[] = 
{
	{ &t301_TI, 4},
};
extern TypeInfo t302_TI;
extern TypeInfo t53_TI;
static Il2CppRGCTXData t59_RGCTXData[6] = 
{
	&m1328_MI/* Method Usage */,
	&t53_0_0_0/* Type Usage */,
	&t302_TI/* Class Usage */,
	&m1329_MI/* Method Usage */,
	&m1330_MI/* Method Usage */,
	&t53_TI/* Class Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t59_0_0_0;
extern Il2CppType t59_1_0_0;
extern TypeInfo t566_TI;
struct t59;
extern Il2CppGenericClass t59_GC;
TypeInfo t59_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityEvent`1", "UnityEngine.Events", t59_MIs, NULL, t59_FIs, NULL, &t566_TI, NULL, NULL, &t59_TI, NULL, t59_VT, &EmptyCustomAttributesCache, &t59_TI, &t59_0_0_0, &t59_1_0_0, t59_IOs, &t59_GC, NULL, NULL, NULL, t59_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t59), 0, -1, 0, 0, -1, 1056897, 0, false, false, false, false, true, false, false, false, false, false, false, false, 7, 0, 1, 0, 0, 8, 0, 1};
#include "t2315.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2315_TI;
#include "t2315MD.h"

#include "t2120.h"
#include "t2119.h"
extern TypeInfo t2119_TI;
#include "t2119MD.h"
extern MethodInfo m11783_MI;
extern MethodInfo m10321_MI;
extern MethodInfo m10322_MI;


extern MethodInfo m11776_MI;
 void m11776_gshared (t2315 * __this, MethodInfo* method)
{
	{
		__this->f4 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), 1));
		m2812(__this, &m2812_MI);
		return;
	}
}
extern MethodInfo m11778_MI;
 void m11778_gshared (t2315 * __this, t2120 * p0, MethodInfo* method)
{
	{
		t556 * L_0 = (( t556 * (*) (t29 * __this, t2120 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL, p0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		m2817(__this, L_0, &m2817_MI);
		return;
	}
}
extern MethodInfo m11780_MI;
 void m11780_gshared (t2315 * __this, t2120 * p0, MethodInfo* method)
{
	{
		t29 * L_0 = m2953(p0, &m2953_MI);
		t557 * L_1 = m2951(p0, &m2951_MI);
		m2818(__this, L_0, L_1, &m2818_MI);
		return;
	}
}
extern MethodInfo m11781_MI;
 t557 * m11781_gshared (t2315 * __this, t7* p0, t29 * p1, MethodInfo* method)
{
	{
		t537* L_0 = ((t537*)SZArrayNew(InitializedTypeInfo(&t537_TI), 1));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_1 = m1554(NULL, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), &m1554_MI);
		ArrayElementTypeCheck (L_0, L_1);
		*((t42 **)(t42 **)SZArrayLdElema(L_0, 0)) = (t42 *)L_1;
		t557 * L_2 = m2820(NULL, p1, p0, L_0, &m2820_MI);
		return L_2;
	}
}
extern MethodInfo m11782_MI;
 t556 * m11782_gshared (t2315 * __this, t29 * p0, t557 * p1, MethodInfo* method)
{
	{
		t2119 * L_0 = (t2119 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		(( void (*) (t2119 * __this, t29 * p0, t557 * p1, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_0, p0, p1, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_0;
	}
}
 t556 * m11783_gshared (t29 * __this, t2120 * p0, MethodInfo* method)
{
	{
		t2119 * L_0 = (t2119 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		(( void (*) (t2119 * __this, t2120 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_0, p0, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_0;
	}
}
extern MethodInfo m11784_MI;
 void m11784_gshared (t2315 * __this, t29 * p0, MethodInfo* method)
{
	{
		t316* L_0 = (__this->f4);
		t29 * L_1 = p0;
		ArrayElementTypeCheck (L_0, ((t29 *)L_1));
		*((t29 **)(t29 **)SZArrayLdElema(L_0, 0)) = (t29 *)((t29 *)L_1);
		t316* L_2 = (__this->f4);
		m2819(__this, L_2, &m2819_MI);
		return;
	}
}
// Metadata Definition UnityEngine.Events.UnityEvent`1<System.Object>
extern Il2CppType t316_0_0_33;
FieldInfo t2315_f4_FieldInfo = 
{
	"m_InvokeArray", &t316_0_0_33, &t2315_TI, offsetof(t2315, f4), &EmptyCustomAttributesCache};
static FieldInfo* t2315_FIs[] =
{
	&t2315_f4_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11776_GM;
MethodInfo m11776_MI = 
{
	".ctor", (methodPointerType)&m11776_gshared, &t2315_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11776_GM};
extern Il2CppType t2120_0_0_0;
extern Il2CppType t2120_0_0_0;
static ParameterInfo t2315_m11778_ParameterInfos[] = 
{
	{"call", 0, 134217728, &EmptyCustomAttributesCache, &t2120_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11778_GM;
MethodInfo m11778_MI = 
{
	"AddListener", (methodPointerType)&m11778_gshared, &t2315_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2315_m11778_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11778_GM};
extern Il2CppType t2120_0_0_0;
static ParameterInfo t2315_m11780_ParameterInfos[] = 
{
	{"call", 0, 134217728, &EmptyCustomAttributesCache, &t2120_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11780_GM;
MethodInfo m11780_MI = 
{
	"RemoveListener", (methodPointerType)&m11780_gshared, &t2315_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2315_m11780_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11780_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2315_m11781_ParameterInfos[] = 
{
	{"name", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"targetObj", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t557_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11781_GM;
MethodInfo m11781_MI = 
{
	"FindMethod_Impl", (methodPointerType)&m11781_gshared, &t2315_TI, &t557_0_0_0, RuntimeInvoker_t29_t29_t29, t2315_m11781_ParameterInfos, &EmptyCustomAttributesCache, 196, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11781_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2315_m11782_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t556_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11782_GM;
MethodInfo m11782_MI = 
{
	"GetDelegate", (methodPointerType)&m11782_gshared, &t2315_TI, &t556_0_0_0, RuntimeInvoker_t29_t29_t29, t2315_m11782_ParameterInfos, &EmptyCustomAttributesCache, 195, 0, 7, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11782_GM};
extern Il2CppType t2120_0_0_0;
static ParameterInfo t2315_m11783_ParameterInfos[] = 
{
	{"action", 0, 134217728, &EmptyCustomAttributesCache, &t2120_0_0_0},
};
extern Il2CppType t556_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11783_GM;
MethodInfo m11783_MI = 
{
	"GetDelegate", (methodPointerType)&m11783_gshared, &t2315_TI, &t556_0_0_0, RuntimeInvoker_t29_t29, t2315_m11783_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11783_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2315_m11784_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11784_GM;
MethodInfo m11784_MI = 
{
	"Invoke", (methodPointerType)&m11784_gshared, &t2315_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2315_m11784_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11784_GM};
static MethodInfo* t2315_MIs[] =
{
	&m11776_MI,
	&m11778_MI,
	&m11780_MI,
	&m11781_MI,
	&m11782_MI,
	&m11783_MI,
	&m11784_MI,
	NULL
};
static MethodInfo* t2315_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1323_MI,
	&m1324_MI,
	&m1325_MI,
	&m11781_MI,
	&m11782_MI,
};
static Il2CppInterfaceOffsetPair t2315_IOs[] = 
{
	{ &t301_TI, 4},
};
extern TypeInfo t2119_TI;
extern TypeInfo t29_TI;
static Il2CppRGCTXData t2315_RGCTXData[6] = 
{
	&m11783_MI/* Method Usage */,
	&t29_0_0_0/* Type Usage */,
	&t2119_TI/* Class Usage */,
	&m10321_MI/* Method Usage */,
	&m10322_MI/* Method Usage */,
	&t29_TI/* Class Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2315_0_0_0;
extern Il2CppType t2315_1_0_0;
struct t2315;
extern Il2CppGenericClass t2315_GC;
TypeInfo t2315_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityEvent`1", "UnityEngine.Events", t2315_MIs, NULL, t2315_FIs, NULL, &t566_TI, NULL, NULL, &t2315_TI, NULL, t2315_VT, &EmptyCustomAttributesCache, &t2315_TI, &t2315_0_0_0, &t2315_1_0_0, t2315_IOs, &t2315_GC, NULL, NULL, NULL, t2315_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2315), 0, -1, 0, 0, -1, 1056897, 0, false, false, false, false, true, false, false, false, false, false, false, false, 7, 0, 1, 0, 0, 8, 0, 1};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2314_TI;
#include "t2314MD.h"



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.BaseEventData>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2314_m11785_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11785_GM;
MethodInfo m11785_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2314_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2314_m11785_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11785_GM};
extern Il2CppType t53_0_0_0;
static ParameterInfo t2314_m11786_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11786_GM;
MethodInfo m11786_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2314_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2314_m11786_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11786_GM};
extern Il2CppType t53_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2314_m11787_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11787_GM;
MethodInfo m11787_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2314_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2314_m11787_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m11787_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2314_m11788_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11788_GM;
MethodInfo m11788_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2314_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2314_m11788_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11788_GM};
static MethodInfo* t2314_MIs[] =
{
	&m11785_MI,
	&m11786_MI,
	&m11787_MI,
	&m11788_MI,
	NULL
};
extern MethodInfo m11786_MI;
extern MethodInfo m11787_MI;
extern MethodInfo m11788_MI;
static MethodInfo* t2314_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m11786_MI,
	&m11787_MI,
	&m11788_MI,
};
static Il2CppInterfaceOffsetPair t2314_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2314_1_0_0;
struct t2314;
extern Il2CppGenericClass t2314_GC;
TypeInfo t2314_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2314_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2314_TI, NULL, t2314_VT, &EmptyCustomAttributesCache, &t2314_TI, &t2314_0_0_0, &t2314_1_0_0, t2314_IOs, &t2314_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2314), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m20332_MI;
struct t556;
#define m20332(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.BaseEventData>
extern Il2CppType t2314_0_0_1;
FieldInfo t302_f0_FieldInfo = 
{
	"Delegate", &t2314_0_0_1, &t302_TI, offsetof(t302, f0), &EmptyCustomAttributesCache};
static FieldInfo* t302_FIs[] =
{
	&t302_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t302_m1329_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1329_GM;
MethodInfo m1329_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t302_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t302_m1329_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1329_GM};
extern Il2CppType t2314_0_0_0;
static ParameterInfo t302_m1330_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2314_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1330_GM;
MethodInfo m1330_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t302_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t302_m1330_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1330_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t302_m11789_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11789_GM;
MethodInfo m11789_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t302_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t302_m11789_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11789_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t302_m11790_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11790_GM;
MethodInfo m11790_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t302_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t302_m11790_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11790_GM};
static MethodInfo* t302_MIs[] =
{
	&m1329_MI,
	&m1330_MI,
	&m11789_MI,
	&m11790_MI,
	NULL
};
extern MethodInfo m11789_MI;
extern MethodInfo m11790_MI;
static MethodInfo* t302_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m11789_MI,
	&m11790_MI,
};
extern TypeInfo t2314_TI;
extern TypeInfo t53_TI;
static Il2CppRGCTXData t302_RGCTXData[5] = 
{
	&t2314_0_0_0/* Type Usage */,
	&t2314_TI/* Class Usage */,
	&m20332_MI/* Method Usage */,
	&t53_TI/* Class Usage */,
	&m11786_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t302_0_0_0;
extern Il2CppType t302_1_0_0;
struct t302;
extern Il2CppGenericClass t302_GC;
TypeInfo t302_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t302_MIs, NULL, t302_FIs, NULL, &t556_TI, NULL, NULL, &t302_TI, NULL, t302_VT, &EmptyCustomAttributesCache, &t302_TI, &t302_0_0_0, &t302_1_0_0, NULL, &t302_GC, NULL, NULL, NULL, t302_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t302), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4078_TI;

#include "t61.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.EventTriggerType>
extern MethodInfo m27202_MI;
static PropertyInfo t4078____Current_PropertyInfo = 
{
	&t4078_TI, "Current", &m27202_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4078_PIs[] =
{
	&t4078____Current_PropertyInfo,
	NULL
};
extern Il2CppType t61_0_0_0;
extern void* RuntimeInvoker_t61 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27202_GM;
MethodInfo m27202_MI = 
{
	"get_Current", NULL, &t4078_TI, &t61_0_0_0, RuntimeInvoker_t61, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27202_GM};
static MethodInfo* t4078_MIs[] =
{
	&m27202_MI,
	NULL
};
static TypeInfo* t4078_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4078_0_0_0;
extern Il2CppType t4078_1_0_0;
struct t4078;
extern Il2CppGenericClass t4078_GC;
TypeInfo t4078_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4078_MIs, t4078_PIs, NULL, NULL, NULL, NULL, NULL, &t4078_TI, t4078_ITIs, NULL, &EmptyCustomAttributesCache, &t4078_TI, &t4078_0_0_0, &t4078_1_0_0, NULL, &t4078_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2316.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2316_TI;
#include "t2316MD.h"

extern TypeInfo t61_TI;
extern MethodInfo m11795_MI;
extern MethodInfo m20334_MI;
struct t20;
 int32_t m20334 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m11791_MI;
 void m11791 (t2316 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m11792_MI;
 t29 * m11792 (t2316 * __this, MethodInfo* method){
	{
		int32_t L_0 = m11795(__this, &m11795_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t61_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m11793_MI;
 void m11793 (t2316 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m11794_MI;
 bool m11794 (t2316 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m11795 (t2316 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m20334(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m20334_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventTriggerType>
extern Il2CppType t20_0_0_1;
FieldInfo t2316_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2316_TI, offsetof(t2316, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2316_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2316_TI, offsetof(t2316, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2316_FIs[] =
{
	&t2316_f0_FieldInfo,
	&t2316_f1_FieldInfo,
	NULL
};
static PropertyInfo t2316____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2316_TI, "System.Collections.IEnumerator.Current", &m11792_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2316____Current_PropertyInfo = 
{
	&t2316_TI, "Current", &m11795_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2316_PIs[] =
{
	&t2316____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2316____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2316_m11791_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11791_GM;
MethodInfo m11791_MI = 
{
	".ctor", (methodPointerType)&m11791, &t2316_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2316_m11791_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11791_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11792_GM;
MethodInfo m11792_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m11792, &t2316_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11792_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11793_GM;
MethodInfo m11793_MI = 
{
	"Dispose", (methodPointerType)&m11793, &t2316_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11793_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11794_GM;
MethodInfo m11794_MI = 
{
	"MoveNext", (methodPointerType)&m11794, &t2316_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11794_GM};
extern Il2CppType t61_0_0_0;
extern void* RuntimeInvoker_t61 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11795_GM;
MethodInfo m11795_MI = 
{
	"get_Current", (methodPointerType)&m11795, &t2316_TI, &t61_0_0_0, RuntimeInvoker_t61, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11795_GM};
static MethodInfo* t2316_MIs[] =
{
	&m11791_MI,
	&m11792_MI,
	&m11793_MI,
	&m11794_MI,
	&m11795_MI,
	NULL
};
static MethodInfo* t2316_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m11792_MI,
	&m11794_MI,
	&m11793_MI,
	&m11795_MI,
};
static TypeInfo* t2316_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4078_TI,
};
static Il2CppInterfaceOffsetPair t2316_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4078_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2316_0_0_0;
extern Il2CppType t2316_1_0_0;
extern Il2CppGenericClass t2316_GC;
TypeInfo t2316_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2316_MIs, t2316_PIs, t2316_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2316_TI, t2316_ITIs, t2316_VT, &EmptyCustomAttributesCache, &t2316_TI, &t2316_0_0_0, &t2316_1_0_0, t2316_IOs, &t2316_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2316)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5226_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventTriggerType>
extern MethodInfo m27203_MI;
static PropertyInfo t5226____Count_PropertyInfo = 
{
	&t5226_TI, "Count", &m27203_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27204_MI;
static PropertyInfo t5226____IsReadOnly_PropertyInfo = 
{
	&t5226_TI, "IsReadOnly", &m27204_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5226_PIs[] =
{
	&t5226____Count_PropertyInfo,
	&t5226____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27203_GM;
MethodInfo m27203_MI = 
{
	"get_Count", NULL, &t5226_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27203_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27204_GM;
MethodInfo m27204_MI = 
{
	"get_IsReadOnly", NULL, &t5226_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27204_GM};
extern Il2CppType t61_0_0_0;
extern Il2CppType t61_0_0_0;
static ParameterInfo t5226_m27205_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t61_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27205_GM;
MethodInfo m27205_MI = 
{
	"Add", NULL, &t5226_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5226_m27205_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27205_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27206_GM;
MethodInfo m27206_MI = 
{
	"Clear", NULL, &t5226_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27206_GM};
extern Il2CppType t61_0_0_0;
static ParameterInfo t5226_m27207_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t61_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27207_GM;
MethodInfo m27207_MI = 
{
	"Contains", NULL, &t5226_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5226_m27207_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27207_GM};
extern Il2CppType t3819_0_0_0;
extern Il2CppType t3819_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5226_m27208_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3819_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27208_GM;
MethodInfo m27208_MI = 
{
	"CopyTo", NULL, &t5226_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5226_m27208_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27208_GM};
extern Il2CppType t61_0_0_0;
static ParameterInfo t5226_m27209_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t61_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27209_GM;
MethodInfo m27209_MI = 
{
	"Remove", NULL, &t5226_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5226_m27209_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27209_GM};
static MethodInfo* t5226_MIs[] =
{
	&m27203_MI,
	&m27204_MI,
	&m27205_MI,
	&m27206_MI,
	&m27207_MI,
	&m27208_MI,
	&m27209_MI,
	NULL
};
extern TypeInfo t5228_TI;
static TypeInfo* t5226_ITIs[] = 
{
	&t603_TI,
	&t5228_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5226_0_0_0;
extern Il2CppType t5226_1_0_0;
struct t5226;
extern Il2CppGenericClass t5226_GC;
TypeInfo t5226_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5226_MIs, t5226_PIs, NULL, NULL, NULL, NULL, NULL, &t5226_TI, t5226_ITIs, NULL, &EmptyCustomAttributesCache, &t5226_TI, &t5226_0_0_0, &t5226_1_0_0, NULL, &t5226_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.EventTriggerType>
extern Il2CppType t4078_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27210_GM;
MethodInfo m27210_MI = 
{
	"GetEnumerator", NULL, &t5228_TI, &t4078_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27210_GM};
static MethodInfo* t5228_MIs[] =
{
	&m27210_MI,
	NULL
};
static TypeInfo* t5228_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5228_0_0_0;
extern Il2CppType t5228_1_0_0;
struct t5228;
extern Il2CppGenericClass t5228_GC;
TypeInfo t5228_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5228_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5228_TI, t5228_ITIs, NULL, &EmptyCustomAttributesCache, &t5228_TI, &t5228_0_0_0, &t5228_1_0_0, NULL, &t5228_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5227_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.EventTriggerType>
extern MethodInfo m27211_MI;
extern MethodInfo m27212_MI;
static PropertyInfo t5227____Item_PropertyInfo = 
{
	&t5227_TI, "Item", &m27211_MI, &m27212_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5227_PIs[] =
{
	&t5227____Item_PropertyInfo,
	NULL
};
extern Il2CppType t61_0_0_0;
static ParameterInfo t5227_m27213_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t61_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27213_GM;
MethodInfo m27213_MI = 
{
	"IndexOf", NULL, &t5227_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5227_m27213_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27213_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t61_0_0_0;
static ParameterInfo t5227_m27214_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t61_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27214_GM;
MethodInfo m27214_MI = 
{
	"Insert", NULL, &t5227_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5227_m27214_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27214_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5227_m27215_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27215_GM;
MethodInfo m27215_MI = 
{
	"RemoveAt", NULL, &t5227_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5227_m27215_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27215_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5227_m27211_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t61_0_0_0;
extern void* RuntimeInvoker_t61_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27211_GM;
MethodInfo m27211_MI = 
{
	"get_Item", NULL, &t5227_TI, &t61_0_0_0, RuntimeInvoker_t61_t44, t5227_m27211_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27211_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t61_0_0_0;
static ParameterInfo t5227_m27212_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t61_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27212_GM;
MethodInfo m27212_MI = 
{
	"set_Item", NULL, &t5227_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5227_m27212_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27212_GM};
static MethodInfo* t5227_MIs[] =
{
	&m27213_MI,
	&m27214_MI,
	&m27215_MI,
	&m27211_MI,
	&m27212_MI,
	NULL
};
static TypeInfo* t5227_ITIs[] = 
{
	&t603_TI,
	&t5226_TI,
	&t5228_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5227_0_0_0;
extern Il2CppType t5227_1_0_0;
struct t5227;
extern Il2CppGenericClass t5227_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5227_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5227_MIs, t5227_PIs, NULL, NULL, NULL, NULL, NULL, &t5227_TI, t5227_ITIs, NULL, &t1908__CustomAttributeCache, &t5227_TI, &t5227_0_0_0, &t5227_1_0_0, NULL, &t5227_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t69.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t69_TI;
#include "t69MD.h"



// Metadata Definition UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerEnterHandler>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t69_m1340_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1340_GM;
MethodInfo m1340_MI = 
{
	".ctor", (methodPointerType)&m10902_gshared, &t69_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t69_m1340_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1340_GM};
extern Il2CppType t30_0_0_0;
extern Il2CppType t30_0_0_0;
extern Il2CppType t53_0_0_0;
static ParameterInfo t69_m11796_ParameterInfos[] = 
{
	{"handler", 0, 134217728, &EmptyCustomAttributesCache, &t30_0_0_0},
	{"eventData", 1, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11796_GM;
MethodInfo m11796_MI = 
{
	"Invoke", (methodPointerType)&m10904_gshared, &t69_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t69_m11796_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11796_GM};
extern Il2CppType t30_0_0_0;
extern Il2CppType t53_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t69_m11797_ParameterInfos[] = 
{
	{"handler", 0, 134217728, &EmptyCustomAttributesCache, &t30_0_0_0},
	{"eventData", 1, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11797_GM;
MethodInfo m11797_MI = 
{
	"BeginInvoke", (methodPointerType)&m10906_gshared, &t69_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t69_m11797_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m11797_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t69_m11798_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11798_GM;
MethodInfo m11798_MI = 
{
	"EndInvoke", (methodPointerType)&m10908_gshared, &t69_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t69_m11798_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11798_GM};
static MethodInfo* t69_MIs[] =
{
	&m1340_MI,
	&m11796_MI,
	&m11797_MI,
	&m11798_MI,
	NULL
};
extern MethodInfo m11796_MI;
extern MethodInfo m11797_MI;
extern MethodInfo m11798_MI;
static MethodInfo* t69_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m11796_MI,
	&m11797_MI,
	&m11798_MI,
};
static Il2CppInterfaceOffsetPair t69_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t69_0_0_0;
extern Il2CppType t69_1_0_0;
struct t69;
extern Il2CppGenericClass t69_GC;
extern TypeInfo t68_TI;
TypeInfo t69_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "EventFunction`1", "", t69_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t68_TI, &t69_TI, NULL, t69_VT, &EmptyCustomAttributesCache, &t69_TI, &t69_0_0_0, &t69_1_0_0, t69_IOs, &t69_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t69), 0, -1, 0, 0, -1, 258, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t70.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t70_TI;
#include "t70MD.h"



// Metadata Definition UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerExitHandler>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t70_m1341_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1341_GM;
MethodInfo m1341_MI = 
{
	".ctor", (methodPointerType)&m10902_gshared, &t70_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t70_m1341_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1341_GM};
extern Il2CppType t32_0_0_0;
extern Il2CppType t32_0_0_0;
extern Il2CppType t53_0_0_0;
static ParameterInfo t70_m11799_ParameterInfos[] = 
{
	{"handler", 0, 134217728, &EmptyCustomAttributesCache, &t32_0_0_0},
	{"eventData", 1, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11799_GM;
MethodInfo m11799_MI = 
{
	"Invoke", (methodPointerType)&m10904_gshared, &t70_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t70_m11799_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11799_GM};
extern Il2CppType t32_0_0_0;
extern Il2CppType t53_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t70_m11800_ParameterInfos[] = 
{
	{"handler", 0, 134217728, &EmptyCustomAttributesCache, &t32_0_0_0},
	{"eventData", 1, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11800_GM;
MethodInfo m11800_MI = 
{
	"BeginInvoke", (methodPointerType)&m10906_gshared, &t70_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t70_m11800_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m11800_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t70_m11801_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11801_GM;
MethodInfo m11801_MI = 
{
	"EndInvoke", (methodPointerType)&m10908_gshared, &t70_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t70_m11801_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11801_GM};
static MethodInfo* t70_MIs[] =
{
	&m1341_MI,
	&m11799_MI,
	&m11800_MI,
	&m11801_MI,
	NULL
};
extern MethodInfo m11799_MI;
extern MethodInfo m11800_MI;
extern MethodInfo m11801_MI;
static MethodInfo* t70_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m11799_MI,
	&m11800_MI,
	&m11801_MI,
};
static Il2CppInterfaceOffsetPair t70_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t70_0_0_0;
extern Il2CppType t70_1_0_0;
struct t70;
extern Il2CppGenericClass t70_GC;
TypeInfo t70_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "EventFunction`1", "", t70_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t68_TI, &t70_TI, NULL, t70_VT, &EmptyCustomAttributesCache, &t70_TI, &t70_0_0_0, &t70_1_0_0, t70_IOs, &t70_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t70), 0, -1, 0, 0, -1, 258, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t71.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t71_TI;
#include "t71MD.h"



// Metadata Definition UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerDownHandler>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t71_m1342_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1342_GM;
MethodInfo m1342_MI = 
{
	".ctor", (methodPointerType)&m10902_gshared, &t71_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t71_m1342_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1342_GM};
extern Il2CppType t89_0_0_0;
extern Il2CppType t89_0_0_0;
extern Il2CppType t53_0_0_0;
static ParameterInfo t71_m11802_ParameterInfos[] = 
{
	{"handler", 0, 134217728, &EmptyCustomAttributesCache, &t89_0_0_0},
	{"eventData", 1, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11802_GM;
MethodInfo m11802_MI = 
{
	"Invoke", (methodPointerType)&m10904_gshared, &t71_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t71_m11802_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11802_GM};
extern Il2CppType t89_0_0_0;
extern Il2CppType t53_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t71_m11803_ParameterInfos[] = 
{
	{"handler", 0, 134217728, &EmptyCustomAttributesCache, &t89_0_0_0},
	{"eventData", 1, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11803_GM;
MethodInfo m11803_MI = 
{
	"BeginInvoke", (methodPointerType)&m10906_gshared, &t71_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t71_m11803_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m11803_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t71_m11804_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11804_GM;
MethodInfo m11804_MI = 
{
	"EndInvoke", (methodPointerType)&m10908_gshared, &t71_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t71_m11804_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11804_GM};
static MethodInfo* t71_MIs[] =
{
	&m1342_MI,
	&m11802_MI,
	&m11803_MI,
	&m11804_MI,
	NULL
};
extern MethodInfo m11802_MI;
extern MethodInfo m11803_MI;
extern MethodInfo m11804_MI;
static MethodInfo* t71_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m11802_MI,
	&m11803_MI,
	&m11804_MI,
};
static Il2CppInterfaceOffsetPair t71_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t71_0_0_0;
extern Il2CppType t71_1_0_0;
struct t71;
extern Il2CppGenericClass t71_GC;
TypeInfo t71_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "EventFunction`1", "", t71_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t68_TI, &t71_TI, NULL, t71_VT, &EmptyCustomAttributesCache, &t71_TI, &t71_0_0_0, &t71_1_0_0, t71_IOs, &t71_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t71), 0, -1, 0, 0, -1, 258, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t72.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t72_TI;
#include "t72MD.h"



// Metadata Definition UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerUpHandler>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t72_m1343_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1343_GM;
MethodInfo m1343_MI = 
{
	".ctor", (methodPointerType)&m10902_gshared, &t72_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t72_m1343_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1343_GM};
extern Il2CppType t90_0_0_0;
extern Il2CppType t90_0_0_0;
extern Il2CppType t53_0_0_0;
static ParameterInfo t72_m11805_ParameterInfos[] = 
{
	{"handler", 0, 134217728, &EmptyCustomAttributesCache, &t90_0_0_0},
	{"eventData", 1, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11805_GM;
MethodInfo m11805_MI = 
{
	"Invoke", (methodPointerType)&m10904_gshared, &t72_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t72_m11805_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11805_GM};
extern Il2CppType t90_0_0_0;
extern Il2CppType t53_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t72_m11806_ParameterInfos[] = 
{
	{"handler", 0, 134217728, &EmptyCustomAttributesCache, &t90_0_0_0},
	{"eventData", 1, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11806_GM;
MethodInfo m11806_MI = 
{
	"BeginInvoke", (methodPointerType)&m10906_gshared, &t72_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t72_m11806_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m11806_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t72_m11807_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11807_GM;
MethodInfo m11807_MI = 
{
	"EndInvoke", (methodPointerType)&m10908_gshared, &t72_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t72_m11807_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11807_GM};
static MethodInfo* t72_MIs[] =
{
	&m1343_MI,
	&m11805_MI,
	&m11806_MI,
	&m11807_MI,
	NULL
};
extern MethodInfo m11805_MI;
extern MethodInfo m11806_MI;
extern MethodInfo m11807_MI;
static MethodInfo* t72_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m11805_MI,
	&m11806_MI,
	&m11807_MI,
};
static Il2CppInterfaceOffsetPair t72_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t72_0_0_0;
extern Il2CppType t72_1_0_0;
struct t72;
extern Il2CppGenericClass t72_GC;
TypeInfo t72_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "EventFunction`1", "", t72_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t68_TI, &t72_TI, NULL, t72_VT, &EmptyCustomAttributesCache, &t72_TI, &t72_0_0_0, &t72_1_0_0, t72_IOs, &t72_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t72), 0, -1, 0, 0, -1, 258, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t73.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t73_TI;
#include "t73MD.h"



// Metadata Definition UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerClickHandler>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t73_m1344_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1344_GM;
MethodInfo m1344_MI = 
{
	".ctor", (methodPointerType)&m10902_gshared, &t73_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t73_m1344_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1344_GM};
extern Il2CppType t91_0_0_0;
extern Il2CppType t91_0_0_0;
extern Il2CppType t53_0_0_0;
static ParameterInfo t73_m11808_ParameterInfos[] = 
{
	{"handler", 0, 134217728, &EmptyCustomAttributesCache, &t91_0_0_0},
	{"eventData", 1, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11808_GM;
MethodInfo m11808_MI = 
{
	"Invoke", (methodPointerType)&m10904_gshared, &t73_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t73_m11808_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11808_GM};
extern Il2CppType t91_0_0_0;
extern Il2CppType t53_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t73_m11809_ParameterInfos[] = 
{
	{"handler", 0, 134217728, &EmptyCustomAttributesCache, &t91_0_0_0},
	{"eventData", 1, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11809_GM;
MethodInfo m11809_MI = 
{
	"BeginInvoke", (methodPointerType)&m10906_gshared, &t73_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t73_m11809_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m11809_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t73_m11810_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11810_GM;
MethodInfo m11810_MI = 
{
	"EndInvoke", (methodPointerType)&m10908_gshared, &t73_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t73_m11810_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11810_GM};
static MethodInfo* t73_MIs[] =
{
	&m1344_MI,
	&m11808_MI,
	&m11809_MI,
	&m11810_MI,
	NULL
};
extern MethodInfo m11808_MI;
extern MethodInfo m11809_MI;
extern MethodInfo m11810_MI;
static MethodInfo* t73_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m11808_MI,
	&m11809_MI,
	&m11810_MI,
};
static Il2CppInterfaceOffsetPair t73_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t73_0_0_0;
extern Il2CppType t73_1_0_0;
struct t73;
extern Il2CppGenericClass t73_GC;
TypeInfo t73_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "EventFunction`1", "", t73_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t68_TI, &t73_TI, NULL, t73_VT, &EmptyCustomAttributesCache, &t73_TI, &t73_0_0_0, &t73_1_0_0, t73_IOs, &t73_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t73), 0, -1, 0, 0, -1, 258, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t74.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t74_TI;
#include "t74MD.h"



// Metadata Definition UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t74_m1345_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1345_GM;
MethodInfo m1345_MI = 
{
	".ctor", (methodPointerType)&m10902_gshared, &t74_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t74_m1345_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1345_GM};
extern Il2CppType t92_0_0_0;
extern Il2CppType t53_0_0_0;
static ParameterInfo t74_m11811_ParameterInfos[] = 
{
	{"handler", 0, 134217728, &EmptyCustomAttributesCache, &t92_0_0_0},
	{"eventData", 1, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11811_GM;
MethodInfo m11811_MI = 
{
	"Invoke", (methodPointerType)&m10904_gshared, &t74_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t74_m11811_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11811_GM};
extern Il2CppType t92_0_0_0;
extern Il2CppType t53_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t74_m11812_ParameterInfos[] = 
{
	{"handler", 0, 134217728, &EmptyCustomAttributesCache, &t92_0_0_0},
	{"eventData", 1, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11812_GM;
MethodInfo m11812_MI = 
{
	"BeginInvoke", (methodPointerType)&m10906_gshared, &t74_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t74_m11812_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m11812_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t74_m11813_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11813_GM;
MethodInfo m11813_MI = 
{
	"EndInvoke", (methodPointerType)&m10908_gshared, &t74_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t74_m11813_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11813_GM};
static MethodInfo* t74_MIs[] =
{
	&m1345_MI,
	&m11811_MI,
	&m11812_MI,
	&m11813_MI,
	NULL
};
extern MethodInfo m11811_MI;
extern MethodInfo m11812_MI;
extern MethodInfo m11813_MI;
static MethodInfo* t74_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m11811_MI,
	&m11812_MI,
	&m11813_MI,
};
static Il2CppInterfaceOffsetPair t74_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t74_0_0_0;
extern Il2CppType t74_1_0_0;
struct t74;
extern Il2CppGenericClass t74_GC;
TypeInfo t74_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "EventFunction`1", "", t74_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t68_TI, &t74_TI, NULL, t74_VT, &EmptyCustomAttributesCache, &t74_TI, &t74_0_0_0, &t74_1_0_0, t74_IOs, &t74_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t74), 0, -1, 0, 0, -1, 258, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t75.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t75_TI;
#include "t75MD.h"



// Metadata Definition UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IBeginDragHandler>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t75_m1346_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1346_GM;
MethodInfo m1346_MI = 
{
	".ctor", (methodPointerType)&m10902_gshared, &t75_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t75_m1346_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1346_GM};
extern Il2CppType t93_0_0_0;
extern Il2CppType t53_0_0_0;
static ParameterInfo t75_m11814_ParameterInfos[] = 
{
	{"handler", 0, 134217728, &EmptyCustomAttributesCache, &t93_0_0_0},
	{"eventData", 1, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11814_GM;
MethodInfo m11814_MI = 
{
	"Invoke", (methodPointerType)&m10904_gshared, &t75_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t75_m11814_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11814_GM};
extern Il2CppType t93_0_0_0;
extern Il2CppType t53_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t75_m11815_ParameterInfos[] = 
{
	{"handler", 0, 134217728, &EmptyCustomAttributesCache, &t93_0_0_0},
	{"eventData", 1, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11815_GM;
MethodInfo m11815_MI = 
{
	"BeginInvoke", (methodPointerType)&m10906_gshared, &t75_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t75_m11815_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m11815_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t75_m11816_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11816_GM;
MethodInfo m11816_MI = 
{
	"EndInvoke", (methodPointerType)&m10908_gshared, &t75_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t75_m11816_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11816_GM};
static MethodInfo* t75_MIs[] =
{
	&m1346_MI,
	&m11814_MI,
	&m11815_MI,
	&m11816_MI,
	NULL
};
extern MethodInfo m11814_MI;
extern MethodInfo m11815_MI;
extern MethodInfo m11816_MI;
static MethodInfo* t75_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m11814_MI,
	&m11815_MI,
	&m11816_MI,
};
static Il2CppInterfaceOffsetPair t75_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t75_0_0_0;
extern Il2CppType t75_1_0_0;
struct t75;
extern Il2CppGenericClass t75_GC;
TypeInfo t75_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "EventFunction`1", "", t75_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t68_TI, &t75_TI, NULL, t75_VT, &EmptyCustomAttributesCache, &t75_TI, &t75_0_0_0, &t75_1_0_0, t75_IOs, &t75_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t75), 0, -1, 0, 0, -1, 258, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t76.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t76_TI;
#include "t76MD.h"



// Metadata Definition UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDragHandler>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t76_m1347_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1347_GM;
MethodInfo m1347_MI = 
{
	".ctor", (methodPointerType)&m10902_gshared, &t76_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t76_m1347_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1347_GM};
extern Il2CppType t94_0_0_0;
extern Il2CppType t53_0_0_0;
static ParameterInfo t76_m11817_ParameterInfos[] = 
{
	{"handler", 0, 134217728, &EmptyCustomAttributesCache, &t94_0_0_0},
	{"eventData", 1, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11817_GM;
MethodInfo m11817_MI = 
{
	"Invoke", (methodPointerType)&m10904_gshared, &t76_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t76_m11817_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11817_GM};
extern Il2CppType t94_0_0_0;
extern Il2CppType t53_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t76_m11818_ParameterInfos[] = 
{
	{"handler", 0, 134217728, &EmptyCustomAttributesCache, &t94_0_0_0},
	{"eventData", 1, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11818_GM;
MethodInfo m11818_MI = 
{
	"BeginInvoke", (methodPointerType)&m10906_gshared, &t76_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t76_m11818_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m11818_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t76_m11819_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11819_GM;
MethodInfo m11819_MI = 
{
	"EndInvoke", (methodPointerType)&m10908_gshared, &t76_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t76_m11819_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11819_GM};
static MethodInfo* t76_MIs[] =
{
	&m1347_MI,
	&m11817_MI,
	&m11818_MI,
	&m11819_MI,
	NULL
};
extern MethodInfo m11817_MI;
extern MethodInfo m11818_MI;
extern MethodInfo m11819_MI;
static MethodInfo* t76_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m11817_MI,
	&m11818_MI,
	&m11819_MI,
};
static Il2CppInterfaceOffsetPair t76_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t76_0_0_0;
extern Il2CppType t76_1_0_0;
struct t76;
extern Il2CppGenericClass t76_GC;
TypeInfo t76_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "EventFunction`1", "", t76_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t68_TI, &t76_TI, NULL, t76_VT, &EmptyCustomAttributesCache, &t76_TI, &t76_0_0_0, &t76_1_0_0, t76_IOs, &t76_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t76), 0, -1, 0, 0, -1, 258, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t77.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t77_TI;
#include "t77MD.h"



// Metadata Definition UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IEndDragHandler>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t77_m1348_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1348_GM;
MethodInfo m1348_MI = 
{
	".ctor", (methodPointerType)&m10902_gshared, &t77_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t77_m1348_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1348_GM};
extern Il2CppType t95_0_0_0;
extern Il2CppType t53_0_0_0;
static ParameterInfo t77_m11820_ParameterInfos[] = 
{
	{"handler", 0, 134217728, &EmptyCustomAttributesCache, &t95_0_0_0},
	{"eventData", 1, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11820_GM;
MethodInfo m11820_MI = 
{
	"Invoke", (methodPointerType)&m10904_gshared, &t77_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t77_m11820_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11820_GM};
extern Il2CppType t95_0_0_0;
extern Il2CppType t53_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t77_m11821_ParameterInfos[] = 
{
	{"handler", 0, 134217728, &EmptyCustomAttributesCache, &t95_0_0_0},
	{"eventData", 1, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11821_GM;
MethodInfo m11821_MI = 
{
	"BeginInvoke", (methodPointerType)&m10906_gshared, &t77_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t77_m11821_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m11821_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t77_m11822_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11822_GM;
MethodInfo m11822_MI = 
{
	"EndInvoke", (methodPointerType)&m10908_gshared, &t77_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t77_m11822_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11822_GM};
static MethodInfo* t77_MIs[] =
{
	&m1348_MI,
	&m11820_MI,
	&m11821_MI,
	&m11822_MI,
	NULL
};
extern MethodInfo m11820_MI;
extern MethodInfo m11821_MI;
extern MethodInfo m11822_MI;
static MethodInfo* t77_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m11820_MI,
	&m11821_MI,
	&m11822_MI,
};
static Il2CppInterfaceOffsetPair t77_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t77_0_0_0;
extern Il2CppType t77_1_0_0;
struct t77;
extern Il2CppGenericClass t77_GC;
TypeInfo t77_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "EventFunction`1", "", t77_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t68_TI, &t77_TI, NULL, t77_VT, &EmptyCustomAttributesCache, &t77_TI, &t77_0_0_0, &t77_1_0_0, t77_IOs, &t77_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t77), 0, -1, 0, 0, -1, 258, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t78.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t78_TI;
#include "t78MD.h"



// Metadata Definition UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDropHandler>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t78_m1349_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1349_GM;
MethodInfo m1349_MI = 
{
	".ctor", (methodPointerType)&m10902_gshared, &t78_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t78_m1349_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1349_GM};
extern Il2CppType t96_0_0_0;
extern Il2CppType t53_0_0_0;
static ParameterInfo t78_m11823_ParameterInfos[] = 
{
	{"handler", 0, 134217728, &EmptyCustomAttributesCache, &t96_0_0_0},
	{"eventData", 1, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11823_GM;
MethodInfo m11823_MI = 
{
	"Invoke", (methodPointerType)&m10904_gshared, &t78_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t78_m11823_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11823_GM};
extern Il2CppType t96_0_0_0;
extern Il2CppType t53_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t78_m11824_ParameterInfos[] = 
{
	{"handler", 0, 134217728, &EmptyCustomAttributesCache, &t96_0_0_0},
	{"eventData", 1, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11824_GM;
MethodInfo m11824_MI = 
{
	"BeginInvoke", (methodPointerType)&m10906_gshared, &t78_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t78_m11824_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m11824_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t78_m11825_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11825_GM;
MethodInfo m11825_MI = 
{
	"EndInvoke", (methodPointerType)&m10908_gshared, &t78_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t78_m11825_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11825_GM};
static MethodInfo* t78_MIs[] =
{
	&m1349_MI,
	&m11823_MI,
	&m11824_MI,
	&m11825_MI,
	NULL
};
extern MethodInfo m11823_MI;
extern MethodInfo m11824_MI;
extern MethodInfo m11825_MI;
static MethodInfo* t78_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m11823_MI,
	&m11824_MI,
	&m11825_MI,
};
static Il2CppInterfaceOffsetPair t78_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t78_0_0_0;
extern Il2CppType t78_1_0_0;
struct t78;
extern Il2CppGenericClass t78_GC;
TypeInfo t78_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "EventFunction`1", "", t78_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t68_TI, &t78_TI, NULL, t78_VT, &EmptyCustomAttributesCache, &t78_TI, &t78_0_0_0, &t78_1_0_0, t78_IOs, &t78_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t78), 0, -1, 0, 0, -1, 258, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t79.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t79_TI;
#include "t79MD.h"



// Metadata Definition UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IScrollHandler>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t79_m1350_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1350_GM;
MethodInfo m1350_MI = 
{
	".ctor", (methodPointerType)&m10902_gshared, &t79_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t79_m1350_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1350_GM};
extern Il2CppType t97_0_0_0;
extern Il2CppType t53_0_0_0;
static ParameterInfo t79_m11826_ParameterInfos[] = 
{
	{"handler", 0, 134217728, &EmptyCustomAttributesCache, &t97_0_0_0},
	{"eventData", 1, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11826_GM;
MethodInfo m11826_MI = 
{
	"Invoke", (methodPointerType)&m10904_gshared, &t79_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t79_m11826_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11826_GM};
extern Il2CppType t97_0_0_0;
extern Il2CppType t53_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t79_m11827_ParameterInfos[] = 
{
	{"handler", 0, 134217728, &EmptyCustomAttributesCache, &t97_0_0_0},
	{"eventData", 1, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11827_GM;
MethodInfo m11827_MI = 
{
	"BeginInvoke", (methodPointerType)&m10906_gshared, &t79_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t79_m11827_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m11827_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t79_m11828_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11828_GM;
MethodInfo m11828_MI = 
{
	"EndInvoke", (methodPointerType)&m10908_gshared, &t79_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t79_m11828_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11828_GM};
static MethodInfo* t79_MIs[] =
{
	&m1350_MI,
	&m11826_MI,
	&m11827_MI,
	&m11828_MI,
	NULL
};
extern MethodInfo m11826_MI;
extern MethodInfo m11827_MI;
extern MethodInfo m11828_MI;
static MethodInfo* t79_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m11826_MI,
	&m11827_MI,
	&m11828_MI,
};
static Il2CppInterfaceOffsetPair t79_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t79_0_0_0;
extern Il2CppType t79_1_0_0;
struct t79;
extern Il2CppGenericClass t79_GC;
TypeInfo t79_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "EventFunction`1", "", t79_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t68_TI, &t79_TI, NULL, t79_VT, &EmptyCustomAttributesCache, &t79_TI, &t79_0_0_0, &t79_1_0_0, t79_IOs, &t79_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t79), 0, -1, 0, 0, -1, 258, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t80.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t80_TI;
#include "t80MD.h"



// Metadata Definition UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IUpdateSelectedHandler>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t80_m1351_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1351_GM;
MethodInfo m1351_MI = 
{
	".ctor", (methodPointerType)&m10902_gshared, &t80_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t80_m1351_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1351_GM};
extern Il2CppType t98_0_0_0;
extern Il2CppType t53_0_0_0;
static ParameterInfo t80_m11829_ParameterInfos[] = 
{
	{"handler", 0, 134217728, &EmptyCustomAttributesCache, &t98_0_0_0},
	{"eventData", 1, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11829_GM;
MethodInfo m11829_MI = 
{
	"Invoke", (methodPointerType)&m10904_gshared, &t80_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t80_m11829_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11829_GM};
extern Il2CppType t98_0_0_0;
extern Il2CppType t53_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t80_m11830_ParameterInfos[] = 
{
	{"handler", 0, 134217728, &EmptyCustomAttributesCache, &t98_0_0_0},
	{"eventData", 1, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11830_GM;
MethodInfo m11830_MI = 
{
	"BeginInvoke", (methodPointerType)&m10906_gshared, &t80_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t80_m11830_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m11830_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t80_m11831_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11831_GM;
MethodInfo m11831_MI = 
{
	"EndInvoke", (methodPointerType)&m10908_gshared, &t80_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t80_m11831_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11831_GM};
static MethodInfo* t80_MIs[] =
{
	&m1351_MI,
	&m11829_MI,
	&m11830_MI,
	&m11831_MI,
	NULL
};
extern MethodInfo m11829_MI;
extern MethodInfo m11830_MI;
extern MethodInfo m11831_MI;
static MethodInfo* t80_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m11829_MI,
	&m11830_MI,
	&m11831_MI,
};
static Il2CppInterfaceOffsetPair t80_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t80_0_0_0;
extern Il2CppType t80_1_0_0;
struct t80;
extern Il2CppGenericClass t80_GC;
TypeInfo t80_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "EventFunction`1", "", t80_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t68_TI, &t80_TI, NULL, t80_VT, &EmptyCustomAttributesCache, &t80_TI, &t80_0_0_0, &t80_1_0_0, t80_IOs, &t80_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t80), 0, -1, 0, 0, -1, 258, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t83.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t83_TI;
#include "t83MD.h"



// Metadata Definition UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IMoveHandler>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t83_m1354_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1354_GM;
MethodInfo m1354_MI = 
{
	".ctor", (methodPointerType)&m10902_gshared, &t83_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t83_m1354_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1354_GM};
extern Il2CppType t101_0_0_0;
extern Il2CppType t53_0_0_0;
static ParameterInfo t83_m11832_ParameterInfos[] = 
{
	{"handler", 0, 134217728, &EmptyCustomAttributesCache, &t101_0_0_0},
	{"eventData", 1, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11832_GM;
MethodInfo m11832_MI = 
{
	"Invoke", (methodPointerType)&m10904_gshared, &t83_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t83_m11832_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11832_GM};
extern Il2CppType t101_0_0_0;
extern Il2CppType t53_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t83_m11833_ParameterInfos[] = 
{
	{"handler", 0, 134217728, &EmptyCustomAttributesCache, &t101_0_0_0},
	{"eventData", 1, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11833_GM;
MethodInfo m11833_MI = 
{
	"BeginInvoke", (methodPointerType)&m10906_gshared, &t83_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t83_m11833_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m11833_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t83_m11834_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11834_GM;
MethodInfo m11834_MI = 
{
	"EndInvoke", (methodPointerType)&m10908_gshared, &t83_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t83_m11834_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11834_GM};
static MethodInfo* t83_MIs[] =
{
	&m1354_MI,
	&m11832_MI,
	&m11833_MI,
	&m11834_MI,
	NULL
};
extern MethodInfo m11832_MI;
extern MethodInfo m11833_MI;
extern MethodInfo m11834_MI;
static MethodInfo* t83_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m11832_MI,
	&m11833_MI,
	&m11834_MI,
};
static Il2CppInterfaceOffsetPair t83_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t83_0_0_0;
extern Il2CppType t83_1_0_0;
struct t83;
extern Il2CppGenericClass t83_GC;
TypeInfo t83_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "EventFunction`1", "", t83_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t68_TI, &t83_TI, NULL, t83_VT, &EmptyCustomAttributesCache, &t83_TI, &t83_0_0_0, &t83_1_0_0, t83_IOs, &t83_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t83), 0, -1, 0, 0, -1, 258, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
