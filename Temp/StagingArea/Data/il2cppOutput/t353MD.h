﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t353;
struct t557;
struct t29;
struct t42;
struct t7;
struct t733;
struct t1132;
#include "t630.h"
#include "t735.h"

 t557 * m2951 (t353 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m2953 (t353 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t353 * m5843 (t29 * __this, t42 * p0, t29 * p1, t557 * p2, bool p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5844 (t353 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5845 (t29 * __this, t42 * p0, t42 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5846 (t29 * __this, t42 * p0, t42 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t353 * m5847 (t29 * __this, t42 * p0, t29 * p1, t557 * p2, bool p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t353 * m2957 (t29 * __this, t42 * p0, t29 * p1, t557 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t353 * m5848 (t29 * __this, t42 * p0, t557 * p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t353 * m5849 (t29 * __this, t42 * p0, t29 * p1, t7* p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t557 * m5850 (t29 * __this, t42 * p0, t42 * p1, t7* p2, int32_t p3, bool p4, bool p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t353 * m5851 (t29 * __this, t42 * p0, t42 * p1, t7* p2, bool p3, bool p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t353 * m5852 (t29 * __this, t42 * p0, t42 * p1, t7* p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t353 * m5853 (t29 * __this, t42 * p0, t29 * p1, t7* p2, bool p3, bool p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t353 * m5854 (t29 * __this, t42 * p0, t29 * p1, t7* p2, bool p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m1702 (t353 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5855 (t353 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5856 (t353 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5857 (t353 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1132* m5858 (t353 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t353 * m1597 (t29 * __this, t353 * p0, t353 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t353 * m5859 (t29 * __this, t1132* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t353 * m5860 (t353 * __this, t353 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t353 * m1598 (t29 * __this, t353 * p0, t353 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t353 * m5861 (t353 * __this, t353 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
