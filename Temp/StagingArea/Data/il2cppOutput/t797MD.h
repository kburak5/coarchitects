﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t797;
struct t29;
struct t791;
struct t796;

 void m3335 (t797 * __this, t796 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3336 (t797 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3337 (t797 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t791 * m3338 (t797 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3339 (t797 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
