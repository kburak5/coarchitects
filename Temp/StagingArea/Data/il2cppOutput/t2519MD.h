﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2519;
struct t29;
struct t20;
struct t136;
struct t201;
struct t2512;
struct t403;
#include "t184.h"

 void m13337 (t2519 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13338 (t2519 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13339 (t2519 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m13340 (t2519 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m13341 (t2519 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13342 (t2519 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m13343 (t2519 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13344 (t2519 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13345 (t2519 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13346 (t2519 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m13347 (t2519 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13348 (t2519 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13349 (t2519 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m13350 (t2519 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13351 (t2519 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13352 (t2519 * __this, t184  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13353 (t2519 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13354 (t2519 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13355 (t2519 * __this, t184  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13356 (t2519 * __this, t201* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m13357 (t2519 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m13358 (t2519 * __this, t184  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13359 (t2519 * __this, int32_t p0, t184  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13360 (t2519 * __this, int32_t p0, t184  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13361 (t2519 * __this, t184  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13362 (t2519 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13363 (t2519 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m13364 (t2519 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t184  m13365 (t2519 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13366 (t2519 * __this, int32_t p0, t184  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13367 (t2519 * __this, int32_t p0, t184  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13368 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t184  m13369 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13370 (t29 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13371 (t29 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13372 (t29 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
