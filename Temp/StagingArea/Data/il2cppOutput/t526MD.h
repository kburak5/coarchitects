﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t526;
struct t526_marshaled;
#include "t17.h"

 int32_t m2671 (t526 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2672 (t526 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2673 (t526 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2674 (t526 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2675 (t526 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2676 (t526 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2677 (t526 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2678 (t526 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m2679 (t526 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m2680 (t526 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m2681 (t526 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m2682 (t526 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m2683 (t526 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m2684 (t526 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m2685 (t526 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m2686 (t526 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
void t526_marshal(const t526& unmarshaled, t526_marshaled& marshaled);
void t526_marshal_back(const t526_marshaled& marshaled, t526& unmarshaled);
void t526_marshal_cleanup(t526_marshaled& marshaled);
