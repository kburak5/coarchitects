﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1374;
struct t42;
struct t7;
struct t557;
struct t316;
struct t733;
#include "t1367.h"
#include "t735.h"

 void m7578 (t1374 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7579 (t1374 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t557 * m7580 (t1374 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7581 (t1374 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7582 (t1374 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7583 (t1374 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7584 (t1374 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7585 (t1374 * __this, t42 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m7586 (t1374 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m7587 (t1374 * __this, t42 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7588 (t1374 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
