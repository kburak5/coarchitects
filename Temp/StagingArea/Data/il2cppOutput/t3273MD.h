﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3273;
struct t29;
struct t20;

 void m18161 (t3273 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18162 (t3273 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18163 (t3273 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18164 (t3273 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m18165 (t3273 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
