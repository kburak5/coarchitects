﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1431;
struct t7;
struct t29;

 void m7783 (t1431 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7784 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7785 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7786 (t1431 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7787 (t1431 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7788 (t1431 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7789 (t1431 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
