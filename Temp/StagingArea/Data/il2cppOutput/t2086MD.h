﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2086;
#include "t1648.h"

 void m10264 (t2086 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m19422 (t2086 * __this, t1648  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19423 (t2086 * __this, t1648  p0, t1648  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
