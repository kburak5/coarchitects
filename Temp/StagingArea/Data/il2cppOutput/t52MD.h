﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t52;
struct t50;
struct t56;
struct t16;
struct t6;
struct t64;
struct t53;
#include "t57.h"
#include "t106.h"

 void m239 (t52 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t50 * m240 (t52 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m241 (t52 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m242 (t52 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t57  m243 (t29 * __this, t56 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m244 (t29 * __this, float p0, float p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m245 (t29 * __this, float p0, float p1, float p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t16 * m246 (t29 * __this, t16 * p0, t16 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m247 (t52 * __this, t6 * p0, t16 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t64 * m248 (t52 * __this, float p0, float p1, float p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t53 * m249 (t52 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m250 (t52 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m251 (t52 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m252 (t52 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m253 (t52 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m254 (t52 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m255 (t52 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
