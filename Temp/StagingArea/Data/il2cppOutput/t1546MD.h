﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1546;
struct t781;
struct t975;

 void m8377 (t1546 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8378 (t1546 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m8379 (t1546 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8380 (t1546 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8381 (t1546 * __this, t781* p0, uint32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8382 (t29 * __this, t975* p0, t781* p1, uint32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8383 (t29 * __this, t975* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8384 (t1546 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8385 (t1546 * __this, uint64_t p0, t781* p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
