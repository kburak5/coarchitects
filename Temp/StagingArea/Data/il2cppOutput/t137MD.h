﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t137;
struct t7;

 void m357 (t137 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m358 (t137 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m359 (t137 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m360 (t137 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m361 (t137 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m362 (t137 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m363 (t137 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m364 (t137 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m365 (t137 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
