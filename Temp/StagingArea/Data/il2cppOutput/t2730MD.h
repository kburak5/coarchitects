﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2730;
struct t29;
struct t20;
#include "t250.h"

 void m14901 (t2730 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m14902 (t2730 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m14903 (t2730 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m14904 (t2730 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m14905 (t2730 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
