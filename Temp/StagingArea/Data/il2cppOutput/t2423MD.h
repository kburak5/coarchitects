﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2423;
struct t29;
struct t20;
#include "t2420.h"

 void m12531 (t2423 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12532 (t2423 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12533 (t2423 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12534 (t2423 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2420  m12535 (t2423 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
