﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1434;
struct t1424;
struct t1425;

 void m7805 (t1434 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7806 (t1434 * __this, int32_t p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7807 (t1434 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7808 (t1434 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7809 (t1434 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7810 (t1434 * __this, t1425 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7811 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7812 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
