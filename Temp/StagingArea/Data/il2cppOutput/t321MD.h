﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t321;
struct t29;
struct t118;
struct t6;
struct t2356;
struct t20;
struct t136;
struct t2343;
#include "t320.h"

 void m12117 (t321 * __this, t118 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12118 (t321 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12119 (t321 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12120 (t321 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12121 (t321 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m12122 (t321 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12123 (t321 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12124 (t321 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12125 (t321 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12126 (t321 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12127 (t321 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12128 (t321 * __this, t2343* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t320  m1425 (t321 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m12129 (t321 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
