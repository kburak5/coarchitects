﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include "t2866.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t2866_TI;
#include "t2866MD.h"

#include "t21.h"
#include "t29.h"
#include "t35.h"
#include "t469.h"
#include "t44.h"
#include "t67.h"

#include "t20.h"

extern MethodInfo m15586_MI;
 void m15586 (t2866 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m15587_MI;
 t469 * m15587 (t2866 * __this, int32_t p0, t469 * p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m15587((t2866 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef t469 * (*FunctionPointerType) (t29 *, t29 * __this, int32_t p0, t469 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	typedef t469 * (*FunctionPointerType) (t29 * __this, int32_t p0, t469 * p1, MethodInfo* method);
	return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
}
extern MethodInfo m15588_MI;
 t29 * m15588 (t2866 * __this, int32_t p0, t469 * p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = Box(InitializedTypeInfo(&t44_TI), &p0);
	__d_args[1] = p1;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m15589_MI;
 t469 * m15589 (t2866 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return (t469 *)__result;
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache,UnityEngine.GUILayoutUtility/LayoutCache>
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2866_m15586_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15586_GM;
MethodInfo m15586_MI = 
{
	".ctor", (methodPointerType)&m15586, &t2866_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2866_m15586_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15586_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t469_0_0_0;
extern Il2CppType t469_0_0_0;
static ParameterInfo t2866_m15587_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t469_0_0_0},
};
extern Il2CppType t469_0_0_0;
extern void* RuntimeInvoker_t29_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15587_GM;
MethodInfo m15587_MI = 
{
	"Invoke", (methodPointerType)&m15587, &t2866_TI, &t469_0_0_0, RuntimeInvoker_t29_t44_t29, t2866_m15587_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15587_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t469_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2866_m15588_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t469_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t44_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15588_GM;
MethodInfo m15588_MI = 
{
	"BeginInvoke", (methodPointerType)&m15588, &t2866_TI, &t66_0_0_0, RuntimeInvoker_t29_t44_t29_t29_t29, t2866_m15588_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m15588_GM};
extern Il2CppType t66_0_0_0;
extern Il2CppType t66_0_0_0;
static ParameterInfo t2866_m15589_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t469_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15589_GM;
MethodInfo m15589_MI = 
{
	"EndInvoke", (methodPointerType)&m15589, &t2866_TI, &t469_0_0_0, RuntimeInvoker_t29_t29, t2866_m15589_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15589_GM};
static MethodInfo* t2866_MIs[] =
{
	&m15586_MI,
	&m15587_MI,
	&m15588_MI,
	&m15589_MI,
	NULL
};
extern MethodInfo m1699_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1700_MI;
extern MethodInfo m1332_MI;
extern MethodInfo m1701_MI;
extern MethodInfo m1702_MI;
extern MethodInfo m1703_MI;
extern MethodInfo m1704_MI;
extern MethodInfo m1705_MI;
static MethodInfo* t2866_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m15587_MI,
	&m15588_MI,
	&m15589_MI,
};
extern TypeInfo t373_TI;
extern TypeInfo t374_TI;
static Il2CppInterfaceOffsetPair t2866_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2866_0_0_0;
extern Il2CppType t2866_1_0_0;
extern TypeInfo t195_TI;
struct t2866;
extern Il2CppGenericClass t2866_GC;
extern TypeInfo t1254_TI;
TypeInfo t2866_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Transform`1", "", t2866_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1254_TI, &t2866_TI, NULL, t2866_VT, &EmptyCustomAttributesCache, &t2866_TI, &t2866_0_0_0, &t2866_1_0_0, t2866_IOs, &t2866_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2866), 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t2856.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2856_TI;
#include "t2856MD.h"

#include "t725.h"


extern MethodInfo m15590_MI;
 void m15590 (t2856 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m15591_MI;
 t725  m15591 (t2856 * __this, int32_t p0, t469 * p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m15591((t2856 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef t725  (*FunctionPointerType) (t29 *, t29 * __this, int32_t p0, t469 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	typedef t725  (*FunctionPointerType) (t29 * __this, int32_t p0, t469 * p1, MethodInfo* method);
	return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
}
extern MethodInfo m15592_MI;
 t29 * m15592 (t2856 * __this, int32_t p0, t469 * p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = Box(InitializedTypeInfo(&t44_TI), &p0);
	__d_args[1] = p1;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m15593_MI;
 t725  m15593 (t2856 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(t725 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache,System.Collections.DictionaryEntry>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2856_m15590_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15590_GM;
MethodInfo m15590_MI = 
{
	".ctor", (methodPointerType)&m15590, &t2856_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2856_m15590_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15590_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t469_0_0_0;
static ParameterInfo t2856_m15591_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t469_0_0_0},
};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15591_GM;
MethodInfo m15591_MI = 
{
	"Invoke", (methodPointerType)&m15591, &t2856_TI, &t725_0_0_0, RuntimeInvoker_t725_t44_t29, t2856_m15591_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15591_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t469_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2856_m15592_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t469_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t44_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15592_GM;
MethodInfo m15592_MI = 
{
	"BeginInvoke", (methodPointerType)&m15592, &t2856_TI, &t66_0_0_0, RuntimeInvoker_t29_t44_t29_t29_t29, t2856_m15592_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m15592_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2856_m15593_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15593_GM;
MethodInfo m15593_MI = 
{
	"EndInvoke", (methodPointerType)&m15593, &t2856_TI, &t725_0_0_0, RuntimeInvoker_t725_t29, t2856_m15593_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15593_GM};
static MethodInfo* t2856_MIs[] =
{
	&m15590_MI,
	&m15591_MI,
	&m15592_MI,
	&m15593_MI,
	NULL
};
static MethodInfo* t2856_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m15591_MI,
	&m15592_MI,
	&m15593_MI,
};
static Il2CppInterfaceOffsetPair t2856_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2856_0_0_0;
extern Il2CppType t2856_1_0_0;
struct t2856;
extern Il2CppGenericClass t2856_GC;
TypeInfo t2856_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Transform`1", "", t2856_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1254_TI, &t2856_TI, NULL, t2856_VT, &EmptyCustomAttributesCache, &t2856_TI, &t2856_0_0_0, &t2856_1_0_0, t2856_IOs, &t2856_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2856), 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t2867.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2867_TI;
#include "t2867MD.h"

#include "t2859.h"


extern MethodInfo m15594_MI;
 void m15594 (t2867 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m15595_MI;
 t2859  m15595 (t2867 * __this, int32_t p0, t469 * p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m15595((t2867 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef t2859  (*FunctionPointerType) (t29 *, t29 * __this, int32_t p0, t469 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	typedef t2859  (*FunctionPointerType) (t29 * __this, int32_t p0, t469 * p1, MethodInfo* method);
	return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
}
extern MethodInfo m15596_MI;
 t29 * m15596 (t2867 * __this, int32_t p0, t469 * p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = Box(InitializedTypeInfo(&t44_TI), &p0);
	__d_args[1] = p1;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m15597_MI;
 t2859  m15597 (t2867 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(t2859 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache,System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2867_m15594_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15594_GM;
MethodInfo m15594_MI = 
{
	".ctor", (methodPointerType)&m15594, &t2867_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2867_m15594_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15594_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t469_0_0_0;
static ParameterInfo t2867_m15595_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t469_0_0_0},
};
extern Il2CppType t2859_0_0_0;
extern void* RuntimeInvoker_t2859_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15595_GM;
MethodInfo m15595_MI = 
{
	"Invoke", (methodPointerType)&m15595, &t2867_TI, &t2859_0_0_0, RuntimeInvoker_t2859_t44_t29, t2867_m15595_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15595_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t469_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2867_m15596_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t469_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t44_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15596_GM;
MethodInfo m15596_MI = 
{
	"BeginInvoke", (methodPointerType)&m15596, &t2867_TI, &t66_0_0_0, RuntimeInvoker_t29_t44_t29_t29_t29, t2867_m15596_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m15596_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2867_m15597_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t2859_0_0_0;
extern void* RuntimeInvoker_t2859_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15597_GM;
MethodInfo m15597_MI = 
{
	"EndInvoke", (methodPointerType)&m15597, &t2867_TI, &t2859_0_0_0, RuntimeInvoker_t2859_t29, t2867_m15597_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15597_GM};
static MethodInfo* t2867_MIs[] =
{
	&m15594_MI,
	&m15595_MI,
	&m15596_MI,
	&m15597_MI,
	NULL
};
static MethodInfo* t2867_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m15595_MI,
	&m15596_MI,
	&m15597_MI,
};
static Il2CppInterfaceOffsetPair t2867_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2867_0_0_0;
extern Il2CppType t2867_1_0_0;
struct t2867;
extern Il2CppGenericClass t2867_GC;
TypeInfo t2867_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Transform`1", "", t2867_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1254_TI, &t2867_TI, NULL, t2867_VT, &EmptyCustomAttributesCache, &t2867_TI, &t2867_0_0_0, &t2867_1_0_0, t2867_IOs, &t2867_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2867), 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t2868.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2868_TI;
#include "t2868MD.h"

#include "t2861.h"
#include "t472.h"
#include "t40.h"
extern TypeInfo t2861_TI;
extern TypeInfo t722_TI;
extern TypeInfo t725_TI;
extern TypeInfo t2859_TI;
extern TypeInfo t44_TI;
extern TypeInfo t469_TI;
extern TypeInfo t40_TI;
#include "t2861MD.h"
#include "t2859MD.h"
#include "t29MD.h"
#include "t472MD.h"
extern MethodInfo m10122_MI;
extern MethodInfo m15580_MI;
extern MethodInfo m15540_MI;
extern MethodInfo m15542_MI;
extern MethodInfo m15600_MI;
extern MethodInfo m1331_MI;
extern MethodInfo m15537_MI;
extern MethodInfo m15579_MI;


extern MethodInfo m15598_MI;
 void m15598 (t2868 * __this, t472 * p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		t2861  L_0 = m15537(p0, &m15537_MI);
		__this->f0 = L_0;
		return;
	}
}
extern MethodInfo m15599_MI;
 bool m15599 (t2868 * __this, MethodInfo* method){
	{
		t2861 * L_0 = &(__this->f0);
		bool L_1 = m15579(L_0, &m15579_MI);
		return L_1;
	}
}
 t725  m15600 (t2868 * __this, MethodInfo* method){
	{
		t2861  L_0 = (__this->f0);
		t2861  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2861_TI), &L_1);
		t725  L_3 = (t725 )InterfaceFuncInvoker0< t725  >::Invoke(&m10122_MI, L_2);
		return L_3;
	}
}
extern MethodInfo m15601_MI;
 t29 * m15601 (t2868 * __this, MethodInfo* method){
	t2859  V_0 = {0};
	{
		t2861 * L_0 = &(__this->f0);
		t2859  L_1 = m15580(L_0, &m15580_MI);
		V_0 = L_1;
		int32_t L_2 = m15540((&V_0), &m15540_MI);
		int32_t L_3 = L_2;
		t29 * L_4 = Box(InitializedTypeInfo(&t44_TI), &L_3);
		return L_4;
	}
}
extern MethodInfo m15602_MI;
 t29 * m15602 (t2868 * __this, MethodInfo* method){
	t2859  V_0 = {0};
	{
		t2861 * L_0 = &(__this->f0);
		t2859  L_1 = m15580(L_0, &m15580_MI);
		V_0 = L_1;
		t469 * L_2 = m15542((&V_0), &m15542_MI);
		t469 * L_3 = L_2;
		return ((t469 *)L_3);
	}
}
extern MethodInfo m15603_MI;
 t29 * m15603 (t2868 * __this, MethodInfo* method){
	{
		t725  L_0 = (t725 )VirtFuncInvoker0< t725  >::Invoke(&m15600_MI, __this);
		t725  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t725_TI), &L_1);
		return L_2;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>
extern Il2CppType t2861_0_0_1;
FieldInfo t2868_f0_FieldInfo = 
{
	"host_enumerator", &t2861_0_0_1, &t2868_TI, offsetof(t2868, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2868_FIs[] =
{
	&t2868_f0_FieldInfo,
	NULL
};
static PropertyInfo t2868____Entry_PropertyInfo = 
{
	&t2868_TI, "Entry", &m15600_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2868____Key_PropertyInfo = 
{
	&t2868_TI, "Key", &m15601_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2868____Value_PropertyInfo = 
{
	&t2868_TI, "Value", &m15602_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2868____Current_PropertyInfo = 
{
	&t2868_TI, "Current", &m15603_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2868_PIs[] =
{
	&t2868____Entry_PropertyInfo,
	&t2868____Key_PropertyInfo,
	&t2868____Value_PropertyInfo,
	&t2868____Current_PropertyInfo,
	NULL
};
extern Il2CppType t472_0_0_0;
extern Il2CppType t472_0_0_0;
static ParameterInfo t2868_m15598_ParameterInfos[] = 
{
	{"host", 0, 134217728, &EmptyCustomAttributesCache, &t472_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15598_GM;
MethodInfo m15598_MI = 
{
	".ctor", (methodPointerType)&m15598, &t2868_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2868_m15598_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15598_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15599_GM;
MethodInfo m15599_MI = 
{
	"MoveNext", (methodPointerType)&m15599, &t2868_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15599_GM};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15600_GM;
MethodInfo m15600_MI = 
{
	"get_Entry", (methodPointerType)&m15600, &t2868_TI, &t725_0_0_0, RuntimeInvoker_t725, NULL, &EmptyCustomAttributesCache, 2534, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15600_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15601_GM;
MethodInfo m15601_MI = 
{
	"get_Key", (methodPointerType)&m15601, &t2868_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15601_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15602_GM;
MethodInfo m15602_MI = 
{
	"get_Value", (methodPointerType)&m15602, &t2868_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15602_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15603_GM;
MethodInfo m15603_MI = 
{
	"get_Current", (methodPointerType)&m15603, &t2868_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15603_GM};
static MethodInfo* t2868_MIs[] =
{
	&m15598_MI,
	&m15599_MI,
	&m15600_MI,
	&m15601_MI,
	&m15602_MI,
	&m15603_MI,
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m1322_MI;
static MethodInfo* t2868_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15603_MI,
	&m15599_MI,
	&m15600_MI,
	&m15601_MI,
	&m15602_MI,
};
extern TypeInfo t136_TI;
static TypeInfo* t2868_ITIs[] = 
{
	&t136_TI,
	&t722_TI,
};
static Il2CppInterfaceOffsetPair t2868_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t722_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2868_0_0_0;
extern Il2CppType t2868_1_0_0;
extern TypeInfo t29_TI;
struct t2868;
extern Il2CppGenericClass t2868_GC;
TypeInfo t2868_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ShimEnumerator", "", t2868_MIs, t2868_PIs, t2868_FIs, NULL, &t29_TI, NULL, &t1254_TI, &t2868_TI, t2868_ITIs, t2868_VT, &EmptyCustomAttributesCache, &t2868_TI, &t2868_0_0_0, &t2868_1_0_0, t2868_IOs, &t2868_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2868), 0, -1, 0, 0, -1, 1056771, 0, false, false, false, false, true, false, false, false, false, false, false, false, 6, 4, 1, 0, 0, 9, 2, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6710_TI;



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<UnityEngine.GUILayoutUtility/LayoutCache>
extern Il2CppType t469_0_0_0;
extern Il2CppType t469_0_0_0;
static ParameterInfo t6710_m29134_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t469_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t469_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29134_GM;
MethodInfo m29134_MI = 
{
	"Equals", NULL, &t6710_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t6710_m29134_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29134_GM};
extern Il2CppType t469_0_0_0;
static ParameterInfo t6710_m29169_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t469_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29169_GM;
MethodInfo m29169_MI = 
{
	"GetHashCode", NULL, &t6710_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6710_m29169_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29169_GM};
static MethodInfo* t6710_MIs[] =
{
	&m29134_MI,
	&m29169_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6710_0_0_0;
extern Il2CppType t6710_1_0_0;
struct t6710;
extern Il2CppGenericClass t6710_GC;
TypeInfo t6710_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t6710_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6710_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6710_TI, &t6710_0_0_0, &t6710_1_0_0, NULL, &t6710_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#include "t2869.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2869_TI;
#include "t2869MD.h"

#include "t42.h"
#include "t43.h"
#include "t1257.h"
#include "mscorlib_ArrayTypes.h"
#include "t2870.h"
extern TypeInfo t6717_TI;
extern TypeInfo t42_TI;
extern TypeInfo t1257_TI;
extern TypeInfo t537_TI;
extern TypeInfo t2870_TI;
#include "t42MD.h"
#include "t931MD.h"
#include "t2870MD.h"
extern Il2CppType t6717_0_0_0;
extern Il2CppType t1257_0_0_0;
extern MethodInfo m1554_MI;
extern MethodInfo m2981_MI;
extern MethodInfo m2979_MI;
extern MethodInfo m8828_MI;
extern MethodInfo m15609_MI;
extern MethodInfo m29170_MI;
extern MethodInfo m29135_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1<UnityEngine.GUILayoutUtility/LayoutCache>
extern Il2CppType t2869_0_0_49;
FieldInfo t2869_f0_FieldInfo = 
{
	"_default", &t2869_0_0_49, &t2869_TI, offsetof(t2869_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2869_FIs[] =
{
	&t2869_f0_FieldInfo,
	NULL
};
extern MethodInfo m15608_MI;
static PropertyInfo t2869____Default_PropertyInfo = 
{
	&t2869_TI, "Default", &m15608_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2869_PIs[] =
{
	&t2869____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15604_GM;
MethodInfo m15604_MI = 
{
	".ctor", (methodPointerType)&m10703_gshared, &t2869_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15604_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15605_GM;
MethodInfo m15605_MI = 
{
	".cctor", (methodPointerType)&m10704_gshared, &t2869_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15605_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2869_m15606_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15606_GM;
MethodInfo m15606_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m10705_gshared, &t2869_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2869_m15606_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15606_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2869_m15607_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15607_GM;
MethodInfo m15607_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m10706_gshared, &t2869_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2869_m15607_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15607_GM};
extern Il2CppType t469_0_0_0;
static ParameterInfo t2869_m29170_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t469_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29170_GM;
MethodInfo m29170_MI = 
{
	"GetHashCode", NULL, &t2869_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2869_m29170_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29170_GM};
extern Il2CppType t469_0_0_0;
extern Il2CppType t469_0_0_0;
static ParameterInfo t2869_m29135_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t469_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t469_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29135_GM;
MethodInfo m29135_MI = 
{
	"Equals", NULL, &t2869_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2869_m29135_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29135_GM};
extern Il2CppType t2869_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15608_GM;
MethodInfo m15608_MI = 
{
	"get_Default", (methodPointerType)&m10707_gshared, &t2869_TI, &t2869_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15608_GM};
static MethodInfo* t2869_MIs[] =
{
	&m15604_MI,
	&m15605_MI,
	&m15606_MI,
	&m15607_MI,
	&m29170_MI,
	&m29135_MI,
	&m15608_MI,
	NULL
};
extern MethodInfo m15607_MI;
extern MethodInfo m15606_MI;
static MethodInfo* t2869_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m29135_MI,
	&m29170_MI,
	&m15607_MI,
	&m15606_MI,
	NULL,
	NULL,
};
extern TypeInfo t734_TI;
static TypeInfo* t2869_ITIs[] = 
{
	&t6710_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t2869_IOs[] = 
{
	{ &t6710_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2869_TI;
extern TypeInfo t2869_TI;
extern TypeInfo t2870_TI;
extern TypeInfo t469_TI;
static Il2CppRGCTXData t2869_RGCTXData[9] = 
{
	&t6717_0_0_0/* Type Usage */,
	&t469_0_0_0/* Type Usage */,
	&t2869_TI/* Class Usage */,
	&t2869_TI/* Static Usage */,
	&t2870_TI/* Class Usage */,
	&m15609_MI/* Method Usage */,
	&t469_TI/* Class Usage */,
	&m29170_MI/* Method Usage */,
	&m29135_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2869_0_0_0;
extern Il2CppType t2869_1_0_0;
struct t2869;
extern Il2CppGenericClass t2869_GC;
TypeInfo t2869_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t2869_MIs, t2869_PIs, t2869_FIs, NULL, &t29_TI, NULL, NULL, &t2869_TI, t2869_ITIs, t2869_VT, &EmptyCustomAttributesCache, &t2869_TI, &t2869_0_0_0, &t2869_1_0_0, t2869_IOs, &t2869_GC, NULL, NULL, NULL, t2869_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2869), 0, -1, sizeof(t2869_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IEquatable`1<UnityEngine.GUILayoutUtility/LayoutCache>
extern Il2CppType t469_0_0_0;
static ParameterInfo t6717_m29171_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t469_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29171_GM;
MethodInfo m29171_MI = 
{
	"Equals", NULL, &t6717_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6717_m29171_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29171_GM};
static MethodInfo* t6717_MIs[] =
{
	&m29171_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6717_1_0_0;
struct t6717;
extern Il2CppGenericClass t6717_GC;
TypeInfo t6717_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t6717_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6717_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6717_TI, &t6717_0_0_0, &t6717_1_0_0, NULL, &t6717_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m15604_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.GUILayoutUtility/LayoutCache>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15609_GM;
MethodInfo m15609_MI = 
{
	".ctor", (methodPointerType)&m10738_gshared, &t2870_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15609_GM};
extern Il2CppType t469_0_0_0;
static ParameterInfo t2870_m15610_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t469_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15610_GM;
MethodInfo m15610_MI = 
{
	"GetHashCode", (methodPointerType)&m10739_gshared, &t2870_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2870_m15610_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15610_GM};
extern Il2CppType t469_0_0_0;
extern Il2CppType t469_0_0_0;
static ParameterInfo t2870_m15611_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t469_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t469_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15611_GM;
MethodInfo m15611_MI = 
{
	"Equals", (methodPointerType)&m10740_gshared, &t2870_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2870_m15611_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15611_GM};
static MethodInfo* t2870_MIs[] =
{
	&m15609_MI,
	&m15610_MI,
	&m15611_MI,
	NULL
};
extern MethodInfo m15611_MI;
extern MethodInfo m15610_MI;
static MethodInfo* t2870_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15611_MI,
	&m15610_MI,
	&m15607_MI,
	&m15606_MI,
	&m15610_MI,
	&m15611_MI,
};
static Il2CppInterfaceOffsetPair t2870_IOs[] = 
{
	{ &t6710_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2869_TI;
extern TypeInfo t2869_TI;
extern TypeInfo t2870_TI;
extern TypeInfo t469_TI;
extern TypeInfo t469_TI;
static Il2CppRGCTXData t2870_RGCTXData[11] = 
{
	&t6717_0_0_0/* Type Usage */,
	&t469_0_0_0/* Type Usage */,
	&t2869_TI/* Class Usage */,
	&t2869_TI/* Static Usage */,
	&t2870_TI/* Class Usage */,
	&m15609_MI/* Method Usage */,
	&t469_TI/* Class Usage */,
	&m29170_MI/* Method Usage */,
	&m29135_MI/* Method Usage */,
	&m15604_MI/* Method Usage */,
	&t469_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2870_0_0_0;
extern Il2CppType t2870_1_0_0;
struct t2870;
extern Il2CppGenericClass t2870_GC;
extern TypeInfo t1256_TI;
TypeInfo t2870_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2870_MIs, NULL, NULL, NULL, &t2869_TI, NULL, &t1256_TI, &t2870_TI, NULL, t2870_VT, &EmptyCustomAttributesCache, &t2870_TI, &t2870_0_0_0, &t2870_1_0_0, t2870_IOs, &t2870_GC, NULL, NULL, NULL, t2870_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2870), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#include "t588.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t588_TI;
#include "t588MD.h"

#include "t914.h"
#include "t474.h"
#include "t475.h"
#include "t7.h"
#include "t1101.h"
#include "UnityEngine_ArrayTypes.h"
extern TypeInfo t914_TI;
extern TypeInfo t474_TI;
extern TypeInfo t475_TI;
extern TypeInfo t7_TI;
extern TypeInfo t1101_TI;
#include "t914MD.h"
#include "t1101MD.h"
extern MethodInfo m15615_MI;
extern MethodInfo m3974_MI;
extern MethodInfo m1430_MI;
extern MethodInfo m2998_MI;
extern MethodInfo m5150_MI;
extern MethodInfo m3964_MI;


// Metadata Definition System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>
extern Il2CppType t475_0_0_1;
FieldInfo t588_f0_FieldInfo = 
{
	"l", &t475_0_0_1, &t588_TI, offsetof(t588, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t588_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t588_TI, offsetof(t588, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t588_f2_FieldInfo = 
{
	"ver", &t44_0_0_1, &t588_TI, offsetof(t588, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t474_0_0_1;
FieldInfo t588_f3_FieldInfo = 
{
	"current", &t474_0_0_1, &t588_TI, offsetof(t588, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t588_FIs[] =
{
	&t588_f0_FieldInfo,
	&t588_f1_FieldInfo,
	&t588_f2_FieldInfo,
	&t588_f3_FieldInfo,
	NULL
};
extern MethodInfo m15613_MI;
static PropertyInfo t588____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t588_TI, "System.Collections.IEnumerator.Current", &m15613_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m2851_MI;
static PropertyInfo t588____Current_PropertyInfo = 
{
	&t588_TI, "Current", &m2851_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t588_PIs[] =
{
	&t588____System_Collections_IEnumerator_Current_PropertyInfo,
	&t588____Current_PropertyInfo,
	NULL
};
extern Il2CppType t475_0_0_0;
extern Il2CppType t475_0_0_0;
static ParameterInfo t588_m15612_ParameterInfos[] = 
{
	{"l", 0, 134217728, &EmptyCustomAttributesCache, &t475_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15612_GM;
MethodInfo m15612_MI = 
{
	".ctor", (methodPointerType)&m10631_gshared, &t588_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t588_m15612_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15612_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15613_GM;
MethodInfo m15613_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10632_gshared, &t588_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15613_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15614_GM;
MethodInfo m15614_MI = 
{
	"Dispose", (methodPointerType)&m10633_gshared, &t588_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15614_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15615_GM;
MethodInfo m15615_MI = 
{
	"VerifyState", (methodPointerType)&m10634_gshared, &t588_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15615_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2852_GM;
MethodInfo m2852_MI = 
{
	"MoveNext", (methodPointerType)&m10635_gshared, &t588_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m2852_GM};
extern Il2CppType t474_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2851_GM;
MethodInfo m2851_MI = 
{
	"get_Current", (methodPointerType)&m10636_gshared, &t588_TI, &t474_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m2851_GM};
static MethodInfo* t588_MIs[] =
{
	&m15612_MI,
	&m15613_MI,
	&m15614_MI,
	&m15615_MI,
	&m2852_MI,
	&m2851_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
extern MethodInfo m2852_MI;
extern MethodInfo m15614_MI;
static MethodInfo* t588_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15613_MI,
	&m2852_MI,
	&m15614_MI,
	&m2851_MI,
};
extern TypeInfo t324_TI;
extern TypeInfo t2872_TI;
static TypeInfo* t588_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2872_TI,
};
static Il2CppInterfaceOffsetPair t588_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2872_TI, 7},
};
extern TypeInfo t474_TI;
extern TypeInfo t588_TI;
static Il2CppRGCTXData t588_RGCTXData[3] = 
{
	&m15615_MI/* Method Usage */,
	&t474_TI/* Class Usage */,
	&t588_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t588_0_0_0;
extern Il2CppType t588_1_0_0;
extern TypeInfo t110_TI;
extern Il2CppGenericClass t588_GC;
extern TypeInfo t1261_TI;
TypeInfo t588_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t588_MIs, t588_PIs, t588_FIs, NULL, &t110_TI, NULL, &t1261_TI, &t588_TI, t588_ITIs, t588_VT, &EmptyCustomAttributesCache, &t588_TI, &t588_0_0_0, &t588_1_0_0, t588_IOs, &t588_GC, NULL, NULL, NULL, t588_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t588)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 4, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.GUILayoutEntry>
extern MethodInfo m29172_MI;
static PropertyInfo t2872____Current_PropertyInfo = 
{
	&t2872_TI, "Current", &m29172_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2872_PIs[] =
{
	&t2872____Current_PropertyInfo,
	NULL
};
extern Il2CppType t474_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29172_GM;
MethodInfo m29172_MI = 
{
	"get_Current", NULL, &t2872_TI, &t474_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29172_GM};
static MethodInfo* t2872_MIs[] =
{
	&m29172_MI,
	NULL
};
static TypeInfo* t2872_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2872_0_0_0;
extern Il2CppType t2872_1_0_0;
struct t2872;
extern Il2CppGenericClass t2872_GC;
TypeInfo t2872_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2872_MIs, t2872_PIs, NULL, NULL, NULL, NULL, NULL, &t2872_TI, t2872_ITIs, NULL, &EmptyCustomAttributesCache, &t2872_TI, &t2872_0_0_0, &t2872_1_0_0, NULL, &t2872_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif
#include "t475MD.h"

#include "t305.h"
#include "t915.h"
#include "t2875.h"
#include "t2876.h"
#include "t338.h"
#include "t2883.h"
#include "t2877.h"
extern TypeInfo t21_TI;
extern TypeInfo t305_TI;
extern TypeInfo t585_TI;
extern TypeInfo t1649_TI;
extern TypeInfo t915_TI;
extern TypeInfo t2871_TI;
extern TypeInfo t2873_TI;
extern TypeInfo t2874_TI;
extern TypeInfo t2875_TI;
extern TypeInfo t338_TI;
extern TypeInfo t2876_TI;
extern TypeInfo t2883_TI;
#include "t305MD.h"
#include "t915MD.h"
#include "t20MD.h"
#include "t602MD.h"
#include "t2875MD.h"
#include "t338MD.h"
#include "t2876MD.h"
#include "t2883MD.h"
extern MethodInfo m2855_MI;
extern MethodInfo m15660_MI;
extern MethodInfo m1935_MI;
extern MethodInfo m4198_MI;
extern MethodInfo m22137_MI;
extern MethodInfo m3975_MI;
extern MethodInfo m15647_MI;
extern MethodInfo m2850_MI;
extern MethodInfo m5952_MI;
extern MethodInfo m15633_MI;
extern MethodInfo m15640_MI;
extern MethodInfo m15645_MI;
extern MethodInfo m15648_MI;
extern MethodInfo m15650_MI;
extern MethodInfo m15634_MI;
extern MethodInfo m15658_MI;
extern MethodInfo m5139_MI;
extern MethodInfo m15659_MI;
extern MethodInfo m29173_MI;
extern MethodInfo m29174_MI;
extern MethodInfo m29175_MI;
extern MethodInfo m3972_MI;
extern MethodInfo m1428_MI;
extern MethodInfo m15649_MI;
extern MethodInfo m15635_MI;
extern MethodInfo m15636_MI;
extern MethodInfo m15666_MI;
extern MethodInfo m5112_MI;
extern MethodInfo m22139_MI;
extern MethodInfo m15643_MI;
extern MethodInfo m15644_MI;
extern MethodInfo m2950_MI;
extern MethodInfo m15741_MI;
extern MethodInfo m15612_MI;
extern MethodInfo m15646_MI;
extern MethodInfo m15652_MI;
extern MethodInfo m5171_MI;
extern MethodInfo m15747_MI;
extern MethodInfo m22141_MI;
extern MethodInfo m22149_MI;
extern MethodInfo m5951_MI;
struct t20;
struct t20;
 void m19788_gshared (t29 * __this, t316** p0, int32_t p1, MethodInfo* method);
#define m19788(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
#define m22137(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
struct t20;
#include "t2881.h"
struct t20;
#include "t2001.h"
 int32_t m10120_gshared (t29 * __this, t316* p0, t29 * p1, int32_t p2, int32_t p3, MethodInfo* method);
#define m10120(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
#define m22139(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
struct t20;
struct t20;
 void m19857_gshared (t29 * __this, t316* p0, int32_t p1, int32_t p2, t29* p3, MethodInfo* method);
#define m19857(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
#define m22141(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
struct t20;
#include "t295.h"
struct t20;
#include "t2181.h"
 void m19964_gshared (t29 * __this, t316* p0, int32_t p1, t2181 * p2, MethodInfo* method);
#define m19964(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)
#define m22149(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)


 t588  m2850 (t475 * __this, MethodInfo* method){
	{
		t588  L_0 = {0};
		m15612(&L_0, __this, &m15612_MI);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>
extern Il2CppType t44_0_0_32849;
FieldInfo t475_f0_FieldInfo = 
{
	"DefaultCapacity", &t44_0_0_32849, &t475_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t2871_0_0_1;
FieldInfo t475_f1_FieldInfo = 
{
	"_items", &t2871_0_0_1, &t475_TI, offsetof(t475, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t475_f2_FieldInfo = 
{
	"_size", &t44_0_0_1, &t475_TI, offsetof(t475, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t475_f3_FieldInfo = 
{
	"_version", &t44_0_0_1, &t475_TI, offsetof(t475, f3), &EmptyCustomAttributesCache};
extern Il2CppType t2871_0_0_49;
FieldInfo t475_f4_FieldInfo = 
{
	"EmptyArray", &t2871_0_0_49, &t475_TI, offsetof(t475_SFs, f4), &EmptyCustomAttributesCache};
static FieldInfo* t475_FIs[] =
{
	&t475_f0_FieldInfo,
	&t475_f1_FieldInfo,
	&t475_f2_FieldInfo,
	&t475_f3_FieldInfo,
	&t475_f4_FieldInfo,
	NULL
};
static const int32_t t475_f0_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t475_f0_DefaultValue = 
{
	&t475_f0_FieldInfo, { (char*)&t475_f0_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t475_FDVs[] = 
{
	&t475_f0_DefaultValue,
	NULL
};
extern MethodInfo m15626_MI;
static PropertyInfo t475____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t475_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m15626_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15627_MI;
static PropertyInfo t475____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t475_TI, "System.Collections.ICollection.IsSynchronized", &m15627_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15628_MI;
static PropertyInfo t475____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t475_TI, "System.Collections.ICollection.SyncRoot", &m15628_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15629_MI;
static PropertyInfo t475____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t475_TI, "System.Collections.IList.IsFixedSize", &m15629_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15630_MI;
static PropertyInfo t475____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t475_TI, "System.Collections.IList.IsReadOnly", &m15630_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15631_MI;
extern MethodInfo m15632_MI;
static PropertyInfo t475____System_Collections_IList_Item_PropertyInfo = 
{
	&t475_TI, "System.Collections.IList.Item", &m15631_MI, &m15632_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t475____Capacity_PropertyInfo = 
{
	&t475_TI, "Capacity", &m15658_MI, &m15659_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m2854_MI;
static PropertyInfo t475____Count_PropertyInfo = 
{
	&t475_TI, "Count", &m2854_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t475____Item_PropertyInfo = 
{
	&t475_TI, "Item", &m2855_MI, &m15660_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t475_PIs[] =
{
	&t475____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t475____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t475____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t475____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t475____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t475____System_Collections_IList_Item_PropertyInfo,
	&t475____Capacity_PropertyInfo,
	&t475____Count_PropertyInfo,
	&t475____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2853_GM;
MethodInfo m2853_MI = 
{
	".ctor", (methodPointerType)&m10537_gshared, &t475_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m2853_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t475_m15616_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15616_GM;
MethodInfo m15616_MI = 
{
	".ctor", (methodPointerType)&m10539_gshared, &t475_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t475_m15616_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15616_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15617_GM;
MethodInfo m15617_MI = 
{
	".cctor", (methodPointerType)&m10541_gshared, &t475_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15617_GM};
extern Il2CppType t2872_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15618_GM;
MethodInfo m15618_MI = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator", (methodPointerType)&m10543_gshared, &t475_TI, &t2872_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 27, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15618_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t475_m15619_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15619_GM;
MethodInfo m15619_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10545_gshared, &t475_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t475_m15619_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15619_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15620_GM;
MethodInfo m15620_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10547_gshared, &t475_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15620_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t475_m15621_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15621_GM;
MethodInfo m15621_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10549_gshared, &t475_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t475_m15621_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15621_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t475_m15622_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15622_GM;
MethodInfo m15622_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10551_gshared, &t475_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t475_m15622_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15622_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t475_m15623_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15623_GM;
MethodInfo m15623_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10553_gshared, &t475_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t475_m15623_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15623_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t475_m15624_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15624_GM;
MethodInfo m15624_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10555_gshared, &t475_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t475_m15624_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15624_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t475_m15625_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15625_GM;
MethodInfo m15625_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10557_gshared, &t475_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t475_m15625_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15625_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15626_GM;
MethodInfo m15626_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10559_gshared, &t475_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15626_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15627_GM;
MethodInfo m15627_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10561_gshared, &t475_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15627_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15628_GM;
MethodInfo m15628_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10563_gshared, &t475_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15628_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15629_GM;
MethodInfo m15629_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10565_gshared, &t475_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15629_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15630_GM;
MethodInfo m15630_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10567_gshared, &t475_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15630_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t475_m15631_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15631_GM;
MethodInfo m15631_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10569_gshared, &t475_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t475_m15631_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15631_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t475_m15632_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15632_GM;
MethodInfo m15632_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10571_gshared, &t475_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t475_m15632_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15632_GM};
extern Il2CppType t474_0_0_0;
extern Il2CppType t474_0_0_0;
static ParameterInfo t475_m15633_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15633_GM;
MethodInfo m15633_MI = 
{
	"Add", (methodPointerType)&m10573_gshared, &t475_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t475_m15633_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15633_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t475_m15634_ParameterInfos[] = 
{
	{"newCount", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15634_GM;
MethodInfo m15634_MI = 
{
	"GrowIfNeeded", (methodPointerType)&m10575_gshared, &t475_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t475_m15634_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15634_GM};
extern Il2CppType t2873_0_0_0;
extern Il2CppType t2873_0_0_0;
static ParameterInfo t475_m15635_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2873_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15635_GM;
MethodInfo m15635_MI = 
{
	"AddCollection", (methodPointerType)&m10577_gshared, &t475_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t475_m15635_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15635_GM};
extern Il2CppType t2874_0_0_0;
extern Il2CppType t2874_0_0_0;
static ParameterInfo t475_m15636_ParameterInfos[] = 
{
	{"enumerable", 0, 134217728, &EmptyCustomAttributesCache, &t2874_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15636_GM;
MethodInfo m15636_MI = 
{
	"AddEnumerable", (methodPointerType)&m10579_gshared, &t475_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t475_m15636_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15636_GM};
extern Il2CppType t2874_0_0_0;
static ParameterInfo t475_m15637_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2874_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15637_GM;
MethodInfo m15637_MI = 
{
	"AddRange", (methodPointerType)&m10581_gshared, &t475_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t475_m15637_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15637_GM};
extern Il2CppType t2875_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15638_GM;
MethodInfo m15638_MI = 
{
	"AsReadOnly", (methodPointerType)&m10583_gshared, &t475_TI, &t2875_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15638_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15639_GM;
MethodInfo m15639_MI = 
{
	"Clear", (methodPointerType)&m10585_gshared, &t475_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15639_GM};
extern Il2CppType t474_0_0_0;
static ParameterInfo t475_m15640_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15640_GM;
MethodInfo m15640_MI = 
{
	"Contains", (methodPointerType)&m10587_gshared, &t475_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t475_m15640_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15640_GM};
extern Il2CppType t2871_0_0_0;
extern Il2CppType t2871_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t475_m15641_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2871_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15641_GM;
MethodInfo m15641_MI = 
{
	"CopyTo", (methodPointerType)&m10589_gshared, &t475_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t475_m15641_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15641_GM};
extern Il2CppType t2876_0_0_0;
extern Il2CppType t2876_0_0_0;
static ParameterInfo t475_m15642_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2876_0_0_0},
};
extern Il2CppType t474_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15642_GM;
MethodInfo m15642_MI = 
{
	"Find", (methodPointerType)&m10591_gshared, &t475_TI, &t474_0_0_0, RuntimeInvoker_t29_t29, t475_m15642_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15642_GM};
extern Il2CppType t2876_0_0_0;
static ParameterInfo t475_m15643_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2876_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15643_GM;
MethodInfo m15643_MI = 
{
	"CheckMatch", (methodPointerType)&m10593_gshared, &t475_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t475_m15643_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15643_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2876_0_0_0;
static ParameterInfo t475_m15644_ParameterInfos[] = 
{
	{"startIndex", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"count", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"match", 2, 134217728, &EmptyCustomAttributesCache, &t2876_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15644_GM;
MethodInfo m15644_MI = 
{
	"GetIndex", (methodPointerType)&m10595_gshared, &t475_TI, &t44_0_0_0, RuntimeInvoker_t44_t44_t44_t29, t475_m15644_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15644_GM};
extern Il2CppType t588_0_0_0;
extern void* RuntimeInvoker_t588 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2850_GM;
MethodInfo m2850_MI = 
{
	"GetEnumerator", (methodPointerType)&m2850, &t475_TI, &t588_0_0_0, RuntimeInvoker_t588, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m2850_GM};
extern Il2CppType t474_0_0_0;
static ParameterInfo t475_m15645_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15645_GM;
MethodInfo m15645_MI = 
{
	"IndexOf", (methodPointerType)&m10598_gshared, &t475_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t475_m15645_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15645_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t475_m15646_ParameterInfos[] = 
{
	{"start", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"delta", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15646_GM;
MethodInfo m15646_MI = 
{
	"Shift", (methodPointerType)&m10600_gshared, &t475_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t475_m15646_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15646_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t475_m15647_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15647_GM;
MethodInfo m15647_MI = 
{
	"CheckIndex", (methodPointerType)&m10602_gshared, &t475_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t475_m15647_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15647_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t474_0_0_0;
static ParameterInfo t475_m15648_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15648_GM;
MethodInfo m15648_MI = 
{
	"Insert", (methodPointerType)&m10604_gshared, &t475_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t475_m15648_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15648_GM};
extern Il2CppType t2874_0_0_0;
static ParameterInfo t475_m15649_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2874_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15649_GM;
MethodInfo m15649_MI = 
{
	"CheckCollection", (methodPointerType)&m10606_gshared, &t475_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t475_m15649_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15649_GM};
extern Il2CppType t474_0_0_0;
static ParameterInfo t475_m15650_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15650_GM;
MethodInfo m15650_MI = 
{
	"Remove", (methodPointerType)&m10608_gshared, &t475_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t475_m15650_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15650_GM};
extern Il2CppType t2876_0_0_0;
static ParameterInfo t475_m15651_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2876_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15651_GM;
MethodInfo m15651_MI = 
{
	"RemoveAll", (methodPointerType)&m10610_gshared, &t475_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t475_m15651_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15651_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t475_m15652_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15652_GM;
MethodInfo m15652_MI = 
{
	"RemoveAt", (methodPointerType)&m10611_gshared, &t475_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t475_m15652_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15652_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15653_GM;
MethodInfo m15653_MI = 
{
	"Reverse", (methodPointerType)&m10613_gshared, &t475_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15653_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15654_GM;
MethodInfo m15654_MI = 
{
	"Sort", (methodPointerType)&m10615_gshared, &t475_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15654_GM};
extern Il2CppType t2877_0_0_0;
extern Il2CppType t2877_0_0_0;
static ParameterInfo t475_m15655_ParameterInfos[] = 
{
	{"comparison", 0, 134217728, &EmptyCustomAttributesCache, &t2877_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15655_GM;
MethodInfo m15655_MI = 
{
	"Sort", (methodPointerType)&m10617_gshared, &t475_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t475_m15655_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15655_GM};
extern Il2CppType t2871_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15656_GM;
MethodInfo m15656_MI = 
{
	"ToArray", (methodPointerType)&m10619_gshared, &t475_TI, &t2871_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15656_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15657_GM;
MethodInfo m15657_MI = 
{
	"TrimExcess", (methodPointerType)&m10621_gshared, &t475_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15657_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15658_GM;
MethodInfo m15658_MI = 
{
	"get_Capacity", (methodPointerType)&m10623_gshared, &t475_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15658_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t475_m15659_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15659_GM;
MethodInfo m15659_MI = 
{
	"set_Capacity", (methodPointerType)&m10625_gshared, &t475_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t475_m15659_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15659_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2854_GM;
MethodInfo m2854_MI = 
{
	"get_Count", (methodPointerType)&m10626_gshared, &t475_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m2854_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t475_m2855_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t474_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2855_GM;
MethodInfo m2855_MI = 
{
	"get_Item", (methodPointerType)&m10627_gshared, &t475_TI, &t474_0_0_0, RuntimeInvoker_t29_t44, t475_m2855_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 1, false, true, 0, NULL, (methodPointerType)NULL, &m2855_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t474_0_0_0;
static ParameterInfo t475_m15660_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15660_GM;
MethodInfo m15660_MI = 
{
	"set_Item", (methodPointerType)&m10629_gshared, &t475_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t475_m15660_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 32, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15660_GM};
static MethodInfo* t475_MIs[] =
{
	&m2853_MI,
	&m15616_MI,
	&m15617_MI,
	&m15618_MI,
	&m15619_MI,
	&m15620_MI,
	&m15621_MI,
	&m15622_MI,
	&m15623_MI,
	&m15624_MI,
	&m15625_MI,
	&m15626_MI,
	&m15627_MI,
	&m15628_MI,
	&m15629_MI,
	&m15630_MI,
	&m15631_MI,
	&m15632_MI,
	&m15633_MI,
	&m15634_MI,
	&m15635_MI,
	&m15636_MI,
	&m15637_MI,
	&m15638_MI,
	&m15639_MI,
	&m15640_MI,
	&m15641_MI,
	&m15642_MI,
	&m15643_MI,
	&m15644_MI,
	&m2850_MI,
	&m15645_MI,
	&m15646_MI,
	&m15647_MI,
	&m15648_MI,
	&m15649_MI,
	&m15650_MI,
	&m15651_MI,
	&m15652_MI,
	&m15653_MI,
	&m15654_MI,
	&m15655_MI,
	&m15656_MI,
	&m15657_MI,
	&m15658_MI,
	&m15659_MI,
	&m2854_MI,
	&m2855_MI,
	&m15660_MI,
	NULL
};
extern MethodInfo m15620_MI;
extern MethodInfo m15619_MI;
extern MethodInfo m15621_MI;
extern MethodInfo m15639_MI;
extern MethodInfo m15622_MI;
extern MethodInfo m15623_MI;
extern MethodInfo m15624_MI;
extern MethodInfo m15625_MI;
extern MethodInfo m15641_MI;
extern MethodInfo m15618_MI;
static MethodInfo* t475_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15620_MI,
	&m2854_MI,
	&m15627_MI,
	&m15628_MI,
	&m15619_MI,
	&m15629_MI,
	&m15630_MI,
	&m15631_MI,
	&m15632_MI,
	&m15621_MI,
	&m15639_MI,
	&m15622_MI,
	&m15623_MI,
	&m15624_MI,
	&m15625_MI,
	&m15652_MI,
	&m2854_MI,
	&m15626_MI,
	&m15633_MI,
	&m15639_MI,
	&m15640_MI,
	&m15641_MI,
	&m15650_MI,
	&m15618_MI,
	&m15645_MI,
	&m15648_MI,
	&m15652_MI,
	&m2855_MI,
	&m15660_MI,
};
extern TypeInfo t603_TI;
extern TypeInfo t674_TI;
extern TypeInfo t868_TI;
extern TypeInfo t2879_TI;
static TypeInfo* t475_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2873_TI,
	&t2874_TI,
	&t2879_TI,
};
static Il2CppInterfaceOffsetPair t475_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2873_TI, 20},
	{ &t2874_TI, 27},
	{ &t2879_TI, 28},
};
extern TypeInfo t475_TI;
extern TypeInfo t2871_TI;
extern TypeInfo t588_TI;
extern TypeInfo t474_TI;
extern TypeInfo t2873_TI;
extern TypeInfo t2875_TI;
static Il2CppRGCTXData t475_RGCTXData[37] = 
{
	&t475_TI/* Static Usage */,
	&t2871_TI/* Array Usage */,
	&m2850_MI/* Method Usage */,
	&t588_TI/* Class Usage */,
	&t474_TI/* Class Usage */,
	&m15633_MI/* Method Usage */,
	&m15640_MI/* Method Usage */,
	&m15645_MI/* Method Usage */,
	&m15647_MI/* Method Usage */,
	&m15648_MI/* Method Usage */,
	&m15650_MI/* Method Usage */,
	&m2855_MI/* Method Usage */,
	&m15660_MI/* Method Usage */,
	&m15634_MI/* Method Usage */,
	&m15658_MI/* Method Usage */,
	&m15659_MI/* Method Usage */,
	&m29173_MI/* Method Usage */,
	&m29174_MI/* Method Usage */,
	&m29175_MI/* Method Usage */,
	&m29172_MI/* Method Usage */,
	&m15649_MI/* Method Usage */,
	&t2873_TI/* Class Usage */,
	&m15635_MI/* Method Usage */,
	&m15636_MI/* Method Usage */,
	&t2875_TI/* Class Usage */,
	&m15666_MI/* Method Usage */,
	&m22139_MI/* Method Usage */,
	&m15643_MI/* Method Usage */,
	&m15644_MI/* Method Usage */,
	&m15741_MI/* Method Usage */,
	&m15612_MI/* Method Usage */,
	&m15646_MI/* Method Usage */,
	&m15652_MI/* Method Usage */,
	&m15747_MI/* Method Usage */,
	&m22141_MI/* Method Usage */,
	&m22149_MI/* Method Usage */,
	&m22137_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t475_1_0_0;
struct t475;
extern Il2CppGenericClass t475_GC;
extern CustomAttributesCache t1261__CustomAttributeCache;
TypeInfo t475_TI = 
{
	&g_mscorlib_dll_Image, NULL, "List`1", "System.Collections.Generic", t475_MIs, t475_PIs, t475_FIs, NULL, &t29_TI, NULL, NULL, &t475_TI, t475_ITIs, t475_VT, &t1261__CustomAttributeCache, &t475_TI, &t475_0_0_0, &t475_1_0_0, t475_IOs, &t475_GC, NULL, t475_FDVs, NULL, t475_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t475), 0, -1, sizeof(t475_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, true, false, false, 49, 9, 5, 0, 0, 33, 6, 6};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.GUILayoutEntry>
static PropertyInfo t2873____Count_PropertyInfo = 
{
	&t2873_TI, "Count", &m29173_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29176_MI;
static PropertyInfo t2873____IsReadOnly_PropertyInfo = 
{
	&t2873_TI, "IsReadOnly", &m29176_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2873_PIs[] =
{
	&t2873____Count_PropertyInfo,
	&t2873____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29173_GM;
MethodInfo m29173_MI = 
{
	"get_Count", NULL, &t2873_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29173_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29176_GM;
MethodInfo m29176_MI = 
{
	"get_IsReadOnly", NULL, &t2873_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29176_GM};
extern Il2CppType t474_0_0_0;
static ParameterInfo t2873_m29177_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29177_GM;
MethodInfo m29177_MI = 
{
	"Add", NULL, &t2873_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2873_m29177_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29177_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29178_GM;
MethodInfo m29178_MI = 
{
	"Clear", NULL, &t2873_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29178_GM};
extern Il2CppType t474_0_0_0;
static ParameterInfo t2873_m29179_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29179_GM;
MethodInfo m29179_MI = 
{
	"Contains", NULL, &t2873_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2873_m29179_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29179_GM};
extern Il2CppType t2871_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2873_m29174_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2871_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29174_GM;
MethodInfo m29174_MI = 
{
	"CopyTo", NULL, &t2873_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2873_m29174_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29174_GM};
extern Il2CppType t474_0_0_0;
static ParameterInfo t2873_m29180_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29180_GM;
MethodInfo m29180_MI = 
{
	"Remove", NULL, &t2873_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2873_m29180_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29180_GM};
static MethodInfo* t2873_MIs[] =
{
	&m29173_MI,
	&m29176_MI,
	&m29177_MI,
	&m29178_MI,
	&m29179_MI,
	&m29174_MI,
	&m29180_MI,
	NULL
};
static TypeInfo* t2873_ITIs[] = 
{
	&t603_TI,
	&t2874_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2873_1_0_0;
struct t2873;
extern Il2CppGenericClass t2873_GC;
TypeInfo t2873_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t2873_MIs, t2873_PIs, NULL, NULL, NULL, NULL, NULL, &t2873_TI, t2873_ITIs, NULL, &EmptyCustomAttributesCache, &t2873_TI, &t2873_0_0_0, &t2873_1_0_0, NULL, &t2873_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.GUILayoutEntry>
extern Il2CppType t2872_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29175_GM;
MethodInfo m29175_MI = 
{
	"GetEnumerator", NULL, &t2874_TI, &t2872_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29175_GM};
static MethodInfo* t2874_MIs[] =
{
	&m29175_MI,
	NULL
};
static TypeInfo* t2874_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2874_1_0_0;
struct t2874;
extern Il2CppGenericClass t2874_GC;
TypeInfo t2874_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t2874_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2874_TI, t2874_ITIs, NULL, &EmptyCustomAttributesCache, &t2874_TI, &t2874_0_0_0, &t2874_1_0_0, NULL, &t2874_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#include "t2878.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2878_TI;
#include "t2878MD.h"

extern MethodInfo m15665_MI;
extern MethodInfo m3969_MI;
extern MethodInfo m22126_MI;
struct t20;
struct t20;
 t29 * m19490_gshared (t20 * __this, int32_t p0, MethodInfo* method);
#define m19490(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
#define m22126(__this, p0, method) (t474 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.GUILayoutEntry>
extern Il2CppType t20_0_0_1;
FieldInfo t2878_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2878_TI, offsetof(t2878, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2878_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2878_TI, offsetof(t2878, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2878_FIs[] =
{
	&t2878_f0_FieldInfo,
	&t2878_f1_FieldInfo,
	NULL
};
extern MethodInfo m15662_MI;
static PropertyInfo t2878____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2878_TI, "System.Collections.IEnumerator.Current", &m15662_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2878____Current_PropertyInfo = 
{
	&t2878_TI, "Current", &m15665_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2878_PIs[] =
{
	&t2878____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2878____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2878_m15661_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15661_GM;
MethodInfo m15661_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2878_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2878_m15661_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15661_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15662_GM;
MethodInfo m15662_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2878_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15662_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15663_GM;
MethodInfo m15663_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2878_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15663_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15664_GM;
MethodInfo m15664_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2878_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15664_GM};
extern Il2CppType t474_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15665_GM;
MethodInfo m15665_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2878_TI, &t474_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15665_GM};
static MethodInfo* t2878_MIs[] =
{
	&m15661_MI,
	&m15662_MI,
	&m15663_MI,
	&m15664_MI,
	&m15665_MI,
	NULL
};
extern MethodInfo m15664_MI;
extern MethodInfo m15663_MI;
static MethodInfo* t2878_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15662_MI,
	&m15664_MI,
	&m15663_MI,
	&m15665_MI,
};
static TypeInfo* t2878_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2872_TI,
};
static Il2CppInterfaceOffsetPair t2878_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2872_TI, 7},
};
extern TypeInfo t474_TI;
static Il2CppRGCTXData t2878_RGCTXData[3] = 
{
	&m15665_MI/* Method Usage */,
	&t474_TI/* Class Usage */,
	&m22126_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2878_0_0_0;
extern Il2CppType t2878_1_0_0;
extern Il2CppGenericClass t2878_GC;
extern TypeInfo t20_TI;
TypeInfo t2878_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2878_MIs, t2878_PIs, t2878_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2878_TI, t2878_ITIs, t2878_VT, &EmptyCustomAttributesCache, &t2878_TI, &t2878_0_0_0, &t2878_1_0_0, t2878_IOs, &t2878_GC, NULL, NULL, NULL, t2878_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2878)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.GUILayoutEntry>
extern MethodInfo m29181_MI;
extern MethodInfo m29182_MI;
static PropertyInfo t2879____Item_PropertyInfo = 
{
	&t2879_TI, "Item", &m29181_MI, &m29182_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2879_PIs[] =
{
	&t2879____Item_PropertyInfo,
	NULL
};
extern Il2CppType t474_0_0_0;
static ParameterInfo t2879_m29183_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29183_GM;
MethodInfo m29183_MI = 
{
	"IndexOf", NULL, &t2879_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2879_m29183_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29183_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t474_0_0_0;
static ParameterInfo t2879_m29184_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29184_GM;
MethodInfo m29184_MI = 
{
	"Insert", NULL, &t2879_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2879_m29184_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29184_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2879_m29185_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29185_GM;
MethodInfo m29185_MI = 
{
	"RemoveAt", NULL, &t2879_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2879_m29185_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29185_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2879_m29181_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t474_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29181_GM;
MethodInfo m29181_MI = 
{
	"get_Item", NULL, &t2879_TI, &t474_0_0_0, RuntimeInvoker_t29_t44, t2879_m29181_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29181_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t474_0_0_0;
static ParameterInfo t2879_m29182_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29182_GM;
MethodInfo m29182_MI = 
{
	"set_Item", NULL, &t2879_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2879_m29182_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29182_GM};
static MethodInfo* t2879_MIs[] =
{
	&m29183_MI,
	&m29184_MI,
	&m29185_MI,
	&m29181_MI,
	&m29182_MI,
	NULL
};
static TypeInfo* t2879_ITIs[] = 
{
	&t603_TI,
	&t2873_TI,
	&t2874_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2879_0_0_0;
extern Il2CppType t2879_1_0_0;
struct t2879;
extern Il2CppGenericClass t2879_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t2879_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t2879_MIs, t2879_PIs, NULL, NULL, NULL, NULL, NULL, &t2879_TI, t2879_ITIs, NULL, &t1908__CustomAttributeCache, &t2879_TI, &t2879_0_0_0, &t2879_1_0_0, NULL, &t2879_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif

#include "t345.h"
extern TypeInfo t345_TI;
#include "t345MD.h"
#include "t2880MD.h"
extern MethodInfo m15695_MI;
extern MethodInfo m1516_MI;
extern MethodInfo m3960_MI;
extern MethodInfo m4154_MI;
extern MethodInfo m15727_MI;
extern MethodInfo m29179_MI;
extern MethodInfo m29183_MI;


// Metadata Definition System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.GUILayoutEntry>
extern Il2CppType t2879_0_0_1;
FieldInfo t2875_f0_FieldInfo = 
{
	"list", &t2879_0_0_1, &t2875_TI, offsetof(t2875, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2875_FIs[] =
{
	&t2875_f0_FieldInfo,
	NULL
};
extern MethodInfo m15672_MI;
extern MethodInfo m15673_MI;
static PropertyInfo t2875____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo = 
{
	&t2875_TI, "System.Collections.Generic.IList<T>.Item", &m15672_MI, &m15673_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15674_MI;
static PropertyInfo t2875____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2875_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m15674_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15684_MI;
static PropertyInfo t2875____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2875_TI, "System.Collections.ICollection.IsSynchronized", &m15684_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15685_MI;
static PropertyInfo t2875____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2875_TI, "System.Collections.ICollection.SyncRoot", &m15685_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15686_MI;
static PropertyInfo t2875____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2875_TI, "System.Collections.IList.IsFixedSize", &m15686_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15687_MI;
static PropertyInfo t2875____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2875_TI, "System.Collections.IList.IsReadOnly", &m15687_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15688_MI;
extern MethodInfo m15689_MI;
static PropertyInfo t2875____System_Collections_IList_Item_PropertyInfo = 
{
	&t2875_TI, "System.Collections.IList.Item", &m15688_MI, &m15689_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15694_MI;
static PropertyInfo t2875____Count_PropertyInfo = 
{
	&t2875_TI, "Count", &m15694_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2875____Item_PropertyInfo = 
{
	&t2875_TI, "Item", &m15695_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2875_PIs[] =
{
	&t2875____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo,
	&t2875____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2875____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2875____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2875____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2875____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2875____System_Collections_IList_Item_PropertyInfo,
	&t2875____Count_PropertyInfo,
	&t2875____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2879_0_0_0;
static ParameterInfo t2875_m15666_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2879_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15666_GM;
MethodInfo m15666_MI = 
{
	".ctor", (methodPointerType)&m10637_gshared, &t2875_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2875_m15666_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15666_GM};
extern Il2CppType t474_0_0_0;
static ParameterInfo t2875_m15667_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15667_GM;
MethodInfo m15667_MI = 
{
	"System.Collections.Generic.ICollection<T>.Add", (methodPointerType)&m10638_gshared, &t2875_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2875_m15667_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15667_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15668_GM;
MethodInfo m15668_MI = 
{
	"System.Collections.Generic.ICollection<T>.Clear", (methodPointerType)&m10639_gshared, &t2875_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15668_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t474_0_0_0;
static ParameterInfo t2875_m15669_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15669_GM;
MethodInfo m15669_MI = 
{
	"System.Collections.Generic.IList<T>.Insert", (methodPointerType)&m10640_gshared, &t2875_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2875_m15669_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15669_GM};
extern Il2CppType t474_0_0_0;
static ParameterInfo t2875_m15670_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15670_GM;
MethodInfo m15670_MI = 
{
	"System.Collections.Generic.ICollection<T>.Remove", (methodPointerType)&m10641_gshared, &t2875_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2875_m15670_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15670_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2875_m15671_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15671_GM;
MethodInfo m15671_MI = 
{
	"System.Collections.Generic.IList<T>.RemoveAt", (methodPointerType)&m10642_gshared, &t2875_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2875_m15671_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15671_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2875_m15672_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t474_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15672_GM;
MethodInfo m15672_MI = 
{
	"System.Collections.Generic.IList<T>.get_Item", (methodPointerType)&m10643_gshared, &t2875_TI, &t474_0_0_0, RuntimeInvoker_t29_t44, t2875_m15672_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15672_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t474_0_0_0;
static ParameterInfo t2875_m15673_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15673_GM;
MethodInfo m15673_MI = 
{
	"System.Collections.Generic.IList<T>.set_Item", (methodPointerType)&m10644_gshared, &t2875_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2875_m15673_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15673_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15674_GM;
MethodInfo m15674_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10645_gshared, &t2875_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15674_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2875_m15675_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15675_GM;
MethodInfo m15675_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10646_gshared, &t2875_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2875_m15675_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15675_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15676_GM;
MethodInfo m15676_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10647_gshared, &t2875_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15676_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2875_m15677_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15677_GM;
MethodInfo m15677_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10648_gshared, &t2875_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2875_m15677_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15677_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15678_GM;
MethodInfo m15678_MI = 
{
	"System.Collections.IList.Clear", (methodPointerType)&m10649_gshared, &t2875_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 14, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15678_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2875_m15679_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15679_GM;
MethodInfo m15679_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10650_gshared, &t2875_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2875_m15679_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15679_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2875_m15680_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15680_GM;
MethodInfo m15680_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10651_gshared, &t2875_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2875_m15680_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15680_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2875_m15681_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15681_GM;
MethodInfo m15681_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10652_gshared, &t2875_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2875_m15681_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15681_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2875_m15682_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15682_GM;
MethodInfo m15682_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10653_gshared, &t2875_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2875_m15682_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15682_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2875_m15683_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15683_GM;
MethodInfo m15683_MI = 
{
	"System.Collections.IList.RemoveAt", (methodPointerType)&m10654_gshared, &t2875_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2875_m15683_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15683_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15684_GM;
MethodInfo m15684_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10655_gshared, &t2875_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15684_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15685_GM;
MethodInfo m15685_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10656_gshared, &t2875_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15685_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15686_GM;
MethodInfo m15686_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10657_gshared, &t2875_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15686_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15687_GM;
MethodInfo m15687_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10658_gshared, &t2875_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15687_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2875_m15688_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15688_GM;
MethodInfo m15688_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10659_gshared, &t2875_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2875_m15688_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15688_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2875_m15689_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15689_GM;
MethodInfo m15689_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10660_gshared, &t2875_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2875_m15689_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15689_GM};
extern Il2CppType t474_0_0_0;
static ParameterInfo t2875_m15690_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15690_GM;
MethodInfo m15690_MI = 
{
	"Contains", (methodPointerType)&m10661_gshared, &t2875_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2875_m15690_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15690_GM};
extern Il2CppType t2871_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2875_m15691_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2871_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15691_GM;
MethodInfo m15691_MI = 
{
	"CopyTo", (methodPointerType)&m10662_gshared, &t2875_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2875_m15691_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15691_GM};
extern Il2CppType t2872_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15692_GM;
MethodInfo m15692_MI = 
{
	"GetEnumerator", (methodPointerType)&m10663_gshared, &t2875_TI, &t2872_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15692_GM};
extern Il2CppType t474_0_0_0;
static ParameterInfo t2875_m15693_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15693_GM;
MethodInfo m15693_MI = 
{
	"IndexOf", (methodPointerType)&m10664_gshared, &t2875_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2875_m15693_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15693_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15694_GM;
MethodInfo m15694_MI = 
{
	"get_Count", (methodPointerType)&m10665_gshared, &t2875_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15694_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2875_m15695_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t474_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15695_GM;
MethodInfo m15695_MI = 
{
	"get_Item", (methodPointerType)&m10666_gshared, &t2875_TI, &t474_0_0_0, RuntimeInvoker_t29_t44, t2875_m15695_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 33, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15695_GM};
static MethodInfo* t2875_MIs[] =
{
	&m15666_MI,
	&m15667_MI,
	&m15668_MI,
	&m15669_MI,
	&m15670_MI,
	&m15671_MI,
	&m15672_MI,
	&m15673_MI,
	&m15674_MI,
	&m15675_MI,
	&m15676_MI,
	&m15677_MI,
	&m15678_MI,
	&m15679_MI,
	&m15680_MI,
	&m15681_MI,
	&m15682_MI,
	&m15683_MI,
	&m15684_MI,
	&m15685_MI,
	&m15686_MI,
	&m15687_MI,
	&m15688_MI,
	&m15689_MI,
	&m15690_MI,
	&m15691_MI,
	&m15692_MI,
	&m15693_MI,
	&m15694_MI,
	&m15695_MI,
	NULL
};
extern MethodInfo m15676_MI;
extern MethodInfo m15675_MI;
extern MethodInfo m15677_MI;
extern MethodInfo m15678_MI;
extern MethodInfo m15679_MI;
extern MethodInfo m15680_MI;
extern MethodInfo m15681_MI;
extern MethodInfo m15682_MI;
extern MethodInfo m15683_MI;
extern MethodInfo m15667_MI;
extern MethodInfo m15668_MI;
extern MethodInfo m15690_MI;
extern MethodInfo m15691_MI;
extern MethodInfo m15670_MI;
extern MethodInfo m15693_MI;
extern MethodInfo m15669_MI;
extern MethodInfo m15671_MI;
extern MethodInfo m15692_MI;
static MethodInfo* t2875_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15676_MI,
	&m15694_MI,
	&m15684_MI,
	&m15685_MI,
	&m15675_MI,
	&m15686_MI,
	&m15687_MI,
	&m15688_MI,
	&m15689_MI,
	&m15677_MI,
	&m15678_MI,
	&m15679_MI,
	&m15680_MI,
	&m15681_MI,
	&m15682_MI,
	&m15683_MI,
	&m15694_MI,
	&m15674_MI,
	&m15667_MI,
	&m15668_MI,
	&m15690_MI,
	&m15691_MI,
	&m15670_MI,
	&m15693_MI,
	&m15669_MI,
	&m15671_MI,
	&m15672_MI,
	&m15673_MI,
	&m15692_MI,
	&m15695_MI,
};
static TypeInfo* t2875_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2873_TI,
	&t2879_TI,
	&t2874_TI,
};
static Il2CppInterfaceOffsetPair t2875_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2873_TI, 20},
	{ &t2879_TI, 27},
	{ &t2874_TI, 32},
};
extern TypeInfo t474_TI;
static Il2CppRGCTXData t2875_RGCTXData[9] = 
{
	&m15695_MI/* Method Usage */,
	&m15727_MI/* Method Usage */,
	&t474_TI/* Class Usage */,
	&m29179_MI/* Method Usage */,
	&m29183_MI/* Method Usage */,
	&m29181_MI/* Method Usage */,
	&m29174_MI/* Method Usage */,
	&m29175_MI/* Method Usage */,
	&m29173_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2875_0_0_0;
extern Il2CppType t2875_1_0_0;
struct t2875;
extern Il2CppGenericClass t2875_GC;
extern CustomAttributesCache t1263__CustomAttributeCache;
TypeInfo t2875_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ReadOnlyCollection`1", "System.Collections.ObjectModel", t2875_MIs, t2875_PIs, t2875_FIs, NULL, &t29_TI, NULL, NULL, &t2875_TI, t2875_ITIs, t2875_VT, &t1263__CustomAttributeCache, &t2875_TI, &t2875_0_0_0, &t2875_1_0_0, t2875_IOs, &t2875_GC, NULL, NULL, NULL, t2875_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2875), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 30, 9, 1, 0, 0, 34, 6, 6};
#include "t2880.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2880_TI;

extern MethodInfo m15730_MI;
extern MethodInfo m15731_MI;
extern MethodInfo m15728_MI;
extern MethodInfo m15726_MI;
extern MethodInfo m2853_MI;
extern MethodInfo m9816_MI;
extern MethodInfo m15719_MI;
extern MethodInfo m15729_MI;
extern MethodInfo m15717_MI;
extern MethodInfo m15722_MI;
extern MethodInfo m15713_MI;
extern MethodInfo m29178_MI;
extern MethodInfo m29184_MI;
extern MethodInfo m29185_MI;
extern MethodInfo m6004_MI;
extern MethodInfo m9815_MI;
extern MethodInfo m9817_MI;


// Metadata Definition System.Collections.ObjectModel.Collection`1<UnityEngine.GUILayoutEntry>
extern Il2CppType t2879_0_0_1;
FieldInfo t2880_f0_FieldInfo = 
{
	"list", &t2879_0_0_1, &t2880_TI, offsetof(t2880, f0), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t2880_f1_FieldInfo = 
{
	"syncRoot", &t29_0_0_1, &t2880_TI, offsetof(t2880, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2880_FIs[] =
{
	&t2880_f0_FieldInfo,
	&t2880_f1_FieldInfo,
	NULL
};
extern MethodInfo m15697_MI;
static PropertyInfo t2880____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2880_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m15697_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15705_MI;
static PropertyInfo t2880____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2880_TI, "System.Collections.ICollection.IsSynchronized", &m15705_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15706_MI;
static PropertyInfo t2880____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2880_TI, "System.Collections.ICollection.SyncRoot", &m15706_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15707_MI;
static PropertyInfo t2880____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2880_TI, "System.Collections.IList.IsFixedSize", &m15707_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15708_MI;
static PropertyInfo t2880____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2880_TI, "System.Collections.IList.IsReadOnly", &m15708_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15709_MI;
extern MethodInfo m15710_MI;
static PropertyInfo t2880____System_Collections_IList_Item_PropertyInfo = 
{
	&t2880_TI, "System.Collections.IList.Item", &m15709_MI, &m15710_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15723_MI;
static PropertyInfo t2880____Count_PropertyInfo = 
{
	&t2880_TI, "Count", &m15723_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15724_MI;
extern MethodInfo m15725_MI;
static PropertyInfo t2880____Item_PropertyInfo = 
{
	&t2880_TI, "Item", &m15724_MI, &m15725_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2880_PIs[] =
{
	&t2880____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2880____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2880____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2880____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2880____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2880____System_Collections_IList_Item_PropertyInfo,
	&t2880____Count_PropertyInfo,
	&t2880____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15696_GM;
MethodInfo m15696_MI = 
{
	".ctor", (methodPointerType)&m10667_gshared, &t2880_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15696_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15697_GM;
MethodInfo m15697_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10668_gshared, &t2880_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15697_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2880_m15698_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15698_GM;
MethodInfo m15698_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10669_gshared, &t2880_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2880_m15698_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15698_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15699_GM;
MethodInfo m15699_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10670_gshared, &t2880_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15699_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2880_m15700_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15700_GM;
MethodInfo m15700_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10671_gshared, &t2880_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2880_m15700_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15700_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2880_m15701_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15701_GM;
MethodInfo m15701_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10672_gshared, &t2880_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2880_m15701_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15701_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2880_m15702_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15702_GM;
MethodInfo m15702_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10673_gshared, &t2880_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2880_m15702_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15702_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2880_m15703_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15703_GM;
MethodInfo m15703_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10674_gshared, &t2880_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2880_m15703_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15703_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2880_m15704_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15704_GM;
MethodInfo m15704_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10675_gshared, &t2880_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2880_m15704_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15704_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15705_GM;
MethodInfo m15705_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10676_gshared, &t2880_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15705_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15706_GM;
MethodInfo m15706_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10677_gshared, &t2880_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15706_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15707_GM;
MethodInfo m15707_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10678_gshared, &t2880_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15707_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15708_GM;
MethodInfo m15708_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10679_gshared, &t2880_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15708_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2880_m15709_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15709_GM;
MethodInfo m15709_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10680_gshared, &t2880_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2880_m15709_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15709_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2880_m15710_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15710_GM;
MethodInfo m15710_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10681_gshared, &t2880_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2880_m15710_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15710_GM};
extern Il2CppType t474_0_0_0;
static ParameterInfo t2880_m15711_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15711_GM;
MethodInfo m15711_MI = 
{
	"Add", (methodPointerType)&m10682_gshared, &t2880_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2880_m15711_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15711_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15712_GM;
MethodInfo m15712_MI = 
{
	"Clear", (methodPointerType)&m10683_gshared, &t2880_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15712_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15713_GM;
MethodInfo m15713_MI = 
{
	"ClearItems", (methodPointerType)&m10684_gshared, &t2880_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 452, 0, 33, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15713_GM};
extern Il2CppType t474_0_0_0;
static ParameterInfo t2880_m15714_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15714_GM;
MethodInfo m15714_MI = 
{
	"Contains", (methodPointerType)&m10685_gshared, &t2880_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2880_m15714_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15714_GM};
extern Il2CppType t2871_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2880_m15715_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2871_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15715_GM;
MethodInfo m15715_MI = 
{
	"CopyTo", (methodPointerType)&m10686_gshared, &t2880_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2880_m15715_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15715_GM};
extern Il2CppType t2872_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15716_GM;
MethodInfo m15716_MI = 
{
	"GetEnumerator", (methodPointerType)&m10687_gshared, &t2880_TI, &t2872_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15716_GM};
extern Il2CppType t474_0_0_0;
static ParameterInfo t2880_m15717_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15717_GM;
MethodInfo m15717_MI = 
{
	"IndexOf", (methodPointerType)&m10688_gshared, &t2880_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2880_m15717_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15717_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t474_0_0_0;
static ParameterInfo t2880_m15718_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15718_GM;
MethodInfo m15718_MI = 
{
	"Insert", (methodPointerType)&m10689_gshared, &t2880_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2880_m15718_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15718_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t474_0_0_0;
static ParameterInfo t2880_m15719_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15719_GM;
MethodInfo m15719_MI = 
{
	"InsertItem", (methodPointerType)&m10690_gshared, &t2880_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2880_m15719_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 34, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15719_GM};
extern Il2CppType t474_0_0_0;
static ParameterInfo t2880_m15720_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15720_GM;
MethodInfo m15720_MI = 
{
	"Remove", (methodPointerType)&m10691_gshared, &t2880_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2880_m15720_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15720_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2880_m15721_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15721_GM;
MethodInfo m15721_MI = 
{
	"RemoveAt", (methodPointerType)&m10692_gshared, &t2880_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2880_m15721_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15721_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2880_m15722_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15722_GM;
MethodInfo m15722_MI = 
{
	"RemoveItem", (methodPointerType)&m10693_gshared, &t2880_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2880_m15722_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 35, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15722_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15723_GM;
MethodInfo m15723_MI = 
{
	"get_Count", (methodPointerType)&m10694_gshared, &t2880_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15723_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2880_m15724_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t474_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15724_GM;
MethodInfo m15724_MI = 
{
	"get_Item", (methodPointerType)&m10695_gshared, &t2880_TI, &t474_0_0_0, RuntimeInvoker_t29_t44, t2880_m15724_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15724_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t474_0_0_0;
static ParameterInfo t2880_m15725_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15725_GM;
MethodInfo m15725_MI = 
{
	"set_Item", (methodPointerType)&m10696_gshared, &t2880_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2880_m15725_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15725_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t474_0_0_0;
static ParameterInfo t2880_m15726_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15726_GM;
MethodInfo m15726_MI = 
{
	"SetItem", (methodPointerType)&m10697_gshared, &t2880_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2880_m15726_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 36, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15726_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2880_m15727_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15727_GM;
MethodInfo m15727_MI = 
{
	"IsValidItem", (methodPointerType)&m10698_gshared, &t2880_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2880_m15727_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15727_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2880_m15728_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t474_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15728_GM;
MethodInfo m15728_MI = 
{
	"ConvertItem", (methodPointerType)&m10699_gshared, &t2880_TI, &t474_0_0_0, RuntimeInvoker_t29_t29, t2880_m15728_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15728_GM};
extern Il2CppType t2879_0_0_0;
static ParameterInfo t2880_m15729_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2879_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15729_GM;
MethodInfo m15729_MI = 
{
	"CheckWritable", (methodPointerType)&m10700_gshared, &t2880_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2880_m15729_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15729_GM};
extern Il2CppType t2879_0_0_0;
static ParameterInfo t2880_m15730_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2879_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15730_GM;
MethodInfo m15730_MI = 
{
	"IsSynchronized", (methodPointerType)&m10701_gshared, &t2880_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2880_m15730_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15730_GM};
extern Il2CppType t2879_0_0_0;
static ParameterInfo t2880_m15731_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2879_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15731_GM;
MethodInfo m15731_MI = 
{
	"IsFixedSize", (methodPointerType)&m10702_gshared, &t2880_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2880_m15731_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15731_GM};
static MethodInfo* t2880_MIs[] =
{
	&m15696_MI,
	&m15697_MI,
	&m15698_MI,
	&m15699_MI,
	&m15700_MI,
	&m15701_MI,
	&m15702_MI,
	&m15703_MI,
	&m15704_MI,
	&m15705_MI,
	&m15706_MI,
	&m15707_MI,
	&m15708_MI,
	&m15709_MI,
	&m15710_MI,
	&m15711_MI,
	&m15712_MI,
	&m15713_MI,
	&m15714_MI,
	&m15715_MI,
	&m15716_MI,
	&m15717_MI,
	&m15718_MI,
	&m15719_MI,
	&m15720_MI,
	&m15721_MI,
	&m15722_MI,
	&m15723_MI,
	&m15724_MI,
	&m15725_MI,
	&m15726_MI,
	&m15727_MI,
	&m15728_MI,
	&m15729_MI,
	&m15730_MI,
	&m15731_MI,
	NULL
};
extern MethodInfo m15699_MI;
extern MethodInfo m15698_MI;
extern MethodInfo m15700_MI;
extern MethodInfo m15712_MI;
extern MethodInfo m15701_MI;
extern MethodInfo m15702_MI;
extern MethodInfo m15703_MI;
extern MethodInfo m15704_MI;
extern MethodInfo m15721_MI;
extern MethodInfo m15711_MI;
extern MethodInfo m15714_MI;
extern MethodInfo m15715_MI;
extern MethodInfo m15720_MI;
extern MethodInfo m15718_MI;
extern MethodInfo m15716_MI;
static MethodInfo* t2880_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15699_MI,
	&m15723_MI,
	&m15705_MI,
	&m15706_MI,
	&m15698_MI,
	&m15707_MI,
	&m15708_MI,
	&m15709_MI,
	&m15710_MI,
	&m15700_MI,
	&m15712_MI,
	&m15701_MI,
	&m15702_MI,
	&m15703_MI,
	&m15704_MI,
	&m15721_MI,
	&m15723_MI,
	&m15697_MI,
	&m15711_MI,
	&m15712_MI,
	&m15714_MI,
	&m15715_MI,
	&m15720_MI,
	&m15717_MI,
	&m15718_MI,
	&m15721_MI,
	&m15724_MI,
	&m15725_MI,
	&m15716_MI,
	&m15713_MI,
	&m15719_MI,
	&m15722_MI,
	&m15726_MI,
};
static TypeInfo* t2880_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2873_TI,
	&t2879_TI,
	&t2874_TI,
};
static Il2CppInterfaceOffsetPair t2880_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2873_TI, 20},
	{ &t2879_TI, 27},
	{ &t2874_TI, 32},
};
extern TypeInfo t475_TI;
extern TypeInfo t474_TI;
static Il2CppRGCTXData t2880_RGCTXData[25] = 
{
	&t475_TI/* Class Usage */,
	&m2853_MI/* Method Usage */,
	&m29176_MI/* Method Usage */,
	&m29175_MI/* Method Usage */,
	&m29173_MI/* Method Usage */,
	&m15728_MI/* Method Usage */,
	&m15719_MI/* Method Usage */,
	&m15727_MI/* Method Usage */,
	&t474_TI/* Class Usage */,
	&m29179_MI/* Method Usage */,
	&m29183_MI/* Method Usage */,
	&m15729_MI/* Method Usage */,
	&m15717_MI/* Method Usage */,
	&m15722_MI/* Method Usage */,
	&m15730_MI/* Method Usage */,
	&m15731_MI/* Method Usage */,
	&m29181_MI/* Method Usage */,
	&m15726_MI/* Method Usage */,
	&m15713_MI/* Method Usage */,
	&m29178_MI/* Method Usage */,
	&m29174_MI/* Method Usage */,
	&m29184_MI/* Method Usage */,
	&m29185_MI/* Method Usage */,
	&m29182_MI/* Method Usage */,
	&t474_0_0_0/* Type Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2880_0_0_0;
extern Il2CppType t2880_1_0_0;
struct t2880;
extern Il2CppGenericClass t2880_GC;
extern CustomAttributesCache t1262__CustomAttributeCache;
TypeInfo t2880_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Collection`1", "System.Collections.ObjectModel", t2880_MIs, t2880_PIs, t2880_FIs, NULL, &t29_TI, NULL, NULL, &t2880_TI, t2880_ITIs, t2880_VT, &t1262__CustomAttributeCache, &t2880_TI, &t2880_0_0_0, &t2880_1_0_0, t2880_IOs, &t2880_GC, NULL, NULL, NULL, t2880_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2880), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 36, 8, 2, 0, 0, 37, 6, 6};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2881_TI;
#include "t2881MD.h"

#include "t2882.h"
extern TypeInfo t6718_TI;
extern TypeInfo t2882_TI;
#include "t2882MD.h"
extern Il2CppType t6718_0_0_0;
extern MethodInfo m15737_MI;
extern MethodInfo m29186_MI;
extern MethodInfo m22138_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1<UnityEngine.GUILayoutEntry>
extern Il2CppType t2881_0_0_49;
FieldInfo t2881_f0_FieldInfo = 
{
	"_default", &t2881_0_0_49, &t2881_TI, offsetof(t2881_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2881_FIs[] =
{
	&t2881_f0_FieldInfo,
	NULL
};
extern MethodInfo m15736_MI;
static PropertyInfo t2881____Default_PropertyInfo = 
{
	&t2881_TI, "Default", &m15736_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2881_PIs[] =
{
	&t2881____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15732_GM;
MethodInfo m15732_MI = 
{
	".ctor", (methodPointerType)&m10703_gshared, &t2881_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15732_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15733_GM;
MethodInfo m15733_MI = 
{
	".cctor", (methodPointerType)&m10704_gshared, &t2881_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15733_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2881_m15734_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15734_GM;
MethodInfo m15734_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m10705_gshared, &t2881_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2881_m15734_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15734_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2881_m15735_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15735_GM;
MethodInfo m15735_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m10706_gshared, &t2881_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2881_m15735_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15735_GM};
extern Il2CppType t474_0_0_0;
static ParameterInfo t2881_m29186_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29186_GM;
MethodInfo m29186_MI = 
{
	"GetHashCode", NULL, &t2881_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2881_m29186_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29186_GM};
extern Il2CppType t474_0_0_0;
extern Il2CppType t474_0_0_0;
static ParameterInfo t2881_m22138_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m22138_GM;
MethodInfo m22138_MI = 
{
	"Equals", NULL, &t2881_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2881_m22138_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m22138_GM};
extern Il2CppType t2881_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15736_GM;
MethodInfo m15736_MI = 
{
	"get_Default", (methodPointerType)&m10707_gshared, &t2881_TI, &t2881_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15736_GM};
static MethodInfo* t2881_MIs[] =
{
	&m15732_MI,
	&m15733_MI,
	&m15734_MI,
	&m15735_MI,
	&m29186_MI,
	&m22138_MI,
	&m15736_MI,
	NULL
};
extern MethodInfo m15735_MI;
extern MethodInfo m15734_MI;
static MethodInfo* t2881_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m22138_MI,
	&m29186_MI,
	&m15735_MI,
	&m15734_MI,
	NULL,
	NULL,
};
extern TypeInfo t6719_TI;
static TypeInfo* t2881_ITIs[] = 
{
	&t6719_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t2881_IOs[] = 
{
	{ &t6719_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2881_TI;
extern TypeInfo t2881_TI;
extern TypeInfo t2882_TI;
extern TypeInfo t474_TI;
static Il2CppRGCTXData t2881_RGCTXData[9] = 
{
	&t6718_0_0_0/* Type Usage */,
	&t474_0_0_0/* Type Usage */,
	&t2881_TI/* Class Usage */,
	&t2881_TI/* Static Usage */,
	&t2882_TI/* Class Usage */,
	&m15737_MI/* Method Usage */,
	&t474_TI/* Class Usage */,
	&m29186_MI/* Method Usage */,
	&m22138_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2881_0_0_0;
extern Il2CppType t2881_1_0_0;
struct t2881;
extern Il2CppGenericClass t2881_GC;
TypeInfo t2881_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t2881_MIs, t2881_PIs, t2881_FIs, NULL, &t29_TI, NULL, NULL, &t2881_TI, t2881_ITIs, t2881_VT, &EmptyCustomAttributesCache, &t2881_TI, &t2881_0_0_0, &t2881_1_0_0, t2881_IOs, &t2881_GC, NULL, NULL, NULL, t2881_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2881), 0, -1, sizeof(t2881_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<UnityEngine.GUILayoutEntry>
extern Il2CppType t474_0_0_0;
extern Il2CppType t474_0_0_0;
static ParameterInfo t6719_m29187_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29187_GM;
MethodInfo m29187_MI = 
{
	"Equals", NULL, &t6719_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t6719_m29187_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29187_GM};
extern Il2CppType t474_0_0_0;
static ParameterInfo t6719_m29188_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29188_GM;
MethodInfo m29188_MI = 
{
	"GetHashCode", NULL, &t6719_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6719_m29188_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29188_GM};
static MethodInfo* t6719_MIs[] =
{
	&m29187_MI,
	&m29188_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6719_0_0_0;
extern Il2CppType t6719_1_0_0;
struct t6719;
extern Il2CppGenericClass t6719_GC;
TypeInfo t6719_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t6719_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6719_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6719_TI, &t6719_0_0_0, &t6719_1_0_0, NULL, &t6719_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IEquatable`1<UnityEngine.GUILayoutEntry>
extern Il2CppType t474_0_0_0;
static ParameterInfo t6718_m29189_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29189_GM;
MethodInfo m29189_MI = 
{
	"Equals", NULL, &t6718_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6718_m29189_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29189_GM};
static MethodInfo* t6718_MIs[] =
{
	&m29189_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6718_1_0_0;
struct t6718;
extern Il2CppGenericClass t6718_GC;
TypeInfo t6718_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t6718_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6718_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6718_TI, &t6718_0_0_0, &t6718_1_0_0, NULL, &t6718_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m15732_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.GUILayoutEntry>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15737_GM;
MethodInfo m15737_MI = 
{
	".ctor", (methodPointerType)&m10738_gshared, &t2882_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15737_GM};
extern Il2CppType t474_0_0_0;
static ParameterInfo t2882_m15738_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15738_GM;
MethodInfo m15738_MI = 
{
	"GetHashCode", (methodPointerType)&m10739_gshared, &t2882_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2882_m15738_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15738_GM};
extern Il2CppType t474_0_0_0;
extern Il2CppType t474_0_0_0;
static ParameterInfo t2882_m15739_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15739_GM;
MethodInfo m15739_MI = 
{
	"Equals", (methodPointerType)&m10740_gshared, &t2882_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2882_m15739_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15739_GM};
static MethodInfo* t2882_MIs[] =
{
	&m15737_MI,
	&m15738_MI,
	&m15739_MI,
	NULL
};
extern MethodInfo m15739_MI;
extern MethodInfo m15738_MI;
static MethodInfo* t2882_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15739_MI,
	&m15738_MI,
	&m15735_MI,
	&m15734_MI,
	&m15738_MI,
	&m15739_MI,
};
static Il2CppInterfaceOffsetPair t2882_IOs[] = 
{
	{ &t6719_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2881_TI;
extern TypeInfo t2881_TI;
extern TypeInfo t2882_TI;
extern TypeInfo t474_TI;
extern TypeInfo t474_TI;
static Il2CppRGCTXData t2882_RGCTXData[11] = 
{
	&t6718_0_0_0/* Type Usage */,
	&t474_0_0_0/* Type Usage */,
	&t2881_TI/* Class Usage */,
	&t2881_TI/* Static Usage */,
	&t2882_TI/* Class Usage */,
	&m15737_MI/* Method Usage */,
	&t474_TI/* Class Usage */,
	&m29186_MI/* Method Usage */,
	&m22138_MI/* Method Usage */,
	&m15732_MI/* Method Usage */,
	&t474_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2882_0_0_0;
extern Il2CppType t2882_1_0_0;
struct t2882;
extern Il2CppGenericClass t2882_GC;
TypeInfo t2882_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2882_MIs, NULL, NULL, NULL, &t2881_TI, NULL, &t1256_TI, &t2882_TI, NULL, t2882_VT, &EmptyCustomAttributesCache, &t2882_TI, &t2882_0_0_0, &t2882_1_0_0, t2882_IOs, &t2882_GC, NULL, NULL, NULL, t2882_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2882), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Predicate`1<UnityEngine.GUILayoutEntry>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2876_m15740_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15740_GM;
MethodInfo m15740_MI = 
{
	".ctor", (methodPointerType)&m10741_gshared, &t2876_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2876_m15740_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15740_GM};
extern Il2CppType t474_0_0_0;
static ParameterInfo t2876_m15741_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15741_GM;
MethodInfo m15741_MI = 
{
	"Invoke", (methodPointerType)&m10742_gshared, &t2876_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2876_m15741_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15741_GM};
extern Il2CppType t474_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2876_m15742_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15742_GM;
MethodInfo m15742_MI = 
{
	"BeginInvoke", (methodPointerType)&m10743_gshared, &t2876_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2876_m15742_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15742_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2876_m15743_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15743_GM;
MethodInfo m15743_MI = 
{
	"EndInvoke", (methodPointerType)&m10744_gshared, &t2876_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2876_m15743_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15743_GM};
static MethodInfo* t2876_MIs[] =
{
	&m15740_MI,
	&m15741_MI,
	&m15742_MI,
	&m15743_MI,
	NULL
};
extern MethodInfo m15742_MI;
extern MethodInfo m15743_MI;
static MethodInfo* t2876_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m15741_MI,
	&m15742_MI,
	&m15743_MI,
};
static Il2CppInterfaceOffsetPair t2876_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2876_1_0_0;
struct t2876;
extern Il2CppGenericClass t2876_GC;
TypeInfo t2876_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Predicate`1", "System", t2876_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2876_TI, NULL, t2876_VT, &EmptyCustomAttributesCache, &t2876_TI, &t2876_0_0_0, &t2876_1_0_0, t2876_IOs, &t2876_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2876), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif

#include "t1247.h"
#include "t2884.h"
extern TypeInfo t4357_TI;
extern TypeInfo t1247_TI;
extern TypeInfo t2884_TI;
#include "t2884MD.h"
extern Il2CppType t4357_0_0_0;
extern Il2CppType t1247_0_0_0;
extern MethodInfo m15748_MI;
extern MethodInfo m29190_MI;
extern MethodInfo m8852_MI;


// Metadata Definition System.Collections.Generic.Comparer`1<UnityEngine.GUILayoutEntry>
extern Il2CppType t2883_0_0_49;
FieldInfo t2883_f0_FieldInfo = 
{
	"_default", &t2883_0_0_49, &t2883_TI, offsetof(t2883_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2883_FIs[] =
{
	&t2883_f0_FieldInfo,
	NULL
};
static PropertyInfo t2883____Default_PropertyInfo = 
{
	&t2883_TI, "Default", &m15747_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2883_PIs[] =
{
	&t2883____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15744_GM;
MethodInfo m15744_MI = 
{
	".ctor", (methodPointerType)&m10745_gshared, &t2883_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15744_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15745_GM;
MethodInfo m15745_MI = 
{
	".cctor", (methodPointerType)&m10746_gshared, &t2883_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15745_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2883_m15746_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15746_GM;
MethodInfo m15746_MI = 
{
	"System.Collections.IComparer.Compare", (methodPointerType)&m10747_gshared, &t2883_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2883_m15746_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15746_GM};
extern Il2CppType t474_0_0_0;
extern Il2CppType t474_0_0_0;
static ParameterInfo t2883_m29190_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29190_GM;
MethodInfo m29190_MI = 
{
	"Compare", NULL, &t2883_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2883_m29190_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29190_GM};
extern Il2CppType t2883_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15747_GM;
MethodInfo m15747_MI = 
{
	"get_Default", (methodPointerType)&m10748_gshared, &t2883_TI, &t2883_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15747_GM};
static MethodInfo* t2883_MIs[] =
{
	&m15744_MI,
	&m15745_MI,
	&m15746_MI,
	&m29190_MI,
	&m15747_MI,
	NULL
};
extern MethodInfo m15746_MI;
static MethodInfo* t2883_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m29190_MI,
	&m15746_MI,
	NULL,
};
extern TypeInfo t4356_TI;
extern TypeInfo t726_TI;
static TypeInfo* t2883_ITIs[] = 
{
	&t4356_TI,
	&t726_TI,
};
static Il2CppInterfaceOffsetPair t2883_IOs[] = 
{
	{ &t4356_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t2883_TI;
extern TypeInfo t2883_TI;
extern TypeInfo t2884_TI;
extern TypeInfo t474_TI;
static Il2CppRGCTXData t2883_RGCTXData[8] = 
{
	&t4357_0_0_0/* Type Usage */,
	&t474_0_0_0/* Type Usage */,
	&t2883_TI/* Class Usage */,
	&t2883_TI/* Static Usage */,
	&t2884_TI/* Class Usage */,
	&m15748_MI/* Method Usage */,
	&t474_TI/* Class Usage */,
	&m29190_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2883_0_0_0;
extern Il2CppType t2883_1_0_0;
struct t2883;
extern Il2CppGenericClass t2883_GC;
TypeInfo t2883_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparer`1", "System.Collections.Generic", t2883_MIs, t2883_PIs, t2883_FIs, NULL, &t29_TI, NULL, NULL, &t2883_TI, t2883_ITIs, t2883_VT, &EmptyCustomAttributesCache, &t2883_TI, &t2883_0_0_0, &t2883_1_0_0, t2883_IOs, &t2883_GC, NULL, NULL, NULL, t2883_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2883), 0, -1, sizeof(t2883_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 5, 1, 1, 0, 0, 7, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IComparer`1<UnityEngine.GUILayoutEntry>
extern Il2CppType t474_0_0_0;
extern Il2CppType t474_0_0_0;
static ParameterInfo t4356_m22146_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m22146_GM;
MethodInfo m22146_MI = 
{
	"Compare", NULL, &t4356_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t4356_m22146_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m22146_GM};
static MethodInfo* t4356_MIs[] =
{
	&m22146_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4356_0_0_0;
extern Il2CppType t4356_1_0_0;
struct t4356;
extern Il2CppGenericClass t4356_GC;
TypeInfo t4356_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparer`1", "System.Collections.Generic", t4356_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4356_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4356_TI, &t4356_0_0_0, &t4356_1_0_0, NULL, &t4356_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IComparable`1<UnityEngine.GUILayoutEntry>
extern Il2CppType t474_0_0_0;
static ParameterInfo t4357_m22147_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m22147_GM;
MethodInfo m22147_MI = 
{
	"CompareTo", NULL, &t4357_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t4357_m22147_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m22147_GM};
static MethodInfo* t4357_MIs[] =
{
	&m22147_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4357_1_0_0;
struct t4357;
extern Il2CppGenericClass t4357_GC;
TypeInfo t4357_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t4357_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4357_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4357_TI, &t4357_0_0_0, &t4357_1_0_0, NULL, &t4357_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo t290_TI;
extern MethodInfo m15744_MI;
extern MethodInfo m22147_MI;
extern MethodInfo m9672_MI;


// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.GUILayoutEntry>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15748_GM;
MethodInfo m15748_MI = 
{
	".ctor", (methodPointerType)&m10749_gshared, &t2884_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15748_GM};
extern Il2CppType t474_0_0_0;
extern Il2CppType t474_0_0_0;
static ParameterInfo t2884_m15749_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15749_GM;
MethodInfo m15749_MI = 
{
	"Compare", (methodPointerType)&m10750_gshared, &t2884_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2884_m15749_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15749_GM};
static MethodInfo* t2884_MIs[] =
{
	&m15748_MI,
	&m15749_MI,
	NULL
};
extern MethodInfo m15749_MI;
static MethodInfo* t2884_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15749_MI,
	&m15746_MI,
	&m15749_MI,
};
static Il2CppInterfaceOffsetPair t2884_IOs[] = 
{
	{ &t4356_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t2883_TI;
extern TypeInfo t2883_TI;
extern TypeInfo t2884_TI;
extern TypeInfo t474_TI;
extern TypeInfo t474_TI;
extern TypeInfo t4357_TI;
static Il2CppRGCTXData t2884_RGCTXData[12] = 
{
	&t4357_0_0_0/* Type Usage */,
	&t474_0_0_0/* Type Usage */,
	&t2883_TI/* Class Usage */,
	&t2883_TI/* Static Usage */,
	&t2884_TI/* Class Usage */,
	&m15748_MI/* Method Usage */,
	&t474_TI/* Class Usage */,
	&m29190_MI/* Method Usage */,
	&m15744_MI/* Method Usage */,
	&t474_TI/* Class Usage */,
	&t4357_TI/* Class Usage */,
	&m22147_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2884_0_0_0;
extern Il2CppType t2884_1_0_0;
struct t2884;
extern Il2CppGenericClass t2884_GC;
extern TypeInfo t1246_TI;
TypeInfo t2884_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2884_MIs, NULL, NULL, NULL, &t2883_TI, NULL, &t1246_TI, &t2884_TI, NULL, t2884_VT, &EmptyCustomAttributesCache, &t2884_TI, &t2884_0_0_0, &t2884_1_0_0, t2884_IOs, &t2884_GC, NULL, NULL, NULL, t2884_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2884), 0, -1, 0, 0, -1, 1048835, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 7, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2877_TI;
#include "t2877MD.h"



// Metadata Definition System.Comparison`1<UnityEngine.GUILayoutEntry>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2877_m15750_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15750_GM;
MethodInfo m15750_MI = 
{
	".ctor", (methodPointerType)&m10796_gshared, &t2877_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2877_m15750_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15750_GM};
extern Il2CppType t474_0_0_0;
extern Il2CppType t474_0_0_0;
static ParameterInfo t2877_m15751_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15751_GM;
MethodInfo m15751_MI = 
{
	"Invoke", (methodPointerType)&m10797_gshared, &t2877_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2877_m15751_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15751_GM};
extern Il2CppType t474_0_0_0;
extern Il2CppType t474_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2877_m15752_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t474_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15752_GM;
MethodInfo m15752_MI = 
{
	"BeginInvoke", (methodPointerType)&m10798_gshared, &t2877_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t2877_m15752_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m15752_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2877_m15753_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15753_GM;
MethodInfo m15753_MI = 
{
	"EndInvoke", (methodPointerType)&m10799_gshared, &t2877_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2877_m15753_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15753_GM};
static MethodInfo* t2877_MIs[] =
{
	&m15750_MI,
	&m15751_MI,
	&m15752_MI,
	&m15753_MI,
	NULL
};
extern MethodInfo m15751_MI;
extern MethodInfo m15752_MI;
extern MethodInfo m15753_MI;
static MethodInfo* t2877_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m15751_MI,
	&m15752_MI,
	&m15753_MI,
};
static Il2CppInterfaceOffsetPair t2877_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2877_1_0_0;
struct t2877;
extern Il2CppGenericClass t2877_GC;
TypeInfo t2877_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparison`1", "System", t2877_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2877_TI, NULL, t2877_VT, &EmptyCustomAttributesCache, &t2877_TI, &t2877_0_0_0, &t2877_1_0_0, t2877_IOs, &t2877_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2877), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4359_TI;

#include "t477.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.GUILayoutOption/Type>
extern MethodInfo m29191_MI;
static PropertyInfo t4359____Current_PropertyInfo = 
{
	&t4359_TI, "Current", &m29191_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4359_PIs[] =
{
	&t4359____Current_PropertyInfo,
	NULL
};
extern Il2CppType t477_0_0_0;
extern void* RuntimeInvoker_t477 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29191_GM;
MethodInfo m29191_MI = 
{
	"get_Current", NULL, &t4359_TI, &t477_0_0_0, RuntimeInvoker_t477, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29191_GM};
static MethodInfo* t4359_MIs[] =
{
	&m29191_MI,
	NULL
};
static TypeInfo* t4359_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4359_0_0_0;
extern Il2CppType t4359_1_0_0;
struct t4359;
extern Il2CppGenericClass t4359_GC;
TypeInfo t4359_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4359_MIs, t4359_PIs, NULL, NULL, NULL, NULL, NULL, &t4359_TI, t4359_ITIs, NULL, &EmptyCustomAttributesCache, &t4359_TI, &t4359_0_0_0, &t4359_1_0_0, NULL, &t4359_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2885.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2885_TI;
#include "t2885MD.h"

extern TypeInfo t477_TI;
extern MethodInfo m15758_MI;
extern MethodInfo m22152_MI;
struct t20;
 int32_t m22152 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m15754_MI;
 void m15754 (t2885 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m15755_MI;
 t29 * m15755 (t2885 * __this, MethodInfo* method){
	{
		int32_t L_0 = m15758(__this, &m15758_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t477_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m15756_MI;
 void m15756 (t2885 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m15757_MI;
 bool m15757 (t2885 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m15758 (t2885 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m22152(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m22152_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.GUILayoutOption/Type>
extern Il2CppType t20_0_0_1;
FieldInfo t2885_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2885_TI, offsetof(t2885, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2885_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2885_TI, offsetof(t2885, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2885_FIs[] =
{
	&t2885_f0_FieldInfo,
	&t2885_f1_FieldInfo,
	NULL
};
static PropertyInfo t2885____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2885_TI, "System.Collections.IEnumerator.Current", &m15755_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2885____Current_PropertyInfo = 
{
	&t2885_TI, "Current", &m15758_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2885_PIs[] =
{
	&t2885____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2885____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2885_m15754_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15754_GM;
MethodInfo m15754_MI = 
{
	".ctor", (methodPointerType)&m15754, &t2885_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2885_m15754_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15754_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15755_GM;
MethodInfo m15755_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m15755, &t2885_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15755_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15756_GM;
MethodInfo m15756_MI = 
{
	"Dispose", (methodPointerType)&m15756, &t2885_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15756_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15757_GM;
MethodInfo m15757_MI = 
{
	"MoveNext", (methodPointerType)&m15757, &t2885_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15757_GM};
extern Il2CppType t477_0_0_0;
extern void* RuntimeInvoker_t477 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15758_GM;
MethodInfo m15758_MI = 
{
	"get_Current", (methodPointerType)&m15758, &t2885_TI, &t477_0_0_0, RuntimeInvoker_t477, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15758_GM};
static MethodInfo* t2885_MIs[] =
{
	&m15754_MI,
	&m15755_MI,
	&m15756_MI,
	&m15757_MI,
	&m15758_MI,
	NULL
};
static MethodInfo* t2885_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15755_MI,
	&m15757_MI,
	&m15756_MI,
	&m15758_MI,
};
static TypeInfo* t2885_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4359_TI,
};
static Il2CppInterfaceOffsetPair t2885_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4359_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2885_0_0_0;
extern Il2CppType t2885_1_0_0;
extern Il2CppGenericClass t2885_GC;
TypeInfo t2885_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2885_MIs, t2885_PIs, t2885_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2885_TI, t2885_ITIs, t2885_VT, &EmptyCustomAttributesCache, &t2885_TI, &t2885_0_0_0, &t2885_1_0_0, t2885_IOs, &t2885_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2885)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5577_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.GUILayoutOption/Type>
extern MethodInfo m29192_MI;
static PropertyInfo t5577____Count_PropertyInfo = 
{
	&t5577_TI, "Count", &m29192_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29193_MI;
static PropertyInfo t5577____IsReadOnly_PropertyInfo = 
{
	&t5577_TI, "IsReadOnly", &m29193_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5577_PIs[] =
{
	&t5577____Count_PropertyInfo,
	&t5577____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29192_GM;
MethodInfo m29192_MI = 
{
	"get_Count", NULL, &t5577_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29192_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29193_GM;
MethodInfo m29193_MI = 
{
	"get_IsReadOnly", NULL, &t5577_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29193_GM};
extern Il2CppType t477_0_0_0;
extern Il2CppType t477_0_0_0;
static ParameterInfo t5577_m29194_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t477_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29194_GM;
MethodInfo m29194_MI = 
{
	"Add", NULL, &t5577_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5577_m29194_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29194_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29195_GM;
MethodInfo m29195_MI = 
{
	"Clear", NULL, &t5577_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29195_GM};
extern Il2CppType t477_0_0_0;
static ParameterInfo t5577_m29196_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t477_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29196_GM;
MethodInfo m29196_MI = 
{
	"Contains", NULL, &t5577_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5577_m29196_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29196_GM};
extern Il2CppType t3744_0_0_0;
extern Il2CppType t3744_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5577_m29197_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3744_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29197_GM;
MethodInfo m29197_MI = 
{
	"CopyTo", NULL, &t5577_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5577_m29197_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29197_GM};
extern Il2CppType t477_0_0_0;
static ParameterInfo t5577_m29198_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t477_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29198_GM;
MethodInfo m29198_MI = 
{
	"Remove", NULL, &t5577_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5577_m29198_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29198_GM};
static MethodInfo* t5577_MIs[] =
{
	&m29192_MI,
	&m29193_MI,
	&m29194_MI,
	&m29195_MI,
	&m29196_MI,
	&m29197_MI,
	&m29198_MI,
	NULL
};
extern TypeInfo t5579_TI;
static TypeInfo* t5577_ITIs[] = 
{
	&t603_TI,
	&t5579_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5577_0_0_0;
extern Il2CppType t5577_1_0_0;
struct t5577;
extern Il2CppGenericClass t5577_GC;
TypeInfo t5577_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5577_MIs, t5577_PIs, NULL, NULL, NULL, NULL, NULL, &t5577_TI, t5577_ITIs, NULL, &EmptyCustomAttributesCache, &t5577_TI, &t5577_0_0_0, &t5577_1_0_0, NULL, &t5577_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.GUILayoutOption/Type>
extern Il2CppType t4359_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29199_GM;
MethodInfo m29199_MI = 
{
	"GetEnumerator", NULL, &t5579_TI, &t4359_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29199_GM};
static MethodInfo* t5579_MIs[] =
{
	&m29199_MI,
	NULL
};
static TypeInfo* t5579_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5579_0_0_0;
extern Il2CppType t5579_1_0_0;
struct t5579;
extern Il2CppGenericClass t5579_GC;
TypeInfo t5579_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5579_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5579_TI, t5579_ITIs, NULL, &EmptyCustomAttributesCache, &t5579_TI, &t5579_0_0_0, &t5579_1_0_0, NULL, &t5579_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5578_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.GUILayoutOption/Type>
extern MethodInfo m29200_MI;
extern MethodInfo m29201_MI;
static PropertyInfo t5578____Item_PropertyInfo = 
{
	&t5578_TI, "Item", &m29200_MI, &m29201_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5578_PIs[] =
{
	&t5578____Item_PropertyInfo,
	NULL
};
extern Il2CppType t477_0_0_0;
static ParameterInfo t5578_m29202_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t477_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29202_GM;
MethodInfo m29202_MI = 
{
	"IndexOf", NULL, &t5578_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5578_m29202_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29202_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t477_0_0_0;
static ParameterInfo t5578_m29203_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t477_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29203_GM;
MethodInfo m29203_MI = 
{
	"Insert", NULL, &t5578_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5578_m29203_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29203_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5578_m29204_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29204_GM;
MethodInfo m29204_MI = 
{
	"RemoveAt", NULL, &t5578_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5578_m29204_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29204_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5578_m29200_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t477_0_0_0;
extern void* RuntimeInvoker_t477_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29200_GM;
MethodInfo m29200_MI = 
{
	"get_Item", NULL, &t5578_TI, &t477_0_0_0, RuntimeInvoker_t477_t44, t5578_m29200_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29200_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t477_0_0_0;
static ParameterInfo t5578_m29201_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t477_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29201_GM;
MethodInfo m29201_MI = 
{
	"set_Item", NULL, &t5578_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5578_m29201_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29201_GM};
static MethodInfo* t5578_MIs[] =
{
	&m29202_MI,
	&m29203_MI,
	&m29204_MI,
	&m29200_MI,
	&m29201_MI,
	NULL
};
static TypeInfo* t5578_ITIs[] = 
{
	&t603_TI,
	&t5577_TI,
	&t5579_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5578_0_0_0;
extern Il2CppType t5578_1_0_0;
struct t5578;
extern Il2CppGenericClass t5578_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5578_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5578_MIs, t5578_PIs, NULL, NULL, NULL, NULL, NULL, &t5578_TI, t5578_ITIs, NULL, &t1908__CustomAttributeCache, &t5578_TI, &t5578_0_0_0, &t5578_1_0_0, NULL, &t5578_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4361_TI;

#include "t463.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.GUISkin>
extern MethodInfo m29205_MI;
static PropertyInfo t4361____Current_PropertyInfo = 
{
	&t4361_TI, "Current", &m29205_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4361_PIs[] =
{
	&t4361____Current_PropertyInfo,
	NULL
};
extern Il2CppType t463_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29205_GM;
MethodInfo m29205_MI = 
{
	"get_Current", NULL, &t4361_TI, &t463_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29205_GM};
static MethodInfo* t4361_MIs[] =
{
	&m29205_MI,
	NULL
};
static TypeInfo* t4361_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4361_0_0_0;
extern Il2CppType t4361_1_0_0;
struct t4361;
extern Il2CppGenericClass t4361_GC;
TypeInfo t4361_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4361_MIs, t4361_PIs, NULL, NULL, NULL, NULL, NULL, &t4361_TI, t4361_ITIs, NULL, &EmptyCustomAttributesCache, &t4361_TI, &t4361_0_0_0, &t4361_1_0_0, NULL, &t4361_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2886.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2886_TI;
#include "t2886MD.h"

extern TypeInfo t463_TI;
extern MethodInfo m15763_MI;
extern MethodInfo m22163_MI;
struct t20;
#define m22163(__this, p0, method) (t463 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.GUISkin>
extern Il2CppType t20_0_0_1;
FieldInfo t2886_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2886_TI, offsetof(t2886, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2886_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2886_TI, offsetof(t2886, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2886_FIs[] =
{
	&t2886_f0_FieldInfo,
	&t2886_f1_FieldInfo,
	NULL
};
extern MethodInfo m15760_MI;
static PropertyInfo t2886____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2886_TI, "System.Collections.IEnumerator.Current", &m15760_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2886____Current_PropertyInfo = 
{
	&t2886_TI, "Current", &m15763_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2886_PIs[] =
{
	&t2886____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2886____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2886_m15759_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15759_GM;
MethodInfo m15759_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2886_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2886_m15759_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15759_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15760_GM;
MethodInfo m15760_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2886_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15760_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15761_GM;
MethodInfo m15761_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2886_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15761_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15762_GM;
MethodInfo m15762_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2886_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15762_GM};
extern Il2CppType t463_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15763_GM;
MethodInfo m15763_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2886_TI, &t463_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15763_GM};
static MethodInfo* t2886_MIs[] =
{
	&m15759_MI,
	&m15760_MI,
	&m15761_MI,
	&m15762_MI,
	&m15763_MI,
	NULL
};
extern MethodInfo m15762_MI;
extern MethodInfo m15761_MI;
static MethodInfo* t2886_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15760_MI,
	&m15762_MI,
	&m15761_MI,
	&m15763_MI,
};
static TypeInfo* t2886_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4361_TI,
};
static Il2CppInterfaceOffsetPair t2886_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4361_TI, 7},
};
extern TypeInfo t463_TI;
static Il2CppRGCTXData t2886_RGCTXData[3] = 
{
	&m15763_MI/* Method Usage */,
	&t463_TI/* Class Usage */,
	&m22163_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2886_0_0_0;
extern Il2CppType t2886_1_0_0;
extern Il2CppGenericClass t2886_GC;
TypeInfo t2886_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2886_MIs, t2886_PIs, t2886_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2886_TI, t2886_ITIs, t2886_VT, &EmptyCustomAttributesCache, &t2886_TI, &t2886_0_0_0, &t2886_1_0_0, t2886_IOs, &t2886_GC, NULL, NULL, NULL, t2886_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2886)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5580_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.GUISkin>
extern MethodInfo m29206_MI;
static PropertyInfo t5580____Count_PropertyInfo = 
{
	&t5580_TI, "Count", &m29206_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29207_MI;
static PropertyInfo t5580____IsReadOnly_PropertyInfo = 
{
	&t5580_TI, "IsReadOnly", &m29207_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5580_PIs[] =
{
	&t5580____Count_PropertyInfo,
	&t5580____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29206_GM;
MethodInfo m29206_MI = 
{
	"get_Count", NULL, &t5580_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29206_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29207_GM;
MethodInfo m29207_MI = 
{
	"get_IsReadOnly", NULL, &t5580_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29207_GM};
extern Il2CppType t463_0_0_0;
extern Il2CppType t463_0_0_0;
static ParameterInfo t5580_m29208_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t463_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29208_GM;
MethodInfo m29208_MI = 
{
	"Add", NULL, &t5580_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5580_m29208_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29208_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29209_GM;
MethodInfo m29209_MI = 
{
	"Clear", NULL, &t5580_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29209_GM};
extern Il2CppType t463_0_0_0;
static ParameterInfo t5580_m29210_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t463_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29210_GM;
MethodInfo m29210_MI = 
{
	"Contains", NULL, &t5580_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5580_m29210_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29210_GM};
extern Il2CppType t3745_0_0_0;
extern Il2CppType t3745_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5580_m29211_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3745_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29211_GM;
MethodInfo m29211_MI = 
{
	"CopyTo", NULL, &t5580_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5580_m29211_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29211_GM};
extern Il2CppType t463_0_0_0;
static ParameterInfo t5580_m29212_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t463_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29212_GM;
MethodInfo m29212_MI = 
{
	"Remove", NULL, &t5580_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5580_m29212_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29212_GM};
static MethodInfo* t5580_MIs[] =
{
	&m29206_MI,
	&m29207_MI,
	&m29208_MI,
	&m29209_MI,
	&m29210_MI,
	&m29211_MI,
	&m29212_MI,
	NULL
};
extern TypeInfo t5582_TI;
static TypeInfo* t5580_ITIs[] = 
{
	&t603_TI,
	&t5582_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5580_0_0_0;
extern Il2CppType t5580_1_0_0;
struct t5580;
extern Il2CppGenericClass t5580_GC;
TypeInfo t5580_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5580_MIs, t5580_PIs, NULL, NULL, NULL, NULL, NULL, &t5580_TI, t5580_ITIs, NULL, &EmptyCustomAttributesCache, &t5580_TI, &t5580_0_0_0, &t5580_1_0_0, NULL, &t5580_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.GUISkin>
extern Il2CppType t4361_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29213_GM;
MethodInfo m29213_MI = 
{
	"GetEnumerator", NULL, &t5582_TI, &t4361_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29213_GM};
static MethodInfo* t5582_MIs[] =
{
	&m29213_MI,
	NULL
};
static TypeInfo* t5582_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5582_0_0_0;
extern Il2CppType t5582_1_0_0;
struct t5582;
extern Il2CppGenericClass t5582_GC;
TypeInfo t5582_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5582_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5582_TI, t5582_ITIs, NULL, &EmptyCustomAttributesCache, &t5582_TI, &t5582_0_0_0, &t5582_1_0_0, NULL, &t5582_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5581_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.GUISkin>
extern MethodInfo m29214_MI;
extern MethodInfo m29215_MI;
static PropertyInfo t5581____Item_PropertyInfo = 
{
	&t5581_TI, "Item", &m29214_MI, &m29215_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5581_PIs[] =
{
	&t5581____Item_PropertyInfo,
	NULL
};
extern Il2CppType t463_0_0_0;
static ParameterInfo t5581_m29216_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t463_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29216_GM;
MethodInfo m29216_MI = 
{
	"IndexOf", NULL, &t5581_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5581_m29216_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29216_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t463_0_0_0;
static ParameterInfo t5581_m29217_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t463_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29217_GM;
MethodInfo m29217_MI = 
{
	"Insert", NULL, &t5581_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5581_m29217_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29217_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5581_m29218_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29218_GM;
MethodInfo m29218_MI = 
{
	"RemoveAt", NULL, &t5581_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5581_m29218_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29218_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5581_m29214_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t463_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29214_GM;
MethodInfo m29214_MI = 
{
	"get_Item", NULL, &t5581_TI, &t463_0_0_0, RuntimeInvoker_t29_t44, t5581_m29214_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29214_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t463_0_0_0;
static ParameterInfo t5581_m29215_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t463_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29215_GM;
MethodInfo m29215_MI = 
{
	"set_Item", NULL, &t5581_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5581_m29215_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29215_GM};
static MethodInfo* t5581_MIs[] =
{
	&m29216_MI,
	&m29217_MI,
	&m29218_MI,
	&m29214_MI,
	&m29215_MI,
	NULL
};
static TypeInfo* t5581_ITIs[] = 
{
	&t603_TI,
	&t5580_TI,
	&t5582_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5581_0_0_0;
extern Il2CppType t5581_1_0_0;
struct t5581;
extern Il2CppGenericClass t5581_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5581_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5581_MIs, t5581_PIs, NULL, NULL, NULL, NULL, NULL, &t5581_TI, t5581_ITIs, NULL, &t1908__CustomAttributeCache, &t5581_TI, &t5581_0_0_0, &t5581_1_0_0, NULL, &t5581_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2887.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2887_TI;
#include "t2887MD.h"

#include "t41.h"
#include "t557.h"
#include "t2888.h"
extern TypeInfo t316_TI;
extern TypeInfo t2888_TI;
#include "t2888MD.h"
extern MethodInfo m15766_MI;
extern MethodInfo m15768_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.GUISkin>
extern Il2CppType t316_0_0_33;
FieldInfo t2887_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2887_TI, offsetof(t2887, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2887_FIs[] =
{
	&t2887_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t463_0_0_0;
static ParameterInfo t2887_m15764_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t463_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15764_GM;
MethodInfo m15764_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2887_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2887_m15764_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15764_GM};
extern Il2CppType t316_0_0_0;
extern Il2CppType t316_0_0_0;
static ParameterInfo t2887_m15765_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15765_GM;
MethodInfo m15765_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2887_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2887_m15765_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15765_GM};
static MethodInfo* t2887_MIs[] =
{
	&m15764_MI,
	&m15765_MI,
	NULL
};
extern MethodInfo m15765_MI;
extern MethodInfo m15769_MI;
static MethodInfo* t2887_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15765_MI,
	&m15769_MI,
};
extern Il2CppType t2889_0_0_0;
extern TypeInfo t2889_TI;
extern MethodInfo m22173_MI;
extern TypeInfo t463_TI;
extern MethodInfo m15771_MI;
extern TypeInfo t463_TI;
static Il2CppRGCTXData t2887_RGCTXData[8] = 
{
	&t2889_0_0_0/* Type Usage */,
	&t2889_TI/* Class Usage */,
	&m22173_MI/* Method Usage */,
	&t463_TI/* Class Usage */,
	&m15771_MI/* Method Usage */,
	&m15766_MI/* Method Usage */,
	&t463_TI/* Class Usage */,
	&m15768_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2887_0_0_0;
extern Il2CppType t2887_1_0_0;
struct t2887;
extern Il2CppGenericClass t2887_GC;
TypeInfo t2887_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2887_MIs, NULL, t2887_FIs, NULL, &t2888_TI, NULL, NULL, &t2887_TI, NULL, t2887_VT, &EmptyCustomAttributesCache, &t2887_TI, &t2887_0_0_0, &t2887_1_0_0, NULL, &t2887_GC, NULL, NULL, NULL, t2887_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2887), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2889.h"
#include "t353.h"
extern TypeInfo t2889_TI;
#include "t556MD.h"
#include "t353MD.h"
#include "t2889MD.h"
extern MethodInfo m2790_MI;
extern MethodInfo m2957_MI;
extern MethodInfo m1597_MI;
extern MethodInfo m2789_MI;
extern MethodInfo m2791_MI;
extern MethodInfo m2953_MI;
extern MethodInfo m2951_MI;
struct t556;
#include "t556.h"
struct t556;
 void m19555_gshared (t29 * __this, t29 * p0, MethodInfo* method);
#define m19555(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)
#define m22173(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.GUISkin>
extern Il2CppType t2889_0_0_1;
FieldInfo t2888_f0_FieldInfo = 
{
	"Delegate", &t2889_0_0_1, &t2888_TI, offsetof(t2888, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2888_FIs[] =
{
	&t2888_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2888_m15766_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15766_GM;
MethodInfo m15766_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2888_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2888_m15766_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15766_GM};
extern Il2CppType t2889_0_0_0;
static ParameterInfo t2888_m15767_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2889_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15767_GM;
MethodInfo m15767_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2888_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2888_m15767_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15767_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2888_m15768_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15768_GM;
MethodInfo m15768_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2888_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2888_m15768_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15768_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2888_m15769_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15769_GM;
MethodInfo m15769_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2888_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2888_m15769_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15769_GM};
static MethodInfo* t2888_MIs[] =
{
	&m15766_MI,
	&m15767_MI,
	&m15768_MI,
	&m15769_MI,
	NULL
};
static MethodInfo* t2888_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15768_MI,
	&m15769_MI,
};
extern TypeInfo t2889_TI;
extern TypeInfo t463_TI;
static Il2CppRGCTXData t2888_RGCTXData[5] = 
{
	&t2889_0_0_0/* Type Usage */,
	&t2889_TI/* Class Usage */,
	&m22173_MI/* Method Usage */,
	&t463_TI/* Class Usage */,
	&m15771_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2888_0_0_0;
extern Il2CppType t2888_1_0_0;
extern TypeInfo t556_TI;
struct t2888;
extern Il2CppGenericClass t2888_GC;
TypeInfo t2888_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2888_MIs, NULL, t2888_FIs, NULL, &t556_TI, NULL, NULL, &t2888_TI, NULL, t2888_VT, &EmptyCustomAttributesCache, &t2888_TI, &t2888_0_0_0, &t2888_1_0_0, NULL, &t2888_GC, NULL, NULL, NULL, t2888_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2888), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.GUISkin>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2889_m15770_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15770_GM;
MethodInfo m15770_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2889_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2889_m15770_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15770_GM};
extern Il2CppType t463_0_0_0;
static ParameterInfo t2889_m15771_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t463_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15771_GM;
MethodInfo m15771_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2889_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2889_m15771_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15771_GM};
extern Il2CppType t463_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2889_m15772_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t463_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15772_GM;
MethodInfo m15772_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2889_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2889_m15772_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15772_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2889_m15773_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15773_GM;
MethodInfo m15773_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2889_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2889_m15773_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15773_GM};
static MethodInfo* t2889_MIs[] =
{
	&m15770_MI,
	&m15771_MI,
	&m15772_MI,
	&m15773_MI,
	NULL
};
extern MethodInfo m15772_MI;
extern MethodInfo m15773_MI;
static MethodInfo* t2889_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m15771_MI,
	&m15772_MI,
	&m15773_MI,
};
static Il2CppInterfaceOffsetPair t2889_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2889_1_0_0;
struct t2889;
extern Il2CppGenericClass t2889_GC;
TypeInfo t2889_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2889_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2889_TI, NULL, t2889_VT, &EmptyCustomAttributesCache, &t2889_TI, &t2889_0_0_0, &t2889_1_0_0, t2889_IOs, &t2889_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2889), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2897_TI;

#include "t466.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.GUIStyle>
extern MethodInfo m29219_MI;
static PropertyInfo t2897____Current_PropertyInfo = 
{
	&t2897_TI, "Current", &m29219_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2897_PIs[] =
{
	&t2897____Current_PropertyInfo,
	NULL
};
extern Il2CppType t466_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29219_GM;
MethodInfo m29219_MI = 
{
	"get_Current", NULL, &t2897_TI, &t466_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29219_GM};
static MethodInfo* t2897_MIs[] =
{
	&m29219_MI,
	NULL
};
static TypeInfo* t2897_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2897_0_0_0;
extern Il2CppType t2897_1_0_0;
struct t2897;
extern Il2CppGenericClass t2897_GC;
TypeInfo t2897_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2897_MIs, t2897_PIs, NULL, NULL, NULL, NULL, NULL, &t2897_TI, t2897_ITIs, NULL, &EmptyCustomAttributesCache, &t2897_TI, &t2897_0_0_0, &t2897_1_0_0, NULL, &t2897_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2890.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2890_TI;
#include "t2890MD.h"

extern TypeInfo t466_TI;
extern MethodInfo m15778_MI;
extern MethodInfo m22175_MI;
struct t20;
#define m22175(__this, p0, method) (t466 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.GUIStyle>
extern Il2CppType t20_0_0_1;
FieldInfo t2890_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2890_TI, offsetof(t2890, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2890_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2890_TI, offsetof(t2890, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2890_FIs[] =
{
	&t2890_f0_FieldInfo,
	&t2890_f1_FieldInfo,
	NULL
};
extern MethodInfo m15775_MI;
static PropertyInfo t2890____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2890_TI, "System.Collections.IEnumerator.Current", &m15775_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2890____Current_PropertyInfo = 
{
	&t2890_TI, "Current", &m15778_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2890_PIs[] =
{
	&t2890____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2890____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2890_m15774_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15774_GM;
MethodInfo m15774_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2890_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2890_m15774_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15774_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15775_GM;
MethodInfo m15775_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2890_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15775_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15776_GM;
MethodInfo m15776_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2890_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15776_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15777_GM;
MethodInfo m15777_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2890_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15777_GM};
extern Il2CppType t466_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15778_GM;
MethodInfo m15778_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2890_TI, &t466_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15778_GM};
static MethodInfo* t2890_MIs[] =
{
	&m15774_MI,
	&m15775_MI,
	&m15776_MI,
	&m15777_MI,
	&m15778_MI,
	NULL
};
extern MethodInfo m15777_MI;
extern MethodInfo m15776_MI;
static MethodInfo* t2890_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15775_MI,
	&m15777_MI,
	&m15776_MI,
	&m15778_MI,
};
static TypeInfo* t2890_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2897_TI,
};
static Il2CppInterfaceOffsetPair t2890_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2897_TI, 7},
};
extern TypeInfo t466_TI;
static Il2CppRGCTXData t2890_RGCTXData[3] = 
{
	&m15778_MI/* Method Usage */,
	&t466_TI/* Class Usage */,
	&m22175_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2890_0_0_0;
extern Il2CppType t2890_1_0_0;
extern Il2CppGenericClass t2890_GC;
TypeInfo t2890_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2890_MIs, t2890_PIs, t2890_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2890_TI, t2890_ITIs, t2890_VT, &EmptyCustomAttributesCache, &t2890_TI, &t2890_0_0_0, &t2890_1_0_0, t2890_IOs, &t2890_GC, NULL, NULL, NULL, t2890_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2890)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5583_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.GUIStyle>
extern MethodInfo m29220_MI;
static PropertyInfo t5583____Count_PropertyInfo = 
{
	&t5583_TI, "Count", &m29220_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29221_MI;
static PropertyInfo t5583____IsReadOnly_PropertyInfo = 
{
	&t5583_TI, "IsReadOnly", &m29221_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5583_PIs[] =
{
	&t5583____Count_PropertyInfo,
	&t5583____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29220_GM;
MethodInfo m29220_MI = 
{
	"get_Count", NULL, &t5583_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29220_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29221_GM;
MethodInfo m29221_MI = 
{
	"get_IsReadOnly", NULL, &t5583_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29221_GM};
extern Il2CppType t466_0_0_0;
extern Il2CppType t466_0_0_0;
static ParameterInfo t5583_m29222_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t466_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29222_GM;
MethodInfo m29222_MI = 
{
	"Add", NULL, &t5583_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5583_m29222_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29222_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29223_GM;
MethodInfo m29223_MI = 
{
	"Clear", NULL, &t5583_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29223_GM};
extern Il2CppType t466_0_0_0;
static ParameterInfo t5583_m29224_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t466_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29224_GM;
MethodInfo m29224_MI = 
{
	"Contains", NULL, &t5583_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5583_m29224_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29224_GM};
extern Il2CppType t482_0_0_0;
extern Il2CppType t482_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5583_m29225_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t482_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29225_GM;
MethodInfo m29225_MI = 
{
	"CopyTo", NULL, &t5583_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5583_m29225_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29225_GM};
extern Il2CppType t466_0_0_0;
static ParameterInfo t5583_m29226_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t466_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29226_GM;
MethodInfo m29226_MI = 
{
	"Remove", NULL, &t5583_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5583_m29226_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29226_GM};
static MethodInfo* t5583_MIs[] =
{
	&m29220_MI,
	&m29221_MI,
	&m29222_MI,
	&m29223_MI,
	&m29224_MI,
	&m29225_MI,
	&m29226_MI,
	NULL
};
extern TypeInfo t5585_TI;
static TypeInfo* t5583_ITIs[] = 
{
	&t603_TI,
	&t5585_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5583_0_0_0;
extern Il2CppType t5583_1_0_0;
struct t5583;
extern Il2CppGenericClass t5583_GC;
TypeInfo t5583_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5583_MIs, t5583_PIs, NULL, NULL, NULL, NULL, NULL, &t5583_TI, t5583_ITIs, NULL, &EmptyCustomAttributesCache, &t5583_TI, &t5583_0_0_0, &t5583_1_0_0, NULL, &t5583_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.GUIStyle>
extern Il2CppType t2897_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29227_GM;
MethodInfo m29227_MI = 
{
	"GetEnumerator", NULL, &t5585_TI, &t2897_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29227_GM};
static MethodInfo* t5585_MIs[] =
{
	&m29227_MI,
	NULL
};
static TypeInfo* t5585_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5585_0_0_0;
extern Il2CppType t5585_1_0_0;
struct t5585;
extern Il2CppGenericClass t5585_GC;
TypeInfo t5585_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5585_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5585_TI, t5585_ITIs, NULL, &EmptyCustomAttributesCache, &t5585_TI, &t5585_0_0_0, &t5585_1_0_0, NULL, &t5585_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5584_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.GUIStyle>
extern MethodInfo m29228_MI;
extern MethodInfo m29229_MI;
static PropertyInfo t5584____Item_PropertyInfo = 
{
	&t5584_TI, "Item", &m29228_MI, &m29229_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5584_PIs[] =
{
	&t5584____Item_PropertyInfo,
	NULL
};
extern Il2CppType t466_0_0_0;
static ParameterInfo t5584_m29230_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t466_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29230_GM;
MethodInfo m29230_MI = 
{
	"IndexOf", NULL, &t5584_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5584_m29230_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29230_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t466_0_0_0;
static ParameterInfo t5584_m29231_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t466_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29231_GM;
MethodInfo m29231_MI = 
{
	"Insert", NULL, &t5584_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5584_m29231_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29231_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5584_m29232_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29232_GM;
MethodInfo m29232_MI = 
{
	"RemoveAt", NULL, &t5584_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5584_m29232_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29232_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5584_m29228_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t466_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29228_GM;
MethodInfo m29228_MI = 
{
	"get_Item", NULL, &t5584_TI, &t466_0_0_0, RuntimeInvoker_t29_t44, t5584_m29228_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29228_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t466_0_0_0;
static ParameterInfo t5584_m29229_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t466_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29229_GM;
MethodInfo m29229_MI = 
{
	"set_Item", NULL, &t5584_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5584_m29229_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29229_GM};
static MethodInfo* t5584_MIs[] =
{
	&m29230_MI,
	&m29231_MI,
	&m29232_MI,
	&m29228_MI,
	&m29229_MI,
	NULL
};
static TypeInfo* t5584_ITIs[] = 
{
	&t603_TI,
	&t5583_TI,
	&t5585_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5584_0_0_0;
extern Il2CppType t5584_1_0_0;
struct t5584;
extern Il2CppGenericClass t5584_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5584_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5584_MIs, t5584_PIs, NULL, NULL, NULL, NULL, NULL, &t5584_TI, t5584_ITIs, NULL, &t1908__CustomAttributeCache, &t5584_TI, &t5584_0_0_0, &t5584_1_0_0, NULL, &t5584_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t483.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t483_TI;
#include "t483MD.h"

#include "t1248.h"
#include "t1258.h"
#include "t592.h"
#include "t733.h"
#include "t735.h"
#include "t2893.h"
#include "t2891.h"
#include "t2899.h"
#include "t2895.h"
#include "t2900.h"
#include "t2901.h"
#include "t2904.h"
extern TypeInfo t2096_TI;
extern TypeInfo t1248_TI;
extern TypeInfo t1258_TI;
extern TypeInfo t592_TI;
extern TypeInfo t2892_TI;
extern TypeInfo t2893_TI;
extern TypeInfo t3541_TI;
extern TypeInfo t2891_TI;
extern TypeInfo t2899_TI;
extern TypeInfo t2895_TI;
extern TypeInfo t2900_TI;
extern TypeInfo t2901_TI;
extern TypeInfo t841_TI;
extern TypeInfo t1965_TI;
extern TypeInfo t446_TI;
extern TypeInfo t482_TI;
extern TypeInfo t719_TI;
extern TypeInfo t2904_TI;
extern TypeInfo t6720_TI;
#include "t1258MD.h"
#include "t592MD.h"
#include "t2893MD.h"
#include "t2891MD.h"
#include "t2899MD.h"
#include "t2895MD.h"
#include "t2900MD.h"
#include "t2901MD.h"
#include "t719MD.h"
#include "t2904MD.h"
#include "t733MD.h"
#include "t7MD.h"
#include "t725MD.h"
extern Il2CppType t2096_0_0_0;
extern Il2CppType t2892_0_0_0;
extern Il2CppType t7_0_0_0;
extern MethodInfo m15808_MI;
extern MethodInfo m15813_MI;
extern MethodInfo m15798_MI;
extern MethodInfo m15814_MI;
extern MethodInfo m2864_MI;
extern MethodInfo m29233_MI;
extern MethodInfo m29234_MI;
extern MethodInfo m6569_MI;
extern MethodInfo m15805_MI;
extern MethodInfo m15829_MI;
extern MethodInfo m15799_MI;
extern MethodInfo m15806_MI;
extern MethodInfo m15812_MI;
extern MethodInfo m15819_MI;
extern MethodInfo m15821_MI;
extern MethodInfo m15815_MI;
extern MethodInfo m15804_MI;
extern MethodInfo m15801_MI;
extern MethodInfo m15817_MI;
extern MethodInfo m15863_MI;
extern MethodInfo m22199_MI;
extern MethodInfo m15802_MI;
extern MethodInfo m15867_MI;
extern MethodInfo m22201_MI;
extern MethodInfo m15847_MI;
extern MethodInfo m15871_MI;
extern MethodInfo m15881_MI;
extern MethodInfo m15800_MI;
extern MethodInfo m15797_MI;
extern MethodInfo m15818_MI;
extern MethodInfo m22202_MI;
extern MethodInfo m6736_MI;
extern MethodInfo m15892_MI;
extern MethodInfo m29235_MI;
extern MethodInfo m3984_MI;
extern MethodInfo m3997_MI;
extern MethodInfo m3996_MI;
extern MethodInfo m3985_MI;
extern MethodInfo m6035_MI;
extern MethodInfo m66_MI;
extern MethodInfo m3973_MI;
extern MethodInfo m2865_MI;
extern MethodInfo m29236_MI;
extern MethodInfo m3965_MI;
struct t483;
 void m22199 (t483 * __this, t3541* p0, int32_t p1, t2891 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t483;
 void m22201 (t483 * __this, t20 * p0, int32_t p1, t2899 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t483;
 void m22202 (t483 * __this, t2892* p0, int32_t p1, t2899 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m15789_MI;
 void m15789 (t483 * __this, t2893  p0, MethodInfo* method){
	{
		t7* L_0 = m15819((&p0), &m15819_MI);
		t466 * L_1 = m15821((&p0), &m15821_MI);
		VirtActionInvoker2< t7*, t466 * >::Invoke(&m15806_MI, __this, L_0, L_1);
		return;
	}
}
extern MethodInfo m15790_MI;
 bool m15790 (t483 * __this, t2893  p0, MethodInfo* method){
	{
		bool L_0 = m15815(__this, p0, &m15815_MI);
		return L_0;
	}
}
extern MethodInfo m15792_MI;
 bool m15792 (t483 * __this, t2893  p0, MethodInfo* method){
	{
		bool L_0 = m15815(__this, p0, &m15815_MI);
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return 0;
	}

IL_000b:
	{
		t7* L_1 = m15819((&p0), &m15819_MI);
		bool L_2 = (bool)VirtFuncInvoker1< bool, t7* >::Invoke(&m15812_MI, __this, L_1);
		return L_2;
	}
}
 t2893  m15802 (t29 * __this, t7* p0, t466 * p1, MethodInfo* method){
	{
		t2893  L_0 = {0};
		m15818(&L_0, p0, p1, &m15818_MI);
		return L_0;
	}
}
 bool m15815 (t483 * __this, t2893  p0, MethodInfo* method){
	t466 * V_0 = {0};
	{
		t7* L_0 = m15819((&p0), &m15819_MI);
		bool L_1 = (bool)VirtFuncInvoker2< bool, t7*, t466 ** >::Invoke(&m2865_MI, __this, L_0, (&V_0));
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		return 0;
	}

IL_0013:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t2904_TI));
		t2904 * L_2 = m15892(NULL, &m15892_MI);
		t466 * L_3 = m15821((&p0), &m15821_MI);
		bool L_4 = (bool)VirtFuncInvoker2< bool, t466 *, t466 * >::Invoke(&m29236_MI, L_2, L_3, V_0);
		return L_4;
	}
}
extern MethodInfo m15816_MI;
 t2895  m15816 (t483 * __this, MethodInfo* method){
	{
		t2895  L_0 = {0};
		m15847(&L_0, __this, &m15847_MI);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>
extern Il2CppType t44_0_0_32849;
FieldInfo t483_f0_FieldInfo = 
{
	"INITIAL_SIZE", &t44_0_0_32849, &t483_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_32849;
FieldInfo t483_f1_FieldInfo = 
{
	"DEFAULT_LOAD_FACTOR", &t22_0_0_32849, &t483_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_32849;
FieldInfo t483_f2_FieldInfo = 
{
	"NO_SLOT", &t44_0_0_32849, &t483_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_32849;
FieldInfo t483_f3_FieldInfo = 
{
	"HASH_FLAG", &t44_0_0_32849, &t483_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t841_0_0_1;
FieldInfo t483_f4_FieldInfo = 
{
	"table", &t841_0_0_1, &t483_TI, offsetof(t483, f4), &EmptyCustomAttributesCache};
extern Il2CppType t1965_0_0_1;
FieldInfo t483_f5_FieldInfo = 
{
	"linkSlots", &t1965_0_0_1, &t483_TI, offsetof(t483, f5), &EmptyCustomAttributesCache};
extern Il2CppType t446_0_0_1;
FieldInfo t483_f6_FieldInfo = 
{
	"keySlots", &t446_0_0_1, &t483_TI, offsetof(t483, f6), &EmptyCustomAttributesCache};
extern Il2CppType t482_0_0_1;
FieldInfo t483_f7_FieldInfo = 
{
	"valueSlots", &t482_0_0_1, &t483_TI, offsetof(t483, f7), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t483_f8_FieldInfo = 
{
	"touchedSlots", &t44_0_0_1, &t483_TI, offsetof(t483, f8), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t483_f9_FieldInfo = 
{
	"emptySlot", &t44_0_0_1, &t483_TI, offsetof(t483, f9), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t483_f10_FieldInfo = 
{
	"count", &t44_0_0_1, &t483_TI, offsetof(t483, f10), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t483_f11_FieldInfo = 
{
	"threshold", &t44_0_0_1, &t483_TI, offsetof(t483, f11), &EmptyCustomAttributesCache};
extern Il2CppType t2096_0_0_1;
FieldInfo t483_f12_FieldInfo = 
{
	"hcp", &t2096_0_0_1, &t483_TI, offsetof(t483, f12), &EmptyCustomAttributesCache};
extern Il2CppType t733_0_0_1;
FieldInfo t483_f13_FieldInfo = 
{
	"serialization_info", &t733_0_0_1, &t483_TI, offsetof(t483, f13), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t483_f14_FieldInfo = 
{
	"generation", &t44_0_0_1, &t483_TI, offsetof(t483, f14), &EmptyCustomAttributesCache};
extern Il2CppType t2891_0_0_17;
extern CustomAttributesCache t1254__CustomAttributeCache_U3CU3Ef__am$cacheB;
FieldInfo t483_f15_FieldInfo = 
{
	"<>f__am$cacheB", &t2891_0_0_17, &t483_TI, offsetof(t483_SFs, f15), &t1254__CustomAttributeCache_U3CU3Ef__am$cacheB};
static FieldInfo* t483_FIs[] =
{
	&t483_f0_FieldInfo,
	&t483_f1_FieldInfo,
	&t483_f2_FieldInfo,
	&t483_f3_FieldInfo,
	&t483_f4_FieldInfo,
	&t483_f5_FieldInfo,
	&t483_f6_FieldInfo,
	&t483_f7_FieldInfo,
	&t483_f8_FieldInfo,
	&t483_f9_FieldInfo,
	&t483_f10_FieldInfo,
	&t483_f11_FieldInfo,
	&t483_f12_FieldInfo,
	&t483_f13_FieldInfo,
	&t483_f14_FieldInfo,
	&t483_f15_FieldInfo,
	NULL
};
static const int32_t t483_f0_DefaultValueData = 10;
static Il2CppFieldDefaultValueEntry t483_f0_DefaultValue = 
{
	&t483_f0_FieldInfo, { (char*)&t483_f0_DefaultValueData, &t44_0_0_0 }};
static const float t483_f1_DefaultValueData = 0.9f;
extern Il2CppType t22_0_0_0;
static Il2CppFieldDefaultValueEntry t483_f1_DefaultValue = 
{
	&t483_f1_FieldInfo, { (char*)&t483_f1_DefaultValueData, &t22_0_0_0 }};
static const int32_t t483_f2_DefaultValueData = -1;
static Il2CppFieldDefaultValueEntry t483_f2_DefaultValue = 
{
	&t483_f2_FieldInfo, { (char*)&t483_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t483_f3_DefaultValueData = -2147483648;
static Il2CppFieldDefaultValueEntry t483_f3_DefaultValue = 
{
	&t483_f3_FieldInfo, { (char*)&t483_f3_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t483_FDVs[] = 
{
	&t483_f0_DefaultValue,
	&t483_f1_DefaultValue,
	&t483_f2_DefaultValue,
	&t483_f3_DefaultValue,
	NULL
};
extern MethodInfo m15782_MI;
extern MethodInfo m15783_MI;
static PropertyInfo t483____System_Collections_IDictionary_Item_PropertyInfo = 
{
	&t483_TI, "System.Collections.IDictionary.Item", &m15782_MI, &m15783_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15786_MI;
static PropertyInfo t483____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t483_TI, "System.Collections.ICollection.IsSynchronized", &m15786_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15787_MI;
static PropertyInfo t483____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t483_TI, "System.Collections.ICollection.SyncRoot", &m15787_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15788_MI;
static PropertyInfo t483____System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_IsReadOnly_PropertyInfo = 
{
	&t483_TI, "System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.IsReadOnly", &m15788_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t483____Count_PropertyInfo = 
{
	&t483_TI, "Count", &m15797_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t483____Item_PropertyInfo = 
{
	&t483_TI, "Item", &m15798_MI, &m2864_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m2866_MI;
static PropertyInfo t483____Values_PropertyInfo = 
{
	&t483_TI, "Values", &m2866_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t483_PIs[] =
{
	&t483____System_Collections_IDictionary_Item_PropertyInfo,
	&t483____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t483____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t483____System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_IsReadOnly_PropertyInfo,
	&t483____Count_PropertyInfo,
	&t483____Item_PropertyInfo,
	&t483____Values_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15779_GM;
MethodInfo m15779_MI = 
{
	".ctor", (methodPointerType)&m12834_gshared, &t483_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15779_GM};
extern Il2CppType t2096_0_0_0;
static ParameterInfo t483_m2863_ParameterInfos[] = 
{
	{"comparer", 0, 134217728, &EmptyCustomAttributesCache, &t2096_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2863_GM;
MethodInfo m2863_MI = 
{
	".ctor", (methodPointerType)&m12836_gshared, &t483_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t483_m2863_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m2863_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t483_m15780_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15780_GM;
MethodInfo m15780_MI = 
{
	".ctor", (methodPointerType)&m12838_gshared, &t483_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t483_m15780_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15780_GM};
extern Il2CppType t733_0_0_0;
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t483_m15781_ParameterInfos[] = 
{
	{"info", 0, 134217728, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134217728, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15781_GM;
MethodInfo m15781_MI = 
{
	".ctor", (methodPointerType)&m12840_gshared, &t483_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t483_m15781_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15781_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t483_m15782_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15782_GM;
MethodInfo m15782_MI = 
{
	"System.Collections.IDictionary.get_Item", (methodPointerType)&m12842_gshared, &t483_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t483_m15782_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15782_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t483_m15783_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15783_GM;
MethodInfo m15783_MI = 
{
	"System.Collections.IDictionary.set_Item", (methodPointerType)&m12844_gshared, &t483_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t483_m15783_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 20, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15783_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t483_m15784_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15784_GM;
MethodInfo m15784_MI = 
{
	"System.Collections.IDictionary.Add", (methodPointerType)&m12846_gshared, &t483_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t483_m15784_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 21, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15784_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t483_m15785_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15785_GM;
MethodInfo m15785_MI = 
{
	"System.Collections.IDictionary.Remove", (methodPointerType)&m12848_gshared, &t483_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t483_m15785_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 23, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15785_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15786_GM;
MethodInfo m15786_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m12850_gshared, &t483_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15786_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15787_GM;
MethodInfo m15787_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m12852_gshared, &t483_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15787_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15788_GM;
MethodInfo m15788_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly", (methodPointerType)&m12854_gshared, &t483_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 11, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15788_GM};
extern Il2CppType t2893_0_0_0;
extern Il2CppType t2893_0_0_0;
static ParameterInfo t483_m15789_ParameterInfos[] = 
{
	{"keyValuePair", 0, 134217728, &EmptyCustomAttributesCache, &t2893_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t2893 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15789_GM;
MethodInfo m15789_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add", (methodPointerType)&m15789, &t483_TI, &t21_0_0_0, RuntimeInvoker_t21_t2893, t483_m15789_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15789_GM};
extern Il2CppType t2893_0_0_0;
static ParameterInfo t483_m15790_ParameterInfos[] = 
{
	{"keyValuePair", 0, 134217728, &EmptyCustomAttributesCache, &t2893_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2893 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15790_GM;
MethodInfo m15790_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains", (methodPointerType)&m15790, &t483_TI, &t40_0_0_0, RuntimeInvoker_t40_t2893, t483_m15790_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 14, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15790_GM};
extern Il2CppType t2892_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t483_m15791_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2892_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15791_GM;
MethodInfo m15791_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo", (methodPointerType)&m12858_gshared, &t483_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t483_m15791_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15791_GM};
extern Il2CppType t2893_0_0_0;
static ParameterInfo t483_m15792_ParameterInfos[] = 
{
	{"keyValuePair", 0, 134217728, &EmptyCustomAttributesCache, &t2893_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2893 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15792_GM;
MethodInfo m15792_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove", (methodPointerType)&m15792, &t483_TI, &t40_0_0_0, RuntimeInvoker_t40_t2893, t483_m15792_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15792_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t483_m15793_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15793_GM;
MethodInfo m15793_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m12861_gshared, &t483_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t483_m15793_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15793_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15794_GM;
MethodInfo m15794_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m12863_gshared, &t483_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15794_GM};
extern Il2CppType t2894_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15795_GM;
MethodInfo m15795_MI = 
{
	"System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator", (methodPointerType)&m12865_gshared, &t483_TI, &t2894_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 17, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15795_GM};
extern Il2CppType t722_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15796_GM;
MethodInfo m15796_MI = 
{
	"System.Collections.IDictionary.GetEnumerator", (methodPointerType)&m12867_gshared, &t483_TI, &t722_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 22, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15796_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15797_GM;
MethodInfo m15797_MI = 
{
	"get_Count", (methodPointerType)&m12869_gshared, &t483_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15797_GM};
extern Il2CppType t7_0_0_0;
static ParameterInfo t483_m15798_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t466_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15798_GM;
MethodInfo m15798_MI = 
{
	"get_Item", (methodPointerType)&m12871_gshared, &t483_TI, &t466_0_0_0, RuntimeInvoker_t29_t29, t483_m15798_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 25, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15798_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t466_0_0_0;
static ParameterInfo t483_m2864_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t466_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2864_GM;
MethodInfo m2864_MI = 
{
	"set_Item", (methodPointerType)&m12873_gshared, &t483_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t483_m2864_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 26, 2, false, true, 0, NULL, (methodPointerType)NULL, &m2864_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2096_0_0_0;
static ParameterInfo t483_m15799_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"hcp", 1, 134217728, &EmptyCustomAttributesCache, &t2096_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15799_GM;
MethodInfo m15799_MI = 
{
	"Init", (methodPointerType)&m12875_gshared, &t483_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t483_m15799_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15799_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t483_m15800_ParameterInfos[] = 
{
	{"size", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15800_GM;
MethodInfo m15800_MI = 
{
	"InitArrays", (methodPointerType)&m12877_gshared, &t483_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t483_m15800_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15800_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t483_m15801_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15801_GM;
MethodInfo m15801_MI = 
{
	"CopyToCheck", (methodPointerType)&m12879_gshared, &t483_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t483_m15801_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15801_GM};
extern Il2CppType t1957_0_0_0;
extern Il2CppType t1957_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t6721_0_0_0;
extern Il2CppType t6721_0_0_0;
static ParameterInfo t483_m29237_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1957_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"transform", 2, 134217728, &EmptyCustomAttributesCache, &t6721_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericContainer m29237_IGC;
extern TypeInfo m29237_gp_TRet_0_TI;
Il2CppGenericParamFull m29237_gp_TRet_0_TI_GenericParamFull = { { &m29237_IGC, 0}, {NULL, "TRet", 0, 0, NULL} };
extern TypeInfo m29237_gp_TElem_1_TI;
Il2CppGenericParamFull m29237_gp_TElem_1_TI_GenericParamFull = { { &m29237_IGC, 1}, {NULL, "TElem", 0, 0, NULL} };
static Il2CppGenericParamFull* m29237_IGPA[2] = 
{
	&m29237_gp_TRet_0_TI_GenericParamFull,
	&m29237_gp_TElem_1_TI_GenericParamFull,
};
extern MethodInfo m29237_MI;
Il2CppGenericContainer m29237_IGC = { { NULL, NULL }, NULL, &m29237_MI, 2, 1, m29237_IGPA };
extern Il2CppGenericMethod m29238_GM;
extern Il2CppType m9954_gp_0_0_0_0;
extern Il2CppType m9954_gp_1_0_0_0;
static Il2CppRGCTXDefinition m29237_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &m29238_GM }/* Method Definition */,
	{ IL2CPP_RGCTX_DATA_CLASS, &m9954_gp_0_0_0_0 }/* Class Definition */,
	{ IL2CPP_RGCTX_DATA_CLASS, &m9954_gp_1_0_0_0 }/* Class Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppGenericMethod m29237_GM;
MethodInfo m29237_MI = 
{
	"Do_CopyTo", NULL, &t483_TI, &t21_0_0_0, NULL, t483_m29237_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, true, true, 0, m29237_RGCTXData, (methodPointerType)NULL, &m29237_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t466_0_0_0;
static ParameterInfo t483_m15802_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t466_0_0_0},
};
extern Il2CppType t2893_0_0_0;
extern void* RuntimeInvoker_t2893_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15802_GM;
MethodInfo m15802_MI = 
{
	"make_pair", (methodPointerType)&m15802, &t483_TI, &t2893_0_0_0, RuntimeInvoker_t2893_t29_t29, t483_m15802_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15802_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t466_0_0_0;
static ParameterInfo t483_m15803_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t466_0_0_0},
};
extern Il2CppType t466_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15803_GM;
MethodInfo m15803_MI = 
{
	"pick_value", (methodPointerType)&m12882_gshared, &t483_TI, &t466_0_0_0, RuntimeInvoker_t29_t29_t29, t483_m15803_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15803_GM};
extern Il2CppType t2892_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t483_m15804_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2892_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15804_GM;
MethodInfo m15804_MI = 
{
	"CopyTo", (methodPointerType)&m12884_gshared, &t483_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t483_m15804_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15804_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t6724_0_0_0;
extern Il2CppType t6724_0_0_0;
static ParameterInfo t483_m29239_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"transform", 2, 134217728, &EmptyCustomAttributesCache, &t6724_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericContainer m29239_IGC;
extern TypeInfo m29239_gp_TRet_0_TI;
Il2CppGenericParamFull m29239_gp_TRet_0_TI_GenericParamFull = { { &m29239_IGC, 0}, {NULL, "TRet", 0, 0, NULL} };
static Il2CppGenericParamFull* m29239_IGPA[1] = 
{
	&m29239_gp_TRet_0_TI_GenericParamFull,
};
extern MethodInfo m29239_MI;
Il2CppGenericContainer m29239_IGC = { { NULL, NULL }, NULL, &m29239_MI, 1, 1, m29239_IGPA };
extern Il2CppType m9959_gp_0_0_0_0;
extern Il2CppGenericMethod m29240_GM;
static Il2CppRGCTXDefinition m29239_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, &m9959_gp_0_0_0_0 }/* Type Definition */,
	{ IL2CPP_RGCTX_DATA_METHOD, &m29240_GM }/* Method Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppGenericMethod m29239_GM;
MethodInfo m29239_MI = 
{
	"Do_ICollectionCopyTo", NULL, &t483_TI, &t21_0_0_0, NULL, t483_m29239_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, true, true, 0, m29239_RGCTXData, (methodPointerType)NULL, &m29239_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15805_GM;
MethodInfo m15805_MI = 
{
	"Resize", (methodPointerType)&m12886_gshared, &t483_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15805_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t466_0_0_0;
static ParameterInfo t483_m15806_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t466_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15806_GM;
MethodInfo m15806_MI = 
{
	"Add", (methodPointerType)&m12887_gshared, &t483_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t483_m15806_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15806_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15807_GM;
MethodInfo m15807_MI = 
{
	"Clear", (methodPointerType)&m12889_gshared, &t483_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 13, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15807_GM};
extern Il2CppType t7_0_0_0;
static ParameterInfo t483_m15808_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15808_GM;
MethodInfo m15808_MI = 
{
	"ContainsKey", (methodPointerType)&m12891_gshared, &t483_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t483_m15808_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15808_GM};
extern Il2CppType t466_0_0_0;
static ParameterInfo t483_m15809_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t466_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15809_GM;
MethodInfo m15809_MI = 
{
	"ContainsValue", (methodPointerType)&m12893_gshared, &t483_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t483_m15809_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15809_GM};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t483_m15810_ParameterInfos[] = 
{
	{"info", 0, 134217728, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134217728, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15810_GM;
MethodInfo m15810_MI = 
{
	"GetObjectData", (methodPointerType)&m12895_gshared, &t483_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t483_m15810_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15810_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t483_m15811_ParameterInfos[] = 
{
	{"sender", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15811_GM;
MethodInfo m15811_MI = 
{
	"OnDeserialization", (methodPointerType)&m12897_gshared, &t483_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t483_m15811_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15811_GM};
extern Il2CppType t7_0_0_0;
static ParameterInfo t483_m15812_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15812_GM;
MethodInfo m15812_MI = 
{
	"Remove", (methodPointerType)&m12899_gshared, &t483_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t483_m15812_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15812_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t466_1_0_2;
extern Il2CppType t466_1_0_0;
static ParameterInfo t483_m2865_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t466_1_0_2},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t4362 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2865_GM;
MethodInfo m2865_MI = 
{
	"TryGetValue", (methodPointerType)&m12900_gshared, &t483_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t4362, t483_m2865_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m2865_GM};
extern Il2CppType t592_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2866_GM;
MethodInfo m2866_MI = 
{
	"get_Values", (methodPointerType)&m12902_gshared, &t483_TI, &t592_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m2866_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t483_m15813_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15813_GM;
MethodInfo m15813_MI = 
{
	"ToTKey", (methodPointerType)&m12904_gshared, &t483_TI, &t7_0_0_0, RuntimeInvoker_t29_t29, t483_m15813_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15813_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t483_m15814_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t466_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15814_GM;
MethodInfo m15814_MI = 
{
	"ToTValue", (methodPointerType)&m12906_gshared, &t483_TI, &t466_0_0_0, RuntimeInvoker_t29_t29, t483_m15814_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15814_GM};
extern Il2CppType t2893_0_0_0;
static ParameterInfo t483_m15815_ParameterInfos[] = 
{
	{"pair", 0, 134217728, &EmptyCustomAttributesCache, &t2893_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2893 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15815_GM;
MethodInfo m15815_MI = 
{
	"ContainsKeyValuePair", (methodPointerType)&m15815, &t483_TI, &t40_0_0_0, RuntimeInvoker_t40_t2893, t483_m15815_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15815_GM};
extern Il2CppType t2895_0_0_0;
extern void* RuntimeInvoker_t2895 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15816_GM;
MethodInfo m15816_MI = 
{
	"GetEnumerator", (methodPointerType)&m15816, &t483_TI, &t2895_0_0_0, RuntimeInvoker_t2895, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15816_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t466_0_0_0;
static ParameterInfo t483_m15817_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t466_0_0_0},
};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725_t29_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1254__CustomAttributeCache_m9975;
extern Il2CppGenericMethod m15817_GM;
MethodInfo m15817_MI = 
{
	"<CopyTo>m__0", (methodPointerType)&m12910_gshared, &t483_TI, &t725_0_0_0, RuntimeInvoker_t725_t29_t29, t483_m15817_ParameterInfos, &t1254__CustomAttributeCache_m9975, 145, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15817_GM};
static MethodInfo* t483_MIs[] =
{
	&m15779_MI,
	&m2863_MI,
	&m15780_MI,
	&m15781_MI,
	&m15782_MI,
	&m15783_MI,
	&m15784_MI,
	&m15785_MI,
	&m15786_MI,
	&m15787_MI,
	&m15788_MI,
	&m15789_MI,
	&m15790_MI,
	&m15791_MI,
	&m15792_MI,
	&m15793_MI,
	&m15794_MI,
	&m15795_MI,
	&m15796_MI,
	&m15797_MI,
	&m15798_MI,
	&m2864_MI,
	&m15799_MI,
	&m15800_MI,
	&m15801_MI,
	&m29237_MI,
	&m15802_MI,
	&m15803_MI,
	&m15804_MI,
	&m29239_MI,
	&m15805_MI,
	&m15806_MI,
	&m15807_MI,
	&m15808_MI,
	&m15809_MI,
	&m15810_MI,
	&m15811_MI,
	&m15812_MI,
	&m2865_MI,
	&m2866_MI,
	&m15813_MI,
	&m15814_MI,
	&m15815_MI,
	&m15816_MI,
	&m15817_MI,
	NULL
};
extern MethodInfo m15794_MI;
extern MethodInfo m15810_MI;
extern MethodInfo m15793_MI;
extern MethodInfo m15807_MI;
extern MethodInfo m15791_MI;
extern MethodInfo m15795_MI;
extern MethodInfo m15784_MI;
extern MethodInfo m15796_MI;
extern MethodInfo m15785_MI;
extern MethodInfo m15811_MI;
static MethodInfo* t483_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15794_MI,
	&m15810_MI,
	&m15797_MI,
	&m15786_MI,
	&m15787_MI,
	&m15793_MI,
	&m15797_MI,
	&m15788_MI,
	&m15789_MI,
	&m15807_MI,
	&m15790_MI,
	&m15791_MI,
	&m15792_MI,
	&m15795_MI,
	&m15812_MI,
	&m15782_MI,
	&m15783_MI,
	&m15784_MI,
	&m15796_MI,
	&m15785_MI,
	&m15811_MI,
	&m15798_MI,
	&m2864_MI,
	&m15806_MI,
	&m15808_MI,
	&m15810_MI,
	&m15811_MI,
	&m2865_MI,
};
extern TypeInfo t5586_TI;
extern TypeInfo t5588_TI;
extern TypeInfo t6726_TI;
extern TypeInfo t721_TI;
extern TypeInfo t918_TI;
static TypeInfo* t483_ITIs[] = 
{
	&t603_TI,
	&t374_TI,
	&t674_TI,
	&t5586_TI,
	&t5588_TI,
	&t6726_TI,
	&t721_TI,
	&t918_TI,
};
static Il2CppInterfaceOffsetPair t483_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t374_TI, 5},
	{ &t674_TI, 6},
	{ &t5586_TI, 10},
	{ &t5588_TI, 17},
	{ &t6726_TI, 18},
	{ &t721_TI, 19},
	{ &t918_TI, 24},
};
extern TypeInfo t7_TI;
extern TypeInfo t466_TI;
extern TypeInfo t2892_TI;
extern TypeInfo t483_TI;
extern TypeInfo t2891_TI;
extern TypeInfo t2899_TI;
extern TypeInfo t2895_TI;
extern TypeInfo t2900_TI;
extern TypeInfo t446_TI;
extern TypeInfo t482_TI;
extern TypeInfo t2893_TI;
extern TypeInfo t2892_TI;
extern TypeInfo t2096_TI;
extern TypeInfo t592_TI;
static Il2CppRGCTXData t483_RGCTXData[52] = 
{
	&m15799_MI/* Method Usage */,
	&t7_TI/* Class Usage */,
	&m15808_MI/* Method Usage */,
	&m15813_MI/* Method Usage */,
	&m15798_MI/* Method Usage */,
	&t466_TI/* Class Usage */,
	&m15814_MI/* Method Usage */,
	&m2864_MI/* Method Usage */,
	&m15806_MI/* Method Usage */,
	&m15812_MI/* Method Usage */,
	&m15819_MI/* Method Usage */,
	&m15821_MI/* Method Usage */,
	&m15815_MI/* Method Usage */,
	&m15804_MI/* Method Usage */,
	&t2892_TI/* Class Usage */,
	&m15801_MI/* Method Usage */,
	&t483_TI/* Static Usage */,
	&m15817_MI/* Method Usage */,
	&t2891_TI/* Class Usage */,
	&m15863_MI/* Method Usage */,
	&m22199_MI/* Method Usage */,
	&m15802_MI/* Method Usage */,
	&t2899_TI/* Class Usage */,
	&m15867_MI/* Method Usage */,
	&m22201_MI/* Method Usage */,
	&t2895_TI/* Class Usage */,
	&m15847_MI/* Method Usage */,
	&t2900_TI/* Class Usage */,
	&m15871_MI/* Method Usage */,
	&m29233_MI/* Method Usage */,
	&m29234_MI/* Method Usage */,
	&m15805_MI/* Method Usage */,
	&m15881_MI/* Method Usage */,
	&m15800_MI/* Method Usage */,
	&t446_TI/* Array Usage */,
	&t482_TI/* Array Usage */,
	&m15797_MI/* Method Usage */,
	&t2893_TI/* Class Usage */,
	&m15818_MI/* Method Usage */,
	&m22202_MI/* Method Usage */,
	&m15892_MI/* Method Usage */,
	&m29235_MI/* Method Usage */,
	&t2892_TI/* Array Usage */,
	&t2096_0_0_0/* Type Usage */,
	&t2096_TI/* Class Usage */,
	&t2892_0_0_0/* Type Usage */,
	&t592_TI/* Class Usage */,
	&m15829_MI/* Method Usage */,
	&t7_0_0_0/* Type Usage */,
	&t466_0_0_0/* Type Usage */,
	&m2865_MI/* Method Usage */,
	&m29236_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t483_0_0_0;
extern Il2CppType t483_1_0_0;
struct t483;
extern Il2CppGenericClass t483_GC;
extern CustomAttributesCache t1254__CustomAttributeCache;
extern CustomAttributesCache t1254__CustomAttributeCache_U3CU3Ef__am$cacheB;
extern CustomAttributesCache t1254__CustomAttributeCache_m9975;
TypeInfo t483_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Dictionary`2", "System.Collections.Generic", t483_MIs, t483_PIs, t483_FIs, NULL, &t29_TI, NULL, NULL, &t483_TI, t483_ITIs, t483_VT, &t1254__CustomAttributeCache, &t483_TI, &t483_0_0_0, &t483_1_0_0, t483_IOs, &t483_GC, NULL, t483_FDVs, NULL, t483_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t483), 0, -1, sizeof(t483_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 45, 7, 16, 0, 0, 32, 8, 8};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>>
extern MethodInfo m29241_MI;
static PropertyInfo t5586____Count_PropertyInfo = 
{
	&t5586_TI, "Count", &m29241_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29242_MI;
static PropertyInfo t5586____IsReadOnly_PropertyInfo = 
{
	&t5586_TI, "IsReadOnly", &m29242_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5586_PIs[] =
{
	&t5586____Count_PropertyInfo,
	&t5586____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29241_GM;
MethodInfo m29241_MI = 
{
	"get_Count", NULL, &t5586_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29241_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29242_GM;
MethodInfo m29242_MI = 
{
	"get_IsReadOnly", NULL, &t5586_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29242_GM};
extern Il2CppType t2893_0_0_0;
static ParameterInfo t5586_m29243_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2893_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t2893 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29243_GM;
MethodInfo m29243_MI = 
{
	"Add", NULL, &t5586_TI, &t21_0_0_0, RuntimeInvoker_t21_t2893, t5586_m29243_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29243_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29244_GM;
MethodInfo m29244_MI = 
{
	"Clear", NULL, &t5586_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29244_GM};
extern Il2CppType t2893_0_0_0;
static ParameterInfo t5586_m29245_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2893_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2893 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29245_GM;
MethodInfo m29245_MI = 
{
	"Contains", NULL, &t5586_TI, &t40_0_0_0, RuntimeInvoker_t40_t2893, t5586_m29245_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29245_GM};
extern Il2CppType t2892_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5586_m29246_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2892_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29246_GM;
MethodInfo m29246_MI = 
{
	"CopyTo", NULL, &t5586_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5586_m29246_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29246_GM};
extern Il2CppType t2893_0_0_0;
static ParameterInfo t5586_m29247_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2893_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2893 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29247_GM;
MethodInfo m29247_MI = 
{
	"Remove", NULL, &t5586_TI, &t40_0_0_0, RuntimeInvoker_t40_t2893, t5586_m29247_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29247_GM};
static MethodInfo* t5586_MIs[] =
{
	&m29241_MI,
	&m29242_MI,
	&m29243_MI,
	&m29244_MI,
	&m29245_MI,
	&m29246_MI,
	&m29247_MI,
	NULL
};
static TypeInfo* t5586_ITIs[] = 
{
	&t603_TI,
	&t5588_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5586_0_0_0;
extern Il2CppType t5586_1_0_0;
struct t5586;
extern Il2CppGenericClass t5586_GC;
TypeInfo t5586_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5586_MIs, t5586_PIs, NULL, NULL, NULL, NULL, NULL, &t5586_TI, t5586_ITIs, NULL, &EmptyCustomAttributesCache, &t5586_TI, &t5586_0_0_0, &t5586_1_0_0, NULL, &t5586_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>>
extern Il2CppType t2894_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29248_GM;
MethodInfo m29248_MI = 
{
	"GetEnumerator", NULL, &t5588_TI, &t2894_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29248_GM};
static MethodInfo* t5588_MIs[] =
{
	&m29248_MI,
	NULL
};
static TypeInfo* t5588_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5588_0_0_0;
extern Il2CppType t5588_1_0_0;
struct t5588;
extern Il2CppGenericClass t5588_GC;
TypeInfo t5588_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5588_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5588_TI, t5588_ITIs, NULL, &EmptyCustomAttributesCache, &t5588_TI, &t5588_0_0_0, &t5588_1_0_0, NULL, &t5588_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2894_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>>
extern MethodInfo m29249_MI;
static PropertyInfo t2894____Current_PropertyInfo = 
{
	&t2894_TI, "Current", &m29249_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2894_PIs[] =
{
	&t2894____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2893_0_0_0;
extern void* RuntimeInvoker_t2893 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29249_GM;
MethodInfo m29249_MI = 
{
	"get_Current", NULL, &t2894_TI, &t2893_0_0_0, RuntimeInvoker_t2893, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29249_GM};
static MethodInfo* t2894_MIs[] =
{
	&m29249_MI,
	NULL
};
static TypeInfo* t2894_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2894_0_0_0;
extern Il2CppType t2894_1_0_0;
struct t2894;
extern Il2CppGenericClass t2894_GC;
TypeInfo t2894_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2894_MIs, t2894_PIs, NULL, NULL, NULL, NULL, NULL, &t2894_TI, t2894_ITIs, NULL, &EmptyCustomAttributesCache, &t2894_TI, &t2894_0_0_0, &t2894_1_0_0, NULL, &t2894_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m15820_MI;
extern MethodInfo m15822_MI;
extern MethodInfo m4008_MI;


// Metadata Definition System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>
extern Il2CppType t7_0_0_1;
FieldInfo t2893_f0_FieldInfo = 
{
	"key", &t7_0_0_1, &t2893_TI, offsetof(t2893, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t466_0_0_1;
FieldInfo t2893_f1_FieldInfo = 
{
	"value", &t466_0_0_1, &t2893_TI, offsetof(t2893, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2893_FIs[] =
{
	&t2893_f0_FieldInfo,
	&t2893_f1_FieldInfo,
	NULL
};
static PropertyInfo t2893____Key_PropertyInfo = 
{
	&t2893_TI, "Key", &m15819_MI, &m15820_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2893____Value_PropertyInfo = 
{
	&t2893_TI, "Value", &m15821_MI, &m15822_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2893_PIs[] =
{
	&t2893____Key_PropertyInfo,
	&t2893____Value_PropertyInfo,
	NULL
};
extern Il2CppType t7_0_0_0;
extern Il2CppType t466_0_0_0;
static ParameterInfo t2893_m15818_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t466_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15818_GM;
MethodInfo m15818_MI = 
{
	".ctor", (methodPointerType)&m12917_gshared, &t2893_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2893_m15818_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15818_GM};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15819_GM;
MethodInfo m15819_MI = 
{
	"get_Key", (methodPointerType)&m12918_gshared, &t2893_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15819_GM};
extern Il2CppType t7_0_0_0;
static ParameterInfo t2893_m15820_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15820_GM;
MethodInfo m15820_MI = 
{
	"set_Key", (methodPointerType)&m12919_gshared, &t2893_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2893_m15820_ParameterInfos, &EmptyCustomAttributesCache, 2177, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15820_GM};
extern Il2CppType t466_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15821_GM;
MethodInfo m15821_MI = 
{
	"get_Value", (methodPointerType)&m12920_gshared, &t2893_TI, &t466_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15821_GM};
extern Il2CppType t466_0_0_0;
static ParameterInfo t2893_m15822_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t466_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15822_GM;
MethodInfo m15822_MI = 
{
	"set_Value", (methodPointerType)&m12921_gshared, &t2893_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2893_m15822_ParameterInfos, &EmptyCustomAttributesCache, 2177, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15822_GM};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15823_GM;
MethodInfo m15823_MI = 
{
	"ToString", (methodPointerType)&m12922_gshared, &t2893_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15823_GM};
static MethodInfo* t2893_MIs[] =
{
	&m15818_MI,
	&m15819_MI,
	&m15820_MI,
	&m15821_MI,
	&m15822_MI,
	&m15823_MI,
	NULL
};
extern MethodInfo m15823_MI;
static MethodInfo* t2893_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m15823_MI,
};
extern TypeInfo t7_TI;
extern TypeInfo t466_TI;
static Il2CppRGCTXData t2893_RGCTXData[6] = 
{
	&m15820_MI/* Method Usage */,
	&m15822_MI/* Method Usage */,
	&m15819_MI/* Method Usage */,
	&t7_TI/* Class Usage */,
	&m15821_MI/* Method Usage */,
	&t466_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2893_1_0_0;
extern Il2CppGenericClass t2893_GC;
extern CustomAttributesCache t1259__CustomAttributeCache;
TypeInfo t2893_TI = 
{
	&g_mscorlib_dll_Image, NULL, "KeyValuePair`2", "System.Collections.Generic", t2893_MIs, t2893_PIs, t2893_FIs, NULL, &t110_TI, NULL, NULL, &t2893_TI, NULL, t2893_VT, &t1259__CustomAttributeCache, &t2893_TI, &t2893_0_0_0, &t2893_1_0_0, NULL, &t2893_GC, NULL, NULL, NULL, t2893_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2893)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057033, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 2, 0, 0, 4, 0, 0};
#include "t2896.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2896_TI;
#include "t2896MD.h"

extern MethodInfo m15828_MI;
extern MethodInfo m22186_MI;
struct t20;
 t2893  m22186 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m15824_MI;
 void m15824 (t2896 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m15825_MI;
 t29 * m15825 (t2896 * __this, MethodInfo* method){
	{
		t2893  L_0 = m15828(__this, &m15828_MI);
		t2893  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2893_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m15826_MI;
 void m15826 (t2896 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m15827_MI;
 bool m15827 (t2896 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 t2893  m15828 (t2896 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		t2893  L_8 = m22186(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m22186_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>>
extern Il2CppType t20_0_0_1;
FieldInfo t2896_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2896_TI, offsetof(t2896, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2896_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2896_TI, offsetof(t2896, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2896_FIs[] =
{
	&t2896_f0_FieldInfo,
	&t2896_f1_FieldInfo,
	NULL
};
static PropertyInfo t2896____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2896_TI, "System.Collections.IEnumerator.Current", &m15825_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2896____Current_PropertyInfo = 
{
	&t2896_TI, "Current", &m15828_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2896_PIs[] =
{
	&t2896____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2896____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2896_m15824_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15824_GM;
MethodInfo m15824_MI = 
{
	".ctor", (methodPointerType)&m15824, &t2896_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2896_m15824_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15824_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15825_GM;
MethodInfo m15825_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m15825, &t2896_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15825_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15826_GM;
MethodInfo m15826_MI = 
{
	"Dispose", (methodPointerType)&m15826, &t2896_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15826_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15827_GM;
MethodInfo m15827_MI = 
{
	"MoveNext", (methodPointerType)&m15827, &t2896_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15827_GM};
extern Il2CppType t2893_0_0_0;
extern void* RuntimeInvoker_t2893 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15828_GM;
MethodInfo m15828_MI = 
{
	"get_Current", (methodPointerType)&m15828, &t2896_TI, &t2893_0_0_0, RuntimeInvoker_t2893, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15828_GM};
static MethodInfo* t2896_MIs[] =
{
	&m15824_MI,
	&m15825_MI,
	&m15826_MI,
	&m15827_MI,
	&m15828_MI,
	NULL
};
static MethodInfo* t2896_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15825_MI,
	&m15827_MI,
	&m15826_MI,
	&m15828_MI,
};
static TypeInfo* t2896_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2894_TI,
};
static Il2CppInterfaceOffsetPair t2896_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2894_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2896_0_0_0;
extern Il2CppType t2896_1_0_0;
extern Il2CppGenericClass t2896_GC;
TypeInfo t2896_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2896_MIs, t2896_PIs, t2896_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2896_TI, t2896_ITIs, t2896_VT, &EmptyCustomAttributesCache, &t2896_TI, &t2896_0_0_0, &t2896_1_0_0, t2896_IOs, &t2896_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2896)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5587_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>>
extern MethodInfo m29250_MI;
extern MethodInfo m29251_MI;
static PropertyInfo t5587____Item_PropertyInfo = 
{
	&t5587_TI, "Item", &m29250_MI, &m29251_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5587_PIs[] =
{
	&t5587____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2893_0_0_0;
static ParameterInfo t5587_m29252_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2893_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t2893 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29252_GM;
MethodInfo m29252_MI = 
{
	"IndexOf", NULL, &t5587_TI, &t44_0_0_0, RuntimeInvoker_t44_t2893, t5587_m29252_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29252_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2893_0_0_0;
static ParameterInfo t5587_m29253_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t2893_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t2893 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29253_GM;
MethodInfo m29253_MI = 
{
	"Insert", NULL, &t5587_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t2893, t5587_m29253_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29253_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5587_m29254_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29254_GM;
MethodInfo m29254_MI = 
{
	"RemoveAt", NULL, &t5587_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5587_m29254_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29254_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5587_m29250_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2893_0_0_0;
extern void* RuntimeInvoker_t2893_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29250_GM;
MethodInfo m29250_MI = 
{
	"get_Item", NULL, &t5587_TI, &t2893_0_0_0, RuntimeInvoker_t2893_t44, t5587_m29250_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29250_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2893_0_0_0;
static ParameterInfo t5587_m29251_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2893_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t2893 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29251_GM;
MethodInfo m29251_MI = 
{
	"set_Item", NULL, &t5587_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t2893, t5587_m29251_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29251_GM};
static MethodInfo* t5587_MIs[] =
{
	&m29252_MI,
	&m29253_MI,
	&m29254_MI,
	&m29250_MI,
	&m29251_MI,
	NULL
};
static TypeInfo* t5587_ITIs[] = 
{
	&t603_TI,
	&t5586_TI,
	&t5588_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5587_0_0_0;
extern Il2CppType t5587_1_0_0;
struct t5587;
extern Il2CppGenericClass t5587_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5587_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5587_MIs, t5587_PIs, NULL, NULL, NULL, NULL, NULL, &t5587_TI, t5587_ITIs, NULL, &t1908__CustomAttributeCache, &t5587_TI, &t5587_0_0_0, &t5587_1_0_0, NULL, &t5587_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IDictionary`2<System.String,UnityEngine.GUIStyle>
extern Il2CppType t7_0_0_0;
static ParameterInfo t6726_m29255_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29255_GM;
MethodInfo m29255_MI = 
{
	"Remove", NULL, &t6726_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6726_m29255_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29255_GM};
static MethodInfo* t6726_MIs[] =
{
	&m29255_MI,
	NULL
};
static TypeInfo* t6726_ITIs[] = 
{
	&t603_TI,
	&t5586_TI,
	&t5588_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6726_0_0_0;
extern Il2CppType t6726_1_0_0;
struct t6726;
extern Il2CppGenericClass t6726_GC;
extern CustomAttributesCache t1975__CustomAttributeCache;
TypeInfo t6726_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IDictionary`2", "System.Collections.Generic", t6726_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6726_TI, t6726_ITIs, NULL, &t1975__CustomAttributeCache, &t6726_TI, &t6726_0_0_0, &t6726_1_0_0, NULL, &t6726_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<System.String>
extern Il2CppType t7_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t2096_m29234_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29234_GM;
MethodInfo m29234_MI = 
{
	"Equals", NULL, &t2096_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2096_m29234_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29234_GM};
extern Il2CppType t7_0_0_0;
static ParameterInfo t2096_m29233_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29233_GM;
MethodInfo m29233_MI = 
{
	"GetHashCode", NULL, &t2096_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2096_m29233_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29233_GM};
static MethodInfo* t2096_MIs[] =
{
	&m29234_MI,
	&m29233_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2096_1_0_0;
struct t2096;
extern Il2CppGenericClass t2096_GC;
TypeInfo t2096_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t2096_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2096_TI, NULL, NULL, &EmptyCustomAttributesCache, &t2096_TI, &t2096_0_0_0, &t2096_1_0_0, NULL, &t2096_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t593.h"
#include "t2898.h"
extern TypeInfo t593_TI;
extern TypeInfo t2898_TI;
#include "t2898MD.h"
#include "t593MD.h"
extern MethodInfo m3988_MI;
extern MethodInfo m15809_MI;
extern MethodInfo m2867_MI;
extern MethodInfo m15840_MI;
extern MethodInfo m15803_MI;
extern MethodInfo m15859_MI;
extern MethodInfo m22197_MI;
extern MethodInfo m22198_MI;
extern MethodInfo m15842_MI;
struct t483;
struct t2461;
#include "t2461.h"
#include "t2470.h"
 void m20791_gshared (t2461 * __this, t20 * p0, int32_t p1, t2470 * p2, MethodInfo* method);
#define m20791(__this, p0, p1, p2, method) (void)m20791_gshared((t2461 *)__this, (t20 *)p0, (int32_t)p1, (t2470 *)p2, method)
#define m22197(__this, p0, p1, p2, method) (void)m20791_gshared((t2461 *)__this, (t20 *)p0, (int32_t)p1, (t2470 *)p2, method)
struct t483;
 void m22198 (t483 * __this, t482* p0, int32_t p1, t2898 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;


 t593  m2867 (t592 * __this, MethodInfo* method){
	{
		t483 * L_0 = (__this->f0);
		t593  L_1 = {0};
		m15842(&L_1, L_0, &m15842_MI);
		return L_1;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/ValueCollection<System.String,UnityEngine.GUIStyle>
extern Il2CppType t483_0_0_1;
FieldInfo t592_f0_FieldInfo = 
{
	"dictionary", &t483_0_0_1, &t592_TI, offsetof(t592, f0), &EmptyCustomAttributesCache};
static FieldInfo* t592_FIs[] =
{
	&t592_f0_FieldInfo,
	NULL
};
extern MethodInfo m15837_MI;
static PropertyInfo t592____System_Collections_Generic_ICollectionU3CTValueU3E_IsReadOnly_PropertyInfo = 
{
	&t592_TI, "System.Collections.Generic.ICollection<TValue>.IsReadOnly", &m15837_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15838_MI;
static PropertyInfo t592____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t592_TI, "System.Collections.ICollection.IsSynchronized", &m15838_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15839_MI;
static PropertyInfo t592____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t592_TI, "System.Collections.ICollection.SyncRoot", &m15839_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15841_MI;
static PropertyInfo t592____Count_PropertyInfo = 
{
	&t592_TI, "Count", &m15841_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t592_PIs[] =
{
	&t592____System_Collections_Generic_ICollectionU3CTValueU3E_IsReadOnly_PropertyInfo,
	&t592____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t592____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t592____Count_PropertyInfo,
	NULL
};
extern Il2CppType t483_0_0_0;
static ParameterInfo t592_m15829_ParameterInfos[] = 
{
	{"dictionary", 0, 134217728, &EmptyCustomAttributesCache, &t483_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15829_GM;
MethodInfo m15829_MI = 
{
	".ctor", (methodPointerType)&m12928_gshared, &t592_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t592_m15829_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15829_GM};
extern Il2CppType t466_0_0_0;
static ParameterInfo t592_m15830_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t466_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15830_GM;
MethodInfo m15830_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Add", (methodPointerType)&m12929_gshared, &t592_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t592_m15830_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15830_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15831_GM;
MethodInfo m15831_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Clear", (methodPointerType)&m12930_gshared, &t592_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 12, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15831_GM};
extern Il2CppType t466_0_0_0;
static ParameterInfo t592_m15832_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t466_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15832_GM;
MethodInfo m15832_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Contains", (methodPointerType)&m12931_gshared, &t592_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t592_m15832_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15832_GM};
extern Il2CppType t466_0_0_0;
static ParameterInfo t592_m15833_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t466_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15833_GM;
MethodInfo m15833_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Remove", (methodPointerType)&m12932_gshared, &t592_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t592_m15833_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15833_GM};
extern Il2CppType t2897_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15834_GM;
MethodInfo m15834_MI = 
{
	"System.Collections.Generic.IEnumerable<TValue>.GetEnumerator", (methodPointerType)&m12933_gshared, &t592_TI, &t2897_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 16, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15834_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t592_m15835_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15835_GM;
MethodInfo m15835_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m12934_gshared, &t592_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t592_m15835_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15835_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15836_GM;
MethodInfo m15836_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m12935_gshared, &t592_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15836_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15837_GM;
MethodInfo m15837_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.get_IsReadOnly", (methodPointerType)&m12936_gshared, &t592_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15837_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15838_GM;
MethodInfo m15838_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m12937_gshared, &t592_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15838_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15839_GM;
MethodInfo m15839_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m12938_gshared, &t592_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15839_GM};
extern Il2CppType t482_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t592_m15840_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t482_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15840_GM;
MethodInfo m15840_MI = 
{
	"CopyTo", (methodPointerType)&m12939_gshared, &t592_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t592_m15840_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 14, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15840_GM};
extern Il2CppType t593_0_0_0;
extern void* RuntimeInvoker_t593 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2867_GM;
MethodInfo m2867_MI = 
{
	"GetEnumerator", (methodPointerType)&m2867, &t592_TI, &t593_0_0_0, RuntimeInvoker_t593, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m2867_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15841_GM;
MethodInfo m15841_MI = 
{
	"get_Count", (methodPointerType)&m12941_gshared, &t592_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15841_GM};
static MethodInfo* t592_MIs[] =
{
	&m15829_MI,
	&m15830_MI,
	&m15831_MI,
	&m15832_MI,
	&m15833_MI,
	&m15834_MI,
	&m15835_MI,
	&m15836_MI,
	&m15837_MI,
	&m15838_MI,
	&m15839_MI,
	&m15840_MI,
	&m2867_MI,
	&m15841_MI,
	NULL
};
extern MethodInfo m15836_MI;
extern MethodInfo m15835_MI;
extern MethodInfo m15830_MI;
extern MethodInfo m15831_MI;
extern MethodInfo m15832_MI;
extern MethodInfo m15833_MI;
extern MethodInfo m15834_MI;
static MethodInfo* t592_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15836_MI,
	&m15841_MI,
	&m15838_MI,
	&m15839_MI,
	&m15835_MI,
	&m15841_MI,
	&m15837_MI,
	&m15830_MI,
	&m15831_MI,
	&m15832_MI,
	&m15840_MI,
	&m15833_MI,
	&m15834_MI,
};
static TypeInfo* t592_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t5583_TI,
	&t5585_TI,
};
static Il2CppInterfaceOffsetPair t592_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t5583_TI, 9},
	{ &t5585_TI, 16},
};
extern TypeInfo t593_TI;
extern TypeInfo t482_TI;
extern TypeInfo t2898_TI;
static Il2CppRGCTXData t592_RGCTXData[13] = 
{
	&m15809_MI/* Method Usage */,
	&m2867_MI/* Method Usage */,
	&t593_TI/* Class Usage */,
	&t482_TI/* Class Usage */,
	&m15840_MI/* Method Usage */,
	&m15801_MI/* Method Usage */,
	&m15803_MI/* Method Usage */,
	&t2898_TI/* Class Usage */,
	&m15859_MI/* Method Usage */,
	&m22197_MI/* Method Usage */,
	&m22198_MI/* Method Usage */,
	&m15842_MI/* Method Usage */,
	&m15797_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t592_0_0_0;
extern Il2CppType t592_1_0_0;
struct t592;
extern Il2CppGenericClass t592_GC;
extern CustomAttributesCache t1252__CustomAttributeCache;
TypeInfo t592_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ValueCollection", "", t592_MIs, t592_PIs, t592_FIs, NULL, &t29_TI, NULL, &t1254_TI, &t592_TI, t592_ITIs, t592_VT, &t1252__CustomAttributeCache, &t592_TI, &t592_0_0_0, &t592_1_0_0, t592_IOs, &t592_GC, NULL, NULL, NULL, t592_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t592), 0, -1, 0, 0, -1, 1057026, 0, false, false, false, false, true, false, false, false, false, false, false, false, 14, 4, 1, 0, 0, 17, 4, 4};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m15855_MI;
extern MethodInfo m15858_MI;
extern MethodInfo m15852_MI;


// Metadata Definition System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>
extern Il2CppType t2895_0_0_1;
FieldInfo t593_f0_FieldInfo = 
{
	"host_enumerator", &t2895_0_0_1, &t593_TI, offsetof(t593, f0) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t593_FIs[] =
{
	&t593_f0_FieldInfo,
	NULL
};
extern MethodInfo m15843_MI;
static PropertyInfo t593____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t593_TI, "System.Collections.IEnumerator.Current", &m15843_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15846_MI;
static PropertyInfo t593____Current_PropertyInfo = 
{
	&t593_TI, "Current", &m15846_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t593_PIs[] =
{
	&t593____System_Collections_IEnumerator_Current_PropertyInfo,
	&t593____Current_PropertyInfo,
	NULL
};
extern Il2CppType t483_0_0_0;
static ParameterInfo t593_m15842_ParameterInfos[] = 
{
	{"host", 0, 134217728, &EmptyCustomAttributesCache, &t483_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15842_GM;
MethodInfo m15842_MI = 
{
	".ctor", (methodPointerType)&m12942_gshared, &t593_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t593_m15842_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15842_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15843_GM;
MethodInfo m15843_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m12943_gshared, &t593_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15843_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15844_GM;
MethodInfo m15844_MI = 
{
	"Dispose", (methodPointerType)&m12944_gshared, &t593_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15844_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15845_GM;
MethodInfo m15845_MI = 
{
	"MoveNext", (methodPointerType)&m12945_gshared, &t593_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15845_GM};
extern Il2CppType t466_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15846_GM;
MethodInfo m15846_MI = 
{
	"get_Current", (methodPointerType)&m12946_gshared, &t593_TI, &t466_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15846_GM};
static MethodInfo* t593_MIs[] =
{
	&m15842_MI,
	&m15843_MI,
	&m15844_MI,
	&m15845_MI,
	&m15846_MI,
	NULL
};
extern MethodInfo m15845_MI;
extern MethodInfo m15844_MI;
static MethodInfo* t593_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15843_MI,
	&m15845_MI,
	&m15844_MI,
	&m15846_MI,
};
static TypeInfo* t593_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2897_TI,
};
static Il2CppInterfaceOffsetPair t593_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2897_TI, 7},
};
extern TypeInfo t466_TI;
static Il2CppRGCTXData t593_RGCTXData[6] = 
{
	&m15816_MI/* Method Usage */,
	&m15855_MI/* Method Usage */,
	&t466_TI/* Class Usage */,
	&m15858_MI/* Method Usage */,
	&m15852_MI/* Method Usage */,
	&m15821_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t593_0_0_0;
extern Il2CppType t593_1_0_0;
extern Il2CppGenericClass t593_GC;
extern TypeInfo t1252_TI;
TypeInfo t593_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t593_MIs, t593_PIs, t593_FIs, NULL, &t110_TI, NULL, &t1252_TI, &t593_TI, t593_ITIs, t593_VT, &EmptyCustomAttributesCache, &t593_TI, &t593_0_0_0, &t593_1_0_0, t593_IOs, &t593_GC, NULL, NULL, NULL, t593_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t593)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 1, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m15857_MI;
extern MethodInfo m15854_MI;
extern MethodInfo m15856_MI;


extern MethodInfo m15853_MI;
 t2893  m15853 (t2895 * __this, MethodInfo* method){
	{
		t2893  L_0 = (__this->f3);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>
extern Il2CppType t483_0_0_1;
FieldInfo t2895_f0_FieldInfo = 
{
	"dictionary", &t483_0_0_1, &t2895_TI, offsetof(t2895, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2895_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t2895_TI, offsetof(t2895, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2895_f2_FieldInfo = 
{
	"stamp", &t44_0_0_1, &t2895_TI, offsetof(t2895, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t2893_0_0_3;
FieldInfo t2895_f3_FieldInfo = 
{
	"current", &t2893_0_0_3, &t2895_TI, offsetof(t2895, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2895_FIs[] =
{
	&t2895_f0_FieldInfo,
	&t2895_f1_FieldInfo,
	&t2895_f2_FieldInfo,
	&t2895_f3_FieldInfo,
	NULL
};
extern MethodInfo m15848_MI;
static PropertyInfo t2895____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2895_TI, "System.Collections.IEnumerator.Current", &m15848_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15849_MI;
static PropertyInfo t2895____System_Collections_IDictionaryEnumerator_Entry_PropertyInfo = 
{
	&t2895_TI, "System.Collections.IDictionaryEnumerator.Entry", &m15849_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15850_MI;
static PropertyInfo t2895____System_Collections_IDictionaryEnumerator_Key_PropertyInfo = 
{
	&t2895_TI, "System.Collections.IDictionaryEnumerator.Key", &m15850_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15851_MI;
static PropertyInfo t2895____System_Collections_IDictionaryEnumerator_Value_PropertyInfo = 
{
	&t2895_TI, "System.Collections.IDictionaryEnumerator.Value", &m15851_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2895____Current_PropertyInfo = 
{
	&t2895_TI, "Current", &m15853_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2895____CurrentKey_PropertyInfo = 
{
	&t2895_TI, "CurrentKey", &m15854_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2895____CurrentValue_PropertyInfo = 
{
	&t2895_TI, "CurrentValue", &m15855_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2895_PIs[] =
{
	&t2895____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2895____System_Collections_IDictionaryEnumerator_Entry_PropertyInfo,
	&t2895____System_Collections_IDictionaryEnumerator_Key_PropertyInfo,
	&t2895____System_Collections_IDictionaryEnumerator_Value_PropertyInfo,
	&t2895____Current_PropertyInfo,
	&t2895____CurrentKey_PropertyInfo,
	&t2895____CurrentValue_PropertyInfo,
	NULL
};
extern Il2CppType t483_0_0_0;
static ParameterInfo t2895_m15847_ParameterInfos[] = 
{
	{"dictionary", 0, 134217728, &EmptyCustomAttributesCache, &t483_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15847_GM;
MethodInfo m15847_MI = 
{
	".ctor", (methodPointerType)&m12947_gshared, &t2895_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2895_m15847_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15847_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15848_GM;
MethodInfo m15848_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m12948_gshared, &t2895_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15848_GM};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15849_GM;
MethodInfo m15849_MI = 
{
	"System.Collections.IDictionaryEnumerator.get_Entry", (methodPointerType)&m12949_gshared, &t2895_TI, &t725_0_0_0, RuntimeInvoker_t725, NULL, &EmptyCustomAttributesCache, 2529, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15849_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15850_GM;
MethodInfo m15850_MI = 
{
	"System.Collections.IDictionaryEnumerator.get_Key", (methodPointerType)&m12950_gshared, &t2895_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15850_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15851_GM;
MethodInfo m15851_MI = 
{
	"System.Collections.IDictionaryEnumerator.get_Value", (methodPointerType)&m12951_gshared, &t2895_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15851_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15852_GM;
MethodInfo m15852_MI = 
{
	"MoveNext", (methodPointerType)&m12952_gshared, &t2895_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15852_GM};
extern Il2CppType t2893_0_0_0;
extern void* RuntimeInvoker_t2893 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15853_GM;
MethodInfo m15853_MI = 
{
	"get_Current", (methodPointerType)&m15853, &t2895_TI, &t2893_0_0_0, RuntimeInvoker_t2893, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15853_GM};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15854_GM;
MethodInfo m15854_MI = 
{
	"get_CurrentKey", (methodPointerType)&m12954_gshared, &t2895_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2179, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15854_GM};
extern Il2CppType t466_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15855_GM;
MethodInfo m15855_MI = 
{
	"get_CurrentValue", (methodPointerType)&m12955_gshared, &t2895_TI, &t466_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2179, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15855_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15856_GM;
MethodInfo m15856_MI = 
{
	"VerifyState", (methodPointerType)&m12956_gshared, &t2895_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15856_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15857_GM;
MethodInfo m15857_MI = 
{
	"VerifyCurrent", (methodPointerType)&m12957_gshared, &t2895_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15857_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15858_GM;
MethodInfo m15858_MI = 
{
	"Dispose", (methodPointerType)&m12958_gshared, &t2895_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15858_GM};
static MethodInfo* t2895_MIs[] =
{
	&m15847_MI,
	&m15848_MI,
	&m15849_MI,
	&m15850_MI,
	&m15851_MI,
	&m15852_MI,
	&m15853_MI,
	&m15854_MI,
	&m15855_MI,
	&m15856_MI,
	&m15857_MI,
	&m15858_MI,
	NULL
};
static MethodInfo* t2895_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15848_MI,
	&m15852_MI,
	&m15858_MI,
	&m15853_MI,
	&m15849_MI,
	&m15850_MI,
	&m15851_MI,
};
static TypeInfo* t2895_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2894_TI,
	&t722_TI,
};
static Il2CppInterfaceOffsetPair t2895_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2894_TI, 7},
	{ &t722_TI, 8},
};
extern TypeInfo t2893_TI;
extern TypeInfo t7_TI;
extern TypeInfo t466_TI;
static Il2CppRGCTXData t2895_RGCTXData[10] = 
{
	&m15857_MI/* Method Usage */,
	&t2893_TI/* Class Usage */,
	&m15819_MI/* Method Usage */,
	&t7_TI/* Class Usage */,
	&m15821_MI/* Method Usage */,
	&t466_TI/* Class Usage */,
	&m15854_MI/* Method Usage */,
	&m15855_MI/* Method Usage */,
	&m15856_MI/* Method Usage */,
	&m15818_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2895_0_0_0;
extern Il2CppType t2895_1_0_0;
extern Il2CppGenericClass t2895_GC;
TypeInfo t2895_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t2895_MIs, t2895_PIs, t2895_FIs, NULL, &t110_TI, NULL, &t1254_TI, &t2895_TI, t2895_ITIs, t2895_VT, &EmptyCustomAttributesCache, &t2895_TI, &t2895_0_0_0, &t2895_1_0_0, t2895_IOs, &t2895_GC, NULL, NULL, NULL, t2895_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2895)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 12, 7, 4, 0, 0, 11, 4, 4};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.Dictionary`2/Transform`1<System.String,UnityEngine.GUIStyle,UnityEngine.GUIStyle>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2898_m15859_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15859_GM;
MethodInfo m15859_MI = 
{
	".ctor", (methodPointerType)&m12959_gshared, &t2898_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2898_m15859_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15859_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t466_0_0_0;
static ParameterInfo t2898_m15860_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t466_0_0_0},
};
extern Il2CppType t466_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15860_GM;
MethodInfo m15860_MI = 
{
	"Invoke", (methodPointerType)&m12960_gshared, &t2898_TI, &t466_0_0_0, RuntimeInvoker_t29_t29_t29, t2898_m15860_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15860_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t466_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2898_m15861_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t466_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15861_GM;
MethodInfo m15861_MI = 
{
	"BeginInvoke", (methodPointerType)&m12961_gshared, &t2898_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t2898_m15861_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m15861_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2898_m15862_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t466_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15862_GM;
MethodInfo m15862_MI = 
{
	"EndInvoke", (methodPointerType)&m12962_gshared, &t2898_TI, &t466_0_0_0, RuntimeInvoker_t29_t29, t2898_m15862_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15862_GM};
static MethodInfo* t2898_MIs[] =
{
	&m15859_MI,
	&m15860_MI,
	&m15861_MI,
	&m15862_MI,
	NULL
};
extern MethodInfo m15860_MI;
extern MethodInfo m15861_MI;
extern MethodInfo m15862_MI;
static MethodInfo* t2898_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m15860_MI,
	&m15861_MI,
	&m15862_MI,
};
static Il2CppInterfaceOffsetPair t2898_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2898_0_0_0;
extern Il2CppType t2898_1_0_0;
struct t2898;
extern Il2CppGenericClass t2898_GC;
TypeInfo t2898_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Transform`1", "", t2898_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1254_TI, &t2898_TI, NULL, t2898_VT, &EmptyCustomAttributesCache, &t2898_TI, &t2898_0_0_0, &t2898_1_0_0, t2898_IOs, &t2898_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2898), 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif



 void m15863 (t2891 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m15864_MI;
 t725  m15864 (t2891 * __this, t7* p0, t466 * p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m15864((t2891 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef t725  (*FunctionPointerType) (t29 *, t29 * __this, t7* p0, t466 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef t725  (*FunctionPointerType) (t29 * __this, t7* p0, t466 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef t725  (*FunctionPointerType) (t29 * __this, t466 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m15865_MI;
 t29 * m15865 (t2891 * __this, t7* p0, t466 * p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = p1;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m15866_MI;
 t725  m15866 (t2891 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(t725 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Transform`1<System.String,UnityEngine.GUIStyle,System.Collections.DictionaryEntry>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2891_m15863_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15863_GM;
MethodInfo m15863_MI = 
{
	".ctor", (methodPointerType)&m15863, &t2891_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2891_m15863_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15863_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t466_0_0_0;
static ParameterInfo t2891_m15864_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t466_0_0_0},
};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15864_GM;
MethodInfo m15864_MI = 
{
	"Invoke", (methodPointerType)&m15864, &t2891_TI, &t725_0_0_0, RuntimeInvoker_t725_t29_t29, t2891_m15864_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15864_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t466_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2891_m15865_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t466_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15865_GM;
MethodInfo m15865_MI = 
{
	"BeginInvoke", (methodPointerType)&m15865, &t2891_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t2891_m15865_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m15865_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2891_m15866_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15866_GM;
MethodInfo m15866_MI = 
{
	"EndInvoke", (methodPointerType)&m15866, &t2891_TI, &t725_0_0_0, RuntimeInvoker_t725_t29, t2891_m15866_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15866_GM};
static MethodInfo* t2891_MIs[] =
{
	&m15863_MI,
	&m15864_MI,
	&m15865_MI,
	&m15866_MI,
	NULL
};
static MethodInfo* t2891_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m15864_MI,
	&m15865_MI,
	&m15866_MI,
};
static Il2CppInterfaceOffsetPair t2891_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2891_0_0_0;
extern Il2CppType t2891_1_0_0;
struct t2891;
extern Il2CppGenericClass t2891_GC;
TypeInfo t2891_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Transform`1", "", t2891_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1254_TI, &t2891_TI, NULL, t2891_VT, &EmptyCustomAttributesCache, &t2891_TI, &t2891_0_0_0, &t2891_1_0_0, t2891_IOs, &t2891_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2891), 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif



 void m15867 (t2899 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m15868_MI;
 t2893  m15868 (t2899 * __this, t7* p0, t466 * p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m15868((t2899 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef t2893  (*FunctionPointerType) (t29 *, t29 * __this, t7* p0, t466 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef t2893  (*FunctionPointerType) (t29 * __this, t7* p0, t466 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef t2893  (*FunctionPointerType) (t29 * __this, t466 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m15869_MI;
 t29 * m15869 (t2899 * __this, t7* p0, t466 * p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = p1;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m15870_MI;
 t2893  m15870 (t2899 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(t2893 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Transform`1<System.String,UnityEngine.GUIStyle,System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2899_m15867_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15867_GM;
MethodInfo m15867_MI = 
{
	".ctor", (methodPointerType)&m15867, &t2899_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2899_m15867_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15867_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t466_0_0_0;
static ParameterInfo t2899_m15868_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t466_0_0_0},
};
extern Il2CppType t2893_0_0_0;
extern void* RuntimeInvoker_t2893_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15868_GM;
MethodInfo m15868_MI = 
{
	"Invoke", (methodPointerType)&m15868, &t2899_TI, &t2893_0_0_0, RuntimeInvoker_t2893_t29_t29, t2899_m15868_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15868_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t466_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2899_m15869_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t466_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15869_GM;
MethodInfo m15869_MI = 
{
	"BeginInvoke", (methodPointerType)&m15869, &t2899_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t2899_m15869_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m15869_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2899_m15870_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t2893_0_0_0;
extern void* RuntimeInvoker_t2893_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15870_GM;
MethodInfo m15870_MI = 
{
	"EndInvoke", (methodPointerType)&m15870, &t2899_TI, &t2893_0_0_0, RuntimeInvoker_t2893_t29, t2899_m15870_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15870_GM};
static MethodInfo* t2899_MIs[] =
{
	&m15867_MI,
	&m15868_MI,
	&m15869_MI,
	&m15870_MI,
	NULL
};
static MethodInfo* t2899_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m15868_MI,
	&m15869_MI,
	&m15870_MI,
};
static Il2CppInterfaceOffsetPair t2899_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2899_0_0_0;
extern Il2CppType t2899_1_0_0;
struct t2899;
extern Il2CppGenericClass t2899_GC;
TypeInfo t2899_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Transform`1", "", t2899_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1254_TI, &t2899_TI, NULL, t2899_VT, &EmptyCustomAttributesCache, &t2899_TI, &t2899_0_0_0, &t2899_1_0_0, t2899_IOs, &t2899_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2899), 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m15873_MI;


// Metadata Definition System.Collections.Generic.Dictionary`2/ShimEnumerator<System.String,UnityEngine.GUIStyle>
extern Il2CppType t2895_0_0_1;
FieldInfo t2900_f0_FieldInfo = 
{
	"host_enumerator", &t2895_0_0_1, &t2900_TI, offsetof(t2900, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2900_FIs[] =
{
	&t2900_f0_FieldInfo,
	NULL
};
static PropertyInfo t2900____Entry_PropertyInfo = 
{
	&t2900_TI, "Entry", &m15873_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15874_MI;
static PropertyInfo t2900____Key_PropertyInfo = 
{
	&t2900_TI, "Key", &m15874_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15875_MI;
static PropertyInfo t2900____Value_PropertyInfo = 
{
	&t2900_TI, "Value", &m15875_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15876_MI;
static PropertyInfo t2900____Current_PropertyInfo = 
{
	&t2900_TI, "Current", &m15876_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2900_PIs[] =
{
	&t2900____Entry_PropertyInfo,
	&t2900____Key_PropertyInfo,
	&t2900____Value_PropertyInfo,
	&t2900____Current_PropertyInfo,
	NULL
};
extern Il2CppType t483_0_0_0;
static ParameterInfo t2900_m15871_ParameterInfos[] = 
{
	{"host", 0, 134217728, &EmptyCustomAttributesCache, &t483_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15871_GM;
MethodInfo m15871_MI = 
{
	".ctor", (methodPointerType)&m12971_gshared, &t2900_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2900_m15871_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15871_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15872_GM;
MethodInfo m15872_MI = 
{
	"MoveNext", (methodPointerType)&m12972_gshared, &t2900_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15872_GM};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15873_GM;
MethodInfo m15873_MI = 
{
	"get_Entry", (methodPointerType)&m12973_gshared, &t2900_TI, &t725_0_0_0, RuntimeInvoker_t725, NULL, &EmptyCustomAttributesCache, 2534, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15873_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15874_GM;
MethodInfo m15874_MI = 
{
	"get_Key", (methodPointerType)&m12974_gshared, &t2900_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15874_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15875_GM;
MethodInfo m15875_MI = 
{
	"get_Value", (methodPointerType)&m12975_gshared, &t2900_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15875_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15876_GM;
MethodInfo m15876_MI = 
{
	"get_Current", (methodPointerType)&m12976_gshared, &t2900_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15876_GM};
static MethodInfo* t2900_MIs[] =
{
	&m15871_MI,
	&m15872_MI,
	&m15873_MI,
	&m15874_MI,
	&m15875_MI,
	&m15876_MI,
	NULL
};
extern MethodInfo m15872_MI;
static MethodInfo* t2900_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15876_MI,
	&m15872_MI,
	&m15873_MI,
	&m15874_MI,
	&m15875_MI,
};
static TypeInfo* t2900_ITIs[] = 
{
	&t136_TI,
	&t722_TI,
};
static Il2CppInterfaceOffsetPair t2900_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t722_TI, 6},
};
extern TypeInfo t2895_TI;
extern TypeInfo t7_TI;
extern TypeInfo t466_TI;
static Il2CppRGCTXData t2900_RGCTXData[9] = 
{
	&m15816_MI/* Method Usage */,
	&m15852_MI/* Method Usage */,
	&t2895_TI/* Class Usage */,
	&m15853_MI/* Method Usage */,
	&m15819_MI/* Method Usage */,
	&t7_TI/* Class Usage */,
	&m15821_MI/* Method Usage */,
	&t466_TI/* Class Usage */,
	&m15873_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2900_0_0_0;
extern Il2CppType t2900_1_0_0;
struct t2900;
extern Il2CppGenericClass t2900_GC;
TypeInfo t2900_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ShimEnumerator", "", t2900_MIs, t2900_PIs, t2900_FIs, NULL, &t29_TI, NULL, &t1254_TI, &t2900_TI, t2900_ITIs, t2900_VT, &EmptyCustomAttributesCache, &t2900_TI, &t2900_0_0_0, &t2900_1_0_0, t2900_IOs, &t2900_GC, NULL, NULL, NULL, t2900_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2900), 0, -1, 0, 0, -1, 1056771, 0, false, false, false, false, true, false, false, false, false, false, false, false, 6, 4, 1, 0, 0, 9, 2, 2};
#ifndef _MSC_VER
#else
#endif

#include "t2903.h"
extern TypeInfo t1746_TI;
extern TypeInfo t2903_TI;
#include "t2903MD.h"
extern Il2CppType t1746_0_0_0;
extern MethodInfo m15885_MI;
extern MethodInfo m29256_MI;
extern MethodInfo m24137_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1<System.String>
extern Il2CppType t2901_0_0_49;
FieldInfo t2901_f0_FieldInfo = 
{
	"_default", &t2901_0_0_49, &t2901_TI, offsetof(t2901_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2901_FIs[] =
{
	&t2901_f0_FieldInfo,
	NULL
};
static PropertyInfo t2901____Default_PropertyInfo = 
{
	&t2901_TI, "Default", &m15881_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2901_PIs[] =
{
	&t2901____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15877_GM;
MethodInfo m15877_MI = 
{
	".ctor", (methodPointerType)&m10703_gshared, &t2901_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15877_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15878_GM;
MethodInfo m15878_MI = 
{
	".cctor", (methodPointerType)&m10704_gshared, &t2901_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15878_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2901_m15879_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15879_GM;
MethodInfo m15879_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m10705_gshared, &t2901_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2901_m15879_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15879_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2901_m15880_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15880_GM;
MethodInfo m15880_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m10706_gshared, &t2901_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2901_m15880_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15880_GM};
extern Il2CppType t7_0_0_0;
static ParameterInfo t2901_m29256_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29256_GM;
MethodInfo m29256_MI = 
{
	"GetHashCode", NULL, &t2901_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2901_m29256_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29256_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t2901_m24137_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m24137_GM;
MethodInfo m24137_MI = 
{
	"Equals", NULL, &t2901_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2901_m24137_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m24137_GM};
extern Il2CppType t2901_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15881_GM;
MethodInfo m15881_MI = 
{
	"get_Default", (methodPointerType)&m10707_gshared, &t2901_TI, &t2901_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15881_GM};
static MethodInfo* t2901_MIs[] =
{
	&m15877_MI,
	&m15878_MI,
	&m15879_MI,
	&m15880_MI,
	&m29256_MI,
	&m24137_MI,
	&m15881_MI,
	NULL
};
extern MethodInfo m15880_MI;
extern MethodInfo m15879_MI;
static MethodInfo* t2901_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m24137_MI,
	&m29256_MI,
	&m15880_MI,
	&m15879_MI,
	NULL,
	NULL,
};
static TypeInfo* t2901_ITIs[] = 
{
	&t2096_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t2901_IOs[] = 
{
	{ &t2096_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2901_TI;
extern TypeInfo t2901_TI;
extern TypeInfo t2903_TI;
extern TypeInfo t7_TI;
static Il2CppRGCTXData t2901_RGCTXData[9] = 
{
	&t1746_0_0_0/* Type Usage */,
	&t7_0_0_0/* Type Usage */,
	&t2901_TI/* Class Usage */,
	&t2901_TI/* Static Usage */,
	&t2903_TI/* Class Usage */,
	&m15885_MI/* Method Usage */,
	&t7_TI/* Class Usage */,
	&m29256_MI/* Method Usage */,
	&m24137_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2901_0_0_0;
extern Il2CppType t2901_1_0_0;
struct t2901;
extern Il2CppGenericClass t2901_GC;
TypeInfo t2901_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t2901_MIs, t2901_PIs, t2901_FIs, NULL, &t29_TI, NULL, NULL, &t2901_TI, t2901_ITIs, t2901_VT, &EmptyCustomAttributesCache, &t2901_TI, &t2901_0_0_0, &t2901_1_0_0, t2901_IOs, &t2901_GC, NULL, NULL, NULL, t2901_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2901), 0, -1, sizeof(t2901_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#include "t2902.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2902_TI;
#include "t2902MD.h"

extern MethodInfo m15877_MI;
extern MethodInfo m27357_MI;


extern MethodInfo m15882_MI;
 void m15882 (t2902 * __this, MethodInfo* method){
	{
		m15877(__this, &m15877_MI);
		return;
	}
}
extern MethodInfo m15883_MI;
 int32_t m15883 (t2902 * __this, t7* p0, MethodInfo* method){
	{
		t7* L_0 = p0;
		if (((t7*)L_0))
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}

IL_000a:
	{
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1322_MI, (*(&p0)));
		return L_1;
	}
}
extern MethodInfo m15884_MI;
 bool m15884 (t2902 * __this, t7* p0, t7* p1, MethodInfo* method){
	{
		t7* L_0 = p0;
		if (((t7*)L_0))
		{
			goto IL_0012;
		}
	}
	{
		t7* L_1 = p1;
		return ((((t7*)((t7*)L_1)) == ((t29 *)NULL))? 1 : 0);
	}

IL_0012:
	{
		bool L_2 = (bool)InterfaceFuncInvoker1< bool, t7* >::Invoke(&m27357_MI, (*(&p0)), p1);
		return L_2;
	}
}
// Metadata Definition System.Collections.Generic.GenericEqualityComparer`1<System.String>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15882_GM;
MethodInfo m15882_MI = 
{
	".ctor", (methodPointerType)&m15882, &t2902_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15882_GM};
extern Il2CppType t7_0_0_0;
static ParameterInfo t2902_m15883_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15883_GM;
MethodInfo m15883_MI = 
{
	"GetHashCode", (methodPointerType)&m15883, &t2902_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2902_m15883_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15883_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t2902_m15884_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15884_GM;
MethodInfo m15884_MI = 
{
	"Equals", (methodPointerType)&m15884, &t2902_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2902_m15884_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15884_GM};
static MethodInfo* t2902_MIs[] =
{
	&m15882_MI,
	&m15883_MI,
	&m15884_MI,
	NULL
};
static MethodInfo* t2902_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15884_MI,
	&m15883_MI,
	&m15880_MI,
	&m15879_MI,
	&m15883_MI,
	&m15884_MI,
};
static Il2CppInterfaceOffsetPair t2902_IOs[] = 
{
	{ &t2096_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2901_TI;
extern TypeInfo t2901_TI;
extern TypeInfo t2903_TI;
extern TypeInfo t7_TI;
static Il2CppRGCTXData t2902_RGCTXData[9] = 
{
	&t1746_0_0_0/* Type Usage */,
	&t7_0_0_0/* Type Usage */,
	&t2901_TI/* Class Usage */,
	&t2901_TI/* Static Usage */,
	&t2903_TI/* Class Usage */,
	&m15885_MI/* Method Usage */,
	&t7_TI/* Class Usage */,
	&m29256_MI/* Method Usage */,
	&m24137_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2902_0_0_0;
extern Il2CppType t2902_1_0_0;
struct t2902;
extern Il2CppGenericClass t2902_GC;
TypeInfo t2902_TI = 
{
	&g_mscorlib_dll_Image, NULL, "GenericEqualityComparer`1", "System.Collections.Generic", t2902_MIs, NULL, NULL, NULL, &t2901_TI, NULL, NULL, &t2902_TI, NULL, t2902_VT, &EmptyCustomAttributesCache, &t2902_TI, &t2902_0_0_0, &t2902_1_0_0, t2902_IOs, &t2902_GC, NULL, NULL, NULL, t2902_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2902), 0, -1, 0, 0, -1, 1057024, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.String>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15885_GM;
MethodInfo m15885_MI = 
{
	".ctor", (methodPointerType)&m10738_gshared, &t2903_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15885_GM};
extern Il2CppType t7_0_0_0;
static ParameterInfo t2903_m15886_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15886_GM;
MethodInfo m15886_MI = 
{
	"GetHashCode", (methodPointerType)&m10739_gshared, &t2903_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2903_m15886_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15886_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t2903_m15887_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15887_GM;
MethodInfo m15887_MI = 
{
	"Equals", (methodPointerType)&m10740_gshared, &t2903_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2903_m15887_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15887_GM};
static MethodInfo* t2903_MIs[] =
{
	&m15885_MI,
	&m15886_MI,
	&m15887_MI,
	NULL
};
extern MethodInfo m15887_MI;
extern MethodInfo m15886_MI;
static MethodInfo* t2903_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15887_MI,
	&m15886_MI,
	&m15880_MI,
	&m15879_MI,
	&m15886_MI,
	&m15887_MI,
};
static Il2CppInterfaceOffsetPair t2903_IOs[] = 
{
	{ &t2096_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2901_TI;
extern TypeInfo t2901_TI;
extern TypeInfo t2903_TI;
extern TypeInfo t7_TI;
extern TypeInfo t7_TI;
static Il2CppRGCTXData t2903_RGCTXData[11] = 
{
	&t1746_0_0_0/* Type Usage */,
	&t7_0_0_0/* Type Usage */,
	&t2901_TI/* Class Usage */,
	&t2901_TI/* Static Usage */,
	&t2903_TI/* Class Usage */,
	&m15885_MI/* Method Usage */,
	&t7_TI/* Class Usage */,
	&m29256_MI/* Method Usage */,
	&m24137_MI/* Method Usage */,
	&m15877_MI/* Method Usage */,
	&t7_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2903_0_0_0;
extern Il2CppType t2903_1_0_0;
struct t2903;
extern Il2CppGenericClass t2903_GC;
TypeInfo t2903_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2903_MIs, NULL, NULL, NULL, &t2901_TI, NULL, &t1256_TI, &t2903_TI, NULL, t2903_VT, &EmptyCustomAttributesCache, &t2903_TI, &t2903_0_0_0, &t2903_1_0_0, t2903_IOs, &t2903_GC, NULL, NULL, NULL, t2903_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2903), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<UnityEngine.GUIStyle>
extern Il2CppType t466_0_0_0;
extern Il2CppType t466_0_0_0;
static ParameterInfo t6720_m29235_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t466_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t466_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29235_GM;
MethodInfo m29235_MI = 
{
	"Equals", NULL, &t6720_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t6720_m29235_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29235_GM};
extern Il2CppType t466_0_0_0;
static ParameterInfo t6720_m29257_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t466_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29257_GM;
MethodInfo m29257_MI = 
{
	"GetHashCode", NULL, &t6720_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6720_m29257_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29257_GM};
static MethodInfo* t6720_MIs[] =
{
	&m29235_MI,
	&m29257_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6720_0_0_0;
extern Il2CppType t6720_1_0_0;
struct t6720;
extern Il2CppGenericClass t6720_GC;
TypeInfo t6720_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t6720_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6720_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6720_TI, &t6720_0_0_0, &t6720_1_0_0, NULL, &t6720_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2905.h"
extern TypeInfo t6727_TI;
extern TypeInfo t2905_TI;
#include "t2905MD.h"
extern Il2CppType t6727_0_0_0;
extern MethodInfo m15893_MI;
extern MethodInfo m29258_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1<UnityEngine.GUIStyle>
extern Il2CppType t2904_0_0_49;
FieldInfo t2904_f0_FieldInfo = 
{
	"_default", &t2904_0_0_49, &t2904_TI, offsetof(t2904_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2904_FIs[] =
{
	&t2904_f0_FieldInfo,
	NULL
};
static PropertyInfo t2904____Default_PropertyInfo = 
{
	&t2904_TI, "Default", &m15892_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2904_PIs[] =
{
	&t2904____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15888_GM;
MethodInfo m15888_MI = 
{
	".ctor", (methodPointerType)&m10703_gshared, &t2904_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15888_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15889_GM;
MethodInfo m15889_MI = 
{
	".cctor", (methodPointerType)&m10704_gshared, &t2904_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15889_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2904_m15890_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15890_GM;
MethodInfo m15890_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m10705_gshared, &t2904_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2904_m15890_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15890_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2904_m15891_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15891_GM;
MethodInfo m15891_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m10706_gshared, &t2904_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2904_m15891_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15891_GM};
extern Il2CppType t466_0_0_0;
static ParameterInfo t2904_m29258_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t466_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29258_GM;
MethodInfo m29258_MI = 
{
	"GetHashCode", NULL, &t2904_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2904_m29258_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29258_GM};
extern Il2CppType t466_0_0_0;
extern Il2CppType t466_0_0_0;
static ParameterInfo t2904_m29236_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t466_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t466_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29236_GM;
MethodInfo m29236_MI = 
{
	"Equals", NULL, &t2904_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2904_m29236_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29236_GM};
extern Il2CppType t2904_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15892_GM;
MethodInfo m15892_MI = 
{
	"get_Default", (methodPointerType)&m10707_gshared, &t2904_TI, &t2904_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15892_GM};
static MethodInfo* t2904_MIs[] =
{
	&m15888_MI,
	&m15889_MI,
	&m15890_MI,
	&m15891_MI,
	&m29258_MI,
	&m29236_MI,
	&m15892_MI,
	NULL
};
extern MethodInfo m15891_MI;
extern MethodInfo m15890_MI;
static MethodInfo* t2904_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m29236_MI,
	&m29258_MI,
	&m15891_MI,
	&m15890_MI,
	NULL,
	NULL,
};
static TypeInfo* t2904_ITIs[] = 
{
	&t6720_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t2904_IOs[] = 
{
	{ &t6720_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2904_TI;
extern TypeInfo t2904_TI;
extern TypeInfo t2905_TI;
extern TypeInfo t466_TI;
static Il2CppRGCTXData t2904_RGCTXData[9] = 
{
	&t6727_0_0_0/* Type Usage */,
	&t466_0_0_0/* Type Usage */,
	&t2904_TI/* Class Usage */,
	&t2904_TI/* Static Usage */,
	&t2905_TI/* Class Usage */,
	&m15893_MI/* Method Usage */,
	&t466_TI/* Class Usage */,
	&m29258_MI/* Method Usage */,
	&m29236_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2904_0_0_0;
extern Il2CppType t2904_1_0_0;
struct t2904;
extern Il2CppGenericClass t2904_GC;
TypeInfo t2904_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t2904_MIs, t2904_PIs, t2904_FIs, NULL, &t29_TI, NULL, NULL, &t2904_TI, t2904_ITIs, t2904_VT, &EmptyCustomAttributesCache, &t2904_TI, &t2904_0_0_0, &t2904_1_0_0, t2904_IOs, &t2904_GC, NULL, NULL, NULL, t2904_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2904), 0, -1, sizeof(t2904_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IEquatable`1<UnityEngine.GUIStyle>
extern Il2CppType t466_0_0_0;
static ParameterInfo t6727_m29259_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t466_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29259_GM;
MethodInfo m29259_MI = 
{
	"Equals", NULL, &t6727_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6727_m29259_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29259_GM};
static MethodInfo* t6727_MIs[] =
{
	&m29259_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6727_1_0_0;
struct t6727;
extern Il2CppGenericClass t6727_GC;
TypeInfo t6727_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t6727_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6727_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6727_TI, &t6727_0_0_0, &t6727_1_0_0, NULL, &t6727_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m15888_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.GUIStyle>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15893_GM;
MethodInfo m15893_MI = 
{
	".ctor", (methodPointerType)&m10738_gshared, &t2905_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15893_GM};
extern Il2CppType t466_0_0_0;
static ParameterInfo t2905_m15894_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t466_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15894_GM;
MethodInfo m15894_MI = 
{
	"GetHashCode", (methodPointerType)&m10739_gshared, &t2905_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2905_m15894_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15894_GM};
extern Il2CppType t466_0_0_0;
extern Il2CppType t466_0_0_0;
static ParameterInfo t2905_m15895_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t466_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t466_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15895_GM;
MethodInfo m15895_MI = 
{
	"Equals", (methodPointerType)&m10740_gshared, &t2905_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2905_m15895_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15895_GM};
static MethodInfo* t2905_MIs[] =
{
	&m15893_MI,
	&m15894_MI,
	&m15895_MI,
	NULL
};
extern MethodInfo m15895_MI;
extern MethodInfo m15894_MI;
static MethodInfo* t2905_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15895_MI,
	&m15894_MI,
	&m15891_MI,
	&m15890_MI,
	&m15894_MI,
	&m15895_MI,
};
static Il2CppInterfaceOffsetPair t2905_IOs[] = 
{
	{ &t6720_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2904_TI;
extern TypeInfo t2904_TI;
extern TypeInfo t2905_TI;
extern TypeInfo t466_TI;
extern TypeInfo t466_TI;
static Il2CppRGCTXData t2905_RGCTXData[11] = 
{
	&t6727_0_0_0/* Type Usage */,
	&t466_0_0_0/* Type Usage */,
	&t2904_TI/* Class Usage */,
	&t2904_TI/* Static Usage */,
	&t2905_TI/* Class Usage */,
	&m15893_MI/* Method Usage */,
	&t466_TI/* Class Usage */,
	&m29258_MI/* Method Usage */,
	&m29236_MI/* Method Usage */,
	&m15888_MI/* Method Usage */,
	&t466_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2905_0_0_0;
extern Il2CppType t2905_1_0_0;
struct t2905;
extern Il2CppGenericClass t2905_GC;
TypeInfo t2905_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2905_MIs, NULL, NULL, NULL, &t2904_TI, NULL, &t1256_TI, &t2905_TI, NULL, t2905_VT, &EmptyCustomAttributesCache, &t2905_TI, &t2905_0_0_0, &t2905_1_0_0, t2905_IOs, &t2905_GC, NULL, NULL, NULL, t2905_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2905), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4365_TI;

#include "t149.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.FontStyle>
extern MethodInfo m29260_MI;
static PropertyInfo t4365____Current_PropertyInfo = 
{
	&t4365_TI, "Current", &m29260_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4365_PIs[] =
{
	&t4365____Current_PropertyInfo,
	NULL
};
extern Il2CppType t149_0_0_0;
extern void* RuntimeInvoker_t149 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29260_GM;
MethodInfo m29260_MI = 
{
	"get_Current", NULL, &t4365_TI, &t149_0_0_0, RuntimeInvoker_t149, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29260_GM};
static MethodInfo* t4365_MIs[] =
{
	&m29260_MI,
	NULL
};
static TypeInfo* t4365_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4365_0_0_0;
extern Il2CppType t4365_1_0_0;
struct t4365;
extern Il2CppGenericClass t4365_GC;
TypeInfo t4365_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4365_MIs, t4365_PIs, NULL, NULL, NULL, NULL, NULL, &t4365_TI, t4365_ITIs, NULL, &EmptyCustomAttributesCache, &t4365_TI, &t4365_0_0_0, &t4365_1_0_0, NULL, &t4365_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2906.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2906_TI;
#include "t2906MD.h"

extern TypeInfo t149_TI;
extern MethodInfo m15900_MI;
extern MethodInfo m22204_MI;
struct t20;
 int32_t m22204 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m15896_MI;
 void m15896 (t2906 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m15897_MI;
 t29 * m15897 (t2906 * __this, MethodInfo* method){
	{
		int32_t L_0 = m15900(__this, &m15900_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t149_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m15898_MI;
 void m15898 (t2906 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m15899_MI;
 bool m15899 (t2906 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m15900 (t2906 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m22204(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m22204_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.FontStyle>
extern Il2CppType t20_0_0_1;
FieldInfo t2906_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2906_TI, offsetof(t2906, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2906_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2906_TI, offsetof(t2906, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2906_FIs[] =
{
	&t2906_f0_FieldInfo,
	&t2906_f1_FieldInfo,
	NULL
};
static PropertyInfo t2906____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2906_TI, "System.Collections.IEnumerator.Current", &m15897_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2906____Current_PropertyInfo = 
{
	&t2906_TI, "Current", &m15900_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2906_PIs[] =
{
	&t2906____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2906____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2906_m15896_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15896_GM;
MethodInfo m15896_MI = 
{
	".ctor", (methodPointerType)&m15896, &t2906_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2906_m15896_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15896_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15897_GM;
MethodInfo m15897_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m15897, &t2906_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15897_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15898_GM;
MethodInfo m15898_MI = 
{
	"Dispose", (methodPointerType)&m15898, &t2906_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15898_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15899_GM;
MethodInfo m15899_MI = 
{
	"MoveNext", (methodPointerType)&m15899, &t2906_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15899_GM};
extern Il2CppType t149_0_0_0;
extern void* RuntimeInvoker_t149 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15900_GM;
MethodInfo m15900_MI = 
{
	"get_Current", (methodPointerType)&m15900, &t2906_TI, &t149_0_0_0, RuntimeInvoker_t149, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15900_GM};
static MethodInfo* t2906_MIs[] =
{
	&m15896_MI,
	&m15897_MI,
	&m15898_MI,
	&m15899_MI,
	&m15900_MI,
	NULL
};
static MethodInfo* t2906_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15897_MI,
	&m15899_MI,
	&m15898_MI,
	&m15900_MI,
};
static TypeInfo* t2906_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4365_TI,
};
static Il2CppInterfaceOffsetPair t2906_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4365_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2906_0_0_0;
extern Il2CppType t2906_1_0_0;
extern Il2CppGenericClass t2906_GC;
TypeInfo t2906_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2906_MIs, t2906_PIs, t2906_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2906_TI, t2906_ITIs, t2906_VT, &EmptyCustomAttributesCache, &t2906_TI, &t2906_0_0_0, &t2906_1_0_0, t2906_IOs, &t2906_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2906)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5589_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.FontStyle>
extern MethodInfo m29261_MI;
static PropertyInfo t5589____Count_PropertyInfo = 
{
	&t5589_TI, "Count", &m29261_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29262_MI;
static PropertyInfo t5589____IsReadOnly_PropertyInfo = 
{
	&t5589_TI, "IsReadOnly", &m29262_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5589_PIs[] =
{
	&t5589____Count_PropertyInfo,
	&t5589____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29261_GM;
MethodInfo m29261_MI = 
{
	"get_Count", NULL, &t5589_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29261_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29262_GM;
MethodInfo m29262_MI = 
{
	"get_IsReadOnly", NULL, &t5589_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29262_GM};
extern Il2CppType t149_0_0_0;
extern Il2CppType t149_0_0_0;
static ParameterInfo t5589_m29263_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t149_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29263_GM;
MethodInfo m29263_MI = 
{
	"Add", NULL, &t5589_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5589_m29263_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29263_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29264_GM;
MethodInfo m29264_MI = 
{
	"Clear", NULL, &t5589_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29264_GM};
extern Il2CppType t149_0_0_0;
static ParameterInfo t5589_m29265_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t149_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29265_GM;
MethodInfo m29265_MI = 
{
	"Contains", NULL, &t5589_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5589_m29265_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29265_GM};
extern Il2CppType t3746_0_0_0;
extern Il2CppType t3746_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5589_m29266_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3746_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29266_GM;
MethodInfo m29266_MI = 
{
	"CopyTo", NULL, &t5589_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5589_m29266_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29266_GM};
extern Il2CppType t149_0_0_0;
static ParameterInfo t5589_m29267_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t149_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29267_GM;
MethodInfo m29267_MI = 
{
	"Remove", NULL, &t5589_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5589_m29267_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29267_GM};
static MethodInfo* t5589_MIs[] =
{
	&m29261_MI,
	&m29262_MI,
	&m29263_MI,
	&m29264_MI,
	&m29265_MI,
	&m29266_MI,
	&m29267_MI,
	NULL
};
extern TypeInfo t5591_TI;
static TypeInfo* t5589_ITIs[] = 
{
	&t603_TI,
	&t5591_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5589_0_0_0;
extern Il2CppType t5589_1_0_0;
struct t5589;
extern Il2CppGenericClass t5589_GC;
TypeInfo t5589_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5589_MIs, t5589_PIs, NULL, NULL, NULL, NULL, NULL, &t5589_TI, t5589_ITIs, NULL, &EmptyCustomAttributesCache, &t5589_TI, &t5589_0_0_0, &t5589_1_0_0, NULL, &t5589_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.FontStyle>
extern Il2CppType t4365_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29268_GM;
MethodInfo m29268_MI = 
{
	"GetEnumerator", NULL, &t5591_TI, &t4365_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29268_GM};
static MethodInfo* t5591_MIs[] =
{
	&m29268_MI,
	NULL
};
static TypeInfo* t5591_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5591_0_0_0;
extern Il2CppType t5591_1_0_0;
struct t5591;
extern Il2CppGenericClass t5591_GC;
TypeInfo t5591_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5591_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5591_TI, t5591_ITIs, NULL, &EmptyCustomAttributesCache, &t5591_TI, &t5591_0_0_0, &t5591_1_0_0, NULL, &t5591_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5590_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.FontStyle>
extern MethodInfo m29269_MI;
extern MethodInfo m29270_MI;
static PropertyInfo t5590____Item_PropertyInfo = 
{
	&t5590_TI, "Item", &m29269_MI, &m29270_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5590_PIs[] =
{
	&t5590____Item_PropertyInfo,
	NULL
};
extern Il2CppType t149_0_0_0;
static ParameterInfo t5590_m29271_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t149_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29271_GM;
MethodInfo m29271_MI = 
{
	"IndexOf", NULL, &t5590_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5590_m29271_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29271_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t149_0_0_0;
static ParameterInfo t5590_m29272_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t149_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29272_GM;
MethodInfo m29272_MI = 
{
	"Insert", NULL, &t5590_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5590_m29272_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29272_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5590_m29273_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29273_GM;
MethodInfo m29273_MI = 
{
	"RemoveAt", NULL, &t5590_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5590_m29273_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29273_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5590_m29269_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t149_0_0_0;
extern void* RuntimeInvoker_t149_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29269_GM;
MethodInfo m29269_MI = 
{
	"get_Item", NULL, &t5590_TI, &t149_0_0_0, RuntimeInvoker_t149_t44, t5590_m29269_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29269_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t149_0_0_0;
static ParameterInfo t5590_m29270_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t149_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29270_GM;
MethodInfo m29270_MI = 
{
	"set_Item", NULL, &t5590_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5590_m29270_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29270_GM};
static MethodInfo* t5590_MIs[] =
{
	&m29271_MI,
	&m29272_MI,
	&m29273_MI,
	&m29269_MI,
	&m29270_MI,
	NULL
};
static TypeInfo* t5590_ITIs[] = 
{
	&t603_TI,
	&t5589_TI,
	&t5591_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5590_0_0_0;
extern Il2CppType t5590_1_0_0;
struct t5590;
extern Il2CppGenericClass t5590_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5590_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5590_MIs, t5590_PIs, NULL, NULL, NULL, NULL, NULL, &t5590_TI, t5590_ITIs, NULL, &t1908__CustomAttributeCache, &t5590_TI, &t5590_0_0_0, &t5590_1_0_0, NULL, &t5590_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4366_TI;

#include "t205.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.TouchScreenKeyboardType>
extern MethodInfo m29274_MI;
static PropertyInfo t4366____Current_PropertyInfo = 
{
	&t4366_TI, "Current", &m29274_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4366_PIs[] =
{
	&t4366____Current_PropertyInfo,
	NULL
};
extern Il2CppType t205_0_0_0;
extern void* RuntimeInvoker_t205 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29274_GM;
MethodInfo m29274_MI = 
{
	"get_Current", NULL, &t4366_TI, &t205_0_0_0, RuntimeInvoker_t205, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29274_GM};
static MethodInfo* t4366_MIs[] =
{
	&m29274_MI,
	NULL
};
static TypeInfo* t4366_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4366_0_0_0;
extern Il2CppType t4366_1_0_0;
struct t4366;
extern Il2CppGenericClass t4366_GC;
TypeInfo t4366_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4366_MIs, t4366_PIs, NULL, NULL, NULL, NULL, NULL, &t4366_TI, t4366_ITIs, NULL, &EmptyCustomAttributesCache, &t4366_TI, &t4366_0_0_0, &t4366_1_0_0, NULL, &t4366_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2907.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2907_TI;
#include "t2907MD.h"

extern TypeInfo t205_TI;
extern MethodInfo m15905_MI;
extern MethodInfo m22215_MI;
struct t20;
 int32_t m22215 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m15901_MI;
 void m15901 (t2907 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m15902_MI;
 t29 * m15902 (t2907 * __this, MethodInfo* method){
	{
		int32_t L_0 = m15905(__this, &m15905_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t205_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m15903_MI;
 void m15903 (t2907 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m15904_MI;
 bool m15904 (t2907 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m15905 (t2907 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m22215(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m22215_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.TouchScreenKeyboardType>
extern Il2CppType t20_0_0_1;
FieldInfo t2907_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2907_TI, offsetof(t2907, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2907_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2907_TI, offsetof(t2907, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2907_FIs[] =
{
	&t2907_f0_FieldInfo,
	&t2907_f1_FieldInfo,
	NULL
};
static PropertyInfo t2907____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2907_TI, "System.Collections.IEnumerator.Current", &m15902_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2907____Current_PropertyInfo = 
{
	&t2907_TI, "Current", &m15905_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2907_PIs[] =
{
	&t2907____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2907____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2907_m15901_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15901_GM;
MethodInfo m15901_MI = 
{
	".ctor", (methodPointerType)&m15901, &t2907_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2907_m15901_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15901_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15902_GM;
MethodInfo m15902_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m15902, &t2907_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15902_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15903_GM;
MethodInfo m15903_MI = 
{
	"Dispose", (methodPointerType)&m15903, &t2907_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15903_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15904_GM;
MethodInfo m15904_MI = 
{
	"MoveNext", (methodPointerType)&m15904, &t2907_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15904_GM};
extern Il2CppType t205_0_0_0;
extern void* RuntimeInvoker_t205 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15905_GM;
MethodInfo m15905_MI = 
{
	"get_Current", (methodPointerType)&m15905, &t2907_TI, &t205_0_0_0, RuntimeInvoker_t205, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15905_GM};
static MethodInfo* t2907_MIs[] =
{
	&m15901_MI,
	&m15902_MI,
	&m15903_MI,
	&m15904_MI,
	&m15905_MI,
	NULL
};
static MethodInfo* t2907_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15902_MI,
	&m15904_MI,
	&m15903_MI,
	&m15905_MI,
};
static TypeInfo* t2907_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4366_TI,
};
static Il2CppInterfaceOffsetPair t2907_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4366_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2907_0_0_0;
extern Il2CppType t2907_1_0_0;
extern Il2CppGenericClass t2907_GC;
TypeInfo t2907_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2907_MIs, t2907_PIs, t2907_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2907_TI, t2907_ITIs, t2907_VT, &EmptyCustomAttributesCache, &t2907_TI, &t2907_0_0_0, &t2907_1_0_0, t2907_IOs, &t2907_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2907)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5592_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.TouchScreenKeyboardType>
extern MethodInfo m29275_MI;
static PropertyInfo t5592____Count_PropertyInfo = 
{
	&t5592_TI, "Count", &m29275_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29276_MI;
static PropertyInfo t5592____IsReadOnly_PropertyInfo = 
{
	&t5592_TI, "IsReadOnly", &m29276_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5592_PIs[] =
{
	&t5592____Count_PropertyInfo,
	&t5592____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29275_GM;
MethodInfo m29275_MI = 
{
	"get_Count", NULL, &t5592_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29275_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29276_GM;
MethodInfo m29276_MI = 
{
	"get_IsReadOnly", NULL, &t5592_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29276_GM};
extern Il2CppType t205_0_0_0;
extern Il2CppType t205_0_0_0;
static ParameterInfo t5592_m29277_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t205_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29277_GM;
MethodInfo m29277_MI = 
{
	"Add", NULL, &t5592_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5592_m29277_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29277_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29278_GM;
MethodInfo m29278_MI = 
{
	"Clear", NULL, &t5592_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29278_GM};
extern Il2CppType t205_0_0_0;
static ParameterInfo t5592_m29279_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t205_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29279_GM;
MethodInfo m29279_MI = 
{
	"Contains", NULL, &t5592_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5592_m29279_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29279_GM};
extern Il2CppType t3747_0_0_0;
extern Il2CppType t3747_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5592_m29280_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3747_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29280_GM;
MethodInfo m29280_MI = 
{
	"CopyTo", NULL, &t5592_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5592_m29280_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29280_GM};
extern Il2CppType t205_0_0_0;
static ParameterInfo t5592_m29281_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t205_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29281_GM;
MethodInfo m29281_MI = 
{
	"Remove", NULL, &t5592_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5592_m29281_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29281_GM};
static MethodInfo* t5592_MIs[] =
{
	&m29275_MI,
	&m29276_MI,
	&m29277_MI,
	&m29278_MI,
	&m29279_MI,
	&m29280_MI,
	&m29281_MI,
	NULL
};
extern TypeInfo t5594_TI;
static TypeInfo* t5592_ITIs[] = 
{
	&t603_TI,
	&t5594_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5592_0_0_0;
extern Il2CppType t5592_1_0_0;
struct t5592;
extern Il2CppGenericClass t5592_GC;
TypeInfo t5592_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5592_MIs, t5592_PIs, NULL, NULL, NULL, NULL, NULL, &t5592_TI, t5592_ITIs, NULL, &EmptyCustomAttributesCache, &t5592_TI, &t5592_0_0_0, &t5592_1_0_0, NULL, &t5592_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.TouchScreenKeyboardType>
extern Il2CppType t4366_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29282_GM;
MethodInfo m29282_MI = 
{
	"GetEnumerator", NULL, &t5594_TI, &t4366_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29282_GM};
static MethodInfo* t5594_MIs[] =
{
	&m29282_MI,
	NULL
};
static TypeInfo* t5594_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5594_0_0_0;
extern Il2CppType t5594_1_0_0;
struct t5594;
extern Il2CppGenericClass t5594_GC;
TypeInfo t5594_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5594_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5594_TI, t5594_ITIs, NULL, &EmptyCustomAttributesCache, &t5594_TI, &t5594_0_0_0, &t5594_1_0_0, NULL, &t5594_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5593_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.TouchScreenKeyboardType>
extern MethodInfo m29283_MI;
extern MethodInfo m29284_MI;
static PropertyInfo t5593____Item_PropertyInfo = 
{
	&t5593_TI, "Item", &m29283_MI, &m29284_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5593_PIs[] =
{
	&t5593____Item_PropertyInfo,
	NULL
};
extern Il2CppType t205_0_0_0;
static ParameterInfo t5593_m29285_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t205_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29285_GM;
MethodInfo m29285_MI = 
{
	"IndexOf", NULL, &t5593_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5593_m29285_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29285_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t205_0_0_0;
static ParameterInfo t5593_m29286_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t205_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29286_GM;
MethodInfo m29286_MI = 
{
	"Insert", NULL, &t5593_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5593_m29286_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29286_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5593_m29287_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29287_GM;
MethodInfo m29287_MI = 
{
	"RemoveAt", NULL, &t5593_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5593_m29287_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29287_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5593_m29283_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t205_0_0_0;
extern void* RuntimeInvoker_t205_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29283_GM;
MethodInfo m29283_MI = 
{
	"get_Item", NULL, &t5593_TI, &t205_0_0_0, RuntimeInvoker_t205_t44, t5593_m29283_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29283_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t205_0_0_0;
static ParameterInfo t5593_m29284_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t205_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29284_GM;
MethodInfo m29284_MI = 
{
	"set_Item", NULL, &t5593_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5593_m29284_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29284_GM};
static MethodInfo* t5593_MIs[] =
{
	&m29285_MI,
	&m29286_MI,
	&m29287_MI,
	&m29283_MI,
	&m29284_MI,
	NULL
};
static TypeInfo* t5593_ITIs[] = 
{
	&t603_TI,
	&t5592_TI,
	&t5594_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5593_0_0_0;
extern Il2CppType t5593_1_0_0;
struct t5593;
extern Il2CppGenericClass t5593_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5593_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5593_MIs, t5593_PIs, NULL, NULL, NULL, NULL, NULL, &t5593_TI, t5593_ITIs, NULL, &t1908__CustomAttributeCache, &t5593_TI, &t5593_0_0_0, &t5593_1_0_0, NULL, &t5593_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t485.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t485_TI;
#include "t485MD.h"

#include "t2909.h"
#include "t2911.h"
#include "t2908.h"
#include "t2917.h"
#include "t2913.h"
#include "t2918.h"
#include "t2361.h"
extern TypeInfo t2909_TI;
extern TypeInfo t2910_TI;
extern TypeInfo t2911_TI;
extern TypeInfo t2908_TI;
extern TypeInfo t2917_TI;
extern TypeInfo t2913_TI;
extern TypeInfo t2918_TI;
extern TypeInfo t2361_TI;
extern TypeInfo t2344_TI;
#include "t2909MD.h"
#include "t2911MD.h"
#include "t2908MD.h"
#include "t2917MD.h"
#include "t2913MD.h"
#include "t2918MD.h"
#include "t2361MD.h"
extern Il2CppType t2910_0_0_0;
extern MethodInfo m15935_MI;
extern MethodInfo m15941_MI;
extern MethodInfo m15925_MI;
extern MethodInfo m15942_MI;
extern MethodInfo m15926_MI;
extern MethodInfo m15933_MI;
extern MethodInfo m15957_MI;
extern MethodInfo m15927_MI;
extern MethodInfo m4039_MI;
extern MethodInfo m15939_MI;
extern MethodInfo m15947_MI;
extern MethodInfo m15949_MI;
extern MethodInfo m15943_MI;
extern MethodInfo m15932_MI;
extern MethodInfo m15929_MI;
extern MethodInfo m15945_MI;
extern MethodInfo m15992_MI;
extern MethodInfo m22239_MI;
extern MethodInfo m15930_MI;
extern MethodInfo m15996_MI;
extern MethodInfo m22241_MI;
extern MethodInfo m15976_MI;
extern MethodInfo m16000_MI;
extern MethodInfo m15928_MI;
extern MethodInfo m15924_MI;
extern MethodInfo m15946_MI;
extern MethodInfo m22242_MI;
extern MethodInfo m12170_MI;
extern MethodInfo m27290_MI;
extern MethodInfo m4040_MI;
extern MethodInfo m27426_MI;
struct t485;
 void m22239 (t485 * __this, t3541* p0, int32_t p1, t2908 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t485;
 void m22241 (t485 * __this, t20 * p0, int32_t p1, t2917 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t485;
 void m22242 (t485 * __this, t2910* p0, int32_t p1, t2917 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m15906_MI;
 void m15906 (t485 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		m15927(__this, ((int32_t)10), (t29*)NULL, &m15927_MI);
		return;
	}
}
extern MethodInfo m15907_MI;
 void m15907 (t485 * __this, t29* p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		m15927(__this, ((int32_t)10), p0, &m15927_MI);
		return;
	}
}
extern MethodInfo m4038_MI;
 void m4038 (t485 * __this, int32_t p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		m15927(__this, p0, (t29*)NULL, &m15927_MI);
		return;
	}
}
extern MethodInfo m15908_MI;
 void m15908 (t485 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		__this->f13 = p0;
		return;
	}
}
extern MethodInfo m15909_MI;
 t29 * m15909 (t485 * __this, t29 * p0, MethodInfo* method){
	{
		if (!((t7*)IsInst(p0, (&t7_TI))))
		{
			goto IL_0029;
		}
	}
	{
		bool L_0 = (bool)VirtFuncInvoker1< bool, t7* >::Invoke(&m15935_MI, __this, ((t7*)Castclass(p0, (&t7_TI))));
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		t7* L_1 = m15941(__this, p0, &m15941_MI);
		int32_t L_2 = (int32_t)VirtFuncInvoker1< int32_t, t7* >::Invoke(&m15925_MI, __this, L_1);
		int32_t L_3 = L_2;
		t29 * L_4 = Box(InitializedTypeInfo(&t44_TI), &L_3);
		return L_4;
	}

IL_0029:
	{
		return NULL;
	}
}
extern MethodInfo m15910_MI;
 void m15910 (t485 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	{
		t7* L_0 = m15941(__this, p0, &m15941_MI);
		int32_t L_1 = m15942(__this, p1, &m15942_MI);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m15926_MI, __this, L_0, L_1);
		return;
	}
}
extern MethodInfo m15911_MI;
 void m15911 (t485 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	{
		t7* L_0 = m15941(__this, p0, &m15941_MI);
		int32_t L_1 = m15942(__this, p1, &m15942_MI);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, __this, L_0, L_1);
		return;
	}
}
extern MethodInfo m15912_MI;
 void m15912 (t485 * __this, t29 * p0, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		if (!((t7*)IsInst(p0, (&t7_TI))))
		{
			goto IL_0023;
		}
	}
	{
		VirtFuncInvoker1< bool, t7* >::Invoke(&m15939_MI, __this, ((t7*)Castclass(p0, (&t7_TI))));
	}

IL_0023:
	{
		return;
	}
}
extern MethodInfo m15913_MI;
 bool m15913 (t485 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m15914_MI;
 t29 * m15914 (t485 * __this, MethodInfo* method){
	{
		return __this;
	}
}
extern MethodInfo m15915_MI;
 bool m15915 (t485 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m15916_MI;
 void m15916 (t485 * __this, t2911  p0, MethodInfo* method){
	{
		t7* L_0 = m15947((&p0), &m15947_MI);
		int32_t L_1 = m15949((&p0), &m15949_MI);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, __this, L_0, L_1);
		return;
	}
}
extern MethodInfo m15917_MI;
 bool m15917 (t485 * __this, t2911  p0, MethodInfo* method){
	{
		bool L_0 = m15943(__this, p0, &m15943_MI);
		return L_0;
	}
}
extern MethodInfo m15918_MI;
 void m15918 (t485 * __this, t2910* p0, int32_t p1, MethodInfo* method){
	{
		m15932(__this, p0, p1, &m15932_MI);
		return;
	}
}
extern MethodInfo m15919_MI;
 bool m15919 (t485 * __this, t2911  p0, MethodInfo* method){
	{
		bool L_0 = m15943(__this, p0, &m15943_MI);
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return 0;
	}

IL_000b:
	{
		t7* L_1 = m15947((&p0), &m15947_MI);
		bool L_2 = (bool)VirtFuncInvoker1< bool, t7* >::Invoke(&m15939_MI, __this, L_1);
		return L_2;
	}
}
extern MethodInfo m15920_MI;
 void m15920 (t485 * __this, t20 * p0, int32_t p1, MethodInfo* method){
	t2910* V_0 = {0};
	t3541* V_1 = {0};
	int32_t G_B5_0 = 0;
	t3541* G_B5_1 = {0};
	t485 * G_B5_2 = {0};
	int32_t G_B4_0 = 0;
	t3541* G_B4_1 = {0};
	t485 * G_B4_2 = {0};
	{
		V_0 = ((t2910*)IsInst(p0, InitializedTypeInfo(&t2910_TI)));
		if (!V_0)
		{
			goto IL_0013;
		}
	}
	{
		m15932(__this, V_0, p1, &m15932_MI);
		return;
	}

IL_0013:
	{
		m15929(__this, p0, p1, &m15929_MI);
		V_1 = ((t3541*)IsInst(p0, InitializedTypeInfo(&t3541_TI)));
		if (!V_1)
		{
			goto IL_004b;
		}
	}
	{
		G_B4_0 = p1;
		G_B4_1 = V_1;
		G_B4_2 = ((t485 *)(__this));
		if ((((t485_SFs*)InitializedTypeInfo(&t485_TI)->static_fields)->f15))
		{
			G_B5_0 = p1;
			G_B5_1 = V_1;
			G_B5_2 = ((t485 *)(__this));
			goto IL_0040;
		}
	}
	{
		t35 L_0 = { &m15945_MI };
		t2908 * L_1 = (t2908 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2908_TI));
		m15992(L_1, NULL, L_0, &m15992_MI);
		((t485_SFs*)InitializedTypeInfo(&t485_TI)->static_fields)->f15 = L_1;
		G_B5_0 = G_B4_0;
		G_B5_1 = G_B4_1;
		G_B5_2 = ((t485 *)(G_B4_2));
	}

IL_0040:
	{
		m22239(G_B5_2, G_B5_1, G_B5_0, (((t485_SFs*)InitializedTypeInfo(&t485_TI)->static_fields)->f15), &m22239_MI);
		return;
	}

IL_004b:
	{
		t35 L_2 = { &m15930_MI };
		t2917 * L_3 = (t2917 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2917_TI));
		m15996(L_3, NULL, L_2, &m15996_MI);
		m22241(__this, p0, p1, L_3, &m22241_MI);
		return;
	}
}
extern MethodInfo m15921_MI;
 t29 * m15921 (t485 * __this, MethodInfo* method){
	{
		t2913  L_0 = {0};
		m15976(&L_0, __this, &m15976_MI);
		t2913  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2913_TI), &L_1);
		return (t29 *)L_2;
	}
}
extern MethodInfo m15922_MI;
 t29* m15922 (t485 * __this, MethodInfo* method){
	{
		t2913  L_0 = {0};
		m15976(&L_0, __this, &m15976_MI);
		t2913  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2913_TI), &L_1);
		return (t29*)L_2;
	}
}
extern MethodInfo m15923_MI;
 t29 * m15923 (t485 * __this, MethodInfo* method){
	{
		t2918 * L_0 = (t2918 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2918_TI));
		m16000(L_0, __this, &m16000_MI);
		return L_0;
	}
}
 int32_t m15924 (t485 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f10);
		return L_0;
	}
}
 int32_t m15925 (t485 * __this, t7* p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		t7* L_0 = p0;
		if (((t7*)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t7* >::Invoke(&m29233_MI, L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		t841* L_5 = (__this->f4);
		int32_t L_6 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_5)->max_length)))));
		V_1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_4, L_6))-1));
		goto IL_008f;
	}

IL_0042:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_1))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_007d;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t446* L_10 = (__this->f6);
		int32_t L_11 = V_1;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t7*, t7* >::Invoke(&m29234_MI, L_9, (*(t7**)(t7**)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_007d;
		}
	}
	{
		t841* L_13 = (__this->f7);
		int32_t L_14 = V_1;
		return (*(int32_t*)(int32_t*)SZArrayLdElema(L_13, L_14));
	}

IL_007d:
	{
		t1965* L_15 = (__this->f5);
		int32_t L_16 = (((t1248 *)(t1248 *)SZArrayLdElema(L_15, V_1))->f1);
		V_1 = L_16;
	}

IL_008f:
	{
		if ((((uint32_t)V_1) != ((uint32_t)(-1))))
		{
			goto IL_0042;
		}
	}
	{
		t1258 * L_17 = (t1258 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1258_TI));
		m6569(L_17, &m6569_MI);
		il2cpp_codegen_raise_exception(L_17);
	}
}
 void m15926 (t485 * __this, t7* p0, int32_t p1, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		t7* L_0 = p0;
		if (((t7*)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t7* >::Invoke(&m29233_MI, L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_4)->max_length)))));
		t841* L_5 = (__this->f4);
		int32_t L_6 = V_1;
		V_2 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_6))-1));
		V_3 = (-1);
		if ((((int32_t)V_2) == ((int32_t)(-1))))
		{
			goto IL_0090;
		}
	}

IL_0048:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_2))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_0078;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t446* L_10 = (__this->f6);
		int32_t L_11 = V_2;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t7*, t7* >::Invoke(&m29234_MI, L_9, (*(t7**)(t7**)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_0078;
		}
	}
	{
		goto IL_0090;
	}

IL_0078:
	{
		V_3 = V_2;
		t1965* L_13 = (__this->f5);
		int32_t L_14 = (((t1248 *)(t1248 *)SZArrayLdElema(L_13, V_2))->f1);
		V_2 = L_14;
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0048;
		}
	}

IL_0090:
	{
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0148;
		}
	}
	{
		int32_t L_15 = (__this->f10);
		int32_t L_16 = ((int32_t)(L_15+1));
		V_4 = L_16;
		__this->f10 = L_16;
		int32_t L_17 = (__this->f11);
		if ((((int32_t)V_4) <= ((int32_t)L_17)))
		{
			goto IL_00c9;
		}
	}
	{
		m15933(__this, &m15933_MI);
		t841* L_18 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_18)->max_length)))));
	}

IL_00c9:
	{
		int32_t L_19 = (__this->f9);
		V_2 = L_19;
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_00ea;
		}
	}
	{
		int32_t L_20 = (__this->f8);
		int32_t L_21 = L_20;
		V_4 = L_21;
		__this->f8 = ((int32_t)(L_21+1));
		V_2 = V_4;
		goto IL_0101;
	}

IL_00ea:
	{
		t1965* L_22 = (__this->f5);
		int32_t L_23 = (((t1248 *)(t1248 *)SZArrayLdElema(L_22, V_2))->f1);
		__this->f9 = L_23;
	}

IL_0101:
	{
		t1965* L_24 = (__this->f5);
		t841* L_25 = (__this->f4);
		int32_t L_26 = V_1;
		((t1248 *)(t1248 *)SZArrayLdElema(L_24, V_2))->f1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_25, L_26))-1));
		t841* L_27 = (__this->f4);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_27, V_1)) = (int32_t)((int32_t)(V_2+1));
		t1965* L_28 = (__this->f5);
		((t1248 *)(t1248 *)SZArrayLdElema(L_28, V_2))->f0 = V_0;
		t446* L_29 = (__this->f6);
		*((t7**)(t7**)SZArrayLdElema(L_29, V_2)) = (t7*)p0;
		goto IL_0194;
	}

IL_0148:
	{
		if ((((int32_t)V_3) == ((int32_t)(-1))))
		{
			goto IL_0194;
		}
	}
	{
		t1965* L_30 = (__this->f5);
		t1965* L_31 = (__this->f5);
		int32_t L_32 = (((t1248 *)(t1248 *)SZArrayLdElema(L_31, V_2))->f1);
		((t1248 *)(t1248 *)SZArrayLdElema(L_30, V_3))->f1 = L_32;
		t1965* L_33 = (__this->f5);
		t841* L_34 = (__this->f4);
		int32_t L_35 = V_1;
		((t1248 *)(t1248 *)SZArrayLdElema(L_33, V_2))->f1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_34, L_35))-1));
		t841* L_36 = (__this->f4);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_36, V_1)) = (int32_t)((int32_t)(V_2+1));
	}

IL_0194:
	{
		t841* L_37 = (__this->f7);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_37, V_2)) = (int32_t)p1;
		int32_t L_38 = (__this->f14);
		__this->f14 = ((int32_t)(L_38+1));
		return;
	}
}
 void m15927 (t485 * __this, int32_t p0, t29* p1, MethodInfo* method){
	t29* V_0 = {0};
	t485 * G_B4_0 = {0};
	t485 * G_B3_0 = {0};
	t29* G_B5_0 = {0};
	t485 * G_B5_1 = {0};
	{
		if ((((int32_t)p0) >= ((int32_t)0)))
		{
			goto IL_000f;
		}
	}
	{
		t915 * L_0 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_0, (t7*) &_stringLiteral1163, &m3975_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000f:
	{
		G_B3_0 = ((t485 *)(__this));
		if (!p1)
		{
			G_B4_0 = ((t485 *)(__this));
			goto IL_0018;
		}
	}
	{
		V_0 = p1;
		G_B5_0 = V_0;
		G_B5_1 = ((t485 *)(G_B3_0));
		goto IL_001d;
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t2901_TI));
		t2901 * L_1 = m15881(NULL, &m15881_MI);
		G_B5_0 = ((t29*)(L_1));
		G_B5_1 = ((t485 *)(G_B4_0));
	}

IL_001d:
	{
		G_B5_1->f12 = G_B5_0;
		if (p0)
		{
			goto IL_002b;
		}
	}
	{
		p0 = ((int32_t)10);
	}

IL_002b:
	{
		p0 = ((int32_t)((((int32_t)((float)((float)(((float)p0))/(float)(0.9f)))))+1));
		m15928(__this, p0, &m15928_MI);
		__this->f14 = 0;
		return;
	}
}
 void m15928 (t485 * __this, int32_t p0, MethodInfo* method){
	{
		__this->f4 = ((t841*)SZArrayNew(InitializedTypeInfo(&t841_TI), p0));
		__this->f5 = ((t1965*)SZArrayNew(InitializedTypeInfo(&t1965_TI), p0));
		__this->f9 = (-1);
		__this->f6 = ((t446*)SZArrayNew(InitializedTypeInfo(&t446_TI), p0));
		__this->f7 = ((t841*)SZArrayNew(InitializedTypeInfo(&t841_TI), p0));
		__this->f8 = 0;
		t841* L_0 = (__this->f4);
		__this->f11 = (((int32_t)((float)((float)(((float)(((int32_t)(((t20 *)L_0)->max_length)))))*(float)(0.9f)))));
		int32_t L_1 = (__this->f11);
		if (L_1)
		{
			goto IL_006e;
		}
	}
	{
		t841* L_2 = (__this->f4);
		if ((((int32_t)(((int32_t)(((t20 *)L_2)->max_length)))) <= ((int32_t)0)))
		{
			goto IL_006e;
		}
	}
	{
		__this->f11 = 1;
	}

IL_006e:
	{
		return;
	}
}
 void m15929 (t485 * __this, t20 * p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_001d:
	{
		int32_t L_2 = m3969(p0, &m3969_MI);
		if ((((int32_t)p1) <= ((int32_t)L_2)))
		{
			goto IL_0031;
		}
	}
	{
		t305 * L_3 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_3, (t7*) &_stringLiteral1164, &m1935_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0031:
	{
		int32_t L_4 = m3969(p0, &m3969_MI);
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m15924_MI, __this);
		if ((((int32_t)((int32_t)(L_4-p1))) >= ((int32_t)L_5)))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_6 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_6, (t7*) &_stringLiteral1165, &m1935_MI);
		il2cpp_codegen_raise_exception(L_6);
	}

IL_004c:
	{
		return;
	}
}
 t2911  m15930 (t29 * __this, t7* p0, int32_t p1, MethodInfo* method){
	{
		t2911  L_0 = {0};
		m15946(&L_0, p0, p1, &m15946_MI);
		return L_0;
	}
}
extern MethodInfo m15931_MI;
 int32_t m15931 (t29 * __this, t7* p0, int32_t p1, MethodInfo* method){
	{
		return p1;
	}
}
 void m15932 (t485 * __this, t2910* p0, int32_t p1, MethodInfo* method){
	{
		m15929(__this, (t20 *)(t20 *)p0, p1, &m15929_MI);
		t35 L_0 = { &m15930_MI };
		t2917 * L_1 = (t2917 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2917_TI));
		m15996(L_1, NULL, L_0, &m15996_MI);
		m22242(__this, p0, p1, L_1, &m22242_MI);
		return;
	}
}
 void m15933 (t485 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	t841* V_1 = {0};
	t1965* V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	t446* V_7 = {0};
	t841* V_8 = {0};
	int32_t V_9 = 0;
	{
		t841* L_0 = (__this->f4);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t719_TI));
		int32_t L_1 = m6736(NULL, ((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)(((t20 *)L_0)->max_length)))<<(int32_t)1))|(int32_t)1)), &m6736_MI);
		V_0 = L_1;
		V_1 = ((t841*)SZArrayNew(InitializedTypeInfo(&t841_TI), V_0));
		V_2 = ((t1965*)SZArrayNew(InitializedTypeInfo(&t1965_TI), V_0));
		V_3 = 0;
		goto IL_00ab;
	}

IL_0027:
	{
		t841* L_2 = (__this->f4);
		int32_t L_3 = V_3;
		V_4 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_2, L_3))-1));
		goto IL_00a2;
	}

IL_0035:
	{
		t29* L_4 = (__this->f12);
		t446* L_5 = (__this->f6);
		int32_t L_6 = V_4;
		int32_t L_7 = (int32_t)InterfaceFuncInvoker1< int32_t, t7* >::Invoke(&m29233_MI, L_4, (*(t7**)(t7**)SZArrayLdElema(L_5, L_6)));
		int32_t L_8 = ((int32_t)((int32_t)L_7|(int32_t)((int32_t)-2147483648)));
		V_9 = L_8;
		((t1248 *)(t1248 *)SZArrayLdElema(V_2, V_4))->f0 = L_8;
		V_5 = V_9;
		V_6 = ((int32_t)(((int32_t)((int32_t)V_5&(int32_t)((int32_t)2147483647)))%V_0));
		int32_t L_9 = V_6;
		((t1248 *)(t1248 *)SZArrayLdElema(V_2, V_4))->f1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(V_1, L_9))-1));
		*((int32_t*)(int32_t*)SZArrayLdElema(V_1, V_6)) = (int32_t)((int32_t)(V_4+1));
		t1965* L_10 = (__this->f5);
		int32_t L_11 = (((t1248 *)(t1248 *)SZArrayLdElema(L_10, V_4))->f1);
		V_4 = L_11;
	}

IL_00a2:
	{
		if ((((uint32_t)V_4) != ((uint32_t)(-1))))
		{
			goto IL_0035;
		}
	}
	{
		V_3 = ((int32_t)(V_3+1));
	}

IL_00ab:
	{
		t841* L_12 = (__this->f4);
		if ((((int32_t)V_3) < ((int32_t)(((int32_t)(((t20 *)L_12)->max_length))))))
		{
			goto IL_0027;
		}
	}
	{
		__this->f4 = V_1;
		__this->f5 = V_2;
		V_7 = ((t446*)SZArrayNew(InitializedTypeInfo(&t446_TI), V_0));
		V_8 = ((t841*)SZArrayNew(InitializedTypeInfo(&t841_TI), V_0));
		t446* L_13 = (__this->f6);
		int32_t L_14 = (__this->f8);
		m5952(NULL, (t20 *)(t20 *)L_13, 0, (t20 *)(t20 *)V_7, 0, L_14, &m5952_MI);
		t841* L_15 = (__this->f7);
		int32_t L_16 = (__this->f8);
		m5952(NULL, (t20 *)(t20 *)L_15, 0, (t20 *)(t20 *)V_8, 0, L_16, &m5952_MI);
		__this->f6 = V_7;
		__this->f7 = V_8;
		__this->f11 = (((int32_t)((float)((float)(((float)V_0))*(float)(0.9f)))));
		return;
	}
}
 void m4039 (t485 * __this, t7* p0, int32_t p1, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		t7* L_0 = p0;
		if (((t7*)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t7* >::Invoke(&m29233_MI, L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_4)->max_length)))));
		t841* L_5 = (__this->f4);
		int32_t L_6 = V_1;
		V_2 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_6))-1));
		goto IL_008f;
	}

IL_0044:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_2))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_007d;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t446* L_10 = (__this->f6);
		int32_t L_11 = V_2;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t7*, t7* >::Invoke(&m29234_MI, L_9, (*(t7**)(t7**)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_007d;
		}
	}
	{
		t305 * L_13 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_13, (t7*) &_stringLiteral1167, &m1935_MI);
		il2cpp_codegen_raise_exception(L_13);
	}

IL_007d:
	{
		t1965* L_14 = (__this->f5);
		int32_t L_15 = (((t1248 *)(t1248 *)SZArrayLdElema(L_14, V_2))->f1);
		V_2 = L_15;
	}

IL_008f:
	{
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0044;
		}
	}
	{
		int32_t L_16 = (__this->f10);
		int32_t L_17 = ((int32_t)(L_16+1));
		V_3 = L_17;
		__this->f10 = L_17;
		int32_t L_18 = (__this->f11);
		if ((((int32_t)V_3) <= ((int32_t)L_18)))
		{
			goto IL_00c3;
		}
	}
	{
		m15933(__this, &m15933_MI);
		t841* L_19 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_19)->max_length)))));
	}

IL_00c3:
	{
		int32_t L_20 = (__this->f9);
		V_2 = L_20;
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_00e2;
		}
	}
	{
		int32_t L_21 = (__this->f8);
		int32_t L_22 = L_21;
		V_3 = L_22;
		__this->f8 = ((int32_t)(L_22+1));
		V_2 = V_3;
		goto IL_00f9;
	}

IL_00e2:
	{
		t1965* L_23 = (__this->f5);
		int32_t L_24 = (((t1248 *)(t1248 *)SZArrayLdElema(L_23, V_2))->f1);
		__this->f9 = L_24;
	}

IL_00f9:
	{
		t1965* L_25 = (__this->f5);
		((t1248 *)(t1248 *)SZArrayLdElema(L_25, V_2))->f0 = V_0;
		t1965* L_26 = (__this->f5);
		t841* L_27 = (__this->f4);
		int32_t L_28 = V_1;
		((t1248 *)(t1248 *)SZArrayLdElema(L_26, V_2))->f1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_27, L_28))-1));
		t841* L_29 = (__this->f4);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_29, V_1)) = (int32_t)((int32_t)(V_2+1));
		t446* L_30 = (__this->f6);
		*((t7**)(t7**)SZArrayLdElema(L_30, V_2)) = (t7*)p0;
		t841* L_31 = (__this->f7);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_31, V_2)) = (int32_t)p1;
		int32_t L_32 = (__this->f14);
		__this->f14 = ((int32_t)(L_32+1));
		return;
	}
}
extern MethodInfo m15934_MI;
 void m15934 (t485 * __this, MethodInfo* method){
	{
		__this->f10 = 0;
		t841* L_0 = (__this->f4);
		t841* L_1 = (__this->f4);
		m5112(NULL, (t20 *)(t20 *)L_0, 0, (((int32_t)(((t20 *)L_1)->max_length))), &m5112_MI);
		t446* L_2 = (__this->f6);
		t446* L_3 = (__this->f6);
		m5112(NULL, (t20 *)(t20 *)L_2, 0, (((int32_t)(((t20 *)L_3)->max_length))), &m5112_MI);
		t841* L_4 = (__this->f7);
		t841* L_5 = (__this->f7);
		m5112(NULL, (t20 *)(t20 *)L_4, 0, (((int32_t)(((t20 *)L_5)->max_length))), &m5112_MI);
		t1965* L_6 = (__this->f5);
		t1965* L_7 = (__this->f5);
		m5112(NULL, (t20 *)(t20 *)L_6, 0, (((int32_t)(((t20 *)L_7)->max_length))), &m5112_MI);
		__this->f9 = (-1);
		__this->f8 = 0;
		int32_t L_8 = (__this->f14);
		__this->f14 = ((int32_t)(L_8+1));
		return;
	}
}
 bool m15935 (t485 * __this, t7* p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		t7* L_0 = p0;
		if (((t7*)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t7* >::Invoke(&m29233_MI, L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		t841* L_5 = (__this->f4);
		int32_t L_6 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_5)->max_length)))));
		V_1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_4, L_6))-1));
		goto IL_0084;
	}

IL_0042:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_1))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_0072;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t446* L_10 = (__this->f6);
		int32_t L_11 = V_1;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t7*, t7* >::Invoke(&m29234_MI, L_9, (*(t7**)(t7**)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_0072;
		}
	}
	{
		return 1;
	}

IL_0072:
	{
		t1965* L_13 = (__this->f5);
		int32_t L_14 = (((t1248 *)(t1248 *)SZArrayLdElema(L_13, V_1))->f1);
		V_1 = L_14;
	}

IL_0084:
	{
		if ((((uint32_t)V_1) != ((uint32_t)(-1))))
		{
			goto IL_0042;
		}
	}
	{
		return 0;
	}
}
extern MethodInfo m15936_MI;
 bool m15936 (t485 * __this, int32_t p0, MethodInfo* method){
	t29* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t2361_TI));
		t2361 * L_0 = m12170(NULL, &m12170_MI);
		V_0 = L_0;
		V_1 = 0;
		goto IL_0048;
	}

IL_000a:
	{
		t841* L_1 = (__this->f4);
		int32_t L_2 = V_1;
		V_2 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_1, L_2))-1));
		goto IL_0040;
	}

IL_0017:
	{
		t841* L_3 = (__this->f7);
		int32_t L_4 = V_2;
		bool L_5 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(&m27290_MI, V_0, (*(int32_t*)(int32_t*)SZArrayLdElema(L_3, L_4)), p0);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		return 1;
	}

IL_002e:
	{
		t1965* L_6 = (__this->f5);
		int32_t L_7 = (((t1248 *)(t1248 *)SZArrayLdElema(L_6, V_2))->f1);
		V_2 = L_7;
	}

IL_0040:
	{
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0017;
		}
	}
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0048:
	{
		t841* L_8 = (__this->f4);
		if ((((int32_t)V_1) < ((int32_t)(((int32_t)(((t20 *)L_8)->max_length))))))
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}
}
extern MethodInfo m15937_MI;
 void m15937 (t485 * __this, t733 * p0, t735  p1, MethodInfo* method){
	t2910* V_0 = {0};
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral206, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = (__this->f14);
		m3984(p0, (t7*) &_stringLiteral208, L_1, &m3984_MI);
		t29* L_2 = (__this->f12);
		m3997(p0, (t7*) &_stringLiteral210, L_2, &m3997_MI);
		V_0 = (t2910*)NULL;
		int32_t L_3 = (__this->f10);
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_4 = (__this->f10);
		V_0 = ((t2910*)SZArrayNew(InitializedTypeInfo(&t2910_TI), L_4));
		m15932(__this, V_0, 0, &m15932_MI);
	}

IL_004f:
	{
		t841* L_5 = (__this->f4);
		m3984(p0, (t7*) &_stringLiteral1168, (((int32_t)(((t20 *)L_5)->max_length))), &m3984_MI);
		m3997(p0, (t7*) &_stringLiteral1169, (t29 *)(t29 *)V_0, &m3997_MI);
		return;
	}
}
extern MethodInfo m15938_MI;
 void m15938 (t485 * __this, t29 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	t2910* V_1 = {0};
	int32_t V_2 = 0;
	{
		t733 * L_0 = (__this->f13);
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		return;
	}

IL_0009:
	{
		t733 * L_1 = (__this->f13);
		int32_t L_2 = m3996(L_1, (t7*) &_stringLiteral208, &m3996_MI);
		__this->f14 = L_2;
		t733 * L_3 = (__this->f13);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_4 = m1554(NULL, LoadTypeToken(&t2096_0_0_0), &m1554_MI);
		t29 * L_5 = m3985(L_3, (t7*) &_stringLiteral210, L_4, &m3985_MI);
		__this->f12 = ((t29*)Castclass(L_5, InitializedTypeInfo(&t2096_TI)));
		t733 * L_6 = (__this->f13);
		int32_t L_7 = m3996(L_6, (t7*) &_stringLiteral1168, &m3996_MI);
		V_0 = L_7;
		t733 * L_8 = (__this->f13);
		t42 * L_9 = m1554(NULL, LoadTypeToken(&t2910_0_0_0), &m1554_MI);
		t29 * L_10 = m3985(L_8, (t7*) &_stringLiteral1169, L_9, &m3985_MI);
		V_1 = ((t2910*)Castclass(L_10, InitializedTypeInfo(&t2910_TI)));
		if ((((int32_t)V_0) >= ((int32_t)((int32_t)10))))
		{
			goto IL_007d;
		}
	}
	{
		V_0 = ((int32_t)10);
	}

IL_007d:
	{
		m15928(__this, V_0, &m15928_MI);
		__this->f10 = 0;
		if (!V_1)
		{
			goto IL_00ba;
		}
	}
	{
		V_2 = 0;
		goto IL_00b4;
	}

IL_0092:
	{
		t7* L_11 = m15947(((t2911 *)(t2911 *)SZArrayLdElema(V_1, V_2)), &m15947_MI);
		int32_t L_12 = m15949(((t2911 *)(t2911 *)SZArrayLdElema(V_1, V_2)), &m15949_MI);
		VirtActionInvoker2< t7*, int32_t >::Invoke(&m4039_MI, __this, L_11, L_12);
		V_2 = ((int32_t)(V_2+1));
	}

IL_00b4:
	{
		if ((((int32_t)V_2) < ((int32_t)(((int32_t)(((t20 *)V_1)->max_length))))))
		{
			goto IL_0092;
		}
	}

IL_00ba:
	{
		int32_t L_13 = (__this->f14);
		__this->f14 = ((int32_t)(L_13+1));
		__this->f13 = (t733 *)NULL;
		return;
	}
}
 bool m15939 (t485 * __this, t7* p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	t7* V_4 = {0};
	int32_t V_5 = 0;
	{
		t7* L_0 = p0;
		if (((t7*)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t7* >::Invoke(&m29233_MI, L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_4)->max_length)))));
		t841* L_5 = (__this->f4);
		int32_t L_6 = V_1;
		V_2 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_6))-1));
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0048;
		}
	}
	{
		return 0;
	}

IL_0048:
	{
		V_3 = (-1);
	}

IL_004a:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_2))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_007a;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t446* L_10 = (__this->f6);
		int32_t L_11 = V_2;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t7*, t7* >::Invoke(&m29234_MI, L_9, (*(t7**)(t7**)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_007a;
		}
	}
	{
		goto IL_0092;
	}

IL_007a:
	{
		V_3 = V_2;
		t1965* L_13 = (__this->f5);
		int32_t L_14 = (((t1248 *)(t1248 *)SZArrayLdElema(L_13, V_2))->f1);
		V_2 = L_14;
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_004a;
		}
	}

IL_0092:
	{
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0098;
		}
	}
	{
		return 0;
	}

IL_0098:
	{
		int32_t L_15 = (__this->f10);
		__this->f10 = ((int32_t)(L_15-1));
		if ((((uint32_t)V_3) != ((uint32_t)(-1))))
		{
			goto IL_00c7;
		}
	}
	{
		t841* L_16 = (__this->f4);
		t1965* L_17 = (__this->f5);
		int32_t L_18 = (((t1248 *)(t1248 *)SZArrayLdElema(L_17, V_2))->f1);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_16, V_1)) = (int32_t)((int32_t)(L_18+1));
		goto IL_00e9;
	}

IL_00c7:
	{
		t1965* L_19 = (__this->f5);
		t1965* L_20 = (__this->f5);
		int32_t L_21 = (((t1248 *)(t1248 *)SZArrayLdElema(L_20, V_2))->f1);
		((t1248 *)(t1248 *)SZArrayLdElema(L_19, V_3))->f1 = L_21;
	}

IL_00e9:
	{
		t1965* L_22 = (__this->f5);
		int32_t L_23 = (__this->f9);
		((t1248 *)(t1248 *)SZArrayLdElema(L_22, V_2))->f1 = L_23;
		__this->f9 = V_2;
		t1965* L_24 = (__this->f5);
		((t1248 *)(t1248 *)SZArrayLdElema(L_24, V_2))->f0 = 0;
		t446* L_25 = (__this->f6);
		Initobj (&t7_TI, (&V_4));
		*((t7**)(t7**)SZArrayLdElema(L_25, V_2)) = (t7*)V_4;
		t841* L_26 = (__this->f7);
		Initobj (&t44_TI, (&V_5));
		*((int32_t*)(int32_t*)SZArrayLdElema(L_26, V_2)) = (int32_t)V_5;
		int32_t L_27 = (__this->f14);
		__this->f14 = ((int32_t)(L_27+1));
		return 1;
	}
}
 bool m4040 (t485 * __this, t7* p0, int32_t* p1, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		t7* L_0 = p0;
		if (((t7*)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t7* >::Invoke(&m29233_MI, L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		t841* L_5 = (__this->f4);
		int32_t L_6 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_5)->max_length)))));
		V_1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_4, L_6))-1));
		goto IL_0096;
	}

IL_0042:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_1))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_0084;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t446* L_10 = (__this->f6);
		int32_t L_11 = V_1;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t7*, t7* >::Invoke(&m29234_MI, L_9, (*(t7**)(t7**)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_0084;
		}
	}
	{
		t841* L_13 = (__this->f7);
		int32_t L_14 = V_1;
		*p1 = (*(int32_t*)(int32_t*)SZArrayLdElema(L_13, L_14));
		return 1;
	}

IL_0084:
	{
		t1965* L_15 = (__this->f5);
		int32_t L_16 = (((t1248 *)(t1248 *)SZArrayLdElema(L_15, V_1))->f1);
		V_1 = L_16;
	}

IL_0096:
	{
		if ((((uint32_t)V_1) != ((uint32_t)(-1))))
		{
			goto IL_0042;
		}
	}
	{
		Initobj (&t44_TI, (&V_2));
		*p1 = V_2;
		return 0;
	}
}
extern MethodInfo m15940_MI;
 t2909 * m15940 (t485 * __this, MethodInfo* method){
	{
		t2909 * L_0 = (t2909 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2909_TI));
		m15957(L_0, __this, &m15957_MI);
		return L_0;
	}
}
 t7* m15941 (t485 * __this, t29 * p0, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		if (((t7*)IsInst(p0, (&t7_TI))))
		{
			goto IL_003a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_1 = m1554(NULL, LoadTypeToken(&t7_0_0_0), &m1554_MI);
		t7* L_2 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m6035_MI, L_1);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_3 = m66(NULL, (t7*) &_stringLiteral1170, L_2, &m66_MI);
		t305 * L_4 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m3973(L_4, L_3, (t7*) &_stringLiteral195, &m3973_MI);
		il2cpp_codegen_raise_exception(L_4);
	}

IL_003a:
	{
		return ((t7*)Castclass(p0, (&t7_TI)));
	}
}
 int32_t m15942 (t485 * __this, t29 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		if (p0)
		{
			goto IL_001e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_0 = m1554(NULL, LoadTypeToken(&t44_0_0_0), &m1554_MI);
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6004_MI, L_0);
		if (L_1)
		{
			goto IL_001e;
		}
	}
	{
		Initobj (&t44_TI, (&V_0));
		return V_0;
	}

IL_001e:
	{
		if (((t29 *)IsInst(p0, InitializedTypeInfo(&t44_TI))))
		{
			goto IL_004a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_2 = m1554(NULL, LoadTypeToken(&t44_0_0_0), &m1554_MI);
		t7* L_3 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m6035_MI, L_2);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_4 = m66(NULL, (t7*) &_stringLiteral1170, L_3, &m66_MI);
		t305 * L_5 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m3973(L_5, L_4, (t7*) &_stringLiteral401, &m3973_MI);
		il2cpp_codegen_raise_exception(L_5);
	}

IL_004a:
	{
		return ((*(int32_t*)((int32_t*)UnBox (p0, InitializedTypeInfo(&t44_TI)))));
	}
}
 bool m15943 (t485 * __this, t2911  p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		t7* L_0 = m15947((&p0), &m15947_MI);
		bool L_1 = (bool)VirtFuncInvoker2< bool, t7*, int32_t* >::Invoke(&m4040_MI, __this, L_0, (&V_0));
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		return 0;
	}

IL_0013:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t2361_TI));
		t2361 * L_2 = m12170(NULL, &m12170_MI);
		int32_t L_3 = m15949((&p0), &m15949_MI);
		bool L_4 = (bool)VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(&m27426_MI, L_2, L_3, V_0);
		return L_4;
	}
}
extern MethodInfo m15944_MI;
 t2913  m15944 (t485 * __this, MethodInfo* method){
	{
		t2913  L_0 = {0};
		m15976(&L_0, __this, &m15976_MI);
		return L_0;
	}
}
 t725  m15945 (t29 * __this, t7* p0, int32_t p1, MethodInfo* method){
	{
		t7* L_0 = p0;
		int32_t L_1 = p1;
		t29 * L_2 = Box(InitializedTypeInfo(&t44_TI), &L_1);
		t725  L_3 = {0};
		m3965(&L_3, ((t7*)L_0), L_2, &m3965_MI);
		return L_3;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2<System.String,System.Int32>
extern Il2CppType t44_0_0_32849;
FieldInfo t485_f0_FieldInfo = 
{
	"INITIAL_SIZE", &t44_0_0_32849, &t485_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_32849;
FieldInfo t485_f1_FieldInfo = 
{
	"DEFAULT_LOAD_FACTOR", &t22_0_0_32849, &t485_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_32849;
FieldInfo t485_f2_FieldInfo = 
{
	"NO_SLOT", &t44_0_0_32849, &t485_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_32849;
FieldInfo t485_f3_FieldInfo = 
{
	"HASH_FLAG", &t44_0_0_32849, &t485_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t841_0_0_1;
FieldInfo t485_f4_FieldInfo = 
{
	"table", &t841_0_0_1, &t485_TI, offsetof(t485, f4), &EmptyCustomAttributesCache};
extern Il2CppType t1965_0_0_1;
FieldInfo t485_f5_FieldInfo = 
{
	"linkSlots", &t1965_0_0_1, &t485_TI, offsetof(t485, f5), &EmptyCustomAttributesCache};
extern Il2CppType t446_0_0_1;
FieldInfo t485_f6_FieldInfo = 
{
	"keySlots", &t446_0_0_1, &t485_TI, offsetof(t485, f6), &EmptyCustomAttributesCache};
extern Il2CppType t841_0_0_1;
FieldInfo t485_f7_FieldInfo = 
{
	"valueSlots", &t841_0_0_1, &t485_TI, offsetof(t485, f7), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t485_f8_FieldInfo = 
{
	"touchedSlots", &t44_0_0_1, &t485_TI, offsetof(t485, f8), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t485_f9_FieldInfo = 
{
	"emptySlot", &t44_0_0_1, &t485_TI, offsetof(t485, f9), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t485_f10_FieldInfo = 
{
	"count", &t44_0_0_1, &t485_TI, offsetof(t485, f10), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t485_f11_FieldInfo = 
{
	"threshold", &t44_0_0_1, &t485_TI, offsetof(t485, f11), &EmptyCustomAttributesCache};
extern Il2CppType t2096_0_0_1;
FieldInfo t485_f12_FieldInfo = 
{
	"hcp", &t2096_0_0_1, &t485_TI, offsetof(t485, f12), &EmptyCustomAttributesCache};
extern Il2CppType t733_0_0_1;
FieldInfo t485_f13_FieldInfo = 
{
	"serialization_info", &t733_0_0_1, &t485_TI, offsetof(t485, f13), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t485_f14_FieldInfo = 
{
	"generation", &t44_0_0_1, &t485_TI, offsetof(t485, f14), &EmptyCustomAttributesCache};
extern Il2CppType t2908_0_0_17;
extern CustomAttributesCache t1254__CustomAttributeCache_U3CU3Ef__am$cacheB;
FieldInfo t485_f15_FieldInfo = 
{
	"<>f__am$cacheB", &t2908_0_0_17, &t485_TI, offsetof(t485_SFs, f15), &t1254__CustomAttributeCache_U3CU3Ef__am$cacheB};
static FieldInfo* t485_FIs[] =
{
	&t485_f0_FieldInfo,
	&t485_f1_FieldInfo,
	&t485_f2_FieldInfo,
	&t485_f3_FieldInfo,
	&t485_f4_FieldInfo,
	&t485_f5_FieldInfo,
	&t485_f6_FieldInfo,
	&t485_f7_FieldInfo,
	&t485_f8_FieldInfo,
	&t485_f9_FieldInfo,
	&t485_f10_FieldInfo,
	&t485_f11_FieldInfo,
	&t485_f12_FieldInfo,
	&t485_f13_FieldInfo,
	&t485_f14_FieldInfo,
	&t485_f15_FieldInfo,
	NULL
};
static const int32_t t485_f0_DefaultValueData = 10;
static Il2CppFieldDefaultValueEntry t485_f0_DefaultValue = 
{
	&t485_f0_FieldInfo, { (char*)&t485_f0_DefaultValueData, &t44_0_0_0 }};
static const float t485_f1_DefaultValueData = 0.9f;
static Il2CppFieldDefaultValueEntry t485_f1_DefaultValue = 
{
	&t485_f1_FieldInfo, { (char*)&t485_f1_DefaultValueData, &t22_0_0_0 }};
static const int32_t t485_f2_DefaultValueData = -1;
static Il2CppFieldDefaultValueEntry t485_f2_DefaultValue = 
{
	&t485_f2_FieldInfo, { (char*)&t485_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t485_f3_DefaultValueData = -2147483648;
static Il2CppFieldDefaultValueEntry t485_f3_DefaultValue = 
{
	&t485_f3_FieldInfo, { (char*)&t485_f3_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t485_FDVs[] = 
{
	&t485_f0_DefaultValue,
	&t485_f1_DefaultValue,
	&t485_f2_DefaultValue,
	&t485_f3_DefaultValue,
	NULL
};
static PropertyInfo t485____System_Collections_IDictionary_Item_PropertyInfo = 
{
	&t485_TI, "System.Collections.IDictionary.Item", &m15909_MI, &m15910_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t485____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t485_TI, "System.Collections.ICollection.IsSynchronized", &m15913_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t485____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t485_TI, "System.Collections.ICollection.SyncRoot", &m15914_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t485____System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_IsReadOnly_PropertyInfo = 
{
	&t485_TI, "System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.IsReadOnly", &m15915_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t485____Count_PropertyInfo = 
{
	&t485_TI, "Count", &m15924_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t485____Item_PropertyInfo = 
{
	&t485_TI, "Item", &m15925_MI, &m15926_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t485____Values_PropertyInfo = 
{
	&t485_TI, "Values", &m15940_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t485_PIs[] =
{
	&t485____System_Collections_IDictionary_Item_PropertyInfo,
	&t485____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t485____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t485____System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_IsReadOnly_PropertyInfo,
	&t485____Count_PropertyInfo,
	&t485____Item_PropertyInfo,
	&t485____Values_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15906_GM;
MethodInfo m15906_MI = 
{
	".ctor", (methodPointerType)&m15906, &t485_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15906_GM};
extern Il2CppType t2096_0_0_0;
static ParameterInfo t485_m15907_ParameterInfos[] = 
{
	{"comparer", 0, 134217728, &EmptyCustomAttributesCache, &t2096_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15907_GM;
MethodInfo m15907_MI = 
{
	".ctor", (methodPointerType)&m15907, &t485_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t485_m15907_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15907_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t485_m4038_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m4038_GM;
MethodInfo m4038_MI = 
{
	".ctor", (methodPointerType)&m4038, &t485_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t485_m4038_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m4038_GM};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t485_m15908_ParameterInfos[] = 
{
	{"info", 0, 134217728, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134217728, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15908_GM;
MethodInfo m15908_MI = 
{
	".ctor", (methodPointerType)&m15908, &t485_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t485_m15908_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15908_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t485_m15909_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15909_GM;
MethodInfo m15909_MI = 
{
	"System.Collections.IDictionary.get_Item", (methodPointerType)&m15909, &t485_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t485_m15909_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15909_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t485_m15910_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15910_GM;
MethodInfo m15910_MI = 
{
	"System.Collections.IDictionary.set_Item", (methodPointerType)&m15910, &t485_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t485_m15910_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 20, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15910_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t485_m15911_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15911_GM;
MethodInfo m15911_MI = 
{
	"System.Collections.IDictionary.Add", (methodPointerType)&m15911, &t485_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t485_m15911_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 21, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15911_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t485_m15912_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15912_GM;
MethodInfo m15912_MI = 
{
	"System.Collections.IDictionary.Remove", (methodPointerType)&m15912, &t485_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t485_m15912_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 23, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15912_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15913_GM;
MethodInfo m15913_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m15913, &t485_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15913_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15914_GM;
MethodInfo m15914_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m15914, &t485_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15914_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15915_GM;
MethodInfo m15915_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly", (methodPointerType)&m15915, &t485_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 11, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15915_GM};
extern Il2CppType t2911_0_0_0;
extern Il2CppType t2911_0_0_0;
static ParameterInfo t485_m15916_ParameterInfos[] = 
{
	{"keyValuePair", 0, 134217728, &EmptyCustomAttributesCache, &t2911_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t2911 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15916_GM;
MethodInfo m15916_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add", (methodPointerType)&m15916, &t485_TI, &t21_0_0_0, RuntimeInvoker_t21_t2911, t485_m15916_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15916_GM};
extern Il2CppType t2911_0_0_0;
static ParameterInfo t485_m15917_ParameterInfos[] = 
{
	{"keyValuePair", 0, 134217728, &EmptyCustomAttributesCache, &t2911_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2911 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15917_GM;
MethodInfo m15917_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains", (methodPointerType)&m15917, &t485_TI, &t40_0_0_0, RuntimeInvoker_t40_t2911, t485_m15917_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 14, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15917_GM};
extern Il2CppType t2910_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t485_m15918_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2910_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15918_GM;
MethodInfo m15918_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo", (methodPointerType)&m15918, &t485_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t485_m15918_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15918_GM};
extern Il2CppType t2911_0_0_0;
static ParameterInfo t485_m15919_ParameterInfos[] = 
{
	{"keyValuePair", 0, 134217728, &EmptyCustomAttributesCache, &t2911_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2911 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15919_GM;
MethodInfo m15919_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove", (methodPointerType)&m15919, &t485_TI, &t40_0_0_0, RuntimeInvoker_t40_t2911, t485_m15919_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15919_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t485_m15920_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15920_GM;
MethodInfo m15920_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m15920, &t485_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t485_m15920_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15920_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15921_GM;
MethodInfo m15921_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m15921, &t485_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15921_GM};
extern Il2CppType t2912_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15922_GM;
MethodInfo m15922_MI = 
{
	"System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator", (methodPointerType)&m15922, &t485_TI, &t2912_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 17, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15922_GM};
extern Il2CppType t722_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15923_GM;
MethodInfo m15923_MI = 
{
	"System.Collections.IDictionary.GetEnumerator", (methodPointerType)&m15923, &t485_TI, &t722_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 22, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15923_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15924_GM;
MethodInfo m15924_MI = 
{
	"get_Count", (methodPointerType)&m15924, &t485_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15924_GM};
extern Il2CppType t7_0_0_0;
static ParameterInfo t485_m15925_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15925_GM;
MethodInfo m15925_MI = 
{
	"get_Item", (methodPointerType)&m15925, &t485_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t485_m15925_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 25, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15925_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t485_m15926_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15926_GM;
MethodInfo m15926_MI = 
{
	"set_Item", (methodPointerType)&m15926, &t485_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t485_m15926_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 26, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15926_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2096_0_0_0;
static ParameterInfo t485_m15927_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"hcp", 1, 134217728, &EmptyCustomAttributesCache, &t2096_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15927_GM;
MethodInfo m15927_MI = 
{
	"Init", (methodPointerType)&m15927, &t485_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t485_m15927_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15927_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t485_m15928_ParameterInfos[] = 
{
	{"size", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15928_GM;
MethodInfo m15928_MI = 
{
	"InitArrays", (methodPointerType)&m15928, &t485_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t485_m15928_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15928_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t485_m15929_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15929_GM;
MethodInfo m15929_MI = 
{
	"CopyToCheck", (methodPointerType)&m15929, &t485_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t485_m15929_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15929_GM};
extern Il2CppType t1957_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t6728_0_0_0;
extern Il2CppType t6728_0_0_0;
static ParameterInfo t485_m29288_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1957_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"transform", 2, 134217728, &EmptyCustomAttributesCache, &t6728_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericContainer m29288_IGC;
extern TypeInfo m29288_gp_TRet_0_TI;
Il2CppGenericParamFull m29288_gp_TRet_0_TI_GenericParamFull = { { &m29288_IGC, 0}, {NULL, "TRet", 0, 0, NULL} };
extern TypeInfo m29288_gp_TElem_1_TI;
Il2CppGenericParamFull m29288_gp_TElem_1_TI_GenericParamFull = { { &m29288_IGC, 1}, {NULL, "TElem", 0, 0, NULL} };
static Il2CppGenericParamFull* m29288_IGPA[2] = 
{
	&m29288_gp_TRet_0_TI_GenericParamFull,
	&m29288_gp_TElem_1_TI_GenericParamFull,
};
extern MethodInfo m29288_MI;
Il2CppGenericContainer m29288_IGC = { { NULL, NULL }, NULL, &m29288_MI, 2, 1, m29288_IGPA };
extern Il2CppGenericMethod m29289_GM;
static Il2CppRGCTXDefinition m29288_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &m29289_GM }/* Method Definition */,
	{ IL2CPP_RGCTX_DATA_CLASS, &m9954_gp_0_0_0_0 }/* Class Definition */,
	{ IL2CPP_RGCTX_DATA_CLASS, &m9954_gp_1_0_0_0 }/* Class Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppGenericMethod m29288_GM;
MethodInfo m29288_MI = 
{
	"Do_CopyTo", NULL, &t485_TI, &t21_0_0_0, NULL, t485_m29288_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, true, true, 0, m29288_RGCTXData, (methodPointerType)NULL, &m29288_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t485_m15930_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2911_0_0_0;
extern void* RuntimeInvoker_t2911_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15930_GM;
MethodInfo m15930_MI = 
{
	"make_pair", (methodPointerType)&m15930, &t485_TI, &t2911_0_0_0, RuntimeInvoker_t2911_t29_t44, t485_m15930_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15930_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t485_m15931_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15931_GM;
MethodInfo m15931_MI = 
{
	"pick_value", (methodPointerType)&m15931, &t485_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t44, t485_m15931_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15931_GM};
extern Il2CppType t2910_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t485_m15932_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2910_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15932_GM;
MethodInfo m15932_MI = 
{
	"CopyTo", (methodPointerType)&m15932, &t485_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t485_m15932_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15932_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t6731_0_0_0;
extern Il2CppType t6731_0_0_0;
static ParameterInfo t485_m29290_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"transform", 2, 134217728, &EmptyCustomAttributesCache, &t6731_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericContainer m29290_IGC;
extern TypeInfo m29290_gp_TRet_0_TI;
Il2CppGenericParamFull m29290_gp_TRet_0_TI_GenericParamFull = { { &m29290_IGC, 0}, {NULL, "TRet", 0, 0, NULL} };
static Il2CppGenericParamFull* m29290_IGPA[1] = 
{
	&m29290_gp_TRet_0_TI_GenericParamFull,
};
extern MethodInfo m29290_MI;
Il2CppGenericContainer m29290_IGC = { { NULL, NULL }, NULL, &m29290_MI, 1, 1, m29290_IGPA };
extern Il2CppGenericMethod m29291_GM;
static Il2CppRGCTXDefinition m29290_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, &m9959_gp_0_0_0_0 }/* Type Definition */,
	{ IL2CPP_RGCTX_DATA_METHOD, &m29291_GM }/* Method Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppGenericMethod m29290_GM;
MethodInfo m29290_MI = 
{
	"Do_ICollectionCopyTo", NULL, &t485_TI, &t21_0_0_0, NULL, t485_m29290_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, true, true, 0, m29290_RGCTXData, (methodPointerType)NULL, &m29290_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15933_GM;
MethodInfo m15933_MI = 
{
	"Resize", (methodPointerType)&m15933, &t485_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15933_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t485_m4039_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m4039_GM;
MethodInfo m4039_MI = 
{
	"Add", (methodPointerType)&m4039, &t485_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t485_m4039_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 2, false, true, 0, NULL, (methodPointerType)NULL, &m4039_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15934_GM;
MethodInfo m15934_MI = 
{
	"Clear", (methodPointerType)&m15934, &t485_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 13, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15934_GM};
extern Il2CppType t7_0_0_0;
static ParameterInfo t485_m15935_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15935_GM;
MethodInfo m15935_MI = 
{
	"ContainsKey", (methodPointerType)&m15935, &t485_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t485_m15935_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15935_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t485_m15936_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15936_GM;
MethodInfo m15936_MI = 
{
	"ContainsValue", (methodPointerType)&m15936, &t485_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t485_m15936_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15936_GM};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t485_m15937_ParameterInfos[] = 
{
	{"info", 0, 134217728, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134217728, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15937_GM;
MethodInfo m15937_MI = 
{
	"GetObjectData", (methodPointerType)&m15937, &t485_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t485_m15937_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15937_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t485_m15938_ParameterInfos[] = 
{
	{"sender", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15938_GM;
MethodInfo m15938_MI = 
{
	"OnDeserialization", (methodPointerType)&m15938, &t485_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t485_m15938_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15938_GM};
extern Il2CppType t7_0_0_0;
static ParameterInfo t485_m15939_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15939_GM;
MethodInfo m15939_MI = 
{
	"Remove", (methodPointerType)&m15939, &t485_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t485_m15939_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15939_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t44_1_0_2;
extern Il2CppType t44_1_0_0;
static ParameterInfo t485_m4040_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_1_0_2},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t390 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m4040_GM;
MethodInfo m4040_MI = 
{
	"TryGetValue", (methodPointerType)&m4040, &t485_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t390, t485_m4040_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m4040_GM};
extern Il2CppType t2909_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15940_GM;
MethodInfo m15940_MI = 
{
	"get_Values", (methodPointerType)&m15940, &t485_TI, &t2909_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15940_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t485_m15941_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15941_GM;
MethodInfo m15941_MI = 
{
	"ToTKey", (methodPointerType)&m15941, &t485_TI, &t7_0_0_0, RuntimeInvoker_t29_t29, t485_m15941_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15941_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t485_m15942_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15942_GM;
MethodInfo m15942_MI = 
{
	"ToTValue", (methodPointerType)&m15942, &t485_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t485_m15942_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15942_GM};
extern Il2CppType t2911_0_0_0;
static ParameterInfo t485_m15943_ParameterInfos[] = 
{
	{"pair", 0, 134217728, &EmptyCustomAttributesCache, &t2911_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2911 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15943_GM;
MethodInfo m15943_MI = 
{
	"ContainsKeyValuePair", (methodPointerType)&m15943, &t485_TI, &t40_0_0_0, RuntimeInvoker_t40_t2911, t485_m15943_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15943_GM};
extern Il2CppType t2913_0_0_0;
extern void* RuntimeInvoker_t2913 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15944_GM;
MethodInfo m15944_MI = 
{
	"GetEnumerator", (methodPointerType)&m15944, &t485_TI, &t2913_0_0_0, RuntimeInvoker_t2913, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15944_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t485_m15945_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725_t29_t44 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1254__CustomAttributeCache_m9975;
extern Il2CppGenericMethod m15945_GM;
MethodInfo m15945_MI = 
{
	"<CopyTo>m__0", (methodPointerType)&m15945, &t485_TI, &t725_0_0_0, RuntimeInvoker_t725_t29_t44, t485_m15945_ParameterInfos, &t1254__CustomAttributeCache_m9975, 145, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15945_GM};
static MethodInfo* t485_MIs[] =
{
	&m15906_MI,
	&m15907_MI,
	&m4038_MI,
	&m15908_MI,
	&m15909_MI,
	&m15910_MI,
	&m15911_MI,
	&m15912_MI,
	&m15913_MI,
	&m15914_MI,
	&m15915_MI,
	&m15916_MI,
	&m15917_MI,
	&m15918_MI,
	&m15919_MI,
	&m15920_MI,
	&m15921_MI,
	&m15922_MI,
	&m15923_MI,
	&m15924_MI,
	&m15925_MI,
	&m15926_MI,
	&m15927_MI,
	&m15928_MI,
	&m15929_MI,
	&m29288_MI,
	&m15930_MI,
	&m15931_MI,
	&m15932_MI,
	&m29290_MI,
	&m15933_MI,
	&m4039_MI,
	&m15934_MI,
	&m15935_MI,
	&m15936_MI,
	&m15937_MI,
	&m15938_MI,
	&m15939_MI,
	&m4040_MI,
	&m15940_MI,
	&m15941_MI,
	&m15942_MI,
	&m15943_MI,
	&m15944_MI,
	&m15945_MI,
	NULL
};
static MethodInfo* t485_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15921_MI,
	&m15937_MI,
	&m15924_MI,
	&m15913_MI,
	&m15914_MI,
	&m15920_MI,
	&m15924_MI,
	&m15915_MI,
	&m15916_MI,
	&m15934_MI,
	&m15917_MI,
	&m15918_MI,
	&m15919_MI,
	&m15922_MI,
	&m15939_MI,
	&m15909_MI,
	&m15910_MI,
	&m15911_MI,
	&m15923_MI,
	&m15912_MI,
	&m15938_MI,
	&m15925_MI,
	&m15926_MI,
	&m4039_MI,
	&m15935_MI,
	&m15937_MI,
	&m15938_MI,
	&m4040_MI,
};
extern TypeInfo t5595_TI;
extern TypeInfo t5597_TI;
extern TypeInfo t6733_TI;
static TypeInfo* t485_ITIs[] = 
{
	&t603_TI,
	&t374_TI,
	&t674_TI,
	&t5595_TI,
	&t5597_TI,
	&t6733_TI,
	&t721_TI,
	&t918_TI,
};
static Il2CppInterfaceOffsetPair t485_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t374_TI, 5},
	{ &t674_TI, 6},
	{ &t5595_TI, 10},
	{ &t5597_TI, 17},
	{ &t6733_TI, 18},
	{ &t721_TI, 19},
	{ &t918_TI, 24},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t485_0_0_0;
extern Il2CppType t485_1_0_0;
struct t485;
extern Il2CppGenericClass t485_GC;
extern CustomAttributesCache t1254__CustomAttributeCache;
extern CustomAttributesCache t1254__CustomAttributeCache_U3CU3Ef__am$cacheB;
extern CustomAttributesCache t1254__CustomAttributeCache_m9975;
TypeInfo t485_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Dictionary`2", "System.Collections.Generic", t485_MIs, t485_PIs, t485_FIs, NULL, &t29_TI, NULL, NULL, &t485_TI, t485_ITIs, t485_VT, &t1254__CustomAttributeCache, &t485_TI, &t485_0_0_0, &t485_1_0_0, t485_IOs, &t485_GC, NULL, t485_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t485), 0, -1, sizeof(t485_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 45, 7, 16, 0, 0, 32, 8, 8};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>>
extern MethodInfo m29292_MI;
static PropertyInfo t5595____Count_PropertyInfo = 
{
	&t5595_TI, "Count", &m29292_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29293_MI;
static PropertyInfo t5595____IsReadOnly_PropertyInfo = 
{
	&t5595_TI, "IsReadOnly", &m29293_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5595_PIs[] =
{
	&t5595____Count_PropertyInfo,
	&t5595____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29292_GM;
MethodInfo m29292_MI = 
{
	"get_Count", NULL, &t5595_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29292_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29293_GM;
MethodInfo m29293_MI = 
{
	"get_IsReadOnly", NULL, &t5595_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29293_GM};
extern Il2CppType t2911_0_0_0;
static ParameterInfo t5595_m29294_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2911_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t2911 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29294_GM;
MethodInfo m29294_MI = 
{
	"Add", NULL, &t5595_TI, &t21_0_0_0, RuntimeInvoker_t21_t2911, t5595_m29294_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29294_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29295_GM;
MethodInfo m29295_MI = 
{
	"Clear", NULL, &t5595_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29295_GM};
extern Il2CppType t2911_0_0_0;
static ParameterInfo t5595_m29296_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2911_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2911 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29296_GM;
MethodInfo m29296_MI = 
{
	"Contains", NULL, &t5595_TI, &t40_0_0_0, RuntimeInvoker_t40_t2911, t5595_m29296_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29296_GM};
extern Il2CppType t2910_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5595_m29297_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2910_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29297_GM;
MethodInfo m29297_MI = 
{
	"CopyTo", NULL, &t5595_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5595_m29297_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29297_GM};
extern Il2CppType t2911_0_0_0;
static ParameterInfo t5595_m29298_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2911_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2911 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29298_GM;
MethodInfo m29298_MI = 
{
	"Remove", NULL, &t5595_TI, &t40_0_0_0, RuntimeInvoker_t40_t2911, t5595_m29298_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29298_GM};
static MethodInfo* t5595_MIs[] =
{
	&m29292_MI,
	&m29293_MI,
	&m29294_MI,
	&m29295_MI,
	&m29296_MI,
	&m29297_MI,
	&m29298_MI,
	NULL
};
static TypeInfo* t5595_ITIs[] = 
{
	&t603_TI,
	&t5597_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5595_0_0_0;
extern Il2CppType t5595_1_0_0;
struct t5595;
extern Il2CppGenericClass t5595_GC;
TypeInfo t5595_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5595_MIs, t5595_PIs, NULL, NULL, NULL, NULL, NULL, &t5595_TI, t5595_ITIs, NULL, &EmptyCustomAttributesCache, &t5595_TI, &t5595_0_0_0, &t5595_1_0_0, NULL, &t5595_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>>
extern Il2CppType t2912_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29299_GM;
MethodInfo m29299_MI = 
{
	"GetEnumerator", NULL, &t5597_TI, &t2912_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29299_GM};
static MethodInfo* t5597_MIs[] =
{
	&m29299_MI,
	NULL
};
static TypeInfo* t5597_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5597_0_0_0;
extern Il2CppType t5597_1_0_0;
struct t5597;
extern Il2CppGenericClass t5597_GC;
TypeInfo t5597_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5597_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5597_TI, t5597_ITIs, NULL, &EmptyCustomAttributesCache, &t5597_TI, &t5597_0_0_0, &t5597_1_0_0, NULL, &t5597_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2912_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>>
extern MethodInfo m29300_MI;
static PropertyInfo t2912____Current_PropertyInfo = 
{
	&t2912_TI, "Current", &m29300_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2912_PIs[] =
{
	&t2912____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2911_0_0_0;
extern void* RuntimeInvoker_t2911 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29300_GM;
MethodInfo m29300_MI = 
{
	"get_Current", NULL, &t2912_TI, &t2911_0_0_0, RuntimeInvoker_t2911, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29300_GM};
static MethodInfo* t2912_MIs[] =
{
	&m29300_MI,
	NULL
};
static TypeInfo* t2912_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2912_0_0_0;
extern Il2CppType t2912_1_0_0;
struct t2912;
extern Il2CppGenericClass t2912_GC;
TypeInfo t2912_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2912_MIs, t2912_PIs, NULL, NULL, NULL, NULL, NULL, &t2912_TI, t2912_ITIs, NULL, &EmptyCustomAttributesCache, &t2912_TI, &t2912_0_0_0, &t2912_1_0_0, NULL, &t2912_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m15948_MI;
extern MethodInfo m15950_MI;


 void m15946 (t2911 * __this, t7* p0, int32_t p1, MethodInfo* method){
	{
		m15948(__this, p0, &m15948_MI);
		m15950(__this, p1, &m15950_MI);
		return;
	}
}
 t7* m15947 (t2911 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f0);
		return L_0;
	}
}
 void m15948 (t2911 * __this, t7* p0, MethodInfo* method){
	{
		__this->f0 = p0;
		return;
	}
}
 int32_t m15949 (t2911 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		return L_0;
	}
}
 void m15950 (t2911 * __this, int32_t p0, MethodInfo* method){
	{
		__this->f1 = p0;
		return;
	}
}
extern MethodInfo m15951_MI;
 t7* m15951 (t2911 * __this, MethodInfo* method){
	t7* V_0 = {0};
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	t446* G_B2_1 = {0};
	t446* G_B2_2 = {0};
	int32_t G_B1_0 = 0;
	t446* G_B1_1 = {0};
	t446* G_B1_2 = {0};
	t7* G_B3_0 = {0};
	int32_t G_B3_1 = 0;
	t446* G_B3_2 = {0};
	t446* G_B3_3 = {0};
	int32_t G_B5_0 = 0;
	t446* G_B5_1 = {0};
	t446* G_B5_2 = {0};
	int32_t G_B4_0 = 0;
	t446* G_B4_1 = {0};
	t446* G_B4_2 = {0};
	t7* G_B6_0 = {0};
	int32_t G_B6_1 = 0;
	t446* G_B6_2 = {0};
	t446* G_B6_3 = {0};
	{
		t446* L_0 = ((t446*)SZArrayNew(InitializedTypeInfo(&t446_TI), 5));
		ArrayElementTypeCheck (L_0, (t7*) &_stringLiteral175);
		*((t7**)(t7**)SZArrayLdElema(L_0, 0)) = (t7*)(t7*) &_stringLiteral175;
		t446* L_1 = L_0;
		t7* L_2 = m15947(__this, &m15947_MI);
		t7* L_3 = L_2;
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!((t7*)L_3))
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0033;
		}
	}
	{
		t7* L_4 = m15947(__this, &m15947_MI);
		V_0 = L_4;
		t7* L_5 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1332_MI, (*(&V_0)));
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_0038;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		G_B3_0 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_0038:
	{
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		*((t7**)(t7**)SZArrayLdElema(G_B3_2, G_B3_1)) = (t7*)G_B3_0;
		t446* L_6 = G_B3_3;
		ArrayElementTypeCheck (L_6, (t7*) &_stringLiteral184);
		*((t7**)(t7**)SZArrayLdElema(L_6, 2)) = (t7*)(t7*) &_stringLiteral184;
		t446* L_7 = L_6;
		int32_t L_8 = m15949(__this, &m15949_MI);
		int32_t L_9 = L_8;
		t29 * L_10 = Box(InitializedTypeInfo(&t44_TI), &L_9);
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_10)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0066;
		}
	}
	{
		int32_t L_11 = m15949(__this, &m15949_MI);
		V_1 = L_11;
		t7* L_12 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1332_MI, Box(InitializedTypeInfo(&t44_TI), &(*(&V_1))));
		G_B6_0 = L_12;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_006b;
	}

IL_0066:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		G_B6_0 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_006b:
	{
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		*((t7**)(t7**)SZArrayLdElema(G_B6_2, G_B6_1)) = (t7*)G_B6_0;
		t446* L_13 = G_B6_3;
		ArrayElementTypeCheck (L_13, (t7*) &_stringLiteral176);
		*((t7**)(t7**)SZArrayLdElema(L_13, 4)) = (t7*)(t7*) &_stringLiteral176;
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_14 = m4008(NULL, L_13, &m4008_MI);
		return L_14;
	}
}
// Metadata Definition System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>
extern Il2CppType t7_0_0_1;
FieldInfo t2911_f0_FieldInfo = 
{
	"key", &t7_0_0_1, &t2911_TI, offsetof(t2911, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2911_f1_FieldInfo = 
{
	"value", &t44_0_0_1, &t2911_TI, offsetof(t2911, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2911_FIs[] =
{
	&t2911_f0_FieldInfo,
	&t2911_f1_FieldInfo,
	NULL
};
static PropertyInfo t2911____Key_PropertyInfo = 
{
	&t2911_TI, "Key", &m15947_MI, &m15948_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2911____Value_PropertyInfo = 
{
	&t2911_TI, "Value", &m15949_MI, &m15950_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2911_PIs[] =
{
	&t2911____Key_PropertyInfo,
	&t2911____Value_PropertyInfo,
	NULL
};
extern Il2CppType t7_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2911_m15946_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15946_GM;
MethodInfo m15946_MI = 
{
	".ctor", (methodPointerType)&m15946, &t2911_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2911_m15946_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15946_GM};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15947_GM;
MethodInfo m15947_MI = 
{
	"get_Key", (methodPointerType)&m15947, &t2911_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15947_GM};
extern Il2CppType t7_0_0_0;
static ParameterInfo t2911_m15948_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15948_GM;
MethodInfo m15948_MI = 
{
	"set_Key", (methodPointerType)&m15948, &t2911_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2911_m15948_ParameterInfos, &EmptyCustomAttributesCache, 2177, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15948_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15949_GM;
MethodInfo m15949_MI = 
{
	"get_Value", (methodPointerType)&m15949, &t2911_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15949_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2911_m15950_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15950_GM;
MethodInfo m15950_MI = 
{
	"set_Value", (methodPointerType)&m15950, &t2911_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2911_m15950_ParameterInfos, &EmptyCustomAttributesCache, 2177, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15950_GM};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15951_GM;
MethodInfo m15951_MI = 
{
	"ToString", (methodPointerType)&m15951, &t2911_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15951_GM};
static MethodInfo* t2911_MIs[] =
{
	&m15946_MI,
	&m15947_MI,
	&m15948_MI,
	&m15949_MI,
	&m15950_MI,
	&m15951_MI,
	NULL
};
static MethodInfo* t2911_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m15951_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2911_1_0_0;
extern Il2CppGenericClass t2911_GC;
extern CustomAttributesCache t1259__CustomAttributeCache;
TypeInfo t2911_TI = 
{
	&g_mscorlib_dll_Image, NULL, "KeyValuePair`2", "System.Collections.Generic", t2911_MIs, t2911_PIs, t2911_FIs, NULL, &t110_TI, NULL, NULL, &t2911_TI, NULL, t2911_VT, &t1259__CustomAttributeCache, &t2911_TI, &t2911_0_0_0, &t2911_1_0_0, NULL, &t2911_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2911)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057033, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 2, 0, 0, 4, 0, 0};
#include "t2914.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2914_TI;
#include "t2914MD.h"

extern MethodInfo m15956_MI;
extern MethodInfo m22226_MI;
struct t20;
 t2911  m22226 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m15952_MI;
 void m15952 (t2914 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m15953_MI;
 t29 * m15953 (t2914 * __this, MethodInfo* method){
	{
		t2911  L_0 = m15956(__this, &m15956_MI);
		t2911  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2911_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m15954_MI;
 void m15954 (t2914 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m15955_MI;
 bool m15955 (t2914 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 t2911  m15956 (t2914 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		t2911  L_8 = m22226(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m22226_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>>
extern Il2CppType t20_0_0_1;
FieldInfo t2914_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2914_TI, offsetof(t2914, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2914_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2914_TI, offsetof(t2914, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2914_FIs[] =
{
	&t2914_f0_FieldInfo,
	&t2914_f1_FieldInfo,
	NULL
};
static PropertyInfo t2914____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2914_TI, "System.Collections.IEnumerator.Current", &m15953_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2914____Current_PropertyInfo = 
{
	&t2914_TI, "Current", &m15956_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2914_PIs[] =
{
	&t2914____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2914____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2914_m15952_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15952_GM;
MethodInfo m15952_MI = 
{
	".ctor", (methodPointerType)&m15952, &t2914_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2914_m15952_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15952_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15953_GM;
MethodInfo m15953_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m15953, &t2914_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15953_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15954_GM;
MethodInfo m15954_MI = 
{
	"Dispose", (methodPointerType)&m15954, &t2914_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15954_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15955_GM;
MethodInfo m15955_MI = 
{
	"MoveNext", (methodPointerType)&m15955, &t2914_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15955_GM};
extern Il2CppType t2911_0_0_0;
extern void* RuntimeInvoker_t2911 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15956_GM;
MethodInfo m15956_MI = 
{
	"get_Current", (methodPointerType)&m15956, &t2914_TI, &t2911_0_0_0, RuntimeInvoker_t2911, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15956_GM};
static MethodInfo* t2914_MIs[] =
{
	&m15952_MI,
	&m15953_MI,
	&m15954_MI,
	&m15955_MI,
	&m15956_MI,
	NULL
};
static MethodInfo* t2914_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15953_MI,
	&m15955_MI,
	&m15954_MI,
	&m15956_MI,
};
static TypeInfo* t2914_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2912_TI,
};
static Il2CppInterfaceOffsetPair t2914_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2912_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2914_0_0_0;
extern Il2CppType t2914_1_0_0;
extern Il2CppGenericClass t2914_GC;
TypeInfo t2914_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2914_MIs, t2914_PIs, t2914_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2914_TI, t2914_ITIs, t2914_VT, &EmptyCustomAttributesCache, &t2914_TI, &t2914_0_0_0, &t2914_1_0_0, t2914_IOs, &t2914_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2914)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5596_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>>
extern MethodInfo m29301_MI;
extern MethodInfo m29302_MI;
static PropertyInfo t5596____Item_PropertyInfo = 
{
	&t5596_TI, "Item", &m29301_MI, &m29302_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5596_PIs[] =
{
	&t5596____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2911_0_0_0;
static ParameterInfo t5596_m29303_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2911_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t2911 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29303_GM;
MethodInfo m29303_MI = 
{
	"IndexOf", NULL, &t5596_TI, &t44_0_0_0, RuntimeInvoker_t44_t2911, t5596_m29303_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29303_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2911_0_0_0;
static ParameterInfo t5596_m29304_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t2911_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t2911 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29304_GM;
MethodInfo m29304_MI = 
{
	"Insert", NULL, &t5596_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t2911, t5596_m29304_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29304_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5596_m29305_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29305_GM;
MethodInfo m29305_MI = 
{
	"RemoveAt", NULL, &t5596_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5596_m29305_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29305_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5596_m29301_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2911_0_0_0;
extern void* RuntimeInvoker_t2911_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29301_GM;
MethodInfo m29301_MI = 
{
	"get_Item", NULL, &t5596_TI, &t2911_0_0_0, RuntimeInvoker_t2911_t44, t5596_m29301_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29301_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2911_0_0_0;
static ParameterInfo t5596_m29302_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2911_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t2911 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29302_GM;
MethodInfo m29302_MI = 
{
	"set_Item", NULL, &t5596_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t2911, t5596_m29302_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29302_GM};
static MethodInfo* t5596_MIs[] =
{
	&m29303_MI,
	&m29304_MI,
	&m29305_MI,
	&m29301_MI,
	&m29302_MI,
	NULL
};
static TypeInfo* t5596_ITIs[] = 
{
	&t603_TI,
	&t5595_TI,
	&t5597_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5596_0_0_0;
extern Il2CppType t5596_1_0_0;
struct t5596;
extern Il2CppGenericClass t5596_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5596_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5596_MIs, t5596_PIs, NULL, NULL, NULL, NULL, NULL, &t5596_TI, t5596_ITIs, NULL, &t1908__CustomAttributeCache, &t5596_TI, &t5596_0_0_0, &t5596_1_0_0, NULL, &t5596_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IDictionary`2<System.String,System.Int32>
extern Il2CppType t7_0_0_0;
static ParameterInfo t6733_m29306_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29306_GM;
MethodInfo m29306_MI = 
{
	"Remove", NULL, &t6733_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6733_m29306_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29306_GM};
static MethodInfo* t6733_MIs[] =
{
	&m29306_MI,
	NULL
};
static TypeInfo* t6733_ITIs[] = 
{
	&t603_TI,
	&t5595_TI,
	&t5597_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6733_0_0_0;
extern Il2CppType t6733_1_0_0;
struct t6733;
extern Il2CppGenericClass t6733_GC;
extern CustomAttributesCache t1975__CustomAttributeCache;
TypeInfo t6733_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IDictionary`2", "System.Collections.Generic", t6733_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6733_TI, t6733_ITIs, NULL, &t1975__CustomAttributeCache, &t6733_TI, &t6733_0_0_0, &t6733_1_0_0, NULL, &t6733_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2915.h"
#include "t2916.h"
extern TypeInfo t2915_TI;
extern TypeInfo t2916_TI;
#include "t2916MD.h"
#include "t2915MD.h"
extern MethodInfo m15969_MI;
extern MethodInfo m15968_MI;
extern MethodInfo m15988_MI;
extern MethodInfo m22237_MI;
extern MethodInfo m22238_MI;
extern MethodInfo m15971_MI;
struct t485;
 void m22237 (t485 * __this, t20 * p0, int32_t p1, t2916 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t485;
 void m22238 (t485 * __this, t841* p0, int32_t p1, t2916 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;


 void m15957 (t2909 * __this, t485 * p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		if (p0)
		{
			goto IL_0014;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1173, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0014:
	{
		__this->f0 = p0;
		return;
	}
}
extern MethodInfo m15958_MI;
 void m15958 (t2909 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral1174, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m15959_MI;
 void m15959 (t2909 * __this, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral1174, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m15960_MI;
 bool m15960 (t2909 * __this, int32_t p0, MethodInfo* method){
	{
		t485 * L_0 = (__this->f0);
		bool L_1 = m15936(L_0, p0, &m15936_MI);
		return L_1;
	}
}
extern MethodInfo m15961_MI;
 bool m15961 (t2909 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral1174, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m15962_MI;
 t29* m15962 (t2909 * __this, MethodInfo* method){
	{
		t2915  L_0 = m15969(__this, &m15969_MI);
		t2915  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2915_TI), &L_1);
		return (t29*)L_2;
	}
}
extern MethodInfo m15963_MI;
 void m15963 (t2909 * __this, t20 * p0, int32_t p1, MethodInfo* method){
	t841* V_0 = {0};
	{
		V_0 = ((t841*)IsInst(p0, InitializedTypeInfo(&t841_TI)));
		if (!V_0)
		{
			goto IL_0013;
		}
	}
	{
		VirtActionInvoker2< t841*, int32_t >::Invoke(&m15968_MI, __this, V_0, p1);
		return;
	}

IL_0013:
	{
		t485 * L_0 = (__this->f0);
		m15929(L_0, p0, p1, &m15929_MI);
		t485 * L_1 = (__this->f0);
		t35 L_2 = { &m15931_MI };
		t2916 * L_3 = (t2916 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2916_TI));
		m15988(L_3, NULL, L_2, &m15988_MI);
		m22237(L_1, p0, p1, L_3, &m22237_MI);
		return;
	}
}
extern MethodInfo m15964_MI;
 t29 * m15964 (t2909 * __this, MethodInfo* method){
	{
		t2915  L_0 = m15969(__this, &m15969_MI);
		t2915  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2915_TI), &L_1);
		return (t29 *)L_2;
	}
}
extern MethodInfo m15965_MI;
 bool m15965 (t2909 * __this, MethodInfo* method){
	{
		return 1;
	}
}
extern MethodInfo m15966_MI;
 bool m15966 (t2909 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m15967_MI;
 t29 * m15967 (t2909 * __this, MethodInfo* method){
	{
		t485 * L_0 = (__this->f0);
		t29 * L_1 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m9816_MI, L_0);
		return L_1;
	}
}
 void m15968 (t2909 * __this, t841* p0, int32_t p1, MethodInfo* method){
	{
		t485 * L_0 = (__this->f0);
		m15929(L_0, (t20 *)(t20 *)p0, p1, &m15929_MI);
		t485 * L_1 = (__this->f0);
		t35 L_2 = { &m15931_MI };
		t2916 * L_3 = (t2916 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2916_TI));
		m15988(L_3, NULL, L_2, &m15988_MI);
		m22238(L_1, p0, p1, L_3, &m22238_MI);
		return;
	}
}
 t2915  m15969 (t2909 * __this, MethodInfo* method){
	{
		t485 * L_0 = (__this->f0);
		t2915  L_1 = {0};
		m15971(&L_1, L_0, &m15971_MI);
		return L_1;
	}
}
extern MethodInfo m15970_MI;
 int32_t m15970 (t2909 * __this, MethodInfo* method){
	{
		t485 * L_0 = (__this->f0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m15924_MI, L_0);
		return L_1;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Int32>
extern Il2CppType t485_0_0_1;
FieldInfo t2909_f0_FieldInfo = 
{
	"dictionary", &t485_0_0_1, &t2909_TI, offsetof(t2909, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2909_FIs[] =
{
	&t2909_f0_FieldInfo,
	NULL
};
static PropertyInfo t2909____System_Collections_Generic_ICollectionU3CTValueU3E_IsReadOnly_PropertyInfo = 
{
	&t2909_TI, "System.Collections.Generic.ICollection<TValue>.IsReadOnly", &m15965_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2909____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2909_TI, "System.Collections.ICollection.IsSynchronized", &m15966_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2909____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2909_TI, "System.Collections.ICollection.SyncRoot", &m15967_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2909____Count_PropertyInfo = 
{
	&t2909_TI, "Count", &m15970_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2909_PIs[] =
{
	&t2909____System_Collections_Generic_ICollectionU3CTValueU3E_IsReadOnly_PropertyInfo,
	&t2909____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2909____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2909____Count_PropertyInfo,
	NULL
};
extern Il2CppType t485_0_0_0;
static ParameterInfo t2909_m15957_ParameterInfos[] = 
{
	{"dictionary", 0, 134217728, &EmptyCustomAttributesCache, &t485_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15957_GM;
MethodInfo m15957_MI = 
{
	".ctor", (methodPointerType)&m15957, &t2909_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2909_m15957_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15957_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2909_m15958_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15958_GM;
MethodInfo m15958_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Add", (methodPointerType)&m15958, &t2909_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2909_m15958_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15958_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15959_GM;
MethodInfo m15959_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Clear", (methodPointerType)&m15959, &t2909_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 12, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15959_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2909_m15960_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15960_GM;
MethodInfo m15960_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Contains", (methodPointerType)&m15960, &t2909_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t2909_m15960_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15960_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2909_m15961_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15961_GM;
MethodInfo m15961_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Remove", (methodPointerType)&m15961, &t2909_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t2909_m15961_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15961_GM};
extern Il2CppType t2424_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15962_GM;
MethodInfo m15962_MI = 
{
	"System.Collections.Generic.IEnumerable<TValue>.GetEnumerator", (methodPointerType)&m15962, &t2909_TI, &t2424_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 16, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15962_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2909_m15963_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15963_GM;
MethodInfo m15963_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m15963, &t2909_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2909_m15963_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15963_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15964_GM;
MethodInfo m15964_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m15964, &t2909_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15964_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15965_GM;
MethodInfo m15965_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.get_IsReadOnly", (methodPointerType)&m15965, &t2909_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15965_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15966_GM;
MethodInfo m15966_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m15966, &t2909_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15966_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15967_GM;
MethodInfo m15967_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m15967, &t2909_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15967_GM};
extern Il2CppType t841_0_0_0;
extern Il2CppType t841_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2909_m15968_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t841_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15968_GM;
MethodInfo m15968_MI = 
{
	"CopyTo", (methodPointerType)&m15968, &t2909_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2909_m15968_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 14, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15968_GM};
extern Il2CppType t2915_0_0_0;
extern void* RuntimeInvoker_t2915 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15969_GM;
MethodInfo m15969_MI = 
{
	"GetEnumerator", (methodPointerType)&m15969, &t2909_TI, &t2915_0_0_0, RuntimeInvoker_t2915, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15969_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15970_GM;
MethodInfo m15970_MI = 
{
	"get_Count", (methodPointerType)&m15970, &t2909_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15970_GM};
static MethodInfo* t2909_MIs[] =
{
	&m15957_MI,
	&m15958_MI,
	&m15959_MI,
	&m15960_MI,
	&m15961_MI,
	&m15962_MI,
	&m15963_MI,
	&m15964_MI,
	&m15965_MI,
	&m15966_MI,
	&m15967_MI,
	&m15968_MI,
	&m15969_MI,
	&m15970_MI,
	NULL
};
static MethodInfo* t2909_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15964_MI,
	&m15970_MI,
	&m15966_MI,
	&m15967_MI,
	&m15963_MI,
	&m15970_MI,
	&m15965_MI,
	&m15958_MI,
	&m15959_MI,
	&m15960_MI,
	&m15968_MI,
	&m15961_MI,
	&m15962_MI,
};
extern TypeInfo t5140_TI;
extern TypeInfo t5142_TI;
static TypeInfo* t2909_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t5140_TI,
	&t5142_TI,
};
static Il2CppInterfaceOffsetPair t2909_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t5140_TI, 9},
	{ &t5142_TI, 16},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2909_0_0_0;
extern Il2CppType t2909_1_0_0;
struct t2909;
extern Il2CppGenericClass t2909_GC;
extern CustomAttributesCache t1252__CustomAttributeCache;
TypeInfo t2909_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ValueCollection", "", t2909_MIs, t2909_PIs, t2909_FIs, NULL, &t29_TI, NULL, &t1254_TI, &t2909_TI, t2909_ITIs, t2909_VT, &t1252__CustomAttributeCache, &t2909_TI, &t2909_0_0_0, &t2909_1_0_0, t2909_IOs, &t2909_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2909), 0, -1, 0, 0, -1, 1057026, 0, false, false, false, false, true, false, false, false, false, false, false, false, 14, 4, 1, 0, 0, 17, 4, 4};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m15984_MI;
extern MethodInfo m15987_MI;
extern MethodInfo m15981_MI;


 void m15971 (t2915 * __this, t485 * p0, MethodInfo* method){
	{
		t2913  L_0 = m15944(p0, &m15944_MI);
		__this->f0 = L_0;
		return;
	}
}
extern MethodInfo m15972_MI;
 t29 * m15972 (t2915 * __this, MethodInfo* method){
	{
		t2913 * L_0 = &(__this->f0);
		int32_t L_1 = m15984(L_0, &m15984_MI);
		int32_t L_2 = L_1;
		t29 * L_3 = Box(InitializedTypeInfo(&t44_TI), &L_2);
		return L_3;
	}
}
extern MethodInfo m15973_MI;
 void m15973 (t2915 * __this, MethodInfo* method){
	{
		t2913 * L_0 = &(__this->f0);
		m15987(L_0, &m15987_MI);
		return;
	}
}
extern MethodInfo m15974_MI;
 bool m15974 (t2915 * __this, MethodInfo* method){
	{
		t2913 * L_0 = &(__this->f0);
		bool L_1 = m15981(L_0, &m15981_MI);
		return L_1;
	}
}
extern MethodInfo m15975_MI;
 int32_t m15975 (t2915 * __this, MethodInfo* method){
	{
		t2913 * L_0 = &(__this->f0);
		t2911 * L_1 = &(L_0->f3);
		int32_t L_2 = m15949(L_1, &m15949_MI);
		return L_2;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Int32>
extern Il2CppType t2913_0_0_1;
FieldInfo t2915_f0_FieldInfo = 
{
	"host_enumerator", &t2913_0_0_1, &t2915_TI, offsetof(t2915, f0) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2915_FIs[] =
{
	&t2915_f0_FieldInfo,
	NULL
};
static PropertyInfo t2915____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2915_TI, "System.Collections.IEnumerator.Current", &m15972_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2915____Current_PropertyInfo = 
{
	&t2915_TI, "Current", &m15975_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2915_PIs[] =
{
	&t2915____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2915____Current_PropertyInfo,
	NULL
};
extern Il2CppType t485_0_0_0;
static ParameterInfo t2915_m15971_ParameterInfos[] = 
{
	{"host", 0, 134217728, &EmptyCustomAttributesCache, &t485_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15971_GM;
MethodInfo m15971_MI = 
{
	".ctor", (methodPointerType)&m15971, &t2915_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2915_m15971_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15971_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15972_GM;
MethodInfo m15972_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m15972, &t2915_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15972_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15973_GM;
MethodInfo m15973_MI = 
{
	"Dispose", (methodPointerType)&m15973, &t2915_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15973_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15974_GM;
MethodInfo m15974_MI = 
{
	"MoveNext", (methodPointerType)&m15974, &t2915_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15974_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15975_GM;
MethodInfo m15975_MI = 
{
	"get_Current", (methodPointerType)&m15975, &t2915_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15975_GM};
static MethodInfo* t2915_MIs[] =
{
	&m15971_MI,
	&m15972_MI,
	&m15973_MI,
	&m15974_MI,
	&m15975_MI,
	NULL
};
static MethodInfo* t2915_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15972_MI,
	&m15974_MI,
	&m15973_MI,
	&m15975_MI,
};
extern TypeInfo t2424_TI;
static TypeInfo* t2915_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2424_TI,
};
static Il2CppInterfaceOffsetPair t2915_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2424_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2915_0_0_0;
extern Il2CppType t2915_1_0_0;
extern Il2CppGenericClass t2915_GC;
TypeInfo t2915_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t2915_MIs, t2915_PIs, t2915_FIs, NULL, &t110_TI, NULL, &t1252_TI, &t2915_TI, t2915_ITIs, t2915_VT, &EmptyCustomAttributesCache, &t2915_TI, &t2915_0_0_0, &t2915_1_0_0, t2915_IOs, &t2915_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2915)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 1, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m15986_MI;
extern MethodInfo m15983_MI;
extern MethodInfo m15985_MI;


 void m15976 (t2913 * __this, t485 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		int32_t L_0 = (p0->f14);
		__this->f2 = L_0;
		return;
	}
}
extern MethodInfo m15977_MI;
 t29 * m15977 (t2913 * __this, MethodInfo* method){
	{
		m15986(__this, &m15986_MI);
		t2911  L_0 = (__this->f3);
		t2911  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2911_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m15978_MI;
 t725  m15978 (t2913 * __this, MethodInfo* method){
	{
		m15986(__this, &m15986_MI);
		t2911 * L_0 = &(__this->f3);
		t7* L_1 = m15947(L_0, &m15947_MI);
		t7* L_2 = L_1;
		t2911 * L_3 = &(__this->f3);
		int32_t L_4 = m15949(L_3, &m15949_MI);
		int32_t L_5 = L_4;
		t29 * L_6 = Box(InitializedTypeInfo(&t44_TI), &L_5);
		t725  L_7 = {0};
		m3965(&L_7, ((t7*)L_2), L_6, &m3965_MI);
		return L_7;
	}
}
extern MethodInfo m15979_MI;
 t29 * m15979 (t2913 * __this, MethodInfo* method){
	{
		t7* L_0 = m15983(__this, &m15983_MI);
		t7* L_1 = L_0;
		return ((t7*)L_1);
	}
}
extern MethodInfo m15980_MI;
 t29 * m15980 (t2913 * __this, MethodInfo* method){
	{
		int32_t L_0 = m15984(__this, &m15984_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t44_TI), &L_1);
		return L_2;
	}
}
 bool m15981 (t2913 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		m15985(__this, &m15985_MI);
		int32_t L_0 = (__this->f1);
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0011;
		}
	}
	{
		return 0;
	}

IL_0011:
	{
		goto IL_0072;
	}

IL_0013:
	{
		int32_t L_1 = (__this->f1);
		int32_t L_2 = L_1;
		V_1 = L_2;
		__this->f1 = ((int32_t)(L_2+1));
		V_0 = V_1;
		t485 * L_3 = (__this->f0);
		t1965* L_4 = (L_3->f5);
		int32_t L_5 = (((t1248 *)(t1248 *)SZArrayLdElema(L_4, V_0))->f0);
		if (!((int32_t)((int32_t)L_5&(int32_t)((int32_t)-2147483648))))
		{
			goto IL_0072;
		}
	}
	{
		t485 * L_6 = (__this->f0);
		t446* L_7 = (L_6->f6);
		int32_t L_8 = V_0;
		t485 * L_9 = (__this->f0);
		t841* L_10 = (L_9->f7);
		int32_t L_11 = V_0;
		t2911  L_12 = {0};
		m15946(&L_12, (*(t7**)(t7**)SZArrayLdElema(L_7, L_8)), (*(int32_t*)(int32_t*)SZArrayLdElema(L_10, L_11)), &m15946_MI);
		__this->f3 = L_12;
		return 1;
	}

IL_0072:
	{
		int32_t L_13 = (__this->f1);
		t485 * L_14 = (__this->f0);
		int32_t L_15 = (L_14->f8);
		if ((((int32_t)L_13) < ((int32_t)L_15)))
		{
			goto IL_0013;
		}
	}
	{
		__this->f1 = (-1);
		return 0;
	}
}
extern MethodInfo m15982_MI;
 t2911  m15982 (t2913 * __this, MethodInfo* method){
	{
		t2911  L_0 = (__this->f3);
		return L_0;
	}
}
 t7* m15983 (t2913 * __this, MethodInfo* method){
	{
		m15986(__this, &m15986_MI);
		t2911 * L_0 = &(__this->f3);
		t7* L_1 = m15947(L_0, &m15947_MI);
		return L_1;
	}
}
 int32_t m15984 (t2913 * __this, MethodInfo* method){
	{
		m15986(__this, &m15986_MI);
		t2911 * L_0 = &(__this->f3);
		int32_t L_1 = m15949(L_0, &m15949_MI);
		return L_1;
	}
}
 void m15985 (t2913 * __this, MethodInfo* method){
	{
		t485 * L_0 = (__this->f0);
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		t1101 * L_1 = (t1101 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1101_TI));
		m5150(L_1, (t7*)NULL, &m5150_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_000f:
	{
		t485 * L_2 = (__this->f0);
		int32_t L_3 = (L_2->f14);
		int32_t L_4 = (__this->f2);
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_002d;
		}
	}
	{
		t914 * L_5 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_5, (t7*) &_stringLiteral1171, &m3964_MI);
		il2cpp_codegen_raise_exception(L_5);
	}

IL_002d:
	{
		return;
	}
}
 void m15986 (t2913 * __this, MethodInfo* method){
	{
		m15985(__this, &m15985_MI);
		int32_t L_0 = (__this->f1);
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001a;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1172, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_001a:
	{
		return;
	}
}
 void m15987 (t2913 * __this, MethodInfo* method){
	{
		__this->f0 = (t485 *)NULL;
		return;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>
extern Il2CppType t485_0_0_1;
FieldInfo t2913_f0_FieldInfo = 
{
	"dictionary", &t485_0_0_1, &t2913_TI, offsetof(t2913, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2913_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t2913_TI, offsetof(t2913, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2913_f2_FieldInfo = 
{
	"stamp", &t44_0_0_1, &t2913_TI, offsetof(t2913, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t2911_0_0_3;
FieldInfo t2913_f3_FieldInfo = 
{
	"current", &t2911_0_0_3, &t2913_TI, offsetof(t2913, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2913_FIs[] =
{
	&t2913_f0_FieldInfo,
	&t2913_f1_FieldInfo,
	&t2913_f2_FieldInfo,
	&t2913_f3_FieldInfo,
	NULL
};
static PropertyInfo t2913____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2913_TI, "System.Collections.IEnumerator.Current", &m15977_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2913____System_Collections_IDictionaryEnumerator_Entry_PropertyInfo = 
{
	&t2913_TI, "System.Collections.IDictionaryEnumerator.Entry", &m15978_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2913____System_Collections_IDictionaryEnumerator_Key_PropertyInfo = 
{
	&t2913_TI, "System.Collections.IDictionaryEnumerator.Key", &m15979_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2913____System_Collections_IDictionaryEnumerator_Value_PropertyInfo = 
{
	&t2913_TI, "System.Collections.IDictionaryEnumerator.Value", &m15980_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2913____Current_PropertyInfo = 
{
	&t2913_TI, "Current", &m15982_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2913____CurrentKey_PropertyInfo = 
{
	&t2913_TI, "CurrentKey", &m15983_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2913____CurrentValue_PropertyInfo = 
{
	&t2913_TI, "CurrentValue", &m15984_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2913_PIs[] =
{
	&t2913____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2913____System_Collections_IDictionaryEnumerator_Entry_PropertyInfo,
	&t2913____System_Collections_IDictionaryEnumerator_Key_PropertyInfo,
	&t2913____System_Collections_IDictionaryEnumerator_Value_PropertyInfo,
	&t2913____Current_PropertyInfo,
	&t2913____CurrentKey_PropertyInfo,
	&t2913____CurrentValue_PropertyInfo,
	NULL
};
extern Il2CppType t485_0_0_0;
static ParameterInfo t2913_m15976_ParameterInfos[] = 
{
	{"dictionary", 0, 134217728, &EmptyCustomAttributesCache, &t485_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15976_GM;
MethodInfo m15976_MI = 
{
	".ctor", (methodPointerType)&m15976, &t2913_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2913_m15976_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15976_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15977_GM;
MethodInfo m15977_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m15977, &t2913_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15977_GM};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15978_GM;
MethodInfo m15978_MI = 
{
	"System.Collections.IDictionaryEnumerator.get_Entry", (methodPointerType)&m15978, &t2913_TI, &t725_0_0_0, RuntimeInvoker_t725, NULL, &EmptyCustomAttributesCache, 2529, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15978_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15979_GM;
MethodInfo m15979_MI = 
{
	"System.Collections.IDictionaryEnumerator.get_Key", (methodPointerType)&m15979, &t2913_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15979_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15980_GM;
MethodInfo m15980_MI = 
{
	"System.Collections.IDictionaryEnumerator.get_Value", (methodPointerType)&m15980, &t2913_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15980_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15981_GM;
MethodInfo m15981_MI = 
{
	"MoveNext", (methodPointerType)&m15981, &t2913_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15981_GM};
extern Il2CppType t2911_0_0_0;
extern void* RuntimeInvoker_t2911 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15982_GM;
MethodInfo m15982_MI = 
{
	"get_Current", (methodPointerType)&m15982, &t2913_TI, &t2911_0_0_0, RuntimeInvoker_t2911, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15982_GM};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15983_GM;
MethodInfo m15983_MI = 
{
	"get_CurrentKey", (methodPointerType)&m15983, &t2913_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2179, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15983_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15984_GM;
MethodInfo m15984_MI = 
{
	"get_CurrentValue", (methodPointerType)&m15984, &t2913_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2179, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15984_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15985_GM;
MethodInfo m15985_MI = 
{
	"VerifyState", (methodPointerType)&m15985, &t2913_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15985_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15986_GM;
MethodInfo m15986_MI = 
{
	"VerifyCurrent", (methodPointerType)&m15986, &t2913_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15986_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15987_GM;
MethodInfo m15987_MI = 
{
	"Dispose", (methodPointerType)&m15987, &t2913_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15987_GM};
static MethodInfo* t2913_MIs[] =
{
	&m15976_MI,
	&m15977_MI,
	&m15978_MI,
	&m15979_MI,
	&m15980_MI,
	&m15981_MI,
	&m15982_MI,
	&m15983_MI,
	&m15984_MI,
	&m15985_MI,
	&m15986_MI,
	&m15987_MI,
	NULL
};
static MethodInfo* t2913_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15977_MI,
	&m15981_MI,
	&m15987_MI,
	&m15982_MI,
	&m15978_MI,
	&m15979_MI,
	&m15980_MI,
};
static TypeInfo* t2913_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2912_TI,
	&t722_TI,
};
static Il2CppInterfaceOffsetPair t2913_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2912_TI, 7},
	{ &t722_TI, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2913_0_0_0;
extern Il2CppType t2913_1_0_0;
extern Il2CppGenericClass t2913_GC;
TypeInfo t2913_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t2913_MIs, t2913_PIs, t2913_FIs, NULL, &t110_TI, NULL, &t1254_TI, &t2913_TI, t2913_ITIs, t2913_VT, &EmptyCustomAttributesCache, &t2913_TI, &t2913_0_0_0, &t2913_1_0_0, t2913_IOs, &t2913_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2913)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 12, 7, 4, 0, 0, 11, 4, 4};
#ifndef _MSC_VER
#else
#endif



 void m15988 (t2916 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m15989_MI;
 int32_t m15989 (t2916 * __this, t7* p0, int32_t p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m15989((t2916 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (t29 *, t29 * __this, t7* p0, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (t29 * __this, t7* p0, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (t29 * __this, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m15990_MI;
 t29 * m15990 (t2916 * __this, t7* p0, int32_t p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = Box(InitializedTypeInfo(&t44_TI), &p1);
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m15991_MI;
 int32_t m15991 (t2916 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Int32,System.Int32>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2916_m15988_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15988_GM;
MethodInfo m15988_MI = 
{
	".ctor", (methodPointerType)&m15988, &t2916_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2916_m15988_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15988_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2916_m15989_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15989_GM;
MethodInfo m15989_MI = 
{
	"Invoke", (methodPointerType)&m15989, &t2916_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t44, t2916_m15989_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15989_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2916_m15990_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15990_GM;
MethodInfo m15990_MI = 
{
	"BeginInvoke", (methodPointerType)&m15990, &t2916_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t44_t29_t29, t2916_m15990_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m15990_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2916_m15991_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15991_GM;
MethodInfo m15991_MI = 
{
	"EndInvoke", (methodPointerType)&m15991, &t2916_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2916_m15991_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15991_GM};
static MethodInfo* t2916_MIs[] =
{
	&m15988_MI,
	&m15989_MI,
	&m15990_MI,
	&m15991_MI,
	NULL
};
static MethodInfo* t2916_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m15989_MI,
	&m15990_MI,
	&m15991_MI,
};
static Il2CppInterfaceOffsetPair t2916_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2916_0_0_0;
extern Il2CppType t2916_1_0_0;
struct t2916;
extern Il2CppGenericClass t2916_GC;
TypeInfo t2916_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Transform`1", "", t2916_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1254_TI, &t2916_TI, NULL, t2916_VT, &EmptyCustomAttributesCache, &t2916_TI, &t2916_0_0_0, &t2916_1_0_0, t2916_IOs, &t2916_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2916), 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif



 void m15992 (t2908 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m15993_MI;
 t725  m15993 (t2908 * __this, t7* p0, int32_t p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m15993((t2908 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef t725  (*FunctionPointerType) (t29 *, t29 * __this, t7* p0, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef t725  (*FunctionPointerType) (t29 * __this, t7* p0, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef t725  (*FunctionPointerType) (t29 * __this, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m15994_MI;
 t29 * m15994 (t2908 * __this, t7* p0, int32_t p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = Box(InitializedTypeInfo(&t44_TI), &p1);
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m15995_MI;
 t725  m15995 (t2908 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(t725 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Int32,System.Collections.DictionaryEntry>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2908_m15992_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15992_GM;
MethodInfo m15992_MI = 
{
	".ctor", (methodPointerType)&m15992, &t2908_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2908_m15992_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15992_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2908_m15993_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15993_GM;
MethodInfo m15993_MI = 
{
	"Invoke", (methodPointerType)&m15993, &t2908_TI, &t725_0_0_0, RuntimeInvoker_t725_t29_t44, t2908_m15993_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15993_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2908_m15994_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15994_GM;
MethodInfo m15994_MI = 
{
	"BeginInvoke", (methodPointerType)&m15994, &t2908_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t44_t29_t29, t2908_m15994_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m15994_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2908_m15995_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15995_GM;
MethodInfo m15995_MI = 
{
	"EndInvoke", (methodPointerType)&m15995, &t2908_TI, &t725_0_0_0, RuntimeInvoker_t725_t29, t2908_m15995_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15995_GM};
static MethodInfo* t2908_MIs[] =
{
	&m15992_MI,
	&m15993_MI,
	&m15994_MI,
	&m15995_MI,
	NULL
};
static MethodInfo* t2908_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m15993_MI,
	&m15994_MI,
	&m15995_MI,
};
static Il2CppInterfaceOffsetPair t2908_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2908_0_0_0;
extern Il2CppType t2908_1_0_0;
struct t2908;
extern Il2CppGenericClass t2908_GC;
TypeInfo t2908_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Transform`1", "", t2908_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1254_TI, &t2908_TI, NULL, t2908_VT, &EmptyCustomAttributesCache, &t2908_TI, &t2908_0_0_0, &t2908_1_0_0, t2908_IOs, &t2908_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2908), 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif



 void m15996 (t2917 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m15997_MI;
 t2911  m15997 (t2917 * __this, t7* p0, int32_t p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m15997((t2917 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef t2911  (*FunctionPointerType) (t29 *, t29 * __this, t7* p0, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef t2911  (*FunctionPointerType) (t29 * __this, t7* p0, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef t2911  (*FunctionPointerType) (t29 * __this, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m15998_MI;
 t29 * m15998 (t2917 * __this, t7* p0, int32_t p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = Box(InitializedTypeInfo(&t44_TI), &p1);
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m15999_MI;
 t2911  m15999 (t2917 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(t2911 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Int32,System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2917_m15996_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15996_GM;
MethodInfo m15996_MI = 
{
	".ctor", (methodPointerType)&m15996, &t2917_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2917_m15996_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15996_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2917_m15997_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2911_0_0_0;
extern void* RuntimeInvoker_t2911_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15997_GM;
MethodInfo m15997_MI = 
{
	"Invoke", (methodPointerType)&m15997, &t2917_TI, &t2911_0_0_0, RuntimeInvoker_t2911_t29_t44, t2917_m15997_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15997_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2917_m15998_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15998_GM;
MethodInfo m15998_MI = 
{
	"BeginInvoke", (methodPointerType)&m15998, &t2917_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t44_t29_t29, t2917_m15998_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m15998_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2917_m15999_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t2911_0_0_0;
extern void* RuntimeInvoker_t2911_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15999_GM;
MethodInfo m15999_MI = 
{
	"EndInvoke", (methodPointerType)&m15999, &t2917_TI, &t2911_0_0_0, RuntimeInvoker_t2911_t29, t2917_m15999_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15999_GM};
static MethodInfo* t2917_MIs[] =
{
	&m15996_MI,
	&m15997_MI,
	&m15998_MI,
	&m15999_MI,
	NULL
};
static MethodInfo* t2917_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m15997_MI,
	&m15998_MI,
	&m15999_MI,
};
static Il2CppInterfaceOffsetPair t2917_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2917_0_0_0;
extern Il2CppType t2917_1_0_0;
struct t2917;
extern Il2CppGenericClass t2917_GC;
TypeInfo t2917_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Transform`1", "", t2917_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1254_TI, &t2917_TI, NULL, t2917_VT, &EmptyCustomAttributesCache, &t2917_TI, &t2917_0_0_0, &t2917_1_0_0, t2917_IOs, &t2917_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2917), 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m16002_MI;


 void m16000 (t2918 * __this, t485 * p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		t2913  L_0 = m15944(p0, &m15944_MI);
		__this->f0 = L_0;
		return;
	}
}
extern MethodInfo m16001_MI;
 bool m16001 (t2918 * __this, MethodInfo* method){
	{
		t2913 * L_0 = &(__this->f0);
		bool L_1 = m15981(L_0, &m15981_MI);
		return L_1;
	}
}
 t725  m16002 (t2918 * __this, MethodInfo* method){
	{
		t2913  L_0 = (__this->f0);
		t2913  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2913_TI), &L_1);
		t725  L_3 = (t725 )InterfaceFuncInvoker0< t725  >::Invoke(&m10122_MI, L_2);
		return L_3;
	}
}
extern MethodInfo m16003_MI;
 t29 * m16003 (t2918 * __this, MethodInfo* method){
	t2911  V_0 = {0};
	{
		t2913 * L_0 = &(__this->f0);
		t2911  L_1 = m15982(L_0, &m15982_MI);
		V_0 = L_1;
		t7* L_2 = m15947((&V_0), &m15947_MI);
		t7* L_3 = L_2;
		return ((t7*)L_3);
	}
}
extern MethodInfo m16004_MI;
 t29 * m16004 (t2918 * __this, MethodInfo* method){
	t2911  V_0 = {0};
	{
		t2913 * L_0 = &(__this->f0);
		t2911  L_1 = m15982(L_0, &m15982_MI);
		V_0 = L_1;
		int32_t L_2 = m15949((&V_0), &m15949_MI);
		int32_t L_3 = L_2;
		t29 * L_4 = Box(InitializedTypeInfo(&t44_TI), &L_3);
		return L_4;
	}
}
extern MethodInfo m16005_MI;
 t29 * m16005 (t2918 * __this, MethodInfo* method){
	{
		t725  L_0 = (t725 )VirtFuncInvoker0< t725  >::Invoke(&m16002_MI, __this);
		t725  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t725_TI), &L_1);
		return L_2;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/ShimEnumerator<System.String,System.Int32>
extern Il2CppType t2913_0_0_1;
FieldInfo t2918_f0_FieldInfo = 
{
	"host_enumerator", &t2913_0_0_1, &t2918_TI, offsetof(t2918, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2918_FIs[] =
{
	&t2918_f0_FieldInfo,
	NULL
};
static PropertyInfo t2918____Entry_PropertyInfo = 
{
	&t2918_TI, "Entry", &m16002_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2918____Key_PropertyInfo = 
{
	&t2918_TI, "Key", &m16003_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2918____Value_PropertyInfo = 
{
	&t2918_TI, "Value", &m16004_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2918____Current_PropertyInfo = 
{
	&t2918_TI, "Current", &m16005_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2918_PIs[] =
{
	&t2918____Entry_PropertyInfo,
	&t2918____Key_PropertyInfo,
	&t2918____Value_PropertyInfo,
	&t2918____Current_PropertyInfo,
	NULL
};
extern Il2CppType t485_0_0_0;
static ParameterInfo t2918_m16000_ParameterInfos[] = 
{
	{"host", 0, 134217728, &EmptyCustomAttributesCache, &t485_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16000_GM;
MethodInfo m16000_MI = 
{
	".ctor", (methodPointerType)&m16000, &t2918_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2918_m16000_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16000_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16001_GM;
MethodInfo m16001_MI = 
{
	"MoveNext", (methodPointerType)&m16001, &t2918_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16001_GM};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16002_GM;
MethodInfo m16002_MI = 
{
	"get_Entry", (methodPointerType)&m16002, &t2918_TI, &t725_0_0_0, RuntimeInvoker_t725, NULL, &EmptyCustomAttributesCache, 2534, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16002_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16003_GM;
MethodInfo m16003_MI = 
{
	"get_Key", (methodPointerType)&m16003, &t2918_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16003_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16004_GM;
MethodInfo m16004_MI = 
{
	"get_Value", (methodPointerType)&m16004, &t2918_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16004_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16005_GM;
MethodInfo m16005_MI = 
{
	"get_Current", (methodPointerType)&m16005, &t2918_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16005_GM};
static MethodInfo* t2918_MIs[] =
{
	&m16000_MI,
	&m16001_MI,
	&m16002_MI,
	&m16003_MI,
	&m16004_MI,
	&m16005_MI,
	NULL
};
static MethodInfo* t2918_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16005_MI,
	&m16001_MI,
	&m16002_MI,
	&m16003_MI,
	&m16004_MI,
};
static TypeInfo* t2918_ITIs[] = 
{
	&t136_TI,
	&t722_TI,
};
static Il2CppInterfaceOffsetPair t2918_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t722_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2918_0_0_0;
extern Il2CppType t2918_1_0_0;
struct t2918;
extern Il2CppGenericClass t2918_GC;
TypeInfo t2918_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ShimEnumerator", "", t2918_MIs, t2918_PIs, t2918_FIs, NULL, &t29_TI, NULL, &t1254_TI, &t2918_TI, t2918_ITIs, t2918_VT, &EmptyCustomAttributesCache, &t2918_TI, &t2918_0_0_0, &t2918_1_0_0, t2918_IOs, &t2918_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2918), 0, -1, 0, 0, -1, 1056771, 0, false, false, false, false, true, false, false, false, false, false, false, false, 6, 4, 1, 0, 0, 9, 2, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4369_TI;

#include "t383.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.KeyCode>
extern MethodInfo m29307_MI;
static PropertyInfo t4369____Current_PropertyInfo = 
{
	&t4369_TI, "Current", &m29307_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4369_PIs[] =
{
	&t4369____Current_PropertyInfo,
	NULL
};
extern Il2CppType t383_0_0_0;
extern void* RuntimeInvoker_t383 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29307_GM;
MethodInfo m29307_MI = 
{
	"get_Current", NULL, &t4369_TI, &t383_0_0_0, RuntimeInvoker_t383, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29307_GM};
static MethodInfo* t4369_MIs[] =
{
	&m29307_MI,
	NULL
};
static TypeInfo* t4369_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4369_0_0_0;
extern Il2CppType t4369_1_0_0;
struct t4369;
extern Il2CppGenericClass t4369_GC;
TypeInfo t4369_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4369_MIs, t4369_PIs, NULL, NULL, NULL, NULL, NULL, &t4369_TI, t4369_ITIs, NULL, &EmptyCustomAttributesCache, &t4369_TI, &t4369_0_0_0, &t4369_1_0_0, NULL, &t4369_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2919.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2919_TI;
#include "t2919MD.h"

extern TypeInfo t383_TI;
extern MethodInfo m16010_MI;
extern MethodInfo m22244_MI;
struct t20;
 int32_t m22244 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m16006_MI;
 void m16006 (t2919 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m16007_MI;
 t29 * m16007 (t2919 * __this, MethodInfo* method){
	{
		int32_t L_0 = m16010(__this, &m16010_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t383_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m16008_MI;
 void m16008 (t2919 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m16009_MI;
 bool m16009 (t2919 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m16010 (t2919 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m22244(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m22244_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.KeyCode>
extern Il2CppType t20_0_0_1;
FieldInfo t2919_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2919_TI, offsetof(t2919, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2919_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2919_TI, offsetof(t2919, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2919_FIs[] =
{
	&t2919_f0_FieldInfo,
	&t2919_f1_FieldInfo,
	NULL
};
static PropertyInfo t2919____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2919_TI, "System.Collections.IEnumerator.Current", &m16007_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2919____Current_PropertyInfo = 
{
	&t2919_TI, "Current", &m16010_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2919_PIs[] =
{
	&t2919____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2919____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2919_m16006_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16006_GM;
MethodInfo m16006_MI = 
{
	".ctor", (methodPointerType)&m16006, &t2919_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2919_m16006_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16006_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16007_GM;
MethodInfo m16007_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m16007, &t2919_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16007_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16008_GM;
MethodInfo m16008_MI = 
{
	"Dispose", (methodPointerType)&m16008, &t2919_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16008_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16009_GM;
MethodInfo m16009_MI = 
{
	"MoveNext", (methodPointerType)&m16009, &t2919_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16009_GM};
extern Il2CppType t383_0_0_0;
extern void* RuntimeInvoker_t383 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16010_GM;
MethodInfo m16010_MI = 
{
	"get_Current", (methodPointerType)&m16010, &t2919_TI, &t383_0_0_0, RuntimeInvoker_t383, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16010_GM};
static MethodInfo* t2919_MIs[] =
{
	&m16006_MI,
	&m16007_MI,
	&m16008_MI,
	&m16009_MI,
	&m16010_MI,
	NULL
};
static MethodInfo* t2919_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16007_MI,
	&m16009_MI,
	&m16008_MI,
	&m16010_MI,
};
static TypeInfo* t2919_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4369_TI,
};
static Il2CppInterfaceOffsetPair t2919_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4369_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2919_0_0_0;
extern Il2CppType t2919_1_0_0;
extern Il2CppGenericClass t2919_GC;
TypeInfo t2919_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2919_MIs, t2919_PIs, t2919_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2919_TI, t2919_ITIs, t2919_VT, &EmptyCustomAttributesCache, &t2919_TI, &t2919_0_0_0, &t2919_1_0_0, t2919_IOs, &t2919_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2919)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5598_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.KeyCode>
extern MethodInfo m29308_MI;
static PropertyInfo t5598____Count_PropertyInfo = 
{
	&t5598_TI, "Count", &m29308_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29309_MI;
static PropertyInfo t5598____IsReadOnly_PropertyInfo = 
{
	&t5598_TI, "IsReadOnly", &m29309_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5598_PIs[] =
{
	&t5598____Count_PropertyInfo,
	&t5598____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29308_GM;
MethodInfo m29308_MI = 
{
	"get_Count", NULL, &t5598_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29308_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29309_GM;
MethodInfo m29309_MI = 
{
	"get_IsReadOnly", NULL, &t5598_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29309_GM};
extern Il2CppType t383_0_0_0;
extern Il2CppType t383_0_0_0;
static ParameterInfo t5598_m29310_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t383_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29310_GM;
MethodInfo m29310_MI = 
{
	"Add", NULL, &t5598_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5598_m29310_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29310_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29311_GM;
MethodInfo m29311_MI = 
{
	"Clear", NULL, &t5598_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29311_GM};
extern Il2CppType t383_0_0_0;
static ParameterInfo t5598_m29312_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t383_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29312_GM;
MethodInfo m29312_MI = 
{
	"Contains", NULL, &t5598_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5598_m29312_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29312_GM};
extern Il2CppType t3748_0_0_0;
extern Il2CppType t3748_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5598_m29313_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3748_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29313_GM;
MethodInfo m29313_MI = 
{
	"CopyTo", NULL, &t5598_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5598_m29313_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29313_GM};
extern Il2CppType t383_0_0_0;
static ParameterInfo t5598_m29314_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t383_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29314_GM;
MethodInfo m29314_MI = 
{
	"Remove", NULL, &t5598_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5598_m29314_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29314_GM};
static MethodInfo* t5598_MIs[] =
{
	&m29308_MI,
	&m29309_MI,
	&m29310_MI,
	&m29311_MI,
	&m29312_MI,
	&m29313_MI,
	&m29314_MI,
	NULL
};
extern TypeInfo t5600_TI;
static TypeInfo* t5598_ITIs[] = 
{
	&t603_TI,
	&t5600_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5598_0_0_0;
extern Il2CppType t5598_1_0_0;
struct t5598;
extern Il2CppGenericClass t5598_GC;
TypeInfo t5598_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5598_MIs, t5598_PIs, NULL, NULL, NULL, NULL, NULL, &t5598_TI, t5598_ITIs, NULL, &EmptyCustomAttributesCache, &t5598_TI, &t5598_0_0_0, &t5598_1_0_0, NULL, &t5598_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.KeyCode>
extern Il2CppType t4369_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29315_GM;
MethodInfo m29315_MI = 
{
	"GetEnumerator", NULL, &t5600_TI, &t4369_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29315_GM};
static MethodInfo* t5600_MIs[] =
{
	&m29315_MI,
	NULL
};
static TypeInfo* t5600_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5600_0_0_0;
extern Il2CppType t5600_1_0_0;
struct t5600;
extern Il2CppGenericClass t5600_GC;
TypeInfo t5600_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5600_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5600_TI, t5600_ITIs, NULL, &EmptyCustomAttributesCache, &t5600_TI, &t5600_0_0_0, &t5600_1_0_0, NULL, &t5600_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5599_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.KeyCode>
extern MethodInfo m29316_MI;
extern MethodInfo m29317_MI;
static PropertyInfo t5599____Item_PropertyInfo = 
{
	&t5599_TI, "Item", &m29316_MI, &m29317_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5599_PIs[] =
{
	&t5599____Item_PropertyInfo,
	NULL
};
extern Il2CppType t383_0_0_0;
static ParameterInfo t5599_m29318_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t383_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29318_GM;
MethodInfo m29318_MI = 
{
	"IndexOf", NULL, &t5599_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5599_m29318_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29318_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t383_0_0_0;
static ParameterInfo t5599_m29319_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t383_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29319_GM;
MethodInfo m29319_MI = 
{
	"Insert", NULL, &t5599_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5599_m29319_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29319_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5599_m29320_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29320_GM;
MethodInfo m29320_MI = 
{
	"RemoveAt", NULL, &t5599_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5599_m29320_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29320_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5599_m29316_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t383_0_0_0;
extern void* RuntimeInvoker_t383_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29316_GM;
MethodInfo m29316_MI = 
{
	"get_Item", NULL, &t5599_TI, &t383_0_0_0, RuntimeInvoker_t383_t44, t5599_m29316_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29316_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t383_0_0_0;
static ParameterInfo t5599_m29317_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t383_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29317_GM;
MethodInfo m29317_MI = 
{
	"set_Item", NULL, &t5599_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5599_m29317_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29317_GM};
static MethodInfo* t5599_MIs[] =
{
	&m29318_MI,
	&m29319_MI,
	&m29320_MI,
	&m29316_MI,
	&m29317_MI,
	NULL
};
static TypeInfo* t5599_ITIs[] = 
{
	&t603_TI,
	&t5598_TI,
	&t5600_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5599_0_0_0;
extern Il2CppType t5599_1_0_0;
struct t5599;
extern Il2CppGenericClass t5599_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5599_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5599_MIs, t5599_PIs, NULL, NULL, NULL, NULL, NULL, &t5599_TI, t5599_ITIs, NULL, &t1908__CustomAttributeCache, &t5599_TI, &t5599_0_0_0, &t5599_1_0_0, NULL, &t5599_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4371_TI;

#include "t384.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventType>
extern MethodInfo m29321_MI;
static PropertyInfo t4371____Current_PropertyInfo = 
{
	&t4371_TI, "Current", &m29321_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4371_PIs[] =
{
	&t4371____Current_PropertyInfo,
	NULL
};
extern Il2CppType t384_0_0_0;
extern void* RuntimeInvoker_t384 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29321_GM;
MethodInfo m29321_MI = 
{
	"get_Current", NULL, &t4371_TI, &t384_0_0_0, RuntimeInvoker_t384, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29321_GM};
static MethodInfo* t4371_MIs[] =
{
	&m29321_MI,
	NULL
};
static TypeInfo* t4371_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4371_0_0_0;
extern Il2CppType t4371_1_0_0;
struct t4371;
extern Il2CppGenericClass t4371_GC;
TypeInfo t4371_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4371_MIs, t4371_PIs, NULL, NULL, NULL, NULL, NULL, &t4371_TI, t4371_ITIs, NULL, &EmptyCustomAttributesCache, &t4371_TI, &t4371_0_0_0, &t4371_1_0_0, NULL, &t4371_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2920.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2920_TI;
#include "t2920MD.h"

extern TypeInfo t384_TI;
extern MethodInfo m16015_MI;
extern MethodInfo m22255_MI;
struct t20;
 int32_t m22255 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m16011_MI;
 void m16011 (t2920 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m16012_MI;
 t29 * m16012 (t2920 * __this, MethodInfo* method){
	{
		int32_t L_0 = m16015(__this, &m16015_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t384_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m16013_MI;
 void m16013 (t2920 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m16014_MI;
 bool m16014 (t2920 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m16015 (t2920 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m22255(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m22255_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventType>
extern Il2CppType t20_0_0_1;
FieldInfo t2920_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2920_TI, offsetof(t2920, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2920_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2920_TI, offsetof(t2920, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2920_FIs[] =
{
	&t2920_f0_FieldInfo,
	&t2920_f1_FieldInfo,
	NULL
};
static PropertyInfo t2920____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2920_TI, "System.Collections.IEnumerator.Current", &m16012_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2920____Current_PropertyInfo = 
{
	&t2920_TI, "Current", &m16015_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2920_PIs[] =
{
	&t2920____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2920____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2920_m16011_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16011_GM;
MethodInfo m16011_MI = 
{
	".ctor", (methodPointerType)&m16011, &t2920_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2920_m16011_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16011_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16012_GM;
MethodInfo m16012_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m16012, &t2920_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16012_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16013_GM;
MethodInfo m16013_MI = 
{
	"Dispose", (methodPointerType)&m16013, &t2920_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16013_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16014_GM;
MethodInfo m16014_MI = 
{
	"MoveNext", (methodPointerType)&m16014, &t2920_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16014_GM};
extern Il2CppType t384_0_0_0;
extern void* RuntimeInvoker_t384 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16015_GM;
MethodInfo m16015_MI = 
{
	"get_Current", (methodPointerType)&m16015, &t2920_TI, &t384_0_0_0, RuntimeInvoker_t384, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16015_GM};
static MethodInfo* t2920_MIs[] =
{
	&m16011_MI,
	&m16012_MI,
	&m16013_MI,
	&m16014_MI,
	&m16015_MI,
	NULL
};
static MethodInfo* t2920_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16012_MI,
	&m16014_MI,
	&m16013_MI,
	&m16015_MI,
};
static TypeInfo* t2920_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4371_TI,
};
static Il2CppInterfaceOffsetPair t2920_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4371_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2920_0_0_0;
extern Il2CppType t2920_1_0_0;
extern Il2CppGenericClass t2920_GC;
TypeInfo t2920_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2920_MIs, t2920_PIs, t2920_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2920_TI, t2920_ITIs, t2920_VT, &EmptyCustomAttributesCache, &t2920_TI, &t2920_0_0_0, &t2920_1_0_0, t2920_IOs, &t2920_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2920)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
