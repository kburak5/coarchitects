﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2521;
#include "t184.h"

 void m13378 (t2521 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m13379 (t2521 * __this, t184  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13380 (t2521 * __this, t184  p0, t184  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
