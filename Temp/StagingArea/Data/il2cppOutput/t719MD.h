﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t719;
struct t726;
struct t732;
struct t29;
struct t674;
struct t721;
struct t733;
struct t734;
struct t136;
struct t20;
struct t722;
struct t1278;
struct t841;
#include "t735.h"
#include "t1274.h"

 void m4209 (t719 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6709 (t719 * __this, int32_t p0, float p1, t29 * p2, t29 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6710 (t719 * __this, int32_t p0, float p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4215 (t719 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6711 (t719 * __this, t719 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3958 (t719 * __this, int32_t p0, t29 * p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6712 (t719 * __this, t29 * p0, float p1, t29 * p2, t29 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3963 (t719 * __this, t29 * p0, t29 * p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4026 (t719 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6713 (t719 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3979 (t719 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6714 (t719 * __this, int32_t p0, float p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6715 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m6716 (t719 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6717 (t719 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6718 (t719 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4218 (t719 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6719 (t719 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m6720 (t719 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m4267 (t719 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m5221 (t719 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3989 (t719 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4216 (t719 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6721 (t719 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3990 (t719 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4266 (t719 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4219 (t719 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m6722 (t719 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4217 (t719 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4030 (t719 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m6723 (t719 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6724 (t719 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6725 (t719 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6726 (t719 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6727 (t719 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6728 (t719 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6729 (t719 * __this, t1278* p0, t841* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6730 (t719 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6731 (t719 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6732 (t719 * __this, t29 * p0, t29 * p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6733 (t719 * __this, t20 * p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6734 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6735 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6736 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
