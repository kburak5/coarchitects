﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t660;
struct t29;
struct t316;
struct t631;
struct t633;
#include "t1149.h"
#include "t630.h"

 void m7514 (t660 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7515 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7516 (t660 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m2982 (t660 * __this, t316* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
