﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t602;
#include "t1126.h"

 float m2875 (t29 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9283 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m9284 (t29 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m2880 (t29 * __this, double p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m2878 (t29 * __this, double p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m2877 (t29 * __this, double p0, double p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5139 (t29 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9285 (t29 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m9286 (t29 * __this, t1126  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m2879 (t29 * __this, double p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m2872 (t29 * __this, double p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m2873 (t29 * __this, double p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m9287 (t29 * __this, double p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m2876 (t29 * __this, double p0, double p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m2874 (t29 * __this, double p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
