﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3197;
struct t29;
struct t770;

 void m17774 (t3197 * __this, t770 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17775 (t3197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17776 (t3197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17777 (t3197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17778 (t3197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
