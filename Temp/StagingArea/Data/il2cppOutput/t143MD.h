﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t143;
struct t29;
struct t145;
struct t66;
struct t67;
#include "t35.h"

#include "t2181MD.h"
#define m1526(__this, p0, p1, method) (void)m10796_gshared((t2181 *)__this, (t29 *)p0, (t35)p1, method)
#define m12728(__this, p0, p1, method) (int32_t)m10797_gshared((t2181 *)__this, (t29 *)p0, (t29 *)p1, method)
#define m12729(__this, p0, p1, p2, p3, method) (t29 *)m10798_gshared((t2181 *)__this, (t29 *)p0, (t29 *)p1, (t67 *)p2, (t29 *)p3, method)
#define m12730(__this, p0, method) (int32_t)m10799_gshared((t2181 *)__this, (t29 *)p0, method)
