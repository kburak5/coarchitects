﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3192;
struct t7;

 void m17749 (t3192 * __this, t7* p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m17750 (t3192 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17751 (t3192 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17752 (t3192 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17753 (t3192 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m17754 (t3192 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
