﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t938;
struct t20;

 int32_t m8878 (t29 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4055 (t29 * __this, t20 * p0, int32_t p1, t20 * p2, int32_t p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8879 (t29 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8880 (t29 * __this, t20 * p0, int32_t p1, t20 * p2, int32_t p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
