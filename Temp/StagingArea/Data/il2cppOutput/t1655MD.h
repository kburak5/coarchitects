﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1655;
struct t629;

 void m9309 (t1655 * __this, t629 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t629 * m9310 (t1655 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9311 (t1655 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
