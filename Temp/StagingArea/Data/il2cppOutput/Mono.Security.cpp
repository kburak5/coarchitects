﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
#include "stringLiterals.h"

extern TypeInfo t968_TI;
extern TypeInfo t969_TI;
extern TypeInfo t970_TI;
extern TypeInfo t971_TI;
extern TypeInfo t973_TI;
extern TypeInfo t972_TI;
extern TypeInfo t977_TI;
extern TypeInfo t978_TI;
extern TypeInfo t979_TI;
extern TypeInfo t981_TI;
extern TypeInfo t793_TI;
extern TypeInfo t942_TI;
extern TypeInfo t982_TI;
extern TypeInfo t983_TI;
extern TypeInfo t984_TI;
extern TypeInfo t985_TI;
extern TypeInfo t986_TI;
extern TypeInfo t954_TI;
extern TypeInfo t989_TI;
extern TypeInfo t990_TI;
extern TypeInfo t991_TI;
extern TypeInfo t992_TI;
extern TypeInfo t993_TI;
extern TypeInfo t994_TI;
extern TypeInfo t995_TI;
extern TypeInfo t987_TI;
extern TypeInfo t998_TI;
extern TypeInfo t933_TI;
extern TypeInfo t1000_TI;
extern TypeInfo t1001_TI;
extern TypeInfo t944_TI;
extern TypeInfo t940_TI;
extern TypeInfo t780_TI;
extern TypeInfo t951_TI;
extern TypeInfo t945_TI;
extern TypeInfo t1002_TI;
extern TypeInfo t1003_TI;
extern TypeInfo t810_TI;
extern TypeInfo t808_TI;
extern TypeInfo t809_TI;
extern TypeInfo t949_TI;
extern TypeInfo t822_TI;
extern TypeInfo t952_TI;
extern TypeInfo t823_TI;
extern TypeInfo t950_TI;
extern TypeInfo t1004_TI;
extern TypeInfo t1005_TI;
extern TypeInfo t1006_TI;
extern TypeInfo t1007_TI;
extern TypeInfo t1008_TI;
extern TypeInfo t1009_TI;
extern TypeInfo t1010_TI;
extern TypeInfo t1011_TI;
extern TypeInfo t1012_TI;
extern TypeInfo t1014_TI;
extern TypeInfo t1015_TI;
extern TypeInfo t1016_TI;
extern TypeInfo t1017_TI;
extern TypeInfo t1018_TI;
extern TypeInfo t1019_TI;
extern TypeInfo t1025_TI;
extern TypeInfo t1029_TI;
extern TypeInfo t1030_TI;
extern TypeInfo t1032_TI;
extern TypeInfo t1038_TI;
extern TypeInfo t1039_TI;
extern TypeInfo t1024_TI;
extern TypeInfo t1020_TI;
extern TypeInfo t1022_TI;
extern TypeInfo t1044_TI;
extern TypeInfo t1021_TI;
extern TypeInfo t1045_TI;
extern TypeInfo t1048_TI;
extern TypeInfo t1051_TI;
extern TypeInfo t1033_TI;
extern TypeInfo t1052_TI;
extern TypeInfo t1054_TI;
extern TypeInfo t1043_TI;
extern TypeInfo t1042_TI;
extern TypeInfo t1026_TI;
extern TypeInfo t1056_TI;
extern TypeInfo t1057_TI;
extern TypeInfo t1031_TI;
extern TypeInfo t1028_TI;
extern TypeInfo t1061_TI;
extern TypeInfo t1062_TI;
extern TypeInfo t1060_TI;
extern TypeInfo t1027_TI;
extern TypeInfo t1041_TI;
extern TypeInfo t1065_TI;
extern TypeInfo t1040_TI;
extern TypeInfo t1036_TI;
extern TypeInfo t1067_TI;
extern TypeInfo t1035_TI;
extern TypeInfo t1037_TI;
extern TypeInfo t1068_TI;
extern TypeInfo t1069_TI;
extern TypeInfo t1070_TI;
extern TypeInfo t1071_TI;
extern TypeInfo t1072_TI;
extern TypeInfo t1073_TI;
extern TypeInfo t1074_TI;
extern TypeInfo t1075_TI;
extern TypeInfo t1076_TI;
extern TypeInfo t1077_TI;
extern TypeInfo t1078_TI;
extern TypeInfo t980_TI;
extern TypeInfo t1058_TI;
extern TypeInfo t1059_TI;
extern TypeInfo t1046_TI;
extern TypeInfo t1047_TI;
extern TypeInfo t1079_TI;
extern TypeInfo t1080_TI;
extern TypeInfo t1081_TI;
extern TypeInfo t1082_TI;
extern TypeInfo t1083_TI;
extern TypeInfo t1084_TI;
extern TypeInfo t1085_TI;
extern TypeInfo t1086_TI;
extern TypeInfo t1087_TI;
extern TypeInfo t1088_TI;
#include "utils/RegisterRuntimeInitializeAndCleanup.h"
#include <map>
struct TypeInfo;
struct MethodInfo;
TypeInfo* g_Mono_Security_Assembly_Types[122] = 
{
	&t968_TI,
	&t969_TI,
	&t970_TI,
	&t971_TI,
	&t973_TI,
	&t972_TI,
	&t977_TI,
	&t978_TI,
	&t979_TI,
	&t981_TI,
	&t793_TI,
	&t942_TI,
	&t982_TI,
	&t983_TI,
	&t984_TI,
	&t985_TI,
	&t986_TI,
	&t954_TI,
	&t989_TI,
	&t990_TI,
	&t991_TI,
	&t992_TI,
	&t993_TI,
	&t994_TI,
	&t995_TI,
	&t987_TI,
	&t998_TI,
	&t933_TI,
	&t1000_TI,
	&t1001_TI,
	&t944_TI,
	&t940_TI,
	&t780_TI,
	&t951_TI,
	&t945_TI,
	&t1002_TI,
	&t1003_TI,
	&t810_TI,
	&t808_TI,
	&t809_TI,
	&t949_TI,
	&t822_TI,
	&t952_TI,
	&t823_TI,
	&t950_TI,
	&t1004_TI,
	&t1005_TI,
	&t1006_TI,
	&t1007_TI,
	&t1008_TI,
	&t1009_TI,
	&t1010_TI,
	&t1011_TI,
	&t1012_TI,
	&t1014_TI,
	&t1015_TI,
	&t1016_TI,
	&t1017_TI,
	&t1018_TI,
	&t1019_TI,
	&t1025_TI,
	&t1029_TI,
	&t1030_TI,
	&t1032_TI,
	&t1038_TI,
	&t1039_TI,
	&t1024_TI,
	&t1020_TI,
	&t1022_TI,
	&t1044_TI,
	&t1021_TI,
	&t1045_TI,
	&t1048_TI,
	&t1051_TI,
	&t1033_TI,
	&t1052_TI,
	&t1054_TI,
	&t1043_TI,
	&t1042_TI,
	&t1026_TI,
	&t1056_TI,
	&t1057_TI,
	&t1031_TI,
	&t1028_TI,
	&t1061_TI,
	&t1062_TI,
	&t1060_TI,
	&t1027_TI,
	&t1041_TI,
	&t1065_TI,
	&t1040_TI,
	&t1036_TI,
	&t1067_TI,
	&t1035_TI,
	&t1037_TI,
	&t1068_TI,
	&t1069_TI,
	&t1070_TI,
	&t1071_TI,
	&t1072_TI,
	&t1073_TI,
	&t1074_TI,
	&t1075_TI,
	&t1076_TI,
	&t1077_TI,
	&t1078_TI,
	&t980_TI,
	&t1058_TI,
	&t1059_TI,
	&t1046_TI,
	&t1047_TI,
	&t1079_TI,
	&t1080_TI,
	&t1081_TI,
	&t1082_TI,
	&t1083_TI,
	&t1084_TI,
	&t1085_TI,
	&t1086_TI,
	&t1087_TI,
	&t1088_TI,
	NULL,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern CustomAttributesCache g_Mono_Security_Assembly__CustomAttributeCache;
Il2CppAssembly g_Mono_Security_Assembly = 
{
	{ "Mono.Security", 0, 0, "\x0\x24\x0\x0\x4\x80\x0\x0\x94\x0\x0\x0\x6\x2\x0\x0\x0\x24\x0\x0\x52\x53\x41\x31\x0\x4\x0\x0\x1\x0\x1\x0\x79\x15\x99\x77\xD2\xD0\x3A\x8E\x6B\xEA\x7A\x2E\x74\xE8\xD1\xAF\xCC\x93\xE8\x85\x19\x74\x95\x2B\xB4\x80\xA1\x2C\x91\x34\x47\x4D\x4\x6\x24\x47\xC3\x7E\xE\x68\xC0\x80\x53\x6F\xCF\x3C\x3F\xBE\x2F\xF9\xC9\x79\xCE\x99\x84\x75\xE5\x6\xE8\xCE\x82\xDD\x5B\xF\x35\xD\xC1\xE\x93\xBF\x2E\xEE\xCF\x87\x4B\x24\x77\xC\x50\x81\xDB\xEA\x74\x47\xFD\xDA\xFA\x27\x7B\x22\xDE\x47\xD6\xFF\xEA\x44\x96\x74\xA4\xF9\xFC\xCF\x84\xD1\x50\x69\x8\x93\x80\x28\x4D\xBD\xD3\x5F\x46\xCD\xFF\x12\xA1\xBD\x78\xE4\xEF\x0\x65\xD0\x16\xDF", { 0x07, 0x38, 0xEB, 0x9F, 0x13, 0x2E, 0xD7, 0x56 }, 32772, 0, 1, 2, 0, 5, 0 },
	&g_Mono_Security_dll_Image,
	&g_Mono_Security_Assembly__CustomAttributeCache,
};
Il2CppImage g_Mono_Security_dll_Image = 
{
	 "Mono.Security.dll" ,
	&g_Mono_Security_Assembly,
	g_Mono_Security_Assembly_Types,
	121,
	NULL,
};
static void s_Mono_SecurityRegistration()
{
	RegisterAssembly (&g_Mono_Security_Assembly);
}
static il2cpp::utils::RegisterRuntimeInitializeAndCleanup s_Mono_SecurityRegistrationVariable(&s_Mono_SecurityRegistration, NULL);
