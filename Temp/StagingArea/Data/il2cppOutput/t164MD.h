﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t164;
struct t7;
struct t29;
#include "t17.h"
#include "t23.h"
#include "t164.h"

 void m1823 (t164 * __this, float p0, float p1, float p2, float p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1579 (t164 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1661 (t164 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1580 (t164 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1659 (t164 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m1669 (t164 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m1834 (t164 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1573 (t164 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1660 (t164 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1574 (t164 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1657 (t164 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m1653 (t164 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1707 (t164 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1706 (t164 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1679 (t164 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1681 (t164 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2342 (t164 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2343 (t164 * __this, t23  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2344 (t164 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2345 (t164 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1822 (t29 * __this, t164  p0, t164  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
