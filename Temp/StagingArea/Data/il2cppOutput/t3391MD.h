﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3391;
struct t29;
struct t20;
#include "t1384.h"

 void m18827 (t3391 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18828 (t3391 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18829 (t3391 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18830 (t3391 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18831 (t3391 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
