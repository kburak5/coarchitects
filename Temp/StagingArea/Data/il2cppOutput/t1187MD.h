﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1187;
struct t1187_marshaled;

 void m6122 (t1187 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
void t1187_marshal(const t1187& unmarshaled, t1187_marshaled& marshaled);
void t1187_marshal_back(const t1187_marshaled& marshaled, t1187& unmarshaled);
void t1187_marshal_cleanup(t1187_marshaled& marshaled);
