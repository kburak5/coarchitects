﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t210;
struct t139;
#include "t209.h"
#include "t210.h"

 int32_t m708 (t210 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m709 (t210 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t139 * m710 (t210 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m711 (t210 * __this, t139 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t139 * m712 (t210 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m713 (t210 * __this, t139 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t139 * m714 (t210 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m715 (t210 * __this, t139 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t139 * m716 (t210 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m717 (t210 * __this, t139 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t210  m718 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
