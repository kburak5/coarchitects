﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3508;
struct t29;
struct t20;
#include "t1127.h"

 void m19479 (t3508 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m19480 (t3508 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m19481 (t3508 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19482 (t3508 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m19483 (t3508 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
