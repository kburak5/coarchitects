﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1465;
struct t42;
struct t1469;
struct t29;
struct t7;
#include "t35.h"

 void m7947 (t1465 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7948 (t1465 * __this, t42 * p0, t1469 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7949 (t1465 * __this, t42 * p0, t35 p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7950 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7951 (t1465 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7952 (t1465 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7953 (t1465 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7954 (t1465 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
