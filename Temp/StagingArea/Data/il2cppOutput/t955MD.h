﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t955;
struct t781;
struct t7;

 void m5128 (t955 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5129 (t955 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5130 (t955 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4195 (t955 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m5156 (t955 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t955 * m5154 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m5133 (t955 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5134 (t955 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5135 (t955 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5131 (t955 * __this, t781* p0, int32_t p1, int32_t p2, t781* p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m5132 (t955 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
