﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1304;
struct t1034;
struct t943;
struct t781;
struct t200;
struct t7;
#include "t1126.h"

 void m6971 (t1304 * __this, t1034 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6972 (t1304 * __this, t1034 * p0, t943 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6973 (t1304 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6974 (t1304 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6975 (t1304 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6976 (t1304 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6977 (t1304 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6978 (t1304 * __this, t200* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6979 (t1304 * __this, t200* p0, int32_t p1, int32_t p2, int32_t* p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6980 (t1304 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6981 (t1304 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m6982 (t1304 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m6983 (t1304 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m6984 (t1304 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m6985 (t1304 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m6986 (t1304 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6987 (t1304 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m6988 (t1304 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int8_t m6989 (t1304 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6990 (t1304 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m6991 (t1304 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m6992 (t1304 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m6993 (t1304 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m6994 (t1304 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6995 (t1304 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
