﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1545;
struct t1116;
struct t781;

 void m8370 (t1545 * __this, t1116 * p0, bool p1, t781* p2, t781* p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8371 (t1545 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8372 (t1545 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8373 (t1545 * __this, t781* p0, int32_t p1, int32_t p2, t781* p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m8374 (t1545 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
