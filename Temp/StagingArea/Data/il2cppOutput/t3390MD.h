﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3390;
struct t29;
struct t20;
#include "t1370.h"

 void m18822 (t3390 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18823 (t3390 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18824 (t3390 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18825 (t3390 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18826 (t3390 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
