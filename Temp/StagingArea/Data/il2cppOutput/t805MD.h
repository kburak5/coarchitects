﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t805;
struct t805_marshaled;
struct t7;
#include "t811.h"

 void m3411 (t805 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3412 (t805 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3413 (t805 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3414 (t805 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3415 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
void t805_marshal(const t805& unmarshaled, t805_marshaled& marshaled);
void t805_marshal_back(const t805_marshaled& marshaled, t805& unmarshaled);
void t805_marshal_cleanup(t805_marshaled& marshaled);
