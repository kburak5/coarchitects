﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t63;
struct t29;
struct t60;
struct t2300;
struct t20;
struct t136;
struct t2301;
struct t2302;
struct t2303;
struct t2299;
struct t2304;
struct t2305;
#include "t2306.h"

#include "t294MD.h"
#define m11630(__this, method) (void)m10537_gshared((t294 *)__this, method)
#define m11631(__this, p0, method) (void)m10539_gshared((t294 *)__this, (int32_t)p0, method)
#define m11632(__this, method) (void)m10541_gshared((t29 *)__this, method)
#define m11633(__this, method) (t29*)m10543_gshared((t294 *)__this, method)
#define m11634(__this, p0, p1, method) (void)m10545_gshared((t294 *)__this, (t20 *)p0, (int32_t)p1, method)
#define m11635(__this, method) (t29 *)m10547_gshared((t294 *)__this, method)
#define m11636(__this, p0, method) (int32_t)m10549_gshared((t294 *)__this, (t29 *)p0, method)
#define m11637(__this, p0, method) (bool)m10551_gshared((t294 *)__this, (t29 *)p0, method)
#define m11638(__this, p0, method) (int32_t)m10553_gshared((t294 *)__this, (t29 *)p0, method)
#define m11639(__this, p0, p1, method) (void)m10555_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m11640(__this, p0, method) (void)m10557_gshared((t294 *)__this, (t29 *)p0, method)
#define m11641(__this, method) (bool)m10559_gshared((t294 *)__this, method)
#define m11642(__this, method) (bool)m10561_gshared((t294 *)__this, method)
#define m11643(__this, method) (t29 *)m10563_gshared((t294 *)__this, method)
#define m11644(__this, method) (bool)m10565_gshared((t294 *)__this, method)
#define m11645(__this, method) (bool)m10567_gshared((t294 *)__this, method)
#define m11646(__this, p0, method) (t29 *)m10569_gshared((t294 *)__this, (int32_t)p0, method)
#define m11647(__this, p0, p1, method) (void)m10571_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m11648(__this, p0, method) (void)m10573_gshared((t294 *)__this, (t29 *)p0, method)
#define m11649(__this, p0, method) (void)m10575_gshared((t294 *)__this, (int32_t)p0, method)
#define m11650(__this, p0, method) (void)m10577_gshared((t294 *)__this, (t29*)p0, method)
#define m11651(__this, p0, method) (void)m10579_gshared((t294 *)__this, (t29*)p0, method)
#define m11652(__this, p0, method) (void)m10581_gshared((t294 *)__this, (t29*)p0, method)
#define m11653(__this, method) (t2303 *)m10583_gshared((t294 *)__this, method)
#define m11654(__this, method) (void)m10585_gshared((t294 *)__this, method)
#define m11655(__this, p0, method) (bool)m10587_gshared((t294 *)__this, (t29 *)p0, method)
#define m11656(__this, p0, p1, method) (void)m10589_gshared((t294 *)__this, (t316*)p0, (int32_t)p1, method)
#define m11657(__this, p0, method) (t60 *)m10591_gshared((t294 *)__this, (t2180 *)p0, method)
#define m11658(__this, p0, method) (void)m10593_gshared((t29 *)__this, (t2180 *)p0, method)
#define m11659(__this, p0, p1, p2, method) (int32_t)m10595_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, (t2180 *)p2, method)
 t2306  m11660 (t63 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
#define m11661(__this, p0, method) (int32_t)m10598_gshared((t294 *)__this, (t29 *)p0, method)
#define m11662(__this, p0, p1, method) (void)m10600_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, method)
#define m11663(__this, p0, method) (void)m10602_gshared((t294 *)__this, (int32_t)p0, method)
#define m11664(__this, p0, p1, method) (void)m10604_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m11665(__this, p0, method) (void)m10606_gshared((t294 *)__this, (t29*)p0, method)
#define m11666(__this, p0, method) (bool)m10608_gshared((t294 *)__this, (t29 *)p0, method)
#define m11667(__this, p0, method) (int32_t)m10610_gshared((t294 *)__this, (t2180 *)p0, method)
#define m11668(__this, p0, method) (void)m10611_gshared((t294 *)__this, (int32_t)p0, method)
#define m11669(__this, method) (void)m10613_gshared((t294 *)__this, method)
#define m11670(__this, method) (void)m10615_gshared((t294 *)__this, method)
#define m11671(__this, p0, method) (void)m10617_gshared((t294 *)__this, (t2181 *)p0, method)
#define m11672(__this, method) (t2299*)m10619_gshared((t294 *)__this, method)
#define m11673(__this, method) (void)m10621_gshared((t294 *)__this, method)
#define m11674(__this, method) (int32_t)m10623_gshared((t294 *)__this, method)
#define m11675(__this, p0, method) (void)m10625_gshared((t294 *)__this, (int32_t)p0, method)
#define m1333(__this, method) (int32_t)m10626_gshared((t294 *)__this, method)
#define m1334(__this, p0, method) (t60 *)m10627_gshared((t294 *)__this, (int32_t)p0, method)
#define m11676(__this, p0, p1, method) (void)m10629_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
