﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t259;
struct t263;
struct t2;
struct t264;
#include "t150.h"

 void m1137 (t259 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t263 * m1138 (t259 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1139 (t259 * __this, t263 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1140 (t259 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1141 (t259 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2 * m1142 (t259 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t264 * m1143 (t259 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1144 (t259 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1145 (t259 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1146 (t259 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1147 (t259 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1148 (t259 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1149 (t259 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1150 (t259 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1151 (t259 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1152 (t259 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1153 (t259 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1154 (t259 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1155 (t259 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1156 (t259 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1157 (t259 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1158 (t259 * __this, int32_t p0, float p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1159 (t259 * __this, float p0, float p1, float p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1160 (t259 * __this, t2 * p0, int32_t p1, float p2, float p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1161 (t259 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1162 (t259 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1163 (t259 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1164 (t259 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
