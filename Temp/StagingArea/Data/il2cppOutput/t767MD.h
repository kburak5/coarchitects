﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t767;
struct t748;
struct t29;

 void m3232 (t767 * __this, t748 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3233 (t767 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3234 (t767 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
