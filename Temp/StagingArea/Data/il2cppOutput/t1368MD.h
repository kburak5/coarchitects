﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1368;
struct t29;
struct t353;
struct t66;
struct t67;
#include "t35.h"

 void m7517 (t1368 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7518 (t1368 * __this, t29 * p0, t353 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7519 (t1368 * __this, t29 * p0, t353 * p1, t67 * p2, t29 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7520 (t1368 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
