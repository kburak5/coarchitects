﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1345;
struct t42;
struct t7;
struct t1346;
struct t1142;
struct t316;
struct t29;
struct t631;
struct t633;
struct t295;
#include "t1347.h"
#include "t927.h"
#include "t630.h"

 int32_t m7319 (t1345 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7320 (t1345 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t927  m7321 (t1345 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7322 (t1345 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7323 (t1345 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7324 (t1345 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m7325 (t1345 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m7326 (t1345 * __this, t42 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7327 (t1345 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7328 (t1345 * __this, t42 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7329 (t1345 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7330 (t1345 * __this, t29 * p0, t29 * p1, int32_t p2, t631 * p3, t633 * p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1346 * m7331 (t1345 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t295 * m7332 (t1345 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1142 * m7333 (t1345 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
