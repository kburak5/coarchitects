﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2502;
struct t29;
struct t350;

#include "t2001MD.h"
#define m13218(__this, method) (void)m10703_gshared((t2001 *)__this, method)
#define m13219(__this, method) (void)m10704_gshared((t29 *)__this, method)
#define m13220(__this, p0, method) (int32_t)m10705_gshared((t2001 *)__this, (t29 *)p0, method)
#define m13221(__this, p0, p1, method) (bool)m10706_gshared((t2001 *)__this, (t29 *)p0, (t29 *)p1, method)
#define m13222(__this, method) (t2502 *)m10707_gshared((t29 *)__this, method)
