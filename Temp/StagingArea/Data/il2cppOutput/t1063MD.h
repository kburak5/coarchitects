﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1063;
struct t781;
#include "t1064.h"

 void m5250 (t1063 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5273 (t1063 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5274 (t1063 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7080 (t1063 * __this, t781* p0, int32_t p1, int32_t p2, bool p3, bool p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7081 (t1063 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7082 (t1063 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5272 (t1063 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7083 (t1063 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7084 (t1063 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m5259 (t1063 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m5258 (t1063 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5262 (t1063 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7085 (t1063 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5276 (t1063 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5260 (t1063 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7086 (t1063 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m5263 (t1063 * __this, int64_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7087 (t1063 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7088 (t1063 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5264 (t1063 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m5275 (t1063 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5261 (t1063 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7089 (t1063 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
