﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2908;
struct t29;
struct t7;
struct t66;
struct t67;
#include "t35.h"
#include "t725.h"

 void m15992 (t2908 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t725  m15993 (t2908 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15994 (t2908 * __this, t7* p0, int32_t p1, t67 * p2, t29 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t725  m15995 (t2908 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
