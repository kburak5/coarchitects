﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1229;
struct t1219;
struct t7;
#include "t465.h"

 int32_t m6501 (t29 * __this, t1219 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6502 (t29 * __this, t1219 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m6503 (t29 * __this, t1219 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
