﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2260;
struct t29;
struct t20;
struct t136;
struct t2251;
struct t2252;
struct t2259;
#include "t57.h"

 void m11344 (t2260 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m11345 (t2260 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11346 (t2260 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m11347 (t2260 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m11348 (t2260 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m11349 (t2260 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m11350 (t2260 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11351 (t2260 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11352 (t2260 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m11353 (t2260 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m11354 (t2260 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m11355 (t2260 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m11356 (t2260 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m11357 (t2260 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11358 (t2260 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11359 (t2260 * __this, t57  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11360 (t2260 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11361 (t2260 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m11362 (t2260 * __this, t57  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11363 (t2260 * __this, t2251* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m11364 (t2260 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m11365 (t2260 * __this, t57  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11366 (t2260 * __this, int32_t p0, t57  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11367 (t2260 * __this, int32_t p0, t57  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m11368 (t2260 * __this, t57  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11369 (t2260 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11370 (t2260 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m11371 (t2260 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t57  m11372 (t2260 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11373 (t2260 * __this, int32_t p0, t57  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11374 (t2260 * __this, int32_t p0, t57  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m11375 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t57  m11376 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11377 (t29 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m11378 (t29 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m11379 (t29 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
