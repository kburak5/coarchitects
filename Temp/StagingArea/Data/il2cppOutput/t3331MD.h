﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3331;
struct t29;
struct t20;
#include "t1280.h"

 void m18527 (t3331 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18528 (t3331 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18529 (t3331 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18530 (t3331 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18531 (t3331 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
