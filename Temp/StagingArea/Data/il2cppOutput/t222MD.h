﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t222;
struct t2;
struct t217;
struct t220;
struct t6;
struct t25;
#include "t219.h"
#include "t17.h"
#include "t140.h"
#include "t224.h"

 void m775 (t222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2 * m776 (t222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m777 (t222 * __this, t2 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m778 (t222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m779 (t222 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m780 (t222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m781 (t222 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m782 (t222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m783 (t222 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m784 (t222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m785 (t222 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m786 (t222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m787 (t222 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m788 (t222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m789 (t222 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m790 (t222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m791 (t222 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t217 * m792 (t222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m793 (t222 * __this, t217 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t217 * m794 (t222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m795 (t222 * __this, t217 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t220 * m796 (t222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m797 (t222 * __this, t220 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2 * m798 (t222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m799 (t222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m800 (t222 * __this, t17  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m801 (t222 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m802 (t222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m803 (t222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m804 (t222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m805 (t222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m806 (t222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m807 (t222 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m808 (t222 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m809 (t222 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m810 (t222 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m811 (t222 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m812 (t222 * __this, t17  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m813 (t222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m814 (t222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m815 (t222 * __this, t17  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m816 (t222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m817 (t222 * __this, t17  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m818 (t222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m819 (t222 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m820 (t222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m821 (t222 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m822 (t222 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m823 (t222 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m824 (t222 * __this, float p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m825 (t29 * __this, float p0, float p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m826 (t222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t224  m827 (t222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m828 (t222 * __this, t17  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m829 (t222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t25 * m830 (t222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
