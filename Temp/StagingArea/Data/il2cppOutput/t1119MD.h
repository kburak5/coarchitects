﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1119;
struct t633;
struct t29;
struct t7;
struct t1192;
#include "t1120.h"

 void m6808 (t1119 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6809 (t1119 * __this, t633 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6810 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6811 (t1119 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6812 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6813 (t1119 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6814 (t1119 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6815 (t1119 * __this, t7* p0, int32_t p1, int32_t p2, t7* p3, int32_t p4, int32_t p5, int32_t p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6816 (t1119 * __this, t29 * p0, t7* p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6817 (t1119 * __this, t7* p0, int32_t p1, int32_t p2, t7* p3, int32_t p4, bool p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6818 (t1119 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6819 (t1119 * __this, t7* p0, int32_t p1, int32_t p2, t7* p3, int32_t p4, int32_t p5, int32_t p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6820 (t1119 * __this, t7* p0, int32_t p1, int32_t p2, t7* p3, int32_t p4, int32_t p5, int32_t p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6821 (t1119 * __this, t7* p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5214 (t1119 * __this, t7* p0, t7* p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6822 (t1119 * __this, t7* p0, int32_t p1, int32_t p2, t7* p3, int32_t p4, int32_t p5, int32_t p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6823 (t1119 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6824 (t1119 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1192 * m6825 (t1119 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6826 (t1119 * __this, t7* p0, t7* p1, int32_t p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6827 (t1119 * __this, t7* p0, int32_t p1, int32_t p2, t7* p3, int32_t p4, bool p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6828 (t1119 * __this, t7* p0, int32_t p1, int32_t p2, t7* p3, int32_t p4, bool p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6829 (t1119 * __this, t7* p0, t7* p1, int32_t p2, int32_t p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6830 (t1119 * __this, t7* p0, t7* p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6831 (t1119 * __this, t7* p0, t7* p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6832 (t1119 * __this, t7* p0, t7* p1, int32_t p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6833 (t1119 * __this, t7* p0, t7* p1, int32_t p2, int32_t p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6834 (t1119 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6835 (t1119 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
