﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2213;
struct t29;
struct t53;
struct t66;
struct t67;
#include "t35.h"

 void m10902_gshared (t2213 * __this, t29 * p0, t35 p1, MethodInfo* method);
#define m10902(__this, p0, p1, method) (void)m10902_gshared((t2213 *)__this, (t29 *)p0, (t35)p1, method)
 void m10904_gshared (t2213 * __this, t29 * p0, t53 * p1, MethodInfo* method);
#define m10904(__this, p0, p1, method) (void)m10904_gshared((t2213 *)__this, (t29 *)p0, (t53 *)p1, method)
 t29 * m10906_gshared (t2213 * __this, t29 * p0, t53 * p1, t67 * p2, t29 * p3, MethodInfo* method);
#define m10906(__this, p0, p1, p2, p3, method) (t29 *)m10906_gshared((t2213 *)__this, (t29 *)p0, (t53 *)p1, (t67 *)p2, (t29 *)p3, method)
 void m10908_gshared (t2213 * __this, t29 * p0, MethodInfo* method);
#define m10908(__this, p0, method) (void)m10908_gshared((t2213 *)__this, (t29 *)p0, method)
