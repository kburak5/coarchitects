﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1025;
struct t29;
struct t1019;
struct t7;
struct t136;
struct t20;
struct t1027;
struct t1028;
#include "t1026.h"
#include "t1018.h"
#include "t1021.h"
#include "t1022.h"

 void m4646 (t1025 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m4647 (t1025 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4648 (t1025 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4649 (t1025 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m4650 (t1025 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m4651 (t1025 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4652 (t1025 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4653 (t1025 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4654 (t1025 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4655 (t1025 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4656 (t1025 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4657 (t1025 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1019 * m4658 (t1025 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1019 * m4659 (t1025 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4660 (t1025 * __this, int32_t p0, t1019 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1019 * m4661 (t1025 * __this, int16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4662 (t1025 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4663 (t1025 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4664 (t1025 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4665 (t1025 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4666 (t1025 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4667 (t1025 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4668 (t1025 * __this, int16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1019 * m4669 (t1025 * __this, int16_t p0, t7* p1, int32_t p2, int32_t p3, int32_t p4, bool p5, bool p6, uint8_t p7, uint8_t p8, int16_t p9, uint8_t p10, uint8_t p11, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1027 * m4670 (t1025 * __this, t1027 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1028 * m4671 (t1025 * __this, t1028 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4672 (t1025 * __this, t7* p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
