﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t746;
struct t750;
struct t733;
struct t7;
struct t295;
struct t42;
#include "t735.h"

 void m3269 (t746 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3270 (t746 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3271 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3272 (t746 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3273 (t29 * __this, t7* p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t295 * m3274 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3275 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3276 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3277 (t746 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3278 (t29 * __this, t7* p0, t42 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
