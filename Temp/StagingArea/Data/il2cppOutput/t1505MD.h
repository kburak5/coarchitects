﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1505;
struct t29;
struct t42;
struct t7;

 void m8079 (t1505 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m8080 (t1505 * __this, t29 * p0, t42 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8081 (t1505 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m8082 (t1505 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8083 (t1505 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m8084 (t1505 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8085 (t1505 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
