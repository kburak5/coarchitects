﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t3510_TI;


#include "t20.h"

// Metadata Definition BackgroundMove[]
static MethodInfo* t3510_MIs[] =
{
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m1332_MI;
extern MethodInfo m4200_MI;
extern MethodInfo m5904_MI;
extern MethodInfo m5920_MI;
extern MethodInfo m5921_MI;
extern MethodInfo m4199_MI;
extern MethodInfo m5922_MI;
extern MethodInfo m5923_MI;
extern MethodInfo m5895_MI;
extern MethodInfo m5896_MI;
extern MethodInfo m5897_MI;
extern MethodInfo m5898_MI;
extern MethodInfo m5899_MI;
extern MethodInfo m5900_MI;
extern MethodInfo m5901_MI;
extern MethodInfo m5902_MI;
extern MethodInfo m5903_MI;
extern MethodInfo m4006_MI;
extern MethodInfo m5905_MI;
extern MethodInfo m5906_MI;
extern MethodInfo m19493_MI;
extern MethodInfo m5907_MI;
extern MethodInfo m19495_MI;
extern MethodInfo m19497_MI;
extern MethodInfo m19505_MI;
extern MethodInfo m19506_MI;
extern MethodInfo m19507_MI;
extern MethodInfo m5908_MI;
extern MethodInfo m19492_MI;
extern MethodInfo m19509_MI;
extern MethodInfo m19510_MI;
extern MethodInfo m19513_MI;
extern MethodInfo m19514_MI;
extern MethodInfo m19515_MI;
extern MethodInfo m19516_MI;
extern MethodInfo m19517_MI;
extern MethodInfo m19518_MI;
extern MethodInfo m19512_MI;
extern MethodInfo m19520_MI;
extern MethodInfo m19521_MI;
extern MethodInfo m19524_MI;
extern MethodInfo m19525_MI;
extern MethodInfo m19526_MI;
extern MethodInfo m19527_MI;
extern MethodInfo m19528_MI;
extern MethodInfo m19529_MI;
extern MethodInfo m19523_MI;
extern MethodInfo m19531_MI;
extern MethodInfo m19532_MI;
extern MethodInfo m19535_MI;
extern MethodInfo m19536_MI;
extern MethodInfo m19537_MI;
extern MethodInfo m19538_MI;
extern MethodInfo m19539_MI;
extern MethodInfo m19540_MI;
extern MethodInfo m19534_MI;
extern MethodInfo m19542_MI;
extern MethodInfo m19543_MI;
extern MethodInfo m19546_MI;
extern MethodInfo m19547_MI;
extern MethodInfo m19548_MI;
extern MethodInfo m19549_MI;
extern MethodInfo m19550_MI;
extern MethodInfo m19551_MI;
extern MethodInfo m19545_MI;
extern MethodInfo m19553_MI;
extern MethodInfo m19554_MI;
extern MethodInfo m19494_MI;
extern MethodInfo m19496_MI;
extern MethodInfo m19498_MI;
extern MethodInfo m19499_MI;
extern MethodInfo m19500_MI;
extern MethodInfo m19501_MI;
extern MethodInfo m19490_MI;
extern MethodInfo m19503_MI;
extern MethodInfo m19504_MI;
static MethodInfo* t3510_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19493_MI,
	&m5907_MI,
	&m19495_MI,
	&m19497_MI,
	&m19505_MI,
	&m19506_MI,
	&m19507_MI,
	&m5908_MI,
	&m19492_MI,
	&m19509_MI,
	&m19510_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5056_TI;
extern TypeInfo t5057_TI;
extern TypeInfo t5058_TI;
extern TypeInfo t5059_TI;
extern TypeInfo t5060_TI;
extern TypeInfo t5061_TI;
extern TypeInfo t5062_TI;
extern TypeInfo t5063_TI;
extern TypeInfo t5064_TI;
extern TypeInfo t2240_TI;
extern TypeInfo t2245_TI;
extern TypeInfo t2241_TI;
extern TypeInfo t5065_TI;
extern TypeInfo t5066_TI;
extern TypeInfo t5067_TI;
extern TypeInfo t2182_TI;
extern TypeInfo t2186_TI;
extern TypeInfo t2183_TI;
static TypeInfo* t3510_ITIs[] = 
{
	&t5056_TI,
	&t5057_TI,
	&t5058_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
extern TypeInfo t603_TI;
extern TypeInfo t373_TI;
extern TypeInfo t674_TI;
extern TypeInfo t868_TI;
static Il2CppInterfaceOffsetPair t3510_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5056_TI, 21},
	{ &t5057_TI, 28},
	{ &t5058_TI, 33},
	{ &t5059_TI, 34},
	{ &t5060_TI, 41},
	{ &t5061_TI, 46},
	{ &t5062_TI, 47},
	{ &t5063_TI, 54},
	{ &t5064_TI, 59},
	{ &t2240_TI, 60},
	{ &t2245_TI, 67},
	{ &t2241_TI, 72},
	{ &t5065_TI, 73},
	{ &t5066_TI, 80},
	{ &t5067_TI, 85},
	{ &t2182_TI, 86},
	{ &t2186_TI, 93},
	{ &t2183_TI, 98},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType t3510_0_0_0;
extern Il2CppType t3510_1_0_0;
extern TypeInfo t20_TI;
struct t1;
extern TypeInfo t1_TI;
TypeInfo t3510_TI = 
{
	&g_AssemblyU2DCSharp_dll_Image, NULL, "BackgroundMove[]", "", t3510_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1_TI, t3510_ITIs, t3510_VT, &EmptyCustomAttributesCache, &t3510_TI, &t3510_0_0_0, &t3510_1_0_0, t3510_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 99, 18, 22};
#include "mscorlib_ArrayTypes.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t316_TI;



// Metadata Definition System.Object[]
static MethodInfo* t316_MIs[] =
{
	NULL
};
static MethodInfo* t316_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
static TypeInfo* t316_ITIs[] = 
{
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t316_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2182_TI, 21},
	{ &t2186_TI, 28},
	{ &t2183_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t316_0_0_0;
extern Il2CppType t316_1_0_0;
struct t29;
extern TypeInfo t29_TI;
extern CustomAttributesCache t29__CustomAttributeCache;
extern CustomAttributesCache t29__CustomAttributeCache_m1331;
extern CustomAttributesCache t29__CustomAttributeCache_m46;
extern CustomAttributesCache t29__CustomAttributeCache_m2868;
TypeInfo t316_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Object[]", "System", t316_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t29_TI, t316_ITIs, t316_VT, &EmptyCustomAttributesCache, &t316_TI, &t316_0_0_0, &t316_1_0_0, t316_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 1056769, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#include "UnityEngine_ArrayTypes.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3728_TI;



// Metadata Definition UnityEngine.MonoBehaviour[]
static MethodInfo* t3728_MIs[] =
{
	NULL
};
static MethodInfo* t3728_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
static TypeInfo* t3728_ITIs[] = 
{
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3728_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5059_TI, 21},
	{ &t5060_TI, 28},
	{ &t5061_TI, 33},
	{ &t5062_TI, 34},
	{ &t5063_TI, 41},
	{ &t5064_TI, 46},
	{ &t2240_TI, 47},
	{ &t2245_TI, 54},
	{ &t2241_TI, 59},
	{ &t5065_TI, 60},
	{ &t5066_TI, 67},
	{ &t5067_TI, 72},
	{ &t2182_TI, 73},
	{ &t2186_TI, 80},
	{ &t2183_TI, 85},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3728_0_0_0;
extern Il2CppType t3728_1_0_0;
struct t4;
extern TypeInfo t4_TI;
extern CustomAttributesCache t4__CustomAttributeCache_m2538;
extern CustomAttributesCache t4__CustomAttributeCache_m2540;
extern CustomAttributesCache t4__CustomAttributeCache_m2541;
TypeInfo t3728_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "MonoBehaviour[]", "UnityEngine", t3728_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t4_TI, t3728_ITIs, t3728_VT, &EmptyCustomAttributesCache, &t3728_TI, &t3728_0_0_0, &t3728_1_0_0, t3728_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t4 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 86, 15, 19};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3729_TI;



// Metadata Definition UnityEngine.Behaviour[]
static MethodInfo* t3729_MIs[] =
{
	NULL
};
static MethodInfo* t3729_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
static TypeInfo* t3729_ITIs[] = 
{
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3729_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5062_TI, 21},
	{ &t5063_TI, 28},
	{ &t5064_TI, 33},
	{ &t2240_TI, 34},
	{ &t2245_TI, 41},
	{ &t2241_TI, 46},
	{ &t5065_TI, 47},
	{ &t5066_TI, 54},
	{ &t5067_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3729_0_0_0;
extern Il2CppType t3729_1_0_0;
struct t317;
extern TypeInfo t317_TI;
extern CustomAttributesCache t317__CustomAttributeCache_m1390;
extern CustomAttributesCache t317__CustomAttributeCache_m1773;
extern CustomAttributesCache t317__CustomAttributeCache_m1391;
TypeInfo t3729_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Behaviour[]", "UnityEngine", t3729_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t317_TI, t3729_ITIs, t3729_VT, &EmptyCustomAttributesCache, &t3729_TI, &t3729_0_0_0, &t3729_1_0_0, t3729_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t317 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2238_TI;



// Metadata Definition UnityEngine.Component[]
static MethodInfo* t2238_MIs[] =
{
	NULL
};
static MethodInfo* t2238_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
static TypeInfo* t2238_ITIs[] = 
{
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2238_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2240_TI, 21},
	{ &t2245_TI, 28},
	{ &t2241_TI, 33},
	{ &t5065_TI, 34},
	{ &t5066_TI, 41},
	{ &t5067_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2238_0_0_0;
extern Il2CppType t2238_1_0_0;
struct t28;
extern TypeInfo t28_TI;
extern CustomAttributesCache t28__CustomAttributeCache_m39;
extern CustomAttributesCache t28__CustomAttributeCache_m1392;
extern CustomAttributesCache t28__CustomAttributeCache_m1987;
extern CustomAttributesCache t28__CustomAttributeCache_m2889;
extern CustomAttributesCache t28__CustomAttributeCache_m2552;
TypeInfo t2238_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Component[]", "UnityEngine", t2238_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t28_TI, t2238_ITIs, t2238_VT, &EmptyCustomAttributesCache, &t2238_TI, &t2238_0_0_0, &t2238_1_0_0, t2238_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t28 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t444_TI;



// Metadata Definition UnityEngine.Object[]
static MethodInfo* t444_MIs[] =
{
	NULL
};
static MethodInfo* t444_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
static TypeInfo* t444_ITIs[] = 
{
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t444_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5065_TI, 21},
	{ &t5066_TI, 28},
	{ &t5067_TI, 33},
	{ &t2182_TI, 34},
	{ &t2186_TI, 41},
	{ &t2183_TI, 46},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t444_0_0_0;
extern Il2CppType t444_1_0_0;
struct t41;
extern TypeInfo t41_TI;
extern CustomAttributesCache t41__CustomAttributeCache_m2545;
extern CustomAttributesCache t41__CustomAttributeCache_m67;
extern CustomAttributesCache t41__CustomAttributeCache_m2546;
extern CustomAttributesCache t41__CustomAttributeCache_m1820;
extern CustomAttributesCache t41__CustomAttributeCache_m1385;
extern CustomAttributesCache t41__CustomAttributeCache_m1908;
extern CustomAttributesCache t41__CustomAttributeCache_m1778;
extern CustomAttributesCache t41__CustomAttributeCache_m48;
TypeInfo t444_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Object[]", "UnityEngine", t444_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t41_TI, t444_ITIs, t444_VT, &EmptyCustomAttributesCache, &t444_TI, &t444_0_0_0, &t444_1_0_0, t444_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t41 *), -1, 0, 0, -1, 1048585, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3511_TI;



// Metadata Definition ButtonScale[]
static MethodInfo* t3511_MIs[] =
{
	NULL
};
extern MethodInfo m19559_MI;
extern MethodInfo m19560_MI;
extern MethodInfo m19561_MI;
extern MethodInfo m19562_MI;
extern MethodInfo m19563_MI;
extern MethodInfo m19564_MI;
extern MethodInfo m19558_MI;
extern MethodInfo m19566_MI;
extern MethodInfo m19567_MI;
extern MethodInfo m19570_MI;
extern MethodInfo m19571_MI;
extern MethodInfo m19572_MI;
extern MethodInfo m19573_MI;
extern MethodInfo m19574_MI;
extern MethodInfo m19575_MI;
extern MethodInfo m19569_MI;
extern MethodInfo m19577_MI;
extern MethodInfo m19578_MI;
extern MethodInfo m19581_MI;
extern MethodInfo m19582_MI;
extern MethodInfo m19583_MI;
extern MethodInfo m19584_MI;
extern MethodInfo m19585_MI;
extern MethodInfo m19586_MI;
extern MethodInfo m19580_MI;
extern MethodInfo m19588_MI;
extern MethodInfo m19589_MI;
extern MethodInfo m19592_MI;
extern MethodInfo m19593_MI;
extern MethodInfo m19594_MI;
extern MethodInfo m19595_MI;
extern MethodInfo m19596_MI;
extern MethodInfo m19597_MI;
extern MethodInfo m19591_MI;
extern MethodInfo m19599_MI;
extern MethodInfo m19600_MI;
static MethodInfo* t3511_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19559_MI,
	&m5907_MI,
	&m19560_MI,
	&m19561_MI,
	&m19562_MI,
	&m19563_MI,
	&m19564_MI,
	&m5908_MI,
	&m19558_MI,
	&m19566_MI,
	&m19567_MI,
	&m5905_MI,
	&m5906_MI,
	&m19570_MI,
	&m5907_MI,
	&m19571_MI,
	&m19572_MI,
	&m19573_MI,
	&m19574_MI,
	&m19575_MI,
	&m5908_MI,
	&m19569_MI,
	&m19577_MI,
	&m19578_MI,
	&m5905_MI,
	&m5906_MI,
	&m19581_MI,
	&m5907_MI,
	&m19582_MI,
	&m19583_MI,
	&m19584_MI,
	&m19585_MI,
	&m19586_MI,
	&m5908_MI,
	&m19580_MI,
	&m19588_MI,
	&m19589_MI,
	&m5905_MI,
	&m5906_MI,
	&m19592_MI,
	&m5907_MI,
	&m19593_MI,
	&m19594_MI,
	&m19595_MI,
	&m19596_MI,
	&m19597_MI,
	&m5908_MI,
	&m19591_MI,
	&m19599_MI,
	&m19600_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5068_TI;
extern TypeInfo t5069_TI;
extern TypeInfo t5070_TI;
extern TypeInfo t5071_TI;
extern TypeInfo t5072_TI;
extern TypeInfo t5073_TI;
extern TypeInfo t2216_TI;
extern TypeInfo t312_TI;
extern TypeInfo t2217_TI;
extern TypeInfo t5074_TI;
extern TypeInfo t5075_TI;
extern TypeInfo t5076_TI;
static TypeInfo* t3511_ITIs[] = 
{
	&t5068_TI,
	&t5069_TI,
	&t5070_TI,
	&t5071_TI,
	&t5072_TI,
	&t5073_TI,
	&t2216_TI,
	&t312_TI,
	&t2217_TI,
	&t5074_TI,
	&t5075_TI,
	&t5076_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3511_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5068_TI, 21},
	{ &t5069_TI, 28},
	{ &t5070_TI, 33},
	{ &t5071_TI, 34},
	{ &t5072_TI, 41},
	{ &t5073_TI, 46},
	{ &t2216_TI, 47},
	{ &t312_TI, 54},
	{ &t2217_TI, 59},
	{ &t5074_TI, 60},
	{ &t5075_TI, 67},
	{ &t5076_TI, 72},
	{ &t5059_TI, 73},
	{ &t5060_TI, 80},
	{ &t5061_TI, 85},
	{ &t5062_TI, 86},
	{ &t5063_TI, 93},
	{ &t5064_TI, 98},
	{ &t2240_TI, 99},
	{ &t2245_TI, 106},
	{ &t2241_TI, 111},
	{ &t5065_TI, 112},
	{ &t5066_TI, 119},
	{ &t5067_TI, 124},
	{ &t2182_TI, 125},
	{ &t2186_TI, 132},
	{ &t2183_TI, 137},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType t3511_0_0_0;
extern Il2CppType t3511_1_0_0;
struct t5;
extern TypeInfo t5_TI;
TypeInfo t3511_TI = 
{
	&g_AssemblyU2DCSharp_dll_Image, NULL, "ButtonScale[]", "", t3511_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t5_TI, t3511_ITIs, t3511_VT, &EmptyCustomAttributesCache, &t3511_TI, &t3511_0_0_0, &t3511_1_0_0, t3511_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t5 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 138, 27, 31};
#include "UnityEngine.UI_ArrayTypes.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3798_TI;



// Metadata Definition UnityEngine.EventSystems.IPointerEnterHandler[]
static MethodInfo* t3798_MIs[] =
{
	NULL
};
static MethodInfo* t3798_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19570_MI,
	&m5907_MI,
	&m19571_MI,
	&m19572_MI,
	&m19573_MI,
	&m19574_MI,
	&m19575_MI,
	&m5908_MI,
	&m19569_MI,
	&m19577_MI,
	&m19578_MI,
	&m5905_MI,
	&m5906_MI,
	&m19581_MI,
	&m5907_MI,
	&m19582_MI,
	&m19583_MI,
	&m19584_MI,
	&m19585_MI,
	&m19586_MI,
	&m5908_MI,
	&m19580_MI,
	&m19588_MI,
	&m19589_MI,
};
static TypeInfo* t3798_ITIs[] = 
{
	&t5071_TI,
	&t5072_TI,
	&t5073_TI,
	&t2216_TI,
	&t312_TI,
	&t2217_TI,
};
static Il2CppInterfaceOffsetPair t3798_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5071_TI, 21},
	{ &t5072_TI, 28},
	{ &t5073_TI, 33},
	{ &t2216_TI, 34},
	{ &t312_TI, 41},
	{ &t2217_TI, 46},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3798_0_0_0;
extern Il2CppType t3798_1_0_0;
struct t30;
extern TypeInfo t30_TI;
TypeInfo t3798_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "IPointerEnterHandler[]", "UnityEngine.EventSystems", t3798_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t30_TI, t3798_ITIs, t3798_VT, &EmptyCustomAttributesCache, &t3798_TI, &t3798_0_0_0, &t3798_1_0_0, t3798_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2214_TI;



// Metadata Definition UnityEngine.EventSystems.IEventSystemHandler[]
static MethodInfo* t2214_MIs[] =
{
	NULL
};
static MethodInfo* t2214_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19581_MI,
	&m5907_MI,
	&m19582_MI,
	&m19583_MI,
	&m19584_MI,
	&m19585_MI,
	&m19586_MI,
	&m5908_MI,
	&m19580_MI,
	&m19588_MI,
	&m19589_MI,
};
static TypeInfo* t2214_ITIs[] = 
{
	&t2216_TI,
	&t312_TI,
	&t2217_TI,
};
static Il2CppInterfaceOffsetPair t2214_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2216_TI, 21},
	{ &t312_TI, 28},
	{ &t2217_TI, 33},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t2214_0_0_0;
extern Il2CppType t2214_1_0_0;
struct t31;
extern TypeInfo t31_TI;
TypeInfo t2214_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "IEventSystemHandler[]", "UnityEngine.EventSystems", t2214_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t31_TI, t2214_ITIs, t2214_VT, &EmptyCustomAttributesCache, &t2214_TI, &t2214_0_0_0, &t2214_1_0_0, t2214_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3799_TI;



// Metadata Definition UnityEngine.EventSystems.IPointerExitHandler[]
static MethodInfo* t3799_MIs[] =
{
	NULL
};
static MethodInfo* t3799_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19592_MI,
	&m5907_MI,
	&m19593_MI,
	&m19594_MI,
	&m19595_MI,
	&m19596_MI,
	&m19597_MI,
	&m5908_MI,
	&m19591_MI,
	&m19599_MI,
	&m19600_MI,
	&m5905_MI,
	&m5906_MI,
	&m19581_MI,
	&m5907_MI,
	&m19582_MI,
	&m19583_MI,
	&m19584_MI,
	&m19585_MI,
	&m19586_MI,
	&m5908_MI,
	&m19580_MI,
	&m19588_MI,
	&m19589_MI,
};
static TypeInfo* t3799_ITIs[] = 
{
	&t5074_TI,
	&t5075_TI,
	&t5076_TI,
	&t2216_TI,
	&t312_TI,
	&t2217_TI,
};
static Il2CppInterfaceOffsetPair t3799_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5074_TI, 21},
	{ &t5075_TI, 28},
	{ &t5076_TI, 33},
	{ &t2216_TI, 34},
	{ &t312_TI, 41},
	{ &t2217_TI, 46},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3799_0_0_0;
extern Il2CppType t3799_1_0_0;
struct t32;
extern TypeInfo t32_TI;
TypeInfo t3799_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "IPointerExitHandler[]", "UnityEngine.EventSystems", t3799_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t32_TI, t3799_ITIs, t3799_VT, &EmptyCustomAttributesCache, &t3799_TI, &t3799_0_0_0, &t3799_1_0_0, t3799_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3512_TI;



// Metadata Definition LoadScene_0[]
static MethodInfo* t3512_MIs[] =
{
	NULL
};
extern MethodInfo m19604_MI;
extern MethodInfo m19605_MI;
extern MethodInfo m19606_MI;
extern MethodInfo m19607_MI;
extern MethodInfo m19608_MI;
extern MethodInfo m19609_MI;
extern MethodInfo m19603_MI;
extern MethodInfo m19611_MI;
extern MethodInfo m19612_MI;
static MethodInfo* t3512_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19604_MI,
	&m5907_MI,
	&m19605_MI,
	&m19606_MI,
	&m19607_MI,
	&m19608_MI,
	&m19609_MI,
	&m5908_MI,
	&m19603_MI,
	&m19611_MI,
	&m19612_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5077_TI;
extern TypeInfo t5078_TI;
extern TypeInfo t5079_TI;
static TypeInfo* t3512_ITIs[] = 
{
	&t5077_TI,
	&t5078_TI,
	&t5079_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3512_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5077_TI, 21},
	{ &t5078_TI, 28},
	{ &t5079_TI, 33},
	{ &t5059_TI, 34},
	{ &t5060_TI, 41},
	{ &t5061_TI, 46},
	{ &t5062_TI, 47},
	{ &t5063_TI, 54},
	{ &t5064_TI, 59},
	{ &t2240_TI, 60},
	{ &t2245_TI, 67},
	{ &t2241_TI, 72},
	{ &t5065_TI, 73},
	{ &t5066_TI, 80},
	{ &t5067_TI, 85},
	{ &t2182_TI, 86},
	{ &t2186_TI, 93},
	{ &t2183_TI, 98},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType t3512_0_0_0;
extern Il2CppType t3512_1_0_0;
struct t8;
extern TypeInfo t8_TI;
extern CustomAttributesCache t8__CustomAttributeCache_m9;
TypeInfo t3512_TI = 
{
	&g_AssemblyU2DCSharp_dll_Image, NULL, "LoadScene_0[]", "", t3512_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t8_TI, t3512_ITIs, t3512_VT, &EmptyCustomAttributesCache, &t3512_TI, &t3512_0_0_0, &t3512_1_0_0, t3512_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t8 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 99, 18, 22};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3513_TI;



// Metadata Definition LoadScene_1[]
static MethodInfo* t3513_MIs[] =
{
	NULL
};
extern MethodInfo m19616_MI;
extern MethodInfo m19617_MI;
extern MethodInfo m19618_MI;
extern MethodInfo m19619_MI;
extern MethodInfo m19620_MI;
extern MethodInfo m19621_MI;
extern MethodInfo m19615_MI;
extern MethodInfo m19623_MI;
extern MethodInfo m19624_MI;
static MethodInfo* t3513_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19616_MI,
	&m5907_MI,
	&m19617_MI,
	&m19618_MI,
	&m19619_MI,
	&m19620_MI,
	&m19621_MI,
	&m5908_MI,
	&m19615_MI,
	&m19623_MI,
	&m19624_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5080_TI;
extern TypeInfo t5081_TI;
extern TypeInfo t5082_TI;
static TypeInfo* t3513_ITIs[] = 
{
	&t5080_TI,
	&t5081_TI,
	&t5082_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3513_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5080_TI, 21},
	{ &t5081_TI, 28},
	{ &t5082_TI, 33},
	{ &t5059_TI, 34},
	{ &t5060_TI, 41},
	{ &t5061_TI, 46},
	{ &t5062_TI, 47},
	{ &t5063_TI, 54},
	{ &t5064_TI, 59},
	{ &t2240_TI, 60},
	{ &t2245_TI, 67},
	{ &t2241_TI, 72},
	{ &t5065_TI, 73},
	{ &t5066_TI, 80},
	{ &t5067_TI, 85},
	{ &t2182_TI, 86},
	{ &t2186_TI, 93},
	{ &t2183_TI, 98},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType t3513_0_0_0;
extern Il2CppType t3513_1_0_0;
struct t10;
extern TypeInfo t10_TI;
extern CustomAttributesCache t10__CustomAttributeCache_m13;
TypeInfo t3513_TI = 
{
	&g_AssemblyU2DCSharp_dll_Image, NULL, "LoadScene_1[]", "", t3513_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t10_TI, t3513_ITIs, t3513_VT, &EmptyCustomAttributesCache, &t3513_TI, &t3513_0_0_0, &t3513_1_0_0, t3513_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t10 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 99, 18, 22};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3514_TI;



// Metadata Definition LoadScene_2[]
static MethodInfo* t3514_MIs[] =
{
	NULL
};
extern MethodInfo m19628_MI;
extern MethodInfo m19629_MI;
extern MethodInfo m19630_MI;
extern MethodInfo m19631_MI;
extern MethodInfo m19632_MI;
extern MethodInfo m19633_MI;
extern MethodInfo m19627_MI;
extern MethodInfo m19635_MI;
extern MethodInfo m19636_MI;
static MethodInfo* t3514_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19628_MI,
	&m5907_MI,
	&m19629_MI,
	&m19630_MI,
	&m19631_MI,
	&m19632_MI,
	&m19633_MI,
	&m5908_MI,
	&m19627_MI,
	&m19635_MI,
	&m19636_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5083_TI;
extern TypeInfo t5084_TI;
extern TypeInfo t5085_TI;
static TypeInfo* t3514_ITIs[] = 
{
	&t5083_TI,
	&t5084_TI,
	&t5085_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3514_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5083_TI, 21},
	{ &t5084_TI, 28},
	{ &t5085_TI, 33},
	{ &t5059_TI, 34},
	{ &t5060_TI, 41},
	{ &t5061_TI, 46},
	{ &t5062_TI, 47},
	{ &t5063_TI, 54},
	{ &t5064_TI, 59},
	{ &t2240_TI, 60},
	{ &t2245_TI, 67},
	{ &t2241_TI, 72},
	{ &t5065_TI, 73},
	{ &t5066_TI, 80},
	{ &t5067_TI, 85},
	{ &t2182_TI, 86},
	{ &t2186_TI, 93},
	{ &t2183_TI, 98},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType t3514_0_0_0;
extern Il2CppType t3514_1_0_0;
struct t11;
extern TypeInfo t11_TI;
extern CustomAttributesCache t11__CustomAttributeCache_m17;
TypeInfo t3514_TI = 
{
	&g_AssemblyU2DCSharp_dll_Image, NULL, "LoadScene_2[]", "", t3514_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t11_TI, t3514_ITIs, t3514_VT, &EmptyCustomAttributesCache, &t3514_TI, &t3514_0_0_0, &t3514_1_0_0, t3514_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t11 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 99, 18, 22};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3515_TI;



// Metadata Definition LoadScene_3[]
static MethodInfo* t3515_MIs[] =
{
	NULL
};
extern MethodInfo m19640_MI;
extern MethodInfo m19641_MI;
extern MethodInfo m19642_MI;
extern MethodInfo m19643_MI;
extern MethodInfo m19644_MI;
extern MethodInfo m19645_MI;
extern MethodInfo m19639_MI;
extern MethodInfo m19647_MI;
extern MethodInfo m19648_MI;
static MethodInfo* t3515_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19640_MI,
	&m5907_MI,
	&m19641_MI,
	&m19642_MI,
	&m19643_MI,
	&m19644_MI,
	&m19645_MI,
	&m5908_MI,
	&m19639_MI,
	&m19647_MI,
	&m19648_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5086_TI;
extern TypeInfo t5087_TI;
extern TypeInfo t5088_TI;
static TypeInfo* t3515_ITIs[] = 
{
	&t5086_TI,
	&t5087_TI,
	&t5088_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3515_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5086_TI, 21},
	{ &t5087_TI, 28},
	{ &t5088_TI, 33},
	{ &t5059_TI, 34},
	{ &t5060_TI, 41},
	{ &t5061_TI, 46},
	{ &t5062_TI, 47},
	{ &t5063_TI, 54},
	{ &t5064_TI, 59},
	{ &t2240_TI, 60},
	{ &t2245_TI, 67},
	{ &t2241_TI, 72},
	{ &t5065_TI, 73},
	{ &t5066_TI, 80},
	{ &t5067_TI, 85},
	{ &t2182_TI, 86},
	{ &t2186_TI, 93},
	{ &t2183_TI, 98},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType t3515_0_0_0;
extern Il2CppType t3515_1_0_0;
struct t12;
extern TypeInfo t12_TI;
extern CustomAttributesCache t12__CustomAttributeCache_m21;
TypeInfo t3515_TI = 
{
	&g_AssemblyU2DCSharp_dll_Image, NULL, "LoadScene_3[]", "", t3515_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t12_TI, t3515_ITIs, t3515_VT, &EmptyCustomAttributesCache, &t3515_TI, &t3515_0_0_0, &t3515_1_0_0, t3515_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t12 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 99, 18, 22};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3516_TI;



// Metadata Definition LoadScene_4[]
static MethodInfo* t3516_MIs[] =
{
	NULL
};
extern MethodInfo m19652_MI;
extern MethodInfo m19653_MI;
extern MethodInfo m19654_MI;
extern MethodInfo m19655_MI;
extern MethodInfo m19656_MI;
extern MethodInfo m19657_MI;
extern MethodInfo m19651_MI;
extern MethodInfo m19659_MI;
extern MethodInfo m19660_MI;
static MethodInfo* t3516_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19652_MI,
	&m5907_MI,
	&m19653_MI,
	&m19654_MI,
	&m19655_MI,
	&m19656_MI,
	&m19657_MI,
	&m5908_MI,
	&m19651_MI,
	&m19659_MI,
	&m19660_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5089_TI;
extern TypeInfo t5090_TI;
extern TypeInfo t5091_TI;
static TypeInfo* t3516_ITIs[] = 
{
	&t5089_TI,
	&t5090_TI,
	&t5091_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3516_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5089_TI, 21},
	{ &t5090_TI, 28},
	{ &t5091_TI, 33},
	{ &t5059_TI, 34},
	{ &t5060_TI, 41},
	{ &t5061_TI, 46},
	{ &t5062_TI, 47},
	{ &t5063_TI, 54},
	{ &t5064_TI, 59},
	{ &t2240_TI, 60},
	{ &t2245_TI, 67},
	{ &t2241_TI, 72},
	{ &t5065_TI, 73},
	{ &t5066_TI, 80},
	{ &t5067_TI, 85},
	{ &t2182_TI, 86},
	{ &t2186_TI, 93},
	{ &t2183_TI, 98},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType t3516_0_0_0;
extern Il2CppType t3516_1_0_0;
struct t13;
extern TypeInfo t13_TI;
extern CustomAttributesCache t13__CustomAttributeCache_m25;
TypeInfo t3516_TI = 
{
	&g_AssemblyU2DCSharp_dll_Image, NULL, "LoadScene_4[]", "", t3516_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t13_TI, t3516_ITIs, t3516_VT, &EmptyCustomAttributesCache, &t3516_TI, &t3516_0_0_0, &t3516_1_0_0, t3516_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t13 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 99, 18, 22};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3517_TI;



// Metadata Definition ModifyTransform[]
static MethodInfo* t3517_MIs[] =
{
	NULL
};
extern MethodInfo m19664_MI;
extern MethodInfo m19665_MI;
extern MethodInfo m19666_MI;
extern MethodInfo m19667_MI;
extern MethodInfo m19668_MI;
extern MethodInfo m19669_MI;
extern MethodInfo m19663_MI;
extern MethodInfo m19671_MI;
extern MethodInfo m19672_MI;
static MethodInfo* t3517_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19664_MI,
	&m5907_MI,
	&m19665_MI,
	&m19666_MI,
	&m19667_MI,
	&m19668_MI,
	&m19669_MI,
	&m5908_MI,
	&m19663_MI,
	&m19671_MI,
	&m19672_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5092_TI;
extern TypeInfo t5093_TI;
extern TypeInfo t5094_TI;
static TypeInfo* t3517_ITIs[] = 
{
	&t5092_TI,
	&t5093_TI,
	&t5094_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3517_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5092_TI, 21},
	{ &t5093_TI, 28},
	{ &t5094_TI, 33},
	{ &t5059_TI, 34},
	{ &t5060_TI, 41},
	{ &t5061_TI, 46},
	{ &t5062_TI, 47},
	{ &t5063_TI, 54},
	{ &t5064_TI, 59},
	{ &t2240_TI, 60},
	{ &t2245_TI, 67},
	{ &t2241_TI, 72},
	{ &t5065_TI, 73},
	{ &t5066_TI, 80},
	{ &t5067_TI, 85},
	{ &t2182_TI, 86},
	{ &t2186_TI, 93},
	{ &t2183_TI, 98},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType t3517_0_0_0;
extern Il2CppType t3517_1_0_0;
struct t14;
extern TypeInfo t14_TI;
TypeInfo t3517_TI = 
{
	&g_AssemblyU2DCSharp_dll_Image, NULL, "ModifyTransform[]", "", t3517_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t14_TI, t3517_ITIs, t3517_VT, &EmptyCustomAttributesCache, &t3517_TI, &t3517_0_0_0, &t3517_1_0_0, t3517_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t14 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 99, 18, 22};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3518_TI;



// Metadata Definition PointerMovement[]
static MethodInfo* t3518_MIs[] =
{
	NULL
};
extern MethodInfo m19676_MI;
extern MethodInfo m19677_MI;
extern MethodInfo m19678_MI;
extern MethodInfo m19679_MI;
extern MethodInfo m19680_MI;
extern MethodInfo m19681_MI;
extern MethodInfo m19675_MI;
extern MethodInfo m19683_MI;
extern MethodInfo m19684_MI;
static MethodInfo* t3518_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19676_MI,
	&m5907_MI,
	&m19677_MI,
	&m19678_MI,
	&m19679_MI,
	&m19680_MI,
	&m19681_MI,
	&m5908_MI,
	&m19675_MI,
	&m19683_MI,
	&m19684_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5095_TI;
extern TypeInfo t5096_TI;
extern TypeInfo t5097_TI;
static TypeInfo* t3518_ITIs[] = 
{
	&t5095_TI,
	&t5096_TI,
	&t5097_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3518_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5095_TI, 21},
	{ &t5096_TI, 28},
	{ &t5097_TI, 33},
	{ &t5059_TI, 34},
	{ &t5060_TI, 41},
	{ &t5061_TI, 46},
	{ &t5062_TI, 47},
	{ &t5063_TI, 54},
	{ &t5064_TI, 59},
	{ &t2240_TI, 60},
	{ &t2245_TI, 67},
	{ &t2241_TI, 72},
	{ &t5065_TI, 73},
	{ &t5066_TI, 80},
	{ &t5067_TI, 85},
	{ &t2182_TI, 86},
	{ &t2186_TI, 93},
	{ &t2183_TI, 98},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType t3518_0_0_0;
extern Il2CppType t3518_1_0_0;
struct t18;
extern TypeInfo t18_TI;
TypeInfo t3518_TI = 
{
	&g_AssemblyU2DCSharp_dll_Image, NULL, "PointerMovement[]", "", t3518_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t18_TI, t3518_ITIs, t3518_VT, &EmptyCustomAttributesCache, &t3518_TI, &t3518_0_0_0, &t3518_1_0_0, t3518_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t18 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 99, 18, 22};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3519_TI;



// Metadata Definition ScreenResolution[]
static MethodInfo* t3519_MIs[] =
{
	NULL
};
extern MethodInfo m19688_MI;
extern MethodInfo m19689_MI;
extern MethodInfo m19690_MI;
extern MethodInfo m19691_MI;
extern MethodInfo m19692_MI;
extern MethodInfo m19693_MI;
extern MethodInfo m19687_MI;
extern MethodInfo m19695_MI;
extern MethodInfo m19696_MI;
static MethodInfo* t3519_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19688_MI,
	&m5907_MI,
	&m19689_MI,
	&m19690_MI,
	&m19691_MI,
	&m19692_MI,
	&m19693_MI,
	&m5908_MI,
	&m19687_MI,
	&m19695_MI,
	&m19696_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5098_TI;
extern TypeInfo t5099_TI;
extern TypeInfo t5100_TI;
static TypeInfo* t3519_ITIs[] = 
{
	&t5098_TI,
	&t5099_TI,
	&t5100_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3519_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5098_TI, 21},
	{ &t5099_TI, 28},
	{ &t5100_TI, 33},
	{ &t5059_TI, 34},
	{ &t5060_TI, 41},
	{ &t5061_TI, 46},
	{ &t5062_TI, 47},
	{ &t5063_TI, 54},
	{ &t5064_TI, 59},
	{ &t2240_TI, 60},
	{ &t2245_TI, 67},
	{ &t2241_TI, 72},
	{ &t5065_TI, 73},
	{ &t5066_TI, 80},
	{ &t5067_TI, 85},
	{ &t2182_TI, 86},
	{ &t2186_TI, 93},
	{ &t2183_TI, 98},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType t3519_0_0_0;
extern Il2CppType t3519_1_0_0;
struct t19;
extern TypeInfo t19_TI;
TypeInfo t3519_TI = 
{
	&g_AssemblyU2DCSharp_dll_Image, NULL, "ScreenResolution[]", "", t3519_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t19_TI, t3519_ITIs, t3519_VT, &EmptyCustomAttributesCache, &t3519_TI, &t3519_0_0_0, &t3519_1_0_0, t3519_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t19 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 99, 18, 22};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3800_TI;



// Metadata Definition UnityEngine.EventSystems.EventHandle[]
static MethodInfo* t3800_MIs[] =
{
	NULL
};
extern MethodInfo m19700_MI;
extern MethodInfo m19701_MI;
extern MethodInfo m19702_MI;
extern MethodInfo m19703_MI;
extern MethodInfo m19704_MI;
extern MethodInfo m19705_MI;
extern MethodInfo m19699_MI;
extern MethodInfo m19707_MI;
extern MethodInfo m19708_MI;
extern MethodInfo m19711_MI;
extern MethodInfo m19712_MI;
extern MethodInfo m19713_MI;
extern MethodInfo m19714_MI;
extern MethodInfo m19715_MI;
extern MethodInfo m19716_MI;
extern MethodInfo m19710_MI;
extern MethodInfo m19718_MI;
extern MethodInfo m19719_MI;
extern MethodInfo m19722_MI;
extern MethodInfo m19723_MI;
extern MethodInfo m19724_MI;
extern MethodInfo m19725_MI;
extern MethodInfo m19726_MI;
extern MethodInfo m19727_MI;
extern MethodInfo m19721_MI;
extern MethodInfo m19729_MI;
extern MethodInfo m19730_MI;
extern MethodInfo m19733_MI;
extern MethodInfo m19734_MI;
extern MethodInfo m19735_MI;
extern MethodInfo m19736_MI;
extern MethodInfo m19737_MI;
extern MethodInfo m19738_MI;
extern MethodInfo m19732_MI;
extern MethodInfo m19740_MI;
extern MethodInfo m19741_MI;
extern MethodInfo m19744_MI;
extern MethodInfo m19745_MI;
extern MethodInfo m19746_MI;
extern MethodInfo m19747_MI;
extern MethodInfo m19748_MI;
extern MethodInfo m19749_MI;
extern MethodInfo m19743_MI;
extern MethodInfo m19751_MI;
extern MethodInfo m19752_MI;
extern MethodInfo m19755_MI;
extern MethodInfo m19756_MI;
extern MethodInfo m19757_MI;
extern MethodInfo m19758_MI;
extern MethodInfo m19759_MI;
extern MethodInfo m19760_MI;
extern MethodInfo m19754_MI;
extern MethodInfo m19762_MI;
extern MethodInfo m19763_MI;
static MethodInfo* t3800_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19700_MI,
	&m5907_MI,
	&m19701_MI,
	&m19702_MI,
	&m19703_MI,
	&m19704_MI,
	&m19705_MI,
	&m5908_MI,
	&m19699_MI,
	&m19707_MI,
	&m19708_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5101_TI;
extern TypeInfo t5102_TI;
extern TypeInfo t5103_TI;
extern TypeInfo t5104_TI;
extern TypeInfo t5105_TI;
extern TypeInfo t5106_TI;
extern TypeInfo t5107_TI;
extern TypeInfo t5108_TI;
extern TypeInfo t5109_TI;
extern TypeInfo t5110_TI;
extern TypeInfo t5111_TI;
extern TypeInfo t5112_TI;
extern TypeInfo t5113_TI;
extern TypeInfo t5114_TI;
extern TypeInfo t5115_TI;
extern TypeInfo t5116_TI;
extern TypeInfo t5117_TI;
extern TypeInfo t5118_TI;
static TypeInfo* t3800_ITIs[] = 
{
	&t5101_TI,
	&t5102_TI,
	&t5103_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3800_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5101_TI, 21},
	{ &t5102_TI, 28},
	{ &t5103_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3800_0_0_0;
extern Il2CppType t3800_1_0_0;
#include "t48.h"
extern TypeInfo t48_TI;
extern TypeInfo t44_TI;
extern CustomAttributesCache t48__CustomAttributeCache;
TypeInfo t3800_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "EventHandle[]", "UnityEngine.EventSystems", t3800_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t48_TI, t3800_ITIs, t3800_VT, &EmptyCustomAttributesCache, &t44_TI, &t3800_0_0_0, &t3800_1_0_0, t3800_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3520_TI;



// Metadata Definition System.Enum[]
static MethodInfo* t3520_MIs[] =
{
	NULL
};
static MethodInfo* t3520_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
static TypeInfo* t3520_ITIs[] = 
{
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3520_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5104_TI, 21},
	{ &t5105_TI, 28},
	{ &t5106_TI, 33},
	{ &t5107_TI, 34},
	{ &t5108_TI, 41},
	{ &t5109_TI, 46},
	{ &t5110_TI, 47},
	{ &t5111_TI, 54},
	{ &t5112_TI, 59},
	{ &t5113_TI, 60},
	{ &t5114_TI, 67},
	{ &t5115_TI, 72},
	{ &t5116_TI, 73},
	{ &t5117_TI, 80},
	{ &t5118_TI, 85},
	{ &t2182_TI, 86},
	{ &t2186_TI, 93},
	{ &t2183_TI, 98},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3520_0_0_0;
extern Il2CppType t3520_1_0_0;
struct t49;
extern TypeInfo t49_TI;
extern CustomAttributesCache t49__CustomAttributeCache;
extern CustomAttributesCache t49__CustomAttributeCache_m5867;
extern CustomAttributesCache t49__CustomAttributeCache_m5239;
extern CustomAttributesCache t49__CustomAttributeCache_m5869;
extern CustomAttributesCache t49__CustomAttributeCache_m4220;
extern CustomAttributesCache t49__CustomAttributeCache_m1263;
extern CustomAttributesCache t49__CustomAttributeCache_m1251;
extern CustomAttributesCache t49__CustomAttributeCache_m5874;
extern CustomAttributesCache t49__CustomAttributeCache_m5875;
extern CustomAttributesCache t49__CustomAttributeCache_m5876;
extern CustomAttributesCache t49__CustomAttributeCache_m5877;
extern CustomAttributesCache t49__CustomAttributeCache_m5878;
extern CustomAttributesCache t49__CustomAttributeCache_m5879;
extern CustomAttributesCache t49__CustomAttributeCache_m5880;
extern CustomAttributesCache t49__CustomAttributeCache_m5881;
extern CustomAttributesCache t49__CustomAttributeCache_m5882;
extern CustomAttributesCache t49__CustomAttributeCache_m5886;
TypeInfo t3520_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enum[]", "System", t3520_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t49_TI, t3520_ITIs, t3520_VT, &EmptyCustomAttributesCache, &t3520_TI, &t3520_0_0_0, &t3520_1_0_0, t3520_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t49 *), -1, sizeof(t3520_SFs), 0, -1, 1056897, 1, false, false, false, false, false, true, false, false, false, true, false, false, 0, 0, 0, 0, 0, 99, 18, 22};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3521_TI;



// Metadata Definition System.IFormattable[]
static MethodInfo* t3521_MIs[] =
{
	NULL
};
static MethodInfo* t3521_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
};
static TypeInfo* t3521_ITIs[] = 
{
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
};
static Il2CppInterfaceOffsetPair t3521_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5107_TI, 21},
	{ &t5108_TI, 28},
	{ &t5109_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3521_0_0_0;
extern Il2CppType t3521_1_0_0;
struct t288;
extern TypeInfo t288_TI;
extern CustomAttributesCache t288__CustomAttributeCache;
TypeInfo t3521_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IFormattable[]", "System", t3521_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t288_TI, t3521_ITIs, t3521_VT, &EmptyCustomAttributesCache, &t3521_TI, &t3521_0_0_0, &t3521_1_0_0, t3521_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3522_TI;



// Metadata Definition System.IConvertible[]
static MethodInfo* t3522_MIs[] =
{
	NULL
};
static MethodInfo* t3522_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
};
static TypeInfo* t3522_ITIs[] = 
{
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
};
static Il2CppInterfaceOffsetPair t3522_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5110_TI, 21},
	{ &t5111_TI, 28},
	{ &t5112_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3522_0_0_0;
extern Il2CppType t3522_1_0_0;
struct t289;
extern TypeInfo t289_TI;
extern CustomAttributesCache t289__CustomAttributeCache;
TypeInfo t3522_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IConvertible[]", "System", t3522_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t289_TI, t3522_ITIs, t3522_VT, &EmptyCustomAttributesCache, &t3522_TI, &t3522_0_0_0, &t3522_1_0_0, t3522_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3523_TI;



// Metadata Definition System.IComparable[]
static MethodInfo* t3523_MIs[] =
{
	NULL
};
static MethodInfo* t3523_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
};
static TypeInfo* t3523_ITIs[] = 
{
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
};
static Il2CppInterfaceOffsetPair t3523_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5113_TI, 21},
	{ &t5114_TI, 28},
	{ &t5115_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3523_0_0_0;
extern Il2CppType t3523_1_0_0;
struct t290;
extern TypeInfo t290_TI;
extern CustomAttributesCache t290__CustomAttributeCache;
TypeInfo t3523_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable[]", "System", t3523_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t290_TI, t3523_ITIs, t3523_VT, &EmptyCustomAttributesCache, &t3523_TI, &t3523_0_0_0, &t3523_1_0_0, t3523_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3524_TI;



// Metadata Definition System.ValueType[]
static MethodInfo* t3524_MIs[] =
{
	NULL
};
static MethodInfo* t3524_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
static TypeInfo* t3524_ITIs[] = 
{
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3524_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5116_TI, 21},
	{ &t5117_TI, 28},
	{ &t5118_TI, 33},
	{ &t2182_TI, 34},
	{ &t2186_TI, 41},
	{ &t2183_TI, 46},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3524_0_0_0;
extern Il2CppType t3524_1_0_0;
struct t110;
extern TypeInfo t110_TI;
extern CustomAttributesCache t110__CustomAttributeCache;
TypeInfo t3524_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ValueType[]", "System", t3524_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t110_TI, t3524_ITIs, t3524_VT, &EmptyCustomAttributesCache, &t3524_TI, &t3524_0_0_0, &t3524_1_0_0, t3524_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t110 *), -1, 0, 0, -1, 1056897, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3801_TI;



// Metadata Definition UnityEngine.EventSystems.EventSystem[]
static MethodInfo* t3801_MIs[] =
{
	NULL
};
extern MethodInfo m19766_MI;
extern MethodInfo m19767_MI;
extern MethodInfo m19768_MI;
extern MethodInfo m19769_MI;
extern MethodInfo m19770_MI;
extern MethodInfo m19771_MI;
extern MethodInfo m19765_MI;
extern MethodInfo m19773_MI;
extern MethodInfo m19774_MI;
extern MethodInfo m19777_MI;
extern MethodInfo m19778_MI;
extern MethodInfo m19779_MI;
extern MethodInfo m19780_MI;
extern MethodInfo m19781_MI;
extern MethodInfo m19782_MI;
extern MethodInfo m19776_MI;
extern MethodInfo m19784_MI;
extern MethodInfo m19785_MI;
static MethodInfo* t3801_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19766_MI,
	&m5907_MI,
	&m19767_MI,
	&m19768_MI,
	&m19769_MI,
	&m19770_MI,
	&m19771_MI,
	&m5908_MI,
	&m19765_MI,
	&m19773_MI,
	&m19774_MI,
	&m5905_MI,
	&m5906_MI,
	&m19777_MI,
	&m5907_MI,
	&m19778_MI,
	&m19779_MI,
	&m19780_MI,
	&m19781_MI,
	&m19782_MI,
	&m5908_MI,
	&m19776_MI,
	&m19784_MI,
	&m19785_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5119_TI;
extern TypeInfo t5120_TI;
extern TypeInfo t5121_TI;
extern TypeInfo t5122_TI;
extern TypeInfo t5123_TI;
extern TypeInfo t5124_TI;
static TypeInfo* t3801_ITIs[] = 
{
	&t5119_TI,
	&t5120_TI,
	&t5121_TI,
	&t5122_TI,
	&t5123_TI,
	&t5124_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3801_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5119_TI, 21},
	{ &t5120_TI, 28},
	{ &t5121_TI, 33},
	{ &t5122_TI, 34},
	{ &t5123_TI, 41},
	{ &t5124_TI, 46},
	{ &t5059_TI, 47},
	{ &t5060_TI, 54},
	{ &t5061_TI, 59},
	{ &t5062_TI, 60},
	{ &t5063_TI, 67},
	{ &t5064_TI, 72},
	{ &t2240_TI, 73},
	{ &t2245_TI, 80},
	{ &t2241_TI, 85},
	{ &t5065_TI, 86},
	{ &t5066_TI, 93},
	{ &t5067_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3801_0_0_0;
extern Il2CppType t3801_1_0_0;
struct t50;
extern TypeInfo t50_TI;
extern CustomAttributesCache t50__CustomAttributeCache;
extern CustomAttributesCache t50__CustomAttributeCache_m_FirstSelected;
extern CustomAttributesCache t50__CustomAttributeCache_m_sendNavigationEvents;
extern CustomAttributesCache t50__CustomAttributeCache_m_DragThreshold;
extern CustomAttributesCache t50__CustomAttributeCache_U3CcurrentU3Ek__BackingField;
extern CustomAttributesCache t50__CustomAttributeCache_m75;
extern CustomAttributesCache t50__CustomAttributeCache_m76;
TypeInfo t3801_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "EventSystem[]", "UnityEngine.EventSystems", t3801_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t50_TI, t3801_ITIs, t3801_VT, &EmptyCustomAttributesCache, &t3801_TI, &t3801_0_0_0, &t3801_1_0_0, t3801_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t50 *), -1, sizeof(t3801_SFs), 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, true, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3802_TI;



// Metadata Definition UnityEngine.EventSystems.UIBehaviour[]
static MethodInfo* t3802_MIs[] =
{
	NULL
};
static MethodInfo* t3802_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19777_MI,
	&m5907_MI,
	&m19778_MI,
	&m19779_MI,
	&m19780_MI,
	&m19781_MI,
	&m19782_MI,
	&m5908_MI,
	&m19776_MI,
	&m19784_MI,
	&m19785_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
static TypeInfo* t3802_ITIs[] = 
{
	&t5122_TI,
	&t5123_TI,
	&t5124_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3802_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5122_TI, 21},
	{ &t5123_TI, 28},
	{ &t5124_TI, 33},
	{ &t5059_TI, 34},
	{ &t5060_TI, 41},
	{ &t5061_TI, 46},
	{ &t5062_TI, 47},
	{ &t5063_TI, 54},
	{ &t5064_TI, 59},
	{ &t2240_TI, 60},
	{ &t2245_TI, 67},
	{ &t2241_TI, 72},
	{ &t5065_TI, 73},
	{ &t5066_TI, 80},
	{ &t5067_TI, 85},
	{ &t2182_TI, 86},
	{ &t2186_TI, 93},
	{ &t2183_TI, 98},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3802_0_0_0;
extern Il2CppType t3802_1_0_0;
struct t55;
extern TypeInfo t55_TI;
TypeInfo t3802_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "UIBehaviour[]", "UnityEngine.EventSystems", t3802_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t55_TI, t3802_ITIs, t3802_VT, &EmptyCustomAttributesCache, &t3802_TI, &t3802_0_0_0, &t3802_1_0_0, t3802_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t55 *), -1, 0, 0, -1, 1048705, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 99, 18, 22};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t537_TI;



// Metadata Definition System.Type[]
static MethodInfo* t537_MIs[] =
{
	NULL
};
extern MethodInfo m19792_MI;
extern MethodInfo m19793_MI;
extern MethodInfo m19794_MI;
extern MethodInfo m19795_MI;
extern MethodInfo m19796_MI;
extern MethodInfo m19797_MI;
extern MethodInfo m19791_MI;
extern MethodInfo m19799_MI;
extern MethodInfo m19800_MI;
extern MethodInfo m19803_MI;
extern MethodInfo m19804_MI;
extern MethodInfo m19805_MI;
extern MethodInfo m19806_MI;
extern MethodInfo m19807_MI;
extern MethodInfo m19808_MI;
extern MethodInfo m19802_MI;
extern MethodInfo m19810_MI;
extern MethodInfo m19811_MI;
extern MethodInfo m19814_MI;
extern MethodInfo m19815_MI;
extern MethodInfo m19816_MI;
extern MethodInfo m19817_MI;
extern MethodInfo m19818_MI;
extern MethodInfo m19819_MI;
extern MethodInfo m19813_MI;
extern MethodInfo m19821_MI;
extern MethodInfo m19822_MI;
extern MethodInfo m19825_MI;
extern MethodInfo m19826_MI;
extern MethodInfo m19827_MI;
extern MethodInfo m19828_MI;
extern MethodInfo m19829_MI;
extern MethodInfo m19830_MI;
extern MethodInfo m19824_MI;
extern MethodInfo m19832_MI;
extern MethodInfo m19833_MI;
extern MethodInfo m19836_MI;
extern MethodInfo m19837_MI;
extern MethodInfo m19838_MI;
extern MethodInfo m19839_MI;
extern MethodInfo m19840_MI;
extern MethodInfo m19841_MI;
extern MethodInfo m19835_MI;
extern MethodInfo m19843_MI;
extern MethodInfo m19844_MI;
extern MethodInfo m19847_MI;
extern MethodInfo m19848_MI;
extern MethodInfo m19849_MI;
extern MethodInfo m19850_MI;
extern MethodInfo m19851_MI;
extern MethodInfo m19852_MI;
extern MethodInfo m19846_MI;
extern MethodInfo m19854_MI;
extern MethodInfo m19855_MI;
static MethodInfo* t537_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19792_MI,
	&m5907_MI,
	&m19793_MI,
	&m19794_MI,
	&m19795_MI,
	&m19796_MI,
	&m19797_MI,
	&m5908_MI,
	&m19791_MI,
	&m19799_MI,
	&m19800_MI,
	&m5905_MI,
	&m5906_MI,
	&m19803_MI,
	&m5907_MI,
	&m19804_MI,
	&m19805_MI,
	&m19806_MI,
	&m19807_MI,
	&m19808_MI,
	&m5908_MI,
	&m19802_MI,
	&m19810_MI,
	&m19811_MI,
	&m5905_MI,
	&m5906_MI,
	&m19814_MI,
	&m5907_MI,
	&m19815_MI,
	&m19816_MI,
	&m19817_MI,
	&m19818_MI,
	&m19819_MI,
	&m5908_MI,
	&m19813_MI,
	&m19821_MI,
	&m19822_MI,
	&m5905_MI,
	&m5906_MI,
	&m19825_MI,
	&m5907_MI,
	&m19826_MI,
	&m19827_MI,
	&m19828_MI,
	&m19829_MI,
	&m19830_MI,
	&m5908_MI,
	&m19824_MI,
	&m19832_MI,
	&m19833_MI,
	&m5905_MI,
	&m5906_MI,
	&m19836_MI,
	&m5907_MI,
	&m19837_MI,
	&m19838_MI,
	&m19839_MI,
	&m19840_MI,
	&m19841_MI,
	&m5908_MI,
	&m19835_MI,
	&m19843_MI,
	&m19844_MI,
	&m5905_MI,
	&m5906_MI,
	&m19847_MI,
	&m5907_MI,
	&m19848_MI,
	&m19849_MI,
	&m19850_MI,
	&m19851_MI,
	&m19852_MI,
	&m5908_MI,
	&m19846_MI,
	&m19854_MI,
	&m19855_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t3069_TI;
extern TypeInfo t3075_TI;
extern TypeInfo t3070_TI;
extern TypeInfo t5125_TI;
extern TypeInfo t5126_TI;
extern TypeInfo t5127_TI;
extern TypeInfo t5128_TI;
extern TypeInfo t5129_TI;
extern TypeInfo t5130_TI;
extern TypeInfo t5131_TI;
extern TypeInfo t5132_TI;
extern TypeInfo t5133_TI;
extern TypeInfo t5134_TI;
extern TypeInfo t5135_TI;
extern TypeInfo t5136_TI;
extern TypeInfo t5137_TI;
extern TypeInfo t5138_TI;
extern TypeInfo t5139_TI;
static TypeInfo* t537_ITIs[] = 
{
	&t3069_TI,
	&t3075_TI,
	&t3070_TI,
	&t5125_TI,
	&t5126_TI,
	&t5127_TI,
	&t5128_TI,
	&t5129_TI,
	&t5130_TI,
	&t5131_TI,
	&t5132_TI,
	&t5133_TI,
	&t5134_TI,
	&t5135_TI,
	&t5136_TI,
	&t5137_TI,
	&t5138_TI,
	&t5139_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t537_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t3069_TI, 21},
	{ &t3075_TI, 28},
	{ &t3070_TI, 33},
	{ &t5125_TI, 34},
	{ &t5126_TI, 41},
	{ &t5127_TI, 46},
	{ &t5128_TI, 47},
	{ &t5129_TI, 54},
	{ &t5130_TI, 59},
	{ &t5131_TI, 60},
	{ &t5132_TI, 67},
	{ &t5133_TI, 72},
	{ &t5134_TI, 73},
	{ &t5135_TI, 80},
	{ &t5136_TI, 85},
	{ &t5137_TI, 86},
	{ &t5138_TI, 93},
	{ &t5139_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t537_0_0_0;
extern Il2CppType t537_1_0_0;
struct t42;
extern TypeInfo t42_TI;
extern CustomAttributesCache t42__CustomAttributeCache;
extern CustomAttributesCache t42__CustomAttributeCache_m6019;
extern CustomAttributesCache t42__CustomAttributeCache_m2980;
extern CustomAttributesCache t42__CustomAttributeCache_m6033;
extern CustomAttributesCache t42__CustomAttributeCache_m6034;
extern CustomAttributesCache t42__CustomAttributeCache_m9854;
TypeInfo t537_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Type[]", "System", t537_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t42_TI, t537_ITIs, t537_VT, &EmptyCustomAttributesCache, &t537_TI, &t537_0_0_0, &t537_1_0_0, t537_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t42 *), -1, sizeof(t537_SFs), 0, -1, 1056897, 1, false, false, false, false, false, true, false, false, false, true, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3525_TI;



// Metadata Definition System.Reflection.IReflect[]
static MethodInfo* t3525_MIs[] =
{
	NULL
};
static MethodInfo* t3525_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19803_MI,
	&m5907_MI,
	&m19804_MI,
	&m19805_MI,
	&m19806_MI,
	&m19807_MI,
	&m19808_MI,
	&m5908_MI,
	&m19802_MI,
	&m19810_MI,
	&m19811_MI,
};
static TypeInfo* t3525_ITIs[] = 
{
	&t5125_TI,
	&t5126_TI,
	&t5127_TI,
};
static Il2CppInterfaceOffsetPair t3525_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5125_TI, 21},
	{ &t5126_TI, 28},
	{ &t5127_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3525_0_0_0;
extern Il2CppType t3525_1_0_0;
struct t1913;
extern TypeInfo t1913_TI;
extern CustomAttributesCache t1913__CustomAttributeCache;
TypeInfo t3525_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IReflect[]", "System.Reflection", t3525_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1913_TI, t3525_ITIs, t3525_VT, &EmptyCustomAttributesCache, &t3525_TI, &t3525_0_0_0, &t3525_1_0_0, t3525_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3526_TI;



// Metadata Definition System.Runtime.InteropServices._Type[]
static MethodInfo* t3526_MIs[] =
{
	NULL
};
static MethodInfo* t3526_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19814_MI,
	&m5907_MI,
	&m19815_MI,
	&m19816_MI,
	&m19817_MI,
	&m19818_MI,
	&m19819_MI,
	&m5908_MI,
	&m19813_MI,
	&m19821_MI,
	&m19822_MI,
};
static TypeInfo* t3526_ITIs[] = 
{
	&t5128_TI,
	&t5129_TI,
	&t5130_TI,
};
static Il2CppInterfaceOffsetPair t3526_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5128_TI, 21},
	{ &t5129_TI, 28},
	{ &t5130_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3526_0_0_0;
extern Il2CppType t3526_1_0_0;
struct t1914;
extern TypeInfo t1914_TI;
extern CustomAttributesCache t1914__CustomAttributeCache;
TypeInfo t3526_TI = 
{
	&g_mscorlib_dll_Image, NULL, "_Type[]", "System.Runtime.InteropServices", t3526_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1914_TI, t3526_ITIs, t3526_VT, &EmptyCustomAttributesCache, &t3526_TI, &t3526_0_0_0, &t3526_1_0_0, t3526_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1501_TI;



// Metadata Definition System.Reflection.MemberInfo[]
static MethodInfo* t1501_MIs[] =
{
	NULL
};
static MethodInfo* t1501_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19825_MI,
	&m5907_MI,
	&m19826_MI,
	&m19827_MI,
	&m19828_MI,
	&m19829_MI,
	&m19830_MI,
	&m5908_MI,
	&m19824_MI,
	&m19832_MI,
	&m19833_MI,
	&m5905_MI,
	&m5906_MI,
	&m19836_MI,
	&m5907_MI,
	&m19837_MI,
	&m19838_MI,
	&m19839_MI,
	&m19840_MI,
	&m19841_MI,
	&m5908_MI,
	&m19835_MI,
	&m19843_MI,
	&m19844_MI,
	&m5905_MI,
	&m5906_MI,
	&m19847_MI,
	&m5907_MI,
	&m19848_MI,
	&m19849_MI,
	&m19850_MI,
	&m19851_MI,
	&m19852_MI,
	&m5908_MI,
	&m19846_MI,
	&m19854_MI,
	&m19855_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
static TypeInfo* t1501_ITIs[] = 
{
	&t5131_TI,
	&t5132_TI,
	&t5133_TI,
	&t5134_TI,
	&t5135_TI,
	&t5136_TI,
	&t5137_TI,
	&t5138_TI,
	&t5139_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t1501_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5131_TI, 21},
	{ &t5132_TI, 28},
	{ &t5133_TI, 33},
	{ &t5134_TI, 34},
	{ &t5135_TI, 41},
	{ &t5136_TI, 46},
	{ &t5137_TI, 47},
	{ &t5138_TI, 54},
	{ &t5139_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1501_0_0_0;
extern Il2CppType t1501_1_0_0;
struct t296;
extern TypeInfo t296_TI;
extern CustomAttributesCache t296__CustomAttributeCache;
TypeInfo t1501_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MemberInfo[]", "System.Reflection", t1501_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t296_TI, t1501_ITIs, t1501_VT, &EmptyCustomAttributesCache, &t1501_TI, &t1501_0_0_0, &t1501_1_0_0, t1501_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t296 *), -1, 0, 0, -1, 1056897, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3527_TI;



// Metadata Definition System.Reflection.ICustomAttributeProvider[]
static MethodInfo* t3527_MIs[] =
{
	NULL
};
static MethodInfo* t3527_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19836_MI,
	&m5907_MI,
	&m19837_MI,
	&m19838_MI,
	&m19839_MI,
	&m19840_MI,
	&m19841_MI,
	&m5908_MI,
	&m19835_MI,
	&m19843_MI,
	&m19844_MI,
};
static TypeInfo* t3527_ITIs[] = 
{
	&t5134_TI,
	&t5135_TI,
	&t5136_TI,
};
static Il2CppInterfaceOffsetPair t3527_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5134_TI, 21},
	{ &t5135_TI, 28},
	{ &t5136_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3527_0_0_0;
extern Il2CppType t3527_1_0_0;
struct t1657;
extern TypeInfo t1657_TI;
extern CustomAttributesCache t1657__CustomAttributeCache;
TypeInfo t3527_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICustomAttributeProvider[]", "System.Reflection", t3527_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1657_TI, t3527_ITIs, t3527_VT, &EmptyCustomAttributesCache, &t3527_TI, &t3527_0_0_0, &t3527_1_0_0, t3527_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3528_TI;



// Metadata Definition System.Runtime.InteropServices._MemberInfo[]
static MethodInfo* t3528_MIs[] =
{
	NULL
};
static MethodInfo* t3528_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19847_MI,
	&m5907_MI,
	&m19848_MI,
	&m19849_MI,
	&m19850_MI,
	&m19851_MI,
	&m19852_MI,
	&m5908_MI,
	&m19846_MI,
	&m19854_MI,
	&m19855_MI,
};
static TypeInfo* t3528_ITIs[] = 
{
	&t5137_TI,
	&t5138_TI,
	&t5139_TI,
};
static Il2CppInterfaceOffsetPair t3528_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5137_TI, 21},
	{ &t5138_TI, 28},
	{ &t5139_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3528_0_0_0;
extern Il2CppType t3528_1_0_0;
struct t1915;
extern TypeInfo t1915_TI;
extern CustomAttributesCache t1915__CustomAttributeCache;
TypeInfo t3528_TI = 
{
	&g_mscorlib_dll_Image, NULL, "_MemberInfo[]", "System.Runtime.InteropServices", t3528_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1915_TI, t3528_ITIs, t3528_VT, &EmptyCustomAttributesCache, &t3528_TI, &t3528_0_0_0, &t3528_1_0_0, t3528_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t841_TI;



// Metadata Definition System.Int32[]
static MethodInfo* t841_MIs[] =
{
	NULL
};
extern MethodInfo m19862_MI;
extern MethodInfo m19863_MI;
extern MethodInfo m19864_MI;
extern MethodInfo m19865_MI;
extern MethodInfo m19866_MI;
extern MethodInfo m19867_MI;
extern MethodInfo m19861_MI;
extern MethodInfo m19869_MI;
extern MethodInfo m19870_MI;
extern MethodInfo m19873_MI;
extern MethodInfo m19874_MI;
extern MethodInfo m19875_MI;
extern MethodInfo m19876_MI;
extern MethodInfo m19877_MI;
extern MethodInfo m19878_MI;
extern MethodInfo m19872_MI;
extern MethodInfo m19880_MI;
extern MethodInfo m19881_MI;
extern MethodInfo m19884_MI;
extern MethodInfo m19885_MI;
extern MethodInfo m19886_MI;
extern MethodInfo m19887_MI;
extern MethodInfo m19888_MI;
extern MethodInfo m19889_MI;
extern MethodInfo m19883_MI;
extern MethodInfo m19891_MI;
extern MethodInfo m19892_MI;
static MethodInfo* t841_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19862_MI,
	&m5907_MI,
	&m19863_MI,
	&m19864_MI,
	&m19865_MI,
	&m19866_MI,
	&m19867_MI,
	&m5908_MI,
	&m19861_MI,
	&m19869_MI,
	&m19870_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19873_MI,
	&m5907_MI,
	&m19874_MI,
	&m19875_MI,
	&m19876_MI,
	&m19877_MI,
	&m19878_MI,
	&m5908_MI,
	&m19872_MI,
	&m19880_MI,
	&m19881_MI,
	&m5905_MI,
	&m5906_MI,
	&m19884_MI,
	&m5907_MI,
	&m19885_MI,
	&m19886_MI,
	&m19887_MI,
	&m19888_MI,
	&m19889_MI,
	&m5908_MI,
	&m19883_MI,
	&m19891_MI,
	&m19892_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5140_TI;
extern TypeInfo t5141_TI;
extern TypeInfo t5142_TI;
extern TypeInfo t5143_TI;
extern TypeInfo t5144_TI;
extern TypeInfo t5145_TI;
extern TypeInfo t5146_TI;
extern TypeInfo t5147_TI;
extern TypeInfo t5148_TI;
static TypeInfo* t841_ITIs[] = 
{
	&t5140_TI,
	&t5141_TI,
	&t5142_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5143_TI,
	&t5144_TI,
	&t5145_TI,
	&t5146_TI,
	&t5147_TI,
	&t5148_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t841_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5140_TI, 21},
	{ &t5141_TI, 28},
	{ &t5142_TI, 33},
	{ &t5107_TI, 34},
	{ &t5108_TI, 41},
	{ &t5109_TI, 46},
	{ &t5110_TI, 47},
	{ &t5111_TI, 54},
	{ &t5112_TI, 59},
	{ &t5113_TI, 60},
	{ &t5114_TI, 67},
	{ &t5115_TI, 72},
	{ &t5143_TI, 73},
	{ &t5144_TI, 80},
	{ &t5145_TI, 85},
	{ &t5146_TI, 86},
	{ &t5147_TI, 93},
	{ &t5148_TI, 98},
	{ &t5116_TI, 99},
	{ &t5117_TI, 106},
	{ &t5118_TI, 111},
	{ &t2182_TI, 112},
	{ &t2186_TI, 119},
	{ &t2183_TI, 124},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t841_0_0_0;
extern Il2CppType t841_1_0_0;
#include "t44.h"
extern CustomAttributesCache t44__CustomAttributeCache;
TypeInfo t841_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Int32[]", "System", t841_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t44_TI, t841_ITIs, t841_VT, &EmptyCustomAttributesCache, &t841_TI, &t841_0_0_0, &t841_1_0_0, t841_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 1057033, 1, false, false, false, false, false, true, false, false, false, false, false, true, 0, 0, 0, 0, 0, 125, 24, 28};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3529_TI;



// Metadata Definition System.IComparable`1<System.Int32>[]
static MethodInfo* t3529_MIs[] =
{
	NULL
};
static MethodInfo* t3529_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19873_MI,
	&m5907_MI,
	&m19874_MI,
	&m19875_MI,
	&m19876_MI,
	&m19877_MI,
	&m19878_MI,
	&m5908_MI,
	&m19872_MI,
	&m19880_MI,
	&m19881_MI,
};
static TypeInfo* t3529_ITIs[] = 
{
	&t5143_TI,
	&t5144_TI,
	&t5145_TI,
};
static Il2CppInterfaceOffsetPair t3529_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5143_TI, 21},
	{ &t5144_TI, 28},
	{ &t5145_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3529_0_0_0;
extern Il2CppType t3529_1_0_0;
struct t1706;
extern TypeInfo t1706_TI;
TypeInfo t3529_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1[]", "System", t3529_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1706_TI, t3529_ITIs, t3529_VT, &EmptyCustomAttributesCache, &t3529_TI, &t3529_0_0_0, &t3529_1_0_0, t3529_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29*), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3530_TI;



// Metadata Definition System.IEquatable`1<System.Int32>[]
static MethodInfo* t3530_MIs[] =
{
	NULL
};
static MethodInfo* t3530_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19884_MI,
	&m5907_MI,
	&m19885_MI,
	&m19886_MI,
	&m19887_MI,
	&m19888_MI,
	&m19889_MI,
	&m5908_MI,
	&m19883_MI,
	&m19891_MI,
	&m19892_MI,
};
static TypeInfo* t3530_ITIs[] = 
{
	&t5146_TI,
	&t5147_TI,
	&t5148_TI,
};
static Il2CppInterfaceOffsetPair t3530_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5146_TI, 21},
	{ &t5147_TI, 28},
	{ &t5148_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3530_0_0_0;
extern Il2CppType t3530_1_0_0;
struct t1707;
extern TypeInfo t1707_TI;
TypeInfo t3530_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1[]", "System", t3530_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1707_TI, t3530_ITIs, t3530_VT, &EmptyCustomAttributesCache, &t3530_TI, &t3530_0_0_0, &t3530_1_0_0, t3530_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29*), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1140_TI;



// Metadata Definition System.Double[]
static MethodInfo* t1140_MIs[] =
{
	NULL
};
extern MethodInfo m19895_MI;
extern MethodInfo m19896_MI;
extern MethodInfo m19897_MI;
extern MethodInfo m19898_MI;
extern MethodInfo m19899_MI;
extern MethodInfo m19900_MI;
extern MethodInfo m19894_MI;
extern MethodInfo m19902_MI;
extern MethodInfo m19903_MI;
extern MethodInfo m19906_MI;
extern MethodInfo m19907_MI;
extern MethodInfo m19908_MI;
extern MethodInfo m19909_MI;
extern MethodInfo m19910_MI;
extern MethodInfo m19911_MI;
extern MethodInfo m19905_MI;
extern MethodInfo m19913_MI;
extern MethodInfo m19914_MI;
extern MethodInfo m19917_MI;
extern MethodInfo m19918_MI;
extern MethodInfo m19919_MI;
extern MethodInfo m19920_MI;
extern MethodInfo m19921_MI;
extern MethodInfo m19922_MI;
extern MethodInfo m19916_MI;
extern MethodInfo m19924_MI;
extern MethodInfo m19925_MI;
static MethodInfo* t1140_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19895_MI,
	&m5907_MI,
	&m19896_MI,
	&m19897_MI,
	&m19898_MI,
	&m19899_MI,
	&m19900_MI,
	&m5908_MI,
	&m19894_MI,
	&m19902_MI,
	&m19903_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19906_MI,
	&m5907_MI,
	&m19907_MI,
	&m19908_MI,
	&m19909_MI,
	&m19910_MI,
	&m19911_MI,
	&m5908_MI,
	&m19905_MI,
	&m19913_MI,
	&m19914_MI,
	&m5905_MI,
	&m5906_MI,
	&m19917_MI,
	&m5907_MI,
	&m19918_MI,
	&m19919_MI,
	&m19920_MI,
	&m19921_MI,
	&m19922_MI,
	&m5908_MI,
	&m19916_MI,
	&m19924_MI,
	&m19925_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5149_TI;
extern TypeInfo t5150_TI;
extern TypeInfo t5151_TI;
extern TypeInfo t5152_TI;
extern TypeInfo t5153_TI;
extern TypeInfo t5154_TI;
extern TypeInfo t5155_TI;
extern TypeInfo t5156_TI;
extern TypeInfo t5157_TI;
static TypeInfo* t1140_ITIs[] = 
{
	&t5149_TI,
	&t5150_TI,
	&t5151_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5152_TI,
	&t5153_TI,
	&t5154_TI,
	&t5155_TI,
	&t5156_TI,
	&t5157_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t1140_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5149_TI, 21},
	{ &t5150_TI, 28},
	{ &t5151_TI, 33},
	{ &t5107_TI, 34},
	{ &t5108_TI, 41},
	{ &t5109_TI, 46},
	{ &t5110_TI, 47},
	{ &t5111_TI, 54},
	{ &t5112_TI, 59},
	{ &t5113_TI, 60},
	{ &t5114_TI, 67},
	{ &t5115_TI, 72},
	{ &t5152_TI, 73},
	{ &t5153_TI, 80},
	{ &t5154_TI, 85},
	{ &t5155_TI, 86},
	{ &t5156_TI, 93},
	{ &t5157_TI, 98},
	{ &t5116_TI, 99},
	{ &t5117_TI, 106},
	{ &t5118_TI, 111},
	{ &t2182_TI, 112},
	{ &t2186_TI, 119},
	{ &t2183_TI, 124},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1140_0_0_0;
extern Il2CppType t1140_1_0_0;
#include "t601.h"
extern TypeInfo t601_TI;
extern CustomAttributesCache t601__CustomAttributeCache;
extern CustomAttributesCache t601__CustomAttributeCache_m5685;
TypeInfo t1140_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Double[]", "System", t1140_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t601_TI, t1140_ITIs, t1140_VT, &EmptyCustomAttributesCache, &t1140_TI, &t1140_0_0_0, &t1140_1_0_0, t1140_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (double), -1, 0, 0, -1, 1057033, 1, false, false, false, false, false, true, false, false, false, false, false, true, 0, 0, 0, 0, 0, 125, 24, 28};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3531_TI;



// Metadata Definition System.IComparable`1<System.Double>[]
static MethodInfo* t3531_MIs[] =
{
	NULL
};
static MethodInfo* t3531_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19906_MI,
	&m5907_MI,
	&m19907_MI,
	&m19908_MI,
	&m19909_MI,
	&m19910_MI,
	&m19911_MI,
	&m5908_MI,
	&m19905_MI,
	&m19913_MI,
	&m19914_MI,
};
static TypeInfo* t3531_ITIs[] = 
{
	&t5152_TI,
	&t5153_TI,
	&t5154_TI,
};
static Il2CppInterfaceOffsetPair t3531_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5152_TI, 21},
	{ &t5153_TI, 28},
	{ &t5154_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3531_0_0_0;
extern Il2CppType t3531_1_0_0;
struct t1754;
extern TypeInfo t1754_TI;
TypeInfo t3531_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1[]", "System", t3531_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1754_TI, t3531_ITIs, t3531_VT, &EmptyCustomAttributesCache, &t3531_TI, &t3531_0_0_0, &t3531_1_0_0, t3531_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29*), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3532_TI;



// Metadata Definition System.IEquatable`1<System.Double>[]
static MethodInfo* t3532_MIs[] =
{
	NULL
};
static MethodInfo* t3532_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19917_MI,
	&m5907_MI,
	&m19918_MI,
	&m19919_MI,
	&m19920_MI,
	&m19921_MI,
	&m19922_MI,
	&m5908_MI,
	&m19916_MI,
	&m19924_MI,
	&m19925_MI,
};
static TypeInfo* t3532_ITIs[] = 
{
	&t5155_TI,
	&t5156_TI,
	&t5157_TI,
};
static Il2CppInterfaceOffsetPair t3532_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5155_TI, 21},
	{ &t5156_TI, 28},
	{ &t5157_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3532_0_0_0;
extern Il2CppType t3532_1_0_0;
struct t1755;
extern TypeInfo t1755_TI;
TypeInfo t3532_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1[]", "System", t3532_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1755_TI, t3532_ITIs, t3532_VT, &EmptyCustomAttributesCache, &t3532_TI, &t3532_0_0_0, &t3532_1_0_0, t3532_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29*), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t200_TI;



// Metadata Definition System.Char[]
static MethodInfo* t200_MIs[] =
{
	NULL
};
extern MethodInfo m19928_MI;
extern MethodInfo m19929_MI;
extern MethodInfo m19930_MI;
extern MethodInfo m19931_MI;
extern MethodInfo m19932_MI;
extern MethodInfo m19933_MI;
extern MethodInfo m19927_MI;
extern MethodInfo m19935_MI;
extern MethodInfo m19936_MI;
extern MethodInfo m19939_MI;
extern MethodInfo m19940_MI;
extern MethodInfo m19941_MI;
extern MethodInfo m19942_MI;
extern MethodInfo m19943_MI;
extern MethodInfo m19944_MI;
extern MethodInfo m19938_MI;
extern MethodInfo m19946_MI;
extern MethodInfo m19947_MI;
extern MethodInfo m19950_MI;
extern MethodInfo m19951_MI;
extern MethodInfo m19952_MI;
extern MethodInfo m19953_MI;
extern MethodInfo m19954_MI;
extern MethodInfo m19955_MI;
extern MethodInfo m19949_MI;
extern MethodInfo m19957_MI;
extern MethodInfo m19958_MI;
static MethodInfo* t200_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19928_MI,
	&m5907_MI,
	&m19929_MI,
	&m19930_MI,
	&m19931_MI,
	&m19932_MI,
	&m19933_MI,
	&m5908_MI,
	&m19927_MI,
	&m19935_MI,
	&m19936_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19939_MI,
	&m5907_MI,
	&m19940_MI,
	&m19941_MI,
	&m19942_MI,
	&m19943_MI,
	&m19944_MI,
	&m5908_MI,
	&m19938_MI,
	&m19946_MI,
	&m19947_MI,
	&m5905_MI,
	&m5906_MI,
	&m19950_MI,
	&m5907_MI,
	&m19951_MI,
	&m19952_MI,
	&m19953_MI,
	&m19954_MI,
	&m19955_MI,
	&m5908_MI,
	&m19949_MI,
	&m19957_MI,
	&m19958_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5158_TI;
extern TypeInfo t5159_TI;
extern TypeInfo t1747_TI;
extern TypeInfo t5160_TI;
extern TypeInfo t5161_TI;
extern TypeInfo t5162_TI;
extern TypeInfo t5163_TI;
extern TypeInfo t5164_TI;
extern TypeInfo t5165_TI;
static TypeInfo* t200_ITIs[] = 
{
	&t5158_TI,
	&t5159_TI,
	&t1747_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5160_TI,
	&t5161_TI,
	&t5162_TI,
	&t5163_TI,
	&t5164_TI,
	&t5165_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t200_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5158_TI, 21},
	{ &t5159_TI, 28},
	{ &t1747_TI, 33},
	{ &t5110_TI, 34},
	{ &t5111_TI, 41},
	{ &t5112_TI, 46},
	{ &t5113_TI, 47},
	{ &t5114_TI, 54},
	{ &t5115_TI, 59},
	{ &t5160_TI, 60},
	{ &t5161_TI, 67},
	{ &t5162_TI, 72},
	{ &t5163_TI, 73},
	{ &t5164_TI, 80},
	{ &t5165_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t200_0_0_0;
extern Il2CppType t200_1_0_0;
#include "t194.h"
extern TypeInfo t194_TI;
extern CustomAttributesCache t194__CustomAttributeCache;
TypeInfo t200_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Char[]", "System", t200_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t194_TI, t200_ITIs, t200_VT, &EmptyCustomAttributesCache, &t200_TI, &t200_0_0_0, &t200_1_0_0, t200_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (uint16_t), -1, sizeof(t200_SFs), 0, -1, 8457, 1, false, false, false, false, false, true, false, false, false, true, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3533_TI;



// Metadata Definition System.IComparable`1<System.Char>[]
static MethodInfo* t3533_MIs[] =
{
	NULL
};
static MethodInfo* t3533_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19939_MI,
	&m5907_MI,
	&m19940_MI,
	&m19941_MI,
	&m19942_MI,
	&m19943_MI,
	&m19944_MI,
	&m5908_MI,
	&m19938_MI,
	&m19946_MI,
	&m19947_MI,
};
static TypeInfo* t3533_ITIs[] = 
{
	&t5160_TI,
	&t5161_TI,
	&t5162_TI,
};
static Il2CppInterfaceOffsetPair t3533_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5160_TI, 21},
	{ &t5161_TI, 28},
	{ &t5162_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3533_0_0_0;
extern Il2CppType t3533_1_0_0;
struct t1739;
extern TypeInfo t1739_TI;
TypeInfo t3533_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1[]", "System", t3533_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1739_TI, t3533_ITIs, t3533_VT, &EmptyCustomAttributesCache, &t3533_TI, &t3533_0_0_0, &t3533_1_0_0, t3533_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29*), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3534_TI;



// Metadata Definition System.IEquatable`1<System.Char>[]
static MethodInfo* t3534_MIs[] =
{
	NULL
};
static MethodInfo* t3534_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19950_MI,
	&m5907_MI,
	&m19951_MI,
	&m19952_MI,
	&m19953_MI,
	&m19954_MI,
	&m19955_MI,
	&m5908_MI,
	&m19949_MI,
	&m19957_MI,
	&m19958_MI,
};
static TypeInfo* t3534_ITIs[] = 
{
	&t5163_TI,
	&t5164_TI,
	&t5165_TI,
};
static Il2CppInterfaceOffsetPair t3534_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5163_TI, 21},
	{ &t5164_TI, 28},
	{ &t5165_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3534_0_0_0;
extern Il2CppType t3534_1_0_0;
struct t1740;
extern TypeInfo t1740_TI;
TypeInfo t3534_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1[]", "System", t3534_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1740_TI, t3534_ITIs, t3534_VT, &EmptyCustomAttributesCache, &t3534_TI, &t3534_0_0_0, &t3534_1_0_0, t3534_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29*), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2172_TI;



// Metadata Definition UnityEngine.EventSystems.BaseInputModule[]
static MethodInfo* t2172_MIs[] =
{
	NULL
};
extern MethodInfo m19968_MI;
extern MethodInfo m19969_MI;
extern MethodInfo m19970_MI;
extern MethodInfo m19971_MI;
extern MethodInfo m19972_MI;
extern MethodInfo m19973_MI;
extern MethodInfo m19967_MI;
extern MethodInfo m19975_MI;
extern MethodInfo m19976_MI;
static MethodInfo* t2172_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m19968_MI,
	&m5907_MI,
	&m19969_MI,
	&m19970_MI,
	&m19971_MI,
	&m19972_MI,
	&m19973_MI,
	&m5908_MI,
	&m19967_MI,
	&m19975_MI,
	&m19976_MI,
	&m5905_MI,
	&m5906_MI,
	&m19777_MI,
	&m5907_MI,
	&m19778_MI,
	&m19779_MI,
	&m19780_MI,
	&m19781_MI,
	&m19782_MI,
	&m5908_MI,
	&m19776_MI,
	&m19784_MI,
	&m19785_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t2174_TI;
extern TypeInfo t2207_TI;
extern TypeInfo t2175_TI;
static TypeInfo* t2172_ITIs[] = 
{
	&t2174_TI,
	&t2207_TI,
	&t2175_TI,
	&t5122_TI,
	&t5123_TI,
	&t5124_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2172_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2174_TI, 21},
	{ &t2207_TI, 28},
	{ &t2175_TI, 33},
	{ &t5122_TI, 34},
	{ &t5123_TI, 41},
	{ &t5124_TI, 46},
	{ &t5059_TI, 47},
	{ &t5060_TI, 54},
	{ &t5061_TI, 59},
	{ &t5062_TI, 60},
	{ &t5063_TI, 67},
	{ &t5064_TI, 72},
	{ &t2240_TI, 73},
	{ &t2245_TI, 80},
	{ &t2241_TI, 85},
	{ &t5065_TI, 86},
	{ &t5066_TI, 93},
	{ &t5067_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t2172_0_0_0;
extern Il2CppType t2172_1_0_0;
struct t52;
extern TypeInfo t52_TI;
extern CustomAttributesCache t52__CustomAttributeCache;
TypeInfo t2172_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "BaseInputModule[]", "UnityEngine.EventSystems", t2172_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t52_TI, t2172_ITIs, t2172_VT, &EmptyCustomAttributesCache, &t2172_TI, &t2172_0_0_0, &t2172_1_0_0, t2172_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t52 *), -1, 0, 0, -1, 1048705, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2231_TI;



// Metadata Definition System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>[]
static MethodInfo* t2231_MIs[] =
{
	NULL
};
extern MethodInfo m20011_MI;
extern MethodInfo m20012_MI;
extern MethodInfo m20013_MI;
extern MethodInfo m20014_MI;
extern MethodInfo m20015_MI;
extern MethodInfo m20016_MI;
extern MethodInfo m20010_MI;
extern MethodInfo m20018_MI;
extern MethodInfo m20019_MI;
extern MethodInfo m20022_MI;
extern MethodInfo m20023_MI;
extern MethodInfo m20024_MI;
extern MethodInfo m20025_MI;
extern MethodInfo m20026_MI;
extern MethodInfo m20027_MI;
extern MethodInfo m20021_MI;
extern MethodInfo m20029_MI;
extern MethodInfo m20030_MI;
extern MethodInfo m20033_MI;
extern MethodInfo m20034_MI;
extern MethodInfo m20035_MI;
extern MethodInfo m20036_MI;
extern MethodInfo m20037_MI;
extern MethodInfo m20038_MI;
extern MethodInfo m20032_MI;
extern MethodInfo m20040_MI;
extern MethodInfo m20041_MI;
extern MethodInfo m20044_MI;
extern MethodInfo m20045_MI;
extern MethodInfo m20046_MI;
extern MethodInfo m20047_MI;
extern MethodInfo m20048_MI;
extern MethodInfo m20049_MI;
extern MethodInfo m20043_MI;
extern MethodInfo m20051_MI;
extern MethodInfo m20052_MI;
static MethodInfo* t2231_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20011_MI,
	&m5907_MI,
	&m20012_MI,
	&m20013_MI,
	&m20014_MI,
	&m20015_MI,
	&m20016_MI,
	&m5908_MI,
	&m20010_MI,
	&m20018_MI,
	&m20019_MI,
	&m5905_MI,
	&m5906_MI,
	&m20022_MI,
	&m5907_MI,
	&m20023_MI,
	&m20024_MI,
	&m20025_MI,
	&m20026_MI,
	&m20027_MI,
	&m5908_MI,
	&m20021_MI,
	&m20029_MI,
	&m20030_MI,
	&m5905_MI,
	&m5906_MI,
	&m20033_MI,
	&m5907_MI,
	&m20034_MI,
	&m20035_MI,
	&m20036_MI,
	&m20037_MI,
	&m20038_MI,
	&m5908_MI,
	&m20032_MI,
	&m20040_MI,
	&m20041_MI,
	&m5905_MI,
	&m5906_MI,
	&m20044_MI,
	&m5907_MI,
	&m20045_MI,
	&m20046_MI,
	&m20047_MI,
	&m20048_MI,
	&m20049_MI,
	&m5908_MI,
	&m20043_MI,
	&m20051_MI,
	&m20052_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5166_TI;
extern TypeInfo t5167_TI;
extern TypeInfo t5168_TI;
extern TypeInfo t5169_TI;
extern TypeInfo t5170_TI;
extern TypeInfo t5171_TI;
extern TypeInfo t5172_TI;
extern TypeInfo t5173_TI;
extern TypeInfo t5174_TI;
extern TypeInfo t5175_TI;
extern TypeInfo t5176_TI;
extern TypeInfo t5177_TI;
static TypeInfo* t2231_ITIs[] = 
{
	&t5166_TI,
	&t5167_TI,
	&t5168_TI,
	&t5169_TI,
	&t5170_TI,
	&t5171_TI,
	&t5172_TI,
	&t5173_TI,
	&t5174_TI,
	&t5175_TI,
	&t5176_TI,
	&t5177_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2231_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5166_TI, 21},
	{ &t5167_TI, 28},
	{ &t5168_TI, 33},
	{ &t5169_TI, 34},
	{ &t5170_TI, 41},
	{ &t5171_TI, 46},
	{ &t5172_TI, 47},
	{ &t5173_TI, 54},
	{ &t5174_TI, 59},
	{ &t5175_TI, 60},
	{ &t5176_TI, 67},
	{ &t5177_TI, 72},
	{ &t2182_TI, 73},
	{ &t2186_TI, 80},
	{ &t2183_TI, 85},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2231_0_0_0;
extern Il2CppType t2231_1_0_0;
struct t105;
extern TypeInfo t105_TI;
extern CustomAttributesCache t1261__CustomAttributeCache;
TypeInfo t2231_TI = 
{
	&g_mscorlib_dll_Image, NULL, "List`1[]", "System.Collections.Generic", t2231_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t105_TI, t2231_ITIs, t2231_VT, &EmptyCustomAttributesCache, &t2231_TI, &t2231_0_0_0, &t2231_1_0_0, t2231_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t105 *), -1, sizeof(t2231_SFs), 0, -1, 1056769, 1, false, false, false, false, false, true, false, false, false, true, false, false, 0, 0, 0, 0, 0, 86, 15, 19};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3535_TI;



// Metadata Definition System.Collections.IEnumerable[]
static MethodInfo* t3535_MIs[] =
{
	NULL
};
static MethodInfo* t3535_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20022_MI,
	&m5907_MI,
	&m20023_MI,
	&m20024_MI,
	&m20025_MI,
	&m20026_MI,
	&m20027_MI,
	&m5908_MI,
	&m20021_MI,
	&m20029_MI,
	&m20030_MI,
};
static TypeInfo* t3535_ITIs[] = 
{
	&t5169_TI,
	&t5170_TI,
	&t5171_TI,
};
static Il2CppInterfaceOffsetPair t3535_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5169_TI, 21},
	{ &t5170_TI, 28},
	{ &t5171_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3535_0_0_0;
extern Il2CppType t3535_1_0_0;
struct t603;
extern CustomAttributesCache t603__CustomAttributeCache;
extern CustomAttributesCache t603__CustomAttributeCache_m4154;
TypeInfo t3535_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable[]", "System.Collections", t3535_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t603_TI, t3535_ITIs, t3535_VT, &EmptyCustomAttributesCache, &t3535_TI, &t3535_0_0_0, &t3535_1_0_0, t3535_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3536_TI;



// Metadata Definition System.Collections.ICollection[]
static MethodInfo* t3536_MIs[] =
{
	NULL
};
static MethodInfo* t3536_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20033_MI,
	&m5907_MI,
	&m20034_MI,
	&m20035_MI,
	&m20036_MI,
	&m20037_MI,
	&m20038_MI,
	&m5908_MI,
	&m20032_MI,
	&m20040_MI,
	&m20041_MI,
	&m5905_MI,
	&m5906_MI,
	&m20022_MI,
	&m5907_MI,
	&m20023_MI,
	&m20024_MI,
	&m20025_MI,
	&m20026_MI,
	&m20027_MI,
	&m5908_MI,
	&m20021_MI,
	&m20029_MI,
	&m20030_MI,
};
static TypeInfo* t3536_ITIs[] = 
{
	&t5172_TI,
	&t5173_TI,
	&t5174_TI,
	&t5169_TI,
	&t5170_TI,
	&t5171_TI,
};
static Il2CppInterfaceOffsetPair t3536_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5172_TI, 21},
	{ &t5173_TI, 28},
	{ &t5174_TI, 33},
	{ &t5169_TI, 34},
	{ &t5170_TI, 41},
	{ &t5171_TI, 46},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3536_0_0_0;
extern Il2CppType t3536_1_0_0;
struct t674;
extern CustomAttributesCache t674__CustomAttributeCache;
TypeInfo t3536_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection[]", "System.Collections", t3536_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t674_TI, t3536_ITIs, t3536_VT, &EmptyCustomAttributesCache, &t3536_TI, &t3536_0_0_0, &t3536_1_0_0, t3536_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3537_TI;



// Metadata Definition System.Collections.IList[]
static MethodInfo* t3537_MIs[] =
{
	NULL
};
static MethodInfo* t3537_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20044_MI,
	&m5907_MI,
	&m20045_MI,
	&m20046_MI,
	&m20047_MI,
	&m20048_MI,
	&m20049_MI,
	&m5908_MI,
	&m20043_MI,
	&m20051_MI,
	&m20052_MI,
	&m5905_MI,
	&m5906_MI,
	&m20022_MI,
	&m5907_MI,
	&m20023_MI,
	&m20024_MI,
	&m20025_MI,
	&m20026_MI,
	&m20027_MI,
	&m5908_MI,
	&m20021_MI,
	&m20029_MI,
	&m20030_MI,
	&m5905_MI,
	&m5906_MI,
	&m20033_MI,
	&m5907_MI,
	&m20034_MI,
	&m20035_MI,
	&m20036_MI,
	&m20037_MI,
	&m20038_MI,
	&m5908_MI,
	&m20032_MI,
	&m20040_MI,
	&m20041_MI,
};
static TypeInfo* t3537_ITIs[] = 
{
	&t5175_TI,
	&t5176_TI,
	&t5177_TI,
	&t5169_TI,
	&t5170_TI,
	&t5171_TI,
	&t5172_TI,
	&t5173_TI,
	&t5174_TI,
};
static Il2CppInterfaceOffsetPair t3537_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5175_TI, 21},
	{ &t5176_TI, 28},
	{ &t5177_TI, 33},
	{ &t5169_TI, 34},
	{ &t5170_TI, 41},
	{ &t5171_TI, 46},
	{ &t5172_TI, 47},
	{ &t5173_TI, 54},
	{ &t5174_TI, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3537_0_0_0;
extern Il2CppType t3537_1_0_0;
struct t868;
extern CustomAttributesCache t868__CustomAttributeCache;
TypeInfo t3537_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList[]", "System.Collections", t3537_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t868_TI, t3537_ITIs, t3537_VT, &EmptyCustomAttributesCache, &t3537_TI, &t3537_0_0_0, &t3537_1_0_0, t3537_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2251_TI;



// Metadata Definition UnityEngine.EventSystems.RaycastResult[]
static MethodInfo* t2251_MIs[] =
{
	NULL
};
extern MethodInfo m20079_MI;
extern MethodInfo m20080_MI;
extern MethodInfo m20081_MI;
extern MethodInfo m20082_MI;
extern MethodInfo m20083_MI;
extern MethodInfo m20084_MI;
extern MethodInfo m20078_MI;
extern MethodInfo m20086_MI;
extern MethodInfo m20087_MI;
static MethodInfo* t2251_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20079_MI,
	&m5907_MI,
	&m20080_MI,
	&m20081_MI,
	&m20082_MI,
	&m20083_MI,
	&m20084_MI,
	&m5908_MI,
	&m20078_MI,
	&m20086_MI,
	&m20087_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t2253_TI;
extern TypeInfo t2259_TI;
extern TypeInfo t2254_TI;
static TypeInfo* t2251_ITIs[] = 
{
	&t2253_TI,
	&t2259_TI,
	&t2254_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2251_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2253_TI, 21},
	{ &t2259_TI, 28},
	{ &t2254_TI, 33},
	{ &t5116_TI, 34},
	{ &t5117_TI, 41},
	{ &t5118_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t2251_0_0_0;
extern Il2CppType t2251_1_0_0;
#include "t57.h"
extern TypeInfo t57_TI;
TypeInfo t2251_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "RaycastResult[]", "UnityEngine.EventSystems", t2251_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t57_TI, t2251_ITIs, t2251_VT, &EmptyCustomAttributesCache, &t2251_TI, &t2251_0_0_0, &t2251_1_0_0, t2251_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t57 ), -1, 0, 0, -1, 1048841, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2265_TI;



// Metadata Definition UnityEngine.EventSystems.BaseRaycaster[]
static MethodInfo* t2265_MIs[] =
{
	NULL
};
extern MethodInfo m20105_MI;
extern MethodInfo m20106_MI;
extern MethodInfo m20107_MI;
extern MethodInfo m20108_MI;
extern MethodInfo m20109_MI;
extern MethodInfo m20110_MI;
extern MethodInfo m20104_MI;
extern MethodInfo m20112_MI;
extern MethodInfo m20113_MI;
static MethodInfo* t2265_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20105_MI,
	&m5907_MI,
	&m20106_MI,
	&m20107_MI,
	&m20108_MI,
	&m20109_MI,
	&m20110_MI,
	&m5908_MI,
	&m20104_MI,
	&m20112_MI,
	&m20113_MI,
	&m5905_MI,
	&m5906_MI,
	&m19777_MI,
	&m5907_MI,
	&m19778_MI,
	&m19779_MI,
	&m19780_MI,
	&m19781_MI,
	&m19782_MI,
	&m5908_MI,
	&m19776_MI,
	&m19784_MI,
	&m19785_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t2267_TI;
extern TypeInfo t2274_TI;
extern TypeInfo t2268_TI;
static TypeInfo* t2265_ITIs[] = 
{
	&t2267_TI,
	&t2274_TI,
	&t2268_TI,
	&t5122_TI,
	&t5123_TI,
	&t5124_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2265_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2267_TI, 21},
	{ &t2274_TI, 28},
	{ &t2268_TI, 33},
	{ &t5122_TI, 34},
	{ &t5123_TI, 41},
	{ &t5124_TI, 46},
	{ &t5059_TI, 47},
	{ &t5060_TI, 54},
	{ &t5061_TI, 59},
	{ &t5062_TI, 60},
	{ &t5063_TI, 67},
	{ &t5064_TI, 72},
	{ &t2240_TI, 73},
	{ &t2245_TI, 80},
	{ &t2241_TI, 85},
	{ &t5065_TI, 86},
	{ &t5066_TI, 93},
	{ &t5067_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t2265_0_0_0;
extern Il2CppType t2265_1_0_0;
struct t109;
extern TypeInfo t109_TI;
extern CustomAttributesCache t109__CustomAttributeCache_t109____priority_PropertyInfo;
TypeInfo t2265_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "BaseRaycaster[]", "UnityEngine.EventSystems", t2265_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t109_TI, t2265_ITIs, t2265_VT, &EmptyCustomAttributesCache, &t2265_TI, &t2265_0_0_0, &t2265_1_0_0, t2265_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t109 *), -1, 0, 0, -1, 1048705, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3803_TI;



// Metadata Definition UnityEngine.EventSystems.EventTrigger[]
static MethodInfo* t3803_MIs[] =
{
	NULL
};
extern MethodInfo m20131_MI;
extern MethodInfo m20132_MI;
extern MethodInfo m20133_MI;
extern MethodInfo m20134_MI;
extern MethodInfo m20135_MI;
extern MethodInfo m20136_MI;
extern MethodInfo m20130_MI;
extern MethodInfo m20138_MI;
extern MethodInfo m20139_MI;
extern MethodInfo m20142_MI;
extern MethodInfo m20143_MI;
extern MethodInfo m20144_MI;
extern MethodInfo m20145_MI;
extern MethodInfo m20146_MI;
extern MethodInfo m20147_MI;
extern MethodInfo m20141_MI;
extern MethodInfo m20149_MI;
extern MethodInfo m20150_MI;
extern MethodInfo m20153_MI;
extern MethodInfo m20154_MI;
extern MethodInfo m20155_MI;
extern MethodInfo m20156_MI;
extern MethodInfo m20157_MI;
extern MethodInfo m20158_MI;
extern MethodInfo m20152_MI;
extern MethodInfo m20160_MI;
extern MethodInfo m20161_MI;
extern MethodInfo m20164_MI;
extern MethodInfo m20165_MI;
extern MethodInfo m20166_MI;
extern MethodInfo m20167_MI;
extern MethodInfo m20168_MI;
extern MethodInfo m20169_MI;
extern MethodInfo m20163_MI;
extern MethodInfo m20171_MI;
extern MethodInfo m20172_MI;
extern MethodInfo m20175_MI;
extern MethodInfo m20176_MI;
extern MethodInfo m20177_MI;
extern MethodInfo m20178_MI;
extern MethodInfo m20179_MI;
extern MethodInfo m20180_MI;
extern MethodInfo m20174_MI;
extern MethodInfo m20182_MI;
extern MethodInfo m20183_MI;
extern MethodInfo m20186_MI;
extern MethodInfo m20187_MI;
extern MethodInfo m20188_MI;
extern MethodInfo m20189_MI;
extern MethodInfo m20190_MI;
extern MethodInfo m20191_MI;
extern MethodInfo m20185_MI;
extern MethodInfo m20193_MI;
extern MethodInfo m20194_MI;
extern MethodInfo m20197_MI;
extern MethodInfo m20198_MI;
extern MethodInfo m20199_MI;
extern MethodInfo m20200_MI;
extern MethodInfo m20201_MI;
extern MethodInfo m20202_MI;
extern MethodInfo m20196_MI;
extern MethodInfo m20204_MI;
extern MethodInfo m20205_MI;
extern MethodInfo m20208_MI;
extern MethodInfo m20209_MI;
extern MethodInfo m20210_MI;
extern MethodInfo m20211_MI;
extern MethodInfo m20212_MI;
extern MethodInfo m20213_MI;
extern MethodInfo m20207_MI;
extern MethodInfo m20215_MI;
extern MethodInfo m20216_MI;
extern MethodInfo m20219_MI;
extern MethodInfo m20220_MI;
extern MethodInfo m20221_MI;
extern MethodInfo m20222_MI;
extern MethodInfo m20223_MI;
extern MethodInfo m20224_MI;
extern MethodInfo m20218_MI;
extern MethodInfo m20226_MI;
extern MethodInfo m20227_MI;
extern MethodInfo m20230_MI;
extern MethodInfo m20231_MI;
extern MethodInfo m20232_MI;
extern MethodInfo m20233_MI;
extern MethodInfo m20234_MI;
extern MethodInfo m20235_MI;
extern MethodInfo m20229_MI;
extern MethodInfo m20237_MI;
extern MethodInfo m20238_MI;
extern MethodInfo m20241_MI;
extern MethodInfo m20242_MI;
extern MethodInfo m20243_MI;
extern MethodInfo m20244_MI;
extern MethodInfo m20245_MI;
extern MethodInfo m20246_MI;
extern MethodInfo m20240_MI;
extern MethodInfo m20248_MI;
extern MethodInfo m20249_MI;
extern MethodInfo m20252_MI;
extern MethodInfo m20253_MI;
extern MethodInfo m20254_MI;
extern MethodInfo m20255_MI;
extern MethodInfo m20256_MI;
extern MethodInfo m20257_MI;
extern MethodInfo m20251_MI;
extern MethodInfo m20259_MI;
extern MethodInfo m20260_MI;
extern MethodInfo m20263_MI;
extern MethodInfo m20264_MI;
extern MethodInfo m20265_MI;
extern MethodInfo m20266_MI;
extern MethodInfo m20267_MI;
extern MethodInfo m20268_MI;
extern MethodInfo m20262_MI;
extern MethodInfo m20270_MI;
extern MethodInfo m20271_MI;
extern MethodInfo m20274_MI;
extern MethodInfo m20275_MI;
extern MethodInfo m20276_MI;
extern MethodInfo m20277_MI;
extern MethodInfo m20278_MI;
extern MethodInfo m20279_MI;
extern MethodInfo m20273_MI;
extern MethodInfo m20281_MI;
extern MethodInfo m20282_MI;
extern MethodInfo m20285_MI;
extern MethodInfo m20286_MI;
extern MethodInfo m20287_MI;
extern MethodInfo m20288_MI;
extern MethodInfo m20289_MI;
extern MethodInfo m20290_MI;
extern MethodInfo m20284_MI;
extern MethodInfo m20292_MI;
extern MethodInfo m20293_MI;
extern MethodInfo m20296_MI;
extern MethodInfo m20297_MI;
extern MethodInfo m20298_MI;
extern MethodInfo m20299_MI;
extern MethodInfo m20300_MI;
extern MethodInfo m20301_MI;
extern MethodInfo m20295_MI;
extern MethodInfo m20303_MI;
extern MethodInfo m20304_MI;
static MethodInfo* t3803_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20131_MI,
	&m5907_MI,
	&m20132_MI,
	&m20133_MI,
	&m20134_MI,
	&m20135_MI,
	&m20136_MI,
	&m5908_MI,
	&m20130_MI,
	&m20138_MI,
	&m20139_MI,
	&m5905_MI,
	&m5906_MI,
	&m19581_MI,
	&m5907_MI,
	&m19582_MI,
	&m19583_MI,
	&m19584_MI,
	&m19585_MI,
	&m19586_MI,
	&m5908_MI,
	&m19580_MI,
	&m19588_MI,
	&m19589_MI,
	&m5905_MI,
	&m5906_MI,
	&m19570_MI,
	&m5907_MI,
	&m19571_MI,
	&m19572_MI,
	&m19573_MI,
	&m19574_MI,
	&m19575_MI,
	&m5908_MI,
	&m19569_MI,
	&m19577_MI,
	&m19578_MI,
	&m5905_MI,
	&m5906_MI,
	&m19592_MI,
	&m5907_MI,
	&m19593_MI,
	&m19594_MI,
	&m19595_MI,
	&m19596_MI,
	&m19597_MI,
	&m5908_MI,
	&m19591_MI,
	&m19599_MI,
	&m19600_MI,
	&m5905_MI,
	&m5906_MI,
	&m20142_MI,
	&m5907_MI,
	&m20143_MI,
	&m20144_MI,
	&m20145_MI,
	&m20146_MI,
	&m20147_MI,
	&m5908_MI,
	&m20141_MI,
	&m20149_MI,
	&m20150_MI,
	&m5905_MI,
	&m5906_MI,
	&m20153_MI,
	&m5907_MI,
	&m20154_MI,
	&m20155_MI,
	&m20156_MI,
	&m20157_MI,
	&m20158_MI,
	&m5908_MI,
	&m20152_MI,
	&m20160_MI,
	&m20161_MI,
	&m5905_MI,
	&m5906_MI,
	&m20164_MI,
	&m5907_MI,
	&m20165_MI,
	&m20166_MI,
	&m20167_MI,
	&m20168_MI,
	&m20169_MI,
	&m5908_MI,
	&m20163_MI,
	&m20171_MI,
	&m20172_MI,
	&m5905_MI,
	&m5906_MI,
	&m20175_MI,
	&m5907_MI,
	&m20176_MI,
	&m20177_MI,
	&m20178_MI,
	&m20179_MI,
	&m20180_MI,
	&m5908_MI,
	&m20174_MI,
	&m20182_MI,
	&m20183_MI,
	&m5905_MI,
	&m5906_MI,
	&m20186_MI,
	&m5907_MI,
	&m20187_MI,
	&m20188_MI,
	&m20189_MI,
	&m20190_MI,
	&m20191_MI,
	&m5908_MI,
	&m20185_MI,
	&m20193_MI,
	&m20194_MI,
	&m5905_MI,
	&m5906_MI,
	&m20197_MI,
	&m5907_MI,
	&m20198_MI,
	&m20199_MI,
	&m20200_MI,
	&m20201_MI,
	&m20202_MI,
	&m5908_MI,
	&m20196_MI,
	&m20204_MI,
	&m20205_MI,
	&m5905_MI,
	&m5906_MI,
	&m20208_MI,
	&m5907_MI,
	&m20209_MI,
	&m20210_MI,
	&m20211_MI,
	&m20212_MI,
	&m20213_MI,
	&m5908_MI,
	&m20207_MI,
	&m20215_MI,
	&m20216_MI,
	&m5905_MI,
	&m5906_MI,
	&m20219_MI,
	&m5907_MI,
	&m20220_MI,
	&m20221_MI,
	&m20222_MI,
	&m20223_MI,
	&m20224_MI,
	&m5908_MI,
	&m20218_MI,
	&m20226_MI,
	&m20227_MI,
	&m5905_MI,
	&m5906_MI,
	&m20230_MI,
	&m5907_MI,
	&m20231_MI,
	&m20232_MI,
	&m20233_MI,
	&m20234_MI,
	&m20235_MI,
	&m5908_MI,
	&m20229_MI,
	&m20237_MI,
	&m20238_MI,
	&m5905_MI,
	&m5906_MI,
	&m20241_MI,
	&m5907_MI,
	&m20242_MI,
	&m20243_MI,
	&m20244_MI,
	&m20245_MI,
	&m20246_MI,
	&m5908_MI,
	&m20240_MI,
	&m20248_MI,
	&m20249_MI,
	&m5905_MI,
	&m5906_MI,
	&m20252_MI,
	&m5907_MI,
	&m20253_MI,
	&m20254_MI,
	&m20255_MI,
	&m20256_MI,
	&m20257_MI,
	&m5908_MI,
	&m20251_MI,
	&m20259_MI,
	&m20260_MI,
	&m5905_MI,
	&m5906_MI,
	&m20263_MI,
	&m5907_MI,
	&m20264_MI,
	&m20265_MI,
	&m20266_MI,
	&m20267_MI,
	&m20268_MI,
	&m5908_MI,
	&m20262_MI,
	&m20270_MI,
	&m20271_MI,
	&m5905_MI,
	&m5906_MI,
	&m20274_MI,
	&m5907_MI,
	&m20275_MI,
	&m20276_MI,
	&m20277_MI,
	&m20278_MI,
	&m20279_MI,
	&m5908_MI,
	&m20273_MI,
	&m20281_MI,
	&m20282_MI,
	&m5905_MI,
	&m5906_MI,
	&m20285_MI,
	&m5907_MI,
	&m20286_MI,
	&m20287_MI,
	&m20288_MI,
	&m20289_MI,
	&m20290_MI,
	&m5908_MI,
	&m20284_MI,
	&m20292_MI,
	&m20293_MI,
	&m5905_MI,
	&m5906_MI,
	&m20296_MI,
	&m5907_MI,
	&m20297_MI,
	&m20298_MI,
	&m20299_MI,
	&m20300_MI,
	&m20301_MI,
	&m5908_MI,
	&m20295_MI,
	&m20303_MI,
	&m20304_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5178_TI;
extern TypeInfo t5179_TI;
extern TypeInfo t5180_TI;
extern TypeInfo t5181_TI;
extern TypeInfo t5182_TI;
extern TypeInfo t5183_TI;
extern TypeInfo t5184_TI;
extern TypeInfo t5185_TI;
extern TypeInfo t5186_TI;
extern TypeInfo t5187_TI;
extern TypeInfo t5188_TI;
extern TypeInfo t5189_TI;
extern TypeInfo t5190_TI;
extern TypeInfo t5191_TI;
extern TypeInfo t5192_TI;
extern TypeInfo t5193_TI;
extern TypeInfo t5194_TI;
extern TypeInfo t5195_TI;
extern TypeInfo t5196_TI;
extern TypeInfo t5197_TI;
extern TypeInfo t5198_TI;
extern TypeInfo t5199_TI;
extern TypeInfo t5200_TI;
extern TypeInfo t5201_TI;
extern TypeInfo t5202_TI;
extern TypeInfo t5203_TI;
extern TypeInfo t5204_TI;
extern TypeInfo t5205_TI;
extern TypeInfo t5206_TI;
extern TypeInfo t5207_TI;
extern TypeInfo t5208_TI;
extern TypeInfo t5209_TI;
extern TypeInfo t5210_TI;
extern TypeInfo t5211_TI;
extern TypeInfo t5212_TI;
extern TypeInfo t5213_TI;
extern TypeInfo t5214_TI;
extern TypeInfo t5215_TI;
extern TypeInfo t5216_TI;
extern TypeInfo t5217_TI;
extern TypeInfo t5218_TI;
extern TypeInfo t5219_TI;
extern TypeInfo t5220_TI;
extern TypeInfo t5221_TI;
extern TypeInfo t5222_TI;
extern TypeInfo t5223_TI;
extern TypeInfo t5224_TI;
extern TypeInfo t5225_TI;
static TypeInfo* t3803_ITIs[] = 
{
	&t5178_TI,
	&t5179_TI,
	&t5180_TI,
	&t2216_TI,
	&t312_TI,
	&t2217_TI,
	&t5071_TI,
	&t5072_TI,
	&t5073_TI,
	&t5074_TI,
	&t5075_TI,
	&t5076_TI,
	&t5181_TI,
	&t5182_TI,
	&t5183_TI,
	&t5184_TI,
	&t5185_TI,
	&t5186_TI,
	&t5187_TI,
	&t5188_TI,
	&t5189_TI,
	&t5190_TI,
	&t5191_TI,
	&t5192_TI,
	&t5193_TI,
	&t5194_TI,
	&t5195_TI,
	&t5196_TI,
	&t5197_TI,
	&t5198_TI,
	&t5199_TI,
	&t5200_TI,
	&t5201_TI,
	&t5202_TI,
	&t5203_TI,
	&t5204_TI,
	&t5205_TI,
	&t5206_TI,
	&t5207_TI,
	&t5208_TI,
	&t5209_TI,
	&t5210_TI,
	&t5211_TI,
	&t5212_TI,
	&t5213_TI,
	&t5214_TI,
	&t5215_TI,
	&t5216_TI,
	&t5217_TI,
	&t5218_TI,
	&t5219_TI,
	&t5220_TI,
	&t5221_TI,
	&t5222_TI,
	&t5223_TI,
	&t5224_TI,
	&t5225_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3803_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5178_TI, 21},
	{ &t5179_TI, 28},
	{ &t5180_TI, 33},
	{ &t2216_TI, 34},
	{ &t312_TI, 41},
	{ &t2217_TI, 46},
	{ &t5071_TI, 47},
	{ &t5072_TI, 54},
	{ &t5073_TI, 59},
	{ &t5074_TI, 60},
	{ &t5075_TI, 67},
	{ &t5076_TI, 72},
	{ &t5181_TI, 73},
	{ &t5182_TI, 80},
	{ &t5183_TI, 85},
	{ &t5184_TI, 86},
	{ &t5185_TI, 93},
	{ &t5186_TI, 98},
	{ &t5187_TI, 99},
	{ &t5188_TI, 106},
	{ &t5189_TI, 111},
	{ &t5190_TI, 112},
	{ &t5191_TI, 119},
	{ &t5192_TI, 124},
	{ &t5193_TI, 125},
	{ &t5194_TI, 132},
	{ &t5195_TI, 137},
	{ &t5196_TI, 138},
	{ &t5197_TI, 145},
	{ &t5198_TI, 150},
	{ &t5199_TI, 151},
	{ &t5200_TI, 158},
	{ &t5201_TI, 163},
	{ &t5202_TI, 164},
	{ &t5203_TI, 171},
	{ &t5204_TI, 176},
	{ &t5205_TI, 177},
	{ &t5206_TI, 184},
	{ &t5207_TI, 189},
	{ &t5208_TI, 190},
	{ &t5209_TI, 197},
	{ &t5210_TI, 202},
	{ &t5211_TI, 203},
	{ &t5212_TI, 210},
	{ &t5213_TI, 215},
	{ &t5214_TI, 216},
	{ &t5215_TI, 223},
	{ &t5216_TI, 228},
	{ &t5217_TI, 229},
	{ &t5218_TI, 236},
	{ &t5219_TI, 241},
	{ &t5220_TI, 242},
	{ &t5221_TI, 249},
	{ &t5222_TI, 254},
	{ &t5223_TI, 255},
	{ &t5224_TI, 262},
	{ &t5225_TI, 267},
	{ &t5059_TI, 268},
	{ &t5060_TI, 275},
	{ &t5061_TI, 280},
	{ &t5062_TI, 281},
	{ &t5063_TI, 288},
	{ &t5064_TI, 293},
	{ &t2240_TI, 294},
	{ &t2245_TI, 301},
	{ &t2241_TI, 306},
	{ &t5065_TI, 307},
	{ &t5066_TI, 314},
	{ &t5067_TI, 319},
	{ &t2182_TI, 320},
	{ &t2186_TI, 327},
	{ &t2183_TI, 332},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3803_0_0_0;
extern Il2CppType t3803_1_0_0;
struct t62;
extern TypeInfo t62_TI;
extern CustomAttributesCache t62__CustomAttributeCache;
TypeInfo t3803_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "EventTrigger[]", "UnityEngine.EventSystems", t3803_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t62_TI, t3803_ITIs, t3803_VT, &EmptyCustomAttributesCache, &t3803_TI, &t3803_0_0_0, &t3803_1_0_0, t3803_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t62 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 333, 72, 76};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3804_TI;



// Metadata Definition UnityEngine.EventSystems.IPointerDownHandler[]
static MethodInfo* t3804_MIs[] =
{
	NULL
};
static MethodInfo* t3804_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20142_MI,
	&m5907_MI,
	&m20143_MI,
	&m20144_MI,
	&m20145_MI,
	&m20146_MI,
	&m20147_MI,
	&m5908_MI,
	&m20141_MI,
	&m20149_MI,
	&m20150_MI,
	&m5905_MI,
	&m5906_MI,
	&m19581_MI,
	&m5907_MI,
	&m19582_MI,
	&m19583_MI,
	&m19584_MI,
	&m19585_MI,
	&m19586_MI,
	&m5908_MI,
	&m19580_MI,
	&m19588_MI,
	&m19589_MI,
};
static TypeInfo* t3804_ITIs[] = 
{
	&t5181_TI,
	&t5182_TI,
	&t5183_TI,
	&t2216_TI,
	&t312_TI,
	&t2217_TI,
};
static Il2CppInterfaceOffsetPair t3804_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5181_TI, 21},
	{ &t5182_TI, 28},
	{ &t5183_TI, 33},
	{ &t2216_TI, 34},
	{ &t312_TI, 41},
	{ &t2217_TI, 46},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3804_0_0_0;
extern Il2CppType t3804_1_0_0;
struct t89;
extern TypeInfo t89_TI;
TypeInfo t3804_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "IPointerDownHandler[]", "UnityEngine.EventSystems", t3804_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t89_TI, t3804_ITIs, t3804_VT, &EmptyCustomAttributesCache, &t3804_TI, &t3804_0_0_0, &t3804_1_0_0, t3804_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3805_TI;



// Metadata Definition UnityEngine.EventSystems.IPointerUpHandler[]
static MethodInfo* t3805_MIs[] =
{
	NULL
};
static MethodInfo* t3805_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20153_MI,
	&m5907_MI,
	&m20154_MI,
	&m20155_MI,
	&m20156_MI,
	&m20157_MI,
	&m20158_MI,
	&m5908_MI,
	&m20152_MI,
	&m20160_MI,
	&m20161_MI,
	&m5905_MI,
	&m5906_MI,
	&m19581_MI,
	&m5907_MI,
	&m19582_MI,
	&m19583_MI,
	&m19584_MI,
	&m19585_MI,
	&m19586_MI,
	&m5908_MI,
	&m19580_MI,
	&m19588_MI,
	&m19589_MI,
};
static TypeInfo* t3805_ITIs[] = 
{
	&t5184_TI,
	&t5185_TI,
	&t5186_TI,
	&t2216_TI,
	&t312_TI,
	&t2217_TI,
};
static Il2CppInterfaceOffsetPair t3805_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5184_TI, 21},
	{ &t5185_TI, 28},
	{ &t5186_TI, 33},
	{ &t2216_TI, 34},
	{ &t312_TI, 41},
	{ &t2217_TI, 46},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3805_0_0_0;
extern Il2CppType t3805_1_0_0;
struct t90;
extern TypeInfo t90_TI;
TypeInfo t3805_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "IPointerUpHandler[]", "UnityEngine.EventSystems", t3805_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t90_TI, t3805_ITIs, t3805_VT, &EmptyCustomAttributesCache, &t3805_TI, &t3805_0_0_0, &t3805_1_0_0, t3805_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3806_TI;



// Metadata Definition UnityEngine.EventSystems.IPointerClickHandler[]
static MethodInfo* t3806_MIs[] =
{
	NULL
};
static MethodInfo* t3806_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20164_MI,
	&m5907_MI,
	&m20165_MI,
	&m20166_MI,
	&m20167_MI,
	&m20168_MI,
	&m20169_MI,
	&m5908_MI,
	&m20163_MI,
	&m20171_MI,
	&m20172_MI,
	&m5905_MI,
	&m5906_MI,
	&m19581_MI,
	&m5907_MI,
	&m19582_MI,
	&m19583_MI,
	&m19584_MI,
	&m19585_MI,
	&m19586_MI,
	&m5908_MI,
	&m19580_MI,
	&m19588_MI,
	&m19589_MI,
};
static TypeInfo* t3806_ITIs[] = 
{
	&t5187_TI,
	&t5188_TI,
	&t5189_TI,
	&t2216_TI,
	&t312_TI,
	&t2217_TI,
};
static Il2CppInterfaceOffsetPair t3806_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5187_TI, 21},
	{ &t5188_TI, 28},
	{ &t5189_TI, 33},
	{ &t2216_TI, 34},
	{ &t312_TI, 41},
	{ &t2217_TI, 46},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3806_0_0_0;
extern Il2CppType t3806_1_0_0;
struct t91;
extern TypeInfo t91_TI;
TypeInfo t3806_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "IPointerClickHandler[]", "UnityEngine.EventSystems", t3806_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t91_TI, t3806_ITIs, t3806_VT, &EmptyCustomAttributesCache, &t3806_TI, &t3806_0_0_0, &t3806_1_0_0, t3806_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3807_TI;



// Metadata Definition UnityEngine.EventSystems.IBeginDragHandler[]
static MethodInfo* t3807_MIs[] =
{
	NULL
};
static MethodInfo* t3807_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20175_MI,
	&m5907_MI,
	&m20176_MI,
	&m20177_MI,
	&m20178_MI,
	&m20179_MI,
	&m20180_MI,
	&m5908_MI,
	&m20174_MI,
	&m20182_MI,
	&m20183_MI,
	&m5905_MI,
	&m5906_MI,
	&m19581_MI,
	&m5907_MI,
	&m19582_MI,
	&m19583_MI,
	&m19584_MI,
	&m19585_MI,
	&m19586_MI,
	&m5908_MI,
	&m19580_MI,
	&m19588_MI,
	&m19589_MI,
};
static TypeInfo* t3807_ITIs[] = 
{
	&t5190_TI,
	&t5191_TI,
	&t5192_TI,
	&t2216_TI,
	&t312_TI,
	&t2217_TI,
};
static Il2CppInterfaceOffsetPair t3807_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5190_TI, 21},
	{ &t5191_TI, 28},
	{ &t5192_TI, 33},
	{ &t2216_TI, 34},
	{ &t312_TI, 41},
	{ &t2217_TI, 46},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3807_0_0_0;
extern Il2CppType t3807_1_0_0;
struct t93;
extern TypeInfo t93_TI;
TypeInfo t3807_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "IBeginDragHandler[]", "UnityEngine.EventSystems", t3807_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t93_TI, t3807_ITIs, t3807_VT, &EmptyCustomAttributesCache, &t3807_TI, &t3807_0_0_0, &t3807_1_0_0, t3807_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3808_TI;



// Metadata Definition UnityEngine.EventSystems.IInitializePotentialDragHandler[]
static MethodInfo* t3808_MIs[] =
{
	NULL
};
static MethodInfo* t3808_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20186_MI,
	&m5907_MI,
	&m20187_MI,
	&m20188_MI,
	&m20189_MI,
	&m20190_MI,
	&m20191_MI,
	&m5908_MI,
	&m20185_MI,
	&m20193_MI,
	&m20194_MI,
	&m5905_MI,
	&m5906_MI,
	&m19581_MI,
	&m5907_MI,
	&m19582_MI,
	&m19583_MI,
	&m19584_MI,
	&m19585_MI,
	&m19586_MI,
	&m5908_MI,
	&m19580_MI,
	&m19588_MI,
	&m19589_MI,
};
static TypeInfo* t3808_ITIs[] = 
{
	&t5193_TI,
	&t5194_TI,
	&t5195_TI,
	&t2216_TI,
	&t312_TI,
	&t2217_TI,
};
static Il2CppInterfaceOffsetPair t3808_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5193_TI, 21},
	{ &t5194_TI, 28},
	{ &t5195_TI, 33},
	{ &t2216_TI, 34},
	{ &t312_TI, 41},
	{ &t2217_TI, 46},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3808_0_0_0;
extern Il2CppType t3808_1_0_0;
struct t92;
extern TypeInfo t92_TI;
TypeInfo t3808_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "IInitializePotentialDragHandler[]", "UnityEngine.EventSystems", t3808_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t92_TI, t3808_ITIs, t3808_VT, &EmptyCustomAttributesCache, &t3808_TI, &t3808_0_0_0, &t3808_1_0_0, t3808_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3809_TI;



// Metadata Definition UnityEngine.EventSystems.IDragHandler[]
static MethodInfo* t3809_MIs[] =
{
	NULL
};
static MethodInfo* t3809_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20197_MI,
	&m5907_MI,
	&m20198_MI,
	&m20199_MI,
	&m20200_MI,
	&m20201_MI,
	&m20202_MI,
	&m5908_MI,
	&m20196_MI,
	&m20204_MI,
	&m20205_MI,
	&m5905_MI,
	&m5906_MI,
	&m19581_MI,
	&m5907_MI,
	&m19582_MI,
	&m19583_MI,
	&m19584_MI,
	&m19585_MI,
	&m19586_MI,
	&m5908_MI,
	&m19580_MI,
	&m19588_MI,
	&m19589_MI,
};
static TypeInfo* t3809_ITIs[] = 
{
	&t5196_TI,
	&t5197_TI,
	&t5198_TI,
	&t2216_TI,
	&t312_TI,
	&t2217_TI,
};
static Il2CppInterfaceOffsetPair t3809_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5196_TI, 21},
	{ &t5197_TI, 28},
	{ &t5198_TI, 33},
	{ &t2216_TI, 34},
	{ &t312_TI, 41},
	{ &t2217_TI, 46},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3809_0_0_0;
extern Il2CppType t3809_1_0_0;
struct t94;
extern TypeInfo t94_TI;
TypeInfo t3809_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "IDragHandler[]", "UnityEngine.EventSystems", t3809_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t94_TI, t3809_ITIs, t3809_VT, &EmptyCustomAttributesCache, &t3809_TI, &t3809_0_0_0, &t3809_1_0_0, t3809_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3810_TI;



// Metadata Definition UnityEngine.EventSystems.IEndDragHandler[]
static MethodInfo* t3810_MIs[] =
{
	NULL
};
static MethodInfo* t3810_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20208_MI,
	&m5907_MI,
	&m20209_MI,
	&m20210_MI,
	&m20211_MI,
	&m20212_MI,
	&m20213_MI,
	&m5908_MI,
	&m20207_MI,
	&m20215_MI,
	&m20216_MI,
	&m5905_MI,
	&m5906_MI,
	&m19581_MI,
	&m5907_MI,
	&m19582_MI,
	&m19583_MI,
	&m19584_MI,
	&m19585_MI,
	&m19586_MI,
	&m5908_MI,
	&m19580_MI,
	&m19588_MI,
	&m19589_MI,
};
static TypeInfo* t3810_ITIs[] = 
{
	&t5199_TI,
	&t5200_TI,
	&t5201_TI,
	&t2216_TI,
	&t312_TI,
	&t2217_TI,
};
static Il2CppInterfaceOffsetPair t3810_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5199_TI, 21},
	{ &t5200_TI, 28},
	{ &t5201_TI, 33},
	{ &t2216_TI, 34},
	{ &t312_TI, 41},
	{ &t2217_TI, 46},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3810_0_0_0;
extern Il2CppType t3810_1_0_0;
struct t95;
extern TypeInfo t95_TI;
TypeInfo t3810_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "IEndDragHandler[]", "UnityEngine.EventSystems", t3810_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t95_TI, t3810_ITIs, t3810_VT, &EmptyCustomAttributesCache, &t3810_TI, &t3810_0_0_0, &t3810_1_0_0, t3810_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3811_TI;



// Metadata Definition UnityEngine.EventSystems.IDropHandler[]
static MethodInfo* t3811_MIs[] =
{
	NULL
};
static MethodInfo* t3811_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20219_MI,
	&m5907_MI,
	&m20220_MI,
	&m20221_MI,
	&m20222_MI,
	&m20223_MI,
	&m20224_MI,
	&m5908_MI,
	&m20218_MI,
	&m20226_MI,
	&m20227_MI,
	&m5905_MI,
	&m5906_MI,
	&m19581_MI,
	&m5907_MI,
	&m19582_MI,
	&m19583_MI,
	&m19584_MI,
	&m19585_MI,
	&m19586_MI,
	&m5908_MI,
	&m19580_MI,
	&m19588_MI,
	&m19589_MI,
};
static TypeInfo* t3811_ITIs[] = 
{
	&t5202_TI,
	&t5203_TI,
	&t5204_TI,
	&t2216_TI,
	&t312_TI,
	&t2217_TI,
};
static Il2CppInterfaceOffsetPair t3811_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5202_TI, 21},
	{ &t5203_TI, 28},
	{ &t5204_TI, 33},
	{ &t2216_TI, 34},
	{ &t312_TI, 41},
	{ &t2217_TI, 46},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3811_0_0_0;
extern Il2CppType t3811_1_0_0;
struct t96;
extern TypeInfo t96_TI;
TypeInfo t3811_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "IDropHandler[]", "UnityEngine.EventSystems", t3811_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t96_TI, t3811_ITIs, t3811_VT, &EmptyCustomAttributesCache, &t3811_TI, &t3811_0_0_0, &t3811_1_0_0, t3811_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3812_TI;



// Metadata Definition UnityEngine.EventSystems.IScrollHandler[]
static MethodInfo* t3812_MIs[] =
{
	NULL
};
static MethodInfo* t3812_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20230_MI,
	&m5907_MI,
	&m20231_MI,
	&m20232_MI,
	&m20233_MI,
	&m20234_MI,
	&m20235_MI,
	&m5908_MI,
	&m20229_MI,
	&m20237_MI,
	&m20238_MI,
	&m5905_MI,
	&m5906_MI,
	&m19581_MI,
	&m5907_MI,
	&m19582_MI,
	&m19583_MI,
	&m19584_MI,
	&m19585_MI,
	&m19586_MI,
	&m5908_MI,
	&m19580_MI,
	&m19588_MI,
	&m19589_MI,
};
static TypeInfo* t3812_ITIs[] = 
{
	&t5205_TI,
	&t5206_TI,
	&t5207_TI,
	&t2216_TI,
	&t312_TI,
	&t2217_TI,
};
static Il2CppInterfaceOffsetPair t3812_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5205_TI, 21},
	{ &t5206_TI, 28},
	{ &t5207_TI, 33},
	{ &t2216_TI, 34},
	{ &t312_TI, 41},
	{ &t2217_TI, 46},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3812_0_0_0;
extern Il2CppType t3812_1_0_0;
struct t97;
extern TypeInfo t97_TI;
TypeInfo t3812_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "IScrollHandler[]", "UnityEngine.EventSystems", t3812_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t97_TI, t3812_ITIs, t3812_VT, &EmptyCustomAttributesCache, &t3812_TI, &t3812_0_0_0, &t3812_1_0_0, t3812_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3813_TI;



// Metadata Definition UnityEngine.EventSystems.IUpdateSelectedHandler[]
static MethodInfo* t3813_MIs[] =
{
	NULL
};
static MethodInfo* t3813_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20241_MI,
	&m5907_MI,
	&m20242_MI,
	&m20243_MI,
	&m20244_MI,
	&m20245_MI,
	&m20246_MI,
	&m5908_MI,
	&m20240_MI,
	&m20248_MI,
	&m20249_MI,
	&m5905_MI,
	&m5906_MI,
	&m19581_MI,
	&m5907_MI,
	&m19582_MI,
	&m19583_MI,
	&m19584_MI,
	&m19585_MI,
	&m19586_MI,
	&m5908_MI,
	&m19580_MI,
	&m19588_MI,
	&m19589_MI,
};
static TypeInfo* t3813_ITIs[] = 
{
	&t5208_TI,
	&t5209_TI,
	&t5210_TI,
	&t2216_TI,
	&t312_TI,
	&t2217_TI,
};
static Il2CppInterfaceOffsetPair t3813_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5208_TI, 21},
	{ &t5209_TI, 28},
	{ &t5210_TI, 33},
	{ &t2216_TI, 34},
	{ &t312_TI, 41},
	{ &t2217_TI, 46},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3813_0_0_0;
extern Il2CppType t3813_1_0_0;
struct t98;
extern TypeInfo t98_TI;
TypeInfo t3813_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "IUpdateSelectedHandler[]", "UnityEngine.EventSystems", t3813_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t98_TI, t3813_ITIs, t3813_VT, &EmptyCustomAttributesCache, &t3813_TI, &t3813_0_0_0, &t3813_1_0_0, t3813_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3814_TI;



// Metadata Definition UnityEngine.EventSystems.ISelectHandler[]
static MethodInfo* t3814_MIs[] =
{
	NULL
};
static MethodInfo* t3814_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20252_MI,
	&m5907_MI,
	&m20253_MI,
	&m20254_MI,
	&m20255_MI,
	&m20256_MI,
	&m20257_MI,
	&m5908_MI,
	&m20251_MI,
	&m20259_MI,
	&m20260_MI,
	&m5905_MI,
	&m5906_MI,
	&m19581_MI,
	&m5907_MI,
	&m19582_MI,
	&m19583_MI,
	&m19584_MI,
	&m19585_MI,
	&m19586_MI,
	&m5908_MI,
	&m19580_MI,
	&m19588_MI,
	&m19589_MI,
};
static TypeInfo* t3814_ITIs[] = 
{
	&t5211_TI,
	&t5212_TI,
	&t5213_TI,
	&t2216_TI,
	&t312_TI,
	&t2217_TI,
};
static Il2CppInterfaceOffsetPair t3814_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5211_TI, 21},
	{ &t5212_TI, 28},
	{ &t5213_TI, 33},
	{ &t2216_TI, 34},
	{ &t312_TI, 41},
	{ &t2217_TI, 46},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3814_0_0_0;
extern Il2CppType t3814_1_0_0;
struct t99;
extern TypeInfo t99_TI;
TypeInfo t3814_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "ISelectHandler[]", "UnityEngine.EventSystems", t3814_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t99_TI, t3814_ITIs, t3814_VT, &EmptyCustomAttributesCache, &t3814_TI, &t3814_0_0_0, &t3814_1_0_0, t3814_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3815_TI;



// Metadata Definition UnityEngine.EventSystems.IDeselectHandler[]
static MethodInfo* t3815_MIs[] =
{
	NULL
};
static MethodInfo* t3815_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20263_MI,
	&m5907_MI,
	&m20264_MI,
	&m20265_MI,
	&m20266_MI,
	&m20267_MI,
	&m20268_MI,
	&m5908_MI,
	&m20262_MI,
	&m20270_MI,
	&m20271_MI,
	&m5905_MI,
	&m5906_MI,
	&m19581_MI,
	&m5907_MI,
	&m19582_MI,
	&m19583_MI,
	&m19584_MI,
	&m19585_MI,
	&m19586_MI,
	&m5908_MI,
	&m19580_MI,
	&m19588_MI,
	&m19589_MI,
};
static TypeInfo* t3815_ITIs[] = 
{
	&t5214_TI,
	&t5215_TI,
	&t5216_TI,
	&t2216_TI,
	&t312_TI,
	&t2217_TI,
};
static Il2CppInterfaceOffsetPair t3815_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5214_TI, 21},
	{ &t5215_TI, 28},
	{ &t5216_TI, 33},
	{ &t2216_TI, 34},
	{ &t312_TI, 41},
	{ &t2217_TI, 46},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3815_0_0_0;
extern Il2CppType t3815_1_0_0;
struct t100;
extern TypeInfo t100_TI;
TypeInfo t3815_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "IDeselectHandler[]", "UnityEngine.EventSystems", t3815_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t100_TI, t3815_ITIs, t3815_VT, &EmptyCustomAttributesCache, &t3815_TI, &t3815_0_0_0, &t3815_1_0_0, t3815_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3816_TI;



// Metadata Definition UnityEngine.EventSystems.IMoveHandler[]
static MethodInfo* t3816_MIs[] =
{
	NULL
};
static MethodInfo* t3816_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20274_MI,
	&m5907_MI,
	&m20275_MI,
	&m20276_MI,
	&m20277_MI,
	&m20278_MI,
	&m20279_MI,
	&m5908_MI,
	&m20273_MI,
	&m20281_MI,
	&m20282_MI,
	&m5905_MI,
	&m5906_MI,
	&m19581_MI,
	&m5907_MI,
	&m19582_MI,
	&m19583_MI,
	&m19584_MI,
	&m19585_MI,
	&m19586_MI,
	&m5908_MI,
	&m19580_MI,
	&m19588_MI,
	&m19589_MI,
};
static TypeInfo* t3816_ITIs[] = 
{
	&t5217_TI,
	&t5218_TI,
	&t5219_TI,
	&t2216_TI,
	&t312_TI,
	&t2217_TI,
};
static Il2CppInterfaceOffsetPair t3816_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5217_TI, 21},
	{ &t5218_TI, 28},
	{ &t5219_TI, 33},
	{ &t2216_TI, 34},
	{ &t312_TI, 41},
	{ &t2217_TI, 46},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3816_0_0_0;
extern Il2CppType t3816_1_0_0;
struct t101;
extern TypeInfo t101_TI;
TypeInfo t3816_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "IMoveHandler[]", "UnityEngine.EventSystems", t3816_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t101_TI, t3816_ITIs, t3816_VT, &EmptyCustomAttributesCache, &t3816_TI, &t3816_0_0_0, &t3816_1_0_0, t3816_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3817_TI;



// Metadata Definition UnityEngine.EventSystems.ISubmitHandler[]
static MethodInfo* t3817_MIs[] =
{
	NULL
};
static MethodInfo* t3817_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20285_MI,
	&m5907_MI,
	&m20286_MI,
	&m20287_MI,
	&m20288_MI,
	&m20289_MI,
	&m20290_MI,
	&m5908_MI,
	&m20284_MI,
	&m20292_MI,
	&m20293_MI,
	&m5905_MI,
	&m5906_MI,
	&m19581_MI,
	&m5907_MI,
	&m19582_MI,
	&m19583_MI,
	&m19584_MI,
	&m19585_MI,
	&m19586_MI,
	&m5908_MI,
	&m19580_MI,
	&m19588_MI,
	&m19589_MI,
};
static TypeInfo* t3817_ITIs[] = 
{
	&t5220_TI,
	&t5221_TI,
	&t5222_TI,
	&t2216_TI,
	&t312_TI,
	&t2217_TI,
};
static Il2CppInterfaceOffsetPair t3817_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5220_TI, 21},
	{ &t5221_TI, 28},
	{ &t5222_TI, 33},
	{ &t2216_TI, 34},
	{ &t312_TI, 41},
	{ &t2217_TI, 46},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3817_0_0_0;
extern Il2CppType t3817_1_0_0;
struct t102;
extern TypeInfo t102_TI;
TypeInfo t3817_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "ISubmitHandler[]", "UnityEngine.EventSystems", t3817_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t102_TI, t3817_ITIs, t3817_VT, &EmptyCustomAttributesCache, &t3817_TI, &t3817_0_0_0, &t3817_1_0_0, t3817_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3818_TI;



// Metadata Definition UnityEngine.EventSystems.ICancelHandler[]
static MethodInfo* t3818_MIs[] =
{
	NULL
};
static MethodInfo* t3818_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20296_MI,
	&m5907_MI,
	&m20297_MI,
	&m20298_MI,
	&m20299_MI,
	&m20300_MI,
	&m20301_MI,
	&m5908_MI,
	&m20295_MI,
	&m20303_MI,
	&m20304_MI,
	&m5905_MI,
	&m5906_MI,
	&m19581_MI,
	&m5907_MI,
	&m19582_MI,
	&m19583_MI,
	&m19584_MI,
	&m19585_MI,
	&m19586_MI,
	&m5908_MI,
	&m19580_MI,
	&m19588_MI,
	&m19589_MI,
};
static TypeInfo* t3818_ITIs[] = 
{
	&t5223_TI,
	&t5224_TI,
	&t5225_TI,
	&t2216_TI,
	&t312_TI,
	&t2217_TI,
};
static Il2CppInterfaceOffsetPair t3818_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5223_TI, 21},
	{ &t5224_TI, 28},
	{ &t5225_TI, 33},
	{ &t2216_TI, 34},
	{ &t312_TI, 41},
	{ &t2217_TI, 46},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3818_0_0_0;
extern Il2CppType t3818_1_0_0;
struct t103;
extern TypeInfo t103_TI;
TypeInfo t3818_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "ICancelHandler[]", "UnityEngine.EventSystems", t3818_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t103_TI, t3818_ITIs, t3818_VT, &EmptyCustomAttributesCache, &t3818_TI, &t3818_0_0_0, &t3818_1_0_0, t3818_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2299_TI;



// Metadata Definition UnityEngine.EventSystems.EventTrigger/Entry[]
static MethodInfo* t2299_MIs[] =
{
	NULL
};
extern MethodInfo m20308_MI;
extern MethodInfo m20309_MI;
extern MethodInfo m20310_MI;
extern MethodInfo m20311_MI;
extern MethodInfo m20312_MI;
extern MethodInfo m20313_MI;
extern MethodInfo m20307_MI;
extern MethodInfo m20315_MI;
extern MethodInfo m20316_MI;
static MethodInfo* t2299_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20308_MI,
	&m5907_MI,
	&m20309_MI,
	&m20310_MI,
	&m20311_MI,
	&m20312_MI,
	&m20313_MI,
	&m5908_MI,
	&m20307_MI,
	&m20315_MI,
	&m20316_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t2301_TI;
extern TypeInfo t2308_TI;
extern TypeInfo t2302_TI;
static TypeInfo* t2299_ITIs[] = 
{
	&t2301_TI,
	&t2308_TI,
	&t2302_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2299_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2301_TI, 21},
	{ &t2308_TI, 28},
	{ &t2302_TI, 33},
	{ &t2182_TI, 34},
	{ &t2186_TI, 41},
	{ &t2183_TI, 46},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t2299_0_0_0;
extern Il2CppType t2299_1_0_0;
struct t60;
extern TypeInfo t60_TI;
TypeInfo t2299_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Entry[]", "", t2299_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t60_TI, t2299_ITIs, t2299_VT, &EmptyCustomAttributesCache, &t2299_TI, &t2299_0_0_0, &t2299_1_0_0, t2299_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t60 *), -1, 0, 0, -1, 1056770, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3819_TI;



// Metadata Definition UnityEngine.EventSystems.EventTriggerType[]
static MethodInfo* t3819_MIs[] =
{
	NULL
};
extern MethodInfo m20335_MI;
extern MethodInfo m20336_MI;
extern MethodInfo m20337_MI;
extern MethodInfo m20338_MI;
extern MethodInfo m20339_MI;
extern MethodInfo m20340_MI;
extern MethodInfo m20334_MI;
extern MethodInfo m20342_MI;
extern MethodInfo m20343_MI;
static MethodInfo* t3819_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20335_MI,
	&m5907_MI,
	&m20336_MI,
	&m20337_MI,
	&m20338_MI,
	&m20339_MI,
	&m20340_MI,
	&m5908_MI,
	&m20334_MI,
	&m20342_MI,
	&m20343_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5226_TI;
extern TypeInfo t5227_TI;
extern TypeInfo t5228_TI;
static TypeInfo* t3819_ITIs[] = 
{
	&t5226_TI,
	&t5227_TI,
	&t5228_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3819_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5226_TI, 21},
	{ &t5227_TI, 28},
	{ &t5228_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3819_0_0_0;
extern Il2CppType t3819_1_0_0;
#include "t61.h"
extern TypeInfo t61_TI;
TypeInfo t3819_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "EventTriggerType[]", "UnityEngine.EventSystems", t3819_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t61_TI, t3819_ITIs, t3819_VT, &EmptyCustomAttributesCache, &t44_TI, &t3819_0_0_0, &t3819_1_0_0, t3819_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2317_TI;



// Metadata Definition UnityEngine.Transform[]
static MethodInfo* t2317_MIs[] =
{
	NULL
};
extern MethodInfo m20346_MI;
extern MethodInfo m20347_MI;
extern MethodInfo m20348_MI;
extern MethodInfo m20349_MI;
extern MethodInfo m20350_MI;
extern MethodInfo m20351_MI;
extern MethodInfo m20345_MI;
extern MethodInfo m20353_MI;
extern MethodInfo m20354_MI;
static MethodInfo* t2317_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20346_MI,
	&m5907_MI,
	&m20347_MI,
	&m20348_MI,
	&m20349_MI,
	&m20350_MI,
	&m20351_MI,
	&m5908_MI,
	&m20345_MI,
	&m20353_MI,
	&m20354_MI,
	&m5905_MI,
	&m5906_MI,
	&m20022_MI,
	&m5907_MI,
	&m20023_MI,
	&m20024_MI,
	&m20025_MI,
	&m20026_MI,
	&m20027_MI,
	&m5908_MI,
	&m20021_MI,
	&m20029_MI,
	&m20030_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t304_TI;
extern TypeInfo t104_TI;
extern TypeInfo t2319_TI;
static TypeInfo* t2317_ITIs[] = 
{
	&t304_TI,
	&t104_TI,
	&t2319_TI,
	&t5169_TI,
	&t5170_TI,
	&t5171_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2317_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t304_TI, 21},
	{ &t104_TI, 28},
	{ &t2319_TI, 33},
	{ &t5169_TI, 34},
	{ &t5170_TI, 41},
	{ &t5171_TI, 46},
	{ &t2240_TI, 47},
	{ &t2245_TI, 54},
	{ &t2241_TI, 59},
	{ &t5065_TI, 60},
	{ &t5066_TI, 67},
	{ &t5067_TI, 72},
	{ &t2182_TI, 73},
	{ &t2186_TI, 80},
	{ &t2183_TI, 85},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2317_0_0_0;
extern Il2CppType t2317_1_0_0;
struct t25;
extern TypeInfo t25_TI;
extern CustomAttributesCache t25__CustomAttributeCache_m2562;
extern CustomAttributesCache t25__CustomAttributeCache_m2563;
extern CustomAttributesCache t25__CustomAttributeCache_m2564;
extern CustomAttributesCache t25__CustomAttributeCache_m2565;
extern CustomAttributesCache t25__CustomAttributeCache_m2566;
extern CustomAttributesCache t25__CustomAttributeCache_m2567;
extern CustomAttributesCache t25__CustomAttributeCache_m2568;
extern CustomAttributesCache t25__CustomAttributeCache_m2569;
extern CustomAttributesCache t25__CustomAttributeCache_m2570;
extern CustomAttributesCache t25__CustomAttributeCache_m2571;
extern CustomAttributesCache t25__CustomAttributeCache_m2572;
extern CustomAttributesCache t25__CustomAttributeCache_m2573;
extern CustomAttributesCache t25__CustomAttributeCache_m2574;
extern CustomAttributesCache t25__CustomAttributeCache_m2575;
extern CustomAttributesCache t25__CustomAttributeCache_m2576;
extern CustomAttributesCache t25__CustomAttributeCache_m1993;
extern CustomAttributesCache t25__CustomAttributeCache_m1780;
extern CustomAttributesCache t25__CustomAttributeCache_m1991;
TypeInfo t2317_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Transform[]", "UnityEngine", t2317_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t25_TI, t2317_ITIs, t2317_VT, &EmptyCustomAttributesCache, &t2317_TI, &t2317_0_0_0, &t2317_1_0_0, t2317_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t25 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 86, 15, 19};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3820_TI;



// Metadata Definition UnityEngine.EventSystems.MoveDirection[]
static MethodInfo* t3820_MIs[] =
{
	NULL
};
extern MethodInfo m20372_MI;
extern MethodInfo m20373_MI;
extern MethodInfo m20374_MI;
extern MethodInfo m20375_MI;
extern MethodInfo m20376_MI;
extern MethodInfo m20377_MI;
extern MethodInfo m20371_MI;
extern MethodInfo m20379_MI;
extern MethodInfo m20380_MI;
static MethodInfo* t3820_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20372_MI,
	&m5907_MI,
	&m20373_MI,
	&m20374_MI,
	&m20375_MI,
	&m20376_MI,
	&m20377_MI,
	&m5908_MI,
	&m20371_MI,
	&m20379_MI,
	&m20380_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5229_TI;
extern TypeInfo t5230_TI;
extern TypeInfo t5231_TI;
static TypeInfo* t3820_ITIs[] = 
{
	&t5229_TI,
	&t5230_TI,
	&t5231_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3820_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5229_TI, 21},
	{ &t5230_TI, 28},
	{ &t5231_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3820_0_0_0;
extern Il2CppType t3820_1_0_0;
#include "t106.h"
extern TypeInfo t106_TI;
TypeInfo t3820_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "MoveDirection[]", "UnityEngine.EventSystems", t3820_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t106_TI, t3820_ITIs, t3820_VT, &EmptyCustomAttributesCache, &t44_TI, &t3820_0_0_0, &t3820_1_0_0, t3820_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3821_TI;



// Metadata Definition UnityEngine.EventSystems.PointerEventData/InputButton[]
static MethodInfo* t3821_MIs[] =
{
	NULL
};
extern MethodInfo m20384_MI;
extern MethodInfo m20385_MI;
extern MethodInfo m20386_MI;
extern MethodInfo m20387_MI;
extern MethodInfo m20388_MI;
extern MethodInfo m20389_MI;
extern MethodInfo m20383_MI;
extern MethodInfo m20391_MI;
extern MethodInfo m20392_MI;
static MethodInfo* t3821_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20384_MI,
	&m5907_MI,
	&m20385_MI,
	&m20386_MI,
	&m20387_MI,
	&m20388_MI,
	&m20389_MI,
	&m5908_MI,
	&m20383_MI,
	&m20391_MI,
	&m20392_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5232_TI;
extern TypeInfo t5233_TI;
extern TypeInfo t5234_TI;
static TypeInfo* t3821_ITIs[] = 
{
	&t5232_TI,
	&t5233_TI,
	&t5234_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3821_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5232_TI, 21},
	{ &t5233_TI, 28},
	{ &t5234_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3821_0_0_0;
extern Il2CppType t3821_1_0_0;
#include "t111.h"
extern TypeInfo t111_TI;
TypeInfo t3821_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "InputButton[]", "", t3821_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t111_TI, t3821_ITIs, t3821_VT, &EmptyCustomAttributesCache, &t44_TI, &t3821_0_0_0, &t3821_1_0_0, t3821_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 258, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3822_TI;



// Metadata Definition UnityEngine.EventSystems.PointerEventData/FramePressState[]
static MethodInfo* t3822_MIs[] =
{
	NULL
};
extern MethodInfo m20395_MI;
extern MethodInfo m20396_MI;
extern MethodInfo m20397_MI;
extern MethodInfo m20398_MI;
extern MethodInfo m20399_MI;
extern MethodInfo m20400_MI;
extern MethodInfo m20394_MI;
extern MethodInfo m20402_MI;
extern MethodInfo m20403_MI;
static MethodInfo* t3822_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20395_MI,
	&m5907_MI,
	&m20396_MI,
	&m20397_MI,
	&m20398_MI,
	&m20399_MI,
	&m20400_MI,
	&m5908_MI,
	&m20394_MI,
	&m20402_MI,
	&m20403_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5235_TI;
extern TypeInfo t5236_TI;
extern TypeInfo t5237_TI;
static TypeInfo* t3822_ITIs[] = 
{
	&t5235_TI,
	&t5236_TI,
	&t5237_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3822_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5235_TI, 21},
	{ &t5236_TI, 28},
	{ &t5237_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3822_0_0_0;
extern Il2CppType t3822_1_0_0;
#include "t112.h"
extern TypeInfo t112_TI;
TypeInfo t3822_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "FramePressState[]", "", t3822_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t112_TI, t3822_ITIs, t3822_VT, &EmptyCustomAttributesCache, &t44_TI, &t3822_0_0_0, &t3822_1_0_0, t3822_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 258, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3823_TI;



// Metadata Definition UnityEngine.EventSystems.PointerInputModule[]
static MethodInfo* t3823_MIs[] =
{
	NULL
};
extern MethodInfo m20411_MI;
extern MethodInfo m20412_MI;
extern MethodInfo m20413_MI;
extern MethodInfo m20414_MI;
extern MethodInfo m20415_MI;
extern MethodInfo m20416_MI;
extern MethodInfo m20410_MI;
extern MethodInfo m20418_MI;
extern MethodInfo m20419_MI;
static MethodInfo* t3823_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20411_MI,
	&m5907_MI,
	&m20412_MI,
	&m20413_MI,
	&m20414_MI,
	&m20415_MI,
	&m20416_MI,
	&m5908_MI,
	&m20410_MI,
	&m20418_MI,
	&m20419_MI,
	&m5905_MI,
	&m5906_MI,
	&m19968_MI,
	&m5907_MI,
	&m19969_MI,
	&m19970_MI,
	&m19971_MI,
	&m19972_MI,
	&m19973_MI,
	&m5908_MI,
	&m19967_MI,
	&m19975_MI,
	&m19976_MI,
	&m5905_MI,
	&m5906_MI,
	&m19777_MI,
	&m5907_MI,
	&m19778_MI,
	&m19779_MI,
	&m19780_MI,
	&m19781_MI,
	&m19782_MI,
	&m5908_MI,
	&m19776_MI,
	&m19784_MI,
	&m19785_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5238_TI;
extern TypeInfo t5239_TI;
extern TypeInfo t5240_TI;
static TypeInfo* t3823_ITIs[] = 
{
	&t5238_TI,
	&t5239_TI,
	&t5240_TI,
	&t2174_TI,
	&t2207_TI,
	&t2175_TI,
	&t5122_TI,
	&t5123_TI,
	&t5124_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3823_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5238_TI, 21},
	{ &t5239_TI, 28},
	{ &t5240_TI, 33},
	{ &t2174_TI, 34},
	{ &t2207_TI, 41},
	{ &t2175_TI, 46},
	{ &t5122_TI, 47},
	{ &t5123_TI, 54},
	{ &t5124_TI, 59},
	{ &t5059_TI, 60},
	{ &t5060_TI, 67},
	{ &t5061_TI, 72},
	{ &t5062_TI, 73},
	{ &t5063_TI, 80},
	{ &t5064_TI, 85},
	{ &t2240_TI, 86},
	{ &t2245_TI, 93},
	{ &t2241_TI, 98},
	{ &t5065_TI, 99},
	{ &t5066_TI, 106},
	{ &t5067_TI, 111},
	{ &t2182_TI, 112},
	{ &t2186_TI, 119},
	{ &t2183_TI, 124},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3823_0_0_0;
extern Il2CppType t3823_1_0_0;
struct t117;
extern TypeInfo t117_TI;
TypeInfo t3823_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "PointerInputModule[]", "UnityEngine.EventSystems", t3823_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t117_TI, t3823_ITIs, t3823_VT, &EmptyCustomAttributesCache, &t3823_TI, &t3823_0_0_0, &t3823_1_0_0, t3823_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t117 *), -1, 0, 0, -1, 1048705, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 125, 24, 28};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t446_TI;



// Metadata Definition System.String[]
static MethodInfo* t446_MIs[] =
{
	NULL
};
extern MethodInfo m20423_MI;
extern MethodInfo m20424_MI;
extern MethodInfo m20425_MI;
extern MethodInfo m20426_MI;
extern MethodInfo m20427_MI;
extern MethodInfo m20428_MI;
extern MethodInfo m20422_MI;
extern MethodInfo m20430_MI;
extern MethodInfo m20431_MI;
extern MethodInfo m20434_MI;
extern MethodInfo m20435_MI;
extern MethodInfo m20436_MI;
extern MethodInfo m20437_MI;
extern MethodInfo m20438_MI;
extern MethodInfo m20439_MI;
extern MethodInfo m20433_MI;
extern MethodInfo m20441_MI;
extern MethodInfo m20442_MI;
extern MethodInfo m20445_MI;
extern MethodInfo m20446_MI;
extern MethodInfo m20447_MI;
extern MethodInfo m20448_MI;
extern MethodInfo m20449_MI;
extern MethodInfo m20450_MI;
extern MethodInfo m20444_MI;
extern MethodInfo m20452_MI;
extern MethodInfo m20453_MI;
extern MethodInfo m20456_MI;
extern MethodInfo m20457_MI;
extern MethodInfo m20458_MI;
extern MethodInfo m20459_MI;
extern MethodInfo m20460_MI;
extern MethodInfo m20461_MI;
extern MethodInfo m20455_MI;
extern MethodInfo m20463_MI;
extern MethodInfo m20464_MI;
static MethodInfo* t446_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20423_MI,
	&m5907_MI,
	&m20424_MI,
	&m20425_MI,
	&m20426_MI,
	&m20427_MI,
	&m20428_MI,
	&m5908_MI,
	&m20422_MI,
	&m20430_MI,
	&m20431_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m20022_MI,
	&m5907_MI,
	&m20023_MI,
	&m20024_MI,
	&m20025_MI,
	&m20026_MI,
	&m20027_MI,
	&m5908_MI,
	&m20021_MI,
	&m20029_MI,
	&m20030_MI,
	&m5905_MI,
	&m5906_MI,
	&m20434_MI,
	&m5907_MI,
	&m20435_MI,
	&m20436_MI,
	&m20437_MI,
	&m20438_MI,
	&m20439_MI,
	&m5908_MI,
	&m20433_MI,
	&m20441_MI,
	&m20442_MI,
	&m5905_MI,
	&m5906_MI,
	&m20445_MI,
	&m5907_MI,
	&m20446_MI,
	&m20447_MI,
	&m20448_MI,
	&m20449_MI,
	&m20450_MI,
	&m5908_MI,
	&m20444_MI,
	&m20452_MI,
	&m20453_MI,
	&m5905_MI,
	&m5906_MI,
	&m20456_MI,
	&m5907_MI,
	&m20457_MI,
	&m20458_MI,
	&m20459_MI,
	&m20460_MI,
	&m20461_MI,
	&m5908_MI,
	&m20455_MI,
	&m20463_MI,
	&m20464_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t3277_TI;
extern TypeInfo t3283_TI;
extern TypeInfo t3278_TI;
extern TypeInfo t5241_TI;
extern TypeInfo t5242_TI;
extern TypeInfo t5243_TI;
extern TypeInfo t5244_TI;
extern TypeInfo t5245_TI;
extern TypeInfo t5246_TI;
extern TypeInfo t5247_TI;
extern TypeInfo t5248_TI;
extern TypeInfo t5249_TI;
static TypeInfo* t446_ITIs[] = 
{
	&t3277_TI,
	&t3283_TI,
	&t3278_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5169_TI,
	&t5170_TI,
	&t5171_TI,
	&t5241_TI,
	&t5242_TI,
	&t5243_TI,
	&t5244_TI,
	&t5245_TI,
	&t5246_TI,
	&t5247_TI,
	&t5248_TI,
	&t5249_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t446_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t3277_TI, 21},
	{ &t3283_TI, 28},
	{ &t3278_TI, 33},
	{ &t5110_TI, 34},
	{ &t5111_TI, 41},
	{ &t5112_TI, 46},
	{ &t5113_TI, 47},
	{ &t5114_TI, 54},
	{ &t5115_TI, 59},
	{ &t5169_TI, 60},
	{ &t5170_TI, 67},
	{ &t5171_TI, 72},
	{ &t5241_TI, 73},
	{ &t5242_TI, 80},
	{ &t5243_TI, 85},
	{ &t5244_TI, 86},
	{ &t5245_TI, 93},
	{ &t5246_TI, 98},
	{ &t5247_TI, 99},
	{ &t5248_TI, 106},
	{ &t5249_TI, 111},
	{ &t2182_TI, 112},
	{ &t2186_TI, 119},
	{ &t2183_TI, 124},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t446_0_0_0;
extern Il2CppType t446_1_0_0;
struct t7;
extern TypeInfo t7_TI;
extern CustomAttributesCache t7__CustomAttributeCache;
extern CustomAttributesCache t7__CustomAttributeCache_m5554;
extern CustomAttributesCache t7__CustomAttributeCache_m5575;
extern CustomAttributesCache t7__CustomAttributeCache_m4180;
extern CustomAttributesCache t7__CustomAttributeCache_m5579;
extern CustomAttributesCache t7__CustomAttributeCache_m5580;
extern CustomAttributesCache t7__CustomAttributeCache_m4070;
extern CustomAttributesCache t7__CustomAttributeCache_m2843;
TypeInfo t446_TI = 
{
	&g_mscorlib_dll_Image, NULL, "String[]", "System", t446_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t7_TI, t446_ITIs, t446_VT, &EmptyCustomAttributesCache, &t446_TI, &t446_0_0_0, &t446_1_0_0, t446_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t7*), -1, sizeof(t446_SFs), 0, -1, 1057025, 1, false, false, false, false, false, true, false, false, false, true, false, false, 0, 0, 0, 0, 0, 125, 24, 28};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3538_TI;



// Metadata Definition System.ICloneable[]
static MethodInfo* t3538_MIs[] =
{
	NULL
};
static MethodInfo* t3538_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20434_MI,
	&m5907_MI,
	&m20435_MI,
	&m20436_MI,
	&m20437_MI,
	&m20438_MI,
	&m20439_MI,
	&m5908_MI,
	&m20433_MI,
	&m20441_MI,
	&m20442_MI,
};
static TypeInfo* t3538_ITIs[] = 
{
	&t5241_TI,
	&t5242_TI,
	&t5243_TI,
};
static Il2CppInterfaceOffsetPair t3538_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5241_TI, 21},
	{ &t5242_TI, 28},
	{ &t5243_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3538_0_0_0;
extern Il2CppType t3538_1_0_0;
struct t373;
extern CustomAttributesCache t373__CustomAttributeCache;
TypeInfo t3538_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICloneable[]", "System", t3538_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t373_TI, t3538_ITIs, t3538_VT, &EmptyCustomAttributesCache, &t3538_TI, &t3538_0_0_0, &t3538_1_0_0, t3538_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3539_TI;



// Metadata Definition System.IComparable`1<System.String>[]
static MethodInfo* t3539_MIs[] =
{
	NULL
};
static MethodInfo* t3539_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20445_MI,
	&m5907_MI,
	&m20446_MI,
	&m20447_MI,
	&m20448_MI,
	&m20449_MI,
	&m20450_MI,
	&m5908_MI,
	&m20444_MI,
	&m20452_MI,
	&m20453_MI,
};
static TypeInfo* t3539_ITIs[] = 
{
	&t5244_TI,
	&t5245_TI,
	&t5246_TI,
};
static Il2CppInterfaceOffsetPair t3539_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5244_TI, 21},
	{ &t5245_TI, 28},
	{ &t5246_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3539_0_0_0;
extern Il2CppType t3539_1_0_0;
struct t1745;
extern TypeInfo t1745_TI;
TypeInfo t3539_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1[]", "System", t3539_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1745_TI, t3539_ITIs, t3539_VT, &EmptyCustomAttributesCache, &t3539_TI, &t3539_0_0_0, &t3539_1_0_0, t3539_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29*), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3540_TI;



// Metadata Definition System.IEquatable`1<System.String>[]
static MethodInfo* t3540_MIs[] =
{
	NULL
};
static MethodInfo* t3540_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20456_MI,
	&m5907_MI,
	&m20457_MI,
	&m20458_MI,
	&m20459_MI,
	&m20460_MI,
	&m20461_MI,
	&m5908_MI,
	&m20455_MI,
	&m20463_MI,
	&m20464_MI,
};
static TypeInfo* t3540_ITIs[] = 
{
	&t5247_TI,
	&t5248_TI,
	&t5249_TI,
};
static Il2CppInterfaceOffsetPair t3540_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5247_TI, 21},
	{ &t5248_TI, 28},
	{ &t5249_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3540_0_0_0;
extern Il2CppType t3540_1_0_0;
struct t1746;
extern TypeInfo t1746_TI;
TypeInfo t3540_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1[]", "System", t3540_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1746_TI, t3540_ITIs, t3540_VT, &EmptyCustomAttributesCache, &t3540_TI, &t3540_0_0_0, &t3540_1_0_0, t3540_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29*), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2346_TI;



// Metadata Definition System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>[]
static MethodInfo* t2346_MIs[] =
{
	NULL
};
extern MethodInfo m20467_MI;
extern MethodInfo m20468_MI;
extern MethodInfo m20469_MI;
extern MethodInfo m20470_MI;
extern MethodInfo m20471_MI;
extern MethodInfo m20472_MI;
extern MethodInfo m20466_MI;
extern MethodInfo m20474_MI;
extern MethodInfo m20475_MI;
static MethodInfo* t2346_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20467_MI,
	&m5907_MI,
	&m20468_MI,
	&m20469_MI,
	&m20470_MI,
	&m20471_MI,
	&m20472_MI,
	&m5908_MI,
	&m20466_MI,
	&m20474_MI,
	&m20475_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5250_TI;
extern TypeInfo t5251_TI;
extern TypeInfo t5252_TI;
static TypeInfo* t2346_ITIs[] = 
{
	&t5250_TI,
	&t5251_TI,
	&t5252_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2346_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5250_TI, 21},
	{ &t5251_TI, 28},
	{ &t5252_TI, 33},
	{ &t5116_TI, 34},
	{ &t5117_TI, 41},
	{ &t5118_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2346_0_0_0;
extern Il2CppType t2346_1_0_0;
#include "t322.h"
extern TypeInfo t322_TI;
extern CustomAttributesCache t1259__CustomAttributeCache;
TypeInfo t2346_TI = 
{
	&g_mscorlib_dll_Image, NULL, "KeyValuePair`2[]", "System.Collections.Generic", t2346_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t322_TI, t2346_ITIs, t2346_VT, &EmptyCustomAttributesCache, &t2346_TI, &t2346_0_0_0, &t2346_1_0_0, t2346_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t322 ), -1, 0, 0, -1, 1057033, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1965_TI;



// Metadata Definition System.Collections.Generic.Link[]
static MethodInfo* t1965_MIs[] =
{
	NULL
};
extern MethodInfo m20478_MI;
extern MethodInfo m20479_MI;
extern MethodInfo m20480_MI;
extern MethodInfo m20481_MI;
extern MethodInfo m20482_MI;
extern MethodInfo m20483_MI;
extern MethodInfo m20477_MI;
extern MethodInfo m20485_MI;
extern MethodInfo m20486_MI;
static MethodInfo* t1965_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20478_MI,
	&m5907_MI,
	&m20479_MI,
	&m20480_MI,
	&m20481_MI,
	&m20482_MI,
	&m20483_MI,
	&m5908_MI,
	&m20477_MI,
	&m20485_MI,
	&m20486_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5253_TI;
extern TypeInfo t5254_TI;
extern TypeInfo t5255_TI;
static TypeInfo* t1965_ITIs[] = 
{
	&t5253_TI,
	&t5254_TI,
	&t5255_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t1965_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5253_TI, 21},
	{ &t5254_TI, 28},
	{ &t5255_TI, 33},
	{ &t5116_TI, 34},
	{ &t5117_TI, 41},
	{ &t5118_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1965_0_0_0;
extern Il2CppType t1965_1_0_0;
#include "t1248.h"
extern TypeInfo t1248_TI;
TypeInfo t1965_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Link[]", "System.Collections.Generic", t1965_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1248_TI, t1965_ITIs, t1965_VT, &EmptyCustomAttributesCache, &t1965_TI, &t1965_0_0_0, &t1965_1_0_0, t1965_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1248 ), -1, 0, 0, -1, 1048840, 1, false, false, false, false, false, true, false, false, false, false, false, true, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2343_TI;



// Metadata Definition UnityEngine.EventSystems.PointerEventData[]
static MethodInfo* t2343_MIs[] =
{
	NULL
};
extern MethodInfo m20489_MI;
extern MethodInfo m20490_MI;
extern MethodInfo m20491_MI;
extern MethodInfo m20492_MI;
extern MethodInfo m20493_MI;
extern MethodInfo m20494_MI;
extern MethodInfo m20488_MI;
extern MethodInfo m20496_MI;
extern MethodInfo m20497_MI;
extern MethodInfo m20500_MI;
extern MethodInfo m20501_MI;
extern MethodInfo m20502_MI;
extern MethodInfo m20503_MI;
extern MethodInfo m20504_MI;
extern MethodInfo m20505_MI;
extern MethodInfo m20499_MI;
extern MethodInfo m20507_MI;
extern MethodInfo m20508_MI;
static MethodInfo* t2343_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20489_MI,
	&m5907_MI,
	&m20490_MI,
	&m20491_MI,
	&m20492_MI,
	&m20493_MI,
	&m20494_MI,
	&m5908_MI,
	&m20488_MI,
	&m20496_MI,
	&m20497_MI,
	&m5905_MI,
	&m5906_MI,
	&m20500_MI,
	&m5907_MI,
	&m20501_MI,
	&m20502_MI,
	&m20503_MI,
	&m20504_MI,
	&m20505_MI,
	&m5908_MI,
	&m20499_MI,
	&m20507_MI,
	&m20508_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5256_TI;
extern TypeInfo t5257_TI;
extern TypeInfo t5258_TI;
extern TypeInfo t5259_TI;
extern TypeInfo t5260_TI;
extern TypeInfo t5261_TI;
static TypeInfo* t2343_ITIs[] = 
{
	&t5256_TI,
	&t5257_TI,
	&t5258_TI,
	&t5259_TI,
	&t5260_TI,
	&t5261_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2343_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5256_TI, 21},
	{ &t5257_TI, 28},
	{ &t5258_TI, 33},
	{ &t5259_TI, 34},
	{ &t5260_TI, 41},
	{ &t5261_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t2343_0_0_0;
extern Il2CppType t2343_1_0_0;
struct t6;
extern TypeInfo t6_TI;
extern CustomAttributesCache t6__CustomAttributeCache_U3CpointerEnterU3Ek__BackingField;
extern CustomAttributesCache t6__CustomAttributeCache_U3ClastPressU3Ek__BackingField;
extern CustomAttributesCache t6__CustomAttributeCache_U3CrawPointerPressU3Ek__BackingField;
extern CustomAttributesCache t6__CustomAttributeCache_U3CpointerDragU3Ek__BackingField;
extern CustomAttributesCache t6__CustomAttributeCache_U3CpointerCurrentRaycastU3Ek__BackingField;
extern CustomAttributesCache t6__CustomAttributeCache_U3CpointerPressRaycastU3Ek__BackingField;
extern CustomAttributesCache t6__CustomAttributeCache_U3CeligibleForClickU3Ek__BackingField;
extern CustomAttributesCache t6__CustomAttributeCache_U3CpointerIdU3Ek__BackingField;
extern CustomAttributesCache t6__CustomAttributeCache_U3CpositionU3Ek__BackingField;
extern CustomAttributesCache t6__CustomAttributeCache_U3CdeltaU3Ek__BackingField;
extern CustomAttributesCache t6__CustomAttributeCache_U3CpressPositionU3Ek__BackingField;
extern CustomAttributesCache t6__CustomAttributeCache_U3CworldPositionU3Ek__BackingField;
extern CustomAttributesCache t6__CustomAttributeCache_U3CworldNormalU3Ek__BackingField;
extern CustomAttributesCache t6__CustomAttributeCache_U3CclickTimeU3Ek__BackingField;
extern CustomAttributesCache t6__CustomAttributeCache_U3CclickCountU3Ek__BackingField;
extern CustomAttributesCache t6__CustomAttributeCache_U3CscrollDeltaU3Ek__BackingField;
extern CustomAttributesCache t6__CustomAttributeCache_U3CuseDragThresholdU3Ek__BackingField;
extern CustomAttributesCache t6__CustomAttributeCache_U3CdraggingU3Ek__BackingField;
extern CustomAttributesCache t6__CustomAttributeCache_U3CbuttonU3Ek__BackingField;
extern CustomAttributesCache t6__CustomAttributeCache_m194;
extern CustomAttributesCache t6__CustomAttributeCache_m195;
extern CustomAttributesCache t6__CustomAttributeCache_m196;
extern CustomAttributesCache t6__CustomAttributeCache_m197;
extern CustomAttributesCache t6__CustomAttributeCache_m198;
extern CustomAttributesCache t6__CustomAttributeCache_m199;
extern CustomAttributesCache t6__CustomAttributeCache_m200;
extern CustomAttributesCache t6__CustomAttributeCache_m201;
extern CustomAttributesCache t6__CustomAttributeCache_m202;
extern CustomAttributesCache t6__CustomAttributeCache_m203;
extern CustomAttributesCache t6__CustomAttributeCache_m204;
extern CustomAttributesCache t6__CustomAttributeCache_m205;
extern CustomAttributesCache t6__CustomAttributeCache_m206;
extern CustomAttributesCache t6__CustomAttributeCache_m207;
extern CustomAttributesCache t6__CustomAttributeCache_m208;
extern CustomAttributesCache t6__CustomAttributeCache_m209;
extern CustomAttributesCache t6__CustomAttributeCache_m210;
extern CustomAttributesCache t6__CustomAttributeCache_m211;
extern CustomAttributesCache t6__CustomAttributeCache_m212;
extern CustomAttributesCache t6__CustomAttributeCache_m213;
extern CustomAttributesCache t6__CustomAttributeCache_m214;
extern CustomAttributesCache t6__CustomAttributeCache_m215;
extern CustomAttributesCache t6__CustomAttributeCache_m216;
extern CustomAttributesCache t6__CustomAttributeCache_m217;
extern CustomAttributesCache t6__CustomAttributeCache_m218;
extern CustomAttributesCache t6__CustomAttributeCache_m219;
extern CustomAttributesCache t6__CustomAttributeCache_m220;
extern CustomAttributesCache t6__CustomAttributeCache_m221;
extern CustomAttributesCache t6__CustomAttributeCache_m222;
extern CustomAttributesCache t6__CustomAttributeCache_m223;
extern CustomAttributesCache t6__CustomAttributeCache_m224;
extern CustomAttributesCache t6__CustomAttributeCache_m225;
extern CustomAttributesCache t6__CustomAttributeCache_m226;
extern CustomAttributesCache t6__CustomAttributeCache_m227;
extern CustomAttributesCache t6__CustomAttributeCache_m228;
extern CustomAttributesCache t6__CustomAttributeCache_m229;
extern CustomAttributesCache t6__CustomAttributeCache_m230;
extern CustomAttributesCache t6__CustomAttributeCache_m231;
TypeInfo t2343_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "PointerEventData[]", "UnityEngine.EventSystems", t2343_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t6_TI, t2343_ITIs, t2343_VT, &EmptyCustomAttributesCache, &t2343_TI, &t2343_0_0_0, &t2343_1_0_0, t2343_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t6 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3824_TI;



// Metadata Definition UnityEngine.EventSystems.BaseEventData[]
static MethodInfo* t3824_MIs[] =
{
	NULL
};
static MethodInfo* t3824_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20500_MI,
	&m5907_MI,
	&m20501_MI,
	&m20502_MI,
	&m20503_MI,
	&m20504_MI,
	&m20505_MI,
	&m5908_MI,
	&m20499_MI,
	&m20507_MI,
	&m20508_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
static TypeInfo* t3824_ITIs[] = 
{
	&t5259_TI,
	&t5260_TI,
	&t5261_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3824_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5259_TI, 21},
	{ &t5260_TI, 28},
	{ &t5261_TI, 33},
	{ &t2182_TI, 34},
	{ &t2186_TI, 41},
	{ &t2183_TI, 46},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3824_0_0_0;
extern Il2CppType t3824_1_0_0;
struct t53;
extern TypeInfo t53_TI;
TypeInfo t3824_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "BaseEventData[]", "UnityEngine.EventSystems", t3824_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t53_TI, t3824_ITIs, t3824_VT, &EmptyCustomAttributesCache, &t3824_TI, &t3824_0_0_0, &t3824_1_0_0, t3824_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t53 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3541_TI;



// Metadata Definition System.Collections.DictionaryEntry[]
static MethodInfo* t3541_MIs[] =
{
	NULL
};
extern MethodInfo m20514_MI;
extern MethodInfo m20515_MI;
extern MethodInfo m20516_MI;
extern MethodInfo m20517_MI;
extern MethodInfo m20518_MI;
extern MethodInfo m20519_MI;
extern MethodInfo m20513_MI;
extern MethodInfo m20521_MI;
extern MethodInfo m20522_MI;
static MethodInfo* t3541_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20514_MI,
	&m5907_MI,
	&m20515_MI,
	&m20516_MI,
	&m20517_MI,
	&m20518_MI,
	&m20519_MI,
	&m5908_MI,
	&m20513_MI,
	&m20521_MI,
	&m20522_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5262_TI;
extern TypeInfo t5263_TI;
extern TypeInfo t5264_TI;
static TypeInfo* t3541_ITIs[] = 
{
	&t5262_TI,
	&t5263_TI,
	&t5264_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3541_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5262_TI, 21},
	{ &t5263_TI, 28},
	{ &t5264_TI, 33},
	{ &t5116_TI, 34},
	{ &t5117_TI, 41},
	{ &t5118_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3541_0_0_0;
extern Il2CppType t3541_1_0_0;
#include "t725.h"
extern TypeInfo t725_TI;
extern CustomAttributesCache t725__CustomAttributeCache;
TypeInfo t3541_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DictionaryEntry[]", "System.Collections", t3541_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t725_TI, t3541_ITIs, t3541_VT, &EmptyCustomAttributesCache, &t3541_TI, &t3541_0_0_0, &t3541_1_0_0, t3541_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t725 ), -1, 0, 0, -1, 1057033, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2366_TI;



// Metadata Definition UnityEngine.EventSystems.PointerInputModule/ButtonState[]
static MethodInfo* t2366_MIs[] =
{
	NULL
};
extern MethodInfo m20536_MI;
extern MethodInfo m20537_MI;
extern MethodInfo m20538_MI;
extern MethodInfo m20539_MI;
extern MethodInfo m20540_MI;
extern MethodInfo m20541_MI;
extern MethodInfo m20535_MI;
extern MethodInfo m20543_MI;
extern MethodInfo m20544_MI;
static MethodInfo* t2366_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20536_MI,
	&m5907_MI,
	&m20537_MI,
	&m20538_MI,
	&m20539_MI,
	&m20540_MI,
	&m20541_MI,
	&m5908_MI,
	&m20535_MI,
	&m20543_MI,
	&m20544_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t2368_TI;
extern TypeInfo t2375_TI;
extern TypeInfo t2369_TI;
static TypeInfo* t2366_ITIs[] = 
{
	&t2368_TI,
	&t2375_TI,
	&t2369_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2366_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2368_TI, 21},
	{ &t2375_TI, 28},
	{ &t2369_TI, 33},
	{ &t2182_TI, 34},
	{ &t2186_TI, 41},
	{ &t2183_TI, 46},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t2366_0_0_0;
extern Il2CppType t2366_1_0_0;
struct t113;
extern TypeInfo t113_TI;
TypeInfo t2366_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "ButtonState[]", "", t2366_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t113_TI, t2366_ITIs, t2366_VT, &EmptyCustomAttributesCache, &t2366_TI, &t2366_0_0_0, &t2366_1_0_0, t2366_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t113 *), -1, 0, 0, -1, 1048580, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3825_TI;



// Metadata Definition UnityEngine.EventSystems.StandaloneInputModule[]
static MethodInfo* t3825_MIs[] =
{
	NULL
};
extern MethodInfo m20562_MI;
extern MethodInfo m20563_MI;
extern MethodInfo m20564_MI;
extern MethodInfo m20565_MI;
extern MethodInfo m20566_MI;
extern MethodInfo m20567_MI;
extern MethodInfo m20561_MI;
extern MethodInfo m20569_MI;
extern MethodInfo m20570_MI;
static MethodInfo* t3825_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20562_MI,
	&m5907_MI,
	&m20563_MI,
	&m20564_MI,
	&m20565_MI,
	&m20566_MI,
	&m20567_MI,
	&m5908_MI,
	&m20561_MI,
	&m20569_MI,
	&m20570_MI,
	&m5905_MI,
	&m5906_MI,
	&m20411_MI,
	&m5907_MI,
	&m20412_MI,
	&m20413_MI,
	&m20414_MI,
	&m20415_MI,
	&m20416_MI,
	&m5908_MI,
	&m20410_MI,
	&m20418_MI,
	&m20419_MI,
	&m5905_MI,
	&m5906_MI,
	&m19968_MI,
	&m5907_MI,
	&m19969_MI,
	&m19970_MI,
	&m19971_MI,
	&m19972_MI,
	&m19973_MI,
	&m5908_MI,
	&m19967_MI,
	&m19975_MI,
	&m19976_MI,
	&m5905_MI,
	&m5906_MI,
	&m19777_MI,
	&m5907_MI,
	&m19778_MI,
	&m19779_MI,
	&m19780_MI,
	&m19781_MI,
	&m19782_MI,
	&m5908_MI,
	&m19776_MI,
	&m19784_MI,
	&m19785_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5265_TI;
extern TypeInfo t5266_TI;
extern TypeInfo t5267_TI;
static TypeInfo* t3825_ITIs[] = 
{
	&t5265_TI,
	&t5266_TI,
	&t5267_TI,
	&t5238_TI,
	&t5239_TI,
	&t5240_TI,
	&t2174_TI,
	&t2207_TI,
	&t2175_TI,
	&t5122_TI,
	&t5123_TI,
	&t5124_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3825_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5265_TI, 21},
	{ &t5266_TI, 28},
	{ &t5267_TI, 33},
	{ &t5238_TI, 34},
	{ &t5239_TI, 41},
	{ &t5240_TI, 46},
	{ &t2174_TI, 47},
	{ &t2207_TI, 54},
	{ &t2175_TI, 59},
	{ &t5122_TI, 60},
	{ &t5123_TI, 67},
	{ &t5124_TI, 72},
	{ &t5059_TI, 73},
	{ &t5060_TI, 80},
	{ &t5061_TI, 85},
	{ &t5062_TI, 86},
	{ &t5063_TI, 93},
	{ &t5064_TI, 98},
	{ &t2240_TI, 99},
	{ &t2245_TI, 106},
	{ &t2241_TI, 111},
	{ &t5065_TI, 112},
	{ &t5066_TI, 119},
	{ &t5067_TI, 124},
	{ &t2182_TI, 125},
	{ &t2186_TI, 132},
	{ &t2183_TI, 137},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3825_0_0_0;
extern Il2CppType t3825_1_0_0;
struct t121;
extern TypeInfo t121_TI;
extern CustomAttributesCache t121__CustomAttributeCache;
extern CustomAttributesCache t121__CustomAttributeCache_m_HorizontalAxis;
extern CustomAttributesCache t121__CustomAttributeCache_m_VerticalAxis;
extern CustomAttributesCache t121__CustomAttributeCache_m_SubmitButton;
extern CustomAttributesCache t121__CustomAttributeCache_m_CancelButton;
extern CustomAttributesCache t121__CustomAttributeCache_m_InputActionsPerSecond;
extern CustomAttributesCache t121__CustomAttributeCache_m_AllowActivationOnMobileDevice;
extern CustomAttributesCache t121__CustomAttributeCache_t121____inputMode_PropertyInfo;
TypeInfo t3825_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "StandaloneInputModule[]", "UnityEngine.EventSystems", t3825_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t121_TI, t3825_ITIs, t3825_VT, &EmptyCustomAttributesCache, &t3825_TI, &t3825_0_0_0, &t3825_1_0_0, t3825_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t121 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 138, 27, 31};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3826_TI;



// Metadata Definition UnityEngine.EventSystems.StandaloneInputModule/InputMode[]
static MethodInfo* t3826_MIs[] =
{
	NULL
};
extern MethodInfo m20600_MI;
extern MethodInfo m20601_MI;
extern MethodInfo m20602_MI;
extern MethodInfo m20603_MI;
extern MethodInfo m20604_MI;
extern MethodInfo m20605_MI;
extern MethodInfo m20599_MI;
extern MethodInfo m20607_MI;
extern MethodInfo m20608_MI;
static MethodInfo* t3826_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20600_MI,
	&m5907_MI,
	&m20601_MI,
	&m20602_MI,
	&m20603_MI,
	&m20604_MI,
	&m20605_MI,
	&m5908_MI,
	&m20599_MI,
	&m20607_MI,
	&m20608_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5268_TI;
extern TypeInfo t5269_TI;
extern TypeInfo t5270_TI;
static TypeInfo* t3826_ITIs[] = 
{
	&t5268_TI,
	&t5269_TI,
	&t5270_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3826_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5268_TI, 21},
	{ &t5269_TI, 28},
	{ &t5270_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3826_0_0_0;
extern Il2CppType t3826_1_0_0;
#include "t120.h"
extern TypeInfo t120_TI;
extern CustomAttributesCache t120__CustomAttributeCache;
TypeInfo t3826_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "InputMode[]", "", t3826_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t120_TI, t3826_ITIs, t3826_VT, &EmptyCustomAttributesCache, &t44_TI, &t3826_0_0_0, &t3826_1_0_0, t3826_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 258, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3827_TI;



// Metadata Definition UnityEngine.EventSystems.TouchInputModule[]
static MethodInfo* t3827_MIs[] =
{
	NULL
};
extern MethodInfo m20611_MI;
extern MethodInfo m20612_MI;
extern MethodInfo m20613_MI;
extern MethodInfo m20614_MI;
extern MethodInfo m20615_MI;
extern MethodInfo m20616_MI;
extern MethodInfo m20610_MI;
extern MethodInfo m20618_MI;
extern MethodInfo m20619_MI;
static MethodInfo* t3827_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20611_MI,
	&m5907_MI,
	&m20612_MI,
	&m20613_MI,
	&m20614_MI,
	&m20615_MI,
	&m20616_MI,
	&m5908_MI,
	&m20610_MI,
	&m20618_MI,
	&m20619_MI,
	&m5905_MI,
	&m5906_MI,
	&m20411_MI,
	&m5907_MI,
	&m20412_MI,
	&m20413_MI,
	&m20414_MI,
	&m20415_MI,
	&m20416_MI,
	&m5908_MI,
	&m20410_MI,
	&m20418_MI,
	&m20419_MI,
	&m5905_MI,
	&m5906_MI,
	&m19968_MI,
	&m5907_MI,
	&m19969_MI,
	&m19970_MI,
	&m19971_MI,
	&m19972_MI,
	&m19973_MI,
	&m5908_MI,
	&m19967_MI,
	&m19975_MI,
	&m19976_MI,
	&m5905_MI,
	&m5906_MI,
	&m19777_MI,
	&m5907_MI,
	&m19778_MI,
	&m19779_MI,
	&m19780_MI,
	&m19781_MI,
	&m19782_MI,
	&m5908_MI,
	&m19776_MI,
	&m19784_MI,
	&m19785_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5271_TI;
extern TypeInfo t5272_TI;
extern TypeInfo t5273_TI;
static TypeInfo* t3827_ITIs[] = 
{
	&t5271_TI,
	&t5272_TI,
	&t5273_TI,
	&t5238_TI,
	&t5239_TI,
	&t5240_TI,
	&t2174_TI,
	&t2207_TI,
	&t2175_TI,
	&t5122_TI,
	&t5123_TI,
	&t5124_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3827_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5271_TI, 21},
	{ &t5272_TI, 28},
	{ &t5273_TI, 33},
	{ &t5238_TI, 34},
	{ &t5239_TI, 41},
	{ &t5240_TI, 46},
	{ &t2174_TI, 47},
	{ &t2207_TI, 54},
	{ &t2175_TI, 59},
	{ &t5122_TI, 60},
	{ &t5123_TI, 67},
	{ &t5124_TI, 72},
	{ &t5059_TI, 73},
	{ &t5060_TI, 80},
	{ &t5061_TI, 85},
	{ &t5062_TI, 86},
	{ &t5063_TI, 93},
	{ &t5064_TI, 98},
	{ &t2240_TI, 99},
	{ &t2245_TI, 106},
	{ &t2241_TI, 111},
	{ &t5065_TI, 112},
	{ &t5066_TI, 119},
	{ &t5067_TI, 124},
	{ &t2182_TI, 125},
	{ &t2186_TI, 132},
	{ &t2183_TI, 137},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3827_0_0_0;
extern Il2CppType t3827_1_0_0;
struct t122;
extern TypeInfo t122_TI;
extern CustomAttributesCache t122__CustomAttributeCache;
extern CustomAttributesCache t122__CustomAttributeCache_m_AllowActivationOnStandalone;
TypeInfo t3827_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "TouchInputModule[]", "UnityEngine.EventSystems", t3827_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t122_TI, t3827_ITIs, t3827_VT, &EmptyCustomAttributesCache, &t3827_TI, &t3827_0_0_0, &t3827_1_0_0, t3827_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t122 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 138, 27, 31};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3828_TI;



// Metadata Definition UnityEngine.EventSystems.Physics2DRaycaster[]
static MethodInfo* t3828_MIs[] =
{
	NULL
};
extern MethodInfo m20624_MI;
extern MethodInfo m20625_MI;
extern MethodInfo m20626_MI;
extern MethodInfo m20627_MI;
extern MethodInfo m20628_MI;
extern MethodInfo m20629_MI;
extern MethodInfo m20623_MI;
extern MethodInfo m20631_MI;
extern MethodInfo m20632_MI;
extern MethodInfo m20635_MI;
extern MethodInfo m20636_MI;
extern MethodInfo m20637_MI;
extern MethodInfo m20638_MI;
extern MethodInfo m20639_MI;
extern MethodInfo m20640_MI;
extern MethodInfo m20634_MI;
extern MethodInfo m20642_MI;
extern MethodInfo m20643_MI;
static MethodInfo* t3828_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20624_MI,
	&m5907_MI,
	&m20625_MI,
	&m20626_MI,
	&m20627_MI,
	&m20628_MI,
	&m20629_MI,
	&m5908_MI,
	&m20623_MI,
	&m20631_MI,
	&m20632_MI,
	&m5905_MI,
	&m5906_MI,
	&m20635_MI,
	&m5907_MI,
	&m20636_MI,
	&m20637_MI,
	&m20638_MI,
	&m20639_MI,
	&m20640_MI,
	&m5908_MI,
	&m20634_MI,
	&m20642_MI,
	&m20643_MI,
	&m5905_MI,
	&m5906_MI,
	&m20105_MI,
	&m5907_MI,
	&m20106_MI,
	&m20107_MI,
	&m20108_MI,
	&m20109_MI,
	&m20110_MI,
	&m5908_MI,
	&m20104_MI,
	&m20112_MI,
	&m20113_MI,
	&m5905_MI,
	&m5906_MI,
	&m19777_MI,
	&m5907_MI,
	&m19778_MI,
	&m19779_MI,
	&m19780_MI,
	&m19781_MI,
	&m19782_MI,
	&m5908_MI,
	&m19776_MI,
	&m19784_MI,
	&m19785_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5274_TI;
extern TypeInfo t5275_TI;
extern TypeInfo t5276_TI;
extern TypeInfo t5277_TI;
extern TypeInfo t5278_TI;
extern TypeInfo t5279_TI;
static TypeInfo* t3828_ITIs[] = 
{
	&t5274_TI,
	&t5275_TI,
	&t5276_TI,
	&t5277_TI,
	&t5278_TI,
	&t5279_TI,
	&t2267_TI,
	&t2274_TI,
	&t2268_TI,
	&t5122_TI,
	&t5123_TI,
	&t5124_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3828_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5274_TI, 21},
	{ &t5275_TI, 28},
	{ &t5276_TI, 33},
	{ &t5277_TI, 34},
	{ &t5278_TI, 41},
	{ &t5279_TI, 46},
	{ &t2267_TI, 47},
	{ &t2274_TI, 54},
	{ &t2268_TI, 59},
	{ &t5122_TI, 60},
	{ &t5123_TI, 67},
	{ &t5124_TI, 72},
	{ &t5059_TI, 73},
	{ &t5060_TI, 80},
	{ &t5061_TI, 85},
	{ &t5062_TI, 86},
	{ &t5063_TI, 93},
	{ &t5064_TI, 98},
	{ &t2240_TI, 99},
	{ &t2245_TI, 106},
	{ &t2241_TI, 111},
	{ &t5065_TI, 112},
	{ &t5066_TI, 119},
	{ &t5067_TI, 124},
	{ &t2182_TI, 125},
	{ &t2186_TI, 132},
	{ &t2183_TI, 137},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3828_0_0_0;
extern Il2CppType t3828_1_0_0;
struct t123;
extern TypeInfo t123_TI;
extern CustomAttributesCache t123__CustomAttributeCache;
TypeInfo t3828_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Physics2DRaycaster[]", "UnityEngine.EventSystems", t3828_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t123_TI, t3828_ITIs, t3828_VT, &EmptyCustomAttributesCache, &t3828_TI, &t3828_0_0_0, &t3828_1_0_0, t3828_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t123 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 138, 27, 31};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3829_TI;



// Metadata Definition UnityEngine.EventSystems.PhysicsRaycaster[]
static MethodInfo* t3829_MIs[] =
{
	NULL
};
static MethodInfo* t3829_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20635_MI,
	&m5907_MI,
	&m20636_MI,
	&m20637_MI,
	&m20638_MI,
	&m20639_MI,
	&m20640_MI,
	&m5908_MI,
	&m20634_MI,
	&m20642_MI,
	&m20643_MI,
	&m5905_MI,
	&m5906_MI,
	&m20105_MI,
	&m5907_MI,
	&m20106_MI,
	&m20107_MI,
	&m20108_MI,
	&m20109_MI,
	&m20110_MI,
	&m5908_MI,
	&m20104_MI,
	&m20112_MI,
	&m20113_MI,
	&m5905_MI,
	&m5906_MI,
	&m19777_MI,
	&m5907_MI,
	&m19778_MI,
	&m19779_MI,
	&m19780_MI,
	&m19781_MI,
	&m19782_MI,
	&m5908_MI,
	&m19776_MI,
	&m19784_MI,
	&m19785_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
static TypeInfo* t3829_ITIs[] = 
{
	&t5277_TI,
	&t5278_TI,
	&t5279_TI,
	&t2267_TI,
	&t2274_TI,
	&t2268_TI,
	&t5122_TI,
	&t5123_TI,
	&t5124_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3829_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5277_TI, 21},
	{ &t5278_TI, 28},
	{ &t5279_TI, 33},
	{ &t2267_TI, 34},
	{ &t2274_TI, 41},
	{ &t2268_TI, 46},
	{ &t5122_TI, 47},
	{ &t5123_TI, 54},
	{ &t5124_TI, 59},
	{ &t5059_TI, 60},
	{ &t5060_TI, 67},
	{ &t5061_TI, 72},
	{ &t5062_TI, 73},
	{ &t5063_TI, 80},
	{ &t5064_TI, 85},
	{ &t2240_TI, 86},
	{ &t2245_TI, 93},
	{ &t2241_TI, 98},
	{ &t5065_TI, 99},
	{ &t5066_TI, 106},
	{ &t5067_TI, 111},
	{ &t2182_TI, 112},
	{ &t2186_TI, 119},
	{ &t2183_TI, 124},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3829_0_0_0;
extern Il2CppType t3829_1_0_0;
struct t124;
extern TypeInfo t124_TI;
extern CustomAttributesCache t124__CustomAttributeCache;
extern CustomAttributesCache t124__CustomAttributeCache_m_EventMask;
extern CustomAttributesCache t124__CustomAttributeCache_U3CU3Ef__am$cache2;
extern CustomAttributesCache t124__CustomAttributeCache_m340;
TypeInfo t3829_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "PhysicsRaycaster[]", "UnityEngine.EventSystems", t3829_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t124_TI, t3829_ITIs, t3829_VT, &EmptyCustomAttributesCache, &t3829_TI, &t3829_0_0_0, &t3829_1_0_0, t3829_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t124 *), -1, sizeof(t3829_SFs), 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 125, 24, 28};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t335_TI;



// Metadata Definition UnityEngine.RaycastHit2D[]
static MethodInfo* t335_MIs[] =
{
	NULL
};
extern MethodInfo m20647_MI;
extern MethodInfo m20648_MI;
extern MethodInfo m20649_MI;
extern MethodInfo m20650_MI;
extern MethodInfo m20651_MI;
extern MethodInfo m20652_MI;
extern MethodInfo m20646_MI;
extern MethodInfo m20654_MI;
extern MethodInfo m20655_MI;
static MethodInfo* t335_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20647_MI,
	&m5907_MI,
	&m20648_MI,
	&m20649_MI,
	&m20650_MI,
	&m20651_MI,
	&m20652_MI,
	&m5908_MI,
	&m20646_MI,
	&m20654_MI,
	&m20655_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5280_TI;
extern TypeInfo t5281_TI;
extern TypeInfo t5282_TI;
static TypeInfo* t335_ITIs[] = 
{
	&t5280_TI,
	&t5281_TI,
	&t5282_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t335_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5280_TI, 21},
	{ &t5281_TI, 28},
	{ &t5282_TI, 33},
	{ &t5116_TI, 34},
	{ &t5117_TI, 41},
	{ &t5118_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t335_0_0_0;
extern Il2CppType t335_1_0_0;
#include "t330.h"
extern TypeInfo t330_TI;
TypeInfo t335_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "RaycastHit2D[]", "UnityEngine", t335_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t330_TI, t335_ITIs, t335_VT, &EmptyCustomAttributesCache, &t335_TI, &t335_0_0_0, &t335_1_0_0, t335_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t330 ), -1, 0, 0, -1, 1048841, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t339_TI;



// Metadata Definition UnityEngine.RaycastHit[]
static MethodInfo* t339_MIs[] =
{
	NULL
};
extern MethodInfo m20659_MI;
extern MethodInfo m20660_MI;
extern MethodInfo m20661_MI;
extern MethodInfo m20662_MI;
extern MethodInfo m20663_MI;
extern MethodInfo m20664_MI;
extern MethodInfo m20658_MI;
extern MethodInfo m20666_MI;
extern MethodInfo m20667_MI;
static MethodInfo* t339_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20659_MI,
	&m5907_MI,
	&m20660_MI,
	&m20661_MI,
	&m20662_MI,
	&m20663_MI,
	&m20664_MI,
	&m5908_MI,
	&m20658_MI,
	&m20666_MI,
	&m20667_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5283_TI;
extern TypeInfo t5284_TI;
extern TypeInfo t5285_TI;
static TypeInfo* t339_ITIs[] = 
{
	&t5283_TI,
	&t5284_TI,
	&t5285_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t339_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5283_TI, 21},
	{ &t5284_TI, 28},
	{ &t5285_TI, 33},
	{ &t5116_TI, 34},
	{ &t5117_TI, 41},
	{ &t5118_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t339_0_0_0;
extern Il2CppType t339_1_0_0;
#include "t127.h"
extern TypeInfo t127_TI;
TypeInfo t339_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "RaycastHit[]", "UnityEngine", t339_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t127_TI, t339_ITIs, t339_VT, &EmptyCustomAttributesCache, &t339_TI, &t339_0_0_0, &t339_1_0_0, t339_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t127 ), -1, 0, 0, -1, 1048841, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3830_TI;



// Metadata Definition UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode[]
static MethodInfo* t3830_MIs[] =
{
	NULL
};
extern MethodInfo m20674_MI;
extern MethodInfo m20675_MI;
extern MethodInfo m20676_MI;
extern MethodInfo m20677_MI;
extern MethodInfo m20678_MI;
extern MethodInfo m20679_MI;
extern MethodInfo m20673_MI;
extern MethodInfo m20681_MI;
extern MethodInfo m20682_MI;
static MethodInfo* t3830_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20674_MI,
	&m5907_MI,
	&m20675_MI,
	&m20676_MI,
	&m20677_MI,
	&m20678_MI,
	&m20679_MI,
	&m5908_MI,
	&m20673_MI,
	&m20681_MI,
	&m20682_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5286_TI;
extern TypeInfo t5287_TI;
extern TypeInfo t5288_TI;
static TypeInfo* t3830_ITIs[] = 
{
	&t5286_TI,
	&t5287_TI,
	&t5288_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3830_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5286_TI, 21},
	{ &t5287_TI, 28},
	{ &t5288_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3830_0_0_0;
extern Il2CppType t3830_1_0_0;
#include "t128.h"
extern TypeInfo t128_TI;
TypeInfo t3830_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "ColorTweenMode[]", "", t3830_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t128_TI, t3830_ITIs, t3830_VT, &EmptyCustomAttributesCache, &t44_TI, &t3830_0_0_0, &t3830_1_0_0, t3830_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 258, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3831_TI;



// Metadata Definition UnityEngine.UI.Button[]
static MethodInfo* t3831_MIs[] =
{
	NULL
};
extern MethodInfo m20685_MI;
extern MethodInfo m20686_MI;
extern MethodInfo m20687_MI;
extern MethodInfo m20688_MI;
extern MethodInfo m20689_MI;
extern MethodInfo m20690_MI;
extern MethodInfo m20684_MI;
extern MethodInfo m20692_MI;
extern MethodInfo m20693_MI;
extern MethodInfo m20696_MI;
extern MethodInfo m20697_MI;
extern MethodInfo m20698_MI;
extern MethodInfo m20699_MI;
extern MethodInfo m20700_MI;
extern MethodInfo m20701_MI;
extern MethodInfo m20695_MI;
extern MethodInfo m20703_MI;
extern MethodInfo m20704_MI;
static MethodInfo* t3831_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20685_MI,
	&m5907_MI,
	&m20686_MI,
	&m20687_MI,
	&m20688_MI,
	&m20689_MI,
	&m20690_MI,
	&m5908_MI,
	&m20684_MI,
	&m20692_MI,
	&m20693_MI,
	&m5905_MI,
	&m5906_MI,
	&m19581_MI,
	&m5907_MI,
	&m19582_MI,
	&m19583_MI,
	&m19584_MI,
	&m19585_MI,
	&m19586_MI,
	&m5908_MI,
	&m19580_MI,
	&m19588_MI,
	&m19589_MI,
	&m5905_MI,
	&m5906_MI,
	&m20164_MI,
	&m5907_MI,
	&m20165_MI,
	&m20166_MI,
	&m20167_MI,
	&m20168_MI,
	&m20169_MI,
	&m5908_MI,
	&m20163_MI,
	&m20171_MI,
	&m20172_MI,
	&m5905_MI,
	&m5906_MI,
	&m20285_MI,
	&m5907_MI,
	&m20286_MI,
	&m20287_MI,
	&m20288_MI,
	&m20289_MI,
	&m20290_MI,
	&m5908_MI,
	&m20284_MI,
	&m20292_MI,
	&m20293_MI,
	&m5905_MI,
	&m5906_MI,
	&m20696_MI,
	&m5907_MI,
	&m20697_MI,
	&m20698_MI,
	&m20699_MI,
	&m20700_MI,
	&m20701_MI,
	&m5908_MI,
	&m20695_MI,
	&m20703_MI,
	&m20704_MI,
	&m5905_MI,
	&m5906_MI,
	&m19581_MI,
	&m5907_MI,
	&m19582_MI,
	&m19583_MI,
	&m19584_MI,
	&m19585_MI,
	&m19586_MI,
	&m5908_MI,
	&m19580_MI,
	&m19588_MI,
	&m19589_MI,
	&m5905_MI,
	&m5906_MI,
	&m19570_MI,
	&m5907_MI,
	&m19571_MI,
	&m19572_MI,
	&m19573_MI,
	&m19574_MI,
	&m19575_MI,
	&m5908_MI,
	&m19569_MI,
	&m19577_MI,
	&m19578_MI,
	&m5905_MI,
	&m5906_MI,
	&m19592_MI,
	&m5907_MI,
	&m19593_MI,
	&m19594_MI,
	&m19595_MI,
	&m19596_MI,
	&m19597_MI,
	&m5908_MI,
	&m19591_MI,
	&m19599_MI,
	&m19600_MI,
	&m5905_MI,
	&m5906_MI,
	&m20142_MI,
	&m5907_MI,
	&m20143_MI,
	&m20144_MI,
	&m20145_MI,
	&m20146_MI,
	&m20147_MI,
	&m5908_MI,
	&m20141_MI,
	&m20149_MI,
	&m20150_MI,
	&m5905_MI,
	&m5906_MI,
	&m20153_MI,
	&m5907_MI,
	&m20154_MI,
	&m20155_MI,
	&m20156_MI,
	&m20157_MI,
	&m20158_MI,
	&m5908_MI,
	&m20152_MI,
	&m20160_MI,
	&m20161_MI,
	&m5905_MI,
	&m5906_MI,
	&m20252_MI,
	&m5907_MI,
	&m20253_MI,
	&m20254_MI,
	&m20255_MI,
	&m20256_MI,
	&m20257_MI,
	&m5908_MI,
	&m20251_MI,
	&m20259_MI,
	&m20260_MI,
	&m5905_MI,
	&m5906_MI,
	&m20263_MI,
	&m5907_MI,
	&m20264_MI,
	&m20265_MI,
	&m20266_MI,
	&m20267_MI,
	&m20268_MI,
	&m5908_MI,
	&m20262_MI,
	&m20270_MI,
	&m20271_MI,
	&m5905_MI,
	&m5906_MI,
	&m20274_MI,
	&m5907_MI,
	&m20275_MI,
	&m20276_MI,
	&m20277_MI,
	&m20278_MI,
	&m20279_MI,
	&m5908_MI,
	&m20273_MI,
	&m20281_MI,
	&m20282_MI,
	&m5905_MI,
	&m5906_MI,
	&m19777_MI,
	&m5907_MI,
	&m19778_MI,
	&m19779_MI,
	&m19780_MI,
	&m19781_MI,
	&m19782_MI,
	&m5908_MI,
	&m19776_MI,
	&m19784_MI,
	&m19785_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5289_TI;
extern TypeInfo t5290_TI;
extern TypeInfo t5291_TI;
extern TypeInfo t2640_TI;
extern TypeInfo t2646_TI;
extern TypeInfo t2641_TI;
static TypeInfo* t3831_ITIs[] = 
{
	&t5289_TI,
	&t5290_TI,
	&t5291_TI,
	&t2216_TI,
	&t312_TI,
	&t2217_TI,
	&t5187_TI,
	&t5188_TI,
	&t5189_TI,
	&t5220_TI,
	&t5221_TI,
	&t5222_TI,
	&t2640_TI,
	&t2646_TI,
	&t2641_TI,
	&t2216_TI,
	&t312_TI,
	&t2217_TI,
	&t5071_TI,
	&t5072_TI,
	&t5073_TI,
	&t5074_TI,
	&t5075_TI,
	&t5076_TI,
	&t5181_TI,
	&t5182_TI,
	&t5183_TI,
	&t5184_TI,
	&t5185_TI,
	&t5186_TI,
	&t5211_TI,
	&t5212_TI,
	&t5213_TI,
	&t5214_TI,
	&t5215_TI,
	&t5216_TI,
	&t5217_TI,
	&t5218_TI,
	&t5219_TI,
	&t5122_TI,
	&t5123_TI,
	&t5124_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3831_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5289_TI, 21},
	{ &t5290_TI, 28},
	{ &t5291_TI, 33},
	{ &t2216_TI, 34},
	{ &t312_TI, 41},
	{ &t2217_TI, 46},
	{ &t5187_TI, 47},
	{ &t5188_TI, 54},
	{ &t5189_TI, 59},
	{ &t5220_TI, 60},
	{ &t5221_TI, 67},
	{ &t5222_TI, 72},
	{ &t2640_TI, 73},
	{ &t2646_TI, 80},
	{ &t2641_TI, 85},
	{ &t2216_TI, 86},
	{ &t312_TI, 93},
	{ &t2217_TI, 98},
	{ &t5071_TI, 99},
	{ &t5072_TI, 106},
	{ &t5073_TI, 111},
	{ &t5074_TI, 112},
	{ &t5075_TI, 119},
	{ &t5076_TI, 124},
	{ &t5181_TI, 125},
	{ &t5182_TI, 132},
	{ &t5183_TI, 137},
	{ &t5184_TI, 138},
	{ &t5185_TI, 145},
	{ &t5186_TI, 150},
	{ &t5211_TI, 151},
	{ &t5212_TI, 158},
	{ &t5213_TI, 163},
	{ &t5214_TI, 164},
	{ &t5215_TI, 171},
	{ &t5216_TI, 176},
	{ &t5217_TI, 177},
	{ &t5218_TI, 184},
	{ &t5219_TI, 189},
	{ &t5122_TI, 190},
	{ &t5123_TI, 197},
	{ &t5124_TI, 202},
	{ &t5059_TI, 203},
	{ &t5060_TI, 210},
	{ &t5061_TI, 215},
	{ &t5062_TI, 216},
	{ &t5063_TI, 223},
	{ &t5064_TI, 228},
	{ &t2240_TI, 229},
	{ &t2245_TI, 236},
	{ &t2241_TI, 241},
	{ &t5065_TI, 242},
	{ &t5066_TI, 249},
	{ &t5067_TI, 254},
	{ &t2182_TI, 255},
	{ &t2186_TI, 262},
	{ &t2183_TI, 267},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3831_0_0_0;
extern Il2CppType t3831_1_0_0;
struct t9;
extern TypeInfo t9_TI;
extern CustomAttributesCache t9__CustomAttributeCache;
extern CustomAttributesCache t9__CustomAttributeCache_m_OnClick;
extern CustomAttributesCache t9__CustomAttributeCache_m378;
TypeInfo t3831_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Button[]", "UnityEngine.UI", t3831_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t9_TI, t3831_ITIs, t3831_VT, &EmptyCustomAttributesCache, &t3831_TI, &t3831_0_0_0, &t3831_1_0_0, t3831_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t9 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 268, 57, 61};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2638_TI;



// Metadata Definition UnityEngine.UI.Selectable[]
static MethodInfo* t2638_MIs[] =
{
	NULL
};
static MethodInfo* t2638_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20696_MI,
	&m5907_MI,
	&m20697_MI,
	&m20698_MI,
	&m20699_MI,
	&m20700_MI,
	&m20701_MI,
	&m5908_MI,
	&m20695_MI,
	&m20703_MI,
	&m20704_MI,
	&m5905_MI,
	&m5906_MI,
	&m19581_MI,
	&m5907_MI,
	&m19582_MI,
	&m19583_MI,
	&m19584_MI,
	&m19585_MI,
	&m19586_MI,
	&m5908_MI,
	&m19580_MI,
	&m19588_MI,
	&m19589_MI,
	&m5905_MI,
	&m5906_MI,
	&m19570_MI,
	&m5907_MI,
	&m19571_MI,
	&m19572_MI,
	&m19573_MI,
	&m19574_MI,
	&m19575_MI,
	&m5908_MI,
	&m19569_MI,
	&m19577_MI,
	&m19578_MI,
	&m5905_MI,
	&m5906_MI,
	&m19592_MI,
	&m5907_MI,
	&m19593_MI,
	&m19594_MI,
	&m19595_MI,
	&m19596_MI,
	&m19597_MI,
	&m5908_MI,
	&m19591_MI,
	&m19599_MI,
	&m19600_MI,
	&m5905_MI,
	&m5906_MI,
	&m20142_MI,
	&m5907_MI,
	&m20143_MI,
	&m20144_MI,
	&m20145_MI,
	&m20146_MI,
	&m20147_MI,
	&m5908_MI,
	&m20141_MI,
	&m20149_MI,
	&m20150_MI,
	&m5905_MI,
	&m5906_MI,
	&m20153_MI,
	&m5907_MI,
	&m20154_MI,
	&m20155_MI,
	&m20156_MI,
	&m20157_MI,
	&m20158_MI,
	&m5908_MI,
	&m20152_MI,
	&m20160_MI,
	&m20161_MI,
	&m5905_MI,
	&m5906_MI,
	&m20252_MI,
	&m5907_MI,
	&m20253_MI,
	&m20254_MI,
	&m20255_MI,
	&m20256_MI,
	&m20257_MI,
	&m5908_MI,
	&m20251_MI,
	&m20259_MI,
	&m20260_MI,
	&m5905_MI,
	&m5906_MI,
	&m20263_MI,
	&m5907_MI,
	&m20264_MI,
	&m20265_MI,
	&m20266_MI,
	&m20267_MI,
	&m20268_MI,
	&m5908_MI,
	&m20262_MI,
	&m20270_MI,
	&m20271_MI,
	&m5905_MI,
	&m5906_MI,
	&m20274_MI,
	&m5907_MI,
	&m20275_MI,
	&m20276_MI,
	&m20277_MI,
	&m20278_MI,
	&m20279_MI,
	&m5908_MI,
	&m20273_MI,
	&m20281_MI,
	&m20282_MI,
	&m5905_MI,
	&m5906_MI,
	&m19777_MI,
	&m5907_MI,
	&m19778_MI,
	&m19779_MI,
	&m19780_MI,
	&m19781_MI,
	&m19782_MI,
	&m5908_MI,
	&m19776_MI,
	&m19784_MI,
	&m19785_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
static TypeInfo* t2638_ITIs[] = 
{
	&t2640_TI,
	&t2646_TI,
	&t2641_TI,
	&t2216_TI,
	&t312_TI,
	&t2217_TI,
	&t5071_TI,
	&t5072_TI,
	&t5073_TI,
	&t5074_TI,
	&t5075_TI,
	&t5076_TI,
	&t5181_TI,
	&t5182_TI,
	&t5183_TI,
	&t5184_TI,
	&t5185_TI,
	&t5186_TI,
	&t5211_TI,
	&t5212_TI,
	&t5213_TI,
	&t5214_TI,
	&t5215_TI,
	&t5216_TI,
	&t5217_TI,
	&t5218_TI,
	&t5219_TI,
	&t5122_TI,
	&t5123_TI,
	&t5124_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2638_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2640_TI, 21},
	{ &t2646_TI, 28},
	{ &t2641_TI, 33},
	{ &t2216_TI, 34},
	{ &t312_TI, 41},
	{ &t2217_TI, 46},
	{ &t5071_TI, 47},
	{ &t5072_TI, 54},
	{ &t5073_TI, 59},
	{ &t5074_TI, 60},
	{ &t5075_TI, 67},
	{ &t5076_TI, 72},
	{ &t5181_TI, 73},
	{ &t5182_TI, 80},
	{ &t5183_TI, 85},
	{ &t5184_TI, 86},
	{ &t5185_TI, 93},
	{ &t5186_TI, 98},
	{ &t5211_TI, 99},
	{ &t5212_TI, 106},
	{ &t5213_TI, 111},
	{ &t5214_TI, 112},
	{ &t5215_TI, 119},
	{ &t5216_TI, 124},
	{ &t5217_TI, 125},
	{ &t5218_TI, 132},
	{ &t5219_TI, 137},
	{ &t5122_TI, 138},
	{ &t5123_TI, 145},
	{ &t5124_TI, 150},
	{ &t5059_TI, 151},
	{ &t5060_TI, 158},
	{ &t5061_TI, 163},
	{ &t5062_TI, 164},
	{ &t5063_TI, 171},
	{ &t5064_TI, 176},
	{ &t2240_TI, 177},
	{ &t2245_TI, 184},
	{ &t2241_TI, 189},
	{ &t5065_TI, 190},
	{ &t5066_TI, 197},
	{ &t5067_TI, 202},
	{ &t2182_TI, 203},
	{ &t2186_TI, 210},
	{ &t2183_TI, 215},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t2638_0_0_0;
extern Il2CppType t2638_1_0_0;
struct t139;
extern TypeInfo t139_TI;
extern CustomAttributesCache t139__CustomAttributeCache;
extern CustomAttributesCache t139__CustomAttributeCache_m_Navigation;
extern CustomAttributesCache t139__CustomAttributeCache_m_Transition;
extern CustomAttributesCache t139__CustomAttributeCache_m_Colors;
extern CustomAttributesCache t139__CustomAttributeCache_m_SpriteState;
extern CustomAttributesCache t139__CustomAttributeCache_m_AnimationTriggers;
extern CustomAttributesCache t139__CustomAttributeCache_m_Interactable;
extern CustomAttributesCache t139__CustomAttributeCache_m_TargetGraphic;
extern CustomAttributesCache t139__CustomAttributeCache_U3CisPointerInsideU3Ek__BackingField;
extern CustomAttributesCache t139__CustomAttributeCache_U3CisPointerDownU3Ek__BackingField;
extern CustomAttributesCache t139__CustomAttributeCache_U3ChasSelectionU3Ek__BackingField;
extern CustomAttributesCache t139__CustomAttributeCache_m848;
extern CustomAttributesCache t139__CustomAttributeCache_m849;
extern CustomAttributesCache t139__CustomAttributeCache_m850;
extern CustomAttributesCache t139__CustomAttributeCache_m851;
extern CustomAttributesCache t139__CustomAttributeCache_m852;
extern CustomAttributesCache t139__CustomAttributeCache_m853;
extern CustomAttributesCache t139__CustomAttributeCache_m879;
TypeInfo t2638_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Selectable[]", "UnityEngine.UI", t2638_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t139_TI, t2638_ITIs, t2638_VT, &EmptyCustomAttributesCache, &t2638_TI, &t2638_0_0_0, &t2638_1_0_0, t2638_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t139 *), -1, sizeof(t2638_SFs), 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, true, false, false, 0, 0, 0, 0, 0, 216, 45, 49};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3832_TI;



// Metadata Definition UnityEngine.UI.CanvasUpdate[]
static MethodInfo* t3832_MIs[] =
{
	NULL
};
extern MethodInfo m20708_MI;
extern MethodInfo m20709_MI;
extern MethodInfo m20710_MI;
extern MethodInfo m20711_MI;
extern MethodInfo m20712_MI;
extern MethodInfo m20713_MI;
extern MethodInfo m20707_MI;
extern MethodInfo m20715_MI;
extern MethodInfo m20716_MI;
static MethodInfo* t3832_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20708_MI,
	&m5907_MI,
	&m20709_MI,
	&m20710_MI,
	&m20711_MI,
	&m20712_MI,
	&m20713_MI,
	&m5908_MI,
	&m20707_MI,
	&m20715_MI,
	&m20716_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5292_TI;
extern TypeInfo t5293_TI;
extern TypeInfo t5294_TI;
static TypeInfo* t3832_ITIs[] = 
{
	&t5292_TI,
	&t5293_TI,
	&t5294_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3832_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5292_TI, 21},
	{ &t5293_TI, 28},
	{ &t5294_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3832_0_0_0;
extern Il2CppType t3832_1_0_0;
#include "t140.h"
extern TypeInfo t140_TI;
TypeInfo t3832_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "CanvasUpdate[]", "UnityEngine.UI", t3832_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t140_TI, t3832_ITIs, t3832_VT, &EmptyCustomAttributesCache, &t44_TI, &t3832_0_0_0, &t3832_1_0_0, t3832_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2419_TI;



// Metadata Definition System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
static MethodInfo* t2419_MIs[] =
{
	NULL
};
extern MethodInfo m20719_MI;
extern MethodInfo m20720_MI;
extern MethodInfo m20721_MI;
extern MethodInfo m20722_MI;
extern MethodInfo m20723_MI;
extern MethodInfo m20724_MI;
extern MethodInfo m20718_MI;
extern MethodInfo m20726_MI;
extern MethodInfo m20727_MI;
static MethodInfo* t2419_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20719_MI,
	&m5907_MI,
	&m20720_MI,
	&m20721_MI,
	&m20722_MI,
	&m20723_MI,
	&m20724_MI,
	&m5908_MI,
	&m20718_MI,
	&m20726_MI,
	&m20727_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5295_TI;
extern TypeInfo t5296_TI;
extern TypeInfo t5297_TI;
static TypeInfo* t2419_ITIs[] = 
{
	&t5295_TI,
	&t5296_TI,
	&t5297_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2419_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5295_TI, 21},
	{ &t5296_TI, 28},
	{ &t5297_TI, 33},
	{ &t5116_TI, 34},
	{ &t5117_TI, 41},
	{ &t5118_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2419_0_0_0;
extern Il2CppType t2419_1_0_0;
#include "t2420.h"
extern TypeInfo t2420_TI;
extern CustomAttributesCache t1259__CustomAttributeCache;
TypeInfo t2419_TI = 
{
	&g_mscorlib_dll_Image, NULL, "KeyValuePair`2[]", "System.Collections.Generic", t2419_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t2420_TI, t2419_ITIs, t2419_VT, &EmptyCustomAttributesCache, &t2419_TI, &t2419_0_0_0, &t2419_1_0_0, t2419_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t2420 ), -1, 0, 0, -1, 1057033, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2413_TI;



// Metadata Definition UnityEngine.UI.ICanvasElement[]
static MethodInfo* t2413_MIs[] =
{
	NULL
};
extern MethodInfo m20737_MI;
extern MethodInfo m20738_MI;
extern MethodInfo m20739_MI;
extern MethodInfo m20740_MI;
extern MethodInfo m20741_MI;
extern MethodInfo m20742_MI;
extern MethodInfo m20736_MI;
extern MethodInfo m20744_MI;
extern MethodInfo m20745_MI;
static MethodInfo* t2413_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20737_MI,
	&m5907_MI,
	&m20738_MI,
	&m20739_MI,
	&m20740_MI,
	&m20741_MI,
	&m20742_MI,
	&m5908_MI,
	&m20736_MI,
	&m20744_MI,
	&m20745_MI,
};
extern TypeInfo t2430_TI;
extern TypeInfo t2434_TI;
extern TypeInfo t2431_TI;
static TypeInfo* t2413_ITIs[] = 
{
	&t2430_TI,
	&t2434_TI,
	&t2431_TI,
};
static Il2CppInterfaceOffsetPair t2413_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2430_TI, 21},
	{ &t2434_TI, 28},
	{ &t2431_TI, 33},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t2413_0_0_0;
extern Il2CppType t2413_1_0_0;
struct t145;
extern TypeInfo t145_TI;
TypeInfo t2413_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "ICanvasElement[]", "UnityEngine.UI", t2413_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t145_TI, t2413_ITIs, t2413_VT, &EmptyCustomAttributesCache, &t2413_TI, &t2413_0_0_0, &t2413_1_0_0, t2413_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2443_TI;



// Metadata Definition System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>[]
static MethodInfo* t2443_MIs[] =
{
	NULL
};
extern MethodInfo m20763_MI;
extern MethodInfo m20764_MI;
extern MethodInfo m20765_MI;
extern MethodInfo m20766_MI;
extern MethodInfo m20767_MI;
extern MethodInfo m20768_MI;
extern MethodInfo m20762_MI;
extern MethodInfo m20770_MI;
extern MethodInfo m20771_MI;
static MethodInfo* t2443_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20763_MI,
	&m5907_MI,
	&m20764_MI,
	&m20765_MI,
	&m20766_MI,
	&m20767_MI,
	&m20768_MI,
	&m5908_MI,
	&m20762_MI,
	&m20770_MI,
	&m20771_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5298_TI;
extern TypeInfo t5299_TI;
extern TypeInfo t5300_TI;
static TypeInfo* t2443_ITIs[] = 
{
	&t5298_TI,
	&t5299_TI,
	&t5300_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2443_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5298_TI, 21},
	{ &t5299_TI, 28},
	{ &t5300_TI, 33},
	{ &t5116_TI, 34},
	{ &t5117_TI, 41},
	{ &t5118_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2443_0_0_0;
extern Il2CppType t2443_1_0_0;
#include "t2444.h"
extern TypeInfo t2444_TI;
extern CustomAttributesCache t1259__CustomAttributeCache;
TypeInfo t2443_TI = 
{
	&g_mscorlib_dll_Image, NULL, "KeyValuePair`2[]", "System.Collections.Generic", t2443_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t2444_TI, t2443_ITIs, t2443_VT, &EmptyCustomAttributesCache, &t2443_TI, &t2443_0_0_0, &t2443_1_0_0, t2443_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t2444 ), -1, 0, 0, -1, 1057033, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2462_TI;



// Metadata Definition System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
static MethodInfo* t2462_MIs[] =
{
	NULL
};
extern MethodInfo m20781_MI;
extern MethodInfo m20782_MI;
extern MethodInfo m20783_MI;
extern MethodInfo m20784_MI;
extern MethodInfo m20785_MI;
extern MethodInfo m20786_MI;
extern MethodInfo m20780_MI;
extern MethodInfo m20788_MI;
extern MethodInfo m20789_MI;
static MethodInfo* t2462_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20781_MI,
	&m5907_MI,
	&m20782_MI,
	&m20783_MI,
	&m20784_MI,
	&m20785_MI,
	&m20786_MI,
	&m5908_MI,
	&m20780_MI,
	&m20788_MI,
	&m20789_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5301_TI;
extern TypeInfo t5302_TI;
extern TypeInfo t5303_TI;
static TypeInfo* t2462_ITIs[] = 
{
	&t5301_TI,
	&t5302_TI,
	&t5303_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2462_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5301_TI, 21},
	{ &t5302_TI, 28},
	{ &t5303_TI, 33},
	{ &t5116_TI, 34},
	{ &t5117_TI, 41},
	{ &t5118_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2462_0_0_0;
extern Il2CppType t2462_1_0_0;
#include "t2465.h"
extern TypeInfo t2465_TI;
extern CustomAttributesCache t1259__CustomAttributeCache;
TypeInfo t2462_TI = 
{
	&g_mscorlib_dll_Image, NULL, "KeyValuePair`2[]", "System.Collections.Generic", t2462_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t2465_TI, t2462_ITIs, t2462_VT, &EmptyCustomAttributesCache, &t2462_TI, &t2462_0_0_0, &t2462_1_0_0, t2462_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t2465 ), -1, 0, 0, -1, 1057033, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2473_TI;



// Metadata Definition UnityEngine.UI.Text[]
static MethodInfo* t2473_MIs[] =
{
	NULL
};
extern MethodInfo m20798_MI;
extern MethodInfo m20799_MI;
extern MethodInfo m20800_MI;
extern MethodInfo m20801_MI;
extern MethodInfo m20802_MI;
extern MethodInfo m20803_MI;
extern MethodInfo m20797_MI;
extern MethodInfo m20805_MI;
extern MethodInfo m20806_MI;
extern MethodInfo m20809_MI;
extern MethodInfo m20810_MI;
extern MethodInfo m20811_MI;
extern MethodInfo m20812_MI;
extern MethodInfo m20813_MI;
extern MethodInfo m20814_MI;
extern MethodInfo m20808_MI;
extern MethodInfo m20816_MI;
extern MethodInfo m20817_MI;
extern MethodInfo m20820_MI;
extern MethodInfo m20821_MI;
extern MethodInfo m20822_MI;
extern MethodInfo m20823_MI;
extern MethodInfo m20824_MI;
extern MethodInfo m20825_MI;
extern MethodInfo m20819_MI;
extern MethodInfo m20827_MI;
extern MethodInfo m20828_MI;
extern MethodInfo m20831_MI;
extern MethodInfo m20832_MI;
extern MethodInfo m20833_MI;
extern MethodInfo m20834_MI;
extern MethodInfo m20835_MI;
extern MethodInfo m20836_MI;
extern MethodInfo m20830_MI;
extern MethodInfo m20838_MI;
extern MethodInfo m20839_MI;
extern MethodInfo m20842_MI;
extern MethodInfo m20843_MI;
extern MethodInfo m20844_MI;
extern MethodInfo m20845_MI;
extern MethodInfo m20846_MI;
extern MethodInfo m20847_MI;
extern MethodInfo m20841_MI;
extern MethodInfo m20849_MI;
extern MethodInfo m20850_MI;
static MethodInfo* t2473_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20798_MI,
	&m5907_MI,
	&m20799_MI,
	&m20800_MI,
	&m20801_MI,
	&m20802_MI,
	&m20803_MI,
	&m5908_MI,
	&m20797_MI,
	&m20805_MI,
	&m20806_MI,
	&m5905_MI,
	&m5906_MI,
	&m20809_MI,
	&m5907_MI,
	&m20810_MI,
	&m20811_MI,
	&m20812_MI,
	&m20813_MI,
	&m20814_MI,
	&m5908_MI,
	&m20808_MI,
	&m20816_MI,
	&m20817_MI,
	&m5905_MI,
	&m5906_MI,
	&m20820_MI,
	&m5907_MI,
	&m20821_MI,
	&m20822_MI,
	&m20823_MI,
	&m20824_MI,
	&m20825_MI,
	&m5908_MI,
	&m20819_MI,
	&m20827_MI,
	&m20828_MI,
	&m5905_MI,
	&m5906_MI,
	&m20831_MI,
	&m5907_MI,
	&m20832_MI,
	&m20833_MI,
	&m20834_MI,
	&m20835_MI,
	&m20836_MI,
	&m5908_MI,
	&m20830_MI,
	&m20838_MI,
	&m20839_MI,
	&m5905_MI,
	&m5906_MI,
	&m20842_MI,
	&m5907_MI,
	&m20843_MI,
	&m20844_MI,
	&m20845_MI,
	&m20846_MI,
	&m20847_MI,
	&m5908_MI,
	&m20841_MI,
	&m20849_MI,
	&m20850_MI,
	&m5905_MI,
	&m5906_MI,
	&m20737_MI,
	&m5907_MI,
	&m20738_MI,
	&m20739_MI,
	&m20740_MI,
	&m20741_MI,
	&m20742_MI,
	&m5908_MI,
	&m20736_MI,
	&m20744_MI,
	&m20745_MI,
	&m5905_MI,
	&m5906_MI,
	&m19777_MI,
	&m5907_MI,
	&m19778_MI,
	&m19779_MI,
	&m19780_MI,
	&m19781_MI,
	&m19782_MI,
	&m5908_MI,
	&m19776_MI,
	&m19784_MI,
	&m19785_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t2475_TI;
extern TypeInfo t2486_TI;
extern TypeInfo t2476_TI;
extern TypeInfo t5304_TI;
extern TypeInfo t5305_TI;
extern TypeInfo t5306_TI;
extern TypeInfo t5307_TI;
extern TypeInfo t5308_TI;
extern TypeInfo t5309_TI;
extern TypeInfo t5310_TI;
extern TypeInfo t5311_TI;
extern TypeInfo t5312_TI;
extern TypeInfo t363_TI;
extern TypeInfo t171_TI;
extern TypeInfo t2547_TI;
static TypeInfo* t2473_ITIs[] = 
{
	&t2475_TI,
	&t2486_TI,
	&t2476_TI,
	&t5304_TI,
	&t5305_TI,
	&t5306_TI,
	&t5307_TI,
	&t5308_TI,
	&t5309_TI,
	&t5310_TI,
	&t5311_TI,
	&t5312_TI,
	&t363_TI,
	&t171_TI,
	&t2547_TI,
	&t2430_TI,
	&t2434_TI,
	&t2431_TI,
	&t5122_TI,
	&t5123_TI,
	&t5124_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2473_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2475_TI, 21},
	{ &t2486_TI, 28},
	{ &t2476_TI, 33},
	{ &t5304_TI, 34},
	{ &t5305_TI, 41},
	{ &t5306_TI, 46},
	{ &t5307_TI, 47},
	{ &t5308_TI, 54},
	{ &t5309_TI, 59},
	{ &t5310_TI, 60},
	{ &t5311_TI, 67},
	{ &t5312_TI, 72},
	{ &t363_TI, 73},
	{ &t171_TI, 80},
	{ &t2547_TI, 85},
	{ &t2430_TI, 86},
	{ &t2434_TI, 93},
	{ &t2431_TI, 98},
	{ &t5122_TI, 99},
	{ &t5123_TI, 106},
	{ &t5124_TI, 111},
	{ &t5059_TI, 112},
	{ &t5060_TI, 119},
	{ &t5061_TI, 124},
	{ &t5062_TI, 125},
	{ &t5063_TI, 132},
	{ &t5064_TI, 137},
	{ &t2240_TI, 138},
	{ &t2245_TI, 145},
	{ &t2241_TI, 150},
	{ &t5065_TI, 151},
	{ &t5066_TI, 158},
	{ &t5067_TI, 163},
	{ &t2182_TI, 164},
	{ &t2186_TI, 171},
	{ &t2183_TI, 176},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t2473_0_0_0;
extern Il2CppType t2473_1_0_0;
struct t15;
extern TypeInfo t15_TI;
extern CustomAttributesCache t15__CustomAttributeCache;
extern CustomAttributesCache t15__CustomAttributeCache_m_FontData;
extern CustomAttributesCache t15__CustomAttributeCache_m_Text;
TypeInfo t2473_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Text[]", "UnityEngine.UI", t2473_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t15_TI, t2473_ITIs, t2473_VT, &EmptyCustomAttributesCache, &t2473_TI, &t2473_0_0_0, &t2473_1_0_0, t2473_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t15 *), -1, sizeof(t2473_SFs), 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, true, false, false, 0, 0, 0, 0, 0, 177, 36, 40};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3833_TI;



// Metadata Definition UnityEngine.UI.ILayoutElement[]
static MethodInfo* t3833_MIs[] =
{
	NULL
};
static MethodInfo* t3833_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20809_MI,
	&m5907_MI,
	&m20810_MI,
	&m20811_MI,
	&m20812_MI,
	&m20813_MI,
	&m20814_MI,
	&m5908_MI,
	&m20808_MI,
	&m20816_MI,
	&m20817_MI,
};
static TypeInfo* t3833_ITIs[] = 
{
	&t5304_TI,
	&t5305_TI,
	&t5306_TI,
};
static Il2CppInterfaceOffsetPair t3833_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5304_TI, 21},
	{ &t5305_TI, 28},
	{ &t5306_TI, 33},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3833_0_0_0;
extern Il2CppType t3833_1_0_0;
struct t271;
extern TypeInfo t271_TI;
TypeInfo t3833_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "ILayoutElement[]", "UnityEngine.UI", t3833_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t271_TI, t3833_ITIs, t3833_VT, &EmptyCustomAttributesCache, &t3833_TI, &t3833_0_0_0, &t3833_1_0_0, t3833_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3834_TI;



// Metadata Definition UnityEngine.UI.MaskableGraphic[]
static MethodInfo* t3834_MIs[] =
{
	NULL
};
static MethodInfo* t3834_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20820_MI,
	&m5907_MI,
	&m20821_MI,
	&m20822_MI,
	&m20823_MI,
	&m20824_MI,
	&m20825_MI,
	&m5908_MI,
	&m20819_MI,
	&m20827_MI,
	&m20828_MI,
	&m5905_MI,
	&m5906_MI,
	&m20831_MI,
	&m5907_MI,
	&m20832_MI,
	&m20833_MI,
	&m20834_MI,
	&m20835_MI,
	&m20836_MI,
	&m5908_MI,
	&m20830_MI,
	&m20838_MI,
	&m20839_MI,
	&m5905_MI,
	&m5906_MI,
	&m20842_MI,
	&m5907_MI,
	&m20843_MI,
	&m20844_MI,
	&m20845_MI,
	&m20846_MI,
	&m20847_MI,
	&m5908_MI,
	&m20841_MI,
	&m20849_MI,
	&m20850_MI,
	&m5905_MI,
	&m5906_MI,
	&m20737_MI,
	&m5907_MI,
	&m20738_MI,
	&m20739_MI,
	&m20740_MI,
	&m20741_MI,
	&m20742_MI,
	&m5908_MI,
	&m20736_MI,
	&m20744_MI,
	&m20745_MI,
	&m5905_MI,
	&m5906_MI,
	&m19777_MI,
	&m5907_MI,
	&m19778_MI,
	&m19779_MI,
	&m19780_MI,
	&m19781_MI,
	&m19782_MI,
	&m5908_MI,
	&m19776_MI,
	&m19784_MI,
	&m19785_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
static TypeInfo* t3834_ITIs[] = 
{
	&t5307_TI,
	&t5308_TI,
	&t5309_TI,
	&t5310_TI,
	&t5311_TI,
	&t5312_TI,
	&t363_TI,
	&t171_TI,
	&t2547_TI,
	&t2430_TI,
	&t2434_TI,
	&t2431_TI,
	&t5122_TI,
	&t5123_TI,
	&t5124_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3834_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5307_TI, 21},
	{ &t5308_TI, 28},
	{ &t5309_TI, 33},
	{ &t5310_TI, 34},
	{ &t5311_TI, 41},
	{ &t5312_TI, 46},
	{ &t363_TI, 47},
	{ &t171_TI, 54},
	{ &t2547_TI, 59},
	{ &t2430_TI, 60},
	{ &t2434_TI, 67},
	{ &t2431_TI, 72},
	{ &t5122_TI, 73},
	{ &t5123_TI, 80},
	{ &t5124_TI, 85},
	{ &t5059_TI, 86},
	{ &t5060_TI, 93},
	{ &t5061_TI, 98},
	{ &t5062_TI, 99},
	{ &t5063_TI, 106},
	{ &t5064_TI, 111},
	{ &t2240_TI, 112},
	{ &t2245_TI, 119},
	{ &t2241_TI, 124},
	{ &t5065_TI, 125},
	{ &t5066_TI, 132},
	{ &t5067_TI, 137},
	{ &t2182_TI, 138},
	{ &t2186_TI, 145},
	{ &t2183_TI, 150},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3834_0_0_0;
extern Il2CppType t3834_1_0_0;
struct t182;
extern TypeInfo t182_TI;
TypeInfo t3834_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "MaskableGraphic[]", "UnityEngine.UI", t3834_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t182_TI, t3834_ITIs, t3834_VT, &EmptyCustomAttributesCache, &t3834_TI, &t3834_0_0_0, &t3834_1_0_0, t3834_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t182 *), -1, 0, 0, -1, 1048705, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 151, 30, 34};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3835_TI;



// Metadata Definition UnityEngine.UI.IMaskable[]
static MethodInfo* t3835_MIs[] =
{
	NULL
};
static MethodInfo* t3835_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20831_MI,
	&m5907_MI,
	&m20832_MI,
	&m20833_MI,
	&m20834_MI,
	&m20835_MI,
	&m20836_MI,
	&m5908_MI,
	&m20830_MI,
	&m20838_MI,
	&m20839_MI,
};
static TypeInfo* t3835_ITIs[] = 
{
	&t5310_TI,
	&t5311_TI,
	&t5312_TI,
};
static Il2CppInterfaceOffsetPair t3835_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5310_TI, 21},
	{ &t5311_TI, 28},
	{ &t5312_TI, 33},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3835_0_0_0;
extern Il2CppType t3835_1_0_0;
struct t369;
extern TypeInfo t369_TI;
TypeInfo t3835_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "IMaskable[]", "UnityEngine.UI", t3835_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t369_TI, t3835_ITIs, t3835_VT, &EmptyCustomAttributesCache, &t3835_TI, &t3835_0_0_0, &t3835_1_0_0, t3835_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
