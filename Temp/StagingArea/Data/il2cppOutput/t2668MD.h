﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2668;
struct t29;
struct t20;
#include "t207.h"

 void m14466 (t2668 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m14467 (t2668 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m14468 (t2668 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m14469 (t2668 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m14470 (t2668 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
