﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3104;
struct t29;
struct t553;
struct t3112;
struct t20;
struct t136;
struct t3101;
#include "t552.h"
#include "t3113.h"

 void m17179 (t3104 * __this, t553 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17180 (t3104 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17181 (t3104 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17182 (t3104 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17183 (t3104 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m17184 (t3104 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17185 (t3104 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17186 (t3104 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17187 (t3104 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17188 (t3104 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17189 (t3104 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17190 (t3104 * __this, t3101* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t3113  m17191 (t3104 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17192 (t3104 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
