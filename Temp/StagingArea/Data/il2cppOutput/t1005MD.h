﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1005;
struct t731;
struct t809;
struct t7;

 void m4570 (t1005 * __this, t809 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4571 (t1005 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4572 (t1005 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t731 * m4573 (t1005 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4574 (t1005 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
