﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3487;
struct t29;
struct t20;
#include "t1628.h"

 void m19379 (t3487 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m19380 (t3487 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m19381 (t3487 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19382 (t3487 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m19383 (t3487 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
