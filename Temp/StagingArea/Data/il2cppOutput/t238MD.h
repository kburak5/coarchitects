﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t238;
#include "t132.h"
#include "t17.h"
#include "t238.h"

 bool m2776 (t238 * __this, t132  p0, t132  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2777 (t238 * __this, t17  p0, t17  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2778 (t238 * __this, t238  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
