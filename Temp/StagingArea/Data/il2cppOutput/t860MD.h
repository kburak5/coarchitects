﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t860;
struct t860_marshaled;

 int32_t m3653 (t860 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3654 (t860 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3655 (t860 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3656 (t860 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
void t860_marshal(const t860& unmarshaled, t860_marshaled& marshaled);
void t860_marshal_back(const t860_marshaled& marshaled, t860& unmarshaled);
void t860_marshal_cleanup(t860_marshaled& marshaled);
