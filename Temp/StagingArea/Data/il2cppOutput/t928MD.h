﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t928;
struct t20;
#include "t35.h"
#include "t927.h"

 void m6077 (t29 * __this, t20 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4025 (t29 * __this, t20 * p0, t927  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6078 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
