﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3218;
struct t29;
struct t20;
#include "t814.h"

 void m17890 (t3218 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17891 (t3218 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17892 (t3218 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17893 (t3218 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17894 (t3218 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
