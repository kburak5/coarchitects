﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t180;
struct t157;
#include "t164.h"
#include "t183.h"

 t164  m1652 (t180 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1649 (t180 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t157 * m1646 (t180 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t164  m1678 (t180 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t183  m1647 (t180 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
