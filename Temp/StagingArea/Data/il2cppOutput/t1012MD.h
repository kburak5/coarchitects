﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1012;
struct t781;
struct t7;

 void m4593 (t1012 * __this, t7* p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4594 (t1012 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4595 (t1012 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4596 (t1012 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4597 (t1012 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4598 (t1012 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4599 (t1012 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
