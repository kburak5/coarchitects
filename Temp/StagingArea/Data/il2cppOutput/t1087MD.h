﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1087;
struct t1087_marshaled;

void t1087_marshal(const t1087& unmarshaled, t1087_marshaled& marshaled);
void t1087_marshal_back(const t1087_marshaled& marshaled, t1087& unmarshaled);
void t1087_marshal_cleanup(t1087_marshaled& marshaled);
