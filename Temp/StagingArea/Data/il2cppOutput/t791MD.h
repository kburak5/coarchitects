﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t791;
struct t792;
struct t786;
struct t777;
struct t776;
struct t7;
struct t779;
struct t780;
struct t781;
struct t793;
struct t292;
#include "t465.h"
#include "t794.h"
#include "t795.h"

 void m3303 (t791 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3304 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t792 * m3305 (t791 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t786 * m3306 (t791 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m3307 (t791 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m3308 (t791 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t777 * m3309 (t791 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t776 * m3310 (t791 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3311 (t791 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t779 * m3312 (t791 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t786 * m3313 (t791 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3314 (t791 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3315 (t791 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3316 (t791 * __this, int32_t p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t793 * m3317 (t791 * __this, t781* p0, t793 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3318 (t791 * __this, t793 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3319 (t791 * __this, t781* p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3320 (t791 * __this, t781* p0, t7* p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3321 (t791 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3322 (t791 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3323 (t791 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3324 (t29 * __this, t292 * p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3325 (t791 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t780 * m3326 (t791 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
