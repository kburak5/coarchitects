﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t626;
struct t1094;
struct t29;
struct t42;
struct t7;
#include "t465.h"
#include "t1126.h"
#include "t923.h"

 bool m5497 (uint16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m5498 (uint16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m5499 (uint16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m5500 (uint16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m5501 (uint16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m5502 (uint16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m5503 (uint16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5504 (uint16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m5505 (uint16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int8_t m5506 (uint16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m5507 (uint16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m5508 (uint16_t* __this, t42 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m5509 (uint16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m5510 (uint16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m5511 (uint16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5512 (uint16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5513 (uint16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5514 (uint16_t* __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5515 (uint16_t* __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5516 (uint16_t* __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m5517 (t29 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m5518 (t29 * __this, t7* p0, int32_t p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5519 (t29 * __this, t7* p0, uint16_t* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5520 (t29 * __this, t7* p0, int32_t p1, t29 * p2, uint16_t* p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5521 (uint16_t* __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5522 (uint16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5523 (uint16_t* __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5524 (uint16_t* __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
