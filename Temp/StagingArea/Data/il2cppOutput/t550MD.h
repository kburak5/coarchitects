﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t550;
struct t229;
#include "t515.h"

 void m2762 (t550 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2763 (t550 * __this, t229 * p0, t515  p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2764 (t550 * __this, t229 * p0, t515  p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2765 (t550 * __this, t229 * p0, t515  p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2766 (t550 * __this, t229 * p0, t515  p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2767 (t550 * __this, t229 * p0, t515  p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2768 (t550 * __this, t229 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2769 (t550 * __this, t229 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
