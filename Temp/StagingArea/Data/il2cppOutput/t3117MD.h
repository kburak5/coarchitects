﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3117;
struct t29;
struct t204;
struct t204_marshaled;

#include "t2001MD.h"
#define m17228(__this, method) (void)m10703_gshared((t2001 *)__this, method)
#define m17229(__this, method) (void)m10704_gshared((t29 *)__this, method)
#define m17230(__this, p0, method) (int32_t)m10705_gshared((t2001 *)__this, (t29 *)p0, method)
#define m17231(__this, p0, p1, method) (bool)m10706_gshared((t2001 *)__this, (t29 *)p0, (t29 *)p1, method)
#define m17232(__this, method) (t3117 *)m10707_gshared((t29 *)__this, method)
