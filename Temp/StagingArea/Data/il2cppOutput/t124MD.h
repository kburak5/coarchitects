﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t124;
struct t24;
struct t6;
struct t56;
#include "t126.h"
#include "t127.h"

 void m333 (t124 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t24 * m334 (t124 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m335 (t124 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m336 (t124 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t126  m337 (t124 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m338 (t124 * __this, t126  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m339 (t124 * __this, t6 * p0, t56 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m340 (t29 * __this, t127  p0, t127  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
