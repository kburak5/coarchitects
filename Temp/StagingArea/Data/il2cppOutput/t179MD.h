﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t179;
struct t180;
struct t162;
struct t163;
struct t181;
struct t24;
#include "t172.h"
#include "t173.h"
#include "t183.h"
#include "t184.h"
#include "t17.h"
#include "t164.h"

 void m507 (t179 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m508 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t180 * m509 (t179 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m510 (t179 * __this, t180 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t180 * m511 (t179 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m512 (t179 * __this, t180 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m513 (t179 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m514 (t179 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m515 (t179 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m516 (t179 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m517 (t179 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m518 (t179 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m519 (t179 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m520 (t179 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m521 (t179 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m522 (t179 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m523 (t179 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m524 (t179 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m525 (t179 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m526 (t179 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m527 (t179 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m528 (t179 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t162 * m529 (t179 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m530 (t179 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m531 (t179 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m532 (t179 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m533 (t179 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t183  m534 (t179 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m535 (t179 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m536 (t179 * __this, t163 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m537 (t179 * __this, t163 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m538 (t179 * __this, t163 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m539 (t179 * __this, t163 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m540 (t179 * __this, t163 * p0, t184  p1, t17  p2, t17  p3, t17  p4, t17  p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t183  m541 (t179 * __this, t183  p0, t164  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m542 (t179 * __this, t163 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m543 (t29 * __this, t181* p0, t181* p1, float p2, bool p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m544 (t29 * __this, t181* p0, float p1, float p2, bool p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m545 (t179 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m546 (t179 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m547 (t179 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m548 (t179 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m549 (t179 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m550 (t179 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m551 (t179 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m552 (t179 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m553 (t179 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m554 (t179 * __this, t17  p0, t24 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m555 (t179 * __this, t17  p0, t164  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
