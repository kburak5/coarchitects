﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1195;
struct t29;
struct t1196;
struct t66;
struct t67;
#include "t35.h"
#include "t1197.h"

 void m9616 (t1195 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9617 (t1195 * __this, t1196 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m9618 (t1195 * __this, t1196 * p0, int32_t p1, t67 * p2, t29 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9619 (t1195 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
