﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1307;
struct t7;
struct t733;
#include "t735.h"

 void m7069 (t1307 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7070 (t1307 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7071 (t1307 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7072 (t1307 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7073 (t1307 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7074 (t1307 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7075 (t1307 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
