﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1032;
struct t1034;
struct t1030;
struct t1035;
struct t1036;
struct t781;
#include "t1037.h"

 void m4681 (t1032 * __this, t1034 * p0, t1030 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1035 * m4682 (t1032 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4683 (t1032 * __this, t1036 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1035 * m4684 (t1032 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1035 * m4685 (t1032 * __this, uint8_t p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
