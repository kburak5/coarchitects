﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1178;
struct t29;

 void m6093 (t1178 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6094 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6095 (t1178 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
