﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2230;
struct t29;
struct t2229;

 void m11075_gshared (t2230 * __this, t2229 * p0, MethodInfo* method);
#define m11075(__this, p0, method) (void)m11075_gshared((t2230 *)__this, (t2229 *)p0, method)
 t29 * m11076_gshared (t2230 * __this, MethodInfo* method);
#define m11076(__this, method) (t29 *)m11076_gshared((t2230 *)__this, method)
 void m11077_gshared (t2230 * __this, MethodInfo* method);
#define m11077(__this, method) (void)m11077_gshared((t2230 *)__this, method)
 bool m11078_gshared (t2230 * __this, MethodInfo* method);
#define m11078(__this, method) (bool)m11078_gshared((t2230 *)__this, method)
 t29 * m11079_gshared (t2230 * __this, MethodInfo* method);
#define m11079(__this, method) (t29 *)m11079_gshared((t2230 *)__this, method)
