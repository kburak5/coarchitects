﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1191;
struct t7;
struct t633;
struct t29;
#include "t21.h"

 void m6959 (t1191 * __this, t633 * p0, int32_t p1, void* p2, bool p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6960 (t1191 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6961 (t1191 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6962 (t1191 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6963 (t1191 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6964 (t1191 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m6965 (t1191 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m6966 (t1191 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6967 (t1191 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
