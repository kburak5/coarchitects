﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3458;
struct t29;
struct t20;
#include "t1117.h"

 void m19160 (t3458 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m19161 (t3458 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m19162 (t3458 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19163 (t3458 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m19164 (t3458 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
