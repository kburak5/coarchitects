﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1310;
struct t1034;
struct t943;
struct t7;
struct t200;

 void m7165 (t1310 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7166 (t1310 * __this, t1034 * p0, t943 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7167 (t1310 * __this, t1034 * p0, t943 * p1, bool p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7168 (t1310 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7169 (t1310 * __this, t7* p0, t943 * p1, bool p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7170 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7171 (t1310 * __this, t1034 * p0, t943 * p1, bool p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7172 (t1310 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7173 (t1310 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7174 (t1310 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7175 (t1310 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7176 (t1310 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7177 (t1310 * __this, t200* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7178 (t1310 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7179 (t1310 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7180 (t1310 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
