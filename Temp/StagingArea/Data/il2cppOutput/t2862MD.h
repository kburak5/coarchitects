﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2862;
struct t29;
struct t20;
#include "t2859.h"

 void m15545 (t2862 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15546 (t2862 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15547 (t2862 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15548 (t2862 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2859  m15549 (t2862 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
