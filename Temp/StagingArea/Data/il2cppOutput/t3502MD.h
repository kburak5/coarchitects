﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3502;
struct t29;
struct t20;
#include "t939.h"

 void m19452 (t3502 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m19453 (t3502 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m19454 (t3502 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19455 (t3502 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m19456 (t3502 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
