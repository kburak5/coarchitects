﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3119;
struct t29;
#include "t552.h"

 void m17236 (t3119 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17237 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17238 (t3119 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17239 (t3119 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t3119 * m17240 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
