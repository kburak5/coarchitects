﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3386;
struct t29;
struct t20;
#include "t1367.h"

 void m18802 (t3386 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18803 (t3386 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18804 (t3386 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18805 (t3386 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18806 (t3386 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
