﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1330;
struct t1240;
struct t7;
struct t200;

 void m7218 (t1330 * __this, t1240 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7219 (t1330 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7220 (t1330 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7221 (t1330 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7222 (t1330 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7223 (t1330 * __this, t200* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
