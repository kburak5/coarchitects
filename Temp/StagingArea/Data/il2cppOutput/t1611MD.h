﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1611;
struct t7;
struct t929;
struct t1359;
struct t1425;

 t7* m8835 (t1611 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1611 * m8836 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1611 * m8837 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t929 * m8838 (t1611 * __this, t7* p0, t1359 * p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t929 * m8839 (t1611 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t929 * m8840 (t1611 * __this, t7* p0, t1359 * p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1425 * m8841 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1425 * m8842 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8843 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8844 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8845 (t1611 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
