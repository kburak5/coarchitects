﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1663;
struct t7;
struct t1125;
struct t292;

 void m9383 (t1663 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9384 (t29 * __this, t7* p0, bool* p1, bool p2, int32_t* p3, int32_t* p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1663 * m9385 (t29 * __this, t7* p0, int32_t p1, int32_t p2, t1125 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m9386 (t1663 * __this, t7* p0, int32_t p1, int32_t p2, t1125 * p3, bool p4, t292 * p5, t292 * p6, t292 * p7, MethodInfo* method) IL2CPP_METHOD_ATTR;
