﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2458;
struct t148;
struct t350;
struct t7;

#include "t2465MD.h"
#define m12977(__this, p0, p1, method) (void)m12917_gshared((t2465 *)__this, (t29 *)p0, (t29 *)p1, method)
#define m12978(__this, method) (t148 *)m12918_gshared((t2465 *)__this, method)
#define m12979(__this, p0, method) (void)m12919_gshared((t2465 *)__this, (t29 *)p0, method)
#define m12980(__this, method) (t350 *)m12920_gshared((t2465 *)__this, method)
#define m12981(__this, p0, method) (void)m12921_gshared((t2465 *)__this, (t29 *)p0, method)
#define m12982(__this, method) (t7*)m12922_gshared((t2465 *)__this, method)
