﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2865;
struct t29;
struct t469;
struct t472;

 void m15569 (t2865 * __this, t472 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15570 (t2865 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15571 (t2865 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15572 (t2865 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t469 * m15573 (t2865 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
