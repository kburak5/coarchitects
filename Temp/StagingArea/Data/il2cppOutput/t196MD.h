﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t196;
struct t29;

 void m562 (t196 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m563 (t196 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m564 (t196 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m565 (t196 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m566 (t196 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m567 (t196 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
