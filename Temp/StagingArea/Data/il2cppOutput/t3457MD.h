﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3457;
struct t29;
struct t20;
#include "t1099.h"

 void m19155 (t3457 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m19156 (t3457 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m19157 (t3457 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19158 (t3457 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m19159 (t3457 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
