﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1113;
struct t781;
struct t7;

 void m8182 (t1113 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8183 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1113 * m5200 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1113 * m8184 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8185 (t29 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8186 (t29 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m8187 (t1113 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8188 (t1113 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
