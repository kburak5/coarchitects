﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1661;
struct t7;
struct t733;
#include "t735.h"

 void m9374 (t1661 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9375 (t1661 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9376 (t1661 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
