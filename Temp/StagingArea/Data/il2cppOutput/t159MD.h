﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t159;
struct t156;
struct t162;
struct t163;
struct t29;
struct t201;
#include "t132.h"

 void m1593 (t159 * __this, t132  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2721 (t29 * __this, t159 * p0, t132 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t132  m1591 (t159 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2005 (t159 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1578 (t159 * __this, t156 * p0, t162 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1576 (t159 * __this, t163 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2722 (t159 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1737 (t159 * __this, t201* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2723 (t159 * __this, t201* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1569 (t159 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1552 (t159 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
