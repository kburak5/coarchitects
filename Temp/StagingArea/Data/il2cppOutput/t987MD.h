﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t987;
struct t781;

 void m4443 (t987 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4444 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4445 (t987 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4446 (t987 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
