﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1582;
struct t1581;
struct t781;

 void m8574 (t1582 * __this, t1581 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8575 (t1582 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8576 (t1582 * __this, t781* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m8577 (t1582 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8578 (t1582 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
