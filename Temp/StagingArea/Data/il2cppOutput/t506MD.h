﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t506;
struct t29;
struct t507;
struct t2981;
struct t20;
struct t136;
struct t2982;
struct t2983;
struct t2984;
struct t2980;
struct t2985;
struct t2986;
#include "t2987.h"

#include "t294MD.h"
#define m2901(__this, method) (void)m10537_gshared((t294 *)__this, method)
#define m16247(__this, p0, method) (void)m10539_gshared((t294 *)__this, (int32_t)p0, method)
#define m16248(__this, method) (void)m10541_gshared((t29 *)__this, method)
#define m16249(__this, method) (t29*)m10543_gshared((t294 *)__this, method)
#define m16250(__this, p0, p1, method) (void)m10545_gshared((t294 *)__this, (t20 *)p0, (int32_t)p1, method)
#define m16251(__this, method) (t29 *)m10547_gshared((t294 *)__this, method)
#define m16252(__this, p0, method) (int32_t)m10549_gshared((t294 *)__this, (t29 *)p0, method)
#define m16253(__this, p0, method) (bool)m10551_gshared((t294 *)__this, (t29 *)p0, method)
#define m16254(__this, p0, method) (int32_t)m10553_gshared((t294 *)__this, (t29 *)p0, method)
#define m16255(__this, p0, p1, method) (void)m10555_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m16256(__this, p0, method) (void)m10557_gshared((t294 *)__this, (t29 *)p0, method)
#define m16257(__this, method) (bool)m10559_gshared((t294 *)__this, method)
#define m16258(__this, method) (bool)m10561_gshared((t294 *)__this, method)
#define m16259(__this, method) (t29 *)m10563_gshared((t294 *)__this, method)
#define m16260(__this, method) (bool)m10565_gshared((t294 *)__this, method)
#define m16261(__this, method) (bool)m10567_gshared((t294 *)__this, method)
#define m16262(__this, p0, method) (t29 *)m10569_gshared((t294 *)__this, (int32_t)p0, method)
#define m16263(__this, p0, p1, method) (void)m10571_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m16264(__this, p0, method) (void)m10573_gshared((t294 *)__this, (t29 *)p0, method)
#define m16265(__this, p0, method) (void)m10575_gshared((t294 *)__this, (int32_t)p0, method)
#define m16266(__this, p0, method) (void)m10577_gshared((t294 *)__this, (t29*)p0, method)
#define m16267(__this, p0, method) (void)m10579_gshared((t294 *)__this, (t29*)p0, method)
#define m16268(__this, p0, method) (void)m10581_gshared((t294 *)__this, (t29*)p0, method)
#define m16269(__this, method) (t2984 *)m10583_gshared((t294 *)__this, method)
#define m16270(__this, method) (void)m10585_gshared((t294 *)__this, method)
#define m16271(__this, p0, method) (bool)m10587_gshared((t294 *)__this, (t29 *)p0, method)
#define m16272(__this, p0, p1, method) (void)m10589_gshared((t294 *)__this, (t316*)p0, (int32_t)p1, method)
#define m16273(__this, p0, method) (t507 *)m10591_gshared((t294 *)__this, (t2180 *)p0, method)
#define m16274(__this, p0, method) (void)m10593_gshared((t29 *)__this, (t2180 *)p0, method)
#define m16275(__this, p0, p1, p2, method) (int32_t)m10595_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, (t2180 *)p2, method)
 t2987  m16276 (t506 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
#define m16277(__this, p0, method) (int32_t)m10598_gshared((t294 *)__this, (t29 *)p0, method)
#define m16278(__this, p0, p1, method) (void)m10600_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, method)
#define m16279(__this, p0, method) (void)m10602_gshared((t294 *)__this, (int32_t)p0, method)
#define m16280(__this, p0, p1, method) (void)m10604_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m16281(__this, p0, method) (void)m10606_gshared((t294 *)__this, (t29*)p0, method)
#define m16282(__this, p0, method) (bool)m10608_gshared((t294 *)__this, (t29 *)p0, method)
#define m16283(__this, p0, method) (int32_t)m10610_gshared((t294 *)__this, (t2180 *)p0, method)
#define m16284(__this, p0, method) (void)m10611_gshared((t294 *)__this, (int32_t)p0, method)
#define m16285(__this, method) (void)m10613_gshared((t294 *)__this, method)
#define m16286(__this, method) (void)m10615_gshared((t294 *)__this, method)
#define m16287(__this, p0, method) (void)m10617_gshared((t294 *)__this, (t2181 *)p0, method)
#define m16288(__this, method) (t2980*)m10619_gshared((t294 *)__this, method)
#define m16289(__this, method) (void)m10621_gshared((t294 *)__this, method)
#define m16290(__this, method) (int32_t)m10623_gshared((t294 *)__this, method)
#define m16291(__this, p0, method) (void)m10625_gshared((t294 *)__this, (int32_t)p0, method)
#define m16292(__this, method) (int32_t)m10626_gshared((t294 *)__this, method)
#define m16293(__this, p0, method) (t507 *)m10627_gshared((t294 *)__this, (int32_t)p0, method)
#define m16294(__this, p0, p1, method) (void)m10629_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
