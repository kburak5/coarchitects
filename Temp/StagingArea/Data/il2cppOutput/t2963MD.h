﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2963;
struct t29;
struct t20;
#include "t385.h"

 void m16186 (t2963 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m16187 (t2963 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16188 (t2963 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16189 (t2963 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16190 (t2963 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
