﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1238;
struct t7;

 void m6538 (t1238 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6539 (t1238 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6540 (t1238 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6541 (t1238 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6542 (t1238 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6543 (t1238 * __this, t7* p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
