﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t59;
struct t2314;
struct t557;
struct t7;
struct t29;
struct t556;
struct t53;

#include "t2315MD.h"
#define m1320(__this, method) (void)m11776_gshared((t2315 *)__this, method)
#define m11777(__this, p0, method) (void)m11778_gshared((t2315 *)__this, (t2120 *)p0, method)
#define m11779(__this, p0, method) (void)m11780_gshared((t2315 *)__this, (t2120 *)p0, method)
#define m1326(__this, p0, p1, method) (t557 *)m11781_gshared((t2315 *)__this, (t7*)p0, (t29 *)p1, method)
#define m1327(__this, p0, p1, method) (t556 *)m11782_gshared((t2315 *)__this, (t29 *)p0, (t557 *)p1, method)
#define m1328(__this, p0, method) (t556 *)m11783_gshared((t29 *)__this, (t2120 *)p0, method)
#define m1335(__this, p0, method) (void)m11784_gshared((t2315 *)__this, (t29 *)p0, method)
