﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t731;
struct t29;
struct t674;
struct t20;
struct t136;
struct t726;
struct t316;
struct t42;
struct t7;

 void m3980 (t731 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4031 (t731 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4181 (t731 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6646 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3978 (t731 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4258 (t731 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3976 (t731 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5208 (t731 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5209 (t731 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4175 (t731 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m4176 (t731 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6647 (t731 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6648 (t731 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3991 (t731 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4178 (t731 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5210 (t731 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5211 (t731 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6649 (t731 * __this, t29 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6650 (t731 * __this, t29 * p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4157 (t731 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6651 (t731 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5179 (t731 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4265 (t731 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4254 (t731 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4177 (t731 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6652 (t731 * __this, int32_t p0, t20 * p1, int32_t p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3981 (t731 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4124 (t731 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4264 (t731 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6653 (t731 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m6654 (t731 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t20 * m4158 (t731 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6655 (t29 * __this, t7* p0, t29 * p1, t7* p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t731 * m6656 (t29 * __this, t731 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t731 * m5159 (t29 * __this, t731 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
