﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2913;
struct t29;
struct t7;
struct t485;
#include "t725.h"
#include "t2911.h"

 void m15976 (t2913 * __this, t485 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15977 (t2913 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t725  m15978 (t2913 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15979 (t2913 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15980 (t2913 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15981 (t2913 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2911  m15982 (t2913 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m15983 (t2913 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m15984 (t2913 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15985 (t2913 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15986 (t2913 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15987 (t2913 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
