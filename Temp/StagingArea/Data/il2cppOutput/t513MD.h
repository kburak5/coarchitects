﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t513;
struct t7;
struct t41;
struct t41_marshaled;
struct t514;
#include "t445.h"
#include "t515.h"
#include "t516.h"

 void m2618 (t513 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2619 (t513 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2620 (t513 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2621 (t513 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2622 (t513 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m2623 (t513 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2624 (t513 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2625 (t513 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2626 (t513 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t41 * m2627 (t513 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2628 (t513 * __this, t41 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2629 (t513 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2630 (t513 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m2631 (t513 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2632 (t513 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2633 (t513 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2634 (t513 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2635 (t513 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2636 (t513 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t514 * m2637 (t513 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t515  m2638 (t513 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t516  m2639 (t513 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
