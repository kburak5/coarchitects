﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3045;
struct t29;
struct t66;
struct t67;
#include "t35.h"
#include "t380.h"

 void m16809 (t3045 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16810 (t3045 * __this, t380  p0, t380  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m16811 (t3045 * __this, t380  p0, t380  p1, t67 * p2, t29 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16812 (t3045 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
