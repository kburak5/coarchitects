﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3318;
struct t29;
struct t20;
#include "t1173.h"

 void m18472 (t3318 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18473 (t3318 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18474 (t3318 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18475 (t3318 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1173  m18476 (t3318 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
