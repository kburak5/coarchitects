﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t981;
struct t972;
struct t29;

 void m4365 (t981 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t972 * m4366 (t981 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t972 * m4367 (t981 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t972 * m4368 (t981 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4369 (t981 * __this, t972 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
