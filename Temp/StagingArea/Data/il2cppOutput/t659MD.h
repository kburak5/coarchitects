﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t659;
struct t41;
struct t41_marshaled;
struct t557;
struct t316;

 void m2977 (t659 * __this, t41 * p0, t557 * p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17291 (t659 * __this, t316* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
