﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3126;
struct t29;
struct t66;
struct t67;
#include "t35.h"

 void m17264_gshared (t3126 * __this, t29 * p0, t35 p1, MethodInfo* method);
#define m17264(__this, p0, p1, method) (void)m17264_gshared((t3126 *)__this, (t29 *)p0, (t35)p1, method)
 void m17265_gshared (t3126 * __this, t29 * p0, t29 * p1, t29 * p2, MethodInfo* method);
#define m17265(__this, p0, p1, p2, method) (void)m17265_gshared((t3126 *)__this, (t29 *)p0, (t29 *)p1, (t29 *)p2, method)
 t29 * m17266_gshared (t3126 * __this, t29 * p0, t29 * p1, t29 * p2, t67 * p3, t29 * p4, MethodInfo* method);
#define m17266(__this, p0, p1, p2, p3, p4, method) (t29 *)m17266_gshared((t3126 *)__this, (t29 *)p0, (t29 *)p1, (t29 *)p2, (t67 *)p3, (t29 *)p4, method)
 void m17267_gshared (t3126 * __this, t29 * p0, MethodInfo* method);
#define m17267(__this, p0, method) (void)m17267_gshared((t3126 *)__this, (t29 *)p0, method)
