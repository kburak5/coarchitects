﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1210;
struct t997;
struct t781;

 void m6344 (t1210 * __this, t997 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6345 (t1210 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6346 (t1210 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6347 (t1210 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
