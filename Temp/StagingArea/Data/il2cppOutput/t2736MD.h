﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2736;
struct t29;
struct t20;
#include "t253.h"

 void m14926 (t2736 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m14927 (t2736 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m14928 (t2736 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m14929 (t2736 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m14930 (t2736 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
