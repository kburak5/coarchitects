﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1638;
struct t29;

 void m9220 (t1638 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9221 (t1638 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9222 (t1638 * __this, int8_t p0, int8_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
