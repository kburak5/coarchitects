﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t864;
struct t7;

 void m3751 (t864 * __this, t7* p0, bool p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3752 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3753 (t864 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3754 (t864 * __this, t7* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3755 (t864 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3756 (t864 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m3757 (t864 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
