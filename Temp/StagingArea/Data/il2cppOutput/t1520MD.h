﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1520;
struct t7;
struct t29;
struct t42;

 void m8138 (t1520 * __this, t7* p0, t42 * p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8139 (t1520 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m8140 (t1520 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
