﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
#include "stringLiterals.h"

extern TypeInfo t713_TI;
extern TypeInfo t714_TI;
extern TypeInfo t715_TI;
extern TypeInfo t716_TI;
extern TypeInfo t717_TI;
extern TypeInfo t718_TI;
extern TypeInfo t723_TI;
extern TypeInfo t724_TI;
extern TypeInfo t720_TI;
extern TypeInfo t727_TI;
extern TypeInfo t728_TI;
extern TypeInfo t730_TI;
extern TypeInfo t729_TI;
extern TypeInfo t736_TI;
extern TypeInfo t606_TI;
extern TypeInfo t737_TI;
extern TypeInfo t738_TI;
extern TypeInfo t739_TI;
extern TypeInfo t740_TI;
extern TypeInfo t741_TI;
extern TypeInfo t742_TI;
extern TypeInfo t743_TI;
extern TypeInfo t747_TI;
extern TypeInfo t752_TI;
extern TypeInfo t753_TI;
extern TypeInfo t754_TI;
extern TypeInfo t757_TI;
extern TypeInfo t758_TI;
extern TypeInfo t759_TI;
extern TypeInfo t761_TI;
extern TypeInfo t769_TI;
extern TypeInfo t773_TI;
extern TypeInfo t763_TI;
extern TypeInfo t765_TI;
extern TypeInfo t750_TI;
extern TypeInfo t920_TI;
extern TypeInfo t766_TI;
extern TypeInfo t744_TI;
extern TypeInfo t767_TI;
extern TypeInfo t768_TI;
extern TypeInfo t749_TI;
extern TypeInfo t772_TI;
extern TypeInfo t746_TI;
extern TypeInfo t775_TI;
extern TypeInfo t776_TI;
extern TypeInfo t784_TI;
extern TypeInfo t785_TI;
extern TypeInfo t786_TI;
extern TypeInfo t787_TI;
extern TypeInfo t788_TI;
extern TypeInfo t791_TI;
extern TypeInfo t796_TI;
extern TypeInfo t797_TI;
extern TypeInfo t799_TI;
extern TypeInfo t762_TI;
extern TypeInfo t756_TI;
extern TypeInfo t806_TI;
extern TypeInfo t802_TI;
extern TypeInfo t812_TI;
extern TypeInfo t803_TI;
extern TypeInfo t805_TI;
extern TypeInfo t811_TI;
extern TypeInfo t818_TI;
extern TypeInfo t789_TI;
extern TypeInfo t792_TI;
extern TypeInfo t819_TI;
extern TypeInfo t798_TI;
extern TypeInfo t820_TI;
extern TypeInfo t821_TI;
extern TypeInfo t794_TI;
extern TypeInfo t814_TI;
extern TypeInfo t815_TI;
extern TypeInfo t807_TI;
extern TypeInfo t824_TI;
extern TypeInfo t825_TI;
extern TypeInfo t817_TI;
extern TypeInfo t790_TI;
extern TypeInfo t778_TI;
extern TypeInfo t779_TI;
extern TypeInfo t813_TI;
extern TypeInfo t826_TI;
extern TypeInfo t827_TI;
extern TypeInfo t830_TI;
extern TypeInfo t831_TI;
extern TypeInfo t833_TI;
extern TypeInfo t834_TI;
extern TypeInfo t828_TI;
extern TypeInfo t837_TI;
extern TypeInfo t838_TI;
extern TypeInfo t829_TI;
extern TypeInfo t842_TI;
extern TypeInfo t843_TI;
extern TypeInfo t844_TI;
extern TypeInfo t845_TI;
extern TypeInfo t836_TI;
extern TypeInfo t840_TI;
extern TypeInfo t846_TI;
extern TypeInfo t839_TI;
extern TypeInfo t848_TI;
extern TypeInfo t847_TI;
extern TypeInfo t849_TI;
extern TypeInfo t850_TI;
extern TypeInfo t852_TI;
extern TypeInfo t878_TI;
extern TypeInfo t853_TI;
extern TypeInfo t854_TI;
extern TypeInfo t855_TI;
extern TypeInfo t857_TI;
extern TypeInfo t856_TI;
extern TypeInfo t859_TI;
extern TypeInfo t860_TI;
extern TypeInfo t861_TI;
extern TypeInfo t862_TI;
extern TypeInfo t863_TI;
extern TypeInfo t866_TI;
extern TypeInfo t867_TI;
extern TypeInfo t869_TI;
extern TypeInfo t870_TI;
extern TypeInfo t871_TI;
extern TypeInfo t864_TI;
extern TypeInfo t877_TI;
extern TypeInfo t875_TI;
extern TypeInfo t880_TI;
extern TypeInfo t873_TI;
extern TypeInfo t872_TI;
extern TypeInfo t881_TI;
extern TypeInfo t882_TI;
extern TypeInfo t883_TI;
extern TypeInfo t884_TI;
extern TypeInfo t874_TI;
extern TypeInfo t885_TI;
extern TypeInfo t876_TI;
extern TypeInfo t887_TI;
extern TypeInfo t886_TI;
extern TypeInfo t888_TI;
extern TypeInfo t889_TI;
extern TypeInfo t890_TI;
extern TypeInfo t891_TI;
extern TypeInfo t879_TI;
extern TypeInfo t892_TI;
extern TypeInfo t894_TI;
extern TypeInfo t895_TI;
extern TypeInfo t748_TI;
extern TypeInfo t900_TI;
extern TypeInfo t897_TI;
extern TypeInfo t899_TI;
extern TypeInfo t893_TI;
extern TypeInfo t898_TI;
extern TypeInfo t902_TI;
extern TypeInfo t755_TI;
extern TypeInfo t903_TI;
extern TypeInfo t904_TI;
extern TypeInfo t905_TI;
#include "utils/RegisterRuntimeInitializeAndCleanup.h"
#include <map>
struct TypeInfo;
struct MethodInfo;
TypeInfo* g_System_Assembly_Types[154] = 
{
	&t713_TI,
	&t714_TI,
	&t715_TI,
	&t716_TI,
	&t717_TI,
	&t718_TI,
	&t723_TI,
	&t724_TI,
	&t720_TI,
	&t727_TI,
	&t728_TI,
	&t730_TI,
	&t729_TI,
	&t736_TI,
	&t606_TI,
	&t737_TI,
	&t738_TI,
	&t739_TI,
	&t740_TI,
	&t741_TI,
	&t742_TI,
	&t743_TI,
	&t747_TI,
	&t752_TI,
	&t753_TI,
	&t754_TI,
	&t757_TI,
	&t758_TI,
	&t759_TI,
	&t761_TI,
	&t769_TI,
	&t773_TI,
	&t763_TI,
	&t765_TI,
	&t750_TI,
	&t920_TI,
	&t766_TI,
	&t744_TI,
	&t767_TI,
	&t768_TI,
	&t749_TI,
	&t772_TI,
	&t746_TI,
	&t775_TI,
	&t776_TI,
	&t784_TI,
	&t785_TI,
	&t786_TI,
	&t787_TI,
	&t788_TI,
	&t791_TI,
	&t796_TI,
	&t797_TI,
	&t799_TI,
	&t762_TI,
	&t756_TI,
	&t806_TI,
	&t802_TI,
	&t812_TI,
	&t803_TI,
	&t805_TI,
	&t811_TI,
	&t818_TI,
	&t789_TI,
	&t792_TI,
	&t819_TI,
	&t798_TI,
	&t820_TI,
	&t821_TI,
	&t794_TI,
	&t814_TI,
	&t815_TI,
	&t807_TI,
	&t824_TI,
	&t825_TI,
	&t817_TI,
	&t790_TI,
	&t778_TI,
	&t779_TI,
	&t813_TI,
	&t826_TI,
	&t827_TI,
	&t830_TI,
	&t831_TI,
	&t833_TI,
	&t834_TI,
	&t828_TI,
	&t837_TI,
	&t838_TI,
	&t829_TI,
	&t842_TI,
	&t843_TI,
	&t844_TI,
	&t845_TI,
	&t836_TI,
	&t840_TI,
	&t846_TI,
	&t839_TI,
	&t848_TI,
	&t847_TI,
	&t849_TI,
	&t850_TI,
	&t852_TI,
	&t878_TI,
	&t853_TI,
	&t854_TI,
	&t855_TI,
	&t857_TI,
	&t856_TI,
	&t859_TI,
	&t860_TI,
	&t861_TI,
	&t862_TI,
	&t863_TI,
	&t866_TI,
	&t867_TI,
	&t869_TI,
	&t870_TI,
	&t871_TI,
	&t864_TI,
	&t877_TI,
	&t875_TI,
	&t880_TI,
	&t873_TI,
	&t872_TI,
	&t881_TI,
	&t882_TI,
	&t883_TI,
	&t884_TI,
	&t874_TI,
	&t885_TI,
	&t876_TI,
	&t887_TI,
	&t886_TI,
	&t888_TI,
	&t889_TI,
	&t890_TI,
	&t891_TI,
	&t879_TI,
	&t892_TI,
	&t894_TI,
	&t895_TI,
	&t748_TI,
	&t900_TI,
	&t897_TI,
	&t899_TI,
	&t893_TI,
	&t898_TI,
	&t902_TI,
	&t755_TI,
	&t903_TI,
	&t904_TI,
	&t905_TI,
	NULL,
};
extern Il2CppImage g_System_dll_Image;
extern CustomAttributesCache g_System_Assembly__CustomAttributeCache;
Il2CppAssembly g_System_Assembly = 
{
	{ "System", 0, 0, "\x0\x24\x0\x0\x4\x80\x0\x0\x94\x0\x0\x0\x6\x2\x0\x0\x0\x24\x0\x0\x52\x53\x41\x31\x0\x4\x0\x0\x1\x0\x1\x0\x8D\x56\xC7\x6F\x9E\x86\x49\x38\x30\x49\xF3\x83\xC4\x4B\xE0\xEC\x20\x41\x81\x82\x2A\x6C\x31\xCF\x5E\xB7\xEF\x48\x69\x44\xD0\x32\x18\x8E\xA1\xD3\x92\x7\x63\x71\x2C\xCB\x12\xD7\x5F\xB7\x7E\x98\x11\x14\x9E\x61\x48\xE5\xD3\x2F\xBA\xAB\x37\x61\x1C\x18\x78\xDD\xC1\x9E\x20\xEF\x13\x5D\xC\xB2\xCF\xF2\xBF\xEC\x3D\x11\x58\x10\xC3\xD9\x6\x96\x38\xFE\x4B\xE2\x15\xDB\xF7\x95\x86\x19\x20\xE5\xAB\x6F\x7D\xB2\xE2\xCE\xEF\x13\x6A\xC2\x3D\x5D\xD2\xBF\x3\x17\x0\xAE\xC2\x32\xF6\xC6\xB1\xC7\x85\xB4\x30\x5C\x12\x3B\x37\xAB", { 0x7C, 0xEC, 0x85, 0xD7, 0xBE, 0xA7, 0x79, 0x8E }, 32772, 0, 1, 2, 0, 5, 0 },
	&g_System_dll_Image,
	&g_System_Assembly__CustomAttributeCache,
};
Il2CppImage g_System_dll_Image = 
{
	 "System.dll" ,
	&g_System_Assembly,
	g_System_Assembly_Types,
	153,
	NULL,
};
static void s_SystemRegistration()
{
	RegisterAssembly (&g_System_Assembly);
}
static il2cpp::utils::RegisterRuntimeInitializeAndCleanup s_SystemRegistrationVariable(&s_SystemRegistration, NULL);
