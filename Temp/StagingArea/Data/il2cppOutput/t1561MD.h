﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1561;
struct t1115;
struct t781;

 void m8465 (t1561 * __this, t1115 * p0, bool p1, t781* p2, t781* p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8466 (t1561 * __this, t781* p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m8467 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
