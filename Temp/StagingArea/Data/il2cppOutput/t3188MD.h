﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3188;
struct t29;
struct t20;
#include "t766.h"

 void m17703 (t3188 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17704 (t3188 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17705 (t3188 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17706 (t3188 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17707 (t3188 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
