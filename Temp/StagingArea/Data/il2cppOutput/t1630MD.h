﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1630;
struct t7;
struct t292;
struct t1296;
#include "t465.h"
#include "t1631.h"

 int32_t m9201 (t29 * __this, t7* p0, int32_t p1, uint16_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9202 (t29 * __this, t292 * p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9203 (t29 * __this, t7* p0, int32_t p1, t292 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m9204 (t29 * __this, uint16_t p0, t1296 * p1, bool* p2, bool* p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m9205 (t29 * __this, uint16_t p0, t1296 * p1, bool* p2, bool* p3, bool p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m9206 (t29 * __this, t465  p0, t7* p1, t1296 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m9207 (t29 * __this, t465  p0, t1631  p1, t7* p2, t1296 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
