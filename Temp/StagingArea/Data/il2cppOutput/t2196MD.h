﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2196;
struct t29;

 void m10749_gshared (t2196 * __this, MethodInfo* method);
#define m10749(__this, method) (void)m10749_gshared((t2196 *)__this, method)
 int32_t m10750_gshared (t2196 * __this, t29 * p0, t29 * p1, MethodInfo* method);
#define m10750(__this, p0, p1, method) (int32_t)m10750_gshared((t2196 *)__this, (t29 *)p0, (t29 *)p1, method)
