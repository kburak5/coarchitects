﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1534;
struct t781;

 void m8249 (t1534 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8250 (t1534 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8251 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8252 (t1534 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
