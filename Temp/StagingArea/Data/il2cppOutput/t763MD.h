﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t763;
struct t764;
struct t7;
struct t29;
#include "t742.h"

 void m3181 (t763 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3182 (t763 * __this, t764* p0, int64_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3183 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m3184 (t29 * __this, int16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m3185 (t29 * __this, int16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m3186 (t29 * __this, int16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t763 * m3187 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3188 (t29 * __this, t7* p0, t763 ** p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t763 * m3189 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t763 * m3190 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m3191 (t763 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m3192 (t763 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3193 (t763 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3194 (t29 * __this, t763 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3195 (t763 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3196 (t29 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3197 (t763 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3198 (t763 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3199 (t29 * __this, int32_t p0, int32_t p1, int32_t p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
