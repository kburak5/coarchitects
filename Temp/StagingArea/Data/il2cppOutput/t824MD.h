﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t824;
struct t7;
struct t778;
struct t781;
struct t776;
#include "t825.h"
#include "t790.h"

 void m3453 (t824 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3454 (t824 * __this, t778 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3455 (t824 * __this, t781* p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3456 (t824 * __this, t7* p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3457 (t824 * __this, t776 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3458 (t824 * __this, t776 * p0, int32_t p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3459 (t824 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3460 (t824 * __this, t778 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m3461 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m3462 (t29 * __this, uint16_t p0, uint16_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m3463 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3464 (t824 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m3465 (t824 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3466 (t824 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
