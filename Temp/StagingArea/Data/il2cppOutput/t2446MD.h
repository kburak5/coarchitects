﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2446;
struct t29;
struct t145;
struct t365;
#include "t725.h"
#include "t2444.h"

 void m12804 (t2446 * __this, t365 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12805 (t2446 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t725  m12806 (t2446 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12807 (t2446 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12808 (t2446 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12809 (t2446 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2444  m12810 (t2446 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12811 (t2446 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m12812 (t2446 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12813 (t2446 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12814 (t2446 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12815 (t2446 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
