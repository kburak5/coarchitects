﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1216;
struct t1215;
struct t781;
struct t7;
struct t1196;
#include "t934.h"

 void m6379 (t1216 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6380 (t1216 * __this, t1215 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6381 (t1216 * __this, t1215 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6382 (t1216 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6383 (t1216 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6384 (t1216 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6385 (t1216 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6386 (t1216 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6387 (t1216 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t934  m6388 (t1216 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6389 (t1216 * __this, t934  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6390 (t1216 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6391 (t1216 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6392 (t1216 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6393 (t1216 * __this, t1196 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
