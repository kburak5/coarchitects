﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t812;
struct t29;
struct t806;
struct t603;

 void m3400 (t812 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3401 (t812 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t806 * m3402 (t812 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3403 (t812 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
