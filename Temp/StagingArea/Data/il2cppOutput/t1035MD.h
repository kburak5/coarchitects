﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1035;
struct t1020;
struct t781;
#include "t1037.h"
#include "t1024.h"

 void m5012 (t1035 * __this, t1020 * p0, uint8_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5013 (t1035 * __this, t1020 * p0, uint8_t p1, uint8_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5014 (t1035 * __this, t1020 * p0, uint8_t p1, t781* p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1020 * m5015 (t1035 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m5016 (t1035 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m5017 (t1035 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5018 (t1035 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5019 (t1035 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m5020 (t1035 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5021 (t29 * __this, t781* p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
