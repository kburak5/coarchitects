﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1000;
struct t7;
struct t793;

 void m4463 (t1000 * __this, t7* p0, t793 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4464 (t1000 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t793 * m4465 (t1000 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
