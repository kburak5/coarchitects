﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include "System_ArrayTypes.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t3906_TI;


#include "t20.h"

// Metadata Definition System.Security.Cryptography.X509Certificates.StoreName[]
static MethodInfo* t3906_MIs[] =
{
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m1332_MI;
extern MethodInfo m4200_MI;
extern MethodInfo m5904_MI;
extern MethodInfo m5920_MI;
extern MethodInfo m5921_MI;
extern MethodInfo m4199_MI;
extern MethodInfo m5922_MI;
extern MethodInfo m5923_MI;
extern MethodInfo m5895_MI;
extern MethodInfo m5896_MI;
extern MethodInfo m5897_MI;
extern MethodInfo m5898_MI;
extern MethodInfo m5899_MI;
extern MethodInfo m5900_MI;
extern MethodInfo m5901_MI;
extern MethodInfo m5902_MI;
extern MethodInfo m5903_MI;
extern MethodInfo m4006_MI;
extern MethodInfo m5905_MI;
extern MethodInfo m5906_MI;
extern MethodInfo m23420_MI;
extern MethodInfo m5907_MI;
extern MethodInfo m23421_MI;
extern MethodInfo m23422_MI;
extern MethodInfo m23423_MI;
extern MethodInfo m23424_MI;
extern MethodInfo m23425_MI;
extern MethodInfo m5908_MI;
extern MethodInfo m23419_MI;
extern MethodInfo m23427_MI;
extern MethodInfo m23428_MI;
extern MethodInfo m19711_MI;
extern MethodInfo m19712_MI;
extern MethodInfo m19713_MI;
extern MethodInfo m19714_MI;
extern MethodInfo m19715_MI;
extern MethodInfo m19716_MI;
extern MethodInfo m19710_MI;
extern MethodInfo m19718_MI;
extern MethodInfo m19719_MI;
extern MethodInfo m19722_MI;
extern MethodInfo m19723_MI;
extern MethodInfo m19724_MI;
extern MethodInfo m19725_MI;
extern MethodInfo m19726_MI;
extern MethodInfo m19727_MI;
extern MethodInfo m19721_MI;
extern MethodInfo m19729_MI;
extern MethodInfo m19730_MI;
extern MethodInfo m19733_MI;
extern MethodInfo m19734_MI;
extern MethodInfo m19735_MI;
extern MethodInfo m19736_MI;
extern MethodInfo m19737_MI;
extern MethodInfo m19738_MI;
extern MethodInfo m19732_MI;
extern MethodInfo m19740_MI;
extern MethodInfo m19741_MI;
extern MethodInfo m19744_MI;
extern MethodInfo m19745_MI;
extern MethodInfo m19746_MI;
extern MethodInfo m19747_MI;
extern MethodInfo m19748_MI;
extern MethodInfo m19749_MI;
extern MethodInfo m19743_MI;
extern MethodInfo m19751_MI;
extern MethodInfo m19752_MI;
extern MethodInfo m19755_MI;
extern MethodInfo m19756_MI;
extern MethodInfo m19757_MI;
extern MethodInfo m19758_MI;
extern MethodInfo m19759_MI;
extern MethodInfo m19760_MI;
extern MethodInfo m19754_MI;
extern MethodInfo m19762_MI;
extern MethodInfo m19763_MI;
extern MethodInfo m19494_MI;
extern MethodInfo m19496_MI;
extern MethodInfo m19498_MI;
extern MethodInfo m19499_MI;
extern MethodInfo m19500_MI;
extern MethodInfo m19501_MI;
extern MethodInfo m19490_MI;
extern MethodInfo m19503_MI;
extern MethodInfo m19504_MI;
static MethodInfo* t3906_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23420_MI,
	&m5907_MI,
	&m23421_MI,
	&m23422_MI,
	&m23423_MI,
	&m23424_MI,
	&m23425_MI,
	&m5908_MI,
	&m23419_MI,
	&m23427_MI,
	&m23428_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5874_TI;
extern TypeInfo t5875_TI;
extern TypeInfo t5876_TI;
extern TypeInfo t5104_TI;
extern TypeInfo t5105_TI;
extern TypeInfo t5106_TI;
extern TypeInfo t5107_TI;
extern TypeInfo t5108_TI;
extern TypeInfo t5109_TI;
extern TypeInfo t5110_TI;
extern TypeInfo t5111_TI;
extern TypeInfo t5112_TI;
extern TypeInfo t5113_TI;
extern TypeInfo t5114_TI;
extern TypeInfo t5115_TI;
extern TypeInfo t5116_TI;
extern TypeInfo t5117_TI;
extern TypeInfo t5118_TI;
extern TypeInfo t2182_TI;
extern TypeInfo t2186_TI;
extern TypeInfo t2183_TI;
static TypeInfo* t3906_ITIs[] = 
{
	&t5874_TI,
	&t5875_TI,
	&t5876_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
extern TypeInfo t603_TI;
extern TypeInfo t373_TI;
extern TypeInfo t674_TI;
extern TypeInfo t868_TI;
static Il2CppInterfaceOffsetPair t3906_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5874_TI, 21},
	{ &t5875_TI, 28},
	{ &t5876_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t3906_0_0_0;
extern Il2CppType t3906_1_0_0;
extern TypeInfo t20_TI;
#include "t785.h"
extern TypeInfo t785_TI;
extern TypeInfo t44_TI;
TypeInfo t3906_TI = 
{
	&g_System_dll_Image, NULL, "StoreName[]", "System.Security.Cryptography.X509Certificates", t3906_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t785_TI, t3906_ITIs, t3906_VT, &EmptyCustomAttributesCache, &t44_TI, &t3906_0_0_0, &t3906_1_0_0, t3906_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3907_TI;



// Metadata Definition System.Security.Cryptography.X509Certificates.X500DistinguishedNameFlags[]
static MethodInfo* t3907_MIs[] =
{
	NULL
};
extern MethodInfo m23431_MI;
extern MethodInfo m23432_MI;
extern MethodInfo m23433_MI;
extern MethodInfo m23434_MI;
extern MethodInfo m23435_MI;
extern MethodInfo m23436_MI;
extern MethodInfo m23430_MI;
extern MethodInfo m23438_MI;
extern MethodInfo m23439_MI;
static MethodInfo* t3907_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23431_MI,
	&m5907_MI,
	&m23432_MI,
	&m23433_MI,
	&m23434_MI,
	&m23435_MI,
	&m23436_MI,
	&m5908_MI,
	&m23430_MI,
	&m23438_MI,
	&m23439_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5877_TI;
extern TypeInfo t5878_TI;
extern TypeInfo t5879_TI;
static TypeInfo* t3907_ITIs[] = 
{
	&t5877_TI,
	&t5878_TI,
	&t5879_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3907_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5877_TI, 21},
	{ &t5878_TI, 28},
	{ &t5879_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t3907_0_0_0;
extern Il2CppType t3907_1_0_0;
#include "t787.h"
extern TypeInfo t787_TI;
extern CustomAttributesCache t787__CustomAttributeCache;
TypeInfo t3907_TI = 
{
	&g_System_dll_Image, NULL, "X500DistinguishedNameFlags[]", "System.Security.Cryptography.X509Certificates", t3907_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t787_TI, t3907_ITIs, t3907_VT, &EmptyCustomAttributesCache, &t44_TI, &t3907_0_0_0, &t3907_1_0_0, t3907_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#include "mscorlib_ArrayTypes.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t801_TI;



// Metadata Definition System.Security.Cryptography.X509Certificates.X509Certificate[]
static MethodInfo* t801_MIs[] =
{
	NULL
};
extern MethodInfo m23442_MI;
extern MethodInfo m23443_MI;
extern MethodInfo m23444_MI;
extern MethodInfo m23445_MI;
extern MethodInfo m23446_MI;
extern MethodInfo m23447_MI;
extern MethodInfo m23441_MI;
extern MethodInfo m23449_MI;
extern MethodInfo m23450_MI;
extern MethodInfo m22439_MI;
extern MethodInfo m22440_MI;
extern MethodInfo m22441_MI;
extern MethodInfo m22442_MI;
extern MethodInfo m22443_MI;
extern MethodInfo m22444_MI;
extern MethodInfo m22438_MI;
extern MethodInfo m22446_MI;
extern MethodInfo m22447_MI;
extern MethodInfo m23453_MI;
extern MethodInfo m23454_MI;
extern MethodInfo m23455_MI;
extern MethodInfo m23456_MI;
extern MethodInfo m23457_MI;
extern MethodInfo m23458_MI;
extern MethodInfo m23452_MI;
extern MethodInfo m23460_MI;
extern MethodInfo m23461_MI;
static MethodInfo* t801_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23442_MI,
	&m5907_MI,
	&m23443_MI,
	&m23444_MI,
	&m23445_MI,
	&m23446_MI,
	&m23447_MI,
	&m5908_MI,
	&m23441_MI,
	&m23449_MI,
	&m23450_MI,
	&m5905_MI,
	&m5906_MI,
	&m22439_MI,
	&m5907_MI,
	&m22440_MI,
	&m22441_MI,
	&m22442_MI,
	&m22443_MI,
	&m22444_MI,
	&m5908_MI,
	&m22438_MI,
	&m22446_MI,
	&m22447_MI,
	&m5905_MI,
	&m5906_MI,
	&m23453_MI,
	&m5907_MI,
	&m23454_MI,
	&m23455_MI,
	&m23456_MI,
	&m23457_MI,
	&m23458_MI,
	&m5908_MI,
	&m23452_MI,
	&m23460_MI,
	&m23461_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5880_TI;
extern TypeInfo t5881_TI;
extern TypeInfo t5882_TI;
extern TypeInfo t5649_TI;
extern TypeInfo t5650_TI;
extern TypeInfo t5651_TI;
extern TypeInfo t5883_TI;
extern TypeInfo t5884_TI;
extern TypeInfo t5885_TI;
static TypeInfo* t801_ITIs[] = 
{
	&t5880_TI,
	&t5881_TI,
	&t5882_TI,
	&t5649_TI,
	&t5650_TI,
	&t5651_TI,
	&t5883_TI,
	&t5884_TI,
	&t5885_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t801_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5880_TI, 21},
	{ &t5881_TI, 28},
	{ &t5882_TI, 33},
	{ &t5649_TI, 34},
	{ &t5650_TI, 41},
	{ &t5651_TI, 46},
	{ &t5883_TI, 47},
	{ &t5884_TI, 54},
	{ &t5885_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t801_0_0_0;
extern Il2CppType t801_1_0_0;
struct t745;
extern TypeInfo t745_TI;
extern CustomAttributesCache t745__CustomAttributeCache;
extern CustomAttributesCache t745__CustomAttributeCache_m4119;
extern CustomAttributesCache t745__CustomAttributeCache_m4120;
extern CustomAttributesCache t745__CustomAttributeCache_m4111;
extern CustomAttributesCache t745__CustomAttributeCache_m4101;
extern CustomAttributesCache t745__CustomAttributeCache_m4103;
TypeInfo t801_TI = 
{
	&g_mscorlib_dll_Image, NULL, "X509Certificate[]", "System.Security.Cryptography.X509Certificates", t801_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t745_TI, t801_ITIs, t801_VT, &EmptyCustomAttributesCache, &t801_TI, &t801_0_0_0, &t801_1_0_0, t801_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t745 *), -1, 0, 0, -1, 1056769, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3555_TI;



// Metadata Definition System.Runtime.Serialization.IDeserializationCallback[]
static MethodInfo* t3555_MIs[] =
{
	NULL
};
static MethodInfo* t3555_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23453_MI,
	&m5907_MI,
	&m23454_MI,
	&m23455_MI,
	&m23456_MI,
	&m23457_MI,
	&m23458_MI,
	&m5908_MI,
	&m23452_MI,
	&m23460_MI,
	&m23461_MI,
};
static TypeInfo* t3555_ITIs[] = 
{
	&t5883_TI,
	&t5884_TI,
	&t5885_TI,
};
static Il2CppInterfaceOffsetPair t3555_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5883_TI, 21},
	{ &t5884_TI, 28},
	{ &t5885_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3555_0_0_0;
extern Il2CppType t3555_1_0_0;
struct t918;
extern TypeInfo t918_TI;
extern CustomAttributesCache t918__CustomAttributeCache;
TypeInfo t3555_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IDeserializationCallback[]", "System.Runtime.Serialization", t3555_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t918_TI, t3555_ITIs, t3555_VT, &EmptyCustomAttributesCache, &t3555_TI, &t3555_0_0_0, &t3555_1_0_0, t3555_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t804_TI;



// Metadata Definition System.Security.Cryptography.X509Certificates.X509ChainStatus[]
static MethodInfo* t804_MIs[] =
{
	NULL
};
extern MethodInfo m23464_MI;
extern MethodInfo m23465_MI;
extern MethodInfo m23466_MI;
extern MethodInfo m23467_MI;
extern MethodInfo m23468_MI;
extern MethodInfo m23469_MI;
extern MethodInfo m23463_MI;
extern MethodInfo m23471_MI;
extern MethodInfo m23472_MI;
static MethodInfo* t804_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23464_MI,
	&m5907_MI,
	&m23465_MI,
	&m23466_MI,
	&m23467_MI,
	&m23468_MI,
	&m23469_MI,
	&m5908_MI,
	&m23463_MI,
	&m23471_MI,
	&m23472_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5886_TI;
extern TypeInfo t5887_TI;
extern TypeInfo t5888_TI;
static TypeInfo* t804_ITIs[] = 
{
	&t5886_TI,
	&t5887_TI,
	&t5888_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t804_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5886_TI, 21},
	{ &t5887_TI, 28},
	{ &t5888_TI, 33},
	{ &t5116_TI, 34},
	{ &t5117_TI, 41},
	{ &t5118_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t804_0_0_0;
extern Il2CppType t804_1_0_0;
#include "t805.h"
extern TypeInfo t805_TI;
TypeInfo t804_TI = 
{
	&g_System_dll_Image, NULL, "X509ChainStatus[]", "System.Security.Cryptography.X509Certificates", t804_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t805_TI, t804_ITIs, t804_VT, &EmptyCustomAttributesCache, &t804_TI, &t804_0_0_0, &t804_1_0_0, t804_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t805 ), -1, 0, 0, -1, 1048841, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3908_TI;



// Metadata Definition System.Security.Cryptography.X509Certificates.X509ChainStatusFlags[]
static MethodInfo* t3908_MIs[] =
{
	NULL
};
extern MethodInfo m23475_MI;
extern MethodInfo m23476_MI;
extern MethodInfo m23477_MI;
extern MethodInfo m23478_MI;
extern MethodInfo m23479_MI;
extern MethodInfo m23480_MI;
extern MethodInfo m23474_MI;
extern MethodInfo m23482_MI;
extern MethodInfo m23483_MI;
static MethodInfo* t3908_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23475_MI,
	&m5907_MI,
	&m23476_MI,
	&m23477_MI,
	&m23478_MI,
	&m23479_MI,
	&m23480_MI,
	&m5908_MI,
	&m23474_MI,
	&m23482_MI,
	&m23483_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5889_TI;
extern TypeInfo t5890_TI;
extern TypeInfo t5891_TI;
static TypeInfo* t3908_ITIs[] = 
{
	&t5889_TI,
	&t5890_TI,
	&t5891_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3908_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5889_TI, 21},
	{ &t5890_TI, 28},
	{ &t5891_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t3908_0_0_0;
extern Il2CppType t3908_1_0_0;
#include "t811.h"
extern TypeInfo t811_TI;
extern CustomAttributesCache t811__CustomAttributeCache;
TypeInfo t3908_TI = 
{
	&g_System_dll_Image, NULL, "X509ChainStatusFlags[]", "System.Security.Cryptography.X509Certificates", t3908_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t811_TI, t3908_ITIs, t3908_VT, &EmptyCustomAttributesCache, &t44_TI, &t3908_0_0_0, &t3908_1_0_0, t3908_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3909_TI;



// Metadata Definition System.Security.Cryptography.X509Certificates.X509FindType[]
static MethodInfo* t3909_MIs[] =
{
	NULL
};
extern MethodInfo m23486_MI;
extern MethodInfo m23487_MI;
extern MethodInfo m23488_MI;
extern MethodInfo m23489_MI;
extern MethodInfo m23490_MI;
extern MethodInfo m23491_MI;
extern MethodInfo m23485_MI;
extern MethodInfo m23493_MI;
extern MethodInfo m23494_MI;
static MethodInfo* t3909_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23486_MI,
	&m5907_MI,
	&m23487_MI,
	&m23488_MI,
	&m23489_MI,
	&m23490_MI,
	&m23491_MI,
	&m5908_MI,
	&m23485_MI,
	&m23493_MI,
	&m23494_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5892_TI;
extern TypeInfo t5893_TI;
extern TypeInfo t5894_TI;
static TypeInfo* t3909_ITIs[] = 
{
	&t5892_TI,
	&t5893_TI,
	&t5894_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3909_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5892_TI, 21},
	{ &t5893_TI, 28},
	{ &t5894_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t3909_0_0_0;
extern Il2CppType t3909_1_0_0;
#include "t798.h"
extern TypeInfo t798_TI;
TypeInfo t3909_TI = 
{
	&g_System_dll_Image, NULL, "X509FindType[]", "System.Security.Cryptography.X509Certificates", t3909_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t798_TI, t3909_ITIs, t3909_VT, &EmptyCustomAttributesCache, &t44_TI, &t3909_0_0_0, &t3909_1_0_0, t3909_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3910_TI;



// Metadata Definition System.Security.Cryptography.X509Certificates.X509KeyUsageFlags[]
static MethodInfo* t3910_MIs[] =
{
	NULL
};
extern MethodInfo m23497_MI;
extern MethodInfo m23498_MI;
extern MethodInfo m23499_MI;
extern MethodInfo m23500_MI;
extern MethodInfo m23501_MI;
extern MethodInfo m23502_MI;
extern MethodInfo m23496_MI;
extern MethodInfo m23504_MI;
extern MethodInfo m23505_MI;
static MethodInfo* t3910_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23497_MI,
	&m5907_MI,
	&m23498_MI,
	&m23499_MI,
	&m23500_MI,
	&m23501_MI,
	&m23502_MI,
	&m5908_MI,
	&m23496_MI,
	&m23504_MI,
	&m23505_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5895_TI;
extern TypeInfo t5896_TI;
extern TypeInfo t5897_TI;
static TypeInfo* t3910_ITIs[] = 
{
	&t5895_TI,
	&t5896_TI,
	&t5897_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3910_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5895_TI, 21},
	{ &t5896_TI, 28},
	{ &t5897_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t3910_0_0_0;
extern Il2CppType t3910_1_0_0;
#include "t821.h"
extern TypeInfo t821_TI;
extern CustomAttributesCache t821__CustomAttributeCache;
TypeInfo t3910_TI = 
{
	&g_System_dll_Image, NULL, "X509KeyUsageFlags[]", "System.Security.Cryptography.X509Certificates", t3910_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t821_TI, t3910_ITIs, t3910_VT, &EmptyCustomAttributesCache, &t44_TI, &t3910_0_0_0, &t3910_1_0_0, t3910_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3911_TI;



// Metadata Definition System.Security.Cryptography.X509Certificates.X509NameType[]
static MethodInfo* t3911_MIs[] =
{
	NULL
};
extern MethodInfo m23508_MI;
extern MethodInfo m23509_MI;
extern MethodInfo m23510_MI;
extern MethodInfo m23511_MI;
extern MethodInfo m23512_MI;
extern MethodInfo m23513_MI;
extern MethodInfo m23507_MI;
extern MethodInfo m23515_MI;
extern MethodInfo m23516_MI;
static MethodInfo* t3911_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23508_MI,
	&m5907_MI,
	&m23509_MI,
	&m23510_MI,
	&m23511_MI,
	&m23512_MI,
	&m23513_MI,
	&m5908_MI,
	&m23507_MI,
	&m23515_MI,
	&m23516_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5898_TI;
extern TypeInfo t5899_TI;
extern TypeInfo t5900_TI;
static TypeInfo* t3911_ITIs[] = 
{
	&t5898_TI,
	&t5899_TI,
	&t5900_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3911_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5898_TI, 21},
	{ &t5899_TI, 28},
	{ &t5900_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t3911_0_0_0;
extern Il2CppType t3911_1_0_0;
#include "t794.h"
extern TypeInfo t794_TI;
TypeInfo t3911_TI = 
{
	&g_System_dll_Image, NULL, "X509NameType[]", "System.Security.Cryptography.X509Certificates", t3911_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t794_TI, t3911_ITIs, t3911_VT, &EmptyCustomAttributesCache, &t44_TI, &t3911_0_0_0, &t3911_1_0_0, t3911_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3912_TI;



// Metadata Definition System.Security.Cryptography.X509Certificates.X509RevocationFlag[]
static MethodInfo* t3912_MIs[] =
{
	NULL
};
extern MethodInfo m23519_MI;
extern MethodInfo m23520_MI;
extern MethodInfo m23521_MI;
extern MethodInfo m23522_MI;
extern MethodInfo m23523_MI;
extern MethodInfo m23524_MI;
extern MethodInfo m23518_MI;
extern MethodInfo m23526_MI;
extern MethodInfo m23527_MI;
static MethodInfo* t3912_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23519_MI,
	&m5907_MI,
	&m23520_MI,
	&m23521_MI,
	&m23522_MI,
	&m23523_MI,
	&m23524_MI,
	&m5908_MI,
	&m23518_MI,
	&m23526_MI,
	&m23527_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5901_TI;
extern TypeInfo t5902_TI;
extern TypeInfo t5903_TI;
static TypeInfo* t3912_ITIs[] = 
{
	&t5901_TI,
	&t5902_TI,
	&t5903_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3912_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5901_TI, 21},
	{ &t5902_TI, 28},
	{ &t5903_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t3912_0_0_0;
extern Il2CppType t3912_1_0_0;
#include "t814.h"
extern TypeInfo t814_TI;
TypeInfo t3912_TI = 
{
	&g_System_dll_Image, NULL, "X509RevocationFlag[]", "System.Security.Cryptography.X509Certificates", t3912_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t814_TI, t3912_ITIs, t3912_VT, &EmptyCustomAttributesCache, &t44_TI, &t3912_0_0_0, &t3912_1_0_0, t3912_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3913_TI;



// Metadata Definition System.Security.Cryptography.X509Certificates.X509RevocationMode[]
static MethodInfo* t3913_MIs[] =
{
	NULL
};
extern MethodInfo m23530_MI;
extern MethodInfo m23531_MI;
extern MethodInfo m23532_MI;
extern MethodInfo m23533_MI;
extern MethodInfo m23534_MI;
extern MethodInfo m23535_MI;
extern MethodInfo m23529_MI;
extern MethodInfo m23537_MI;
extern MethodInfo m23538_MI;
static MethodInfo* t3913_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23530_MI,
	&m5907_MI,
	&m23531_MI,
	&m23532_MI,
	&m23533_MI,
	&m23534_MI,
	&m23535_MI,
	&m5908_MI,
	&m23529_MI,
	&m23537_MI,
	&m23538_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5904_TI;
extern TypeInfo t5905_TI;
extern TypeInfo t5906_TI;
static TypeInfo* t3913_ITIs[] = 
{
	&t5904_TI,
	&t5905_TI,
	&t5906_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3913_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5904_TI, 21},
	{ &t5905_TI, 28},
	{ &t5906_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t3913_0_0_0;
extern Il2CppType t3913_1_0_0;
#include "t815.h"
extern TypeInfo t815_TI;
TypeInfo t3913_TI = 
{
	&g_System_dll_Image, NULL, "X509RevocationMode[]", "System.Security.Cryptography.X509Certificates", t3913_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t815_TI, t3913_ITIs, t3913_VT, &EmptyCustomAttributesCache, &t44_TI, &t3913_0_0_0, &t3913_1_0_0, t3913_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3914_TI;



// Metadata Definition System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierHashAlgorithm[]
static MethodInfo* t3914_MIs[] =
{
	NULL
};
extern MethodInfo m23541_MI;
extern MethodInfo m23542_MI;
extern MethodInfo m23543_MI;
extern MethodInfo m23544_MI;
extern MethodInfo m23545_MI;
extern MethodInfo m23546_MI;
extern MethodInfo m23540_MI;
extern MethodInfo m23548_MI;
extern MethodInfo m23549_MI;
static MethodInfo* t3914_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23541_MI,
	&m5907_MI,
	&m23542_MI,
	&m23543_MI,
	&m23544_MI,
	&m23545_MI,
	&m23546_MI,
	&m5908_MI,
	&m23540_MI,
	&m23548_MI,
	&m23549_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5907_TI;
extern TypeInfo t5908_TI;
extern TypeInfo t5909_TI;
static TypeInfo* t3914_ITIs[] = 
{
	&t5907_TI,
	&t5908_TI,
	&t5909_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3914_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5907_TI, 21},
	{ &t5908_TI, 28},
	{ &t5909_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t3914_0_0_0;
extern Il2CppType t3914_1_0_0;
#include "t825.h"
extern TypeInfo t825_TI;
TypeInfo t3914_TI = 
{
	&g_System_dll_Image, NULL, "X509SubjectKeyIdentifierHashAlgorithm[]", "System.Security.Cryptography.X509Certificates", t3914_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t825_TI, t3914_ITIs, t3914_VT, &EmptyCustomAttributesCache, &t44_TI, &t3914_0_0_0, &t3914_1_0_0, t3914_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3915_TI;



// Metadata Definition System.Security.Cryptography.X509Certificates.X509VerificationFlags[]
static MethodInfo* t3915_MIs[] =
{
	NULL
};
extern MethodInfo m23552_MI;
extern MethodInfo m23553_MI;
extern MethodInfo m23554_MI;
extern MethodInfo m23555_MI;
extern MethodInfo m23556_MI;
extern MethodInfo m23557_MI;
extern MethodInfo m23551_MI;
extern MethodInfo m23559_MI;
extern MethodInfo m23560_MI;
static MethodInfo* t3915_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23552_MI,
	&m5907_MI,
	&m23553_MI,
	&m23554_MI,
	&m23555_MI,
	&m23556_MI,
	&m23557_MI,
	&m5908_MI,
	&m23551_MI,
	&m23559_MI,
	&m23560_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5910_TI;
extern TypeInfo t5911_TI;
extern TypeInfo t5912_TI;
static TypeInfo* t3915_ITIs[] = 
{
	&t5910_TI,
	&t5911_TI,
	&t5912_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3915_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5910_TI, 21},
	{ &t5911_TI, 28},
	{ &t5912_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t3915_0_0_0;
extern Il2CppType t3915_1_0_0;
#include "t817.h"
extern TypeInfo t817_TI;
extern CustomAttributesCache t817__CustomAttributeCache;
TypeInfo t3915_TI = 
{
	&g_System_dll_Image, NULL, "X509VerificationFlags[]", "System.Security.Cryptography.X509Certificates", t3915_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t817_TI, t3915_ITIs, t3915_VT, &EmptyCustomAttributesCache, &t44_TI, &t3915_0_0_0, &t3915_1_0_0, t3915_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3916_TI;



// Metadata Definition System.Security.Cryptography.AsnDecodeStatus[]
static MethodInfo* t3916_MIs[] =
{
	NULL
};
extern MethodInfo m23563_MI;
extern MethodInfo m23564_MI;
extern MethodInfo m23565_MI;
extern MethodInfo m23566_MI;
extern MethodInfo m23567_MI;
extern MethodInfo m23568_MI;
extern MethodInfo m23562_MI;
extern MethodInfo m23570_MI;
extern MethodInfo m23571_MI;
static MethodInfo* t3916_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23563_MI,
	&m5907_MI,
	&m23564_MI,
	&m23565_MI,
	&m23566_MI,
	&m23567_MI,
	&m23568_MI,
	&m5908_MI,
	&m23562_MI,
	&m23570_MI,
	&m23571_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5913_TI;
extern TypeInfo t5914_TI;
extern TypeInfo t5915_TI;
static TypeInfo* t3916_ITIs[] = 
{
	&t5913_TI,
	&t5914_TI,
	&t5915_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3916_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5913_TI, 21},
	{ &t5914_TI, 28},
	{ &t5915_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t3916_0_0_0;
extern Il2CppType t3916_1_0_0;
#include "t790.h"
extern TypeInfo t790_TI;
TypeInfo t3916_TI = 
{
	&g_System_dll_Image, NULL, "AsnDecodeStatus[]", "System.Security.Cryptography", t3916_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t790_TI, t3916_ITIs, t3916_VT, &EmptyCustomAttributesCache, &t44_TI, &t3916_0_0_0, &t3916_1_0_0, t3916_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 256, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t832_TI;



// Metadata Definition System.Text.RegularExpressions.Capture[]
static MethodInfo* t832_MIs[] =
{
	NULL
};
extern MethodInfo m23574_MI;
extern MethodInfo m23575_MI;
extern MethodInfo m23576_MI;
extern MethodInfo m23577_MI;
extern MethodInfo m23578_MI;
extern MethodInfo m23579_MI;
extern MethodInfo m23573_MI;
extern MethodInfo m23581_MI;
extern MethodInfo m23582_MI;
static MethodInfo* t832_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23574_MI,
	&m5907_MI,
	&m23575_MI,
	&m23576_MI,
	&m23577_MI,
	&m23578_MI,
	&m23579_MI,
	&m5908_MI,
	&m23573_MI,
	&m23581_MI,
	&m23582_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5916_TI;
extern TypeInfo t5917_TI;
extern TypeInfo t5918_TI;
static TypeInfo* t832_ITIs[] = 
{
	&t5916_TI,
	&t5917_TI,
	&t5918_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t832_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5916_TI, 21},
	{ &t5917_TI, 28},
	{ &t5918_TI, 33},
	{ &t2182_TI, 34},
	{ &t2186_TI, 41},
	{ &t2183_TI, 46},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t832_0_0_0;
extern Il2CppType t832_1_0_0;
struct t830;
extern TypeInfo t830_TI;
TypeInfo t832_TI = 
{
	&g_System_dll_Image, NULL, "Capture[]", "System.Text.RegularExpressions", t832_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t830_TI, t832_ITIs, t832_VT, &EmptyCustomAttributesCache, &t832_TI, &t832_0_0_0, &t832_1_0_0, t832_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t830 *), -1, 0, 0, -1, 1056769, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t835_TI;



// Metadata Definition System.Text.RegularExpressions.Group[]
static MethodInfo* t835_MIs[] =
{
	NULL
};
extern MethodInfo m23585_MI;
extern MethodInfo m23586_MI;
extern MethodInfo m23587_MI;
extern MethodInfo m23588_MI;
extern MethodInfo m23589_MI;
extern MethodInfo m23590_MI;
extern MethodInfo m23584_MI;
extern MethodInfo m23592_MI;
extern MethodInfo m23593_MI;
static MethodInfo* t835_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23585_MI,
	&m5907_MI,
	&m23586_MI,
	&m23587_MI,
	&m23588_MI,
	&m23589_MI,
	&m23590_MI,
	&m5908_MI,
	&m23584_MI,
	&m23592_MI,
	&m23593_MI,
	&m5905_MI,
	&m5906_MI,
	&m23574_MI,
	&m5907_MI,
	&m23575_MI,
	&m23576_MI,
	&m23577_MI,
	&m23578_MI,
	&m23579_MI,
	&m5908_MI,
	&m23573_MI,
	&m23581_MI,
	&m23582_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5919_TI;
extern TypeInfo t5920_TI;
extern TypeInfo t5921_TI;
static TypeInfo* t835_ITIs[] = 
{
	&t5919_TI,
	&t5920_TI,
	&t5921_TI,
	&t5916_TI,
	&t5917_TI,
	&t5918_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t835_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5919_TI, 21},
	{ &t5920_TI, 28},
	{ &t5921_TI, 33},
	{ &t5916_TI, 34},
	{ &t5917_TI, 41},
	{ &t5918_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t835_0_0_0;
extern Il2CppType t835_1_0_0;
struct t833;
extern TypeInfo t833_TI;
TypeInfo t835_TI = 
{
	&g_System_dll_Image, NULL, "Group[]", "System.Text.RegularExpressions", t835_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t833_TI, t835_ITIs, t835_VT, &EmptyCustomAttributesCache, &t835_TI, &t835_0_0_0, &t835_1_0_0, t835_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t833 *), -1, sizeof(t835_SFs), 0, -1, 1056769, 1, false, false, false, false, false, true, false, false, false, true, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3917_TI;



// Metadata Definition System.Text.RegularExpressions.RegexOptions[]
static MethodInfo* t3917_MIs[] =
{
	NULL
};
extern MethodInfo m23596_MI;
extern MethodInfo m23597_MI;
extern MethodInfo m23598_MI;
extern MethodInfo m23599_MI;
extern MethodInfo m23600_MI;
extern MethodInfo m23601_MI;
extern MethodInfo m23595_MI;
extern MethodInfo m23603_MI;
extern MethodInfo m23604_MI;
static MethodInfo* t3917_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23596_MI,
	&m5907_MI,
	&m23597_MI,
	&m23598_MI,
	&m23599_MI,
	&m23600_MI,
	&m23601_MI,
	&m5908_MI,
	&m23595_MI,
	&m23603_MI,
	&m23604_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5922_TI;
extern TypeInfo t5923_TI;
extern TypeInfo t5924_TI;
static TypeInfo* t3917_ITIs[] = 
{
	&t5922_TI,
	&t5923_TI,
	&t5924_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3917_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5922_TI, 21},
	{ &t5923_TI, 28},
	{ &t5924_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t3917_0_0_0;
extern Il2CppType t3917_1_0_0;
#include "t842.h"
extern TypeInfo t842_TI;
extern CustomAttributesCache t842__CustomAttributeCache;
TypeInfo t3917_TI = 
{
	&g_System_dll_Image, NULL, "RegexOptions[]", "System.Text.RegularExpressions", t3917_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t842_TI, t3917_ITIs, t3917_VT, &EmptyCustomAttributesCache, &t44_TI, &t3917_0_0_0, &t3917_1_0_0, t3917_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3918_TI;



// Metadata Definition System.Text.RegularExpressions.OpCode[]
static MethodInfo* t3918_MIs[] =
{
	NULL
};
extern MethodInfo m23609_MI;
extern MethodInfo m23610_MI;
extern MethodInfo m23611_MI;
extern MethodInfo m23612_MI;
extern MethodInfo m23613_MI;
extern MethodInfo m23614_MI;
extern MethodInfo m23608_MI;
extern MethodInfo m23616_MI;
extern MethodInfo m23617_MI;
static MethodInfo* t3918_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23609_MI,
	&m5907_MI,
	&m23610_MI,
	&m23611_MI,
	&m23612_MI,
	&m23613_MI,
	&m23614_MI,
	&m5908_MI,
	&m23608_MI,
	&m23616_MI,
	&m23617_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5925_TI;
extern TypeInfo t5926_TI;
extern TypeInfo t5927_TI;
static TypeInfo* t3918_ITIs[] = 
{
	&t5925_TI,
	&t5926_TI,
	&t5927_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3918_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5925_TI, 21},
	{ &t5926_TI, 28},
	{ &t5927_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t3918_0_0_0;
extern Il2CppType t3918_1_0_0;
#include "t843.h"
extern TypeInfo t843_TI;
extern TypeInfo t626_TI;
TypeInfo t3918_TI = 
{
	&g_System_dll_Image, NULL, "OpCode[]", "System.Text.RegularExpressions", t3918_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t843_TI, t3918_ITIs, t3918_VT, &EmptyCustomAttributesCache, &t626_TI, &t3918_0_0_0, &t3918_1_0_0, t3918_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (uint16_t), -1, 0, 0, -1, 256, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3919_TI;



// Metadata Definition System.Text.RegularExpressions.OpFlags[]
static MethodInfo* t3919_MIs[] =
{
	NULL
};
extern MethodInfo m23620_MI;
extern MethodInfo m23621_MI;
extern MethodInfo m23622_MI;
extern MethodInfo m23623_MI;
extern MethodInfo m23624_MI;
extern MethodInfo m23625_MI;
extern MethodInfo m23619_MI;
extern MethodInfo m23627_MI;
extern MethodInfo m23628_MI;
static MethodInfo* t3919_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23620_MI,
	&m5907_MI,
	&m23621_MI,
	&m23622_MI,
	&m23623_MI,
	&m23624_MI,
	&m23625_MI,
	&m5908_MI,
	&m23619_MI,
	&m23627_MI,
	&m23628_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5928_TI;
extern TypeInfo t5929_TI;
extern TypeInfo t5930_TI;
static TypeInfo* t3919_ITIs[] = 
{
	&t5928_TI,
	&t5929_TI,
	&t5930_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3919_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5928_TI, 21},
	{ &t5929_TI, 28},
	{ &t5930_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t3919_0_0_0;
extern Il2CppType t3919_1_0_0;
#include "t844.h"
extern TypeInfo t844_TI;
extern CustomAttributesCache t844__CustomAttributeCache;
TypeInfo t3919_TI = 
{
	&g_System_dll_Image, NULL, "OpFlags[]", "System.Text.RegularExpressions", t3919_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t844_TI, t3919_ITIs, t3919_VT, &EmptyCustomAttributesCache, &t626_TI, &t3919_0_0_0, &t3919_1_0_0, t3919_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (uint16_t), -1, 0, 0, -1, 256, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3920_TI;



// Metadata Definition System.Text.RegularExpressions.Position[]
static MethodInfo* t3920_MIs[] =
{
	NULL
};
extern MethodInfo m23631_MI;
extern MethodInfo m23632_MI;
extern MethodInfo m23633_MI;
extern MethodInfo m23634_MI;
extern MethodInfo m23635_MI;
extern MethodInfo m23636_MI;
extern MethodInfo m23630_MI;
extern MethodInfo m23638_MI;
extern MethodInfo m23639_MI;
static MethodInfo* t3920_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23631_MI,
	&m5907_MI,
	&m23632_MI,
	&m23633_MI,
	&m23634_MI,
	&m23635_MI,
	&m23636_MI,
	&m5908_MI,
	&m23630_MI,
	&m23638_MI,
	&m23639_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5931_TI;
extern TypeInfo t5932_TI;
extern TypeInfo t5933_TI;
static TypeInfo* t3920_ITIs[] = 
{
	&t5931_TI,
	&t5932_TI,
	&t5933_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3920_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5931_TI, 21},
	{ &t5932_TI, 28},
	{ &t5933_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t3920_0_0_0;
extern Il2CppType t3920_1_0_0;
#include "t845.h"
extern TypeInfo t845_TI;
TypeInfo t3920_TI = 
{
	&g_System_dll_Image, NULL, "Position[]", "System.Text.RegularExpressions", t3920_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t845_TI, t3920_ITIs, t3920_VT, &EmptyCustomAttributesCache, &t626_TI, &t3920_0_0_0, &t3920_1_0_0, t3920_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (uint16_t), -1, 0, 0, -1, 256, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3921_TI;



// Metadata Definition System.Text.RegularExpressions.Category[]
static MethodInfo* t3921_MIs[] =
{
	NULL
};
extern MethodInfo m23642_MI;
extern MethodInfo m23643_MI;
extern MethodInfo m23644_MI;
extern MethodInfo m23645_MI;
extern MethodInfo m23646_MI;
extern MethodInfo m23647_MI;
extern MethodInfo m23641_MI;
extern MethodInfo m23649_MI;
extern MethodInfo m23650_MI;
static MethodInfo* t3921_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23642_MI,
	&m5907_MI,
	&m23643_MI,
	&m23644_MI,
	&m23645_MI,
	&m23646_MI,
	&m23647_MI,
	&m5908_MI,
	&m23641_MI,
	&m23649_MI,
	&m23650_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5934_TI;
extern TypeInfo t5935_TI;
extern TypeInfo t5936_TI;
static TypeInfo* t3921_ITIs[] = 
{
	&t5934_TI,
	&t5935_TI,
	&t5936_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3921_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5934_TI, 21},
	{ &t5935_TI, 28},
	{ &t5936_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t3921_0_0_0;
extern Il2CppType t3921_1_0_0;
#include "t849.h"
extern TypeInfo t849_TI;
TypeInfo t3921_TI = 
{
	&g_System_dll_Image, NULL, "Category[]", "System.Text.RegularExpressions", t3921_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t849_TI, t3921_ITIs, t3921_VT, &EmptyCustomAttributesCache, &t626_TI, &t3921_0_0_0, &t3921_1_0_0, t3921_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (uint16_t), -1, 0, 0, -1, 256, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t865_TI;



// Metadata Definition System.Text.RegularExpressions.Mark[]
static MethodInfo* t865_MIs[] =
{
	NULL
};
extern MethodInfo m23653_MI;
extern MethodInfo m23654_MI;
extern MethodInfo m23655_MI;
extern MethodInfo m23656_MI;
extern MethodInfo m23657_MI;
extern MethodInfo m23658_MI;
extern MethodInfo m23652_MI;
extern MethodInfo m23660_MI;
extern MethodInfo m23661_MI;
static MethodInfo* t865_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23653_MI,
	&m5907_MI,
	&m23654_MI,
	&m23655_MI,
	&m23656_MI,
	&m23657_MI,
	&m23658_MI,
	&m5908_MI,
	&m23652_MI,
	&m23660_MI,
	&m23661_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5937_TI;
extern TypeInfo t5938_TI;
extern TypeInfo t5939_TI;
static TypeInfo* t865_ITIs[] = 
{
	&t5937_TI,
	&t5938_TI,
	&t5939_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t865_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5937_TI, 21},
	{ &t5938_TI, 28},
	{ &t5939_TI, 33},
	{ &t5116_TI, 34},
	{ &t5117_TI, 41},
	{ &t5118_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t865_0_0_0;
extern Il2CppType t865_1_0_0;
#include "t859.h"
extern TypeInfo t859_TI;
TypeInfo t865_TI = 
{
	&g_System_dll_Image, NULL, "Mark[]", "System.Text.RegularExpressions", t865_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t859_TI, t865_ITIs, t865_VT, &EmptyCustomAttributesCache, &t865_TI, &t865_0_0_0, &t865_1_0_0, t865_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t859 ), -1, 0, 0, -1, 1048840, 1, false, false, false, false, false, true, false, false, false, false, false, true, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3922_TI;



// Metadata Definition System.Text.RegularExpressions.Interpreter/Mode[]
static MethodInfo* t3922_MIs[] =
{
	NULL
};
extern MethodInfo m23664_MI;
extern MethodInfo m23665_MI;
extern MethodInfo m23666_MI;
extern MethodInfo m23667_MI;
extern MethodInfo m23668_MI;
extern MethodInfo m23669_MI;
extern MethodInfo m23663_MI;
extern MethodInfo m23671_MI;
extern MethodInfo m23672_MI;
static MethodInfo* t3922_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23664_MI,
	&m5907_MI,
	&m23665_MI,
	&m23666_MI,
	&m23667_MI,
	&m23668_MI,
	&m23669_MI,
	&m5908_MI,
	&m23663_MI,
	&m23671_MI,
	&m23672_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5940_TI;
extern TypeInfo t5941_TI;
extern TypeInfo t5942_TI;
static TypeInfo* t3922_ITIs[] = 
{
	&t5940_TI,
	&t5941_TI,
	&t5942_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3922_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5940_TI, 21},
	{ &t5941_TI, 28},
	{ &t5942_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t3922_0_0_0;
extern Il2CppType t3922_1_0_0;
#include "t862.h"
extern TypeInfo t862_TI;
TypeInfo t3922_TI = 
{
	&g_System_dll_Image, NULL, "Mode[]", "", t3922_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t862_TI, t3922_ITIs, t3922_VT, &EmptyCustomAttributesCache, &t44_TI, &t3922_0_0_0, &t3922_1_0_0, t3922_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 259, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t896_TI;



// Metadata Definition System.Uri/UriScheme[]
static MethodInfo* t896_MIs[] =
{
	NULL
};
extern MethodInfo m23675_MI;
extern MethodInfo m23676_MI;
extern MethodInfo m23677_MI;
extern MethodInfo m23678_MI;
extern MethodInfo m23679_MI;
extern MethodInfo m23680_MI;
extern MethodInfo m23674_MI;
extern MethodInfo m23682_MI;
extern MethodInfo m23683_MI;
static MethodInfo* t896_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23675_MI,
	&m5907_MI,
	&m23676_MI,
	&m23677_MI,
	&m23678_MI,
	&m23679_MI,
	&m23680_MI,
	&m5908_MI,
	&m23674_MI,
	&m23682_MI,
	&m23683_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5943_TI;
extern TypeInfo t5944_TI;
extern TypeInfo t5945_TI;
static TypeInfo* t896_ITIs[] = 
{
	&t5943_TI,
	&t5944_TI,
	&t5945_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t896_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5943_TI, 21},
	{ &t5944_TI, 28},
	{ &t5945_TI, 33},
	{ &t5116_TI, 34},
	{ &t5117_TI, 41},
	{ &t5118_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t896_0_0_0;
extern Il2CppType t896_1_0_0;
#include "t895.h"
extern TypeInfo t895_TI;
TypeInfo t896_TI = 
{
	&g_System_dll_Image, NULL, "UriScheme[]", "", t896_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t895_TI, t896_ITIs, t896_VT, &EmptyCustomAttributesCache, &t896_TI, &t896_0_0_0, &t896_1_0_0, t896_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t895 ), -1, 0, 0, -1, 1048843, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3923_TI;



// Metadata Definition System.UriHostNameType[]
static MethodInfo* t3923_MIs[] =
{
	NULL
};
extern MethodInfo m23686_MI;
extern MethodInfo m23687_MI;
extern MethodInfo m23688_MI;
extern MethodInfo m23689_MI;
extern MethodInfo m23690_MI;
extern MethodInfo m23691_MI;
extern MethodInfo m23685_MI;
extern MethodInfo m23693_MI;
extern MethodInfo m23694_MI;
static MethodInfo* t3923_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23686_MI,
	&m5907_MI,
	&m23687_MI,
	&m23688_MI,
	&m23689_MI,
	&m23690_MI,
	&m23691_MI,
	&m5908_MI,
	&m23685_MI,
	&m23693_MI,
	&m23694_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5946_TI;
extern TypeInfo t5947_TI;
extern TypeInfo t5948_TI;
static TypeInfo* t3923_ITIs[] = 
{
	&t5946_TI,
	&t5947_TI,
	&t5948_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3923_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5946_TI, 21},
	{ &t5947_TI, 28},
	{ &t5948_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t3923_0_0_0;
extern Il2CppType t3923_1_0_0;
#include "t897.h"
extern TypeInfo t897_TI;
TypeInfo t3923_TI = 
{
	&g_System_dll_Image, NULL, "UriHostNameType[]", "System", t3923_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t897_TI, t3923_ITIs, t3923_VT, &EmptyCustomAttributesCache, &t44_TI, &t3923_0_0_0, &t3923_1_0_0, t3923_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3924_TI;



// Metadata Definition System.UriKind[]
static MethodInfo* t3924_MIs[] =
{
	NULL
};
extern MethodInfo m23697_MI;
extern MethodInfo m23698_MI;
extern MethodInfo m23699_MI;
extern MethodInfo m23700_MI;
extern MethodInfo m23701_MI;
extern MethodInfo m23702_MI;
extern MethodInfo m23696_MI;
extern MethodInfo m23704_MI;
extern MethodInfo m23705_MI;
static MethodInfo* t3924_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23697_MI,
	&m5907_MI,
	&m23698_MI,
	&m23699_MI,
	&m23700_MI,
	&m23701_MI,
	&m23702_MI,
	&m5908_MI,
	&m23696_MI,
	&m23704_MI,
	&m23705_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5949_TI;
extern TypeInfo t5950_TI;
extern TypeInfo t5951_TI;
static TypeInfo* t3924_ITIs[] = 
{
	&t5949_TI,
	&t5950_TI,
	&t5951_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3924_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5949_TI, 21},
	{ &t5950_TI, 28},
	{ &t5951_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t3924_0_0_0;
extern Il2CppType t3924_1_0_0;
#include "t899.h"
extern TypeInfo t899_TI;
TypeInfo t3924_TI = 
{
	&g_System_dll_Image, NULL, "UriKind[]", "System", t3924_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t899_TI, t3924_ITIs, t3924_VT, &EmptyCustomAttributesCache, &t44_TI, &t3924_0_0_0, &t3924_1_0_0, t3924_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3925_TI;



// Metadata Definition System.UriPartial[]
static MethodInfo* t3925_MIs[] =
{
	NULL
};
extern MethodInfo m23708_MI;
extern MethodInfo m23709_MI;
extern MethodInfo m23710_MI;
extern MethodInfo m23711_MI;
extern MethodInfo m23712_MI;
extern MethodInfo m23713_MI;
extern MethodInfo m23707_MI;
extern MethodInfo m23715_MI;
extern MethodInfo m23716_MI;
static MethodInfo* t3925_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23708_MI,
	&m5907_MI,
	&m23709_MI,
	&m23710_MI,
	&m23711_MI,
	&m23712_MI,
	&m23713_MI,
	&m5908_MI,
	&m23707_MI,
	&m23715_MI,
	&m23716_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5952_TI;
extern TypeInfo t5953_TI;
extern TypeInfo t5954_TI;
static TypeInfo* t3925_ITIs[] = 
{
	&t5952_TI,
	&t5953_TI,
	&t5954_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3925_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5952_TI, 21},
	{ &t5953_TI, 28},
	{ &t5954_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t3925_0_0_0;
extern Il2CppType t3925_1_0_0;
#include "t898.h"
extern TypeInfo t898_TI;
TypeInfo t3925_TI = 
{
	&g_System_dll_Image, NULL, "UriPartial[]", "System", t3925_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t898_TI, t3925_ITIs, t3925_VT, &EmptyCustomAttributesCache, &t44_TI, &t3925_0_0_0, &t3925_1_0_0, t3925_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t975_TI;



// Metadata Definition System.UInt32[]
static MethodInfo* t975_MIs[] =
{
	NULL
};
extern MethodInfo m23719_MI;
extern MethodInfo m23720_MI;
extern MethodInfo m23721_MI;
extern MethodInfo m23722_MI;
extern MethodInfo m23723_MI;
extern MethodInfo m23724_MI;
extern MethodInfo m23718_MI;
extern MethodInfo m23726_MI;
extern MethodInfo m23727_MI;
extern MethodInfo m23730_MI;
extern MethodInfo m23731_MI;
extern MethodInfo m23732_MI;
extern MethodInfo m23733_MI;
extern MethodInfo m23734_MI;
extern MethodInfo m23735_MI;
extern MethodInfo m23729_MI;
extern MethodInfo m23737_MI;
extern MethodInfo m23738_MI;
extern MethodInfo m23741_MI;
extern MethodInfo m23742_MI;
extern MethodInfo m23743_MI;
extern MethodInfo m23744_MI;
extern MethodInfo m23745_MI;
extern MethodInfo m23746_MI;
extern MethodInfo m23740_MI;
extern MethodInfo m23748_MI;
extern MethodInfo m23749_MI;
static MethodInfo* t975_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23719_MI,
	&m5907_MI,
	&m23720_MI,
	&m23721_MI,
	&m23722_MI,
	&m23723_MI,
	&m23724_MI,
	&m5908_MI,
	&m23718_MI,
	&m23726_MI,
	&m23727_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m23730_MI,
	&m5907_MI,
	&m23731_MI,
	&m23732_MI,
	&m23733_MI,
	&m23734_MI,
	&m23735_MI,
	&m5908_MI,
	&m23729_MI,
	&m23737_MI,
	&m23738_MI,
	&m5905_MI,
	&m5906_MI,
	&m23741_MI,
	&m5907_MI,
	&m23742_MI,
	&m23743_MI,
	&m23744_MI,
	&m23745_MI,
	&m23746_MI,
	&m5908_MI,
	&m23740_MI,
	&m23748_MI,
	&m23749_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5955_TI;
extern TypeInfo t5956_TI;
extern TypeInfo t5957_TI;
extern TypeInfo t5958_TI;
extern TypeInfo t5959_TI;
extern TypeInfo t5960_TI;
extern TypeInfo t5961_TI;
extern TypeInfo t5962_TI;
extern TypeInfo t5963_TI;
static TypeInfo* t975_ITIs[] = 
{
	&t5955_TI,
	&t5956_TI,
	&t5957_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5958_TI,
	&t5959_TI,
	&t5960_TI,
	&t5961_TI,
	&t5962_TI,
	&t5963_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t975_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5955_TI, 21},
	{ &t5956_TI, 28},
	{ &t5957_TI, 33},
	{ &t5107_TI, 34},
	{ &t5108_TI, 41},
	{ &t5109_TI, 46},
	{ &t5110_TI, 47},
	{ &t5111_TI, 54},
	{ &t5112_TI, 59},
	{ &t5113_TI, 60},
	{ &t5114_TI, 67},
	{ &t5115_TI, 72},
	{ &t5958_TI, 73},
	{ &t5959_TI, 80},
	{ &t5960_TI, 85},
	{ &t5961_TI, 86},
	{ &t5962_TI, 93},
	{ &t5963_TI, 98},
	{ &t5116_TI, 99},
	{ &t5117_TI, 106},
	{ &t5118_TI, 111},
	{ &t2182_TI, 112},
	{ &t2186_TI, 119},
	{ &t2183_TI, 124},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t975_0_0_0;
extern Il2CppType t975_1_0_0;
#include "t344.h"
extern TypeInfo t344_TI;
extern CustomAttributesCache t344__CustomAttributeCache;
extern CustomAttributesCache t344__CustomAttributeCache_m5382;
extern CustomAttributesCache t344__CustomAttributeCache_m5383;
extern CustomAttributesCache t344__CustomAttributeCache_m4278;
extern CustomAttributesCache t344__CustomAttributeCache_m5384;
TypeInfo t975_TI = 
{
	&g_mscorlib_dll_Image, NULL, "UInt32[]", "System", t975_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t344_TI, t975_ITIs, t975_VT, &EmptyCustomAttributesCache, &t975_TI, &t975_0_0_0, &t975_1_0_0, t975_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (uint32_t), -1, 0, 0, -1, 1057033, 1, false, false, false, false, false, true, false, false, false, false, false, true, 0, 0, 0, 0, 0, 125, 24, 28};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3556_TI;



// Metadata Definition System.IComparable`1<System.UInt32>[]
static MethodInfo* t3556_MIs[] =
{
	NULL
};
static MethodInfo* t3556_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23730_MI,
	&m5907_MI,
	&m23731_MI,
	&m23732_MI,
	&m23733_MI,
	&m23734_MI,
	&m23735_MI,
	&m5908_MI,
	&m23729_MI,
	&m23737_MI,
	&m23738_MI,
};
static TypeInfo* t3556_ITIs[] = 
{
	&t5958_TI,
	&t5959_TI,
	&t5960_TI,
};
static Il2CppInterfaceOffsetPair t3556_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5958_TI, 21},
	{ &t5959_TI, 28},
	{ &t5960_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3556_0_0_0;
extern Il2CppType t3556_1_0_0;
struct t1716;
extern TypeInfo t1716_TI;
TypeInfo t3556_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1[]", "System", t3556_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1716_TI, t3556_ITIs, t3556_VT, &EmptyCustomAttributesCache, &t3556_TI, &t3556_0_0_0, &t3556_1_0_0, t3556_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29*), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3557_TI;



// Metadata Definition System.IEquatable`1<System.UInt32>[]
static MethodInfo* t3557_MIs[] =
{
	NULL
};
static MethodInfo* t3557_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23741_MI,
	&m5907_MI,
	&m23742_MI,
	&m23743_MI,
	&m23744_MI,
	&m23745_MI,
	&m23746_MI,
	&m5908_MI,
	&m23740_MI,
	&m23748_MI,
	&m23749_MI,
};
static TypeInfo* t3557_ITIs[] = 
{
	&t5961_TI,
	&t5962_TI,
	&t5963_TI,
};
static Il2CppInterfaceOffsetPair t3557_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5961_TI, 21},
	{ &t5962_TI, 28},
	{ &t5963_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3557_0_0_0;
extern Il2CppType t3557_1_0_0;
struct t1717;
extern TypeInfo t1717_TI;
TypeInfo t3557_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1[]", "System", t3557_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1717_TI, t3557_ITIs, t3557_VT, &EmptyCustomAttributesCache, &t3557_TI, &t3557_0_0_0, &t3557_1_0_0, t3557_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29*), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#include "Mono.Security_ArrayTypes.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t974_TI;



// Metadata Definition Mono.Math.BigInteger[]
static MethodInfo* t974_MIs[] =
{
	NULL
};
extern MethodInfo m23752_MI;
extern MethodInfo m23753_MI;
extern MethodInfo m23754_MI;
extern MethodInfo m23755_MI;
extern MethodInfo m23756_MI;
extern MethodInfo m23757_MI;
extern MethodInfo m23751_MI;
extern MethodInfo m23759_MI;
extern MethodInfo m23760_MI;
static MethodInfo* t974_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23752_MI,
	&m5907_MI,
	&m23753_MI,
	&m23754_MI,
	&m23755_MI,
	&m23756_MI,
	&m23757_MI,
	&m5908_MI,
	&m23751_MI,
	&m23759_MI,
	&m23760_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5964_TI;
extern TypeInfo t5965_TI;
extern TypeInfo t5966_TI;
static TypeInfo* t974_ITIs[] = 
{
	&t5964_TI,
	&t5965_TI,
	&t5966_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t974_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5964_TI, 21},
	{ &t5965_TI, 28},
	{ &t5966_TI, 33},
	{ &t2182_TI, 34},
	{ &t2186_TI, 41},
	{ &t2183_TI, 46},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType t974_0_0_0;
extern Il2CppType t974_1_0_0;
struct t972;
extern TypeInfo t972_TI;
extern CustomAttributesCache t972__CustomAttributeCache_m4315;
extern CustomAttributesCache t972__CustomAttributeCache_m4317;
extern CustomAttributesCache t972__CustomAttributeCache_m4319;
extern CustomAttributesCache t972__CustomAttributeCache_m4326;
extern CustomAttributesCache t972__CustomAttributeCache_m4327;
extern CustomAttributesCache t972__CustomAttributeCache_m4330;
extern CustomAttributesCache t972__CustomAttributeCache_m4331;
extern CustomAttributesCache t972__CustomAttributeCache_m4341;
extern CustomAttributesCache t972__CustomAttributeCache_m4345;
extern CustomAttributesCache t972__CustomAttributeCache_m4351;
extern CustomAttributesCache t972__CustomAttributeCache_m4352;
TypeInfo t974_TI = 
{
	&g_Mono_Security_dll_Image, NULL, "BigInteger[]", "Mono.Math", t974_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t972_TI, t974_ITIs, t974_VT, &EmptyCustomAttributesCache, &t974_TI, &t974_0_0_0, &t974_1_0_0, t974_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t972 *), -1, sizeof(t974_SFs), 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, true, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3926_TI;



// Metadata Definition Mono.Math.BigInteger/Sign[]
static MethodInfo* t3926_MIs[] =
{
	NULL
};
extern MethodInfo m23763_MI;
extern MethodInfo m23764_MI;
extern MethodInfo m23765_MI;
extern MethodInfo m23766_MI;
extern MethodInfo m23767_MI;
extern MethodInfo m23768_MI;
extern MethodInfo m23762_MI;
extern MethodInfo m23770_MI;
extern MethodInfo m23771_MI;
static MethodInfo* t3926_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23763_MI,
	&m5907_MI,
	&m23764_MI,
	&m23765_MI,
	&m23766_MI,
	&m23767_MI,
	&m23768_MI,
	&m5908_MI,
	&m23762_MI,
	&m23770_MI,
	&m23771_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5967_TI;
extern TypeInfo t5968_TI;
extern TypeInfo t5969_TI;
static TypeInfo* t3926_ITIs[] = 
{
	&t5967_TI,
	&t5968_TI,
	&t5969_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3926_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5967_TI, 21},
	{ &t5968_TI, 28},
	{ &t5969_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType t3926_0_0_0;
extern Il2CppType t3926_1_0_0;
#include "t970.h"
extern TypeInfo t970_TI;
TypeInfo t3926_TI = 
{
	&g_Mono_Security_dll_Image, NULL, "Sign[]", "", t3926_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t970_TI, t3926_ITIs, t3926_VT, &EmptyCustomAttributesCache, &t44_TI, &t3926_0_0_0, &t3926_1_0_0, t3926_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 258, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3927_TI;



// Metadata Definition Mono.Math.Prime.ConfidenceFactor[]
static MethodInfo* t3927_MIs[] =
{
	NULL
};
extern MethodInfo m23774_MI;
extern MethodInfo m23775_MI;
extern MethodInfo m23776_MI;
extern MethodInfo m23777_MI;
extern MethodInfo m23778_MI;
extern MethodInfo m23779_MI;
extern MethodInfo m23773_MI;
extern MethodInfo m23781_MI;
extern MethodInfo m23782_MI;
static MethodInfo* t3927_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23774_MI,
	&m5907_MI,
	&m23775_MI,
	&m23776_MI,
	&m23777_MI,
	&m23778_MI,
	&m23779_MI,
	&m5908_MI,
	&m23773_MI,
	&m23781_MI,
	&m23782_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5970_TI;
extern TypeInfo t5971_TI;
extern TypeInfo t5972_TI;
static TypeInfo* t3927_ITIs[] = 
{
	&t5970_TI,
	&t5971_TI,
	&t5972_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3927_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5970_TI, 21},
	{ &t5971_TI, 28},
	{ &t5972_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType t3927_0_0_0;
extern Il2CppType t3927_1_0_0;
#include "t977.h"
extern TypeInfo t977_TI;
TypeInfo t3927_TI = 
{
	&g_Mono_Security_dll_Image, NULL, "ConfidenceFactor[]", "Mono.Math.Prime", t3927_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t977_TI, t3927_ITIs, t3927_VT, &EmptyCustomAttributesCache, &t44_TI, &t3927_0_0_0, &t3927_1_0_0, t3927_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t996_TI;



// Metadata Definition System.Security.Cryptography.KeySizes[]
static MethodInfo* t996_MIs[] =
{
	NULL
};
extern MethodInfo m23785_MI;
extern MethodInfo m23786_MI;
extern MethodInfo m23787_MI;
extern MethodInfo m23788_MI;
extern MethodInfo m23789_MI;
extern MethodInfo m23790_MI;
extern MethodInfo m23784_MI;
extern MethodInfo m23792_MI;
extern MethodInfo m23793_MI;
static MethodInfo* t996_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23785_MI,
	&m5907_MI,
	&m23786_MI,
	&m23787_MI,
	&m23788_MI,
	&m23789_MI,
	&m23790_MI,
	&m5908_MI,
	&m23784_MI,
	&m23792_MI,
	&m23793_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5973_TI;
extern TypeInfo t5974_TI;
extern TypeInfo t5975_TI;
static TypeInfo* t996_ITIs[] = 
{
	&t5973_TI,
	&t5974_TI,
	&t5975_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t996_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5973_TI, 21},
	{ &t5974_TI, 28},
	{ &t5975_TI, 33},
	{ &t2182_TI, 34},
	{ &t2186_TI, 41},
	{ &t2183_TI, 46},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t996_0_0_0;
extern Il2CppType t996_1_0_0;
struct t1100;
extern TypeInfo t1100_TI;
TypeInfo t996_TI = 
{
	&g_mscorlib_dll_Image, NULL, "KeySizes[]", "System.Security.Cryptography", t996_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1100_TI, t996_ITIs, t996_VT, &EmptyCustomAttributesCache, &t996_TI, &t996_0_0_0, &t996_1_0_0, t996_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1100 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3928_TI;



// Metadata Definition Mono.Security.X509.X509ChainStatusFlags[]
static MethodInfo* t3928_MIs[] =
{
	NULL
};
extern MethodInfo m23796_MI;
extern MethodInfo m23797_MI;
extern MethodInfo m23798_MI;
extern MethodInfo m23799_MI;
extern MethodInfo m23800_MI;
extern MethodInfo m23801_MI;
extern MethodInfo m23795_MI;
extern MethodInfo m23803_MI;
extern MethodInfo m23804_MI;
static MethodInfo* t3928_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23796_MI,
	&m5907_MI,
	&m23797_MI,
	&m23798_MI,
	&m23799_MI,
	&m23800_MI,
	&m23801_MI,
	&m5908_MI,
	&m23795_MI,
	&m23803_MI,
	&m23804_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5976_TI;
extern TypeInfo t5977_TI;
extern TypeInfo t5978_TI;
static TypeInfo* t3928_ITIs[] = 
{
	&t5976_TI,
	&t5977_TI,
	&t5978_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3928_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5976_TI, 21},
	{ &t5977_TI, 28},
	{ &t5978_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType t3928_0_0_0;
extern Il2CppType t3928_1_0_0;
#include "t1003.h"
extern TypeInfo t1003_TI;
extern CustomAttributesCache t1003__CustomAttributeCache;
TypeInfo t3928_TI = 
{
	&g_Mono_Security_dll_Image, NULL, "X509ChainStatusFlags[]", "Mono.Security.X509", t3928_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1003_TI, t3928_ITIs, t3928_VT, &EmptyCustomAttributesCache, &t44_TI, &t3928_0_0_0, &t3928_1_0_0, t3928_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3929_TI;



// Metadata Definition Mono.Security.X509.Extensions.KeyUsages[]
static MethodInfo* t3929_MIs[] =
{
	NULL
};
extern MethodInfo m23807_MI;
extern MethodInfo m23808_MI;
extern MethodInfo m23809_MI;
extern MethodInfo m23810_MI;
extern MethodInfo m23811_MI;
extern MethodInfo m23812_MI;
extern MethodInfo m23806_MI;
extern MethodInfo m23814_MI;
extern MethodInfo m23815_MI;
static MethodInfo* t3929_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23807_MI,
	&m5907_MI,
	&m23808_MI,
	&m23809_MI,
	&m23810_MI,
	&m23811_MI,
	&m23812_MI,
	&m5908_MI,
	&m23806_MI,
	&m23814_MI,
	&m23815_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5979_TI;
extern TypeInfo t5980_TI;
extern TypeInfo t5981_TI;
static TypeInfo* t3929_ITIs[] = 
{
	&t5979_TI,
	&t5980_TI,
	&t5981_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3929_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5979_TI, 21},
	{ &t5980_TI, 28},
	{ &t5981_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType t3929_0_0_0;
extern Il2CppType t3929_1_0_0;
#include "t1007.h"
extern TypeInfo t1007_TI;
extern CustomAttributesCache t1007__CustomAttributeCache;
TypeInfo t3929_TI = 
{
	&g_Mono_Security_dll_Image, NULL, "KeyUsages[]", "Mono.Security.X509.Extensions", t3929_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1007_TI, t3929_ITIs, t3929_VT, &EmptyCustomAttributesCache, &t44_TI, &t3929_0_0_0, &t3929_1_0_0, t3929_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3930_TI;



// Metadata Definition Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes[]
static MethodInfo* t3930_MIs[] =
{
	NULL
};
extern MethodInfo m23818_MI;
extern MethodInfo m23819_MI;
extern MethodInfo m23820_MI;
extern MethodInfo m23821_MI;
extern MethodInfo m23822_MI;
extern MethodInfo m23823_MI;
extern MethodInfo m23817_MI;
extern MethodInfo m23825_MI;
extern MethodInfo m23826_MI;
static MethodInfo* t3930_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23818_MI,
	&m5907_MI,
	&m23819_MI,
	&m23820_MI,
	&m23821_MI,
	&m23822_MI,
	&m23823_MI,
	&m5908_MI,
	&m23817_MI,
	&m23825_MI,
	&m23826_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5982_TI;
extern TypeInfo t5983_TI;
extern TypeInfo t5984_TI;
static TypeInfo* t3930_ITIs[] = 
{
	&t5982_TI,
	&t5983_TI,
	&t5984_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3930_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5982_TI, 21},
	{ &t5983_TI, 28},
	{ &t5984_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType t3930_0_0_0;
extern Il2CppType t3930_1_0_0;
#include "t1009.h"
extern TypeInfo t1009_TI;
extern CustomAttributesCache t1009__CustomAttributeCache;
TypeInfo t3930_TI = 
{
	&g_Mono_Security_dll_Image, NULL, "CertTypes[]", "", t3930_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1009_TI, t3930_ITIs, t3930_VT, &EmptyCustomAttributesCache, &t44_TI, &t3930_0_0_0, &t3930_1_0_0, t3930_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 258, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3931_TI;



// Metadata Definition Mono.Security.Protocol.Tls.AlertLevel[]
static MethodInfo* t3931_MIs[] =
{
	NULL
};
extern MethodInfo m23829_MI;
extern MethodInfo m23830_MI;
extern MethodInfo m23831_MI;
extern MethodInfo m23832_MI;
extern MethodInfo m23833_MI;
extern MethodInfo m23834_MI;
extern MethodInfo m23828_MI;
extern MethodInfo m23836_MI;
extern MethodInfo m23837_MI;
static MethodInfo* t3931_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23829_MI,
	&m5907_MI,
	&m23830_MI,
	&m23831_MI,
	&m23832_MI,
	&m23833_MI,
	&m23834_MI,
	&m5908_MI,
	&m23828_MI,
	&m23836_MI,
	&m23837_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5985_TI;
extern TypeInfo t5986_TI;
extern TypeInfo t5987_TI;
static TypeInfo* t3931_ITIs[] = 
{
	&t5985_TI,
	&t5986_TI,
	&t5987_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3931_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5985_TI, 21},
	{ &t5986_TI, 28},
	{ &t5987_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType t3931_0_0_0;
extern Il2CppType t3931_1_0_0;
#include "t1015.h"
extern TypeInfo t1015_TI;
extern TypeInfo t348_TI;
TypeInfo t3931_TI = 
{
	&g_Mono_Security_dll_Image, NULL, "AlertLevel[]", "Mono.Security.Protocol.Tls", t3931_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1015_TI, t3931_ITIs, t3931_VT, &EmptyCustomAttributesCache, &t348_TI, &t3931_0_0_0, &t3931_1_0_0, t3931_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (uint8_t), -1, 0, 0, -1, 8448, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3932_TI;



// Metadata Definition Mono.Security.Protocol.Tls.AlertDescription[]
static MethodInfo* t3932_MIs[] =
{
	NULL
};
extern MethodInfo m23840_MI;
extern MethodInfo m23841_MI;
extern MethodInfo m23842_MI;
extern MethodInfo m23843_MI;
extern MethodInfo m23844_MI;
extern MethodInfo m23845_MI;
extern MethodInfo m23839_MI;
extern MethodInfo m23847_MI;
extern MethodInfo m23848_MI;
static MethodInfo* t3932_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23840_MI,
	&m5907_MI,
	&m23841_MI,
	&m23842_MI,
	&m23843_MI,
	&m23844_MI,
	&m23845_MI,
	&m5908_MI,
	&m23839_MI,
	&m23847_MI,
	&m23848_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5988_TI;
extern TypeInfo t5989_TI;
extern TypeInfo t5990_TI;
static TypeInfo* t3932_ITIs[] = 
{
	&t5988_TI,
	&t5989_TI,
	&t5990_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3932_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5988_TI, 21},
	{ &t5989_TI, 28},
	{ &t5990_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType t3932_0_0_0;
extern Il2CppType t3932_1_0_0;
#include "t1016.h"
extern TypeInfo t1016_TI;
TypeInfo t3932_TI = 
{
	&g_Mono_Security_dll_Image, NULL, "AlertDescription[]", "Mono.Security.Protocol.Tls", t3932_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1016_TI, t3932_ITIs, t3932_VT, &EmptyCustomAttributesCache, &t348_TI, &t3932_0_0_0, &t3932_1_0_0, t3932_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (uint8_t), -1, 0, 0, -1, 8448, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3933_TI;



// Metadata Definition Mono.Security.Protocol.Tls.CipherAlgorithmType[]
static MethodInfo* t3933_MIs[] =
{
	NULL
};
extern MethodInfo m23851_MI;
extern MethodInfo m23852_MI;
extern MethodInfo m23853_MI;
extern MethodInfo m23854_MI;
extern MethodInfo m23855_MI;
extern MethodInfo m23856_MI;
extern MethodInfo m23850_MI;
extern MethodInfo m23858_MI;
extern MethodInfo m23859_MI;
static MethodInfo* t3933_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23851_MI,
	&m5907_MI,
	&m23852_MI,
	&m23853_MI,
	&m23854_MI,
	&m23855_MI,
	&m23856_MI,
	&m5908_MI,
	&m23850_MI,
	&m23858_MI,
	&m23859_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5991_TI;
extern TypeInfo t5992_TI;
extern TypeInfo t5993_TI;
static TypeInfo* t3933_ITIs[] = 
{
	&t5991_TI,
	&t5992_TI,
	&t5993_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3933_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5991_TI, 21},
	{ &t5992_TI, 28},
	{ &t5993_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType t3933_0_0_0;
extern Il2CppType t3933_1_0_0;
#include "t1018.h"
extern TypeInfo t1018_TI;
TypeInfo t3933_TI = 
{
	&g_Mono_Security_dll_Image, NULL, "CipherAlgorithmType[]", "Mono.Security.Protocol.Tls", t3933_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1018_TI, t3933_ITIs, t3933_VT, &EmptyCustomAttributesCache, &t44_TI, &t3933_0_0_0, &t3933_1_0_0, t3933_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1118_TI;



// Metadata Definition System.Byte[][]
static MethodInfo* t1118_MIs[] =
{
	NULL
};
extern MethodInfo m23862_MI;
extern MethodInfo m23863_MI;
extern MethodInfo m23864_MI;
extern MethodInfo m23865_MI;
extern MethodInfo m23866_MI;
extern MethodInfo m23867_MI;
extern MethodInfo m23861_MI;
extern MethodInfo m23869_MI;
extern MethodInfo m23870_MI;
extern MethodInfo m23387_MI;
extern MethodInfo m23388_MI;
extern MethodInfo m23389_MI;
extern MethodInfo m23390_MI;
extern MethodInfo m23391_MI;
extern MethodInfo m23392_MI;
extern MethodInfo m23386_MI;
extern MethodInfo m23394_MI;
extern MethodInfo m23395_MI;
extern MethodInfo m23398_MI;
extern MethodInfo m23399_MI;
extern MethodInfo m23400_MI;
extern MethodInfo m23401_MI;
extern MethodInfo m23402_MI;
extern MethodInfo m23403_MI;
extern MethodInfo m23397_MI;
extern MethodInfo m23405_MI;
extern MethodInfo m23406_MI;
static MethodInfo* t1118_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23862_MI,
	&m5907_MI,
	&m23863_MI,
	&m23864_MI,
	&m23865_MI,
	&m23866_MI,
	&m23867_MI,
	&m5908_MI,
	&m23861_MI,
	&m23869_MI,
	&m23870_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m23387_MI,
	&m5907_MI,
	&m23388_MI,
	&m23389_MI,
	&m23390_MI,
	&m23391_MI,
	&m23392_MI,
	&m5908_MI,
	&m23386_MI,
	&m23394_MI,
	&m23395_MI,
	&m5905_MI,
	&m5906_MI,
	&m23398_MI,
	&m5907_MI,
	&m23399_MI,
	&m23400_MI,
	&m23401_MI,
	&m23402_MI,
	&m23403_MI,
	&m5908_MI,
	&m23397_MI,
	&m23405_MI,
	&m23406_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5994_TI;
extern TypeInfo t5995_TI;
extern TypeInfo t5996_TI;
extern TypeInfo t5865_TI;
extern TypeInfo t5866_TI;
extern TypeInfo t5867_TI;
extern TypeInfo t5868_TI;
extern TypeInfo t5869_TI;
extern TypeInfo t5870_TI;
static TypeInfo* t1118_ITIs[] = 
{
	&t5994_TI,
	&t5995_TI,
	&t5996_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5865_TI,
	&t5866_TI,
	&t5867_TI,
	&t5868_TI,
	&t5869_TI,
	&t5870_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t1118_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5994_TI, 21},
	{ &t5995_TI, 28},
	{ &t5996_TI, 33},
	{ &t5107_TI, 34},
	{ &t5108_TI, 41},
	{ &t5109_TI, 46},
	{ &t5110_TI, 47},
	{ &t5111_TI, 54},
	{ &t5112_TI, 59},
	{ &t5113_TI, 60},
	{ &t5114_TI, 67},
	{ &t5115_TI, 72},
	{ &t5865_TI, 73},
	{ &t5866_TI, 80},
	{ &t5867_TI, 85},
	{ &t5868_TI, 86},
	{ &t5869_TI, 93},
	{ &t5870_TI, 98},
	{ &t5116_TI, 99},
	{ &t5117_TI, 106},
	{ &t5118_TI, 111},
	{ &t2182_TI, 112},
	{ &t2186_TI, 119},
	{ &t2183_TI, 124},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1118_0_0_0;
extern Il2CppType t1118_1_0_0;
struct t781;
extern TypeInfo t781_TI;
extern CustomAttributesCache t348__CustomAttributeCache;
TypeInfo t1118_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Byte[][]", "System", t1118_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t781_TI, t1118_ITIs, t1118_VT, &EmptyCustomAttributesCache, &t1118_TI, &t1118_0_0_0, &t1118_1_0_0, t1118_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t781*), -1, 0, 0, -1, 1057033, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 125, 24, 28};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3934_TI;



// Metadata Definition Mono.Security.Protocol.Tls.ContentType[]
static MethodInfo* t3934_MIs[] =
{
	NULL
};
extern MethodInfo m23873_MI;
extern MethodInfo m23874_MI;
extern MethodInfo m23875_MI;
extern MethodInfo m23876_MI;
extern MethodInfo m23877_MI;
extern MethodInfo m23878_MI;
extern MethodInfo m23872_MI;
extern MethodInfo m23880_MI;
extern MethodInfo m23881_MI;
static MethodInfo* t3934_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23873_MI,
	&m5907_MI,
	&m23874_MI,
	&m23875_MI,
	&m23876_MI,
	&m23877_MI,
	&m23878_MI,
	&m5908_MI,
	&m23872_MI,
	&m23880_MI,
	&m23881_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5997_TI;
extern TypeInfo t5998_TI;
extern TypeInfo t5999_TI;
static TypeInfo* t3934_ITIs[] = 
{
	&t5997_TI,
	&t5998_TI,
	&t5999_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3934_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5997_TI, 21},
	{ &t5998_TI, 28},
	{ &t5999_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType t3934_0_0_0;
extern Il2CppType t3934_1_0_0;
#include "t1024.h"
extern TypeInfo t1024_TI;
TypeInfo t3934_TI = 
{
	&g_Mono_Security_dll_Image, NULL, "ContentType[]", "Mono.Security.Protocol.Tls", t3934_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1024_TI, t3934_ITIs, t3934_VT, &EmptyCustomAttributesCache, &t348_TI, &t3934_0_0_0, &t3934_1_0_0, t3934_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (uint8_t), -1, 0, 0, -1, 8448, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3935_TI;



// Metadata Definition Mono.Security.Protocol.Tls.ExchangeAlgorithmType[]
static MethodInfo* t3935_MIs[] =
{
	NULL
};
extern MethodInfo m23884_MI;
extern MethodInfo m23885_MI;
extern MethodInfo m23886_MI;
extern MethodInfo m23887_MI;
extern MethodInfo m23888_MI;
extern MethodInfo m23889_MI;
extern MethodInfo m23883_MI;
extern MethodInfo m23891_MI;
extern MethodInfo m23892_MI;
static MethodInfo* t3935_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23884_MI,
	&m5907_MI,
	&m23885_MI,
	&m23886_MI,
	&m23887_MI,
	&m23888_MI,
	&m23889_MI,
	&m5908_MI,
	&m23883_MI,
	&m23891_MI,
	&m23892_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6000_TI;
extern TypeInfo t6001_TI;
extern TypeInfo t6002_TI;
static TypeInfo* t3935_ITIs[] = 
{
	&t6000_TI,
	&t6001_TI,
	&t6002_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3935_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6000_TI, 21},
	{ &t6001_TI, 28},
	{ &t6002_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType t3935_0_0_0;
extern Il2CppType t3935_1_0_0;
#include "t1022.h"
extern TypeInfo t1022_TI;
TypeInfo t3935_TI = 
{
	&g_Mono_Security_dll_Image, NULL, "ExchangeAlgorithmType[]", "Mono.Security.Protocol.Tls", t3935_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1022_TI, t3935_ITIs, t3935_VT, &EmptyCustomAttributesCache, &t44_TI, &t3935_0_0_0, &t3935_1_0_0, t3935_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3936_TI;



// Metadata Definition Mono.Security.Protocol.Tls.HandshakeState[]
static MethodInfo* t3936_MIs[] =
{
	NULL
};
extern MethodInfo m23895_MI;
extern MethodInfo m23896_MI;
extern MethodInfo m23897_MI;
extern MethodInfo m23898_MI;
extern MethodInfo m23899_MI;
extern MethodInfo m23900_MI;
extern MethodInfo m23894_MI;
extern MethodInfo m23902_MI;
extern MethodInfo m23903_MI;
static MethodInfo* t3936_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23895_MI,
	&m5907_MI,
	&m23896_MI,
	&m23897_MI,
	&m23898_MI,
	&m23899_MI,
	&m23900_MI,
	&m5908_MI,
	&m23894_MI,
	&m23902_MI,
	&m23903_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6003_TI;
extern TypeInfo t6004_TI;
extern TypeInfo t6005_TI;
static TypeInfo* t3936_ITIs[] = 
{
	&t6003_TI,
	&t6004_TI,
	&t6005_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3936_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6003_TI, 21},
	{ &t6004_TI, 28},
	{ &t6005_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType t3936_0_0_0;
extern Il2CppType t3936_1_0_0;
#include "t1044.h"
extern TypeInfo t1044_TI;
TypeInfo t3936_TI = 
{
	&g_Mono_Security_dll_Image, NULL, "HandshakeState[]", "Mono.Security.Protocol.Tls", t3936_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1044_TI, t3936_ITIs, t3936_VT, &EmptyCustomAttributesCache, &t44_TI, &t3936_0_0_0, &t3936_1_0_0, t3936_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8448, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3937_TI;



// Metadata Definition Mono.Security.Protocol.Tls.HashAlgorithmType[]
static MethodInfo* t3937_MIs[] =
{
	NULL
};
extern MethodInfo m23906_MI;
extern MethodInfo m23907_MI;
extern MethodInfo m23908_MI;
extern MethodInfo m23909_MI;
extern MethodInfo m23910_MI;
extern MethodInfo m23911_MI;
extern MethodInfo m23905_MI;
extern MethodInfo m23913_MI;
extern MethodInfo m23914_MI;
static MethodInfo* t3937_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23906_MI,
	&m5907_MI,
	&m23907_MI,
	&m23908_MI,
	&m23909_MI,
	&m23910_MI,
	&m23911_MI,
	&m5908_MI,
	&m23905_MI,
	&m23913_MI,
	&m23914_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6006_TI;
extern TypeInfo t6007_TI;
extern TypeInfo t6008_TI;
static TypeInfo* t3937_ITIs[] = 
{
	&t6006_TI,
	&t6007_TI,
	&t6008_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3937_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6006_TI, 21},
	{ &t6007_TI, 28},
	{ &t6008_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType t3937_0_0_0;
extern Il2CppType t3937_1_0_0;
#include "t1021.h"
extern TypeInfo t1021_TI;
TypeInfo t3937_TI = 
{
	&g_Mono_Security_dll_Image, NULL, "HashAlgorithmType[]", "Mono.Security.Protocol.Tls", t3937_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1021_TI, t3937_ITIs, t3937_VT, &EmptyCustomAttributesCache, &t44_TI, &t3937_0_0_0, &t3937_1_0_0, t3937_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3938_TI;



// Metadata Definition Mono.Security.Protocol.Tls.SecurityCompressionType[]
static MethodInfo* t3938_MIs[] =
{
	NULL
};
extern MethodInfo m23917_MI;
extern MethodInfo m23918_MI;
extern MethodInfo m23919_MI;
extern MethodInfo m23920_MI;
extern MethodInfo m23921_MI;
extern MethodInfo m23922_MI;
extern MethodInfo m23916_MI;
extern MethodInfo m23924_MI;
extern MethodInfo m23925_MI;
static MethodInfo* t3938_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23917_MI,
	&m5907_MI,
	&m23918_MI,
	&m23919_MI,
	&m23920_MI,
	&m23921_MI,
	&m23922_MI,
	&m5908_MI,
	&m23916_MI,
	&m23924_MI,
	&m23925_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6009_TI;
extern TypeInfo t6010_TI;
extern TypeInfo t6011_TI;
static TypeInfo* t3938_ITIs[] = 
{
	&t6009_TI,
	&t6010_TI,
	&t6011_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3938_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6009_TI, 21},
	{ &t6010_TI, 28},
	{ &t6011_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType t3938_0_0_0;
extern Il2CppType t3938_1_0_0;
#include "t1043.h"
extern TypeInfo t1043_TI;
TypeInfo t3938_TI = 
{
	&g_Mono_Security_dll_Image, NULL, "SecurityCompressionType[]", "Mono.Security.Protocol.Tls", t3938_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1043_TI, t3938_ITIs, t3938_VT, &EmptyCustomAttributesCache, &t44_TI, &t3938_0_0_0, &t3938_1_0_0, t3938_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3939_TI;



// Metadata Definition Mono.Security.Protocol.Tls.SecurityProtocolType[]
static MethodInfo* t3939_MIs[] =
{
	NULL
};
extern MethodInfo m23928_MI;
extern MethodInfo m23929_MI;
extern MethodInfo m23930_MI;
extern MethodInfo m23931_MI;
extern MethodInfo m23932_MI;
extern MethodInfo m23933_MI;
extern MethodInfo m23927_MI;
extern MethodInfo m23935_MI;
extern MethodInfo m23936_MI;
static MethodInfo* t3939_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23928_MI,
	&m5907_MI,
	&m23929_MI,
	&m23930_MI,
	&m23931_MI,
	&m23932_MI,
	&m23933_MI,
	&m5908_MI,
	&m23927_MI,
	&m23935_MI,
	&m23936_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6012_TI;
extern TypeInfo t6013_TI;
extern TypeInfo t6014_TI;
static TypeInfo* t3939_ITIs[] = 
{
	&t6012_TI,
	&t6013_TI,
	&t6014_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3939_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6012_TI, 21},
	{ &t6013_TI, 28},
	{ &t6014_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType t3939_0_0_0;
extern Il2CppType t3939_1_0_0;
#include "t1026.h"
extern TypeInfo t1026_TI;
extern CustomAttributesCache t1026__CustomAttributeCache;
TypeInfo t3939_TI = 
{
	&g_Mono_Security_dll_Image, NULL, "SecurityProtocolType[]", "Mono.Security.Protocol.Tls", t3939_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1026_TI, t3939_ITIs, t3939_VT, &EmptyCustomAttributesCache, &t44_TI, &t3939_0_0_0, &t3939_1_0_0, t3939_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8449, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1066_TI;



// Metadata Definition Mono.Security.Protocol.Tls.Handshake.ClientCertificateType[]
static MethodInfo* t1066_MIs[] =
{
	NULL
};
extern MethodInfo m23939_MI;
extern MethodInfo m23940_MI;
extern MethodInfo m23941_MI;
extern MethodInfo m23942_MI;
extern MethodInfo m23943_MI;
extern MethodInfo m23944_MI;
extern MethodInfo m23938_MI;
extern MethodInfo m23946_MI;
extern MethodInfo m23947_MI;
static MethodInfo* t1066_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23939_MI,
	&m5907_MI,
	&m23940_MI,
	&m23941_MI,
	&m23942_MI,
	&m23943_MI,
	&m23944_MI,
	&m5908_MI,
	&m23938_MI,
	&m23946_MI,
	&m23947_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6015_TI;
extern TypeInfo t6016_TI;
extern TypeInfo t6017_TI;
static TypeInfo* t1066_ITIs[] = 
{
	&t6015_TI,
	&t6016_TI,
	&t6017_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t1066_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6015_TI, 21},
	{ &t6016_TI, 28},
	{ &t6017_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType t1066_0_0_0;
extern Il2CppType t1066_1_0_0;
#include "t1067.h"
extern TypeInfo t1067_TI;
TypeInfo t1066_TI = 
{
	&g_Mono_Security_dll_Image, NULL, "ClientCertificateType[]", "Mono.Security.Protocol.Tls.Handshake", t1066_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1067_TI, t1066_ITIs, t1066_VT, &EmptyCustomAttributesCache, &t44_TI, &t1066_0_0_0, &t1066_1_0_0, t1066_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 8448, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3940_TI;



// Metadata Definition Mono.Security.Protocol.Tls.Handshake.HandshakeType[]
static MethodInfo* t3940_MIs[] =
{
	NULL
};
extern MethodInfo m23950_MI;
extern MethodInfo m23951_MI;
extern MethodInfo m23952_MI;
extern MethodInfo m23953_MI;
extern MethodInfo m23954_MI;
extern MethodInfo m23955_MI;
extern MethodInfo m23949_MI;
extern MethodInfo m23957_MI;
extern MethodInfo m23958_MI;
static MethodInfo* t3940_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23950_MI,
	&m5907_MI,
	&m23951_MI,
	&m23952_MI,
	&m23953_MI,
	&m23954_MI,
	&m23955_MI,
	&m5908_MI,
	&m23949_MI,
	&m23957_MI,
	&m23958_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6018_TI;
extern TypeInfo t6019_TI;
extern TypeInfo t6020_TI;
static TypeInfo* t3940_ITIs[] = 
{
	&t6018_TI,
	&t6019_TI,
	&t6020_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3940_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6018_TI, 21},
	{ &t6019_TI, 28},
	{ &t6020_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType t3940_0_0_0;
extern Il2CppType t3940_1_0_0;
#include "t1037.h"
extern TypeInfo t1037_TI;
TypeInfo t3940_TI = 
{
	&g_Mono_Security_dll_Image, NULL, "HandshakeType[]", "Mono.Security.Protocol.Tls.Handshake", t3940_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1037_TI, t3940_ITIs, t3940_VT, &EmptyCustomAttributesCache, &t348_TI, &t3940_0_0_0, &t3940_1_0_0, t3940_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (uint8_t), -1, 0, 0, -1, 8448, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3558_TI;



// Metadata Definition System.SerializableAttribute[]
static MethodInfo* t3558_MIs[] =
{
	NULL
};
extern MethodInfo m23961_MI;
extern MethodInfo m23962_MI;
extern MethodInfo m23963_MI;
extern MethodInfo m23964_MI;
extern MethodInfo m23965_MI;
extern MethodInfo m23966_MI;
extern MethodInfo m23960_MI;
extern MethodInfo m23968_MI;
extern MethodInfo m23969_MI;
extern MethodInfo m22323_MI;
extern MethodInfo m22324_MI;
extern MethodInfo m22325_MI;
extern MethodInfo m22326_MI;
extern MethodInfo m22327_MI;
extern MethodInfo m22328_MI;
extern MethodInfo m22322_MI;
extern MethodInfo m22330_MI;
extern MethodInfo m22331_MI;
extern MethodInfo m22334_MI;
extern MethodInfo m22335_MI;
extern MethodInfo m22336_MI;
extern MethodInfo m22337_MI;
extern MethodInfo m22338_MI;
extern MethodInfo m22339_MI;
extern MethodInfo m22333_MI;
extern MethodInfo m22341_MI;
extern MethodInfo m22342_MI;
static MethodInfo* t3558_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23961_MI,
	&m5907_MI,
	&m23962_MI,
	&m23963_MI,
	&m23964_MI,
	&m23965_MI,
	&m23966_MI,
	&m5908_MI,
	&m23960_MI,
	&m23968_MI,
	&m23969_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6021_TI;
extern TypeInfo t6022_TI;
extern TypeInfo t6023_TI;
extern TypeInfo t5619_TI;
extern TypeInfo t5620_TI;
extern TypeInfo t5621_TI;
extern TypeInfo t5622_TI;
extern TypeInfo t5623_TI;
extern TypeInfo t5624_TI;
static TypeInfo* t3558_ITIs[] = 
{
	&t6021_TI,
	&t6022_TI,
	&t6023_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3558_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6021_TI, 21},
	{ &t6022_TI, 28},
	{ &t6023_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3558_0_0_0;
extern Il2CppType t3558_1_0_0;
struct t1128;
extern TypeInfo t1128_TI;
extern CustomAttributesCache t1128__CustomAttributeCache;
TypeInfo t3558_TI = 
{
	&g_mscorlib_dll_Image, NULL, "SerializableAttribute[]", "System", t3558_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1128_TI, t3558_ITIs, t3558_VT, &EmptyCustomAttributesCache, &t3558_TI, &t3558_0_0_0, &t3558_1_0_0, t3558_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1128 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3559_TI;



// Metadata Definition System.AttributeUsageAttribute[]
static MethodInfo* t3559_MIs[] =
{
	NULL
};
extern MethodInfo m23972_MI;
extern MethodInfo m23973_MI;
extern MethodInfo m23974_MI;
extern MethodInfo m23975_MI;
extern MethodInfo m23976_MI;
extern MethodInfo m23977_MI;
extern MethodInfo m23971_MI;
extern MethodInfo m23979_MI;
extern MethodInfo m23980_MI;
static MethodInfo* t3559_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23972_MI,
	&m5907_MI,
	&m23973_MI,
	&m23974_MI,
	&m23975_MI,
	&m23976_MI,
	&m23977_MI,
	&m5908_MI,
	&m23971_MI,
	&m23979_MI,
	&m23980_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6024_TI;
extern TypeInfo t6025_TI;
extern TypeInfo t6026_TI;
static TypeInfo* t3559_ITIs[] = 
{
	&t6024_TI,
	&t6025_TI,
	&t6026_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3559_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6024_TI, 21},
	{ &t6025_TI, 28},
	{ &t6026_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3559_0_0_0;
extern Il2CppType t3559_1_0_0;
struct t629;
extern TypeInfo t629_TI;
extern CustomAttributesCache t629__CustomAttributeCache;
TypeInfo t3559_TI = 
{
	&g_mscorlib_dll_Image, NULL, "AttributeUsageAttribute[]", "System", t3559_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t629_TI, t3559_ITIs, t3559_VT, &EmptyCustomAttributesCache, &t3559_TI, &t3559_0_0_0, &t3559_1_0_0, t3559_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t629 *), -1, 0, 0, -1, 1057025, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3560_TI;



// Metadata Definition System.Runtime.InteropServices.ComVisibleAttribute[]
static MethodInfo* t3560_MIs[] =
{
	NULL
};
extern MethodInfo m23983_MI;
extern MethodInfo m23984_MI;
extern MethodInfo m23985_MI;
extern MethodInfo m23986_MI;
extern MethodInfo m23987_MI;
extern MethodInfo m23988_MI;
extern MethodInfo m23982_MI;
extern MethodInfo m23990_MI;
extern MethodInfo m23991_MI;
static MethodInfo* t3560_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23983_MI,
	&m5907_MI,
	&m23984_MI,
	&m23985_MI,
	&m23986_MI,
	&m23987_MI,
	&m23988_MI,
	&m5908_MI,
	&m23982_MI,
	&m23990_MI,
	&m23991_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6027_TI;
extern TypeInfo t6028_TI;
extern TypeInfo t6029_TI;
static TypeInfo* t3560_ITIs[] = 
{
	&t6027_TI,
	&t6028_TI,
	&t6029_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3560_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6027_TI, 21},
	{ &t6028_TI, 28},
	{ &t6029_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3560_0_0_0;
extern Il2CppType t3560_1_0_0;
struct t437;
extern TypeInfo t437_TI;
extern CustomAttributesCache t437__CustomAttributeCache;
TypeInfo t3560_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ComVisibleAttribute[]", "System.Runtime.InteropServices", t3560_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t437_TI, t3560_ITIs, t3560_VT, &EmptyCustomAttributesCache, &t3560_TI, &t3560_0_0_0, &t3560_1_0_0, t3560_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t437 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1139_TI;



// Metadata Definition System.Int64[]
static MethodInfo* t1139_MIs[] =
{
	NULL
};
extern MethodInfo m23994_MI;
extern MethodInfo m23995_MI;
extern MethodInfo m23996_MI;
extern MethodInfo m23997_MI;
extern MethodInfo m23998_MI;
extern MethodInfo m23999_MI;
extern MethodInfo m23993_MI;
extern MethodInfo m24001_MI;
extern MethodInfo m24002_MI;
extern MethodInfo m24005_MI;
extern MethodInfo m24006_MI;
extern MethodInfo m24007_MI;
extern MethodInfo m24008_MI;
extern MethodInfo m24009_MI;
extern MethodInfo m24010_MI;
extern MethodInfo m24004_MI;
extern MethodInfo m24012_MI;
extern MethodInfo m24013_MI;
extern MethodInfo m24016_MI;
extern MethodInfo m24017_MI;
extern MethodInfo m24018_MI;
extern MethodInfo m24019_MI;
extern MethodInfo m24020_MI;
extern MethodInfo m24021_MI;
extern MethodInfo m24015_MI;
extern MethodInfo m24023_MI;
extern MethodInfo m24024_MI;
static MethodInfo* t1139_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m23994_MI,
	&m5907_MI,
	&m23995_MI,
	&m23996_MI,
	&m23997_MI,
	&m23998_MI,
	&m23999_MI,
	&m5908_MI,
	&m23993_MI,
	&m24001_MI,
	&m24002_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m24005_MI,
	&m5907_MI,
	&m24006_MI,
	&m24007_MI,
	&m24008_MI,
	&m24009_MI,
	&m24010_MI,
	&m5908_MI,
	&m24004_MI,
	&m24012_MI,
	&m24013_MI,
	&m5905_MI,
	&m5906_MI,
	&m24016_MI,
	&m5907_MI,
	&m24017_MI,
	&m24018_MI,
	&m24019_MI,
	&m24020_MI,
	&m24021_MI,
	&m5908_MI,
	&m24015_MI,
	&m24023_MI,
	&m24024_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6030_TI;
extern TypeInfo t6031_TI;
extern TypeInfo t6032_TI;
extern TypeInfo t6033_TI;
extern TypeInfo t6034_TI;
extern TypeInfo t6035_TI;
extern TypeInfo t6036_TI;
extern TypeInfo t6037_TI;
extern TypeInfo t6038_TI;
static TypeInfo* t1139_ITIs[] = 
{
	&t6030_TI,
	&t6031_TI,
	&t6032_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t6033_TI,
	&t6034_TI,
	&t6035_TI,
	&t6036_TI,
	&t6037_TI,
	&t6038_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t1139_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6030_TI, 21},
	{ &t6031_TI, 28},
	{ &t6032_TI, 33},
	{ &t5107_TI, 34},
	{ &t5108_TI, 41},
	{ &t5109_TI, 46},
	{ &t5110_TI, 47},
	{ &t5111_TI, 54},
	{ &t5112_TI, 59},
	{ &t5113_TI, 60},
	{ &t5114_TI, 67},
	{ &t5115_TI, 72},
	{ &t6033_TI, 73},
	{ &t6034_TI, 80},
	{ &t6035_TI, 85},
	{ &t6036_TI, 86},
	{ &t6037_TI, 93},
	{ &t6038_TI, 98},
	{ &t5116_TI, 99},
	{ &t5117_TI, 106},
	{ &t5118_TI, 111},
	{ &t2182_TI, 112},
	{ &t2186_TI, 119},
	{ &t2183_TI, 124},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1139_0_0_0;
extern Il2CppType t1139_1_0_0;
#include "t919.h"
extern TypeInfo t919_TI;
extern CustomAttributesCache t919__CustomAttributeCache;
TypeInfo t1139_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Int64[]", "System", t1139_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t919_TI, t1139_ITIs, t1139_VT, &EmptyCustomAttributesCache, &t1139_TI, &t1139_0_0_0, &t1139_1_0_0, t1139_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int64_t), -1, 0, 0, -1, 1057033, 1, false, false, false, false, false, true, false, false, false, false, false, true, 0, 0, 0, 0, 0, 125, 24, 28};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3561_TI;



// Metadata Definition System.IComparable`1<System.Int64>[]
static MethodInfo* t3561_MIs[] =
{
	NULL
};
static MethodInfo* t3561_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24005_MI,
	&m5907_MI,
	&m24006_MI,
	&m24007_MI,
	&m24008_MI,
	&m24009_MI,
	&m24010_MI,
	&m5908_MI,
	&m24004_MI,
	&m24012_MI,
	&m24013_MI,
};
static TypeInfo* t3561_ITIs[] = 
{
	&t6033_TI,
	&t6034_TI,
	&t6035_TI,
};
static Il2CppInterfaceOffsetPair t3561_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6033_TI, 21},
	{ &t6034_TI, 28},
	{ &t6035_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3561_0_0_0;
extern Il2CppType t3561_1_0_0;
struct t1713;
extern TypeInfo t1713_TI;
TypeInfo t3561_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1[]", "System", t3561_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1713_TI, t3561_ITIs, t3561_VT, &EmptyCustomAttributesCache, &t3561_TI, &t3561_0_0_0, &t3561_1_0_0, t3561_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29*), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3562_TI;



// Metadata Definition System.IEquatable`1<System.Int64>[]
static MethodInfo* t3562_MIs[] =
{
	NULL
};
static MethodInfo* t3562_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24016_MI,
	&m5907_MI,
	&m24017_MI,
	&m24018_MI,
	&m24019_MI,
	&m24020_MI,
	&m24021_MI,
	&m5908_MI,
	&m24015_MI,
	&m24023_MI,
	&m24024_MI,
};
static TypeInfo* t3562_ITIs[] = 
{
	&t6036_TI,
	&t6037_TI,
	&t6038_TI,
};
static Il2CppInterfaceOffsetPair t3562_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6036_TI, 21},
	{ &t6037_TI, 28},
	{ &t6038_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3562_0_0_0;
extern Il2CppType t3562_1_0_0;
struct t1714;
extern TypeInfo t1714_TI;
TypeInfo t3562_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1[]", "System", t3562_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1714_TI, t3562_ITIs, t3562_VT, &EmptyCustomAttributesCache, &t3562_TI, &t3562_0_0_0, &t3562_1_0_0, t3562_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29*), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3563_TI;



// Metadata Definition System.CLSCompliantAttribute[]
static MethodInfo* t3563_MIs[] =
{
	NULL
};
extern MethodInfo m24027_MI;
extern MethodInfo m24028_MI;
extern MethodInfo m24029_MI;
extern MethodInfo m24030_MI;
extern MethodInfo m24031_MI;
extern MethodInfo m24032_MI;
extern MethodInfo m24026_MI;
extern MethodInfo m24034_MI;
extern MethodInfo m24035_MI;
static MethodInfo* t3563_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24027_MI,
	&m5907_MI,
	&m24028_MI,
	&m24029_MI,
	&m24030_MI,
	&m24031_MI,
	&m24032_MI,
	&m5908_MI,
	&m24026_MI,
	&m24034_MI,
	&m24035_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6039_TI;
extern TypeInfo t6040_TI;
extern TypeInfo t6041_TI;
static TypeInfo* t3563_ITIs[] = 
{
	&t6039_TI,
	&t6040_TI,
	&t6041_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3563_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6039_TI, 21},
	{ &t6040_TI, 28},
	{ &t6041_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3563_0_0_0;
extern Il2CppType t3563_1_0_0;
struct t707;
extern TypeInfo t707_TI;
extern CustomAttributesCache t707__CustomAttributeCache;
TypeInfo t3563_TI = 
{
	&g_mscorlib_dll_Image, NULL, "CLSCompliantAttribute[]", "System", t3563_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t707_TI, t3563_ITIs, t3563_VT, &EmptyCustomAttributesCache, &t3563_TI, &t3563_0_0_0, &t3563_1_0_0, t3563_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t707 *), -1, 0, 0, -1, 1057025, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1552_TI;



// Metadata Definition System.UInt64[]
static MethodInfo* t1552_MIs[] =
{
	NULL
};
extern MethodInfo m24038_MI;
extern MethodInfo m24039_MI;
extern MethodInfo m24040_MI;
extern MethodInfo m24041_MI;
extern MethodInfo m24042_MI;
extern MethodInfo m24043_MI;
extern MethodInfo m24037_MI;
extern MethodInfo m24045_MI;
extern MethodInfo m24046_MI;
extern MethodInfo m24049_MI;
extern MethodInfo m24050_MI;
extern MethodInfo m24051_MI;
extern MethodInfo m24052_MI;
extern MethodInfo m24053_MI;
extern MethodInfo m24054_MI;
extern MethodInfo m24048_MI;
extern MethodInfo m24056_MI;
extern MethodInfo m24057_MI;
extern MethodInfo m24060_MI;
extern MethodInfo m24061_MI;
extern MethodInfo m24062_MI;
extern MethodInfo m24063_MI;
extern MethodInfo m24064_MI;
extern MethodInfo m24065_MI;
extern MethodInfo m24059_MI;
extern MethodInfo m24067_MI;
extern MethodInfo m24068_MI;
static MethodInfo* t1552_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24038_MI,
	&m5907_MI,
	&m24039_MI,
	&m24040_MI,
	&m24041_MI,
	&m24042_MI,
	&m24043_MI,
	&m5908_MI,
	&m24037_MI,
	&m24045_MI,
	&m24046_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m24049_MI,
	&m5907_MI,
	&m24050_MI,
	&m24051_MI,
	&m24052_MI,
	&m24053_MI,
	&m24054_MI,
	&m5908_MI,
	&m24048_MI,
	&m24056_MI,
	&m24057_MI,
	&m5905_MI,
	&m5906_MI,
	&m24060_MI,
	&m5907_MI,
	&m24061_MI,
	&m24062_MI,
	&m24063_MI,
	&m24064_MI,
	&m24065_MI,
	&m5908_MI,
	&m24059_MI,
	&m24067_MI,
	&m24068_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6042_TI;
extern TypeInfo t6043_TI;
extern TypeInfo t6044_TI;
extern TypeInfo t6045_TI;
extern TypeInfo t6046_TI;
extern TypeInfo t6047_TI;
extern TypeInfo t6048_TI;
extern TypeInfo t6049_TI;
extern TypeInfo t6050_TI;
static TypeInfo* t1552_ITIs[] = 
{
	&t6042_TI,
	&t6043_TI,
	&t6044_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t6045_TI,
	&t6046_TI,
	&t6047_TI,
	&t6048_TI,
	&t6049_TI,
	&t6050_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t1552_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6042_TI, 21},
	{ &t6043_TI, 28},
	{ &t6044_TI, 33},
	{ &t5107_TI, 34},
	{ &t5108_TI, 41},
	{ &t5109_TI, 46},
	{ &t5110_TI, 47},
	{ &t5111_TI, 54},
	{ &t5112_TI, 59},
	{ &t5113_TI, 60},
	{ &t5114_TI, 67},
	{ &t5115_TI, 72},
	{ &t6045_TI, 73},
	{ &t6046_TI, 80},
	{ &t6047_TI, 85},
	{ &t6048_TI, 86},
	{ &t6049_TI, 93},
	{ &t6050_TI, 98},
	{ &t5116_TI, 99},
	{ &t5117_TI, 106},
	{ &t5118_TI, 111},
	{ &t2182_TI, 112},
	{ &t2186_TI, 119},
	{ &t2183_TI, 124},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1552_0_0_0;
extern Il2CppType t1552_1_0_0;
#include "t1089.h"
extern TypeInfo t1089_TI;
extern CustomAttributesCache t1089__CustomAttributeCache;
extern CustomAttributesCache t1089__CustomAttributeCache_m5409;
extern CustomAttributesCache t1089__CustomAttributeCache_m5411;
extern CustomAttributesCache t1089__CustomAttributeCache_m5412;
TypeInfo t1552_TI = 
{
	&g_mscorlib_dll_Image, NULL, "UInt64[]", "System", t1552_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1089_TI, t1552_ITIs, t1552_VT, &EmptyCustomAttributesCache, &t1552_TI, &t1552_0_0_0, &t1552_1_0_0, t1552_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (uint64_t), -1, 0, 0, -1, 1057033, 1, false, false, false, false, false, true, false, false, false, false, false, true, 0, 0, 0, 0, 0, 125, 24, 28};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3564_TI;



// Metadata Definition System.IComparable`1<System.UInt64>[]
static MethodInfo* t3564_MIs[] =
{
	NULL
};
static MethodInfo* t3564_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24049_MI,
	&m5907_MI,
	&m24050_MI,
	&m24051_MI,
	&m24052_MI,
	&m24053_MI,
	&m24054_MI,
	&m5908_MI,
	&m24048_MI,
	&m24056_MI,
	&m24057_MI,
};
static TypeInfo* t3564_ITIs[] = 
{
	&t6045_TI,
	&t6046_TI,
	&t6047_TI,
};
static Il2CppInterfaceOffsetPair t3564_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6045_TI, 21},
	{ &t6046_TI, 28},
	{ &t6047_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3564_0_0_0;
extern Il2CppType t3564_1_0_0;
struct t1719;
extern TypeInfo t1719_TI;
TypeInfo t3564_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1[]", "System", t3564_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1719_TI, t3564_ITIs, t3564_VT, &EmptyCustomAttributesCache, &t3564_TI, &t3564_0_0_0, &t3564_1_0_0, t3564_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29*), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3565_TI;



// Metadata Definition System.IEquatable`1<System.UInt64>[]
static MethodInfo* t3565_MIs[] =
{
	NULL
};
static MethodInfo* t3565_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24060_MI,
	&m5907_MI,
	&m24061_MI,
	&m24062_MI,
	&m24063_MI,
	&m24064_MI,
	&m24065_MI,
	&m5908_MI,
	&m24059_MI,
	&m24067_MI,
	&m24068_MI,
};
static TypeInfo* t3565_ITIs[] = 
{
	&t6048_TI,
	&t6049_TI,
	&t6050_TI,
};
static Il2CppInterfaceOffsetPair t3565_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6048_TI, 21},
	{ &t6049_TI, 28},
	{ &t6050_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3565_0_0_0;
extern Il2CppType t3565_1_0_0;
struct t1720;
extern TypeInfo t1720_TI;
TypeInfo t3565_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1[]", "System", t3565_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1720_TI, t3565_ITIs, t3565_VT, &EmptyCustomAttributesCache, &t3565_TI, &t3565_0_0_0, &t3565_1_0_0, t3565_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29*), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1595_TI;



// Metadata Definition System.SByte[]
static MethodInfo* t1595_MIs[] =
{
	NULL
};
extern MethodInfo m24071_MI;
extern MethodInfo m24072_MI;
extern MethodInfo m24073_MI;
extern MethodInfo m24074_MI;
extern MethodInfo m24075_MI;
extern MethodInfo m24076_MI;
extern MethodInfo m24070_MI;
extern MethodInfo m24078_MI;
extern MethodInfo m24079_MI;
extern MethodInfo m24082_MI;
extern MethodInfo m24083_MI;
extern MethodInfo m24084_MI;
extern MethodInfo m24085_MI;
extern MethodInfo m24086_MI;
extern MethodInfo m24087_MI;
extern MethodInfo m24081_MI;
extern MethodInfo m24089_MI;
extern MethodInfo m24090_MI;
extern MethodInfo m24093_MI;
extern MethodInfo m24094_MI;
extern MethodInfo m24095_MI;
extern MethodInfo m24096_MI;
extern MethodInfo m24097_MI;
extern MethodInfo m24098_MI;
extern MethodInfo m24092_MI;
extern MethodInfo m24100_MI;
extern MethodInfo m24101_MI;
static MethodInfo* t1595_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24071_MI,
	&m5907_MI,
	&m24072_MI,
	&m24073_MI,
	&m24074_MI,
	&m24075_MI,
	&m24076_MI,
	&m5908_MI,
	&m24070_MI,
	&m24078_MI,
	&m24079_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m24082_MI,
	&m5907_MI,
	&m24083_MI,
	&m24084_MI,
	&m24085_MI,
	&m24086_MI,
	&m24087_MI,
	&m5908_MI,
	&m24081_MI,
	&m24089_MI,
	&m24090_MI,
	&m5905_MI,
	&m5906_MI,
	&m24093_MI,
	&m5907_MI,
	&m24094_MI,
	&m24095_MI,
	&m24096_MI,
	&m24097_MI,
	&m24098_MI,
	&m5908_MI,
	&m24092_MI,
	&m24100_MI,
	&m24101_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6051_TI;
extern TypeInfo t6052_TI;
extern TypeInfo t6053_TI;
extern TypeInfo t6054_TI;
extern TypeInfo t6055_TI;
extern TypeInfo t6056_TI;
extern TypeInfo t6057_TI;
extern TypeInfo t6058_TI;
extern TypeInfo t6059_TI;
static TypeInfo* t1595_ITIs[] = 
{
	&t6051_TI,
	&t6052_TI,
	&t6053_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t6054_TI,
	&t6055_TI,
	&t6056_TI,
	&t6057_TI,
	&t6058_TI,
	&t6059_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t1595_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6051_TI, 21},
	{ &t6052_TI, 28},
	{ &t6053_TI, 33},
	{ &t5107_TI, 34},
	{ &t5108_TI, 41},
	{ &t5109_TI, 46},
	{ &t5110_TI, 47},
	{ &t5111_TI, 54},
	{ &t5112_TI, 59},
	{ &t5113_TI, 60},
	{ &t5114_TI, 67},
	{ &t5115_TI, 72},
	{ &t6054_TI, 73},
	{ &t6055_TI, 80},
	{ &t6056_TI, 85},
	{ &t6057_TI, 86},
	{ &t6058_TI, 93},
	{ &t6059_TI, 98},
	{ &t5116_TI, 99},
	{ &t5117_TI, 106},
	{ &t5118_TI, 111},
	{ &t2182_TI, 112},
	{ &t2186_TI, 119},
	{ &t2183_TI, 124},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1595_0_0_0;
extern Il2CppType t1595_1_0_0;
#include "t297.h"
extern TypeInfo t297_TI;
extern CustomAttributesCache t297__CustomAttributeCache;
extern CustomAttributesCache t297__CustomAttributeCache_m5462;
extern CustomAttributesCache t297__CustomAttributeCache_m5463;
extern CustomAttributesCache t297__CustomAttributeCache_m5464;
TypeInfo t1595_TI = 
{
	&g_mscorlib_dll_Image, NULL, "SByte[]", "System", t1595_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t297_TI, t1595_ITIs, t1595_VT, &EmptyCustomAttributesCache, &t1595_TI, &t1595_0_0_0, &t1595_1_0_0, t1595_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int8_t), -1, 0, 0, -1, 1057033, 1, false, false, false, false, false, true, false, false, false, false, false, true, 0, 0, 0, 0, 0, 125, 24, 28};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3566_TI;



// Metadata Definition System.IComparable`1<System.SByte>[]
static MethodInfo* t3566_MIs[] =
{
	NULL
};
static MethodInfo* t3566_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24082_MI,
	&m5907_MI,
	&m24083_MI,
	&m24084_MI,
	&m24085_MI,
	&m24086_MI,
	&m24087_MI,
	&m5908_MI,
	&m24081_MI,
	&m24089_MI,
	&m24090_MI,
};
static TypeInfo* t3566_ITIs[] = 
{
	&t6054_TI,
	&t6055_TI,
	&t6056_TI,
};
static Il2CppInterfaceOffsetPair t3566_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6054_TI, 21},
	{ &t6055_TI, 28},
	{ &t6056_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3566_0_0_0;
extern Il2CppType t3566_1_0_0;
struct t1724;
extern TypeInfo t1724_TI;
TypeInfo t3566_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1[]", "System", t3566_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1724_TI, t3566_ITIs, t3566_VT, &EmptyCustomAttributesCache, &t3566_TI, &t3566_0_0_0, &t3566_1_0_0, t3566_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29*), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3567_TI;



// Metadata Definition System.IEquatable`1<System.SByte>[]
static MethodInfo* t3567_MIs[] =
{
	NULL
};
static MethodInfo* t3567_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24093_MI,
	&m5907_MI,
	&m24094_MI,
	&m24095_MI,
	&m24096_MI,
	&m24097_MI,
	&m24098_MI,
	&m5908_MI,
	&m24092_MI,
	&m24100_MI,
	&m24101_MI,
};
static TypeInfo* t3567_ITIs[] = 
{
	&t6057_TI,
	&t6058_TI,
	&t6059_TI,
};
static Il2CppInterfaceOffsetPair t3567_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6057_TI, 21},
	{ &t6058_TI, 28},
	{ &t6059_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3567_0_0_0;
extern Il2CppType t3567_1_0_0;
struct t1725;
extern TypeInfo t1725_TI;
TypeInfo t3567_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1[]", "System", t3567_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1725_TI, t3567_ITIs, t3567_VT, &EmptyCustomAttributesCache, &t3567_TI, &t3567_0_0_0, &t3567_1_0_0, t3567_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29*), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1763_TI;



// Metadata Definition System.Int16[]
static MethodInfo* t1763_MIs[] =
{
	NULL
};
extern MethodInfo m24104_MI;
extern MethodInfo m24105_MI;
extern MethodInfo m24106_MI;
extern MethodInfo m24107_MI;
extern MethodInfo m24108_MI;
extern MethodInfo m24109_MI;
extern MethodInfo m24103_MI;
extern MethodInfo m24111_MI;
extern MethodInfo m24112_MI;
extern MethodInfo m24115_MI;
extern MethodInfo m24116_MI;
extern MethodInfo m24117_MI;
extern MethodInfo m24118_MI;
extern MethodInfo m24119_MI;
extern MethodInfo m24120_MI;
extern MethodInfo m24114_MI;
extern MethodInfo m24122_MI;
extern MethodInfo m24123_MI;
extern MethodInfo m24126_MI;
extern MethodInfo m24127_MI;
extern MethodInfo m24128_MI;
extern MethodInfo m24129_MI;
extern MethodInfo m24130_MI;
extern MethodInfo m24131_MI;
extern MethodInfo m24125_MI;
extern MethodInfo m24133_MI;
extern MethodInfo m24134_MI;
static MethodInfo* t1763_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24104_MI,
	&m5907_MI,
	&m24105_MI,
	&m24106_MI,
	&m24107_MI,
	&m24108_MI,
	&m24109_MI,
	&m5908_MI,
	&m24103_MI,
	&m24111_MI,
	&m24112_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m24115_MI,
	&m5907_MI,
	&m24116_MI,
	&m24117_MI,
	&m24118_MI,
	&m24119_MI,
	&m24120_MI,
	&m5908_MI,
	&m24114_MI,
	&m24122_MI,
	&m24123_MI,
	&m5905_MI,
	&m5906_MI,
	&m24126_MI,
	&m5907_MI,
	&m24127_MI,
	&m24128_MI,
	&m24129_MI,
	&m24130_MI,
	&m24131_MI,
	&m5908_MI,
	&m24125_MI,
	&m24133_MI,
	&m24134_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6060_TI;
extern TypeInfo t6061_TI;
extern TypeInfo t6062_TI;
extern TypeInfo t6063_TI;
extern TypeInfo t6064_TI;
extern TypeInfo t6065_TI;
extern TypeInfo t6066_TI;
extern TypeInfo t6067_TI;
extern TypeInfo t6068_TI;
static TypeInfo* t1763_ITIs[] = 
{
	&t6060_TI,
	&t6061_TI,
	&t6062_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t6063_TI,
	&t6064_TI,
	&t6065_TI,
	&t6066_TI,
	&t6067_TI,
	&t6068_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t1763_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6060_TI, 21},
	{ &t6061_TI, 28},
	{ &t6062_TI, 33},
	{ &t5107_TI, 34},
	{ &t5108_TI, 41},
	{ &t5109_TI, 46},
	{ &t5110_TI, 47},
	{ &t5111_TI, 54},
	{ &t5112_TI, 59},
	{ &t5113_TI, 60},
	{ &t5114_TI, 67},
	{ &t5115_TI, 72},
	{ &t6063_TI, 73},
	{ &t6064_TI, 80},
	{ &t6065_TI, 85},
	{ &t6066_TI, 86},
	{ &t6067_TI, 93},
	{ &t6068_TI, 98},
	{ &t5116_TI, 99},
	{ &t5117_TI, 106},
	{ &t5118_TI, 111},
	{ &t2182_TI, 112},
	{ &t2186_TI, 119},
	{ &t2183_TI, 124},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1763_0_0_0;
extern Il2CppType t1763_1_0_0;
#include "t372.h"
extern TypeInfo t372_TI;
extern CustomAttributesCache t372__CustomAttributeCache;
TypeInfo t1763_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Int16[]", "System", t1763_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t372_TI, t1763_ITIs, t1763_VT, &EmptyCustomAttributesCache, &t1763_TI, &t1763_0_0_0, &t1763_1_0_0, t1763_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int16_t), -1, 0, 0, -1, 1057033, 1, false, false, false, false, false, true, false, false, false, false, false, true, 0, 0, 0, 0, 0, 125, 24, 28};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3568_TI;



// Metadata Definition System.IComparable`1<System.Int16>[]
static MethodInfo* t3568_MIs[] =
{
	NULL
};
static MethodInfo* t3568_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24115_MI,
	&m5907_MI,
	&m24116_MI,
	&m24117_MI,
	&m24118_MI,
	&m24119_MI,
	&m24120_MI,
	&m5908_MI,
	&m24114_MI,
	&m24122_MI,
	&m24123_MI,
};
static TypeInfo* t3568_ITIs[] = 
{
	&t6063_TI,
	&t6064_TI,
	&t6065_TI,
};
static Il2CppInterfaceOffsetPair t3568_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6063_TI, 21},
	{ &t6064_TI, 28},
	{ &t6065_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3568_0_0_0;
extern Il2CppType t3568_1_0_0;
struct t1727;
extern TypeInfo t1727_TI;
TypeInfo t3568_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1[]", "System", t3568_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1727_TI, t3568_ITIs, t3568_VT, &EmptyCustomAttributesCache, &t3568_TI, &t3568_0_0_0, &t3568_1_0_0, t3568_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29*), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3569_TI;



// Metadata Definition System.IEquatable`1<System.Int16>[]
static MethodInfo* t3569_MIs[] =
{
	NULL
};
static MethodInfo* t3569_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24126_MI,
	&m5907_MI,
	&m24127_MI,
	&m24128_MI,
	&m24129_MI,
	&m24130_MI,
	&m24131_MI,
	&m5908_MI,
	&m24125_MI,
	&m24133_MI,
	&m24134_MI,
};
static TypeInfo* t3569_ITIs[] = 
{
	&t6066_TI,
	&t6067_TI,
	&t6068_TI,
};
static Il2CppInterfaceOffsetPair t3569_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6066_TI, 21},
	{ &t6067_TI, 28},
	{ &t6068_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3569_0_0_0;
extern Il2CppType t3569_1_0_0;
struct t1728;
extern TypeInfo t1728_TI;
TypeInfo t3569_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1[]", "System", t3569_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1728_TI, t3569_ITIs, t3569_VT, &EmptyCustomAttributesCache, &t3569_TI, &t3569_0_0_0, &t3569_1_0_0, t3569_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29*), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3570_TI;



// Metadata Definition System.Single[,]
static MethodInfo* t3570_MIs[] =
{
	NULL
};
extern MethodInfo m22562_MI;
extern MethodInfo m22563_MI;
extern MethodInfo m22564_MI;
extern MethodInfo m22565_MI;
extern MethodInfo m22566_MI;
extern MethodInfo m22567_MI;
extern MethodInfo m22561_MI;
extern MethodInfo m22569_MI;
extern MethodInfo m22570_MI;
extern MethodInfo m22573_MI;
extern MethodInfo m22574_MI;
extern MethodInfo m22575_MI;
extern MethodInfo m22576_MI;
extern MethodInfo m22577_MI;
extern MethodInfo m22578_MI;
extern MethodInfo m22572_MI;
extern MethodInfo m22580_MI;
extern MethodInfo m22581_MI;
extern MethodInfo m22584_MI;
extern MethodInfo m22585_MI;
extern MethodInfo m22586_MI;
extern MethodInfo m22587_MI;
extern MethodInfo m22588_MI;
extern MethodInfo m22589_MI;
extern MethodInfo m22583_MI;
extern MethodInfo m22591_MI;
extern MethodInfo m22592_MI;
static MethodInfo* t3570_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22562_MI,
	&m5907_MI,
	&m22563_MI,
	&m22564_MI,
	&m22565_MI,
	&m22566_MI,
	&m22567_MI,
	&m5908_MI,
	&m22561_MI,
	&m22569_MI,
	&m22570_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m22573_MI,
	&m5907_MI,
	&m22574_MI,
	&m22575_MI,
	&m22576_MI,
	&m22577_MI,
	&m22578_MI,
	&m5908_MI,
	&m22572_MI,
	&m22580_MI,
	&m22581_MI,
	&m5905_MI,
	&m5906_MI,
	&m22584_MI,
	&m5907_MI,
	&m22585_MI,
	&m22586_MI,
	&m22587_MI,
	&m22588_MI,
	&m22589_MI,
	&m5908_MI,
	&m22583_MI,
	&m22591_MI,
	&m22592_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5673_TI;
extern TypeInfo t5674_TI;
extern TypeInfo t5675_TI;
extern TypeInfo t5676_TI;
extern TypeInfo t5677_TI;
extern TypeInfo t5678_TI;
extern TypeInfo t5679_TI;
extern TypeInfo t5680_TI;
extern TypeInfo t5681_TI;
static TypeInfo* t3570_ITIs[] = 
{
	&t5673_TI,
	&t5674_TI,
	&t5675_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5676_TI,
	&t5677_TI,
	&t5678_TI,
	&t5679_TI,
	&t5680_TI,
	&t5681_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3570_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5673_TI, 21},
	{ &t5674_TI, 28},
	{ &t5675_TI, 33},
	{ &t5107_TI, 34},
	{ &t5108_TI, 41},
	{ &t5109_TI, 46},
	{ &t5110_TI, 47},
	{ &t5111_TI, 54},
	{ &t5112_TI, 59},
	{ &t5113_TI, 60},
	{ &t5114_TI, 67},
	{ &t5115_TI, 72},
	{ &t5676_TI, 73},
	{ &t5677_TI, 80},
	{ &t5678_TI, 85},
	{ &t5679_TI, 86},
	{ &t5680_TI, 93},
	{ &t5681_TI, 98},
	{ &t5116_TI, 99},
	{ &t5117_TI, 106},
	{ &t5118_TI, 111},
	{ &t2182_TI, 112},
	{ &t2186_TI, 119},
	{ &t2183_TI, 124},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3570_0_0_0;
extern Il2CppType t3570_1_0_0;
#include "t22.h"
extern TypeInfo t22_TI;
extern CustomAttributesCache t22__CustomAttributeCache;
extern CustomAttributesCache t22__CustomAttributeCache_m5657;
TypeInfo t3570_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Single[,]", "System", t3570_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t22_TI, t3570_ITIs, t3570_VT, &EmptyCustomAttributesCache, &t3570_TI, &t3570_0_0_0, &t3570_1_0_0, t3570_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (float), -1, 0, 0, -1, 1057033, 2, false, false, false, false, false, true, false, false, false, false, false, true, 0, 0, 0, 0, 0, 125, 24, 28};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3571_TI;



// Metadata Definition System.UIntPtr[]
static MethodInfo* t3571_MIs[] =
{
	NULL
};
extern MethodInfo m24152_MI;
extern MethodInfo m24153_MI;
extern MethodInfo m24154_MI;
extern MethodInfo m24155_MI;
extern MethodInfo m24156_MI;
extern MethodInfo m24157_MI;
extern MethodInfo m24151_MI;
extern MethodInfo m24159_MI;
extern MethodInfo m24160_MI;
static MethodInfo* t3571_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24152_MI,
	&m5907_MI,
	&m24153_MI,
	&m24154_MI,
	&m24155_MI,
	&m24156_MI,
	&m24157_MI,
	&m5908_MI,
	&m24151_MI,
	&m24159_MI,
	&m24160_MI,
	&m5905_MI,
	&m5906_MI,
	&m22439_MI,
	&m5907_MI,
	&m22440_MI,
	&m22441_MI,
	&m22442_MI,
	&m22443_MI,
	&m22444_MI,
	&m5908_MI,
	&m22438_MI,
	&m22446_MI,
	&m22447_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6069_TI;
extern TypeInfo t6070_TI;
extern TypeInfo t6071_TI;
static TypeInfo* t3571_ITIs[] = 
{
	&t6069_TI,
	&t6070_TI,
	&t6071_TI,
	&t5649_TI,
	&t5650_TI,
	&t5651_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3571_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6069_TI, 21},
	{ &t6070_TI, 28},
	{ &t6071_TI, 33},
	{ &t5649_TI, 34},
	{ &t5650_TI, 41},
	{ &t5651_TI, 46},
	{ &t5116_TI, 47},
	{ &t5117_TI, 54},
	{ &t5118_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3571_0_0_0;
extern Il2CppType t3571_1_0_0;
#include "t1131.h"
extern TypeInfo t1131_TI;
extern CustomAttributesCache t1131__CustomAttributeCache;
extern CustomAttributesCache t1131__CustomAttributeCache_m5823;
extern CustomAttributesCache t1131__CustomAttributeCache_m5830;
extern CustomAttributesCache t1131__CustomAttributeCache_m5838;
extern CustomAttributesCache t1131__CustomAttributeCache_m5839;
TypeInfo t3571_TI = 
{
	&g_mscorlib_dll_Image, NULL, "UIntPtr[]", "System", t3571_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1131_TI, t3571_ITIs, t3571_VT, &EmptyCustomAttributesCache, &t3571_TI, &t3571_0_0_0, &t3571_1_0_0, t3571_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1131 ), -1, sizeof(t3571_SFs), 0, -1, 1057033, 1, false, false, false, false, false, true, false, false, false, true, false, true, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1132_TI;



// Metadata Definition System.Delegate[]
static MethodInfo* t1132_MIs[] =
{
	NULL
};
extern MethodInfo m24163_MI;
extern MethodInfo m24164_MI;
extern MethodInfo m24165_MI;
extern MethodInfo m24166_MI;
extern MethodInfo m24167_MI;
extern MethodInfo m24168_MI;
extern MethodInfo m24162_MI;
extern MethodInfo m24170_MI;
extern MethodInfo m24171_MI;
extern MethodInfo m20434_MI;
extern MethodInfo m20435_MI;
extern MethodInfo m20436_MI;
extern MethodInfo m20437_MI;
extern MethodInfo m20438_MI;
extern MethodInfo m20439_MI;
extern MethodInfo m20433_MI;
extern MethodInfo m20441_MI;
extern MethodInfo m20442_MI;
static MethodInfo* t1132_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24163_MI,
	&m5907_MI,
	&m24164_MI,
	&m24165_MI,
	&m24166_MI,
	&m24167_MI,
	&m24168_MI,
	&m5908_MI,
	&m24162_MI,
	&m24170_MI,
	&m24171_MI,
	&m5905_MI,
	&m5906_MI,
	&m20434_MI,
	&m5907_MI,
	&m20435_MI,
	&m20436_MI,
	&m20437_MI,
	&m20438_MI,
	&m20439_MI,
	&m5908_MI,
	&m20433_MI,
	&m20441_MI,
	&m20442_MI,
	&m5905_MI,
	&m5906_MI,
	&m22439_MI,
	&m5907_MI,
	&m22440_MI,
	&m22441_MI,
	&m22442_MI,
	&m22443_MI,
	&m22444_MI,
	&m5908_MI,
	&m22438_MI,
	&m22446_MI,
	&m22447_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6072_TI;
extern TypeInfo t6073_TI;
extern TypeInfo t6074_TI;
extern TypeInfo t5241_TI;
extern TypeInfo t5242_TI;
extern TypeInfo t5243_TI;
static TypeInfo* t1132_ITIs[] = 
{
	&t6072_TI,
	&t6073_TI,
	&t6074_TI,
	&t5241_TI,
	&t5242_TI,
	&t5243_TI,
	&t5649_TI,
	&t5650_TI,
	&t5651_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t1132_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6072_TI, 21},
	{ &t6073_TI, 28},
	{ &t6074_TI, 33},
	{ &t5241_TI, 34},
	{ &t5242_TI, 41},
	{ &t5243_TI, 46},
	{ &t5649_TI, 47},
	{ &t5650_TI, 54},
	{ &t5651_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1132_0_0_0;
extern Il2CppType t1132_1_0_0;
struct t353;
extern TypeInfo t353_TI;
extern CustomAttributesCache t353__CustomAttributeCache;
extern CustomAttributesCache t353__CustomAttributeCache_m5859;
TypeInfo t1132_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Delegate[]", "System", t1132_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t353_TI, t1132_ITIs, t1132_VT, &EmptyCustomAttributesCache, &t1132_TI, &t1132_0_0_0, &t1132_1_0_0, t1132_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t353 *), -1, 0, 0, -1, 1056897, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3572_TI;



// Metadata Definition System.FlagsAttribute[]
static MethodInfo* t3572_MIs[] =
{
	NULL
};
extern MethodInfo m24174_MI;
extern MethodInfo m24175_MI;
extern MethodInfo m24176_MI;
extern MethodInfo m24177_MI;
extern MethodInfo m24178_MI;
extern MethodInfo m24179_MI;
extern MethodInfo m24173_MI;
extern MethodInfo m24181_MI;
extern MethodInfo m24182_MI;
static MethodInfo* t3572_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24174_MI,
	&m5907_MI,
	&m24175_MI,
	&m24176_MI,
	&m24177_MI,
	&m24178_MI,
	&m24179_MI,
	&m5908_MI,
	&m24173_MI,
	&m24181_MI,
	&m24182_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6075_TI;
extern TypeInfo t6076_TI;
extern TypeInfo t6077_TI;
static TypeInfo* t3572_ITIs[] = 
{
	&t6075_TI,
	&t6076_TI,
	&t6077_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3572_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6075_TI, 21},
	{ &t6076_TI, 28},
	{ &t6077_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3572_0_0_0;
extern Il2CppType t3572_1_0_0;
struct t291;
extern TypeInfo t291_TI;
extern CustomAttributesCache t291__CustomAttributeCache;
TypeInfo t3572_TI = 
{
	&g_mscorlib_dll_Image, NULL, "FlagsAttribute[]", "System", t3572_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t291_TI, t3572_ITIs, t3572_VT, &EmptyCustomAttributesCache, &t3572_TI, &t3572_0_0_0, &t3572_1_0_0, t3572_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t291 *), -1, 0, 0, -1, 1056769, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1145_TI;



// Metadata Definition System.Reflection.MethodInfo[]
static MethodInfo* t1145_MIs[] =
{
	NULL
};
extern MethodInfo m24213_MI;
extern MethodInfo m24214_MI;
extern MethodInfo m24215_MI;
extern MethodInfo m24216_MI;
extern MethodInfo m24217_MI;
extern MethodInfo m24218_MI;
extern MethodInfo m24212_MI;
extern MethodInfo m24220_MI;
extern MethodInfo m24221_MI;
extern MethodInfo m24224_MI;
extern MethodInfo m24225_MI;
extern MethodInfo m24226_MI;
extern MethodInfo m24227_MI;
extern MethodInfo m24228_MI;
extern MethodInfo m24229_MI;
extern MethodInfo m24223_MI;
extern MethodInfo m24231_MI;
extern MethodInfo m24232_MI;
extern MethodInfo m24235_MI;
extern MethodInfo m24236_MI;
extern MethodInfo m24237_MI;
extern MethodInfo m24238_MI;
extern MethodInfo m24239_MI;
extern MethodInfo m24240_MI;
extern MethodInfo m24234_MI;
extern MethodInfo m24242_MI;
extern MethodInfo m24243_MI;
extern MethodInfo m24246_MI;
extern MethodInfo m24247_MI;
extern MethodInfo m24248_MI;
extern MethodInfo m24249_MI;
extern MethodInfo m24250_MI;
extern MethodInfo m24251_MI;
extern MethodInfo m24245_MI;
extern MethodInfo m24253_MI;
extern MethodInfo m24254_MI;
extern MethodInfo m19825_MI;
extern MethodInfo m19826_MI;
extern MethodInfo m19827_MI;
extern MethodInfo m19828_MI;
extern MethodInfo m19829_MI;
extern MethodInfo m19830_MI;
extern MethodInfo m19824_MI;
extern MethodInfo m19832_MI;
extern MethodInfo m19833_MI;
extern MethodInfo m19836_MI;
extern MethodInfo m19837_MI;
extern MethodInfo m19838_MI;
extern MethodInfo m19839_MI;
extern MethodInfo m19840_MI;
extern MethodInfo m19841_MI;
extern MethodInfo m19835_MI;
extern MethodInfo m19843_MI;
extern MethodInfo m19844_MI;
extern MethodInfo m19847_MI;
extern MethodInfo m19848_MI;
extern MethodInfo m19849_MI;
extern MethodInfo m19850_MI;
extern MethodInfo m19851_MI;
extern MethodInfo m19852_MI;
extern MethodInfo m19846_MI;
extern MethodInfo m19854_MI;
extern MethodInfo m19855_MI;
static MethodInfo* t1145_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24213_MI,
	&m5907_MI,
	&m24214_MI,
	&m24215_MI,
	&m24216_MI,
	&m24217_MI,
	&m24218_MI,
	&m5908_MI,
	&m24212_MI,
	&m24220_MI,
	&m24221_MI,
	&m5905_MI,
	&m5906_MI,
	&m24224_MI,
	&m5907_MI,
	&m24225_MI,
	&m24226_MI,
	&m24227_MI,
	&m24228_MI,
	&m24229_MI,
	&m5908_MI,
	&m24223_MI,
	&m24231_MI,
	&m24232_MI,
	&m5905_MI,
	&m5906_MI,
	&m24235_MI,
	&m5907_MI,
	&m24236_MI,
	&m24237_MI,
	&m24238_MI,
	&m24239_MI,
	&m24240_MI,
	&m5908_MI,
	&m24234_MI,
	&m24242_MI,
	&m24243_MI,
	&m5905_MI,
	&m5906_MI,
	&m24246_MI,
	&m5907_MI,
	&m24247_MI,
	&m24248_MI,
	&m24249_MI,
	&m24250_MI,
	&m24251_MI,
	&m5908_MI,
	&m24245_MI,
	&m24253_MI,
	&m24254_MI,
	&m5905_MI,
	&m5906_MI,
	&m19825_MI,
	&m5907_MI,
	&m19826_MI,
	&m19827_MI,
	&m19828_MI,
	&m19829_MI,
	&m19830_MI,
	&m5908_MI,
	&m19824_MI,
	&m19832_MI,
	&m19833_MI,
	&m5905_MI,
	&m5906_MI,
	&m19836_MI,
	&m5907_MI,
	&m19837_MI,
	&m19838_MI,
	&m19839_MI,
	&m19840_MI,
	&m19841_MI,
	&m5908_MI,
	&m19835_MI,
	&m19843_MI,
	&m19844_MI,
	&m5905_MI,
	&m5906_MI,
	&m19847_MI,
	&m5907_MI,
	&m19848_MI,
	&m19849_MI,
	&m19850_MI,
	&m19851_MI,
	&m19852_MI,
	&m5908_MI,
	&m19846_MI,
	&m19854_MI,
	&m19855_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6078_TI;
extern TypeInfo t6079_TI;
extern TypeInfo t6080_TI;
extern TypeInfo t6081_TI;
extern TypeInfo t6082_TI;
extern TypeInfo t6083_TI;
extern TypeInfo t6084_TI;
extern TypeInfo t6085_TI;
extern TypeInfo t6086_TI;
extern TypeInfo t6087_TI;
extern TypeInfo t6088_TI;
extern TypeInfo t6089_TI;
extern TypeInfo t5131_TI;
extern TypeInfo t5132_TI;
extern TypeInfo t5133_TI;
extern TypeInfo t5134_TI;
extern TypeInfo t5135_TI;
extern TypeInfo t5136_TI;
extern TypeInfo t5137_TI;
extern TypeInfo t5138_TI;
extern TypeInfo t5139_TI;
static TypeInfo* t1145_ITIs[] = 
{
	&t6078_TI,
	&t6079_TI,
	&t6080_TI,
	&t6081_TI,
	&t6082_TI,
	&t6083_TI,
	&t6084_TI,
	&t6085_TI,
	&t6086_TI,
	&t6087_TI,
	&t6088_TI,
	&t6089_TI,
	&t5131_TI,
	&t5132_TI,
	&t5133_TI,
	&t5134_TI,
	&t5135_TI,
	&t5136_TI,
	&t5137_TI,
	&t5138_TI,
	&t5139_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t1145_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6078_TI, 21},
	{ &t6079_TI, 28},
	{ &t6080_TI, 33},
	{ &t6081_TI, 34},
	{ &t6082_TI, 41},
	{ &t6083_TI, 46},
	{ &t6084_TI, 47},
	{ &t6085_TI, 54},
	{ &t6086_TI, 59},
	{ &t6087_TI, 60},
	{ &t6088_TI, 67},
	{ &t6089_TI, 72},
	{ &t5131_TI, 73},
	{ &t5132_TI, 80},
	{ &t5133_TI, 85},
	{ &t5134_TI, 86},
	{ &t5135_TI, 93},
	{ &t5136_TI, 98},
	{ &t5137_TI, 99},
	{ &t5138_TI, 106},
	{ &t5139_TI, 111},
	{ &t2182_TI, 112},
	{ &t2186_TI, 119},
	{ &t2183_TI, 124},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1145_0_0_0;
extern Il2CppType t1145_1_0_0;
struct t557;
extern TypeInfo t557_TI;
extern CustomAttributesCache t557__CustomAttributeCache;
extern CustomAttributesCache t557__CustomAttributeCache_m7558;
TypeInfo t1145_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MethodInfo[]", "System.Reflection", t1145_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t557_TI, t1145_ITIs, t1145_VT, &EmptyCustomAttributesCache, &t1145_TI, &t1145_0_0_0, &t1145_1_0_0, t1145_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t557 *), -1, 0, 0, -1, 1056897, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 125, 24, 28};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3573_TI;



// Metadata Definition System.Runtime.InteropServices._MethodInfo[]
static MethodInfo* t3573_MIs[] =
{
	NULL
};
static MethodInfo* t3573_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24224_MI,
	&m5907_MI,
	&m24225_MI,
	&m24226_MI,
	&m24227_MI,
	&m24228_MI,
	&m24229_MI,
	&m5908_MI,
	&m24223_MI,
	&m24231_MI,
	&m24232_MI,
};
static TypeInfo* t3573_ITIs[] = 
{
	&t6081_TI,
	&t6082_TI,
	&t6083_TI,
};
static Il2CppInterfaceOffsetPair t3573_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6081_TI, 21},
	{ &t6082_TI, 28},
	{ &t6083_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3573_0_0_0;
extern Il2CppType t3573_1_0_0;
struct t2015;
extern TypeInfo t2015_TI;
extern CustomAttributesCache t2015__CustomAttributeCache;
TypeInfo t3573_TI = 
{
	&g_mscorlib_dll_Image, NULL, "_MethodInfo[]", "System.Runtime.InteropServices", t3573_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t2015_TI, t3573_ITIs, t3573_VT, &EmptyCustomAttributesCache, &t3573_TI, &t3573_0_0_0, &t3573_1_0_0, t3573_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1365_TI;



// Metadata Definition System.Reflection.MethodBase[]
static MethodInfo* t1365_MIs[] =
{
	NULL
};
static MethodInfo* t1365_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24235_MI,
	&m5907_MI,
	&m24236_MI,
	&m24237_MI,
	&m24238_MI,
	&m24239_MI,
	&m24240_MI,
	&m5908_MI,
	&m24234_MI,
	&m24242_MI,
	&m24243_MI,
	&m5905_MI,
	&m5906_MI,
	&m24246_MI,
	&m5907_MI,
	&m24247_MI,
	&m24248_MI,
	&m24249_MI,
	&m24250_MI,
	&m24251_MI,
	&m5908_MI,
	&m24245_MI,
	&m24253_MI,
	&m24254_MI,
	&m5905_MI,
	&m5906_MI,
	&m19825_MI,
	&m5907_MI,
	&m19826_MI,
	&m19827_MI,
	&m19828_MI,
	&m19829_MI,
	&m19830_MI,
	&m5908_MI,
	&m19824_MI,
	&m19832_MI,
	&m19833_MI,
	&m5905_MI,
	&m5906_MI,
	&m19836_MI,
	&m5907_MI,
	&m19837_MI,
	&m19838_MI,
	&m19839_MI,
	&m19840_MI,
	&m19841_MI,
	&m5908_MI,
	&m19835_MI,
	&m19843_MI,
	&m19844_MI,
	&m5905_MI,
	&m5906_MI,
	&m19847_MI,
	&m5907_MI,
	&m19848_MI,
	&m19849_MI,
	&m19850_MI,
	&m19851_MI,
	&m19852_MI,
	&m5908_MI,
	&m19846_MI,
	&m19854_MI,
	&m19855_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
static TypeInfo* t1365_ITIs[] = 
{
	&t6084_TI,
	&t6085_TI,
	&t6086_TI,
	&t6087_TI,
	&t6088_TI,
	&t6089_TI,
	&t5131_TI,
	&t5132_TI,
	&t5133_TI,
	&t5134_TI,
	&t5135_TI,
	&t5136_TI,
	&t5137_TI,
	&t5138_TI,
	&t5139_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t1365_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6084_TI, 21},
	{ &t6085_TI, 28},
	{ &t6086_TI, 33},
	{ &t6087_TI, 34},
	{ &t6088_TI, 41},
	{ &t6089_TI, 46},
	{ &t5131_TI, 47},
	{ &t5132_TI, 54},
	{ &t5133_TI, 59},
	{ &t5134_TI, 60},
	{ &t5135_TI, 67},
	{ &t5136_TI, 72},
	{ &t5137_TI, 73},
	{ &t5138_TI, 80},
	{ &t5139_TI, 85},
	{ &t2182_TI, 86},
	{ &t2186_TI, 93},
	{ &t2183_TI, 98},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1365_0_0_0;
extern Il2CppType t1365_1_0_0;
struct t636;
extern TypeInfo t636_TI;
extern CustomAttributesCache t636__CustomAttributeCache;
extern CustomAttributesCache t636__CustomAttributeCache_m7546;
extern CustomAttributesCache t636__CustomAttributeCache_m7550;
TypeInfo t1365_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MethodBase[]", "System.Reflection", t1365_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t636_TI, t1365_ITIs, t1365_VT, &EmptyCustomAttributesCache, &t1365_TI, &t1365_0_0_0, &t1365_1_0_0, t1365_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t636 *), -1, 0, 0, -1, 1056897, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 99, 18, 22};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3574_TI;



// Metadata Definition System.Runtime.InteropServices._MethodBase[]
static MethodInfo* t3574_MIs[] =
{
	NULL
};
static MethodInfo* t3574_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24246_MI,
	&m5907_MI,
	&m24247_MI,
	&m24248_MI,
	&m24249_MI,
	&m24250_MI,
	&m24251_MI,
	&m5908_MI,
	&m24245_MI,
	&m24253_MI,
	&m24254_MI,
};
static TypeInfo* t3574_ITIs[] = 
{
	&t6087_TI,
	&t6088_TI,
	&t6089_TI,
};
static Il2CppInterfaceOffsetPair t3574_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6087_TI, 21},
	{ &t6088_TI, 28},
	{ &t6089_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3574_0_0_0;
extern Il2CppType t3574_1_0_0;
struct t2010;
extern TypeInfo t2010_TI;
extern CustomAttributesCache t2010__CustomAttributeCache;
TypeInfo t3574_TI = 
{
	&g_mscorlib_dll_Image, NULL, "_MethodBase[]", "System.Runtime.InteropServices", t3574_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t2010_TI, t3574_ITIs, t3574_VT, &EmptyCustomAttributesCache, &t3574_TI, &t3574_0_0_0, &t3574_1_0_0, t3574_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1147_TI;



// Metadata Definition System.Reflection.ConstructorInfo[]
static MethodInfo* t1147_MIs[] =
{
	NULL
};
extern MethodInfo m24257_MI;
extern MethodInfo m24258_MI;
extern MethodInfo m24259_MI;
extern MethodInfo m24260_MI;
extern MethodInfo m24261_MI;
extern MethodInfo m24262_MI;
extern MethodInfo m24256_MI;
extern MethodInfo m24264_MI;
extern MethodInfo m24265_MI;
extern MethodInfo m24268_MI;
extern MethodInfo m24269_MI;
extern MethodInfo m24270_MI;
extern MethodInfo m24271_MI;
extern MethodInfo m24272_MI;
extern MethodInfo m24273_MI;
extern MethodInfo m24267_MI;
extern MethodInfo m24275_MI;
extern MethodInfo m24276_MI;
static MethodInfo* t1147_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24257_MI,
	&m5907_MI,
	&m24258_MI,
	&m24259_MI,
	&m24260_MI,
	&m24261_MI,
	&m24262_MI,
	&m5908_MI,
	&m24256_MI,
	&m24264_MI,
	&m24265_MI,
	&m5905_MI,
	&m5906_MI,
	&m24268_MI,
	&m5907_MI,
	&m24269_MI,
	&m24270_MI,
	&m24271_MI,
	&m24272_MI,
	&m24273_MI,
	&m5908_MI,
	&m24267_MI,
	&m24275_MI,
	&m24276_MI,
	&m5905_MI,
	&m5906_MI,
	&m24235_MI,
	&m5907_MI,
	&m24236_MI,
	&m24237_MI,
	&m24238_MI,
	&m24239_MI,
	&m24240_MI,
	&m5908_MI,
	&m24234_MI,
	&m24242_MI,
	&m24243_MI,
	&m5905_MI,
	&m5906_MI,
	&m24246_MI,
	&m5907_MI,
	&m24247_MI,
	&m24248_MI,
	&m24249_MI,
	&m24250_MI,
	&m24251_MI,
	&m5908_MI,
	&m24245_MI,
	&m24253_MI,
	&m24254_MI,
	&m5905_MI,
	&m5906_MI,
	&m19825_MI,
	&m5907_MI,
	&m19826_MI,
	&m19827_MI,
	&m19828_MI,
	&m19829_MI,
	&m19830_MI,
	&m5908_MI,
	&m19824_MI,
	&m19832_MI,
	&m19833_MI,
	&m5905_MI,
	&m5906_MI,
	&m19836_MI,
	&m5907_MI,
	&m19837_MI,
	&m19838_MI,
	&m19839_MI,
	&m19840_MI,
	&m19841_MI,
	&m5908_MI,
	&m19835_MI,
	&m19843_MI,
	&m19844_MI,
	&m5905_MI,
	&m5906_MI,
	&m19847_MI,
	&m5907_MI,
	&m19848_MI,
	&m19849_MI,
	&m19850_MI,
	&m19851_MI,
	&m19852_MI,
	&m5908_MI,
	&m19846_MI,
	&m19854_MI,
	&m19855_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6090_TI;
extern TypeInfo t6091_TI;
extern TypeInfo t6092_TI;
extern TypeInfo t6093_TI;
extern TypeInfo t6094_TI;
extern TypeInfo t6095_TI;
static TypeInfo* t1147_ITIs[] = 
{
	&t6090_TI,
	&t6091_TI,
	&t6092_TI,
	&t6093_TI,
	&t6094_TI,
	&t6095_TI,
	&t6084_TI,
	&t6085_TI,
	&t6086_TI,
	&t6087_TI,
	&t6088_TI,
	&t6089_TI,
	&t5131_TI,
	&t5132_TI,
	&t5133_TI,
	&t5134_TI,
	&t5135_TI,
	&t5136_TI,
	&t5137_TI,
	&t5138_TI,
	&t5139_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t1147_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6090_TI, 21},
	{ &t6091_TI, 28},
	{ &t6092_TI, 33},
	{ &t6093_TI, 34},
	{ &t6094_TI, 41},
	{ &t6095_TI, 46},
	{ &t6084_TI, 47},
	{ &t6085_TI, 54},
	{ &t6086_TI, 59},
	{ &t6087_TI, 60},
	{ &t6088_TI, 67},
	{ &t6089_TI, 72},
	{ &t5131_TI, 73},
	{ &t5132_TI, 80},
	{ &t5133_TI, 85},
	{ &t5134_TI, 86},
	{ &t5135_TI, 93},
	{ &t5136_TI, 98},
	{ &t5137_TI, 99},
	{ &t5138_TI, 106},
	{ &t5139_TI, 111},
	{ &t2182_TI, 112},
	{ &t2186_TI, 119},
	{ &t2183_TI, 124},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1147_0_0_0;
extern Il2CppType t1147_1_0_0;
struct t660;
extern TypeInfo t660_TI;
extern CustomAttributesCache t660__CustomAttributeCache;
extern CustomAttributesCache t660__CustomAttributeCache_ConstructorName;
extern CustomAttributesCache t660__CustomAttributeCache_TypeConstructorName;
extern CustomAttributesCache t660__CustomAttributeCache_m2982;
extern CustomAttributesCache t660__CustomAttributeCache_t660____MemberType_PropertyInfo;
TypeInfo t1147_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ConstructorInfo[]", "System.Reflection", t1147_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t660_TI, t1147_ITIs, t1147_VT, &EmptyCustomAttributesCache, &t1147_TI, &t1147_0_0_0, &t1147_1_0_0, t1147_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t660 *), -1, sizeof(t1147_SFs), 0, -1, 1056897, 1, false, false, false, false, false, true, false, false, false, true, false, false, 0, 0, 0, 0, 0, 125, 24, 28};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3575_TI;



// Metadata Definition System.Runtime.InteropServices._ConstructorInfo[]
static MethodInfo* t3575_MIs[] =
{
	NULL
};
static MethodInfo* t3575_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24268_MI,
	&m5907_MI,
	&m24269_MI,
	&m24270_MI,
	&m24271_MI,
	&m24272_MI,
	&m24273_MI,
	&m5908_MI,
	&m24267_MI,
	&m24275_MI,
	&m24276_MI,
};
static TypeInfo* t3575_ITIs[] = 
{
	&t6093_TI,
	&t6094_TI,
	&t6095_TI,
};
static Il2CppInterfaceOffsetPair t3575_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6093_TI, 21},
	{ &t6094_TI, 28},
	{ &t6095_TI, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3575_0_0_0;
extern Il2CppType t3575_1_0_0;
struct t2009;
extern TypeInfo t2009_TI;
extern CustomAttributesCache t2009__CustomAttributeCache;
TypeInfo t3575_TI = 
{
	&g_mscorlib_dll_Image, NULL, "_ConstructorInfo[]", "System.Runtime.InteropServices", t3575_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t2009_TI, t3575_ITIs, t3575_VT, &EmptyCustomAttributesCache, &t3575_TI, &t3575_0_0_0, &t3575_1_0_0, t3575_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2057_TI;



// Metadata Definition System.MonoType[]
static MethodInfo* t2057_MIs[] =
{
	NULL
};
extern MethodInfo m24279_MI;
extern MethodInfo m24280_MI;
extern MethodInfo m24281_MI;
extern MethodInfo m24282_MI;
extern MethodInfo m24283_MI;
extern MethodInfo m24284_MI;
extern MethodInfo m24278_MI;
extern MethodInfo m24286_MI;
extern MethodInfo m24287_MI;
extern MethodInfo m19792_MI;
extern MethodInfo m19793_MI;
extern MethodInfo m19794_MI;
extern MethodInfo m19795_MI;
extern MethodInfo m19796_MI;
extern MethodInfo m19797_MI;
extern MethodInfo m19791_MI;
extern MethodInfo m19799_MI;
extern MethodInfo m19800_MI;
extern MethodInfo m19803_MI;
extern MethodInfo m19804_MI;
extern MethodInfo m19805_MI;
extern MethodInfo m19806_MI;
extern MethodInfo m19807_MI;
extern MethodInfo m19808_MI;
extern MethodInfo m19802_MI;
extern MethodInfo m19810_MI;
extern MethodInfo m19811_MI;
extern MethodInfo m19814_MI;
extern MethodInfo m19815_MI;
extern MethodInfo m19816_MI;
extern MethodInfo m19817_MI;
extern MethodInfo m19818_MI;
extern MethodInfo m19819_MI;
extern MethodInfo m19813_MI;
extern MethodInfo m19821_MI;
extern MethodInfo m19822_MI;
static MethodInfo* t2057_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24279_MI,
	&m5907_MI,
	&m24280_MI,
	&m24281_MI,
	&m24282_MI,
	&m24283_MI,
	&m24284_MI,
	&m5908_MI,
	&m24278_MI,
	&m24286_MI,
	&m24287_MI,
	&m5905_MI,
	&m5906_MI,
	&m22439_MI,
	&m5907_MI,
	&m22440_MI,
	&m22441_MI,
	&m22442_MI,
	&m22443_MI,
	&m22444_MI,
	&m5908_MI,
	&m22438_MI,
	&m22446_MI,
	&m22447_MI,
	&m5905_MI,
	&m5906_MI,
	&m19792_MI,
	&m5907_MI,
	&m19793_MI,
	&m19794_MI,
	&m19795_MI,
	&m19796_MI,
	&m19797_MI,
	&m5908_MI,
	&m19791_MI,
	&m19799_MI,
	&m19800_MI,
	&m5905_MI,
	&m5906_MI,
	&m19803_MI,
	&m5907_MI,
	&m19804_MI,
	&m19805_MI,
	&m19806_MI,
	&m19807_MI,
	&m19808_MI,
	&m5908_MI,
	&m19802_MI,
	&m19810_MI,
	&m19811_MI,
	&m5905_MI,
	&m5906_MI,
	&m19814_MI,
	&m5907_MI,
	&m19815_MI,
	&m19816_MI,
	&m19817_MI,
	&m19818_MI,
	&m19819_MI,
	&m5908_MI,
	&m19813_MI,
	&m19821_MI,
	&m19822_MI,
	&m5905_MI,
	&m5906_MI,
	&m19825_MI,
	&m5907_MI,
	&m19826_MI,
	&m19827_MI,
	&m19828_MI,
	&m19829_MI,
	&m19830_MI,
	&m5908_MI,
	&m19824_MI,
	&m19832_MI,
	&m19833_MI,
	&m5905_MI,
	&m5906_MI,
	&m19836_MI,
	&m5907_MI,
	&m19837_MI,
	&m19838_MI,
	&m19839_MI,
	&m19840_MI,
	&m19841_MI,
	&m5908_MI,
	&m19835_MI,
	&m19843_MI,
	&m19844_MI,
	&m5905_MI,
	&m5906_MI,
	&m19847_MI,
	&m5907_MI,
	&m19848_MI,
	&m19849_MI,
	&m19850_MI,
	&m19851_MI,
	&m19852_MI,
	&m5908_MI,
	&m19846_MI,
	&m19854_MI,
	&m19855_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6096_TI;
extern TypeInfo t6097_TI;
extern TypeInfo t6098_TI;
extern TypeInfo t3069_TI;
extern TypeInfo t3075_TI;
extern TypeInfo t3070_TI;
extern TypeInfo t5125_TI;
extern TypeInfo t5126_TI;
extern TypeInfo t5127_TI;
extern TypeInfo t5128_TI;
extern TypeInfo t5129_TI;
extern TypeInfo t5130_TI;
static TypeInfo* t2057_ITIs[] = 
{
	&t6096_TI,
	&t6097_TI,
	&t6098_TI,
	&t5649_TI,
	&t5650_TI,
	&t5651_TI,
	&t3069_TI,
	&t3075_TI,
	&t3070_TI,
	&t5125_TI,
	&t5126_TI,
	&t5127_TI,
	&t5128_TI,
	&t5129_TI,
	&t5130_TI,
	&t5131_TI,
	&t5132_TI,
	&t5133_TI,
	&t5134_TI,
	&t5135_TI,
	&t5136_TI,
	&t5137_TI,
	&t5138_TI,
	&t5139_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2057_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6096_TI, 21},
	{ &t6097_TI, 28},
	{ &t6098_TI, 33},
	{ &t5649_TI, 34},
	{ &t5650_TI, 41},
	{ &t5651_TI, 46},
	{ &t3069_TI, 47},
	{ &t3075_TI, 54},
	{ &t3070_TI, 59},
	{ &t5125_TI, 60},
	{ &t5126_TI, 67},
	{ &t5127_TI, 72},
	{ &t5128_TI, 73},
	{ &t5129_TI, 80},
	{ &t5130_TI, 85},
	{ &t5131_TI, 86},
	{ &t5132_TI, 93},
	{ &t5133_TI, 98},
	{ &t5134_TI, 99},
	{ &t5135_TI, 106},
	{ &t5136_TI, 111},
	{ &t5137_TI, 112},
	{ &t5138_TI, 119},
	{ &t5139_TI, 124},
	{ &t2182_TI, 125},
	{ &t2186_TI, 132},
	{ &t2183_TI, 137},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2057_0_0_0;
extern Il2CppType t2057_1_0_0;
struct t1660;
extern TypeInfo t1660_TI;
TypeInfo t2057_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MonoType[]", "System", t2057_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1660_TI, t2057_ITIs, t2057_VT, &EmptyCustomAttributesCache, &t2057_TI, &t2057_0_0_0, &t2057_1_0_0, t2057_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1660 *), -1, 0, 0, -1, 1056768, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 138, 27, 31};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3576_TI;



// Metadata Definition System.ParamArrayAttribute[]
static MethodInfo* t3576_MIs[] =
{
	NULL
};
extern MethodInfo m24290_MI;
extern MethodInfo m24291_MI;
extern MethodInfo m24292_MI;
extern MethodInfo m24293_MI;
extern MethodInfo m24294_MI;
extern MethodInfo m24295_MI;
extern MethodInfo m24289_MI;
extern MethodInfo m24297_MI;
extern MethodInfo m24298_MI;
static MethodInfo* t3576_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24290_MI,
	&m5907_MI,
	&m24291_MI,
	&m24292_MI,
	&m24293_MI,
	&m24294_MI,
	&m24295_MI,
	&m5908_MI,
	&m24289_MI,
	&m24297_MI,
	&m24298_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6099_TI;
extern TypeInfo t6100_TI;
extern TypeInfo t6101_TI;
static TypeInfo* t3576_ITIs[] = 
{
	&t6099_TI,
	&t6100_TI,
	&t6101_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3576_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6099_TI, 21},
	{ &t6100_TI, 28},
	{ &t6101_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3576_0_0_0;
extern Il2CppType t3576_1_0_0;
struct t391;
extern TypeInfo t391_TI;
extern CustomAttributesCache t391__CustomAttributeCache;
TypeInfo t3576_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ParamArrayAttribute[]", "System", t3576_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t391_TI, t3576_ITIs, t3576_VT, &EmptyCustomAttributesCache, &t3576_TI, &t3576_0_0_0, &t3576_1_0_0, t3576_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t391 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3577_TI;



// Metadata Definition System.Runtime.InteropServices.OutAttribute[]
static MethodInfo* t3577_MIs[] =
{
	NULL
};
extern MethodInfo m24301_MI;
extern MethodInfo m24302_MI;
extern MethodInfo m24303_MI;
extern MethodInfo m24304_MI;
extern MethodInfo m24305_MI;
extern MethodInfo m24306_MI;
extern MethodInfo m24300_MI;
extern MethodInfo m24308_MI;
extern MethodInfo m24309_MI;
static MethodInfo* t3577_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24301_MI,
	&m5907_MI,
	&m24302_MI,
	&m24303_MI,
	&m24304_MI,
	&m24305_MI,
	&m24306_MI,
	&m5908_MI,
	&m24300_MI,
	&m24308_MI,
	&m24309_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6102_TI;
extern TypeInfo t6103_TI;
extern TypeInfo t6104_TI;
static TypeInfo* t3577_ITIs[] = 
{
	&t6102_TI,
	&t6103_TI,
	&t6104_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3577_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6102_TI, 21},
	{ &t6103_TI, 28},
	{ &t6104_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3577_0_0_0;
extern Il2CppType t3577_1_0_0;
struct t1151;
extern TypeInfo t1151_TI;
extern CustomAttributesCache t1151__CustomAttributeCache;
TypeInfo t3577_TI = 
{
	&g_mscorlib_dll_Image, NULL, "OutAttribute[]", "System.Runtime.InteropServices", t3577_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1151_TI, t3577_ITIs, t3577_VT, &EmptyCustomAttributesCache, &t3577_TI, &t3577_0_0_0, &t3577_1_0_0, t3577_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1151 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3578_TI;



// Metadata Definition System.ObsoleteAttribute[]
static MethodInfo* t3578_MIs[] =
{
	NULL
};
extern MethodInfo m24312_MI;
extern MethodInfo m24313_MI;
extern MethodInfo m24314_MI;
extern MethodInfo m24315_MI;
extern MethodInfo m24316_MI;
extern MethodInfo m24317_MI;
extern MethodInfo m24311_MI;
extern MethodInfo m24319_MI;
extern MethodInfo m24320_MI;
static MethodInfo* t3578_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24312_MI,
	&m5907_MI,
	&m24313_MI,
	&m24314_MI,
	&m24315_MI,
	&m24316_MI,
	&m24317_MI,
	&m5908_MI,
	&m24311_MI,
	&m24319_MI,
	&m24320_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6105_TI;
extern TypeInfo t6106_TI;
extern TypeInfo t6107_TI;
static TypeInfo* t3578_ITIs[] = 
{
	&t6105_TI,
	&t6106_TI,
	&t6107_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3578_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6105_TI, 21},
	{ &t6106_TI, 28},
	{ &t6107_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3578_0_0_0;
extern Il2CppType t3578_1_0_0;
struct t327;
extern TypeInfo t327_TI;
extern CustomAttributesCache t327__CustomAttributeCache;
TypeInfo t3578_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ObsoleteAttribute[]", "System", t3578_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t327_TI, t3578_ITIs, t3578_VT, &EmptyCustomAttributesCache, &t3578_TI, &t3578_0_0_0, &t3578_1_0_0, t3578_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t327 *), -1, 0, 0, -1, 1057025, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3579_TI;



// Metadata Definition System.Runtime.InteropServices.DllImportAttribute[]
static MethodInfo* t3579_MIs[] =
{
	NULL
};
extern MethodInfo m24323_MI;
extern MethodInfo m24324_MI;
extern MethodInfo m24325_MI;
extern MethodInfo m24326_MI;
extern MethodInfo m24327_MI;
extern MethodInfo m24328_MI;
extern MethodInfo m24322_MI;
extern MethodInfo m24330_MI;
extern MethodInfo m24331_MI;
static MethodInfo* t3579_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24323_MI,
	&m5907_MI,
	&m24324_MI,
	&m24325_MI,
	&m24326_MI,
	&m24327_MI,
	&m24328_MI,
	&m5908_MI,
	&m24322_MI,
	&m24330_MI,
	&m24331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6108_TI;
extern TypeInfo t6109_TI;
extern TypeInfo t6110_TI;
static TypeInfo* t3579_ITIs[] = 
{
	&t6108_TI,
	&t6109_TI,
	&t6110_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3579_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6108_TI, 21},
	{ &t6109_TI, 28},
	{ &t6110_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3579_0_0_0;
extern Il2CppType t3579_1_0_0;
struct t1152;
extern TypeInfo t1152_TI;
extern CustomAttributesCache t1152__CustomAttributeCache;
TypeInfo t3579_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DllImportAttribute[]", "System.Runtime.InteropServices", t3579_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1152_TI, t3579_ITIs, t3579_VT, &EmptyCustomAttributesCache, &t3579_TI, &t3579_0_0_0, &t3579_1_0_0, t3579_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1152 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3580_TI;



// Metadata Definition System.Runtime.InteropServices.MarshalAsAttribute[]
static MethodInfo* t3580_MIs[] =
{
	NULL
};
extern MethodInfo m24334_MI;
extern MethodInfo m24335_MI;
extern MethodInfo m24336_MI;
extern MethodInfo m24337_MI;
extern MethodInfo m24338_MI;
extern MethodInfo m24339_MI;
extern MethodInfo m24333_MI;
extern MethodInfo m24341_MI;
extern MethodInfo m24342_MI;
static MethodInfo* t3580_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24334_MI,
	&m5907_MI,
	&m24335_MI,
	&m24336_MI,
	&m24337_MI,
	&m24338_MI,
	&m24339_MI,
	&m5908_MI,
	&m24333_MI,
	&m24341_MI,
	&m24342_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6111_TI;
extern TypeInfo t6112_TI;
extern TypeInfo t6113_TI;
static TypeInfo* t3580_ITIs[] = 
{
	&t6111_TI,
	&t6112_TI,
	&t6113_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3580_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6111_TI, 21},
	{ &t6112_TI, 28},
	{ &t6113_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3580_0_0_0;
extern Il2CppType t3580_1_0_0;
struct t1155;
extern TypeInfo t1155_TI;
extern CustomAttributesCache t1155__CustomAttributeCache;
extern CustomAttributesCache t1155__CustomAttributeCache_MarshalType;
extern CustomAttributesCache t1155__CustomAttributeCache_MarshalTypeRef;
TypeInfo t3580_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MarshalAsAttribute[]", "System.Runtime.InteropServices", t3580_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1155_TI, t3580_ITIs, t3580_VT, &EmptyCustomAttributesCache, &t3580_TI, &t3580_0_0_0, &t3580_1_0_0, t3580_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1155 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3581_TI;



// Metadata Definition System.Runtime.InteropServices.InAttribute[]
static MethodInfo* t3581_MIs[] =
{
	NULL
};
extern MethodInfo m24345_MI;
extern MethodInfo m24346_MI;
extern MethodInfo m24347_MI;
extern MethodInfo m24348_MI;
extern MethodInfo m24349_MI;
extern MethodInfo m24350_MI;
extern MethodInfo m24344_MI;
extern MethodInfo m24352_MI;
extern MethodInfo m24353_MI;
static MethodInfo* t3581_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24345_MI,
	&m5907_MI,
	&m24346_MI,
	&m24347_MI,
	&m24348_MI,
	&m24349_MI,
	&m24350_MI,
	&m5908_MI,
	&m24344_MI,
	&m24352_MI,
	&m24353_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6114_TI;
extern TypeInfo t6115_TI;
extern TypeInfo t6116_TI;
static TypeInfo* t3581_ITIs[] = 
{
	&t6114_TI,
	&t6115_TI,
	&t6116_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3581_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6114_TI, 21},
	{ &t6115_TI, 28},
	{ &t6116_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3581_0_0_0;
extern Il2CppType t3581_1_0_0;
struct t1157;
extern TypeInfo t1157_TI;
extern CustomAttributesCache t1157__CustomAttributeCache;
TypeInfo t3581_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InAttribute[]", "System.Runtime.InteropServices", t3581_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1157_TI, t3581_ITIs, t3581_VT, &EmptyCustomAttributesCache, &t3581_TI, &t3581_0_0_0, &t3581_1_0_0, t3581_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1157 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3582_TI;



// Metadata Definition System.Runtime.InteropServices.GuidAttribute[]
static MethodInfo* t3582_MIs[] =
{
	NULL
};
extern MethodInfo m24356_MI;
extern MethodInfo m24357_MI;
extern MethodInfo m24358_MI;
extern MethodInfo m24359_MI;
extern MethodInfo m24360_MI;
extern MethodInfo m24361_MI;
extern MethodInfo m24355_MI;
extern MethodInfo m24363_MI;
extern MethodInfo m24364_MI;
static MethodInfo* t3582_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24356_MI,
	&m5907_MI,
	&m24357_MI,
	&m24358_MI,
	&m24359_MI,
	&m24360_MI,
	&m24361_MI,
	&m5908_MI,
	&m24355_MI,
	&m24363_MI,
	&m24364_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6117_TI;
extern TypeInfo t6118_TI;
extern TypeInfo t6119_TI;
static TypeInfo* t3582_ITIs[] = 
{
	&t6117_TI,
	&t6118_TI,
	&t6119_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3582_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6117_TI, 21},
	{ &t6118_TI, 28},
	{ &t6119_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3582_0_0_0;
extern Il2CppType t3582_1_0_0;
struct t436;
extern TypeInfo t436_TI;
extern CustomAttributesCache t436__CustomAttributeCache;
TypeInfo t3582_TI = 
{
	&g_mscorlib_dll_Image, NULL, "GuidAttribute[]", "System.Runtime.InteropServices", t3582_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t436_TI, t3582_ITIs, t3582_VT, &EmptyCustomAttributesCache, &t3582_TI, &t3582_0_0_0, &t3582_1_0_0, t3582_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t436 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3583_TI;



// Metadata Definition System.Runtime.InteropServices.ComImportAttribute[]
static MethodInfo* t3583_MIs[] =
{
	NULL
};
extern MethodInfo m24367_MI;
extern MethodInfo m24368_MI;
extern MethodInfo m24369_MI;
extern MethodInfo m24370_MI;
extern MethodInfo m24371_MI;
extern MethodInfo m24372_MI;
extern MethodInfo m24366_MI;
extern MethodInfo m24374_MI;
extern MethodInfo m24375_MI;
static MethodInfo* t3583_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24367_MI,
	&m5907_MI,
	&m24368_MI,
	&m24369_MI,
	&m24370_MI,
	&m24371_MI,
	&m24372_MI,
	&m5908_MI,
	&m24366_MI,
	&m24374_MI,
	&m24375_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6120_TI;
extern TypeInfo t6121_TI;
extern TypeInfo t6122_TI;
static TypeInfo* t3583_ITIs[] = 
{
	&t6120_TI,
	&t6121_TI,
	&t6122_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3583_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6120_TI, 21},
	{ &t6121_TI, 28},
	{ &t6122_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3583_0_0_0;
extern Il2CppType t3583_1_0_0;
struct t1158;
extern TypeInfo t1158_TI;
extern CustomAttributesCache t1158__CustomAttributeCache;
TypeInfo t3583_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ComImportAttribute[]", "System.Runtime.InteropServices", t3583_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1158_TI, t3583_ITIs, t3583_VT, &EmptyCustomAttributesCache, &t3583_TI, &t3583_0_0_0, &t3583_1_0_0, t3583_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1158 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3584_TI;



// Metadata Definition System.Runtime.InteropServices.OptionalAttribute[]
static MethodInfo* t3584_MIs[] =
{
	NULL
};
extern MethodInfo m24378_MI;
extern MethodInfo m24379_MI;
extern MethodInfo m24380_MI;
extern MethodInfo m24381_MI;
extern MethodInfo m24382_MI;
extern MethodInfo m24383_MI;
extern MethodInfo m24377_MI;
extern MethodInfo m24385_MI;
extern MethodInfo m24386_MI;
static MethodInfo* t3584_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24378_MI,
	&m5907_MI,
	&m24379_MI,
	&m24380_MI,
	&m24381_MI,
	&m24382_MI,
	&m24383_MI,
	&m5908_MI,
	&m24377_MI,
	&m24385_MI,
	&m24386_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6123_TI;
extern TypeInfo t6124_TI;
extern TypeInfo t6125_TI;
static TypeInfo* t3584_ITIs[] = 
{
	&t6123_TI,
	&t6124_TI,
	&t6125_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3584_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6123_TI, 21},
	{ &t6124_TI, 28},
	{ &t6125_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3584_0_0_0;
extern Il2CppType t3584_1_0_0;
struct t1159;
extern TypeInfo t1159_TI;
extern CustomAttributesCache t1159__CustomAttributeCache;
TypeInfo t3584_TI = 
{
	&g_mscorlib_dll_Image, NULL, "OptionalAttribute[]", "System.Runtime.InteropServices", t3584_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1159_TI, t3584_ITIs, t3584_VT, &EmptyCustomAttributesCache, &t3584_TI, &t3584_0_0_0, &t3584_1_0_0, t3584_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1159 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3585_TI;



// Metadata Definition System.Runtime.CompilerServices.CompilerGeneratedAttribute[]
static MethodInfo* t3585_MIs[] =
{
	NULL
};
extern MethodInfo m24389_MI;
extern MethodInfo m24390_MI;
extern MethodInfo m24391_MI;
extern MethodInfo m24392_MI;
extern MethodInfo m24393_MI;
extern MethodInfo m24394_MI;
extern MethodInfo m24388_MI;
extern MethodInfo m24396_MI;
extern MethodInfo m24397_MI;
static MethodInfo* t3585_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24389_MI,
	&m5907_MI,
	&m24390_MI,
	&m24391_MI,
	&m24392_MI,
	&m24393_MI,
	&m24394_MI,
	&m5908_MI,
	&m24388_MI,
	&m24396_MI,
	&m24397_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6126_TI;
extern TypeInfo t6127_TI;
extern TypeInfo t6128_TI;
static TypeInfo* t3585_ITIs[] = 
{
	&t6126_TI,
	&t6127_TI,
	&t6128_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3585_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6126_TI, 21},
	{ &t6127_TI, 28},
	{ &t6128_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3585_0_0_0;
extern Il2CppType t3585_1_0_0;
struct t38;
extern TypeInfo t38_TI;
extern CustomAttributesCache t38__CustomAttributeCache;
TypeInfo t3585_TI = 
{
	&g_mscorlib_dll_Image, NULL, "CompilerGeneratedAttribute[]", "System.Runtime.CompilerServices", t3585_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t38_TI, t3585_ITIs, t3585_VT, &EmptyCustomAttributesCache, &t3585_TI, &t3585_0_0_0, &t3585_1_0_0, t3585_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t38 *), -1, 0, 0, -1, 1057025, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3586_TI;



// Metadata Definition System.Runtime.CompilerServices.InternalsVisibleToAttribute[]
static MethodInfo* t3586_MIs[] =
{
	NULL
};
extern MethodInfo m24400_MI;
extern MethodInfo m24401_MI;
extern MethodInfo m24402_MI;
extern MethodInfo m24403_MI;
extern MethodInfo m24404_MI;
extern MethodInfo m24405_MI;
extern MethodInfo m24399_MI;
extern MethodInfo m24407_MI;
extern MethodInfo m24408_MI;
static MethodInfo* t3586_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24400_MI,
	&m5907_MI,
	&m24401_MI,
	&m24402_MI,
	&m24403_MI,
	&m24404_MI,
	&m24405_MI,
	&m5908_MI,
	&m24399_MI,
	&m24407_MI,
	&m24408_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6129_TI;
extern TypeInfo t6130_TI;
extern TypeInfo t6131_TI;
static TypeInfo* t3586_ITIs[] = 
{
	&t6129_TI,
	&t6130_TI,
	&t6131_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3586_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6129_TI, 21},
	{ &t6130_TI, 28},
	{ &t6131_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3586_0_0_0;
extern Il2CppType t3586_1_0_0;
struct t685;
extern TypeInfo t685_TI;
extern CustomAttributesCache t685__CustomAttributeCache;
TypeInfo t3586_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalsVisibleToAttribute[]", "System.Runtime.CompilerServices", t3586_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t685_TI, t3586_ITIs, t3586_VT, &EmptyCustomAttributesCache, &t3586_TI, &t3586_0_0_0, &t3586_1_0_0, t3586_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t685 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3587_TI;



// Metadata Definition System.Runtime.CompilerServices.RuntimeCompatibilityAttribute[]
static MethodInfo* t3587_MIs[] =
{
	NULL
};
extern MethodInfo m24411_MI;
extern MethodInfo m24412_MI;
extern MethodInfo m24413_MI;
extern MethodInfo m24414_MI;
extern MethodInfo m24415_MI;
extern MethodInfo m24416_MI;
extern MethodInfo m24410_MI;
extern MethodInfo m24418_MI;
extern MethodInfo m24419_MI;
static MethodInfo* t3587_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24411_MI,
	&m5907_MI,
	&m24412_MI,
	&m24413_MI,
	&m24414_MI,
	&m24415_MI,
	&m24416_MI,
	&m5908_MI,
	&m24410_MI,
	&m24418_MI,
	&m24419_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6132_TI;
extern TypeInfo t6133_TI;
extern TypeInfo t6134_TI;
static TypeInfo* t3587_ITIs[] = 
{
	&t6132_TI,
	&t6133_TI,
	&t6134_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3587_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6132_TI, 21},
	{ &t6133_TI, 28},
	{ &t6134_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3587_0_0_0;
extern Il2CppType t3587_1_0_0;
struct t46;
extern TypeInfo t46_TI;
extern CustomAttributesCache t46__CustomAttributeCache;
TypeInfo t3587_TI = 
{
	&g_mscorlib_dll_Image, NULL, "RuntimeCompatibilityAttribute[]", "System.Runtime.CompilerServices", t3587_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t46_TI, t3587_ITIs, t3587_VT, &EmptyCustomAttributesCache, &t3587_TI, &t3587_0_0_0, &t3587_1_0_0, t3587_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t46 *), -1, 0, 0, -1, 1057025, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3588_TI;



// Metadata Definition System.Diagnostics.DebuggerHiddenAttribute[]
static MethodInfo* t3588_MIs[] =
{
	NULL
};
extern MethodInfo m24422_MI;
extern MethodInfo m24423_MI;
extern MethodInfo m24424_MI;
extern MethodInfo m24425_MI;
extern MethodInfo m24426_MI;
extern MethodInfo m24427_MI;
extern MethodInfo m24421_MI;
extern MethodInfo m24429_MI;
extern MethodInfo m24430_MI;
static MethodInfo* t3588_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24422_MI,
	&m5907_MI,
	&m24423_MI,
	&m24424_MI,
	&m24425_MI,
	&m24426_MI,
	&m24427_MI,
	&m5908_MI,
	&m24421_MI,
	&m24429_MI,
	&m24430_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6135_TI;
extern TypeInfo t6136_TI;
extern TypeInfo t6137_TI;
static TypeInfo* t3588_ITIs[] = 
{
	&t6135_TI,
	&t6136_TI,
	&t6137_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3588_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6135_TI, 21},
	{ &t6136_TI, 28},
	{ &t6137_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3588_0_0_0;
extern Il2CppType t3588_1_0_0;
struct t342;
extern TypeInfo t342_TI;
extern CustomAttributesCache t342__CustomAttributeCache;
TypeInfo t3588_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DebuggerHiddenAttribute[]", "System.Diagnostics", t3588_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t342_TI, t3588_ITIs, t3588_VT, &EmptyCustomAttributesCache, &t3588_TI, &t3588_0_0_0, &t3588_1_0_0, t3588_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t342 *), -1, 0, 0, -1, 1057025, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3589_TI;



// Metadata Definition System.Reflection.DefaultMemberAttribute[]
static MethodInfo* t3589_MIs[] =
{
	NULL
};
extern MethodInfo m24433_MI;
extern MethodInfo m24434_MI;
extern MethodInfo m24435_MI;
extern MethodInfo m24436_MI;
extern MethodInfo m24437_MI;
extern MethodInfo m24438_MI;
extern MethodInfo m24432_MI;
extern MethodInfo m24440_MI;
extern MethodInfo m24441_MI;
static MethodInfo* t3589_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24433_MI,
	&m5907_MI,
	&m24434_MI,
	&m24435_MI,
	&m24436_MI,
	&m24437_MI,
	&m24438_MI,
	&m5908_MI,
	&m24432_MI,
	&m24440_MI,
	&m24441_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6138_TI;
extern TypeInfo t6139_TI;
extern TypeInfo t6140_TI;
static TypeInfo* t3589_ITIs[] = 
{
	&t6138_TI,
	&t6139_TI,
	&t6140_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3589_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6138_TI, 21},
	{ &t6139_TI, 28},
	{ &t6140_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3589_0_0_0;
extern Il2CppType t3589_1_0_0;
struct t425;
extern TypeInfo t425_TI;
extern CustomAttributesCache t425__CustomAttributeCache;
TypeInfo t3589_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultMemberAttribute[]", "System.Reflection", t3589_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t425_TI, t3589_ITIs, t3589_VT, &EmptyCustomAttributesCache, &t3589_TI, &t3589_0_0_0, &t3589_1_0_0, t3589_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t425 *), -1, 0, 0, -1, 1057025, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3590_TI;



// Metadata Definition System.Runtime.CompilerServices.DecimalConstantAttribute[]
static MethodInfo* t3590_MIs[] =
{
	NULL
};
extern MethodInfo m24444_MI;
extern MethodInfo m24445_MI;
extern MethodInfo m24446_MI;
extern MethodInfo m24447_MI;
extern MethodInfo m24448_MI;
extern MethodInfo m24449_MI;
extern MethodInfo m24443_MI;
extern MethodInfo m24451_MI;
extern MethodInfo m24452_MI;
static MethodInfo* t3590_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24444_MI,
	&m5907_MI,
	&m24445_MI,
	&m24446_MI,
	&m24447_MI,
	&m24448_MI,
	&m24449_MI,
	&m5908_MI,
	&m24443_MI,
	&m24451_MI,
	&m24452_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6141_TI;
extern TypeInfo t6142_TI;
extern TypeInfo t6143_TI;
static TypeInfo* t3590_ITIs[] = 
{
	&t6141_TI,
	&t6142_TI,
	&t6143_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3590_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6141_TI, 21},
	{ &t6142_TI, 28},
	{ &t6143_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3590_0_0_0;
extern Il2CppType t3590_1_0_0;
struct t1160;
extern TypeInfo t1160_TI;
extern CustomAttributesCache t1160__CustomAttributeCache;
extern CustomAttributesCache t1160__CustomAttributeCache_m6069;
TypeInfo t3590_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DecimalConstantAttribute[]", "System.Runtime.CompilerServices", t3590_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1160_TI, t3590_ITIs, t3590_VT, &EmptyCustomAttributesCache, &t3590_TI, &t3590_0_0_0, &t3590_1_0_0, t3590_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1160 *), -1, 0, 0, -1, 1057025, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3591_TI;



// Metadata Definition System.Runtime.InteropServices.FieldOffsetAttribute[]
static MethodInfo* t3591_MIs[] =
{
	NULL
};
extern MethodInfo m24455_MI;
extern MethodInfo m24456_MI;
extern MethodInfo m24457_MI;
extern MethodInfo m24458_MI;
extern MethodInfo m24459_MI;
extern MethodInfo m24460_MI;
extern MethodInfo m24454_MI;
extern MethodInfo m24462_MI;
extern MethodInfo m24463_MI;
static MethodInfo* t3591_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24455_MI,
	&m5907_MI,
	&m24456_MI,
	&m24457_MI,
	&m24458_MI,
	&m24459_MI,
	&m24460_MI,
	&m5908_MI,
	&m24454_MI,
	&m24462_MI,
	&m24463_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6144_TI;
extern TypeInfo t6145_TI;
extern TypeInfo t6146_TI;
static TypeInfo* t3591_ITIs[] = 
{
	&t6144_TI,
	&t6145_TI,
	&t6146_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3591_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6144_TI, 21},
	{ &t6145_TI, 28},
	{ &t6146_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3591_0_0_0;
extern Il2CppType t3591_1_0_0;
struct t1161;
extern TypeInfo t1161_TI;
extern CustomAttributesCache t1161__CustomAttributeCache;
TypeInfo t3591_TI = 
{
	&g_mscorlib_dll_Image, NULL, "FieldOffsetAttribute[]", "System.Runtime.InteropServices", t3591_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1161_TI, t3591_ITIs, t3591_VT, &EmptyCustomAttributesCache, &t3591_TI, &t3591_0_0_0, &t3591_1_0_0, t3591_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1161 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3592_TI;



// Metadata Definition System.MonoTODOAttribute[]
static MethodInfo* t3592_MIs[] =
{
	NULL
};
extern MethodInfo m24466_MI;
extern MethodInfo m24467_MI;
extern MethodInfo m24468_MI;
extern MethodInfo m24469_MI;
extern MethodInfo m24470_MI;
extern MethodInfo m24471_MI;
extern MethodInfo m24465_MI;
extern MethodInfo m24473_MI;
extern MethodInfo m24474_MI;
static MethodInfo* t3592_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24466_MI,
	&m5907_MI,
	&m24467_MI,
	&m24468_MI,
	&m24469_MI,
	&m24470_MI,
	&m24471_MI,
	&m5908_MI,
	&m24465_MI,
	&m24473_MI,
	&m24474_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6147_TI;
extern TypeInfo t6148_TI;
extern TypeInfo t6149_TI;
static TypeInfo* t3592_ITIs[] = 
{
	&t6147_TI,
	&t6148_TI,
	&t6149_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3592_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6147_TI, 21},
	{ &t6148_TI, 28},
	{ &t6149_TI, 33},
	{ &t5619_TI, 34},
	{ &t5620_TI, 41},
	{ &t5621_TI, 46},
	{ &t5622_TI, 47},
	{ &t5623_TI, 54},
	{ &t5624_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3592_0_0_0;
extern Il2CppType t3592_1_0_0;
struct t1168;
extern TypeInfo t1168_TI;
extern CustomAttributesCache t1168__CustomAttributeCache;
TypeInfo t3592_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MonoTODOAttribute[]", "System", t3592_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1168_TI, t3592_ITIs, t3592_VT, &EmptyCustomAttributesCache, &t3592_TI, &t3592_0_0_0, &t3592_1_0_0, t3592_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1168 *), -1, 0, 0, -1, 1048576, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3593_TI;



// Metadata Definition System.MonoDocumentationNoteAttribute[]
static MethodInfo* t3593_MIs[] =
{
	NULL
};
extern MethodInfo m24477_MI;
extern MethodInfo m24478_MI;
extern MethodInfo m24479_MI;
extern MethodInfo m24480_MI;
extern MethodInfo m24481_MI;
extern MethodInfo m24482_MI;
extern MethodInfo m24476_MI;
extern MethodInfo m24484_MI;
extern MethodInfo m24485_MI;
static MethodInfo* t3593_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24477_MI,
	&m5907_MI,
	&m24478_MI,
	&m24479_MI,
	&m24480_MI,
	&m24481_MI,
	&m24482_MI,
	&m5908_MI,
	&m24476_MI,
	&m24484_MI,
	&m24485_MI,
	&m5905_MI,
	&m5906_MI,
	&m24466_MI,
	&m5907_MI,
	&m24467_MI,
	&m24468_MI,
	&m24469_MI,
	&m24470_MI,
	&m24471_MI,
	&m5908_MI,
	&m24465_MI,
	&m24473_MI,
	&m24474_MI,
	&m5905_MI,
	&m5906_MI,
	&m22323_MI,
	&m5907_MI,
	&m22324_MI,
	&m22325_MI,
	&m22326_MI,
	&m22327_MI,
	&m22328_MI,
	&m5908_MI,
	&m22322_MI,
	&m22330_MI,
	&m22331_MI,
	&m5905_MI,
	&m5906_MI,
	&m22334_MI,
	&m5907_MI,
	&m22335_MI,
	&m22336_MI,
	&m22337_MI,
	&m22338_MI,
	&m22339_MI,
	&m5908_MI,
	&m22333_MI,
	&m22341_MI,
	&m22342_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6150_TI;
extern TypeInfo t6151_TI;
extern TypeInfo t6152_TI;
static TypeInfo* t3593_ITIs[] = 
{
	&t6150_TI,
	&t6151_TI,
	&t6152_TI,
	&t6147_TI,
	&t6148_TI,
	&t6149_TI,
	&t5619_TI,
	&t5620_TI,
	&t5621_TI,
	&t5622_TI,
	&t5623_TI,
	&t5624_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3593_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6150_TI, 21},
	{ &t6151_TI, 28},
	{ &t6152_TI, 33},
	{ &t6147_TI, 34},
	{ &t6148_TI, 41},
	{ &t6149_TI, 46},
	{ &t5619_TI, 47},
	{ &t5620_TI, 54},
	{ &t5621_TI, 59},
	{ &t5622_TI, 60},
	{ &t5623_TI, 67},
	{ &t5624_TI, 72},
	{ &t2182_TI, 73},
	{ &t2186_TI, 80},
	{ &t2183_TI, 85},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3593_0_0_0;
extern Il2CppType t3593_1_0_0;
struct t1169;
extern TypeInfo t1169_TI;
extern CustomAttributesCache t1169__CustomAttributeCache;
TypeInfo t3593_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MonoDocumentationNoteAttribute[]", "System", t3593_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1169_TI, t3593_ITIs, t3593_VT, &EmptyCustomAttributesCache, &t3593_TI, &t3593_0_0_0, &t3593_1_0_0, t3593_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1169 *), -1, 0, 0, -1, 1048576, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 86, 15, 19};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1175_TI;



// Metadata Definition Mono.Globalization.Unicode.CodePointIndexer/TableRange[]
static MethodInfo* t1175_MIs[] =
{
	NULL
};
extern MethodInfo m24488_MI;
extern MethodInfo m24489_MI;
extern MethodInfo m24490_MI;
extern MethodInfo m24491_MI;
extern MethodInfo m24492_MI;
extern MethodInfo m24493_MI;
extern MethodInfo m24487_MI;
extern MethodInfo m24495_MI;
extern MethodInfo m24496_MI;
static MethodInfo* t1175_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24488_MI,
	&m5907_MI,
	&m24489_MI,
	&m24490_MI,
	&m24491_MI,
	&m24492_MI,
	&m24493_MI,
	&m5908_MI,
	&m24487_MI,
	&m24495_MI,
	&m24496_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6153_TI;
extern TypeInfo t6154_TI;
extern TypeInfo t6155_TI;
static TypeInfo* t1175_ITIs[] = 
{
	&t6153_TI,
	&t6154_TI,
	&t6155_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t1175_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6153_TI, 21},
	{ &t6154_TI, 28},
	{ &t6155_TI, 33},
	{ &t5116_TI, 34},
	{ &t5117_TI, 41},
	{ &t5118_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1175_0_0_0;
extern Il2CppType t1175_1_0_0;
#include "t1173.h"
extern TypeInfo t1173_TI;
TypeInfo t1175_TI = 
{
	&g_mscorlib_dll_Image, NULL, "TableRange[]", "", t1175_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1173_TI, t1175_ITIs, t1175_VT, &EmptyCustomAttributesCache, &t1175_TI, &t1175_0_0_0, &t1175_1_0_0, t1175_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1173 ), -1, 0, 0, -1, 1057037, 1, false, false, false, false, false, true, false, false, false, false, false, true, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1182_TI;



// Metadata Definition Mono.Globalization.Unicode.TailoringInfo[]
static MethodInfo* t1182_MIs[] =
{
	NULL
};
extern MethodInfo m24499_MI;
extern MethodInfo m24500_MI;
extern MethodInfo m24501_MI;
extern MethodInfo m24502_MI;
extern MethodInfo m24503_MI;
extern MethodInfo m24504_MI;
extern MethodInfo m24498_MI;
extern MethodInfo m24506_MI;
extern MethodInfo m24507_MI;
static MethodInfo* t1182_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24499_MI,
	&m5907_MI,
	&m24500_MI,
	&m24501_MI,
	&m24502_MI,
	&m24503_MI,
	&m24504_MI,
	&m5908_MI,
	&m24498_MI,
	&m24506_MI,
	&m24507_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6156_TI;
extern TypeInfo t6157_TI;
extern TypeInfo t6158_TI;
static TypeInfo* t1182_ITIs[] = 
{
	&t6156_TI,
	&t6157_TI,
	&t6158_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t1182_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6156_TI, 21},
	{ &t6157_TI, 28},
	{ &t6158_TI, 33},
	{ &t2182_TI, 34},
	{ &t2186_TI, 41},
	{ &t2183_TI, 46},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1182_0_0_0;
extern Il2CppType t1182_1_0_0;
struct t1176;
extern TypeInfo t1176_TI;
TypeInfo t1182_TI = 
{
	&g_mscorlib_dll_Image, NULL, "TailoringInfo[]", "Mono.Globalization.Unicode", t1182_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1176_TI, t1182_ITIs, t1182_VT, &EmptyCustomAttributesCache, &t1182_TI, &t1182_0_0_0, &t1182_1_0_0, t1182_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1176 *), -1, 0, 0, -1, 1048576, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1183_TI;



// Metadata Definition Mono.Globalization.Unicode.Contraction[]
static MethodInfo* t1183_MIs[] =
{
	NULL
};
extern MethodInfo m24510_MI;
extern MethodInfo m24511_MI;
extern MethodInfo m24512_MI;
extern MethodInfo m24513_MI;
extern MethodInfo m24514_MI;
extern MethodInfo m24515_MI;
extern MethodInfo m24509_MI;
extern MethodInfo m24517_MI;
extern MethodInfo m24518_MI;
static MethodInfo* t1183_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24510_MI,
	&m5907_MI,
	&m24511_MI,
	&m24512_MI,
	&m24513_MI,
	&m24514_MI,
	&m24515_MI,
	&m5908_MI,
	&m24509_MI,
	&m24517_MI,
	&m24518_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6159_TI;
extern TypeInfo t6160_TI;
extern TypeInfo t6161_TI;
static TypeInfo* t1183_ITIs[] = 
{
	&t6159_TI,
	&t6160_TI,
	&t6161_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t1183_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6159_TI, 21},
	{ &t6160_TI, 28},
	{ &t6161_TI, 33},
	{ &t2182_TI, 34},
	{ &t2186_TI, 41},
	{ &t2183_TI, 46},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1183_0_0_0;
extern Il2CppType t1183_1_0_0;
struct t1177;
extern TypeInfo t1177_TI;
TypeInfo t1183_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Contraction[]", "Mono.Globalization.Unicode", t1183_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1177_TI, t1183_ITIs, t1183_VT, &EmptyCustomAttributesCache, &t1183_TI, &t1183_0_0_0, &t1183_1_0_0, t1183_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1177 *), -1, 0, 0, -1, 1048576, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1184_TI;



// Metadata Definition Mono.Globalization.Unicode.Level2Map[]
static MethodInfo* t1184_MIs[] =
{
	NULL
};
extern MethodInfo m24521_MI;
extern MethodInfo m24522_MI;
extern MethodInfo m24523_MI;
extern MethodInfo m24524_MI;
extern MethodInfo m24525_MI;
extern MethodInfo m24526_MI;
extern MethodInfo m24520_MI;
extern MethodInfo m24528_MI;
extern MethodInfo m24529_MI;
static MethodInfo* t1184_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24521_MI,
	&m5907_MI,
	&m24522_MI,
	&m24523_MI,
	&m24524_MI,
	&m24525_MI,
	&m24526_MI,
	&m5908_MI,
	&m24520_MI,
	&m24528_MI,
	&m24529_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6162_TI;
extern TypeInfo t6163_TI;
extern TypeInfo t6164_TI;
static TypeInfo* t1184_ITIs[] = 
{
	&t6162_TI,
	&t6163_TI,
	&t6164_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t1184_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6162_TI, 21},
	{ &t6163_TI, 28},
	{ &t6164_TI, 33},
	{ &t2182_TI, 34},
	{ &t2186_TI, 41},
	{ &t2183_TI, 46},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1184_0_0_0;
extern Il2CppType t1184_1_0_0;
struct t1179;
extern TypeInfo t1179_TI;
TypeInfo t1184_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Level2Map[]", "Mono.Globalization.Unicode", t1184_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1179_TI, t1184_ITIs, t1184_VT, &EmptyCustomAttributesCache, &t1184_TI, &t1184_0_0_0, &t1184_1_0_0, t1184_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t1179 *), -1, 0, 0, -1, 1048576, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3594_TI;



// Metadata Definition Mono.Globalization.Unicode.SimpleCollator/ExtenderType[]
static MethodInfo* t3594_MIs[] =
{
	NULL
};
extern MethodInfo m24532_MI;
extern MethodInfo m24533_MI;
extern MethodInfo m24534_MI;
extern MethodInfo m24535_MI;
extern MethodInfo m24536_MI;
extern MethodInfo m24537_MI;
extern MethodInfo m24531_MI;
extern MethodInfo m24539_MI;
extern MethodInfo m24540_MI;
static MethodInfo* t3594_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24532_MI,
	&m5907_MI,
	&m24533_MI,
	&m24534_MI,
	&m24535_MI,
	&m24536_MI,
	&m24537_MI,
	&m5908_MI,
	&m24531_MI,
	&m24539_MI,
	&m24540_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6165_TI;
extern TypeInfo t6166_TI;
extern TypeInfo t6167_TI;
static TypeInfo* t3594_ITIs[] = 
{
	&t6165_TI,
	&t6166_TI,
	&t6167_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3594_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6165_TI, 21},
	{ &t6166_TI, 28},
	{ &t6167_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3594_0_0_0;
extern Il2CppType t3594_1_0_0;
#include "t1189.h"
extern TypeInfo t1189_TI;
TypeInfo t3594_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ExtenderType[]", "", t3594_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1189_TI, t3594_ITIs, t3594_VT, &EmptyCustomAttributesCache, &t44_TI, &t3594_0_0_0, &t3594_1_0_0, t3594_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 259, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3595_TI;



// Metadata Definition Mono.Math.Prime.ConfidenceFactor[]
static MethodInfo* t3595_MIs[] =
{
	NULL
};
extern MethodInfo m24543_MI;
extern MethodInfo m24544_MI;
extern MethodInfo m24545_MI;
extern MethodInfo m24546_MI;
extern MethodInfo m24547_MI;
extern MethodInfo m24548_MI;
extern MethodInfo m24542_MI;
extern MethodInfo m24550_MI;
extern MethodInfo m24551_MI;
static MethodInfo* t3595_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m24543_MI,
	&m5907_MI,
	&m24544_MI,
	&m24545_MI,
	&m24546_MI,
	&m24547_MI,
	&m24548_MI,
	&m5908_MI,
	&m24542_MI,
	&m24550_MI,
	&m24551_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t6168_TI;
extern TypeInfo t6169_TI;
extern TypeInfo t6170_TI;
static TypeInfo* t3595_ITIs[] = 
{
	&t6168_TI,
	&t6169_TI,
	&t6170_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3595_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t6168_TI, 21},
	{ &t6169_TI, 28},
	{ &t6170_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3595_0_0_0;
extern Il2CppType t3595_1_0_0;
#include "t1197.h"
extern TypeInfo t1197_TI;
TypeInfo t3595_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ConfidenceFactor[]", "Mono.Math.Prime", t3595_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t1197_TI, t3595_ITIs, t3595_VT, &EmptyCustomAttributesCache, &t44_TI, &t3595_0_0_0, &t3595_1_0_0, t3595_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 256, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
