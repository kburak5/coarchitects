﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2447;
struct t29;
struct t20;
#include "t2444.h"

 void m12780 (t2447 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12781 (t2447 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12782 (t2447 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12783 (t2447 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2444  m12784 (t2447 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
