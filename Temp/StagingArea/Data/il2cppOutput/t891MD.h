﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t891;
struct t878;
#include "t849.h"
#include "t866.h"

 void m3844 (t891 * __this, bool p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3845 (t891 * __this, uint16_t p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3846 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3847 (t891 * __this, uint16_t p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3848 (t891 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3849 (t891 * __this, uint16_t p0, uint16_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3850 (t891 * __this, t29 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3851 (t891 * __this, int32_t* p0, int32_t* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3852 (t891 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m3853 (t29 * __this, t866  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
