﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1653;
struct t7;
struct t733;
#include "t735.h"

 void m9303 (t1653 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9304 (t1653 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9305 (t1653 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9306 (t1653 * __this, t7* p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m9307 (t1653 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
