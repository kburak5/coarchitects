﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t779;
struct t7;

 void m3483 (t779 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3484 (t779 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3485 (t779 * __this, t7* p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3486 (t779 * __this, t779 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3487 (t779 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3488 (t779 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3489 (t779 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
