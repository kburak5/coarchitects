﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1562;
struct t29;
struct t7;

 bool m8468 (t1562 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8469 (t1562 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8470 (t1562 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
