﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3500;
struct t29;
struct t20;
#include "t1644.h"

 void m19442 (t3500 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m19443 (t3500 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m19444 (t3500 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19445 (t3500 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m19446 (t3500 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
