﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
#include "stringLiterals.h"

extern TypeInfo t47_TI;
extern TypeInfo t48_TI;
extern TypeInfo t31_TI;
extern TypeInfo t30_TI;
extern TypeInfo t32_TI;
extern TypeInfo t89_TI;
extern TypeInfo t90_TI;
extern TypeInfo t91_TI;
extern TypeInfo t93_TI;
extern TypeInfo t92_TI;
extern TypeInfo t94_TI;
extern TypeInfo t95_TI;
extern TypeInfo t96_TI;
extern TypeInfo t97_TI;
extern TypeInfo t98_TI;
extern TypeInfo t99_TI;
extern TypeInfo t100_TI;
extern TypeInfo t101_TI;
extern TypeInfo t102_TI;
extern TypeInfo t103_TI;
extern TypeInfo t50_TI;
extern TypeInfo t58_TI;
extern TypeInfo t60_TI;
extern TypeInfo t62_TI;
extern TypeInfo t61_TI;
extern TypeInfo t65_TI;
extern TypeInfo t68_TI;
extern TypeInfo t106_TI;
extern TypeInfo t107_TI;
extern TypeInfo t57_TI;
extern TypeInfo t55_TI;
extern TypeInfo t64_TI;
extern TypeInfo t53_TI;
extern TypeInfo t111_TI;
extern TypeInfo t112_TI;
extern TypeInfo t6_TI;
extern TypeInfo t52_TI;
extern TypeInfo t113_TI;
extern TypeInfo t115_TI;
extern TypeInfo t114_TI;
extern TypeInfo t117_TI;
extern TypeInfo t120_TI;
extern TypeInfo t121_TI;
extern TypeInfo t122_TI;
extern TypeInfo t109_TI;
extern TypeInfo t123_TI;
extern TypeInfo t124_TI;
extern TypeInfo t340_TI;
extern TypeInfo t128_TI;
extern TypeInfo t129_TI;
extern TypeInfo t131_TI;
extern TypeInfo t134_TI;
extern TypeInfo t135_TI;
extern TypeInfo t137_TI;
extern TypeInfo t33_TI;
extern TypeInfo t138_TI;
extern TypeInfo t9_TI;
extern TypeInfo t140_TI;
extern TypeInfo t145_TI;
extern TypeInfo t141_TI;
extern TypeInfo t146_TI;
extern TypeInfo t147_TI;
extern TypeInfo t153_TI;
extern TypeInfo t155_TI;
extern TypeInfo t165_TI;
extern TypeInfo t166_TI;
extern TypeInfo t169_TI;
extern TypeInfo t355_TI;
extern TypeInfo t172_TI;
extern TypeInfo t173_TI;
extern TypeInfo t174_TI;
extern TypeInfo t175_TI;
extern TypeInfo t176_TI;
extern TypeInfo t177_TI;
extern TypeInfo t178_TI;
extern TypeInfo t179_TI;
extern TypeInfo t370_TI;
extern TypeInfo t369_TI;
extern TypeInfo t185_TI;
extern TypeInfo t186_TI;
extern TypeInfo t187_TI;
extern TypeInfo t188_TI;
extern TypeInfo t189_TI;
extern TypeInfo t191_TI;
extern TypeInfo t192_TI;
extern TypeInfo t193_TI;
extern TypeInfo t196_TI;
extern TypeInfo t198_TI;
extern TypeInfo t197_TI;
extern TypeInfo t182_TI;
extern TypeInfo t208_TI;
extern TypeInfo t209_TI;
extern TypeInfo t210_TI;
extern TypeInfo t211_TI;
extern TypeInfo t212_TI;
extern TypeInfo t213_TI;
extern TypeInfo t215_TI;
extern TypeInfo t216_TI;
extern TypeInfo t217_TI;
extern TypeInfo t219_TI;
extern TypeInfo t220_TI;
extern TypeInfo t222_TI;
extern TypeInfo t225_TI;
extern TypeInfo t207_TI;
extern TypeInfo t139_TI;
extern TypeInfo t230_TI;
extern TypeInfo t231_TI;
extern TypeInfo t232_TI;
extern TypeInfo t233_TI;
extern TypeInfo t234_TI;
extern TypeInfo t228_TI;
extern TypeInfo t235_TI;
extern TypeInfo t236_TI;
extern TypeInfo t15_TI;
extern TypeInfo t239_TI;
extern TypeInfo t240_TI;
extern TypeInfo t39_TI;
extern TypeInfo t242_TI;
extern TypeInfo t247_TI;
extern TypeInfo t248_TI;
extern TypeInfo t249_TI;
extern TypeInfo t250_TI;
extern TypeInfo t251_TI;
extern TypeInfo t252_TI;
extern TypeInfo t253_TI;
extern TypeInfo t254_TI;
extern TypeInfo t255_TI;
extern TypeInfo t256_TI;
extern TypeInfo t257_TI;
extern TypeInfo t258_TI;
extern TypeInfo t260_TI;
extern TypeInfo t261_TI;
extern TypeInfo t271_TI;
extern TypeInfo t409_TI;
extern TypeInfo t411_TI;
extern TypeInfo t410_TI;
extern TypeInfo t412_TI;
extern TypeInfo t262_TI;
extern TypeInfo t259_TI;
extern TypeInfo t265_TI;
extern TypeInfo t269_TI;
extern TypeInfo t272_TI;
extern TypeInfo t354_TI;
extern TypeInfo t273_TI;
extern TypeInfo t274_TI;
extern TypeInfo t275_TI;
extern TypeInfo t279_TI;
extern TypeInfo t282_TI;
extern TypeInfo t283_TI;
extern TypeInfo t356_TI;
extern TypeInfo t284_TI;
extern TypeInfo t286_TI;
extern TypeInfo t285_TI;
#include "utils/RegisterRuntimeInitializeAndCleanup.h"
#include <map>
struct TypeInfo;
struct MethodInfo;
TypeInfo* g_UnityEngine_UI_Assembly_Types[154] = 
{
	&t47_TI,
	&t48_TI,
	&t31_TI,
	&t30_TI,
	&t32_TI,
	&t89_TI,
	&t90_TI,
	&t91_TI,
	&t93_TI,
	&t92_TI,
	&t94_TI,
	&t95_TI,
	&t96_TI,
	&t97_TI,
	&t98_TI,
	&t99_TI,
	&t100_TI,
	&t101_TI,
	&t102_TI,
	&t103_TI,
	&t50_TI,
	&t58_TI,
	&t60_TI,
	&t62_TI,
	&t61_TI,
	&t65_TI,
	&t68_TI,
	&t106_TI,
	&t107_TI,
	&t57_TI,
	&t55_TI,
	&t64_TI,
	&t53_TI,
	&t111_TI,
	&t112_TI,
	&t6_TI,
	&t52_TI,
	&t113_TI,
	&t115_TI,
	&t114_TI,
	&t117_TI,
	&t120_TI,
	&t121_TI,
	&t122_TI,
	&t109_TI,
	&t123_TI,
	&t124_TI,
	&t340_TI,
	&t128_TI,
	&t129_TI,
	&t131_TI,
	&t134_TI,
	&t135_TI,
	&t137_TI,
	&t33_TI,
	&t138_TI,
	&t9_TI,
	&t140_TI,
	&t145_TI,
	&t141_TI,
	&t146_TI,
	&t147_TI,
	&t153_TI,
	&t155_TI,
	&t165_TI,
	&t166_TI,
	&t169_TI,
	&t355_TI,
	&t172_TI,
	&t173_TI,
	&t174_TI,
	&t175_TI,
	&t176_TI,
	&t177_TI,
	&t178_TI,
	&t179_TI,
	&t370_TI,
	&t369_TI,
	&t185_TI,
	&t186_TI,
	&t187_TI,
	&t188_TI,
	&t189_TI,
	&t191_TI,
	&t192_TI,
	&t193_TI,
	&t196_TI,
	&t198_TI,
	&t197_TI,
	&t182_TI,
	&t208_TI,
	&t209_TI,
	&t210_TI,
	&t211_TI,
	&t212_TI,
	&t213_TI,
	&t215_TI,
	&t216_TI,
	&t217_TI,
	&t219_TI,
	&t220_TI,
	&t222_TI,
	&t225_TI,
	&t207_TI,
	&t139_TI,
	&t230_TI,
	&t231_TI,
	&t232_TI,
	&t233_TI,
	&t234_TI,
	&t228_TI,
	&t235_TI,
	&t236_TI,
	&t15_TI,
	&t239_TI,
	&t240_TI,
	&t39_TI,
	&t242_TI,
	&t247_TI,
	&t248_TI,
	&t249_TI,
	&t250_TI,
	&t251_TI,
	&t252_TI,
	&t253_TI,
	&t254_TI,
	&t255_TI,
	&t256_TI,
	&t257_TI,
	&t258_TI,
	&t260_TI,
	&t261_TI,
	&t271_TI,
	&t409_TI,
	&t411_TI,
	&t410_TI,
	&t412_TI,
	&t262_TI,
	&t259_TI,
	&t265_TI,
	&t269_TI,
	&t272_TI,
	&t354_TI,
	&t273_TI,
	&t274_TI,
	&t275_TI,
	&t279_TI,
	&t282_TI,
	&t283_TI,
	&t356_TI,
	&t284_TI,
	&t286_TI,
	&t285_TI,
	NULL,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern CustomAttributesCache g_UnityEngine_UI_Assembly__CustomAttributeCache;
Il2CppAssembly g_UnityEngine_UI_Assembly = 
{
	{ "UnityEngine.UI", 0, 0, 0, { 0 }, 32772, 0, 0, 1, 0, 0, 0 },
	&g_UnityEngine_UI_dll_Image,
	&g_UnityEngine_UI_Assembly__CustomAttributeCache,
};
Il2CppImage g_UnityEngine_UI_dll_Image = 
{
	 "UnityEngine.UI.dll" ,
	&g_UnityEngine_UI_Assembly,
	g_UnityEngine_UI_Assembly_Types,
	153,
	NULL,
};
static void s_UnityEngine_UIRegistration()
{
	RegisterAssembly (&g_UnityEngine_UI_Assembly);
}
static il2cpp::utils::RegisterRuntimeInitializeAndCleanup s_UnityEngine_UIRegistrationVariable(&s_UnityEngine_UIRegistration, NULL);
