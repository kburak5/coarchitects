﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1691;
struct t1691_marshaled;

void t1691_marshal(const t1691& unmarshaled, t1691_marshaled& marshaled);
void t1691_marshal_back(const t1691_marshaled& marshaled, t1691& unmarshaled);
void t1691_marshal_cleanup(t1691_marshaled& marshaled);
