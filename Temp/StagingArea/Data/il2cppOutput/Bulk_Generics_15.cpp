﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t4329_TI;

#include "t445.h"

#include "t20.h"

// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.SendMessageOptions>
extern MethodInfo m28966_MI;
static PropertyInfo t4329____Current_PropertyInfo = 
{
	&t4329_TI, "Current", &m28966_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4329_PIs[] =
{
	&t4329____Current_PropertyInfo,
	NULL
};
extern Il2CppType t445_0_0_0;
extern void* RuntimeInvoker_t445 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28966_GM;
MethodInfo m28966_MI = 
{
	"get_Current", NULL, &t4329_TI, &t445_0_0_0, RuntimeInvoker_t445, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28966_GM};
static MethodInfo* t4329_MIs[] =
{
	&m28966_MI,
	NULL
};
extern TypeInfo t136_TI;
extern TypeInfo t324_TI;
static TypeInfo* t4329_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4329_0_0_0;
extern Il2CppType t4329_1_0_0;
struct t4329;
extern Il2CppGenericClass t4329_GC;
TypeInfo t4329_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4329_MIs, t4329_PIs, NULL, NULL, NULL, NULL, NULL, &t4329_TI, t4329_ITIs, NULL, &EmptyCustomAttributesCache, &t4329_TI, &t4329_0_0_0, &t4329_1_0_0, NULL, &t4329_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2819.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2819_TI;
#include "t2819MD.h"

#include "t29.h"
#include "t44.h"
#include "t7.h"
#include "t914.h"
#include "t21.h"
#include "t40.h"
extern TypeInfo t445_TI;
extern TypeInfo t914_TI;
#include "t914MD.h"
#include "t20MD.h"
extern MethodInfo m15363_MI;
extern MethodInfo m3964_MI;
extern MethodInfo m3969_MI;
extern MethodInfo m21957_MI;
struct t20;
#include "t915.h"
 int32_t m21957 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m15359_MI;
 void m15359 (t2819 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m15360_MI;
 t29 * m15360 (t2819 * __this, MethodInfo* method){
	{
		int32_t L_0 = m15363(__this, &m15363_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t445_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m15361_MI;
 void m15361 (t2819 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m15362_MI;
 bool m15362 (t2819 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m15363 (t2819 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m21957(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m21957_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.SendMessageOptions>
extern Il2CppType t20_0_0_1;
FieldInfo t2819_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2819_TI, offsetof(t2819, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2819_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2819_TI, offsetof(t2819, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2819_FIs[] =
{
	&t2819_f0_FieldInfo,
	&t2819_f1_FieldInfo,
	NULL
};
static PropertyInfo t2819____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2819_TI, "System.Collections.IEnumerator.Current", &m15360_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2819____Current_PropertyInfo = 
{
	&t2819_TI, "Current", &m15363_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2819_PIs[] =
{
	&t2819____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2819____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
static ParameterInfo t2819_m15359_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15359_GM;
MethodInfo m15359_MI = 
{
	".ctor", (methodPointerType)&m15359, &t2819_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2819_m15359_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15359_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15360_GM;
MethodInfo m15360_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m15360, &t2819_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15360_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15361_GM;
MethodInfo m15361_MI = 
{
	"Dispose", (methodPointerType)&m15361, &t2819_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15361_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15362_GM;
MethodInfo m15362_MI = 
{
	"MoveNext", (methodPointerType)&m15362, &t2819_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15362_GM};
extern Il2CppType t445_0_0_0;
extern void* RuntimeInvoker_t445 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15363_GM;
MethodInfo m15363_MI = 
{
	"get_Current", (methodPointerType)&m15363, &t2819_TI, &t445_0_0_0, RuntimeInvoker_t445, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15363_GM};
static MethodInfo* t2819_MIs[] =
{
	&m15359_MI,
	&m15360_MI,
	&m15361_MI,
	&m15362_MI,
	&m15363_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
static MethodInfo* t2819_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15360_MI,
	&m15362_MI,
	&m15361_MI,
	&m15363_MI,
};
static TypeInfo* t2819_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4329_TI,
};
static Il2CppInterfaceOffsetPair t2819_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4329_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2819_0_0_0;
extern Il2CppType t2819_1_0_0;
extern TypeInfo t110_TI;
extern Il2CppGenericClass t2819_GC;
extern TypeInfo t20_TI;
TypeInfo t2819_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2819_MIs, t2819_PIs, t2819_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2819_TI, t2819_ITIs, t2819_VT, &EmptyCustomAttributesCache, &t2819_TI, &t2819_0_0_0, &t2819_1_0_0, t2819_IOs, &t2819_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2819)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5535_TI;

#include "UnityEngine_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.SendMessageOptions>
extern MethodInfo m28967_MI;
static PropertyInfo t5535____Count_PropertyInfo = 
{
	&t5535_TI, "Count", &m28967_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28968_MI;
static PropertyInfo t5535____IsReadOnly_PropertyInfo = 
{
	&t5535_TI, "IsReadOnly", &m28968_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5535_PIs[] =
{
	&t5535____Count_PropertyInfo,
	&t5535____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28967_GM;
MethodInfo m28967_MI = 
{
	"get_Count", NULL, &t5535_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28967_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28968_GM;
MethodInfo m28968_MI = 
{
	"get_IsReadOnly", NULL, &t5535_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28968_GM};
extern Il2CppType t445_0_0_0;
extern Il2CppType t445_0_0_0;
static ParameterInfo t5535_m28969_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t445_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28969_GM;
MethodInfo m28969_MI = 
{
	"Add", NULL, &t5535_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5535_m28969_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28969_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28970_GM;
MethodInfo m28970_MI = 
{
	"Clear", NULL, &t5535_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28970_GM};
extern Il2CppType t445_0_0_0;
static ParameterInfo t5535_m28971_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t445_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28971_GM;
MethodInfo m28971_MI = 
{
	"Contains", NULL, &t5535_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5535_m28971_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28971_GM};
extern Il2CppType t3733_0_0_0;
extern Il2CppType t3733_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5535_m28972_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3733_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28972_GM;
MethodInfo m28972_MI = 
{
	"CopyTo", NULL, &t5535_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5535_m28972_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28972_GM};
extern Il2CppType t445_0_0_0;
static ParameterInfo t5535_m28973_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t445_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28973_GM;
MethodInfo m28973_MI = 
{
	"Remove", NULL, &t5535_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5535_m28973_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28973_GM};
static MethodInfo* t5535_MIs[] =
{
	&m28967_MI,
	&m28968_MI,
	&m28969_MI,
	&m28970_MI,
	&m28971_MI,
	&m28972_MI,
	&m28973_MI,
	NULL
};
extern TypeInfo t603_TI;
extern TypeInfo t5537_TI;
static TypeInfo* t5535_ITIs[] = 
{
	&t603_TI,
	&t5537_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5535_0_0_0;
extern Il2CppType t5535_1_0_0;
struct t5535;
extern Il2CppGenericClass t5535_GC;
TypeInfo t5535_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5535_MIs, t5535_PIs, NULL, NULL, NULL, NULL, NULL, &t5535_TI, t5535_ITIs, NULL, &EmptyCustomAttributesCache, &t5535_TI, &t5535_0_0_0, &t5535_1_0_0, NULL, &t5535_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.SendMessageOptions>
extern Il2CppType t4329_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28974_GM;
MethodInfo m28974_MI = 
{
	"GetEnumerator", NULL, &t5537_TI, &t4329_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28974_GM};
static MethodInfo* t5537_MIs[] =
{
	&m28974_MI,
	NULL
};
static TypeInfo* t5537_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5537_0_0_0;
extern Il2CppType t5537_1_0_0;
struct t5537;
extern Il2CppGenericClass t5537_GC;
TypeInfo t5537_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5537_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5537_TI, t5537_ITIs, NULL, &EmptyCustomAttributesCache, &t5537_TI, &t5537_0_0_0, &t5537_1_0_0, NULL, &t5537_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5536_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.SendMessageOptions>
extern MethodInfo m28975_MI;
extern MethodInfo m28976_MI;
static PropertyInfo t5536____Item_PropertyInfo = 
{
	&t5536_TI, "Item", &m28975_MI, &m28976_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5536_PIs[] =
{
	&t5536____Item_PropertyInfo,
	NULL
};
extern Il2CppType t445_0_0_0;
static ParameterInfo t5536_m28977_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t445_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28977_GM;
MethodInfo m28977_MI = 
{
	"IndexOf", NULL, &t5536_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5536_m28977_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28977_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t445_0_0_0;
static ParameterInfo t5536_m28978_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t445_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28978_GM;
MethodInfo m28978_MI = 
{
	"Insert", NULL, &t5536_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5536_m28978_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28978_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5536_m28979_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28979_GM;
MethodInfo m28979_MI = 
{
	"RemoveAt", NULL, &t5536_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5536_m28979_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28979_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5536_m28975_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t445_0_0_0;
extern void* RuntimeInvoker_t445_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28975_GM;
MethodInfo m28975_MI = 
{
	"get_Item", NULL, &t5536_TI, &t445_0_0_0, RuntimeInvoker_t445_t44, t5536_m28975_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28975_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t445_0_0_0;
static ParameterInfo t5536_m28976_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t445_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28976_GM;
MethodInfo m28976_MI = 
{
	"set_Item", NULL, &t5536_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5536_m28976_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28976_GM};
static MethodInfo* t5536_MIs[] =
{
	&m28977_MI,
	&m28978_MI,
	&m28979_MI,
	&m28975_MI,
	&m28976_MI,
	NULL
};
static TypeInfo* t5536_ITIs[] = 
{
	&t603_TI,
	&t5535_TI,
	&t5537_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5536_0_0_0;
extern Il2CppType t5536_1_0_0;
struct t5536;
extern Il2CppGenericClass t5536_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5536_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5536_MIs, t5536_PIs, NULL, NULL, NULL, NULL, NULL, &t5536_TI, t5536_ITIs, NULL, &t1908__CustomAttributeCache, &t5536_TI, &t5536_0_0_0, &t5536_1_0_0, NULL, &t5536_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4331_TI;

#include "t376.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.RuntimePlatform>
extern MethodInfo m28980_MI;
static PropertyInfo t4331____Current_PropertyInfo = 
{
	&t4331_TI, "Current", &m28980_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4331_PIs[] =
{
	&t4331____Current_PropertyInfo,
	NULL
};
extern Il2CppType t376_0_0_0;
extern void* RuntimeInvoker_t376 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28980_GM;
MethodInfo m28980_MI = 
{
	"get_Current", NULL, &t4331_TI, &t376_0_0_0, RuntimeInvoker_t376, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28980_GM};
static MethodInfo* t4331_MIs[] =
{
	&m28980_MI,
	NULL
};
static TypeInfo* t4331_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4331_0_0_0;
extern Il2CppType t4331_1_0_0;
struct t4331;
extern Il2CppGenericClass t4331_GC;
TypeInfo t4331_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4331_MIs, t4331_PIs, NULL, NULL, NULL, NULL, NULL, &t4331_TI, t4331_ITIs, NULL, &EmptyCustomAttributesCache, &t4331_TI, &t4331_0_0_0, &t4331_1_0_0, NULL, &t4331_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2820.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2820_TI;
#include "t2820MD.h"

extern TypeInfo t376_TI;
extern MethodInfo m15368_MI;
extern MethodInfo m21968_MI;
struct t20;
 int32_t m21968 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m15364_MI;
 void m15364 (t2820 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m15365_MI;
 t29 * m15365 (t2820 * __this, MethodInfo* method){
	{
		int32_t L_0 = m15368(__this, &m15368_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t376_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m15366_MI;
 void m15366 (t2820 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m15367_MI;
 bool m15367 (t2820 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m15368 (t2820 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m21968(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m21968_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.RuntimePlatform>
extern Il2CppType t20_0_0_1;
FieldInfo t2820_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2820_TI, offsetof(t2820, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2820_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2820_TI, offsetof(t2820, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2820_FIs[] =
{
	&t2820_f0_FieldInfo,
	&t2820_f1_FieldInfo,
	NULL
};
static PropertyInfo t2820____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2820_TI, "System.Collections.IEnumerator.Current", &m15365_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2820____Current_PropertyInfo = 
{
	&t2820_TI, "Current", &m15368_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2820_PIs[] =
{
	&t2820____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2820____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2820_m15364_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15364_GM;
MethodInfo m15364_MI = 
{
	".ctor", (methodPointerType)&m15364, &t2820_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2820_m15364_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15364_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15365_GM;
MethodInfo m15365_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m15365, &t2820_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15365_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15366_GM;
MethodInfo m15366_MI = 
{
	"Dispose", (methodPointerType)&m15366, &t2820_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15366_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15367_GM;
MethodInfo m15367_MI = 
{
	"MoveNext", (methodPointerType)&m15367, &t2820_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15367_GM};
extern Il2CppType t376_0_0_0;
extern void* RuntimeInvoker_t376 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15368_GM;
MethodInfo m15368_MI = 
{
	"get_Current", (methodPointerType)&m15368, &t2820_TI, &t376_0_0_0, RuntimeInvoker_t376, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15368_GM};
static MethodInfo* t2820_MIs[] =
{
	&m15364_MI,
	&m15365_MI,
	&m15366_MI,
	&m15367_MI,
	&m15368_MI,
	NULL
};
static MethodInfo* t2820_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15365_MI,
	&m15367_MI,
	&m15366_MI,
	&m15368_MI,
};
static TypeInfo* t2820_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4331_TI,
};
static Il2CppInterfaceOffsetPair t2820_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4331_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2820_0_0_0;
extern Il2CppType t2820_1_0_0;
extern Il2CppGenericClass t2820_GC;
TypeInfo t2820_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2820_MIs, t2820_PIs, t2820_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2820_TI, t2820_ITIs, t2820_VT, &EmptyCustomAttributesCache, &t2820_TI, &t2820_0_0_0, &t2820_1_0_0, t2820_IOs, &t2820_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2820)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5538_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.RuntimePlatform>
extern MethodInfo m28981_MI;
static PropertyInfo t5538____Count_PropertyInfo = 
{
	&t5538_TI, "Count", &m28981_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28982_MI;
static PropertyInfo t5538____IsReadOnly_PropertyInfo = 
{
	&t5538_TI, "IsReadOnly", &m28982_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5538_PIs[] =
{
	&t5538____Count_PropertyInfo,
	&t5538____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28981_GM;
MethodInfo m28981_MI = 
{
	"get_Count", NULL, &t5538_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28981_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28982_GM;
MethodInfo m28982_MI = 
{
	"get_IsReadOnly", NULL, &t5538_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28982_GM};
extern Il2CppType t376_0_0_0;
extern Il2CppType t376_0_0_0;
static ParameterInfo t5538_m28983_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t376_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28983_GM;
MethodInfo m28983_MI = 
{
	"Add", NULL, &t5538_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5538_m28983_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28983_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28984_GM;
MethodInfo m28984_MI = 
{
	"Clear", NULL, &t5538_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28984_GM};
extern Il2CppType t376_0_0_0;
static ParameterInfo t5538_m28985_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t376_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28985_GM;
MethodInfo m28985_MI = 
{
	"Contains", NULL, &t5538_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5538_m28985_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28985_GM};
extern Il2CppType t3734_0_0_0;
extern Il2CppType t3734_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5538_m28986_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3734_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28986_GM;
MethodInfo m28986_MI = 
{
	"CopyTo", NULL, &t5538_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5538_m28986_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28986_GM};
extern Il2CppType t376_0_0_0;
static ParameterInfo t5538_m28987_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t376_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28987_GM;
MethodInfo m28987_MI = 
{
	"Remove", NULL, &t5538_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5538_m28987_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28987_GM};
static MethodInfo* t5538_MIs[] =
{
	&m28981_MI,
	&m28982_MI,
	&m28983_MI,
	&m28984_MI,
	&m28985_MI,
	&m28986_MI,
	&m28987_MI,
	NULL
};
extern TypeInfo t5540_TI;
static TypeInfo* t5538_ITIs[] = 
{
	&t603_TI,
	&t5540_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5538_0_0_0;
extern Il2CppType t5538_1_0_0;
struct t5538;
extern Il2CppGenericClass t5538_GC;
TypeInfo t5538_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5538_MIs, t5538_PIs, NULL, NULL, NULL, NULL, NULL, &t5538_TI, t5538_ITIs, NULL, &EmptyCustomAttributesCache, &t5538_TI, &t5538_0_0_0, &t5538_1_0_0, NULL, &t5538_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.RuntimePlatform>
extern Il2CppType t4331_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28988_GM;
MethodInfo m28988_MI = 
{
	"GetEnumerator", NULL, &t5540_TI, &t4331_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28988_GM};
static MethodInfo* t5540_MIs[] =
{
	&m28988_MI,
	NULL
};
static TypeInfo* t5540_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5540_0_0_0;
extern Il2CppType t5540_1_0_0;
struct t5540;
extern Il2CppGenericClass t5540_GC;
TypeInfo t5540_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5540_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5540_TI, t5540_ITIs, NULL, &EmptyCustomAttributesCache, &t5540_TI, &t5540_0_0_0, &t5540_1_0_0, NULL, &t5540_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5539_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.RuntimePlatform>
extern MethodInfo m28989_MI;
extern MethodInfo m28990_MI;
static PropertyInfo t5539____Item_PropertyInfo = 
{
	&t5539_TI, "Item", &m28989_MI, &m28990_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5539_PIs[] =
{
	&t5539____Item_PropertyInfo,
	NULL
};
extern Il2CppType t376_0_0_0;
static ParameterInfo t5539_m28991_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t376_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28991_GM;
MethodInfo m28991_MI = 
{
	"IndexOf", NULL, &t5539_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5539_m28991_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28991_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t376_0_0_0;
static ParameterInfo t5539_m28992_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t376_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28992_GM;
MethodInfo m28992_MI = 
{
	"Insert", NULL, &t5539_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5539_m28992_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28992_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5539_m28993_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28993_GM;
MethodInfo m28993_MI = 
{
	"RemoveAt", NULL, &t5539_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5539_m28993_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28993_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5539_m28989_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t376_0_0_0;
extern void* RuntimeInvoker_t376_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28989_GM;
MethodInfo m28989_MI = 
{
	"get_Item", NULL, &t5539_TI, &t376_0_0_0, RuntimeInvoker_t376_t44, t5539_m28989_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28989_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t376_0_0_0;
static ParameterInfo t5539_m28990_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t376_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28990_GM;
MethodInfo m28990_MI = 
{
	"set_Item", NULL, &t5539_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5539_m28990_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28990_GM};
static MethodInfo* t5539_MIs[] =
{
	&m28991_MI,
	&m28992_MI,
	&m28993_MI,
	&m28989_MI,
	&m28990_MI,
	NULL
};
static TypeInfo* t5539_ITIs[] = 
{
	&t603_TI,
	&t5538_TI,
	&t5540_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5539_0_0_0;
extern Il2CppType t5539_1_0_0;
struct t5539;
extern Il2CppGenericClass t5539_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5539_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5539_MIs, t5539_PIs, NULL, NULL, NULL, NULL, NULL, &t5539_TI, t5539_ITIs, NULL, &t1908__CustomAttributeCache, &t5539_TI, &t5539_0_0_0, &t5539_1_0_0, NULL, &t5539_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4333_TI;

#include "t447.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.LogType>
extern MethodInfo m28994_MI;
static PropertyInfo t4333____Current_PropertyInfo = 
{
	&t4333_TI, "Current", &m28994_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4333_PIs[] =
{
	&t4333____Current_PropertyInfo,
	NULL
};
extern Il2CppType t447_0_0_0;
extern void* RuntimeInvoker_t447 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28994_GM;
MethodInfo m28994_MI = 
{
	"get_Current", NULL, &t4333_TI, &t447_0_0_0, RuntimeInvoker_t447, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28994_GM};
static MethodInfo* t4333_MIs[] =
{
	&m28994_MI,
	NULL
};
static TypeInfo* t4333_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4333_0_0_0;
extern Il2CppType t4333_1_0_0;
struct t4333;
extern Il2CppGenericClass t4333_GC;
TypeInfo t4333_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4333_MIs, t4333_PIs, NULL, NULL, NULL, NULL, NULL, &t4333_TI, t4333_ITIs, NULL, &EmptyCustomAttributesCache, &t4333_TI, &t4333_0_0_0, &t4333_1_0_0, NULL, &t4333_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2821.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2821_TI;
#include "t2821MD.h"

extern TypeInfo t447_TI;
extern MethodInfo m15373_MI;
extern MethodInfo m21979_MI;
struct t20;
 int32_t m21979 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m15369_MI;
 void m15369 (t2821 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m15370_MI;
 t29 * m15370 (t2821 * __this, MethodInfo* method){
	{
		int32_t L_0 = m15373(__this, &m15373_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t447_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m15371_MI;
 void m15371 (t2821 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m15372_MI;
 bool m15372 (t2821 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m15373 (t2821 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m21979(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m21979_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.LogType>
extern Il2CppType t20_0_0_1;
FieldInfo t2821_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2821_TI, offsetof(t2821, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2821_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2821_TI, offsetof(t2821, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2821_FIs[] =
{
	&t2821_f0_FieldInfo,
	&t2821_f1_FieldInfo,
	NULL
};
static PropertyInfo t2821____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2821_TI, "System.Collections.IEnumerator.Current", &m15370_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2821____Current_PropertyInfo = 
{
	&t2821_TI, "Current", &m15373_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2821_PIs[] =
{
	&t2821____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2821____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2821_m15369_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15369_GM;
MethodInfo m15369_MI = 
{
	".ctor", (methodPointerType)&m15369, &t2821_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2821_m15369_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15369_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15370_GM;
MethodInfo m15370_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m15370, &t2821_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15370_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15371_GM;
MethodInfo m15371_MI = 
{
	"Dispose", (methodPointerType)&m15371, &t2821_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15371_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15372_GM;
MethodInfo m15372_MI = 
{
	"MoveNext", (methodPointerType)&m15372, &t2821_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15372_GM};
extern Il2CppType t447_0_0_0;
extern void* RuntimeInvoker_t447 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15373_GM;
MethodInfo m15373_MI = 
{
	"get_Current", (methodPointerType)&m15373, &t2821_TI, &t447_0_0_0, RuntimeInvoker_t447, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15373_GM};
static MethodInfo* t2821_MIs[] =
{
	&m15369_MI,
	&m15370_MI,
	&m15371_MI,
	&m15372_MI,
	&m15373_MI,
	NULL
};
static MethodInfo* t2821_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15370_MI,
	&m15372_MI,
	&m15371_MI,
	&m15373_MI,
};
static TypeInfo* t2821_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4333_TI,
};
static Il2CppInterfaceOffsetPair t2821_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4333_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2821_0_0_0;
extern Il2CppType t2821_1_0_0;
extern Il2CppGenericClass t2821_GC;
TypeInfo t2821_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2821_MIs, t2821_PIs, t2821_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2821_TI, t2821_ITIs, t2821_VT, &EmptyCustomAttributesCache, &t2821_TI, &t2821_0_0_0, &t2821_1_0_0, t2821_IOs, &t2821_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2821)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5541_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.LogType>
extern MethodInfo m28995_MI;
static PropertyInfo t5541____Count_PropertyInfo = 
{
	&t5541_TI, "Count", &m28995_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28996_MI;
static PropertyInfo t5541____IsReadOnly_PropertyInfo = 
{
	&t5541_TI, "IsReadOnly", &m28996_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5541_PIs[] =
{
	&t5541____Count_PropertyInfo,
	&t5541____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28995_GM;
MethodInfo m28995_MI = 
{
	"get_Count", NULL, &t5541_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28995_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28996_GM;
MethodInfo m28996_MI = 
{
	"get_IsReadOnly", NULL, &t5541_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28996_GM};
extern Il2CppType t447_0_0_0;
extern Il2CppType t447_0_0_0;
static ParameterInfo t5541_m28997_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t447_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28997_GM;
MethodInfo m28997_MI = 
{
	"Add", NULL, &t5541_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5541_m28997_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28997_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28998_GM;
MethodInfo m28998_MI = 
{
	"Clear", NULL, &t5541_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28998_GM};
extern Il2CppType t447_0_0_0;
static ParameterInfo t5541_m28999_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t447_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28999_GM;
MethodInfo m28999_MI = 
{
	"Contains", NULL, &t5541_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5541_m28999_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28999_GM};
extern Il2CppType t3735_0_0_0;
extern Il2CppType t3735_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5541_m29000_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3735_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29000_GM;
MethodInfo m29000_MI = 
{
	"CopyTo", NULL, &t5541_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5541_m29000_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29000_GM};
extern Il2CppType t447_0_0_0;
static ParameterInfo t5541_m29001_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t447_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29001_GM;
MethodInfo m29001_MI = 
{
	"Remove", NULL, &t5541_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5541_m29001_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29001_GM};
static MethodInfo* t5541_MIs[] =
{
	&m28995_MI,
	&m28996_MI,
	&m28997_MI,
	&m28998_MI,
	&m28999_MI,
	&m29000_MI,
	&m29001_MI,
	NULL
};
extern TypeInfo t5543_TI;
static TypeInfo* t5541_ITIs[] = 
{
	&t603_TI,
	&t5543_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5541_0_0_0;
extern Il2CppType t5541_1_0_0;
struct t5541;
extern Il2CppGenericClass t5541_GC;
TypeInfo t5541_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5541_MIs, t5541_PIs, NULL, NULL, NULL, NULL, NULL, &t5541_TI, t5541_ITIs, NULL, &EmptyCustomAttributesCache, &t5541_TI, &t5541_0_0_0, &t5541_1_0_0, NULL, &t5541_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.LogType>
extern Il2CppType t4333_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29002_GM;
MethodInfo m29002_MI = 
{
	"GetEnumerator", NULL, &t5543_TI, &t4333_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29002_GM};
static MethodInfo* t5543_MIs[] =
{
	&m29002_MI,
	NULL
};
static TypeInfo* t5543_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5543_0_0_0;
extern Il2CppType t5543_1_0_0;
struct t5543;
extern Il2CppGenericClass t5543_GC;
TypeInfo t5543_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5543_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5543_TI, t5543_ITIs, NULL, &EmptyCustomAttributesCache, &t5543_TI, &t5543_0_0_0, &t5543_1_0_0, NULL, &t5543_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5542_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.LogType>
extern MethodInfo m29003_MI;
extern MethodInfo m29004_MI;
static PropertyInfo t5542____Item_PropertyInfo = 
{
	&t5542_TI, "Item", &m29003_MI, &m29004_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5542_PIs[] =
{
	&t5542____Item_PropertyInfo,
	NULL
};
extern Il2CppType t447_0_0_0;
static ParameterInfo t5542_m29005_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t447_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29005_GM;
MethodInfo m29005_MI = 
{
	"IndexOf", NULL, &t5542_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5542_m29005_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29005_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t447_0_0_0;
static ParameterInfo t5542_m29006_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t447_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29006_GM;
MethodInfo m29006_MI = 
{
	"Insert", NULL, &t5542_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5542_m29006_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29006_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5542_m29007_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29007_GM;
MethodInfo m29007_MI = 
{
	"RemoveAt", NULL, &t5542_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5542_m29007_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29007_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5542_m29003_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t447_0_0_0;
extern void* RuntimeInvoker_t447_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29003_GM;
MethodInfo m29003_MI = 
{
	"get_Item", NULL, &t5542_TI, &t447_0_0_0, RuntimeInvoker_t447_t44, t5542_m29003_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29003_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t447_0_0_0;
static ParameterInfo t5542_m29004_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t447_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29004_GM;
MethodInfo m29004_MI = 
{
	"set_Item", NULL, &t5542_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5542_m29004_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29004_GM};
static MethodInfo* t5542_MIs[] =
{
	&m29005_MI,
	&m29006_MI,
	&m29007_MI,
	&m29003_MI,
	&m29004_MI,
	NULL
};
static TypeInfo* t5542_ITIs[] = 
{
	&t603_TI,
	&t5541_TI,
	&t5543_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5542_0_0_0;
extern Il2CppType t5542_1_0_0;
struct t5542;
extern Il2CppGenericClass t5542_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5542_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5542_MIs, t5542_PIs, NULL, NULL, NULL, NULL, NULL, &t5542_TI, t5542_ITIs, NULL, &t1908__CustomAttributeCache, &t5542_TI, &t5542_0_0_0, &t5542_1_0_0, NULL, &t5542_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4335_TI;

#include "t450.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.ScriptableObject>
extern MethodInfo m29008_MI;
static PropertyInfo t4335____Current_PropertyInfo = 
{
	&t4335_TI, "Current", &m29008_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4335_PIs[] =
{
	&t4335____Current_PropertyInfo,
	NULL
};
extern Il2CppType t450_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29008_GM;
MethodInfo m29008_MI = 
{
	"get_Current", NULL, &t4335_TI, &t450_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29008_GM};
static MethodInfo* t4335_MIs[] =
{
	&m29008_MI,
	NULL
};
static TypeInfo* t4335_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4335_0_0_0;
extern Il2CppType t4335_1_0_0;
struct t4335;
extern Il2CppGenericClass t4335_GC;
TypeInfo t4335_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4335_MIs, t4335_PIs, NULL, NULL, NULL, NULL, NULL, &t4335_TI, t4335_ITIs, NULL, &EmptyCustomAttributesCache, &t4335_TI, &t4335_0_0_0, &t4335_1_0_0, NULL, &t4335_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2822.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2822_TI;
#include "t2822MD.h"

extern TypeInfo t450_TI;
extern MethodInfo m15378_MI;
extern MethodInfo m21990_MI;
struct t20;
struct t20;
 t29 * m19490_gshared (t20 * __this, int32_t p0, MethodInfo* method);
#define m19490(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
#define m21990(__this, p0, method) (t450 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.ScriptableObject>
extern Il2CppType t20_0_0_1;
FieldInfo t2822_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2822_TI, offsetof(t2822, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2822_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2822_TI, offsetof(t2822, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2822_FIs[] =
{
	&t2822_f0_FieldInfo,
	&t2822_f1_FieldInfo,
	NULL
};
extern MethodInfo m15375_MI;
static PropertyInfo t2822____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2822_TI, "System.Collections.IEnumerator.Current", &m15375_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2822____Current_PropertyInfo = 
{
	&t2822_TI, "Current", &m15378_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2822_PIs[] =
{
	&t2822____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2822____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2822_m15374_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15374_GM;
MethodInfo m15374_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2822_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2822_m15374_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15374_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15375_GM;
MethodInfo m15375_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2822_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15375_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15376_GM;
MethodInfo m15376_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2822_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15376_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15377_GM;
MethodInfo m15377_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2822_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15377_GM};
extern Il2CppType t450_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15378_GM;
MethodInfo m15378_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2822_TI, &t450_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15378_GM};
static MethodInfo* t2822_MIs[] =
{
	&m15374_MI,
	&m15375_MI,
	&m15376_MI,
	&m15377_MI,
	&m15378_MI,
	NULL
};
extern MethodInfo m15377_MI;
extern MethodInfo m15376_MI;
static MethodInfo* t2822_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15375_MI,
	&m15377_MI,
	&m15376_MI,
	&m15378_MI,
};
static TypeInfo* t2822_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4335_TI,
};
static Il2CppInterfaceOffsetPair t2822_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4335_TI, 7},
};
extern TypeInfo t450_TI;
static Il2CppRGCTXData t2822_RGCTXData[3] = 
{
	&m15378_MI/* Method Usage */,
	&t450_TI/* Class Usage */,
	&m21990_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2822_0_0_0;
extern Il2CppType t2822_1_0_0;
extern Il2CppGenericClass t2822_GC;
TypeInfo t2822_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2822_MIs, t2822_PIs, t2822_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2822_TI, t2822_ITIs, t2822_VT, &EmptyCustomAttributesCache, &t2822_TI, &t2822_0_0_0, &t2822_1_0_0, t2822_IOs, &t2822_GC, NULL, NULL, NULL, t2822_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2822)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5544_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.ScriptableObject>
extern MethodInfo m29009_MI;
static PropertyInfo t5544____Count_PropertyInfo = 
{
	&t5544_TI, "Count", &m29009_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29010_MI;
static PropertyInfo t5544____IsReadOnly_PropertyInfo = 
{
	&t5544_TI, "IsReadOnly", &m29010_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5544_PIs[] =
{
	&t5544____Count_PropertyInfo,
	&t5544____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29009_GM;
MethodInfo m29009_MI = 
{
	"get_Count", NULL, &t5544_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29009_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29010_GM;
MethodInfo m29010_MI = 
{
	"get_IsReadOnly", NULL, &t5544_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29010_GM};
extern Il2CppType t450_0_0_0;
extern Il2CppType t450_0_0_0;
static ParameterInfo t5544_m29011_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t450_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29011_GM;
MethodInfo m29011_MI = 
{
	"Add", NULL, &t5544_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5544_m29011_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29011_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29012_GM;
MethodInfo m29012_MI = 
{
	"Clear", NULL, &t5544_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29012_GM};
extern Il2CppType t450_0_0_0;
static ParameterInfo t5544_m29013_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t450_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29013_GM;
MethodInfo m29013_MI = 
{
	"Contains", NULL, &t5544_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5544_m29013_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29013_GM};
extern Il2CppType t3736_0_0_0;
extern Il2CppType t3736_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5544_m29014_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3736_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29014_GM;
MethodInfo m29014_MI = 
{
	"CopyTo", NULL, &t5544_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5544_m29014_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29014_GM};
extern Il2CppType t450_0_0_0;
static ParameterInfo t5544_m29015_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t450_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29015_GM;
MethodInfo m29015_MI = 
{
	"Remove", NULL, &t5544_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5544_m29015_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29015_GM};
static MethodInfo* t5544_MIs[] =
{
	&m29009_MI,
	&m29010_MI,
	&m29011_MI,
	&m29012_MI,
	&m29013_MI,
	&m29014_MI,
	&m29015_MI,
	NULL
};
extern TypeInfo t5546_TI;
static TypeInfo* t5544_ITIs[] = 
{
	&t603_TI,
	&t5546_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5544_0_0_0;
extern Il2CppType t5544_1_0_0;
struct t5544;
extern Il2CppGenericClass t5544_GC;
TypeInfo t5544_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5544_MIs, t5544_PIs, NULL, NULL, NULL, NULL, NULL, &t5544_TI, t5544_ITIs, NULL, &EmptyCustomAttributesCache, &t5544_TI, &t5544_0_0_0, &t5544_1_0_0, NULL, &t5544_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.ScriptableObject>
extern Il2CppType t4335_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29016_GM;
MethodInfo m29016_MI = 
{
	"GetEnumerator", NULL, &t5546_TI, &t4335_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29016_GM};
static MethodInfo* t5546_MIs[] =
{
	&m29016_MI,
	NULL
};
static TypeInfo* t5546_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5546_0_0_0;
extern Il2CppType t5546_1_0_0;
struct t5546;
extern Il2CppGenericClass t5546_GC;
TypeInfo t5546_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5546_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5546_TI, t5546_ITIs, NULL, &EmptyCustomAttributesCache, &t5546_TI, &t5546_0_0_0, &t5546_1_0_0, NULL, &t5546_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5545_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.ScriptableObject>
extern MethodInfo m29017_MI;
extern MethodInfo m29018_MI;
static PropertyInfo t5545____Item_PropertyInfo = 
{
	&t5545_TI, "Item", &m29017_MI, &m29018_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5545_PIs[] =
{
	&t5545____Item_PropertyInfo,
	NULL
};
extern Il2CppType t450_0_0_0;
static ParameterInfo t5545_m29019_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t450_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29019_GM;
MethodInfo m29019_MI = 
{
	"IndexOf", NULL, &t5545_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5545_m29019_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29019_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t450_0_0_0;
static ParameterInfo t5545_m29020_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t450_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29020_GM;
MethodInfo m29020_MI = 
{
	"Insert", NULL, &t5545_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5545_m29020_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29020_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5545_m29021_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29021_GM;
MethodInfo m29021_MI = 
{
	"RemoveAt", NULL, &t5545_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5545_m29021_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29021_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5545_m29017_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t450_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29017_GM;
MethodInfo m29017_MI = 
{
	"get_Item", NULL, &t5545_TI, &t450_0_0_0, RuntimeInvoker_t29_t44, t5545_m29017_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29017_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t450_0_0_0;
static ParameterInfo t5545_m29018_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t450_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29018_GM;
MethodInfo m29018_MI = 
{
	"set_Item", NULL, &t5545_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5545_m29018_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29018_GM};
static MethodInfo* t5545_MIs[] =
{
	&m29019_MI,
	&m29020_MI,
	&m29021_MI,
	&m29017_MI,
	&m29018_MI,
	NULL
};
static TypeInfo* t5545_ITIs[] = 
{
	&t603_TI,
	&t5544_TI,
	&t5546_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5545_0_0_0;
extern Il2CppType t5545_1_0_0;
struct t5545;
extern Il2CppGenericClass t5545_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5545_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5545_MIs, t5545_PIs, NULL, NULL, NULL, NULL, NULL, &t5545_TI, t5545_ITIs, NULL, &t1908__CustomAttributeCache, &t5545_TI, &t5545_0_0_0, &t5545_1_0_0, NULL, &t5545_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2823.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2823_TI;
#include "t2823MD.h"

#include "t41.h"
#include "t557.h"
#include "mscorlib_ArrayTypes.h"
#include "t2824.h"
extern TypeInfo t316_TI;
extern TypeInfo t29_TI;
extern TypeInfo t2824_TI;
extern TypeInfo t21_TI;
#include "t2824MD.h"
extern MethodInfo m15381_MI;
extern MethodInfo m15383_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.ScriptableObject>
extern Il2CppType t316_0_0_33;
FieldInfo t2823_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2823_TI, offsetof(t2823, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2823_FIs[] =
{
	&t2823_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t450_0_0_0;
static ParameterInfo t2823_m15379_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t450_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15379_GM;
MethodInfo m15379_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2823_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2823_m15379_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15379_GM};
extern Il2CppType t316_0_0_0;
extern Il2CppType t316_0_0_0;
static ParameterInfo t2823_m15380_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15380_GM;
MethodInfo m15380_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2823_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2823_m15380_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15380_GM};
static MethodInfo* t2823_MIs[] =
{
	&m15379_MI,
	&m15380_MI,
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m1332_MI;
extern MethodInfo m15380_MI;
extern MethodInfo m15384_MI;
static MethodInfo* t2823_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15380_MI,
	&m15384_MI,
};
extern Il2CppType t2825_0_0_0;
extern TypeInfo t2825_TI;
extern MethodInfo m22000_MI;
extern TypeInfo t450_TI;
extern MethodInfo m15386_MI;
extern TypeInfo t450_TI;
static Il2CppRGCTXData t2823_RGCTXData[8] = 
{
	&t2825_0_0_0/* Type Usage */,
	&t2825_TI/* Class Usage */,
	&m22000_MI/* Method Usage */,
	&t450_TI/* Class Usage */,
	&m15386_MI/* Method Usage */,
	&m15381_MI/* Method Usage */,
	&t450_TI/* Class Usage */,
	&m15383_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2823_0_0_0;
extern Il2CppType t2823_1_0_0;
struct t2823;
extern Il2CppGenericClass t2823_GC;
TypeInfo t2823_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2823_MIs, NULL, t2823_FIs, NULL, &t2824_TI, NULL, NULL, &t2823_TI, NULL, t2823_VT, &EmptyCustomAttributesCache, &t2823_TI, &t2823_0_0_0, &t2823_1_0_0, NULL, &t2823_GC, NULL, NULL, NULL, t2823_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2823), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2825.h"
#include "t42.h"
#include "t43.h"
#include "t353.h"
#include "t305.h"
extern TypeInfo t2825_TI;
extern TypeInfo t42_TI;
extern TypeInfo t305_TI;
#include "t556MD.h"
#include "t42MD.h"
#include "t353MD.h"
#include "t305MD.h"
#include "t2825MD.h"
extern MethodInfo m2790_MI;
extern MethodInfo m1554_MI;
extern MethodInfo m2957_MI;
extern MethodInfo m1597_MI;
extern MethodInfo m2789_MI;
extern MethodInfo m1935_MI;
extern MethodInfo m2791_MI;
extern MethodInfo m2953_MI;
extern MethodInfo m2951_MI;
struct t556;
#include "t556.h"
struct t556;
 void m19555_gshared (t29 * __this, t29 * p0, MethodInfo* method);
#define m19555(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)
#define m22000(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.ScriptableObject>
extern Il2CppType t2825_0_0_1;
FieldInfo t2824_f0_FieldInfo = 
{
	"Delegate", &t2825_0_0_1, &t2824_TI, offsetof(t2824, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2824_FIs[] =
{
	&t2824_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2824_m15381_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15381_GM;
MethodInfo m15381_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2824_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2824_m15381_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15381_GM};
extern Il2CppType t2825_0_0_0;
static ParameterInfo t2824_m15382_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2825_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15382_GM;
MethodInfo m15382_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2824_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2824_m15382_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15382_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2824_m15383_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15383_GM;
MethodInfo m15383_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2824_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2824_m15383_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15383_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2824_m15384_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15384_GM;
MethodInfo m15384_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2824_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2824_m15384_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15384_GM};
static MethodInfo* t2824_MIs[] =
{
	&m15381_MI,
	&m15382_MI,
	&m15383_MI,
	&m15384_MI,
	NULL
};
static MethodInfo* t2824_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15383_MI,
	&m15384_MI,
};
extern TypeInfo t2825_TI;
extern TypeInfo t450_TI;
static Il2CppRGCTXData t2824_RGCTXData[5] = 
{
	&t2825_0_0_0/* Type Usage */,
	&t2825_TI/* Class Usage */,
	&m22000_MI/* Method Usage */,
	&t450_TI/* Class Usage */,
	&m15386_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2824_0_0_0;
extern Il2CppType t2824_1_0_0;
extern TypeInfo t556_TI;
struct t2824;
extern Il2CppGenericClass t2824_GC;
TypeInfo t2824_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2824_MIs, NULL, t2824_FIs, NULL, &t556_TI, NULL, NULL, &t2824_TI, NULL, t2824_VT, &EmptyCustomAttributesCache, &t2824_TI, &t2824_0_0_0, &t2824_1_0_0, NULL, &t2824_GC, NULL, NULL, NULL, t2824_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2824), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t35.h"
#include "t67.h"


// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.ScriptableObject>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2825_m15385_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15385_GM;
MethodInfo m15385_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2825_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2825_m15385_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15385_GM};
extern Il2CppType t450_0_0_0;
static ParameterInfo t2825_m15386_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t450_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15386_GM;
MethodInfo m15386_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2825_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2825_m15386_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15386_GM};
extern Il2CppType t450_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2825_m15387_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t450_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15387_GM;
MethodInfo m15387_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2825_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2825_m15387_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15387_GM};
extern Il2CppType t66_0_0_0;
extern Il2CppType t66_0_0_0;
static ParameterInfo t2825_m15388_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15388_GM;
MethodInfo m15388_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2825_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2825_m15388_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15388_GM};
static MethodInfo* t2825_MIs[] =
{
	&m15385_MI,
	&m15386_MI,
	&m15387_MI,
	&m15388_MI,
	NULL
};
extern MethodInfo m1699_MI;
extern MethodInfo m1700_MI;
extern MethodInfo m1701_MI;
extern MethodInfo m1702_MI;
extern MethodInfo m1703_MI;
extern MethodInfo m1704_MI;
extern MethodInfo m1705_MI;
extern MethodInfo m15387_MI;
extern MethodInfo m15388_MI;
static MethodInfo* t2825_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m15386_MI,
	&m15387_MI,
	&m15388_MI,
};
extern TypeInfo t373_TI;
extern TypeInfo t374_TI;
static Il2CppInterfaceOffsetPair t2825_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2825_1_0_0;
extern TypeInfo t195_TI;
struct t2825;
extern Il2CppGenericClass t2825_GC;
TypeInfo t2825_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2825_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2825_TI, NULL, t2825_VT, &EmptyCustomAttributesCache, &t2825_TI, &t2825_0_0_0, &t2825_1_0_0, t2825_IOs, &t2825_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2825), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4337_TI;

#include "t334.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Renderer>
extern MethodInfo m29022_MI;
static PropertyInfo t4337____Current_PropertyInfo = 
{
	&t4337_TI, "Current", &m29022_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4337_PIs[] =
{
	&t4337____Current_PropertyInfo,
	NULL
};
extern Il2CppType t334_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29022_GM;
MethodInfo m29022_MI = 
{
	"get_Current", NULL, &t4337_TI, &t334_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29022_GM};
static MethodInfo* t4337_MIs[] =
{
	&m29022_MI,
	NULL
};
static TypeInfo* t4337_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4337_0_0_0;
extern Il2CppType t4337_1_0_0;
struct t4337;
extern Il2CppGenericClass t4337_GC;
TypeInfo t4337_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4337_MIs, t4337_PIs, NULL, NULL, NULL, NULL, NULL, &t4337_TI, t4337_ITIs, NULL, &EmptyCustomAttributesCache, &t4337_TI, &t4337_0_0_0, &t4337_1_0_0, NULL, &t4337_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2826.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2826_TI;
#include "t2826MD.h"

extern TypeInfo t334_TI;
extern MethodInfo m15393_MI;
extern MethodInfo m22002_MI;
struct t20;
#define m22002(__this, p0, method) (t334 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Renderer>
extern Il2CppType t20_0_0_1;
FieldInfo t2826_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2826_TI, offsetof(t2826, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2826_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2826_TI, offsetof(t2826, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2826_FIs[] =
{
	&t2826_f0_FieldInfo,
	&t2826_f1_FieldInfo,
	NULL
};
extern MethodInfo m15390_MI;
static PropertyInfo t2826____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2826_TI, "System.Collections.IEnumerator.Current", &m15390_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2826____Current_PropertyInfo = 
{
	&t2826_TI, "Current", &m15393_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2826_PIs[] =
{
	&t2826____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2826____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2826_m15389_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15389_GM;
MethodInfo m15389_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2826_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2826_m15389_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15389_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15390_GM;
MethodInfo m15390_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2826_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15390_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15391_GM;
MethodInfo m15391_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2826_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15391_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15392_GM;
MethodInfo m15392_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2826_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15392_GM};
extern Il2CppType t334_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15393_GM;
MethodInfo m15393_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2826_TI, &t334_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15393_GM};
static MethodInfo* t2826_MIs[] =
{
	&m15389_MI,
	&m15390_MI,
	&m15391_MI,
	&m15392_MI,
	&m15393_MI,
	NULL
};
extern MethodInfo m15392_MI;
extern MethodInfo m15391_MI;
static MethodInfo* t2826_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15390_MI,
	&m15392_MI,
	&m15391_MI,
	&m15393_MI,
};
static TypeInfo* t2826_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4337_TI,
};
static Il2CppInterfaceOffsetPair t2826_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4337_TI, 7},
};
extern TypeInfo t334_TI;
static Il2CppRGCTXData t2826_RGCTXData[3] = 
{
	&m15393_MI/* Method Usage */,
	&t334_TI/* Class Usage */,
	&m22002_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2826_0_0_0;
extern Il2CppType t2826_1_0_0;
extern Il2CppGenericClass t2826_GC;
TypeInfo t2826_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2826_MIs, t2826_PIs, t2826_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2826_TI, t2826_ITIs, t2826_VT, &EmptyCustomAttributesCache, &t2826_TI, &t2826_0_0_0, &t2826_1_0_0, t2826_IOs, &t2826_GC, NULL, NULL, NULL, t2826_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2826)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5547_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Renderer>
extern MethodInfo m29023_MI;
static PropertyInfo t5547____Count_PropertyInfo = 
{
	&t5547_TI, "Count", &m29023_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29024_MI;
static PropertyInfo t5547____IsReadOnly_PropertyInfo = 
{
	&t5547_TI, "IsReadOnly", &m29024_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5547_PIs[] =
{
	&t5547____Count_PropertyInfo,
	&t5547____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29023_GM;
MethodInfo m29023_MI = 
{
	"get_Count", NULL, &t5547_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29023_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29024_GM;
MethodInfo m29024_MI = 
{
	"get_IsReadOnly", NULL, &t5547_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29024_GM};
extern Il2CppType t334_0_0_0;
extern Il2CppType t334_0_0_0;
static ParameterInfo t5547_m29025_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t334_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29025_GM;
MethodInfo m29025_MI = 
{
	"Add", NULL, &t5547_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5547_m29025_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29025_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29026_GM;
MethodInfo m29026_MI = 
{
	"Clear", NULL, &t5547_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29026_GM};
extern Il2CppType t334_0_0_0;
static ParameterInfo t5547_m29027_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t334_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29027_GM;
MethodInfo m29027_MI = 
{
	"Contains", NULL, &t5547_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5547_m29027_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29027_GM};
extern Il2CppType t3737_0_0_0;
extern Il2CppType t3737_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5547_m29028_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3737_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29028_GM;
MethodInfo m29028_MI = 
{
	"CopyTo", NULL, &t5547_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5547_m29028_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29028_GM};
extern Il2CppType t334_0_0_0;
static ParameterInfo t5547_m29029_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t334_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29029_GM;
MethodInfo m29029_MI = 
{
	"Remove", NULL, &t5547_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5547_m29029_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29029_GM};
static MethodInfo* t5547_MIs[] =
{
	&m29023_MI,
	&m29024_MI,
	&m29025_MI,
	&m29026_MI,
	&m29027_MI,
	&m29028_MI,
	&m29029_MI,
	NULL
};
extern TypeInfo t5549_TI;
static TypeInfo* t5547_ITIs[] = 
{
	&t603_TI,
	&t5549_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5547_0_0_0;
extern Il2CppType t5547_1_0_0;
struct t5547;
extern Il2CppGenericClass t5547_GC;
TypeInfo t5547_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5547_MIs, t5547_PIs, NULL, NULL, NULL, NULL, NULL, &t5547_TI, t5547_ITIs, NULL, &EmptyCustomAttributesCache, &t5547_TI, &t5547_0_0_0, &t5547_1_0_0, NULL, &t5547_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Renderer>
extern Il2CppType t4337_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29030_GM;
MethodInfo m29030_MI = 
{
	"GetEnumerator", NULL, &t5549_TI, &t4337_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29030_GM};
static MethodInfo* t5549_MIs[] =
{
	&m29030_MI,
	NULL
};
static TypeInfo* t5549_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5549_0_0_0;
extern Il2CppType t5549_1_0_0;
struct t5549;
extern Il2CppGenericClass t5549_GC;
TypeInfo t5549_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5549_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5549_TI, t5549_ITIs, NULL, &EmptyCustomAttributesCache, &t5549_TI, &t5549_0_0_0, &t5549_1_0_0, NULL, &t5549_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5548_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Renderer>
extern MethodInfo m29031_MI;
extern MethodInfo m29032_MI;
static PropertyInfo t5548____Item_PropertyInfo = 
{
	&t5548_TI, "Item", &m29031_MI, &m29032_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5548_PIs[] =
{
	&t5548____Item_PropertyInfo,
	NULL
};
extern Il2CppType t334_0_0_0;
static ParameterInfo t5548_m29033_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t334_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29033_GM;
MethodInfo m29033_MI = 
{
	"IndexOf", NULL, &t5548_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5548_m29033_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29033_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t334_0_0_0;
static ParameterInfo t5548_m29034_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t334_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29034_GM;
MethodInfo m29034_MI = 
{
	"Insert", NULL, &t5548_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5548_m29034_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29034_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5548_m29035_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29035_GM;
MethodInfo m29035_MI = 
{
	"RemoveAt", NULL, &t5548_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5548_m29035_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29035_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5548_m29031_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t334_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29031_GM;
MethodInfo m29031_MI = 
{
	"get_Item", NULL, &t5548_TI, &t334_0_0_0, RuntimeInvoker_t29_t44, t5548_m29031_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29031_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t334_0_0_0;
static ParameterInfo t5548_m29032_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t334_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29032_GM;
MethodInfo m29032_MI = 
{
	"set_Item", NULL, &t5548_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5548_m29032_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29032_GM};
static MethodInfo* t5548_MIs[] =
{
	&m29033_MI,
	&m29034_MI,
	&m29035_MI,
	&m29031_MI,
	&m29032_MI,
	NULL
};
static TypeInfo* t5548_ITIs[] = 
{
	&t603_TI,
	&t5547_TI,
	&t5549_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5548_0_0_0;
extern Il2CppType t5548_1_0_0;
struct t5548;
extern Il2CppGenericClass t5548_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5548_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5548_MIs, t5548_PIs, NULL, NULL, NULL, NULL, NULL, &t5548_TI, t5548_ITIs, NULL, &t1908__CustomAttributeCache, &t5548_TI, &t5548_0_0_0, &t5548_1_0_0, NULL, &t5548_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2827.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2827_TI;
#include "t2827MD.h"

#include "t2828.h"
extern TypeInfo t2828_TI;
#include "t2828MD.h"
extern MethodInfo m15396_MI;
extern MethodInfo m15398_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Renderer>
extern Il2CppType t316_0_0_33;
FieldInfo t2827_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2827_TI, offsetof(t2827, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2827_FIs[] =
{
	&t2827_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t334_0_0_0;
static ParameterInfo t2827_m15394_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t334_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15394_GM;
MethodInfo m15394_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2827_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2827_m15394_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15394_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2827_m15395_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15395_GM;
MethodInfo m15395_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2827_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2827_m15395_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15395_GM};
static MethodInfo* t2827_MIs[] =
{
	&m15394_MI,
	&m15395_MI,
	NULL
};
extern MethodInfo m15395_MI;
extern MethodInfo m15399_MI;
static MethodInfo* t2827_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15395_MI,
	&m15399_MI,
};
extern Il2CppType t2829_0_0_0;
extern TypeInfo t2829_TI;
extern MethodInfo m22012_MI;
extern TypeInfo t334_TI;
extern MethodInfo m15401_MI;
extern TypeInfo t334_TI;
static Il2CppRGCTXData t2827_RGCTXData[8] = 
{
	&t2829_0_0_0/* Type Usage */,
	&t2829_TI/* Class Usage */,
	&m22012_MI/* Method Usage */,
	&t334_TI/* Class Usage */,
	&m15401_MI/* Method Usage */,
	&m15396_MI/* Method Usage */,
	&t334_TI/* Class Usage */,
	&m15398_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2827_0_0_0;
extern Il2CppType t2827_1_0_0;
struct t2827;
extern Il2CppGenericClass t2827_GC;
TypeInfo t2827_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2827_MIs, NULL, t2827_FIs, NULL, &t2828_TI, NULL, NULL, &t2827_TI, NULL, t2827_VT, &EmptyCustomAttributesCache, &t2827_TI, &t2827_0_0_0, &t2827_1_0_0, NULL, &t2827_GC, NULL, NULL, NULL, t2827_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2827), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2829.h"
extern TypeInfo t2829_TI;
#include "t2829MD.h"
struct t556;
#define m22012(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.Renderer>
extern Il2CppType t2829_0_0_1;
FieldInfo t2828_f0_FieldInfo = 
{
	"Delegate", &t2829_0_0_1, &t2828_TI, offsetof(t2828, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2828_FIs[] =
{
	&t2828_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2828_m15396_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15396_GM;
MethodInfo m15396_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2828_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2828_m15396_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15396_GM};
extern Il2CppType t2829_0_0_0;
static ParameterInfo t2828_m15397_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2829_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15397_GM;
MethodInfo m15397_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2828_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2828_m15397_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15397_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2828_m15398_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15398_GM;
MethodInfo m15398_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2828_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2828_m15398_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15398_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2828_m15399_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15399_GM;
MethodInfo m15399_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2828_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2828_m15399_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15399_GM};
static MethodInfo* t2828_MIs[] =
{
	&m15396_MI,
	&m15397_MI,
	&m15398_MI,
	&m15399_MI,
	NULL
};
static MethodInfo* t2828_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15398_MI,
	&m15399_MI,
};
extern TypeInfo t2829_TI;
extern TypeInfo t334_TI;
static Il2CppRGCTXData t2828_RGCTXData[5] = 
{
	&t2829_0_0_0/* Type Usage */,
	&t2829_TI/* Class Usage */,
	&m22012_MI/* Method Usage */,
	&t334_TI/* Class Usage */,
	&m15401_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2828_0_0_0;
extern Il2CppType t2828_1_0_0;
struct t2828;
extern Il2CppGenericClass t2828_GC;
TypeInfo t2828_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2828_MIs, NULL, t2828_FIs, NULL, &t556_TI, NULL, NULL, &t2828_TI, NULL, t2828_VT, &EmptyCustomAttributesCache, &t2828_TI, &t2828_0_0_0, &t2828_1_0_0, NULL, &t2828_GC, NULL, NULL, NULL, t2828_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2828), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.Renderer>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2829_m15400_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15400_GM;
MethodInfo m15400_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2829_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2829_m15400_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15400_GM};
extern Il2CppType t334_0_0_0;
static ParameterInfo t2829_m15401_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t334_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15401_GM;
MethodInfo m15401_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2829_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2829_m15401_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15401_GM};
extern Il2CppType t334_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2829_m15402_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t334_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15402_GM;
MethodInfo m15402_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2829_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2829_m15402_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15402_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2829_m15403_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15403_GM;
MethodInfo m15403_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2829_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2829_m15403_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15403_GM};
static MethodInfo* t2829_MIs[] =
{
	&m15400_MI,
	&m15401_MI,
	&m15402_MI,
	&m15403_MI,
	NULL
};
extern MethodInfo m15402_MI;
extern MethodInfo m15403_MI;
static MethodInfo* t2829_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m15401_MI,
	&m15402_MI,
	&m15403_MI,
};
static Il2CppInterfaceOffsetPair t2829_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2829_1_0_0;
struct t2829;
extern Il2CppGenericClass t2829_GC;
TypeInfo t2829_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2829_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2829_TI, NULL, t2829_VT, &EmptyCustomAttributesCache, &t2829_TI, &t2829_0_0_0, &t2829_1_0_0, t2829_IOs, &t2829_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2829), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4339_TI;

#include "t162.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Texture>
extern MethodInfo m29036_MI;
static PropertyInfo t4339____Current_PropertyInfo = 
{
	&t4339_TI, "Current", &m29036_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4339_PIs[] =
{
	&t4339____Current_PropertyInfo,
	NULL
};
extern Il2CppType t162_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29036_GM;
MethodInfo m29036_MI = 
{
	"get_Current", NULL, &t4339_TI, &t162_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29036_GM};
static MethodInfo* t4339_MIs[] =
{
	&m29036_MI,
	NULL
};
static TypeInfo* t4339_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4339_0_0_0;
extern Il2CppType t4339_1_0_0;
struct t4339;
extern Il2CppGenericClass t4339_GC;
TypeInfo t4339_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4339_MIs, t4339_PIs, NULL, NULL, NULL, NULL, NULL, &t4339_TI, t4339_ITIs, NULL, &EmptyCustomAttributesCache, &t4339_TI, &t4339_0_0_0, &t4339_1_0_0, NULL, &t4339_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2830.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2830_TI;
#include "t2830MD.h"

extern TypeInfo t162_TI;
extern MethodInfo m15408_MI;
extern MethodInfo m22014_MI;
struct t20;
#define m22014(__this, p0, method) (t162 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Texture>
extern Il2CppType t20_0_0_1;
FieldInfo t2830_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2830_TI, offsetof(t2830, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2830_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2830_TI, offsetof(t2830, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2830_FIs[] =
{
	&t2830_f0_FieldInfo,
	&t2830_f1_FieldInfo,
	NULL
};
extern MethodInfo m15405_MI;
static PropertyInfo t2830____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2830_TI, "System.Collections.IEnumerator.Current", &m15405_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2830____Current_PropertyInfo = 
{
	&t2830_TI, "Current", &m15408_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2830_PIs[] =
{
	&t2830____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2830____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2830_m15404_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15404_GM;
MethodInfo m15404_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2830_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2830_m15404_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15404_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15405_GM;
MethodInfo m15405_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2830_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15405_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15406_GM;
MethodInfo m15406_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2830_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15406_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15407_GM;
MethodInfo m15407_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2830_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15407_GM};
extern Il2CppType t162_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15408_GM;
MethodInfo m15408_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2830_TI, &t162_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15408_GM};
static MethodInfo* t2830_MIs[] =
{
	&m15404_MI,
	&m15405_MI,
	&m15406_MI,
	&m15407_MI,
	&m15408_MI,
	NULL
};
extern MethodInfo m15407_MI;
extern MethodInfo m15406_MI;
static MethodInfo* t2830_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15405_MI,
	&m15407_MI,
	&m15406_MI,
	&m15408_MI,
};
static TypeInfo* t2830_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4339_TI,
};
static Il2CppInterfaceOffsetPair t2830_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4339_TI, 7},
};
extern TypeInfo t162_TI;
static Il2CppRGCTXData t2830_RGCTXData[3] = 
{
	&m15408_MI/* Method Usage */,
	&t162_TI/* Class Usage */,
	&m22014_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2830_0_0_0;
extern Il2CppType t2830_1_0_0;
extern Il2CppGenericClass t2830_GC;
TypeInfo t2830_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2830_MIs, t2830_PIs, t2830_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2830_TI, t2830_ITIs, t2830_VT, &EmptyCustomAttributesCache, &t2830_TI, &t2830_0_0_0, &t2830_1_0_0, t2830_IOs, &t2830_GC, NULL, NULL, NULL, t2830_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2830)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5550_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Texture>
extern MethodInfo m29037_MI;
static PropertyInfo t5550____Count_PropertyInfo = 
{
	&t5550_TI, "Count", &m29037_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29038_MI;
static PropertyInfo t5550____IsReadOnly_PropertyInfo = 
{
	&t5550_TI, "IsReadOnly", &m29038_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5550_PIs[] =
{
	&t5550____Count_PropertyInfo,
	&t5550____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29037_GM;
MethodInfo m29037_MI = 
{
	"get_Count", NULL, &t5550_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29037_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29038_GM;
MethodInfo m29038_MI = 
{
	"get_IsReadOnly", NULL, &t5550_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29038_GM};
extern Il2CppType t162_0_0_0;
extern Il2CppType t162_0_0_0;
static ParameterInfo t5550_m29039_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t162_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29039_GM;
MethodInfo m29039_MI = 
{
	"Add", NULL, &t5550_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5550_m29039_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29039_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29040_GM;
MethodInfo m29040_MI = 
{
	"Clear", NULL, &t5550_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29040_GM};
extern Il2CppType t162_0_0_0;
static ParameterInfo t5550_m29041_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t162_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29041_GM;
MethodInfo m29041_MI = 
{
	"Contains", NULL, &t5550_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5550_m29041_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29041_GM};
extern Il2CppType t3738_0_0_0;
extern Il2CppType t3738_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5550_m29042_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3738_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29042_GM;
MethodInfo m29042_MI = 
{
	"CopyTo", NULL, &t5550_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5550_m29042_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29042_GM};
extern Il2CppType t162_0_0_0;
static ParameterInfo t5550_m29043_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t162_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29043_GM;
MethodInfo m29043_MI = 
{
	"Remove", NULL, &t5550_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5550_m29043_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29043_GM};
static MethodInfo* t5550_MIs[] =
{
	&m29037_MI,
	&m29038_MI,
	&m29039_MI,
	&m29040_MI,
	&m29041_MI,
	&m29042_MI,
	&m29043_MI,
	NULL
};
extern TypeInfo t5552_TI;
static TypeInfo* t5550_ITIs[] = 
{
	&t603_TI,
	&t5552_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5550_0_0_0;
extern Il2CppType t5550_1_0_0;
struct t5550;
extern Il2CppGenericClass t5550_GC;
TypeInfo t5550_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5550_MIs, t5550_PIs, NULL, NULL, NULL, NULL, NULL, &t5550_TI, t5550_ITIs, NULL, &EmptyCustomAttributesCache, &t5550_TI, &t5550_0_0_0, &t5550_1_0_0, NULL, &t5550_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Texture>
extern Il2CppType t4339_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29044_GM;
MethodInfo m29044_MI = 
{
	"GetEnumerator", NULL, &t5552_TI, &t4339_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29044_GM};
static MethodInfo* t5552_MIs[] =
{
	&m29044_MI,
	NULL
};
static TypeInfo* t5552_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5552_0_0_0;
extern Il2CppType t5552_1_0_0;
struct t5552;
extern Il2CppGenericClass t5552_GC;
TypeInfo t5552_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5552_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5552_TI, t5552_ITIs, NULL, &EmptyCustomAttributesCache, &t5552_TI, &t5552_0_0_0, &t5552_1_0_0, NULL, &t5552_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5551_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Texture>
extern MethodInfo m29045_MI;
extern MethodInfo m29046_MI;
static PropertyInfo t5551____Item_PropertyInfo = 
{
	&t5551_TI, "Item", &m29045_MI, &m29046_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5551_PIs[] =
{
	&t5551____Item_PropertyInfo,
	NULL
};
extern Il2CppType t162_0_0_0;
static ParameterInfo t5551_m29047_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t162_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29047_GM;
MethodInfo m29047_MI = 
{
	"IndexOf", NULL, &t5551_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5551_m29047_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29047_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t162_0_0_0;
static ParameterInfo t5551_m29048_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t162_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29048_GM;
MethodInfo m29048_MI = 
{
	"Insert", NULL, &t5551_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5551_m29048_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29048_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5551_m29049_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29049_GM;
MethodInfo m29049_MI = 
{
	"RemoveAt", NULL, &t5551_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5551_m29049_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29049_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5551_m29045_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t162_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29045_GM;
MethodInfo m29045_MI = 
{
	"get_Item", NULL, &t5551_TI, &t162_0_0_0, RuntimeInvoker_t29_t44, t5551_m29045_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29045_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t162_0_0_0;
static ParameterInfo t5551_m29046_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t162_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29046_GM;
MethodInfo m29046_MI = 
{
	"set_Item", NULL, &t5551_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5551_m29046_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29046_GM};
static MethodInfo* t5551_MIs[] =
{
	&m29047_MI,
	&m29048_MI,
	&m29049_MI,
	&m29045_MI,
	&m29046_MI,
	NULL
};
static TypeInfo* t5551_ITIs[] = 
{
	&t603_TI,
	&t5550_TI,
	&t5552_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5551_0_0_0;
extern Il2CppType t5551_1_0_0;
struct t5551;
extern Il2CppGenericClass t5551_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5551_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5551_MIs, t5551_PIs, NULL, NULL, NULL, NULL, NULL, &t5551_TI, t5551_ITIs, NULL, &t1908__CustomAttributeCache, &t5551_TI, &t5551_0_0_0, &t5551_1_0_0, NULL, &t5551_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2831.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2831_TI;
#include "t2831MD.h"

#include "t2832.h"
extern TypeInfo t2832_TI;
#include "t2832MD.h"
extern MethodInfo m15411_MI;
extern MethodInfo m15413_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Texture>
extern Il2CppType t316_0_0_33;
FieldInfo t2831_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2831_TI, offsetof(t2831, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2831_FIs[] =
{
	&t2831_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t162_0_0_0;
static ParameterInfo t2831_m15409_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t162_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15409_GM;
MethodInfo m15409_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2831_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2831_m15409_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15409_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2831_m15410_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15410_GM;
MethodInfo m15410_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2831_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2831_m15410_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15410_GM};
static MethodInfo* t2831_MIs[] =
{
	&m15409_MI,
	&m15410_MI,
	NULL
};
extern MethodInfo m15410_MI;
extern MethodInfo m15414_MI;
static MethodInfo* t2831_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15410_MI,
	&m15414_MI,
};
extern Il2CppType t2833_0_0_0;
extern TypeInfo t2833_TI;
extern MethodInfo m22024_MI;
extern TypeInfo t162_TI;
extern MethodInfo m15416_MI;
extern TypeInfo t162_TI;
static Il2CppRGCTXData t2831_RGCTXData[8] = 
{
	&t2833_0_0_0/* Type Usage */,
	&t2833_TI/* Class Usage */,
	&m22024_MI/* Method Usage */,
	&t162_TI/* Class Usage */,
	&m15416_MI/* Method Usage */,
	&m15411_MI/* Method Usage */,
	&t162_TI/* Class Usage */,
	&m15413_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2831_0_0_0;
extern Il2CppType t2831_1_0_0;
struct t2831;
extern Il2CppGenericClass t2831_GC;
TypeInfo t2831_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2831_MIs, NULL, t2831_FIs, NULL, &t2832_TI, NULL, NULL, &t2831_TI, NULL, t2831_VT, &EmptyCustomAttributesCache, &t2831_TI, &t2831_0_0_0, &t2831_1_0_0, NULL, &t2831_GC, NULL, NULL, NULL, t2831_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2831), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2833.h"
extern TypeInfo t2833_TI;
#include "t2833MD.h"
struct t556;
#define m22024(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.Texture>
extern Il2CppType t2833_0_0_1;
FieldInfo t2832_f0_FieldInfo = 
{
	"Delegate", &t2833_0_0_1, &t2832_TI, offsetof(t2832, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2832_FIs[] =
{
	&t2832_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2832_m15411_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15411_GM;
MethodInfo m15411_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2832_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2832_m15411_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15411_GM};
extern Il2CppType t2833_0_0_0;
static ParameterInfo t2832_m15412_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2833_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15412_GM;
MethodInfo m15412_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2832_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2832_m15412_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15412_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2832_m15413_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15413_GM;
MethodInfo m15413_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2832_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2832_m15413_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15413_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2832_m15414_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15414_GM;
MethodInfo m15414_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2832_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2832_m15414_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15414_GM};
static MethodInfo* t2832_MIs[] =
{
	&m15411_MI,
	&m15412_MI,
	&m15413_MI,
	&m15414_MI,
	NULL
};
static MethodInfo* t2832_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15413_MI,
	&m15414_MI,
};
extern TypeInfo t2833_TI;
extern TypeInfo t162_TI;
static Il2CppRGCTXData t2832_RGCTXData[5] = 
{
	&t2833_0_0_0/* Type Usage */,
	&t2833_TI/* Class Usage */,
	&m22024_MI/* Method Usage */,
	&t162_TI/* Class Usage */,
	&m15416_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2832_0_0_0;
extern Il2CppType t2832_1_0_0;
struct t2832;
extern Il2CppGenericClass t2832_GC;
TypeInfo t2832_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2832_MIs, NULL, t2832_FIs, NULL, &t556_TI, NULL, NULL, &t2832_TI, NULL, t2832_VT, &EmptyCustomAttributesCache, &t2832_TI, &t2832_0_0_0, &t2832_1_0_0, NULL, &t2832_GC, NULL, NULL, NULL, t2832_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2832), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.Texture>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2833_m15415_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15415_GM;
MethodInfo m15415_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2833_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2833_m15415_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15415_GM};
extern Il2CppType t162_0_0_0;
static ParameterInfo t2833_m15416_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t162_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15416_GM;
MethodInfo m15416_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2833_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2833_m15416_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15416_GM};
extern Il2CppType t162_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2833_m15417_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t162_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15417_GM;
MethodInfo m15417_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2833_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2833_m15417_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15417_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2833_m15418_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15418_GM;
MethodInfo m15418_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2833_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2833_m15418_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15418_GM};
static MethodInfo* t2833_MIs[] =
{
	&m15415_MI,
	&m15416_MI,
	&m15417_MI,
	&m15418_MI,
	NULL
};
extern MethodInfo m15417_MI;
extern MethodInfo m15418_MI;
static MethodInfo* t2833_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m15416_MI,
	&m15417_MI,
	&m15418_MI,
};
static Il2CppInterfaceOffsetPair t2833_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2833_1_0_0;
struct t2833;
extern Il2CppGenericClass t2833_GC;
TypeInfo t2833_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2833_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2833_TI, NULL, t2833_VT, &EmptyCustomAttributesCache, &t2833_TI, &t2833_0_0_0, &t2833_1_0_0, t2833_IOs, &t2833_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2833), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4341_TI;

#include "t157.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Texture2D>
extern MethodInfo m29050_MI;
static PropertyInfo t4341____Current_PropertyInfo = 
{
	&t4341_TI, "Current", &m29050_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4341_PIs[] =
{
	&t4341____Current_PropertyInfo,
	NULL
};
extern Il2CppType t157_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29050_GM;
MethodInfo m29050_MI = 
{
	"get_Current", NULL, &t4341_TI, &t157_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29050_GM};
static MethodInfo* t4341_MIs[] =
{
	&m29050_MI,
	NULL
};
static TypeInfo* t4341_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4341_0_0_0;
extern Il2CppType t4341_1_0_0;
struct t4341;
extern Il2CppGenericClass t4341_GC;
TypeInfo t4341_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4341_MIs, t4341_PIs, NULL, NULL, NULL, NULL, NULL, &t4341_TI, t4341_ITIs, NULL, &EmptyCustomAttributesCache, &t4341_TI, &t4341_0_0_0, &t4341_1_0_0, NULL, &t4341_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2834.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2834_TI;
#include "t2834MD.h"

extern TypeInfo t157_TI;
extern MethodInfo m15423_MI;
extern MethodInfo m22026_MI;
struct t20;
#define m22026(__this, p0, method) (t157 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Texture2D>
extern Il2CppType t20_0_0_1;
FieldInfo t2834_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2834_TI, offsetof(t2834, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2834_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2834_TI, offsetof(t2834, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2834_FIs[] =
{
	&t2834_f0_FieldInfo,
	&t2834_f1_FieldInfo,
	NULL
};
extern MethodInfo m15420_MI;
static PropertyInfo t2834____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2834_TI, "System.Collections.IEnumerator.Current", &m15420_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2834____Current_PropertyInfo = 
{
	&t2834_TI, "Current", &m15423_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2834_PIs[] =
{
	&t2834____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2834____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2834_m15419_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15419_GM;
MethodInfo m15419_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2834_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2834_m15419_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15419_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15420_GM;
MethodInfo m15420_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2834_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15420_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15421_GM;
MethodInfo m15421_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2834_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15421_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15422_GM;
MethodInfo m15422_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2834_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15422_GM};
extern Il2CppType t157_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15423_GM;
MethodInfo m15423_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2834_TI, &t157_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15423_GM};
static MethodInfo* t2834_MIs[] =
{
	&m15419_MI,
	&m15420_MI,
	&m15421_MI,
	&m15422_MI,
	&m15423_MI,
	NULL
};
extern MethodInfo m15422_MI;
extern MethodInfo m15421_MI;
static MethodInfo* t2834_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15420_MI,
	&m15422_MI,
	&m15421_MI,
	&m15423_MI,
};
static TypeInfo* t2834_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4341_TI,
};
static Il2CppInterfaceOffsetPair t2834_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4341_TI, 7},
};
extern TypeInfo t157_TI;
static Il2CppRGCTXData t2834_RGCTXData[3] = 
{
	&m15423_MI/* Method Usage */,
	&t157_TI/* Class Usage */,
	&m22026_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2834_0_0_0;
extern Il2CppType t2834_1_0_0;
extern Il2CppGenericClass t2834_GC;
TypeInfo t2834_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2834_MIs, t2834_PIs, t2834_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2834_TI, t2834_ITIs, t2834_VT, &EmptyCustomAttributesCache, &t2834_TI, &t2834_0_0_0, &t2834_1_0_0, t2834_IOs, &t2834_GC, NULL, NULL, NULL, t2834_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2834)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5553_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Texture2D>
extern MethodInfo m29051_MI;
static PropertyInfo t5553____Count_PropertyInfo = 
{
	&t5553_TI, "Count", &m29051_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29052_MI;
static PropertyInfo t5553____IsReadOnly_PropertyInfo = 
{
	&t5553_TI, "IsReadOnly", &m29052_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5553_PIs[] =
{
	&t5553____Count_PropertyInfo,
	&t5553____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29051_GM;
MethodInfo m29051_MI = 
{
	"get_Count", NULL, &t5553_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29051_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29052_GM;
MethodInfo m29052_MI = 
{
	"get_IsReadOnly", NULL, &t5553_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29052_GM};
extern Il2CppType t157_0_0_0;
extern Il2CppType t157_0_0_0;
static ParameterInfo t5553_m29053_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t157_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29053_GM;
MethodInfo m29053_MI = 
{
	"Add", NULL, &t5553_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5553_m29053_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29053_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29054_GM;
MethodInfo m29054_MI = 
{
	"Clear", NULL, &t5553_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29054_GM};
extern Il2CppType t157_0_0_0;
static ParameterInfo t5553_m29055_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t157_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29055_GM;
MethodInfo m29055_MI = 
{
	"Contains", NULL, &t5553_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5553_m29055_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29055_GM};
extern Il2CppType t3739_0_0_0;
extern Il2CppType t3739_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5553_m29056_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3739_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29056_GM;
MethodInfo m29056_MI = 
{
	"CopyTo", NULL, &t5553_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5553_m29056_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29056_GM};
extern Il2CppType t157_0_0_0;
static ParameterInfo t5553_m29057_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t157_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29057_GM;
MethodInfo m29057_MI = 
{
	"Remove", NULL, &t5553_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5553_m29057_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29057_GM};
static MethodInfo* t5553_MIs[] =
{
	&m29051_MI,
	&m29052_MI,
	&m29053_MI,
	&m29054_MI,
	&m29055_MI,
	&m29056_MI,
	&m29057_MI,
	NULL
};
extern TypeInfo t5555_TI;
static TypeInfo* t5553_ITIs[] = 
{
	&t603_TI,
	&t5555_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5553_0_0_0;
extern Il2CppType t5553_1_0_0;
struct t5553;
extern Il2CppGenericClass t5553_GC;
TypeInfo t5553_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5553_MIs, t5553_PIs, NULL, NULL, NULL, NULL, NULL, &t5553_TI, t5553_ITIs, NULL, &EmptyCustomAttributesCache, &t5553_TI, &t5553_0_0_0, &t5553_1_0_0, NULL, &t5553_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Texture2D>
extern Il2CppType t4341_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29058_GM;
MethodInfo m29058_MI = 
{
	"GetEnumerator", NULL, &t5555_TI, &t4341_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29058_GM};
static MethodInfo* t5555_MIs[] =
{
	&m29058_MI,
	NULL
};
static TypeInfo* t5555_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5555_0_0_0;
extern Il2CppType t5555_1_0_0;
struct t5555;
extern Il2CppGenericClass t5555_GC;
TypeInfo t5555_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5555_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5555_TI, t5555_ITIs, NULL, &EmptyCustomAttributesCache, &t5555_TI, &t5555_0_0_0, &t5555_1_0_0, NULL, &t5555_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5554_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Texture2D>
extern MethodInfo m29059_MI;
extern MethodInfo m29060_MI;
static PropertyInfo t5554____Item_PropertyInfo = 
{
	&t5554_TI, "Item", &m29059_MI, &m29060_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5554_PIs[] =
{
	&t5554____Item_PropertyInfo,
	NULL
};
extern Il2CppType t157_0_0_0;
static ParameterInfo t5554_m29061_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t157_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29061_GM;
MethodInfo m29061_MI = 
{
	"IndexOf", NULL, &t5554_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5554_m29061_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29061_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t157_0_0_0;
static ParameterInfo t5554_m29062_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t157_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29062_GM;
MethodInfo m29062_MI = 
{
	"Insert", NULL, &t5554_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5554_m29062_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29062_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5554_m29063_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29063_GM;
MethodInfo m29063_MI = 
{
	"RemoveAt", NULL, &t5554_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5554_m29063_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29063_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5554_m29059_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t157_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29059_GM;
MethodInfo m29059_MI = 
{
	"get_Item", NULL, &t5554_TI, &t157_0_0_0, RuntimeInvoker_t29_t44, t5554_m29059_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29059_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t157_0_0_0;
static ParameterInfo t5554_m29060_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t157_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29060_GM;
MethodInfo m29060_MI = 
{
	"set_Item", NULL, &t5554_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5554_m29060_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29060_GM};
static MethodInfo* t5554_MIs[] =
{
	&m29061_MI,
	&m29062_MI,
	&m29063_MI,
	&m29059_MI,
	&m29060_MI,
	NULL
};
static TypeInfo* t5554_ITIs[] = 
{
	&t603_TI,
	&t5553_TI,
	&t5555_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5554_0_0_0;
extern Il2CppType t5554_1_0_0;
struct t5554;
extern Il2CppGenericClass t5554_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5554_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5554_MIs, t5554_PIs, NULL, NULL, NULL, NULL, NULL, &t5554_TI, t5554_ITIs, NULL, &t1908__CustomAttributeCache, &t5554_TI, &t5554_0_0_0, &t5554_1_0_0, NULL, &t5554_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2835.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2835_TI;
#include "t2835MD.h"

#include "t2836.h"
extern TypeInfo t2836_TI;
#include "t2836MD.h"
extern MethodInfo m15426_MI;
extern MethodInfo m15428_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Texture2D>
extern Il2CppType t316_0_0_33;
FieldInfo t2835_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2835_TI, offsetof(t2835, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2835_FIs[] =
{
	&t2835_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t157_0_0_0;
static ParameterInfo t2835_m15424_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t157_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15424_GM;
MethodInfo m15424_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2835_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2835_m15424_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15424_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2835_m15425_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15425_GM;
MethodInfo m15425_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2835_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2835_m15425_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15425_GM};
static MethodInfo* t2835_MIs[] =
{
	&m15424_MI,
	&m15425_MI,
	NULL
};
extern MethodInfo m15425_MI;
extern MethodInfo m15429_MI;
static MethodInfo* t2835_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15425_MI,
	&m15429_MI,
};
extern Il2CppType t2837_0_0_0;
extern TypeInfo t2837_TI;
extern MethodInfo m22036_MI;
extern TypeInfo t157_TI;
extern MethodInfo m15431_MI;
extern TypeInfo t157_TI;
static Il2CppRGCTXData t2835_RGCTXData[8] = 
{
	&t2837_0_0_0/* Type Usage */,
	&t2837_TI/* Class Usage */,
	&m22036_MI/* Method Usage */,
	&t157_TI/* Class Usage */,
	&m15431_MI/* Method Usage */,
	&m15426_MI/* Method Usage */,
	&t157_TI/* Class Usage */,
	&m15428_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2835_0_0_0;
extern Il2CppType t2835_1_0_0;
struct t2835;
extern Il2CppGenericClass t2835_GC;
TypeInfo t2835_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2835_MIs, NULL, t2835_FIs, NULL, &t2836_TI, NULL, NULL, &t2835_TI, NULL, t2835_VT, &EmptyCustomAttributesCache, &t2835_TI, &t2835_0_0_0, &t2835_1_0_0, NULL, &t2835_GC, NULL, NULL, NULL, t2835_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2835), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2837.h"
extern TypeInfo t2837_TI;
#include "t2837MD.h"
struct t556;
#define m22036(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.Texture2D>
extern Il2CppType t2837_0_0_1;
FieldInfo t2836_f0_FieldInfo = 
{
	"Delegate", &t2837_0_0_1, &t2836_TI, offsetof(t2836, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2836_FIs[] =
{
	&t2836_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2836_m15426_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15426_GM;
MethodInfo m15426_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2836_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2836_m15426_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15426_GM};
extern Il2CppType t2837_0_0_0;
static ParameterInfo t2836_m15427_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2837_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15427_GM;
MethodInfo m15427_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2836_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2836_m15427_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15427_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2836_m15428_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15428_GM;
MethodInfo m15428_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2836_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2836_m15428_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15428_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2836_m15429_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15429_GM;
MethodInfo m15429_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2836_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2836_m15429_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15429_GM};
static MethodInfo* t2836_MIs[] =
{
	&m15426_MI,
	&m15427_MI,
	&m15428_MI,
	&m15429_MI,
	NULL
};
static MethodInfo* t2836_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15428_MI,
	&m15429_MI,
};
extern TypeInfo t2837_TI;
extern TypeInfo t157_TI;
static Il2CppRGCTXData t2836_RGCTXData[5] = 
{
	&t2837_0_0_0/* Type Usage */,
	&t2837_TI/* Class Usage */,
	&m22036_MI/* Method Usage */,
	&t157_TI/* Class Usage */,
	&m15431_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2836_0_0_0;
extern Il2CppType t2836_1_0_0;
struct t2836;
extern Il2CppGenericClass t2836_GC;
TypeInfo t2836_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2836_MIs, NULL, t2836_FIs, NULL, &t556_TI, NULL, NULL, &t2836_TI, NULL, t2836_VT, &EmptyCustomAttributesCache, &t2836_TI, &t2836_0_0_0, &t2836_1_0_0, NULL, &t2836_GC, NULL, NULL, NULL, t2836_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2836), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.Texture2D>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2837_m15430_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15430_GM;
MethodInfo m15430_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2837_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2837_m15430_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15430_GM};
extern Il2CppType t157_0_0_0;
static ParameterInfo t2837_m15431_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t157_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15431_GM;
MethodInfo m15431_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2837_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2837_m15431_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15431_GM};
extern Il2CppType t157_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2837_m15432_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t157_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15432_GM;
MethodInfo m15432_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2837_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2837_m15432_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15432_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2837_m15433_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15433_GM;
MethodInfo m15433_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2837_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2837_m15433_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15433_GM};
static MethodInfo* t2837_MIs[] =
{
	&m15430_MI,
	&m15431_MI,
	&m15432_MI,
	&m15433_MI,
	NULL
};
extern MethodInfo m15432_MI;
extern MethodInfo m15433_MI;
static MethodInfo* t2837_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m15431_MI,
	&m15432_MI,
	&m15433_MI,
};
static Il2CppInterfaceOffsetPair t2837_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2837_1_0_0;
struct t2837;
extern Il2CppGenericClass t2837_GC;
TypeInfo t2837_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2837_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2837_TI, NULL, t2837_VT, &EmptyCustomAttributesCache, &t2837_TI, &t2837_0_0_0, &t2837_1_0_0, t2837_IOs, &t2837_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2837), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4343_TI;

#include "t453.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.RenderTexture>
extern MethodInfo m29064_MI;
static PropertyInfo t4343____Current_PropertyInfo = 
{
	&t4343_TI, "Current", &m29064_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4343_PIs[] =
{
	&t4343____Current_PropertyInfo,
	NULL
};
extern Il2CppType t453_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29064_GM;
MethodInfo m29064_MI = 
{
	"get_Current", NULL, &t4343_TI, &t453_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29064_GM};
static MethodInfo* t4343_MIs[] =
{
	&m29064_MI,
	NULL
};
static TypeInfo* t4343_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4343_0_0_0;
extern Il2CppType t4343_1_0_0;
struct t4343;
extern Il2CppGenericClass t4343_GC;
TypeInfo t4343_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4343_MIs, t4343_PIs, NULL, NULL, NULL, NULL, NULL, &t4343_TI, t4343_ITIs, NULL, &EmptyCustomAttributesCache, &t4343_TI, &t4343_0_0_0, &t4343_1_0_0, NULL, &t4343_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2838.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2838_TI;
#include "t2838MD.h"

extern TypeInfo t453_TI;
extern MethodInfo m15438_MI;
extern MethodInfo m22038_MI;
struct t20;
#define m22038(__this, p0, method) (t453 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.RenderTexture>
extern Il2CppType t20_0_0_1;
FieldInfo t2838_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2838_TI, offsetof(t2838, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2838_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2838_TI, offsetof(t2838, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2838_FIs[] =
{
	&t2838_f0_FieldInfo,
	&t2838_f1_FieldInfo,
	NULL
};
extern MethodInfo m15435_MI;
static PropertyInfo t2838____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2838_TI, "System.Collections.IEnumerator.Current", &m15435_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2838____Current_PropertyInfo = 
{
	&t2838_TI, "Current", &m15438_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2838_PIs[] =
{
	&t2838____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2838____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2838_m15434_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15434_GM;
MethodInfo m15434_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2838_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2838_m15434_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15434_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15435_GM;
MethodInfo m15435_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2838_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15435_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15436_GM;
MethodInfo m15436_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2838_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15436_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15437_GM;
MethodInfo m15437_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2838_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15437_GM};
extern Il2CppType t453_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15438_GM;
MethodInfo m15438_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2838_TI, &t453_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15438_GM};
static MethodInfo* t2838_MIs[] =
{
	&m15434_MI,
	&m15435_MI,
	&m15436_MI,
	&m15437_MI,
	&m15438_MI,
	NULL
};
extern MethodInfo m15437_MI;
extern MethodInfo m15436_MI;
static MethodInfo* t2838_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15435_MI,
	&m15437_MI,
	&m15436_MI,
	&m15438_MI,
};
static TypeInfo* t2838_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4343_TI,
};
static Il2CppInterfaceOffsetPair t2838_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4343_TI, 7},
};
extern TypeInfo t453_TI;
static Il2CppRGCTXData t2838_RGCTXData[3] = 
{
	&m15438_MI/* Method Usage */,
	&t453_TI/* Class Usage */,
	&m22038_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2838_0_0_0;
extern Il2CppType t2838_1_0_0;
extern Il2CppGenericClass t2838_GC;
TypeInfo t2838_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2838_MIs, t2838_PIs, t2838_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2838_TI, t2838_ITIs, t2838_VT, &EmptyCustomAttributesCache, &t2838_TI, &t2838_0_0_0, &t2838_1_0_0, t2838_IOs, &t2838_GC, NULL, NULL, NULL, t2838_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2838)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5556_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.RenderTexture>
extern MethodInfo m29065_MI;
static PropertyInfo t5556____Count_PropertyInfo = 
{
	&t5556_TI, "Count", &m29065_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29066_MI;
static PropertyInfo t5556____IsReadOnly_PropertyInfo = 
{
	&t5556_TI, "IsReadOnly", &m29066_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5556_PIs[] =
{
	&t5556____Count_PropertyInfo,
	&t5556____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29065_GM;
MethodInfo m29065_MI = 
{
	"get_Count", NULL, &t5556_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29065_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29066_GM;
MethodInfo m29066_MI = 
{
	"get_IsReadOnly", NULL, &t5556_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29066_GM};
extern Il2CppType t453_0_0_0;
extern Il2CppType t453_0_0_0;
static ParameterInfo t5556_m29067_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t453_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29067_GM;
MethodInfo m29067_MI = 
{
	"Add", NULL, &t5556_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5556_m29067_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29067_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29068_GM;
MethodInfo m29068_MI = 
{
	"Clear", NULL, &t5556_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29068_GM};
extern Il2CppType t453_0_0_0;
static ParameterInfo t5556_m29069_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t453_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29069_GM;
MethodInfo m29069_MI = 
{
	"Contains", NULL, &t5556_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5556_m29069_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29069_GM};
extern Il2CppType t3740_0_0_0;
extern Il2CppType t3740_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5556_m29070_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3740_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29070_GM;
MethodInfo m29070_MI = 
{
	"CopyTo", NULL, &t5556_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5556_m29070_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29070_GM};
extern Il2CppType t453_0_0_0;
static ParameterInfo t5556_m29071_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t453_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29071_GM;
MethodInfo m29071_MI = 
{
	"Remove", NULL, &t5556_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5556_m29071_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29071_GM};
static MethodInfo* t5556_MIs[] =
{
	&m29065_MI,
	&m29066_MI,
	&m29067_MI,
	&m29068_MI,
	&m29069_MI,
	&m29070_MI,
	&m29071_MI,
	NULL
};
extern TypeInfo t5558_TI;
static TypeInfo* t5556_ITIs[] = 
{
	&t603_TI,
	&t5558_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5556_0_0_0;
extern Il2CppType t5556_1_0_0;
struct t5556;
extern Il2CppGenericClass t5556_GC;
TypeInfo t5556_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5556_MIs, t5556_PIs, NULL, NULL, NULL, NULL, NULL, &t5556_TI, t5556_ITIs, NULL, &EmptyCustomAttributesCache, &t5556_TI, &t5556_0_0_0, &t5556_1_0_0, NULL, &t5556_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.RenderTexture>
extern Il2CppType t4343_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29072_GM;
MethodInfo m29072_MI = 
{
	"GetEnumerator", NULL, &t5558_TI, &t4343_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29072_GM};
static MethodInfo* t5558_MIs[] =
{
	&m29072_MI,
	NULL
};
static TypeInfo* t5558_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5558_0_0_0;
extern Il2CppType t5558_1_0_0;
struct t5558;
extern Il2CppGenericClass t5558_GC;
TypeInfo t5558_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5558_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5558_TI, t5558_ITIs, NULL, &EmptyCustomAttributesCache, &t5558_TI, &t5558_0_0_0, &t5558_1_0_0, NULL, &t5558_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5557_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.RenderTexture>
extern MethodInfo m29073_MI;
extern MethodInfo m29074_MI;
static PropertyInfo t5557____Item_PropertyInfo = 
{
	&t5557_TI, "Item", &m29073_MI, &m29074_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5557_PIs[] =
{
	&t5557____Item_PropertyInfo,
	NULL
};
extern Il2CppType t453_0_0_0;
static ParameterInfo t5557_m29075_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t453_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29075_GM;
MethodInfo m29075_MI = 
{
	"IndexOf", NULL, &t5557_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5557_m29075_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29075_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t453_0_0_0;
static ParameterInfo t5557_m29076_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t453_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29076_GM;
MethodInfo m29076_MI = 
{
	"Insert", NULL, &t5557_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5557_m29076_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29076_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5557_m29077_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29077_GM;
MethodInfo m29077_MI = 
{
	"RemoveAt", NULL, &t5557_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5557_m29077_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29077_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5557_m29073_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t453_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29073_GM;
MethodInfo m29073_MI = 
{
	"get_Item", NULL, &t5557_TI, &t453_0_0_0, RuntimeInvoker_t29_t44, t5557_m29073_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29073_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t453_0_0_0;
static ParameterInfo t5557_m29074_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t453_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29074_GM;
MethodInfo m29074_MI = 
{
	"set_Item", NULL, &t5557_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5557_m29074_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29074_GM};
static MethodInfo* t5557_MIs[] =
{
	&m29075_MI,
	&m29076_MI,
	&m29077_MI,
	&m29073_MI,
	&m29074_MI,
	NULL
};
static TypeInfo* t5557_ITIs[] = 
{
	&t603_TI,
	&t5556_TI,
	&t5558_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5557_0_0_0;
extern Il2CppType t5557_1_0_0;
struct t5557;
extern Il2CppGenericClass t5557_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5557_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5557_MIs, t5557_PIs, NULL, NULL, NULL, NULL, NULL, &t5557_TI, t5557_ITIs, NULL, &t1908__CustomAttributeCache, &t5557_TI, &t5557_0_0_0, &t5557_1_0_0, NULL, &t5557_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2839.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2839_TI;
#include "t2839MD.h"

#include "t2840.h"
extern TypeInfo t2840_TI;
#include "t2840MD.h"
extern MethodInfo m15441_MI;
extern MethodInfo m15443_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.RenderTexture>
extern Il2CppType t316_0_0_33;
FieldInfo t2839_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2839_TI, offsetof(t2839, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2839_FIs[] =
{
	&t2839_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t453_0_0_0;
static ParameterInfo t2839_m15439_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t453_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15439_GM;
MethodInfo m15439_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2839_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2839_m15439_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15439_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2839_m15440_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15440_GM;
MethodInfo m15440_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2839_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2839_m15440_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15440_GM};
static MethodInfo* t2839_MIs[] =
{
	&m15439_MI,
	&m15440_MI,
	NULL
};
extern MethodInfo m15440_MI;
extern MethodInfo m15444_MI;
static MethodInfo* t2839_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15440_MI,
	&m15444_MI,
};
extern Il2CppType t2841_0_0_0;
extern TypeInfo t2841_TI;
extern MethodInfo m22048_MI;
extern TypeInfo t453_TI;
extern MethodInfo m15446_MI;
extern TypeInfo t453_TI;
static Il2CppRGCTXData t2839_RGCTXData[8] = 
{
	&t2841_0_0_0/* Type Usage */,
	&t2841_TI/* Class Usage */,
	&m22048_MI/* Method Usage */,
	&t453_TI/* Class Usage */,
	&m15446_MI/* Method Usage */,
	&m15441_MI/* Method Usage */,
	&t453_TI/* Class Usage */,
	&m15443_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2839_0_0_0;
extern Il2CppType t2839_1_0_0;
struct t2839;
extern Il2CppGenericClass t2839_GC;
TypeInfo t2839_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2839_MIs, NULL, t2839_FIs, NULL, &t2840_TI, NULL, NULL, &t2839_TI, NULL, t2839_VT, &EmptyCustomAttributesCache, &t2839_TI, &t2839_0_0_0, &t2839_1_0_0, NULL, &t2839_GC, NULL, NULL, NULL, t2839_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2839), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2841.h"
extern TypeInfo t2841_TI;
#include "t2841MD.h"
struct t556;
#define m22048(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.RenderTexture>
extern Il2CppType t2841_0_0_1;
FieldInfo t2840_f0_FieldInfo = 
{
	"Delegate", &t2841_0_0_1, &t2840_TI, offsetof(t2840, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2840_FIs[] =
{
	&t2840_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2840_m15441_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15441_GM;
MethodInfo m15441_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2840_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2840_m15441_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15441_GM};
extern Il2CppType t2841_0_0_0;
static ParameterInfo t2840_m15442_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2841_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15442_GM;
MethodInfo m15442_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2840_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2840_m15442_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15442_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2840_m15443_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15443_GM;
MethodInfo m15443_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2840_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2840_m15443_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15443_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2840_m15444_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15444_GM;
MethodInfo m15444_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2840_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2840_m15444_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15444_GM};
static MethodInfo* t2840_MIs[] =
{
	&m15441_MI,
	&m15442_MI,
	&m15443_MI,
	&m15444_MI,
	NULL
};
static MethodInfo* t2840_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15443_MI,
	&m15444_MI,
};
extern TypeInfo t2841_TI;
extern TypeInfo t453_TI;
static Il2CppRGCTXData t2840_RGCTXData[5] = 
{
	&t2841_0_0_0/* Type Usage */,
	&t2841_TI/* Class Usage */,
	&m22048_MI/* Method Usage */,
	&t453_TI/* Class Usage */,
	&m15446_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2840_0_0_0;
extern Il2CppType t2840_1_0_0;
struct t2840;
extern Il2CppGenericClass t2840_GC;
TypeInfo t2840_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2840_MIs, NULL, t2840_FIs, NULL, &t556_TI, NULL, NULL, &t2840_TI, NULL, t2840_VT, &EmptyCustomAttributesCache, &t2840_TI, &t2840_0_0_0, &t2840_1_0_0, NULL, &t2840_GC, NULL, NULL, NULL, t2840_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2840), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.RenderTexture>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2841_m15445_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15445_GM;
MethodInfo m15445_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2841_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2841_m15445_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15445_GM};
extern Il2CppType t453_0_0_0;
static ParameterInfo t2841_m15446_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t453_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15446_GM;
MethodInfo m15446_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2841_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2841_m15446_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15446_GM};
extern Il2CppType t453_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2841_m15447_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t453_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15447_GM;
MethodInfo m15447_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2841_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2841_m15447_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15447_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2841_m15448_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15448_GM;
MethodInfo m15448_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2841_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2841_m15448_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15448_GM};
static MethodInfo* t2841_MIs[] =
{
	&m15445_MI,
	&m15446_MI,
	&m15447_MI,
	&m15448_MI,
	NULL
};
extern MethodInfo m15447_MI;
extern MethodInfo m15448_MI;
static MethodInfo* t2841_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m15446_MI,
	&m15447_MI,
	&m15448_MI,
};
static Il2CppInterfaceOffsetPair t2841_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2841_1_0_0;
struct t2841;
extern Il2CppGenericClass t2841_GC;
TypeInfo t2841_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2841_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2841_TI, NULL, t2841_VT, &EmptyCustomAttributesCache, &t2841_TI, &t2841_0_0_0, &t2841_1_0_0, t2841_IOs, &t2841_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2841), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4345_TI;

#include "t454.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.ReflectionProbe>
extern MethodInfo m29078_MI;
static PropertyInfo t4345____Current_PropertyInfo = 
{
	&t4345_TI, "Current", &m29078_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4345_PIs[] =
{
	&t4345____Current_PropertyInfo,
	NULL
};
extern Il2CppType t454_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29078_GM;
MethodInfo m29078_MI = 
{
	"get_Current", NULL, &t4345_TI, &t454_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29078_GM};
static MethodInfo* t4345_MIs[] =
{
	&m29078_MI,
	NULL
};
static TypeInfo* t4345_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4345_0_0_0;
extern Il2CppType t4345_1_0_0;
struct t4345;
extern Il2CppGenericClass t4345_GC;
TypeInfo t4345_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4345_MIs, t4345_PIs, NULL, NULL, NULL, NULL, NULL, &t4345_TI, t4345_ITIs, NULL, &EmptyCustomAttributesCache, &t4345_TI, &t4345_0_0_0, &t4345_1_0_0, NULL, &t4345_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2842.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2842_TI;
#include "t2842MD.h"

extern TypeInfo t454_TI;
extern MethodInfo m15453_MI;
extern MethodInfo m22050_MI;
struct t20;
#define m22050(__this, p0, method) (t454 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.ReflectionProbe>
extern Il2CppType t20_0_0_1;
FieldInfo t2842_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2842_TI, offsetof(t2842, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2842_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2842_TI, offsetof(t2842, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2842_FIs[] =
{
	&t2842_f0_FieldInfo,
	&t2842_f1_FieldInfo,
	NULL
};
extern MethodInfo m15450_MI;
static PropertyInfo t2842____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2842_TI, "System.Collections.IEnumerator.Current", &m15450_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2842____Current_PropertyInfo = 
{
	&t2842_TI, "Current", &m15453_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2842_PIs[] =
{
	&t2842____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2842____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2842_m15449_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15449_GM;
MethodInfo m15449_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2842_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2842_m15449_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15449_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15450_GM;
MethodInfo m15450_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2842_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15450_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15451_GM;
MethodInfo m15451_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2842_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15451_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15452_GM;
MethodInfo m15452_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2842_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15452_GM};
extern Il2CppType t454_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15453_GM;
MethodInfo m15453_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2842_TI, &t454_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15453_GM};
static MethodInfo* t2842_MIs[] =
{
	&m15449_MI,
	&m15450_MI,
	&m15451_MI,
	&m15452_MI,
	&m15453_MI,
	NULL
};
extern MethodInfo m15452_MI;
extern MethodInfo m15451_MI;
static MethodInfo* t2842_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15450_MI,
	&m15452_MI,
	&m15451_MI,
	&m15453_MI,
};
static TypeInfo* t2842_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4345_TI,
};
static Il2CppInterfaceOffsetPair t2842_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4345_TI, 7},
};
extern TypeInfo t454_TI;
static Il2CppRGCTXData t2842_RGCTXData[3] = 
{
	&m15453_MI/* Method Usage */,
	&t454_TI/* Class Usage */,
	&m22050_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2842_0_0_0;
extern Il2CppType t2842_1_0_0;
extern Il2CppGenericClass t2842_GC;
TypeInfo t2842_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2842_MIs, t2842_PIs, t2842_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2842_TI, t2842_ITIs, t2842_VT, &EmptyCustomAttributesCache, &t2842_TI, &t2842_0_0_0, &t2842_1_0_0, t2842_IOs, &t2842_GC, NULL, NULL, NULL, t2842_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2842)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5559_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.ReflectionProbe>
extern MethodInfo m29079_MI;
static PropertyInfo t5559____Count_PropertyInfo = 
{
	&t5559_TI, "Count", &m29079_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29080_MI;
static PropertyInfo t5559____IsReadOnly_PropertyInfo = 
{
	&t5559_TI, "IsReadOnly", &m29080_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5559_PIs[] =
{
	&t5559____Count_PropertyInfo,
	&t5559____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29079_GM;
MethodInfo m29079_MI = 
{
	"get_Count", NULL, &t5559_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29079_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29080_GM;
MethodInfo m29080_MI = 
{
	"get_IsReadOnly", NULL, &t5559_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29080_GM};
extern Il2CppType t454_0_0_0;
extern Il2CppType t454_0_0_0;
static ParameterInfo t5559_m29081_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t454_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29081_GM;
MethodInfo m29081_MI = 
{
	"Add", NULL, &t5559_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5559_m29081_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29081_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29082_GM;
MethodInfo m29082_MI = 
{
	"Clear", NULL, &t5559_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29082_GM};
extern Il2CppType t454_0_0_0;
static ParameterInfo t5559_m29083_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t454_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29083_GM;
MethodInfo m29083_MI = 
{
	"Contains", NULL, &t5559_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5559_m29083_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29083_GM};
extern Il2CppType t3741_0_0_0;
extern Il2CppType t3741_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5559_m29084_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3741_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29084_GM;
MethodInfo m29084_MI = 
{
	"CopyTo", NULL, &t5559_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5559_m29084_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29084_GM};
extern Il2CppType t454_0_0_0;
static ParameterInfo t5559_m29085_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t454_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29085_GM;
MethodInfo m29085_MI = 
{
	"Remove", NULL, &t5559_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5559_m29085_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29085_GM};
static MethodInfo* t5559_MIs[] =
{
	&m29079_MI,
	&m29080_MI,
	&m29081_MI,
	&m29082_MI,
	&m29083_MI,
	&m29084_MI,
	&m29085_MI,
	NULL
};
extern TypeInfo t5561_TI;
static TypeInfo* t5559_ITIs[] = 
{
	&t603_TI,
	&t5561_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5559_0_0_0;
extern Il2CppType t5559_1_0_0;
struct t5559;
extern Il2CppGenericClass t5559_GC;
TypeInfo t5559_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5559_MIs, t5559_PIs, NULL, NULL, NULL, NULL, NULL, &t5559_TI, t5559_ITIs, NULL, &EmptyCustomAttributesCache, &t5559_TI, &t5559_0_0_0, &t5559_1_0_0, NULL, &t5559_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.ReflectionProbe>
extern Il2CppType t4345_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29086_GM;
MethodInfo m29086_MI = 
{
	"GetEnumerator", NULL, &t5561_TI, &t4345_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29086_GM};
static MethodInfo* t5561_MIs[] =
{
	&m29086_MI,
	NULL
};
static TypeInfo* t5561_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5561_0_0_0;
extern Il2CppType t5561_1_0_0;
struct t5561;
extern Il2CppGenericClass t5561_GC;
TypeInfo t5561_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5561_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5561_TI, t5561_ITIs, NULL, &EmptyCustomAttributesCache, &t5561_TI, &t5561_0_0_0, &t5561_1_0_0, NULL, &t5561_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5560_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.ReflectionProbe>
extern MethodInfo m29087_MI;
extern MethodInfo m29088_MI;
static PropertyInfo t5560____Item_PropertyInfo = 
{
	&t5560_TI, "Item", &m29087_MI, &m29088_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5560_PIs[] =
{
	&t5560____Item_PropertyInfo,
	NULL
};
extern Il2CppType t454_0_0_0;
static ParameterInfo t5560_m29089_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t454_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29089_GM;
MethodInfo m29089_MI = 
{
	"IndexOf", NULL, &t5560_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5560_m29089_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29089_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t454_0_0_0;
static ParameterInfo t5560_m29090_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t454_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29090_GM;
MethodInfo m29090_MI = 
{
	"Insert", NULL, &t5560_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5560_m29090_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29090_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5560_m29091_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29091_GM;
MethodInfo m29091_MI = 
{
	"RemoveAt", NULL, &t5560_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5560_m29091_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29091_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5560_m29087_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t454_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29087_GM;
MethodInfo m29087_MI = 
{
	"get_Item", NULL, &t5560_TI, &t454_0_0_0, RuntimeInvoker_t29_t44, t5560_m29087_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29087_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t454_0_0_0;
static ParameterInfo t5560_m29088_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t454_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29088_GM;
MethodInfo m29088_MI = 
{
	"set_Item", NULL, &t5560_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5560_m29088_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29088_GM};
static MethodInfo* t5560_MIs[] =
{
	&m29089_MI,
	&m29090_MI,
	&m29091_MI,
	&m29087_MI,
	&m29088_MI,
	NULL
};
static TypeInfo* t5560_ITIs[] = 
{
	&t603_TI,
	&t5559_TI,
	&t5561_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5560_0_0_0;
extern Il2CppType t5560_1_0_0;
struct t5560;
extern Il2CppGenericClass t5560_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5560_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5560_MIs, t5560_PIs, NULL, NULL, NULL, NULL, NULL, &t5560_TI, t5560_ITIs, NULL, &t1908__CustomAttributeCache, &t5560_TI, &t5560_0_0_0, &t5560_1_0_0, NULL, &t5560_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2843.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2843_TI;
#include "t2843MD.h"

#include "t2844.h"
extern TypeInfo t2844_TI;
#include "t2844MD.h"
extern MethodInfo m15456_MI;
extern MethodInfo m15458_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.ReflectionProbe>
extern Il2CppType t316_0_0_33;
FieldInfo t2843_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2843_TI, offsetof(t2843, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2843_FIs[] =
{
	&t2843_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t454_0_0_0;
static ParameterInfo t2843_m15454_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t454_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15454_GM;
MethodInfo m15454_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2843_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2843_m15454_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15454_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2843_m15455_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15455_GM;
MethodInfo m15455_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2843_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2843_m15455_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15455_GM};
static MethodInfo* t2843_MIs[] =
{
	&m15454_MI,
	&m15455_MI,
	NULL
};
extern MethodInfo m15455_MI;
extern MethodInfo m15459_MI;
static MethodInfo* t2843_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15455_MI,
	&m15459_MI,
};
extern Il2CppType t2845_0_0_0;
extern TypeInfo t2845_TI;
extern MethodInfo m22060_MI;
extern TypeInfo t454_TI;
extern MethodInfo m15461_MI;
extern TypeInfo t454_TI;
static Il2CppRGCTXData t2843_RGCTXData[8] = 
{
	&t2845_0_0_0/* Type Usage */,
	&t2845_TI/* Class Usage */,
	&m22060_MI/* Method Usage */,
	&t454_TI/* Class Usage */,
	&m15461_MI/* Method Usage */,
	&m15456_MI/* Method Usage */,
	&t454_TI/* Class Usage */,
	&m15458_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2843_0_0_0;
extern Il2CppType t2843_1_0_0;
struct t2843;
extern Il2CppGenericClass t2843_GC;
TypeInfo t2843_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2843_MIs, NULL, t2843_FIs, NULL, &t2844_TI, NULL, NULL, &t2843_TI, NULL, t2843_VT, &EmptyCustomAttributesCache, &t2843_TI, &t2843_0_0_0, &t2843_1_0_0, NULL, &t2843_GC, NULL, NULL, NULL, t2843_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2843), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2845.h"
extern TypeInfo t2845_TI;
#include "t2845MD.h"
struct t556;
#define m22060(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.ReflectionProbe>
extern Il2CppType t2845_0_0_1;
FieldInfo t2844_f0_FieldInfo = 
{
	"Delegate", &t2845_0_0_1, &t2844_TI, offsetof(t2844, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2844_FIs[] =
{
	&t2844_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2844_m15456_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15456_GM;
MethodInfo m15456_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2844_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2844_m15456_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15456_GM};
extern Il2CppType t2845_0_0_0;
static ParameterInfo t2844_m15457_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2845_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15457_GM;
MethodInfo m15457_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2844_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2844_m15457_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15457_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2844_m15458_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15458_GM;
MethodInfo m15458_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2844_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2844_m15458_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15458_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2844_m15459_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15459_GM;
MethodInfo m15459_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2844_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2844_m15459_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15459_GM};
static MethodInfo* t2844_MIs[] =
{
	&m15456_MI,
	&m15457_MI,
	&m15458_MI,
	&m15459_MI,
	NULL
};
static MethodInfo* t2844_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15458_MI,
	&m15459_MI,
};
extern TypeInfo t2845_TI;
extern TypeInfo t454_TI;
static Il2CppRGCTXData t2844_RGCTXData[5] = 
{
	&t2845_0_0_0/* Type Usage */,
	&t2845_TI/* Class Usage */,
	&m22060_MI/* Method Usage */,
	&t454_TI/* Class Usage */,
	&m15461_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2844_0_0_0;
extern Il2CppType t2844_1_0_0;
struct t2844;
extern Il2CppGenericClass t2844_GC;
TypeInfo t2844_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2844_MIs, NULL, t2844_FIs, NULL, &t556_TI, NULL, NULL, &t2844_TI, NULL, t2844_VT, &EmptyCustomAttributesCache, &t2844_TI, &t2844_0_0_0, &t2844_1_0_0, NULL, &t2844_GC, NULL, NULL, NULL, t2844_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2844), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.ReflectionProbe>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2845_m15460_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15460_GM;
MethodInfo m15460_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2845_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2845_m15460_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15460_GM};
extern Il2CppType t454_0_0_0;
static ParameterInfo t2845_m15461_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t454_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15461_GM;
MethodInfo m15461_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2845_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2845_m15461_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15461_GM};
extern Il2CppType t454_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2845_m15462_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t454_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15462_GM;
MethodInfo m15462_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2845_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2845_m15462_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15462_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2845_m15463_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15463_GM;
MethodInfo m15463_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2845_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2845_m15463_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15463_GM};
static MethodInfo* t2845_MIs[] =
{
	&m15460_MI,
	&m15461_MI,
	&m15462_MI,
	&m15463_MI,
	NULL
};
extern MethodInfo m15462_MI;
extern MethodInfo m15463_MI;
static MethodInfo* t2845_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m15461_MI,
	&m15462_MI,
	&m15463_MI,
};
static Il2CppInterfaceOffsetPair t2845_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2845_1_0_0;
struct t2845;
extern Il2CppGenericClass t2845_GC;
TypeInfo t2845_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2845_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2845_TI, NULL, t2845_VT, &EmptyCustomAttributesCache, &t2845_TI, &t2845_0_0_0, &t2845_1_0_0, t2845_IOs, &t2845_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2845), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4347_TI;

#include "t455.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.GUIElement>
extern MethodInfo m29092_MI;
static PropertyInfo t4347____Current_PropertyInfo = 
{
	&t4347_TI, "Current", &m29092_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4347_PIs[] =
{
	&t4347____Current_PropertyInfo,
	NULL
};
extern Il2CppType t455_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29092_GM;
MethodInfo m29092_MI = 
{
	"get_Current", NULL, &t4347_TI, &t455_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29092_GM};
static MethodInfo* t4347_MIs[] =
{
	&m29092_MI,
	NULL
};
static TypeInfo* t4347_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4347_0_0_0;
extern Il2CppType t4347_1_0_0;
struct t4347;
extern Il2CppGenericClass t4347_GC;
TypeInfo t4347_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4347_MIs, t4347_PIs, NULL, NULL, NULL, NULL, NULL, &t4347_TI, t4347_ITIs, NULL, &EmptyCustomAttributesCache, &t4347_TI, &t4347_0_0_0, &t4347_1_0_0, NULL, &t4347_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2846.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2846_TI;
#include "t2846MD.h"

extern TypeInfo t455_TI;
extern MethodInfo m15468_MI;
extern MethodInfo m22062_MI;
struct t20;
#define m22062(__this, p0, method) (t455 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.GUIElement>
extern Il2CppType t20_0_0_1;
FieldInfo t2846_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2846_TI, offsetof(t2846, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2846_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2846_TI, offsetof(t2846, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2846_FIs[] =
{
	&t2846_f0_FieldInfo,
	&t2846_f1_FieldInfo,
	NULL
};
extern MethodInfo m15465_MI;
static PropertyInfo t2846____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2846_TI, "System.Collections.IEnumerator.Current", &m15465_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2846____Current_PropertyInfo = 
{
	&t2846_TI, "Current", &m15468_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2846_PIs[] =
{
	&t2846____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2846____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2846_m15464_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15464_GM;
MethodInfo m15464_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2846_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2846_m15464_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15464_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15465_GM;
MethodInfo m15465_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2846_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15465_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15466_GM;
MethodInfo m15466_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2846_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15466_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15467_GM;
MethodInfo m15467_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2846_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15467_GM};
extern Il2CppType t455_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15468_GM;
MethodInfo m15468_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2846_TI, &t455_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15468_GM};
static MethodInfo* t2846_MIs[] =
{
	&m15464_MI,
	&m15465_MI,
	&m15466_MI,
	&m15467_MI,
	&m15468_MI,
	NULL
};
extern MethodInfo m15467_MI;
extern MethodInfo m15466_MI;
static MethodInfo* t2846_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15465_MI,
	&m15467_MI,
	&m15466_MI,
	&m15468_MI,
};
static TypeInfo* t2846_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4347_TI,
};
static Il2CppInterfaceOffsetPair t2846_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4347_TI, 7},
};
extern TypeInfo t455_TI;
static Il2CppRGCTXData t2846_RGCTXData[3] = 
{
	&m15468_MI/* Method Usage */,
	&t455_TI/* Class Usage */,
	&m22062_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2846_0_0_0;
extern Il2CppType t2846_1_0_0;
extern Il2CppGenericClass t2846_GC;
TypeInfo t2846_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2846_MIs, t2846_PIs, t2846_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2846_TI, t2846_ITIs, t2846_VT, &EmptyCustomAttributesCache, &t2846_TI, &t2846_0_0_0, &t2846_1_0_0, t2846_IOs, &t2846_GC, NULL, NULL, NULL, t2846_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2846)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5562_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.GUIElement>
extern MethodInfo m29093_MI;
static PropertyInfo t5562____Count_PropertyInfo = 
{
	&t5562_TI, "Count", &m29093_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29094_MI;
static PropertyInfo t5562____IsReadOnly_PropertyInfo = 
{
	&t5562_TI, "IsReadOnly", &m29094_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5562_PIs[] =
{
	&t5562____Count_PropertyInfo,
	&t5562____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29093_GM;
MethodInfo m29093_MI = 
{
	"get_Count", NULL, &t5562_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29093_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29094_GM;
MethodInfo m29094_MI = 
{
	"get_IsReadOnly", NULL, &t5562_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29094_GM};
extern Il2CppType t455_0_0_0;
extern Il2CppType t455_0_0_0;
static ParameterInfo t5562_m29095_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t455_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29095_GM;
MethodInfo m29095_MI = 
{
	"Add", NULL, &t5562_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5562_m29095_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29095_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29096_GM;
MethodInfo m29096_MI = 
{
	"Clear", NULL, &t5562_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29096_GM};
extern Il2CppType t455_0_0_0;
static ParameterInfo t5562_m29097_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t455_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29097_GM;
MethodInfo m29097_MI = 
{
	"Contains", NULL, &t5562_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5562_m29097_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29097_GM};
extern Il2CppType t3742_0_0_0;
extern Il2CppType t3742_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5562_m29098_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3742_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29098_GM;
MethodInfo m29098_MI = 
{
	"CopyTo", NULL, &t5562_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5562_m29098_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29098_GM};
extern Il2CppType t455_0_0_0;
static ParameterInfo t5562_m29099_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t455_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29099_GM;
MethodInfo m29099_MI = 
{
	"Remove", NULL, &t5562_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5562_m29099_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29099_GM};
static MethodInfo* t5562_MIs[] =
{
	&m29093_MI,
	&m29094_MI,
	&m29095_MI,
	&m29096_MI,
	&m29097_MI,
	&m29098_MI,
	&m29099_MI,
	NULL
};
extern TypeInfo t5564_TI;
static TypeInfo* t5562_ITIs[] = 
{
	&t603_TI,
	&t5564_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5562_0_0_0;
extern Il2CppType t5562_1_0_0;
struct t5562;
extern Il2CppGenericClass t5562_GC;
TypeInfo t5562_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5562_MIs, t5562_PIs, NULL, NULL, NULL, NULL, NULL, &t5562_TI, t5562_ITIs, NULL, &EmptyCustomAttributesCache, &t5562_TI, &t5562_0_0_0, &t5562_1_0_0, NULL, &t5562_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.GUIElement>
extern Il2CppType t4347_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29100_GM;
MethodInfo m29100_MI = 
{
	"GetEnumerator", NULL, &t5564_TI, &t4347_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29100_GM};
static MethodInfo* t5564_MIs[] =
{
	&m29100_MI,
	NULL
};
static TypeInfo* t5564_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5564_0_0_0;
extern Il2CppType t5564_1_0_0;
struct t5564;
extern Il2CppGenericClass t5564_GC;
TypeInfo t5564_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5564_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5564_TI, t5564_ITIs, NULL, &EmptyCustomAttributesCache, &t5564_TI, &t5564_0_0_0, &t5564_1_0_0, NULL, &t5564_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5563_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.GUIElement>
extern MethodInfo m29101_MI;
extern MethodInfo m29102_MI;
static PropertyInfo t5563____Item_PropertyInfo = 
{
	&t5563_TI, "Item", &m29101_MI, &m29102_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5563_PIs[] =
{
	&t5563____Item_PropertyInfo,
	NULL
};
extern Il2CppType t455_0_0_0;
static ParameterInfo t5563_m29103_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t455_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29103_GM;
MethodInfo m29103_MI = 
{
	"IndexOf", NULL, &t5563_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5563_m29103_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29103_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t455_0_0_0;
static ParameterInfo t5563_m29104_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t455_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29104_GM;
MethodInfo m29104_MI = 
{
	"Insert", NULL, &t5563_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5563_m29104_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29104_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5563_m29105_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29105_GM;
MethodInfo m29105_MI = 
{
	"RemoveAt", NULL, &t5563_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5563_m29105_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29105_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5563_m29101_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t455_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29101_GM;
MethodInfo m29101_MI = 
{
	"get_Item", NULL, &t5563_TI, &t455_0_0_0, RuntimeInvoker_t29_t44, t5563_m29101_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29101_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t455_0_0_0;
static ParameterInfo t5563_m29102_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t455_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29102_GM;
MethodInfo m29102_MI = 
{
	"set_Item", NULL, &t5563_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5563_m29102_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29102_GM};
static MethodInfo* t5563_MIs[] =
{
	&m29103_MI,
	&m29104_MI,
	&m29105_MI,
	&m29101_MI,
	&m29102_MI,
	NULL
};
static TypeInfo* t5563_ITIs[] = 
{
	&t603_TI,
	&t5562_TI,
	&t5564_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5563_0_0_0;
extern Il2CppType t5563_1_0_0;
struct t5563;
extern Il2CppGenericClass t5563_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5563_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5563_MIs, t5563_PIs, NULL, NULL, NULL, NULL, NULL, &t5563_TI, t5563_ITIs, NULL, &t1908__CustomAttributeCache, &t5563_TI, &t5563_0_0_0, &t5563_1_0_0, NULL, &t5563_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2847.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2847_TI;
#include "t2847MD.h"

#include "t2848.h"
extern TypeInfo t2848_TI;
#include "t2848MD.h"
extern MethodInfo m15471_MI;
extern MethodInfo m15473_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.GUIElement>
extern Il2CppType t316_0_0_33;
FieldInfo t2847_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2847_TI, offsetof(t2847, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2847_FIs[] =
{
	&t2847_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t455_0_0_0;
static ParameterInfo t2847_m15469_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t455_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15469_GM;
MethodInfo m15469_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2847_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2847_m15469_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15469_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2847_m15470_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15470_GM;
MethodInfo m15470_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2847_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2847_m15470_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15470_GM};
static MethodInfo* t2847_MIs[] =
{
	&m15469_MI,
	&m15470_MI,
	NULL
};
extern MethodInfo m15470_MI;
extern MethodInfo m15474_MI;
static MethodInfo* t2847_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15470_MI,
	&m15474_MI,
};
extern Il2CppType t2849_0_0_0;
extern TypeInfo t2849_TI;
extern MethodInfo m22072_MI;
extern TypeInfo t455_TI;
extern MethodInfo m15476_MI;
extern TypeInfo t455_TI;
static Il2CppRGCTXData t2847_RGCTXData[8] = 
{
	&t2849_0_0_0/* Type Usage */,
	&t2849_TI/* Class Usage */,
	&m22072_MI/* Method Usage */,
	&t455_TI/* Class Usage */,
	&m15476_MI/* Method Usage */,
	&m15471_MI/* Method Usage */,
	&t455_TI/* Class Usage */,
	&m15473_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2847_0_0_0;
extern Il2CppType t2847_1_0_0;
struct t2847;
extern Il2CppGenericClass t2847_GC;
TypeInfo t2847_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2847_MIs, NULL, t2847_FIs, NULL, &t2848_TI, NULL, NULL, &t2847_TI, NULL, t2847_VT, &EmptyCustomAttributesCache, &t2847_TI, &t2847_0_0_0, &t2847_1_0_0, NULL, &t2847_GC, NULL, NULL, NULL, t2847_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2847), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2849.h"
extern TypeInfo t2849_TI;
#include "t2849MD.h"
struct t556;
#define m22072(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.GUIElement>
extern Il2CppType t2849_0_0_1;
FieldInfo t2848_f0_FieldInfo = 
{
	"Delegate", &t2849_0_0_1, &t2848_TI, offsetof(t2848, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2848_FIs[] =
{
	&t2848_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2848_m15471_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15471_GM;
MethodInfo m15471_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2848_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2848_m15471_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15471_GM};
extern Il2CppType t2849_0_0_0;
static ParameterInfo t2848_m15472_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2849_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15472_GM;
MethodInfo m15472_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2848_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2848_m15472_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15472_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2848_m15473_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15473_GM;
MethodInfo m15473_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2848_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2848_m15473_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15473_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2848_m15474_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15474_GM;
MethodInfo m15474_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2848_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2848_m15474_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15474_GM};
static MethodInfo* t2848_MIs[] =
{
	&m15471_MI,
	&m15472_MI,
	&m15473_MI,
	&m15474_MI,
	NULL
};
static MethodInfo* t2848_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15473_MI,
	&m15474_MI,
};
extern TypeInfo t2849_TI;
extern TypeInfo t455_TI;
static Il2CppRGCTXData t2848_RGCTXData[5] = 
{
	&t2849_0_0_0/* Type Usage */,
	&t2849_TI/* Class Usage */,
	&m22072_MI/* Method Usage */,
	&t455_TI/* Class Usage */,
	&m15476_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2848_0_0_0;
extern Il2CppType t2848_1_0_0;
struct t2848;
extern Il2CppGenericClass t2848_GC;
TypeInfo t2848_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2848_MIs, NULL, t2848_FIs, NULL, &t556_TI, NULL, NULL, &t2848_TI, NULL, t2848_VT, &EmptyCustomAttributesCache, &t2848_TI, &t2848_0_0_0, &t2848_1_0_0, NULL, &t2848_GC, NULL, NULL, NULL, t2848_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2848), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.GUIElement>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2849_m15475_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15475_GM;
MethodInfo m15475_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2849_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2849_m15475_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15475_GM};
extern Il2CppType t455_0_0_0;
static ParameterInfo t2849_m15476_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t455_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15476_GM;
MethodInfo m15476_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2849_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2849_m15476_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15476_GM};
extern Il2CppType t455_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2849_m15477_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t455_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15477_GM;
MethodInfo m15477_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2849_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2849_m15477_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15477_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2849_m15478_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15478_GM;
MethodInfo m15478_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2849_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2849_m15478_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15478_GM};
static MethodInfo* t2849_MIs[] =
{
	&m15475_MI,
	&m15476_MI,
	&m15477_MI,
	&m15478_MI,
	NULL
};
extern MethodInfo m15477_MI;
extern MethodInfo m15478_MI;
static MethodInfo* t2849_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m15476_MI,
	&m15477_MI,
	&m15478_MI,
};
static Il2CppInterfaceOffsetPair t2849_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2849_1_0_0;
struct t2849;
extern Il2CppGenericClass t2849_GC;
TypeInfo t2849_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2849_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2849_TI, NULL, t2849_VT, &EmptyCustomAttributesCache, &t2849_TI, &t2849_0_0_0, &t2849_1_0_0, t2849_IOs, &t2849_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2849), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4349_TI;

#include "t456.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.GUILayer>
extern MethodInfo m29106_MI;
static PropertyInfo t4349____Current_PropertyInfo = 
{
	&t4349_TI, "Current", &m29106_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4349_PIs[] =
{
	&t4349____Current_PropertyInfo,
	NULL
};
extern Il2CppType t456_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29106_GM;
MethodInfo m29106_MI = 
{
	"get_Current", NULL, &t4349_TI, &t456_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29106_GM};
static MethodInfo* t4349_MIs[] =
{
	&m29106_MI,
	NULL
};
static TypeInfo* t4349_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4349_0_0_0;
extern Il2CppType t4349_1_0_0;
struct t4349;
extern Il2CppGenericClass t4349_GC;
TypeInfo t4349_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4349_MIs, t4349_PIs, NULL, NULL, NULL, NULL, NULL, &t4349_TI, t4349_ITIs, NULL, &EmptyCustomAttributesCache, &t4349_TI, &t4349_0_0_0, &t4349_1_0_0, NULL, &t4349_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2850.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2850_TI;
#include "t2850MD.h"

extern TypeInfo t456_TI;
extern MethodInfo m15483_MI;
extern MethodInfo m22074_MI;
struct t20;
#define m22074(__this, p0, method) (t456 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.GUILayer>
extern Il2CppType t20_0_0_1;
FieldInfo t2850_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2850_TI, offsetof(t2850, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2850_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2850_TI, offsetof(t2850, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2850_FIs[] =
{
	&t2850_f0_FieldInfo,
	&t2850_f1_FieldInfo,
	NULL
};
extern MethodInfo m15480_MI;
static PropertyInfo t2850____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2850_TI, "System.Collections.IEnumerator.Current", &m15480_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2850____Current_PropertyInfo = 
{
	&t2850_TI, "Current", &m15483_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2850_PIs[] =
{
	&t2850____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2850____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2850_m15479_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15479_GM;
MethodInfo m15479_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2850_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2850_m15479_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15479_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15480_GM;
MethodInfo m15480_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2850_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15480_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15481_GM;
MethodInfo m15481_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2850_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15481_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15482_GM;
MethodInfo m15482_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2850_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15482_GM};
extern Il2CppType t456_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15483_GM;
MethodInfo m15483_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2850_TI, &t456_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15483_GM};
static MethodInfo* t2850_MIs[] =
{
	&m15479_MI,
	&m15480_MI,
	&m15481_MI,
	&m15482_MI,
	&m15483_MI,
	NULL
};
extern MethodInfo m15482_MI;
extern MethodInfo m15481_MI;
static MethodInfo* t2850_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15480_MI,
	&m15482_MI,
	&m15481_MI,
	&m15483_MI,
};
static TypeInfo* t2850_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4349_TI,
};
static Il2CppInterfaceOffsetPair t2850_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4349_TI, 7},
};
extern TypeInfo t456_TI;
static Il2CppRGCTXData t2850_RGCTXData[3] = 
{
	&m15483_MI/* Method Usage */,
	&t456_TI/* Class Usage */,
	&m22074_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2850_0_0_0;
extern Il2CppType t2850_1_0_0;
extern Il2CppGenericClass t2850_GC;
TypeInfo t2850_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2850_MIs, t2850_PIs, t2850_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2850_TI, t2850_ITIs, t2850_VT, &EmptyCustomAttributesCache, &t2850_TI, &t2850_0_0_0, &t2850_1_0_0, t2850_IOs, &t2850_GC, NULL, NULL, NULL, t2850_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2850)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5565_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.GUILayer>
extern MethodInfo m29107_MI;
static PropertyInfo t5565____Count_PropertyInfo = 
{
	&t5565_TI, "Count", &m29107_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29108_MI;
static PropertyInfo t5565____IsReadOnly_PropertyInfo = 
{
	&t5565_TI, "IsReadOnly", &m29108_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5565_PIs[] =
{
	&t5565____Count_PropertyInfo,
	&t5565____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29107_GM;
MethodInfo m29107_MI = 
{
	"get_Count", NULL, &t5565_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29107_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29108_GM;
MethodInfo m29108_MI = 
{
	"get_IsReadOnly", NULL, &t5565_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29108_GM};
extern Il2CppType t456_0_0_0;
extern Il2CppType t456_0_0_0;
static ParameterInfo t5565_m29109_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t456_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29109_GM;
MethodInfo m29109_MI = 
{
	"Add", NULL, &t5565_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5565_m29109_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29109_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29110_GM;
MethodInfo m29110_MI = 
{
	"Clear", NULL, &t5565_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29110_GM};
extern Il2CppType t456_0_0_0;
static ParameterInfo t5565_m29111_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t456_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29111_GM;
MethodInfo m29111_MI = 
{
	"Contains", NULL, &t5565_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5565_m29111_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29111_GM};
extern Il2CppType t3743_0_0_0;
extern Il2CppType t3743_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5565_m29112_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3743_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29112_GM;
MethodInfo m29112_MI = 
{
	"CopyTo", NULL, &t5565_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5565_m29112_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29112_GM};
extern Il2CppType t456_0_0_0;
static ParameterInfo t5565_m29113_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t456_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29113_GM;
MethodInfo m29113_MI = 
{
	"Remove", NULL, &t5565_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5565_m29113_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29113_GM};
static MethodInfo* t5565_MIs[] =
{
	&m29107_MI,
	&m29108_MI,
	&m29109_MI,
	&m29110_MI,
	&m29111_MI,
	&m29112_MI,
	&m29113_MI,
	NULL
};
extern TypeInfo t5567_TI;
static TypeInfo* t5565_ITIs[] = 
{
	&t603_TI,
	&t5567_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5565_0_0_0;
extern Il2CppType t5565_1_0_0;
struct t5565;
extern Il2CppGenericClass t5565_GC;
TypeInfo t5565_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5565_MIs, t5565_PIs, NULL, NULL, NULL, NULL, NULL, &t5565_TI, t5565_ITIs, NULL, &EmptyCustomAttributesCache, &t5565_TI, &t5565_0_0_0, &t5565_1_0_0, NULL, &t5565_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.GUILayer>
extern Il2CppType t4349_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29114_GM;
MethodInfo m29114_MI = 
{
	"GetEnumerator", NULL, &t5567_TI, &t4349_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29114_GM};
static MethodInfo* t5567_MIs[] =
{
	&m29114_MI,
	NULL
};
static TypeInfo* t5567_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5567_0_0_0;
extern Il2CppType t5567_1_0_0;
struct t5567;
extern Il2CppGenericClass t5567_GC;
TypeInfo t5567_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5567_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5567_TI, t5567_ITIs, NULL, &EmptyCustomAttributesCache, &t5567_TI, &t5567_0_0_0, &t5567_1_0_0, NULL, &t5567_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5566_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.GUILayer>
extern MethodInfo m29115_MI;
extern MethodInfo m29116_MI;
static PropertyInfo t5566____Item_PropertyInfo = 
{
	&t5566_TI, "Item", &m29115_MI, &m29116_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5566_PIs[] =
{
	&t5566____Item_PropertyInfo,
	NULL
};
extern Il2CppType t456_0_0_0;
static ParameterInfo t5566_m29117_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t456_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29117_GM;
MethodInfo m29117_MI = 
{
	"IndexOf", NULL, &t5566_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5566_m29117_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29117_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t456_0_0_0;
static ParameterInfo t5566_m29118_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t456_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29118_GM;
MethodInfo m29118_MI = 
{
	"Insert", NULL, &t5566_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5566_m29118_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29118_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5566_m29119_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29119_GM;
MethodInfo m29119_MI = 
{
	"RemoveAt", NULL, &t5566_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5566_m29119_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29119_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5566_m29115_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t456_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29115_GM;
MethodInfo m29115_MI = 
{
	"get_Item", NULL, &t5566_TI, &t456_0_0_0, RuntimeInvoker_t29_t44, t5566_m29115_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29115_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t456_0_0_0;
static ParameterInfo t5566_m29116_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t456_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29116_GM;
MethodInfo m29116_MI = 
{
	"set_Item", NULL, &t5566_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5566_m29116_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29116_GM};
static MethodInfo* t5566_MIs[] =
{
	&m29117_MI,
	&m29118_MI,
	&m29119_MI,
	&m29115_MI,
	&m29116_MI,
	NULL
};
static TypeInfo* t5566_ITIs[] = 
{
	&t603_TI,
	&t5565_TI,
	&t5567_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5566_0_0_0;
extern Il2CppType t5566_1_0_0;
struct t5566;
extern Il2CppGenericClass t5566_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5566_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5566_MIs, t5566_PIs, NULL, NULL, NULL, NULL, NULL, &t5566_TI, t5566_ITIs, NULL, &t1908__CustomAttributeCache, &t5566_TI, &t5566_0_0_0, &t5566_1_0_0, NULL, &t5566_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2851.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2851_TI;
#include "t2851MD.h"

#include "t2852.h"
extern TypeInfo t2852_TI;
#include "t2852MD.h"
extern MethodInfo m15486_MI;
extern MethodInfo m15488_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.GUILayer>
extern Il2CppType t316_0_0_33;
FieldInfo t2851_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2851_TI, offsetof(t2851, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2851_FIs[] =
{
	&t2851_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t456_0_0_0;
static ParameterInfo t2851_m15484_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t456_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15484_GM;
MethodInfo m15484_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2851_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2851_m15484_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15484_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2851_m15485_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15485_GM;
MethodInfo m15485_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2851_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2851_m15485_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15485_GM};
static MethodInfo* t2851_MIs[] =
{
	&m15484_MI,
	&m15485_MI,
	NULL
};
extern MethodInfo m15485_MI;
extern MethodInfo m15489_MI;
static MethodInfo* t2851_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15485_MI,
	&m15489_MI,
};
extern Il2CppType t2853_0_0_0;
extern TypeInfo t2853_TI;
extern MethodInfo m22084_MI;
extern TypeInfo t456_TI;
extern MethodInfo m15491_MI;
extern TypeInfo t456_TI;
static Il2CppRGCTXData t2851_RGCTXData[8] = 
{
	&t2853_0_0_0/* Type Usage */,
	&t2853_TI/* Class Usage */,
	&m22084_MI/* Method Usage */,
	&t456_TI/* Class Usage */,
	&m15491_MI/* Method Usage */,
	&m15486_MI/* Method Usage */,
	&t456_TI/* Class Usage */,
	&m15488_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2851_0_0_0;
extern Il2CppType t2851_1_0_0;
struct t2851;
extern Il2CppGenericClass t2851_GC;
TypeInfo t2851_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2851_MIs, NULL, t2851_FIs, NULL, &t2852_TI, NULL, NULL, &t2851_TI, NULL, t2851_VT, &EmptyCustomAttributesCache, &t2851_TI, &t2851_0_0_0, &t2851_1_0_0, NULL, &t2851_GC, NULL, NULL, NULL, t2851_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2851), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2853.h"
extern TypeInfo t2853_TI;
#include "t2853MD.h"
struct t556;
#define m22084(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.GUILayer>
extern Il2CppType t2853_0_0_1;
FieldInfo t2852_f0_FieldInfo = 
{
	"Delegate", &t2853_0_0_1, &t2852_TI, offsetof(t2852, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2852_FIs[] =
{
	&t2852_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2852_m15486_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15486_GM;
MethodInfo m15486_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2852_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2852_m15486_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15486_GM};
extern Il2CppType t2853_0_0_0;
static ParameterInfo t2852_m15487_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2853_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15487_GM;
MethodInfo m15487_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2852_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2852_m15487_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15487_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2852_m15488_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15488_GM;
MethodInfo m15488_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2852_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2852_m15488_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15488_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2852_m15489_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15489_GM;
MethodInfo m15489_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2852_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2852_m15489_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15489_GM};
static MethodInfo* t2852_MIs[] =
{
	&m15486_MI,
	&m15487_MI,
	&m15488_MI,
	&m15489_MI,
	NULL
};
static MethodInfo* t2852_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15488_MI,
	&m15489_MI,
};
extern TypeInfo t2853_TI;
extern TypeInfo t456_TI;
static Il2CppRGCTXData t2852_RGCTXData[5] = 
{
	&t2853_0_0_0/* Type Usage */,
	&t2853_TI/* Class Usage */,
	&m22084_MI/* Method Usage */,
	&t456_TI/* Class Usage */,
	&m15491_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2852_0_0_0;
extern Il2CppType t2852_1_0_0;
struct t2852;
extern Il2CppGenericClass t2852_GC;
TypeInfo t2852_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2852_MIs, NULL, t2852_FIs, NULL, &t556_TI, NULL, NULL, &t2852_TI, NULL, t2852_VT, &EmptyCustomAttributesCache, &t2852_TI, &t2852_0_0_0, &t2852_1_0_0, NULL, &t2852_GC, NULL, NULL, NULL, t2852_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2852), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.GUILayer>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2853_m15490_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15490_GM;
MethodInfo m15490_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2853_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2853_m15490_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15490_GM};
extern Il2CppType t456_0_0_0;
static ParameterInfo t2853_m15491_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t456_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15491_GM;
MethodInfo m15491_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2853_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2853_m15491_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15491_GM};
extern Il2CppType t456_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2853_m15492_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t456_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15492_GM;
MethodInfo m15492_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2853_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2853_m15492_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15492_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2853_m15493_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15493_GM;
MethodInfo m15493_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2853_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2853_m15493_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15493_GM};
static MethodInfo* t2853_MIs[] =
{
	&m15490_MI,
	&m15491_MI,
	&m15492_MI,
	&m15493_MI,
	NULL
};
extern MethodInfo m15492_MI;
extern MethodInfo m15493_MI;
static MethodInfo* t2853_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m15491_MI,
	&m15492_MI,
	&m15493_MI,
};
static Il2CppInterfaceOffsetPair t2853_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2853_1_0_0;
struct t2853;
extern Il2CppGenericClass t2853_GC;
TypeInfo t2853_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2853_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2853_TI, NULL, t2853_VT, &EmptyCustomAttributesCache, &t2853_TI, &t2853_0_0_0, &t2853_1_0_0, t2853_IOs, &t2853_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2853), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4351_TI;

#include "t468.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.GUILayoutOption>
extern MethodInfo m29120_MI;
static PropertyInfo t4351____Current_PropertyInfo = 
{
	&t4351_TI, "Current", &m29120_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4351_PIs[] =
{
	&t4351____Current_PropertyInfo,
	NULL
};
extern Il2CppType t468_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29120_GM;
MethodInfo m29120_MI = 
{
	"get_Current", NULL, &t4351_TI, &t468_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29120_GM};
static MethodInfo* t4351_MIs[] =
{
	&m29120_MI,
	NULL
};
static TypeInfo* t4351_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4351_0_0_0;
extern Il2CppType t4351_1_0_0;
struct t4351;
extern Il2CppGenericClass t4351_GC;
TypeInfo t4351_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4351_MIs, t4351_PIs, NULL, NULL, NULL, NULL, NULL, &t4351_TI, t4351_ITIs, NULL, &EmptyCustomAttributesCache, &t4351_TI, &t4351_0_0_0, &t4351_1_0_0, NULL, &t4351_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2854.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2854_TI;
#include "t2854MD.h"

extern TypeInfo t468_TI;
extern MethodInfo m15498_MI;
extern MethodInfo m22086_MI;
struct t20;
#define m22086(__this, p0, method) (t468 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.GUILayoutOption>
extern Il2CppType t20_0_0_1;
FieldInfo t2854_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2854_TI, offsetof(t2854, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2854_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2854_TI, offsetof(t2854, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2854_FIs[] =
{
	&t2854_f0_FieldInfo,
	&t2854_f1_FieldInfo,
	NULL
};
extern MethodInfo m15495_MI;
static PropertyInfo t2854____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2854_TI, "System.Collections.IEnumerator.Current", &m15495_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2854____Current_PropertyInfo = 
{
	&t2854_TI, "Current", &m15498_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2854_PIs[] =
{
	&t2854____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2854____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2854_m15494_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15494_GM;
MethodInfo m15494_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2854_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2854_m15494_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15494_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15495_GM;
MethodInfo m15495_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2854_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15495_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15496_GM;
MethodInfo m15496_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2854_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15496_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15497_GM;
MethodInfo m15497_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2854_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15497_GM};
extern Il2CppType t468_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15498_GM;
MethodInfo m15498_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2854_TI, &t468_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15498_GM};
static MethodInfo* t2854_MIs[] =
{
	&m15494_MI,
	&m15495_MI,
	&m15496_MI,
	&m15497_MI,
	&m15498_MI,
	NULL
};
extern MethodInfo m15497_MI;
extern MethodInfo m15496_MI;
static MethodInfo* t2854_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15495_MI,
	&m15497_MI,
	&m15496_MI,
	&m15498_MI,
};
static TypeInfo* t2854_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4351_TI,
};
static Il2CppInterfaceOffsetPair t2854_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4351_TI, 7},
};
extern TypeInfo t468_TI;
static Il2CppRGCTXData t2854_RGCTXData[3] = 
{
	&m15498_MI/* Method Usage */,
	&t468_TI/* Class Usage */,
	&m22086_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2854_0_0_0;
extern Il2CppType t2854_1_0_0;
extern Il2CppGenericClass t2854_GC;
TypeInfo t2854_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2854_MIs, t2854_PIs, t2854_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2854_TI, t2854_ITIs, t2854_VT, &EmptyCustomAttributesCache, &t2854_TI, &t2854_0_0_0, &t2854_1_0_0, t2854_IOs, &t2854_GC, NULL, NULL, NULL, t2854_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2854)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5568_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.GUILayoutOption>
extern MethodInfo m29121_MI;
static PropertyInfo t5568____Count_PropertyInfo = 
{
	&t5568_TI, "Count", &m29121_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29122_MI;
static PropertyInfo t5568____IsReadOnly_PropertyInfo = 
{
	&t5568_TI, "IsReadOnly", &m29122_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5568_PIs[] =
{
	&t5568____Count_PropertyInfo,
	&t5568____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29121_GM;
MethodInfo m29121_MI = 
{
	"get_Count", NULL, &t5568_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29121_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29122_GM;
MethodInfo m29122_MI = 
{
	"get_IsReadOnly", NULL, &t5568_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29122_GM};
extern Il2CppType t468_0_0_0;
extern Il2CppType t468_0_0_0;
static ParameterInfo t5568_m29123_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t468_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29123_GM;
MethodInfo m29123_MI = 
{
	"Add", NULL, &t5568_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5568_m29123_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29123_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29124_GM;
MethodInfo m29124_MI = 
{
	"Clear", NULL, &t5568_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29124_GM};
extern Il2CppType t468_0_0_0;
static ParameterInfo t5568_m29125_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t468_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29125_GM;
MethodInfo m29125_MI = 
{
	"Contains", NULL, &t5568_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5568_m29125_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29125_GM};
extern Il2CppType t473_0_0_0;
extern Il2CppType t473_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5568_m29126_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t473_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29126_GM;
MethodInfo m29126_MI = 
{
	"CopyTo", NULL, &t5568_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5568_m29126_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29126_GM};
extern Il2CppType t468_0_0_0;
static ParameterInfo t5568_m29127_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t468_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29127_GM;
MethodInfo m29127_MI = 
{
	"Remove", NULL, &t5568_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5568_m29127_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29127_GM};
static MethodInfo* t5568_MIs[] =
{
	&m29121_MI,
	&m29122_MI,
	&m29123_MI,
	&m29124_MI,
	&m29125_MI,
	&m29126_MI,
	&m29127_MI,
	NULL
};
extern TypeInfo t5570_TI;
static TypeInfo* t5568_ITIs[] = 
{
	&t603_TI,
	&t5570_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5568_0_0_0;
extern Il2CppType t5568_1_0_0;
struct t5568;
extern Il2CppGenericClass t5568_GC;
TypeInfo t5568_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5568_MIs, t5568_PIs, NULL, NULL, NULL, NULL, NULL, &t5568_TI, t5568_ITIs, NULL, &EmptyCustomAttributesCache, &t5568_TI, &t5568_0_0_0, &t5568_1_0_0, NULL, &t5568_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.GUILayoutOption>
extern Il2CppType t4351_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29128_GM;
MethodInfo m29128_MI = 
{
	"GetEnumerator", NULL, &t5570_TI, &t4351_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29128_GM};
static MethodInfo* t5570_MIs[] =
{
	&m29128_MI,
	NULL
};
static TypeInfo* t5570_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5570_0_0_0;
extern Il2CppType t5570_1_0_0;
struct t5570;
extern Il2CppGenericClass t5570_GC;
TypeInfo t5570_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5570_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5570_TI, t5570_ITIs, NULL, &EmptyCustomAttributesCache, &t5570_TI, &t5570_0_0_0, &t5570_1_0_0, NULL, &t5570_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5569_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.GUILayoutOption>
extern MethodInfo m29129_MI;
extern MethodInfo m29130_MI;
static PropertyInfo t5569____Item_PropertyInfo = 
{
	&t5569_TI, "Item", &m29129_MI, &m29130_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5569_PIs[] =
{
	&t5569____Item_PropertyInfo,
	NULL
};
extern Il2CppType t468_0_0_0;
static ParameterInfo t5569_m29131_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t468_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29131_GM;
MethodInfo m29131_MI = 
{
	"IndexOf", NULL, &t5569_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5569_m29131_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29131_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t468_0_0_0;
static ParameterInfo t5569_m29132_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t468_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29132_GM;
MethodInfo m29132_MI = 
{
	"Insert", NULL, &t5569_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5569_m29132_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29132_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5569_m29133_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29133_GM;
MethodInfo m29133_MI = 
{
	"RemoveAt", NULL, &t5569_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5569_m29133_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29133_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5569_m29129_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t468_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29129_GM;
MethodInfo m29129_MI = 
{
	"get_Item", NULL, &t5569_TI, &t468_0_0_0, RuntimeInvoker_t29_t44, t5569_m29129_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29129_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t468_0_0_0;
static ParameterInfo t5569_m29130_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t468_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29130_GM;
MethodInfo m29130_MI = 
{
	"set_Item", NULL, &t5569_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5569_m29130_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29130_GM};
static MethodInfo* t5569_MIs[] =
{
	&m29131_MI,
	&m29132_MI,
	&m29133_MI,
	&m29129_MI,
	&m29130_MI,
	NULL
};
static TypeInfo* t5569_ITIs[] = 
{
	&t603_TI,
	&t5568_TI,
	&t5570_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5569_0_0_0;
extern Il2CppType t5569_1_0_0;
struct t5569;
extern Il2CppGenericClass t5569_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5569_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5569_MIs, t5569_PIs, NULL, NULL, NULL, NULL, NULL, &t5569_TI, t5569_ITIs, NULL, &t1908__CustomAttributeCache, &t5569_TI, &t5569_0_0_0, &t5569_1_0_0, NULL, &t5569_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t472.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t472_TI;
#include "t472MD.h"

#include "t469.h"
#include "t338.h"
#include "t1248.h"
#include "t1258.h"
#include "t2857.h"
#include "t733.h"
#include "t735.h"
#include "t2859.h"
#include "t725.h"
#include "t2856.h"
#include "t2867.h"
#include "t2861.h"
#include "t2868.h"
#include "t2361.h"
#include "t2869.h"
extern TypeInfo t44_TI;
extern TypeInfo t40_TI;
extern TypeInfo t469_TI;
extern TypeInfo t338_TI;
extern TypeInfo t2344_TI;
extern TypeInfo t1248_TI;
extern TypeInfo t1258_TI;
extern TypeInfo t2857_TI;
extern TypeInfo t2858_TI;
extern TypeInfo t2859_TI;
extern TypeInfo t3541_TI;
extern TypeInfo t725_TI;
extern TypeInfo t2856_TI;
extern TypeInfo t2867_TI;
extern TypeInfo t2861_TI;
extern TypeInfo t2868_TI;
extern TypeInfo t915_TI;
extern TypeInfo t2361_TI;
extern TypeInfo t841_TI;
extern TypeInfo t1965_TI;
extern TypeInfo t2855_TI;
extern TypeInfo t719_TI;
extern TypeInfo t2869_TI;
extern TypeInfo t6710_TI;
extern TypeInfo t7_TI;
#include "t338MD.h"
#include "t1258MD.h"
#include "t2857MD.h"
#include "t29MD.h"
#include "t2859MD.h"
#include "t2856MD.h"
#include "t2867MD.h"
#include "t2861MD.h"
#include "t2868MD.h"
#include "t915MD.h"
#include "t2361MD.h"
#include "t719MD.h"
#include "t2869MD.h"
#include "t733MD.h"
#include "t7MD.h"
#include "t725MD.h"
extern Il2CppType t2344_0_0_0;
extern Il2CppType t2858_0_0_0;
extern Il2CppType t469_0_0_0;
extern MethodInfo m15528_MI;
extern MethodInfo m15534_MI;
extern MethodInfo m15518_MI;
extern MethodInfo m15535_MI;
extern MethodInfo m2848_MI;
extern MethodInfo m2950_MI;
extern MethodInfo m27289_MI;
extern MethodInfo m27290_MI;
extern MethodInfo m6569_MI;
extern MethodInfo m15525_MI;
extern MethodInfo m15555_MI;
extern MethodInfo m1331_MI;
extern MethodInfo m15519_MI;
extern MethodInfo m15526_MI;
extern MethodInfo m15532_MI;
extern MethodInfo m15540_MI;
extern MethodInfo m15542_MI;
extern MethodInfo m15536_MI;
extern MethodInfo m15524_MI;
extern MethodInfo m15521_MI;
extern MethodInfo m15538_MI;
extern MethodInfo m15590_MI;
extern MethodInfo m22121_MI;
extern MethodInfo m15522_MI;
extern MethodInfo m15594_MI;
extern MethodInfo m22123_MI;
extern MethodInfo m15574_MI;
extern MethodInfo m15598_MI;
extern MethodInfo m3975_MI;
extern MethodInfo m12170_MI;
extern MethodInfo m15520_MI;
extern MethodInfo m15517_MI;
extern MethodInfo m15539_MI;
extern MethodInfo m22124_MI;
extern MethodInfo m6736_MI;
extern MethodInfo m5952_MI;
extern MethodInfo m5112_MI;
extern MethodInfo m15608_MI;
extern MethodInfo m29134_MI;
extern MethodInfo m3984_MI;
extern MethodInfo m3997_MI;
extern MethodInfo m3996_MI;
extern MethodInfo m3985_MI;
extern MethodInfo m6035_MI;
extern MethodInfo m66_MI;
extern MethodInfo m3973_MI;
extern MethodInfo m6004_MI;
extern MethodInfo m2847_MI;
extern MethodInfo m29135_MI;
extern MethodInfo m3965_MI;
struct t472;
 void m22121 (t472 * __this, t3541* p0, int32_t p1, t2856 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t472;
#include "t295.h"
 void m22123 (t472 * __this, t20 * p0, int32_t p1, t2867 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t472;
 void m22124 (t472 * __this, t2858* p0, int32_t p1, t2867 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m2846_MI;
 void m2846 (t472 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		m15519(__this, ((int32_t)10), (t29*)NULL, &m15519_MI);
		return;
	}
}
extern MethodInfo m15499_MI;
 void m15499 (t472 * __this, t29* p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		m15519(__this, ((int32_t)10), p0, &m15519_MI);
		return;
	}
}
extern MethodInfo m15500_MI;
 void m15500 (t472 * __this, int32_t p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		m15519(__this, p0, (t29*)NULL, &m15519_MI);
		return;
	}
}
extern MethodInfo m15501_MI;
 void m15501 (t472 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		__this->f13 = p0;
		return;
	}
}
extern MethodInfo m15502_MI;
 t29 * m15502 (t472 * __this, t29 * p0, MethodInfo* method){
	{
		if (!((t29 *)IsInst(p0, InitializedTypeInfo(&t44_TI))))
		{
			goto IL_0029;
		}
	}
	{
		bool L_0 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(&m15528_MI, __this, ((*(int32_t*)((int32_t*)UnBox (p0, InitializedTypeInfo(&t44_TI))))));
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		int32_t L_1 = m15534(__this, p0, &m15534_MI);
		t469 * L_2 = (t469 *)VirtFuncInvoker1< t469 *, int32_t >::Invoke(&m15518_MI, __this, L_1);
		t469 * L_3 = L_2;
		return ((t469 *)L_3);
	}

IL_0029:
	{
		return NULL;
	}
}
extern MethodInfo m15503_MI;
 void m15503 (t472 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	{
		int32_t L_0 = m15534(__this, p0, &m15534_MI);
		t469 * L_1 = m15535(__this, p1, &m15535_MI);
		VirtActionInvoker2< int32_t, t469 * >::Invoke(&m2848_MI, __this, L_0, L_1);
		return;
	}
}
extern MethodInfo m15504_MI;
 void m15504 (t472 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	{
		int32_t L_0 = m15534(__this, p0, &m15534_MI);
		t469 * L_1 = m15535(__this, p1, &m15535_MI);
		VirtActionInvoker2< int32_t, t469 * >::Invoke(&m15526_MI, __this, L_0, L_1);
		return;
	}
}
extern MethodInfo m15505_MI;
 void m15505 (t472 * __this, t29 * p0, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		if (!((t29 *)IsInst(p0, InitializedTypeInfo(&t44_TI))))
		{
			goto IL_0023;
		}
	}
	{
		VirtFuncInvoker1< bool, int32_t >::Invoke(&m15532_MI, __this, ((*(int32_t*)((int32_t*)UnBox (p0, InitializedTypeInfo(&t44_TI))))));
	}

IL_0023:
	{
		return;
	}
}
extern MethodInfo m15506_MI;
 bool m15506 (t472 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m15507_MI;
 t29 * m15507 (t472 * __this, MethodInfo* method){
	{
		return __this;
	}
}
extern MethodInfo m15508_MI;
 bool m15508 (t472 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m15509_MI;
 void m15509 (t472 * __this, t2859  p0, MethodInfo* method){
	{
		int32_t L_0 = m15540((&p0), &m15540_MI);
		t469 * L_1 = m15542((&p0), &m15542_MI);
		VirtActionInvoker2< int32_t, t469 * >::Invoke(&m15526_MI, __this, L_0, L_1);
		return;
	}
}
extern MethodInfo m15510_MI;
 bool m15510 (t472 * __this, t2859  p0, MethodInfo* method){
	{
		bool L_0 = m15536(__this, p0, &m15536_MI);
		return L_0;
	}
}
extern MethodInfo m15511_MI;
 void m15511 (t472 * __this, t2858* p0, int32_t p1, MethodInfo* method){
	{
		m15524(__this, p0, p1, &m15524_MI);
		return;
	}
}
extern MethodInfo m15512_MI;
 bool m15512 (t472 * __this, t2859  p0, MethodInfo* method){
	{
		bool L_0 = m15536(__this, p0, &m15536_MI);
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return 0;
	}

IL_000b:
	{
		int32_t L_1 = m15540((&p0), &m15540_MI);
		bool L_2 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(&m15532_MI, __this, L_1);
		return L_2;
	}
}
extern MethodInfo m15513_MI;
 void m15513 (t472 * __this, t20 * p0, int32_t p1, MethodInfo* method){
	t2858* V_0 = {0};
	t3541* V_1 = {0};
	int32_t G_B5_0 = 0;
	t3541* G_B5_1 = {0};
	t472 * G_B5_2 = {0};
	int32_t G_B4_0 = 0;
	t3541* G_B4_1 = {0};
	t472 * G_B4_2 = {0};
	{
		V_0 = ((t2858*)IsInst(p0, InitializedTypeInfo(&t2858_TI)));
		if (!V_0)
		{
			goto IL_0013;
		}
	}
	{
		m15524(__this, V_0, p1, &m15524_MI);
		return;
	}

IL_0013:
	{
		m15521(__this, p0, p1, &m15521_MI);
		V_1 = ((t3541*)IsInst(p0, InitializedTypeInfo(&t3541_TI)));
		if (!V_1)
		{
			goto IL_004b;
		}
	}
	{
		G_B4_0 = p1;
		G_B4_1 = V_1;
		G_B4_2 = ((t472 *)(__this));
		if ((((t472_SFs*)InitializedTypeInfo(&t472_TI)->static_fields)->f15))
		{
			G_B5_0 = p1;
			G_B5_1 = V_1;
			G_B5_2 = ((t472 *)(__this));
			goto IL_0040;
		}
	}
	{
		t35 L_0 = { &m15538_MI };
		t2856 * L_1 = (t2856 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2856_TI));
		m15590(L_1, NULL, L_0, &m15590_MI);
		((t472_SFs*)InitializedTypeInfo(&t472_TI)->static_fields)->f15 = L_1;
		G_B5_0 = G_B4_0;
		G_B5_1 = G_B4_1;
		G_B5_2 = ((t472 *)(G_B4_2));
	}

IL_0040:
	{
		m22121(G_B5_2, G_B5_1, G_B5_0, (((t472_SFs*)InitializedTypeInfo(&t472_TI)->static_fields)->f15), &m22121_MI);
		return;
	}

IL_004b:
	{
		t35 L_2 = { &m15522_MI };
		t2867 * L_3 = (t2867 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2867_TI));
		m15594(L_3, NULL, L_2, &m15594_MI);
		m22123(__this, p0, p1, L_3, &m22123_MI);
		return;
	}
}
extern MethodInfo m15514_MI;
 t29 * m15514 (t472 * __this, MethodInfo* method){
	{
		t2861  L_0 = {0};
		m15574(&L_0, __this, &m15574_MI);
		t2861  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2861_TI), &L_1);
		return (t29 *)L_2;
	}
}
extern MethodInfo m15515_MI;
 t29* m15515 (t472 * __this, MethodInfo* method){
	{
		t2861  L_0 = {0};
		m15574(&L_0, __this, &m15574_MI);
		t2861  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2861_TI), &L_1);
		return (t29*)L_2;
	}
}
extern MethodInfo m15516_MI;
 t29 * m15516 (t472 * __this, MethodInfo* method){
	{
		t2868 * L_0 = (t2868 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2868_TI));
		m15598(L_0, __this, &m15598_MI);
		return L_0;
	}
}
 int32_t m15517 (t472 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f10);
		return L_0;
	}
}
 t469 * m15518 (t472 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t44_TI), &L_0);
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_2 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_2, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0013:
	{
		t29* L_3 = (__this->f12);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(&m27289_MI, L_3, p0);
		V_0 = ((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		t841* L_5 = (__this->f4);
		t841* L_6 = (__this->f4);
		int32_t L_7 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_6)->max_length)))));
		V_1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_7))-1));
		goto IL_008f;
	}

IL_0042:
	{
		t1965* L_8 = (__this->f5);
		int32_t L_9 = (((t1248 *)(t1248 *)SZArrayLdElema(L_8, V_1))->f0);
		if ((((uint32_t)L_9) != ((uint32_t)V_0)))
		{
			goto IL_007d;
		}
	}
	{
		t29* L_10 = (__this->f12);
		t841* L_11 = (__this->f6);
		int32_t L_12 = V_1;
		bool L_13 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(&m27290_MI, L_10, (*(int32_t*)(int32_t*)SZArrayLdElema(L_11, L_12)), p0);
		if (!L_13)
		{
			goto IL_007d;
		}
	}
	{
		t2855* L_14 = (__this->f7);
		int32_t L_15 = V_1;
		return (*(t469 **)(t469 **)SZArrayLdElema(L_14, L_15));
	}

IL_007d:
	{
		t1965* L_16 = (__this->f5);
		int32_t L_17 = (((t1248 *)(t1248 *)SZArrayLdElema(L_16, V_1))->f1);
		V_1 = L_17;
	}

IL_008f:
	{
		if ((((uint32_t)V_1) != ((uint32_t)(-1))))
		{
			goto IL_0042;
		}
	}
	{
		t1258 * L_18 = (t1258 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1258_TI));
		m6569(L_18, &m6569_MI);
		il2cpp_codegen_raise_exception(L_18);
	}
}
 void m2848 (t472 * __this, int32_t p0, t469 * p1, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		int32_t L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t44_TI), &L_0);
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_2 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_2, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0013:
	{
		t29* L_3 = (__this->f12);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(&m27289_MI, L_3, p0);
		V_0 = ((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		t841* L_5 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_5)->max_length)))));
		t841* L_6 = (__this->f4);
		int32_t L_7 = V_1;
		V_2 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_6, L_7))-1));
		V_3 = (-1);
		if ((((int32_t)V_2) == ((int32_t)(-1))))
		{
			goto IL_0090;
		}
	}

IL_0048:
	{
		t1965* L_8 = (__this->f5);
		int32_t L_9 = (((t1248 *)(t1248 *)SZArrayLdElema(L_8, V_2))->f0);
		if ((((uint32_t)L_9) != ((uint32_t)V_0)))
		{
			goto IL_0078;
		}
	}
	{
		t29* L_10 = (__this->f12);
		t841* L_11 = (__this->f6);
		int32_t L_12 = V_2;
		bool L_13 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(&m27290_MI, L_10, (*(int32_t*)(int32_t*)SZArrayLdElema(L_11, L_12)), p0);
		if (!L_13)
		{
			goto IL_0078;
		}
	}
	{
		goto IL_0090;
	}

IL_0078:
	{
		V_3 = V_2;
		t1965* L_14 = (__this->f5);
		int32_t L_15 = (((t1248 *)(t1248 *)SZArrayLdElema(L_14, V_2))->f1);
		V_2 = L_15;
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0048;
		}
	}

IL_0090:
	{
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0148;
		}
	}
	{
		int32_t L_16 = (__this->f10);
		int32_t L_17 = ((int32_t)(L_16+1));
		V_4 = L_17;
		__this->f10 = L_17;
		int32_t L_18 = (__this->f11);
		if ((((int32_t)V_4) <= ((int32_t)L_18)))
		{
			goto IL_00c9;
		}
	}
	{
		m15525(__this, &m15525_MI);
		t841* L_19 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_19)->max_length)))));
	}

IL_00c9:
	{
		int32_t L_20 = (__this->f9);
		V_2 = L_20;
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_00ea;
		}
	}
	{
		int32_t L_21 = (__this->f8);
		int32_t L_22 = L_21;
		V_4 = L_22;
		__this->f8 = ((int32_t)(L_22+1));
		V_2 = V_4;
		goto IL_0101;
	}

IL_00ea:
	{
		t1965* L_23 = (__this->f5);
		int32_t L_24 = (((t1248 *)(t1248 *)SZArrayLdElema(L_23, V_2))->f1);
		__this->f9 = L_24;
	}

IL_0101:
	{
		t1965* L_25 = (__this->f5);
		t841* L_26 = (__this->f4);
		int32_t L_27 = V_1;
		((t1248 *)(t1248 *)SZArrayLdElema(L_25, V_2))->f1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_26, L_27))-1));
		t841* L_28 = (__this->f4);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_28, V_1)) = (int32_t)((int32_t)(V_2+1));
		t1965* L_29 = (__this->f5);
		((t1248 *)(t1248 *)SZArrayLdElema(L_29, V_2))->f0 = V_0;
		t841* L_30 = (__this->f6);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_30, V_2)) = (int32_t)p0;
		goto IL_0194;
	}

IL_0148:
	{
		if ((((int32_t)V_3) == ((int32_t)(-1))))
		{
			goto IL_0194;
		}
	}
	{
		t1965* L_31 = (__this->f5);
		t1965* L_32 = (__this->f5);
		int32_t L_33 = (((t1248 *)(t1248 *)SZArrayLdElema(L_32, V_2))->f1);
		((t1248 *)(t1248 *)SZArrayLdElema(L_31, V_3))->f1 = L_33;
		t1965* L_34 = (__this->f5);
		t841* L_35 = (__this->f4);
		int32_t L_36 = V_1;
		((t1248 *)(t1248 *)SZArrayLdElema(L_34, V_2))->f1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_35, L_36))-1));
		t841* L_37 = (__this->f4);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_37, V_1)) = (int32_t)((int32_t)(V_2+1));
	}

IL_0194:
	{
		t2855* L_38 = (__this->f7);
		*((t469 **)(t469 **)SZArrayLdElema(L_38, V_2)) = (t469 *)p1;
		int32_t L_39 = (__this->f14);
		__this->f14 = ((int32_t)(L_39+1));
		return;
	}
}
 void m15519 (t472 * __this, int32_t p0, t29* p1, MethodInfo* method){
	t29* V_0 = {0};
	t472 * G_B4_0 = {0};
	t472 * G_B3_0 = {0};
	t29* G_B5_0 = {0};
	t472 * G_B5_1 = {0};
	{
		if ((((int32_t)p0) >= ((int32_t)0)))
		{
			goto IL_000f;
		}
	}
	{
		t915 * L_0 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_0, (t7*) &_stringLiteral1163, &m3975_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000f:
	{
		G_B3_0 = ((t472 *)(__this));
		if (!p1)
		{
			G_B4_0 = ((t472 *)(__this));
			goto IL_0018;
		}
	}
	{
		V_0 = p1;
		G_B5_0 = V_0;
		G_B5_1 = ((t472 *)(G_B3_0));
		goto IL_001d;
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t2361_TI));
		t2361 * L_1 = m12170(NULL, &m12170_MI);
		G_B5_0 = ((t29*)(L_1));
		G_B5_1 = ((t472 *)(G_B4_0));
	}

IL_001d:
	{
		G_B5_1->f12 = G_B5_0;
		if (p0)
		{
			goto IL_002b;
		}
	}
	{
		p0 = ((int32_t)10);
	}

IL_002b:
	{
		p0 = ((int32_t)((((int32_t)((float)((float)(((float)p0))/(float)(0.9f)))))+1));
		m15520(__this, p0, &m15520_MI);
		__this->f14 = 0;
		return;
	}
}
 void m15520 (t472 * __this, int32_t p0, MethodInfo* method){
	{
		__this->f4 = ((t841*)SZArrayNew(InitializedTypeInfo(&t841_TI), p0));
		__this->f5 = ((t1965*)SZArrayNew(InitializedTypeInfo(&t1965_TI), p0));
		__this->f9 = (-1);
		__this->f6 = ((t841*)SZArrayNew(InitializedTypeInfo(&t841_TI), p0));
		__this->f7 = ((t2855*)SZArrayNew(InitializedTypeInfo(&t2855_TI), p0));
		__this->f8 = 0;
		t841* L_0 = (__this->f4);
		__this->f11 = (((int32_t)((float)((float)(((float)(((int32_t)(((t20 *)L_0)->max_length)))))*(float)(0.9f)))));
		int32_t L_1 = (__this->f11);
		if (L_1)
		{
			goto IL_006e;
		}
	}
	{
		t841* L_2 = (__this->f4);
		if ((((int32_t)(((int32_t)(((t20 *)L_2)->max_length)))) <= ((int32_t)0)))
		{
			goto IL_006e;
		}
	}
	{
		__this->f11 = 1;
	}

IL_006e:
	{
		return;
	}
}
 void m15521 (t472 * __this, t20 * p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_001d:
	{
		int32_t L_2 = m3969(p0, &m3969_MI);
		if ((((int32_t)p1) <= ((int32_t)L_2)))
		{
			goto IL_0031;
		}
	}
	{
		t305 * L_3 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_3, (t7*) &_stringLiteral1164, &m1935_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0031:
	{
		int32_t L_4 = m3969(p0, &m3969_MI);
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m15517_MI, __this);
		if ((((int32_t)((int32_t)(L_4-p1))) >= ((int32_t)L_5)))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_6 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_6, (t7*) &_stringLiteral1165, &m1935_MI);
		il2cpp_codegen_raise_exception(L_6);
	}

IL_004c:
	{
		return;
	}
}
 t2859  m15522 (t29 * __this, int32_t p0, t469 * p1, MethodInfo* method){
	{
		t2859  L_0 = {0};
		m15539(&L_0, p0, p1, &m15539_MI);
		return L_0;
	}
}
extern MethodInfo m15523_MI;
 t469 * m15523 (t29 * __this, int32_t p0, t469 * p1, MethodInfo* method){
	{
		return p1;
	}
}
 void m15524 (t472 * __this, t2858* p0, int32_t p1, MethodInfo* method){
	{
		m15521(__this, (t20 *)(t20 *)p0, p1, &m15521_MI);
		t35 L_0 = { &m15522_MI };
		t2867 * L_1 = (t2867 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2867_TI));
		m15594(L_1, NULL, L_0, &m15594_MI);
		m22124(__this, p0, p1, L_1, &m22124_MI);
		return;
	}
}
 void m15525 (t472 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	t841* V_1 = {0};
	t1965* V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	t841* V_7 = {0};
	t2855* V_8 = {0};
	int32_t V_9 = 0;
	{
		t841* L_0 = (__this->f4);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t719_TI));
		int32_t L_1 = m6736(NULL, ((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)(((t20 *)L_0)->max_length)))<<(int32_t)1))|(int32_t)1)), &m6736_MI);
		V_0 = L_1;
		V_1 = ((t841*)SZArrayNew(InitializedTypeInfo(&t841_TI), V_0));
		V_2 = ((t1965*)SZArrayNew(InitializedTypeInfo(&t1965_TI), V_0));
		V_3 = 0;
		goto IL_00ab;
	}

IL_0027:
	{
		t841* L_2 = (__this->f4);
		int32_t L_3 = V_3;
		V_4 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_2, L_3))-1));
		goto IL_00a2;
	}

IL_0035:
	{
		t29* L_4 = (__this->f12);
		t841* L_5 = (__this->f6);
		int32_t L_6 = V_4;
		int32_t L_7 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(&m27289_MI, L_4, (*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_6)));
		int32_t L_8 = ((int32_t)((int32_t)L_7|(int32_t)((int32_t)-2147483648)));
		V_9 = L_8;
		((t1248 *)(t1248 *)SZArrayLdElema(V_2, V_4))->f0 = L_8;
		V_5 = V_9;
		V_6 = ((int32_t)(((int32_t)((int32_t)V_5&(int32_t)((int32_t)2147483647)))%V_0));
		int32_t L_9 = V_6;
		((t1248 *)(t1248 *)SZArrayLdElema(V_2, V_4))->f1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(V_1, L_9))-1));
		*((int32_t*)(int32_t*)SZArrayLdElema(V_1, V_6)) = (int32_t)((int32_t)(V_4+1));
		t1965* L_10 = (__this->f5);
		int32_t L_11 = (((t1248 *)(t1248 *)SZArrayLdElema(L_10, V_4))->f1);
		V_4 = L_11;
	}

IL_00a2:
	{
		if ((((uint32_t)V_4) != ((uint32_t)(-1))))
		{
			goto IL_0035;
		}
	}
	{
		V_3 = ((int32_t)(V_3+1));
	}

IL_00ab:
	{
		t841* L_12 = (__this->f4);
		if ((((int32_t)V_3) < ((int32_t)(((int32_t)(((t20 *)L_12)->max_length))))))
		{
			goto IL_0027;
		}
	}
	{
		__this->f4 = V_1;
		__this->f5 = V_2;
		V_7 = ((t841*)SZArrayNew(InitializedTypeInfo(&t841_TI), V_0));
		V_8 = ((t2855*)SZArrayNew(InitializedTypeInfo(&t2855_TI), V_0));
		t841* L_13 = (__this->f6);
		int32_t L_14 = (__this->f8);
		m5952(NULL, (t20 *)(t20 *)L_13, 0, (t20 *)(t20 *)V_7, 0, L_14, &m5952_MI);
		t2855* L_15 = (__this->f7);
		int32_t L_16 = (__this->f8);
		m5952(NULL, (t20 *)(t20 *)L_15, 0, (t20 *)(t20 *)V_8, 0, L_16, &m5952_MI);
		__this->f6 = V_7;
		__this->f7 = V_8;
		__this->f11 = (((int32_t)((float)((float)(((float)V_0))*(float)(0.9f)))));
		return;
	}
}
 void m15526 (t472 * __this, int32_t p0, t469 * p1, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		int32_t L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t44_TI), &L_0);
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_2 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_2, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0013:
	{
		t29* L_3 = (__this->f12);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(&m27289_MI, L_3, p0);
		V_0 = ((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		t841* L_5 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_5)->max_length)))));
		t841* L_6 = (__this->f4);
		int32_t L_7 = V_1;
		V_2 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_6, L_7))-1));
		goto IL_008f;
	}

IL_0044:
	{
		t1965* L_8 = (__this->f5);
		int32_t L_9 = (((t1248 *)(t1248 *)SZArrayLdElema(L_8, V_2))->f0);
		if ((((uint32_t)L_9) != ((uint32_t)V_0)))
		{
			goto IL_007d;
		}
	}
	{
		t29* L_10 = (__this->f12);
		t841* L_11 = (__this->f6);
		int32_t L_12 = V_2;
		bool L_13 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(&m27290_MI, L_10, (*(int32_t*)(int32_t*)SZArrayLdElema(L_11, L_12)), p0);
		if (!L_13)
		{
			goto IL_007d;
		}
	}
	{
		t305 * L_14 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_14, (t7*) &_stringLiteral1167, &m1935_MI);
		il2cpp_codegen_raise_exception(L_14);
	}

IL_007d:
	{
		t1965* L_15 = (__this->f5);
		int32_t L_16 = (((t1248 *)(t1248 *)SZArrayLdElema(L_15, V_2))->f1);
		V_2 = L_16;
	}

IL_008f:
	{
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0044;
		}
	}
	{
		int32_t L_17 = (__this->f10);
		int32_t L_18 = ((int32_t)(L_17+1));
		V_3 = L_18;
		__this->f10 = L_18;
		int32_t L_19 = (__this->f11);
		if ((((int32_t)V_3) <= ((int32_t)L_19)))
		{
			goto IL_00c3;
		}
	}
	{
		m15525(__this, &m15525_MI);
		t841* L_20 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_20)->max_length)))));
	}

IL_00c3:
	{
		int32_t L_21 = (__this->f9);
		V_2 = L_21;
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_00e2;
		}
	}
	{
		int32_t L_22 = (__this->f8);
		int32_t L_23 = L_22;
		V_3 = L_23;
		__this->f8 = ((int32_t)(L_23+1));
		V_2 = V_3;
		goto IL_00f9;
	}

IL_00e2:
	{
		t1965* L_24 = (__this->f5);
		int32_t L_25 = (((t1248 *)(t1248 *)SZArrayLdElema(L_24, V_2))->f1);
		__this->f9 = L_25;
	}

IL_00f9:
	{
		t1965* L_26 = (__this->f5);
		((t1248 *)(t1248 *)SZArrayLdElema(L_26, V_2))->f0 = V_0;
		t1965* L_27 = (__this->f5);
		t841* L_28 = (__this->f4);
		int32_t L_29 = V_1;
		((t1248 *)(t1248 *)SZArrayLdElema(L_27, V_2))->f1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_28, L_29))-1));
		t841* L_30 = (__this->f4);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_30, V_1)) = (int32_t)((int32_t)(V_2+1));
		t841* L_31 = (__this->f6);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_31, V_2)) = (int32_t)p0;
		t2855* L_32 = (__this->f7);
		*((t469 **)(t469 **)SZArrayLdElema(L_32, V_2)) = (t469 *)p1;
		int32_t L_33 = (__this->f14);
		__this->f14 = ((int32_t)(L_33+1));
		return;
	}
}
extern MethodInfo m15527_MI;
 void m15527 (t472 * __this, MethodInfo* method){
	{
		__this->f10 = 0;
		t841* L_0 = (__this->f4);
		t841* L_1 = (__this->f4);
		m5112(NULL, (t20 *)(t20 *)L_0, 0, (((int32_t)(((t20 *)L_1)->max_length))), &m5112_MI);
		t841* L_2 = (__this->f6);
		t841* L_3 = (__this->f6);
		m5112(NULL, (t20 *)(t20 *)L_2, 0, (((int32_t)(((t20 *)L_3)->max_length))), &m5112_MI);
		t2855* L_4 = (__this->f7);
		t2855* L_5 = (__this->f7);
		m5112(NULL, (t20 *)(t20 *)L_4, 0, (((int32_t)(((t20 *)L_5)->max_length))), &m5112_MI);
		t1965* L_6 = (__this->f5);
		t1965* L_7 = (__this->f5);
		m5112(NULL, (t20 *)(t20 *)L_6, 0, (((int32_t)(((t20 *)L_7)->max_length))), &m5112_MI);
		__this->f9 = (-1);
		__this->f8 = 0;
		int32_t L_8 = (__this->f14);
		__this->f14 = ((int32_t)(L_8+1));
		return;
	}
}
 bool m15528 (t472 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t44_TI), &L_0);
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_2 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_2, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0013:
	{
		t29* L_3 = (__this->f12);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(&m27289_MI, L_3, p0);
		V_0 = ((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		t841* L_5 = (__this->f4);
		t841* L_6 = (__this->f4);
		int32_t L_7 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_6)->max_length)))));
		V_1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_7))-1));
		goto IL_0084;
	}

IL_0042:
	{
		t1965* L_8 = (__this->f5);
		int32_t L_9 = (((t1248 *)(t1248 *)SZArrayLdElema(L_8, V_1))->f0);
		if ((((uint32_t)L_9) != ((uint32_t)V_0)))
		{
			goto IL_0072;
		}
	}
	{
		t29* L_10 = (__this->f12);
		t841* L_11 = (__this->f6);
		int32_t L_12 = V_1;
		bool L_13 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(&m27290_MI, L_10, (*(int32_t*)(int32_t*)SZArrayLdElema(L_11, L_12)), p0);
		if (!L_13)
		{
			goto IL_0072;
		}
	}
	{
		return 1;
	}

IL_0072:
	{
		t1965* L_14 = (__this->f5);
		int32_t L_15 = (((t1248 *)(t1248 *)SZArrayLdElema(L_14, V_1))->f1);
		V_1 = L_15;
	}

IL_0084:
	{
		if ((((uint32_t)V_1) != ((uint32_t)(-1))))
		{
			goto IL_0042;
		}
	}
	{
		return 0;
	}
}
extern MethodInfo m15529_MI;
 bool m15529 (t472 * __this, t469 * p0, MethodInfo* method){
	t29* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t2869_TI));
		t2869 * L_0 = m15608(NULL, &m15608_MI);
		V_0 = L_0;
		V_1 = 0;
		goto IL_0048;
	}

IL_000a:
	{
		t841* L_1 = (__this->f4);
		int32_t L_2 = V_1;
		V_2 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_1, L_2))-1));
		goto IL_0040;
	}

IL_0017:
	{
		t2855* L_3 = (__this->f7);
		int32_t L_4 = V_2;
		bool L_5 = (bool)InterfaceFuncInvoker2< bool, t469 *, t469 * >::Invoke(&m29134_MI, V_0, (*(t469 **)(t469 **)SZArrayLdElema(L_3, L_4)), p0);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		return 1;
	}

IL_002e:
	{
		t1965* L_6 = (__this->f5);
		int32_t L_7 = (((t1248 *)(t1248 *)SZArrayLdElema(L_6, V_2))->f1);
		V_2 = L_7;
	}

IL_0040:
	{
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0017;
		}
	}
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0048:
	{
		t841* L_8 = (__this->f4);
		if ((((int32_t)V_1) < ((int32_t)(((int32_t)(((t20 *)L_8)->max_length))))))
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}
}
extern MethodInfo m15530_MI;
 void m15530 (t472 * __this, t733 * p0, t735  p1, MethodInfo* method){
	t2858* V_0 = {0};
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral206, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = (__this->f14);
		m3984(p0, (t7*) &_stringLiteral208, L_1, &m3984_MI);
		t29* L_2 = (__this->f12);
		m3997(p0, (t7*) &_stringLiteral210, L_2, &m3997_MI);
		V_0 = (t2858*)NULL;
		int32_t L_3 = (__this->f10);
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_4 = (__this->f10);
		V_0 = ((t2858*)SZArrayNew(InitializedTypeInfo(&t2858_TI), L_4));
		m15524(__this, V_0, 0, &m15524_MI);
	}

IL_004f:
	{
		t841* L_5 = (__this->f4);
		m3984(p0, (t7*) &_stringLiteral1168, (((int32_t)(((t20 *)L_5)->max_length))), &m3984_MI);
		m3997(p0, (t7*) &_stringLiteral1169, (t29 *)(t29 *)V_0, &m3997_MI);
		return;
	}
}
extern MethodInfo m15531_MI;
 void m15531 (t472 * __this, t29 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	t2858* V_1 = {0};
	int32_t V_2 = 0;
	{
		t733 * L_0 = (__this->f13);
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		return;
	}

IL_0009:
	{
		t733 * L_1 = (__this->f13);
		int32_t L_2 = m3996(L_1, (t7*) &_stringLiteral208, &m3996_MI);
		__this->f14 = L_2;
		t733 * L_3 = (__this->f13);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_4 = m1554(NULL, LoadTypeToken(&t2344_0_0_0), &m1554_MI);
		t29 * L_5 = m3985(L_3, (t7*) &_stringLiteral210, L_4, &m3985_MI);
		__this->f12 = ((t29*)Castclass(L_5, InitializedTypeInfo(&t2344_TI)));
		t733 * L_6 = (__this->f13);
		int32_t L_7 = m3996(L_6, (t7*) &_stringLiteral1168, &m3996_MI);
		V_0 = L_7;
		t733 * L_8 = (__this->f13);
		t42 * L_9 = m1554(NULL, LoadTypeToken(&t2858_0_0_0), &m1554_MI);
		t29 * L_10 = m3985(L_8, (t7*) &_stringLiteral1169, L_9, &m3985_MI);
		V_1 = ((t2858*)Castclass(L_10, InitializedTypeInfo(&t2858_TI)));
		if ((((int32_t)V_0) >= ((int32_t)((int32_t)10))))
		{
			goto IL_007d;
		}
	}
	{
		V_0 = ((int32_t)10);
	}

IL_007d:
	{
		m15520(__this, V_0, &m15520_MI);
		__this->f10 = 0;
		if (!V_1)
		{
			goto IL_00ba;
		}
	}
	{
		V_2 = 0;
		goto IL_00b4;
	}

IL_0092:
	{
		int32_t L_11 = m15540(((t2859 *)(t2859 *)SZArrayLdElema(V_1, V_2)), &m15540_MI);
		t469 * L_12 = m15542(((t2859 *)(t2859 *)SZArrayLdElema(V_1, V_2)), &m15542_MI);
		VirtActionInvoker2< int32_t, t469 * >::Invoke(&m15526_MI, __this, L_11, L_12);
		V_2 = ((int32_t)(V_2+1));
	}

IL_00b4:
	{
		if ((((int32_t)V_2) < ((int32_t)(((int32_t)(((t20 *)V_1)->max_length))))))
		{
			goto IL_0092;
		}
	}

IL_00ba:
	{
		int32_t L_13 = (__this->f14);
		__this->f14 = ((int32_t)(L_13+1));
		__this->f13 = (t733 *)NULL;
		return;
	}
}
 bool m15532 (t472 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	t469 * V_5 = {0};
	{
		int32_t L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t44_TI), &L_0);
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_2 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_2, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0013:
	{
		t29* L_3 = (__this->f12);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(&m27289_MI, L_3, p0);
		V_0 = ((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		t841* L_5 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_5)->max_length)))));
		t841* L_6 = (__this->f4);
		int32_t L_7 = V_1;
		V_2 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_6, L_7))-1));
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0048;
		}
	}
	{
		return 0;
	}

IL_0048:
	{
		V_3 = (-1);
	}

IL_004a:
	{
		t1965* L_8 = (__this->f5);
		int32_t L_9 = (((t1248 *)(t1248 *)SZArrayLdElema(L_8, V_2))->f0);
		if ((((uint32_t)L_9) != ((uint32_t)V_0)))
		{
			goto IL_007a;
		}
	}
	{
		t29* L_10 = (__this->f12);
		t841* L_11 = (__this->f6);
		int32_t L_12 = V_2;
		bool L_13 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(&m27290_MI, L_10, (*(int32_t*)(int32_t*)SZArrayLdElema(L_11, L_12)), p0);
		if (!L_13)
		{
			goto IL_007a;
		}
	}
	{
		goto IL_0092;
	}

IL_007a:
	{
		V_3 = V_2;
		t1965* L_14 = (__this->f5);
		int32_t L_15 = (((t1248 *)(t1248 *)SZArrayLdElema(L_14, V_2))->f1);
		V_2 = L_15;
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_004a;
		}
	}

IL_0092:
	{
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0098;
		}
	}
	{
		return 0;
	}

IL_0098:
	{
		int32_t L_16 = (__this->f10);
		__this->f10 = ((int32_t)(L_16-1));
		if ((((uint32_t)V_3) != ((uint32_t)(-1))))
		{
			goto IL_00c7;
		}
	}
	{
		t841* L_17 = (__this->f4);
		t1965* L_18 = (__this->f5);
		int32_t L_19 = (((t1248 *)(t1248 *)SZArrayLdElema(L_18, V_2))->f1);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_17, V_1)) = (int32_t)((int32_t)(L_19+1));
		goto IL_00e9;
	}

IL_00c7:
	{
		t1965* L_20 = (__this->f5);
		t1965* L_21 = (__this->f5);
		int32_t L_22 = (((t1248 *)(t1248 *)SZArrayLdElema(L_21, V_2))->f1);
		((t1248 *)(t1248 *)SZArrayLdElema(L_20, V_3))->f1 = L_22;
	}

IL_00e9:
	{
		t1965* L_23 = (__this->f5);
		int32_t L_24 = (__this->f9);
		((t1248 *)(t1248 *)SZArrayLdElema(L_23, V_2))->f1 = L_24;
		__this->f9 = V_2;
		t1965* L_25 = (__this->f5);
		((t1248 *)(t1248 *)SZArrayLdElema(L_25, V_2))->f0 = 0;
		t841* L_26 = (__this->f6);
		Initobj (&t44_TI, (&V_4));
		*((int32_t*)(int32_t*)SZArrayLdElema(L_26, V_2)) = (int32_t)V_4;
		t2855* L_27 = (__this->f7);
		Initobj (&t469_TI, (&V_5));
		*((t469 **)(t469 **)SZArrayLdElema(L_27, V_2)) = (t469 *)V_5;
		int32_t L_28 = (__this->f14);
		__this->f14 = ((int32_t)(L_28+1));
		return 1;
	}
}
 bool m2847 (t472 * __this, int32_t p0, t469 ** p1, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	t469 * V_2 = {0};
	{
		int32_t L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t44_TI), &L_0);
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_2 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_2, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0013:
	{
		t29* L_3 = (__this->f12);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(&m27289_MI, L_3, p0);
		V_0 = ((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		t841* L_5 = (__this->f4);
		t841* L_6 = (__this->f4);
		int32_t L_7 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_6)->max_length)))));
		V_1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_7))-1));
		goto IL_0096;
	}

IL_0042:
	{
		t1965* L_8 = (__this->f5);
		int32_t L_9 = (((t1248 *)(t1248 *)SZArrayLdElema(L_8, V_1))->f0);
		if ((((uint32_t)L_9) != ((uint32_t)V_0)))
		{
			goto IL_0084;
		}
	}
	{
		t29* L_10 = (__this->f12);
		t841* L_11 = (__this->f6);
		int32_t L_12 = V_1;
		bool L_13 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(&m27290_MI, L_10, (*(int32_t*)(int32_t*)SZArrayLdElema(L_11, L_12)), p0);
		if (!L_13)
		{
			goto IL_0084;
		}
	}
	{
		t2855* L_14 = (__this->f7);
		int32_t L_15 = V_1;
		*p1 = (*(t469 **)(t469 **)SZArrayLdElema(L_14, L_15));
		return 1;
	}

IL_0084:
	{
		t1965* L_16 = (__this->f5);
		int32_t L_17 = (((t1248 *)(t1248 *)SZArrayLdElema(L_16, V_1))->f1);
		V_1 = L_17;
	}

IL_0096:
	{
		if ((((uint32_t)V_1) != ((uint32_t)(-1))))
		{
			goto IL_0042;
		}
	}
	{
		Initobj (&t469_TI, (&V_2));
		*p1 = V_2;
		return 0;
	}
}
extern MethodInfo m15533_MI;
 t2857 * m15533 (t472 * __this, MethodInfo* method){
	{
		t2857 * L_0 = (t2857 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2857_TI));
		m15555(L_0, __this, &m15555_MI);
		return L_0;
	}
}
 int32_t m15534 (t472 * __this, t29 * p0, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		if (((t29 *)IsInst(p0, InitializedTypeInfo(&t44_TI))))
		{
			goto IL_003a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_1 = m1554(NULL, LoadTypeToken(&t44_0_0_0), &m1554_MI);
		t7* L_2 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m6035_MI, L_1);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_3 = m66(NULL, (t7*) &_stringLiteral1170, L_2, &m66_MI);
		t305 * L_4 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m3973(L_4, L_3, (t7*) &_stringLiteral195, &m3973_MI);
		il2cpp_codegen_raise_exception(L_4);
	}

IL_003a:
	{
		return ((*(int32_t*)((int32_t*)UnBox (p0, InitializedTypeInfo(&t44_TI)))));
	}
}
 t469 * m15535 (t472 * __this, t29 * p0, MethodInfo* method){
	t469 * V_0 = {0};
	{
		if (p0)
		{
			goto IL_001e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_0 = m1554(NULL, LoadTypeToken(&t469_0_0_0), &m1554_MI);
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6004_MI, L_0);
		if (L_1)
		{
			goto IL_001e;
		}
	}
	{
		Initobj (&t469_TI, (&V_0));
		return V_0;
	}

IL_001e:
	{
		if (((t469 *)IsInst(p0, InitializedTypeInfo(&t469_TI))))
		{
			goto IL_004a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_2 = m1554(NULL, LoadTypeToken(&t469_0_0_0), &m1554_MI);
		t7* L_3 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m6035_MI, L_2);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_4 = m66(NULL, (t7*) &_stringLiteral1170, L_3, &m66_MI);
		t305 * L_5 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m3973(L_5, L_4, (t7*) &_stringLiteral401, &m3973_MI);
		il2cpp_codegen_raise_exception(L_5);
	}

IL_004a:
	{
		return ((t469 *)Castclass(p0, InitializedTypeInfo(&t469_TI)));
	}
}
 bool m15536 (t472 * __this, t2859  p0, MethodInfo* method){
	t469 * V_0 = {0};
	{
		int32_t L_0 = m15540((&p0), &m15540_MI);
		bool L_1 = (bool)VirtFuncInvoker2< bool, int32_t, t469 ** >::Invoke(&m2847_MI, __this, L_0, (&V_0));
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		return 0;
	}

IL_0013:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t2869_TI));
		t2869 * L_2 = m15608(NULL, &m15608_MI);
		t469 * L_3 = m15542((&p0), &m15542_MI);
		bool L_4 = (bool)VirtFuncInvoker2< bool, t469 *, t469 * >::Invoke(&m29135_MI, L_2, L_3, V_0);
		return L_4;
	}
}
extern MethodInfo m15537_MI;
 t2861  m15537 (t472 * __this, MethodInfo* method){
	{
		t2861  L_0 = {0};
		m15574(&L_0, __this, &m15574_MI);
		return L_0;
	}
}
 t725  m15538 (t29 * __this, int32_t p0, t469 * p1, MethodInfo* method){
	{
		int32_t L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t44_TI), &L_0);
		t469 * L_2 = p1;
		t725  L_3 = {0};
		m3965(&L_3, L_1, ((t469 *)L_2), &m3965_MI);
		return L_3;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>
extern Il2CppType t44_0_0_32849;
FieldInfo t472_f0_FieldInfo = 
{
	"INITIAL_SIZE", &t44_0_0_32849, &t472_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_32849;
FieldInfo t472_f1_FieldInfo = 
{
	"DEFAULT_LOAD_FACTOR", &t22_0_0_32849, &t472_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_32849;
FieldInfo t472_f2_FieldInfo = 
{
	"NO_SLOT", &t44_0_0_32849, &t472_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_32849;
FieldInfo t472_f3_FieldInfo = 
{
	"HASH_FLAG", &t44_0_0_32849, &t472_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t841_0_0_1;
FieldInfo t472_f4_FieldInfo = 
{
	"table", &t841_0_0_1, &t472_TI, offsetof(t472, f4), &EmptyCustomAttributesCache};
extern Il2CppType t1965_0_0_1;
FieldInfo t472_f5_FieldInfo = 
{
	"linkSlots", &t1965_0_0_1, &t472_TI, offsetof(t472, f5), &EmptyCustomAttributesCache};
extern Il2CppType t841_0_0_1;
FieldInfo t472_f6_FieldInfo = 
{
	"keySlots", &t841_0_0_1, &t472_TI, offsetof(t472, f6), &EmptyCustomAttributesCache};
extern Il2CppType t2855_0_0_1;
FieldInfo t472_f7_FieldInfo = 
{
	"valueSlots", &t2855_0_0_1, &t472_TI, offsetof(t472, f7), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t472_f8_FieldInfo = 
{
	"touchedSlots", &t44_0_0_1, &t472_TI, offsetof(t472, f8), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t472_f9_FieldInfo = 
{
	"emptySlot", &t44_0_0_1, &t472_TI, offsetof(t472, f9), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t472_f10_FieldInfo = 
{
	"count", &t44_0_0_1, &t472_TI, offsetof(t472, f10), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t472_f11_FieldInfo = 
{
	"threshold", &t44_0_0_1, &t472_TI, offsetof(t472, f11), &EmptyCustomAttributesCache};
extern Il2CppType t2344_0_0_1;
FieldInfo t472_f12_FieldInfo = 
{
	"hcp", &t2344_0_0_1, &t472_TI, offsetof(t472, f12), &EmptyCustomAttributesCache};
extern Il2CppType t733_0_0_1;
FieldInfo t472_f13_FieldInfo = 
{
	"serialization_info", &t733_0_0_1, &t472_TI, offsetof(t472, f13), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t472_f14_FieldInfo = 
{
	"generation", &t44_0_0_1, &t472_TI, offsetof(t472, f14), &EmptyCustomAttributesCache};
extern Il2CppType t2856_0_0_17;
extern CustomAttributesCache t1254__CustomAttributeCache_U3CU3Ef__am$cacheB;
FieldInfo t472_f15_FieldInfo = 
{
	"<>f__am$cacheB", &t2856_0_0_17, &t472_TI, offsetof(t472_SFs, f15), &t1254__CustomAttributeCache_U3CU3Ef__am$cacheB};
static FieldInfo* t472_FIs[] =
{
	&t472_f0_FieldInfo,
	&t472_f1_FieldInfo,
	&t472_f2_FieldInfo,
	&t472_f3_FieldInfo,
	&t472_f4_FieldInfo,
	&t472_f5_FieldInfo,
	&t472_f6_FieldInfo,
	&t472_f7_FieldInfo,
	&t472_f8_FieldInfo,
	&t472_f9_FieldInfo,
	&t472_f10_FieldInfo,
	&t472_f11_FieldInfo,
	&t472_f12_FieldInfo,
	&t472_f13_FieldInfo,
	&t472_f14_FieldInfo,
	&t472_f15_FieldInfo,
	NULL
};
static const int32_t t472_f0_DefaultValueData = 10;
static Il2CppFieldDefaultValueEntry t472_f0_DefaultValue = 
{
	&t472_f0_FieldInfo, { (char*)&t472_f0_DefaultValueData, &t44_0_0_0 }};
static const float t472_f1_DefaultValueData = 0.9f;
extern Il2CppType t22_0_0_0;
static Il2CppFieldDefaultValueEntry t472_f1_DefaultValue = 
{
	&t472_f1_FieldInfo, { (char*)&t472_f1_DefaultValueData, &t22_0_0_0 }};
static const int32_t t472_f2_DefaultValueData = -1;
static Il2CppFieldDefaultValueEntry t472_f2_DefaultValue = 
{
	&t472_f2_FieldInfo, { (char*)&t472_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t472_f3_DefaultValueData = -2147483648;
static Il2CppFieldDefaultValueEntry t472_f3_DefaultValue = 
{
	&t472_f3_FieldInfo, { (char*)&t472_f3_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t472_FDVs[] = 
{
	&t472_f0_DefaultValue,
	&t472_f1_DefaultValue,
	&t472_f2_DefaultValue,
	&t472_f3_DefaultValue,
	NULL
};
static PropertyInfo t472____System_Collections_IDictionary_Item_PropertyInfo = 
{
	&t472_TI, "System.Collections.IDictionary.Item", &m15502_MI, &m15503_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t472____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t472_TI, "System.Collections.ICollection.IsSynchronized", &m15506_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t472____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t472_TI, "System.Collections.ICollection.SyncRoot", &m15507_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t472____System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_IsReadOnly_PropertyInfo = 
{
	&t472_TI, "System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.IsReadOnly", &m15508_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t472____Count_PropertyInfo = 
{
	&t472_TI, "Count", &m15517_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t472____Item_PropertyInfo = 
{
	&t472_TI, "Item", &m15518_MI, &m2848_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t472____Values_PropertyInfo = 
{
	&t472_TI, "Values", &m15533_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t472_PIs[] =
{
	&t472____System_Collections_IDictionary_Item_PropertyInfo,
	&t472____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t472____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t472____System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_IsReadOnly_PropertyInfo,
	&t472____Count_PropertyInfo,
	&t472____Item_PropertyInfo,
	&t472____Values_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2846_GM;
MethodInfo m2846_MI = 
{
	".ctor", (methodPointerType)&m2846, &t472_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m2846_GM};
extern Il2CppType t2344_0_0_0;
static ParameterInfo t472_m15499_ParameterInfos[] = 
{
	{"comparer", 0, 134217728, &EmptyCustomAttributesCache, &t2344_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15499_GM;
MethodInfo m15499_MI = 
{
	".ctor", (methodPointerType)&m15499, &t472_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t472_m15499_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15499_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t472_m15500_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15500_GM;
MethodInfo m15500_MI = 
{
	".ctor", (methodPointerType)&m15500, &t472_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t472_m15500_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15500_GM};
extern Il2CppType t733_0_0_0;
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t472_m15501_ParameterInfos[] = 
{
	{"info", 0, 134217728, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134217728, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15501_GM;
MethodInfo m15501_MI = 
{
	".ctor", (methodPointerType)&m15501, &t472_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t472_m15501_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15501_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t472_m15502_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15502_GM;
MethodInfo m15502_MI = 
{
	"System.Collections.IDictionary.get_Item", (methodPointerType)&m15502, &t472_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t472_m15502_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15502_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t472_m15503_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15503_GM;
MethodInfo m15503_MI = 
{
	"System.Collections.IDictionary.set_Item", (methodPointerType)&m15503, &t472_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t472_m15503_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 20, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15503_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t472_m15504_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15504_GM;
MethodInfo m15504_MI = 
{
	"System.Collections.IDictionary.Add", (methodPointerType)&m15504, &t472_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t472_m15504_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 21, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15504_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t472_m15505_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15505_GM;
MethodInfo m15505_MI = 
{
	"System.Collections.IDictionary.Remove", (methodPointerType)&m15505, &t472_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t472_m15505_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 23, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15505_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15506_GM;
MethodInfo m15506_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m15506, &t472_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15506_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15507_GM;
MethodInfo m15507_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m15507, &t472_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15507_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15508_GM;
MethodInfo m15508_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly", (methodPointerType)&m15508, &t472_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 11, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15508_GM};
extern Il2CppType t2859_0_0_0;
extern Il2CppType t2859_0_0_0;
static ParameterInfo t472_m15509_ParameterInfos[] = 
{
	{"keyValuePair", 0, 134217728, &EmptyCustomAttributesCache, &t2859_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t2859 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15509_GM;
MethodInfo m15509_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add", (methodPointerType)&m15509, &t472_TI, &t21_0_0_0, RuntimeInvoker_t21_t2859, t472_m15509_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15509_GM};
extern Il2CppType t2859_0_0_0;
static ParameterInfo t472_m15510_ParameterInfos[] = 
{
	{"keyValuePair", 0, 134217728, &EmptyCustomAttributesCache, &t2859_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2859 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15510_GM;
MethodInfo m15510_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains", (methodPointerType)&m15510, &t472_TI, &t40_0_0_0, RuntimeInvoker_t40_t2859, t472_m15510_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 14, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15510_GM};
extern Il2CppType t2858_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t472_m15511_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2858_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15511_GM;
MethodInfo m15511_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo", (methodPointerType)&m15511, &t472_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t472_m15511_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15511_GM};
extern Il2CppType t2859_0_0_0;
static ParameterInfo t472_m15512_ParameterInfos[] = 
{
	{"keyValuePair", 0, 134217728, &EmptyCustomAttributesCache, &t2859_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2859 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15512_GM;
MethodInfo m15512_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove", (methodPointerType)&m15512, &t472_TI, &t40_0_0_0, RuntimeInvoker_t40_t2859, t472_m15512_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15512_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t472_m15513_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15513_GM;
MethodInfo m15513_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m15513, &t472_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t472_m15513_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15513_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15514_GM;
MethodInfo m15514_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m15514, &t472_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15514_GM};
extern Il2CppType t2860_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15515_GM;
MethodInfo m15515_MI = 
{
	"System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator", (methodPointerType)&m15515, &t472_TI, &t2860_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 17, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15515_GM};
extern Il2CppType t722_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15516_GM;
MethodInfo m15516_MI = 
{
	"System.Collections.IDictionary.GetEnumerator", (methodPointerType)&m15516, &t472_TI, &t722_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 22, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15516_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15517_GM;
MethodInfo m15517_MI = 
{
	"get_Count", (methodPointerType)&m15517, &t472_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15517_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t472_m15518_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t469_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15518_GM;
MethodInfo m15518_MI = 
{
	"get_Item", (methodPointerType)&m15518, &t472_TI, &t469_0_0_0, RuntimeInvoker_t29_t44, t472_m15518_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 25, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15518_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t469_0_0_0;
static ParameterInfo t472_m2848_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t469_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2848_GM;
MethodInfo m2848_MI = 
{
	"set_Item", (methodPointerType)&m2848, &t472_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t472_m2848_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 26, 2, false, true, 0, NULL, (methodPointerType)NULL, &m2848_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2344_0_0_0;
static ParameterInfo t472_m15519_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"hcp", 1, 134217728, &EmptyCustomAttributesCache, &t2344_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15519_GM;
MethodInfo m15519_MI = 
{
	"Init", (methodPointerType)&m15519, &t472_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t472_m15519_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15519_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t472_m15520_ParameterInfos[] = 
{
	{"size", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15520_GM;
MethodInfo m15520_MI = 
{
	"InitArrays", (methodPointerType)&m15520, &t472_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t472_m15520_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15520_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t472_m15521_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15521_GM;
MethodInfo m15521_MI = 
{
	"CopyToCheck", (methodPointerType)&m15521, &t472_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t472_m15521_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15521_GM};
extern Il2CppType t1957_0_0_0;
extern Il2CppType t1957_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t6711_0_0_0;
extern Il2CppType t6711_0_0_0;
static ParameterInfo t472_m29136_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1957_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"transform", 2, 134217728, &EmptyCustomAttributesCache, &t6711_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericContainer m29136_IGC;
extern TypeInfo m29136_gp_TRet_0_TI;
Il2CppGenericParamFull m29136_gp_TRet_0_TI_GenericParamFull = { { &m29136_IGC, 0}, {NULL, "TRet", 0, 0, NULL} };
extern TypeInfo m29136_gp_TElem_1_TI;
Il2CppGenericParamFull m29136_gp_TElem_1_TI_GenericParamFull = { { &m29136_IGC, 1}, {NULL, "TElem", 0, 0, NULL} };
static Il2CppGenericParamFull* m29136_IGPA[2] = 
{
	&m29136_gp_TRet_0_TI_GenericParamFull,
	&m29136_gp_TElem_1_TI_GenericParamFull,
};
extern MethodInfo m29136_MI;
Il2CppGenericContainer m29136_IGC = { { NULL, NULL }, NULL, &m29136_MI, 2, 1, m29136_IGPA };
extern Il2CppGenericMethod m29137_GM;
extern Il2CppType m9954_gp_0_0_0_0;
extern Il2CppType m9954_gp_1_0_0_0;
static Il2CppRGCTXDefinition m29136_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &m29137_GM }/* Method Definition */,
	{ IL2CPP_RGCTX_DATA_CLASS, &m9954_gp_0_0_0_0 }/* Class Definition */,
	{ IL2CPP_RGCTX_DATA_CLASS, &m9954_gp_1_0_0_0 }/* Class Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppGenericMethod m29136_GM;
MethodInfo m29136_MI = 
{
	"Do_CopyTo", NULL, &t472_TI, &t21_0_0_0, NULL, t472_m29136_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, true, true, 0, m29136_RGCTXData, (methodPointerType)NULL, &m29136_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t469_0_0_0;
static ParameterInfo t472_m15522_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t469_0_0_0},
};
extern Il2CppType t2859_0_0_0;
extern void* RuntimeInvoker_t2859_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15522_GM;
MethodInfo m15522_MI = 
{
	"make_pair", (methodPointerType)&m15522, &t472_TI, &t2859_0_0_0, RuntimeInvoker_t2859_t44_t29, t472_m15522_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15522_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t469_0_0_0;
static ParameterInfo t472_m15523_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t469_0_0_0},
};
extern Il2CppType t469_0_0_0;
extern void* RuntimeInvoker_t29_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15523_GM;
MethodInfo m15523_MI = 
{
	"pick_value", (methodPointerType)&m15523, &t472_TI, &t469_0_0_0, RuntimeInvoker_t29_t44_t29, t472_m15523_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15523_GM};
extern Il2CppType t2858_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t472_m15524_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2858_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15524_GM;
MethodInfo m15524_MI = 
{
	"CopyTo", (methodPointerType)&m15524, &t472_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t472_m15524_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15524_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t6714_0_0_0;
extern Il2CppType t6714_0_0_0;
static ParameterInfo t472_m29138_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"transform", 2, 134217728, &EmptyCustomAttributesCache, &t6714_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericContainer m29138_IGC;
extern TypeInfo m29138_gp_TRet_0_TI;
Il2CppGenericParamFull m29138_gp_TRet_0_TI_GenericParamFull = { { &m29138_IGC, 0}, {NULL, "TRet", 0, 0, NULL} };
static Il2CppGenericParamFull* m29138_IGPA[1] = 
{
	&m29138_gp_TRet_0_TI_GenericParamFull,
};
extern MethodInfo m29138_MI;
Il2CppGenericContainer m29138_IGC = { { NULL, NULL }, NULL, &m29138_MI, 1, 1, m29138_IGPA };
extern Il2CppType m9959_gp_0_0_0_0;
extern Il2CppGenericMethod m29139_GM;
static Il2CppRGCTXDefinition m29138_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, &m9959_gp_0_0_0_0 }/* Type Definition */,
	{ IL2CPP_RGCTX_DATA_METHOD, &m29139_GM }/* Method Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppGenericMethod m29138_GM;
MethodInfo m29138_MI = 
{
	"Do_ICollectionCopyTo", NULL, &t472_TI, &t21_0_0_0, NULL, t472_m29138_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, true, true, 0, m29138_RGCTXData, (methodPointerType)NULL, &m29138_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15525_GM;
MethodInfo m15525_MI = 
{
	"Resize", (methodPointerType)&m15525, &t472_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15525_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t469_0_0_0;
static ParameterInfo t472_m15526_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t469_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15526_GM;
MethodInfo m15526_MI = 
{
	"Add", (methodPointerType)&m15526, &t472_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t472_m15526_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15526_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15527_GM;
MethodInfo m15527_MI = 
{
	"Clear", (methodPointerType)&m15527, &t472_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 13, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15527_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t472_m15528_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15528_GM;
MethodInfo m15528_MI = 
{
	"ContainsKey", (methodPointerType)&m15528, &t472_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t472_m15528_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15528_GM};
extern Il2CppType t469_0_0_0;
static ParameterInfo t472_m15529_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t469_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15529_GM;
MethodInfo m15529_MI = 
{
	"ContainsValue", (methodPointerType)&m15529, &t472_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t472_m15529_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15529_GM};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t472_m15530_ParameterInfos[] = 
{
	{"info", 0, 134217728, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134217728, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15530_GM;
MethodInfo m15530_MI = 
{
	"GetObjectData", (methodPointerType)&m15530, &t472_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t472_m15530_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15530_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t472_m15531_ParameterInfos[] = 
{
	{"sender", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15531_GM;
MethodInfo m15531_MI = 
{
	"OnDeserialization", (methodPointerType)&m15531, &t472_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t472_m15531_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15531_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t472_m15532_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15532_GM;
MethodInfo m15532_MI = 
{
	"Remove", (methodPointerType)&m15532, &t472_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t472_m15532_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15532_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t469_1_0_2;
extern Il2CppType t469_1_0_0;
static ParameterInfo t472_m2847_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t469_1_0_2},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44_t4353 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2847_GM;
MethodInfo m2847_MI = 
{
	"TryGetValue", (methodPointerType)&m2847, &t472_TI, &t40_0_0_0, RuntimeInvoker_t40_t44_t4353, t472_m2847_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m2847_GM};
extern Il2CppType t2857_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15533_GM;
MethodInfo m15533_MI = 
{
	"get_Values", (methodPointerType)&m15533, &t472_TI, &t2857_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15533_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t472_m15534_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15534_GM;
MethodInfo m15534_MI = 
{
	"ToTKey", (methodPointerType)&m15534, &t472_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t472_m15534_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15534_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t472_m15535_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t469_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15535_GM;
MethodInfo m15535_MI = 
{
	"ToTValue", (methodPointerType)&m15535, &t472_TI, &t469_0_0_0, RuntimeInvoker_t29_t29, t472_m15535_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15535_GM};
extern Il2CppType t2859_0_0_0;
static ParameterInfo t472_m15536_ParameterInfos[] = 
{
	{"pair", 0, 134217728, &EmptyCustomAttributesCache, &t2859_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2859 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15536_GM;
MethodInfo m15536_MI = 
{
	"ContainsKeyValuePair", (methodPointerType)&m15536, &t472_TI, &t40_0_0_0, RuntimeInvoker_t40_t2859, t472_m15536_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15536_GM};
extern Il2CppType t2861_0_0_0;
extern void* RuntimeInvoker_t2861 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15537_GM;
MethodInfo m15537_MI = 
{
	"GetEnumerator", (methodPointerType)&m15537, &t472_TI, &t2861_0_0_0, RuntimeInvoker_t2861, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15537_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t469_0_0_0;
static ParameterInfo t472_m15538_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t469_0_0_0},
};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725_t44_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1254__CustomAttributeCache_m9975;
extern Il2CppGenericMethod m15538_GM;
MethodInfo m15538_MI = 
{
	"<CopyTo>m__0", (methodPointerType)&m15538, &t472_TI, &t725_0_0_0, RuntimeInvoker_t725_t44_t29, t472_m15538_ParameterInfos, &t1254__CustomAttributeCache_m9975, 145, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15538_GM};
static MethodInfo* t472_MIs[] =
{
	&m2846_MI,
	&m15499_MI,
	&m15500_MI,
	&m15501_MI,
	&m15502_MI,
	&m15503_MI,
	&m15504_MI,
	&m15505_MI,
	&m15506_MI,
	&m15507_MI,
	&m15508_MI,
	&m15509_MI,
	&m15510_MI,
	&m15511_MI,
	&m15512_MI,
	&m15513_MI,
	&m15514_MI,
	&m15515_MI,
	&m15516_MI,
	&m15517_MI,
	&m15518_MI,
	&m2848_MI,
	&m15519_MI,
	&m15520_MI,
	&m15521_MI,
	&m29136_MI,
	&m15522_MI,
	&m15523_MI,
	&m15524_MI,
	&m29138_MI,
	&m15525_MI,
	&m15526_MI,
	&m15527_MI,
	&m15528_MI,
	&m15529_MI,
	&m15530_MI,
	&m15531_MI,
	&m15532_MI,
	&m2847_MI,
	&m15533_MI,
	&m15534_MI,
	&m15535_MI,
	&m15536_MI,
	&m15537_MI,
	&m15538_MI,
	NULL
};
static MethodInfo* t472_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15514_MI,
	&m15530_MI,
	&m15517_MI,
	&m15506_MI,
	&m15507_MI,
	&m15513_MI,
	&m15517_MI,
	&m15508_MI,
	&m15509_MI,
	&m15527_MI,
	&m15510_MI,
	&m15511_MI,
	&m15512_MI,
	&m15515_MI,
	&m15532_MI,
	&m15502_MI,
	&m15503_MI,
	&m15504_MI,
	&m15516_MI,
	&m15505_MI,
	&m15531_MI,
	&m15518_MI,
	&m2848_MI,
	&m15526_MI,
	&m15528_MI,
	&m15530_MI,
	&m15531_MI,
	&m2847_MI,
};
extern TypeInfo t674_TI;
extern TypeInfo t5571_TI;
extern TypeInfo t5573_TI;
extern TypeInfo t6716_TI;
extern TypeInfo t721_TI;
extern TypeInfo t918_TI;
static TypeInfo* t472_ITIs[] = 
{
	&t603_TI,
	&t374_TI,
	&t674_TI,
	&t5571_TI,
	&t5573_TI,
	&t6716_TI,
	&t721_TI,
	&t918_TI,
};
static Il2CppInterfaceOffsetPair t472_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t374_TI, 5},
	{ &t674_TI, 6},
	{ &t5571_TI, 10},
	{ &t5573_TI, 17},
	{ &t6716_TI, 18},
	{ &t721_TI, 19},
	{ &t918_TI, 24},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t472_0_0_0;
extern Il2CppType t472_1_0_0;
struct t472;
extern Il2CppGenericClass t472_GC;
extern CustomAttributesCache t1254__CustomAttributeCache;
extern CustomAttributesCache t1254__CustomAttributeCache_U3CU3Ef__am$cacheB;
extern CustomAttributesCache t1254__CustomAttributeCache_m9975;
TypeInfo t472_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Dictionary`2", "System.Collections.Generic", t472_MIs, t472_PIs, t472_FIs, NULL, &t29_TI, NULL, NULL, &t472_TI, t472_ITIs, t472_VT, &t1254__CustomAttributeCache, &t472_TI, &t472_0_0_0, &t472_1_0_0, t472_IOs, &t472_GC, NULL, t472_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t472), 0, -1, sizeof(t472_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 45, 7, 16, 0, 0, 32, 8, 8};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>>
extern MethodInfo m29140_MI;
static PropertyInfo t5571____Count_PropertyInfo = 
{
	&t5571_TI, "Count", &m29140_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29141_MI;
static PropertyInfo t5571____IsReadOnly_PropertyInfo = 
{
	&t5571_TI, "IsReadOnly", &m29141_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5571_PIs[] =
{
	&t5571____Count_PropertyInfo,
	&t5571____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29140_GM;
MethodInfo m29140_MI = 
{
	"get_Count", NULL, &t5571_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29140_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29141_GM;
MethodInfo m29141_MI = 
{
	"get_IsReadOnly", NULL, &t5571_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29141_GM};
extern Il2CppType t2859_0_0_0;
static ParameterInfo t5571_m29142_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2859_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t2859 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29142_GM;
MethodInfo m29142_MI = 
{
	"Add", NULL, &t5571_TI, &t21_0_0_0, RuntimeInvoker_t21_t2859, t5571_m29142_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29142_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29143_GM;
MethodInfo m29143_MI = 
{
	"Clear", NULL, &t5571_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29143_GM};
extern Il2CppType t2859_0_0_0;
static ParameterInfo t5571_m29144_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2859_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2859 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29144_GM;
MethodInfo m29144_MI = 
{
	"Contains", NULL, &t5571_TI, &t40_0_0_0, RuntimeInvoker_t40_t2859, t5571_m29144_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29144_GM};
extern Il2CppType t2858_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5571_m29145_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2858_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29145_GM;
MethodInfo m29145_MI = 
{
	"CopyTo", NULL, &t5571_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5571_m29145_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29145_GM};
extern Il2CppType t2859_0_0_0;
static ParameterInfo t5571_m29146_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2859_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2859 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29146_GM;
MethodInfo m29146_MI = 
{
	"Remove", NULL, &t5571_TI, &t40_0_0_0, RuntimeInvoker_t40_t2859, t5571_m29146_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29146_GM};
static MethodInfo* t5571_MIs[] =
{
	&m29140_MI,
	&m29141_MI,
	&m29142_MI,
	&m29143_MI,
	&m29144_MI,
	&m29145_MI,
	&m29146_MI,
	NULL
};
static TypeInfo* t5571_ITIs[] = 
{
	&t603_TI,
	&t5573_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5571_0_0_0;
extern Il2CppType t5571_1_0_0;
struct t5571;
extern Il2CppGenericClass t5571_GC;
TypeInfo t5571_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5571_MIs, t5571_PIs, NULL, NULL, NULL, NULL, NULL, &t5571_TI, t5571_ITIs, NULL, &EmptyCustomAttributesCache, &t5571_TI, &t5571_0_0_0, &t5571_1_0_0, NULL, &t5571_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>>
extern Il2CppType t2860_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29147_GM;
MethodInfo m29147_MI = 
{
	"GetEnumerator", NULL, &t5573_TI, &t2860_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29147_GM};
static MethodInfo* t5573_MIs[] =
{
	&m29147_MI,
	NULL
};
static TypeInfo* t5573_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5573_0_0_0;
extern Il2CppType t5573_1_0_0;
struct t5573;
extern Il2CppGenericClass t5573_GC;
TypeInfo t5573_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5573_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5573_TI, t5573_ITIs, NULL, &EmptyCustomAttributesCache, &t5573_TI, &t5573_0_0_0, &t5573_1_0_0, NULL, &t5573_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2860_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>>
extern MethodInfo m29148_MI;
static PropertyInfo t2860____Current_PropertyInfo = 
{
	&t2860_TI, "Current", &m29148_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2860_PIs[] =
{
	&t2860____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2859_0_0_0;
extern void* RuntimeInvoker_t2859 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29148_GM;
MethodInfo m29148_MI = 
{
	"get_Current", NULL, &t2860_TI, &t2859_0_0_0, RuntimeInvoker_t2859, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29148_GM};
static MethodInfo* t2860_MIs[] =
{
	&m29148_MI,
	NULL
};
static TypeInfo* t2860_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2860_0_0_0;
extern Il2CppType t2860_1_0_0;
struct t2860;
extern Il2CppGenericClass t2860_GC;
TypeInfo t2860_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2860_MIs, t2860_PIs, NULL, NULL, NULL, NULL, NULL, &t2860_TI, t2860_ITIs, NULL, &EmptyCustomAttributesCache, &t2860_TI, &t2860_0_0_0, &t2860_1_0_0, NULL, &t2860_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo t446_TI;
extern MethodInfo m15541_MI;
extern MethodInfo m15543_MI;
extern MethodInfo m4008_MI;


 void m15539 (t2859 * __this, int32_t p0, t469 * p1, MethodInfo* method){
	{
		m15541(__this, p0, &m15541_MI);
		m15543(__this, p1, &m15543_MI);
		return;
	}
}
 int32_t m15540 (t2859 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f0);
		return L_0;
	}
}
 void m15541 (t2859 * __this, int32_t p0, MethodInfo* method){
	{
		__this->f0 = p0;
		return;
	}
}
 t469 * m15542 (t2859 * __this, MethodInfo* method){
	{
		t469 * L_0 = (__this->f1);
		return L_0;
	}
}
 void m15543 (t2859 * __this, t469 * p0, MethodInfo* method){
	{
		__this->f1 = p0;
		return;
	}
}
extern MethodInfo m15544_MI;
 t7* m15544 (t2859 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	t469 * V_1 = {0};
	int32_t G_B2_0 = 0;
	t446* G_B2_1 = {0};
	t446* G_B2_2 = {0};
	int32_t G_B1_0 = 0;
	t446* G_B1_1 = {0};
	t446* G_B1_2 = {0};
	t7* G_B3_0 = {0};
	int32_t G_B3_1 = 0;
	t446* G_B3_2 = {0};
	t446* G_B3_3 = {0};
	int32_t G_B5_0 = 0;
	t446* G_B5_1 = {0};
	t446* G_B5_2 = {0};
	int32_t G_B4_0 = 0;
	t446* G_B4_1 = {0};
	t446* G_B4_2 = {0};
	t7* G_B6_0 = {0};
	int32_t G_B6_1 = 0;
	t446* G_B6_2 = {0};
	t446* G_B6_3 = {0};
	{
		t446* L_0 = ((t446*)SZArrayNew(InitializedTypeInfo(&t446_TI), 5));
		ArrayElementTypeCheck (L_0, (t7*) &_stringLiteral175);
		*((t7**)(t7**)SZArrayLdElema(L_0, 0)) = (t7*)(t7*) &_stringLiteral175;
		t446* L_1 = L_0;
		int32_t L_2 = m15540(__this, &m15540_MI);
		int32_t L_3 = L_2;
		t29 * L_4 = Box(InitializedTypeInfo(&t44_TI), &L_3);
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_4)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0033;
		}
	}
	{
		int32_t L_5 = m15540(__this, &m15540_MI);
		V_0 = L_5;
		t7* L_6 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1332_MI, Box(InitializedTypeInfo(&t44_TI), &(*(&V_0))));
		G_B3_0 = L_6;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_0038;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		G_B3_0 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_0038:
	{
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		*((t7**)(t7**)SZArrayLdElema(G_B3_2, G_B3_1)) = (t7*)G_B3_0;
		t446* L_7 = G_B3_3;
		ArrayElementTypeCheck (L_7, (t7*) &_stringLiteral184);
		*((t7**)(t7**)SZArrayLdElema(L_7, 2)) = (t7*)(t7*) &_stringLiteral184;
		t446* L_8 = L_7;
		t469 * L_9 = m15542(__this, &m15542_MI);
		t469 * L_10 = L_9;
		G_B4_0 = 3;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
		if (!((t469 *)L_10))
		{
			G_B5_0 = 3;
			G_B5_1 = L_8;
			G_B5_2 = L_8;
			goto IL_0066;
		}
	}
	{
		t469 * L_11 = m15542(__this, &m15542_MI);
		V_1 = L_11;
		t7* L_12 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1332_MI, (*(&V_1)));
		G_B6_0 = L_12;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_006b;
	}

IL_0066:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		G_B6_0 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_006b:
	{
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		*((t7**)(t7**)SZArrayLdElema(G_B6_2, G_B6_1)) = (t7*)G_B6_0;
		t446* L_13 = G_B6_3;
		ArrayElementTypeCheck (L_13, (t7*) &_stringLiteral176);
		*((t7**)(t7**)SZArrayLdElema(L_13, 4)) = (t7*)(t7*) &_stringLiteral176;
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_14 = m4008(NULL, L_13, &m4008_MI);
		return L_14;
	}
}
// Metadata Definition System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>
extern Il2CppType t44_0_0_1;
FieldInfo t2859_f0_FieldInfo = 
{
	"key", &t44_0_0_1, &t2859_TI, offsetof(t2859, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t469_0_0_1;
FieldInfo t2859_f1_FieldInfo = 
{
	"value", &t469_0_0_1, &t2859_TI, offsetof(t2859, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2859_FIs[] =
{
	&t2859_f0_FieldInfo,
	&t2859_f1_FieldInfo,
	NULL
};
static PropertyInfo t2859____Key_PropertyInfo = 
{
	&t2859_TI, "Key", &m15540_MI, &m15541_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2859____Value_PropertyInfo = 
{
	&t2859_TI, "Value", &m15542_MI, &m15543_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2859_PIs[] =
{
	&t2859____Key_PropertyInfo,
	&t2859____Value_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern Il2CppType t469_0_0_0;
static ParameterInfo t2859_m15539_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t469_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15539_GM;
MethodInfo m15539_MI = 
{
	".ctor", (methodPointerType)&m15539, &t2859_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2859_m15539_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15539_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15540_GM;
MethodInfo m15540_MI = 
{
	"get_Key", (methodPointerType)&m15540, &t2859_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15540_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2859_m15541_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15541_GM;
MethodInfo m15541_MI = 
{
	"set_Key", (methodPointerType)&m15541, &t2859_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2859_m15541_ParameterInfos, &EmptyCustomAttributesCache, 2177, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15541_GM};
extern Il2CppType t469_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15542_GM;
MethodInfo m15542_MI = 
{
	"get_Value", (methodPointerType)&m15542, &t2859_TI, &t469_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15542_GM};
extern Il2CppType t469_0_0_0;
static ParameterInfo t2859_m15543_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t469_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15543_GM;
MethodInfo m15543_MI = 
{
	"set_Value", (methodPointerType)&m15543, &t2859_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2859_m15543_ParameterInfos, &EmptyCustomAttributesCache, 2177, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15543_GM};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15544_GM;
MethodInfo m15544_MI = 
{
	"ToString", (methodPointerType)&m15544, &t2859_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15544_GM};
static MethodInfo* t2859_MIs[] =
{
	&m15539_MI,
	&m15540_MI,
	&m15541_MI,
	&m15542_MI,
	&m15543_MI,
	&m15544_MI,
	NULL
};
static MethodInfo* t2859_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m15544_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2859_1_0_0;
extern Il2CppGenericClass t2859_GC;
extern CustomAttributesCache t1259__CustomAttributeCache;
TypeInfo t2859_TI = 
{
	&g_mscorlib_dll_Image, NULL, "KeyValuePair`2", "System.Collections.Generic", t2859_MIs, t2859_PIs, t2859_FIs, NULL, &t110_TI, NULL, NULL, &t2859_TI, NULL, t2859_VT, &t1259__CustomAttributeCache, &t2859_TI, &t2859_0_0_0, &t2859_1_0_0, NULL, &t2859_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2859)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057033, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 2, 0, 0, 4, 0, 0};
#include "t2862.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2862_TI;
#include "t2862MD.h"

extern MethodInfo m15549_MI;
extern MethodInfo m22097_MI;
struct t20;
 t2859  m22097 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m15545_MI;
 void m15545 (t2862 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m15546_MI;
 t29 * m15546 (t2862 * __this, MethodInfo* method){
	{
		t2859  L_0 = m15549(__this, &m15549_MI);
		t2859  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2859_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m15547_MI;
 void m15547 (t2862 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m15548_MI;
 bool m15548 (t2862 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 t2859  m15549 (t2862 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		t2859  L_8 = m22097(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m22097_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>>
extern Il2CppType t20_0_0_1;
FieldInfo t2862_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2862_TI, offsetof(t2862, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2862_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2862_TI, offsetof(t2862, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2862_FIs[] =
{
	&t2862_f0_FieldInfo,
	&t2862_f1_FieldInfo,
	NULL
};
static PropertyInfo t2862____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2862_TI, "System.Collections.IEnumerator.Current", &m15546_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2862____Current_PropertyInfo = 
{
	&t2862_TI, "Current", &m15549_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2862_PIs[] =
{
	&t2862____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2862____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2862_m15545_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15545_GM;
MethodInfo m15545_MI = 
{
	".ctor", (methodPointerType)&m15545, &t2862_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2862_m15545_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15545_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15546_GM;
MethodInfo m15546_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m15546, &t2862_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15546_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15547_GM;
MethodInfo m15547_MI = 
{
	"Dispose", (methodPointerType)&m15547, &t2862_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15547_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15548_GM;
MethodInfo m15548_MI = 
{
	"MoveNext", (methodPointerType)&m15548, &t2862_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15548_GM};
extern Il2CppType t2859_0_0_0;
extern void* RuntimeInvoker_t2859 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15549_GM;
MethodInfo m15549_MI = 
{
	"get_Current", (methodPointerType)&m15549, &t2862_TI, &t2859_0_0_0, RuntimeInvoker_t2859, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15549_GM};
static MethodInfo* t2862_MIs[] =
{
	&m15545_MI,
	&m15546_MI,
	&m15547_MI,
	&m15548_MI,
	&m15549_MI,
	NULL
};
static MethodInfo* t2862_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15546_MI,
	&m15548_MI,
	&m15547_MI,
	&m15549_MI,
};
static TypeInfo* t2862_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2860_TI,
};
static Il2CppInterfaceOffsetPair t2862_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2860_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2862_0_0_0;
extern Il2CppType t2862_1_0_0;
extern Il2CppGenericClass t2862_GC;
TypeInfo t2862_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2862_MIs, t2862_PIs, t2862_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2862_TI, t2862_ITIs, t2862_VT, &EmptyCustomAttributesCache, &t2862_TI, &t2862_0_0_0, &t2862_1_0_0, t2862_IOs, &t2862_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2862)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5572_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>>
extern MethodInfo m29149_MI;
extern MethodInfo m29150_MI;
static PropertyInfo t5572____Item_PropertyInfo = 
{
	&t5572_TI, "Item", &m29149_MI, &m29150_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5572_PIs[] =
{
	&t5572____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2859_0_0_0;
static ParameterInfo t5572_m29151_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2859_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t2859 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29151_GM;
MethodInfo m29151_MI = 
{
	"IndexOf", NULL, &t5572_TI, &t44_0_0_0, RuntimeInvoker_t44_t2859, t5572_m29151_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29151_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2859_0_0_0;
static ParameterInfo t5572_m29152_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t2859_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t2859 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29152_GM;
MethodInfo m29152_MI = 
{
	"Insert", NULL, &t5572_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t2859, t5572_m29152_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29152_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5572_m29153_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29153_GM;
MethodInfo m29153_MI = 
{
	"RemoveAt", NULL, &t5572_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5572_m29153_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29153_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5572_m29149_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2859_0_0_0;
extern void* RuntimeInvoker_t2859_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29149_GM;
MethodInfo m29149_MI = 
{
	"get_Item", NULL, &t5572_TI, &t2859_0_0_0, RuntimeInvoker_t2859_t44, t5572_m29149_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29149_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2859_0_0_0;
static ParameterInfo t5572_m29150_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2859_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t2859 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29150_GM;
MethodInfo m29150_MI = 
{
	"set_Item", NULL, &t5572_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t2859, t5572_m29150_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29150_GM};
static MethodInfo* t5572_MIs[] =
{
	&m29151_MI,
	&m29152_MI,
	&m29153_MI,
	&m29149_MI,
	&m29150_MI,
	NULL
};
static TypeInfo* t5572_ITIs[] = 
{
	&t603_TI,
	&t5571_TI,
	&t5573_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5572_0_0_0;
extern Il2CppType t5572_1_0_0;
struct t5572;
extern Il2CppGenericClass t5572_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5572_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5572_MIs, t5572_PIs, NULL, NULL, NULL, NULL, NULL, &t5572_TI, t5572_ITIs, NULL, &t1908__CustomAttributeCache, &t5572_TI, &t5572_0_0_0, &t5572_1_0_0, NULL, &t5572_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IDictionary`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>
extern Il2CppType t44_0_0_0;
static ParameterInfo t6716_m29154_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29154_GM;
MethodInfo m29154_MI = 
{
	"Remove", NULL, &t6716_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6716_m29154_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29154_GM};
static MethodInfo* t6716_MIs[] =
{
	&m29154_MI,
	NULL
};
static TypeInfo* t6716_ITIs[] = 
{
	&t603_TI,
	&t5571_TI,
	&t5573_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6716_0_0_0;
extern Il2CppType t6716_1_0_0;
struct t6716;
extern Il2CppGenericClass t6716_GC;
extern CustomAttributesCache t1975__CustomAttributeCache;
TypeInfo t6716_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IDictionary`2", "System.Collections.Generic", t6716_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6716_TI, t6716_ITIs, NULL, &t1975__CustomAttributeCache, &t6716_TI, &t6716_0_0_0, &t6716_1_0_0, NULL, &t6716_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2864_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.GUILayoutUtility/LayoutCache>
extern MethodInfo m29155_MI;
static PropertyInfo t2864____Current_PropertyInfo = 
{
	&t2864_TI, "Current", &m29155_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2864_PIs[] =
{
	&t2864____Current_PropertyInfo,
	NULL
};
extern Il2CppType t469_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29155_GM;
MethodInfo m29155_MI = 
{
	"get_Current", NULL, &t2864_TI, &t469_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29155_GM};
static MethodInfo* t2864_MIs[] =
{
	&m29155_MI,
	NULL
};
static TypeInfo* t2864_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2864_0_0_0;
extern Il2CppType t2864_1_0_0;
struct t2864;
extern Il2CppGenericClass t2864_GC;
TypeInfo t2864_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2864_MIs, t2864_PIs, NULL, NULL, NULL, NULL, NULL, &t2864_TI, t2864_ITIs, NULL, &EmptyCustomAttributesCache, &t2864_TI, &t2864_0_0_0, &t2864_1_0_0, NULL, &t2864_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2863.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2863_TI;
#include "t2863MD.h"

extern MethodInfo m15554_MI;
extern MethodInfo m22108_MI;
struct t20;
#define m22108(__this, p0, method) (t469 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.GUILayoutUtility/LayoutCache>
extern Il2CppType t20_0_0_1;
FieldInfo t2863_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2863_TI, offsetof(t2863, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2863_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2863_TI, offsetof(t2863, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2863_FIs[] =
{
	&t2863_f0_FieldInfo,
	&t2863_f1_FieldInfo,
	NULL
};
extern MethodInfo m15551_MI;
static PropertyInfo t2863____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2863_TI, "System.Collections.IEnumerator.Current", &m15551_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2863____Current_PropertyInfo = 
{
	&t2863_TI, "Current", &m15554_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2863_PIs[] =
{
	&t2863____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2863____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2863_m15550_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15550_GM;
MethodInfo m15550_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2863_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2863_m15550_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15550_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15551_GM;
MethodInfo m15551_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2863_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15551_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15552_GM;
MethodInfo m15552_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2863_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15552_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15553_GM;
MethodInfo m15553_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2863_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15553_GM};
extern Il2CppType t469_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15554_GM;
MethodInfo m15554_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2863_TI, &t469_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15554_GM};
static MethodInfo* t2863_MIs[] =
{
	&m15550_MI,
	&m15551_MI,
	&m15552_MI,
	&m15553_MI,
	&m15554_MI,
	NULL
};
extern MethodInfo m15553_MI;
extern MethodInfo m15552_MI;
static MethodInfo* t2863_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15551_MI,
	&m15553_MI,
	&m15552_MI,
	&m15554_MI,
};
static TypeInfo* t2863_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2864_TI,
};
static Il2CppInterfaceOffsetPair t2863_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2864_TI, 7},
};
extern TypeInfo t469_TI;
static Il2CppRGCTXData t2863_RGCTXData[3] = 
{
	&m15554_MI/* Method Usage */,
	&t469_TI/* Class Usage */,
	&m22108_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2863_0_0_0;
extern Il2CppType t2863_1_0_0;
extern Il2CppGenericClass t2863_GC;
TypeInfo t2863_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2863_MIs, t2863_PIs, t2863_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2863_TI, t2863_ITIs, t2863_VT, &EmptyCustomAttributesCache, &t2863_TI, &t2863_0_0_0, &t2863_1_0_0, t2863_IOs, &t2863_GC, NULL, NULL, NULL, t2863_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2863)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5574_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.GUILayoutUtility/LayoutCache>
extern MethodInfo m29156_MI;
static PropertyInfo t5574____Count_PropertyInfo = 
{
	&t5574_TI, "Count", &m29156_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29157_MI;
static PropertyInfo t5574____IsReadOnly_PropertyInfo = 
{
	&t5574_TI, "IsReadOnly", &m29157_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5574_PIs[] =
{
	&t5574____Count_PropertyInfo,
	&t5574____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29156_GM;
MethodInfo m29156_MI = 
{
	"get_Count", NULL, &t5574_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29156_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29157_GM;
MethodInfo m29157_MI = 
{
	"get_IsReadOnly", NULL, &t5574_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29157_GM};
extern Il2CppType t469_0_0_0;
static ParameterInfo t5574_m29158_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t469_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29158_GM;
MethodInfo m29158_MI = 
{
	"Add", NULL, &t5574_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5574_m29158_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29158_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29159_GM;
MethodInfo m29159_MI = 
{
	"Clear", NULL, &t5574_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29159_GM};
extern Il2CppType t469_0_0_0;
static ParameterInfo t5574_m29160_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t469_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29160_GM;
MethodInfo m29160_MI = 
{
	"Contains", NULL, &t5574_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5574_m29160_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29160_GM};
extern Il2CppType t2855_0_0_0;
extern Il2CppType t2855_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5574_m29161_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2855_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29161_GM;
MethodInfo m29161_MI = 
{
	"CopyTo", NULL, &t5574_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5574_m29161_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29161_GM};
extern Il2CppType t469_0_0_0;
static ParameterInfo t5574_m29162_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t469_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29162_GM;
MethodInfo m29162_MI = 
{
	"Remove", NULL, &t5574_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5574_m29162_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29162_GM};
static MethodInfo* t5574_MIs[] =
{
	&m29156_MI,
	&m29157_MI,
	&m29158_MI,
	&m29159_MI,
	&m29160_MI,
	&m29161_MI,
	&m29162_MI,
	NULL
};
extern TypeInfo t5576_TI;
static TypeInfo* t5574_ITIs[] = 
{
	&t603_TI,
	&t5576_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5574_0_0_0;
extern Il2CppType t5574_1_0_0;
struct t5574;
extern Il2CppGenericClass t5574_GC;
TypeInfo t5574_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5574_MIs, t5574_PIs, NULL, NULL, NULL, NULL, NULL, &t5574_TI, t5574_ITIs, NULL, &EmptyCustomAttributesCache, &t5574_TI, &t5574_0_0_0, &t5574_1_0_0, NULL, &t5574_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.GUILayoutUtility/LayoutCache>
extern Il2CppType t2864_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29163_GM;
MethodInfo m29163_MI = 
{
	"GetEnumerator", NULL, &t5576_TI, &t2864_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29163_GM};
static MethodInfo* t5576_MIs[] =
{
	&m29163_MI,
	NULL
};
static TypeInfo* t5576_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5576_0_0_0;
extern Il2CppType t5576_1_0_0;
struct t5576;
extern Il2CppGenericClass t5576_GC;
TypeInfo t5576_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5576_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5576_TI, t5576_ITIs, NULL, &EmptyCustomAttributesCache, &t5576_TI, &t5576_0_0_0, &t5576_1_0_0, NULL, &t5576_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5575_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.GUILayoutUtility/LayoutCache>
extern MethodInfo m29164_MI;
extern MethodInfo m29165_MI;
static PropertyInfo t5575____Item_PropertyInfo = 
{
	&t5575_TI, "Item", &m29164_MI, &m29165_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5575_PIs[] =
{
	&t5575____Item_PropertyInfo,
	NULL
};
extern Il2CppType t469_0_0_0;
static ParameterInfo t5575_m29166_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t469_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29166_GM;
MethodInfo m29166_MI = 
{
	"IndexOf", NULL, &t5575_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5575_m29166_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29166_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t469_0_0_0;
static ParameterInfo t5575_m29167_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t469_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29167_GM;
MethodInfo m29167_MI = 
{
	"Insert", NULL, &t5575_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5575_m29167_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29167_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5575_m29168_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29168_GM;
MethodInfo m29168_MI = 
{
	"RemoveAt", NULL, &t5575_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5575_m29168_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29168_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5575_m29164_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t469_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29164_GM;
MethodInfo m29164_MI = 
{
	"get_Item", NULL, &t5575_TI, &t469_0_0_0, RuntimeInvoker_t29_t44, t5575_m29164_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29164_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t469_0_0_0;
static ParameterInfo t5575_m29165_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t469_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29165_GM;
MethodInfo m29165_MI = 
{
	"set_Item", NULL, &t5575_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5575_m29165_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29165_GM};
static MethodInfo* t5575_MIs[] =
{
	&m29166_MI,
	&m29167_MI,
	&m29168_MI,
	&m29164_MI,
	&m29165_MI,
	NULL
};
static TypeInfo* t5575_ITIs[] = 
{
	&t603_TI,
	&t5574_TI,
	&t5576_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5575_0_0_0;
extern Il2CppType t5575_1_0_0;
struct t5575;
extern Il2CppGenericClass t5575_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5575_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5575_MIs, t5575_PIs, NULL, NULL, NULL, NULL, NULL, &t5575_TI, t5575_ITIs, NULL, &t1908__CustomAttributeCache, &t5575_TI, &t5575_0_0_0, &t5575_1_0_0, NULL, &t5575_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif

#include "t345.h"
#include "t2865.h"
#include "t2866.h"
extern TypeInfo t345_TI;
extern TypeInfo t2865_TI;
extern TypeInfo t2866_TI;
#include "t345MD.h"
#include "t2866MD.h"
#include "t2865MD.h"
extern MethodInfo m9816_MI;
extern MethodInfo m3988_MI;
extern MethodInfo m15567_MI;
extern MethodInfo m15566_MI;
extern MethodInfo m15586_MI;
extern MethodInfo m22119_MI;
extern MethodInfo m22120_MI;
extern MethodInfo m15569_MI;
struct t472;
 void m22119 (t472 * __this, t20 * p0, int32_t p1, t2866 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t472;
 void m22120 (t472 * __this, t2855* p0, int32_t p1, t2866 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;


 void m15555 (t2857 * __this, t472 * p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		if (p0)
		{
			goto IL_0014;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1173, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0014:
	{
		__this->f0 = p0;
		return;
	}
}
extern MethodInfo m15556_MI;
 void m15556 (t2857 * __this, t469 * p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral1174, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m15557_MI;
 void m15557 (t2857 * __this, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral1174, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m15558_MI;
 bool m15558 (t2857 * __this, t469 * p0, MethodInfo* method){
	{
		t472 * L_0 = (__this->f0);
		bool L_1 = m15529(L_0, p0, &m15529_MI);
		return L_1;
	}
}
extern MethodInfo m15559_MI;
 bool m15559 (t2857 * __this, t469 * p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral1174, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m15560_MI;
 t29* m15560 (t2857 * __this, MethodInfo* method){
	{
		t2865  L_0 = m15567(__this, &m15567_MI);
		t2865  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2865_TI), &L_1);
		return (t29*)L_2;
	}
}
extern MethodInfo m15561_MI;
 void m15561 (t2857 * __this, t20 * p0, int32_t p1, MethodInfo* method){
	t2855* V_0 = {0};
	{
		V_0 = ((t2855*)IsInst(p0, InitializedTypeInfo(&t2855_TI)));
		if (!V_0)
		{
			goto IL_0013;
		}
	}
	{
		VirtActionInvoker2< t2855*, int32_t >::Invoke(&m15566_MI, __this, V_0, p1);
		return;
	}

IL_0013:
	{
		t472 * L_0 = (__this->f0);
		m15521(L_0, p0, p1, &m15521_MI);
		t472 * L_1 = (__this->f0);
		t35 L_2 = { &m15523_MI };
		t2866 * L_3 = (t2866 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2866_TI));
		m15586(L_3, NULL, L_2, &m15586_MI);
		m22119(L_1, p0, p1, L_3, &m22119_MI);
		return;
	}
}
extern MethodInfo m15562_MI;
 t29 * m15562 (t2857 * __this, MethodInfo* method){
	{
		t2865  L_0 = m15567(__this, &m15567_MI);
		t2865  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2865_TI), &L_1);
		return (t29 *)L_2;
	}
}
extern MethodInfo m15563_MI;
 bool m15563 (t2857 * __this, MethodInfo* method){
	{
		return 1;
	}
}
extern MethodInfo m15564_MI;
 bool m15564 (t2857 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m15565_MI;
 t29 * m15565 (t2857 * __this, MethodInfo* method){
	{
		t472 * L_0 = (__this->f0);
		t29 * L_1 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m9816_MI, L_0);
		return L_1;
	}
}
 void m15566 (t2857 * __this, t2855* p0, int32_t p1, MethodInfo* method){
	{
		t472 * L_0 = (__this->f0);
		m15521(L_0, (t20 *)(t20 *)p0, p1, &m15521_MI);
		t472 * L_1 = (__this->f0);
		t35 L_2 = { &m15523_MI };
		t2866 * L_3 = (t2866 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2866_TI));
		m15586(L_3, NULL, L_2, &m15586_MI);
		m22120(L_1, p0, p1, L_3, &m22120_MI);
		return;
	}
}
 t2865  m15567 (t2857 * __this, MethodInfo* method){
	{
		t472 * L_0 = (__this->f0);
		t2865  L_1 = {0};
		m15569(&L_1, L_0, &m15569_MI);
		return L_1;
	}
}
extern MethodInfo m15568_MI;
 int32_t m15568 (t2857 * __this, MethodInfo* method){
	{
		t472 * L_0 = (__this->f0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m15517_MI, L_0);
		return L_1;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>
extern Il2CppType t472_0_0_1;
FieldInfo t2857_f0_FieldInfo = 
{
	"dictionary", &t472_0_0_1, &t2857_TI, offsetof(t2857, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2857_FIs[] =
{
	&t2857_f0_FieldInfo,
	NULL
};
static PropertyInfo t2857____System_Collections_Generic_ICollectionU3CTValueU3E_IsReadOnly_PropertyInfo = 
{
	&t2857_TI, "System.Collections.Generic.ICollection<TValue>.IsReadOnly", &m15563_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2857____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2857_TI, "System.Collections.ICollection.IsSynchronized", &m15564_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2857____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2857_TI, "System.Collections.ICollection.SyncRoot", &m15565_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2857____Count_PropertyInfo = 
{
	&t2857_TI, "Count", &m15568_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2857_PIs[] =
{
	&t2857____System_Collections_Generic_ICollectionU3CTValueU3E_IsReadOnly_PropertyInfo,
	&t2857____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2857____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2857____Count_PropertyInfo,
	NULL
};
extern Il2CppType t472_0_0_0;
static ParameterInfo t2857_m15555_ParameterInfos[] = 
{
	{"dictionary", 0, 134217728, &EmptyCustomAttributesCache, &t472_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15555_GM;
MethodInfo m15555_MI = 
{
	".ctor", (methodPointerType)&m15555, &t2857_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2857_m15555_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15555_GM};
extern Il2CppType t469_0_0_0;
static ParameterInfo t2857_m15556_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t469_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15556_GM;
MethodInfo m15556_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Add", (methodPointerType)&m15556, &t2857_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2857_m15556_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15556_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15557_GM;
MethodInfo m15557_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Clear", (methodPointerType)&m15557, &t2857_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 12, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15557_GM};
extern Il2CppType t469_0_0_0;
static ParameterInfo t2857_m15558_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t469_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15558_GM;
MethodInfo m15558_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Contains", (methodPointerType)&m15558, &t2857_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2857_m15558_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15558_GM};
extern Il2CppType t469_0_0_0;
static ParameterInfo t2857_m15559_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t469_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15559_GM;
MethodInfo m15559_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Remove", (methodPointerType)&m15559, &t2857_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2857_m15559_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15559_GM};
extern Il2CppType t2864_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15560_GM;
MethodInfo m15560_MI = 
{
	"System.Collections.Generic.IEnumerable<TValue>.GetEnumerator", (methodPointerType)&m15560, &t2857_TI, &t2864_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 16, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15560_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2857_m15561_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15561_GM;
MethodInfo m15561_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m15561, &t2857_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2857_m15561_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15561_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15562_GM;
MethodInfo m15562_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m15562, &t2857_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15562_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15563_GM;
MethodInfo m15563_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.get_IsReadOnly", (methodPointerType)&m15563, &t2857_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15563_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15564_GM;
MethodInfo m15564_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m15564, &t2857_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15564_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15565_GM;
MethodInfo m15565_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m15565, &t2857_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15565_GM};
extern Il2CppType t2855_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2857_m15566_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2855_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15566_GM;
MethodInfo m15566_MI = 
{
	"CopyTo", (methodPointerType)&m15566, &t2857_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2857_m15566_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 14, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15566_GM};
extern Il2CppType t2865_0_0_0;
extern void* RuntimeInvoker_t2865 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15567_GM;
MethodInfo m15567_MI = 
{
	"GetEnumerator", (methodPointerType)&m15567, &t2857_TI, &t2865_0_0_0, RuntimeInvoker_t2865, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15567_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15568_GM;
MethodInfo m15568_MI = 
{
	"get_Count", (methodPointerType)&m15568, &t2857_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15568_GM};
static MethodInfo* t2857_MIs[] =
{
	&m15555_MI,
	&m15556_MI,
	&m15557_MI,
	&m15558_MI,
	&m15559_MI,
	&m15560_MI,
	&m15561_MI,
	&m15562_MI,
	&m15563_MI,
	&m15564_MI,
	&m15565_MI,
	&m15566_MI,
	&m15567_MI,
	&m15568_MI,
	NULL
};
static MethodInfo* t2857_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15562_MI,
	&m15568_MI,
	&m15564_MI,
	&m15565_MI,
	&m15561_MI,
	&m15568_MI,
	&m15563_MI,
	&m15556_MI,
	&m15557_MI,
	&m15558_MI,
	&m15566_MI,
	&m15559_MI,
	&m15560_MI,
};
static TypeInfo* t2857_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t5574_TI,
	&t5576_TI,
};
static Il2CppInterfaceOffsetPair t2857_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t5574_TI, 9},
	{ &t5576_TI, 16},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2857_0_0_0;
extern Il2CppType t2857_1_0_0;
struct t2857;
extern Il2CppGenericClass t2857_GC;
extern TypeInfo t1254_TI;
extern CustomAttributesCache t1252__CustomAttributeCache;
TypeInfo t2857_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ValueCollection", "", t2857_MIs, t2857_PIs, t2857_FIs, NULL, &t29_TI, NULL, &t1254_TI, &t2857_TI, t2857_ITIs, t2857_VT, &t1252__CustomAttributeCache, &t2857_TI, &t2857_0_0_0, &t2857_1_0_0, t2857_IOs, &t2857_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2857), 0, -1, 0, 0, -1, 1057026, 0, false, false, false, false, true, false, false, false, false, false, false, false, 14, 4, 1, 0, 0, 17, 4, 4};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m15582_MI;
extern MethodInfo m15585_MI;
extern MethodInfo m15579_MI;


 void m15569 (t2865 * __this, t472 * p0, MethodInfo* method){
	{
		t2861  L_0 = m15537(p0, &m15537_MI);
		__this->f0 = L_0;
		return;
	}
}
extern MethodInfo m15570_MI;
 t29 * m15570 (t2865 * __this, MethodInfo* method){
	{
		t2861 * L_0 = &(__this->f0);
		t469 * L_1 = m15582(L_0, &m15582_MI);
		t469 * L_2 = L_1;
		return ((t469 *)L_2);
	}
}
extern MethodInfo m15571_MI;
 void m15571 (t2865 * __this, MethodInfo* method){
	{
		t2861 * L_0 = &(__this->f0);
		m15585(L_0, &m15585_MI);
		return;
	}
}
extern MethodInfo m15572_MI;
 bool m15572 (t2865 * __this, MethodInfo* method){
	{
		t2861 * L_0 = &(__this->f0);
		bool L_1 = m15579(L_0, &m15579_MI);
		return L_1;
	}
}
extern MethodInfo m15573_MI;
 t469 * m15573 (t2865 * __this, MethodInfo* method){
	{
		t2861 * L_0 = &(__this->f0);
		t2859 * L_1 = &(L_0->f3);
		t469 * L_2 = m15542(L_1, &m15542_MI);
		return L_2;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>
extern Il2CppType t2861_0_0_1;
FieldInfo t2865_f0_FieldInfo = 
{
	"host_enumerator", &t2861_0_0_1, &t2865_TI, offsetof(t2865, f0) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2865_FIs[] =
{
	&t2865_f0_FieldInfo,
	NULL
};
static PropertyInfo t2865____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2865_TI, "System.Collections.IEnumerator.Current", &m15570_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2865____Current_PropertyInfo = 
{
	&t2865_TI, "Current", &m15573_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2865_PIs[] =
{
	&t2865____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2865____Current_PropertyInfo,
	NULL
};
extern Il2CppType t472_0_0_0;
static ParameterInfo t2865_m15569_ParameterInfos[] = 
{
	{"host", 0, 134217728, &EmptyCustomAttributesCache, &t472_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15569_GM;
MethodInfo m15569_MI = 
{
	".ctor", (methodPointerType)&m15569, &t2865_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2865_m15569_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15569_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15570_GM;
MethodInfo m15570_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m15570, &t2865_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15570_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15571_GM;
MethodInfo m15571_MI = 
{
	"Dispose", (methodPointerType)&m15571, &t2865_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15571_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15572_GM;
MethodInfo m15572_MI = 
{
	"MoveNext", (methodPointerType)&m15572, &t2865_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15572_GM};
extern Il2CppType t469_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15573_GM;
MethodInfo m15573_MI = 
{
	"get_Current", (methodPointerType)&m15573, &t2865_TI, &t469_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15573_GM};
static MethodInfo* t2865_MIs[] =
{
	&m15569_MI,
	&m15570_MI,
	&m15571_MI,
	&m15572_MI,
	&m15573_MI,
	NULL
};
static MethodInfo* t2865_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15570_MI,
	&m15572_MI,
	&m15571_MI,
	&m15573_MI,
};
static TypeInfo* t2865_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2864_TI,
};
static Il2CppInterfaceOffsetPair t2865_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2864_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2865_0_0_0;
extern Il2CppType t2865_1_0_0;
extern Il2CppGenericClass t2865_GC;
extern TypeInfo t1252_TI;
TypeInfo t2865_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t2865_MIs, t2865_PIs, t2865_FIs, NULL, &t110_TI, NULL, &t1252_TI, &t2865_TI, t2865_ITIs, t2865_VT, &EmptyCustomAttributesCache, &t2865_TI, &t2865_0_0_0, &t2865_1_0_0, t2865_IOs, &t2865_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2865)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 1, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif

#include "t1101.h"
extern TypeInfo t1101_TI;
#include "t1101MD.h"
extern MethodInfo m15584_MI;
extern MethodInfo m15581_MI;
extern MethodInfo m15583_MI;
extern MethodInfo m5150_MI;


 void m15574 (t2861 * __this, t472 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		int32_t L_0 = (p0->f14);
		__this->f2 = L_0;
		return;
	}
}
extern MethodInfo m15575_MI;
 t29 * m15575 (t2861 * __this, MethodInfo* method){
	{
		m15584(__this, &m15584_MI);
		t2859  L_0 = (__this->f3);
		t2859  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2859_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m15576_MI;
 t725  m15576 (t2861 * __this, MethodInfo* method){
	{
		m15584(__this, &m15584_MI);
		t2859 * L_0 = &(__this->f3);
		int32_t L_1 = m15540(L_0, &m15540_MI);
		int32_t L_2 = L_1;
		t29 * L_3 = Box(InitializedTypeInfo(&t44_TI), &L_2);
		t2859 * L_4 = &(__this->f3);
		t469 * L_5 = m15542(L_4, &m15542_MI);
		t469 * L_6 = L_5;
		t725  L_7 = {0};
		m3965(&L_7, L_3, ((t469 *)L_6), &m3965_MI);
		return L_7;
	}
}
extern MethodInfo m15577_MI;
 t29 * m15577 (t2861 * __this, MethodInfo* method){
	{
		int32_t L_0 = m15581(__this, &m15581_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t44_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m15578_MI;
 t29 * m15578 (t2861 * __this, MethodInfo* method){
	{
		t469 * L_0 = m15582(__this, &m15582_MI);
		t469 * L_1 = L_0;
		return ((t469 *)L_1);
	}
}
 bool m15579 (t2861 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		m15583(__this, &m15583_MI);
		int32_t L_0 = (__this->f1);
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0011;
		}
	}
	{
		return 0;
	}

IL_0011:
	{
		goto IL_0072;
	}

IL_0013:
	{
		int32_t L_1 = (__this->f1);
		int32_t L_2 = L_1;
		V_1 = L_2;
		__this->f1 = ((int32_t)(L_2+1));
		V_0 = V_1;
		t472 * L_3 = (__this->f0);
		t1965* L_4 = (L_3->f5);
		int32_t L_5 = (((t1248 *)(t1248 *)SZArrayLdElema(L_4, V_0))->f0);
		if (!((int32_t)((int32_t)L_5&(int32_t)((int32_t)-2147483648))))
		{
			goto IL_0072;
		}
	}
	{
		t472 * L_6 = (__this->f0);
		t841* L_7 = (L_6->f6);
		int32_t L_8 = V_0;
		t472 * L_9 = (__this->f0);
		t2855* L_10 = (L_9->f7);
		int32_t L_11 = V_0;
		t2859  L_12 = {0};
		m15539(&L_12, (*(int32_t*)(int32_t*)SZArrayLdElema(L_7, L_8)), (*(t469 **)(t469 **)SZArrayLdElema(L_10, L_11)), &m15539_MI);
		__this->f3 = L_12;
		return 1;
	}

IL_0072:
	{
		int32_t L_13 = (__this->f1);
		t472 * L_14 = (__this->f0);
		int32_t L_15 = (L_14->f8);
		if ((((int32_t)L_13) < ((int32_t)L_15)))
		{
			goto IL_0013;
		}
	}
	{
		__this->f1 = (-1);
		return 0;
	}
}
extern MethodInfo m15580_MI;
 t2859  m15580 (t2861 * __this, MethodInfo* method){
	{
		t2859  L_0 = (__this->f3);
		return L_0;
	}
}
 int32_t m15581 (t2861 * __this, MethodInfo* method){
	{
		m15584(__this, &m15584_MI);
		t2859 * L_0 = &(__this->f3);
		int32_t L_1 = m15540(L_0, &m15540_MI);
		return L_1;
	}
}
 t469 * m15582 (t2861 * __this, MethodInfo* method){
	{
		m15584(__this, &m15584_MI);
		t2859 * L_0 = &(__this->f3);
		t469 * L_1 = m15542(L_0, &m15542_MI);
		return L_1;
	}
}
 void m15583 (t2861 * __this, MethodInfo* method){
	{
		t472 * L_0 = (__this->f0);
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		t1101 * L_1 = (t1101 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1101_TI));
		m5150(L_1, (t7*)NULL, &m5150_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_000f:
	{
		t472 * L_2 = (__this->f0);
		int32_t L_3 = (L_2->f14);
		int32_t L_4 = (__this->f2);
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_002d;
		}
	}
	{
		t914 * L_5 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_5, (t7*) &_stringLiteral1171, &m3964_MI);
		il2cpp_codegen_raise_exception(L_5);
	}

IL_002d:
	{
		return;
	}
}
 void m15584 (t2861 * __this, MethodInfo* method){
	{
		m15583(__this, &m15583_MI);
		int32_t L_0 = (__this->f1);
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001a;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1172, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_001a:
	{
		return;
	}
}
 void m15585 (t2861 * __this, MethodInfo* method){
	{
		__this->f0 = (t472 *)NULL;
		return;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>
extern Il2CppType t472_0_0_1;
FieldInfo t2861_f0_FieldInfo = 
{
	"dictionary", &t472_0_0_1, &t2861_TI, offsetof(t2861, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2861_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t2861_TI, offsetof(t2861, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2861_f2_FieldInfo = 
{
	"stamp", &t44_0_0_1, &t2861_TI, offsetof(t2861, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t2859_0_0_3;
FieldInfo t2861_f3_FieldInfo = 
{
	"current", &t2859_0_0_3, &t2861_TI, offsetof(t2861, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2861_FIs[] =
{
	&t2861_f0_FieldInfo,
	&t2861_f1_FieldInfo,
	&t2861_f2_FieldInfo,
	&t2861_f3_FieldInfo,
	NULL
};
static PropertyInfo t2861____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2861_TI, "System.Collections.IEnumerator.Current", &m15575_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2861____System_Collections_IDictionaryEnumerator_Entry_PropertyInfo = 
{
	&t2861_TI, "System.Collections.IDictionaryEnumerator.Entry", &m15576_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2861____System_Collections_IDictionaryEnumerator_Key_PropertyInfo = 
{
	&t2861_TI, "System.Collections.IDictionaryEnumerator.Key", &m15577_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2861____System_Collections_IDictionaryEnumerator_Value_PropertyInfo = 
{
	&t2861_TI, "System.Collections.IDictionaryEnumerator.Value", &m15578_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2861____Current_PropertyInfo = 
{
	&t2861_TI, "Current", &m15580_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2861____CurrentKey_PropertyInfo = 
{
	&t2861_TI, "CurrentKey", &m15581_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2861____CurrentValue_PropertyInfo = 
{
	&t2861_TI, "CurrentValue", &m15582_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2861_PIs[] =
{
	&t2861____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2861____System_Collections_IDictionaryEnumerator_Entry_PropertyInfo,
	&t2861____System_Collections_IDictionaryEnumerator_Key_PropertyInfo,
	&t2861____System_Collections_IDictionaryEnumerator_Value_PropertyInfo,
	&t2861____Current_PropertyInfo,
	&t2861____CurrentKey_PropertyInfo,
	&t2861____CurrentValue_PropertyInfo,
	NULL
};
extern Il2CppType t472_0_0_0;
static ParameterInfo t2861_m15574_ParameterInfos[] = 
{
	{"dictionary", 0, 134217728, &EmptyCustomAttributesCache, &t472_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15574_GM;
MethodInfo m15574_MI = 
{
	".ctor", (methodPointerType)&m15574, &t2861_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2861_m15574_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15574_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15575_GM;
MethodInfo m15575_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m15575, &t2861_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15575_GM};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15576_GM;
MethodInfo m15576_MI = 
{
	"System.Collections.IDictionaryEnumerator.get_Entry", (methodPointerType)&m15576, &t2861_TI, &t725_0_0_0, RuntimeInvoker_t725, NULL, &EmptyCustomAttributesCache, 2529, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15576_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15577_GM;
MethodInfo m15577_MI = 
{
	"System.Collections.IDictionaryEnumerator.get_Key", (methodPointerType)&m15577, &t2861_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15577_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15578_GM;
MethodInfo m15578_MI = 
{
	"System.Collections.IDictionaryEnumerator.get_Value", (methodPointerType)&m15578, &t2861_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15578_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15579_GM;
MethodInfo m15579_MI = 
{
	"MoveNext", (methodPointerType)&m15579, &t2861_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15579_GM};
extern Il2CppType t2859_0_0_0;
extern void* RuntimeInvoker_t2859 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15580_GM;
MethodInfo m15580_MI = 
{
	"get_Current", (methodPointerType)&m15580, &t2861_TI, &t2859_0_0_0, RuntimeInvoker_t2859, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15580_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15581_GM;
MethodInfo m15581_MI = 
{
	"get_CurrentKey", (methodPointerType)&m15581, &t2861_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2179, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15581_GM};
extern Il2CppType t469_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15582_GM;
MethodInfo m15582_MI = 
{
	"get_CurrentValue", (methodPointerType)&m15582, &t2861_TI, &t469_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2179, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15582_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15583_GM;
MethodInfo m15583_MI = 
{
	"VerifyState", (methodPointerType)&m15583, &t2861_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15583_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15584_GM;
MethodInfo m15584_MI = 
{
	"VerifyCurrent", (methodPointerType)&m15584, &t2861_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15584_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15585_GM;
MethodInfo m15585_MI = 
{
	"Dispose", (methodPointerType)&m15585, &t2861_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15585_GM};
static MethodInfo* t2861_MIs[] =
{
	&m15574_MI,
	&m15575_MI,
	&m15576_MI,
	&m15577_MI,
	&m15578_MI,
	&m15579_MI,
	&m15580_MI,
	&m15581_MI,
	&m15582_MI,
	&m15583_MI,
	&m15584_MI,
	&m15585_MI,
	NULL
};
static MethodInfo* t2861_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15575_MI,
	&m15579_MI,
	&m15585_MI,
	&m15580_MI,
	&m15576_MI,
	&m15577_MI,
	&m15578_MI,
};
extern TypeInfo t722_TI;
static TypeInfo* t2861_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2860_TI,
	&t722_TI,
};
static Il2CppInterfaceOffsetPair t2861_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2860_TI, 7},
	{ &t722_TI, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2861_0_0_0;
extern Il2CppType t2861_1_0_0;
extern Il2CppGenericClass t2861_GC;
TypeInfo t2861_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t2861_MIs, t2861_PIs, t2861_FIs, NULL, &t110_TI, NULL, &t1254_TI, &t2861_TI, t2861_ITIs, t2861_VT, &EmptyCustomAttributesCache, &t2861_TI, &t2861_0_0_0, &t2861_1_0_0, t2861_IOs, &t2861_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2861)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 12, 7, 4, 0, 0, 11, 4, 4};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
