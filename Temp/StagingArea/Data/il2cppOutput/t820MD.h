﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t820;
struct t778;
struct t781;
struct t7;
#include "t821.h"
#include "t790.h"

 void m3438 (t820 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3439 (t820 * __this, t778 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3440 (t820 * __this, int32_t p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3441 (t820 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3442 (t820 * __this, t778 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3443 (t820 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3444 (t820 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m3445 (t820 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3446 (t820 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
