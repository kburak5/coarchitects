﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t761;
struct t748;
struct t744;
struct t733;
#include "t735.h"

 void m3173 (t761 * __this, t748 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3174 (t761 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3175 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3176 (t761 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t748 * m3177 (t761 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t744 * m3178 (t761 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t744 * m3179 (t761 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3180 (t761 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
