﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3270;
struct t29;
struct t20;

 void m18146 (t3270 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18147 (t3270 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18148 (t3270 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18149 (t3270 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int8_t m18150 (t3270 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
