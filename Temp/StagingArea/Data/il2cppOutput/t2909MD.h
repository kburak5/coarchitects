﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2909;
struct t29;
struct t485;
struct t2424;
struct t20;
struct t136;
struct t841;
#include "t2915.h"

 void m15957 (t2909 * __this, t485 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15958 (t2909 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15959 (t2909 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15960 (t2909 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15961 (t2909 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m15962 (t2909 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15963 (t2909 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15964 (t2909 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15965 (t2909 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15966 (t2909 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15967 (t2909 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15968 (t2909 * __this, t841* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2915  m15969 (t2909 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m15970 (t2909 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
