﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1202;
struct t1196;
struct t1203;
struct t975;
#include "t1200.h"

 t1196 * m6213 (t29 * __this, t1196 * p0, t1196 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1196 * m6214 (t29 * __this, t1196 * p0, t1196 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6215 (t29 * __this, t1196 * p0, t1196 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6216 (t29 * __this, t1196 * p0, t1196 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6217 (t29 * __this, t1196 * p0, t1196 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m6218 (t29 * __this, t1196 * p0, uint32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m6219 (t29 * __this, t1196 * p0, uint32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1203* m6220 (t29 * __this, t1196 * p0, uint32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1203* m6221 (t29 * __this, t1196 * p0, t1196 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1196 * m6222 (t29 * __this, t1196 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1196 * m6223 (t29 * __this, t1196 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1196 * m6224 (t29 * __this, t1196 * p0, uint32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6225 (t29 * __this, t975* p0, uint32_t p1, uint32_t p2, t975* p3, uint32_t p4, uint32_t p5, t975* p6, uint32_t p7, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6226 (t29 * __this, t975* p0, int32_t p1, int32_t p2, t975* p3, int32_t p4, int32_t p5, t975* p6, int32_t p7, int32_t p8, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m6227 (t29 * __this, t1196 * p0, uint32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1196 * m6228 (t29 * __this, t1196 * p0, t1196 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
