﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t718;
struct t721;
struct t29;
struct t136;
struct t20;
struct t722;

 void m3072 (t718 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3073 (t718 * __this, int32_t p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3074 (t718 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3075 (t718 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3076 (t718 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3077 (t718 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3078 (t718 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3079 (t718 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3080 (t718 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3081 (t718 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3082 (t718 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3083 (t718 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3084 (t718 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3085 (t718 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
