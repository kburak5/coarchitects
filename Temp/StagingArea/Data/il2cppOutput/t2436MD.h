﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2436;
struct t29;
struct t145;

#include "t2001MD.h"
#define m12711(__this, method) (void)m10703_gshared((t2001 *)__this, method)
#define m12712(__this, method) (void)m10704_gshared((t29 *)__this, method)
#define m12713(__this, p0, method) (int32_t)m10705_gshared((t2001 *)__this, (t29 *)p0, method)
#define m12714(__this, p0, p1, method) (bool)m10706_gshared((t2001 *)__this, (t29 *)p0, (t29 *)p1, method)
#define m12715(__this, method) (t2436 *)m10707_gshared((t29 *)__this, method)
