﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1201;
struct t1196;

 void m6207 (t1201 * __this, t1196 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6208 (t1201 * __this, t1196 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1196 * m6209 (t1201 * __this, t1196 * p0, t1196 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1196 * m6210 (t1201 * __this, t1196 * p0, t1196 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1196 * m6211 (t1201 * __this, t1196 * p0, t1196 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1196 * m6212 (t1201 * __this, uint32_t p0, t1196 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
