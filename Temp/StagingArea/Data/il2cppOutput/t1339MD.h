﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1339;
struct t1340;
struct t42;
struct t7;
struct t1142;
struct t638;
struct t29;
struct t631;
struct t316;
struct t633;
struct t295;
#include "t1150.h"
#include "t1343.h"
#include "t1342.h"
#include "t630.h"

 int32_t m7268 (t1339 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1340 * m7269 (t1339 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t638* m7270 (t1339 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t638* m7271 (t1339 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7272 (t1339 * __this, t29 * p0, int32_t p1, t631 * p2, t316* p3, t633 * p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7273 (t1339 * __this, int32_t p0, t631 * p1, t316* p2, t633 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1343  m7274 (t1339 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7275 (t1339 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7276 (t1339 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7277 (t1339 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7278 (t1339 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7279 (t1339 * __this, t42 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m7280 (t1339 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m7281 (t1339 * __this, t42 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1142 * m7282 (t1339 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7283 (t1339 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7284 (t1339 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t295 * m7285 (t1339 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t295 * m7286 (t1339 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
