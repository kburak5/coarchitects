﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t876;
struct t875;
struct t878;

 void m3811 (t876 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3812 (t876 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3813 (t876 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t875 * m3814 (t876 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3815 (t876 * __this, t875 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3816 (t876 * __this, t29 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3817 (t876 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
