﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1109;
struct t7;
struct t733;
#include "t735.h"

 void m6999 (t1109 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7000 (t1109 * __this, t7* p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7001 (t1109 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7002 (t1109 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7003 (t1109 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1109 * m7004 (t1109 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7005 (t1109 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7006 (t1109 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
