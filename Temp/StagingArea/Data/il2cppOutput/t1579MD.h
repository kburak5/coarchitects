﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1579;
struct t781;

 void m8554 (t1579 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8555 (t1579 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8556 (t1579 * __this, t781* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m8557 (t1579 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
