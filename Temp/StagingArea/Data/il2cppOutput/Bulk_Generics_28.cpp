﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t6106_TI;

#include "t327.h"
#include "t44.h"
#include "t21.h"

#include "t20.h"

// Metadata Definition System.Collections.Generic.IList`1<System.ObsoleteAttribute>
extern MethodInfo m31800_MI;
extern MethodInfo m31801_MI;
static PropertyInfo t6106____Item_PropertyInfo = 
{
	&t6106_TI, "Item", &m31800_MI, &m31801_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6106_PIs[] =
{
	&t6106____Item_PropertyInfo,
	NULL
};
extern Il2CppType t327_0_0_0;
extern Il2CppType t327_0_0_0;
static ParameterInfo t6106_m31802_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t327_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31802_GM;
MethodInfo m31802_MI = 
{
	"IndexOf", NULL, &t6106_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6106_m31802_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31802_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t327_0_0_0;
static ParameterInfo t6106_m31803_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t327_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31803_GM;
MethodInfo m31803_MI = 
{
	"Insert", NULL, &t6106_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6106_m31803_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31803_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6106_m31804_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31804_GM;
MethodInfo m31804_MI = 
{
	"RemoveAt", NULL, &t6106_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6106_m31804_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31804_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6106_m31800_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t327_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31800_GM;
MethodInfo m31800_MI = 
{
	"get_Item", NULL, &t6106_TI, &t327_0_0_0, RuntimeInvoker_t29_t44, t6106_m31800_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31800_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t327_0_0_0;
static ParameterInfo t6106_m31801_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t327_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31801_GM;
MethodInfo m31801_MI = 
{
	"set_Item", NULL, &t6106_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6106_m31801_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31801_GM};
static MethodInfo* t6106_MIs[] =
{
	&m31802_MI,
	&m31803_MI,
	&m31804_MI,
	&m31800_MI,
	&m31801_MI,
	NULL
};
extern TypeInfo t603_TI;
extern TypeInfo t6105_TI;
extern TypeInfo t6107_TI;
static TypeInfo* t6106_ITIs[] = 
{
	&t603_TI,
	&t6105_TI,
	&t6107_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6106_0_0_0;
extern Il2CppType t6106_1_0_0;
struct t6106;
extern Il2CppGenericClass t6106_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6106_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6106_MIs, t6106_PIs, NULL, NULL, NULL, NULL, NULL, &t6106_TI, t6106_ITIs, NULL, &t1908__CustomAttributeCache, &t6106_TI, &t6106_0_0_0, &t6106_1_0_0, NULL, &t6106_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4712_TI;

#include "t1152.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.DllImportAttribute>
extern MethodInfo m31805_MI;
static PropertyInfo t4712____Current_PropertyInfo = 
{
	&t4712_TI, "Current", &m31805_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4712_PIs[] =
{
	&t4712____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1152_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31805_GM;
MethodInfo m31805_MI = 
{
	"get_Current", NULL, &t4712_TI, &t1152_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31805_GM};
static MethodInfo* t4712_MIs[] =
{
	&m31805_MI,
	NULL
};
extern TypeInfo t136_TI;
extern TypeInfo t324_TI;
static TypeInfo* t4712_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4712_0_0_0;
extern Il2CppType t4712_1_0_0;
struct t4712;
extern Il2CppGenericClass t4712_GC;
TypeInfo t4712_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4712_MIs, t4712_PIs, NULL, NULL, NULL, NULL, NULL, &t4712_TI, t4712_ITIs, NULL, &EmptyCustomAttributesCache, &t4712_TI, &t4712_0_0_0, &t4712_1_0_0, NULL, &t4712_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3303.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3303_TI;
#include "t3303MD.h"

#include "t29.h"
#include "t7.h"
#include "t914.h"
#include "t40.h"
extern TypeInfo t1152_TI;
extern TypeInfo t914_TI;
#include "t914MD.h"
#include "t20MD.h"
extern MethodInfo m18401_MI;
extern MethodInfo m3964_MI;
extern MethodInfo m3969_MI;
extern MethodInfo m24322_MI;
struct t20;
#include "t915.h"
struct t20;
 t29 * m19490_gshared (t20 * __this, int32_t p0, MethodInfo* method);
#define m19490(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
#define m24322(__this, p0, method) (t1152 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices.DllImportAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3303_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3303_TI, offsetof(t3303, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3303_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3303_TI, offsetof(t3303, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3303_FIs[] =
{
	&t3303_f0_FieldInfo,
	&t3303_f1_FieldInfo,
	NULL
};
extern MethodInfo m18398_MI;
static PropertyInfo t3303____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3303_TI, "System.Collections.IEnumerator.Current", &m18398_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3303____Current_PropertyInfo = 
{
	&t3303_TI, "Current", &m18401_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3303_PIs[] =
{
	&t3303____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3303____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
static ParameterInfo t3303_m18397_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18397_GM;
MethodInfo m18397_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3303_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3303_m18397_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18397_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18398_GM;
MethodInfo m18398_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3303_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18398_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18399_GM;
MethodInfo m18399_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3303_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18399_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18400_GM;
MethodInfo m18400_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3303_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18400_GM};
extern Il2CppType t1152_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18401_GM;
MethodInfo m18401_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3303_TI, &t1152_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18401_GM};
static MethodInfo* t3303_MIs[] =
{
	&m18397_MI,
	&m18398_MI,
	&m18399_MI,
	&m18400_MI,
	&m18401_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
extern MethodInfo m18400_MI;
extern MethodInfo m18399_MI;
static MethodInfo* t3303_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18398_MI,
	&m18400_MI,
	&m18399_MI,
	&m18401_MI,
};
static TypeInfo* t3303_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4712_TI,
};
static Il2CppInterfaceOffsetPair t3303_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4712_TI, 7},
};
extern TypeInfo t1152_TI;
static Il2CppRGCTXData t3303_RGCTXData[3] = 
{
	&m18401_MI/* Method Usage */,
	&t1152_TI/* Class Usage */,
	&m24322_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3303_0_0_0;
extern Il2CppType t3303_1_0_0;
extern TypeInfo t110_TI;
extern Il2CppGenericClass t3303_GC;
extern TypeInfo t20_TI;
TypeInfo t3303_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3303_MIs, t3303_PIs, t3303_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3303_TI, t3303_ITIs, t3303_VT, &EmptyCustomAttributesCache, &t3303_TI, &t3303_0_0_0, &t3303_1_0_0, t3303_IOs, &t3303_GC, NULL, NULL, NULL, t3303_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3303)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6108_TI;

#include "mscorlib_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.DllImportAttribute>
extern MethodInfo m31806_MI;
static PropertyInfo t6108____Count_PropertyInfo = 
{
	&t6108_TI, "Count", &m31806_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31807_MI;
static PropertyInfo t6108____IsReadOnly_PropertyInfo = 
{
	&t6108_TI, "IsReadOnly", &m31807_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6108_PIs[] =
{
	&t6108____Count_PropertyInfo,
	&t6108____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31806_GM;
MethodInfo m31806_MI = 
{
	"get_Count", NULL, &t6108_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31806_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31807_GM;
MethodInfo m31807_MI = 
{
	"get_IsReadOnly", NULL, &t6108_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31807_GM};
extern Il2CppType t1152_0_0_0;
extern Il2CppType t1152_0_0_0;
static ParameterInfo t6108_m31808_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1152_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31808_GM;
MethodInfo m31808_MI = 
{
	"Add", NULL, &t6108_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6108_m31808_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31808_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31809_GM;
MethodInfo m31809_MI = 
{
	"Clear", NULL, &t6108_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31809_GM};
extern Il2CppType t1152_0_0_0;
static ParameterInfo t6108_m31810_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1152_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31810_GM;
MethodInfo m31810_MI = 
{
	"Contains", NULL, &t6108_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6108_m31810_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31810_GM};
extern Il2CppType t3579_0_0_0;
extern Il2CppType t3579_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6108_m31811_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3579_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31811_GM;
MethodInfo m31811_MI = 
{
	"CopyTo", NULL, &t6108_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6108_m31811_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31811_GM};
extern Il2CppType t1152_0_0_0;
static ParameterInfo t6108_m31812_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1152_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31812_GM;
MethodInfo m31812_MI = 
{
	"Remove", NULL, &t6108_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6108_m31812_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31812_GM};
static MethodInfo* t6108_MIs[] =
{
	&m31806_MI,
	&m31807_MI,
	&m31808_MI,
	&m31809_MI,
	&m31810_MI,
	&m31811_MI,
	&m31812_MI,
	NULL
};
extern TypeInfo t6110_TI;
static TypeInfo* t6108_ITIs[] = 
{
	&t603_TI,
	&t6110_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6108_0_0_0;
extern Il2CppType t6108_1_0_0;
struct t6108;
extern Il2CppGenericClass t6108_GC;
TypeInfo t6108_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6108_MIs, t6108_PIs, NULL, NULL, NULL, NULL, NULL, &t6108_TI, t6108_ITIs, NULL, &EmptyCustomAttributesCache, &t6108_TI, &t6108_0_0_0, &t6108_1_0_0, NULL, &t6108_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.DllImportAttribute>
extern Il2CppType t4712_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31813_GM;
MethodInfo m31813_MI = 
{
	"GetEnumerator", NULL, &t6110_TI, &t4712_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31813_GM};
static MethodInfo* t6110_MIs[] =
{
	&m31813_MI,
	NULL
};
static TypeInfo* t6110_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6110_0_0_0;
extern Il2CppType t6110_1_0_0;
struct t6110;
extern Il2CppGenericClass t6110_GC;
TypeInfo t6110_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6110_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6110_TI, t6110_ITIs, NULL, &EmptyCustomAttributesCache, &t6110_TI, &t6110_0_0_0, &t6110_1_0_0, NULL, &t6110_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6109_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices.DllImportAttribute>
extern MethodInfo m31814_MI;
extern MethodInfo m31815_MI;
static PropertyInfo t6109____Item_PropertyInfo = 
{
	&t6109_TI, "Item", &m31814_MI, &m31815_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6109_PIs[] =
{
	&t6109____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1152_0_0_0;
static ParameterInfo t6109_m31816_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1152_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31816_GM;
MethodInfo m31816_MI = 
{
	"IndexOf", NULL, &t6109_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6109_m31816_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31816_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1152_0_0_0;
static ParameterInfo t6109_m31817_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1152_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31817_GM;
MethodInfo m31817_MI = 
{
	"Insert", NULL, &t6109_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6109_m31817_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31817_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6109_m31818_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31818_GM;
MethodInfo m31818_MI = 
{
	"RemoveAt", NULL, &t6109_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6109_m31818_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31818_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6109_m31814_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1152_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31814_GM;
MethodInfo m31814_MI = 
{
	"get_Item", NULL, &t6109_TI, &t1152_0_0_0, RuntimeInvoker_t29_t44, t6109_m31814_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31814_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1152_0_0_0;
static ParameterInfo t6109_m31815_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1152_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31815_GM;
MethodInfo m31815_MI = 
{
	"set_Item", NULL, &t6109_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6109_m31815_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31815_GM};
static MethodInfo* t6109_MIs[] =
{
	&m31816_MI,
	&m31817_MI,
	&m31818_MI,
	&m31814_MI,
	&m31815_MI,
	NULL
};
static TypeInfo* t6109_ITIs[] = 
{
	&t603_TI,
	&t6108_TI,
	&t6110_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6109_0_0_0;
extern Il2CppType t6109_1_0_0;
struct t6109;
extern Il2CppGenericClass t6109_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6109_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6109_MIs, t6109_PIs, NULL, NULL, NULL, NULL, NULL, &t6109_TI, t6109_ITIs, NULL, &t1908__CustomAttributeCache, &t6109_TI, &t6109_0_0_0, &t6109_1_0_0, NULL, &t6109_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4714_TI;

#include "t1155.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.MarshalAsAttribute>
extern MethodInfo m31819_MI;
static PropertyInfo t4714____Current_PropertyInfo = 
{
	&t4714_TI, "Current", &m31819_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4714_PIs[] =
{
	&t4714____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1155_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31819_GM;
MethodInfo m31819_MI = 
{
	"get_Current", NULL, &t4714_TI, &t1155_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31819_GM};
static MethodInfo* t4714_MIs[] =
{
	&m31819_MI,
	NULL
};
static TypeInfo* t4714_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4714_0_0_0;
extern Il2CppType t4714_1_0_0;
struct t4714;
extern Il2CppGenericClass t4714_GC;
TypeInfo t4714_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4714_MIs, t4714_PIs, NULL, NULL, NULL, NULL, NULL, &t4714_TI, t4714_ITIs, NULL, &EmptyCustomAttributesCache, &t4714_TI, &t4714_0_0_0, &t4714_1_0_0, NULL, &t4714_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3304.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3304_TI;
#include "t3304MD.h"

extern TypeInfo t1155_TI;
extern MethodInfo m18406_MI;
extern MethodInfo m24333_MI;
struct t20;
#define m24333(__this, p0, method) (t1155 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices.MarshalAsAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3304_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3304_TI, offsetof(t3304, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3304_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3304_TI, offsetof(t3304, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3304_FIs[] =
{
	&t3304_f0_FieldInfo,
	&t3304_f1_FieldInfo,
	NULL
};
extern MethodInfo m18403_MI;
static PropertyInfo t3304____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3304_TI, "System.Collections.IEnumerator.Current", &m18403_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3304____Current_PropertyInfo = 
{
	&t3304_TI, "Current", &m18406_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3304_PIs[] =
{
	&t3304____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3304____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3304_m18402_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18402_GM;
MethodInfo m18402_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3304_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3304_m18402_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18402_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18403_GM;
MethodInfo m18403_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3304_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18403_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18404_GM;
MethodInfo m18404_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3304_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18404_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18405_GM;
MethodInfo m18405_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3304_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18405_GM};
extern Il2CppType t1155_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18406_GM;
MethodInfo m18406_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3304_TI, &t1155_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18406_GM};
static MethodInfo* t3304_MIs[] =
{
	&m18402_MI,
	&m18403_MI,
	&m18404_MI,
	&m18405_MI,
	&m18406_MI,
	NULL
};
extern MethodInfo m18405_MI;
extern MethodInfo m18404_MI;
static MethodInfo* t3304_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18403_MI,
	&m18405_MI,
	&m18404_MI,
	&m18406_MI,
};
static TypeInfo* t3304_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4714_TI,
};
static Il2CppInterfaceOffsetPair t3304_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4714_TI, 7},
};
extern TypeInfo t1155_TI;
static Il2CppRGCTXData t3304_RGCTXData[3] = 
{
	&m18406_MI/* Method Usage */,
	&t1155_TI/* Class Usage */,
	&m24333_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3304_0_0_0;
extern Il2CppType t3304_1_0_0;
extern Il2CppGenericClass t3304_GC;
TypeInfo t3304_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3304_MIs, t3304_PIs, t3304_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3304_TI, t3304_ITIs, t3304_VT, &EmptyCustomAttributesCache, &t3304_TI, &t3304_0_0_0, &t3304_1_0_0, t3304_IOs, &t3304_GC, NULL, NULL, NULL, t3304_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3304)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6111_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.MarshalAsAttribute>
extern MethodInfo m31820_MI;
static PropertyInfo t6111____Count_PropertyInfo = 
{
	&t6111_TI, "Count", &m31820_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31821_MI;
static PropertyInfo t6111____IsReadOnly_PropertyInfo = 
{
	&t6111_TI, "IsReadOnly", &m31821_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6111_PIs[] =
{
	&t6111____Count_PropertyInfo,
	&t6111____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31820_GM;
MethodInfo m31820_MI = 
{
	"get_Count", NULL, &t6111_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31820_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31821_GM;
MethodInfo m31821_MI = 
{
	"get_IsReadOnly", NULL, &t6111_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31821_GM};
extern Il2CppType t1155_0_0_0;
extern Il2CppType t1155_0_0_0;
static ParameterInfo t6111_m31822_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1155_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31822_GM;
MethodInfo m31822_MI = 
{
	"Add", NULL, &t6111_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6111_m31822_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31822_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31823_GM;
MethodInfo m31823_MI = 
{
	"Clear", NULL, &t6111_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31823_GM};
extern Il2CppType t1155_0_0_0;
static ParameterInfo t6111_m31824_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1155_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31824_GM;
MethodInfo m31824_MI = 
{
	"Contains", NULL, &t6111_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6111_m31824_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31824_GM};
extern Il2CppType t3580_0_0_0;
extern Il2CppType t3580_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6111_m31825_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3580_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31825_GM;
MethodInfo m31825_MI = 
{
	"CopyTo", NULL, &t6111_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6111_m31825_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31825_GM};
extern Il2CppType t1155_0_0_0;
static ParameterInfo t6111_m31826_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1155_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31826_GM;
MethodInfo m31826_MI = 
{
	"Remove", NULL, &t6111_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6111_m31826_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31826_GM};
static MethodInfo* t6111_MIs[] =
{
	&m31820_MI,
	&m31821_MI,
	&m31822_MI,
	&m31823_MI,
	&m31824_MI,
	&m31825_MI,
	&m31826_MI,
	NULL
};
extern TypeInfo t6113_TI;
static TypeInfo* t6111_ITIs[] = 
{
	&t603_TI,
	&t6113_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6111_0_0_0;
extern Il2CppType t6111_1_0_0;
struct t6111;
extern Il2CppGenericClass t6111_GC;
TypeInfo t6111_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6111_MIs, t6111_PIs, NULL, NULL, NULL, NULL, NULL, &t6111_TI, t6111_ITIs, NULL, &EmptyCustomAttributesCache, &t6111_TI, &t6111_0_0_0, &t6111_1_0_0, NULL, &t6111_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.MarshalAsAttribute>
extern Il2CppType t4714_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31827_GM;
MethodInfo m31827_MI = 
{
	"GetEnumerator", NULL, &t6113_TI, &t4714_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31827_GM};
static MethodInfo* t6113_MIs[] =
{
	&m31827_MI,
	NULL
};
static TypeInfo* t6113_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6113_0_0_0;
extern Il2CppType t6113_1_0_0;
struct t6113;
extern Il2CppGenericClass t6113_GC;
TypeInfo t6113_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6113_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6113_TI, t6113_ITIs, NULL, &EmptyCustomAttributesCache, &t6113_TI, &t6113_0_0_0, &t6113_1_0_0, NULL, &t6113_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6112_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices.MarshalAsAttribute>
extern MethodInfo m31828_MI;
extern MethodInfo m31829_MI;
static PropertyInfo t6112____Item_PropertyInfo = 
{
	&t6112_TI, "Item", &m31828_MI, &m31829_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6112_PIs[] =
{
	&t6112____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1155_0_0_0;
static ParameterInfo t6112_m31830_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1155_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31830_GM;
MethodInfo m31830_MI = 
{
	"IndexOf", NULL, &t6112_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6112_m31830_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31830_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1155_0_0_0;
static ParameterInfo t6112_m31831_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1155_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31831_GM;
MethodInfo m31831_MI = 
{
	"Insert", NULL, &t6112_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6112_m31831_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31831_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6112_m31832_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31832_GM;
MethodInfo m31832_MI = 
{
	"RemoveAt", NULL, &t6112_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6112_m31832_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31832_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6112_m31828_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1155_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31828_GM;
MethodInfo m31828_MI = 
{
	"get_Item", NULL, &t6112_TI, &t1155_0_0_0, RuntimeInvoker_t29_t44, t6112_m31828_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31828_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1155_0_0_0;
static ParameterInfo t6112_m31829_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1155_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31829_GM;
MethodInfo m31829_MI = 
{
	"set_Item", NULL, &t6112_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6112_m31829_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31829_GM};
static MethodInfo* t6112_MIs[] =
{
	&m31830_MI,
	&m31831_MI,
	&m31832_MI,
	&m31828_MI,
	&m31829_MI,
	NULL
};
static TypeInfo* t6112_ITIs[] = 
{
	&t603_TI,
	&t6111_TI,
	&t6113_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6112_0_0_0;
extern Il2CppType t6112_1_0_0;
struct t6112;
extern Il2CppGenericClass t6112_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6112_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6112_MIs, t6112_PIs, NULL, NULL, NULL, NULL, NULL, &t6112_TI, t6112_ITIs, NULL, &t1908__CustomAttributeCache, &t6112_TI, &t6112_0_0_0, &t6112_1_0_0, NULL, &t6112_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4716_TI;

#include "t1157.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.InAttribute>
extern MethodInfo m31833_MI;
static PropertyInfo t4716____Current_PropertyInfo = 
{
	&t4716_TI, "Current", &m31833_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4716_PIs[] =
{
	&t4716____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1157_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31833_GM;
MethodInfo m31833_MI = 
{
	"get_Current", NULL, &t4716_TI, &t1157_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31833_GM};
static MethodInfo* t4716_MIs[] =
{
	&m31833_MI,
	NULL
};
static TypeInfo* t4716_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4716_0_0_0;
extern Il2CppType t4716_1_0_0;
struct t4716;
extern Il2CppGenericClass t4716_GC;
TypeInfo t4716_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4716_MIs, t4716_PIs, NULL, NULL, NULL, NULL, NULL, &t4716_TI, t4716_ITIs, NULL, &EmptyCustomAttributesCache, &t4716_TI, &t4716_0_0_0, &t4716_1_0_0, NULL, &t4716_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3305.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3305_TI;
#include "t3305MD.h"

extern TypeInfo t1157_TI;
extern MethodInfo m18411_MI;
extern MethodInfo m24344_MI;
struct t20;
#define m24344(__this, p0, method) (t1157 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices.InAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3305_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3305_TI, offsetof(t3305, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3305_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3305_TI, offsetof(t3305, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3305_FIs[] =
{
	&t3305_f0_FieldInfo,
	&t3305_f1_FieldInfo,
	NULL
};
extern MethodInfo m18408_MI;
static PropertyInfo t3305____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3305_TI, "System.Collections.IEnumerator.Current", &m18408_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3305____Current_PropertyInfo = 
{
	&t3305_TI, "Current", &m18411_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3305_PIs[] =
{
	&t3305____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3305____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3305_m18407_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18407_GM;
MethodInfo m18407_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3305_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3305_m18407_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18407_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18408_GM;
MethodInfo m18408_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3305_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18408_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18409_GM;
MethodInfo m18409_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3305_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18409_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18410_GM;
MethodInfo m18410_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3305_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18410_GM};
extern Il2CppType t1157_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18411_GM;
MethodInfo m18411_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3305_TI, &t1157_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18411_GM};
static MethodInfo* t3305_MIs[] =
{
	&m18407_MI,
	&m18408_MI,
	&m18409_MI,
	&m18410_MI,
	&m18411_MI,
	NULL
};
extern MethodInfo m18410_MI;
extern MethodInfo m18409_MI;
static MethodInfo* t3305_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18408_MI,
	&m18410_MI,
	&m18409_MI,
	&m18411_MI,
};
static TypeInfo* t3305_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4716_TI,
};
static Il2CppInterfaceOffsetPair t3305_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4716_TI, 7},
};
extern TypeInfo t1157_TI;
static Il2CppRGCTXData t3305_RGCTXData[3] = 
{
	&m18411_MI/* Method Usage */,
	&t1157_TI/* Class Usage */,
	&m24344_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3305_0_0_0;
extern Il2CppType t3305_1_0_0;
extern Il2CppGenericClass t3305_GC;
TypeInfo t3305_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3305_MIs, t3305_PIs, t3305_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3305_TI, t3305_ITIs, t3305_VT, &EmptyCustomAttributesCache, &t3305_TI, &t3305_0_0_0, &t3305_1_0_0, t3305_IOs, &t3305_GC, NULL, NULL, NULL, t3305_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3305)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6114_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.InAttribute>
extern MethodInfo m31834_MI;
static PropertyInfo t6114____Count_PropertyInfo = 
{
	&t6114_TI, "Count", &m31834_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31835_MI;
static PropertyInfo t6114____IsReadOnly_PropertyInfo = 
{
	&t6114_TI, "IsReadOnly", &m31835_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6114_PIs[] =
{
	&t6114____Count_PropertyInfo,
	&t6114____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31834_GM;
MethodInfo m31834_MI = 
{
	"get_Count", NULL, &t6114_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31834_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31835_GM;
MethodInfo m31835_MI = 
{
	"get_IsReadOnly", NULL, &t6114_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31835_GM};
extern Il2CppType t1157_0_0_0;
extern Il2CppType t1157_0_0_0;
static ParameterInfo t6114_m31836_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1157_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31836_GM;
MethodInfo m31836_MI = 
{
	"Add", NULL, &t6114_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6114_m31836_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31836_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31837_GM;
MethodInfo m31837_MI = 
{
	"Clear", NULL, &t6114_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31837_GM};
extern Il2CppType t1157_0_0_0;
static ParameterInfo t6114_m31838_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1157_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31838_GM;
MethodInfo m31838_MI = 
{
	"Contains", NULL, &t6114_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6114_m31838_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31838_GM};
extern Il2CppType t3581_0_0_0;
extern Il2CppType t3581_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6114_m31839_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3581_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31839_GM;
MethodInfo m31839_MI = 
{
	"CopyTo", NULL, &t6114_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6114_m31839_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31839_GM};
extern Il2CppType t1157_0_0_0;
static ParameterInfo t6114_m31840_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1157_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31840_GM;
MethodInfo m31840_MI = 
{
	"Remove", NULL, &t6114_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6114_m31840_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31840_GM};
static MethodInfo* t6114_MIs[] =
{
	&m31834_MI,
	&m31835_MI,
	&m31836_MI,
	&m31837_MI,
	&m31838_MI,
	&m31839_MI,
	&m31840_MI,
	NULL
};
extern TypeInfo t6116_TI;
static TypeInfo* t6114_ITIs[] = 
{
	&t603_TI,
	&t6116_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6114_0_0_0;
extern Il2CppType t6114_1_0_0;
struct t6114;
extern Il2CppGenericClass t6114_GC;
TypeInfo t6114_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6114_MIs, t6114_PIs, NULL, NULL, NULL, NULL, NULL, &t6114_TI, t6114_ITIs, NULL, &EmptyCustomAttributesCache, &t6114_TI, &t6114_0_0_0, &t6114_1_0_0, NULL, &t6114_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.InAttribute>
extern Il2CppType t4716_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31841_GM;
MethodInfo m31841_MI = 
{
	"GetEnumerator", NULL, &t6116_TI, &t4716_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31841_GM};
static MethodInfo* t6116_MIs[] =
{
	&m31841_MI,
	NULL
};
static TypeInfo* t6116_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6116_0_0_0;
extern Il2CppType t6116_1_0_0;
struct t6116;
extern Il2CppGenericClass t6116_GC;
TypeInfo t6116_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6116_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6116_TI, t6116_ITIs, NULL, &EmptyCustomAttributesCache, &t6116_TI, &t6116_0_0_0, &t6116_1_0_0, NULL, &t6116_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6115_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices.InAttribute>
extern MethodInfo m31842_MI;
extern MethodInfo m31843_MI;
static PropertyInfo t6115____Item_PropertyInfo = 
{
	&t6115_TI, "Item", &m31842_MI, &m31843_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6115_PIs[] =
{
	&t6115____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1157_0_0_0;
static ParameterInfo t6115_m31844_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1157_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31844_GM;
MethodInfo m31844_MI = 
{
	"IndexOf", NULL, &t6115_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6115_m31844_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31844_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1157_0_0_0;
static ParameterInfo t6115_m31845_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1157_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31845_GM;
MethodInfo m31845_MI = 
{
	"Insert", NULL, &t6115_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6115_m31845_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31845_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6115_m31846_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31846_GM;
MethodInfo m31846_MI = 
{
	"RemoveAt", NULL, &t6115_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6115_m31846_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31846_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6115_m31842_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1157_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31842_GM;
MethodInfo m31842_MI = 
{
	"get_Item", NULL, &t6115_TI, &t1157_0_0_0, RuntimeInvoker_t29_t44, t6115_m31842_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31842_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1157_0_0_0;
static ParameterInfo t6115_m31843_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1157_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31843_GM;
MethodInfo m31843_MI = 
{
	"set_Item", NULL, &t6115_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6115_m31843_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31843_GM};
static MethodInfo* t6115_MIs[] =
{
	&m31844_MI,
	&m31845_MI,
	&m31846_MI,
	&m31842_MI,
	&m31843_MI,
	NULL
};
static TypeInfo* t6115_ITIs[] = 
{
	&t603_TI,
	&t6114_TI,
	&t6116_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6115_0_0_0;
extern Il2CppType t6115_1_0_0;
struct t6115;
extern Il2CppGenericClass t6115_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6115_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6115_MIs, t6115_PIs, NULL, NULL, NULL, NULL, NULL, &t6115_TI, t6115_ITIs, NULL, &t1908__CustomAttributeCache, &t6115_TI, &t6115_0_0_0, &t6115_1_0_0, NULL, &t6115_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4718_TI;

#include "t436.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.GuidAttribute>
extern MethodInfo m31847_MI;
static PropertyInfo t4718____Current_PropertyInfo = 
{
	&t4718_TI, "Current", &m31847_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4718_PIs[] =
{
	&t4718____Current_PropertyInfo,
	NULL
};
extern Il2CppType t436_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31847_GM;
MethodInfo m31847_MI = 
{
	"get_Current", NULL, &t4718_TI, &t436_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31847_GM};
static MethodInfo* t4718_MIs[] =
{
	&m31847_MI,
	NULL
};
static TypeInfo* t4718_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4718_0_0_0;
extern Il2CppType t4718_1_0_0;
struct t4718;
extern Il2CppGenericClass t4718_GC;
TypeInfo t4718_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4718_MIs, t4718_PIs, NULL, NULL, NULL, NULL, NULL, &t4718_TI, t4718_ITIs, NULL, &EmptyCustomAttributesCache, &t4718_TI, &t4718_0_0_0, &t4718_1_0_0, NULL, &t4718_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3306.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3306_TI;
#include "t3306MD.h"

extern TypeInfo t436_TI;
extern MethodInfo m18416_MI;
extern MethodInfo m24355_MI;
struct t20;
#define m24355(__this, p0, method) (t436 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GuidAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3306_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3306_TI, offsetof(t3306, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3306_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3306_TI, offsetof(t3306, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3306_FIs[] =
{
	&t3306_f0_FieldInfo,
	&t3306_f1_FieldInfo,
	NULL
};
extern MethodInfo m18413_MI;
static PropertyInfo t3306____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3306_TI, "System.Collections.IEnumerator.Current", &m18413_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3306____Current_PropertyInfo = 
{
	&t3306_TI, "Current", &m18416_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3306_PIs[] =
{
	&t3306____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3306____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3306_m18412_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18412_GM;
MethodInfo m18412_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3306_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3306_m18412_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18412_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18413_GM;
MethodInfo m18413_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3306_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18413_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18414_GM;
MethodInfo m18414_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3306_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18414_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18415_GM;
MethodInfo m18415_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3306_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18415_GM};
extern Il2CppType t436_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18416_GM;
MethodInfo m18416_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3306_TI, &t436_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18416_GM};
static MethodInfo* t3306_MIs[] =
{
	&m18412_MI,
	&m18413_MI,
	&m18414_MI,
	&m18415_MI,
	&m18416_MI,
	NULL
};
extern MethodInfo m18415_MI;
extern MethodInfo m18414_MI;
static MethodInfo* t3306_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18413_MI,
	&m18415_MI,
	&m18414_MI,
	&m18416_MI,
};
static TypeInfo* t3306_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4718_TI,
};
static Il2CppInterfaceOffsetPair t3306_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4718_TI, 7},
};
extern TypeInfo t436_TI;
static Il2CppRGCTXData t3306_RGCTXData[3] = 
{
	&m18416_MI/* Method Usage */,
	&t436_TI/* Class Usage */,
	&m24355_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3306_0_0_0;
extern Il2CppType t3306_1_0_0;
extern Il2CppGenericClass t3306_GC;
TypeInfo t3306_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3306_MIs, t3306_PIs, t3306_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3306_TI, t3306_ITIs, t3306_VT, &EmptyCustomAttributesCache, &t3306_TI, &t3306_0_0_0, &t3306_1_0_0, t3306_IOs, &t3306_GC, NULL, NULL, NULL, t3306_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3306)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6117_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.GuidAttribute>
extern MethodInfo m31848_MI;
static PropertyInfo t6117____Count_PropertyInfo = 
{
	&t6117_TI, "Count", &m31848_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31849_MI;
static PropertyInfo t6117____IsReadOnly_PropertyInfo = 
{
	&t6117_TI, "IsReadOnly", &m31849_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6117_PIs[] =
{
	&t6117____Count_PropertyInfo,
	&t6117____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31848_GM;
MethodInfo m31848_MI = 
{
	"get_Count", NULL, &t6117_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31848_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31849_GM;
MethodInfo m31849_MI = 
{
	"get_IsReadOnly", NULL, &t6117_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31849_GM};
extern Il2CppType t436_0_0_0;
extern Il2CppType t436_0_0_0;
static ParameterInfo t6117_m31850_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t436_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31850_GM;
MethodInfo m31850_MI = 
{
	"Add", NULL, &t6117_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6117_m31850_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31850_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31851_GM;
MethodInfo m31851_MI = 
{
	"Clear", NULL, &t6117_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31851_GM};
extern Il2CppType t436_0_0_0;
static ParameterInfo t6117_m31852_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t436_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31852_GM;
MethodInfo m31852_MI = 
{
	"Contains", NULL, &t6117_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6117_m31852_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31852_GM};
extern Il2CppType t3582_0_0_0;
extern Il2CppType t3582_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6117_m31853_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3582_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31853_GM;
MethodInfo m31853_MI = 
{
	"CopyTo", NULL, &t6117_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6117_m31853_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31853_GM};
extern Il2CppType t436_0_0_0;
static ParameterInfo t6117_m31854_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t436_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31854_GM;
MethodInfo m31854_MI = 
{
	"Remove", NULL, &t6117_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6117_m31854_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31854_GM};
static MethodInfo* t6117_MIs[] =
{
	&m31848_MI,
	&m31849_MI,
	&m31850_MI,
	&m31851_MI,
	&m31852_MI,
	&m31853_MI,
	&m31854_MI,
	NULL
};
extern TypeInfo t6119_TI;
static TypeInfo* t6117_ITIs[] = 
{
	&t603_TI,
	&t6119_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6117_0_0_0;
extern Il2CppType t6117_1_0_0;
struct t6117;
extern Il2CppGenericClass t6117_GC;
TypeInfo t6117_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6117_MIs, t6117_PIs, NULL, NULL, NULL, NULL, NULL, &t6117_TI, t6117_ITIs, NULL, &EmptyCustomAttributesCache, &t6117_TI, &t6117_0_0_0, &t6117_1_0_0, NULL, &t6117_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.GuidAttribute>
extern Il2CppType t4718_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31855_GM;
MethodInfo m31855_MI = 
{
	"GetEnumerator", NULL, &t6119_TI, &t4718_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31855_GM};
static MethodInfo* t6119_MIs[] =
{
	&m31855_MI,
	NULL
};
static TypeInfo* t6119_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6119_0_0_0;
extern Il2CppType t6119_1_0_0;
struct t6119;
extern Il2CppGenericClass t6119_GC;
TypeInfo t6119_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6119_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6119_TI, t6119_ITIs, NULL, &EmptyCustomAttributesCache, &t6119_TI, &t6119_0_0_0, &t6119_1_0_0, NULL, &t6119_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6118_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices.GuidAttribute>
extern MethodInfo m31856_MI;
extern MethodInfo m31857_MI;
static PropertyInfo t6118____Item_PropertyInfo = 
{
	&t6118_TI, "Item", &m31856_MI, &m31857_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6118_PIs[] =
{
	&t6118____Item_PropertyInfo,
	NULL
};
extern Il2CppType t436_0_0_0;
static ParameterInfo t6118_m31858_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t436_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31858_GM;
MethodInfo m31858_MI = 
{
	"IndexOf", NULL, &t6118_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6118_m31858_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31858_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t436_0_0_0;
static ParameterInfo t6118_m31859_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t436_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31859_GM;
MethodInfo m31859_MI = 
{
	"Insert", NULL, &t6118_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6118_m31859_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31859_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6118_m31860_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31860_GM;
MethodInfo m31860_MI = 
{
	"RemoveAt", NULL, &t6118_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6118_m31860_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31860_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6118_m31856_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t436_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31856_GM;
MethodInfo m31856_MI = 
{
	"get_Item", NULL, &t6118_TI, &t436_0_0_0, RuntimeInvoker_t29_t44, t6118_m31856_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31856_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t436_0_0_0;
static ParameterInfo t6118_m31857_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t436_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31857_GM;
MethodInfo m31857_MI = 
{
	"set_Item", NULL, &t6118_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6118_m31857_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31857_GM};
static MethodInfo* t6118_MIs[] =
{
	&m31858_MI,
	&m31859_MI,
	&m31860_MI,
	&m31856_MI,
	&m31857_MI,
	NULL
};
static TypeInfo* t6118_ITIs[] = 
{
	&t603_TI,
	&t6117_TI,
	&t6119_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6118_0_0_0;
extern Il2CppType t6118_1_0_0;
struct t6118;
extern Il2CppGenericClass t6118_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6118_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6118_MIs, t6118_PIs, NULL, NULL, NULL, NULL, NULL, &t6118_TI, t6118_ITIs, NULL, &t1908__CustomAttributeCache, &t6118_TI, &t6118_0_0_0, &t6118_1_0_0, NULL, &t6118_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4720_TI;

#include "t1158.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.ComImportAttribute>
extern MethodInfo m31861_MI;
static PropertyInfo t4720____Current_PropertyInfo = 
{
	&t4720_TI, "Current", &m31861_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4720_PIs[] =
{
	&t4720____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1158_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31861_GM;
MethodInfo m31861_MI = 
{
	"get_Current", NULL, &t4720_TI, &t1158_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31861_GM};
static MethodInfo* t4720_MIs[] =
{
	&m31861_MI,
	NULL
};
static TypeInfo* t4720_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4720_0_0_0;
extern Il2CppType t4720_1_0_0;
struct t4720;
extern Il2CppGenericClass t4720_GC;
TypeInfo t4720_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4720_MIs, t4720_PIs, NULL, NULL, NULL, NULL, NULL, &t4720_TI, t4720_ITIs, NULL, &EmptyCustomAttributesCache, &t4720_TI, &t4720_0_0_0, &t4720_1_0_0, NULL, &t4720_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3307.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3307_TI;
#include "t3307MD.h"

extern TypeInfo t1158_TI;
extern MethodInfo m18421_MI;
extern MethodInfo m24366_MI;
struct t20;
#define m24366(__this, p0, method) (t1158 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComImportAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3307_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3307_TI, offsetof(t3307, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3307_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3307_TI, offsetof(t3307, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3307_FIs[] =
{
	&t3307_f0_FieldInfo,
	&t3307_f1_FieldInfo,
	NULL
};
extern MethodInfo m18418_MI;
static PropertyInfo t3307____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3307_TI, "System.Collections.IEnumerator.Current", &m18418_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3307____Current_PropertyInfo = 
{
	&t3307_TI, "Current", &m18421_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3307_PIs[] =
{
	&t3307____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3307____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3307_m18417_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18417_GM;
MethodInfo m18417_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3307_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3307_m18417_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18417_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18418_GM;
MethodInfo m18418_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3307_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18418_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18419_GM;
MethodInfo m18419_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3307_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18419_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18420_GM;
MethodInfo m18420_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3307_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18420_GM};
extern Il2CppType t1158_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18421_GM;
MethodInfo m18421_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3307_TI, &t1158_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18421_GM};
static MethodInfo* t3307_MIs[] =
{
	&m18417_MI,
	&m18418_MI,
	&m18419_MI,
	&m18420_MI,
	&m18421_MI,
	NULL
};
extern MethodInfo m18420_MI;
extern MethodInfo m18419_MI;
static MethodInfo* t3307_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18418_MI,
	&m18420_MI,
	&m18419_MI,
	&m18421_MI,
};
static TypeInfo* t3307_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4720_TI,
};
static Il2CppInterfaceOffsetPair t3307_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4720_TI, 7},
};
extern TypeInfo t1158_TI;
static Il2CppRGCTXData t3307_RGCTXData[3] = 
{
	&m18421_MI/* Method Usage */,
	&t1158_TI/* Class Usage */,
	&m24366_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3307_0_0_0;
extern Il2CppType t3307_1_0_0;
extern Il2CppGenericClass t3307_GC;
TypeInfo t3307_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3307_MIs, t3307_PIs, t3307_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3307_TI, t3307_ITIs, t3307_VT, &EmptyCustomAttributesCache, &t3307_TI, &t3307_0_0_0, &t3307_1_0_0, t3307_IOs, &t3307_GC, NULL, NULL, NULL, t3307_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3307)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6120_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComImportAttribute>
extern MethodInfo m31862_MI;
static PropertyInfo t6120____Count_PropertyInfo = 
{
	&t6120_TI, "Count", &m31862_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31863_MI;
static PropertyInfo t6120____IsReadOnly_PropertyInfo = 
{
	&t6120_TI, "IsReadOnly", &m31863_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6120_PIs[] =
{
	&t6120____Count_PropertyInfo,
	&t6120____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31862_GM;
MethodInfo m31862_MI = 
{
	"get_Count", NULL, &t6120_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31862_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31863_GM;
MethodInfo m31863_MI = 
{
	"get_IsReadOnly", NULL, &t6120_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31863_GM};
extern Il2CppType t1158_0_0_0;
extern Il2CppType t1158_0_0_0;
static ParameterInfo t6120_m31864_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1158_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31864_GM;
MethodInfo m31864_MI = 
{
	"Add", NULL, &t6120_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6120_m31864_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31864_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31865_GM;
MethodInfo m31865_MI = 
{
	"Clear", NULL, &t6120_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31865_GM};
extern Il2CppType t1158_0_0_0;
static ParameterInfo t6120_m31866_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1158_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31866_GM;
MethodInfo m31866_MI = 
{
	"Contains", NULL, &t6120_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6120_m31866_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31866_GM};
extern Il2CppType t3583_0_0_0;
extern Il2CppType t3583_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6120_m31867_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3583_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31867_GM;
MethodInfo m31867_MI = 
{
	"CopyTo", NULL, &t6120_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6120_m31867_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31867_GM};
extern Il2CppType t1158_0_0_0;
static ParameterInfo t6120_m31868_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1158_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31868_GM;
MethodInfo m31868_MI = 
{
	"Remove", NULL, &t6120_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6120_m31868_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31868_GM};
static MethodInfo* t6120_MIs[] =
{
	&m31862_MI,
	&m31863_MI,
	&m31864_MI,
	&m31865_MI,
	&m31866_MI,
	&m31867_MI,
	&m31868_MI,
	NULL
};
extern TypeInfo t6122_TI;
static TypeInfo* t6120_ITIs[] = 
{
	&t603_TI,
	&t6122_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6120_0_0_0;
extern Il2CppType t6120_1_0_0;
struct t6120;
extern Il2CppGenericClass t6120_GC;
TypeInfo t6120_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6120_MIs, t6120_PIs, NULL, NULL, NULL, NULL, NULL, &t6120_TI, t6120_ITIs, NULL, &EmptyCustomAttributesCache, &t6120_TI, &t6120_0_0_0, &t6120_1_0_0, NULL, &t6120_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.ComImportAttribute>
extern Il2CppType t4720_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31869_GM;
MethodInfo m31869_MI = 
{
	"GetEnumerator", NULL, &t6122_TI, &t4720_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31869_GM};
static MethodInfo* t6122_MIs[] =
{
	&m31869_MI,
	NULL
};
static TypeInfo* t6122_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6122_0_0_0;
extern Il2CppType t6122_1_0_0;
struct t6122;
extern Il2CppGenericClass t6122_GC;
TypeInfo t6122_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6122_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6122_TI, t6122_ITIs, NULL, &EmptyCustomAttributesCache, &t6122_TI, &t6122_0_0_0, &t6122_1_0_0, NULL, &t6122_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6121_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices.ComImportAttribute>
extern MethodInfo m31870_MI;
extern MethodInfo m31871_MI;
static PropertyInfo t6121____Item_PropertyInfo = 
{
	&t6121_TI, "Item", &m31870_MI, &m31871_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6121_PIs[] =
{
	&t6121____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1158_0_0_0;
static ParameterInfo t6121_m31872_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1158_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31872_GM;
MethodInfo m31872_MI = 
{
	"IndexOf", NULL, &t6121_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6121_m31872_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31872_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1158_0_0_0;
static ParameterInfo t6121_m31873_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1158_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31873_GM;
MethodInfo m31873_MI = 
{
	"Insert", NULL, &t6121_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6121_m31873_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31873_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6121_m31874_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31874_GM;
MethodInfo m31874_MI = 
{
	"RemoveAt", NULL, &t6121_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6121_m31874_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31874_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6121_m31870_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1158_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31870_GM;
MethodInfo m31870_MI = 
{
	"get_Item", NULL, &t6121_TI, &t1158_0_0_0, RuntimeInvoker_t29_t44, t6121_m31870_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31870_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1158_0_0_0;
static ParameterInfo t6121_m31871_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1158_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31871_GM;
MethodInfo m31871_MI = 
{
	"set_Item", NULL, &t6121_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6121_m31871_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31871_GM};
static MethodInfo* t6121_MIs[] =
{
	&m31872_MI,
	&m31873_MI,
	&m31874_MI,
	&m31870_MI,
	&m31871_MI,
	NULL
};
static TypeInfo* t6121_ITIs[] = 
{
	&t603_TI,
	&t6120_TI,
	&t6122_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6121_0_0_0;
extern Il2CppType t6121_1_0_0;
struct t6121;
extern Il2CppGenericClass t6121_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6121_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6121_MIs, t6121_PIs, NULL, NULL, NULL, NULL, NULL, &t6121_TI, t6121_ITIs, NULL, &t1908__CustomAttributeCache, &t6121_TI, &t6121_0_0_0, &t6121_1_0_0, NULL, &t6121_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4722_TI;

#include "t1159.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.OptionalAttribute>
extern MethodInfo m31875_MI;
static PropertyInfo t4722____Current_PropertyInfo = 
{
	&t4722_TI, "Current", &m31875_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4722_PIs[] =
{
	&t4722____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1159_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31875_GM;
MethodInfo m31875_MI = 
{
	"get_Current", NULL, &t4722_TI, &t1159_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31875_GM};
static MethodInfo* t4722_MIs[] =
{
	&m31875_MI,
	NULL
};
static TypeInfo* t4722_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4722_0_0_0;
extern Il2CppType t4722_1_0_0;
struct t4722;
extern Il2CppGenericClass t4722_GC;
TypeInfo t4722_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4722_MIs, t4722_PIs, NULL, NULL, NULL, NULL, NULL, &t4722_TI, t4722_ITIs, NULL, &EmptyCustomAttributesCache, &t4722_TI, &t4722_0_0_0, &t4722_1_0_0, NULL, &t4722_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3308.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3308_TI;
#include "t3308MD.h"

extern TypeInfo t1159_TI;
extern MethodInfo m18426_MI;
extern MethodInfo m24377_MI;
struct t20;
#define m24377(__this, p0, method) (t1159 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices.OptionalAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3308_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3308_TI, offsetof(t3308, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3308_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3308_TI, offsetof(t3308, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3308_FIs[] =
{
	&t3308_f0_FieldInfo,
	&t3308_f1_FieldInfo,
	NULL
};
extern MethodInfo m18423_MI;
static PropertyInfo t3308____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3308_TI, "System.Collections.IEnumerator.Current", &m18423_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3308____Current_PropertyInfo = 
{
	&t3308_TI, "Current", &m18426_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3308_PIs[] =
{
	&t3308____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3308____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3308_m18422_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18422_GM;
MethodInfo m18422_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3308_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3308_m18422_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18422_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18423_GM;
MethodInfo m18423_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3308_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18423_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18424_GM;
MethodInfo m18424_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3308_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18424_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18425_GM;
MethodInfo m18425_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3308_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18425_GM};
extern Il2CppType t1159_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18426_GM;
MethodInfo m18426_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3308_TI, &t1159_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18426_GM};
static MethodInfo* t3308_MIs[] =
{
	&m18422_MI,
	&m18423_MI,
	&m18424_MI,
	&m18425_MI,
	&m18426_MI,
	NULL
};
extern MethodInfo m18425_MI;
extern MethodInfo m18424_MI;
static MethodInfo* t3308_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18423_MI,
	&m18425_MI,
	&m18424_MI,
	&m18426_MI,
};
static TypeInfo* t3308_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4722_TI,
};
static Il2CppInterfaceOffsetPair t3308_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4722_TI, 7},
};
extern TypeInfo t1159_TI;
static Il2CppRGCTXData t3308_RGCTXData[3] = 
{
	&m18426_MI/* Method Usage */,
	&t1159_TI/* Class Usage */,
	&m24377_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3308_0_0_0;
extern Il2CppType t3308_1_0_0;
extern Il2CppGenericClass t3308_GC;
TypeInfo t3308_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3308_MIs, t3308_PIs, t3308_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3308_TI, t3308_ITIs, t3308_VT, &EmptyCustomAttributesCache, &t3308_TI, &t3308_0_0_0, &t3308_1_0_0, t3308_IOs, &t3308_GC, NULL, NULL, NULL, t3308_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3308)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6123_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.OptionalAttribute>
extern MethodInfo m31876_MI;
static PropertyInfo t6123____Count_PropertyInfo = 
{
	&t6123_TI, "Count", &m31876_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31877_MI;
static PropertyInfo t6123____IsReadOnly_PropertyInfo = 
{
	&t6123_TI, "IsReadOnly", &m31877_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6123_PIs[] =
{
	&t6123____Count_PropertyInfo,
	&t6123____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31876_GM;
MethodInfo m31876_MI = 
{
	"get_Count", NULL, &t6123_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31876_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31877_GM;
MethodInfo m31877_MI = 
{
	"get_IsReadOnly", NULL, &t6123_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31877_GM};
extern Il2CppType t1159_0_0_0;
extern Il2CppType t1159_0_0_0;
static ParameterInfo t6123_m31878_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1159_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31878_GM;
MethodInfo m31878_MI = 
{
	"Add", NULL, &t6123_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6123_m31878_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31878_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31879_GM;
MethodInfo m31879_MI = 
{
	"Clear", NULL, &t6123_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31879_GM};
extern Il2CppType t1159_0_0_0;
static ParameterInfo t6123_m31880_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1159_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31880_GM;
MethodInfo m31880_MI = 
{
	"Contains", NULL, &t6123_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6123_m31880_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31880_GM};
extern Il2CppType t3584_0_0_0;
extern Il2CppType t3584_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6123_m31881_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3584_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31881_GM;
MethodInfo m31881_MI = 
{
	"CopyTo", NULL, &t6123_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6123_m31881_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31881_GM};
extern Il2CppType t1159_0_0_0;
static ParameterInfo t6123_m31882_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1159_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31882_GM;
MethodInfo m31882_MI = 
{
	"Remove", NULL, &t6123_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6123_m31882_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31882_GM};
static MethodInfo* t6123_MIs[] =
{
	&m31876_MI,
	&m31877_MI,
	&m31878_MI,
	&m31879_MI,
	&m31880_MI,
	&m31881_MI,
	&m31882_MI,
	NULL
};
extern TypeInfo t6125_TI;
static TypeInfo* t6123_ITIs[] = 
{
	&t603_TI,
	&t6125_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6123_0_0_0;
extern Il2CppType t6123_1_0_0;
struct t6123;
extern Il2CppGenericClass t6123_GC;
TypeInfo t6123_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6123_MIs, t6123_PIs, NULL, NULL, NULL, NULL, NULL, &t6123_TI, t6123_ITIs, NULL, &EmptyCustomAttributesCache, &t6123_TI, &t6123_0_0_0, &t6123_1_0_0, NULL, &t6123_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.OptionalAttribute>
extern Il2CppType t4722_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31883_GM;
MethodInfo m31883_MI = 
{
	"GetEnumerator", NULL, &t6125_TI, &t4722_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31883_GM};
static MethodInfo* t6125_MIs[] =
{
	&m31883_MI,
	NULL
};
static TypeInfo* t6125_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6125_0_0_0;
extern Il2CppType t6125_1_0_0;
struct t6125;
extern Il2CppGenericClass t6125_GC;
TypeInfo t6125_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6125_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6125_TI, t6125_ITIs, NULL, &EmptyCustomAttributesCache, &t6125_TI, &t6125_0_0_0, &t6125_1_0_0, NULL, &t6125_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6124_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices.OptionalAttribute>
extern MethodInfo m31884_MI;
extern MethodInfo m31885_MI;
static PropertyInfo t6124____Item_PropertyInfo = 
{
	&t6124_TI, "Item", &m31884_MI, &m31885_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6124_PIs[] =
{
	&t6124____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1159_0_0_0;
static ParameterInfo t6124_m31886_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1159_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31886_GM;
MethodInfo m31886_MI = 
{
	"IndexOf", NULL, &t6124_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6124_m31886_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31886_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1159_0_0_0;
static ParameterInfo t6124_m31887_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1159_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31887_GM;
MethodInfo m31887_MI = 
{
	"Insert", NULL, &t6124_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6124_m31887_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31887_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6124_m31888_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31888_GM;
MethodInfo m31888_MI = 
{
	"RemoveAt", NULL, &t6124_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6124_m31888_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31888_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6124_m31884_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1159_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31884_GM;
MethodInfo m31884_MI = 
{
	"get_Item", NULL, &t6124_TI, &t1159_0_0_0, RuntimeInvoker_t29_t44, t6124_m31884_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31884_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1159_0_0_0;
static ParameterInfo t6124_m31885_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1159_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31885_GM;
MethodInfo m31885_MI = 
{
	"set_Item", NULL, &t6124_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6124_m31885_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31885_GM};
static MethodInfo* t6124_MIs[] =
{
	&m31886_MI,
	&m31887_MI,
	&m31888_MI,
	&m31884_MI,
	&m31885_MI,
	NULL
};
static TypeInfo* t6124_ITIs[] = 
{
	&t603_TI,
	&t6123_TI,
	&t6125_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6124_0_0_0;
extern Il2CppType t6124_1_0_0;
struct t6124;
extern Il2CppGenericClass t6124_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6124_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6124_MIs, t6124_PIs, NULL, NULL, NULL, NULL, NULL, &t6124_TI, t6124_ITIs, NULL, &t1908__CustomAttributeCache, &t6124_TI, &t6124_0_0_0, &t6124_1_0_0, NULL, &t6124_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4724_TI;

#include "t38.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.CompilerServices.CompilerGeneratedAttribute>
extern MethodInfo m31889_MI;
static PropertyInfo t4724____Current_PropertyInfo = 
{
	&t4724_TI, "Current", &m31889_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4724_PIs[] =
{
	&t4724____Current_PropertyInfo,
	NULL
};
extern Il2CppType t38_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31889_GM;
MethodInfo m31889_MI = 
{
	"get_Current", NULL, &t4724_TI, &t38_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31889_GM};
static MethodInfo* t4724_MIs[] =
{
	&m31889_MI,
	NULL
};
static TypeInfo* t4724_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4724_0_0_0;
extern Il2CppType t4724_1_0_0;
struct t4724;
extern Il2CppGenericClass t4724_GC;
TypeInfo t4724_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4724_MIs, t4724_PIs, NULL, NULL, NULL, NULL, NULL, &t4724_TI, t4724_ITIs, NULL, &EmptyCustomAttributesCache, &t4724_TI, &t4724_0_0_0, &t4724_1_0_0, NULL, &t4724_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3309.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3309_TI;
#include "t3309MD.h"

extern TypeInfo t38_TI;
extern MethodInfo m18431_MI;
extern MethodInfo m24388_MI;
struct t20;
#define m24388(__this, p0, method) (t38 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.CompilerGeneratedAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3309_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3309_TI, offsetof(t3309, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3309_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3309_TI, offsetof(t3309, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3309_FIs[] =
{
	&t3309_f0_FieldInfo,
	&t3309_f1_FieldInfo,
	NULL
};
extern MethodInfo m18428_MI;
static PropertyInfo t3309____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3309_TI, "System.Collections.IEnumerator.Current", &m18428_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3309____Current_PropertyInfo = 
{
	&t3309_TI, "Current", &m18431_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3309_PIs[] =
{
	&t3309____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3309____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3309_m18427_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18427_GM;
MethodInfo m18427_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3309_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3309_m18427_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18427_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18428_GM;
MethodInfo m18428_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3309_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18428_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18429_GM;
MethodInfo m18429_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3309_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18429_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18430_GM;
MethodInfo m18430_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3309_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18430_GM};
extern Il2CppType t38_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18431_GM;
MethodInfo m18431_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3309_TI, &t38_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18431_GM};
static MethodInfo* t3309_MIs[] =
{
	&m18427_MI,
	&m18428_MI,
	&m18429_MI,
	&m18430_MI,
	&m18431_MI,
	NULL
};
extern MethodInfo m18430_MI;
extern MethodInfo m18429_MI;
static MethodInfo* t3309_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18428_MI,
	&m18430_MI,
	&m18429_MI,
	&m18431_MI,
};
static TypeInfo* t3309_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4724_TI,
};
static Il2CppInterfaceOffsetPair t3309_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4724_TI, 7},
};
extern TypeInfo t38_TI;
static Il2CppRGCTXData t3309_RGCTXData[3] = 
{
	&m18431_MI/* Method Usage */,
	&t38_TI/* Class Usage */,
	&m24388_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3309_0_0_0;
extern Il2CppType t3309_1_0_0;
extern Il2CppGenericClass t3309_GC;
TypeInfo t3309_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3309_MIs, t3309_PIs, t3309_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3309_TI, t3309_ITIs, t3309_VT, &EmptyCustomAttributesCache, &t3309_TI, &t3309_0_0_0, &t3309_1_0_0, t3309_IOs, &t3309_GC, NULL, NULL, NULL, t3309_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3309)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6126_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.CompilerGeneratedAttribute>
extern MethodInfo m31890_MI;
static PropertyInfo t6126____Count_PropertyInfo = 
{
	&t6126_TI, "Count", &m31890_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31891_MI;
static PropertyInfo t6126____IsReadOnly_PropertyInfo = 
{
	&t6126_TI, "IsReadOnly", &m31891_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6126_PIs[] =
{
	&t6126____Count_PropertyInfo,
	&t6126____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31890_GM;
MethodInfo m31890_MI = 
{
	"get_Count", NULL, &t6126_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31890_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31891_GM;
MethodInfo m31891_MI = 
{
	"get_IsReadOnly", NULL, &t6126_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31891_GM};
extern Il2CppType t38_0_0_0;
extern Il2CppType t38_0_0_0;
static ParameterInfo t6126_m31892_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t38_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31892_GM;
MethodInfo m31892_MI = 
{
	"Add", NULL, &t6126_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6126_m31892_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31892_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31893_GM;
MethodInfo m31893_MI = 
{
	"Clear", NULL, &t6126_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31893_GM};
extern Il2CppType t38_0_0_0;
static ParameterInfo t6126_m31894_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t38_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31894_GM;
MethodInfo m31894_MI = 
{
	"Contains", NULL, &t6126_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6126_m31894_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31894_GM};
extern Il2CppType t3585_0_0_0;
extern Il2CppType t3585_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6126_m31895_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3585_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31895_GM;
MethodInfo m31895_MI = 
{
	"CopyTo", NULL, &t6126_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6126_m31895_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31895_GM};
extern Il2CppType t38_0_0_0;
static ParameterInfo t6126_m31896_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t38_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31896_GM;
MethodInfo m31896_MI = 
{
	"Remove", NULL, &t6126_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6126_m31896_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31896_GM};
static MethodInfo* t6126_MIs[] =
{
	&m31890_MI,
	&m31891_MI,
	&m31892_MI,
	&m31893_MI,
	&m31894_MI,
	&m31895_MI,
	&m31896_MI,
	NULL
};
extern TypeInfo t6128_TI;
static TypeInfo* t6126_ITIs[] = 
{
	&t603_TI,
	&t6128_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6126_0_0_0;
extern Il2CppType t6126_1_0_0;
struct t6126;
extern Il2CppGenericClass t6126_GC;
TypeInfo t6126_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6126_MIs, t6126_PIs, NULL, NULL, NULL, NULL, NULL, &t6126_TI, t6126_ITIs, NULL, &EmptyCustomAttributesCache, &t6126_TI, &t6126_0_0_0, &t6126_1_0_0, NULL, &t6126_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.CompilerServices.CompilerGeneratedAttribute>
extern Il2CppType t4724_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31897_GM;
MethodInfo m31897_MI = 
{
	"GetEnumerator", NULL, &t6128_TI, &t4724_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31897_GM};
static MethodInfo* t6128_MIs[] =
{
	&m31897_MI,
	NULL
};
static TypeInfo* t6128_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6128_0_0_0;
extern Il2CppType t6128_1_0_0;
struct t6128;
extern Il2CppGenericClass t6128_GC;
TypeInfo t6128_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6128_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6128_TI, t6128_ITIs, NULL, &EmptyCustomAttributesCache, &t6128_TI, &t6128_0_0_0, &t6128_1_0_0, NULL, &t6128_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6127_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.CompilerServices.CompilerGeneratedAttribute>
extern MethodInfo m31898_MI;
extern MethodInfo m31899_MI;
static PropertyInfo t6127____Item_PropertyInfo = 
{
	&t6127_TI, "Item", &m31898_MI, &m31899_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6127_PIs[] =
{
	&t6127____Item_PropertyInfo,
	NULL
};
extern Il2CppType t38_0_0_0;
static ParameterInfo t6127_m31900_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t38_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31900_GM;
MethodInfo m31900_MI = 
{
	"IndexOf", NULL, &t6127_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6127_m31900_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31900_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t38_0_0_0;
static ParameterInfo t6127_m31901_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t38_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31901_GM;
MethodInfo m31901_MI = 
{
	"Insert", NULL, &t6127_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6127_m31901_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31901_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6127_m31902_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31902_GM;
MethodInfo m31902_MI = 
{
	"RemoveAt", NULL, &t6127_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6127_m31902_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31902_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6127_m31898_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t38_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31898_GM;
MethodInfo m31898_MI = 
{
	"get_Item", NULL, &t6127_TI, &t38_0_0_0, RuntimeInvoker_t29_t44, t6127_m31898_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31898_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t38_0_0_0;
static ParameterInfo t6127_m31899_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t38_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31899_GM;
MethodInfo m31899_MI = 
{
	"set_Item", NULL, &t6127_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6127_m31899_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31899_GM};
static MethodInfo* t6127_MIs[] =
{
	&m31900_MI,
	&m31901_MI,
	&m31902_MI,
	&m31898_MI,
	&m31899_MI,
	NULL
};
static TypeInfo* t6127_ITIs[] = 
{
	&t603_TI,
	&t6126_TI,
	&t6128_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6127_0_0_0;
extern Il2CppType t6127_1_0_0;
struct t6127;
extern Il2CppGenericClass t6127_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6127_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6127_MIs, t6127_PIs, NULL, NULL, NULL, NULL, NULL, &t6127_TI, t6127_ITIs, NULL, &t1908__CustomAttributeCache, &t6127_TI, &t6127_0_0_0, &t6127_1_0_0, NULL, &t6127_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4726_TI;

#include "t685.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.CompilerServices.InternalsVisibleToAttribute>
extern MethodInfo m31903_MI;
static PropertyInfo t4726____Current_PropertyInfo = 
{
	&t4726_TI, "Current", &m31903_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4726_PIs[] =
{
	&t4726____Current_PropertyInfo,
	NULL
};
extern Il2CppType t685_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31903_GM;
MethodInfo m31903_MI = 
{
	"get_Current", NULL, &t4726_TI, &t685_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31903_GM};
static MethodInfo* t4726_MIs[] =
{
	&m31903_MI,
	NULL
};
static TypeInfo* t4726_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4726_0_0_0;
extern Il2CppType t4726_1_0_0;
struct t4726;
extern Il2CppGenericClass t4726_GC;
TypeInfo t4726_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4726_MIs, t4726_PIs, NULL, NULL, NULL, NULL, NULL, &t4726_TI, t4726_ITIs, NULL, &EmptyCustomAttributesCache, &t4726_TI, &t4726_0_0_0, &t4726_1_0_0, NULL, &t4726_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3310.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3310_TI;
#include "t3310MD.h"

extern TypeInfo t685_TI;
extern MethodInfo m18436_MI;
extern MethodInfo m24399_MI;
struct t20;
#define m24399(__this, p0, method) (t685 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.InternalsVisibleToAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3310_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3310_TI, offsetof(t3310, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3310_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3310_TI, offsetof(t3310, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3310_FIs[] =
{
	&t3310_f0_FieldInfo,
	&t3310_f1_FieldInfo,
	NULL
};
extern MethodInfo m18433_MI;
static PropertyInfo t3310____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3310_TI, "System.Collections.IEnumerator.Current", &m18433_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3310____Current_PropertyInfo = 
{
	&t3310_TI, "Current", &m18436_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3310_PIs[] =
{
	&t3310____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3310____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3310_m18432_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18432_GM;
MethodInfo m18432_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3310_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3310_m18432_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18432_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18433_GM;
MethodInfo m18433_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3310_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18433_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18434_GM;
MethodInfo m18434_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3310_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18434_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18435_GM;
MethodInfo m18435_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3310_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18435_GM};
extern Il2CppType t685_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18436_GM;
MethodInfo m18436_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3310_TI, &t685_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18436_GM};
static MethodInfo* t3310_MIs[] =
{
	&m18432_MI,
	&m18433_MI,
	&m18434_MI,
	&m18435_MI,
	&m18436_MI,
	NULL
};
extern MethodInfo m18435_MI;
extern MethodInfo m18434_MI;
static MethodInfo* t3310_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18433_MI,
	&m18435_MI,
	&m18434_MI,
	&m18436_MI,
};
static TypeInfo* t3310_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4726_TI,
};
static Il2CppInterfaceOffsetPair t3310_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4726_TI, 7},
};
extern TypeInfo t685_TI;
static Il2CppRGCTXData t3310_RGCTXData[3] = 
{
	&m18436_MI/* Method Usage */,
	&t685_TI/* Class Usage */,
	&m24399_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3310_0_0_0;
extern Il2CppType t3310_1_0_0;
extern Il2CppGenericClass t3310_GC;
TypeInfo t3310_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3310_MIs, t3310_PIs, t3310_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3310_TI, t3310_ITIs, t3310_VT, &EmptyCustomAttributesCache, &t3310_TI, &t3310_0_0_0, &t3310_1_0_0, t3310_IOs, &t3310_GC, NULL, NULL, NULL, t3310_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3310)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6129_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.InternalsVisibleToAttribute>
extern MethodInfo m31904_MI;
static PropertyInfo t6129____Count_PropertyInfo = 
{
	&t6129_TI, "Count", &m31904_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31905_MI;
static PropertyInfo t6129____IsReadOnly_PropertyInfo = 
{
	&t6129_TI, "IsReadOnly", &m31905_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6129_PIs[] =
{
	&t6129____Count_PropertyInfo,
	&t6129____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31904_GM;
MethodInfo m31904_MI = 
{
	"get_Count", NULL, &t6129_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31904_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31905_GM;
MethodInfo m31905_MI = 
{
	"get_IsReadOnly", NULL, &t6129_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31905_GM};
extern Il2CppType t685_0_0_0;
extern Il2CppType t685_0_0_0;
static ParameterInfo t6129_m31906_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t685_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31906_GM;
MethodInfo m31906_MI = 
{
	"Add", NULL, &t6129_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6129_m31906_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31906_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31907_GM;
MethodInfo m31907_MI = 
{
	"Clear", NULL, &t6129_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31907_GM};
extern Il2CppType t685_0_0_0;
static ParameterInfo t6129_m31908_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t685_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31908_GM;
MethodInfo m31908_MI = 
{
	"Contains", NULL, &t6129_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6129_m31908_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31908_GM};
extern Il2CppType t3586_0_0_0;
extern Il2CppType t3586_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6129_m31909_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3586_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31909_GM;
MethodInfo m31909_MI = 
{
	"CopyTo", NULL, &t6129_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6129_m31909_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31909_GM};
extern Il2CppType t685_0_0_0;
static ParameterInfo t6129_m31910_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t685_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31910_GM;
MethodInfo m31910_MI = 
{
	"Remove", NULL, &t6129_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6129_m31910_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31910_GM};
static MethodInfo* t6129_MIs[] =
{
	&m31904_MI,
	&m31905_MI,
	&m31906_MI,
	&m31907_MI,
	&m31908_MI,
	&m31909_MI,
	&m31910_MI,
	NULL
};
extern TypeInfo t6131_TI;
static TypeInfo* t6129_ITIs[] = 
{
	&t603_TI,
	&t6131_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6129_0_0_0;
extern Il2CppType t6129_1_0_0;
struct t6129;
extern Il2CppGenericClass t6129_GC;
TypeInfo t6129_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6129_MIs, t6129_PIs, NULL, NULL, NULL, NULL, NULL, &t6129_TI, t6129_ITIs, NULL, &EmptyCustomAttributesCache, &t6129_TI, &t6129_0_0_0, &t6129_1_0_0, NULL, &t6129_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.CompilerServices.InternalsVisibleToAttribute>
extern Il2CppType t4726_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31911_GM;
MethodInfo m31911_MI = 
{
	"GetEnumerator", NULL, &t6131_TI, &t4726_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31911_GM};
static MethodInfo* t6131_MIs[] =
{
	&m31911_MI,
	NULL
};
static TypeInfo* t6131_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6131_0_0_0;
extern Il2CppType t6131_1_0_0;
struct t6131;
extern Il2CppGenericClass t6131_GC;
TypeInfo t6131_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6131_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6131_TI, t6131_ITIs, NULL, &EmptyCustomAttributesCache, &t6131_TI, &t6131_0_0_0, &t6131_1_0_0, NULL, &t6131_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6130_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.CompilerServices.InternalsVisibleToAttribute>
extern MethodInfo m31912_MI;
extern MethodInfo m31913_MI;
static PropertyInfo t6130____Item_PropertyInfo = 
{
	&t6130_TI, "Item", &m31912_MI, &m31913_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6130_PIs[] =
{
	&t6130____Item_PropertyInfo,
	NULL
};
extern Il2CppType t685_0_0_0;
static ParameterInfo t6130_m31914_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t685_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31914_GM;
MethodInfo m31914_MI = 
{
	"IndexOf", NULL, &t6130_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6130_m31914_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31914_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t685_0_0_0;
static ParameterInfo t6130_m31915_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t685_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31915_GM;
MethodInfo m31915_MI = 
{
	"Insert", NULL, &t6130_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6130_m31915_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31915_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6130_m31916_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31916_GM;
MethodInfo m31916_MI = 
{
	"RemoveAt", NULL, &t6130_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6130_m31916_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31916_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6130_m31912_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t685_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31912_GM;
MethodInfo m31912_MI = 
{
	"get_Item", NULL, &t6130_TI, &t685_0_0_0, RuntimeInvoker_t29_t44, t6130_m31912_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31912_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t685_0_0_0;
static ParameterInfo t6130_m31913_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t685_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31913_GM;
MethodInfo m31913_MI = 
{
	"set_Item", NULL, &t6130_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6130_m31913_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31913_GM};
static MethodInfo* t6130_MIs[] =
{
	&m31914_MI,
	&m31915_MI,
	&m31916_MI,
	&m31912_MI,
	&m31913_MI,
	NULL
};
static TypeInfo* t6130_ITIs[] = 
{
	&t603_TI,
	&t6129_TI,
	&t6131_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6130_0_0_0;
extern Il2CppType t6130_1_0_0;
struct t6130;
extern Il2CppGenericClass t6130_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6130_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6130_MIs, t6130_PIs, NULL, NULL, NULL, NULL, NULL, &t6130_TI, t6130_ITIs, NULL, &t1908__CustomAttributeCache, &t6130_TI, &t6130_0_0_0, &t6130_1_0_0, NULL, &t6130_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4728_TI;

#include "t46.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.CompilerServices.RuntimeCompatibilityAttribute>
extern MethodInfo m31917_MI;
static PropertyInfo t4728____Current_PropertyInfo = 
{
	&t4728_TI, "Current", &m31917_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4728_PIs[] =
{
	&t4728____Current_PropertyInfo,
	NULL
};
extern Il2CppType t46_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31917_GM;
MethodInfo m31917_MI = 
{
	"get_Current", NULL, &t4728_TI, &t46_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31917_GM};
static MethodInfo* t4728_MIs[] =
{
	&m31917_MI,
	NULL
};
static TypeInfo* t4728_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4728_0_0_0;
extern Il2CppType t4728_1_0_0;
struct t4728;
extern Il2CppGenericClass t4728_GC;
TypeInfo t4728_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4728_MIs, t4728_PIs, NULL, NULL, NULL, NULL, NULL, &t4728_TI, t4728_ITIs, NULL, &EmptyCustomAttributesCache, &t4728_TI, &t4728_0_0_0, &t4728_1_0_0, NULL, &t4728_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3311.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3311_TI;
#include "t3311MD.h"

extern TypeInfo t46_TI;
extern MethodInfo m18441_MI;
extern MethodInfo m24410_MI;
struct t20;
#define m24410(__this, p0, method) (t46 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.RuntimeCompatibilityAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3311_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3311_TI, offsetof(t3311, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3311_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3311_TI, offsetof(t3311, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3311_FIs[] =
{
	&t3311_f0_FieldInfo,
	&t3311_f1_FieldInfo,
	NULL
};
extern MethodInfo m18438_MI;
static PropertyInfo t3311____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3311_TI, "System.Collections.IEnumerator.Current", &m18438_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3311____Current_PropertyInfo = 
{
	&t3311_TI, "Current", &m18441_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3311_PIs[] =
{
	&t3311____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3311____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3311_m18437_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18437_GM;
MethodInfo m18437_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3311_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3311_m18437_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18437_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18438_GM;
MethodInfo m18438_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3311_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18438_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18439_GM;
MethodInfo m18439_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3311_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18439_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18440_GM;
MethodInfo m18440_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3311_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18440_GM};
extern Il2CppType t46_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18441_GM;
MethodInfo m18441_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3311_TI, &t46_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18441_GM};
static MethodInfo* t3311_MIs[] =
{
	&m18437_MI,
	&m18438_MI,
	&m18439_MI,
	&m18440_MI,
	&m18441_MI,
	NULL
};
extern MethodInfo m18440_MI;
extern MethodInfo m18439_MI;
static MethodInfo* t3311_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18438_MI,
	&m18440_MI,
	&m18439_MI,
	&m18441_MI,
};
static TypeInfo* t3311_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4728_TI,
};
static Il2CppInterfaceOffsetPair t3311_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4728_TI, 7},
};
extern TypeInfo t46_TI;
static Il2CppRGCTXData t3311_RGCTXData[3] = 
{
	&m18441_MI/* Method Usage */,
	&t46_TI/* Class Usage */,
	&m24410_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3311_0_0_0;
extern Il2CppType t3311_1_0_0;
extern Il2CppGenericClass t3311_GC;
TypeInfo t3311_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3311_MIs, t3311_PIs, t3311_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3311_TI, t3311_ITIs, t3311_VT, &EmptyCustomAttributesCache, &t3311_TI, &t3311_0_0_0, &t3311_1_0_0, t3311_IOs, &t3311_GC, NULL, NULL, NULL, t3311_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3311)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6132_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.RuntimeCompatibilityAttribute>
extern MethodInfo m31918_MI;
static PropertyInfo t6132____Count_PropertyInfo = 
{
	&t6132_TI, "Count", &m31918_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31919_MI;
static PropertyInfo t6132____IsReadOnly_PropertyInfo = 
{
	&t6132_TI, "IsReadOnly", &m31919_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6132_PIs[] =
{
	&t6132____Count_PropertyInfo,
	&t6132____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31918_GM;
MethodInfo m31918_MI = 
{
	"get_Count", NULL, &t6132_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31918_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31919_GM;
MethodInfo m31919_MI = 
{
	"get_IsReadOnly", NULL, &t6132_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31919_GM};
extern Il2CppType t46_0_0_0;
extern Il2CppType t46_0_0_0;
static ParameterInfo t6132_m31920_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t46_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31920_GM;
MethodInfo m31920_MI = 
{
	"Add", NULL, &t6132_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6132_m31920_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31920_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31921_GM;
MethodInfo m31921_MI = 
{
	"Clear", NULL, &t6132_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31921_GM};
extern Il2CppType t46_0_0_0;
static ParameterInfo t6132_m31922_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t46_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31922_GM;
MethodInfo m31922_MI = 
{
	"Contains", NULL, &t6132_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6132_m31922_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31922_GM};
extern Il2CppType t3587_0_0_0;
extern Il2CppType t3587_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6132_m31923_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3587_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31923_GM;
MethodInfo m31923_MI = 
{
	"CopyTo", NULL, &t6132_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6132_m31923_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31923_GM};
extern Il2CppType t46_0_0_0;
static ParameterInfo t6132_m31924_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t46_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31924_GM;
MethodInfo m31924_MI = 
{
	"Remove", NULL, &t6132_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6132_m31924_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31924_GM};
static MethodInfo* t6132_MIs[] =
{
	&m31918_MI,
	&m31919_MI,
	&m31920_MI,
	&m31921_MI,
	&m31922_MI,
	&m31923_MI,
	&m31924_MI,
	NULL
};
extern TypeInfo t6134_TI;
static TypeInfo* t6132_ITIs[] = 
{
	&t603_TI,
	&t6134_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6132_0_0_0;
extern Il2CppType t6132_1_0_0;
struct t6132;
extern Il2CppGenericClass t6132_GC;
TypeInfo t6132_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6132_MIs, t6132_PIs, NULL, NULL, NULL, NULL, NULL, &t6132_TI, t6132_ITIs, NULL, &EmptyCustomAttributesCache, &t6132_TI, &t6132_0_0_0, &t6132_1_0_0, NULL, &t6132_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.CompilerServices.RuntimeCompatibilityAttribute>
extern Il2CppType t4728_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31925_GM;
MethodInfo m31925_MI = 
{
	"GetEnumerator", NULL, &t6134_TI, &t4728_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31925_GM};
static MethodInfo* t6134_MIs[] =
{
	&m31925_MI,
	NULL
};
static TypeInfo* t6134_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6134_0_0_0;
extern Il2CppType t6134_1_0_0;
struct t6134;
extern Il2CppGenericClass t6134_GC;
TypeInfo t6134_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6134_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6134_TI, t6134_ITIs, NULL, &EmptyCustomAttributesCache, &t6134_TI, &t6134_0_0_0, &t6134_1_0_0, NULL, &t6134_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6133_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.CompilerServices.RuntimeCompatibilityAttribute>
extern MethodInfo m31926_MI;
extern MethodInfo m31927_MI;
static PropertyInfo t6133____Item_PropertyInfo = 
{
	&t6133_TI, "Item", &m31926_MI, &m31927_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6133_PIs[] =
{
	&t6133____Item_PropertyInfo,
	NULL
};
extern Il2CppType t46_0_0_0;
static ParameterInfo t6133_m31928_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t46_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31928_GM;
MethodInfo m31928_MI = 
{
	"IndexOf", NULL, &t6133_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6133_m31928_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31928_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t46_0_0_0;
static ParameterInfo t6133_m31929_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t46_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31929_GM;
MethodInfo m31929_MI = 
{
	"Insert", NULL, &t6133_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6133_m31929_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31929_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6133_m31930_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31930_GM;
MethodInfo m31930_MI = 
{
	"RemoveAt", NULL, &t6133_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6133_m31930_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31930_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6133_m31926_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t46_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31926_GM;
MethodInfo m31926_MI = 
{
	"get_Item", NULL, &t6133_TI, &t46_0_0_0, RuntimeInvoker_t29_t44, t6133_m31926_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31926_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t46_0_0_0;
static ParameterInfo t6133_m31927_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t46_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31927_GM;
MethodInfo m31927_MI = 
{
	"set_Item", NULL, &t6133_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6133_m31927_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31927_GM};
static MethodInfo* t6133_MIs[] =
{
	&m31928_MI,
	&m31929_MI,
	&m31930_MI,
	&m31926_MI,
	&m31927_MI,
	NULL
};
static TypeInfo* t6133_ITIs[] = 
{
	&t603_TI,
	&t6132_TI,
	&t6134_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6133_0_0_0;
extern Il2CppType t6133_1_0_0;
struct t6133;
extern Il2CppGenericClass t6133_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6133_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6133_MIs, t6133_PIs, NULL, NULL, NULL, NULL, NULL, &t6133_TI, t6133_ITIs, NULL, &t1908__CustomAttributeCache, &t6133_TI, &t6133_0_0_0, &t6133_1_0_0, NULL, &t6133_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4730_TI;

#include "t342.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Diagnostics.DebuggerHiddenAttribute>
extern MethodInfo m31931_MI;
static PropertyInfo t4730____Current_PropertyInfo = 
{
	&t4730_TI, "Current", &m31931_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4730_PIs[] =
{
	&t4730____Current_PropertyInfo,
	NULL
};
extern Il2CppType t342_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31931_GM;
MethodInfo m31931_MI = 
{
	"get_Current", NULL, &t4730_TI, &t342_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31931_GM};
static MethodInfo* t4730_MIs[] =
{
	&m31931_MI,
	NULL
};
static TypeInfo* t4730_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4730_0_0_0;
extern Il2CppType t4730_1_0_0;
struct t4730;
extern Il2CppGenericClass t4730_GC;
TypeInfo t4730_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4730_MIs, t4730_PIs, NULL, NULL, NULL, NULL, NULL, &t4730_TI, t4730_ITIs, NULL, &EmptyCustomAttributesCache, &t4730_TI, &t4730_0_0_0, &t4730_1_0_0, NULL, &t4730_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3312.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3312_TI;
#include "t3312MD.h"

extern TypeInfo t342_TI;
extern MethodInfo m18446_MI;
extern MethodInfo m24421_MI;
struct t20;
#define m24421(__this, p0, method) (t342 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Diagnostics.DebuggerHiddenAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3312_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3312_TI, offsetof(t3312, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3312_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3312_TI, offsetof(t3312, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3312_FIs[] =
{
	&t3312_f0_FieldInfo,
	&t3312_f1_FieldInfo,
	NULL
};
extern MethodInfo m18443_MI;
static PropertyInfo t3312____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3312_TI, "System.Collections.IEnumerator.Current", &m18443_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3312____Current_PropertyInfo = 
{
	&t3312_TI, "Current", &m18446_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3312_PIs[] =
{
	&t3312____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3312____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3312_m18442_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18442_GM;
MethodInfo m18442_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3312_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3312_m18442_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18442_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18443_GM;
MethodInfo m18443_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3312_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18443_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18444_GM;
MethodInfo m18444_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3312_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18444_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18445_GM;
MethodInfo m18445_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3312_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18445_GM};
extern Il2CppType t342_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18446_GM;
MethodInfo m18446_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3312_TI, &t342_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18446_GM};
static MethodInfo* t3312_MIs[] =
{
	&m18442_MI,
	&m18443_MI,
	&m18444_MI,
	&m18445_MI,
	&m18446_MI,
	NULL
};
extern MethodInfo m18445_MI;
extern MethodInfo m18444_MI;
static MethodInfo* t3312_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18443_MI,
	&m18445_MI,
	&m18444_MI,
	&m18446_MI,
};
static TypeInfo* t3312_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4730_TI,
};
static Il2CppInterfaceOffsetPair t3312_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4730_TI, 7},
};
extern TypeInfo t342_TI;
static Il2CppRGCTXData t3312_RGCTXData[3] = 
{
	&m18446_MI/* Method Usage */,
	&t342_TI/* Class Usage */,
	&m24421_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3312_0_0_0;
extern Il2CppType t3312_1_0_0;
extern Il2CppGenericClass t3312_GC;
TypeInfo t3312_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3312_MIs, t3312_PIs, t3312_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3312_TI, t3312_ITIs, t3312_VT, &EmptyCustomAttributesCache, &t3312_TI, &t3312_0_0_0, &t3312_1_0_0, t3312_IOs, &t3312_GC, NULL, NULL, NULL, t3312_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3312)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6135_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Diagnostics.DebuggerHiddenAttribute>
extern MethodInfo m31932_MI;
static PropertyInfo t6135____Count_PropertyInfo = 
{
	&t6135_TI, "Count", &m31932_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31933_MI;
static PropertyInfo t6135____IsReadOnly_PropertyInfo = 
{
	&t6135_TI, "IsReadOnly", &m31933_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6135_PIs[] =
{
	&t6135____Count_PropertyInfo,
	&t6135____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31932_GM;
MethodInfo m31932_MI = 
{
	"get_Count", NULL, &t6135_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31932_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31933_GM;
MethodInfo m31933_MI = 
{
	"get_IsReadOnly", NULL, &t6135_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31933_GM};
extern Il2CppType t342_0_0_0;
extern Il2CppType t342_0_0_0;
static ParameterInfo t6135_m31934_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t342_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31934_GM;
MethodInfo m31934_MI = 
{
	"Add", NULL, &t6135_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6135_m31934_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31934_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31935_GM;
MethodInfo m31935_MI = 
{
	"Clear", NULL, &t6135_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31935_GM};
extern Il2CppType t342_0_0_0;
static ParameterInfo t6135_m31936_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t342_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31936_GM;
MethodInfo m31936_MI = 
{
	"Contains", NULL, &t6135_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6135_m31936_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31936_GM};
extern Il2CppType t3588_0_0_0;
extern Il2CppType t3588_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6135_m31937_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3588_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31937_GM;
MethodInfo m31937_MI = 
{
	"CopyTo", NULL, &t6135_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6135_m31937_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31937_GM};
extern Il2CppType t342_0_0_0;
static ParameterInfo t6135_m31938_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t342_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31938_GM;
MethodInfo m31938_MI = 
{
	"Remove", NULL, &t6135_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6135_m31938_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31938_GM};
static MethodInfo* t6135_MIs[] =
{
	&m31932_MI,
	&m31933_MI,
	&m31934_MI,
	&m31935_MI,
	&m31936_MI,
	&m31937_MI,
	&m31938_MI,
	NULL
};
extern TypeInfo t6137_TI;
static TypeInfo* t6135_ITIs[] = 
{
	&t603_TI,
	&t6137_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6135_0_0_0;
extern Il2CppType t6135_1_0_0;
struct t6135;
extern Il2CppGenericClass t6135_GC;
TypeInfo t6135_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6135_MIs, t6135_PIs, NULL, NULL, NULL, NULL, NULL, &t6135_TI, t6135_ITIs, NULL, &EmptyCustomAttributesCache, &t6135_TI, &t6135_0_0_0, &t6135_1_0_0, NULL, &t6135_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Diagnostics.DebuggerHiddenAttribute>
extern Il2CppType t4730_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31939_GM;
MethodInfo m31939_MI = 
{
	"GetEnumerator", NULL, &t6137_TI, &t4730_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31939_GM};
static MethodInfo* t6137_MIs[] =
{
	&m31939_MI,
	NULL
};
static TypeInfo* t6137_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6137_0_0_0;
extern Il2CppType t6137_1_0_0;
struct t6137;
extern Il2CppGenericClass t6137_GC;
TypeInfo t6137_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6137_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6137_TI, t6137_ITIs, NULL, &EmptyCustomAttributesCache, &t6137_TI, &t6137_0_0_0, &t6137_1_0_0, NULL, &t6137_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6136_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Diagnostics.DebuggerHiddenAttribute>
extern MethodInfo m31940_MI;
extern MethodInfo m31941_MI;
static PropertyInfo t6136____Item_PropertyInfo = 
{
	&t6136_TI, "Item", &m31940_MI, &m31941_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6136_PIs[] =
{
	&t6136____Item_PropertyInfo,
	NULL
};
extern Il2CppType t342_0_0_0;
static ParameterInfo t6136_m31942_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t342_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31942_GM;
MethodInfo m31942_MI = 
{
	"IndexOf", NULL, &t6136_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6136_m31942_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31942_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t342_0_0_0;
static ParameterInfo t6136_m31943_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t342_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31943_GM;
MethodInfo m31943_MI = 
{
	"Insert", NULL, &t6136_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6136_m31943_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31943_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6136_m31944_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31944_GM;
MethodInfo m31944_MI = 
{
	"RemoveAt", NULL, &t6136_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6136_m31944_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31944_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6136_m31940_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t342_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31940_GM;
MethodInfo m31940_MI = 
{
	"get_Item", NULL, &t6136_TI, &t342_0_0_0, RuntimeInvoker_t29_t44, t6136_m31940_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31940_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t342_0_0_0;
static ParameterInfo t6136_m31941_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t342_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31941_GM;
MethodInfo m31941_MI = 
{
	"set_Item", NULL, &t6136_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6136_m31941_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31941_GM};
static MethodInfo* t6136_MIs[] =
{
	&m31942_MI,
	&m31943_MI,
	&m31944_MI,
	&m31940_MI,
	&m31941_MI,
	NULL
};
static TypeInfo* t6136_ITIs[] = 
{
	&t603_TI,
	&t6135_TI,
	&t6137_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6136_0_0_0;
extern Il2CppType t6136_1_0_0;
struct t6136;
extern Il2CppGenericClass t6136_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6136_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6136_MIs, t6136_PIs, NULL, NULL, NULL, NULL, NULL, &t6136_TI, t6136_ITIs, NULL, &t1908__CustomAttributeCache, &t6136_TI, &t6136_0_0_0, &t6136_1_0_0, NULL, &t6136_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4732_TI;

#include "t425.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.DefaultMemberAttribute>
extern MethodInfo m31945_MI;
static PropertyInfo t4732____Current_PropertyInfo = 
{
	&t4732_TI, "Current", &m31945_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4732_PIs[] =
{
	&t4732____Current_PropertyInfo,
	NULL
};
extern Il2CppType t425_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31945_GM;
MethodInfo m31945_MI = 
{
	"get_Current", NULL, &t4732_TI, &t425_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31945_GM};
static MethodInfo* t4732_MIs[] =
{
	&m31945_MI,
	NULL
};
static TypeInfo* t4732_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4732_0_0_0;
extern Il2CppType t4732_1_0_0;
struct t4732;
extern Il2CppGenericClass t4732_GC;
TypeInfo t4732_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4732_MIs, t4732_PIs, NULL, NULL, NULL, NULL, NULL, &t4732_TI, t4732_ITIs, NULL, &EmptyCustomAttributesCache, &t4732_TI, &t4732_0_0_0, &t4732_1_0_0, NULL, &t4732_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3313.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3313_TI;
#include "t3313MD.h"

extern TypeInfo t425_TI;
extern MethodInfo m18451_MI;
extern MethodInfo m24432_MI;
struct t20;
#define m24432(__this, p0, method) (t425 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.DefaultMemberAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3313_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3313_TI, offsetof(t3313, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3313_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3313_TI, offsetof(t3313, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3313_FIs[] =
{
	&t3313_f0_FieldInfo,
	&t3313_f1_FieldInfo,
	NULL
};
extern MethodInfo m18448_MI;
static PropertyInfo t3313____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3313_TI, "System.Collections.IEnumerator.Current", &m18448_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3313____Current_PropertyInfo = 
{
	&t3313_TI, "Current", &m18451_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3313_PIs[] =
{
	&t3313____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3313____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3313_m18447_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18447_GM;
MethodInfo m18447_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3313_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3313_m18447_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18447_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18448_GM;
MethodInfo m18448_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3313_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18448_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18449_GM;
MethodInfo m18449_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3313_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18449_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18450_GM;
MethodInfo m18450_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3313_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18450_GM};
extern Il2CppType t425_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18451_GM;
MethodInfo m18451_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3313_TI, &t425_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18451_GM};
static MethodInfo* t3313_MIs[] =
{
	&m18447_MI,
	&m18448_MI,
	&m18449_MI,
	&m18450_MI,
	&m18451_MI,
	NULL
};
extern MethodInfo m18450_MI;
extern MethodInfo m18449_MI;
static MethodInfo* t3313_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18448_MI,
	&m18450_MI,
	&m18449_MI,
	&m18451_MI,
};
static TypeInfo* t3313_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4732_TI,
};
static Il2CppInterfaceOffsetPair t3313_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4732_TI, 7},
};
extern TypeInfo t425_TI;
static Il2CppRGCTXData t3313_RGCTXData[3] = 
{
	&m18451_MI/* Method Usage */,
	&t425_TI/* Class Usage */,
	&m24432_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3313_0_0_0;
extern Il2CppType t3313_1_0_0;
extern Il2CppGenericClass t3313_GC;
TypeInfo t3313_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3313_MIs, t3313_PIs, t3313_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3313_TI, t3313_ITIs, t3313_VT, &EmptyCustomAttributesCache, &t3313_TI, &t3313_0_0_0, &t3313_1_0_0, t3313_IOs, &t3313_GC, NULL, NULL, NULL, t3313_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3313)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6138_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.DefaultMemberAttribute>
extern MethodInfo m31946_MI;
static PropertyInfo t6138____Count_PropertyInfo = 
{
	&t6138_TI, "Count", &m31946_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31947_MI;
static PropertyInfo t6138____IsReadOnly_PropertyInfo = 
{
	&t6138_TI, "IsReadOnly", &m31947_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6138_PIs[] =
{
	&t6138____Count_PropertyInfo,
	&t6138____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31946_GM;
MethodInfo m31946_MI = 
{
	"get_Count", NULL, &t6138_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31946_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31947_GM;
MethodInfo m31947_MI = 
{
	"get_IsReadOnly", NULL, &t6138_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31947_GM};
extern Il2CppType t425_0_0_0;
extern Il2CppType t425_0_0_0;
static ParameterInfo t6138_m31948_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t425_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31948_GM;
MethodInfo m31948_MI = 
{
	"Add", NULL, &t6138_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6138_m31948_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31948_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31949_GM;
MethodInfo m31949_MI = 
{
	"Clear", NULL, &t6138_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31949_GM};
extern Il2CppType t425_0_0_0;
static ParameterInfo t6138_m31950_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t425_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31950_GM;
MethodInfo m31950_MI = 
{
	"Contains", NULL, &t6138_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6138_m31950_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31950_GM};
extern Il2CppType t3589_0_0_0;
extern Il2CppType t3589_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6138_m31951_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3589_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31951_GM;
MethodInfo m31951_MI = 
{
	"CopyTo", NULL, &t6138_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6138_m31951_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31951_GM};
extern Il2CppType t425_0_0_0;
static ParameterInfo t6138_m31952_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t425_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31952_GM;
MethodInfo m31952_MI = 
{
	"Remove", NULL, &t6138_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6138_m31952_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31952_GM};
static MethodInfo* t6138_MIs[] =
{
	&m31946_MI,
	&m31947_MI,
	&m31948_MI,
	&m31949_MI,
	&m31950_MI,
	&m31951_MI,
	&m31952_MI,
	NULL
};
extern TypeInfo t6140_TI;
static TypeInfo* t6138_ITIs[] = 
{
	&t603_TI,
	&t6140_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6138_0_0_0;
extern Il2CppType t6138_1_0_0;
struct t6138;
extern Il2CppGenericClass t6138_GC;
TypeInfo t6138_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6138_MIs, t6138_PIs, NULL, NULL, NULL, NULL, NULL, &t6138_TI, t6138_ITIs, NULL, &EmptyCustomAttributesCache, &t6138_TI, &t6138_0_0_0, &t6138_1_0_0, NULL, &t6138_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.DefaultMemberAttribute>
extern Il2CppType t4732_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31953_GM;
MethodInfo m31953_MI = 
{
	"GetEnumerator", NULL, &t6140_TI, &t4732_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31953_GM};
static MethodInfo* t6140_MIs[] =
{
	&m31953_MI,
	NULL
};
static TypeInfo* t6140_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6140_0_0_0;
extern Il2CppType t6140_1_0_0;
struct t6140;
extern Il2CppGenericClass t6140_GC;
TypeInfo t6140_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6140_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6140_TI, t6140_ITIs, NULL, &EmptyCustomAttributesCache, &t6140_TI, &t6140_0_0_0, &t6140_1_0_0, NULL, &t6140_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6139_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.DefaultMemberAttribute>
extern MethodInfo m31954_MI;
extern MethodInfo m31955_MI;
static PropertyInfo t6139____Item_PropertyInfo = 
{
	&t6139_TI, "Item", &m31954_MI, &m31955_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6139_PIs[] =
{
	&t6139____Item_PropertyInfo,
	NULL
};
extern Il2CppType t425_0_0_0;
static ParameterInfo t6139_m31956_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t425_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31956_GM;
MethodInfo m31956_MI = 
{
	"IndexOf", NULL, &t6139_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6139_m31956_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31956_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t425_0_0_0;
static ParameterInfo t6139_m31957_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t425_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31957_GM;
MethodInfo m31957_MI = 
{
	"Insert", NULL, &t6139_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6139_m31957_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31957_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6139_m31958_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31958_GM;
MethodInfo m31958_MI = 
{
	"RemoveAt", NULL, &t6139_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6139_m31958_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31958_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6139_m31954_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t425_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31954_GM;
MethodInfo m31954_MI = 
{
	"get_Item", NULL, &t6139_TI, &t425_0_0_0, RuntimeInvoker_t29_t44, t6139_m31954_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31954_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t425_0_0_0;
static ParameterInfo t6139_m31955_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t425_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31955_GM;
MethodInfo m31955_MI = 
{
	"set_Item", NULL, &t6139_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6139_m31955_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31955_GM};
static MethodInfo* t6139_MIs[] =
{
	&m31956_MI,
	&m31957_MI,
	&m31958_MI,
	&m31954_MI,
	&m31955_MI,
	NULL
};
static TypeInfo* t6139_ITIs[] = 
{
	&t603_TI,
	&t6138_TI,
	&t6140_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6139_0_0_0;
extern Il2CppType t6139_1_0_0;
struct t6139;
extern Il2CppGenericClass t6139_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6139_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6139_MIs, t6139_PIs, NULL, NULL, NULL, NULL, NULL, &t6139_TI, t6139_ITIs, NULL, &t1908__CustomAttributeCache, &t6139_TI, &t6139_0_0_0, &t6139_1_0_0, NULL, &t6139_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4734_TI;

#include "t1160.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.CompilerServices.DecimalConstantAttribute>
extern MethodInfo m31959_MI;
static PropertyInfo t4734____Current_PropertyInfo = 
{
	&t4734_TI, "Current", &m31959_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4734_PIs[] =
{
	&t4734____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1160_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31959_GM;
MethodInfo m31959_MI = 
{
	"get_Current", NULL, &t4734_TI, &t1160_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31959_GM};
static MethodInfo* t4734_MIs[] =
{
	&m31959_MI,
	NULL
};
static TypeInfo* t4734_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4734_0_0_0;
extern Il2CppType t4734_1_0_0;
struct t4734;
extern Il2CppGenericClass t4734_GC;
TypeInfo t4734_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4734_MIs, t4734_PIs, NULL, NULL, NULL, NULL, NULL, &t4734_TI, t4734_ITIs, NULL, &EmptyCustomAttributesCache, &t4734_TI, &t4734_0_0_0, &t4734_1_0_0, NULL, &t4734_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3314.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3314_TI;
#include "t3314MD.h"

extern TypeInfo t1160_TI;
extern MethodInfo m18456_MI;
extern MethodInfo m24443_MI;
struct t20;
#define m24443(__this, p0, method) (t1160 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.DecimalConstantAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3314_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3314_TI, offsetof(t3314, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3314_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3314_TI, offsetof(t3314, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3314_FIs[] =
{
	&t3314_f0_FieldInfo,
	&t3314_f1_FieldInfo,
	NULL
};
extern MethodInfo m18453_MI;
static PropertyInfo t3314____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3314_TI, "System.Collections.IEnumerator.Current", &m18453_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3314____Current_PropertyInfo = 
{
	&t3314_TI, "Current", &m18456_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3314_PIs[] =
{
	&t3314____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3314____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3314_m18452_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18452_GM;
MethodInfo m18452_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3314_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3314_m18452_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18452_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18453_GM;
MethodInfo m18453_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3314_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18453_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18454_GM;
MethodInfo m18454_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3314_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18454_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18455_GM;
MethodInfo m18455_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3314_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18455_GM};
extern Il2CppType t1160_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18456_GM;
MethodInfo m18456_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3314_TI, &t1160_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18456_GM};
static MethodInfo* t3314_MIs[] =
{
	&m18452_MI,
	&m18453_MI,
	&m18454_MI,
	&m18455_MI,
	&m18456_MI,
	NULL
};
extern MethodInfo m18455_MI;
extern MethodInfo m18454_MI;
static MethodInfo* t3314_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18453_MI,
	&m18455_MI,
	&m18454_MI,
	&m18456_MI,
};
static TypeInfo* t3314_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4734_TI,
};
static Il2CppInterfaceOffsetPair t3314_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4734_TI, 7},
};
extern TypeInfo t1160_TI;
static Il2CppRGCTXData t3314_RGCTXData[3] = 
{
	&m18456_MI/* Method Usage */,
	&t1160_TI/* Class Usage */,
	&m24443_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3314_0_0_0;
extern Il2CppType t3314_1_0_0;
extern Il2CppGenericClass t3314_GC;
TypeInfo t3314_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3314_MIs, t3314_PIs, t3314_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3314_TI, t3314_ITIs, t3314_VT, &EmptyCustomAttributesCache, &t3314_TI, &t3314_0_0_0, &t3314_1_0_0, t3314_IOs, &t3314_GC, NULL, NULL, NULL, t3314_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3314)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6141_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.DecimalConstantAttribute>
extern MethodInfo m31960_MI;
static PropertyInfo t6141____Count_PropertyInfo = 
{
	&t6141_TI, "Count", &m31960_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31961_MI;
static PropertyInfo t6141____IsReadOnly_PropertyInfo = 
{
	&t6141_TI, "IsReadOnly", &m31961_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6141_PIs[] =
{
	&t6141____Count_PropertyInfo,
	&t6141____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31960_GM;
MethodInfo m31960_MI = 
{
	"get_Count", NULL, &t6141_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31960_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31961_GM;
MethodInfo m31961_MI = 
{
	"get_IsReadOnly", NULL, &t6141_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31961_GM};
extern Il2CppType t1160_0_0_0;
extern Il2CppType t1160_0_0_0;
static ParameterInfo t6141_m31962_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1160_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31962_GM;
MethodInfo m31962_MI = 
{
	"Add", NULL, &t6141_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6141_m31962_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31962_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31963_GM;
MethodInfo m31963_MI = 
{
	"Clear", NULL, &t6141_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31963_GM};
extern Il2CppType t1160_0_0_0;
static ParameterInfo t6141_m31964_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1160_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31964_GM;
MethodInfo m31964_MI = 
{
	"Contains", NULL, &t6141_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6141_m31964_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31964_GM};
extern Il2CppType t3590_0_0_0;
extern Il2CppType t3590_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6141_m31965_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3590_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31965_GM;
MethodInfo m31965_MI = 
{
	"CopyTo", NULL, &t6141_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6141_m31965_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31965_GM};
extern Il2CppType t1160_0_0_0;
static ParameterInfo t6141_m31966_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1160_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31966_GM;
MethodInfo m31966_MI = 
{
	"Remove", NULL, &t6141_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6141_m31966_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31966_GM};
static MethodInfo* t6141_MIs[] =
{
	&m31960_MI,
	&m31961_MI,
	&m31962_MI,
	&m31963_MI,
	&m31964_MI,
	&m31965_MI,
	&m31966_MI,
	NULL
};
extern TypeInfo t6143_TI;
static TypeInfo* t6141_ITIs[] = 
{
	&t603_TI,
	&t6143_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6141_0_0_0;
extern Il2CppType t6141_1_0_0;
struct t6141;
extern Il2CppGenericClass t6141_GC;
TypeInfo t6141_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6141_MIs, t6141_PIs, NULL, NULL, NULL, NULL, NULL, &t6141_TI, t6141_ITIs, NULL, &EmptyCustomAttributesCache, &t6141_TI, &t6141_0_0_0, &t6141_1_0_0, NULL, &t6141_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.CompilerServices.DecimalConstantAttribute>
extern Il2CppType t4734_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31967_GM;
MethodInfo m31967_MI = 
{
	"GetEnumerator", NULL, &t6143_TI, &t4734_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31967_GM};
static MethodInfo* t6143_MIs[] =
{
	&m31967_MI,
	NULL
};
static TypeInfo* t6143_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6143_0_0_0;
extern Il2CppType t6143_1_0_0;
struct t6143;
extern Il2CppGenericClass t6143_GC;
TypeInfo t6143_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6143_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6143_TI, t6143_ITIs, NULL, &EmptyCustomAttributesCache, &t6143_TI, &t6143_0_0_0, &t6143_1_0_0, NULL, &t6143_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6142_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.CompilerServices.DecimalConstantAttribute>
extern MethodInfo m31968_MI;
extern MethodInfo m31969_MI;
static PropertyInfo t6142____Item_PropertyInfo = 
{
	&t6142_TI, "Item", &m31968_MI, &m31969_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6142_PIs[] =
{
	&t6142____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1160_0_0_0;
static ParameterInfo t6142_m31970_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1160_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31970_GM;
MethodInfo m31970_MI = 
{
	"IndexOf", NULL, &t6142_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6142_m31970_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31970_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1160_0_0_0;
static ParameterInfo t6142_m31971_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1160_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31971_GM;
MethodInfo m31971_MI = 
{
	"Insert", NULL, &t6142_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6142_m31971_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31971_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6142_m31972_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31972_GM;
MethodInfo m31972_MI = 
{
	"RemoveAt", NULL, &t6142_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6142_m31972_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31972_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6142_m31968_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1160_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31968_GM;
MethodInfo m31968_MI = 
{
	"get_Item", NULL, &t6142_TI, &t1160_0_0_0, RuntimeInvoker_t29_t44, t6142_m31968_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31968_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1160_0_0_0;
static ParameterInfo t6142_m31969_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1160_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31969_GM;
MethodInfo m31969_MI = 
{
	"set_Item", NULL, &t6142_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6142_m31969_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31969_GM};
static MethodInfo* t6142_MIs[] =
{
	&m31970_MI,
	&m31971_MI,
	&m31972_MI,
	&m31968_MI,
	&m31969_MI,
	NULL
};
static TypeInfo* t6142_ITIs[] = 
{
	&t603_TI,
	&t6141_TI,
	&t6143_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6142_0_0_0;
extern Il2CppType t6142_1_0_0;
struct t6142;
extern Il2CppGenericClass t6142_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6142_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6142_MIs, t6142_PIs, NULL, NULL, NULL, NULL, NULL, &t6142_TI, t6142_ITIs, NULL, &t1908__CustomAttributeCache, &t6142_TI, &t6142_0_0_0, &t6142_1_0_0, NULL, &t6142_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4736_TI;

#include "t1161.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.FieldOffsetAttribute>
extern MethodInfo m31973_MI;
static PropertyInfo t4736____Current_PropertyInfo = 
{
	&t4736_TI, "Current", &m31973_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4736_PIs[] =
{
	&t4736____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1161_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31973_GM;
MethodInfo m31973_MI = 
{
	"get_Current", NULL, &t4736_TI, &t1161_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31973_GM};
static MethodInfo* t4736_MIs[] =
{
	&m31973_MI,
	NULL
};
static TypeInfo* t4736_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4736_0_0_0;
extern Il2CppType t4736_1_0_0;
struct t4736;
extern Il2CppGenericClass t4736_GC;
TypeInfo t4736_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4736_MIs, t4736_PIs, NULL, NULL, NULL, NULL, NULL, &t4736_TI, t4736_ITIs, NULL, &EmptyCustomAttributesCache, &t4736_TI, &t4736_0_0_0, &t4736_1_0_0, NULL, &t4736_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3315.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3315_TI;
#include "t3315MD.h"

extern TypeInfo t1161_TI;
extern MethodInfo m18461_MI;
extern MethodInfo m24454_MI;
struct t20;
#define m24454(__this, p0, method) (t1161 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices.FieldOffsetAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3315_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3315_TI, offsetof(t3315, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3315_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3315_TI, offsetof(t3315, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3315_FIs[] =
{
	&t3315_f0_FieldInfo,
	&t3315_f1_FieldInfo,
	NULL
};
extern MethodInfo m18458_MI;
static PropertyInfo t3315____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3315_TI, "System.Collections.IEnumerator.Current", &m18458_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3315____Current_PropertyInfo = 
{
	&t3315_TI, "Current", &m18461_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3315_PIs[] =
{
	&t3315____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3315____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3315_m18457_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18457_GM;
MethodInfo m18457_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3315_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3315_m18457_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18457_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18458_GM;
MethodInfo m18458_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3315_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18458_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18459_GM;
MethodInfo m18459_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3315_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18459_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18460_GM;
MethodInfo m18460_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3315_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18460_GM};
extern Il2CppType t1161_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18461_GM;
MethodInfo m18461_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3315_TI, &t1161_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18461_GM};
static MethodInfo* t3315_MIs[] =
{
	&m18457_MI,
	&m18458_MI,
	&m18459_MI,
	&m18460_MI,
	&m18461_MI,
	NULL
};
extern MethodInfo m18460_MI;
extern MethodInfo m18459_MI;
static MethodInfo* t3315_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18458_MI,
	&m18460_MI,
	&m18459_MI,
	&m18461_MI,
};
static TypeInfo* t3315_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4736_TI,
};
static Il2CppInterfaceOffsetPair t3315_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4736_TI, 7},
};
extern TypeInfo t1161_TI;
static Il2CppRGCTXData t3315_RGCTXData[3] = 
{
	&m18461_MI/* Method Usage */,
	&t1161_TI/* Class Usage */,
	&m24454_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3315_0_0_0;
extern Il2CppType t3315_1_0_0;
extern Il2CppGenericClass t3315_GC;
TypeInfo t3315_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3315_MIs, t3315_PIs, t3315_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3315_TI, t3315_ITIs, t3315_VT, &EmptyCustomAttributesCache, &t3315_TI, &t3315_0_0_0, &t3315_1_0_0, t3315_IOs, &t3315_GC, NULL, NULL, NULL, t3315_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3315)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6144_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.FieldOffsetAttribute>
extern MethodInfo m31974_MI;
static PropertyInfo t6144____Count_PropertyInfo = 
{
	&t6144_TI, "Count", &m31974_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31975_MI;
static PropertyInfo t6144____IsReadOnly_PropertyInfo = 
{
	&t6144_TI, "IsReadOnly", &m31975_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6144_PIs[] =
{
	&t6144____Count_PropertyInfo,
	&t6144____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31974_GM;
MethodInfo m31974_MI = 
{
	"get_Count", NULL, &t6144_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31974_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31975_GM;
MethodInfo m31975_MI = 
{
	"get_IsReadOnly", NULL, &t6144_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31975_GM};
extern Il2CppType t1161_0_0_0;
extern Il2CppType t1161_0_0_0;
static ParameterInfo t6144_m31976_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1161_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31976_GM;
MethodInfo m31976_MI = 
{
	"Add", NULL, &t6144_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6144_m31976_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31976_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31977_GM;
MethodInfo m31977_MI = 
{
	"Clear", NULL, &t6144_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31977_GM};
extern Il2CppType t1161_0_0_0;
static ParameterInfo t6144_m31978_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1161_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31978_GM;
MethodInfo m31978_MI = 
{
	"Contains", NULL, &t6144_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6144_m31978_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31978_GM};
extern Il2CppType t3591_0_0_0;
extern Il2CppType t3591_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6144_m31979_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3591_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31979_GM;
MethodInfo m31979_MI = 
{
	"CopyTo", NULL, &t6144_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6144_m31979_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31979_GM};
extern Il2CppType t1161_0_0_0;
static ParameterInfo t6144_m31980_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1161_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31980_GM;
MethodInfo m31980_MI = 
{
	"Remove", NULL, &t6144_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6144_m31980_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31980_GM};
static MethodInfo* t6144_MIs[] =
{
	&m31974_MI,
	&m31975_MI,
	&m31976_MI,
	&m31977_MI,
	&m31978_MI,
	&m31979_MI,
	&m31980_MI,
	NULL
};
extern TypeInfo t6146_TI;
static TypeInfo* t6144_ITIs[] = 
{
	&t603_TI,
	&t6146_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6144_0_0_0;
extern Il2CppType t6144_1_0_0;
struct t6144;
extern Il2CppGenericClass t6144_GC;
TypeInfo t6144_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6144_MIs, t6144_PIs, NULL, NULL, NULL, NULL, NULL, &t6144_TI, t6144_ITIs, NULL, &EmptyCustomAttributesCache, &t6144_TI, &t6144_0_0_0, &t6144_1_0_0, NULL, &t6144_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.FieldOffsetAttribute>
extern Il2CppType t4736_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31981_GM;
MethodInfo m31981_MI = 
{
	"GetEnumerator", NULL, &t6146_TI, &t4736_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31981_GM};
static MethodInfo* t6146_MIs[] =
{
	&m31981_MI,
	NULL
};
static TypeInfo* t6146_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6146_0_0_0;
extern Il2CppType t6146_1_0_0;
struct t6146;
extern Il2CppGenericClass t6146_GC;
TypeInfo t6146_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6146_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6146_TI, t6146_ITIs, NULL, &EmptyCustomAttributesCache, &t6146_TI, &t6146_0_0_0, &t6146_1_0_0, NULL, &t6146_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6145_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices.FieldOffsetAttribute>
extern MethodInfo m31982_MI;
extern MethodInfo m31983_MI;
static PropertyInfo t6145____Item_PropertyInfo = 
{
	&t6145_TI, "Item", &m31982_MI, &m31983_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6145_PIs[] =
{
	&t6145____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1161_0_0_0;
static ParameterInfo t6145_m31984_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1161_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31984_GM;
MethodInfo m31984_MI = 
{
	"IndexOf", NULL, &t6145_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6145_m31984_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31984_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1161_0_0_0;
static ParameterInfo t6145_m31985_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1161_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31985_GM;
MethodInfo m31985_MI = 
{
	"Insert", NULL, &t6145_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6145_m31985_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31985_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6145_m31986_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31986_GM;
MethodInfo m31986_MI = 
{
	"RemoveAt", NULL, &t6145_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6145_m31986_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31986_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6145_m31982_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1161_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31982_GM;
MethodInfo m31982_MI = 
{
	"get_Item", NULL, &t6145_TI, &t1161_0_0_0, RuntimeInvoker_t29_t44, t6145_m31982_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31982_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1161_0_0_0;
static ParameterInfo t6145_m31983_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1161_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31983_GM;
MethodInfo m31983_MI = 
{
	"set_Item", NULL, &t6145_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6145_m31983_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31983_GM};
static MethodInfo* t6145_MIs[] =
{
	&m31984_MI,
	&m31985_MI,
	&m31986_MI,
	&m31982_MI,
	&m31983_MI,
	NULL
};
static TypeInfo* t6145_ITIs[] = 
{
	&t603_TI,
	&t6144_TI,
	&t6146_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6145_0_0_0;
extern Il2CppType t6145_1_0_0;
struct t6145;
extern Il2CppGenericClass t6145_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6145_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6145_MIs, t6145_PIs, NULL, NULL, NULL, NULL, NULL, &t6145_TI, t6145_ITIs, NULL, &t1908__CustomAttributeCache, &t6145_TI, &t6145_0_0_0, &t6145_1_0_0, NULL, &t6145_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4738_TI;

#include "t1168.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.MonoTODOAttribute>
extern MethodInfo m31987_MI;
static PropertyInfo t4738____Current_PropertyInfo = 
{
	&t4738_TI, "Current", &m31987_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4738_PIs[] =
{
	&t4738____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1168_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31987_GM;
MethodInfo m31987_MI = 
{
	"get_Current", NULL, &t4738_TI, &t1168_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31987_GM};
static MethodInfo* t4738_MIs[] =
{
	&m31987_MI,
	NULL
};
static TypeInfo* t4738_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4738_0_0_0;
extern Il2CppType t4738_1_0_0;
struct t4738;
extern Il2CppGenericClass t4738_GC;
TypeInfo t4738_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4738_MIs, t4738_PIs, NULL, NULL, NULL, NULL, NULL, &t4738_TI, t4738_ITIs, NULL, &EmptyCustomAttributesCache, &t4738_TI, &t4738_0_0_0, &t4738_1_0_0, NULL, &t4738_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3316.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3316_TI;
#include "t3316MD.h"

extern TypeInfo t1168_TI;
extern MethodInfo m18466_MI;
extern MethodInfo m24465_MI;
struct t20;
#define m24465(__this, p0, method) (t1168 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.MonoTODOAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3316_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3316_TI, offsetof(t3316, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3316_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3316_TI, offsetof(t3316, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3316_FIs[] =
{
	&t3316_f0_FieldInfo,
	&t3316_f1_FieldInfo,
	NULL
};
extern MethodInfo m18463_MI;
static PropertyInfo t3316____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3316_TI, "System.Collections.IEnumerator.Current", &m18463_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3316____Current_PropertyInfo = 
{
	&t3316_TI, "Current", &m18466_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3316_PIs[] =
{
	&t3316____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3316____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3316_m18462_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18462_GM;
MethodInfo m18462_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3316_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3316_m18462_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18462_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18463_GM;
MethodInfo m18463_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3316_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18463_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18464_GM;
MethodInfo m18464_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3316_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18464_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18465_GM;
MethodInfo m18465_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3316_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18465_GM};
extern Il2CppType t1168_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18466_GM;
MethodInfo m18466_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3316_TI, &t1168_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18466_GM};
static MethodInfo* t3316_MIs[] =
{
	&m18462_MI,
	&m18463_MI,
	&m18464_MI,
	&m18465_MI,
	&m18466_MI,
	NULL
};
extern MethodInfo m18465_MI;
extern MethodInfo m18464_MI;
static MethodInfo* t3316_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18463_MI,
	&m18465_MI,
	&m18464_MI,
	&m18466_MI,
};
static TypeInfo* t3316_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4738_TI,
};
static Il2CppInterfaceOffsetPair t3316_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4738_TI, 7},
};
extern TypeInfo t1168_TI;
static Il2CppRGCTXData t3316_RGCTXData[3] = 
{
	&m18466_MI/* Method Usage */,
	&t1168_TI/* Class Usage */,
	&m24465_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3316_0_0_0;
extern Il2CppType t3316_1_0_0;
extern Il2CppGenericClass t3316_GC;
TypeInfo t3316_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3316_MIs, t3316_PIs, t3316_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3316_TI, t3316_ITIs, t3316_VT, &EmptyCustomAttributesCache, &t3316_TI, &t3316_0_0_0, &t3316_1_0_0, t3316_IOs, &t3316_GC, NULL, NULL, NULL, t3316_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3316)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6147_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>
extern MethodInfo m31988_MI;
static PropertyInfo t6147____Count_PropertyInfo = 
{
	&t6147_TI, "Count", &m31988_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31989_MI;
static PropertyInfo t6147____IsReadOnly_PropertyInfo = 
{
	&t6147_TI, "IsReadOnly", &m31989_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6147_PIs[] =
{
	&t6147____Count_PropertyInfo,
	&t6147____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31988_GM;
MethodInfo m31988_MI = 
{
	"get_Count", NULL, &t6147_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31988_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31989_GM;
MethodInfo m31989_MI = 
{
	"get_IsReadOnly", NULL, &t6147_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31989_GM};
extern Il2CppType t1168_0_0_0;
extern Il2CppType t1168_0_0_0;
static ParameterInfo t6147_m31990_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1168_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31990_GM;
MethodInfo m31990_MI = 
{
	"Add", NULL, &t6147_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6147_m31990_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31990_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31991_GM;
MethodInfo m31991_MI = 
{
	"Clear", NULL, &t6147_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31991_GM};
extern Il2CppType t1168_0_0_0;
static ParameterInfo t6147_m31992_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1168_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31992_GM;
MethodInfo m31992_MI = 
{
	"Contains", NULL, &t6147_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6147_m31992_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31992_GM};
extern Il2CppType t3592_0_0_0;
extern Il2CppType t3592_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6147_m31993_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3592_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31993_GM;
MethodInfo m31993_MI = 
{
	"CopyTo", NULL, &t6147_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6147_m31993_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31993_GM};
extern Il2CppType t1168_0_0_0;
static ParameterInfo t6147_m31994_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1168_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31994_GM;
MethodInfo m31994_MI = 
{
	"Remove", NULL, &t6147_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6147_m31994_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31994_GM};
static MethodInfo* t6147_MIs[] =
{
	&m31988_MI,
	&m31989_MI,
	&m31990_MI,
	&m31991_MI,
	&m31992_MI,
	&m31993_MI,
	&m31994_MI,
	NULL
};
extern TypeInfo t6149_TI;
static TypeInfo* t6147_ITIs[] = 
{
	&t603_TI,
	&t6149_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6147_0_0_0;
extern Il2CppType t6147_1_0_0;
struct t6147;
extern Il2CppGenericClass t6147_GC;
TypeInfo t6147_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6147_MIs, t6147_PIs, NULL, NULL, NULL, NULL, NULL, &t6147_TI, t6147_ITIs, NULL, &EmptyCustomAttributesCache, &t6147_TI, &t6147_0_0_0, &t6147_1_0_0, NULL, &t6147_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.MonoTODOAttribute>
extern Il2CppType t4738_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31995_GM;
MethodInfo m31995_MI = 
{
	"GetEnumerator", NULL, &t6149_TI, &t4738_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31995_GM};
static MethodInfo* t6149_MIs[] =
{
	&m31995_MI,
	NULL
};
static TypeInfo* t6149_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6149_0_0_0;
extern Il2CppType t6149_1_0_0;
struct t6149;
extern Il2CppGenericClass t6149_GC;
TypeInfo t6149_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6149_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6149_TI, t6149_ITIs, NULL, &EmptyCustomAttributesCache, &t6149_TI, &t6149_0_0_0, &t6149_1_0_0, NULL, &t6149_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6148_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.MonoTODOAttribute>
extern MethodInfo m31996_MI;
extern MethodInfo m31997_MI;
static PropertyInfo t6148____Item_PropertyInfo = 
{
	&t6148_TI, "Item", &m31996_MI, &m31997_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6148_PIs[] =
{
	&t6148____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1168_0_0_0;
static ParameterInfo t6148_m31998_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1168_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31998_GM;
MethodInfo m31998_MI = 
{
	"IndexOf", NULL, &t6148_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6148_m31998_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31998_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1168_0_0_0;
static ParameterInfo t6148_m31999_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1168_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31999_GM;
MethodInfo m31999_MI = 
{
	"Insert", NULL, &t6148_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6148_m31999_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31999_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6148_m32000_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32000_GM;
MethodInfo m32000_MI = 
{
	"RemoveAt", NULL, &t6148_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6148_m32000_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32000_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6148_m31996_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1168_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31996_GM;
MethodInfo m31996_MI = 
{
	"get_Item", NULL, &t6148_TI, &t1168_0_0_0, RuntimeInvoker_t29_t44, t6148_m31996_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31996_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1168_0_0_0;
static ParameterInfo t6148_m31997_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1168_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31997_GM;
MethodInfo m31997_MI = 
{
	"set_Item", NULL, &t6148_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6148_m31997_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31997_GM};
static MethodInfo* t6148_MIs[] =
{
	&m31998_MI,
	&m31999_MI,
	&m32000_MI,
	&m31996_MI,
	&m31997_MI,
	NULL
};
static TypeInfo* t6148_ITIs[] = 
{
	&t603_TI,
	&t6147_TI,
	&t6149_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6148_0_0_0;
extern Il2CppType t6148_1_0_0;
struct t6148;
extern Il2CppGenericClass t6148_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6148_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6148_MIs, t6148_PIs, NULL, NULL, NULL, NULL, NULL, &t6148_TI, t6148_ITIs, NULL, &t1908__CustomAttributeCache, &t6148_TI, &t6148_0_0_0, &t6148_1_0_0, NULL, &t6148_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4740_TI;

#include "t1169.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.MonoDocumentationNoteAttribute>
extern MethodInfo m32001_MI;
static PropertyInfo t4740____Current_PropertyInfo = 
{
	&t4740_TI, "Current", &m32001_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4740_PIs[] =
{
	&t4740____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1169_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32001_GM;
MethodInfo m32001_MI = 
{
	"get_Current", NULL, &t4740_TI, &t1169_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32001_GM};
static MethodInfo* t4740_MIs[] =
{
	&m32001_MI,
	NULL
};
static TypeInfo* t4740_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4740_0_0_0;
extern Il2CppType t4740_1_0_0;
struct t4740;
extern Il2CppGenericClass t4740_GC;
TypeInfo t4740_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4740_MIs, t4740_PIs, NULL, NULL, NULL, NULL, NULL, &t4740_TI, t4740_ITIs, NULL, &EmptyCustomAttributesCache, &t4740_TI, &t4740_0_0_0, &t4740_1_0_0, NULL, &t4740_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3317.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3317_TI;
#include "t3317MD.h"

extern TypeInfo t1169_TI;
extern MethodInfo m18471_MI;
extern MethodInfo m24476_MI;
struct t20;
#define m24476(__this, p0, method) (t1169 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.MonoDocumentationNoteAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3317_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3317_TI, offsetof(t3317, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3317_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3317_TI, offsetof(t3317, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3317_FIs[] =
{
	&t3317_f0_FieldInfo,
	&t3317_f1_FieldInfo,
	NULL
};
extern MethodInfo m18468_MI;
static PropertyInfo t3317____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3317_TI, "System.Collections.IEnumerator.Current", &m18468_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3317____Current_PropertyInfo = 
{
	&t3317_TI, "Current", &m18471_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3317_PIs[] =
{
	&t3317____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3317____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3317_m18467_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18467_GM;
MethodInfo m18467_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3317_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3317_m18467_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18467_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18468_GM;
MethodInfo m18468_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3317_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18468_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18469_GM;
MethodInfo m18469_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3317_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18469_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18470_GM;
MethodInfo m18470_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3317_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18470_GM};
extern Il2CppType t1169_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18471_GM;
MethodInfo m18471_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3317_TI, &t1169_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18471_GM};
static MethodInfo* t3317_MIs[] =
{
	&m18467_MI,
	&m18468_MI,
	&m18469_MI,
	&m18470_MI,
	&m18471_MI,
	NULL
};
extern MethodInfo m18470_MI;
extern MethodInfo m18469_MI;
static MethodInfo* t3317_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18468_MI,
	&m18470_MI,
	&m18469_MI,
	&m18471_MI,
};
static TypeInfo* t3317_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4740_TI,
};
static Il2CppInterfaceOffsetPair t3317_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4740_TI, 7},
};
extern TypeInfo t1169_TI;
static Il2CppRGCTXData t3317_RGCTXData[3] = 
{
	&m18471_MI/* Method Usage */,
	&t1169_TI/* Class Usage */,
	&m24476_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3317_0_0_0;
extern Il2CppType t3317_1_0_0;
extern Il2CppGenericClass t3317_GC;
TypeInfo t3317_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3317_MIs, t3317_PIs, t3317_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3317_TI, t3317_ITIs, t3317_VT, &EmptyCustomAttributesCache, &t3317_TI, &t3317_0_0_0, &t3317_1_0_0, t3317_IOs, &t3317_GC, NULL, NULL, NULL, t3317_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3317)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6150_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.MonoDocumentationNoteAttribute>
extern MethodInfo m32002_MI;
static PropertyInfo t6150____Count_PropertyInfo = 
{
	&t6150_TI, "Count", &m32002_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32003_MI;
static PropertyInfo t6150____IsReadOnly_PropertyInfo = 
{
	&t6150_TI, "IsReadOnly", &m32003_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6150_PIs[] =
{
	&t6150____Count_PropertyInfo,
	&t6150____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32002_GM;
MethodInfo m32002_MI = 
{
	"get_Count", NULL, &t6150_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32002_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32003_GM;
MethodInfo m32003_MI = 
{
	"get_IsReadOnly", NULL, &t6150_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32003_GM};
extern Il2CppType t1169_0_0_0;
extern Il2CppType t1169_0_0_0;
static ParameterInfo t6150_m32004_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1169_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32004_GM;
MethodInfo m32004_MI = 
{
	"Add", NULL, &t6150_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6150_m32004_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32004_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32005_GM;
MethodInfo m32005_MI = 
{
	"Clear", NULL, &t6150_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32005_GM};
extern Il2CppType t1169_0_0_0;
static ParameterInfo t6150_m32006_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1169_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32006_GM;
MethodInfo m32006_MI = 
{
	"Contains", NULL, &t6150_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6150_m32006_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32006_GM};
extern Il2CppType t3593_0_0_0;
extern Il2CppType t3593_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6150_m32007_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3593_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32007_GM;
MethodInfo m32007_MI = 
{
	"CopyTo", NULL, &t6150_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6150_m32007_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32007_GM};
extern Il2CppType t1169_0_0_0;
static ParameterInfo t6150_m32008_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1169_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32008_GM;
MethodInfo m32008_MI = 
{
	"Remove", NULL, &t6150_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6150_m32008_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32008_GM};
static MethodInfo* t6150_MIs[] =
{
	&m32002_MI,
	&m32003_MI,
	&m32004_MI,
	&m32005_MI,
	&m32006_MI,
	&m32007_MI,
	&m32008_MI,
	NULL
};
extern TypeInfo t6152_TI;
static TypeInfo* t6150_ITIs[] = 
{
	&t603_TI,
	&t6152_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6150_0_0_0;
extern Il2CppType t6150_1_0_0;
struct t6150;
extern Il2CppGenericClass t6150_GC;
TypeInfo t6150_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6150_MIs, t6150_PIs, NULL, NULL, NULL, NULL, NULL, &t6150_TI, t6150_ITIs, NULL, &EmptyCustomAttributesCache, &t6150_TI, &t6150_0_0_0, &t6150_1_0_0, NULL, &t6150_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.MonoDocumentationNoteAttribute>
extern Il2CppType t4740_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32009_GM;
MethodInfo m32009_MI = 
{
	"GetEnumerator", NULL, &t6152_TI, &t4740_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32009_GM};
static MethodInfo* t6152_MIs[] =
{
	&m32009_MI,
	NULL
};
static TypeInfo* t6152_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6152_0_0_0;
extern Il2CppType t6152_1_0_0;
struct t6152;
extern Il2CppGenericClass t6152_GC;
TypeInfo t6152_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6152_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6152_TI, t6152_ITIs, NULL, &EmptyCustomAttributesCache, &t6152_TI, &t6152_0_0_0, &t6152_1_0_0, NULL, &t6152_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6151_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.MonoDocumentationNoteAttribute>
extern MethodInfo m32010_MI;
extern MethodInfo m32011_MI;
static PropertyInfo t6151____Item_PropertyInfo = 
{
	&t6151_TI, "Item", &m32010_MI, &m32011_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6151_PIs[] =
{
	&t6151____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1169_0_0_0;
static ParameterInfo t6151_m32012_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1169_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32012_GM;
MethodInfo m32012_MI = 
{
	"IndexOf", NULL, &t6151_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6151_m32012_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32012_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1169_0_0_0;
static ParameterInfo t6151_m32013_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1169_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32013_GM;
MethodInfo m32013_MI = 
{
	"Insert", NULL, &t6151_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6151_m32013_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32013_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6151_m32014_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32014_GM;
MethodInfo m32014_MI = 
{
	"RemoveAt", NULL, &t6151_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6151_m32014_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32014_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6151_m32010_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1169_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32010_GM;
MethodInfo m32010_MI = 
{
	"get_Item", NULL, &t6151_TI, &t1169_0_0_0, RuntimeInvoker_t29_t44, t6151_m32010_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32010_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1169_0_0_0;
static ParameterInfo t6151_m32011_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1169_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32011_GM;
MethodInfo m32011_MI = 
{
	"set_Item", NULL, &t6151_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6151_m32011_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32011_GM};
static MethodInfo* t6151_MIs[] =
{
	&m32012_MI,
	&m32013_MI,
	&m32014_MI,
	&m32010_MI,
	&m32011_MI,
	NULL
};
static TypeInfo* t6151_ITIs[] = 
{
	&t603_TI,
	&t6150_TI,
	&t6152_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6151_0_0_0;
extern Il2CppType t6151_1_0_0;
struct t6151;
extern Il2CppGenericClass t6151_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6151_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6151_MIs, t6151_PIs, NULL, NULL, NULL, NULL, NULL, &t6151_TI, t6151_ITIs, NULL, &t1908__CustomAttributeCache, &t6151_TI, &t6151_0_0_0, &t6151_1_0_0, NULL, &t6151_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4742_TI;

#include "t1173.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>
extern MethodInfo m32015_MI;
static PropertyInfo t4742____Current_PropertyInfo = 
{
	&t4742_TI, "Current", &m32015_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4742_PIs[] =
{
	&t4742____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1173_0_0_0;
extern void* RuntimeInvoker_t1173 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32015_GM;
MethodInfo m32015_MI = 
{
	"get_Current", NULL, &t4742_TI, &t1173_0_0_0, RuntimeInvoker_t1173, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32015_GM};
static MethodInfo* t4742_MIs[] =
{
	&m32015_MI,
	NULL
};
static TypeInfo* t4742_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4742_0_0_0;
extern Il2CppType t4742_1_0_0;
struct t4742;
extern Il2CppGenericClass t4742_GC;
TypeInfo t4742_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4742_MIs, t4742_PIs, NULL, NULL, NULL, NULL, NULL, &t4742_TI, t4742_ITIs, NULL, &EmptyCustomAttributesCache, &t4742_TI, &t4742_0_0_0, &t4742_1_0_0, NULL, &t4742_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3318.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3318_TI;
#include "t3318MD.h"

extern TypeInfo t1173_TI;
extern MethodInfo m18476_MI;
extern MethodInfo m24487_MI;
struct t20;
 t1173  m24487 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18472_MI;
 void m18472 (t3318 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18473_MI;
 t29 * m18473 (t3318 * __this, MethodInfo* method){
	{
		t1173  L_0 = m18476(__this, &m18476_MI);
		t1173  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1173_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18474_MI;
 void m18474 (t3318 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18475_MI;
 bool m18475 (t3318 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 t1173  m18476 (t3318 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		t1173  L_8 = m24487(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m24487_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>
extern Il2CppType t20_0_0_1;
FieldInfo t3318_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3318_TI, offsetof(t3318, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3318_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3318_TI, offsetof(t3318, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3318_FIs[] =
{
	&t3318_f0_FieldInfo,
	&t3318_f1_FieldInfo,
	NULL
};
static PropertyInfo t3318____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3318_TI, "System.Collections.IEnumerator.Current", &m18473_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3318____Current_PropertyInfo = 
{
	&t3318_TI, "Current", &m18476_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3318_PIs[] =
{
	&t3318____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3318____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3318_m18472_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18472_GM;
MethodInfo m18472_MI = 
{
	".ctor", (methodPointerType)&m18472, &t3318_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3318_m18472_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18472_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18473_GM;
MethodInfo m18473_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18473, &t3318_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18473_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18474_GM;
MethodInfo m18474_MI = 
{
	"Dispose", (methodPointerType)&m18474, &t3318_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18474_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18475_GM;
MethodInfo m18475_MI = 
{
	"MoveNext", (methodPointerType)&m18475, &t3318_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18475_GM};
extern Il2CppType t1173_0_0_0;
extern void* RuntimeInvoker_t1173 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18476_GM;
MethodInfo m18476_MI = 
{
	"get_Current", (methodPointerType)&m18476, &t3318_TI, &t1173_0_0_0, RuntimeInvoker_t1173, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18476_GM};
static MethodInfo* t3318_MIs[] =
{
	&m18472_MI,
	&m18473_MI,
	&m18474_MI,
	&m18475_MI,
	&m18476_MI,
	NULL
};
static MethodInfo* t3318_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18473_MI,
	&m18475_MI,
	&m18474_MI,
	&m18476_MI,
};
static TypeInfo* t3318_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4742_TI,
};
static Il2CppInterfaceOffsetPair t3318_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4742_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3318_0_0_0;
extern Il2CppType t3318_1_0_0;
extern Il2CppGenericClass t3318_GC;
TypeInfo t3318_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3318_MIs, t3318_PIs, t3318_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3318_TI, t3318_ITIs, t3318_VT, &EmptyCustomAttributesCache, &t3318_TI, &t3318_0_0_0, &t3318_1_0_0, t3318_IOs, &t3318_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3318)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6153_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>
extern MethodInfo m32016_MI;
static PropertyInfo t6153____Count_PropertyInfo = 
{
	&t6153_TI, "Count", &m32016_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32017_MI;
static PropertyInfo t6153____IsReadOnly_PropertyInfo = 
{
	&t6153_TI, "IsReadOnly", &m32017_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6153_PIs[] =
{
	&t6153____Count_PropertyInfo,
	&t6153____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32016_GM;
MethodInfo m32016_MI = 
{
	"get_Count", NULL, &t6153_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32016_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32017_GM;
MethodInfo m32017_MI = 
{
	"get_IsReadOnly", NULL, &t6153_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32017_GM};
extern Il2CppType t1173_0_0_0;
extern Il2CppType t1173_0_0_0;
static ParameterInfo t6153_m32018_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1173_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t1173 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32018_GM;
MethodInfo m32018_MI = 
{
	"Add", NULL, &t6153_TI, &t21_0_0_0, RuntimeInvoker_t21_t1173, t6153_m32018_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32018_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32019_GM;
MethodInfo m32019_MI = 
{
	"Clear", NULL, &t6153_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32019_GM};
extern Il2CppType t1173_0_0_0;
static ParameterInfo t6153_m32020_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1173_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t1173 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32020_GM;
MethodInfo m32020_MI = 
{
	"Contains", NULL, &t6153_TI, &t40_0_0_0, RuntimeInvoker_t40_t1173, t6153_m32020_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32020_GM};
extern Il2CppType t1175_0_0_0;
extern Il2CppType t1175_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6153_m32021_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1175_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32021_GM;
MethodInfo m32021_MI = 
{
	"CopyTo", NULL, &t6153_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6153_m32021_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32021_GM};
extern Il2CppType t1173_0_0_0;
static ParameterInfo t6153_m32022_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1173_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t1173 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32022_GM;
MethodInfo m32022_MI = 
{
	"Remove", NULL, &t6153_TI, &t40_0_0_0, RuntimeInvoker_t40_t1173, t6153_m32022_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32022_GM};
static MethodInfo* t6153_MIs[] =
{
	&m32016_MI,
	&m32017_MI,
	&m32018_MI,
	&m32019_MI,
	&m32020_MI,
	&m32021_MI,
	&m32022_MI,
	NULL
};
extern TypeInfo t6155_TI;
static TypeInfo* t6153_ITIs[] = 
{
	&t603_TI,
	&t6155_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6153_0_0_0;
extern Il2CppType t6153_1_0_0;
struct t6153;
extern Il2CppGenericClass t6153_GC;
TypeInfo t6153_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6153_MIs, t6153_PIs, NULL, NULL, NULL, NULL, NULL, &t6153_TI, t6153_ITIs, NULL, &EmptyCustomAttributesCache, &t6153_TI, &t6153_0_0_0, &t6153_1_0_0, NULL, &t6153_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>
extern Il2CppType t4742_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32023_GM;
MethodInfo m32023_MI = 
{
	"GetEnumerator", NULL, &t6155_TI, &t4742_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32023_GM};
static MethodInfo* t6155_MIs[] =
{
	&m32023_MI,
	NULL
};
static TypeInfo* t6155_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6155_0_0_0;
extern Il2CppType t6155_1_0_0;
struct t6155;
extern Il2CppGenericClass t6155_GC;
TypeInfo t6155_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6155_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6155_TI, t6155_ITIs, NULL, &EmptyCustomAttributesCache, &t6155_TI, &t6155_0_0_0, &t6155_1_0_0, NULL, &t6155_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6154_TI;



// Metadata Definition System.Collections.Generic.IList`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>
extern MethodInfo m32024_MI;
extern MethodInfo m32025_MI;
static PropertyInfo t6154____Item_PropertyInfo = 
{
	&t6154_TI, "Item", &m32024_MI, &m32025_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6154_PIs[] =
{
	&t6154____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1173_0_0_0;
static ParameterInfo t6154_m32026_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1173_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t1173 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32026_GM;
MethodInfo m32026_MI = 
{
	"IndexOf", NULL, &t6154_TI, &t44_0_0_0, RuntimeInvoker_t44_t1173, t6154_m32026_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32026_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1173_0_0_0;
static ParameterInfo t6154_m32027_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1173_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t1173 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32027_GM;
MethodInfo m32027_MI = 
{
	"Insert", NULL, &t6154_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t1173, t6154_m32027_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32027_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6154_m32028_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32028_GM;
MethodInfo m32028_MI = 
{
	"RemoveAt", NULL, &t6154_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6154_m32028_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32028_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6154_m32024_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1173_0_0_0;
extern void* RuntimeInvoker_t1173_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32024_GM;
MethodInfo m32024_MI = 
{
	"get_Item", NULL, &t6154_TI, &t1173_0_0_0, RuntimeInvoker_t1173_t44, t6154_m32024_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32024_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1173_0_0_0;
static ParameterInfo t6154_m32025_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1173_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t1173 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32025_GM;
MethodInfo m32025_MI = 
{
	"set_Item", NULL, &t6154_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t1173, t6154_m32025_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32025_GM};
static MethodInfo* t6154_MIs[] =
{
	&m32026_MI,
	&m32027_MI,
	&m32028_MI,
	&m32024_MI,
	&m32025_MI,
	NULL
};
static TypeInfo* t6154_ITIs[] = 
{
	&t603_TI,
	&t6153_TI,
	&t6155_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6154_0_0_0;
extern Il2CppType t6154_1_0_0;
struct t6154;
extern Il2CppGenericClass t6154_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6154_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6154_MIs, t6154_PIs, NULL, NULL, NULL, NULL, NULL, &t6154_TI, t6154_ITIs, NULL, &t1908__CustomAttributeCache, &t6154_TI, &t6154_0_0_0, &t6154_1_0_0, NULL, &t6154_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4744_TI;

#include "t1176.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Globalization.Unicode.TailoringInfo>
extern MethodInfo m32029_MI;
static PropertyInfo t4744____Current_PropertyInfo = 
{
	&t4744_TI, "Current", &m32029_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4744_PIs[] =
{
	&t4744____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1176_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32029_GM;
MethodInfo m32029_MI = 
{
	"get_Current", NULL, &t4744_TI, &t1176_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32029_GM};
static MethodInfo* t4744_MIs[] =
{
	&m32029_MI,
	NULL
};
static TypeInfo* t4744_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4744_0_0_0;
extern Il2CppType t4744_1_0_0;
struct t4744;
extern Il2CppGenericClass t4744_GC;
TypeInfo t4744_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4744_MIs, t4744_PIs, NULL, NULL, NULL, NULL, NULL, &t4744_TI, t4744_ITIs, NULL, &EmptyCustomAttributesCache, &t4744_TI, &t4744_0_0_0, &t4744_1_0_0, NULL, &t4744_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3319.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3319_TI;
#include "t3319MD.h"

extern TypeInfo t1176_TI;
extern MethodInfo m18481_MI;
extern MethodInfo m24498_MI;
struct t20;
#define m24498(__this, p0, method) (t1176 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.TailoringInfo>
extern Il2CppType t20_0_0_1;
FieldInfo t3319_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3319_TI, offsetof(t3319, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3319_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3319_TI, offsetof(t3319, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3319_FIs[] =
{
	&t3319_f0_FieldInfo,
	&t3319_f1_FieldInfo,
	NULL
};
extern MethodInfo m18478_MI;
static PropertyInfo t3319____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3319_TI, "System.Collections.IEnumerator.Current", &m18478_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3319____Current_PropertyInfo = 
{
	&t3319_TI, "Current", &m18481_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3319_PIs[] =
{
	&t3319____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3319____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3319_m18477_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18477_GM;
MethodInfo m18477_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3319_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3319_m18477_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18477_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18478_GM;
MethodInfo m18478_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3319_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18478_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18479_GM;
MethodInfo m18479_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3319_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18479_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18480_GM;
MethodInfo m18480_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3319_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18480_GM};
extern Il2CppType t1176_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18481_GM;
MethodInfo m18481_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3319_TI, &t1176_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18481_GM};
static MethodInfo* t3319_MIs[] =
{
	&m18477_MI,
	&m18478_MI,
	&m18479_MI,
	&m18480_MI,
	&m18481_MI,
	NULL
};
extern MethodInfo m18480_MI;
extern MethodInfo m18479_MI;
static MethodInfo* t3319_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18478_MI,
	&m18480_MI,
	&m18479_MI,
	&m18481_MI,
};
static TypeInfo* t3319_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4744_TI,
};
static Il2CppInterfaceOffsetPair t3319_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4744_TI, 7},
};
extern TypeInfo t1176_TI;
static Il2CppRGCTXData t3319_RGCTXData[3] = 
{
	&m18481_MI/* Method Usage */,
	&t1176_TI/* Class Usage */,
	&m24498_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3319_0_0_0;
extern Il2CppType t3319_1_0_0;
extern Il2CppGenericClass t3319_GC;
TypeInfo t3319_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3319_MIs, t3319_PIs, t3319_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3319_TI, t3319_ITIs, t3319_VT, &EmptyCustomAttributesCache, &t3319_TI, &t3319_0_0_0, &t3319_1_0_0, t3319_IOs, &t3319_GC, NULL, NULL, NULL, t3319_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3319)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6156_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Globalization.Unicode.TailoringInfo>
extern MethodInfo m32030_MI;
static PropertyInfo t6156____Count_PropertyInfo = 
{
	&t6156_TI, "Count", &m32030_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32031_MI;
static PropertyInfo t6156____IsReadOnly_PropertyInfo = 
{
	&t6156_TI, "IsReadOnly", &m32031_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6156_PIs[] =
{
	&t6156____Count_PropertyInfo,
	&t6156____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32030_GM;
MethodInfo m32030_MI = 
{
	"get_Count", NULL, &t6156_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32030_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32031_GM;
MethodInfo m32031_MI = 
{
	"get_IsReadOnly", NULL, &t6156_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32031_GM};
extern Il2CppType t1176_0_0_0;
extern Il2CppType t1176_0_0_0;
static ParameterInfo t6156_m32032_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1176_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32032_GM;
MethodInfo m32032_MI = 
{
	"Add", NULL, &t6156_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6156_m32032_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32032_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32033_GM;
MethodInfo m32033_MI = 
{
	"Clear", NULL, &t6156_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32033_GM};
extern Il2CppType t1176_0_0_0;
static ParameterInfo t6156_m32034_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1176_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32034_GM;
MethodInfo m32034_MI = 
{
	"Contains", NULL, &t6156_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6156_m32034_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32034_GM};
extern Il2CppType t1182_0_0_0;
extern Il2CppType t1182_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6156_m32035_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1182_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32035_GM;
MethodInfo m32035_MI = 
{
	"CopyTo", NULL, &t6156_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6156_m32035_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32035_GM};
extern Il2CppType t1176_0_0_0;
static ParameterInfo t6156_m32036_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1176_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32036_GM;
MethodInfo m32036_MI = 
{
	"Remove", NULL, &t6156_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6156_m32036_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32036_GM};
static MethodInfo* t6156_MIs[] =
{
	&m32030_MI,
	&m32031_MI,
	&m32032_MI,
	&m32033_MI,
	&m32034_MI,
	&m32035_MI,
	&m32036_MI,
	NULL
};
extern TypeInfo t6158_TI;
static TypeInfo* t6156_ITIs[] = 
{
	&t603_TI,
	&t6158_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6156_0_0_0;
extern Il2CppType t6156_1_0_0;
struct t6156;
extern Il2CppGenericClass t6156_GC;
TypeInfo t6156_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6156_MIs, t6156_PIs, NULL, NULL, NULL, NULL, NULL, &t6156_TI, t6156_ITIs, NULL, &EmptyCustomAttributesCache, &t6156_TI, &t6156_0_0_0, &t6156_1_0_0, NULL, &t6156_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Globalization.Unicode.TailoringInfo>
extern Il2CppType t4744_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32037_GM;
MethodInfo m32037_MI = 
{
	"GetEnumerator", NULL, &t6158_TI, &t4744_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32037_GM};
static MethodInfo* t6158_MIs[] =
{
	&m32037_MI,
	NULL
};
static TypeInfo* t6158_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6158_0_0_0;
extern Il2CppType t6158_1_0_0;
struct t6158;
extern Il2CppGenericClass t6158_GC;
TypeInfo t6158_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6158_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6158_TI, t6158_ITIs, NULL, &EmptyCustomAttributesCache, &t6158_TI, &t6158_0_0_0, &t6158_1_0_0, NULL, &t6158_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6157_TI;



// Metadata Definition System.Collections.Generic.IList`1<Mono.Globalization.Unicode.TailoringInfo>
extern MethodInfo m32038_MI;
extern MethodInfo m32039_MI;
static PropertyInfo t6157____Item_PropertyInfo = 
{
	&t6157_TI, "Item", &m32038_MI, &m32039_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6157_PIs[] =
{
	&t6157____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1176_0_0_0;
static ParameterInfo t6157_m32040_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1176_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32040_GM;
MethodInfo m32040_MI = 
{
	"IndexOf", NULL, &t6157_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6157_m32040_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32040_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1176_0_0_0;
static ParameterInfo t6157_m32041_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1176_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32041_GM;
MethodInfo m32041_MI = 
{
	"Insert", NULL, &t6157_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6157_m32041_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32041_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6157_m32042_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32042_GM;
MethodInfo m32042_MI = 
{
	"RemoveAt", NULL, &t6157_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6157_m32042_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32042_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6157_m32038_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1176_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32038_GM;
MethodInfo m32038_MI = 
{
	"get_Item", NULL, &t6157_TI, &t1176_0_0_0, RuntimeInvoker_t29_t44, t6157_m32038_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32038_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1176_0_0_0;
static ParameterInfo t6157_m32039_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1176_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32039_GM;
MethodInfo m32039_MI = 
{
	"set_Item", NULL, &t6157_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6157_m32039_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32039_GM};
static MethodInfo* t6157_MIs[] =
{
	&m32040_MI,
	&m32041_MI,
	&m32042_MI,
	&m32038_MI,
	&m32039_MI,
	NULL
};
static TypeInfo* t6157_ITIs[] = 
{
	&t603_TI,
	&t6156_TI,
	&t6158_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6157_0_0_0;
extern Il2CppType t6157_1_0_0;
struct t6157;
extern Il2CppGenericClass t6157_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6157_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6157_MIs, t6157_PIs, NULL, NULL, NULL, NULL, NULL, &t6157_TI, t6157_ITIs, NULL, &t1908__CustomAttributeCache, &t6157_TI, &t6157_0_0_0, &t6157_1_0_0, NULL, &t6157_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4745_TI;

#include "t1177.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Globalization.Unicode.Contraction>
extern MethodInfo m32043_MI;
static PropertyInfo t4745____Current_PropertyInfo = 
{
	&t4745_TI, "Current", &m32043_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4745_PIs[] =
{
	&t4745____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1177_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32043_GM;
MethodInfo m32043_MI = 
{
	"get_Current", NULL, &t4745_TI, &t1177_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32043_GM};
static MethodInfo* t4745_MIs[] =
{
	&m32043_MI,
	NULL
};
static TypeInfo* t4745_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4745_0_0_0;
extern Il2CppType t4745_1_0_0;
struct t4745;
extern Il2CppGenericClass t4745_GC;
TypeInfo t4745_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4745_MIs, t4745_PIs, NULL, NULL, NULL, NULL, NULL, &t4745_TI, t4745_ITIs, NULL, &EmptyCustomAttributesCache, &t4745_TI, &t4745_0_0_0, &t4745_1_0_0, NULL, &t4745_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3320.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3320_TI;
#include "t3320MD.h"

extern TypeInfo t1177_TI;
extern MethodInfo m18486_MI;
extern MethodInfo m24509_MI;
struct t20;
#define m24509(__this, p0, method) (t1177 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.Contraction>
extern Il2CppType t20_0_0_1;
FieldInfo t3320_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3320_TI, offsetof(t3320, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3320_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3320_TI, offsetof(t3320, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3320_FIs[] =
{
	&t3320_f0_FieldInfo,
	&t3320_f1_FieldInfo,
	NULL
};
extern MethodInfo m18483_MI;
static PropertyInfo t3320____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3320_TI, "System.Collections.IEnumerator.Current", &m18483_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3320____Current_PropertyInfo = 
{
	&t3320_TI, "Current", &m18486_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3320_PIs[] =
{
	&t3320____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3320____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3320_m18482_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18482_GM;
MethodInfo m18482_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3320_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3320_m18482_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18482_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18483_GM;
MethodInfo m18483_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3320_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18483_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18484_GM;
MethodInfo m18484_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3320_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18484_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18485_GM;
MethodInfo m18485_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3320_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18485_GM};
extern Il2CppType t1177_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18486_GM;
MethodInfo m18486_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3320_TI, &t1177_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18486_GM};
static MethodInfo* t3320_MIs[] =
{
	&m18482_MI,
	&m18483_MI,
	&m18484_MI,
	&m18485_MI,
	&m18486_MI,
	NULL
};
extern MethodInfo m18485_MI;
extern MethodInfo m18484_MI;
static MethodInfo* t3320_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18483_MI,
	&m18485_MI,
	&m18484_MI,
	&m18486_MI,
};
static TypeInfo* t3320_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4745_TI,
};
static Il2CppInterfaceOffsetPair t3320_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4745_TI, 7},
};
extern TypeInfo t1177_TI;
static Il2CppRGCTXData t3320_RGCTXData[3] = 
{
	&m18486_MI/* Method Usage */,
	&t1177_TI/* Class Usage */,
	&m24509_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3320_0_0_0;
extern Il2CppType t3320_1_0_0;
extern Il2CppGenericClass t3320_GC;
TypeInfo t3320_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3320_MIs, t3320_PIs, t3320_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3320_TI, t3320_ITIs, t3320_VT, &EmptyCustomAttributesCache, &t3320_TI, &t3320_0_0_0, &t3320_1_0_0, t3320_IOs, &t3320_GC, NULL, NULL, NULL, t3320_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3320)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6159_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Globalization.Unicode.Contraction>
extern MethodInfo m32044_MI;
static PropertyInfo t6159____Count_PropertyInfo = 
{
	&t6159_TI, "Count", &m32044_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32045_MI;
static PropertyInfo t6159____IsReadOnly_PropertyInfo = 
{
	&t6159_TI, "IsReadOnly", &m32045_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6159_PIs[] =
{
	&t6159____Count_PropertyInfo,
	&t6159____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32044_GM;
MethodInfo m32044_MI = 
{
	"get_Count", NULL, &t6159_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32044_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32045_GM;
MethodInfo m32045_MI = 
{
	"get_IsReadOnly", NULL, &t6159_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32045_GM};
extern Il2CppType t1177_0_0_0;
extern Il2CppType t1177_0_0_0;
static ParameterInfo t6159_m32046_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1177_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32046_GM;
MethodInfo m32046_MI = 
{
	"Add", NULL, &t6159_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6159_m32046_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32046_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32047_GM;
MethodInfo m32047_MI = 
{
	"Clear", NULL, &t6159_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32047_GM};
extern Il2CppType t1177_0_0_0;
static ParameterInfo t6159_m32048_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1177_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32048_GM;
MethodInfo m32048_MI = 
{
	"Contains", NULL, &t6159_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6159_m32048_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32048_GM};
extern Il2CppType t1183_0_0_0;
extern Il2CppType t1183_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6159_m32049_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1183_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32049_GM;
MethodInfo m32049_MI = 
{
	"CopyTo", NULL, &t6159_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6159_m32049_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32049_GM};
extern Il2CppType t1177_0_0_0;
static ParameterInfo t6159_m32050_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1177_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32050_GM;
MethodInfo m32050_MI = 
{
	"Remove", NULL, &t6159_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6159_m32050_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32050_GM};
static MethodInfo* t6159_MIs[] =
{
	&m32044_MI,
	&m32045_MI,
	&m32046_MI,
	&m32047_MI,
	&m32048_MI,
	&m32049_MI,
	&m32050_MI,
	NULL
};
extern TypeInfo t6161_TI;
static TypeInfo* t6159_ITIs[] = 
{
	&t603_TI,
	&t6161_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6159_0_0_0;
extern Il2CppType t6159_1_0_0;
struct t6159;
extern Il2CppGenericClass t6159_GC;
TypeInfo t6159_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6159_MIs, t6159_PIs, NULL, NULL, NULL, NULL, NULL, &t6159_TI, t6159_ITIs, NULL, &EmptyCustomAttributesCache, &t6159_TI, &t6159_0_0_0, &t6159_1_0_0, NULL, &t6159_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Globalization.Unicode.Contraction>
extern Il2CppType t4745_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32051_GM;
MethodInfo m32051_MI = 
{
	"GetEnumerator", NULL, &t6161_TI, &t4745_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32051_GM};
static MethodInfo* t6161_MIs[] =
{
	&m32051_MI,
	NULL
};
static TypeInfo* t6161_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6161_0_0_0;
extern Il2CppType t6161_1_0_0;
struct t6161;
extern Il2CppGenericClass t6161_GC;
TypeInfo t6161_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6161_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6161_TI, t6161_ITIs, NULL, &EmptyCustomAttributesCache, &t6161_TI, &t6161_0_0_0, &t6161_1_0_0, NULL, &t6161_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6160_TI;



// Metadata Definition System.Collections.Generic.IList`1<Mono.Globalization.Unicode.Contraction>
extern MethodInfo m32052_MI;
extern MethodInfo m32053_MI;
static PropertyInfo t6160____Item_PropertyInfo = 
{
	&t6160_TI, "Item", &m32052_MI, &m32053_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6160_PIs[] =
{
	&t6160____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1177_0_0_0;
static ParameterInfo t6160_m32054_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1177_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32054_GM;
MethodInfo m32054_MI = 
{
	"IndexOf", NULL, &t6160_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6160_m32054_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32054_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1177_0_0_0;
static ParameterInfo t6160_m32055_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1177_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32055_GM;
MethodInfo m32055_MI = 
{
	"Insert", NULL, &t6160_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6160_m32055_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32055_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6160_m32056_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32056_GM;
MethodInfo m32056_MI = 
{
	"RemoveAt", NULL, &t6160_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6160_m32056_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32056_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6160_m32052_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1177_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32052_GM;
MethodInfo m32052_MI = 
{
	"get_Item", NULL, &t6160_TI, &t1177_0_0_0, RuntimeInvoker_t29_t44, t6160_m32052_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32052_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1177_0_0_0;
static ParameterInfo t6160_m32053_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1177_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32053_GM;
MethodInfo m32053_MI = 
{
	"set_Item", NULL, &t6160_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6160_m32053_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32053_GM};
static MethodInfo* t6160_MIs[] =
{
	&m32054_MI,
	&m32055_MI,
	&m32056_MI,
	&m32052_MI,
	&m32053_MI,
	NULL
};
static TypeInfo* t6160_ITIs[] = 
{
	&t603_TI,
	&t6159_TI,
	&t6161_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6160_0_0_0;
extern Il2CppType t6160_1_0_0;
struct t6160;
extern Il2CppGenericClass t6160_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6160_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6160_MIs, t6160_PIs, NULL, NULL, NULL, NULL, NULL, &t6160_TI, t6160_ITIs, NULL, &t1908__CustomAttributeCache, &t6160_TI, &t6160_0_0_0, &t6160_1_0_0, NULL, &t6160_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4747_TI;

#include "t1179.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Globalization.Unicode.Level2Map>
extern MethodInfo m32057_MI;
static PropertyInfo t4747____Current_PropertyInfo = 
{
	&t4747_TI, "Current", &m32057_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4747_PIs[] =
{
	&t4747____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1179_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32057_GM;
MethodInfo m32057_MI = 
{
	"get_Current", NULL, &t4747_TI, &t1179_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32057_GM};
static MethodInfo* t4747_MIs[] =
{
	&m32057_MI,
	NULL
};
static TypeInfo* t4747_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4747_0_0_0;
extern Il2CppType t4747_1_0_0;
struct t4747;
extern Il2CppGenericClass t4747_GC;
TypeInfo t4747_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4747_MIs, t4747_PIs, NULL, NULL, NULL, NULL, NULL, &t4747_TI, t4747_ITIs, NULL, &EmptyCustomAttributesCache, &t4747_TI, &t4747_0_0_0, &t4747_1_0_0, NULL, &t4747_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3321.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3321_TI;
#include "t3321MD.h"

extern TypeInfo t1179_TI;
extern MethodInfo m18491_MI;
extern MethodInfo m24520_MI;
struct t20;
#define m24520(__this, p0, method) (t1179 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.Level2Map>
extern Il2CppType t20_0_0_1;
FieldInfo t3321_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3321_TI, offsetof(t3321, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3321_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3321_TI, offsetof(t3321, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3321_FIs[] =
{
	&t3321_f0_FieldInfo,
	&t3321_f1_FieldInfo,
	NULL
};
extern MethodInfo m18488_MI;
static PropertyInfo t3321____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3321_TI, "System.Collections.IEnumerator.Current", &m18488_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3321____Current_PropertyInfo = 
{
	&t3321_TI, "Current", &m18491_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3321_PIs[] =
{
	&t3321____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3321____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3321_m18487_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18487_GM;
MethodInfo m18487_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3321_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3321_m18487_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18487_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18488_GM;
MethodInfo m18488_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3321_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18488_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18489_GM;
MethodInfo m18489_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3321_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18489_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18490_GM;
MethodInfo m18490_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3321_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18490_GM};
extern Il2CppType t1179_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18491_GM;
MethodInfo m18491_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3321_TI, &t1179_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18491_GM};
static MethodInfo* t3321_MIs[] =
{
	&m18487_MI,
	&m18488_MI,
	&m18489_MI,
	&m18490_MI,
	&m18491_MI,
	NULL
};
extern MethodInfo m18490_MI;
extern MethodInfo m18489_MI;
static MethodInfo* t3321_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18488_MI,
	&m18490_MI,
	&m18489_MI,
	&m18491_MI,
};
static TypeInfo* t3321_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4747_TI,
};
static Il2CppInterfaceOffsetPair t3321_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4747_TI, 7},
};
extern TypeInfo t1179_TI;
static Il2CppRGCTXData t3321_RGCTXData[3] = 
{
	&m18491_MI/* Method Usage */,
	&t1179_TI/* Class Usage */,
	&m24520_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3321_0_0_0;
extern Il2CppType t3321_1_0_0;
extern Il2CppGenericClass t3321_GC;
TypeInfo t3321_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3321_MIs, t3321_PIs, t3321_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3321_TI, t3321_ITIs, t3321_VT, &EmptyCustomAttributesCache, &t3321_TI, &t3321_0_0_0, &t3321_1_0_0, t3321_IOs, &t3321_GC, NULL, NULL, NULL, t3321_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3321)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6162_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Globalization.Unicode.Level2Map>
extern MethodInfo m32058_MI;
static PropertyInfo t6162____Count_PropertyInfo = 
{
	&t6162_TI, "Count", &m32058_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32059_MI;
static PropertyInfo t6162____IsReadOnly_PropertyInfo = 
{
	&t6162_TI, "IsReadOnly", &m32059_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6162_PIs[] =
{
	&t6162____Count_PropertyInfo,
	&t6162____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32058_GM;
MethodInfo m32058_MI = 
{
	"get_Count", NULL, &t6162_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32058_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32059_GM;
MethodInfo m32059_MI = 
{
	"get_IsReadOnly", NULL, &t6162_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32059_GM};
extern Il2CppType t1179_0_0_0;
extern Il2CppType t1179_0_0_0;
static ParameterInfo t6162_m32060_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1179_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32060_GM;
MethodInfo m32060_MI = 
{
	"Add", NULL, &t6162_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6162_m32060_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32060_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32061_GM;
MethodInfo m32061_MI = 
{
	"Clear", NULL, &t6162_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32061_GM};
extern Il2CppType t1179_0_0_0;
static ParameterInfo t6162_m32062_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1179_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32062_GM;
MethodInfo m32062_MI = 
{
	"Contains", NULL, &t6162_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6162_m32062_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32062_GM};
extern Il2CppType t1184_0_0_0;
extern Il2CppType t1184_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6162_m32063_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1184_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32063_GM;
MethodInfo m32063_MI = 
{
	"CopyTo", NULL, &t6162_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6162_m32063_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32063_GM};
extern Il2CppType t1179_0_0_0;
static ParameterInfo t6162_m32064_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1179_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32064_GM;
MethodInfo m32064_MI = 
{
	"Remove", NULL, &t6162_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6162_m32064_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32064_GM};
static MethodInfo* t6162_MIs[] =
{
	&m32058_MI,
	&m32059_MI,
	&m32060_MI,
	&m32061_MI,
	&m32062_MI,
	&m32063_MI,
	&m32064_MI,
	NULL
};
extern TypeInfo t6164_TI;
static TypeInfo* t6162_ITIs[] = 
{
	&t603_TI,
	&t6164_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6162_0_0_0;
extern Il2CppType t6162_1_0_0;
struct t6162;
extern Il2CppGenericClass t6162_GC;
TypeInfo t6162_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6162_MIs, t6162_PIs, NULL, NULL, NULL, NULL, NULL, &t6162_TI, t6162_ITIs, NULL, &EmptyCustomAttributesCache, &t6162_TI, &t6162_0_0_0, &t6162_1_0_0, NULL, &t6162_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Globalization.Unicode.Level2Map>
extern Il2CppType t4747_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32065_GM;
MethodInfo m32065_MI = 
{
	"GetEnumerator", NULL, &t6164_TI, &t4747_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32065_GM};
static MethodInfo* t6164_MIs[] =
{
	&m32065_MI,
	NULL
};
static TypeInfo* t6164_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6164_0_0_0;
extern Il2CppType t6164_1_0_0;
struct t6164;
extern Il2CppGenericClass t6164_GC;
TypeInfo t6164_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6164_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6164_TI, t6164_ITIs, NULL, &EmptyCustomAttributesCache, &t6164_TI, &t6164_0_0_0, &t6164_1_0_0, NULL, &t6164_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6163_TI;



// Metadata Definition System.Collections.Generic.IList`1<Mono.Globalization.Unicode.Level2Map>
extern MethodInfo m32066_MI;
extern MethodInfo m32067_MI;
static PropertyInfo t6163____Item_PropertyInfo = 
{
	&t6163_TI, "Item", &m32066_MI, &m32067_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6163_PIs[] =
{
	&t6163____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1179_0_0_0;
static ParameterInfo t6163_m32068_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1179_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32068_GM;
MethodInfo m32068_MI = 
{
	"IndexOf", NULL, &t6163_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6163_m32068_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32068_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1179_0_0_0;
static ParameterInfo t6163_m32069_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1179_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32069_GM;
MethodInfo m32069_MI = 
{
	"Insert", NULL, &t6163_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6163_m32069_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32069_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6163_m32070_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32070_GM;
MethodInfo m32070_MI = 
{
	"RemoveAt", NULL, &t6163_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6163_m32070_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32070_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6163_m32066_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1179_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32066_GM;
MethodInfo m32066_MI = 
{
	"get_Item", NULL, &t6163_TI, &t1179_0_0_0, RuntimeInvoker_t29_t44, t6163_m32066_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32066_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1179_0_0_0;
static ParameterInfo t6163_m32067_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1179_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32067_GM;
MethodInfo m32067_MI = 
{
	"set_Item", NULL, &t6163_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6163_m32067_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32067_GM};
static MethodInfo* t6163_MIs[] =
{
	&m32068_MI,
	&m32069_MI,
	&m32070_MI,
	&m32066_MI,
	&m32067_MI,
	NULL
};
static TypeInfo* t6163_ITIs[] = 
{
	&t603_TI,
	&t6162_TI,
	&t6164_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6163_0_0_0;
extern Il2CppType t6163_1_0_0;
struct t6163;
extern Il2CppGenericClass t6163_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6163_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6163_MIs, t6163_PIs, NULL, NULL, NULL, NULL, NULL, &t6163_TI, t6163_ITIs, NULL, &t1908__CustomAttributeCache, &t6163_TI, &t6163_0_0_0, &t6163_1_0_0, NULL, &t6163_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4749_TI;

#include "t1189.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Globalization.Unicode.SimpleCollator/ExtenderType>
extern MethodInfo m32071_MI;
static PropertyInfo t4749____Current_PropertyInfo = 
{
	&t4749_TI, "Current", &m32071_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4749_PIs[] =
{
	&t4749____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1189_0_0_0;
extern void* RuntimeInvoker_t1189 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32071_GM;
MethodInfo m32071_MI = 
{
	"get_Current", NULL, &t4749_TI, &t1189_0_0_0, RuntimeInvoker_t1189, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32071_GM};
static MethodInfo* t4749_MIs[] =
{
	&m32071_MI,
	NULL
};
static TypeInfo* t4749_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4749_0_0_0;
extern Il2CppType t4749_1_0_0;
struct t4749;
extern Il2CppGenericClass t4749_GC;
TypeInfo t4749_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4749_MIs, t4749_PIs, NULL, NULL, NULL, NULL, NULL, &t4749_TI, t4749_ITIs, NULL, &EmptyCustomAttributesCache, &t4749_TI, &t4749_0_0_0, &t4749_1_0_0, NULL, &t4749_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3322.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3322_TI;
#include "t3322MD.h"

extern TypeInfo t1189_TI;
extern MethodInfo m18496_MI;
extern MethodInfo m24531_MI;
struct t20;
 int32_t m24531 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18492_MI;
 void m18492 (t3322 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18493_MI;
 t29 * m18493 (t3322 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18496(__this, &m18496_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1189_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18494_MI;
 void m18494 (t3322 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18495_MI;
 bool m18495 (t3322 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18496 (t3322 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m24531(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m24531_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.SimpleCollator/ExtenderType>
extern Il2CppType t20_0_0_1;
FieldInfo t3322_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3322_TI, offsetof(t3322, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3322_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3322_TI, offsetof(t3322, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3322_FIs[] =
{
	&t3322_f0_FieldInfo,
	&t3322_f1_FieldInfo,
	NULL
};
static PropertyInfo t3322____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3322_TI, "System.Collections.IEnumerator.Current", &m18493_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3322____Current_PropertyInfo = 
{
	&t3322_TI, "Current", &m18496_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3322_PIs[] =
{
	&t3322____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3322____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3322_m18492_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18492_GM;
MethodInfo m18492_MI = 
{
	".ctor", (methodPointerType)&m18492, &t3322_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3322_m18492_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18492_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18493_GM;
MethodInfo m18493_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18493, &t3322_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18493_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18494_GM;
MethodInfo m18494_MI = 
{
	"Dispose", (methodPointerType)&m18494, &t3322_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18494_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18495_GM;
MethodInfo m18495_MI = 
{
	"MoveNext", (methodPointerType)&m18495, &t3322_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18495_GM};
extern Il2CppType t1189_0_0_0;
extern void* RuntimeInvoker_t1189 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18496_GM;
MethodInfo m18496_MI = 
{
	"get_Current", (methodPointerType)&m18496, &t3322_TI, &t1189_0_0_0, RuntimeInvoker_t1189, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18496_GM};
static MethodInfo* t3322_MIs[] =
{
	&m18492_MI,
	&m18493_MI,
	&m18494_MI,
	&m18495_MI,
	&m18496_MI,
	NULL
};
static MethodInfo* t3322_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18493_MI,
	&m18495_MI,
	&m18494_MI,
	&m18496_MI,
};
static TypeInfo* t3322_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4749_TI,
};
static Il2CppInterfaceOffsetPair t3322_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4749_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3322_0_0_0;
extern Il2CppType t3322_1_0_0;
extern Il2CppGenericClass t3322_GC;
TypeInfo t3322_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3322_MIs, t3322_PIs, t3322_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3322_TI, t3322_ITIs, t3322_VT, &EmptyCustomAttributesCache, &t3322_TI, &t3322_0_0_0, &t3322_1_0_0, t3322_IOs, &t3322_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3322)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6165_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Globalization.Unicode.SimpleCollator/ExtenderType>
extern MethodInfo m32072_MI;
static PropertyInfo t6165____Count_PropertyInfo = 
{
	&t6165_TI, "Count", &m32072_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32073_MI;
static PropertyInfo t6165____IsReadOnly_PropertyInfo = 
{
	&t6165_TI, "IsReadOnly", &m32073_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6165_PIs[] =
{
	&t6165____Count_PropertyInfo,
	&t6165____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32072_GM;
MethodInfo m32072_MI = 
{
	"get_Count", NULL, &t6165_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32072_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32073_GM;
MethodInfo m32073_MI = 
{
	"get_IsReadOnly", NULL, &t6165_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32073_GM};
extern Il2CppType t1189_0_0_0;
extern Il2CppType t1189_0_0_0;
static ParameterInfo t6165_m32074_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1189_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32074_GM;
MethodInfo m32074_MI = 
{
	"Add", NULL, &t6165_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6165_m32074_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32074_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32075_GM;
MethodInfo m32075_MI = 
{
	"Clear", NULL, &t6165_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32075_GM};
extern Il2CppType t1189_0_0_0;
static ParameterInfo t6165_m32076_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1189_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32076_GM;
MethodInfo m32076_MI = 
{
	"Contains", NULL, &t6165_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6165_m32076_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32076_GM};
extern Il2CppType t3594_0_0_0;
extern Il2CppType t3594_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6165_m32077_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3594_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32077_GM;
MethodInfo m32077_MI = 
{
	"CopyTo", NULL, &t6165_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6165_m32077_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32077_GM};
extern Il2CppType t1189_0_0_0;
static ParameterInfo t6165_m32078_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1189_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32078_GM;
MethodInfo m32078_MI = 
{
	"Remove", NULL, &t6165_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6165_m32078_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32078_GM};
static MethodInfo* t6165_MIs[] =
{
	&m32072_MI,
	&m32073_MI,
	&m32074_MI,
	&m32075_MI,
	&m32076_MI,
	&m32077_MI,
	&m32078_MI,
	NULL
};
extern TypeInfo t6167_TI;
static TypeInfo* t6165_ITIs[] = 
{
	&t603_TI,
	&t6167_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6165_0_0_0;
extern Il2CppType t6165_1_0_0;
struct t6165;
extern Il2CppGenericClass t6165_GC;
TypeInfo t6165_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6165_MIs, t6165_PIs, NULL, NULL, NULL, NULL, NULL, &t6165_TI, t6165_ITIs, NULL, &EmptyCustomAttributesCache, &t6165_TI, &t6165_0_0_0, &t6165_1_0_0, NULL, &t6165_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Globalization.Unicode.SimpleCollator/ExtenderType>
extern Il2CppType t4749_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32079_GM;
MethodInfo m32079_MI = 
{
	"GetEnumerator", NULL, &t6167_TI, &t4749_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32079_GM};
static MethodInfo* t6167_MIs[] =
{
	&m32079_MI,
	NULL
};
static TypeInfo* t6167_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6167_0_0_0;
extern Il2CppType t6167_1_0_0;
struct t6167;
extern Il2CppGenericClass t6167_GC;
TypeInfo t6167_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6167_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6167_TI, t6167_ITIs, NULL, &EmptyCustomAttributesCache, &t6167_TI, &t6167_0_0_0, &t6167_1_0_0, NULL, &t6167_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
