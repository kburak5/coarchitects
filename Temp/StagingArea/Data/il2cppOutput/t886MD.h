﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t886;
struct t7;
struct t878;
struct t879;

 void m3823 (t886 * __this, t7* p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3824 (t29 * __this, t7* p0, t29 * p1, bool p2, bool p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3825 (t886 * __this, t29 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3826 (t886 * __this, int32_t* p0, int32_t* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t879 * m3827 (t886 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3828 (t886 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
