﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t474;
struct t466;
struct t263;
struct t473;
struct t7;

 void m2147 (t474 * __this, float p0, float p1, float p2, float p3, t466 * p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2148 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t466 * m2149 (t474 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2150 (t474 * __this, t466 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t263 * m2151 (t474 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2152 (t474 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2153 (t474 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2154 (t474 * __this, float p0, float p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2155 (t474 * __this, float p0, float p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2156 (t474 * __this, t466 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2157 (t474 * __this, t473* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2158 (t474 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
