﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2442;
struct t29;
struct t365;
struct t2424;
struct t20;
struct t136;
struct t841;
#include "t2448.h"

 void m12785 (t2442 * __this, t365 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12786 (t2442 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12787 (t2442 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12788 (t2442 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12789 (t2442 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m12790 (t2442 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12791 (t2442 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12792 (t2442 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12793 (t2442 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12794 (t2442 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12795 (t2442 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12796 (t2442 * __this, t841* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2448  m12797 (t2442 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m12798 (t2442 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
