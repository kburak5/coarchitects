﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1631;
struct t29;
struct t7;
#include "t816.h"
#include "t1631.h"

 void m10260 (t1631 * __this, t816  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m10261 (t1631 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t816  m10262 (t1631 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19401 (t1631 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19402 (t1631 * __this, t1631  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m19403 (t1631 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m19404 (t1631 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
