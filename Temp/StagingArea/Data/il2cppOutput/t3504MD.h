﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3504;
struct t29;
#include "t816.h"

 void m19463 (t3504 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m19464 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m19465 (t3504 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t3504 * m19466 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
