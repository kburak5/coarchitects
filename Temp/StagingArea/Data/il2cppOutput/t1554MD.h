﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1554;
struct t781;

 void m8418 (t1554 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8419 (t1554 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8420 (t1554 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8421 (t1554 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m8422 (t1554 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8423 (t1554 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8424 (t1554 * __this, t781* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8425 (t1554 * __this, uint64_t p0, t781* p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8426 (t1554 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8427 (t1554 * __this, uint64_t p0, uint64_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8428 (t1554 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m8429 (t1554 * __this, uint64_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m8430 (t1554 * __this, uint64_t p0, uint64_t p1, uint64_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m8431 (t1554 * __this, uint64_t p0, uint64_t p1, uint64_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m8432 (t1554 * __this, uint64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m8433 (t1554 * __this, uint64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m8434 (t1554 * __this, uint64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m8435 (t1554 * __this, uint64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
