﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t568;
struct t29;
struct t565;
struct t3133;
struct t20;
struct t136;
struct t3134;
struct t3135;
struct t3136;
struct t3132;
struct t3137;
struct t3138;
#include "t661.h"

#include "t294MD.h"
#define m2983(__this, method) (void)m10537_gshared((t294 *)__this, method)
#define m17292(__this, p0, method) (void)m10539_gshared((t294 *)__this, (int32_t)p0, method)
#define m17293(__this, method) (void)m10541_gshared((t29 *)__this, method)
#define m17294(__this, method) (t29*)m10543_gshared((t294 *)__this, method)
#define m17295(__this, p0, p1, method) (void)m10545_gshared((t294 *)__this, (t20 *)p0, (int32_t)p1, method)
#define m17296(__this, method) (t29 *)m10547_gshared((t294 *)__this, method)
#define m17297(__this, p0, method) (int32_t)m10549_gshared((t294 *)__this, (t29 *)p0, method)
#define m17298(__this, p0, method) (bool)m10551_gshared((t294 *)__this, (t29 *)p0, method)
#define m17299(__this, p0, method) (int32_t)m10553_gshared((t294 *)__this, (t29 *)p0, method)
#define m17300(__this, p0, p1, method) (void)m10555_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m17301(__this, p0, method) (void)m10557_gshared((t294 *)__this, (t29 *)p0, method)
#define m17302(__this, method) (bool)m10559_gshared((t294 *)__this, method)
#define m17303(__this, method) (bool)m10561_gshared((t294 *)__this, method)
#define m17304(__this, method) (t29 *)m10563_gshared((t294 *)__this, method)
#define m17305(__this, method) (bool)m10565_gshared((t294 *)__this, method)
#define m17306(__this, method) (bool)m10567_gshared((t294 *)__this, method)
#define m17307(__this, p0, method) (t29 *)m10569_gshared((t294 *)__this, (int32_t)p0, method)
#define m17308(__this, p0, p1, method) (void)m10571_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m17309(__this, p0, method) (void)m10573_gshared((t294 *)__this, (t29 *)p0, method)
#define m17310(__this, p0, method) (void)m10575_gshared((t294 *)__this, (int32_t)p0, method)
#define m17311(__this, p0, method) (void)m10577_gshared((t294 *)__this, (t29*)p0, method)
#define m17312(__this, p0, method) (void)m10579_gshared((t294 *)__this, (t29*)p0, method)
#define m17313(__this, p0, method) (void)m10581_gshared((t294 *)__this, (t29*)p0, method)
#define m17314(__this, method) (t3136 *)m10583_gshared((t294 *)__this, method)
#define m17315(__this, method) (void)m10585_gshared((t294 *)__this, method)
#define m17316(__this, p0, method) (bool)m10587_gshared((t294 *)__this, (t29 *)p0, method)
#define m17317(__this, p0, p1, method) (void)m10589_gshared((t294 *)__this, (t316*)p0, (int32_t)p1, method)
#define m17318(__this, p0, method) (t565 *)m10591_gshared((t294 *)__this, (t2180 *)p0, method)
#define m17319(__this, p0, method) (void)m10593_gshared((t29 *)__this, (t2180 *)p0, method)
#define m17320(__this, p0, p1, p2, method) (int32_t)m10595_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, (t2180 *)p2, method)
 t661  m2984 (t568 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
#define m17321(__this, p0, method) (int32_t)m10598_gshared((t294 *)__this, (t29 *)p0, method)
#define m17322(__this, p0, p1, method) (void)m10600_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, method)
#define m17323(__this, p0, method) (void)m10602_gshared((t294 *)__this, (int32_t)p0, method)
#define m17324(__this, p0, p1, method) (void)m10604_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m17325(__this, p0, method) (void)m10606_gshared((t294 *)__this, (t29*)p0, method)
#define m17326(__this, p0, method) (bool)m10608_gshared((t294 *)__this, (t29 *)p0, method)
#define m17327(__this, p0, method) (int32_t)m10610_gshared((t294 *)__this, (t2180 *)p0, method)
#define m17328(__this, p0, method) (void)m10611_gshared((t294 *)__this, (int32_t)p0, method)
#define m17329(__this, method) (void)m10613_gshared((t294 *)__this, method)
#define m17330(__this, method) (void)m10615_gshared((t294 *)__this, method)
#define m17331(__this, p0, method) (void)m10617_gshared((t294 *)__this, (t2181 *)p0, method)
#define m17332(__this, method) (t3132*)m10619_gshared((t294 *)__this, method)
#define m17333(__this, method) (void)m10621_gshared((t294 *)__this, method)
#define m17334(__this, method) (int32_t)m10623_gshared((t294 *)__this, method)
#define m17335(__this, p0, method) (void)m10625_gshared((t294 *)__this, (int32_t)p0, method)
#define m17336(__this, method) (int32_t)m10626_gshared((t294 *)__this, method)
#define m17337(__this, p0, method) (t565 *)m10627_gshared((t294 *)__this, (int32_t)p0, method)
#define m17338(__this, p0, p1, method) (void)m10629_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
