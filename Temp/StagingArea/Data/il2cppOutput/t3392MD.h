﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3392;
struct t29;
struct t66;
struct t67;
#include "t35.h"

 void m18832_gshared (t3392 * __this, t29 * p0, t35 p1, MethodInfo* method);
#define m18832(__this, p0, p1, method) (void)m18832_gshared((t3392 *)__this, (t29 *)p0, (t35)p1, method)
 t29 * m18833_gshared (t3392 * __this, t29 * p0, MethodInfo* method);
#define m18833(__this, p0, method) (t29 *)m18833_gshared((t3392 *)__this, (t29 *)p0, method)
 t29 * m18834_gshared (t3392 * __this, t29 * p0, t67 * p1, t29 * p2, MethodInfo* method);
#define m18834(__this, p0, p1, p2, method) (t29 *)m18834_gshared((t3392 *)__this, (t29 *)p0, (t67 *)p1, (t29 *)p2, method)
 t29 * m18835_gshared (t3392 * __this, t29 * p0, MethodInfo* method);
#define m18835(__this, p0, method) (t29 *)m18835_gshared((t3392 *)__this, (t29 *)p0, method)
