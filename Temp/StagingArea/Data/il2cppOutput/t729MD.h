﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t729;
struct t29;
struct t730;
struct t733;
struct t20;
struct t136;
struct t7;
struct t727;
#include "t735.h"

 void m3123 (t729 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3124 (t729 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3125 (t729 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3126 (t729 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3127 (t729 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3128 (t729 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t730 * m3129 (t729 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3130 (t729 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3131 (t729 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3132 (t729 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3133 (t729 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3134 (t729 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3135 (t729 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3136 (t729 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3137 (t729 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3138 (t729 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t727 * m3139 (t729 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
