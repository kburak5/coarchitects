﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t500;
struct t499;
struct t502;
#include "t503.h"
#include "t35.h"
#include "t23.h"

 void m2510 (t500 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2511 (t500 * __this, t35 p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2512 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2513 (t29 * __this, t499 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2514 (t29 * __this, t499 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2515 (t500 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2516 (t500 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2517 (t500 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2518 (t500 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t503  m2519 (t500 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t503  m2520 (t500 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2521 (t500 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2522 (t500 * __this, int32_t p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2523 (t500 * __this, int32_t p0, int32_t p1, int32_t p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2524 (t500 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2525 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m2526 (t29 * __this, t23  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t500 * m2527 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2528 (t29 * __this, t502* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2529 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2530 (t29 * __this, t35 p0, int32_t* p1, int32_t* p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2531 (t29 * __this, t35 p0, int32_t* p1, int32_t* p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2532 (t29 * __this, t35 p0, t503 * p1, t503 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2533 (t29 * __this, t35 p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2534 (t29 * __this, t35 p0, int32_t p1, int32_t p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2535 (t29 * __this, t35 p0, int32_t p1, int32_t p2, int32_t p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2536 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2537 (t29 * __this, int32_t p0, int32_t p1, int32_t* p2, int32_t* p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
