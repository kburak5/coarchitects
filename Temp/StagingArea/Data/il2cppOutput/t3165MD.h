﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3165;
struct t557;
struct t7;
struct t29;
struct t556;

 void m17596_gshared (t3165 * __this, MethodInfo* method);
#define m17596(__this, method) (void)m17596_gshared((t3165 *)__this, method)
 t557 * m17597_gshared (t3165 * __this, t7* p0, t29 * p1, MethodInfo* method);
#define m17597(__this, p0, p1, method) (t557 *)m17597_gshared((t3165 *)__this, (t7*)p0, (t29 *)p1, method)
 t556 * m17598_gshared (t3165 * __this, t29 * p0, t557 * p1, MethodInfo* method);
#define m17598(__this, p0, p1, method) (t556 *)m17598_gshared((t3165 *)__this, (t29 *)p0, (t557 *)p1, method)
