﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3482;
struct t29;
#include "t465.h"

 void m19358 (t3482 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m19359 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m19360 (t3482 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t3482 * m19361 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
