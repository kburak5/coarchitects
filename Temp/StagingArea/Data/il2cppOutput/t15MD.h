﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t15;
struct t202;
struct t156;
struct t162;
struct t148;
struct t7;
struct t163;
#include "t150.h"
#include "t151.h"
#include "t152.h"
#include "t149.h"
#include "t238.h"
#include "t17.h"

 void m946 (t15 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m947 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t202 * m948 (t15 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t202 * m949 (t15 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t156 * m950 (t15 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t162 * m951 (t15 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m952 (t15 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t148 * m953 (t15 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m954 (t15 * __this, t148 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m955 (t15 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m56 (t15 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m956 (t15 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m957 (t15 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m958 (t15 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m959 (t15 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m960 (t15 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m961 (t15 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m962 (t15 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m963 (t15 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m964 (t15 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m965 (t15 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m966 (t15 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m967 (t15 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m968 (t15 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m969 (t15 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m970 (t15 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m971 (t15 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m972 (t15 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m973 (t15 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m974 (t15 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m975 (t15 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m976 (t15 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m977 (t15 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m978 (t15 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m979 (t15 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t238  m980 (t15 * __this, t17  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m981 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m982 (t15 * __this, t163 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m983 (t15 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m984 (t15 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m985 (t15 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m986 (t15 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m987 (t15 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m988 (t15 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m989 (t15 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m990 (t15 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m991 (t15 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
