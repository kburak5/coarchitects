﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3182;
struct t29;
struct t20;
#include "t741.h"

 void m17673 (t3182 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17674 (t3182 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17675 (t3182 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17676 (t3182 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17677 (t3182 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
