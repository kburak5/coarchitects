﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3443;
struct t29;
struct t20;
#include "t816.h"

 void m19085 (t3443 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m19086 (t3443 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m19087 (t3443 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19088 (t3443 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t816  m19089 (t3443 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
