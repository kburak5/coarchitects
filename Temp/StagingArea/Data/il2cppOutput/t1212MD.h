﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1212;
struct t781;
struct t783;
struct t782;
#include "t936.h"

 void m6360 (t1212 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6361 (t1212 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6362 (t1212 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6363 (t1212 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6364 (t29 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6365 (t29 * __this, t781* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t783 * m6366 (t29 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t782 * m6367 (t29 * __this, t781* p0, t936  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
