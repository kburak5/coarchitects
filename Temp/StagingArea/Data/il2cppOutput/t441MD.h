﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t441;
struct t441_marshaled;

 void m2470 (t441 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2471 (t441 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2472 (t441 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
void t441_marshal(const t441& unmarshaled, t441_marshaled& marshaled);
void t441_marshal_back(const t441_marshaled& marshaled, t441& unmarshaled);
void t441_marshal_cleanup(t441_marshaled& marshaled);
