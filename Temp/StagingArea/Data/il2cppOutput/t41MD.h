﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t41;
struct t41_marshaled;
struct t7;
struct t29;
#include "t385.h"
#include "t35.h"

 void m2544 (t41 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2545 (t29 * __this, t41 * p0, float p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m67 (t29 * __this, t41 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2546 (t29 * __this, t41 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1820 (t29 * __this, t41 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m1385 (t41 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1908 (t41 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1778 (t41 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m48 (t41 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m45 (t41 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m47 (t41 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2547 (t29 * __this, t41 * p0, t41 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2548 (t29 * __this, t41 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2549 (t41 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t35 m2550 (t41 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1293 (t29 * __this, t41 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1297 (t29 * __this, t41 * p0, t41 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1300 (t29 * __this, t41 * p0, t41 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
void t41_marshal(const t41& unmarshaled, t41_marshaled& marshaled);
void t41_marshal_back(const t41_marshaled& marshaled, t41& unmarshaled);
void t41_marshal_cleanup(t41_marshaled& marshaled);
