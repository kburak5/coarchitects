﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t130;
struct t133;
struct t557;
struct t7;
struct t29;
struct t556;
#include "t132.h"

 void m1494 (t130 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1499 (t130 * __this, t133 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12417 (t130 * __this, t133 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t557 * m1495 (t130 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t556 * m1496 (t130 * __this, t29 * p0, t557 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t556 * m12418 (t29 * __this, t133 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1498 (t130 * __this, t132  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
