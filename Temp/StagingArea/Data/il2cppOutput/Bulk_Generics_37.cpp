﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t5047_TI;

#include "t947.h"

#include "t20.h"

// Metadata Definition System.Collections.Generic.IEnumerator`1<System.StringComparison>
extern MethodInfo m34205_MI;
static PropertyInfo t5047____Current_PropertyInfo = 
{
	&t5047_TI, "Current", &m34205_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5047_PIs[] =
{
	&t5047____Current_PropertyInfo,
	NULL
};
extern Il2CppType t947_0_0_0;
extern void* RuntimeInvoker_t947 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34205_GM;
MethodInfo m34205_MI = 
{
	"get_Current", NULL, &t5047_TI, &t947_0_0_0, RuntimeInvoker_t947, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34205_GM};
static MethodInfo* t5047_MIs[] =
{
	&m34205_MI,
	NULL
};
extern TypeInfo t136_TI;
extern TypeInfo t324_TI;
static TypeInfo* t5047_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5047_0_0_0;
extern Il2CppType t5047_1_0_0;
struct t5047;
extern Il2CppGenericClass t5047_GC;
TypeInfo t5047_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t5047_MIs, t5047_PIs, NULL, NULL, NULL, NULL, NULL, &t5047_TI, t5047_ITIs, NULL, &EmptyCustomAttributesCache, &t5047_TI, &t5047_0_0_0, &t5047_1_0_0, NULL, &t5047_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3501.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3501_TI;
#include "t3501MD.h"

#include "t29.h"
#include "t44.h"
#include "t7.h"
#include "t914.h"
#include "t21.h"
#include "t40.h"
extern TypeInfo t947_TI;
extern TypeInfo t914_TI;
#include "t914MD.h"
#include "t20MD.h"
extern MethodInfo m19451_MI;
extern MethodInfo m3964_MI;
extern MethodInfo m3969_MI;
extern MethodInfo m26198_MI;
struct t20;
#include "t915.h"
 int32_t m26198 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m19447_MI;
 void m19447 (t3501 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19448_MI;
 t29 * m19448 (t3501 * __this, MethodInfo* method){
	{
		int32_t L_0 = m19451(__this, &m19451_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t947_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m19449_MI;
 void m19449 (t3501 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19450_MI;
 bool m19450 (t3501 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m19451 (t3501 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m26198(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m26198_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.StringComparison>
extern Il2CppType t20_0_0_1;
FieldInfo t3501_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3501_TI, offsetof(t3501, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3501_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3501_TI, offsetof(t3501, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3501_FIs[] =
{
	&t3501_f0_FieldInfo,
	&t3501_f1_FieldInfo,
	NULL
};
static PropertyInfo t3501____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3501_TI, "System.Collections.IEnumerator.Current", &m19448_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3501____Current_PropertyInfo = 
{
	&t3501_TI, "Current", &m19451_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3501_PIs[] =
{
	&t3501____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3501____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
static ParameterInfo t3501_m19447_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19447_GM;
MethodInfo m19447_MI = 
{
	".ctor", (methodPointerType)&m19447, &t3501_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3501_m19447_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19447_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19448_GM;
MethodInfo m19448_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m19448, &t3501_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19448_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19449_GM;
MethodInfo m19449_MI = 
{
	"Dispose", (methodPointerType)&m19449, &t3501_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19449_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19450_GM;
MethodInfo m19450_MI = 
{
	"MoveNext", (methodPointerType)&m19450, &t3501_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19450_GM};
extern Il2CppType t947_0_0_0;
extern void* RuntimeInvoker_t947 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19451_GM;
MethodInfo m19451_MI = 
{
	"get_Current", (methodPointerType)&m19451, &t3501_TI, &t947_0_0_0, RuntimeInvoker_t947, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19451_GM};
static MethodInfo* t3501_MIs[] =
{
	&m19447_MI,
	&m19448_MI,
	&m19449_MI,
	&m19450_MI,
	&m19451_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
static MethodInfo* t3501_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19448_MI,
	&m19450_MI,
	&m19449_MI,
	&m19451_MI,
};
static TypeInfo* t3501_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t5047_TI,
};
static Il2CppInterfaceOffsetPair t3501_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t5047_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3501_0_0_0;
extern Il2CppType t3501_1_0_0;
extern TypeInfo t110_TI;
extern Il2CppGenericClass t3501_GC;
extern TypeInfo t20_TI;
TypeInfo t3501_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3501_MIs, t3501_PIs, t3501_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3501_TI, t3501_ITIs, t3501_VT, &EmptyCustomAttributesCache, &t3501_TI, &t3501_0_0_0, &t3501_1_0_0, t3501_IOs, &t3501_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3501)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6612_TI;

#include "mscorlib_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<System.StringComparison>
extern MethodInfo m34206_MI;
static PropertyInfo t6612____Count_PropertyInfo = 
{
	&t6612_TI, "Count", &m34206_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m34207_MI;
static PropertyInfo t6612____IsReadOnly_PropertyInfo = 
{
	&t6612_TI, "IsReadOnly", &m34207_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6612_PIs[] =
{
	&t6612____Count_PropertyInfo,
	&t6612____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34206_GM;
MethodInfo m34206_MI = 
{
	"get_Count", NULL, &t6612_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34206_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34207_GM;
MethodInfo m34207_MI = 
{
	"get_IsReadOnly", NULL, &t6612_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34207_GM};
extern Il2CppType t947_0_0_0;
extern Il2CppType t947_0_0_0;
static ParameterInfo t6612_m34208_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t947_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34208_GM;
MethodInfo m34208_MI = 
{
	"Add", NULL, &t6612_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6612_m34208_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34208_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34209_GM;
MethodInfo m34209_MI = 
{
	"Clear", NULL, &t6612_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34209_GM};
extern Il2CppType t947_0_0_0;
static ParameterInfo t6612_m34210_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t947_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34210_GM;
MethodInfo m34210_MI = 
{
	"Contains", NULL, &t6612_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6612_m34210_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34210_GM};
extern Il2CppType t3723_0_0_0;
extern Il2CppType t3723_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6612_m34211_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3723_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34211_GM;
MethodInfo m34211_MI = 
{
	"CopyTo", NULL, &t6612_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6612_m34211_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34211_GM};
extern Il2CppType t947_0_0_0;
static ParameterInfo t6612_m34212_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t947_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34212_GM;
MethodInfo m34212_MI = 
{
	"Remove", NULL, &t6612_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6612_m34212_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34212_GM};
static MethodInfo* t6612_MIs[] =
{
	&m34206_MI,
	&m34207_MI,
	&m34208_MI,
	&m34209_MI,
	&m34210_MI,
	&m34211_MI,
	&m34212_MI,
	NULL
};
extern TypeInfo t603_TI;
extern TypeInfo t6614_TI;
static TypeInfo* t6612_ITIs[] = 
{
	&t603_TI,
	&t6614_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6612_0_0_0;
extern Il2CppType t6612_1_0_0;
struct t6612;
extern Il2CppGenericClass t6612_GC;
TypeInfo t6612_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6612_MIs, t6612_PIs, NULL, NULL, NULL, NULL, NULL, &t6612_TI, t6612_ITIs, NULL, &EmptyCustomAttributesCache, &t6612_TI, &t6612_0_0_0, &t6612_1_0_0, NULL, &t6612_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.StringComparison>
extern Il2CppType t5047_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34213_GM;
MethodInfo m34213_MI = 
{
	"GetEnumerator", NULL, &t6614_TI, &t5047_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34213_GM};
static MethodInfo* t6614_MIs[] =
{
	&m34213_MI,
	NULL
};
static TypeInfo* t6614_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6614_0_0_0;
extern Il2CppType t6614_1_0_0;
struct t6614;
extern Il2CppGenericClass t6614_GC;
TypeInfo t6614_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6614_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6614_TI, t6614_ITIs, NULL, &EmptyCustomAttributesCache, &t6614_TI, &t6614_0_0_0, &t6614_1_0_0, NULL, &t6614_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6613_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.StringComparison>
extern MethodInfo m34214_MI;
extern MethodInfo m34215_MI;
static PropertyInfo t6613____Item_PropertyInfo = 
{
	&t6613_TI, "Item", &m34214_MI, &m34215_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6613_PIs[] =
{
	&t6613____Item_PropertyInfo,
	NULL
};
extern Il2CppType t947_0_0_0;
static ParameterInfo t6613_m34216_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t947_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34216_GM;
MethodInfo m34216_MI = 
{
	"IndexOf", NULL, &t6613_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6613_m34216_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34216_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t947_0_0_0;
static ParameterInfo t6613_m34217_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t947_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34217_GM;
MethodInfo m34217_MI = 
{
	"Insert", NULL, &t6613_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6613_m34217_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34217_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6613_m34218_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34218_GM;
MethodInfo m34218_MI = 
{
	"RemoveAt", NULL, &t6613_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6613_m34218_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34218_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6613_m34214_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t947_0_0_0;
extern void* RuntimeInvoker_t947_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34214_GM;
MethodInfo m34214_MI = 
{
	"get_Item", NULL, &t6613_TI, &t947_0_0_0, RuntimeInvoker_t947_t44, t6613_m34214_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34214_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t947_0_0_0;
static ParameterInfo t6613_m34215_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t947_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34215_GM;
MethodInfo m34215_MI = 
{
	"set_Item", NULL, &t6613_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6613_m34215_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34215_GM};
static MethodInfo* t6613_MIs[] =
{
	&m34216_MI,
	&m34217_MI,
	&m34218_MI,
	&m34214_MI,
	&m34215_MI,
	NULL
};
static TypeInfo* t6613_ITIs[] = 
{
	&t603_TI,
	&t6612_TI,
	&t6614_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6613_0_0_0;
extern Il2CppType t6613_1_0_0;
struct t6613;
extern Il2CppGenericClass t6613_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6613_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6613_MIs, t6613_PIs, NULL, NULL, NULL, NULL, NULL, &t6613_TI, t6613_ITIs, NULL, &t1908__CustomAttributeCache, &t6613_TI, &t6613_0_0_0, &t6613_1_0_0, NULL, &t6613_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5049_TI;

#include "t939.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.StringSplitOptions>
extern MethodInfo m34219_MI;
static PropertyInfo t5049____Current_PropertyInfo = 
{
	&t5049_TI, "Current", &m34219_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5049_PIs[] =
{
	&t5049____Current_PropertyInfo,
	NULL
};
extern Il2CppType t939_0_0_0;
extern void* RuntimeInvoker_t939 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34219_GM;
MethodInfo m34219_MI = 
{
	"get_Current", NULL, &t5049_TI, &t939_0_0_0, RuntimeInvoker_t939, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34219_GM};
static MethodInfo* t5049_MIs[] =
{
	&m34219_MI,
	NULL
};
static TypeInfo* t5049_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5049_0_0_0;
extern Il2CppType t5049_1_0_0;
struct t5049;
extern Il2CppGenericClass t5049_GC;
TypeInfo t5049_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t5049_MIs, t5049_PIs, NULL, NULL, NULL, NULL, NULL, &t5049_TI, t5049_ITIs, NULL, &EmptyCustomAttributesCache, &t5049_TI, &t5049_0_0_0, &t5049_1_0_0, NULL, &t5049_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3502.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3502_TI;
#include "t3502MD.h"

extern TypeInfo t939_TI;
extern MethodInfo m19456_MI;
extern MethodInfo m26209_MI;
struct t20;
 int32_t m26209 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m19452_MI;
 void m19452 (t3502 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19453_MI;
 t29 * m19453 (t3502 * __this, MethodInfo* method){
	{
		int32_t L_0 = m19456(__this, &m19456_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t939_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m19454_MI;
 void m19454 (t3502 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19455_MI;
 bool m19455 (t3502 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m19456 (t3502 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m26209(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m26209_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.StringSplitOptions>
extern Il2CppType t20_0_0_1;
FieldInfo t3502_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3502_TI, offsetof(t3502, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3502_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3502_TI, offsetof(t3502, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3502_FIs[] =
{
	&t3502_f0_FieldInfo,
	&t3502_f1_FieldInfo,
	NULL
};
static PropertyInfo t3502____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3502_TI, "System.Collections.IEnumerator.Current", &m19453_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3502____Current_PropertyInfo = 
{
	&t3502_TI, "Current", &m19456_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3502_PIs[] =
{
	&t3502____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3502____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3502_m19452_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19452_GM;
MethodInfo m19452_MI = 
{
	".ctor", (methodPointerType)&m19452, &t3502_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3502_m19452_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19452_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19453_GM;
MethodInfo m19453_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m19453, &t3502_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19453_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19454_GM;
MethodInfo m19454_MI = 
{
	"Dispose", (methodPointerType)&m19454, &t3502_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19454_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19455_GM;
MethodInfo m19455_MI = 
{
	"MoveNext", (methodPointerType)&m19455, &t3502_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19455_GM};
extern Il2CppType t939_0_0_0;
extern void* RuntimeInvoker_t939 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19456_GM;
MethodInfo m19456_MI = 
{
	"get_Current", (methodPointerType)&m19456, &t3502_TI, &t939_0_0_0, RuntimeInvoker_t939, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19456_GM};
static MethodInfo* t3502_MIs[] =
{
	&m19452_MI,
	&m19453_MI,
	&m19454_MI,
	&m19455_MI,
	&m19456_MI,
	NULL
};
static MethodInfo* t3502_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19453_MI,
	&m19455_MI,
	&m19454_MI,
	&m19456_MI,
};
static TypeInfo* t3502_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t5049_TI,
};
static Il2CppInterfaceOffsetPair t3502_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t5049_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3502_0_0_0;
extern Il2CppType t3502_1_0_0;
extern Il2CppGenericClass t3502_GC;
TypeInfo t3502_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3502_MIs, t3502_PIs, t3502_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3502_TI, t3502_ITIs, t3502_VT, &EmptyCustomAttributesCache, &t3502_TI, &t3502_0_0_0, &t3502_1_0_0, t3502_IOs, &t3502_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3502)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6615_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.StringSplitOptions>
extern MethodInfo m34220_MI;
static PropertyInfo t6615____Count_PropertyInfo = 
{
	&t6615_TI, "Count", &m34220_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m34221_MI;
static PropertyInfo t6615____IsReadOnly_PropertyInfo = 
{
	&t6615_TI, "IsReadOnly", &m34221_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6615_PIs[] =
{
	&t6615____Count_PropertyInfo,
	&t6615____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34220_GM;
MethodInfo m34220_MI = 
{
	"get_Count", NULL, &t6615_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34220_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34221_GM;
MethodInfo m34221_MI = 
{
	"get_IsReadOnly", NULL, &t6615_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34221_GM};
extern Il2CppType t939_0_0_0;
extern Il2CppType t939_0_0_0;
static ParameterInfo t6615_m34222_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t939_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34222_GM;
MethodInfo m34222_MI = 
{
	"Add", NULL, &t6615_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6615_m34222_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34222_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34223_GM;
MethodInfo m34223_MI = 
{
	"Clear", NULL, &t6615_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34223_GM};
extern Il2CppType t939_0_0_0;
static ParameterInfo t6615_m34224_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t939_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34224_GM;
MethodInfo m34224_MI = 
{
	"Contains", NULL, &t6615_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6615_m34224_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34224_GM};
extern Il2CppType t3724_0_0_0;
extern Il2CppType t3724_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6615_m34225_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3724_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34225_GM;
MethodInfo m34225_MI = 
{
	"CopyTo", NULL, &t6615_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6615_m34225_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34225_GM};
extern Il2CppType t939_0_0_0;
static ParameterInfo t6615_m34226_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t939_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34226_GM;
MethodInfo m34226_MI = 
{
	"Remove", NULL, &t6615_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6615_m34226_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34226_GM};
static MethodInfo* t6615_MIs[] =
{
	&m34220_MI,
	&m34221_MI,
	&m34222_MI,
	&m34223_MI,
	&m34224_MI,
	&m34225_MI,
	&m34226_MI,
	NULL
};
extern TypeInfo t6617_TI;
static TypeInfo* t6615_ITIs[] = 
{
	&t603_TI,
	&t6617_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6615_0_0_0;
extern Il2CppType t6615_1_0_0;
struct t6615;
extern Il2CppGenericClass t6615_GC;
TypeInfo t6615_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6615_MIs, t6615_PIs, NULL, NULL, NULL, NULL, NULL, &t6615_TI, t6615_ITIs, NULL, &EmptyCustomAttributesCache, &t6615_TI, &t6615_0_0_0, &t6615_1_0_0, NULL, &t6615_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.StringSplitOptions>
extern Il2CppType t5049_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34227_GM;
MethodInfo m34227_MI = 
{
	"GetEnumerator", NULL, &t6617_TI, &t5049_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34227_GM};
static MethodInfo* t6617_MIs[] =
{
	&m34227_MI,
	NULL
};
static TypeInfo* t6617_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6617_0_0_0;
extern Il2CppType t6617_1_0_0;
struct t6617;
extern Il2CppGenericClass t6617_GC;
TypeInfo t6617_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6617_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6617_TI, t6617_ITIs, NULL, &EmptyCustomAttributesCache, &t6617_TI, &t6617_0_0_0, &t6617_1_0_0, NULL, &t6617_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6616_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.StringSplitOptions>
extern MethodInfo m34228_MI;
extern MethodInfo m34229_MI;
static PropertyInfo t6616____Item_PropertyInfo = 
{
	&t6616_TI, "Item", &m34228_MI, &m34229_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6616_PIs[] =
{
	&t6616____Item_PropertyInfo,
	NULL
};
extern Il2CppType t939_0_0_0;
static ParameterInfo t6616_m34230_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t939_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34230_GM;
MethodInfo m34230_MI = 
{
	"IndexOf", NULL, &t6616_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6616_m34230_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34230_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t939_0_0_0;
static ParameterInfo t6616_m34231_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t939_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34231_GM;
MethodInfo m34231_MI = 
{
	"Insert", NULL, &t6616_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6616_m34231_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34231_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6616_m34232_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34232_GM;
MethodInfo m34232_MI = 
{
	"RemoveAt", NULL, &t6616_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6616_m34232_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34232_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6616_m34228_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t939_0_0_0;
extern void* RuntimeInvoker_t939_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34228_GM;
MethodInfo m34228_MI = 
{
	"get_Item", NULL, &t6616_TI, &t939_0_0_0, RuntimeInvoker_t939_t44, t6616_m34228_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34228_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t939_0_0_0;
static ParameterInfo t6616_m34229_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t939_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34229_GM;
MethodInfo m34229_MI = 
{
	"set_Item", NULL, &t6616_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6616_m34229_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34229_GM};
static MethodInfo* t6616_MIs[] =
{
	&m34230_MI,
	&m34231_MI,
	&m34232_MI,
	&m34228_MI,
	&m34229_MI,
	NULL
};
static TypeInfo* t6616_ITIs[] = 
{
	&t603_TI,
	&t6615_TI,
	&t6617_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6616_0_0_0;
extern Il2CppType t6616_1_0_0;
struct t6616;
extern Il2CppGenericClass t6616_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6616_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6616_MIs, t6616_PIs, NULL, NULL, NULL, NULL, NULL, &t6616_TI, t6616_ITIs, NULL, &t1908__CustomAttributeCache, &t6616_TI, &t6616_0_0_0, &t6616_1_0_0, NULL, &t6616_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5051_TI;

#include "t1671.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.ThreadStaticAttribute>
extern MethodInfo m34233_MI;
static PropertyInfo t5051____Current_PropertyInfo = 
{
	&t5051_TI, "Current", &m34233_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5051_PIs[] =
{
	&t5051____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1671_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34233_GM;
MethodInfo m34233_MI = 
{
	"get_Current", NULL, &t5051_TI, &t1671_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34233_GM};
static MethodInfo* t5051_MIs[] =
{
	&m34233_MI,
	NULL
};
static TypeInfo* t5051_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5051_0_0_0;
extern Il2CppType t5051_1_0_0;
struct t5051;
extern Il2CppGenericClass t5051_GC;
TypeInfo t5051_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t5051_MIs, t5051_PIs, NULL, NULL, NULL, NULL, NULL, &t5051_TI, t5051_ITIs, NULL, &EmptyCustomAttributesCache, &t5051_TI, &t5051_0_0_0, &t5051_1_0_0, NULL, &t5051_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3503.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3503_TI;
#include "t3503MD.h"

extern TypeInfo t1671_TI;
extern MethodInfo m19461_MI;
extern MethodInfo m26220_MI;
struct t20;
struct t20;
 t29 * m19490_gshared (t20 * __this, int32_t p0, MethodInfo* method);
#define m19490(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
#define m26220(__this, p0, method) (t1671 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.ThreadStaticAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3503_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3503_TI, offsetof(t3503, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3503_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3503_TI, offsetof(t3503, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3503_FIs[] =
{
	&t3503_f0_FieldInfo,
	&t3503_f1_FieldInfo,
	NULL
};
extern MethodInfo m19458_MI;
static PropertyInfo t3503____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3503_TI, "System.Collections.IEnumerator.Current", &m19458_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3503____Current_PropertyInfo = 
{
	&t3503_TI, "Current", &m19461_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3503_PIs[] =
{
	&t3503____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3503____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3503_m19457_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19457_GM;
MethodInfo m19457_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3503_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3503_m19457_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19457_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19458_GM;
MethodInfo m19458_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3503_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19458_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19459_GM;
MethodInfo m19459_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3503_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19459_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19460_GM;
MethodInfo m19460_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3503_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19460_GM};
extern Il2CppType t1671_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19461_GM;
MethodInfo m19461_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3503_TI, &t1671_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19461_GM};
static MethodInfo* t3503_MIs[] =
{
	&m19457_MI,
	&m19458_MI,
	&m19459_MI,
	&m19460_MI,
	&m19461_MI,
	NULL
};
extern MethodInfo m19460_MI;
extern MethodInfo m19459_MI;
static MethodInfo* t3503_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19458_MI,
	&m19460_MI,
	&m19459_MI,
	&m19461_MI,
};
static TypeInfo* t3503_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t5051_TI,
};
static Il2CppInterfaceOffsetPair t3503_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t5051_TI, 7},
};
extern TypeInfo t1671_TI;
static Il2CppRGCTXData t3503_RGCTXData[3] = 
{
	&m19461_MI/* Method Usage */,
	&t1671_TI/* Class Usage */,
	&m26220_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3503_0_0_0;
extern Il2CppType t3503_1_0_0;
extern Il2CppGenericClass t3503_GC;
TypeInfo t3503_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3503_MIs, t3503_PIs, t3503_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3503_TI, t3503_ITIs, t3503_VT, &EmptyCustomAttributesCache, &t3503_TI, &t3503_0_0_0, &t3503_1_0_0, t3503_IOs, &t3503_GC, NULL, NULL, NULL, t3503_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3503)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6618_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.ThreadStaticAttribute>
extern MethodInfo m34234_MI;
static PropertyInfo t6618____Count_PropertyInfo = 
{
	&t6618_TI, "Count", &m34234_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m34235_MI;
static PropertyInfo t6618____IsReadOnly_PropertyInfo = 
{
	&t6618_TI, "IsReadOnly", &m34235_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6618_PIs[] =
{
	&t6618____Count_PropertyInfo,
	&t6618____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34234_GM;
MethodInfo m34234_MI = 
{
	"get_Count", NULL, &t6618_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34234_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34235_GM;
MethodInfo m34235_MI = 
{
	"get_IsReadOnly", NULL, &t6618_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34235_GM};
extern Il2CppType t1671_0_0_0;
extern Il2CppType t1671_0_0_0;
static ParameterInfo t6618_m34236_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1671_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34236_GM;
MethodInfo m34236_MI = 
{
	"Add", NULL, &t6618_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6618_m34236_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34236_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34237_GM;
MethodInfo m34237_MI = 
{
	"Clear", NULL, &t6618_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34237_GM};
extern Il2CppType t1671_0_0_0;
static ParameterInfo t6618_m34238_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1671_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34238_GM;
MethodInfo m34238_MI = 
{
	"Contains", NULL, &t6618_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6618_m34238_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34238_GM};
extern Il2CppType t3725_0_0_0;
extern Il2CppType t3725_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6618_m34239_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3725_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34239_GM;
MethodInfo m34239_MI = 
{
	"CopyTo", NULL, &t6618_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6618_m34239_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34239_GM};
extern Il2CppType t1671_0_0_0;
static ParameterInfo t6618_m34240_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1671_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34240_GM;
MethodInfo m34240_MI = 
{
	"Remove", NULL, &t6618_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6618_m34240_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34240_GM};
static MethodInfo* t6618_MIs[] =
{
	&m34234_MI,
	&m34235_MI,
	&m34236_MI,
	&m34237_MI,
	&m34238_MI,
	&m34239_MI,
	&m34240_MI,
	NULL
};
extern TypeInfo t6620_TI;
static TypeInfo* t6618_ITIs[] = 
{
	&t603_TI,
	&t6620_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6618_0_0_0;
extern Il2CppType t6618_1_0_0;
struct t6618;
extern Il2CppGenericClass t6618_GC;
TypeInfo t6618_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6618_MIs, t6618_PIs, NULL, NULL, NULL, NULL, NULL, &t6618_TI, t6618_ITIs, NULL, &EmptyCustomAttributesCache, &t6618_TI, &t6618_0_0_0, &t6618_1_0_0, NULL, &t6618_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.ThreadStaticAttribute>
extern Il2CppType t5051_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34241_GM;
MethodInfo m34241_MI = 
{
	"GetEnumerator", NULL, &t6620_TI, &t5051_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34241_GM};
static MethodInfo* t6620_MIs[] =
{
	&m34241_MI,
	NULL
};
static TypeInfo* t6620_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6620_0_0_0;
extern Il2CppType t6620_1_0_0;
struct t6620;
extern Il2CppGenericClass t6620_GC;
TypeInfo t6620_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6620_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6620_TI, t6620_ITIs, NULL, &EmptyCustomAttributesCache, &t6620_TI, &t6620_0_0_0, &t6620_1_0_0, NULL, &t6620_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6619_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.ThreadStaticAttribute>
extern MethodInfo m34242_MI;
extern MethodInfo m34243_MI;
static PropertyInfo t6619____Item_PropertyInfo = 
{
	&t6619_TI, "Item", &m34242_MI, &m34243_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6619_PIs[] =
{
	&t6619____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1671_0_0_0;
static ParameterInfo t6619_m34244_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1671_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34244_GM;
MethodInfo m34244_MI = 
{
	"IndexOf", NULL, &t6619_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6619_m34244_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34244_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1671_0_0_0;
static ParameterInfo t6619_m34245_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1671_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34245_GM;
MethodInfo m34245_MI = 
{
	"Insert", NULL, &t6619_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6619_m34245_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34245_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6619_m34246_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34246_GM;
MethodInfo m34246_MI = 
{
	"RemoveAt", NULL, &t6619_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6619_m34246_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34246_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6619_m34242_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1671_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34242_GM;
MethodInfo m34242_MI = 
{
	"get_Item", NULL, &t6619_TI, &t1671_0_0_0, RuntimeInvoker_t29_t44, t6619_m34242_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34242_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1671_0_0_0;
static ParameterInfo t6619_m34243_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1671_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34243_GM;
MethodInfo m34243_MI = 
{
	"set_Item", NULL, &t6619_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6619_m34243_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34243_GM};
static MethodInfo* t6619_MIs[] =
{
	&m34244_MI,
	&m34245_MI,
	&m34246_MI,
	&m34242_MI,
	&m34243_MI,
	NULL
};
static TypeInfo* t6619_ITIs[] = 
{
	&t603_TI,
	&t6618_TI,
	&t6620_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6619_0_0_0;
extern Il2CppType t6619_1_0_0;
struct t6619;
extern Il2CppGenericClass t6619_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6619_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6619_MIs, t6619_PIs, NULL, NULL, NULL, NULL, NULL, &t6619_TI, t6619_ITIs, NULL, &t1908__CustomAttributeCache, &t6619_TI, &t6619_0_0_0, &t6619_1_0_0, NULL, &t6619_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2097.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2097_TI;
#include "t2097MD.h"

#include "t816.h"
extern TypeInfo t816_TI;
extern TypeInfo t2099_TI;
extern TypeInfo t44_TI;
#include "t3504MD.h"
extern MethodInfo m19463_MI;
extern MethodInfo m33734_MI;


extern MethodInfo m10268_MI;
 void m10268 (t2097 * __this, MethodInfo* method){
	{
		m19463(__this, &m19463_MI);
		return;
	}
}
extern MethodInfo m19462_MI;
 int32_t m19462 (t2097 * __this, t816  p0, t816  p1, MethodInfo* method){
	int32_t G_B4_0 = 0;
	{
		t816  L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t816_TI), &L_0);
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		t816  L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t816_TI), &L_2);
		if (L_3)
		{
			goto IL_0013;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0014;
	}

IL_0013:
	{
		G_B4_0 = (-1);
	}

IL_0014:
	{
		return G_B4_0;
	}

IL_0015:
	{
		t816  L_4 = p1;
		t29 * L_5 = Box(InitializedTypeInfo(&t816_TI), &L_4);
		if (L_5)
		{
			goto IL_001f;
		}
	}
	{
		return 1;
	}

IL_001f:
	{
		int32_t L_6 = (int32_t)InterfaceFuncInvoker1< int32_t, t816  >::Invoke(&m33734_MI, Box(InitializedTypeInfo(&t816_TI), &(*(&p0))), p1);
		return L_6;
	}
}
// Metadata Definition System.Collections.Generic.GenericComparer`1<System.TimeSpan>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10268_GM;
MethodInfo m10268_MI = 
{
	".ctor", (methodPointerType)&m10268, &t2097_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10268_GM};
extern Il2CppType t816_0_0_0;
extern Il2CppType t816_0_0_0;
extern Il2CppType t816_0_0_0;
static ParameterInfo t2097_m19462_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t816_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t816_t816 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19462_GM;
MethodInfo m19462_MI = 
{
	"Compare", (methodPointerType)&m19462, &t2097_TI, &t44_0_0_0, RuntimeInvoker_t44_t816_t816, t2097_m19462_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19462_GM};
static MethodInfo* t2097_MIs[] =
{
	&m10268_MI,
	&m19462_MI,
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m1332_MI;
extern MethodInfo m19465_MI;
static MethodInfo* t2097_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m19462_MI,
	&m19465_MI,
	&m19462_MI,
};
extern TypeInfo t6770_TI;
extern TypeInfo t726_TI;
static Il2CppInterfaceOffsetPair t2097_IOs[] = 
{
	{ &t6770_TI, 4},
	{ &t726_TI, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2097_0_0_0;
extern Il2CppType t2097_1_0_0;
extern TypeInfo t3504_TI;
struct t2097;
extern Il2CppGenericClass t2097_GC;
TypeInfo t2097_TI = 
{
	&g_mscorlib_dll_Image, NULL, "GenericComparer`1", "System.Collections.Generic", t2097_MIs, NULL, NULL, NULL, &t3504_TI, NULL, NULL, &t2097_TI, NULL, t2097_VT, &EmptyCustomAttributesCache, &t2097_TI, &t2097_0_0_0, &t2097_1_0_0, t2097_IOs, &t2097_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2097), 0, -1, 0, 0, -1, 1057024, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 7, 0, 2};
#include "t3504.h"
#ifndef _MSC_VER
#else
#endif

#include "t42.h"
#include "t43.h"
#include "t1247.h"
#include "t3505.h"
#include "t305.h"
extern TypeInfo t42_TI;
extern TypeInfo t40_TI;
extern TypeInfo t1247_TI;
extern TypeInfo t537_TI;
extern TypeInfo t3505_TI;
extern TypeInfo t305_TI;
#include "t29MD.h"
#include "t42MD.h"
#include "t931MD.h"
#include "t3505MD.h"
#include "t305MD.h"
extern Il2CppType t2099_0_0_0;
extern Il2CppType t1247_0_0_0;
extern MethodInfo m1331_MI;
extern MethodInfo m1554_MI;
extern MethodInfo m2981_MI;
extern MethodInfo m2979_MI;
extern MethodInfo m8828_MI;
extern MethodInfo m19467_MI;
extern MethodInfo m34247_MI;
extern MethodInfo m8852_MI;


 void m19463 (t3504 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m19464_MI;
 void m19464 (t29 * __this, MethodInfo* method){
	t3505 * L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer = (t3505 *)il2cpp_codegen_object_new(InitializedTypeInfo(&t3505_TI));
	m19467(L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer, &m19467_MI);
	((t3504_SFs*)InitializedTypeInfo(&t3504_TI)->static_fields)->f0 = L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer;
}
 int32_t m19465 (t3504 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	int32_t G_B4_0 = 0;
	{
		if (p0)
		{
			goto IL_000b;
		}
	}
	{
		if (p1)
		{
			goto IL_0009;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_000a;
	}

IL_0009:
	{
		G_B4_0 = (-1);
	}

IL_000a:
	{
		return G_B4_0;
	}

IL_000b:
	{
		if (p1)
		{
			goto IL_0010;
		}
	}
	{
		return 1;
	}

IL_0010:
	{
		if (!((t29 *)IsInst(p0, InitializedTypeInfo(&t816_TI))))
		{
			goto IL_0033;
		}
	}
	{
		if (!((t29 *)IsInst(p1, InitializedTypeInfo(&t816_TI))))
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker2< int32_t, t816 , t816  >::Invoke(&m34247_MI, __this, ((*(t816 *)((t816 *)UnBox (p0, InitializedTypeInfo(&t816_TI))))), ((*(t816 *)((t816 *)UnBox (p1, InitializedTypeInfo(&t816_TI))))));
		return L_0;
	}

IL_0033:
	{
		t305 * L_1 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m8852(L_1, &m8852_MI);
		il2cpp_codegen_raise_exception(L_1);
	}
}
extern MethodInfo m19466_MI;
 t3504 * m19466 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t3504_TI));
		return (((t3504_SFs*)InitializedTypeInfo(&t3504_TI)->static_fields)->f0);
	}
}
// Metadata Definition System.Collections.Generic.Comparer`1<System.TimeSpan>
extern Il2CppType t3504_0_0_49;
FieldInfo t3504_f0_FieldInfo = 
{
	"_default", &t3504_0_0_49, &t3504_TI, offsetof(t3504_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3504_FIs[] =
{
	&t3504_f0_FieldInfo,
	NULL
};
static PropertyInfo t3504____Default_PropertyInfo = 
{
	&t3504_TI, "Default", &m19466_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3504_PIs[] =
{
	&t3504____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19463_GM;
MethodInfo m19463_MI = 
{
	".ctor", (methodPointerType)&m19463, &t3504_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19463_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19464_GM;
MethodInfo m19464_MI = 
{
	".cctor", (methodPointerType)&m19464, &t3504_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19464_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3504_m19465_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19465_GM;
MethodInfo m19465_MI = 
{
	"System.Collections.IComparer.Compare", (methodPointerType)&m19465, &t3504_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t3504_m19465_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19465_GM};
extern Il2CppType t816_0_0_0;
extern Il2CppType t816_0_0_0;
static ParameterInfo t3504_m34247_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t816_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t816_t816 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34247_GM;
MethodInfo m34247_MI = 
{
	"Compare", NULL, &t3504_TI, &t44_0_0_0, RuntimeInvoker_t44_t816_t816, t3504_m34247_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34247_GM};
extern Il2CppType t3504_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19466_GM;
MethodInfo m19466_MI = 
{
	"get_Default", (methodPointerType)&m19466, &t3504_TI, &t3504_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19466_GM};
static MethodInfo* t3504_MIs[] =
{
	&m19463_MI,
	&m19464_MI,
	&m19465_MI,
	&m34247_MI,
	&m19466_MI,
	NULL
};
static MethodInfo* t3504_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m34247_MI,
	&m19465_MI,
	NULL,
};
static TypeInfo* t3504_ITIs[] = 
{
	&t6770_TI,
	&t726_TI,
};
static Il2CppInterfaceOffsetPair t3504_IOs[] = 
{
	{ &t6770_TI, 4},
	{ &t726_TI, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3504_0_0_0;
extern Il2CppType t3504_1_0_0;
extern TypeInfo t29_TI;
struct t3504;
extern Il2CppGenericClass t3504_GC;
TypeInfo t3504_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparer`1", "System.Collections.Generic", t3504_MIs, t3504_PIs, t3504_FIs, NULL, &t29_TI, NULL, NULL, &t3504_TI, t3504_ITIs, t3504_VT, &EmptyCustomAttributesCache, &t3504_TI, &t3504_0_0_0, &t3504_1_0_0, t3504_IOs, &t3504_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3504), 0, -1, sizeof(t3504_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 5, 1, 1, 0, 0, 7, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IComparer`1<System.TimeSpan>
extern Il2CppType t816_0_0_0;
extern Il2CppType t816_0_0_0;
static ParameterInfo t6770_m34248_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t816_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t816_t816 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34248_GM;
MethodInfo m34248_MI = 
{
	"Compare", NULL, &t6770_TI, &t44_0_0_0, RuntimeInvoker_t44_t816_t816, t6770_m34248_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34248_GM};
static MethodInfo* t6770_MIs[] =
{
	&m34248_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6770_0_0_0;
extern Il2CppType t6770_1_0_0;
struct t6770;
extern Il2CppGenericClass t6770_GC;
TypeInfo t6770_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparer`1", "System.Collections.Generic", t6770_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6770_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6770_TI, &t6770_0_0_0, &t6770_1_0_0, NULL, &t6770_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo t290_TI;
extern MethodInfo m9672_MI;
extern MethodInfo m1935_MI;


 void m19467 (t3505 * __this, MethodInfo* method){
	{
		m19463(__this, &m19463_MI);
		return;
	}
}
extern MethodInfo m19468_MI;
 int32_t m19468 (t3505 * __this, t816  p0, t816  p1, MethodInfo* method){
	int32_t G_B4_0 = 0;
	{
		t816  L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t816_TI), &L_0);
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		t816  L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t816_TI), &L_2);
		if (L_3)
		{
			goto IL_0013;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0014;
	}

IL_0013:
	{
		G_B4_0 = (-1);
	}

IL_0014:
	{
		return G_B4_0;
	}

IL_0015:
	{
		t816  L_4 = p1;
		t29 * L_5 = Box(InitializedTypeInfo(&t816_TI), &L_4);
		if (L_5)
		{
			goto IL_001f;
		}
	}
	{
		return 1;
	}

IL_001f:
	{
		t816  L_6 = p0;
		t29 * L_7 = Box(InitializedTypeInfo(&t816_TI), &L_6);
		if (!((t29*)IsInst(L_7, InitializedTypeInfo(&t2099_TI))))
		{
			goto IL_003e;
		}
	}
	{
		t816  L_8 = p0;
		t29 * L_9 = Box(InitializedTypeInfo(&t816_TI), &L_8);
		int32_t L_10 = (int32_t)InterfaceFuncInvoker1< int32_t, t816  >::Invoke(&m33734_MI, ((t29*)Castclass(L_9, InitializedTypeInfo(&t2099_TI))), p1);
		return L_10;
	}

IL_003e:
	{
		t816  L_11 = p0;
		t29 * L_12 = Box(InitializedTypeInfo(&t816_TI), &L_11);
		if (!((t29 *)IsInst(L_12, InitializedTypeInfo(&t290_TI))))
		{
			goto IL_0062;
		}
	}
	{
		t816  L_13 = p0;
		t29 * L_14 = Box(InitializedTypeInfo(&t816_TI), &L_13);
		t816  L_15 = p1;
		t29 * L_16 = Box(InitializedTypeInfo(&t816_TI), &L_15);
		int32_t L_17 = (int32_t)InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(&m9672_MI, ((t29 *)Castclass(L_14, InitializedTypeInfo(&t290_TI))), L_16);
		return L_17;
	}

IL_0062:
	{
		t305 * L_18 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_18, (t7*) &_stringLiteral1162, &m1935_MI);
		il2cpp_codegen_raise_exception(L_18);
	}
}
// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer<System.TimeSpan>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19467_GM;
MethodInfo m19467_MI = 
{
	".ctor", (methodPointerType)&m19467, &t3505_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19467_GM};
extern Il2CppType t816_0_0_0;
extern Il2CppType t816_0_0_0;
static ParameterInfo t3505_m19468_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t816_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t816_t816 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19468_GM;
MethodInfo m19468_MI = 
{
	"Compare", (methodPointerType)&m19468, &t3505_TI, &t44_0_0_0, RuntimeInvoker_t44_t816_t816, t3505_m19468_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19468_GM};
static MethodInfo* t3505_MIs[] =
{
	&m19467_MI,
	&m19468_MI,
	NULL
};
static MethodInfo* t3505_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m19468_MI,
	&m19465_MI,
	&m19468_MI,
};
static Il2CppInterfaceOffsetPair t3505_IOs[] = 
{
	{ &t6770_TI, 4},
	{ &t726_TI, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3505_0_0_0;
extern Il2CppType t3505_1_0_0;
struct t3505;
extern Il2CppGenericClass t3505_GC;
extern TypeInfo t1246_TI;
TypeInfo t3505_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t3505_MIs, NULL, NULL, NULL, &t3504_TI, NULL, &t1246_TI, &t3505_TI, NULL, t3505_VT, &EmptyCustomAttributesCache, &t3505_TI, &t3505_0_0_0, &t3505_1_0_0, t3505_IOs, &t3505_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3505), 0, -1, 0, 0, -1, 1048835, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 7, 0, 2};
#include "t2098.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2098_TI;
#include "t2098MD.h"

extern TypeInfo t2100_TI;
#include "t3506MD.h"
extern MethodInfo m19471_MI;
extern MethodInfo m33749_MI;


extern MethodInfo m10269_MI;
 void m10269 (t2098 * __this, MethodInfo* method){
	{
		m19471(__this, &m19471_MI);
		return;
	}
}
extern MethodInfo m19469_MI;
 int32_t m19469 (t2098 * __this, t816  p0, MethodInfo* method){
	{
		t816  L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t816_TI), &L_0);
		if (L_1)
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}

IL_000a:
	{
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1322_MI, Box(InitializedTypeInfo(&t816_TI), &(*(&p0))));
		return L_2;
	}
}
extern MethodInfo m19470_MI;
 bool m19470 (t2098 * __this, t816  p0, t816  p1, MethodInfo* method){
	{
		t816  L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t816_TI), &L_0);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		t816  L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t816_TI), &L_2);
		return ((((t29 *)L_3) == ((t29 *)NULL))? 1 : 0);
	}

IL_0012:
	{
		bool L_4 = (bool)InterfaceFuncInvoker1< bool, t816  >::Invoke(&m33749_MI, Box(InitializedTypeInfo(&t816_TI), &(*(&p0))), p1);
		return L_4;
	}
}
// Metadata Definition System.Collections.Generic.GenericEqualityComparer`1<System.TimeSpan>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m10269_GM;
MethodInfo m10269_MI = 
{
	".ctor", (methodPointerType)&m10269, &t2098_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m10269_GM};
extern Il2CppType t816_0_0_0;
static ParameterInfo t2098_m19469_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t816 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19469_GM;
MethodInfo m19469_MI = 
{
	"GetHashCode", (methodPointerType)&m19469, &t2098_TI, &t44_0_0_0, RuntimeInvoker_t44_t816, t2098_m19469_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19469_GM};
extern Il2CppType t816_0_0_0;
extern Il2CppType t816_0_0_0;
static ParameterInfo t2098_m19470_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t816_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t816_t816 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19470_GM;
MethodInfo m19470_MI = 
{
	"Equals", (methodPointerType)&m19470, &t2098_TI, &t40_0_0_0, RuntimeInvoker_t40_t816_t816, t2098_m19470_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19470_GM};
static MethodInfo* t2098_MIs[] =
{
	&m10269_MI,
	&m19469_MI,
	&m19470_MI,
	NULL
};
extern MethodInfo m19474_MI;
extern MethodInfo m19473_MI;
static MethodInfo* t2098_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m19470_MI,
	&m19469_MI,
	&m19474_MI,
	&m19473_MI,
	&m19469_MI,
	&m19470_MI,
};
extern TypeInfo t6771_TI;
extern TypeInfo t734_TI;
static Il2CppInterfaceOffsetPair t2098_IOs[] = 
{
	{ &t6771_TI, 4},
	{ &t734_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2098_0_0_0;
extern Il2CppType t2098_1_0_0;
extern TypeInfo t3506_TI;
struct t2098;
extern Il2CppGenericClass t2098_GC;
TypeInfo t2098_TI = 
{
	&g_mscorlib_dll_Image, NULL, "GenericEqualityComparer`1", "System.Collections.Generic", t2098_MIs, NULL, NULL, NULL, &t3506_TI, NULL, NULL, &t2098_TI, NULL, t2098_VT, &EmptyCustomAttributesCache, &t2098_TI, &t2098_0_0_0, &t2098_1_0_0, t2098_IOs, &t2098_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2098), 0, -1, 0, 0, -1, 1057024, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#include "t3506.h"
#ifndef _MSC_VER
#else
#endif

#include "t1257.h"
#include "t3507.h"
extern TypeInfo t1257_TI;
extern TypeInfo t3507_TI;
#include "t3507MD.h"
extern Il2CppType t2100_0_0_0;
extern Il2CppType t1257_0_0_0;
extern MethodInfo m19476_MI;
extern MethodInfo m34249_MI;
extern MethodInfo m34250_MI;


 void m19471 (t3506 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m19472_MI;
 void m19472 (t29 * __this, MethodInfo* method){
	t3507 * L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer = (t3507 *)il2cpp_codegen_object_new(InitializedTypeInfo(&t3507_TI));
	m19476(L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer, &m19476_MI);
	((t3506_SFs*)InitializedTypeInfo(&t3506_TI)->static_fields)->f0 = L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer;
}
 int32_t m19473 (t3506 * __this, t29 * p0, MethodInfo* method){
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker1< int32_t, t816  >::Invoke(&m34249_MI, __this, ((*(t816 *)((t816 *)UnBox (p0, InitializedTypeInfo(&t816_TI))))));
		return L_0;
	}
}
 bool m19474 (t3506 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	{
		bool L_0 = (bool)VirtFuncInvoker2< bool, t816 , t816  >::Invoke(&m34250_MI, __this, ((*(t816 *)((t816 *)UnBox (p0, InitializedTypeInfo(&t816_TI))))), ((*(t816 *)((t816 *)UnBox (p1, InitializedTypeInfo(&t816_TI))))));
		return L_0;
	}
}
extern MethodInfo m19475_MI;
 t3506 * m19475 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t3506_TI));
		return (((t3506_SFs*)InitializedTypeInfo(&t3506_TI)->static_fields)->f0);
	}
}
// Metadata Definition System.Collections.Generic.EqualityComparer`1<System.TimeSpan>
extern Il2CppType t3506_0_0_49;
FieldInfo t3506_f0_FieldInfo = 
{
	"_default", &t3506_0_0_49, &t3506_TI, offsetof(t3506_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3506_FIs[] =
{
	&t3506_f0_FieldInfo,
	NULL
};
static PropertyInfo t3506____Default_PropertyInfo = 
{
	&t3506_TI, "Default", &m19475_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3506_PIs[] =
{
	&t3506____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19471_GM;
MethodInfo m19471_MI = 
{
	".ctor", (methodPointerType)&m19471, &t3506_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19471_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19472_GM;
MethodInfo m19472_MI = 
{
	".cctor", (methodPointerType)&m19472, &t3506_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19472_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3506_m19473_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19473_GM;
MethodInfo m19473_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m19473, &t3506_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t3506_m19473_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19473_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3506_m19474_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19474_GM;
MethodInfo m19474_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m19474, &t3506_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t3506_m19474_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19474_GM};
extern Il2CppType t816_0_0_0;
static ParameterInfo t3506_m34249_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t816 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34249_GM;
MethodInfo m34249_MI = 
{
	"GetHashCode", NULL, &t3506_TI, &t44_0_0_0, RuntimeInvoker_t44_t816, t3506_m34249_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34249_GM};
extern Il2CppType t816_0_0_0;
extern Il2CppType t816_0_0_0;
static ParameterInfo t3506_m34250_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t816_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t816_t816 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34250_GM;
MethodInfo m34250_MI = 
{
	"Equals", NULL, &t3506_TI, &t40_0_0_0, RuntimeInvoker_t40_t816_t816, t3506_m34250_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34250_GM};
extern Il2CppType t3506_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19475_GM;
MethodInfo m19475_MI = 
{
	"get_Default", (methodPointerType)&m19475, &t3506_TI, &t3506_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19475_GM};
static MethodInfo* t3506_MIs[] =
{
	&m19471_MI,
	&m19472_MI,
	&m19473_MI,
	&m19474_MI,
	&m34249_MI,
	&m34250_MI,
	&m19475_MI,
	NULL
};
static MethodInfo* t3506_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m34250_MI,
	&m34249_MI,
	&m19474_MI,
	&m19473_MI,
	NULL,
	NULL,
};
static TypeInfo* t3506_ITIs[] = 
{
	&t6771_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t3506_IOs[] = 
{
	{ &t6771_TI, 4},
	{ &t734_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3506_0_0_0;
extern Il2CppType t3506_1_0_0;
struct t3506;
extern Il2CppGenericClass t3506_GC;
TypeInfo t3506_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t3506_MIs, t3506_PIs, t3506_FIs, NULL, &t29_TI, NULL, NULL, &t3506_TI, t3506_ITIs, t3506_VT, &EmptyCustomAttributesCache, &t3506_TI, &t3506_0_0_0, &t3506_1_0_0, t3506_IOs, &t3506_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3506), 0, -1, sizeof(t3506_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<System.TimeSpan>
extern Il2CppType t816_0_0_0;
extern Il2CppType t816_0_0_0;
static ParameterInfo t6771_m34251_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t816_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t816_t816 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34251_GM;
MethodInfo m34251_MI = 
{
	"Equals", NULL, &t6771_TI, &t40_0_0_0, RuntimeInvoker_t40_t816_t816, t6771_m34251_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34251_GM};
extern Il2CppType t816_0_0_0;
static ParameterInfo t6771_m34252_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t816 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34252_GM;
MethodInfo m34252_MI = 
{
	"GetHashCode", NULL, &t6771_TI, &t44_0_0_0, RuntimeInvoker_t44_t816, t6771_m34252_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34252_GM};
static MethodInfo* t6771_MIs[] =
{
	&m34251_MI,
	&m34252_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6771_0_0_0;
extern Il2CppType t6771_1_0_0;
struct t6771;
extern Il2CppGenericClass t6771_GC;
TypeInfo t6771_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t6771_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6771_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6771_TI, &t6771_0_0_0, &t6771_1_0_0, NULL, &t6771_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



 void m19476 (t3507 * __this, MethodInfo* method){
	{
		m19471(__this, &m19471_MI);
		return;
	}
}
extern MethodInfo m19477_MI;
 int32_t m19477 (t3507 * __this, t816  p0, MethodInfo* method){
	{
		t816  L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t816_TI), &L_0);
		if (L_1)
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}

IL_000a:
	{
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1322_MI, Box(InitializedTypeInfo(&t816_TI), &(*(&p0))));
		return L_2;
	}
}
extern MethodInfo m19478_MI;
 bool m19478 (t3507 * __this, t816  p0, t816  p1, MethodInfo* method){
	{
		t816  L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t816_TI), &L_0);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		t816  L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t816_TI), &L_2);
		return ((((t29 *)L_3) == ((t29 *)NULL))? 1 : 0);
	}

IL_0012:
	{
		t816  L_4 = p1;
		t29 * L_5 = Box(InitializedTypeInfo(&t816_TI), &L_4);
		bool L_6 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t816_TI), &(*(&p0))), L_5);
		return L_6;
	}
}
// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19476_GM;
MethodInfo m19476_MI = 
{
	".ctor", (methodPointerType)&m19476, &t3507_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19476_GM};
extern Il2CppType t816_0_0_0;
static ParameterInfo t3507_m19477_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t816 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19477_GM;
MethodInfo m19477_MI = 
{
	"GetHashCode", (methodPointerType)&m19477, &t3507_TI, &t44_0_0_0, RuntimeInvoker_t44_t816, t3507_m19477_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19477_GM};
extern Il2CppType t816_0_0_0;
extern Il2CppType t816_0_0_0;
static ParameterInfo t3507_m19478_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t816_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t816_t816 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19478_GM;
MethodInfo m19478_MI = 
{
	"Equals", (methodPointerType)&m19478, &t3507_TI, &t40_0_0_0, RuntimeInvoker_t40_t816_t816, t3507_m19478_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m19478_GM};
static MethodInfo* t3507_MIs[] =
{
	&m19476_MI,
	&m19477_MI,
	&m19478_MI,
	NULL
};
static MethodInfo* t3507_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m19478_MI,
	&m19477_MI,
	&m19474_MI,
	&m19473_MI,
	&m19477_MI,
	&m19478_MI,
};
static Il2CppInterfaceOffsetPair t3507_IOs[] = 
{
	{ &t6771_TI, 4},
	{ &t734_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3507_0_0_0;
extern Il2CppType t3507_1_0_0;
struct t3507;
extern Il2CppGenericClass t3507_GC;
extern TypeInfo t1256_TI;
TypeInfo t3507_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t3507_MIs, NULL, NULL, NULL, &t3506_TI, NULL, &t1256_TI, &t3507_TI, NULL, t3507_VT, &EmptyCustomAttributesCache, &t3507_TI, &t3507_0_0_0, &t3507_1_0_0, t3507_IOs, &t3507_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3507), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5053_TI;

#include "t1127.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.TypeCode>
extern MethodInfo m34253_MI;
static PropertyInfo t5053____Current_PropertyInfo = 
{
	&t5053_TI, "Current", &m34253_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5053_PIs[] =
{
	&t5053____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1127_0_0_0;
extern void* RuntimeInvoker_t1127 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34253_GM;
MethodInfo m34253_MI = 
{
	"get_Current", NULL, &t5053_TI, &t1127_0_0_0, RuntimeInvoker_t1127, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34253_GM};
static MethodInfo* t5053_MIs[] =
{
	&m34253_MI,
	NULL
};
static TypeInfo* t5053_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5053_0_0_0;
extern Il2CppType t5053_1_0_0;
struct t5053;
extern Il2CppGenericClass t5053_GC;
TypeInfo t5053_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t5053_MIs, t5053_PIs, NULL, NULL, NULL, NULL, NULL, &t5053_TI, t5053_ITIs, NULL, &EmptyCustomAttributesCache, &t5053_TI, &t5053_0_0_0, &t5053_1_0_0, NULL, &t5053_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3508.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3508_TI;
#include "t3508MD.h"

extern TypeInfo t1127_TI;
extern MethodInfo m19483_MI;
extern MethodInfo m26231_MI;
struct t20;
 int32_t m26231 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m19479_MI;
 void m19479 (t3508 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19480_MI;
 t29 * m19480 (t3508 * __this, MethodInfo* method){
	{
		int32_t L_0 = m19483(__this, &m19483_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1127_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m19481_MI;
 void m19481 (t3508 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19482_MI;
 bool m19482 (t3508 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m19483 (t3508 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m26231(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m26231_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.TypeCode>
extern Il2CppType t20_0_0_1;
FieldInfo t3508_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3508_TI, offsetof(t3508, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3508_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3508_TI, offsetof(t3508, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3508_FIs[] =
{
	&t3508_f0_FieldInfo,
	&t3508_f1_FieldInfo,
	NULL
};
static PropertyInfo t3508____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3508_TI, "System.Collections.IEnumerator.Current", &m19480_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3508____Current_PropertyInfo = 
{
	&t3508_TI, "Current", &m19483_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3508_PIs[] =
{
	&t3508____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3508____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3508_m19479_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19479_GM;
MethodInfo m19479_MI = 
{
	".ctor", (methodPointerType)&m19479, &t3508_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3508_m19479_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19479_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19480_GM;
MethodInfo m19480_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m19480, &t3508_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19480_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19481_GM;
MethodInfo m19481_MI = 
{
	"Dispose", (methodPointerType)&m19481, &t3508_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19481_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19482_GM;
MethodInfo m19482_MI = 
{
	"MoveNext", (methodPointerType)&m19482, &t3508_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19482_GM};
extern Il2CppType t1127_0_0_0;
extern void* RuntimeInvoker_t1127 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19483_GM;
MethodInfo m19483_MI = 
{
	"get_Current", (methodPointerType)&m19483, &t3508_TI, &t1127_0_0_0, RuntimeInvoker_t1127, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19483_GM};
static MethodInfo* t3508_MIs[] =
{
	&m19479_MI,
	&m19480_MI,
	&m19481_MI,
	&m19482_MI,
	&m19483_MI,
	NULL
};
static MethodInfo* t3508_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19480_MI,
	&m19482_MI,
	&m19481_MI,
	&m19483_MI,
};
static TypeInfo* t3508_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t5053_TI,
};
static Il2CppInterfaceOffsetPair t3508_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t5053_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3508_0_0_0;
extern Il2CppType t3508_1_0_0;
extern Il2CppGenericClass t3508_GC;
TypeInfo t3508_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3508_MIs, t3508_PIs, t3508_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3508_TI, t3508_ITIs, t3508_VT, &EmptyCustomAttributesCache, &t3508_TI, &t3508_0_0_0, &t3508_1_0_0, t3508_IOs, &t3508_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3508)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6621_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.TypeCode>
extern MethodInfo m34254_MI;
static PropertyInfo t6621____Count_PropertyInfo = 
{
	&t6621_TI, "Count", &m34254_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m34255_MI;
static PropertyInfo t6621____IsReadOnly_PropertyInfo = 
{
	&t6621_TI, "IsReadOnly", &m34255_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6621_PIs[] =
{
	&t6621____Count_PropertyInfo,
	&t6621____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34254_GM;
MethodInfo m34254_MI = 
{
	"get_Count", NULL, &t6621_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34254_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34255_GM;
MethodInfo m34255_MI = 
{
	"get_IsReadOnly", NULL, &t6621_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34255_GM};
extern Il2CppType t1127_0_0_0;
extern Il2CppType t1127_0_0_0;
static ParameterInfo t6621_m34256_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1127_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34256_GM;
MethodInfo m34256_MI = 
{
	"Add", NULL, &t6621_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6621_m34256_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34256_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34257_GM;
MethodInfo m34257_MI = 
{
	"Clear", NULL, &t6621_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34257_GM};
extern Il2CppType t1127_0_0_0;
static ParameterInfo t6621_m34258_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1127_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34258_GM;
MethodInfo m34258_MI = 
{
	"Contains", NULL, &t6621_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6621_m34258_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34258_GM};
extern Il2CppType t3726_0_0_0;
extern Il2CppType t3726_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6621_m34259_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3726_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34259_GM;
MethodInfo m34259_MI = 
{
	"CopyTo", NULL, &t6621_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6621_m34259_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34259_GM};
extern Il2CppType t1127_0_0_0;
static ParameterInfo t6621_m34260_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1127_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34260_GM;
MethodInfo m34260_MI = 
{
	"Remove", NULL, &t6621_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6621_m34260_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34260_GM};
static MethodInfo* t6621_MIs[] =
{
	&m34254_MI,
	&m34255_MI,
	&m34256_MI,
	&m34257_MI,
	&m34258_MI,
	&m34259_MI,
	&m34260_MI,
	NULL
};
extern TypeInfo t6623_TI;
static TypeInfo* t6621_ITIs[] = 
{
	&t603_TI,
	&t6623_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6621_0_0_0;
extern Il2CppType t6621_1_0_0;
struct t6621;
extern Il2CppGenericClass t6621_GC;
TypeInfo t6621_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6621_MIs, t6621_PIs, NULL, NULL, NULL, NULL, NULL, &t6621_TI, t6621_ITIs, NULL, &EmptyCustomAttributesCache, &t6621_TI, &t6621_0_0_0, &t6621_1_0_0, NULL, &t6621_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.TypeCode>
extern Il2CppType t5053_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34261_GM;
MethodInfo m34261_MI = 
{
	"GetEnumerator", NULL, &t6623_TI, &t5053_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34261_GM};
static MethodInfo* t6623_MIs[] =
{
	&m34261_MI,
	NULL
};
static TypeInfo* t6623_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6623_0_0_0;
extern Il2CppType t6623_1_0_0;
struct t6623;
extern Il2CppGenericClass t6623_GC;
TypeInfo t6623_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6623_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6623_TI, t6623_ITIs, NULL, &EmptyCustomAttributesCache, &t6623_TI, &t6623_0_0_0, &t6623_1_0_0, NULL, &t6623_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6622_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.TypeCode>
extern MethodInfo m34262_MI;
extern MethodInfo m34263_MI;
static PropertyInfo t6622____Item_PropertyInfo = 
{
	&t6622_TI, "Item", &m34262_MI, &m34263_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6622_PIs[] =
{
	&t6622____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1127_0_0_0;
static ParameterInfo t6622_m34264_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1127_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34264_GM;
MethodInfo m34264_MI = 
{
	"IndexOf", NULL, &t6622_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6622_m34264_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34264_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1127_0_0_0;
static ParameterInfo t6622_m34265_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1127_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34265_GM;
MethodInfo m34265_MI = 
{
	"Insert", NULL, &t6622_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6622_m34265_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34265_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6622_m34266_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34266_GM;
MethodInfo m34266_MI = 
{
	"RemoveAt", NULL, &t6622_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6622_m34266_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34266_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6622_m34262_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1127_0_0_0;
extern void* RuntimeInvoker_t1127_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34262_GM;
MethodInfo m34262_MI = 
{
	"get_Item", NULL, &t6622_TI, &t1127_0_0_0, RuntimeInvoker_t1127_t44, t6622_m34262_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34262_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1127_0_0_0;
static ParameterInfo t6622_m34263_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1127_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34263_GM;
MethodInfo m34263_MI = 
{
	"set_Item", NULL, &t6622_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6622_m34263_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34263_GM};
static MethodInfo* t6622_MIs[] =
{
	&m34264_MI,
	&m34265_MI,
	&m34266_MI,
	&m34262_MI,
	&m34263_MI,
	NULL
};
static TypeInfo* t6622_ITIs[] = 
{
	&t603_TI,
	&t6621_TI,
	&t6623_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6622_0_0_0;
extern Il2CppType t6622_1_0_0;
struct t6622;
extern Il2CppGenericClass t6622_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6622_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6622_MIs, t6622_PIs, NULL, NULL, NULL, NULL, NULL, &t6622_TI, t6622_ITIs, NULL, &t1908__CustomAttributeCache, &t6622_TI, &t6622_0_0_0, &t6622_1_0_0, NULL, &t6622_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5055_TI;

#include "t1677.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.UnitySerializationHolder/UnityType>
extern MethodInfo m34267_MI;
static PropertyInfo t5055____Current_PropertyInfo = 
{
	&t5055_TI, "Current", &m34267_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5055_PIs[] =
{
	&t5055____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1677_0_0_0;
extern void* RuntimeInvoker_t1677 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34267_GM;
MethodInfo m34267_MI = 
{
	"get_Current", NULL, &t5055_TI, &t1677_0_0_0, RuntimeInvoker_t1677, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34267_GM};
static MethodInfo* t5055_MIs[] =
{
	&m34267_MI,
	NULL
};
static TypeInfo* t5055_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5055_0_0_0;
extern Il2CppType t5055_1_0_0;
struct t5055;
extern Il2CppGenericClass t5055_GC;
TypeInfo t5055_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t5055_MIs, t5055_PIs, NULL, NULL, NULL, NULL, NULL, &t5055_TI, t5055_ITIs, NULL, &EmptyCustomAttributesCache, &t5055_TI, &t5055_0_0_0, &t5055_1_0_0, NULL, &t5055_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3509.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3509_TI;
#include "t3509MD.h"

extern TypeInfo t1677_TI;
extern MethodInfo m19488_MI;
extern MethodInfo m26242_MI;
struct t20;
 uint8_t m26242 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m19484_MI;
 void m19484 (t3509 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19485_MI;
 t29 * m19485 (t3509 * __this, MethodInfo* method){
	{
		uint8_t L_0 = m19488(__this, &m19488_MI);
		uint8_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1677_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m19486_MI;
 void m19486 (t3509 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19487_MI;
 bool m19487 (t3509 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 uint8_t m19488 (t3509 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		uint8_t L_8 = m26242(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m26242_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.UnitySerializationHolder/UnityType>
extern Il2CppType t20_0_0_1;
FieldInfo t3509_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3509_TI, offsetof(t3509, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3509_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3509_TI, offsetof(t3509, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3509_FIs[] =
{
	&t3509_f0_FieldInfo,
	&t3509_f1_FieldInfo,
	NULL
};
static PropertyInfo t3509____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3509_TI, "System.Collections.IEnumerator.Current", &m19485_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3509____Current_PropertyInfo = 
{
	&t3509_TI, "Current", &m19488_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3509_PIs[] =
{
	&t3509____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3509____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3509_m19484_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19484_GM;
MethodInfo m19484_MI = 
{
	".ctor", (methodPointerType)&m19484, &t3509_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3509_m19484_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19484_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19485_GM;
MethodInfo m19485_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m19485, &t3509_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19485_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19486_GM;
MethodInfo m19486_MI = 
{
	"Dispose", (methodPointerType)&m19486, &t3509_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19486_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19487_GM;
MethodInfo m19487_MI = 
{
	"MoveNext", (methodPointerType)&m19487, &t3509_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19487_GM};
extern Il2CppType t1677_0_0_0;
extern void* RuntimeInvoker_t1677 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19488_GM;
MethodInfo m19488_MI = 
{
	"get_Current", (methodPointerType)&m19488, &t3509_TI, &t1677_0_0_0, RuntimeInvoker_t1677, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19488_GM};
static MethodInfo* t3509_MIs[] =
{
	&m19484_MI,
	&m19485_MI,
	&m19486_MI,
	&m19487_MI,
	&m19488_MI,
	NULL
};
static MethodInfo* t3509_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19485_MI,
	&m19487_MI,
	&m19486_MI,
	&m19488_MI,
};
static TypeInfo* t3509_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t5055_TI,
};
static Il2CppInterfaceOffsetPair t3509_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t5055_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3509_0_0_0;
extern Il2CppType t3509_1_0_0;
extern Il2CppGenericClass t3509_GC;
TypeInfo t3509_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3509_MIs, t3509_PIs, t3509_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3509_TI, t3509_ITIs, t3509_VT, &EmptyCustomAttributesCache, &t3509_TI, &t3509_0_0_0, &t3509_1_0_0, t3509_IOs, &t3509_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3509)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6624_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.UnitySerializationHolder/UnityType>
extern MethodInfo m34268_MI;
static PropertyInfo t6624____Count_PropertyInfo = 
{
	&t6624_TI, "Count", &m34268_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m34269_MI;
static PropertyInfo t6624____IsReadOnly_PropertyInfo = 
{
	&t6624_TI, "IsReadOnly", &m34269_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6624_PIs[] =
{
	&t6624____Count_PropertyInfo,
	&t6624____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34268_GM;
MethodInfo m34268_MI = 
{
	"get_Count", NULL, &t6624_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34268_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34269_GM;
MethodInfo m34269_MI = 
{
	"get_IsReadOnly", NULL, &t6624_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34269_GM};
extern Il2CppType t1677_0_0_0;
extern Il2CppType t1677_0_0_0;
static ParameterInfo t6624_m34270_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1677_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34270_GM;
MethodInfo m34270_MI = 
{
	"Add", NULL, &t6624_TI, &t21_0_0_0, RuntimeInvoker_t21_t348, t6624_m34270_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34270_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34271_GM;
MethodInfo m34271_MI = 
{
	"Clear", NULL, &t6624_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34271_GM};
extern Il2CppType t1677_0_0_0;
static ParameterInfo t6624_m34272_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1677_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34272_GM;
MethodInfo m34272_MI = 
{
	"Contains", NULL, &t6624_TI, &t40_0_0_0, RuntimeInvoker_t40_t348, t6624_m34272_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34272_GM};
extern Il2CppType t3727_0_0_0;
extern Il2CppType t3727_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6624_m34273_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3727_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34273_GM;
MethodInfo m34273_MI = 
{
	"CopyTo", NULL, &t6624_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6624_m34273_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34273_GM};
extern Il2CppType t1677_0_0_0;
static ParameterInfo t6624_m34274_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1677_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34274_GM;
MethodInfo m34274_MI = 
{
	"Remove", NULL, &t6624_TI, &t40_0_0_0, RuntimeInvoker_t40_t348, t6624_m34274_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34274_GM};
static MethodInfo* t6624_MIs[] =
{
	&m34268_MI,
	&m34269_MI,
	&m34270_MI,
	&m34271_MI,
	&m34272_MI,
	&m34273_MI,
	&m34274_MI,
	NULL
};
extern TypeInfo t6626_TI;
static TypeInfo* t6624_ITIs[] = 
{
	&t603_TI,
	&t6626_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6624_0_0_0;
extern Il2CppType t6624_1_0_0;
struct t6624;
extern Il2CppGenericClass t6624_GC;
TypeInfo t6624_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6624_MIs, t6624_PIs, NULL, NULL, NULL, NULL, NULL, &t6624_TI, t6624_ITIs, NULL, &EmptyCustomAttributesCache, &t6624_TI, &t6624_0_0_0, &t6624_1_0_0, NULL, &t6624_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.UnitySerializationHolder/UnityType>
extern Il2CppType t5055_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34275_GM;
MethodInfo m34275_MI = 
{
	"GetEnumerator", NULL, &t6626_TI, &t5055_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m34275_GM};
static MethodInfo* t6626_MIs[] =
{
	&m34275_MI,
	NULL
};
static TypeInfo* t6626_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6626_0_0_0;
extern Il2CppType t6626_1_0_0;
struct t6626;
extern Il2CppGenericClass t6626_GC;
TypeInfo t6626_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6626_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6626_TI, t6626_ITIs, NULL, &EmptyCustomAttributesCache, &t6626_TI, &t6626_0_0_0, &t6626_1_0_0, NULL, &t6626_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6625_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.UnitySerializationHolder/UnityType>
extern MethodInfo m34276_MI;
extern MethodInfo m34277_MI;
static PropertyInfo t6625____Item_PropertyInfo = 
{
	&t6625_TI, "Item", &m34276_MI, &m34277_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6625_PIs[] =
{
	&t6625____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1677_0_0_0;
static ParameterInfo t6625_m34278_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1677_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34278_GM;
MethodInfo m34278_MI = 
{
	"IndexOf", NULL, &t6625_TI, &t44_0_0_0, RuntimeInvoker_t44_t348, t6625_m34278_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34278_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1677_0_0_0;
static ParameterInfo t6625_m34279_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1677_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34279_GM;
MethodInfo m34279_MI = 
{
	"Insert", NULL, &t6625_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t348, t6625_m34279_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34279_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6625_m34280_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34280_GM;
MethodInfo m34280_MI = 
{
	"RemoveAt", NULL, &t6625_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6625_m34280_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34280_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6625_m34276_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1677_0_0_0;
extern void* RuntimeInvoker_t1677_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34276_GM;
MethodInfo m34276_MI = 
{
	"get_Item", NULL, &t6625_TI, &t1677_0_0_0, RuntimeInvoker_t1677_t44, t6625_m34276_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34276_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1677_0_0_0;
static ParameterInfo t6625_m34277_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1677_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34277_GM;
MethodInfo m34277_MI = 
{
	"set_Item", NULL, &t6625_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t348, t6625_m34277_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m34277_GM};
static MethodInfo* t6625_MIs[] =
{
	&m34278_MI,
	&m34279_MI,
	&m34280_MI,
	&m34276_MI,
	&m34277_MI,
	NULL
};
static TypeInfo* t6625_ITIs[] = 
{
	&t603_TI,
	&t6624_TI,
	&t6626_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6625_0_0_0;
extern Il2CppType t6625_1_0_0;
struct t6625;
extern Il2CppGenericClass t6625_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6625_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6625_MIs, t6625_PIs, NULL, NULL, NULL, NULL, NULL, &t6625_TI, t6625_ITIs, NULL, &t1908__CustomAttributeCache, &t6625_TI, &t6625_0_0_0, &t6625_1_0_0, NULL, &t6625_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2103_TI;

#include "t760.h"


// Metadata Definition System.IComparable`1<System.Version>
extern Il2CppType t760_0_0_0;
extern Il2CppType t760_0_0_0;
static ParameterInfo t2103_m34281_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t760_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34281_GM;
MethodInfo m34281_MI = 
{
	"CompareTo", NULL, &t2103_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2103_m34281_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34281_GM};
static MethodInfo* t2103_MIs[] =
{
	&m34281_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2103_0_0_0;
extern Il2CppType t2103_1_0_0;
struct t2103;
extern Il2CppGenericClass t2103_GC;
TypeInfo t2103_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t2103_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2103_TI, NULL, NULL, &EmptyCustomAttributesCache, &t2103_TI, &t2103_0_0_0, &t2103_1_0_0, NULL, &t2103_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2104_TI;



// Metadata Definition System.IEquatable`1<System.Version>
extern Il2CppType t760_0_0_0;
static ParameterInfo t2104_m34282_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t760_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m34282_GM;
MethodInfo m34282_MI = 
{
	"Equals", NULL, &t2104_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2104_m34282_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m34282_GM};
static MethodInfo* t2104_MIs[] =
{
	&m34282_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2104_0_0_0;
extern Il2CppType t2104_1_0_0;
struct t2104;
extern Il2CppGenericClass t2104_GC;
TypeInfo t2104_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t2104_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2104_TI, NULL, NULL, &EmptyCustomAttributesCache, &t2104_TI, &t2104_0_0_0, &t2104_1_0_0, NULL, &t2104_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
