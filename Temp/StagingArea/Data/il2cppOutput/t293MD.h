﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t293;
struct t7;
struct t41;
struct t41_marshaled;
struct t295;
struct t29;

 void m2501 (t29 * __this, int32_t p0, t7* p1, t41 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2502 (t29 * __this, t295 * p0, t41 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2503 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1296 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1686 (t29 * __this, t29 * p0, t41 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2504 (t29 * __this, t295 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1531 (t29 * __this, t295 * p0, t41 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2505 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1904 (t29 * __this, t29 * p0, t41 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
