﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t788;
struct t778;
struct t781;
struct t7;
#include "t790.h"

 void m3293 (t788 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3294 (t788 * __this, t778 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3295 (t788 * __this, bool p0, bool p1, int32_t p2, bool p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3296 (t788 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3297 (t788 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3298 (t788 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3299 (t788 * __this, t778 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3300 (t788 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m3301 (t788 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3302 (t788 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
