﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1372;
struct t29;
struct t42;
struct t66;
struct t67;
#include "t35.h"

 void m9624 (t1372 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9625 (t1372 * __this, t42 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m9626 (t1372 * __this, t42 * p0, t29 * p1, t67 * p2, t29 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9627 (t1372 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
