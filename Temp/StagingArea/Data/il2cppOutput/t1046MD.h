﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1046;
struct t29;
struct t745;
struct t762;
struct t7;
struct t66;
struct t67;
#include "t35.h"

 void m5092 (t1046 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t745 * m5093 (t1046 * __this, t762 * p0, t745 * p1, t7* p2, t762 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m5094 (t1046 * __this, t762 * p0, t745 * p1, t7* p2, t762 * p3, t67 * p4, t29 * p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t745 * m5095 (t1046 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
