﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t931;
struct t29;
struct t42;
struct t316;
struct t631;
struct t633;
#include "t630.h"

 t29 * m8828 (t29 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m8829 (t29 * __this, t42 * p0, t316* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m8830 (t29 * __this, t42 * p0, t316* p1, t316* p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m8831 (t29 * __this, t42 * p0, int32_t p1, t631 * p2, t316* p3, t633 * p4, t316* p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m4037 (t29 * __this, t42 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8832 (t29 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8833 (t29 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m8834 (t29 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
