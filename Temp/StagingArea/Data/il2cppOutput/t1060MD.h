﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1060;
struct t745;
struct t945;
struct t1034;
struct t66;
struct t67;
struct t29;
struct t762;
struct t7;
struct t841;
struct t1057;
struct t777;
struct t1062;
struct t781;
#include "t1018.h"
#include "t1021.h"
#include "t1022.h"
#include "t1026.h"
#include "t1064.h"

 void m4904 (t1060 * __this, t1034 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4905 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4906 (t1060 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4907 (t1060 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4908 (t1060 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t745 * m4909 (t1060 * __this, t762 * p0, t745 * p1, t7* p2, t762 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4910 (t1060 * __this, t745 * p0, t841* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1057 * m4911 (t1060 * __this, t945 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t777 * m4912 (t1060 * __this, t745 * p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4913 (t1060 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4914 (t1060 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4915 (t1060 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4916 (t1060 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4917 (t1060 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4918 (t1060 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4919 (t1060 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4920 (t1060 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4921 (t1060 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t745 * m4922 (t1060 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t945 * m4923 (t1060 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4924 (t1060 * __this, t1062 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4925 (t1060 * __this, t1062 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m4926 (t1060 * __this, t781* p0, int32_t p1, int32_t p2, t67 * p3, t29 * p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4927 (t1060 * __this, t1062 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4928 (t1060 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4929 (t1060 * __this, t1062 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4930 (t1060 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m4931 (t1060 * __this, t781* p0, int32_t p1, int32_t p2, t67 * p3, t29 * p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4932 (t1060 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4933 (t1060 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4934 (t1060 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4935 (t1060 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4936 (t1060 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4937 (t1060 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m4938 (t1060 * __this, int64_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4939 (t1060 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4940 (t1060 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4941 (t1060 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4942 (t1060 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4943 (t1060 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4944 (t1060 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m4945 (t1060 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m4946 (t1060 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4947 (t1060 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4948 (t1060 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4949 (t1060 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4950 (t1060 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4951 (t1060 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
