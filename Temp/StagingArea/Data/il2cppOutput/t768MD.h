﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t768;
struct t769;
struct t755;
struct t744;
struct t748;
struct t750;
#include "t766.h"

 void m3235 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3236 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3237 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3238 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t755 * m3239 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t744 * m3240 (t29 * __this, t748 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3241 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
