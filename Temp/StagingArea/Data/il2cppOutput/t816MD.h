﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t816;
struct t29;
struct t7;
#include "t816.h"

 void m9520 (t816 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9521 (t816 * __this, int32_t p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9522 (t816 * __this, int32_t p0, int32_t p1, int32_t p2, int32_t p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9523 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m9524 (t29 * __this, int32_t p0, int32_t p1, int32_t p2, int32_t p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9525 (t816 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9526 (t816 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9527 (t816 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9528 (t816 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9529 (t816 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m9530 (t816 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m9531 (t816 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m9532 (t816 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m9533 (t816 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m9534 (t816 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m9535 (t816 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t816  m9536 (t816 * __this, t816  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9537 (t29 * __this, t816  p0, t816  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9538 (t816 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9539 (t816 * __this, t816  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9540 (t816 * __this, t816  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t816  m9541 (t816 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9542 (t816 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t816  m9543 (t29 * __this, double p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t816  m9544 (t29 * __this, double p0, int64_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9545 (t816 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t816  m9546 (t816 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t816  m9547 (t816 * __this, t816  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m9548 (t816 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t816  m9549 (t29 * __this, t816  p0, t816  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9550 (t29 * __this, t816  p0, t816  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9551 (t29 * __this, t816  p0, t816  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9552 (t29 * __this, t816  p0, t816  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9553 (t29 * __this, t816  p0, t816  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9554 (t29 * __this, t816  p0, t816  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9555 (t29 * __this, t816  p0, t816  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t816  m9556 (t29 * __this, t816  p0, t816  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
