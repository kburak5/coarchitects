﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t542;
struct t7;
#include "t542.h"

 void m2741 (t542 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2742 (t29 * __this, t542  p0, t542  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2743 (t29 * __this, t542  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
