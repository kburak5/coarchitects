﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1566;
struct t733;
struct t7;
struct t781;
#include "t735.h"

 void m8485 (t1566 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8486 (t1566 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8487 (t1566 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8488 (t1566 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m8489 (t1566 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
