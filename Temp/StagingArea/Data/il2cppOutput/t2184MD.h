﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2184;
struct t29;
struct t2186;
struct t20;
struct t136;
struct t316;
struct t346;

 void m10637_gshared (t2184 * __this, t29* p0, MethodInfo* method);
#define m10637(__this, p0, method) (void)m10637_gshared((t2184 *)__this, (t29*)p0, method)
 void m10638_gshared (t2184 * __this, t29 * p0, MethodInfo* method);
#define m10638(__this, p0, method) (void)m10638_gshared((t2184 *)__this, (t29 *)p0, method)
 void m10639_gshared (t2184 * __this, MethodInfo* method);
#define m10639(__this, method) (void)m10639_gshared((t2184 *)__this, method)
 void m10640_gshared (t2184 * __this, int32_t p0, t29 * p1, MethodInfo* method);
#define m10640(__this, p0, p1, method) (void)m10640_gshared((t2184 *)__this, (int32_t)p0, (t29 *)p1, method)
 bool m10641_gshared (t2184 * __this, t29 * p0, MethodInfo* method);
#define m10641(__this, p0, method) (bool)m10641_gshared((t2184 *)__this, (t29 *)p0, method)
 void m10642_gshared (t2184 * __this, int32_t p0, MethodInfo* method);
#define m10642(__this, p0, method) (void)m10642_gshared((t2184 *)__this, (int32_t)p0, method)
 t29 * m10643_gshared (t2184 * __this, int32_t p0, MethodInfo* method);
#define m10643(__this, p0, method) (t29 *)m10643_gshared((t2184 *)__this, (int32_t)p0, method)
 void m10644_gshared (t2184 * __this, int32_t p0, t29 * p1, MethodInfo* method);
#define m10644(__this, p0, p1, method) (void)m10644_gshared((t2184 *)__this, (int32_t)p0, (t29 *)p1, method)
 bool m10645_gshared (t2184 * __this, MethodInfo* method);
#define m10645(__this, method) (bool)m10645_gshared((t2184 *)__this, method)
 void m10646_gshared (t2184 * __this, t20 * p0, int32_t p1, MethodInfo* method);
#define m10646(__this, p0, p1, method) (void)m10646_gshared((t2184 *)__this, (t20 *)p0, (int32_t)p1, method)
 t29 * m10647_gshared (t2184 * __this, MethodInfo* method);
#define m10647(__this, method) (t29 *)m10647_gshared((t2184 *)__this, method)
 int32_t m10648_gshared (t2184 * __this, t29 * p0, MethodInfo* method);
#define m10648(__this, p0, method) (int32_t)m10648_gshared((t2184 *)__this, (t29 *)p0, method)
 void m10649_gshared (t2184 * __this, MethodInfo* method);
#define m10649(__this, method) (void)m10649_gshared((t2184 *)__this, method)
 bool m10650_gshared (t2184 * __this, t29 * p0, MethodInfo* method);
#define m10650(__this, p0, method) (bool)m10650_gshared((t2184 *)__this, (t29 *)p0, method)
 int32_t m10651_gshared (t2184 * __this, t29 * p0, MethodInfo* method);
#define m10651(__this, p0, method) (int32_t)m10651_gshared((t2184 *)__this, (t29 *)p0, method)
 void m10652_gshared (t2184 * __this, int32_t p0, t29 * p1, MethodInfo* method);
#define m10652(__this, p0, p1, method) (void)m10652_gshared((t2184 *)__this, (int32_t)p0, (t29 *)p1, method)
 void m10653_gshared (t2184 * __this, t29 * p0, MethodInfo* method);
#define m10653(__this, p0, method) (void)m10653_gshared((t2184 *)__this, (t29 *)p0, method)
 void m10654_gshared (t2184 * __this, int32_t p0, MethodInfo* method);
#define m10654(__this, p0, method) (void)m10654_gshared((t2184 *)__this, (int32_t)p0, method)
 bool m10655_gshared (t2184 * __this, MethodInfo* method);
#define m10655(__this, method) (bool)m10655_gshared((t2184 *)__this, method)
 t29 * m10656_gshared (t2184 * __this, MethodInfo* method);
#define m10656(__this, method) (t29 *)m10656_gshared((t2184 *)__this, method)
 bool m10657_gshared (t2184 * __this, MethodInfo* method);
#define m10657(__this, method) (bool)m10657_gshared((t2184 *)__this, method)
 bool m10658_gshared (t2184 * __this, MethodInfo* method);
#define m10658(__this, method) (bool)m10658_gshared((t2184 *)__this, method)
 t29 * m10659_gshared (t2184 * __this, int32_t p0, MethodInfo* method);
#define m10659(__this, p0, method) (t29 *)m10659_gshared((t2184 *)__this, (int32_t)p0, method)
 void m10660_gshared (t2184 * __this, int32_t p0, t29 * p1, MethodInfo* method);
#define m10660(__this, p0, p1, method) (void)m10660_gshared((t2184 *)__this, (int32_t)p0, (t29 *)p1, method)
 bool m10661_gshared (t2184 * __this, t29 * p0, MethodInfo* method);
#define m10661(__this, p0, method) (bool)m10661_gshared((t2184 *)__this, (t29 *)p0, method)
 void m10662_gshared (t2184 * __this, t316* p0, int32_t p1, MethodInfo* method);
#define m10662(__this, p0, p1, method) (void)m10662_gshared((t2184 *)__this, (t316*)p0, (int32_t)p1, method)
 t29* m10663_gshared (t2184 * __this, MethodInfo* method);
#define m10663(__this, method) (t29*)m10663_gshared((t2184 *)__this, method)
 int32_t m10664_gshared (t2184 * __this, t29 * p0, MethodInfo* method);
#define m10664(__this, p0, method) (int32_t)m10664_gshared((t2184 *)__this, (t29 *)p0, method)
 int32_t m10665_gshared (t2184 * __this, MethodInfo* method);
#define m10665(__this, method) (int32_t)m10665_gshared((t2184 *)__this, method)
 t29 * m10666_gshared (t2184 * __this, int32_t p0, MethodInfo* method);
#define m10666(__this, p0, method) (t29 *)m10666_gshared((t2184 *)__this, (int32_t)p0, method)
