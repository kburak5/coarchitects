﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3409;
struct t29;
struct t20;
#include "t1154.h"

 void m18915 (t3409 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18916 (t3409 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18917 (t3409 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18918 (t3409 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18919 (t3409 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
