﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t991;
struct t781;

 void m4411 (t991 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4412 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4413 (t991 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4414 (t991 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4415 (t991 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4416 (t991 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4417 (t991 * __this, t781* p0, t781* p1, t781* p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
