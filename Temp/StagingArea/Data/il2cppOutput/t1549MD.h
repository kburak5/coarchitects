﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1549;
struct t781;

 void m8398 (t1549 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8399 (t1549 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m8400 (t1549 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8401 (t1549 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8402 (t1549 * __this, t781* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8403 (t1549 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8404 (t1549 * __this, uint64_t p0, t781* p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
