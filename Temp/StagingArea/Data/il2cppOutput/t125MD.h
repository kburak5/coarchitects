﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t125;
struct t29;
struct t66;
struct t67;
#include "t35.h"
#include "t127.h"

 void m1484 (t125 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m12409 (t125 * __this, t127  p0, t127  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12410 (t125 * __this, t127  p0, t127  p1, t67 * p2, t29 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m12411 (t125 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
