﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2674;
struct t29;
struct t20;
#include "t233.h"

 void m14491 (t2674 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m14492 (t2674 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m14493 (t2674 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m14494 (t2674 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m14495 (t2674 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
