﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3228;
struct t29;
struct t20;
#include "t843.h"

 void m17936 (t3228 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17937 (t3228 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17938 (t3228 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17939 (t3228 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m17940 (t3228 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
