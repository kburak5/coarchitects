﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t558;
struct t29;
struct t557;
struct t34;
struct t316;

 void m2792 (t558 * __this, t29 * p0, t557 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2793 (t558 * __this, t34 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2794 (t558 * __this, t316* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2795 (t558 * __this, t29 * p0, t557 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
