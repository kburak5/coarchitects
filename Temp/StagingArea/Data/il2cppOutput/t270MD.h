﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t270;
struct t29;
struct t271;
struct t66;
struct t67;
#include "t35.h"

 void m2003 (t270 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m2004 (t270 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15176 (t270 * __this, t29 * p0, t67 * p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m15177 (t270 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
