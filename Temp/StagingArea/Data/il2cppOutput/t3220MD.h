﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3220;
struct t29;
struct t20;
#include "t825.h"

 void m17900 (t3220 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17901 (t3220 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17902 (t3220 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17903 (t3220 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17904 (t3220 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
