﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3115;
struct t29;
struct t204;
struct t204_marshaled;
struct t66;
struct t67;
#include "t35.h"
#include "t3106.h"
#include "t552.h"

 void m17218 (t3115 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t3106  m17219 (t3115 * __this, t204 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17220 (t3115 * __this, t204 * p0, int32_t p1, t67 * p2, t29 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t3106  m17221 (t3115 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
