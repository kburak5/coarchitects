﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2508;
struct t29;
struct t20;
struct t2510;
struct t136;
struct t163;
#include "t2511.h"

#include "t2229MD.h"
#define m13246(__this, method) (void)m11064_gshared((t2229 *)__this, method)
#define m13247(__this, method) (bool)m11065_gshared((t2229 *)__this, method)
#define m13248(__this, method) (t29 *)m11066_gshared((t2229 *)__this, method)
#define m13249(__this, p0, p1, method) (void)m11067_gshared((t2229 *)__this, (t20 *)p0, (int32_t)p1, method)
#define m13250(__this, method) (t29*)m11068_gshared((t2229 *)__this, method)
#define m13251(__this, method) (t29 *)m11069_gshared((t2229 *)__this, method)
#define m13252(__this, method) (t163 *)m11070_gshared((t2229 *)__this, method)
#define m13253(__this, method) (t163 *)m11071_gshared((t2229 *)__this, method)
#define m13254(__this, p0, method) (void)m11072_gshared((t2229 *)__this, (t29 *)p0, method)
#define m13255(__this, method) (int32_t)m11073_gshared((t2229 *)__this, method)
 t2511  m13256 (t2508 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
