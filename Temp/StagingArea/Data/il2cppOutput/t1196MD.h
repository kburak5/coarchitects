﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1196;
struct t976;
struct t781;
struct t7;
struct t29;
#include "t1200.h"

 void m6229 (t1196 * __this, int32_t p0, uint32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6230 (t1196 * __this, t1196 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6231 (t1196 * __this, t1196 * p0, uint32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6232 (t1196 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6233 (t1196 * __this, uint32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6234 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t976 * m6235 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1196 * m6236 (t29 * __this, int32_t p0, t976 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1196 * m6237 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6238 (t1196 * __this, t976 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6239 (t1196 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6240 (t1196 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6241 (t1196 * __this, uint32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6242 (t1196 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6243 (t1196 * __this, uint32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6244 (t1196 * __this, uint32_t p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6245 (t1196 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6246 (t1196 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6247 (t1196 * __this, uint32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6248 (t1196 * __this, uint32_t p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6249 (t1196 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6250 (t1196 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6251 (t1196 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6252 (t1196 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6253 (t1196 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1196 * m6254 (t1196 * __this, t1196 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1196 * m6255 (t1196 * __this, t1196 * p0, t1196 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6256 (t1196 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1196 * m6257 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6258 (t1196 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1196 * m6259 (t29 * __this, uint32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1196 * m6260 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1196 * m6261 (t29 * __this, t1196 * p0, t1196 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1196 * m6262 (t29 * __this, t1196 * p0, t1196 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m6263 (t29 * __this, t1196 * p0, uint32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1196 * m6264 (t29 * __this, t1196 * p0, t1196 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1196 * m6265 (t29 * __this, t1196 * p0, t1196 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1196 * m6266 (t29 * __this, t1196 * p0, t1196 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1196 * m6267 (t29 * __this, t1196 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1196 * m6268 (t29 * __this, t1196 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1196 * m6269 (t29 * __this, t1196 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6270 (t29 * __this, t1196 * p0, uint32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6271 (t29 * __this, t1196 * p0, uint32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6272 (t29 * __this, t1196 * p0, t1196 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6273 (t29 * __this, t1196 * p0, t1196 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6274 (t29 * __this, t1196 * p0, t1196 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6275 (t29 * __this, t1196 * p0, t1196 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6276 (t29 * __this, t1196 * p0, t1196 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6277 (t29 * __this, t1196 * p0, t1196 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
