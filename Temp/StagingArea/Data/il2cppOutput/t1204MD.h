﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1204;
struct t781;
struct t783;

 int32_t m6278 (t29 * __this, t781* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m6279 (t29 * __this, t781* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6280 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6281 (t29 * __this, t783 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t783 * m6282 (t29 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t783 * m6283 (t29 * __this, t781* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6284 (t29 * __this, t783 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6285 (t29 * __this, t783 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
