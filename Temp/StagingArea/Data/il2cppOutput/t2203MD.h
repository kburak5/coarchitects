﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2203;
struct t29;
struct t20;

 void m10781 (t2203 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m10782 (t2203 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m10783 (t2203 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m10784 (t2203 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m10785 (t2203 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
