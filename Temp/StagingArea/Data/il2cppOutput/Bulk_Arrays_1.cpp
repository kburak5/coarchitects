﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include "UnityEngine.UI_ArrayTypes.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t2545_TI;


#include "t20.h"

// Metadata Definition UnityEngine.UI.Graphic[]
static MethodInfo* t2545_MIs[] =
{
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m1332_MI;
extern MethodInfo m4200_MI;
extern MethodInfo m5904_MI;
extern MethodInfo m5920_MI;
extern MethodInfo m5921_MI;
extern MethodInfo m4199_MI;
extern MethodInfo m5922_MI;
extern MethodInfo m5923_MI;
extern MethodInfo m5895_MI;
extern MethodInfo m5896_MI;
extern MethodInfo m5897_MI;
extern MethodInfo m5898_MI;
extern MethodInfo m5899_MI;
extern MethodInfo m5900_MI;
extern MethodInfo m5901_MI;
extern MethodInfo m5902_MI;
extern MethodInfo m5903_MI;
extern MethodInfo m4006_MI;
extern MethodInfo m5905_MI;
extern MethodInfo m5906_MI;
extern MethodInfo m20842_MI;
extern MethodInfo m5907_MI;
extern MethodInfo m20843_MI;
extern MethodInfo m20844_MI;
extern MethodInfo m20845_MI;
extern MethodInfo m20846_MI;
extern MethodInfo m20847_MI;
extern MethodInfo m5908_MI;
extern MethodInfo m20841_MI;
extern MethodInfo m20849_MI;
extern MethodInfo m20850_MI;
extern MethodInfo m20737_MI;
extern MethodInfo m20738_MI;
extern MethodInfo m20739_MI;
extern MethodInfo m20740_MI;
extern MethodInfo m20741_MI;
extern MethodInfo m20742_MI;
extern MethodInfo m20736_MI;
extern MethodInfo m20744_MI;
extern MethodInfo m20745_MI;
extern MethodInfo m19777_MI;
extern MethodInfo m19778_MI;
extern MethodInfo m19779_MI;
extern MethodInfo m19780_MI;
extern MethodInfo m19781_MI;
extern MethodInfo m19782_MI;
extern MethodInfo m19776_MI;
extern MethodInfo m19784_MI;
extern MethodInfo m19785_MI;
extern MethodInfo m19513_MI;
extern MethodInfo m19514_MI;
extern MethodInfo m19515_MI;
extern MethodInfo m19516_MI;
extern MethodInfo m19517_MI;
extern MethodInfo m19518_MI;
extern MethodInfo m19512_MI;
extern MethodInfo m19520_MI;
extern MethodInfo m19521_MI;
extern MethodInfo m19524_MI;
extern MethodInfo m19525_MI;
extern MethodInfo m19526_MI;
extern MethodInfo m19527_MI;
extern MethodInfo m19528_MI;
extern MethodInfo m19529_MI;
extern MethodInfo m19523_MI;
extern MethodInfo m19531_MI;
extern MethodInfo m19532_MI;
extern MethodInfo m19535_MI;
extern MethodInfo m19536_MI;
extern MethodInfo m19537_MI;
extern MethodInfo m19538_MI;
extern MethodInfo m19539_MI;
extern MethodInfo m19540_MI;
extern MethodInfo m19534_MI;
extern MethodInfo m19542_MI;
extern MethodInfo m19543_MI;
extern MethodInfo m19546_MI;
extern MethodInfo m19547_MI;
extern MethodInfo m19548_MI;
extern MethodInfo m19549_MI;
extern MethodInfo m19550_MI;
extern MethodInfo m19551_MI;
extern MethodInfo m19545_MI;
extern MethodInfo m19553_MI;
extern MethodInfo m19554_MI;
extern MethodInfo m19494_MI;
extern MethodInfo m19496_MI;
extern MethodInfo m19498_MI;
extern MethodInfo m19499_MI;
extern MethodInfo m19500_MI;
extern MethodInfo m19501_MI;
extern MethodInfo m19490_MI;
extern MethodInfo m19503_MI;
extern MethodInfo m19504_MI;
static MethodInfo* t2545_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20842_MI,
	&m5907_MI,
	&m20843_MI,
	&m20844_MI,
	&m20845_MI,
	&m20846_MI,
	&m20847_MI,
	&m5908_MI,
	&m20841_MI,
	&m20849_MI,
	&m20850_MI,
	&m5905_MI,
	&m5906_MI,
	&m20737_MI,
	&m5907_MI,
	&m20738_MI,
	&m20739_MI,
	&m20740_MI,
	&m20741_MI,
	&m20742_MI,
	&m5908_MI,
	&m20736_MI,
	&m20744_MI,
	&m20745_MI,
	&m5905_MI,
	&m5906_MI,
	&m19777_MI,
	&m5907_MI,
	&m19778_MI,
	&m19779_MI,
	&m19780_MI,
	&m19781_MI,
	&m19782_MI,
	&m5908_MI,
	&m19776_MI,
	&m19784_MI,
	&m19785_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t363_TI;
extern TypeInfo t171_TI;
extern TypeInfo t2547_TI;
extern TypeInfo t2430_TI;
extern TypeInfo t2434_TI;
extern TypeInfo t2431_TI;
extern TypeInfo t5122_TI;
extern TypeInfo t5123_TI;
extern TypeInfo t5124_TI;
extern TypeInfo t5059_TI;
extern TypeInfo t5060_TI;
extern TypeInfo t5061_TI;
extern TypeInfo t5062_TI;
extern TypeInfo t5063_TI;
extern TypeInfo t5064_TI;
extern TypeInfo t2240_TI;
extern TypeInfo t2245_TI;
extern TypeInfo t2241_TI;
extern TypeInfo t5065_TI;
extern TypeInfo t5066_TI;
extern TypeInfo t5067_TI;
extern TypeInfo t2182_TI;
extern TypeInfo t2186_TI;
extern TypeInfo t2183_TI;
static TypeInfo* t2545_ITIs[] = 
{
	&t363_TI,
	&t171_TI,
	&t2547_TI,
	&t2430_TI,
	&t2434_TI,
	&t2431_TI,
	&t5122_TI,
	&t5123_TI,
	&t5124_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
extern TypeInfo t603_TI;
extern TypeInfo t373_TI;
extern TypeInfo t674_TI;
extern TypeInfo t868_TI;
static Il2CppInterfaceOffsetPair t2545_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t363_TI, 21},
	{ &t171_TI, 28},
	{ &t2547_TI, 33},
	{ &t2430_TI, 34},
	{ &t2434_TI, 41},
	{ &t2431_TI, 46},
	{ &t5122_TI, 47},
	{ &t5123_TI, 54},
	{ &t5124_TI, 59},
	{ &t5059_TI, 60},
	{ &t5060_TI, 67},
	{ &t5061_TI, 72},
	{ &t5062_TI, 73},
	{ &t5063_TI, 80},
	{ &t5064_TI, 85},
	{ &t2240_TI, 86},
	{ &t2245_TI, 93},
	{ &t2241_TI, 98},
	{ &t5065_TI, 99},
	{ &t5066_TI, 106},
	{ &t5067_TI, 111},
	{ &t2182_TI, 112},
	{ &t2186_TI, 119},
	{ &t2183_TI, 124},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t2545_0_0_0;
extern Il2CppType t2545_1_0_0;
extern TypeInfo t20_TI;
struct t155;
extern TypeInfo t155_TI;
extern CustomAttributesCache t155__CustomAttributeCache;
extern CustomAttributesCache t155__CustomAttributeCache_m_Material;
extern CustomAttributesCache t155__CustomAttributeCache_m_Color;
extern CustomAttributesCache t155__CustomAttributeCache_U3CU3Ef__am$cacheE;
extern CustomAttributesCache t155__CustomAttributeCache_U3CU3Ef__am$cacheF;
extern CustomAttributesCache t155__CustomAttributeCache_m484;
extern CustomAttributesCache t155__CustomAttributeCache_m485;
TypeInfo t2545_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Graphic[]", "UnityEngine.UI", t2545_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t155_TI, t2545_ITIs, t2545_VT, &EmptyCustomAttributesCache, &t2545_TI, &t2545_0_0_0, &t2545_1_0_0, t2545_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t155 *), -1, sizeof(t2545_SFs), 0, -1, 1048705, 1, false, false, false, false, false, true, false, false, false, true, false, false, 0, 0, 0, 0, 0, 125, 24, 28};
#include "mscorlib_ArrayTypes.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2457_TI;



// Metadata Definition System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>[]
static MethodInfo* t2457_MIs[] =
{
	NULL
};
extern MethodInfo m20868_MI;
extern MethodInfo m20869_MI;
extern MethodInfo m20870_MI;
extern MethodInfo m20871_MI;
extern MethodInfo m20872_MI;
extern MethodInfo m20873_MI;
extern MethodInfo m20867_MI;
extern MethodInfo m20875_MI;
extern MethodInfo m20876_MI;
extern MethodInfo m19755_MI;
extern MethodInfo m19756_MI;
extern MethodInfo m19757_MI;
extern MethodInfo m19758_MI;
extern MethodInfo m19759_MI;
extern MethodInfo m19760_MI;
extern MethodInfo m19754_MI;
extern MethodInfo m19762_MI;
extern MethodInfo m19763_MI;
static MethodInfo* t2457_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20868_MI,
	&m5907_MI,
	&m20869_MI,
	&m20870_MI,
	&m20871_MI,
	&m20872_MI,
	&m20873_MI,
	&m5908_MI,
	&m20867_MI,
	&m20875_MI,
	&m20876_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5313_TI;
extern TypeInfo t5314_TI;
extern TypeInfo t5315_TI;
extern TypeInfo t5116_TI;
extern TypeInfo t5117_TI;
extern TypeInfo t5118_TI;
static TypeInfo* t2457_ITIs[] = 
{
	&t5313_TI,
	&t5314_TI,
	&t5315_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2457_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5313_TI, 21},
	{ &t5314_TI, 28},
	{ &t5315_TI, 33},
	{ &t5116_TI, 34},
	{ &t5117_TI, 41},
	{ &t5118_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2457_0_0_0;
extern Il2CppType t2457_1_0_0;
#include "t2458.h"
extern TypeInfo t2458_TI;
extern CustomAttributesCache t1259__CustomAttributeCache;
TypeInfo t2457_TI = 
{
	&g_mscorlib_dll_Image, NULL, "KeyValuePair`2[]", "System.Collections.Generic", t2457_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t2458_TI, t2457_ITIs, t2457_VT, &EmptyCustomAttributesCache, &t2457_TI, &t2457_0_0_0, &t2457_1_0_0, t2457_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t2458 ), -1, 0, 0, -1, 1057033, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#include "UnityEngine_ArrayTypes.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2452_TI;



// Metadata Definition UnityEngine.Font[]
static MethodInfo* t2452_MIs[] =
{
	NULL
};
extern MethodInfo m20879_MI;
extern MethodInfo m20880_MI;
extern MethodInfo m20881_MI;
extern MethodInfo m20882_MI;
extern MethodInfo m20883_MI;
extern MethodInfo m20884_MI;
extern MethodInfo m20878_MI;
extern MethodInfo m20886_MI;
extern MethodInfo m20887_MI;
static MethodInfo* t2452_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20879_MI,
	&m5907_MI,
	&m20880_MI,
	&m20881_MI,
	&m20882_MI,
	&m20883_MI,
	&m20884_MI,
	&m5908_MI,
	&m20878_MI,
	&m20886_MI,
	&m20887_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5316_TI;
extern TypeInfo t5317_TI;
extern TypeInfo t5318_TI;
static TypeInfo* t2452_ITIs[] = 
{
	&t5316_TI,
	&t5317_TI,
	&t5318_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2452_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5316_TI, 21},
	{ &t5317_TI, 28},
	{ &t5318_TI, 33},
	{ &t5065_TI, 34},
	{ &t5066_TI, 41},
	{ &t5067_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2452_0_0_0;
extern Il2CppType t2452_1_0_0;
struct t148;
extern TypeInfo t148_TI;
extern CustomAttributesCache t148__CustomAttributeCache_m1914;
extern CustomAttributesCache t148__CustomAttributeCache_m1758;
extern CustomAttributesCache t148__CustomAttributeCache_m1916;
extern CustomAttributesCache t148__CustomAttributeCache_m1918;
TypeInfo t2452_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Font[]", "UnityEngine", t2452_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t148_TI, t2452_ITIs, t2452_VT, &EmptyCustomAttributesCache, &t2452_TI, &t2452_0_0_0, &t2452_1_0_0, t2452_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t148 *), -1, sizeof(t2452_SFs), 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2453_TI;



// Metadata Definition System.Collections.Generic.List`1<UnityEngine.UI.Text>[]
static MethodInfo* t2453_MIs[] =
{
	NULL
};
extern MethodInfo m20890_MI;
extern MethodInfo m20891_MI;
extern MethodInfo m20892_MI;
extern MethodInfo m20893_MI;
extern MethodInfo m20894_MI;
extern MethodInfo m20895_MI;
extern MethodInfo m20889_MI;
extern MethodInfo m20897_MI;
extern MethodInfo m20898_MI;
extern MethodInfo m20022_MI;
extern MethodInfo m20023_MI;
extern MethodInfo m20024_MI;
extern MethodInfo m20025_MI;
extern MethodInfo m20026_MI;
extern MethodInfo m20027_MI;
extern MethodInfo m20021_MI;
extern MethodInfo m20029_MI;
extern MethodInfo m20030_MI;
extern MethodInfo m20033_MI;
extern MethodInfo m20034_MI;
extern MethodInfo m20035_MI;
extern MethodInfo m20036_MI;
extern MethodInfo m20037_MI;
extern MethodInfo m20038_MI;
extern MethodInfo m20032_MI;
extern MethodInfo m20040_MI;
extern MethodInfo m20041_MI;
extern MethodInfo m20044_MI;
extern MethodInfo m20045_MI;
extern MethodInfo m20046_MI;
extern MethodInfo m20047_MI;
extern MethodInfo m20048_MI;
extern MethodInfo m20049_MI;
extern MethodInfo m20043_MI;
extern MethodInfo m20051_MI;
extern MethodInfo m20052_MI;
static MethodInfo* t2453_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20890_MI,
	&m5907_MI,
	&m20891_MI,
	&m20892_MI,
	&m20893_MI,
	&m20894_MI,
	&m20895_MI,
	&m5908_MI,
	&m20889_MI,
	&m20897_MI,
	&m20898_MI,
	&m5905_MI,
	&m5906_MI,
	&m20022_MI,
	&m5907_MI,
	&m20023_MI,
	&m20024_MI,
	&m20025_MI,
	&m20026_MI,
	&m20027_MI,
	&m5908_MI,
	&m20021_MI,
	&m20029_MI,
	&m20030_MI,
	&m5905_MI,
	&m5906_MI,
	&m20033_MI,
	&m5907_MI,
	&m20034_MI,
	&m20035_MI,
	&m20036_MI,
	&m20037_MI,
	&m20038_MI,
	&m5908_MI,
	&m20032_MI,
	&m20040_MI,
	&m20041_MI,
	&m5905_MI,
	&m5906_MI,
	&m20044_MI,
	&m5907_MI,
	&m20045_MI,
	&m20046_MI,
	&m20047_MI,
	&m20048_MI,
	&m20049_MI,
	&m5908_MI,
	&m20043_MI,
	&m20051_MI,
	&m20052_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5319_TI;
extern TypeInfo t5320_TI;
extern TypeInfo t5321_TI;
extern TypeInfo t5169_TI;
extern TypeInfo t5170_TI;
extern TypeInfo t5171_TI;
extern TypeInfo t5172_TI;
extern TypeInfo t5173_TI;
extern TypeInfo t5174_TI;
extern TypeInfo t5175_TI;
extern TypeInfo t5176_TI;
extern TypeInfo t5177_TI;
static TypeInfo* t2453_ITIs[] = 
{
	&t5319_TI,
	&t5320_TI,
	&t5321_TI,
	&t5169_TI,
	&t5170_TI,
	&t5171_TI,
	&t5172_TI,
	&t5173_TI,
	&t5174_TI,
	&t5175_TI,
	&t5176_TI,
	&t5177_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2453_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5319_TI, 21},
	{ &t5320_TI, 28},
	{ &t5321_TI, 33},
	{ &t5169_TI, 34},
	{ &t5170_TI, 41},
	{ &t5171_TI, 46},
	{ &t5172_TI, 47},
	{ &t5173_TI, 54},
	{ &t5174_TI, 59},
	{ &t5175_TI, 60},
	{ &t5176_TI, 67},
	{ &t5177_TI, 72},
	{ &t2182_TI, 73},
	{ &t2186_TI, 80},
	{ &t2183_TI, 85},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2453_0_0_0;
extern Il2CppType t2453_1_0_0;
struct t350;
extern TypeInfo t350_TI;
extern CustomAttributesCache t1261__CustomAttributeCache;
TypeInfo t2453_TI = 
{
	&g_mscorlib_dll_Image, NULL, "List`1[]", "System.Collections.Generic", t2453_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t350_TI, t2453_ITIs, t2453_VT, &EmptyCustomAttributesCache, &t2453_TI, &t2453_0_0_0, &t2453_1_0_0, t2453_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t350 *), -1, sizeof(t2453_SFs), 0, -1, 1056769, 1, false, false, false, false, false, true, false, false, false, true, false, false, 0, 0, 0, 0, 0, 86, 15, 19};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t201_TI;



// Metadata Definition UnityEngine.UIVertex[]
static MethodInfo* t201_MIs[] =
{
	NULL
};
extern MethodInfo m20909_MI;
extern MethodInfo m20910_MI;
extern MethodInfo m20911_MI;
extern MethodInfo m20912_MI;
extern MethodInfo m20913_MI;
extern MethodInfo m20914_MI;
extern MethodInfo m20908_MI;
extern MethodInfo m20916_MI;
extern MethodInfo m20917_MI;
static MethodInfo* t201_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20909_MI,
	&m5907_MI,
	&m20910_MI,
	&m20911_MI,
	&m20912_MI,
	&m20913_MI,
	&m20914_MI,
	&m5908_MI,
	&m20908_MI,
	&m20916_MI,
	&m20917_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t404_TI;
extern TypeInfo t403_TI;
extern TypeInfo t2513_TI;
static TypeInfo* t201_ITIs[] = 
{
	&t404_TI,
	&t403_TI,
	&t2513_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t201_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t404_TI, 21},
	{ &t403_TI, 28},
	{ &t2513_TI, 33},
	{ &t5116_TI, 34},
	{ &t5117_TI, 41},
	{ &t5118_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t201_0_0_0;
extern Il2CppType t201_1_0_0;
#include "t184.h"
extern TypeInfo t184_TI;
TypeInfo t201_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UIVertex[]", "UnityEngine", t201_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t184_TI, t201_ITIs, t201_VT, &EmptyCustomAttributesCache, &t201_TI, &t201_0_0_0, &t201_1_0_0, t201_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t184 ), -1, sizeof(t201_SFs), 0, -1, 1048841, 1, false, false, false, false, false, true, false, false, false, true, false, true, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2509_TI;



// Metadata Definition System.Collections.Generic.List`1<UnityEngine.UIVertex>[]
static MethodInfo* t2509_MIs[] =
{
	NULL
};
extern MethodInfo m20935_MI;
extern MethodInfo m20936_MI;
extern MethodInfo m20937_MI;
extern MethodInfo m20938_MI;
extern MethodInfo m20939_MI;
extern MethodInfo m20940_MI;
extern MethodInfo m20934_MI;
extern MethodInfo m20942_MI;
extern MethodInfo m20943_MI;
static MethodInfo* t2509_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20935_MI,
	&m5907_MI,
	&m20936_MI,
	&m20937_MI,
	&m20938_MI,
	&m20939_MI,
	&m20940_MI,
	&m5908_MI,
	&m20934_MI,
	&m20942_MI,
	&m20943_MI,
	&m5905_MI,
	&m5906_MI,
	&m20022_MI,
	&m5907_MI,
	&m20023_MI,
	&m20024_MI,
	&m20025_MI,
	&m20026_MI,
	&m20027_MI,
	&m5908_MI,
	&m20021_MI,
	&m20029_MI,
	&m20030_MI,
	&m5905_MI,
	&m5906_MI,
	&m20033_MI,
	&m5907_MI,
	&m20034_MI,
	&m20035_MI,
	&m20036_MI,
	&m20037_MI,
	&m20038_MI,
	&m5908_MI,
	&m20032_MI,
	&m20040_MI,
	&m20041_MI,
	&m5905_MI,
	&m5906_MI,
	&m20044_MI,
	&m5907_MI,
	&m20045_MI,
	&m20046_MI,
	&m20047_MI,
	&m20048_MI,
	&m20049_MI,
	&m5908_MI,
	&m20043_MI,
	&m20051_MI,
	&m20052_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5322_TI;
extern TypeInfo t5323_TI;
extern TypeInfo t5324_TI;
static TypeInfo* t2509_ITIs[] = 
{
	&t5322_TI,
	&t5323_TI,
	&t5324_TI,
	&t5169_TI,
	&t5170_TI,
	&t5171_TI,
	&t5172_TI,
	&t5173_TI,
	&t5174_TI,
	&t5175_TI,
	&t5176_TI,
	&t5177_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2509_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5322_TI, 21},
	{ &t5323_TI, 28},
	{ &t5324_TI, 33},
	{ &t5169_TI, 34},
	{ &t5170_TI, 41},
	{ &t5171_TI, 46},
	{ &t5172_TI, 47},
	{ &t5173_TI, 54},
	{ &t5174_TI, 59},
	{ &t5175_TI, 60},
	{ &t5176_TI, 67},
	{ &t5177_TI, 72},
	{ &t2182_TI, 73},
	{ &t2186_TI, 80},
	{ &t2183_TI, 85},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2509_0_0_0;
extern Il2CppType t2509_1_0_0;
struct t163;
extern TypeInfo t163_TI;
extern CustomAttributesCache t1261__CustomAttributeCache;
TypeInfo t2509_TI = 
{
	&g_mscorlib_dll_Image, NULL, "List`1[]", "System.Collections.Generic", t2509_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t163_TI, t2509_ITIs, t2509_VT, &EmptyCustomAttributesCache, &t2509_TI, &t2509_0_0_0, &t2509_1_0_0, t2509_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t163 *), -1, sizeof(t2509_SFs), 0, -1, 1056769, 1, false, false, false, false, false, true, false, false, false, true, false, false, 0, 0, 0, 0, 0, 86, 15, 19};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2526_TI;



// Metadata Definition UnityEngine.Canvas[]
static MethodInfo* t2526_MIs[] =
{
	NULL
};
extern MethodInfo m20949_MI;
extern MethodInfo m20950_MI;
extern MethodInfo m20951_MI;
extern MethodInfo m20952_MI;
extern MethodInfo m20953_MI;
extern MethodInfo m20954_MI;
extern MethodInfo m20948_MI;
extern MethodInfo m20956_MI;
extern MethodInfo m20957_MI;
static MethodInfo* t2526_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20949_MI,
	&m5907_MI,
	&m20950_MI,
	&m20951_MI,
	&m20952_MI,
	&m20953_MI,
	&m20954_MI,
	&m5908_MI,
	&m20948_MI,
	&m20956_MI,
	&m20957_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t2528_TI;
extern TypeInfo t2535_TI;
extern TypeInfo t2529_TI;
static TypeInfo* t2526_ITIs[] = 
{
	&t2528_TI,
	&t2535_TI,
	&t2529_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2526_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2528_TI, 21},
	{ &t2535_TI, 28},
	{ &t2529_TI, 33},
	{ &t5062_TI, 34},
	{ &t5063_TI, 41},
	{ &t5064_TI, 46},
	{ &t2240_TI, 47},
	{ &t2245_TI, 54},
	{ &t2241_TI, 59},
	{ &t5065_TI, 60},
	{ &t5066_TI, 67},
	{ &t5067_TI, 72},
	{ &t2182_TI, 73},
	{ &t2186_TI, 80},
	{ &t2183_TI, 85},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2526_0_0_0;
extern Il2CppType t2526_1_0_0;
struct t3;
extern TypeInfo t3_TI;
extern CustomAttributesCache t3__CustomAttributeCache_m1605;
extern CustomAttributesCache t3__CustomAttributeCache_m1947;
extern CustomAttributesCache t3__CustomAttributeCache_m1609;
extern CustomAttributesCache t3__CustomAttributeCache_m1917;
extern CustomAttributesCache t3__CustomAttributeCache_m1951;
extern CustomAttributesCache t3__CustomAttributeCache_m1650;
extern CustomAttributesCache t3__CustomAttributeCache_m1952;
extern CustomAttributesCache t3__CustomAttributeCache_m1588;
extern CustomAttributesCache t3__CustomAttributeCache_m1607;
extern CustomAttributesCache t3__CustomAttributeCache_m1606;
extern CustomAttributesCache t3__CustomAttributeCache_m1625;
extern CustomAttributesCache t3__CustomAttributeCache_m1551;
extern CustomAttributesCache t3__CustomAttributeCache_m1913;
TypeInfo t2526_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Canvas[]", "UnityEngine", t2526_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t3_TI, t2526_ITIs, t2526_VT, &EmptyCustomAttributesCache, &t2526_TI, &t2526_0_0_0, &t2526_1_0_0, t2526_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t3 *), -1, sizeof(t2526_SFs), 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 86, 15, 19};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3836_TI;



// Metadata Definition UnityEngine.UI.GraphicRaycaster[]
static MethodInfo* t3836_MIs[] =
{
	NULL
};
extern MethodInfo m20975_MI;
extern MethodInfo m20976_MI;
extern MethodInfo m20977_MI;
extern MethodInfo m20978_MI;
extern MethodInfo m20979_MI;
extern MethodInfo m20980_MI;
extern MethodInfo m20974_MI;
extern MethodInfo m20982_MI;
extern MethodInfo m20983_MI;
extern MethodInfo m20105_MI;
extern MethodInfo m20106_MI;
extern MethodInfo m20107_MI;
extern MethodInfo m20108_MI;
extern MethodInfo m20109_MI;
extern MethodInfo m20110_MI;
extern MethodInfo m20104_MI;
extern MethodInfo m20112_MI;
extern MethodInfo m20113_MI;
static MethodInfo* t3836_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m20975_MI,
	&m5907_MI,
	&m20976_MI,
	&m20977_MI,
	&m20978_MI,
	&m20979_MI,
	&m20980_MI,
	&m5908_MI,
	&m20974_MI,
	&m20982_MI,
	&m20983_MI,
	&m5905_MI,
	&m5906_MI,
	&m20105_MI,
	&m5907_MI,
	&m20106_MI,
	&m20107_MI,
	&m20108_MI,
	&m20109_MI,
	&m20110_MI,
	&m5908_MI,
	&m20104_MI,
	&m20112_MI,
	&m20113_MI,
	&m5905_MI,
	&m5906_MI,
	&m19777_MI,
	&m5907_MI,
	&m19778_MI,
	&m19779_MI,
	&m19780_MI,
	&m19781_MI,
	&m19782_MI,
	&m5908_MI,
	&m19776_MI,
	&m19784_MI,
	&m19785_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5325_TI;
extern TypeInfo t5326_TI;
extern TypeInfo t5327_TI;
extern TypeInfo t2267_TI;
extern TypeInfo t2274_TI;
extern TypeInfo t2268_TI;
static TypeInfo* t3836_ITIs[] = 
{
	&t5325_TI,
	&t5326_TI,
	&t5327_TI,
	&t2267_TI,
	&t2274_TI,
	&t2268_TI,
	&t5122_TI,
	&t5123_TI,
	&t5124_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3836_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5325_TI, 21},
	{ &t5326_TI, 28},
	{ &t5327_TI, 33},
	{ &t2267_TI, 34},
	{ &t2274_TI, 41},
	{ &t2268_TI, 46},
	{ &t5122_TI, 47},
	{ &t5123_TI, 54},
	{ &t5124_TI, 59},
	{ &t5059_TI, 60},
	{ &t5060_TI, 67},
	{ &t5061_TI, 72},
	{ &t5062_TI, 73},
	{ &t5063_TI, 80},
	{ &t5064_TI, 85},
	{ &t2240_TI, 86},
	{ &t2245_TI, 93},
	{ &t2241_TI, 98},
	{ &t5065_TI, 99},
	{ &t5066_TI, 106},
	{ &t5067_TI, 111},
	{ &t2182_TI, 112},
	{ &t2186_TI, 119},
	{ &t2183_TI, 124},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3836_0_0_0;
extern Il2CppType t3836_1_0_0;
struct t166;
extern TypeInfo t166_TI;
extern CustomAttributesCache t166__CustomAttributeCache;
extern CustomAttributesCache t166__CustomAttributeCache_m_IgnoreReversedGraphics;
extern CustomAttributesCache t166__CustomAttributeCache_m_BlockingObjects;
extern CustomAttributesCache t166__CustomAttributeCache_m_BlockingMask;
extern CustomAttributesCache t166__CustomAttributeCache_U3CU3Ef__am$cache6;
extern CustomAttributesCache t166__CustomAttributeCache_m500;
TypeInfo t3836_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "GraphicRaycaster[]", "UnityEngine.UI", t3836_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t166_TI, t3836_ITIs, t3836_VT, &EmptyCustomAttributesCache, &t3836_TI, &t3836_0_0_0, &t3836_1_0_0, t3836_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t166 *), -1, sizeof(t3836_SFs), 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, true, false, false, 0, 0, 0, 0, 0, 125, 24, 28};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3837_TI;



// Metadata Definition UnityEngine.UI.GraphicRaycaster/BlockingObjects[]
static MethodInfo* t3837_MIs[] =
{
	NULL
};
extern MethodInfo m21002_MI;
extern MethodInfo m21003_MI;
extern MethodInfo m21004_MI;
extern MethodInfo m21005_MI;
extern MethodInfo m21006_MI;
extern MethodInfo m21007_MI;
extern MethodInfo m21001_MI;
extern MethodInfo m21009_MI;
extern MethodInfo m21010_MI;
extern MethodInfo m19711_MI;
extern MethodInfo m19712_MI;
extern MethodInfo m19713_MI;
extern MethodInfo m19714_MI;
extern MethodInfo m19715_MI;
extern MethodInfo m19716_MI;
extern MethodInfo m19710_MI;
extern MethodInfo m19718_MI;
extern MethodInfo m19719_MI;
extern MethodInfo m19722_MI;
extern MethodInfo m19723_MI;
extern MethodInfo m19724_MI;
extern MethodInfo m19725_MI;
extern MethodInfo m19726_MI;
extern MethodInfo m19727_MI;
extern MethodInfo m19721_MI;
extern MethodInfo m19729_MI;
extern MethodInfo m19730_MI;
extern MethodInfo m19733_MI;
extern MethodInfo m19734_MI;
extern MethodInfo m19735_MI;
extern MethodInfo m19736_MI;
extern MethodInfo m19737_MI;
extern MethodInfo m19738_MI;
extern MethodInfo m19732_MI;
extern MethodInfo m19740_MI;
extern MethodInfo m19741_MI;
extern MethodInfo m19744_MI;
extern MethodInfo m19745_MI;
extern MethodInfo m19746_MI;
extern MethodInfo m19747_MI;
extern MethodInfo m19748_MI;
extern MethodInfo m19749_MI;
extern MethodInfo m19743_MI;
extern MethodInfo m19751_MI;
extern MethodInfo m19752_MI;
static MethodInfo* t3837_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21002_MI,
	&m5907_MI,
	&m21003_MI,
	&m21004_MI,
	&m21005_MI,
	&m21006_MI,
	&m21007_MI,
	&m5908_MI,
	&m21001_MI,
	&m21009_MI,
	&m21010_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5328_TI;
extern TypeInfo t5329_TI;
extern TypeInfo t5330_TI;
extern TypeInfo t5104_TI;
extern TypeInfo t5105_TI;
extern TypeInfo t5106_TI;
extern TypeInfo t5107_TI;
extern TypeInfo t5108_TI;
extern TypeInfo t5109_TI;
extern TypeInfo t5110_TI;
extern TypeInfo t5111_TI;
extern TypeInfo t5112_TI;
extern TypeInfo t5113_TI;
extern TypeInfo t5114_TI;
extern TypeInfo t5115_TI;
static TypeInfo* t3837_ITIs[] = 
{
	&t5328_TI,
	&t5329_TI,
	&t5330_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3837_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5328_TI, 21},
	{ &t5329_TI, 28},
	{ &t5330_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3837_0_0_0;
extern Il2CppType t3837_1_0_0;
#include "t165.h"
extern TypeInfo t165_TI;
extern TypeInfo t44_TI;
TypeInfo t3837_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "BlockingObjects[]", "", t3837_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t165_TI, t3837_ITIs, t3837_VT, &EmptyCustomAttributesCache, &t44_TI, &t3837_0_0_0, &t3837_1_0_0, t3837_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 258, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2568_TI;



// Metadata Definition System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>[]
static MethodInfo* t2568_MIs[] =
{
	NULL
};
extern MethodInfo m21013_MI;
extern MethodInfo m21014_MI;
extern MethodInfo m21015_MI;
extern MethodInfo m21016_MI;
extern MethodInfo m21017_MI;
extern MethodInfo m21018_MI;
extern MethodInfo m21012_MI;
extern MethodInfo m21020_MI;
extern MethodInfo m21021_MI;
static MethodInfo* t2568_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21013_MI,
	&m5907_MI,
	&m21014_MI,
	&m21015_MI,
	&m21016_MI,
	&m21017_MI,
	&m21018_MI,
	&m5908_MI,
	&m21012_MI,
	&m21020_MI,
	&m21021_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5331_TI;
extern TypeInfo t5332_TI;
extern TypeInfo t5333_TI;
static TypeInfo* t2568_ITIs[] = 
{
	&t5331_TI,
	&t5332_TI,
	&t5333_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2568_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5331_TI, 21},
	{ &t5332_TI, 28},
	{ &t5333_TI, 33},
	{ &t5116_TI, 34},
	{ &t5117_TI, 41},
	{ &t5118_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2568_0_0_0;
extern Il2CppType t2568_1_0_0;
#include "t2569.h"
extern TypeInfo t2569_TI;
extern CustomAttributesCache t1259__CustomAttributeCache;
TypeInfo t2568_TI = 
{
	&g_mscorlib_dll_Image, NULL, "KeyValuePair`2[]", "System.Collections.Generic", t2568_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t2569_TI, t2568_ITIs, t2568_VT, &EmptyCustomAttributesCache, &t2568_TI, &t2568_0_0_0, &t2568_1_0_0, t2568_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t2569 ), -1, 0, 0, -1, 1057033, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2561_TI;



// Metadata Definition System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>[]
static MethodInfo* t2561_MIs[] =
{
	NULL
};
extern MethodInfo m21031_MI;
extern MethodInfo m21032_MI;
extern MethodInfo m21033_MI;
extern MethodInfo m21034_MI;
extern MethodInfo m21035_MI;
extern MethodInfo m21036_MI;
extern MethodInfo m21030_MI;
extern MethodInfo m21038_MI;
extern MethodInfo m21039_MI;
static MethodInfo* t2561_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21031_MI,
	&m5907_MI,
	&m21032_MI,
	&m21033_MI,
	&m21034_MI,
	&m21035_MI,
	&m21036_MI,
	&m5908_MI,
	&m21030_MI,
	&m21038_MI,
	&m21039_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5334_TI;
extern TypeInfo t5335_TI;
extern TypeInfo t5336_TI;
static TypeInfo* t2561_ITIs[] = 
{
	&t5334_TI,
	&t5335_TI,
	&t5336_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2561_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5334_TI, 21},
	{ &t5335_TI, 28},
	{ &t5336_TI, 33},
	{ &t5116_TI, 34},
	{ &t5117_TI, 41},
	{ &t5118_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2561_0_0_0;
extern Il2CppType t2561_1_0_0;
#include "t2562.h"
extern TypeInfo t2562_TI;
extern CustomAttributesCache t1259__CustomAttributeCache;
TypeInfo t2561_TI = 
{
	&g_mscorlib_dll_Image, NULL, "KeyValuePair`2[]", "System.Collections.Generic", t2561_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t2562_TI, t2561_ITIs, t2561_VT, &EmptyCustomAttributesCache, &t2561_TI, &t2561_0_0_0, &t2561_1_0_0, t2561_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t2562 ), -1, 0, 0, -1, 1057033, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2557_TI;



// Metadata Definition UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>[]
static MethodInfo* t2557_MIs[] =
{
	NULL
};
extern MethodInfo m21042_MI;
extern MethodInfo m21043_MI;
extern MethodInfo m21044_MI;
extern MethodInfo m21045_MI;
extern MethodInfo m21046_MI;
extern MethodInfo m21047_MI;
extern MethodInfo m21041_MI;
extern MethodInfo m21049_MI;
extern MethodInfo m21050_MI;
static MethodInfo* t2557_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21042_MI,
	&m5907_MI,
	&m21043_MI,
	&m21044_MI,
	&m21045_MI,
	&m21046_MI,
	&m21047_MI,
	&m5908_MI,
	&m21041_MI,
	&m21049_MI,
	&m21050_MI,
	&m5905_MI,
	&m5906_MI,
	&m20022_MI,
	&m5907_MI,
	&m20023_MI,
	&m20024_MI,
	&m20025_MI,
	&m20026_MI,
	&m20027_MI,
	&m5908_MI,
	&m20021_MI,
	&m20029_MI,
	&m20030_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5337_TI;
extern TypeInfo t5338_TI;
extern TypeInfo t5339_TI;
static TypeInfo* t2557_ITIs[] = 
{
	&t5337_TI,
	&t5338_TI,
	&t5339_TI,
	&t5169_TI,
	&t5170_TI,
	&t5171_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2557_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5337_TI, 21},
	{ &t5338_TI, 28},
	{ &t5339_TI, 33},
	{ &t5169_TI, 34},
	{ &t5170_TI, 41},
	{ &t5171_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t2557_0_0_0;
extern Il2CppType t2557_1_0_0;
struct t366;
extern TypeInfo t366_TI;
extern CustomAttributesCache t274__CustomAttributeCache;
TypeInfo t2557_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "IndexedSet`1[]", "UnityEngine.UI.Collections", t2557_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t366_TI, t2557_ITIs, t2557_VT, &EmptyCustomAttributesCache, &t2557_TI, &t2557_0_0_0, &t2557_1_0_0, t2557_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t366 *), -1, 0, 0, -1, 1048576, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3838_TI;



// Metadata Definition UnityEngine.UI.Image[]
static MethodInfo* t3838_MIs[] =
{
	NULL
};
extern MethodInfo m21060_MI;
extern MethodInfo m21061_MI;
extern MethodInfo m21062_MI;
extern MethodInfo m21063_MI;
extern MethodInfo m21064_MI;
extern MethodInfo m21065_MI;
extern MethodInfo m21059_MI;
extern MethodInfo m21067_MI;
extern MethodInfo m21068_MI;
extern MethodInfo m21071_MI;
extern MethodInfo m21072_MI;
extern MethodInfo m21073_MI;
extern MethodInfo m21074_MI;
extern MethodInfo m21075_MI;
extern MethodInfo m21076_MI;
extern MethodInfo m21070_MI;
extern MethodInfo m21078_MI;
extern MethodInfo m21079_MI;
extern MethodInfo m21082_MI;
extern MethodInfo m21083_MI;
extern MethodInfo m21084_MI;
extern MethodInfo m21085_MI;
extern MethodInfo m21086_MI;
extern MethodInfo m21087_MI;
extern MethodInfo m21081_MI;
extern MethodInfo m21089_MI;
extern MethodInfo m21090_MI;
extern MethodInfo m20809_MI;
extern MethodInfo m20810_MI;
extern MethodInfo m20811_MI;
extern MethodInfo m20812_MI;
extern MethodInfo m20813_MI;
extern MethodInfo m20814_MI;
extern MethodInfo m20808_MI;
extern MethodInfo m20816_MI;
extern MethodInfo m20817_MI;
extern MethodInfo m20820_MI;
extern MethodInfo m20821_MI;
extern MethodInfo m20822_MI;
extern MethodInfo m20823_MI;
extern MethodInfo m20824_MI;
extern MethodInfo m20825_MI;
extern MethodInfo m20819_MI;
extern MethodInfo m20827_MI;
extern MethodInfo m20828_MI;
extern MethodInfo m20831_MI;
extern MethodInfo m20832_MI;
extern MethodInfo m20833_MI;
extern MethodInfo m20834_MI;
extern MethodInfo m20835_MI;
extern MethodInfo m20836_MI;
extern MethodInfo m20830_MI;
extern MethodInfo m20838_MI;
extern MethodInfo m20839_MI;
static MethodInfo* t3838_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21060_MI,
	&m5907_MI,
	&m21061_MI,
	&m21062_MI,
	&m21063_MI,
	&m21064_MI,
	&m21065_MI,
	&m5908_MI,
	&m21059_MI,
	&m21067_MI,
	&m21068_MI,
	&m5905_MI,
	&m5906_MI,
	&m21071_MI,
	&m5907_MI,
	&m21072_MI,
	&m21073_MI,
	&m21074_MI,
	&m21075_MI,
	&m21076_MI,
	&m5908_MI,
	&m21070_MI,
	&m21078_MI,
	&m21079_MI,
	&m5905_MI,
	&m5906_MI,
	&m21082_MI,
	&m5907_MI,
	&m21083_MI,
	&m21084_MI,
	&m21085_MI,
	&m21086_MI,
	&m21087_MI,
	&m5908_MI,
	&m21081_MI,
	&m21089_MI,
	&m21090_MI,
	&m5905_MI,
	&m5906_MI,
	&m20809_MI,
	&m5907_MI,
	&m20810_MI,
	&m20811_MI,
	&m20812_MI,
	&m20813_MI,
	&m20814_MI,
	&m5908_MI,
	&m20808_MI,
	&m20816_MI,
	&m20817_MI,
	&m5905_MI,
	&m5906_MI,
	&m20820_MI,
	&m5907_MI,
	&m20821_MI,
	&m20822_MI,
	&m20823_MI,
	&m20824_MI,
	&m20825_MI,
	&m5908_MI,
	&m20819_MI,
	&m20827_MI,
	&m20828_MI,
	&m5905_MI,
	&m5906_MI,
	&m20831_MI,
	&m5907_MI,
	&m20832_MI,
	&m20833_MI,
	&m20834_MI,
	&m20835_MI,
	&m20836_MI,
	&m5908_MI,
	&m20830_MI,
	&m20838_MI,
	&m20839_MI,
	&m5905_MI,
	&m5906_MI,
	&m20842_MI,
	&m5907_MI,
	&m20843_MI,
	&m20844_MI,
	&m20845_MI,
	&m20846_MI,
	&m20847_MI,
	&m5908_MI,
	&m20841_MI,
	&m20849_MI,
	&m20850_MI,
	&m5905_MI,
	&m5906_MI,
	&m20737_MI,
	&m5907_MI,
	&m20738_MI,
	&m20739_MI,
	&m20740_MI,
	&m20741_MI,
	&m20742_MI,
	&m5908_MI,
	&m20736_MI,
	&m20744_MI,
	&m20745_MI,
	&m5905_MI,
	&m5906_MI,
	&m19777_MI,
	&m5907_MI,
	&m19778_MI,
	&m19779_MI,
	&m19780_MI,
	&m19781_MI,
	&m19782_MI,
	&m5908_MI,
	&m19776_MI,
	&m19784_MI,
	&m19785_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5340_TI;
extern TypeInfo t5341_TI;
extern TypeInfo t5342_TI;
extern TypeInfo t5343_TI;
extern TypeInfo t5344_TI;
extern TypeInfo t5345_TI;
extern TypeInfo t5346_TI;
extern TypeInfo t5347_TI;
extern TypeInfo t5348_TI;
extern TypeInfo t5304_TI;
extern TypeInfo t5305_TI;
extern TypeInfo t5306_TI;
extern TypeInfo t5307_TI;
extern TypeInfo t5308_TI;
extern TypeInfo t5309_TI;
extern TypeInfo t5310_TI;
extern TypeInfo t5311_TI;
extern TypeInfo t5312_TI;
static TypeInfo* t3838_ITIs[] = 
{
	&t5340_TI,
	&t5341_TI,
	&t5342_TI,
	&t5343_TI,
	&t5344_TI,
	&t5345_TI,
	&t5346_TI,
	&t5347_TI,
	&t5348_TI,
	&t5304_TI,
	&t5305_TI,
	&t5306_TI,
	&t5307_TI,
	&t5308_TI,
	&t5309_TI,
	&t5310_TI,
	&t5311_TI,
	&t5312_TI,
	&t363_TI,
	&t171_TI,
	&t2547_TI,
	&t2430_TI,
	&t2434_TI,
	&t2431_TI,
	&t5122_TI,
	&t5123_TI,
	&t5124_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3838_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5340_TI, 21},
	{ &t5341_TI, 28},
	{ &t5342_TI, 33},
	{ &t5343_TI, 34},
	{ &t5344_TI, 41},
	{ &t5345_TI, 46},
	{ &t5346_TI, 47},
	{ &t5347_TI, 54},
	{ &t5348_TI, 59},
	{ &t5304_TI, 60},
	{ &t5305_TI, 67},
	{ &t5306_TI, 72},
	{ &t5307_TI, 73},
	{ &t5308_TI, 80},
	{ &t5309_TI, 85},
	{ &t5310_TI, 86},
	{ &t5311_TI, 93},
	{ &t5312_TI, 98},
	{ &t363_TI, 99},
	{ &t171_TI, 106},
	{ &t2547_TI, 111},
	{ &t2430_TI, 112},
	{ &t2434_TI, 119},
	{ &t2431_TI, 124},
	{ &t5122_TI, 125},
	{ &t5123_TI, 132},
	{ &t5124_TI, 137},
	{ &t5059_TI, 138},
	{ &t5060_TI, 145},
	{ &t5061_TI, 150},
	{ &t5062_TI, 151},
	{ &t5063_TI, 158},
	{ &t5064_TI, 163},
	{ &t2240_TI, 164},
	{ &t2245_TI, 171},
	{ &t2241_TI, 176},
	{ &t5065_TI, 177},
	{ &t5066_TI, 184},
	{ &t5067_TI, 189},
	{ &t2182_TI, 190},
	{ &t2186_TI, 197},
	{ &t2183_TI, 202},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3838_0_0_0;
extern Il2CppType t3838_1_0_0;
struct t179;
extern TypeInfo t179_TI;
extern CustomAttributesCache t179__CustomAttributeCache;
extern CustomAttributesCache t179__CustomAttributeCache_m_Sprite;
extern CustomAttributesCache t179__CustomAttributeCache_m_Type;
extern CustomAttributesCache t179__CustomAttributeCache_m_PreserveAspect;
extern CustomAttributesCache t179__CustomAttributeCache_m_FillCenter;
extern CustomAttributesCache t179__CustomAttributeCache_m_FillMethod;
extern CustomAttributesCache t179__CustomAttributeCache_m_FillAmount;
extern CustomAttributesCache t179__CustomAttributeCache_m_FillClockwise;
extern CustomAttributesCache t179__CustomAttributeCache_m_FillOrigin;
TypeInfo t3838_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Image[]", "UnityEngine.UI", t3838_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t179_TI, t3838_ITIs, t3838_VT, &EmptyCustomAttributesCache, &t3838_TI, &t3838_0_0_0, &t3838_1_0_0, t3838_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t179 *), -1, sizeof(t3838_SFs), 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, true, false, false, 0, 0, 0, 0, 0, 203, 42, 46};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3730_TI;



// Metadata Definition UnityEngine.ICanvasRaycastFilter[]
static MethodInfo* t3730_MIs[] =
{
	NULL
};
static MethodInfo* t3730_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21071_MI,
	&m5907_MI,
	&m21072_MI,
	&m21073_MI,
	&m21074_MI,
	&m21075_MI,
	&m21076_MI,
	&m5908_MI,
	&m21070_MI,
	&m21078_MI,
	&m21079_MI,
};
static TypeInfo* t3730_ITIs[] = 
{
	&t5343_TI,
	&t5344_TI,
	&t5345_TI,
};
static Il2CppInterfaceOffsetPair t3730_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5343_TI, 21},
	{ &t5344_TI, 28},
	{ &t5345_TI, 33},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3730_0_0_0;
extern Il2CppType t3730_1_0_0;
struct t357;
extern TypeInfo t357_TI;
TypeInfo t3730_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "ICanvasRaycastFilter[]", "UnityEngine", t3730_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t357_TI, t3730_ITIs, t3730_VT, &EmptyCustomAttributesCache, &t3730_TI, &t3730_0_0_0, &t3730_1_0_0, t3730_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3731_TI;



// Metadata Definition UnityEngine.ISerializationCallbackReceiver[]
static MethodInfo* t3731_MIs[] =
{
	NULL
};
static MethodInfo* t3731_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21082_MI,
	&m5907_MI,
	&m21083_MI,
	&m21084_MI,
	&m21085_MI,
	&m21086_MI,
	&m21087_MI,
	&m5908_MI,
	&m21081_MI,
	&m21089_MI,
	&m21090_MI,
};
static TypeInfo* t3731_ITIs[] = 
{
	&t5346_TI,
	&t5347_TI,
	&t5348_TI,
};
static Il2CppInterfaceOffsetPair t3731_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5346_TI, 21},
	{ &t5347_TI, 28},
	{ &t5348_TI, 33},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3731_0_0_0;
extern Il2CppType t3731_1_0_0;
struct t301;
extern TypeInfo t301_TI;
TypeInfo t3731_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "ISerializationCallbackReceiver[]", "UnityEngine", t3731_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t301_TI, t3731_ITIs, t3731_VT, &EmptyCustomAttributesCache, &t3731_TI, &t3731_0_0_0, &t3731_1_0_0, t3731_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t181_TI;



// Metadata Definition UnityEngine.Vector2[]
static MethodInfo* t181_MIs[] =
{
	NULL
};
extern MethodInfo m21094_MI;
extern MethodInfo m21095_MI;
extern MethodInfo m21096_MI;
extern MethodInfo m21097_MI;
extern MethodInfo m21098_MI;
extern MethodInfo m21099_MI;
extern MethodInfo m21093_MI;
extern MethodInfo m21101_MI;
extern MethodInfo m21102_MI;
static MethodInfo* t181_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21094_MI,
	&m5907_MI,
	&m21095_MI,
	&m21096_MI,
	&m21097_MI,
	&m21098_MI,
	&m21099_MI,
	&m5908_MI,
	&m21093_MI,
	&m21101_MI,
	&m21102_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5349_TI;
extern TypeInfo t5350_TI;
extern TypeInfo t5351_TI;
static TypeInfo* t181_ITIs[] = 
{
	&t5349_TI,
	&t5350_TI,
	&t5351_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t181_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5349_TI, 21},
	{ &t5350_TI, 28},
	{ &t5351_TI, 33},
	{ &t5116_TI, 34},
	{ &t5117_TI, 41},
	{ &t5118_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t181_0_0_0;
extern Il2CppType t181_1_0_0;
#include "t17.h"
extern TypeInfo t17_TI;
extern CustomAttributesCache t17__CustomAttributeCache;
TypeInfo t181_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Vector2[]", "UnityEngine", t181_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t17_TI, t181_ITIs, t181_VT, &EmptyCustomAttributesCache, &t181_TI, &t181_0_0_0, &t181_1_0_0, t181_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t17 ), -1, 0, 0, -1, 1048841, 1, false, false, false, false, false, true, false, false, false, false, false, true, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3839_TI;



// Metadata Definition UnityEngine.UI.Image/Type[]
static MethodInfo* t3839_MIs[] =
{
	NULL
};
extern MethodInfo m21105_MI;
extern MethodInfo m21106_MI;
extern MethodInfo m21107_MI;
extern MethodInfo m21108_MI;
extern MethodInfo m21109_MI;
extern MethodInfo m21110_MI;
extern MethodInfo m21104_MI;
extern MethodInfo m21112_MI;
extern MethodInfo m21113_MI;
static MethodInfo* t3839_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21105_MI,
	&m5907_MI,
	&m21106_MI,
	&m21107_MI,
	&m21108_MI,
	&m21109_MI,
	&m21110_MI,
	&m5908_MI,
	&m21104_MI,
	&m21112_MI,
	&m21113_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5352_TI;
extern TypeInfo t5353_TI;
extern TypeInfo t5354_TI;
static TypeInfo* t3839_ITIs[] = 
{
	&t5352_TI,
	&t5353_TI,
	&t5354_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3839_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5352_TI, 21},
	{ &t5353_TI, 28},
	{ &t5354_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3839_0_0_0;
extern Il2CppType t3839_1_0_0;
#include "t172.h"
extern TypeInfo t172_TI;
TypeInfo t3839_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Type[]", "", t3839_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t172_TI, t3839_ITIs, t3839_VT, &EmptyCustomAttributesCache, &t44_TI, &t3839_0_0_0, &t3839_1_0_0, t3839_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 258, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3840_TI;



// Metadata Definition UnityEngine.UI.Image/FillMethod[]
static MethodInfo* t3840_MIs[] =
{
	NULL
};
extern MethodInfo m21116_MI;
extern MethodInfo m21117_MI;
extern MethodInfo m21118_MI;
extern MethodInfo m21119_MI;
extern MethodInfo m21120_MI;
extern MethodInfo m21121_MI;
extern MethodInfo m21115_MI;
extern MethodInfo m21123_MI;
extern MethodInfo m21124_MI;
static MethodInfo* t3840_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21116_MI,
	&m5907_MI,
	&m21117_MI,
	&m21118_MI,
	&m21119_MI,
	&m21120_MI,
	&m21121_MI,
	&m5908_MI,
	&m21115_MI,
	&m21123_MI,
	&m21124_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5355_TI;
extern TypeInfo t5356_TI;
extern TypeInfo t5357_TI;
static TypeInfo* t3840_ITIs[] = 
{
	&t5355_TI,
	&t5356_TI,
	&t5357_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3840_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5355_TI, 21},
	{ &t5356_TI, 28},
	{ &t5357_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3840_0_0_0;
extern Il2CppType t3840_1_0_0;
#include "t173.h"
extern TypeInfo t173_TI;
TypeInfo t3840_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "FillMethod[]", "", t3840_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t173_TI, t3840_ITIs, t3840_VT, &EmptyCustomAttributesCache, &t44_TI, &t3840_0_0_0, &t3840_1_0_0, t3840_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 258, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3841_TI;



// Metadata Definition UnityEngine.UI.Image/OriginHorizontal[]
static MethodInfo* t3841_MIs[] =
{
	NULL
};
extern MethodInfo m21127_MI;
extern MethodInfo m21128_MI;
extern MethodInfo m21129_MI;
extern MethodInfo m21130_MI;
extern MethodInfo m21131_MI;
extern MethodInfo m21132_MI;
extern MethodInfo m21126_MI;
extern MethodInfo m21134_MI;
extern MethodInfo m21135_MI;
static MethodInfo* t3841_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21127_MI,
	&m5907_MI,
	&m21128_MI,
	&m21129_MI,
	&m21130_MI,
	&m21131_MI,
	&m21132_MI,
	&m5908_MI,
	&m21126_MI,
	&m21134_MI,
	&m21135_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5358_TI;
extern TypeInfo t5359_TI;
extern TypeInfo t5360_TI;
static TypeInfo* t3841_ITIs[] = 
{
	&t5358_TI,
	&t5359_TI,
	&t5360_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3841_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5358_TI, 21},
	{ &t5359_TI, 28},
	{ &t5360_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3841_0_0_0;
extern Il2CppType t3841_1_0_0;
#include "t174.h"
extern TypeInfo t174_TI;
TypeInfo t3841_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "OriginHorizontal[]", "", t3841_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t174_TI, t3841_ITIs, t3841_VT, &EmptyCustomAttributesCache, &t44_TI, &t3841_0_0_0, &t3841_1_0_0, t3841_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 258, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3842_TI;



// Metadata Definition UnityEngine.UI.Image/OriginVertical[]
static MethodInfo* t3842_MIs[] =
{
	NULL
};
extern MethodInfo m21138_MI;
extern MethodInfo m21139_MI;
extern MethodInfo m21140_MI;
extern MethodInfo m21141_MI;
extern MethodInfo m21142_MI;
extern MethodInfo m21143_MI;
extern MethodInfo m21137_MI;
extern MethodInfo m21145_MI;
extern MethodInfo m21146_MI;
static MethodInfo* t3842_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21138_MI,
	&m5907_MI,
	&m21139_MI,
	&m21140_MI,
	&m21141_MI,
	&m21142_MI,
	&m21143_MI,
	&m5908_MI,
	&m21137_MI,
	&m21145_MI,
	&m21146_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5361_TI;
extern TypeInfo t5362_TI;
extern TypeInfo t5363_TI;
static TypeInfo* t3842_ITIs[] = 
{
	&t5361_TI,
	&t5362_TI,
	&t5363_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3842_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5361_TI, 21},
	{ &t5362_TI, 28},
	{ &t5363_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3842_0_0_0;
extern Il2CppType t3842_1_0_0;
#include "t175.h"
extern TypeInfo t175_TI;
TypeInfo t3842_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "OriginVertical[]", "", t3842_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t175_TI, t3842_ITIs, t3842_VT, &EmptyCustomAttributesCache, &t44_TI, &t3842_0_0_0, &t3842_1_0_0, t3842_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 258, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3843_TI;



// Metadata Definition UnityEngine.UI.Image/Origin90[]
static MethodInfo* t3843_MIs[] =
{
	NULL
};
extern MethodInfo m21149_MI;
extern MethodInfo m21150_MI;
extern MethodInfo m21151_MI;
extern MethodInfo m21152_MI;
extern MethodInfo m21153_MI;
extern MethodInfo m21154_MI;
extern MethodInfo m21148_MI;
extern MethodInfo m21156_MI;
extern MethodInfo m21157_MI;
static MethodInfo* t3843_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21149_MI,
	&m5907_MI,
	&m21150_MI,
	&m21151_MI,
	&m21152_MI,
	&m21153_MI,
	&m21154_MI,
	&m5908_MI,
	&m21148_MI,
	&m21156_MI,
	&m21157_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5364_TI;
extern TypeInfo t5365_TI;
extern TypeInfo t5366_TI;
static TypeInfo* t3843_ITIs[] = 
{
	&t5364_TI,
	&t5365_TI,
	&t5366_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3843_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5364_TI, 21},
	{ &t5365_TI, 28},
	{ &t5366_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3843_0_0_0;
extern Il2CppType t3843_1_0_0;
#include "t176.h"
extern TypeInfo t176_TI;
TypeInfo t3843_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Origin90[]", "", t3843_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t176_TI, t3843_ITIs, t3843_VT, &EmptyCustomAttributesCache, &t44_TI, &t3843_0_0_0, &t3843_1_0_0, t3843_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 258, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3844_TI;



// Metadata Definition UnityEngine.UI.Image/Origin180[]
static MethodInfo* t3844_MIs[] =
{
	NULL
};
extern MethodInfo m21160_MI;
extern MethodInfo m21161_MI;
extern MethodInfo m21162_MI;
extern MethodInfo m21163_MI;
extern MethodInfo m21164_MI;
extern MethodInfo m21165_MI;
extern MethodInfo m21159_MI;
extern MethodInfo m21167_MI;
extern MethodInfo m21168_MI;
static MethodInfo* t3844_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21160_MI,
	&m5907_MI,
	&m21161_MI,
	&m21162_MI,
	&m21163_MI,
	&m21164_MI,
	&m21165_MI,
	&m5908_MI,
	&m21159_MI,
	&m21167_MI,
	&m21168_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5367_TI;
extern TypeInfo t5368_TI;
extern TypeInfo t5369_TI;
static TypeInfo* t3844_ITIs[] = 
{
	&t5367_TI,
	&t5368_TI,
	&t5369_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3844_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5367_TI, 21},
	{ &t5368_TI, 28},
	{ &t5369_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3844_0_0_0;
extern Il2CppType t3844_1_0_0;
#include "t177.h"
extern TypeInfo t177_TI;
TypeInfo t3844_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Origin180[]", "", t3844_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t177_TI, t3844_ITIs, t3844_VT, &EmptyCustomAttributesCache, &t44_TI, &t3844_0_0_0, &t3844_1_0_0, t3844_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 258, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3845_TI;



// Metadata Definition UnityEngine.UI.Image/Origin360[]
static MethodInfo* t3845_MIs[] =
{
	NULL
};
extern MethodInfo m21171_MI;
extern MethodInfo m21172_MI;
extern MethodInfo m21173_MI;
extern MethodInfo m21174_MI;
extern MethodInfo m21175_MI;
extern MethodInfo m21176_MI;
extern MethodInfo m21170_MI;
extern MethodInfo m21178_MI;
extern MethodInfo m21179_MI;
static MethodInfo* t3845_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21171_MI,
	&m5907_MI,
	&m21172_MI,
	&m21173_MI,
	&m21174_MI,
	&m21175_MI,
	&m21176_MI,
	&m5908_MI,
	&m21170_MI,
	&m21178_MI,
	&m21179_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5370_TI;
extern TypeInfo t5371_TI;
extern TypeInfo t5372_TI;
static TypeInfo* t3845_ITIs[] = 
{
	&t5370_TI,
	&t5371_TI,
	&t5372_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3845_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5370_TI, 21},
	{ &t5371_TI, 28},
	{ &t5372_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3845_0_0_0;
extern Il2CppType t3845_1_0_0;
#include "t178.h"
extern TypeInfo t178_TI;
TypeInfo t3845_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Origin360[]", "", t3845_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t178_TI, t3845_ITIs, t3845_VT, &EmptyCustomAttributesCache, &t44_TI, &t3845_0_0_0, &t3845_1_0_0, t3845_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 258, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3846_TI;



// Metadata Definition UnityEngine.UI.InputField[]
static MethodInfo* t3846_MIs[] =
{
	NULL
};
extern MethodInfo m21182_MI;
extern MethodInfo m21183_MI;
extern MethodInfo m21184_MI;
extern MethodInfo m21185_MI;
extern MethodInfo m21186_MI;
extern MethodInfo m21187_MI;
extern MethodInfo m21181_MI;
extern MethodInfo m21189_MI;
extern MethodInfo m21190_MI;
extern MethodInfo m19581_MI;
extern MethodInfo m19582_MI;
extern MethodInfo m19583_MI;
extern MethodInfo m19584_MI;
extern MethodInfo m19585_MI;
extern MethodInfo m19586_MI;
extern MethodInfo m19580_MI;
extern MethodInfo m19588_MI;
extern MethodInfo m19589_MI;
extern MethodInfo m20164_MI;
extern MethodInfo m20165_MI;
extern MethodInfo m20166_MI;
extern MethodInfo m20167_MI;
extern MethodInfo m20168_MI;
extern MethodInfo m20169_MI;
extern MethodInfo m20163_MI;
extern MethodInfo m20171_MI;
extern MethodInfo m20172_MI;
extern MethodInfo m20175_MI;
extern MethodInfo m20176_MI;
extern MethodInfo m20177_MI;
extern MethodInfo m20178_MI;
extern MethodInfo m20179_MI;
extern MethodInfo m20180_MI;
extern MethodInfo m20174_MI;
extern MethodInfo m20182_MI;
extern MethodInfo m20183_MI;
extern MethodInfo m20197_MI;
extern MethodInfo m20198_MI;
extern MethodInfo m20199_MI;
extern MethodInfo m20200_MI;
extern MethodInfo m20201_MI;
extern MethodInfo m20202_MI;
extern MethodInfo m20196_MI;
extern MethodInfo m20204_MI;
extern MethodInfo m20205_MI;
extern MethodInfo m20208_MI;
extern MethodInfo m20209_MI;
extern MethodInfo m20210_MI;
extern MethodInfo m20211_MI;
extern MethodInfo m20212_MI;
extern MethodInfo m20213_MI;
extern MethodInfo m20207_MI;
extern MethodInfo m20215_MI;
extern MethodInfo m20216_MI;
extern MethodInfo m20241_MI;
extern MethodInfo m20242_MI;
extern MethodInfo m20243_MI;
extern MethodInfo m20244_MI;
extern MethodInfo m20245_MI;
extern MethodInfo m20246_MI;
extern MethodInfo m20240_MI;
extern MethodInfo m20248_MI;
extern MethodInfo m20249_MI;
extern MethodInfo m20285_MI;
extern MethodInfo m20286_MI;
extern MethodInfo m20287_MI;
extern MethodInfo m20288_MI;
extern MethodInfo m20289_MI;
extern MethodInfo m20290_MI;
extern MethodInfo m20284_MI;
extern MethodInfo m20292_MI;
extern MethodInfo m20293_MI;
extern MethodInfo m20696_MI;
extern MethodInfo m20697_MI;
extern MethodInfo m20698_MI;
extern MethodInfo m20699_MI;
extern MethodInfo m20700_MI;
extern MethodInfo m20701_MI;
extern MethodInfo m20695_MI;
extern MethodInfo m20703_MI;
extern MethodInfo m20704_MI;
extern MethodInfo m19570_MI;
extern MethodInfo m19571_MI;
extern MethodInfo m19572_MI;
extern MethodInfo m19573_MI;
extern MethodInfo m19574_MI;
extern MethodInfo m19575_MI;
extern MethodInfo m19569_MI;
extern MethodInfo m19577_MI;
extern MethodInfo m19578_MI;
extern MethodInfo m19592_MI;
extern MethodInfo m19593_MI;
extern MethodInfo m19594_MI;
extern MethodInfo m19595_MI;
extern MethodInfo m19596_MI;
extern MethodInfo m19597_MI;
extern MethodInfo m19591_MI;
extern MethodInfo m19599_MI;
extern MethodInfo m19600_MI;
extern MethodInfo m20142_MI;
extern MethodInfo m20143_MI;
extern MethodInfo m20144_MI;
extern MethodInfo m20145_MI;
extern MethodInfo m20146_MI;
extern MethodInfo m20147_MI;
extern MethodInfo m20141_MI;
extern MethodInfo m20149_MI;
extern MethodInfo m20150_MI;
extern MethodInfo m20153_MI;
extern MethodInfo m20154_MI;
extern MethodInfo m20155_MI;
extern MethodInfo m20156_MI;
extern MethodInfo m20157_MI;
extern MethodInfo m20158_MI;
extern MethodInfo m20152_MI;
extern MethodInfo m20160_MI;
extern MethodInfo m20161_MI;
extern MethodInfo m20252_MI;
extern MethodInfo m20253_MI;
extern MethodInfo m20254_MI;
extern MethodInfo m20255_MI;
extern MethodInfo m20256_MI;
extern MethodInfo m20257_MI;
extern MethodInfo m20251_MI;
extern MethodInfo m20259_MI;
extern MethodInfo m20260_MI;
extern MethodInfo m20263_MI;
extern MethodInfo m20264_MI;
extern MethodInfo m20265_MI;
extern MethodInfo m20266_MI;
extern MethodInfo m20267_MI;
extern MethodInfo m20268_MI;
extern MethodInfo m20262_MI;
extern MethodInfo m20270_MI;
extern MethodInfo m20271_MI;
extern MethodInfo m20274_MI;
extern MethodInfo m20275_MI;
extern MethodInfo m20276_MI;
extern MethodInfo m20277_MI;
extern MethodInfo m20278_MI;
extern MethodInfo m20279_MI;
extern MethodInfo m20273_MI;
extern MethodInfo m20281_MI;
extern MethodInfo m20282_MI;
static MethodInfo* t3846_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21182_MI,
	&m5907_MI,
	&m21183_MI,
	&m21184_MI,
	&m21185_MI,
	&m21186_MI,
	&m21187_MI,
	&m5908_MI,
	&m21181_MI,
	&m21189_MI,
	&m21190_MI,
	&m5905_MI,
	&m5906_MI,
	&m19581_MI,
	&m5907_MI,
	&m19582_MI,
	&m19583_MI,
	&m19584_MI,
	&m19585_MI,
	&m19586_MI,
	&m5908_MI,
	&m19580_MI,
	&m19588_MI,
	&m19589_MI,
	&m5905_MI,
	&m5906_MI,
	&m20164_MI,
	&m5907_MI,
	&m20165_MI,
	&m20166_MI,
	&m20167_MI,
	&m20168_MI,
	&m20169_MI,
	&m5908_MI,
	&m20163_MI,
	&m20171_MI,
	&m20172_MI,
	&m5905_MI,
	&m5906_MI,
	&m20175_MI,
	&m5907_MI,
	&m20176_MI,
	&m20177_MI,
	&m20178_MI,
	&m20179_MI,
	&m20180_MI,
	&m5908_MI,
	&m20174_MI,
	&m20182_MI,
	&m20183_MI,
	&m5905_MI,
	&m5906_MI,
	&m20197_MI,
	&m5907_MI,
	&m20198_MI,
	&m20199_MI,
	&m20200_MI,
	&m20201_MI,
	&m20202_MI,
	&m5908_MI,
	&m20196_MI,
	&m20204_MI,
	&m20205_MI,
	&m5905_MI,
	&m5906_MI,
	&m20208_MI,
	&m5907_MI,
	&m20209_MI,
	&m20210_MI,
	&m20211_MI,
	&m20212_MI,
	&m20213_MI,
	&m5908_MI,
	&m20207_MI,
	&m20215_MI,
	&m20216_MI,
	&m5905_MI,
	&m5906_MI,
	&m20241_MI,
	&m5907_MI,
	&m20242_MI,
	&m20243_MI,
	&m20244_MI,
	&m20245_MI,
	&m20246_MI,
	&m5908_MI,
	&m20240_MI,
	&m20248_MI,
	&m20249_MI,
	&m5905_MI,
	&m5906_MI,
	&m20285_MI,
	&m5907_MI,
	&m20286_MI,
	&m20287_MI,
	&m20288_MI,
	&m20289_MI,
	&m20290_MI,
	&m5908_MI,
	&m20284_MI,
	&m20292_MI,
	&m20293_MI,
	&m5905_MI,
	&m5906_MI,
	&m20737_MI,
	&m5907_MI,
	&m20738_MI,
	&m20739_MI,
	&m20740_MI,
	&m20741_MI,
	&m20742_MI,
	&m5908_MI,
	&m20736_MI,
	&m20744_MI,
	&m20745_MI,
	&m5905_MI,
	&m5906_MI,
	&m20696_MI,
	&m5907_MI,
	&m20697_MI,
	&m20698_MI,
	&m20699_MI,
	&m20700_MI,
	&m20701_MI,
	&m5908_MI,
	&m20695_MI,
	&m20703_MI,
	&m20704_MI,
	&m5905_MI,
	&m5906_MI,
	&m19581_MI,
	&m5907_MI,
	&m19582_MI,
	&m19583_MI,
	&m19584_MI,
	&m19585_MI,
	&m19586_MI,
	&m5908_MI,
	&m19580_MI,
	&m19588_MI,
	&m19589_MI,
	&m5905_MI,
	&m5906_MI,
	&m19570_MI,
	&m5907_MI,
	&m19571_MI,
	&m19572_MI,
	&m19573_MI,
	&m19574_MI,
	&m19575_MI,
	&m5908_MI,
	&m19569_MI,
	&m19577_MI,
	&m19578_MI,
	&m5905_MI,
	&m5906_MI,
	&m19592_MI,
	&m5907_MI,
	&m19593_MI,
	&m19594_MI,
	&m19595_MI,
	&m19596_MI,
	&m19597_MI,
	&m5908_MI,
	&m19591_MI,
	&m19599_MI,
	&m19600_MI,
	&m5905_MI,
	&m5906_MI,
	&m20142_MI,
	&m5907_MI,
	&m20143_MI,
	&m20144_MI,
	&m20145_MI,
	&m20146_MI,
	&m20147_MI,
	&m5908_MI,
	&m20141_MI,
	&m20149_MI,
	&m20150_MI,
	&m5905_MI,
	&m5906_MI,
	&m20153_MI,
	&m5907_MI,
	&m20154_MI,
	&m20155_MI,
	&m20156_MI,
	&m20157_MI,
	&m20158_MI,
	&m5908_MI,
	&m20152_MI,
	&m20160_MI,
	&m20161_MI,
	&m5905_MI,
	&m5906_MI,
	&m20252_MI,
	&m5907_MI,
	&m20253_MI,
	&m20254_MI,
	&m20255_MI,
	&m20256_MI,
	&m20257_MI,
	&m5908_MI,
	&m20251_MI,
	&m20259_MI,
	&m20260_MI,
	&m5905_MI,
	&m5906_MI,
	&m20263_MI,
	&m5907_MI,
	&m20264_MI,
	&m20265_MI,
	&m20266_MI,
	&m20267_MI,
	&m20268_MI,
	&m5908_MI,
	&m20262_MI,
	&m20270_MI,
	&m20271_MI,
	&m5905_MI,
	&m5906_MI,
	&m20274_MI,
	&m5907_MI,
	&m20275_MI,
	&m20276_MI,
	&m20277_MI,
	&m20278_MI,
	&m20279_MI,
	&m5908_MI,
	&m20273_MI,
	&m20281_MI,
	&m20282_MI,
	&m5905_MI,
	&m5906_MI,
	&m19777_MI,
	&m5907_MI,
	&m19778_MI,
	&m19779_MI,
	&m19780_MI,
	&m19781_MI,
	&m19782_MI,
	&m5908_MI,
	&m19776_MI,
	&m19784_MI,
	&m19785_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5373_TI;
extern TypeInfo t5374_TI;
extern TypeInfo t5375_TI;
extern TypeInfo t2216_TI;
extern TypeInfo t312_TI;
extern TypeInfo t2217_TI;
extern TypeInfo t5187_TI;
extern TypeInfo t5188_TI;
extern TypeInfo t5189_TI;
extern TypeInfo t5190_TI;
extern TypeInfo t5191_TI;
extern TypeInfo t5192_TI;
extern TypeInfo t5196_TI;
extern TypeInfo t5197_TI;
extern TypeInfo t5198_TI;
extern TypeInfo t5199_TI;
extern TypeInfo t5200_TI;
extern TypeInfo t5201_TI;
extern TypeInfo t5208_TI;
extern TypeInfo t5209_TI;
extern TypeInfo t5210_TI;
extern TypeInfo t5220_TI;
extern TypeInfo t5221_TI;
extern TypeInfo t5222_TI;
extern TypeInfo t2640_TI;
extern TypeInfo t2646_TI;
extern TypeInfo t2641_TI;
extern TypeInfo t5071_TI;
extern TypeInfo t5072_TI;
extern TypeInfo t5073_TI;
extern TypeInfo t5074_TI;
extern TypeInfo t5075_TI;
extern TypeInfo t5076_TI;
extern TypeInfo t5181_TI;
extern TypeInfo t5182_TI;
extern TypeInfo t5183_TI;
extern TypeInfo t5184_TI;
extern TypeInfo t5185_TI;
extern TypeInfo t5186_TI;
extern TypeInfo t5211_TI;
extern TypeInfo t5212_TI;
extern TypeInfo t5213_TI;
extern TypeInfo t5214_TI;
extern TypeInfo t5215_TI;
extern TypeInfo t5216_TI;
extern TypeInfo t5217_TI;
extern TypeInfo t5218_TI;
extern TypeInfo t5219_TI;
static TypeInfo* t3846_ITIs[] = 
{
	&t5373_TI,
	&t5374_TI,
	&t5375_TI,
	&t2216_TI,
	&t312_TI,
	&t2217_TI,
	&t5187_TI,
	&t5188_TI,
	&t5189_TI,
	&t5190_TI,
	&t5191_TI,
	&t5192_TI,
	&t5196_TI,
	&t5197_TI,
	&t5198_TI,
	&t5199_TI,
	&t5200_TI,
	&t5201_TI,
	&t5208_TI,
	&t5209_TI,
	&t5210_TI,
	&t5220_TI,
	&t5221_TI,
	&t5222_TI,
	&t2430_TI,
	&t2434_TI,
	&t2431_TI,
	&t2640_TI,
	&t2646_TI,
	&t2641_TI,
	&t2216_TI,
	&t312_TI,
	&t2217_TI,
	&t5071_TI,
	&t5072_TI,
	&t5073_TI,
	&t5074_TI,
	&t5075_TI,
	&t5076_TI,
	&t5181_TI,
	&t5182_TI,
	&t5183_TI,
	&t5184_TI,
	&t5185_TI,
	&t5186_TI,
	&t5211_TI,
	&t5212_TI,
	&t5213_TI,
	&t5214_TI,
	&t5215_TI,
	&t5216_TI,
	&t5217_TI,
	&t5218_TI,
	&t5219_TI,
	&t5122_TI,
	&t5123_TI,
	&t5124_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3846_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5373_TI, 21},
	{ &t5374_TI, 28},
	{ &t5375_TI, 33},
	{ &t2216_TI, 34},
	{ &t312_TI, 41},
	{ &t2217_TI, 46},
	{ &t5187_TI, 47},
	{ &t5188_TI, 54},
	{ &t5189_TI, 59},
	{ &t5190_TI, 60},
	{ &t5191_TI, 67},
	{ &t5192_TI, 72},
	{ &t5196_TI, 73},
	{ &t5197_TI, 80},
	{ &t5198_TI, 85},
	{ &t5199_TI, 86},
	{ &t5200_TI, 93},
	{ &t5201_TI, 98},
	{ &t5208_TI, 99},
	{ &t5209_TI, 106},
	{ &t5210_TI, 111},
	{ &t5220_TI, 112},
	{ &t5221_TI, 119},
	{ &t5222_TI, 124},
	{ &t2430_TI, 125},
	{ &t2434_TI, 132},
	{ &t2431_TI, 137},
	{ &t2640_TI, 138},
	{ &t2646_TI, 145},
	{ &t2641_TI, 150},
	{ &t2216_TI, 151},
	{ &t312_TI, 158},
	{ &t2217_TI, 163},
	{ &t5071_TI, 164},
	{ &t5072_TI, 171},
	{ &t5073_TI, 176},
	{ &t5074_TI, 177},
	{ &t5075_TI, 184},
	{ &t5076_TI, 189},
	{ &t5181_TI, 190},
	{ &t5182_TI, 197},
	{ &t5183_TI, 202},
	{ &t5184_TI, 203},
	{ &t5185_TI, 210},
	{ &t5186_TI, 215},
	{ &t5211_TI, 216},
	{ &t5212_TI, 223},
	{ &t5213_TI, 228},
	{ &t5214_TI, 229},
	{ &t5215_TI, 236},
	{ &t5216_TI, 241},
	{ &t5217_TI, 242},
	{ &t5218_TI, 249},
	{ &t5219_TI, 254},
	{ &t5122_TI, 255},
	{ &t5123_TI, 262},
	{ &t5124_TI, 267},
	{ &t5059_TI, 268},
	{ &t5060_TI, 275},
	{ &t5061_TI, 280},
	{ &t5062_TI, 281},
	{ &t5063_TI, 288},
	{ &t5064_TI, 293},
	{ &t2240_TI, 294},
	{ &t2245_TI, 301},
	{ &t2241_TI, 306},
	{ &t5065_TI, 307},
	{ &t5066_TI, 314},
	{ &t5067_TI, 319},
	{ &t2182_TI, 320},
	{ &t2186_TI, 327},
	{ &t2183_TI, 332},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3846_0_0_0;
extern Il2CppType t3846_1_0_0;
struct t197;
extern TypeInfo t197_TI;
extern CustomAttributesCache t197__CustomAttributeCache;
extern CustomAttributesCache t197__CustomAttributeCache_m_TextComponent;
extern CustomAttributesCache t197__CustomAttributeCache_m_Placeholder;
extern CustomAttributesCache t197__CustomAttributeCache_m_ContentType;
extern CustomAttributesCache t197__CustomAttributeCache_m_InputType;
extern CustomAttributesCache t197__CustomAttributeCache_m_AsteriskChar;
extern CustomAttributesCache t197__CustomAttributeCache_m_KeyboardType;
extern CustomAttributesCache t197__CustomAttributeCache_m_LineType;
extern CustomAttributesCache t197__CustomAttributeCache_m_HideMobileInput;
extern CustomAttributesCache t197__CustomAttributeCache_m_CharacterValidation;
extern CustomAttributesCache t197__CustomAttributeCache_m_CharacterLimit;
extern CustomAttributesCache t197__CustomAttributeCache_m_EndEdit;
extern CustomAttributesCache t197__CustomAttributeCache_m_OnValueChange;
extern CustomAttributesCache t197__CustomAttributeCache_m_OnValidateInput;
extern CustomAttributesCache t197__CustomAttributeCache_m_SelectionColor;
extern CustomAttributesCache t197__CustomAttributeCache_m_Text;
extern CustomAttributesCache t197__CustomAttributeCache_m_CaretBlinkRate;
extern CustomAttributesCache t197__CustomAttributeCache_m620;
extern CustomAttributesCache t197__CustomAttributeCache_m637;
TypeInfo t3846_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "InputField[]", "UnityEngine.UI", t3846_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t197_TI, t3846_ITIs, t3846_VT, &EmptyCustomAttributesCache, &t3846_TI, &t3846_0_0_0, &t3846_1_0_0, t3846_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t197 *), -1, sizeof(t3846_SFs), 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, true, false, false, 0, 0, 0, 0, 0, 333, 72, 76};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t206_TI;



// Metadata Definition UnityEngine.UI.InputField/ContentType[]
static MethodInfo* t206_MIs[] =
{
	NULL
};
extern MethodInfo m21194_MI;
extern MethodInfo m21195_MI;
extern MethodInfo m21196_MI;
extern MethodInfo m21197_MI;
extern MethodInfo m21198_MI;
extern MethodInfo m21199_MI;
extern MethodInfo m21193_MI;
extern MethodInfo m21201_MI;
extern MethodInfo m21202_MI;
static MethodInfo* t206_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21194_MI,
	&m5907_MI,
	&m21195_MI,
	&m21196_MI,
	&m21197_MI,
	&m21198_MI,
	&m21199_MI,
	&m5908_MI,
	&m21193_MI,
	&m21201_MI,
	&m21202_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5376_TI;
extern TypeInfo t5377_TI;
extern TypeInfo t5378_TI;
static TypeInfo* t206_ITIs[] = 
{
	&t5376_TI,
	&t5377_TI,
	&t5378_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t206_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5376_TI, 21},
	{ &t5377_TI, 28},
	{ &t5378_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t206_0_0_0;
extern Il2CppType t206_1_0_0;
#include "t185.h"
extern TypeInfo t185_TI;
TypeInfo t206_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "ContentType[]", "", t206_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t185_TI, t206_ITIs, t206_VT, &EmptyCustomAttributesCache, &t44_TI, &t206_0_0_0, &t206_1_0_0, t206_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 258, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t531_TI;



// Metadata Definition UnityEngine.UILineInfo[]
static MethodInfo* t531_MIs[] =
{
	NULL
};
extern MethodInfo m21205_MI;
extern MethodInfo m21206_MI;
extern MethodInfo m21207_MI;
extern MethodInfo m21208_MI;
extern MethodInfo m21209_MI;
extern MethodInfo m21210_MI;
extern MethodInfo m21204_MI;
extern MethodInfo m21212_MI;
extern MethodInfo m21213_MI;
static MethodInfo* t531_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21205_MI,
	&m5907_MI,
	&m21206_MI,
	&m21207_MI,
	&m21208_MI,
	&m21209_MI,
	&m21210_MI,
	&m5908_MI,
	&m21204_MI,
	&m21212_MI,
	&m21213_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t389_TI;
extern TypeInfo t387_TI;
extern TypeInfo t3042_TI;
static TypeInfo* t531_ITIs[] = 
{
	&t389_TI,
	&t387_TI,
	&t3042_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t531_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t389_TI, 21},
	{ &t387_TI, 28},
	{ &t3042_TI, 33},
	{ &t5116_TI, 34},
	{ &t5117_TI, 41},
	{ &t5118_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t531_0_0_0;
extern Il2CppType t531_1_0_0;
#include "t380.h"
extern TypeInfo t380_TI;
TypeInfo t531_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UILineInfo[]", "UnityEngine", t531_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t380_TI, t531_ITIs, t531_VT, &EmptyCustomAttributesCache, &t531_TI, &t531_0_0_0, &t531_1_0_0, t531_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t380 ), -1, 0, 0, -1, 1048841, 1, false, false, false, false, false, true, false, false, false, false, false, true, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t530_TI;



// Metadata Definition UnityEngine.UICharInfo[]
static MethodInfo* t530_MIs[] =
{
	NULL
};
extern MethodInfo m21216_MI;
extern MethodInfo m21217_MI;
extern MethodInfo m21218_MI;
extern MethodInfo m21219_MI;
extern MethodInfo m21220_MI;
extern MethodInfo m21221_MI;
extern MethodInfo m21215_MI;
extern MethodInfo m21223_MI;
extern MethodInfo m21224_MI;
static MethodInfo* t530_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21216_MI,
	&m5907_MI,
	&m21217_MI,
	&m21218_MI,
	&m21219_MI,
	&m21220_MI,
	&m21221_MI,
	&m5908_MI,
	&m21215_MI,
	&m21223_MI,
	&m21224_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t3030_TI;
extern TypeInfo t388_TI;
extern TypeInfo t3031_TI;
static TypeInfo* t530_ITIs[] = 
{
	&t3030_TI,
	&t388_TI,
	&t3031_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t530_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t3030_TI, 21},
	{ &t388_TI, 28},
	{ &t3031_TI, 33},
	{ &t5116_TI, 34},
	{ &t5117_TI, 41},
	{ &t5118_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t530_0_0_0;
extern Il2CppType t530_1_0_0;
#include "t381.h"
extern TypeInfo t381_TI;
TypeInfo t530_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UICharInfo[]", "UnityEngine", t530_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t381_TI, t530_ITIs, t530_VT, &EmptyCustomAttributesCache, &t530_TI, &t530_0_0_0, &t530_1_0_0, t530_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t381 ), -1, 0, 0, -1, 1048841, 1, false, false, false, false, false, true, false, false, false, false, false, true, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3847_TI;



// Metadata Definition UnityEngine.UI.InputField/InputType[]
static MethodInfo* t3847_MIs[] =
{
	NULL
};
extern MethodInfo m21228_MI;
extern MethodInfo m21229_MI;
extern MethodInfo m21230_MI;
extern MethodInfo m21231_MI;
extern MethodInfo m21232_MI;
extern MethodInfo m21233_MI;
extern MethodInfo m21227_MI;
extern MethodInfo m21235_MI;
extern MethodInfo m21236_MI;
static MethodInfo* t3847_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21228_MI,
	&m5907_MI,
	&m21229_MI,
	&m21230_MI,
	&m21231_MI,
	&m21232_MI,
	&m21233_MI,
	&m5908_MI,
	&m21227_MI,
	&m21235_MI,
	&m21236_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5379_TI;
extern TypeInfo t5380_TI;
extern TypeInfo t5381_TI;
static TypeInfo* t3847_ITIs[] = 
{
	&t5379_TI,
	&t5380_TI,
	&t5381_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3847_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5379_TI, 21},
	{ &t5380_TI, 28},
	{ &t5381_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3847_0_0_0;
extern Il2CppType t3847_1_0_0;
#include "t186.h"
extern TypeInfo t186_TI;
TypeInfo t3847_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "InputType[]", "", t3847_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t186_TI, t3847_ITIs, t3847_VT, &EmptyCustomAttributesCache, &t44_TI, &t3847_0_0_0, &t3847_1_0_0, t3847_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 258, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3848_TI;



// Metadata Definition UnityEngine.UI.InputField/CharacterValidation[]
static MethodInfo* t3848_MIs[] =
{
	NULL
};
extern MethodInfo m21239_MI;
extern MethodInfo m21240_MI;
extern MethodInfo m21241_MI;
extern MethodInfo m21242_MI;
extern MethodInfo m21243_MI;
extern MethodInfo m21244_MI;
extern MethodInfo m21238_MI;
extern MethodInfo m21246_MI;
extern MethodInfo m21247_MI;
static MethodInfo* t3848_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21239_MI,
	&m5907_MI,
	&m21240_MI,
	&m21241_MI,
	&m21242_MI,
	&m21243_MI,
	&m21244_MI,
	&m5908_MI,
	&m21238_MI,
	&m21246_MI,
	&m21247_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5382_TI;
extern TypeInfo t5383_TI;
extern TypeInfo t5384_TI;
static TypeInfo* t3848_ITIs[] = 
{
	&t5382_TI,
	&t5383_TI,
	&t5384_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3848_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5382_TI, 21},
	{ &t5383_TI, 28},
	{ &t5384_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3848_0_0_0;
extern Il2CppType t3848_1_0_0;
#include "t187.h"
extern TypeInfo t187_TI;
TypeInfo t3848_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "CharacterValidation[]", "", t3848_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t187_TI, t3848_ITIs, t3848_VT, &EmptyCustomAttributesCache, &t44_TI, &t3848_0_0_0, &t3848_1_0_0, t3848_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 258, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3849_TI;



// Metadata Definition UnityEngine.UI.InputField/LineType[]
static MethodInfo* t3849_MIs[] =
{
	NULL
};
extern MethodInfo m21250_MI;
extern MethodInfo m21251_MI;
extern MethodInfo m21252_MI;
extern MethodInfo m21253_MI;
extern MethodInfo m21254_MI;
extern MethodInfo m21255_MI;
extern MethodInfo m21249_MI;
extern MethodInfo m21257_MI;
extern MethodInfo m21258_MI;
static MethodInfo* t3849_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21250_MI,
	&m5907_MI,
	&m21251_MI,
	&m21252_MI,
	&m21253_MI,
	&m21254_MI,
	&m21255_MI,
	&m5908_MI,
	&m21249_MI,
	&m21257_MI,
	&m21258_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5385_TI;
extern TypeInfo t5386_TI;
extern TypeInfo t5387_TI;
static TypeInfo* t3849_ITIs[] = 
{
	&t5385_TI,
	&t5386_TI,
	&t5387_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3849_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5385_TI, 21},
	{ &t5386_TI, 28},
	{ &t5387_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3849_0_0_0;
extern Il2CppType t3849_1_0_0;
#include "t188.h"
extern TypeInfo t188_TI;
TypeInfo t3849_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "LineType[]", "", t3849_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t188_TI, t3849_ITIs, t3849_VT, &EmptyCustomAttributesCache, &t44_TI, &t3849_0_0_0, &t3849_1_0_0, t3849_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 258, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3850_TI;



// Metadata Definition UnityEngine.UI.InputField/EditState[]
static MethodInfo* t3850_MIs[] =
{
	NULL
};
extern MethodInfo m21261_MI;
extern MethodInfo m21262_MI;
extern MethodInfo m21263_MI;
extern MethodInfo m21264_MI;
extern MethodInfo m21265_MI;
extern MethodInfo m21266_MI;
extern MethodInfo m21260_MI;
extern MethodInfo m21268_MI;
extern MethodInfo m21269_MI;
static MethodInfo* t3850_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21261_MI,
	&m5907_MI,
	&m21262_MI,
	&m21263_MI,
	&m21264_MI,
	&m21265_MI,
	&m21266_MI,
	&m5908_MI,
	&m21260_MI,
	&m21268_MI,
	&m21269_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5388_TI;
extern TypeInfo t5389_TI;
extern TypeInfo t5390_TI;
static TypeInfo* t3850_ITIs[] = 
{
	&t5388_TI,
	&t5389_TI,
	&t5390_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3850_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5388_TI, 21},
	{ &t5389_TI, 28},
	{ &t5390_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3850_0_0_0;
extern Il2CppType t3850_1_0_0;
#include "t192.h"
extern TypeInfo t192_TI;
TypeInfo t3850_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "EditState[]", "", t3850_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t192_TI, t3850_ITIs, t3850_VT, &EmptyCustomAttributesCache, &t44_TI, &t3850_0_0_0, &t3850_1_0_0, t3850_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 260, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3851_TI;



// Metadata Definition UnityEngine.UI.Navigation/Mode[]
static MethodInfo* t3851_MIs[] =
{
	NULL
};
extern MethodInfo m21273_MI;
extern MethodInfo m21274_MI;
extern MethodInfo m21275_MI;
extern MethodInfo m21276_MI;
extern MethodInfo m21277_MI;
extern MethodInfo m21278_MI;
extern MethodInfo m21272_MI;
extern MethodInfo m21280_MI;
extern MethodInfo m21281_MI;
static MethodInfo* t3851_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21273_MI,
	&m5907_MI,
	&m21274_MI,
	&m21275_MI,
	&m21276_MI,
	&m21277_MI,
	&m21278_MI,
	&m5908_MI,
	&m21272_MI,
	&m21280_MI,
	&m21281_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5391_TI;
extern TypeInfo t5392_TI;
extern TypeInfo t5393_TI;
static TypeInfo* t3851_ITIs[] = 
{
	&t5391_TI,
	&t5392_TI,
	&t5393_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3851_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5391_TI, 21},
	{ &t5392_TI, 28},
	{ &t5393_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3851_0_0_0;
extern Il2CppType t3851_1_0_0;
#include "t209.h"
extern TypeInfo t209_TI;
extern CustomAttributesCache t209__CustomAttributeCache;
TypeInfo t3851_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Mode[]", "", t3851_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t209_TI, t3851_ITIs, t3851_VT, &EmptyCustomAttributesCache, &t44_TI, &t3851_0_0_0, &t3851_1_0_0, t3851_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 258, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3852_TI;



// Metadata Definition UnityEngine.UI.RawImage[]
static MethodInfo* t3852_MIs[] =
{
	NULL
};
extern MethodInfo m21284_MI;
extern MethodInfo m21285_MI;
extern MethodInfo m21286_MI;
extern MethodInfo m21287_MI;
extern MethodInfo m21288_MI;
extern MethodInfo m21289_MI;
extern MethodInfo m21283_MI;
extern MethodInfo m21291_MI;
extern MethodInfo m21292_MI;
static MethodInfo* t3852_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21284_MI,
	&m5907_MI,
	&m21285_MI,
	&m21286_MI,
	&m21287_MI,
	&m21288_MI,
	&m21289_MI,
	&m5908_MI,
	&m21283_MI,
	&m21291_MI,
	&m21292_MI,
	&m5905_MI,
	&m5906_MI,
	&m20820_MI,
	&m5907_MI,
	&m20821_MI,
	&m20822_MI,
	&m20823_MI,
	&m20824_MI,
	&m20825_MI,
	&m5908_MI,
	&m20819_MI,
	&m20827_MI,
	&m20828_MI,
	&m5905_MI,
	&m5906_MI,
	&m20831_MI,
	&m5907_MI,
	&m20832_MI,
	&m20833_MI,
	&m20834_MI,
	&m20835_MI,
	&m20836_MI,
	&m5908_MI,
	&m20830_MI,
	&m20838_MI,
	&m20839_MI,
	&m5905_MI,
	&m5906_MI,
	&m20842_MI,
	&m5907_MI,
	&m20843_MI,
	&m20844_MI,
	&m20845_MI,
	&m20846_MI,
	&m20847_MI,
	&m5908_MI,
	&m20841_MI,
	&m20849_MI,
	&m20850_MI,
	&m5905_MI,
	&m5906_MI,
	&m20737_MI,
	&m5907_MI,
	&m20738_MI,
	&m20739_MI,
	&m20740_MI,
	&m20741_MI,
	&m20742_MI,
	&m5908_MI,
	&m20736_MI,
	&m20744_MI,
	&m20745_MI,
	&m5905_MI,
	&m5906_MI,
	&m19777_MI,
	&m5907_MI,
	&m19778_MI,
	&m19779_MI,
	&m19780_MI,
	&m19781_MI,
	&m19782_MI,
	&m5908_MI,
	&m19776_MI,
	&m19784_MI,
	&m19785_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5394_TI;
extern TypeInfo t5395_TI;
extern TypeInfo t5396_TI;
static TypeInfo* t3852_ITIs[] = 
{
	&t5394_TI,
	&t5395_TI,
	&t5396_TI,
	&t5307_TI,
	&t5308_TI,
	&t5309_TI,
	&t5310_TI,
	&t5311_TI,
	&t5312_TI,
	&t363_TI,
	&t171_TI,
	&t2547_TI,
	&t2430_TI,
	&t2434_TI,
	&t2431_TI,
	&t5122_TI,
	&t5123_TI,
	&t5124_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3852_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5394_TI, 21},
	{ &t5395_TI, 28},
	{ &t5396_TI, 33},
	{ &t5307_TI, 34},
	{ &t5308_TI, 41},
	{ &t5309_TI, 46},
	{ &t5310_TI, 47},
	{ &t5311_TI, 54},
	{ &t5312_TI, 59},
	{ &t363_TI, 60},
	{ &t171_TI, 67},
	{ &t2547_TI, 72},
	{ &t2430_TI, 73},
	{ &t2434_TI, 80},
	{ &t2431_TI, 85},
	{ &t5122_TI, 86},
	{ &t5123_TI, 93},
	{ &t5124_TI, 98},
	{ &t5059_TI, 99},
	{ &t5060_TI, 106},
	{ &t5061_TI, 111},
	{ &t5062_TI, 112},
	{ &t5063_TI, 119},
	{ &t5064_TI, 124},
	{ &t2240_TI, 125},
	{ &t2245_TI, 132},
	{ &t2241_TI, 137},
	{ &t5065_TI, 138},
	{ &t5066_TI, 145},
	{ &t5067_TI, 150},
	{ &t2182_TI, 151},
	{ &t2186_TI, 158},
	{ &t2183_TI, 163},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3852_0_0_0;
extern Il2CppType t3852_1_0_0;
struct t211;
extern TypeInfo t211_TI;
extern CustomAttributesCache t211__CustomAttributeCache;
extern CustomAttributesCache t211__CustomAttributeCache_m_Texture;
extern CustomAttributesCache t211__CustomAttributeCache_m_UVRect;
TypeInfo t3852_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "RawImage[]", "UnityEngine.UI", t3852_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t211_TI, t3852_ITIs, t3852_VT, &EmptyCustomAttributesCache, &t3852_TI, &t3852_0_0_0, &t3852_1_0_0, t3852_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t211 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 164, 33, 37};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3853_TI;



// Metadata Definition UnityEngine.UI.Scrollbar[]
static MethodInfo* t3853_MIs[] =
{
	NULL
};
extern MethodInfo m21296_MI;
extern MethodInfo m21297_MI;
extern MethodInfo m21298_MI;
extern MethodInfo m21299_MI;
extern MethodInfo m21300_MI;
extern MethodInfo m21301_MI;
extern MethodInfo m21295_MI;
extern MethodInfo m21303_MI;
extern MethodInfo m21304_MI;
extern MethodInfo m20186_MI;
extern MethodInfo m20187_MI;
extern MethodInfo m20188_MI;
extern MethodInfo m20189_MI;
extern MethodInfo m20190_MI;
extern MethodInfo m20191_MI;
extern MethodInfo m20185_MI;
extern MethodInfo m20193_MI;
extern MethodInfo m20194_MI;
static MethodInfo* t3853_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21296_MI,
	&m5907_MI,
	&m21297_MI,
	&m21298_MI,
	&m21299_MI,
	&m21300_MI,
	&m21301_MI,
	&m5908_MI,
	&m21295_MI,
	&m21303_MI,
	&m21304_MI,
	&m5905_MI,
	&m5906_MI,
	&m19581_MI,
	&m5907_MI,
	&m19582_MI,
	&m19583_MI,
	&m19584_MI,
	&m19585_MI,
	&m19586_MI,
	&m5908_MI,
	&m19580_MI,
	&m19588_MI,
	&m19589_MI,
	&m5905_MI,
	&m5906_MI,
	&m20175_MI,
	&m5907_MI,
	&m20176_MI,
	&m20177_MI,
	&m20178_MI,
	&m20179_MI,
	&m20180_MI,
	&m5908_MI,
	&m20174_MI,
	&m20182_MI,
	&m20183_MI,
	&m5905_MI,
	&m5906_MI,
	&m20186_MI,
	&m5907_MI,
	&m20187_MI,
	&m20188_MI,
	&m20189_MI,
	&m20190_MI,
	&m20191_MI,
	&m5908_MI,
	&m20185_MI,
	&m20193_MI,
	&m20194_MI,
	&m5905_MI,
	&m5906_MI,
	&m20197_MI,
	&m5907_MI,
	&m20198_MI,
	&m20199_MI,
	&m20200_MI,
	&m20201_MI,
	&m20202_MI,
	&m5908_MI,
	&m20196_MI,
	&m20204_MI,
	&m20205_MI,
	&m5905_MI,
	&m5906_MI,
	&m20737_MI,
	&m5907_MI,
	&m20738_MI,
	&m20739_MI,
	&m20740_MI,
	&m20741_MI,
	&m20742_MI,
	&m5908_MI,
	&m20736_MI,
	&m20744_MI,
	&m20745_MI,
	&m5905_MI,
	&m5906_MI,
	&m20696_MI,
	&m5907_MI,
	&m20697_MI,
	&m20698_MI,
	&m20699_MI,
	&m20700_MI,
	&m20701_MI,
	&m5908_MI,
	&m20695_MI,
	&m20703_MI,
	&m20704_MI,
	&m5905_MI,
	&m5906_MI,
	&m19581_MI,
	&m5907_MI,
	&m19582_MI,
	&m19583_MI,
	&m19584_MI,
	&m19585_MI,
	&m19586_MI,
	&m5908_MI,
	&m19580_MI,
	&m19588_MI,
	&m19589_MI,
	&m5905_MI,
	&m5906_MI,
	&m19570_MI,
	&m5907_MI,
	&m19571_MI,
	&m19572_MI,
	&m19573_MI,
	&m19574_MI,
	&m19575_MI,
	&m5908_MI,
	&m19569_MI,
	&m19577_MI,
	&m19578_MI,
	&m5905_MI,
	&m5906_MI,
	&m19592_MI,
	&m5907_MI,
	&m19593_MI,
	&m19594_MI,
	&m19595_MI,
	&m19596_MI,
	&m19597_MI,
	&m5908_MI,
	&m19591_MI,
	&m19599_MI,
	&m19600_MI,
	&m5905_MI,
	&m5906_MI,
	&m20142_MI,
	&m5907_MI,
	&m20143_MI,
	&m20144_MI,
	&m20145_MI,
	&m20146_MI,
	&m20147_MI,
	&m5908_MI,
	&m20141_MI,
	&m20149_MI,
	&m20150_MI,
	&m5905_MI,
	&m5906_MI,
	&m20153_MI,
	&m5907_MI,
	&m20154_MI,
	&m20155_MI,
	&m20156_MI,
	&m20157_MI,
	&m20158_MI,
	&m5908_MI,
	&m20152_MI,
	&m20160_MI,
	&m20161_MI,
	&m5905_MI,
	&m5906_MI,
	&m20252_MI,
	&m5907_MI,
	&m20253_MI,
	&m20254_MI,
	&m20255_MI,
	&m20256_MI,
	&m20257_MI,
	&m5908_MI,
	&m20251_MI,
	&m20259_MI,
	&m20260_MI,
	&m5905_MI,
	&m5906_MI,
	&m20263_MI,
	&m5907_MI,
	&m20264_MI,
	&m20265_MI,
	&m20266_MI,
	&m20267_MI,
	&m20268_MI,
	&m5908_MI,
	&m20262_MI,
	&m20270_MI,
	&m20271_MI,
	&m5905_MI,
	&m5906_MI,
	&m20274_MI,
	&m5907_MI,
	&m20275_MI,
	&m20276_MI,
	&m20277_MI,
	&m20278_MI,
	&m20279_MI,
	&m5908_MI,
	&m20273_MI,
	&m20281_MI,
	&m20282_MI,
	&m5905_MI,
	&m5906_MI,
	&m19777_MI,
	&m5907_MI,
	&m19778_MI,
	&m19779_MI,
	&m19780_MI,
	&m19781_MI,
	&m19782_MI,
	&m5908_MI,
	&m19776_MI,
	&m19784_MI,
	&m19785_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5397_TI;
extern TypeInfo t5398_TI;
extern TypeInfo t5399_TI;
extern TypeInfo t5193_TI;
extern TypeInfo t5194_TI;
extern TypeInfo t5195_TI;
static TypeInfo* t3853_ITIs[] = 
{
	&t5397_TI,
	&t5398_TI,
	&t5399_TI,
	&t2216_TI,
	&t312_TI,
	&t2217_TI,
	&t5190_TI,
	&t5191_TI,
	&t5192_TI,
	&t5193_TI,
	&t5194_TI,
	&t5195_TI,
	&t5196_TI,
	&t5197_TI,
	&t5198_TI,
	&t2430_TI,
	&t2434_TI,
	&t2431_TI,
	&t2640_TI,
	&t2646_TI,
	&t2641_TI,
	&t2216_TI,
	&t312_TI,
	&t2217_TI,
	&t5071_TI,
	&t5072_TI,
	&t5073_TI,
	&t5074_TI,
	&t5075_TI,
	&t5076_TI,
	&t5181_TI,
	&t5182_TI,
	&t5183_TI,
	&t5184_TI,
	&t5185_TI,
	&t5186_TI,
	&t5211_TI,
	&t5212_TI,
	&t5213_TI,
	&t5214_TI,
	&t5215_TI,
	&t5216_TI,
	&t5217_TI,
	&t5218_TI,
	&t5219_TI,
	&t5122_TI,
	&t5123_TI,
	&t5124_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3853_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5397_TI, 21},
	{ &t5398_TI, 28},
	{ &t5399_TI, 33},
	{ &t2216_TI, 34},
	{ &t312_TI, 41},
	{ &t2217_TI, 46},
	{ &t5190_TI, 47},
	{ &t5191_TI, 54},
	{ &t5192_TI, 59},
	{ &t5193_TI, 60},
	{ &t5194_TI, 67},
	{ &t5195_TI, 72},
	{ &t5196_TI, 73},
	{ &t5197_TI, 80},
	{ &t5198_TI, 85},
	{ &t2430_TI, 86},
	{ &t2434_TI, 93},
	{ &t2431_TI, 98},
	{ &t2640_TI, 99},
	{ &t2646_TI, 106},
	{ &t2641_TI, 111},
	{ &t2216_TI, 112},
	{ &t312_TI, 119},
	{ &t2217_TI, 124},
	{ &t5071_TI, 125},
	{ &t5072_TI, 132},
	{ &t5073_TI, 137},
	{ &t5074_TI, 138},
	{ &t5075_TI, 145},
	{ &t5076_TI, 150},
	{ &t5181_TI, 151},
	{ &t5182_TI, 158},
	{ &t5183_TI, 163},
	{ &t5184_TI, 164},
	{ &t5185_TI, 171},
	{ &t5186_TI, 176},
	{ &t5211_TI, 177},
	{ &t5212_TI, 184},
	{ &t5213_TI, 189},
	{ &t5214_TI, 190},
	{ &t5215_TI, 197},
	{ &t5216_TI, 202},
	{ &t5217_TI, 203},
	{ &t5218_TI, 210},
	{ &t5219_TI, 215},
	{ &t5122_TI, 216},
	{ &t5123_TI, 223},
	{ &t5124_TI, 228},
	{ &t5059_TI, 229},
	{ &t5060_TI, 236},
	{ &t5061_TI, 241},
	{ &t5062_TI, 242},
	{ &t5063_TI, 249},
	{ &t5064_TI, 254},
	{ &t2240_TI, 255},
	{ &t2245_TI, 262},
	{ &t2241_TI, 267},
	{ &t5065_TI, 268},
	{ &t5066_TI, 275},
	{ &t5067_TI, 280},
	{ &t2182_TI, 281},
	{ &t2186_TI, 288},
	{ &t2183_TI, 293},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3853_0_0_0;
extern Il2CppType t3853_1_0_0;
struct t217;
extern TypeInfo t217_TI;
extern CustomAttributesCache t217__CustomAttributeCache;
extern CustomAttributesCache t217__CustomAttributeCache_m_HandleRect;
extern CustomAttributesCache t217__CustomAttributeCache_m_Direction;
extern CustomAttributesCache t217__CustomAttributeCache_m_Value;
extern CustomAttributesCache t217__CustomAttributeCache_m_Size;
extern CustomAttributesCache t217__CustomAttributeCache_m_NumberOfSteps;
extern CustomAttributesCache t217__CustomAttributeCache_m_OnValueChanged;
extern CustomAttributesCache t217__CustomAttributeCache_m763;
TypeInfo t3853_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Scrollbar[]", "UnityEngine.UI", t3853_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t217_TI, t3853_ITIs, t3853_VT, &EmptyCustomAttributesCache, &t3853_TI, &t3853_0_0_0, &t3853_1_0_0, t3853_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t217 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 294, 63, 67};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3854_TI;



// Metadata Definition UnityEngine.UI.Scrollbar/Direction[]
static MethodInfo* t3854_MIs[] =
{
	NULL
};
extern MethodInfo m21309_MI;
extern MethodInfo m21310_MI;
extern MethodInfo m21311_MI;
extern MethodInfo m21312_MI;
extern MethodInfo m21313_MI;
extern MethodInfo m21314_MI;
extern MethodInfo m21308_MI;
extern MethodInfo m21316_MI;
extern MethodInfo m21317_MI;
static MethodInfo* t3854_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21309_MI,
	&m5907_MI,
	&m21310_MI,
	&m21311_MI,
	&m21312_MI,
	&m21313_MI,
	&m21314_MI,
	&m5908_MI,
	&m21308_MI,
	&m21316_MI,
	&m21317_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5400_TI;
extern TypeInfo t5401_TI;
extern TypeInfo t5402_TI;
static TypeInfo* t3854_ITIs[] = 
{
	&t5400_TI,
	&t5401_TI,
	&t5402_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3854_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5400_TI, 21},
	{ &t5401_TI, 28},
	{ &t5402_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3854_0_0_0;
extern Il2CppType t3854_1_0_0;
#include "t212.h"
extern TypeInfo t212_TI;
TypeInfo t3854_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Direction[]", "", t3854_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t212_TI, t3854_ITIs, t3854_VT, &EmptyCustomAttributesCache, &t44_TI, &t3854_0_0_0, &t3854_1_0_0, t3854_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 258, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3855_TI;



// Metadata Definition UnityEngine.UI.Scrollbar/Axis[]
static MethodInfo* t3855_MIs[] =
{
	NULL
};
extern MethodInfo m21320_MI;
extern MethodInfo m21321_MI;
extern MethodInfo m21322_MI;
extern MethodInfo m21323_MI;
extern MethodInfo m21324_MI;
extern MethodInfo m21325_MI;
extern MethodInfo m21319_MI;
extern MethodInfo m21327_MI;
extern MethodInfo m21328_MI;
static MethodInfo* t3855_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21320_MI,
	&m5907_MI,
	&m21321_MI,
	&m21322_MI,
	&m21323_MI,
	&m21324_MI,
	&m21325_MI,
	&m5908_MI,
	&m21319_MI,
	&m21327_MI,
	&m21328_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5403_TI;
extern TypeInfo t5404_TI;
extern TypeInfo t5405_TI;
static TypeInfo* t3855_ITIs[] = 
{
	&t5403_TI,
	&t5404_TI,
	&t5405_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3855_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5403_TI, 21},
	{ &t5404_TI, 28},
	{ &t5405_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3855_0_0_0;
extern Il2CppType t3855_1_0_0;
#include "t215.h"
extern TypeInfo t215_TI;
TypeInfo t3855_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Axis[]", "", t3855_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t215_TI, t3855_ITIs, t3855_VT, &EmptyCustomAttributesCache, &t44_TI, &t3855_0_0_0, &t3855_1_0_0, t3855_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 259, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3856_TI;



// Metadata Definition UnityEngine.UI.ScrollRect[]
static MethodInfo* t3856_MIs[] =
{
	NULL
};
extern MethodInfo m21331_MI;
extern MethodInfo m21332_MI;
extern MethodInfo m21333_MI;
extern MethodInfo m21334_MI;
extern MethodInfo m21335_MI;
extern MethodInfo m21336_MI;
extern MethodInfo m21330_MI;
extern MethodInfo m21338_MI;
extern MethodInfo m21339_MI;
extern MethodInfo m20230_MI;
extern MethodInfo m20231_MI;
extern MethodInfo m20232_MI;
extern MethodInfo m20233_MI;
extern MethodInfo m20234_MI;
extern MethodInfo m20235_MI;
extern MethodInfo m20229_MI;
extern MethodInfo m20237_MI;
extern MethodInfo m20238_MI;
static MethodInfo* t3856_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21331_MI,
	&m5907_MI,
	&m21332_MI,
	&m21333_MI,
	&m21334_MI,
	&m21335_MI,
	&m21336_MI,
	&m5908_MI,
	&m21330_MI,
	&m21338_MI,
	&m21339_MI,
	&m5905_MI,
	&m5906_MI,
	&m19581_MI,
	&m5907_MI,
	&m19582_MI,
	&m19583_MI,
	&m19584_MI,
	&m19585_MI,
	&m19586_MI,
	&m5908_MI,
	&m19580_MI,
	&m19588_MI,
	&m19589_MI,
	&m5905_MI,
	&m5906_MI,
	&m20175_MI,
	&m5907_MI,
	&m20176_MI,
	&m20177_MI,
	&m20178_MI,
	&m20179_MI,
	&m20180_MI,
	&m5908_MI,
	&m20174_MI,
	&m20182_MI,
	&m20183_MI,
	&m5905_MI,
	&m5906_MI,
	&m20186_MI,
	&m5907_MI,
	&m20187_MI,
	&m20188_MI,
	&m20189_MI,
	&m20190_MI,
	&m20191_MI,
	&m5908_MI,
	&m20185_MI,
	&m20193_MI,
	&m20194_MI,
	&m5905_MI,
	&m5906_MI,
	&m20197_MI,
	&m5907_MI,
	&m20198_MI,
	&m20199_MI,
	&m20200_MI,
	&m20201_MI,
	&m20202_MI,
	&m5908_MI,
	&m20196_MI,
	&m20204_MI,
	&m20205_MI,
	&m5905_MI,
	&m5906_MI,
	&m20208_MI,
	&m5907_MI,
	&m20209_MI,
	&m20210_MI,
	&m20211_MI,
	&m20212_MI,
	&m20213_MI,
	&m5908_MI,
	&m20207_MI,
	&m20215_MI,
	&m20216_MI,
	&m5905_MI,
	&m5906_MI,
	&m20230_MI,
	&m5907_MI,
	&m20231_MI,
	&m20232_MI,
	&m20233_MI,
	&m20234_MI,
	&m20235_MI,
	&m5908_MI,
	&m20229_MI,
	&m20237_MI,
	&m20238_MI,
	&m5905_MI,
	&m5906_MI,
	&m20737_MI,
	&m5907_MI,
	&m20738_MI,
	&m20739_MI,
	&m20740_MI,
	&m20741_MI,
	&m20742_MI,
	&m5908_MI,
	&m20736_MI,
	&m20744_MI,
	&m20745_MI,
	&m5905_MI,
	&m5906_MI,
	&m19777_MI,
	&m5907_MI,
	&m19778_MI,
	&m19779_MI,
	&m19780_MI,
	&m19781_MI,
	&m19782_MI,
	&m5908_MI,
	&m19776_MI,
	&m19784_MI,
	&m19785_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5406_TI;
extern TypeInfo t5407_TI;
extern TypeInfo t5408_TI;
extern TypeInfo t5205_TI;
extern TypeInfo t5206_TI;
extern TypeInfo t5207_TI;
static TypeInfo* t3856_ITIs[] = 
{
	&t5406_TI,
	&t5407_TI,
	&t5408_TI,
	&t2216_TI,
	&t312_TI,
	&t2217_TI,
	&t5190_TI,
	&t5191_TI,
	&t5192_TI,
	&t5193_TI,
	&t5194_TI,
	&t5195_TI,
	&t5196_TI,
	&t5197_TI,
	&t5198_TI,
	&t5199_TI,
	&t5200_TI,
	&t5201_TI,
	&t5205_TI,
	&t5206_TI,
	&t5207_TI,
	&t2430_TI,
	&t2434_TI,
	&t2431_TI,
	&t5122_TI,
	&t5123_TI,
	&t5124_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3856_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5406_TI, 21},
	{ &t5407_TI, 28},
	{ &t5408_TI, 33},
	{ &t2216_TI, 34},
	{ &t312_TI, 41},
	{ &t2217_TI, 46},
	{ &t5190_TI, 47},
	{ &t5191_TI, 54},
	{ &t5192_TI, 59},
	{ &t5193_TI, 60},
	{ &t5194_TI, 67},
	{ &t5195_TI, 72},
	{ &t5196_TI, 73},
	{ &t5197_TI, 80},
	{ &t5198_TI, 85},
	{ &t5199_TI, 86},
	{ &t5200_TI, 93},
	{ &t5201_TI, 98},
	{ &t5205_TI, 99},
	{ &t5206_TI, 106},
	{ &t5207_TI, 111},
	{ &t2430_TI, 112},
	{ &t2434_TI, 119},
	{ &t2431_TI, 124},
	{ &t5122_TI, 125},
	{ &t5123_TI, 132},
	{ &t5124_TI, 137},
	{ &t5059_TI, 138},
	{ &t5060_TI, 145},
	{ &t5061_TI, 150},
	{ &t5062_TI, 151},
	{ &t5063_TI, 158},
	{ &t5064_TI, 163},
	{ &t2240_TI, 164},
	{ &t2245_TI, 171},
	{ &t2241_TI, 176},
	{ &t5065_TI, 177},
	{ &t5066_TI, 184},
	{ &t5067_TI, 189},
	{ &t2182_TI, 190},
	{ &t2186_TI, 197},
	{ &t2183_TI, 202},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3856_0_0_0;
extern Il2CppType t3856_1_0_0;
struct t222;
extern TypeInfo t222_TI;
extern CustomAttributesCache t222__CustomAttributeCache;
extern CustomAttributesCache t222__CustomAttributeCache_m_Content;
extern CustomAttributesCache t222__CustomAttributeCache_m_Horizontal;
extern CustomAttributesCache t222__CustomAttributeCache_m_Vertical;
extern CustomAttributesCache t222__CustomAttributeCache_m_MovementType;
extern CustomAttributesCache t222__CustomAttributeCache_m_Elasticity;
extern CustomAttributesCache t222__CustomAttributeCache_m_Inertia;
extern CustomAttributesCache t222__CustomAttributeCache_m_DecelerationRate;
extern CustomAttributesCache t222__CustomAttributeCache_m_ScrollSensitivity;
extern CustomAttributesCache t222__CustomAttributeCache_m_HorizontalScrollbar;
extern CustomAttributesCache t222__CustomAttributeCache_m_VerticalScrollbar;
extern CustomAttributesCache t222__CustomAttributeCache_m_OnValueChanged;
TypeInfo t3856_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "ScrollRect[]", "UnityEngine.UI", t3856_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t222_TI, t3856_ITIs, t3856_VT, &EmptyCustomAttributesCache, &t3856_TI, &t3856_0_0_0, &t3856_1_0_0, t3856_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t222 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 203, 42, 46};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t223_TI;



// Metadata Definition UnityEngine.Vector3[]
static MethodInfo* t223_MIs[] =
{
	NULL
};
extern MethodInfo m21343_MI;
extern MethodInfo m21344_MI;
extern MethodInfo m21345_MI;
extern MethodInfo m21346_MI;
extern MethodInfo m21347_MI;
extern MethodInfo m21348_MI;
extern MethodInfo m21342_MI;
extern MethodInfo m21350_MI;
extern MethodInfo m21351_MI;
static MethodInfo* t223_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21343_MI,
	&m5907_MI,
	&m21344_MI,
	&m21345_MI,
	&m21346_MI,
	&m21347_MI,
	&m21348_MI,
	&m5908_MI,
	&m21342_MI,
	&m21350_MI,
	&m21351_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5409_TI;
extern TypeInfo t5410_TI;
extern TypeInfo t5411_TI;
static TypeInfo* t223_ITIs[] = 
{
	&t5409_TI,
	&t5410_TI,
	&t5411_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t223_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5409_TI, 21},
	{ &t5410_TI, 28},
	{ &t5411_TI, 33},
	{ &t5116_TI, 34},
	{ &t5117_TI, 41},
	{ &t5118_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t223_0_0_0;
extern Il2CppType t223_1_0_0;
#include "t23.h"
extern TypeInfo t23_TI;
extern CustomAttributesCache t23__CustomAttributeCache;
TypeInfo t223_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Vector3[]", "UnityEngine", t223_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t23_TI, t223_ITIs, t223_VT, &EmptyCustomAttributesCache, &t223_TI, &t223_0_0_0, &t223_1_0_0, t223_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t23 ), -1, 0, 0, -1, 1048841, 1, false, false, false, false, false, true, false, false, false, false, false, true, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3857_TI;



// Metadata Definition UnityEngine.UI.ScrollRect/MovementType[]
static MethodInfo* t3857_MIs[] =
{
	NULL
};
extern MethodInfo m21355_MI;
extern MethodInfo m21356_MI;
extern MethodInfo m21357_MI;
extern MethodInfo m21358_MI;
extern MethodInfo m21359_MI;
extern MethodInfo m21360_MI;
extern MethodInfo m21354_MI;
extern MethodInfo m21362_MI;
extern MethodInfo m21363_MI;
static MethodInfo* t3857_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21355_MI,
	&m5907_MI,
	&m21356_MI,
	&m21357_MI,
	&m21358_MI,
	&m21359_MI,
	&m21360_MI,
	&m5908_MI,
	&m21354_MI,
	&m21362_MI,
	&m21363_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5412_TI;
extern TypeInfo t5413_TI;
extern TypeInfo t5414_TI;
static TypeInfo* t3857_ITIs[] = 
{
	&t5412_TI,
	&t5413_TI,
	&t5414_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3857_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5412_TI, 21},
	{ &t5413_TI, 28},
	{ &t5414_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3857_0_0_0;
extern Il2CppType t3857_1_0_0;
#include "t219.h"
extern TypeInfo t219_TI;
TypeInfo t3857_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "MovementType[]", "", t3857_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t219_TI, t3857_ITIs, t3857_VT, &EmptyCustomAttributesCache, &t44_TI, &t3857_0_0_0, &t3857_1_0_0, t3857_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 258, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2652_TI;



// Metadata Definition UnityEngine.CanvasGroup[]
static MethodInfo* t2652_MIs[] =
{
	NULL
};
extern MethodInfo m21382_MI;
extern MethodInfo m21383_MI;
extern MethodInfo m21384_MI;
extern MethodInfo m21385_MI;
extern MethodInfo m21386_MI;
extern MethodInfo m21387_MI;
extern MethodInfo m21381_MI;
extern MethodInfo m21389_MI;
extern MethodInfo m21390_MI;
static MethodInfo* t2652_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21382_MI,
	&m5907_MI,
	&m21383_MI,
	&m21384_MI,
	&m21385_MI,
	&m21386_MI,
	&m21387_MI,
	&m5908_MI,
	&m21381_MI,
	&m21389_MI,
	&m21390_MI,
	&m5905_MI,
	&m5906_MI,
	&m21071_MI,
	&m5907_MI,
	&m21072_MI,
	&m21073_MI,
	&m21074_MI,
	&m21075_MI,
	&m21076_MI,
	&m5908_MI,
	&m21070_MI,
	&m21078_MI,
	&m21079_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t2654_TI;
extern TypeInfo t2661_TI;
extern TypeInfo t2655_TI;
static TypeInfo* t2652_ITIs[] = 
{
	&t2654_TI,
	&t2661_TI,
	&t2655_TI,
	&t5343_TI,
	&t5344_TI,
	&t5345_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2652_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2654_TI, 21},
	{ &t2661_TI, 28},
	{ &t2655_TI, 33},
	{ &t5343_TI, 34},
	{ &t5344_TI, 41},
	{ &t5345_TI, 46},
	{ &t2240_TI, 47},
	{ &t2245_TI, 54},
	{ &t2241_TI, 59},
	{ &t5065_TI, 60},
	{ &t5066_TI, 67},
	{ &t5067_TI, 72},
	{ &t2182_TI, 73},
	{ &t2186_TI, 80},
	{ &t2183_TI, 85},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2652_0_0_0;
extern Il2CppType t2652_1_0_0;
struct t352;
extern TypeInfo t352_TI;
extern CustomAttributesCache t352__CustomAttributeCache_m1878;
extern CustomAttributesCache t352__CustomAttributeCache_m2718;
extern CustomAttributesCache t352__CustomAttributeCache_m1586;
TypeInfo t2652_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CanvasGroup[]", "UnityEngine", t2652_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t352_TI, t2652_ITIs, t2652_VT, &EmptyCustomAttributesCache, &t2652_TI, &t2652_0_0_0, &t2652_1_0_0, t2652_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t352 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 86, 15, 19};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3858_TI;



// Metadata Definition UnityEngine.UI.Selectable/Transition[]
static MethodInfo* t3858_MIs[] =
{
	NULL
};
extern MethodInfo m21408_MI;
extern MethodInfo m21409_MI;
extern MethodInfo m21410_MI;
extern MethodInfo m21411_MI;
extern MethodInfo m21412_MI;
extern MethodInfo m21413_MI;
extern MethodInfo m21407_MI;
extern MethodInfo m21415_MI;
extern MethodInfo m21416_MI;
static MethodInfo* t3858_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21408_MI,
	&m5907_MI,
	&m21409_MI,
	&m21410_MI,
	&m21411_MI,
	&m21412_MI,
	&m21413_MI,
	&m5908_MI,
	&m21407_MI,
	&m21415_MI,
	&m21416_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5415_TI;
extern TypeInfo t5416_TI;
extern TypeInfo t5417_TI;
static TypeInfo* t3858_ITIs[] = 
{
	&t5415_TI,
	&t5416_TI,
	&t5417_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3858_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5415_TI, 21},
	{ &t5416_TI, 28},
	{ &t5417_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3858_0_0_0;
extern Il2CppType t3858_1_0_0;
#include "t225.h"
extern TypeInfo t225_TI;
TypeInfo t3858_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Transition[]", "", t3858_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t225_TI, t3858_ITIs, t3858_VT, &EmptyCustomAttributesCache, &t44_TI, &t3858_0_0_0, &t3858_1_0_0, t3858_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 258, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3859_TI;



// Metadata Definition UnityEngine.UI.Selectable/SelectionState[]
static MethodInfo* t3859_MIs[] =
{
	NULL
};
extern MethodInfo m21419_MI;
extern MethodInfo m21420_MI;
extern MethodInfo m21421_MI;
extern MethodInfo m21422_MI;
extern MethodInfo m21423_MI;
extern MethodInfo m21424_MI;
extern MethodInfo m21418_MI;
extern MethodInfo m21426_MI;
extern MethodInfo m21427_MI;
static MethodInfo* t3859_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21419_MI,
	&m5907_MI,
	&m21420_MI,
	&m21421_MI,
	&m21422_MI,
	&m21423_MI,
	&m21424_MI,
	&m5908_MI,
	&m21418_MI,
	&m21426_MI,
	&m21427_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5418_TI;
extern TypeInfo t5419_TI;
extern TypeInfo t5420_TI;
static TypeInfo* t3859_ITIs[] = 
{
	&t5418_TI,
	&t5419_TI,
	&t5420_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3859_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5418_TI, 21},
	{ &t5419_TI, 28},
	{ &t5420_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3859_0_0_0;
extern Il2CppType t3859_1_0_0;
#include "t207.h"
extern TypeInfo t207_TI;
TypeInfo t3859_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "SelectionState[]", "", t3859_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t207_TI, t3859_ITIs, t3859_VT, &EmptyCustomAttributesCache, &t44_TI, &t3859_0_0_0, &t3859_1_0_0, t3859_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 260, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3860_TI;



// Metadata Definition UnityEngine.UI.Slider[]
static MethodInfo* t3860_MIs[] =
{
	NULL
};
extern MethodInfo m21430_MI;
extern MethodInfo m21431_MI;
extern MethodInfo m21432_MI;
extern MethodInfo m21433_MI;
extern MethodInfo m21434_MI;
extern MethodInfo m21435_MI;
extern MethodInfo m21429_MI;
extern MethodInfo m21437_MI;
extern MethodInfo m21438_MI;
static MethodInfo* t3860_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21430_MI,
	&m5907_MI,
	&m21431_MI,
	&m21432_MI,
	&m21433_MI,
	&m21434_MI,
	&m21435_MI,
	&m5908_MI,
	&m21429_MI,
	&m21437_MI,
	&m21438_MI,
	&m5905_MI,
	&m5906_MI,
	&m19581_MI,
	&m5907_MI,
	&m19582_MI,
	&m19583_MI,
	&m19584_MI,
	&m19585_MI,
	&m19586_MI,
	&m5908_MI,
	&m19580_MI,
	&m19588_MI,
	&m19589_MI,
	&m5905_MI,
	&m5906_MI,
	&m20186_MI,
	&m5907_MI,
	&m20187_MI,
	&m20188_MI,
	&m20189_MI,
	&m20190_MI,
	&m20191_MI,
	&m5908_MI,
	&m20185_MI,
	&m20193_MI,
	&m20194_MI,
	&m5905_MI,
	&m5906_MI,
	&m20197_MI,
	&m5907_MI,
	&m20198_MI,
	&m20199_MI,
	&m20200_MI,
	&m20201_MI,
	&m20202_MI,
	&m5908_MI,
	&m20196_MI,
	&m20204_MI,
	&m20205_MI,
	&m5905_MI,
	&m5906_MI,
	&m20737_MI,
	&m5907_MI,
	&m20738_MI,
	&m20739_MI,
	&m20740_MI,
	&m20741_MI,
	&m20742_MI,
	&m5908_MI,
	&m20736_MI,
	&m20744_MI,
	&m20745_MI,
	&m5905_MI,
	&m5906_MI,
	&m20696_MI,
	&m5907_MI,
	&m20697_MI,
	&m20698_MI,
	&m20699_MI,
	&m20700_MI,
	&m20701_MI,
	&m5908_MI,
	&m20695_MI,
	&m20703_MI,
	&m20704_MI,
	&m5905_MI,
	&m5906_MI,
	&m19581_MI,
	&m5907_MI,
	&m19582_MI,
	&m19583_MI,
	&m19584_MI,
	&m19585_MI,
	&m19586_MI,
	&m5908_MI,
	&m19580_MI,
	&m19588_MI,
	&m19589_MI,
	&m5905_MI,
	&m5906_MI,
	&m19570_MI,
	&m5907_MI,
	&m19571_MI,
	&m19572_MI,
	&m19573_MI,
	&m19574_MI,
	&m19575_MI,
	&m5908_MI,
	&m19569_MI,
	&m19577_MI,
	&m19578_MI,
	&m5905_MI,
	&m5906_MI,
	&m19592_MI,
	&m5907_MI,
	&m19593_MI,
	&m19594_MI,
	&m19595_MI,
	&m19596_MI,
	&m19597_MI,
	&m5908_MI,
	&m19591_MI,
	&m19599_MI,
	&m19600_MI,
	&m5905_MI,
	&m5906_MI,
	&m20142_MI,
	&m5907_MI,
	&m20143_MI,
	&m20144_MI,
	&m20145_MI,
	&m20146_MI,
	&m20147_MI,
	&m5908_MI,
	&m20141_MI,
	&m20149_MI,
	&m20150_MI,
	&m5905_MI,
	&m5906_MI,
	&m20153_MI,
	&m5907_MI,
	&m20154_MI,
	&m20155_MI,
	&m20156_MI,
	&m20157_MI,
	&m20158_MI,
	&m5908_MI,
	&m20152_MI,
	&m20160_MI,
	&m20161_MI,
	&m5905_MI,
	&m5906_MI,
	&m20252_MI,
	&m5907_MI,
	&m20253_MI,
	&m20254_MI,
	&m20255_MI,
	&m20256_MI,
	&m20257_MI,
	&m5908_MI,
	&m20251_MI,
	&m20259_MI,
	&m20260_MI,
	&m5905_MI,
	&m5906_MI,
	&m20263_MI,
	&m5907_MI,
	&m20264_MI,
	&m20265_MI,
	&m20266_MI,
	&m20267_MI,
	&m20268_MI,
	&m5908_MI,
	&m20262_MI,
	&m20270_MI,
	&m20271_MI,
	&m5905_MI,
	&m5906_MI,
	&m20274_MI,
	&m5907_MI,
	&m20275_MI,
	&m20276_MI,
	&m20277_MI,
	&m20278_MI,
	&m20279_MI,
	&m5908_MI,
	&m20273_MI,
	&m20281_MI,
	&m20282_MI,
	&m5905_MI,
	&m5906_MI,
	&m19777_MI,
	&m5907_MI,
	&m19778_MI,
	&m19779_MI,
	&m19780_MI,
	&m19781_MI,
	&m19782_MI,
	&m5908_MI,
	&m19776_MI,
	&m19784_MI,
	&m19785_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5421_TI;
extern TypeInfo t5422_TI;
extern TypeInfo t5423_TI;
static TypeInfo* t3860_ITIs[] = 
{
	&t5421_TI,
	&t5422_TI,
	&t5423_TI,
	&t2216_TI,
	&t312_TI,
	&t2217_TI,
	&t5193_TI,
	&t5194_TI,
	&t5195_TI,
	&t5196_TI,
	&t5197_TI,
	&t5198_TI,
	&t2430_TI,
	&t2434_TI,
	&t2431_TI,
	&t2640_TI,
	&t2646_TI,
	&t2641_TI,
	&t2216_TI,
	&t312_TI,
	&t2217_TI,
	&t5071_TI,
	&t5072_TI,
	&t5073_TI,
	&t5074_TI,
	&t5075_TI,
	&t5076_TI,
	&t5181_TI,
	&t5182_TI,
	&t5183_TI,
	&t5184_TI,
	&t5185_TI,
	&t5186_TI,
	&t5211_TI,
	&t5212_TI,
	&t5213_TI,
	&t5214_TI,
	&t5215_TI,
	&t5216_TI,
	&t5217_TI,
	&t5218_TI,
	&t5219_TI,
	&t5122_TI,
	&t5123_TI,
	&t5124_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3860_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5421_TI, 21},
	{ &t5422_TI, 28},
	{ &t5423_TI, 33},
	{ &t2216_TI, 34},
	{ &t312_TI, 41},
	{ &t2217_TI, 46},
	{ &t5193_TI, 47},
	{ &t5194_TI, 54},
	{ &t5195_TI, 59},
	{ &t5196_TI, 60},
	{ &t5197_TI, 67},
	{ &t5198_TI, 72},
	{ &t2430_TI, 73},
	{ &t2434_TI, 80},
	{ &t2431_TI, 85},
	{ &t2640_TI, 86},
	{ &t2646_TI, 93},
	{ &t2641_TI, 98},
	{ &t2216_TI, 99},
	{ &t312_TI, 106},
	{ &t2217_TI, 111},
	{ &t5071_TI, 112},
	{ &t5072_TI, 119},
	{ &t5073_TI, 124},
	{ &t5074_TI, 125},
	{ &t5075_TI, 132},
	{ &t5076_TI, 137},
	{ &t5181_TI, 138},
	{ &t5182_TI, 145},
	{ &t5183_TI, 150},
	{ &t5184_TI, 151},
	{ &t5185_TI, 158},
	{ &t5186_TI, 163},
	{ &t5211_TI, 164},
	{ &t5212_TI, 171},
	{ &t5213_TI, 176},
	{ &t5214_TI, 177},
	{ &t5215_TI, 184},
	{ &t5216_TI, 189},
	{ &t5217_TI, 190},
	{ &t5218_TI, 197},
	{ &t5219_TI, 202},
	{ &t5122_TI, 203},
	{ &t5123_TI, 210},
	{ &t5124_TI, 215},
	{ &t5059_TI, 216},
	{ &t5060_TI, 223},
	{ &t5061_TI, 228},
	{ &t5062_TI, 229},
	{ &t5063_TI, 236},
	{ &t5064_TI, 241},
	{ &t2240_TI, 242},
	{ &t2245_TI, 249},
	{ &t2241_TI, 254},
	{ &t5065_TI, 255},
	{ &t5066_TI, 262},
	{ &t5067_TI, 267},
	{ &t2182_TI, 268},
	{ &t2186_TI, 275},
	{ &t2183_TI, 280},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3860_0_0_0;
extern Il2CppType t3860_1_0_0;
struct t234;
extern TypeInfo t234_TI;
extern CustomAttributesCache t234__CustomAttributeCache;
extern CustomAttributesCache t234__CustomAttributeCache_m_FillRect;
extern CustomAttributesCache t234__CustomAttributeCache_m_HandleRect;
extern CustomAttributesCache t234__CustomAttributeCache_m_Direction;
extern CustomAttributesCache t234__CustomAttributeCache_m_MinValue;
extern CustomAttributesCache t234__CustomAttributeCache_m_MaxValue;
extern CustomAttributesCache t234__CustomAttributeCache_m_WholeNumbers;
extern CustomAttributesCache t234__CustomAttributeCache_m_Value;
extern CustomAttributesCache t234__CustomAttributeCache_m_OnValueChanged;
TypeInfo t3860_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Slider[]", "UnityEngine.UI", t3860_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t234_TI, t3860_ITIs, t3860_VT, &EmptyCustomAttributesCache, &t3860_TI, &t3860_0_0_0, &t3860_1_0_0, t3860_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t234 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 281, 60, 64};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3861_TI;



// Metadata Definition UnityEngine.UI.Slider/Direction[]
static MethodInfo* t3861_MIs[] =
{
	NULL
};
extern MethodInfo m21442_MI;
extern MethodInfo m21443_MI;
extern MethodInfo m21444_MI;
extern MethodInfo m21445_MI;
extern MethodInfo m21446_MI;
extern MethodInfo m21447_MI;
extern MethodInfo m21441_MI;
extern MethodInfo m21449_MI;
extern MethodInfo m21450_MI;
static MethodInfo* t3861_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21442_MI,
	&m5907_MI,
	&m21443_MI,
	&m21444_MI,
	&m21445_MI,
	&m21446_MI,
	&m21447_MI,
	&m5908_MI,
	&m21441_MI,
	&m21449_MI,
	&m21450_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5424_TI;
extern TypeInfo t5425_TI;
extern TypeInfo t5426_TI;
static TypeInfo* t3861_ITIs[] = 
{
	&t5424_TI,
	&t5425_TI,
	&t5426_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3861_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5424_TI, 21},
	{ &t5425_TI, 28},
	{ &t5426_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3861_0_0_0;
extern Il2CppType t3861_1_0_0;
#include "t231.h"
extern TypeInfo t231_TI;
TypeInfo t3861_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Direction[]", "", t3861_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t231_TI, t3861_ITIs, t3861_VT, &EmptyCustomAttributesCache, &t44_TI, &t3861_0_0_0, &t3861_1_0_0, t3861_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 258, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3862_TI;



// Metadata Definition UnityEngine.UI.Slider/Axis[]
static MethodInfo* t3862_MIs[] =
{
	NULL
};
extern MethodInfo m21453_MI;
extern MethodInfo m21454_MI;
extern MethodInfo m21455_MI;
extern MethodInfo m21456_MI;
extern MethodInfo m21457_MI;
extern MethodInfo m21458_MI;
extern MethodInfo m21452_MI;
extern MethodInfo m21460_MI;
extern MethodInfo m21461_MI;
static MethodInfo* t3862_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21453_MI,
	&m5907_MI,
	&m21454_MI,
	&m21455_MI,
	&m21456_MI,
	&m21457_MI,
	&m21458_MI,
	&m5908_MI,
	&m21452_MI,
	&m21460_MI,
	&m21461_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5427_TI;
extern TypeInfo t5428_TI;
extern TypeInfo t5429_TI;
static TypeInfo* t3862_ITIs[] = 
{
	&t5427_TI,
	&t5428_TI,
	&t5429_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3862_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5427_TI, 21},
	{ &t5428_TI, 28},
	{ &t5429_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3862_0_0_0;
extern Il2CppType t3862_1_0_0;
#include "t233.h"
extern TypeInfo t233_TI;
TypeInfo t3862_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Axis[]", "", t3862_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t233_TI, t3862_ITIs, t3862_VT, &EmptyCustomAttributesCache, &t44_TI, &t3862_0_0_0, &t3862_1_0_0, t3862_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 259, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2675_TI;



// Metadata Definition UnityEngine.UI.StencilMaterial/MatEntry[]
static MethodInfo* t2675_MIs[] =
{
	NULL
};
extern MethodInfo m21464_MI;
extern MethodInfo m21465_MI;
extern MethodInfo m21466_MI;
extern MethodInfo m21467_MI;
extern MethodInfo m21468_MI;
extern MethodInfo m21469_MI;
extern MethodInfo m21463_MI;
extern MethodInfo m21471_MI;
extern MethodInfo m21472_MI;
static MethodInfo* t2675_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21464_MI,
	&m5907_MI,
	&m21465_MI,
	&m21466_MI,
	&m21467_MI,
	&m21468_MI,
	&m21469_MI,
	&m5908_MI,
	&m21463_MI,
	&m21471_MI,
	&m21472_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t2677_TI;
extern TypeInfo t2684_TI;
extern TypeInfo t2678_TI;
static TypeInfo* t2675_ITIs[] = 
{
	&t2677_TI,
	&t2684_TI,
	&t2678_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2675_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2677_TI, 21},
	{ &t2684_TI, 28},
	{ &t2678_TI, 33},
	{ &t2182_TI, 34},
	{ &t2186_TI, 41},
	{ &t2183_TI, 46},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t2675_0_0_0;
extern Il2CppType t2675_1_0_0;
struct t235;
extern TypeInfo t235_TI;
TypeInfo t2675_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "MatEntry[]", "", t2675_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t235_TI, t2675_ITIs, t2675_VT, &EmptyCustomAttributesCache, &t2675_TI, &t2675_0_0_0, &t2675_1_0_0, t2675_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t235 *), -1, 0, 0, -1, 1048579, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2704_TI;



// Metadata Definition UnityEngine.UI.Toggle[]
static MethodInfo* t2704_MIs[] =
{
	NULL
};
extern MethodInfo m21491_MI;
extern MethodInfo m21492_MI;
extern MethodInfo m21493_MI;
extern MethodInfo m21494_MI;
extern MethodInfo m21495_MI;
extern MethodInfo m21496_MI;
extern MethodInfo m21490_MI;
extern MethodInfo m21498_MI;
extern MethodInfo m21499_MI;
static MethodInfo* t2704_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21491_MI,
	&m5907_MI,
	&m21492_MI,
	&m21493_MI,
	&m21494_MI,
	&m21495_MI,
	&m21496_MI,
	&m5908_MI,
	&m21490_MI,
	&m21498_MI,
	&m21499_MI,
	&m5905_MI,
	&m5906_MI,
	&m19581_MI,
	&m5907_MI,
	&m19582_MI,
	&m19583_MI,
	&m19584_MI,
	&m19585_MI,
	&m19586_MI,
	&m5908_MI,
	&m19580_MI,
	&m19588_MI,
	&m19589_MI,
	&m5905_MI,
	&m5906_MI,
	&m20164_MI,
	&m5907_MI,
	&m20165_MI,
	&m20166_MI,
	&m20167_MI,
	&m20168_MI,
	&m20169_MI,
	&m5908_MI,
	&m20163_MI,
	&m20171_MI,
	&m20172_MI,
	&m5905_MI,
	&m5906_MI,
	&m20285_MI,
	&m5907_MI,
	&m20286_MI,
	&m20287_MI,
	&m20288_MI,
	&m20289_MI,
	&m20290_MI,
	&m5908_MI,
	&m20284_MI,
	&m20292_MI,
	&m20293_MI,
	&m5905_MI,
	&m5906_MI,
	&m20737_MI,
	&m5907_MI,
	&m20738_MI,
	&m20739_MI,
	&m20740_MI,
	&m20741_MI,
	&m20742_MI,
	&m5908_MI,
	&m20736_MI,
	&m20744_MI,
	&m20745_MI,
	&m5905_MI,
	&m5906_MI,
	&m20696_MI,
	&m5907_MI,
	&m20697_MI,
	&m20698_MI,
	&m20699_MI,
	&m20700_MI,
	&m20701_MI,
	&m5908_MI,
	&m20695_MI,
	&m20703_MI,
	&m20704_MI,
	&m5905_MI,
	&m5906_MI,
	&m19581_MI,
	&m5907_MI,
	&m19582_MI,
	&m19583_MI,
	&m19584_MI,
	&m19585_MI,
	&m19586_MI,
	&m5908_MI,
	&m19580_MI,
	&m19588_MI,
	&m19589_MI,
	&m5905_MI,
	&m5906_MI,
	&m19570_MI,
	&m5907_MI,
	&m19571_MI,
	&m19572_MI,
	&m19573_MI,
	&m19574_MI,
	&m19575_MI,
	&m5908_MI,
	&m19569_MI,
	&m19577_MI,
	&m19578_MI,
	&m5905_MI,
	&m5906_MI,
	&m19592_MI,
	&m5907_MI,
	&m19593_MI,
	&m19594_MI,
	&m19595_MI,
	&m19596_MI,
	&m19597_MI,
	&m5908_MI,
	&m19591_MI,
	&m19599_MI,
	&m19600_MI,
	&m5905_MI,
	&m5906_MI,
	&m20142_MI,
	&m5907_MI,
	&m20143_MI,
	&m20144_MI,
	&m20145_MI,
	&m20146_MI,
	&m20147_MI,
	&m5908_MI,
	&m20141_MI,
	&m20149_MI,
	&m20150_MI,
	&m5905_MI,
	&m5906_MI,
	&m20153_MI,
	&m5907_MI,
	&m20154_MI,
	&m20155_MI,
	&m20156_MI,
	&m20157_MI,
	&m20158_MI,
	&m5908_MI,
	&m20152_MI,
	&m20160_MI,
	&m20161_MI,
	&m5905_MI,
	&m5906_MI,
	&m20252_MI,
	&m5907_MI,
	&m20253_MI,
	&m20254_MI,
	&m20255_MI,
	&m20256_MI,
	&m20257_MI,
	&m5908_MI,
	&m20251_MI,
	&m20259_MI,
	&m20260_MI,
	&m5905_MI,
	&m5906_MI,
	&m20263_MI,
	&m5907_MI,
	&m20264_MI,
	&m20265_MI,
	&m20266_MI,
	&m20267_MI,
	&m20268_MI,
	&m5908_MI,
	&m20262_MI,
	&m20270_MI,
	&m20271_MI,
	&m5905_MI,
	&m5906_MI,
	&m20274_MI,
	&m5907_MI,
	&m20275_MI,
	&m20276_MI,
	&m20277_MI,
	&m20278_MI,
	&m20279_MI,
	&m5908_MI,
	&m20273_MI,
	&m20281_MI,
	&m20282_MI,
	&m5905_MI,
	&m5906_MI,
	&m19777_MI,
	&m5907_MI,
	&m19778_MI,
	&m19779_MI,
	&m19780_MI,
	&m19781_MI,
	&m19782_MI,
	&m5908_MI,
	&m19776_MI,
	&m19784_MI,
	&m19785_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t2706_TI;
extern TypeInfo t2710_TI;
extern TypeInfo t246_TI;
static TypeInfo* t2704_ITIs[] = 
{
	&t2706_TI,
	&t2710_TI,
	&t246_TI,
	&t2216_TI,
	&t312_TI,
	&t2217_TI,
	&t5187_TI,
	&t5188_TI,
	&t5189_TI,
	&t5220_TI,
	&t5221_TI,
	&t5222_TI,
	&t2430_TI,
	&t2434_TI,
	&t2431_TI,
	&t2640_TI,
	&t2646_TI,
	&t2641_TI,
	&t2216_TI,
	&t312_TI,
	&t2217_TI,
	&t5071_TI,
	&t5072_TI,
	&t5073_TI,
	&t5074_TI,
	&t5075_TI,
	&t5076_TI,
	&t5181_TI,
	&t5182_TI,
	&t5183_TI,
	&t5184_TI,
	&t5185_TI,
	&t5186_TI,
	&t5211_TI,
	&t5212_TI,
	&t5213_TI,
	&t5214_TI,
	&t5215_TI,
	&t5216_TI,
	&t5217_TI,
	&t5218_TI,
	&t5219_TI,
	&t5122_TI,
	&t5123_TI,
	&t5124_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2704_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2706_TI, 21},
	{ &t2710_TI, 28},
	{ &t246_TI, 33},
	{ &t2216_TI, 34},
	{ &t312_TI, 41},
	{ &t2217_TI, 46},
	{ &t5187_TI, 47},
	{ &t5188_TI, 54},
	{ &t5189_TI, 59},
	{ &t5220_TI, 60},
	{ &t5221_TI, 67},
	{ &t5222_TI, 72},
	{ &t2430_TI, 73},
	{ &t2434_TI, 80},
	{ &t2431_TI, 85},
	{ &t2640_TI, 86},
	{ &t2646_TI, 93},
	{ &t2641_TI, 98},
	{ &t2216_TI, 99},
	{ &t312_TI, 106},
	{ &t2217_TI, 111},
	{ &t5071_TI, 112},
	{ &t5072_TI, 119},
	{ &t5073_TI, 124},
	{ &t5074_TI, 125},
	{ &t5075_TI, 132},
	{ &t5076_TI, 137},
	{ &t5181_TI, 138},
	{ &t5182_TI, 145},
	{ &t5183_TI, 150},
	{ &t5184_TI, 151},
	{ &t5185_TI, 158},
	{ &t5186_TI, 163},
	{ &t5211_TI, 164},
	{ &t5212_TI, 171},
	{ &t5213_TI, 176},
	{ &t5214_TI, 177},
	{ &t5215_TI, 184},
	{ &t5216_TI, 189},
	{ &t5217_TI, 190},
	{ &t5218_TI, 197},
	{ &t5219_TI, 202},
	{ &t5122_TI, 203},
	{ &t5123_TI, 210},
	{ &t5124_TI, 215},
	{ &t5059_TI, 216},
	{ &t5060_TI, 223},
	{ &t5061_TI, 228},
	{ &t5062_TI, 229},
	{ &t5063_TI, 236},
	{ &t5064_TI, 241},
	{ &t2240_TI, 242},
	{ &t2245_TI, 249},
	{ &t2241_TI, 254},
	{ &t5065_TI, 255},
	{ &t5066_TI, 262},
	{ &t5067_TI, 267},
	{ &t2182_TI, 268},
	{ &t2186_TI, 275},
	{ &t2183_TI, 280},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t2704_0_0_0;
extern Il2CppType t2704_1_0_0;
struct t39;
extern TypeInfo t39_TI;
extern CustomAttributesCache t39__CustomAttributeCache;
extern CustomAttributesCache t39__CustomAttributeCache_m_Group;
extern CustomAttributesCache t39__CustomAttributeCache_m_IsOn;
TypeInfo t2704_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Toggle[]", "UnityEngine.UI", t2704_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t39_TI, t2704_ITIs, t2704_VT, &EmptyCustomAttributesCache, &t2704_TI, &t2704_0_0_0, &t2704_1_0_0, t2704_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t39 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 281, 60, 64};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3863_TI;



// Metadata Definition UnityEngine.UI.Toggle/ToggleTransition[]
static MethodInfo* t3863_MIs[] =
{
	NULL
};
extern MethodInfo m21504_MI;
extern MethodInfo m21505_MI;
extern MethodInfo m21506_MI;
extern MethodInfo m21507_MI;
extern MethodInfo m21508_MI;
extern MethodInfo m21509_MI;
extern MethodInfo m21503_MI;
extern MethodInfo m21511_MI;
extern MethodInfo m21512_MI;
static MethodInfo* t3863_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21504_MI,
	&m5907_MI,
	&m21505_MI,
	&m21506_MI,
	&m21507_MI,
	&m21508_MI,
	&m21509_MI,
	&m5908_MI,
	&m21503_MI,
	&m21511_MI,
	&m21512_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5430_TI;
extern TypeInfo t5431_TI;
extern TypeInfo t5432_TI;
static TypeInfo* t3863_ITIs[] = 
{
	&t5430_TI,
	&t5431_TI,
	&t5432_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3863_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5430_TI, 21},
	{ &t5431_TI, 28},
	{ &t5432_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3863_0_0_0;
extern Il2CppType t3863_1_0_0;
#include "t239.h"
extern TypeInfo t239_TI;
TypeInfo t3863_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "ToggleTransition[]", "", t3863_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t239_TI, t3863_ITIs, t3863_VT, &EmptyCustomAttributesCache, &t44_TI, &t3863_0_0_0, &t3863_1_0_0, t3863_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 258, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3864_TI;



// Metadata Definition UnityEngine.UI.ToggleGroup[]
static MethodInfo* t3864_MIs[] =
{
	NULL
};
extern MethodInfo m21515_MI;
extern MethodInfo m21516_MI;
extern MethodInfo m21517_MI;
extern MethodInfo m21518_MI;
extern MethodInfo m21519_MI;
extern MethodInfo m21520_MI;
extern MethodInfo m21514_MI;
extern MethodInfo m21522_MI;
extern MethodInfo m21523_MI;
static MethodInfo* t3864_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21515_MI,
	&m5907_MI,
	&m21516_MI,
	&m21517_MI,
	&m21518_MI,
	&m21519_MI,
	&m21520_MI,
	&m5908_MI,
	&m21514_MI,
	&m21522_MI,
	&m21523_MI,
	&m5905_MI,
	&m5906_MI,
	&m19777_MI,
	&m5907_MI,
	&m19778_MI,
	&m19779_MI,
	&m19780_MI,
	&m19781_MI,
	&m19782_MI,
	&m5908_MI,
	&m19776_MI,
	&m19784_MI,
	&m19785_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5433_TI;
extern TypeInfo t5434_TI;
extern TypeInfo t5435_TI;
static TypeInfo* t3864_ITIs[] = 
{
	&t5433_TI,
	&t5434_TI,
	&t5435_TI,
	&t5122_TI,
	&t5123_TI,
	&t5124_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3864_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5433_TI, 21},
	{ &t5434_TI, 28},
	{ &t5435_TI, 33},
	{ &t5122_TI, 34},
	{ &t5123_TI, 41},
	{ &t5124_TI, 46},
	{ &t5059_TI, 47},
	{ &t5060_TI, 54},
	{ &t5061_TI, 59},
	{ &t5062_TI, 60},
	{ &t5063_TI, 67},
	{ &t5064_TI, 72},
	{ &t2240_TI, 73},
	{ &t2245_TI, 80},
	{ &t2241_TI, 85},
	{ &t5065_TI, 86},
	{ &t5066_TI, 93},
	{ &t5067_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3864_0_0_0;
extern Il2CppType t3864_1_0_0;
struct t242;
extern TypeInfo t242_TI;
extern CustomAttributesCache t242__CustomAttributeCache;
extern CustomAttributesCache t242__CustomAttributeCache_m_AllowSwitchOff;
extern CustomAttributesCache t242__CustomAttributeCache_U3CU3Ef__am$cache2;
extern CustomAttributesCache t242__CustomAttributeCache_U3CU3Ef__am$cache3;
extern CustomAttributesCache t242__CustomAttributeCache_m1020;
extern CustomAttributesCache t242__CustomAttributeCache_m1021;
TypeInfo t3864_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "ToggleGroup[]", "UnityEngine.UI", t3864_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t242_TI, t3864_ITIs, t3864_VT, &EmptyCustomAttributesCache, &t3864_TI, &t3864_0_0_0, &t3864_1_0_0, t3864_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t242 *), -1, sizeof(t3864_SFs), 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3865_TI;



// Metadata Definition UnityEngine.UI.AspectRatioFitter[]
static MethodInfo* t3865_MIs[] =
{
	NULL
};
extern MethodInfo m21544_MI;
extern MethodInfo m21545_MI;
extern MethodInfo m21546_MI;
extern MethodInfo m21547_MI;
extern MethodInfo m21548_MI;
extern MethodInfo m21549_MI;
extern MethodInfo m21543_MI;
extern MethodInfo m21551_MI;
extern MethodInfo m21552_MI;
extern MethodInfo m21555_MI;
extern MethodInfo m21556_MI;
extern MethodInfo m21557_MI;
extern MethodInfo m21558_MI;
extern MethodInfo m21559_MI;
extern MethodInfo m21560_MI;
extern MethodInfo m21554_MI;
extern MethodInfo m21562_MI;
extern MethodInfo m21563_MI;
extern MethodInfo m21566_MI;
extern MethodInfo m21567_MI;
extern MethodInfo m21568_MI;
extern MethodInfo m21569_MI;
extern MethodInfo m21570_MI;
extern MethodInfo m21571_MI;
extern MethodInfo m21565_MI;
extern MethodInfo m21573_MI;
extern MethodInfo m21574_MI;
static MethodInfo* t3865_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21544_MI,
	&m5907_MI,
	&m21545_MI,
	&m21546_MI,
	&m21547_MI,
	&m21548_MI,
	&m21549_MI,
	&m5908_MI,
	&m21543_MI,
	&m21551_MI,
	&m21552_MI,
	&m5905_MI,
	&m5906_MI,
	&m21555_MI,
	&m5907_MI,
	&m21556_MI,
	&m21557_MI,
	&m21558_MI,
	&m21559_MI,
	&m21560_MI,
	&m5908_MI,
	&m21554_MI,
	&m21562_MI,
	&m21563_MI,
	&m5905_MI,
	&m5906_MI,
	&m21566_MI,
	&m5907_MI,
	&m21567_MI,
	&m21568_MI,
	&m21569_MI,
	&m21570_MI,
	&m21571_MI,
	&m5908_MI,
	&m21565_MI,
	&m21573_MI,
	&m21574_MI,
	&m5905_MI,
	&m5906_MI,
	&m19777_MI,
	&m5907_MI,
	&m19778_MI,
	&m19779_MI,
	&m19780_MI,
	&m19781_MI,
	&m19782_MI,
	&m5908_MI,
	&m19776_MI,
	&m19784_MI,
	&m19785_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5436_TI;
extern TypeInfo t5437_TI;
extern TypeInfo t5438_TI;
extern TypeInfo t5439_TI;
extern TypeInfo t5440_TI;
extern TypeInfo t5441_TI;
extern TypeInfo t5442_TI;
extern TypeInfo t5443_TI;
extern TypeInfo t5444_TI;
static TypeInfo* t3865_ITIs[] = 
{
	&t5436_TI,
	&t5437_TI,
	&t5438_TI,
	&t5439_TI,
	&t5440_TI,
	&t5441_TI,
	&t5442_TI,
	&t5443_TI,
	&t5444_TI,
	&t5122_TI,
	&t5123_TI,
	&t5124_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3865_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5436_TI, 21},
	{ &t5437_TI, 28},
	{ &t5438_TI, 33},
	{ &t5439_TI, 34},
	{ &t5440_TI, 41},
	{ &t5441_TI, 46},
	{ &t5442_TI, 47},
	{ &t5443_TI, 54},
	{ &t5444_TI, 59},
	{ &t5122_TI, 60},
	{ &t5123_TI, 67},
	{ &t5124_TI, 72},
	{ &t5059_TI, 73},
	{ &t5060_TI, 80},
	{ &t5061_TI, 85},
	{ &t5062_TI, 86},
	{ &t5063_TI, 93},
	{ &t5064_TI, 98},
	{ &t2240_TI, 99},
	{ &t2245_TI, 106},
	{ &t2241_TI, 111},
	{ &t5065_TI, 112},
	{ &t5066_TI, 119},
	{ &t5067_TI, 124},
	{ &t2182_TI, 125},
	{ &t2186_TI, 132},
	{ &t2183_TI, 137},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3865_0_0_0;
extern Il2CppType t3865_1_0_0;
struct t248;
extern TypeInfo t248_TI;
extern CustomAttributesCache t248__CustomAttributeCache;
extern CustomAttributesCache t248__CustomAttributeCache_m_AspectMode;
extern CustomAttributesCache t248__CustomAttributeCache_m_AspectRatio;
TypeInfo t3865_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "AspectRatioFitter[]", "UnityEngine.UI", t3865_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t248_TI, t3865_ITIs, t3865_VT, &EmptyCustomAttributesCache, &t3865_TI, &t3865_0_0_0, &t3865_1_0_0, t3865_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t248 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 138, 27, 31};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3866_TI;



// Metadata Definition UnityEngine.UI.ILayoutController[]
static MethodInfo* t3866_MIs[] =
{
	NULL
};
static MethodInfo* t3866_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21555_MI,
	&m5907_MI,
	&m21556_MI,
	&m21557_MI,
	&m21558_MI,
	&m21559_MI,
	&m21560_MI,
	&m5908_MI,
	&m21554_MI,
	&m21562_MI,
	&m21563_MI,
};
static TypeInfo* t3866_ITIs[] = 
{
	&t5439_TI,
	&t5440_TI,
	&t5441_TI,
};
static Il2CppInterfaceOffsetPair t3866_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5439_TI, 21},
	{ &t5440_TI, 28},
	{ &t5441_TI, 33},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3866_0_0_0;
extern Il2CppType t3866_1_0_0;
struct t409;
extern TypeInfo t409_TI;
TypeInfo t3866_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "ILayoutController[]", "UnityEngine.UI", t3866_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t409_TI, t3866_ITIs, t3866_VT, &EmptyCustomAttributesCache, &t3866_TI, &t3866_0_0_0, &t3866_1_0_0, t3866_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3867_TI;



// Metadata Definition UnityEngine.UI.ILayoutSelfController[]
static MethodInfo* t3867_MIs[] =
{
	NULL
};
static MethodInfo* t3867_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21566_MI,
	&m5907_MI,
	&m21567_MI,
	&m21568_MI,
	&m21569_MI,
	&m21570_MI,
	&m21571_MI,
	&m5908_MI,
	&m21565_MI,
	&m21573_MI,
	&m21574_MI,
	&m5905_MI,
	&m5906_MI,
	&m21555_MI,
	&m5907_MI,
	&m21556_MI,
	&m21557_MI,
	&m21558_MI,
	&m21559_MI,
	&m21560_MI,
	&m5908_MI,
	&m21554_MI,
	&m21562_MI,
	&m21563_MI,
};
static TypeInfo* t3867_ITIs[] = 
{
	&t5442_TI,
	&t5443_TI,
	&t5444_TI,
	&t5439_TI,
	&t5440_TI,
	&t5441_TI,
};
static Il2CppInterfaceOffsetPair t3867_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5442_TI, 21},
	{ &t5443_TI, 28},
	{ &t5444_TI, 33},
	{ &t5439_TI, 34},
	{ &t5440_TI, 41},
	{ &t5441_TI, 46},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3867_0_0_0;
extern Il2CppType t3867_1_0_0;
struct t410;
extern TypeInfo t410_TI;
TypeInfo t3867_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "ILayoutSelfController[]", "UnityEngine.UI", t3867_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t410_TI, t3867_ITIs, t3867_VT, &EmptyCustomAttributesCache, &t3867_TI, &t3867_0_0_0, &t3867_1_0_0, t3867_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3868_TI;



// Metadata Definition UnityEngine.UI.AspectRatioFitter/AspectMode[]
static MethodInfo* t3868_MIs[] =
{
	NULL
};
extern MethodInfo m21578_MI;
extern MethodInfo m21579_MI;
extern MethodInfo m21580_MI;
extern MethodInfo m21581_MI;
extern MethodInfo m21582_MI;
extern MethodInfo m21583_MI;
extern MethodInfo m21577_MI;
extern MethodInfo m21585_MI;
extern MethodInfo m21586_MI;
static MethodInfo* t3868_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21578_MI,
	&m5907_MI,
	&m21579_MI,
	&m21580_MI,
	&m21581_MI,
	&m21582_MI,
	&m21583_MI,
	&m5908_MI,
	&m21577_MI,
	&m21585_MI,
	&m21586_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5445_TI;
extern TypeInfo t5446_TI;
extern TypeInfo t5447_TI;
static TypeInfo* t3868_ITIs[] = 
{
	&t5445_TI,
	&t5446_TI,
	&t5447_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3868_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5445_TI, 21},
	{ &t5446_TI, 28},
	{ &t5447_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3868_0_0_0;
extern Il2CppType t3868_1_0_0;
#include "t247.h"
extern TypeInfo t247_TI;
TypeInfo t3868_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "AspectMode[]", "", t3868_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t247_TI, t3868_ITIs, t3868_VT, &EmptyCustomAttributesCache, &t44_TI, &t3868_0_0_0, &t3868_1_0_0, t3868_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 258, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3869_TI;



// Metadata Definition UnityEngine.UI.CanvasScaler[]
static MethodInfo* t3869_MIs[] =
{
	NULL
};
extern MethodInfo m21589_MI;
extern MethodInfo m21590_MI;
extern MethodInfo m21591_MI;
extern MethodInfo m21592_MI;
extern MethodInfo m21593_MI;
extern MethodInfo m21594_MI;
extern MethodInfo m21588_MI;
extern MethodInfo m21596_MI;
extern MethodInfo m21597_MI;
static MethodInfo* t3869_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21589_MI,
	&m5907_MI,
	&m21590_MI,
	&m21591_MI,
	&m21592_MI,
	&m21593_MI,
	&m21594_MI,
	&m5908_MI,
	&m21588_MI,
	&m21596_MI,
	&m21597_MI,
	&m5905_MI,
	&m5906_MI,
	&m19777_MI,
	&m5907_MI,
	&m19778_MI,
	&m19779_MI,
	&m19780_MI,
	&m19781_MI,
	&m19782_MI,
	&m5908_MI,
	&m19776_MI,
	&m19784_MI,
	&m19785_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5448_TI;
extern TypeInfo t5449_TI;
extern TypeInfo t5450_TI;
static TypeInfo* t3869_ITIs[] = 
{
	&t5448_TI,
	&t5449_TI,
	&t5450_TI,
	&t5122_TI,
	&t5123_TI,
	&t5124_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3869_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5448_TI, 21},
	{ &t5449_TI, 28},
	{ &t5450_TI, 33},
	{ &t5122_TI, 34},
	{ &t5123_TI, 41},
	{ &t5124_TI, 46},
	{ &t5059_TI, 47},
	{ &t5060_TI, 54},
	{ &t5061_TI, 59},
	{ &t5062_TI, 60},
	{ &t5063_TI, 67},
	{ &t5064_TI, 72},
	{ &t2240_TI, 73},
	{ &t2245_TI, 80},
	{ &t2241_TI, 85},
	{ &t5065_TI, 86},
	{ &t5066_TI, 93},
	{ &t5067_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3869_0_0_0;
extern Il2CppType t3869_1_0_0;
struct t252;
extern TypeInfo t252_TI;
extern CustomAttributesCache t252__CustomAttributeCache;
extern CustomAttributesCache t252__CustomAttributeCache_m_UiScaleMode;
extern CustomAttributesCache t252__CustomAttributeCache_m_ReferencePixelsPerUnit;
extern CustomAttributesCache t252__CustomAttributeCache_m_ScaleFactor;
extern CustomAttributesCache t252__CustomAttributeCache_m_ReferenceResolution;
extern CustomAttributesCache t252__CustomAttributeCache_m_ScreenMatchMode;
extern CustomAttributesCache t252__CustomAttributeCache_m_MatchWidthOrHeight;
extern CustomAttributesCache t252__CustomAttributeCache_m_PhysicalUnit;
extern CustomAttributesCache t252__CustomAttributeCache_m_FallbackScreenDPI;
extern CustomAttributesCache t252__CustomAttributeCache_m_DefaultSpriteDPI;
extern CustomAttributesCache t252__CustomAttributeCache_m_DynamicPixelsPerUnit;
TypeInfo t3869_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "CanvasScaler[]", "UnityEngine.UI", t3869_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t252_TI, t3869_ITIs, t3869_VT, &EmptyCustomAttributesCache, &t3869_TI, &t3869_0_0_0, &t3869_1_0_0, t3869_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t252 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3870_TI;



// Metadata Definition UnityEngine.UI.CanvasScaler/ScaleMode[]
static MethodInfo* t3870_MIs[] =
{
	NULL
};
extern MethodInfo m21601_MI;
extern MethodInfo m21602_MI;
extern MethodInfo m21603_MI;
extern MethodInfo m21604_MI;
extern MethodInfo m21605_MI;
extern MethodInfo m21606_MI;
extern MethodInfo m21600_MI;
extern MethodInfo m21608_MI;
extern MethodInfo m21609_MI;
static MethodInfo* t3870_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21601_MI,
	&m5907_MI,
	&m21602_MI,
	&m21603_MI,
	&m21604_MI,
	&m21605_MI,
	&m21606_MI,
	&m5908_MI,
	&m21600_MI,
	&m21608_MI,
	&m21609_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5451_TI;
extern TypeInfo t5452_TI;
extern TypeInfo t5453_TI;
static TypeInfo* t3870_ITIs[] = 
{
	&t5451_TI,
	&t5452_TI,
	&t5453_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3870_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5451_TI, 21},
	{ &t5452_TI, 28},
	{ &t5453_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3870_0_0_0;
extern Il2CppType t3870_1_0_0;
#include "t249.h"
extern TypeInfo t249_TI;
TypeInfo t3870_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "ScaleMode[]", "", t3870_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t249_TI, t3870_ITIs, t3870_VT, &EmptyCustomAttributesCache, &t44_TI, &t3870_0_0_0, &t3870_1_0_0, t3870_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 258, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3871_TI;



// Metadata Definition UnityEngine.UI.CanvasScaler/ScreenMatchMode[]
static MethodInfo* t3871_MIs[] =
{
	NULL
};
extern MethodInfo m21612_MI;
extern MethodInfo m21613_MI;
extern MethodInfo m21614_MI;
extern MethodInfo m21615_MI;
extern MethodInfo m21616_MI;
extern MethodInfo m21617_MI;
extern MethodInfo m21611_MI;
extern MethodInfo m21619_MI;
extern MethodInfo m21620_MI;
static MethodInfo* t3871_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21612_MI,
	&m5907_MI,
	&m21613_MI,
	&m21614_MI,
	&m21615_MI,
	&m21616_MI,
	&m21617_MI,
	&m5908_MI,
	&m21611_MI,
	&m21619_MI,
	&m21620_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5454_TI;
extern TypeInfo t5455_TI;
extern TypeInfo t5456_TI;
static TypeInfo* t3871_ITIs[] = 
{
	&t5454_TI,
	&t5455_TI,
	&t5456_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3871_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5454_TI, 21},
	{ &t5455_TI, 28},
	{ &t5456_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3871_0_0_0;
extern Il2CppType t3871_1_0_0;
#include "t250.h"
extern TypeInfo t250_TI;
TypeInfo t3871_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "ScreenMatchMode[]", "", t3871_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t250_TI, t3871_ITIs, t3871_VT, &EmptyCustomAttributesCache, &t44_TI, &t3871_0_0_0, &t3871_1_0_0, t3871_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 258, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3872_TI;



// Metadata Definition UnityEngine.UI.CanvasScaler/Unit[]
static MethodInfo* t3872_MIs[] =
{
	NULL
};
extern MethodInfo m21623_MI;
extern MethodInfo m21624_MI;
extern MethodInfo m21625_MI;
extern MethodInfo m21626_MI;
extern MethodInfo m21627_MI;
extern MethodInfo m21628_MI;
extern MethodInfo m21622_MI;
extern MethodInfo m21630_MI;
extern MethodInfo m21631_MI;
static MethodInfo* t3872_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21623_MI,
	&m5907_MI,
	&m21624_MI,
	&m21625_MI,
	&m21626_MI,
	&m21627_MI,
	&m21628_MI,
	&m5908_MI,
	&m21622_MI,
	&m21630_MI,
	&m21631_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5457_TI;
extern TypeInfo t5458_TI;
extern TypeInfo t5459_TI;
static TypeInfo* t3872_ITIs[] = 
{
	&t5457_TI,
	&t5458_TI,
	&t5459_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3872_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5457_TI, 21},
	{ &t5458_TI, 28},
	{ &t5459_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3872_0_0_0;
extern Il2CppType t3872_1_0_0;
#include "t251.h"
extern TypeInfo t251_TI;
TypeInfo t3872_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Unit[]", "", t3872_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t251_TI, t3872_ITIs, t3872_VT, &EmptyCustomAttributesCache, &t44_TI, &t3872_0_0_0, &t3872_1_0_0, t3872_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 258, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3873_TI;



// Metadata Definition UnityEngine.UI.ContentSizeFitter[]
static MethodInfo* t3873_MIs[] =
{
	NULL
};
extern MethodInfo m21634_MI;
extern MethodInfo m21635_MI;
extern MethodInfo m21636_MI;
extern MethodInfo m21637_MI;
extern MethodInfo m21638_MI;
extern MethodInfo m21639_MI;
extern MethodInfo m21633_MI;
extern MethodInfo m21641_MI;
extern MethodInfo m21642_MI;
static MethodInfo* t3873_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21634_MI,
	&m5907_MI,
	&m21635_MI,
	&m21636_MI,
	&m21637_MI,
	&m21638_MI,
	&m21639_MI,
	&m5908_MI,
	&m21633_MI,
	&m21641_MI,
	&m21642_MI,
	&m5905_MI,
	&m5906_MI,
	&m21555_MI,
	&m5907_MI,
	&m21556_MI,
	&m21557_MI,
	&m21558_MI,
	&m21559_MI,
	&m21560_MI,
	&m5908_MI,
	&m21554_MI,
	&m21562_MI,
	&m21563_MI,
	&m5905_MI,
	&m5906_MI,
	&m21566_MI,
	&m5907_MI,
	&m21567_MI,
	&m21568_MI,
	&m21569_MI,
	&m21570_MI,
	&m21571_MI,
	&m5908_MI,
	&m21565_MI,
	&m21573_MI,
	&m21574_MI,
	&m5905_MI,
	&m5906_MI,
	&m19777_MI,
	&m5907_MI,
	&m19778_MI,
	&m19779_MI,
	&m19780_MI,
	&m19781_MI,
	&m19782_MI,
	&m5908_MI,
	&m19776_MI,
	&m19784_MI,
	&m19785_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5460_TI;
extern TypeInfo t5461_TI;
extern TypeInfo t5462_TI;
static TypeInfo* t3873_ITIs[] = 
{
	&t5460_TI,
	&t5461_TI,
	&t5462_TI,
	&t5439_TI,
	&t5440_TI,
	&t5441_TI,
	&t5442_TI,
	&t5443_TI,
	&t5444_TI,
	&t5122_TI,
	&t5123_TI,
	&t5124_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3873_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5460_TI, 21},
	{ &t5461_TI, 28},
	{ &t5462_TI, 33},
	{ &t5439_TI, 34},
	{ &t5440_TI, 41},
	{ &t5441_TI, 46},
	{ &t5442_TI, 47},
	{ &t5443_TI, 54},
	{ &t5444_TI, 59},
	{ &t5122_TI, 60},
	{ &t5123_TI, 67},
	{ &t5124_TI, 72},
	{ &t5059_TI, 73},
	{ &t5060_TI, 80},
	{ &t5061_TI, 85},
	{ &t5062_TI, 86},
	{ &t5063_TI, 93},
	{ &t5064_TI, 98},
	{ &t2240_TI, 99},
	{ &t2245_TI, 106},
	{ &t2241_TI, 111},
	{ &t5065_TI, 112},
	{ &t5066_TI, 119},
	{ &t5067_TI, 124},
	{ &t2182_TI, 125},
	{ &t2186_TI, 132},
	{ &t2183_TI, 137},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3873_0_0_0;
extern Il2CppType t3873_1_0_0;
struct t254;
extern TypeInfo t254_TI;
extern CustomAttributesCache t254__CustomAttributeCache;
extern CustomAttributesCache t254__CustomAttributeCache_m_HorizontalFit;
extern CustomAttributesCache t254__CustomAttributeCache_m_VerticalFit;
TypeInfo t3873_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "ContentSizeFitter[]", "UnityEngine.UI", t3873_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t254_TI, t3873_ITIs, t3873_VT, &EmptyCustomAttributesCache, &t3873_TI, &t3873_0_0_0, &t3873_1_0_0, t3873_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t254 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 138, 27, 31};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3874_TI;



// Metadata Definition UnityEngine.UI.ContentSizeFitter/FitMode[]
static MethodInfo* t3874_MIs[] =
{
	NULL
};
extern MethodInfo m21646_MI;
extern MethodInfo m21647_MI;
extern MethodInfo m21648_MI;
extern MethodInfo m21649_MI;
extern MethodInfo m21650_MI;
extern MethodInfo m21651_MI;
extern MethodInfo m21645_MI;
extern MethodInfo m21653_MI;
extern MethodInfo m21654_MI;
static MethodInfo* t3874_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21646_MI,
	&m5907_MI,
	&m21647_MI,
	&m21648_MI,
	&m21649_MI,
	&m21650_MI,
	&m21651_MI,
	&m5908_MI,
	&m21645_MI,
	&m21653_MI,
	&m21654_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5463_TI;
extern TypeInfo t5464_TI;
extern TypeInfo t5465_TI;
static TypeInfo* t3874_ITIs[] = 
{
	&t5463_TI,
	&t5464_TI,
	&t5465_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3874_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5463_TI, 21},
	{ &t5464_TI, 28},
	{ &t5465_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3874_0_0_0;
extern Il2CppType t3874_1_0_0;
#include "t253.h"
extern TypeInfo t253_TI;
TypeInfo t3874_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "FitMode[]", "", t3874_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t253_TI, t3874_ITIs, t3874_VT, &EmptyCustomAttributesCache, &t44_TI, &t3874_0_0_0, &t3874_1_0_0, t3874_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 258, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3875_TI;



// Metadata Definition UnityEngine.UI.GridLayoutGroup[]
static MethodInfo* t3875_MIs[] =
{
	NULL
};
extern MethodInfo m21657_MI;
extern MethodInfo m21658_MI;
extern MethodInfo m21659_MI;
extern MethodInfo m21660_MI;
extern MethodInfo m21661_MI;
extern MethodInfo m21662_MI;
extern MethodInfo m21656_MI;
extern MethodInfo m21664_MI;
extern MethodInfo m21665_MI;
extern MethodInfo m21668_MI;
extern MethodInfo m21669_MI;
extern MethodInfo m21670_MI;
extern MethodInfo m21671_MI;
extern MethodInfo m21672_MI;
extern MethodInfo m21673_MI;
extern MethodInfo m21667_MI;
extern MethodInfo m21675_MI;
extern MethodInfo m21676_MI;
extern MethodInfo m21679_MI;
extern MethodInfo m21680_MI;
extern MethodInfo m21681_MI;
extern MethodInfo m21682_MI;
extern MethodInfo m21683_MI;
extern MethodInfo m21684_MI;
extern MethodInfo m21678_MI;
extern MethodInfo m21686_MI;
extern MethodInfo m21687_MI;
static MethodInfo* t3875_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21657_MI,
	&m5907_MI,
	&m21658_MI,
	&m21659_MI,
	&m21660_MI,
	&m21661_MI,
	&m21662_MI,
	&m5908_MI,
	&m21656_MI,
	&m21664_MI,
	&m21665_MI,
	&m5905_MI,
	&m5906_MI,
	&m21668_MI,
	&m5907_MI,
	&m21669_MI,
	&m21670_MI,
	&m21671_MI,
	&m21672_MI,
	&m21673_MI,
	&m5908_MI,
	&m21667_MI,
	&m21675_MI,
	&m21676_MI,
	&m5905_MI,
	&m5906_MI,
	&m20809_MI,
	&m5907_MI,
	&m20810_MI,
	&m20811_MI,
	&m20812_MI,
	&m20813_MI,
	&m20814_MI,
	&m5908_MI,
	&m20808_MI,
	&m20816_MI,
	&m20817_MI,
	&m5905_MI,
	&m5906_MI,
	&m21555_MI,
	&m5907_MI,
	&m21556_MI,
	&m21557_MI,
	&m21558_MI,
	&m21559_MI,
	&m21560_MI,
	&m5908_MI,
	&m21554_MI,
	&m21562_MI,
	&m21563_MI,
	&m5905_MI,
	&m5906_MI,
	&m21679_MI,
	&m5907_MI,
	&m21680_MI,
	&m21681_MI,
	&m21682_MI,
	&m21683_MI,
	&m21684_MI,
	&m5908_MI,
	&m21678_MI,
	&m21686_MI,
	&m21687_MI,
	&m5905_MI,
	&m5906_MI,
	&m19777_MI,
	&m5907_MI,
	&m19778_MI,
	&m19779_MI,
	&m19780_MI,
	&m19781_MI,
	&m19782_MI,
	&m5908_MI,
	&m19776_MI,
	&m19784_MI,
	&m19785_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5466_TI;
extern TypeInfo t5467_TI;
extern TypeInfo t5468_TI;
extern TypeInfo t5469_TI;
extern TypeInfo t5470_TI;
extern TypeInfo t5471_TI;
extern TypeInfo t5472_TI;
extern TypeInfo t5473_TI;
extern TypeInfo t5474_TI;
static TypeInfo* t3875_ITIs[] = 
{
	&t5466_TI,
	&t5467_TI,
	&t5468_TI,
	&t5469_TI,
	&t5470_TI,
	&t5471_TI,
	&t5304_TI,
	&t5305_TI,
	&t5306_TI,
	&t5439_TI,
	&t5440_TI,
	&t5441_TI,
	&t5472_TI,
	&t5473_TI,
	&t5474_TI,
	&t5122_TI,
	&t5123_TI,
	&t5124_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3875_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5466_TI, 21},
	{ &t5467_TI, 28},
	{ &t5468_TI, 33},
	{ &t5469_TI, 34},
	{ &t5470_TI, 41},
	{ &t5471_TI, 46},
	{ &t5304_TI, 47},
	{ &t5305_TI, 54},
	{ &t5306_TI, 59},
	{ &t5439_TI, 60},
	{ &t5440_TI, 67},
	{ &t5441_TI, 72},
	{ &t5472_TI, 73},
	{ &t5473_TI, 80},
	{ &t5474_TI, 85},
	{ &t5122_TI, 86},
	{ &t5123_TI, 93},
	{ &t5124_TI, 98},
	{ &t5059_TI, 99},
	{ &t5060_TI, 106},
	{ &t5061_TI, 111},
	{ &t5062_TI, 112},
	{ &t5063_TI, 119},
	{ &t5064_TI, 124},
	{ &t2240_TI, 125},
	{ &t2245_TI, 132},
	{ &t2241_TI, 137},
	{ &t5065_TI, 138},
	{ &t5066_TI, 145},
	{ &t5067_TI, 150},
	{ &t2182_TI, 151},
	{ &t2186_TI, 158},
	{ &t2183_TI, 163},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3875_0_0_0;
extern Il2CppType t3875_1_0_0;
struct t258;
extern TypeInfo t258_TI;
extern CustomAttributesCache t258__CustomAttributeCache;
extern CustomAttributesCache t258__CustomAttributeCache_m_StartCorner;
extern CustomAttributesCache t258__CustomAttributeCache_m_StartAxis;
extern CustomAttributesCache t258__CustomAttributeCache_m_CellSize;
extern CustomAttributesCache t258__CustomAttributeCache_m_Spacing;
extern CustomAttributesCache t258__CustomAttributeCache_m_Constraint;
extern CustomAttributesCache t258__CustomAttributeCache_m_ConstraintCount;
TypeInfo t3875_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "GridLayoutGroup[]", "UnityEngine.UI", t3875_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t258_TI, t3875_ITIs, t3875_VT, &EmptyCustomAttributesCache, &t3875_TI, &t3875_0_0_0, &t3875_1_0_0, t3875_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t258 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 164, 33, 37};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3876_TI;



// Metadata Definition UnityEngine.UI.LayoutGroup[]
static MethodInfo* t3876_MIs[] =
{
	NULL
};
static MethodInfo* t3876_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21668_MI,
	&m5907_MI,
	&m21669_MI,
	&m21670_MI,
	&m21671_MI,
	&m21672_MI,
	&m21673_MI,
	&m5908_MI,
	&m21667_MI,
	&m21675_MI,
	&m21676_MI,
	&m5905_MI,
	&m5906_MI,
	&m20809_MI,
	&m5907_MI,
	&m20810_MI,
	&m20811_MI,
	&m20812_MI,
	&m20813_MI,
	&m20814_MI,
	&m5908_MI,
	&m20808_MI,
	&m20816_MI,
	&m20817_MI,
	&m5905_MI,
	&m5906_MI,
	&m21555_MI,
	&m5907_MI,
	&m21556_MI,
	&m21557_MI,
	&m21558_MI,
	&m21559_MI,
	&m21560_MI,
	&m5908_MI,
	&m21554_MI,
	&m21562_MI,
	&m21563_MI,
	&m5905_MI,
	&m5906_MI,
	&m21679_MI,
	&m5907_MI,
	&m21680_MI,
	&m21681_MI,
	&m21682_MI,
	&m21683_MI,
	&m21684_MI,
	&m5908_MI,
	&m21678_MI,
	&m21686_MI,
	&m21687_MI,
	&m5905_MI,
	&m5906_MI,
	&m19777_MI,
	&m5907_MI,
	&m19778_MI,
	&m19779_MI,
	&m19780_MI,
	&m19781_MI,
	&m19782_MI,
	&m5908_MI,
	&m19776_MI,
	&m19784_MI,
	&m19785_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
static TypeInfo* t3876_ITIs[] = 
{
	&t5469_TI,
	&t5470_TI,
	&t5471_TI,
	&t5304_TI,
	&t5305_TI,
	&t5306_TI,
	&t5439_TI,
	&t5440_TI,
	&t5441_TI,
	&t5472_TI,
	&t5473_TI,
	&t5474_TI,
	&t5122_TI,
	&t5123_TI,
	&t5124_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3876_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5469_TI, 21},
	{ &t5470_TI, 28},
	{ &t5471_TI, 33},
	{ &t5304_TI, 34},
	{ &t5305_TI, 41},
	{ &t5306_TI, 46},
	{ &t5439_TI, 47},
	{ &t5440_TI, 54},
	{ &t5441_TI, 59},
	{ &t5472_TI, 60},
	{ &t5473_TI, 67},
	{ &t5474_TI, 72},
	{ &t5122_TI, 73},
	{ &t5123_TI, 80},
	{ &t5124_TI, 85},
	{ &t5059_TI, 86},
	{ &t5060_TI, 93},
	{ &t5061_TI, 98},
	{ &t5062_TI, 99},
	{ &t5063_TI, 106},
	{ &t5064_TI, 111},
	{ &t2240_TI, 112},
	{ &t2245_TI, 119},
	{ &t2241_TI, 124},
	{ &t5065_TI, 125},
	{ &t5066_TI, 132},
	{ &t5067_TI, 137},
	{ &t2182_TI, 138},
	{ &t2186_TI, 145},
	{ &t2183_TI, 150},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3876_0_0_0;
extern Il2CppType t3876_1_0_0;
struct t259;
extern TypeInfo t259_TI;
extern CustomAttributesCache t259__CustomAttributeCache;
extern CustomAttributesCache t259__CustomAttributeCache_m_Padding;
extern CustomAttributesCache t259__CustomAttributeCache_m_ChildAlignment;
TypeInfo t3876_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "LayoutGroup[]", "UnityEngine.UI", t3876_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t259_TI, t3876_ITIs, t3876_VT, &EmptyCustomAttributesCache, &t3876_TI, &t3876_0_0_0, &t3876_1_0_0, t3876_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t259 *), -1, 0, 0, -1, 1048705, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 151, 30, 34};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3877_TI;



// Metadata Definition UnityEngine.UI.ILayoutGroup[]
static MethodInfo* t3877_MIs[] =
{
	NULL
};
static MethodInfo* t3877_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21679_MI,
	&m5907_MI,
	&m21680_MI,
	&m21681_MI,
	&m21682_MI,
	&m21683_MI,
	&m21684_MI,
	&m5908_MI,
	&m21678_MI,
	&m21686_MI,
	&m21687_MI,
	&m5905_MI,
	&m5906_MI,
	&m21555_MI,
	&m5907_MI,
	&m21556_MI,
	&m21557_MI,
	&m21558_MI,
	&m21559_MI,
	&m21560_MI,
	&m5908_MI,
	&m21554_MI,
	&m21562_MI,
	&m21563_MI,
};
static TypeInfo* t3877_ITIs[] = 
{
	&t5472_TI,
	&t5473_TI,
	&t5474_TI,
	&t5439_TI,
	&t5440_TI,
	&t5441_TI,
};
static Il2CppInterfaceOffsetPair t3877_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5472_TI, 21},
	{ &t5473_TI, 28},
	{ &t5474_TI, 33},
	{ &t5439_TI, 34},
	{ &t5440_TI, 41},
	{ &t5441_TI, 46},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3877_0_0_0;
extern Il2CppType t3877_1_0_0;
struct t411;
extern TypeInfo t411_TI;
TypeInfo t3877_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "ILayoutGroup[]", "UnityEngine.UI", t3877_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t411_TI, t3877_ITIs, t3877_VT, &EmptyCustomAttributesCache, &t3877_TI, &t3877_0_0_0, &t3877_1_0_0, t3877_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2743_TI;



// Metadata Definition UnityEngine.RectTransform[]
static MethodInfo* t2743_MIs[] =
{
	NULL
};
extern MethodInfo m21691_MI;
extern MethodInfo m21692_MI;
extern MethodInfo m21693_MI;
extern MethodInfo m21694_MI;
extern MethodInfo m21695_MI;
extern MethodInfo m21696_MI;
extern MethodInfo m21690_MI;
extern MethodInfo m21698_MI;
extern MethodInfo m21699_MI;
extern MethodInfo m20346_MI;
extern MethodInfo m20347_MI;
extern MethodInfo m20348_MI;
extern MethodInfo m20349_MI;
extern MethodInfo m20350_MI;
extern MethodInfo m20351_MI;
extern MethodInfo m20345_MI;
extern MethodInfo m20353_MI;
extern MethodInfo m20354_MI;
static MethodInfo* t2743_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21691_MI,
	&m5907_MI,
	&m21692_MI,
	&m21693_MI,
	&m21694_MI,
	&m21695_MI,
	&m21696_MI,
	&m5908_MI,
	&m21690_MI,
	&m21698_MI,
	&m21699_MI,
	&m5905_MI,
	&m5906_MI,
	&m20346_MI,
	&m5907_MI,
	&m20347_MI,
	&m20348_MI,
	&m20349_MI,
	&m20350_MI,
	&m20351_MI,
	&m5908_MI,
	&m20345_MI,
	&m20353_MI,
	&m20354_MI,
	&m5905_MI,
	&m5906_MI,
	&m20022_MI,
	&m5907_MI,
	&m20023_MI,
	&m20024_MI,
	&m20025_MI,
	&m20026_MI,
	&m20027_MI,
	&m5908_MI,
	&m20021_MI,
	&m20029_MI,
	&m20030_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t2745_TI;
extern TypeInfo t2752_TI;
extern TypeInfo t2746_TI;
extern TypeInfo t304_TI;
extern TypeInfo t104_TI;
extern TypeInfo t2319_TI;
static TypeInfo* t2743_ITIs[] = 
{
	&t2745_TI,
	&t2752_TI,
	&t2746_TI,
	&t304_TI,
	&t104_TI,
	&t2319_TI,
	&t5169_TI,
	&t5170_TI,
	&t5171_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2743_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2745_TI, 21},
	{ &t2752_TI, 28},
	{ &t2746_TI, 33},
	{ &t304_TI, 34},
	{ &t104_TI, 41},
	{ &t2319_TI, 46},
	{ &t5169_TI, 47},
	{ &t5170_TI, 54},
	{ &t5171_TI, 59},
	{ &t2240_TI, 60},
	{ &t2245_TI, 67},
	{ &t2241_TI, 72},
	{ &t5065_TI, 73},
	{ &t5066_TI, 80},
	{ &t5067_TI, 85},
	{ &t2182_TI, 86},
	{ &t2186_TI, 93},
	{ &t2183_TI, 98},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2743_0_0_0;
extern Il2CppType t2743_1_0_0;
struct t2;
extern TypeInfo t2_TI;
extern CustomAttributesCache t2__CustomAttributeCache_m2425;
extern CustomAttributesCache t2__CustomAttributeCache_m2426;
extern CustomAttributesCache t2__CustomAttributeCache_m2427;
extern CustomAttributesCache t2__CustomAttributeCache_m2428;
extern CustomAttributesCache t2__CustomAttributeCache_m2429;
extern CustomAttributesCache t2__CustomAttributeCache_m2430;
extern CustomAttributesCache t2__CustomAttributeCache_m2431;
extern CustomAttributesCache t2__CustomAttributeCache_m2432;
extern CustomAttributesCache t2__CustomAttributeCache_m2433;
extern CustomAttributesCache t2__CustomAttributeCache_m2434;
extern CustomAttributesCache t2__CustomAttributeCache_m2435;
TypeInfo t2743_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "RectTransform[]", "UnityEngine", t2743_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t2_TI, t2743_ITIs, t2743_VT, &EmptyCustomAttributesCache, &t2743_TI, &t2743_0_0_0, &t2743_1_0_0, t2743_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t2 *), -1, sizeof(t2743_SFs), 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 99, 18, 22};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3878_TI;



// Metadata Definition UnityEngine.UI.GridLayoutGroup/Corner[]
static MethodInfo* t3878_MIs[] =
{
	NULL
};
extern MethodInfo m21717_MI;
extern MethodInfo m21718_MI;
extern MethodInfo m21719_MI;
extern MethodInfo m21720_MI;
extern MethodInfo m21721_MI;
extern MethodInfo m21722_MI;
extern MethodInfo m21716_MI;
extern MethodInfo m21724_MI;
extern MethodInfo m21725_MI;
static MethodInfo* t3878_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21717_MI,
	&m5907_MI,
	&m21718_MI,
	&m21719_MI,
	&m21720_MI,
	&m21721_MI,
	&m21722_MI,
	&m5908_MI,
	&m21716_MI,
	&m21724_MI,
	&m21725_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5475_TI;
extern TypeInfo t5476_TI;
extern TypeInfo t5477_TI;
static TypeInfo* t3878_ITIs[] = 
{
	&t5475_TI,
	&t5476_TI,
	&t5477_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3878_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5475_TI, 21},
	{ &t5476_TI, 28},
	{ &t5477_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3878_0_0_0;
extern Il2CppType t3878_1_0_0;
#include "t255.h"
extern TypeInfo t255_TI;
TypeInfo t3878_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Corner[]", "", t3878_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t255_TI, t3878_ITIs, t3878_VT, &EmptyCustomAttributesCache, &t44_TI, &t3878_0_0_0, &t3878_1_0_0, t3878_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 258, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3879_TI;



// Metadata Definition UnityEngine.UI.GridLayoutGroup/Axis[]
static MethodInfo* t3879_MIs[] =
{
	NULL
};
extern MethodInfo m21728_MI;
extern MethodInfo m21729_MI;
extern MethodInfo m21730_MI;
extern MethodInfo m21731_MI;
extern MethodInfo m21732_MI;
extern MethodInfo m21733_MI;
extern MethodInfo m21727_MI;
extern MethodInfo m21735_MI;
extern MethodInfo m21736_MI;
static MethodInfo* t3879_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21728_MI,
	&m5907_MI,
	&m21729_MI,
	&m21730_MI,
	&m21731_MI,
	&m21732_MI,
	&m21733_MI,
	&m5908_MI,
	&m21727_MI,
	&m21735_MI,
	&m21736_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5478_TI;
extern TypeInfo t5479_TI;
extern TypeInfo t5480_TI;
static TypeInfo* t3879_ITIs[] = 
{
	&t5478_TI,
	&t5479_TI,
	&t5480_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3879_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5478_TI, 21},
	{ &t5479_TI, 28},
	{ &t5480_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3879_0_0_0;
extern Il2CppType t3879_1_0_0;
#include "t256.h"
extern TypeInfo t256_TI;
TypeInfo t3879_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Axis[]", "", t3879_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t256_TI, t3879_ITIs, t3879_VT, &EmptyCustomAttributesCache, &t44_TI, &t3879_0_0_0, &t3879_1_0_0, t3879_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 258, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3880_TI;



// Metadata Definition UnityEngine.UI.GridLayoutGroup/Constraint[]
static MethodInfo* t3880_MIs[] =
{
	NULL
};
extern MethodInfo m21739_MI;
extern MethodInfo m21740_MI;
extern MethodInfo m21741_MI;
extern MethodInfo m21742_MI;
extern MethodInfo m21743_MI;
extern MethodInfo m21744_MI;
extern MethodInfo m21738_MI;
extern MethodInfo m21746_MI;
extern MethodInfo m21747_MI;
static MethodInfo* t3880_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21739_MI,
	&m5907_MI,
	&m21740_MI,
	&m21741_MI,
	&m21742_MI,
	&m21743_MI,
	&m21744_MI,
	&m5908_MI,
	&m21738_MI,
	&m21746_MI,
	&m21747_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5481_TI;
extern TypeInfo t5482_TI;
extern TypeInfo t5483_TI;
static TypeInfo* t3880_ITIs[] = 
{
	&t5481_TI,
	&t5482_TI,
	&t5483_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3880_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5481_TI, 21},
	{ &t5482_TI, 28},
	{ &t5483_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3880_0_0_0;
extern Il2CppType t3880_1_0_0;
#include "t257.h"
extern TypeInfo t257_TI;
TypeInfo t3880_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Constraint[]", "", t3880_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t257_TI, t3880_ITIs, t3880_VT, &EmptyCustomAttributesCache, &t44_TI, &t3880_0_0_0, &t3880_1_0_0, t3880_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 258, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3881_TI;



// Metadata Definition UnityEngine.UI.HorizontalLayoutGroup[]
static MethodInfo* t3881_MIs[] =
{
	NULL
};
extern MethodInfo m21750_MI;
extern MethodInfo m21751_MI;
extern MethodInfo m21752_MI;
extern MethodInfo m21753_MI;
extern MethodInfo m21754_MI;
extern MethodInfo m21755_MI;
extern MethodInfo m21749_MI;
extern MethodInfo m21757_MI;
extern MethodInfo m21758_MI;
extern MethodInfo m21761_MI;
extern MethodInfo m21762_MI;
extern MethodInfo m21763_MI;
extern MethodInfo m21764_MI;
extern MethodInfo m21765_MI;
extern MethodInfo m21766_MI;
extern MethodInfo m21760_MI;
extern MethodInfo m21768_MI;
extern MethodInfo m21769_MI;
static MethodInfo* t3881_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21750_MI,
	&m5907_MI,
	&m21751_MI,
	&m21752_MI,
	&m21753_MI,
	&m21754_MI,
	&m21755_MI,
	&m5908_MI,
	&m21749_MI,
	&m21757_MI,
	&m21758_MI,
	&m5905_MI,
	&m5906_MI,
	&m21761_MI,
	&m5907_MI,
	&m21762_MI,
	&m21763_MI,
	&m21764_MI,
	&m21765_MI,
	&m21766_MI,
	&m5908_MI,
	&m21760_MI,
	&m21768_MI,
	&m21769_MI,
	&m5905_MI,
	&m5906_MI,
	&m21668_MI,
	&m5907_MI,
	&m21669_MI,
	&m21670_MI,
	&m21671_MI,
	&m21672_MI,
	&m21673_MI,
	&m5908_MI,
	&m21667_MI,
	&m21675_MI,
	&m21676_MI,
	&m5905_MI,
	&m5906_MI,
	&m20809_MI,
	&m5907_MI,
	&m20810_MI,
	&m20811_MI,
	&m20812_MI,
	&m20813_MI,
	&m20814_MI,
	&m5908_MI,
	&m20808_MI,
	&m20816_MI,
	&m20817_MI,
	&m5905_MI,
	&m5906_MI,
	&m21555_MI,
	&m5907_MI,
	&m21556_MI,
	&m21557_MI,
	&m21558_MI,
	&m21559_MI,
	&m21560_MI,
	&m5908_MI,
	&m21554_MI,
	&m21562_MI,
	&m21563_MI,
	&m5905_MI,
	&m5906_MI,
	&m21679_MI,
	&m5907_MI,
	&m21680_MI,
	&m21681_MI,
	&m21682_MI,
	&m21683_MI,
	&m21684_MI,
	&m5908_MI,
	&m21678_MI,
	&m21686_MI,
	&m21687_MI,
	&m5905_MI,
	&m5906_MI,
	&m19777_MI,
	&m5907_MI,
	&m19778_MI,
	&m19779_MI,
	&m19780_MI,
	&m19781_MI,
	&m19782_MI,
	&m5908_MI,
	&m19776_MI,
	&m19784_MI,
	&m19785_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5484_TI;
extern TypeInfo t5485_TI;
extern TypeInfo t5486_TI;
extern TypeInfo t5487_TI;
extern TypeInfo t5488_TI;
extern TypeInfo t5489_TI;
static TypeInfo* t3881_ITIs[] = 
{
	&t5484_TI,
	&t5485_TI,
	&t5486_TI,
	&t5487_TI,
	&t5488_TI,
	&t5489_TI,
	&t5469_TI,
	&t5470_TI,
	&t5471_TI,
	&t5304_TI,
	&t5305_TI,
	&t5306_TI,
	&t5439_TI,
	&t5440_TI,
	&t5441_TI,
	&t5472_TI,
	&t5473_TI,
	&t5474_TI,
	&t5122_TI,
	&t5123_TI,
	&t5124_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3881_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5484_TI, 21},
	{ &t5485_TI, 28},
	{ &t5486_TI, 33},
	{ &t5487_TI, 34},
	{ &t5488_TI, 41},
	{ &t5489_TI, 46},
	{ &t5469_TI, 47},
	{ &t5470_TI, 54},
	{ &t5471_TI, 59},
	{ &t5304_TI, 60},
	{ &t5305_TI, 67},
	{ &t5306_TI, 72},
	{ &t5439_TI, 73},
	{ &t5440_TI, 80},
	{ &t5441_TI, 85},
	{ &t5472_TI, 86},
	{ &t5473_TI, 93},
	{ &t5474_TI, 98},
	{ &t5122_TI, 99},
	{ &t5123_TI, 106},
	{ &t5124_TI, 111},
	{ &t5059_TI, 112},
	{ &t5060_TI, 119},
	{ &t5061_TI, 124},
	{ &t5062_TI, 125},
	{ &t5063_TI, 132},
	{ &t5064_TI, 137},
	{ &t2240_TI, 138},
	{ &t2245_TI, 145},
	{ &t2241_TI, 150},
	{ &t5065_TI, 151},
	{ &t5066_TI, 158},
	{ &t5067_TI, 163},
	{ &t2182_TI, 164},
	{ &t2186_TI, 171},
	{ &t2183_TI, 176},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3881_0_0_0;
extern Il2CppType t3881_1_0_0;
struct t260;
extern TypeInfo t260_TI;
extern CustomAttributesCache t260__CustomAttributeCache;
TypeInfo t3881_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "HorizontalLayoutGroup[]", "UnityEngine.UI", t3881_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t260_TI, t3881_ITIs, t3881_VT, &EmptyCustomAttributesCache, &t3881_TI, &t3881_0_0_0, &t3881_1_0_0, t3881_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t260 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 177, 36, 40};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3882_TI;



// Metadata Definition UnityEngine.UI.HorizontalOrVerticalLayoutGroup[]
static MethodInfo* t3882_MIs[] =
{
	NULL
};
static MethodInfo* t3882_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21761_MI,
	&m5907_MI,
	&m21762_MI,
	&m21763_MI,
	&m21764_MI,
	&m21765_MI,
	&m21766_MI,
	&m5908_MI,
	&m21760_MI,
	&m21768_MI,
	&m21769_MI,
	&m5905_MI,
	&m5906_MI,
	&m21668_MI,
	&m5907_MI,
	&m21669_MI,
	&m21670_MI,
	&m21671_MI,
	&m21672_MI,
	&m21673_MI,
	&m5908_MI,
	&m21667_MI,
	&m21675_MI,
	&m21676_MI,
	&m5905_MI,
	&m5906_MI,
	&m20809_MI,
	&m5907_MI,
	&m20810_MI,
	&m20811_MI,
	&m20812_MI,
	&m20813_MI,
	&m20814_MI,
	&m5908_MI,
	&m20808_MI,
	&m20816_MI,
	&m20817_MI,
	&m5905_MI,
	&m5906_MI,
	&m21555_MI,
	&m5907_MI,
	&m21556_MI,
	&m21557_MI,
	&m21558_MI,
	&m21559_MI,
	&m21560_MI,
	&m5908_MI,
	&m21554_MI,
	&m21562_MI,
	&m21563_MI,
	&m5905_MI,
	&m5906_MI,
	&m21679_MI,
	&m5907_MI,
	&m21680_MI,
	&m21681_MI,
	&m21682_MI,
	&m21683_MI,
	&m21684_MI,
	&m5908_MI,
	&m21678_MI,
	&m21686_MI,
	&m21687_MI,
	&m5905_MI,
	&m5906_MI,
	&m19777_MI,
	&m5907_MI,
	&m19778_MI,
	&m19779_MI,
	&m19780_MI,
	&m19781_MI,
	&m19782_MI,
	&m5908_MI,
	&m19776_MI,
	&m19784_MI,
	&m19785_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
static TypeInfo* t3882_ITIs[] = 
{
	&t5487_TI,
	&t5488_TI,
	&t5489_TI,
	&t5469_TI,
	&t5470_TI,
	&t5471_TI,
	&t5304_TI,
	&t5305_TI,
	&t5306_TI,
	&t5439_TI,
	&t5440_TI,
	&t5441_TI,
	&t5472_TI,
	&t5473_TI,
	&t5474_TI,
	&t5122_TI,
	&t5123_TI,
	&t5124_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3882_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5487_TI, 21},
	{ &t5488_TI, 28},
	{ &t5489_TI, 33},
	{ &t5469_TI, 34},
	{ &t5470_TI, 41},
	{ &t5471_TI, 46},
	{ &t5304_TI, 47},
	{ &t5305_TI, 54},
	{ &t5306_TI, 59},
	{ &t5439_TI, 60},
	{ &t5440_TI, 67},
	{ &t5441_TI, 72},
	{ &t5472_TI, 73},
	{ &t5473_TI, 80},
	{ &t5474_TI, 85},
	{ &t5122_TI, 86},
	{ &t5123_TI, 93},
	{ &t5124_TI, 98},
	{ &t5059_TI, 99},
	{ &t5060_TI, 106},
	{ &t5061_TI, 111},
	{ &t5062_TI, 112},
	{ &t5063_TI, 119},
	{ &t5064_TI, 124},
	{ &t2240_TI, 125},
	{ &t2245_TI, 132},
	{ &t2241_TI, 137},
	{ &t5065_TI, 138},
	{ &t5066_TI, 145},
	{ &t5067_TI, 150},
	{ &t2182_TI, 151},
	{ &t2186_TI, 158},
	{ &t2183_TI, 163},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3882_0_0_0;
extern Il2CppType t3882_1_0_0;
struct t261;
extern TypeInfo t261_TI;
extern CustomAttributesCache t261__CustomAttributeCache_m_Spacing;
extern CustomAttributesCache t261__CustomAttributeCache_m_ChildForceExpandWidth;
extern CustomAttributesCache t261__CustomAttributeCache_m_ChildForceExpandHeight;
TypeInfo t3882_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "HorizontalOrVerticalLayoutGroup[]", "UnityEngine.UI", t3882_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t261_TI, t3882_ITIs, t3882_VT, &EmptyCustomAttributesCache, &t3882_TI, &t3882_0_0_0, &t3882_1_0_0, t3882_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t261 *), -1, 0, 0, -1, 1048705, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 164, 33, 37};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3883_TI;



// Metadata Definition UnityEngine.UI.LayoutElement[]
static MethodInfo* t3883_MIs[] =
{
	NULL
};
extern MethodInfo m21774_MI;
extern MethodInfo m21775_MI;
extern MethodInfo m21776_MI;
extern MethodInfo m21777_MI;
extern MethodInfo m21778_MI;
extern MethodInfo m21779_MI;
extern MethodInfo m21773_MI;
extern MethodInfo m21781_MI;
extern MethodInfo m21782_MI;
extern MethodInfo m21785_MI;
extern MethodInfo m21786_MI;
extern MethodInfo m21787_MI;
extern MethodInfo m21788_MI;
extern MethodInfo m21789_MI;
extern MethodInfo m21790_MI;
extern MethodInfo m21784_MI;
extern MethodInfo m21792_MI;
extern MethodInfo m21793_MI;
static MethodInfo* t3883_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21774_MI,
	&m5907_MI,
	&m21775_MI,
	&m21776_MI,
	&m21777_MI,
	&m21778_MI,
	&m21779_MI,
	&m5908_MI,
	&m21773_MI,
	&m21781_MI,
	&m21782_MI,
	&m5905_MI,
	&m5906_MI,
	&m20809_MI,
	&m5907_MI,
	&m20810_MI,
	&m20811_MI,
	&m20812_MI,
	&m20813_MI,
	&m20814_MI,
	&m5908_MI,
	&m20808_MI,
	&m20816_MI,
	&m20817_MI,
	&m5905_MI,
	&m5906_MI,
	&m21785_MI,
	&m5907_MI,
	&m21786_MI,
	&m21787_MI,
	&m21788_MI,
	&m21789_MI,
	&m21790_MI,
	&m5908_MI,
	&m21784_MI,
	&m21792_MI,
	&m21793_MI,
	&m5905_MI,
	&m5906_MI,
	&m19777_MI,
	&m5907_MI,
	&m19778_MI,
	&m19779_MI,
	&m19780_MI,
	&m19781_MI,
	&m19782_MI,
	&m5908_MI,
	&m19776_MI,
	&m19784_MI,
	&m19785_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5490_TI;
extern TypeInfo t5491_TI;
extern TypeInfo t5492_TI;
extern TypeInfo t5493_TI;
extern TypeInfo t5494_TI;
extern TypeInfo t5495_TI;
static TypeInfo* t3883_ITIs[] = 
{
	&t5490_TI,
	&t5491_TI,
	&t5492_TI,
	&t5304_TI,
	&t5305_TI,
	&t5306_TI,
	&t5493_TI,
	&t5494_TI,
	&t5495_TI,
	&t5122_TI,
	&t5123_TI,
	&t5124_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3883_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5490_TI, 21},
	{ &t5491_TI, 28},
	{ &t5492_TI, 33},
	{ &t5304_TI, 34},
	{ &t5305_TI, 41},
	{ &t5306_TI, 46},
	{ &t5493_TI, 47},
	{ &t5494_TI, 54},
	{ &t5495_TI, 59},
	{ &t5122_TI, 60},
	{ &t5123_TI, 67},
	{ &t5124_TI, 72},
	{ &t5059_TI, 73},
	{ &t5060_TI, 80},
	{ &t5061_TI, 85},
	{ &t5062_TI, 86},
	{ &t5063_TI, 93},
	{ &t5064_TI, 98},
	{ &t2240_TI, 99},
	{ &t2245_TI, 106},
	{ &t2241_TI, 111},
	{ &t5065_TI, 112},
	{ &t5066_TI, 119},
	{ &t5067_TI, 124},
	{ &t2182_TI, 125},
	{ &t2186_TI, 132},
	{ &t2183_TI, 137},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3883_0_0_0;
extern Il2CppType t3883_1_0_0;
struct t262;
extern TypeInfo t262_TI;
extern CustomAttributesCache t262__CustomAttributeCache;
extern CustomAttributesCache t262__CustomAttributeCache_m_IgnoreLayout;
extern CustomAttributesCache t262__CustomAttributeCache_m_MinWidth;
extern CustomAttributesCache t262__CustomAttributeCache_m_MinHeight;
extern CustomAttributesCache t262__CustomAttributeCache_m_PreferredWidth;
extern CustomAttributesCache t262__CustomAttributeCache_m_PreferredHeight;
extern CustomAttributesCache t262__CustomAttributeCache_m_FlexibleWidth;
extern CustomAttributesCache t262__CustomAttributeCache_m_FlexibleHeight;
TypeInfo t3883_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "LayoutElement[]", "UnityEngine.UI", t3883_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t262_TI, t3883_ITIs, t3883_VT, &EmptyCustomAttributesCache, &t3883_TI, &t3883_0_0_0, &t3883_1_0_0, t3883_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t262 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 138, 27, 31};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3884_TI;



// Metadata Definition UnityEngine.UI.ILayoutIgnorer[]
static MethodInfo* t3884_MIs[] =
{
	NULL
};
static MethodInfo* t3884_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21785_MI,
	&m5907_MI,
	&m21786_MI,
	&m21787_MI,
	&m21788_MI,
	&m21789_MI,
	&m21790_MI,
	&m5908_MI,
	&m21784_MI,
	&m21792_MI,
	&m21793_MI,
};
static TypeInfo* t3884_ITIs[] = 
{
	&t5493_TI,
	&t5494_TI,
	&t5495_TI,
};
static Il2CppInterfaceOffsetPair t3884_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5493_TI, 21},
	{ &t5494_TI, 28},
	{ &t5495_TI, 33},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3884_0_0_0;
extern Il2CppType t3884_1_0_0;
struct t412;
extern TypeInfo t412_TI;
TypeInfo t3884_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "ILayoutIgnorer[]", "UnityEngine.UI", t3884_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t412_TI, t3884_ITIs, t3884_VT, &EmptyCustomAttributesCache, &t3884_TI, &t3884_0_0_0, &t3884_1_0_0, t3884_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3885_TI;



// Metadata Definition UnityEngine.UI.VerticalLayoutGroup[]
static MethodInfo* t3885_MIs[] =
{
	NULL
};
extern MethodInfo m21798_MI;
extern MethodInfo m21799_MI;
extern MethodInfo m21800_MI;
extern MethodInfo m21801_MI;
extern MethodInfo m21802_MI;
extern MethodInfo m21803_MI;
extern MethodInfo m21797_MI;
extern MethodInfo m21805_MI;
extern MethodInfo m21806_MI;
static MethodInfo* t3885_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21798_MI,
	&m5907_MI,
	&m21799_MI,
	&m21800_MI,
	&m21801_MI,
	&m21802_MI,
	&m21803_MI,
	&m5908_MI,
	&m21797_MI,
	&m21805_MI,
	&m21806_MI,
	&m5905_MI,
	&m5906_MI,
	&m21761_MI,
	&m5907_MI,
	&m21762_MI,
	&m21763_MI,
	&m21764_MI,
	&m21765_MI,
	&m21766_MI,
	&m5908_MI,
	&m21760_MI,
	&m21768_MI,
	&m21769_MI,
	&m5905_MI,
	&m5906_MI,
	&m21668_MI,
	&m5907_MI,
	&m21669_MI,
	&m21670_MI,
	&m21671_MI,
	&m21672_MI,
	&m21673_MI,
	&m5908_MI,
	&m21667_MI,
	&m21675_MI,
	&m21676_MI,
	&m5905_MI,
	&m5906_MI,
	&m20809_MI,
	&m5907_MI,
	&m20810_MI,
	&m20811_MI,
	&m20812_MI,
	&m20813_MI,
	&m20814_MI,
	&m5908_MI,
	&m20808_MI,
	&m20816_MI,
	&m20817_MI,
	&m5905_MI,
	&m5906_MI,
	&m21555_MI,
	&m5907_MI,
	&m21556_MI,
	&m21557_MI,
	&m21558_MI,
	&m21559_MI,
	&m21560_MI,
	&m5908_MI,
	&m21554_MI,
	&m21562_MI,
	&m21563_MI,
	&m5905_MI,
	&m5906_MI,
	&m21679_MI,
	&m5907_MI,
	&m21680_MI,
	&m21681_MI,
	&m21682_MI,
	&m21683_MI,
	&m21684_MI,
	&m5908_MI,
	&m21678_MI,
	&m21686_MI,
	&m21687_MI,
	&m5905_MI,
	&m5906_MI,
	&m19777_MI,
	&m5907_MI,
	&m19778_MI,
	&m19779_MI,
	&m19780_MI,
	&m19781_MI,
	&m19782_MI,
	&m5908_MI,
	&m19776_MI,
	&m19784_MI,
	&m19785_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5496_TI;
extern TypeInfo t5497_TI;
extern TypeInfo t5498_TI;
static TypeInfo* t3885_ITIs[] = 
{
	&t5496_TI,
	&t5497_TI,
	&t5498_TI,
	&t5487_TI,
	&t5488_TI,
	&t5489_TI,
	&t5469_TI,
	&t5470_TI,
	&t5471_TI,
	&t5304_TI,
	&t5305_TI,
	&t5306_TI,
	&t5439_TI,
	&t5440_TI,
	&t5441_TI,
	&t5472_TI,
	&t5473_TI,
	&t5474_TI,
	&t5122_TI,
	&t5123_TI,
	&t5124_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3885_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5496_TI, 21},
	{ &t5497_TI, 28},
	{ &t5498_TI, 33},
	{ &t5487_TI, 34},
	{ &t5488_TI, 41},
	{ &t5489_TI, 46},
	{ &t5469_TI, 47},
	{ &t5470_TI, 54},
	{ &t5471_TI, 59},
	{ &t5304_TI, 60},
	{ &t5305_TI, 67},
	{ &t5306_TI, 72},
	{ &t5439_TI, 73},
	{ &t5440_TI, 80},
	{ &t5441_TI, 85},
	{ &t5472_TI, 86},
	{ &t5473_TI, 93},
	{ &t5474_TI, 98},
	{ &t5122_TI, 99},
	{ &t5123_TI, 106},
	{ &t5124_TI, 111},
	{ &t5059_TI, 112},
	{ &t5060_TI, 119},
	{ &t5061_TI, 124},
	{ &t5062_TI, 125},
	{ &t5063_TI, 132},
	{ &t5064_TI, 137},
	{ &t2240_TI, 138},
	{ &t2245_TI, 145},
	{ &t2241_TI, 150},
	{ &t5065_TI, 151},
	{ &t5066_TI, 158},
	{ &t5067_TI, 163},
	{ &t2182_TI, 164},
	{ &t2186_TI, 171},
	{ &t2183_TI, 176},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3885_0_0_0;
extern Il2CppType t3885_1_0_0;
struct t272;
extern TypeInfo t272_TI;
extern CustomAttributesCache t272__CustomAttributeCache;
TypeInfo t3885_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "VerticalLayoutGroup[]", "UnityEngine.UI", t3885_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t272_TI, t3885_ITIs, t3885_VT, &EmptyCustomAttributesCache, &t3885_TI, &t3885_0_0_0, &t3885_1_0_0, t3885_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t272 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 177, 36, 40};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3886_TI;



// Metadata Definition UnityEngine.UI.Mask[]
static MethodInfo* t3886_MIs[] =
{
	NULL
};
extern MethodInfo m21810_MI;
extern MethodInfo m21811_MI;
extern MethodInfo m21812_MI;
extern MethodInfo m21813_MI;
extern MethodInfo m21814_MI;
extern MethodInfo m21815_MI;
extern MethodInfo m21809_MI;
extern MethodInfo m21817_MI;
extern MethodInfo m21818_MI;
extern MethodInfo m21821_MI;
extern MethodInfo m21822_MI;
extern MethodInfo m21823_MI;
extern MethodInfo m21824_MI;
extern MethodInfo m21825_MI;
extern MethodInfo m21826_MI;
extern MethodInfo m21820_MI;
extern MethodInfo m21828_MI;
extern MethodInfo m21829_MI;
extern MethodInfo m21832_MI;
extern MethodInfo m21833_MI;
extern MethodInfo m21834_MI;
extern MethodInfo m21835_MI;
extern MethodInfo m21836_MI;
extern MethodInfo m21837_MI;
extern MethodInfo m21831_MI;
extern MethodInfo m21839_MI;
extern MethodInfo m21840_MI;
extern MethodInfo m21843_MI;
extern MethodInfo m21844_MI;
extern MethodInfo m21845_MI;
extern MethodInfo m21846_MI;
extern MethodInfo m21847_MI;
extern MethodInfo m21848_MI;
extern MethodInfo m21842_MI;
extern MethodInfo m21850_MI;
extern MethodInfo m21851_MI;
static MethodInfo* t3886_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21810_MI,
	&m5907_MI,
	&m21811_MI,
	&m21812_MI,
	&m21813_MI,
	&m21814_MI,
	&m21815_MI,
	&m5908_MI,
	&m21809_MI,
	&m21817_MI,
	&m21818_MI,
	&m5905_MI,
	&m5906_MI,
	&m21821_MI,
	&m5907_MI,
	&m21822_MI,
	&m21823_MI,
	&m21824_MI,
	&m21825_MI,
	&m21826_MI,
	&m5908_MI,
	&m21820_MI,
	&m21828_MI,
	&m21829_MI,
	&m5905_MI,
	&m5906_MI,
	&m21832_MI,
	&m5907_MI,
	&m21833_MI,
	&m21834_MI,
	&m21835_MI,
	&m21836_MI,
	&m21837_MI,
	&m5908_MI,
	&m21831_MI,
	&m21839_MI,
	&m21840_MI,
	&m5905_MI,
	&m5906_MI,
	&m21071_MI,
	&m5907_MI,
	&m21072_MI,
	&m21073_MI,
	&m21074_MI,
	&m21075_MI,
	&m21076_MI,
	&m5908_MI,
	&m21070_MI,
	&m21078_MI,
	&m21079_MI,
	&m5905_MI,
	&m5906_MI,
	&m21843_MI,
	&m5907_MI,
	&m21844_MI,
	&m21845_MI,
	&m21846_MI,
	&m21847_MI,
	&m21848_MI,
	&m5908_MI,
	&m21842_MI,
	&m21850_MI,
	&m21851_MI,
	&m5905_MI,
	&m5906_MI,
	&m19777_MI,
	&m5907_MI,
	&m19778_MI,
	&m19779_MI,
	&m19780_MI,
	&m19781_MI,
	&m19782_MI,
	&m5908_MI,
	&m19776_MI,
	&m19784_MI,
	&m19785_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5499_TI;
extern TypeInfo t5500_TI;
extern TypeInfo t5501_TI;
extern TypeInfo t5502_TI;
extern TypeInfo t5503_TI;
extern TypeInfo t5504_TI;
extern TypeInfo t5505_TI;
extern TypeInfo t5506_TI;
extern TypeInfo t5507_TI;
extern TypeInfo t5508_TI;
extern TypeInfo t5509_TI;
extern TypeInfo t5510_TI;
static TypeInfo* t3886_ITIs[] = 
{
	&t5499_TI,
	&t5500_TI,
	&t5501_TI,
	&t5502_TI,
	&t5503_TI,
	&t5504_TI,
	&t5505_TI,
	&t5506_TI,
	&t5507_TI,
	&t5343_TI,
	&t5344_TI,
	&t5345_TI,
	&t5508_TI,
	&t5509_TI,
	&t5510_TI,
	&t5122_TI,
	&t5123_TI,
	&t5124_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3886_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5499_TI, 21},
	{ &t5500_TI, 28},
	{ &t5501_TI, 33},
	{ &t5502_TI, 34},
	{ &t5503_TI, 41},
	{ &t5504_TI, 46},
	{ &t5505_TI, 47},
	{ &t5506_TI, 54},
	{ &t5507_TI, 59},
	{ &t5343_TI, 60},
	{ &t5344_TI, 67},
	{ &t5345_TI, 72},
	{ &t5508_TI, 73},
	{ &t5509_TI, 80},
	{ &t5510_TI, 85},
	{ &t5122_TI, 86},
	{ &t5123_TI, 93},
	{ &t5124_TI, 98},
	{ &t5059_TI, 99},
	{ &t5060_TI, 106},
	{ &t5061_TI, 111},
	{ &t5062_TI, 112},
	{ &t5063_TI, 119},
	{ &t5064_TI, 124},
	{ &t2240_TI, 125},
	{ &t2245_TI, 132},
	{ &t2241_TI, 137},
	{ &t5065_TI, 138},
	{ &t5066_TI, 145},
	{ &t5067_TI, 150},
	{ &t2182_TI, 151},
	{ &t2186_TI, 158},
	{ &t2183_TI, 163},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3886_0_0_0;
extern Il2CppType t3886_1_0_0;
struct t273;
extern TypeInfo t273_TI;
extern CustomAttributesCache t273__CustomAttributeCache;
extern CustomAttributesCache t273__CustomAttributeCache_m_ShowMaskGraphic;
TypeInfo t3886_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Mask[]", "UnityEngine.UI", t3886_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t273_TI, t3886_ITIs, t3886_VT, &EmptyCustomAttributesCache, &t3886_TI, &t3886_0_0_0, &t3886_1_0_0, t3886_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t273 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 164, 33, 37};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3887_TI;



// Metadata Definition UnityEngine.UI.IGraphicEnabledDisabled[]
static MethodInfo* t3887_MIs[] =
{
	NULL
};
static MethodInfo* t3887_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21821_MI,
	&m5907_MI,
	&m21822_MI,
	&m21823_MI,
	&m21824_MI,
	&m21825_MI,
	&m21826_MI,
	&m5908_MI,
	&m21820_MI,
	&m21828_MI,
	&m21829_MI,
};
static TypeInfo* t3887_ITIs[] = 
{
	&t5502_TI,
	&t5503_TI,
	&t5504_TI,
};
static Il2CppInterfaceOffsetPair t3887_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5502_TI, 21},
	{ &t5503_TI, 28},
	{ &t5504_TI, 33},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3887_0_0_0;
extern Il2CppType t3887_1_0_0;
struct t355;
extern TypeInfo t355_TI;
TypeInfo t3887_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "IGraphicEnabledDisabled[]", "UnityEngine.UI", t3887_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t355_TI, t3887_ITIs, t3887_VT, &EmptyCustomAttributesCache, &t3887_TI, &t3887_0_0_0, &t3887_1_0_0, t3887_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 160, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3888_TI;



// Metadata Definition UnityEngine.UI.IMask[]
static MethodInfo* t3888_MIs[] =
{
	NULL
};
static MethodInfo* t3888_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21832_MI,
	&m5907_MI,
	&m21833_MI,
	&m21834_MI,
	&m21835_MI,
	&m21836_MI,
	&m21837_MI,
	&m5908_MI,
	&m21831_MI,
	&m21839_MI,
	&m21840_MI,
};
static TypeInfo* t3888_ITIs[] = 
{
	&t5505_TI,
	&t5506_TI,
	&t5507_TI,
};
static Il2CppInterfaceOffsetPair t3888_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5505_TI, 21},
	{ &t5506_TI, 28},
	{ &t5507_TI, 33},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3888_0_0_0;
extern Il2CppType t3888_1_0_0;
struct t370;
extern TypeInfo t370_TI;
TypeInfo t3888_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "IMask[]", "UnityEngine.UI", t3888_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t370_TI, t3888_ITIs, t3888_VT, &EmptyCustomAttributesCache, &t3888_TI, &t3888_0_0_0, &t3888_1_0_0, t3888_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3889_TI;



// Metadata Definition UnityEngine.UI.IMaterialModifier[]
static MethodInfo* t3889_MIs[] =
{
	NULL
};
static MethodInfo* t3889_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21843_MI,
	&m5907_MI,
	&m21844_MI,
	&m21845_MI,
	&m21846_MI,
	&m21847_MI,
	&m21848_MI,
	&m5908_MI,
	&m21842_MI,
	&m21850_MI,
	&m21851_MI,
};
static TypeInfo* t3889_ITIs[] = 
{
	&t5508_TI,
	&t5509_TI,
	&t5510_TI,
};
static Il2CppInterfaceOffsetPair t3889_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5508_TI, 21},
	{ &t5509_TI, 28},
	{ &t5510_TI, 33},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3889_0_0_0;
extern Il2CppType t3889_1_0_0;
struct t354;
extern TypeInfo t354_TI;
TypeInfo t3889_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "IMaterialModifier[]", "UnityEngine.UI", t3889_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t354_TI, t3889_ITIs, t3889_VT, &EmptyCustomAttributesCache, &t3889_TI, &t3889_0_0_0, &t3889_1_0_0, t3889_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2789_TI;



// Metadata Definition System.Collections.Generic.List`1<UnityEngine.Canvas>[]
static MethodInfo* t2789_MIs[] =
{
	NULL
};
extern MethodInfo m21859_MI;
extern MethodInfo m21860_MI;
extern MethodInfo m21861_MI;
extern MethodInfo m21862_MI;
extern MethodInfo m21863_MI;
extern MethodInfo m21864_MI;
extern MethodInfo m21858_MI;
extern MethodInfo m21866_MI;
extern MethodInfo m21867_MI;
static MethodInfo* t2789_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21859_MI,
	&m5907_MI,
	&m21860_MI,
	&m21861_MI,
	&m21862_MI,
	&m21863_MI,
	&m21864_MI,
	&m5908_MI,
	&m21858_MI,
	&m21866_MI,
	&m21867_MI,
	&m5905_MI,
	&m5906_MI,
	&m20022_MI,
	&m5907_MI,
	&m20023_MI,
	&m20024_MI,
	&m20025_MI,
	&m20026_MI,
	&m20027_MI,
	&m5908_MI,
	&m20021_MI,
	&m20029_MI,
	&m20030_MI,
	&m5905_MI,
	&m5906_MI,
	&m20033_MI,
	&m5907_MI,
	&m20034_MI,
	&m20035_MI,
	&m20036_MI,
	&m20037_MI,
	&m20038_MI,
	&m5908_MI,
	&m20032_MI,
	&m20040_MI,
	&m20041_MI,
	&m5905_MI,
	&m5906_MI,
	&m20044_MI,
	&m5907_MI,
	&m20045_MI,
	&m20046_MI,
	&m20047_MI,
	&m20048_MI,
	&m20049_MI,
	&m5908_MI,
	&m20043_MI,
	&m20051_MI,
	&m20052_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5511_TI;
extern TypeInfo t5512_TI;
extern TypeInfo t5513_TI;
static TypeInfo* t2789_ITIs[] = 
{
	&t5511_TI,
	&t5512_TI,
	&t5513_TI,
	&t5169_TI,
	&t5170_TI,
	&t5171_TI,
	&t5172_TI,
	&t5173_TI,
	&t5174_TI,
	&t5175_TI,
	&t5176_TI,
	&t5177_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2789_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5511_TI, 21},
	{ &t5512_TI, 28},
	{ &t5513_TI, 33},
	{ &t5169_TI, 34},
	{ &t5170_TI, 41},
	{ &t5171_TI, 46},
	{ &t5172_TI, 47},
	{ &t5173_TI, 54},
	{ &t5174_TI, 59},
	{ &t5175_TI, 60},
	{ &t5176_TI, 67},
	{ &t5177_TI, 72},
	{ &t2182_TI, 73},
	{ &t2186_TI, 80},
	{ &t2183_TI, 85},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2789_0_0_0;
extern Il2CppType t2789_1_0_0;
struct t278;
extern TypeInfo t278_TI;
extern CustomAttributesCache t1261__CustomAttributeCache;
TypeInfo t2789_TI = 
{
	&g_mscorlib_dll_Image, NULL, "List`1[]", "System.Collections.Generic", t2789_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t278_TI, t2789_ITIs, t2789_VT, &EmptyCustomAttributesCache, &t2789_TI, &t2789_0_0_0, &t2789_1_0_0, t2789_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t278 *), -1, sizeof(t2789_SFs), 0, -1, 1056769, 1, false, false, false, false, false, true, false, false, false, true, false, false, 0, 0, 0, 0, 0, 86, 15, 19};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2794_TI;



// Metadata Definition System.Collections.Generic.List`1<UnityEngine.Component>[]
static MethodInfo* t2794_MIs[] =
{
	NULL
};
extern MethodInfo m21873_MI;
extern MethodInfo m21874_MI;
extern MethodInfo m21875_MI;
extern MethodInfo m21876_MI;
extern MethodInfo m21877_MI;
extern MethodInfo m21878_MI;
extern MethodInfo m21872_MI;
extern MethodInfo m21880_MI;
extern MethodInfo m21881_MI;
static MethodInfo* t2794_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21873_MI,
	&m5907_MI,
	&m21874_MI,
	&m21875_MI,
	&m21876_MI,
	&m21877_MI,
	&m21878_MI,
	&m5908_MI,
	&m21872_MI,
	&m21880_MI,
	&m21881_MI,
	&m5905_MI,
	&m5906_MI,
	&m20022_MI,
	&m5907_MI,
	&m20023_MI,
	&m20024_MI,
	&m20025_MI,
	&m20026_MI,
	&m20027_MI,
	&m5908_MI,
	&m20021_MI,
	&m20029_MI,
	&m20030_MI,
	&m5905_MI,
	&m5906_MI,
	&m20033_MI,
	&m5907_MI,
	&m20034_MI,
	&m20035_MI,
	&m20036_MI,
	&m20037_MI,
	&m20038_MI,
	&m5908_MI,
	&m20032_MI,
	&m20040_MI,
	&m20041_MI,
	&m5905_MI,
	&m5906_MI,
	&m20044_MI,
	&m5907_MI,
	&m20045_MI,
	&m20046_MI,
	&m20047_MI,
	&m20048_MI,
	&m20049_MI,
	&m5908_MI,
	&m20043_MI,
	&m20051_MI,
	&m20052_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5514_TI;
extern TypeInfo t5515_TI;
extern TypeInfo t5516_TI;
static TypeInfo* t2794_ITIs[] = 
{
	&t5514_TI,
	&t5515_TI,
	&t5516_TI,
	&t5169_TI,
	&t5170_TI,
	&t5171_TI,
	&t5172_TI,
	&t5173_TI,
	&t5174_TI,
	&t5175_TI,
	&t5176_TI,
	&t5177_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2794_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5514_TI, 21},
	{ &t5515_TI, 28},
	{ &t5516_TI, 33},
	{ &t5169_TI, 34},
	{ &t5170_TI, 41},
	{ &t5171_TI, 46},
	{ &t5172_TI, 47},
	{ &t5173_TI, 54},
	{ &t5174_TI, 59},
	{ &t5175_TI, 60},
	{ &t5176_TI, 67},
	{ &t5177_TI, 72},
	{ &t2182_TI, 73},
	{ &t2186_TI, 80},
	{ &t2183_TI, 85},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2794_0_0_0;
extern Il2CppType t2794_1_0_0;
struct t268;
extern TypeInfo t268_TI;
extern CustomAttributesCache t1261__CustomAttributeCache;
TypeInfo t2794_TI = 
{
	&g_mscorlib_dll_Image, NULL, "List`1[]", "System.Collections.Generic", t2794_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t268_TI, t2794_ITIs, t2794_VT, &EmptyCustomAttributesCache, &t2794_TI, &t2794_0_0_0, &t2794_1_0_0, t2794_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t268 *), -1, sizeof(t2794_SFs), 0, -1, 1056769, 1, false, false, false, false, false, true, false, false, false, true, false, false, 0, 0, 0, 0, 0, 86, 15, 19};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3890_TI;



// Metadata Definition UnityEngine.UI.BaseVertexEffect[]
static MethodInfo* t3890_MIs[] =
{
	NULL
};
extern MethodInfo m21887_MI;
extern MethodInfo m21888_MI;
extern MethodInfo m21889_MI;
extern MethodInfo m21890_MI;
extern MethodInfo m21891_MI;
extern MethodInfo m21892_MI;
extern MethodInfo m21886_MI;
extern MethodInfo m21894_MI;
extern MethodInfo m21895_MI;
extern MethodInfo m21898_MI;
extern MethodInfo m21899_MI;
extern MethodInfo m21900_MI;
extern MethodInfo m21901_MI;
extern MethodInfo m21902_MI;
extern MethodInfo m21903_MI;
extern MethodInfo m21897_MI;
extern MethodInfo m21905_MI;
extern MethodInfo m21906_MI;
static MethodInfo* t3890_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21887_MI,
	&m5907_MI,
	&m21888_MI,
	&m21889_MI,
	&m21890_MI,
	&m21891_MI,
	&m21892_MI,
	&m5908_MI,
	&m21886_MI,
	&m21894_MI,
	&m21895_MI,
	&m5905_MI,
	&m5906_MI,
	&m21898_MI,
	&m5907_MI,
	&m21899_MI,
	&m21900_MI,
	&m21901_MI,
	&m21902_MI,
	&m21903_MI,
	&m5908_MI,
	&m21897_MI,
	&m21905_MI,
	&m21906_MI,
	&m5905_MI,
	&m5906_MI,
	&m19777_MI,
	&m5907_MI,
	&m19778_MI,
	&m19779_MI,
	&m19780_MI,
	&m19781_MI,
	&m19782_MI,
	&m5908_MI,
	&m19776_MI,
	&m19784_MI,
	&m19785_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5517_TI;
extern TypeInfo t5518_TI;
extern TypeInfo t5519_TI;
extern TypeInfo t5520_TI;
extern TypeInfo t5521_TI;
extern TypeInfo t5522_TI;
static TypeInfo* t3890_ITIs[] = 
{
	&t5517_TI,
	&t5518_TI,
	&t5519_TI,
	&t5520_TI,
	&t5521_TI,
	&t5522_TI,
	&t5122_TI,
	&t5123_TI,
	&t5124_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3890_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5517_TI, 21},
	{ &t5518_TI, 28},
	{ &t5519_TI, 33},
	{ &t5520_TI, 34},
	{ &t5521_TI, 41},
	{ &t5522_TI, 46},
	{ &t5122_TI, 47},
	{ &t5123_TI, 54},
	{ &t5124_TI, 59},
	{ &t5059_TI, 60},
	{ &t5060_TI, 67},
	{ &t5061_TI, 72},
	{ &t5062_TI, 73},
	{ &t5063_TI, 80},
	{ &t5064_TI, 85},
	{ &t2240_TI, 86},
	{ &t2245_TI, 93},
	{ &t2241_TI, 98},
	{ &t5065_TI, 99},
	{ &t5066_TI, 106},
	{ &t5067_TI, 111},
	{ &t2182_TI, 112},
	{ &t2186_TI, 119},
	{ &t2183_TI, 124},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3890_0_0_0;
extern Il2CppType t3890_1_0_0;
struct t283;
extern TypeInfo t283_TI;
extern CustomAttributesCache t283__CustomAttributeCache;
TypeInfo t3890_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "BaseVertexEffect[]", "UnityEngine.UI", t3890_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t283_TI, t3890_ITIs, t3890_VT, &EmptyCustomAttributesCache, &t3890_TI, &t3890_0_0_0, &t3890_1_0_0, t3890_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t283 *), -1, 0, 0, -1, 1048705, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 125, 24, 28};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3891_TI;



// Metadata Definition UnityEngine.UI.IVertexModifier[]
static MethodInfo* t3891_MIs[] =
{
	NULL
};
static MethodInfo* t3891_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21898_MI,
	&m5907_MI,
	&m21899_MI,
	&m21900_MI,
	&m21901_MI,
	&m21902_MI,
	&m21903_MI,
	&m5908_MI,
	&m21897_MI,
	&m21905_MI,
	&m21906_MI,
};
static TypeInfo* t3891_ITIs[] = 
{
	&t5520_TI,
	&t5521_TI,
	&t5522_TI,
};
static Il2CppInterfaceOffsetPair t3891_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5520_TI, 21},
	{ &t5521_TI, 28},
	{ &t5522_TI, 33},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3891_0_0_0;
extern Il2CppType t3891_1_0_0;
struct t356;
extern TypeInfo t356_TI;
TypeInfo t3891_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "IVertexModifier[]", "UnityEngine.UI", t3891_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t356_TI, t3891_ITIs, t3891_VT, &EmptyCustomAttributesCache, &t3891_TI, &t3891_0_0_0, &t3891_1_0_0, t3891_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t29 *), -1, 0, 0, -1, 161, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 34, 3, 7};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3892_TI;



// Metadata Definition UnityEngine.UI.Outline[]
static MethodInfo* t3892_MIs[] =
{
	NULL
};
extern MethodInfo m21910_MI;
extern MethodInfo m21911_MI;
extern MethodInfo m21912_MI;
extern MethodInfo m21913_MI;
extern MethodInfo m21914_MI;
extern MethodInfo m21915_MI;
extern MethodInfo m21909_MI;
extern MethodInfo m21917_MI;
extern MethodInfo m21918_MI;
extern MethodInfo m21921_MI;
extern MethodInfo m21922_MI;
extern MethodInfo m21923_MI;
extern MethodInfo m21924_MI;
extern MethodInfo m21925_MI;
extern MethodInfo m21926_MI;
extern MethodInfo m21920_MI;
extern MethodInfo m21928_MI;
extern MethodInfo m21929_MI;
static MethodInfo* t3892_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21910_MI,
	&m5907_MI,
	&m21911_MI,
	&m21912_MI,
	&m21913_MI,
	&m21914_MI,
	&m21915_MI,
	&m5908_MI,
	&m21909_MI,
	&m21917_MI,
	&m21918_MI,
	&m5905_MI,
	&m5906_MI,
	&m21921_MI,
	&m5907_MI,
	&m21922_MI,
	&m21923_MI,
	&m21924_MI,
	&m21925_MI,
	&m21926_MI,
	&m5908_MI,
	&m21920_MI,
	&m21928_MI,
	&m21929_MI,
	&m5905_MI,
	&m5906_MI,
	&m21887_MI,
	&m5907_MI,
	&m21888_MI,
	&m21889_MI,
	&m21890_MI,
	&m21891_MI,
	&m21892_MI,
	&m5908_MI,
	&m21886_MI,
	&m21894_MI,
	&m21895_MI,
	&m5905_MI,
	&m5906_MI,
	&m21898_MI,
	&m5907_MI,
	&m21899_MI,
	&m21900_MI,
	&m21901_MI,
	&m21902_MI,
	&m21903_MI,
	&m5908_MI,
	&m21897_MI,
	&m21905_MI,
	&m21906_MI,
	&m5905_MI,
	&m5906_MI,
	&m19777_MI,
	&m5907_MI,
	&m19778_MI,
	&m19779_MI,
	&m19780_MI,
	&m19781_MI,
	&m19782_MI,
	&m5908_MI,
	&m19776_MI,
	&m19784_MI,
	&m19785_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5523_TI;
extern TypeInfo t5524_TI;
extern TypeInfo t5525_TI;
extern TypeInfo t5526_TI;
extern TypeInfo t5527_TI;
extern TypeInfo t5528_TI;
static TypeInfo* t3892_ITIs[] = 
{
	&t5523_TI,
	&t5524_TI,
	&t5525_TI,
	&t5526_TI,
	&t5527_TI,
	&t5528_TI,
	&t5517_TI,
	&t5518_TI,
	&t5519_TI,
	&t5520_TI,
	&t5521_TI,
	&t5522_TI,
	&t5122_TI,
	&t5123_TI,
	&t5124_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3892_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5523_TI, 21},
	{ &t5524_TI, 28},
	{ &t5525_TI, 33},
	{ &t5526_TI, 34},
	{ &t5527_TI, 41},
	{ &t5528_TI, 46},
	{ &t5517_TI, 47},
	{ &t5518_TI, 54},
	{ &t5519_TI, 59},
	{ &t5520_TI, 60},
	{ &t5521_TI, 67},
	{ &t5522_TI, 72},
	{ &t5122_TI, 73},
	{ &t5123_TI, 80},
	{ &t5124_TI, 85},
	{ &t5059_TI, 86},
	{ &t5060_TI, 93},
	{ &t5061_TI, 98},
	{ &t5062_TI, 99},
	{ &t5063_TI, 106},
	{ &t5064_TI, 111},
	{ &t2240_TI, 112},
	{ &t2245_TI, 119},
	{ &t2241_TI, 124},
	{ &t5065_TI, 125},
	{ &t5066_TI, 132},
	{ &t5067_TI, 137},
	{ &t2182_TI, 138},
	{ &t2186_TI, 145},
	{ &t2183_TI, 150},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3892_0_0_0;
extern Il2CppType t3892_1_0_0;
struct t284;
extern TypeInfo t284_TI;
extern CustomAttributesCache t284__CustomAttributeCache;
TypeInfo t3892_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Outline[]", "UnityEngine.UI", t3892_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t284_TI, t3892_ITIs, t3892_VT, &EmptyCustomAttributesCache, &t3892_TI, &t3892_0_0_0, &t3892_1_0_0, t3892_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t284 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 151, 30, 34};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3893_TI;



// Metadata Definition UnityEngine.UI.Shadow[]
static MethodInfo* t3893_MIs[] =
{
	NULL
};
static MethodInfo* t3893_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21921_MI,
	&m5907_MI,
	&m21922_MI,
	&m21923_MI,
	&m21924_MI,
	&m21925_MI,
	&m21926_MI,
	&m5908_MI,
	&m21920_MI,
	&m21928_MI,
	&m21929_MI,
	&m5905_MI,
	&m5906_MI,
	&m21887_MI,
	&m5907_MI,
	&m21888_MI,
	&m21889_MI,
	&m21890_MI,
	&m21891_MI,
	&m21892_MI,
	&m5908_MI,
	&m21886_MI,
	&m21894_MI,
	&m21895_MI,
	&m5905_MI,
	&m5906_MI,
	&m21898_MI,
	&m5907_MI,
	&m21899_MI,
	&m21900_MI,
	&m21901_MI,
	&m21902_MI,
	&m21903_MI,
	&m5908_MI,
	&m21897_MI,
	&m21905_MI,
	&m21906_MI,
	&m5905_MI,
	&m5906_MI,
	&m19777_MI,
	&m5907_MI,
	&m19778_MI,
	&m19779_MI,
	&m19780_MI,
	&m19781_MI,
	&m19782_MI,
	&m5908_MI,
	&m19776_MI,
	&m19784_MI,
	&m19785_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
static TypeInfo* t3893_ITIs[] = 
{
	&t5526_TI,
	&t5527_TI,
	&t5528_TI,
	&t5517_TI,
	&t5518_TI,
	&t5519_TI,
	&t5520_TI,
	&t5521_TI,
	&t5522_TI,
	&t5122_TI,
	&t5123_TI,
	&t5124_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3893_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5526_TI, 21},
	{ &t5527_TI, 28},
	{ &t5528_TI, 33},
	{ &t5517_TI, 34},
	{ &t5518_TI, 41},
	{ &t5519_TI, 46},
	{ &t5520_TI, 47},
	{ &t5521_TI, 54},
	{ &t5522_TI, 59},
	{ &t5122_TI, 60},
	{ &t5123_TI, 67},
	{ &t5124_TI, 72},
	{ &t5059_TI, 73},
	{ &t5060_TI, 80},
	{ &t5061_TI, 85},
	{ &t5062_TI, 86},
	{ &t5063_TI, 93},
	{ &t5064_TI, 98},
	{ &t2240_TI, 99},
	{ &t2245_TI, 106},
	{ &t2241_TI, 111},
	{ &t5065_TI, 112},
	{ &t5066_TI, 119},
	{ &t5067_TI, 124},
	{ &t2182_TI, 125},
	{ &t2186_TI, 132},
	{ &t2183_TI, 137},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3893_0_0_0;
extern Il2CppType t3893_1_0_0;
struct t285;
extern TypeInfo t285_TI;
extern CustomAttributesCache t285__CustomAttributeCache;
extern CustomAttributesCache t285__CustomAttributeCache_m_EffectColor;
extern CustomAttributesCache t285__CustomAttributeCache_m_EffectDistance;
extern CustomAttributesCache t285__CustomAttributeCache_m_UseGraphicAlpha;
TypeInfo t3893_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Shadow[]", "UnityEngine.UI", t3893_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t285_TI, t3893_ITIs, t3893_VT, &EmptyCustomAttributesCache, &t3893_TI, &t3893_0_0_0, &t3893_1_0_0, t3893_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t285 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 138, 27, 31};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3894_TI;



// Metadata Definition UnityEngine.UI.PositionAsUV1[]
static MethodInfo* t3894_MIs[] =
{
	NULL
};
extern MethodInfo m21933_MI;
extern MethodInfo m21934_MI;
extern MethodInfo m21935_MI;
extern MethodInfo m21936_MI;
extern MethodInfo m21937_MI;
extern MethodInfo m21938_MI;
extern MethodInfo m21932_MI;
extern MethodInfo m21940_MI;
extern MethodInfo m21941_MI;
static MethodInfo* t3894_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21933_MI,
	&m5907_MI,
	&m21934_MI,
	&m21935_MI,
	&m21936_MI,
	&m21937_MI,
	&m21938_MI,
	&m5908_MI,
	&m21932_MI,
	&m21940_MI,
	&m21941_MI,
	&m5905_MI,
	&m5906_MI,
	&m21887_MI,
	&m5907_MI,
	&m21888_MI,
	&m21889_MI,
	&m21890_MI,
	&m21891_MI,
	&m21892_MI,
	&m5908_MI,
	&m21886_MI,
	&m21894_MI,
	&m21895_MI,
	&m5905_MI,
	&m5906_MI,
	&m21898_MI,
	&m5907_MI,
	&m21899_MI,
	&m21900_MI,
	&m21901_MI,
	&m21902_MI,
	&m21903_MI,
	&m5908_MI,
	&m21897_MI,
	&m21905_MI,
	&m21906_MI,
	&m5905_MI,
	&m5906_MI,
	&m19777_MI,
	&m5907_MI,
	&m19778_MI,
	&m19779_MI,
	&m19780_MI,
	&m19781_MI,
	&m19782_MI,
	&m5908_MI,
	&m19776_MI,
	&m19784_MI,
	&m19785_MI,
	&m5905_MI,
	&m5906_MI,
	&m19513_MI,
	&m5907_MI,
	&m19514_MI,
	&m19515_MI,
	&m19516_MI,
	&m19517_MI,
	&m19518_MI,
	&m5908_MI,
	&m19512_MI,
	&m19520_MI,
	&m19521_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5529_TI;
extern TypeInfo t5530_TI;
extern TypeInfo t5531_TI;
static TypeInfo* t3894_ITIs[] = 
{
	&t5529_TI,
	&t5530_TI,
	&t5531_TI,
	&t5517_TI,
	&t5518_TI,
	&t5519_TI,
	&t5520_TI,
	&t5521_TI,
	&t5522_TI,
	&t5122_TI,
	&t5123_TI,
	&t5124_TI,
	&t5059_TI,
	&t5060_TI,
	&t5061_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3894_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5529_TI, 21},
	{ &t5530_TI, 28},
	{ &t5531_TI, 33},
	{ &t5517_TI, 34},
	{ &t5518_TI, 41},
	{ &t5519_TI, 46},
	{ &t5520_TI, 47},
	{ &t5521_TI, 54},
	{ &t5522_TI, 59},
	{ &t5122_TI, 60},
	{ &t5123_TI, 67},
	{ &t5124_TI, 72},
	{ &t5059_TI, 73},
	{ &t5060_TI, 80},
	{ &t5061_TI, 85},
	{ &t5062_TI, 86},
	{ &t5063_TI, 93},
	{ &t5064_TI, 98},
	{ &t2240_TI, 99},
	{ &t2245_TI, 106},
	{ &t2241_TI, 111},
	{ &t5065_TI, 112},
	{ &t5066_TI, 119},
	{ &t5067_TI, 124},
	{ &t2182_TI, 125},
	{ &t2186_TI, 132},
	{ &t2183_TI, 137},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t3894_0_0_0;
extern Il2CppType t3894_1_0_0;
struct t286;
extern TypeInfo t286_TI;
extern CustomAttributesCache t286__CustomAttributeCache;
TypeInfo t3894_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "PositionAsUV1[]", "UnityEngine.UI", t3894_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t286_TI, t3894_ITIs, t3894_VT, &EmptyCustomAttributesCache, &t3894_TI, &t3894_0_0_0, &t3894_1_0_0, t3894_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t286 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 138, 27, 31};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3732_TI;



// Metadata Definition UnityEngine.AssetBundle[]
static MethodInfo* t3732_MIs[] =
{
	NULL
};
extern MethodInfo m21946_MI;
extern MethodInfo m21947_MI;
extern MethodInfo m21948_MI;
extern MethodInfo m21949_MI;
extern MethodInfo m21950_MI;
extern MethodInfo m21951_MI;
extern MethodInfo m21945_MI;
extern MethodInfo m21953_MI;
extern MethodInfo m21954_MI;
static MethodInfo* t3732_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21946_MI,
	&m5907_MI,
	&m21947_MI,
	&m21948_MI,
	&m21949_MI,
	&m21950_MI,
	&m21951_MI,
	&m5908_MI,
	&m21945_MI,
	&m21953_MI,
	&m21954_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5532_TI;
extern TypeInfo t5533_TI;
extern TypeInfo t5534_TI;
static TypeInfo* t3732_ITIs[] = 
{
	&t5532_TI,
	&t5533_TI,
	&t5534_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3732_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5532_TI, 21},
	{ &t5533_TI, 28},
	{ &t5534_TI, 33},
	{ &t5065_TI, 34},
	{ &t5066_TI, 41},
	{ &t5067_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3732_0_0_0;
extern Il2CppType t3732_1_0_0;
struct t442;
extern TypeInfo t442_TI;
extern CustomAttributesCache t442__CustomAttributeCache_m2063;
extern CustomAttributesCache t442__CustomAttributeCache_m2064;
extern CustomAttributesCache t442__CustomAttributeCache_m2065;
TypeInfo t3732_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "AssetBundle[]", "UnityEngine", t3732_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t442_TI, t3732_ITIs, t3732_VT, &EmptyCustomAttributesCache, &t3732_TI, &t3732_0_0_0, &t3732_1_0_0, t3732_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t442 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3733_TI;



// Metadata Definition UnityEngine.SendMessageOptions[]
static MethodInfo* t3733_MIs[] =
{
	NULL
};
extern MethodInfo m21958_MI;
extern MethodInfo m21959_MI;
extern MethodInfo m21960_MI;
extern MethodInfo m21961_MI;
extern MethodInfo m21962_MI;
extern MethodInfo m21963_MI;
extern MethodInfo m21957_MI;
extern MethodInfo m21965_MI;
extern MethodInfo m21966_MI;
static MethodInfo* t3733_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21958_MI,
	&m5907_MI,
	&m21959_MI,
	&m21960_MI,
	&m21961_MI,
	&m21962_MI,
	&m21963_MI,
	&m5908_MI,
	&m21957_MI,
	&m21965_MI,
	&m21966_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5535_TI;
extern TypeInfo t5536_TI;
extern TypeInfo t5537_TI;
static TypeInfo* t3733_ITIs[] = 
{
	&t5535_TI,
	&t5536_TI,
	&t5537_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3733_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5535_TI, 21},
	{ &t5536_TI, 28},
	{ &t5537_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3733_0_0_0;
extern Il2CppType t3733_1_0_0;
#include "t445.h"
extern TypeInfo t445_TI;
TypeInfo t3733_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "SendMessageOptions[]", "UnityEngine", t3733_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t445_TI, t3733_ITIs, t3733_VT, &EmptyCustomAttributesCache, &t44_TI, &t3733_0_0_0, &t3733_1_0_0, t3733_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3734_TI;



// Metadata Definition UnityEngine.RuntimePlatform[]
static MethodInfo* t3734_MIs[] =
{
	NULL
};
extern MethodInfo m21969_MI;
extern MethodInfo m21970_MI;
extern MethodInfo m21971_MI;
extern MethodInfo m21972_MI;
extern MethodInfo m21973_MI;
extern MethodInfo m21974_MI;
extern MethodInfo m21968_MI;
extern MethodInfo m21976_MI;
extern MethodInfo m21977_MI;
static MethodInfo* t3734_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21969_MI,
	&m5907_MI,
	&m21970_MI,
	&m21971_MI,
	&m21972_MI,
	&m21973_MI,
	&m21974_MI,
	&m5908_MI,
	&m21968_MI,
	&m21976_MI,
	&m21977_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5538_TI;
extern TypeInfo t5539_TI;
extern TypeInfo t5540_TI;
static TypeInfo* t3734_ITIs[] = 
{
	&t5538_TI,
	&t5539_TI,
	&t5540_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3734_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5538_TI, 21},
	{ &t5539_TI, 28},
	{ &t5540_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3734_0_0_0;
extern Il2CppType t3734_1_0_0;
#include "t376.h"
extern TypeInfo t376_TI;
extern CustomAttributesCache t376__CustomAttributeCache_NaCl;
extern CustomAttributesCache t376__CustomAttributeCache_FlashPlayer;
extern CustomAttributesCache t376__CustomAttributeCache_MetroPlayerX86;
extern CustomAttributesCache t376__CustomAttributeCache_MetroPlayerX64;
extern CustomAttributesCache t376__CustomAttributeCache_MetroPlayerARM;
TypeInfo t3734_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "RuntimePlatform[]", "UnityEngine", t3734_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t376_TI, t3734_ITIs, t3734_VT, &EmptyCustomAttributesCache, &t44_TI, &t3734_0_0_0, &t3734_1_0_0, t3734_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3735_TI;



// Metadata Definition UnityEngine.LogType[]
static MethodInfo* t3735_MIs[] =
{
	NULL
};
extern MethodInfo m21980_MI;
extern MethodInfo m21981_MI;
extern MethodInfo m21982_MI;
extern MethodInfo m21983_MI;
extern MethodInfo m21984_MI;
extern MethodInfo m21985_MI;
extern MethodInfo m21979_MI;
extern MethodInfo m21987_MI;
extern MethodInfo m21988_MI;
static MethodInfo* t3735_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21980_MI,
	&m5907_MI,
	&m21981_MI,
	&m21982_MI,
	&m21983_MI,
	&m21984_MI,
	&m21985_MI,
	&m5908_MI,
	&m21979_MI,
	&m21987_MI,
	&m21988_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5541_TI;
extern TypeInfo t5542_TI;
extern TypeInfo t5543_TI;
static TypeInfo* t3735_ITIs[] = 
{
	&t5541_TI,
	&t5542_TI,
	&t5543_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3735_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5541_TI, 21},
	{ &t5542_TI, 28},
	{ &t5543_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3735_0_0_0;
extern Il2CppType t3735_1_0_0;
#include "t447.h"
extern TypeInfo t447_TI;
TypeInfo t3735_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "LogType[]", "UnityEngine", t3735_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t447_TI, t3735_ITIs, t3735_VT, &EmptyCustomAttributesCache, &t44_TI, &t3735_0_0_0, &t3735_1_0_0, t3735_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 257, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3736_TI;



// Metadata Definition UnityEngine.ScriptableObject[]
static MethodInfo* t3736_MIs[] =
{
	NULL
};
extern MethodInfo m21991_MI;
extern MethodInfo m21992_MI;
extern MethodInfo m21993_MI;
extern MethodInfo m21994_MI;
extern MethodInfo m21995_MI;
extern MethodInfo m21996_MI;
extern MethodInfo m21990_MI;
extern MethodInfo m21998_MI;
extern MethodInfo m21999_MI;
static MethodInfo* t3736_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m21991_MI,
	&m5907_MI,
	&m21992_MI,
	&m21993_MI,
	&m21994_MI,
	&m21995_MI,
	&m21996_MI,
	&m5908_MI,
	&m21990_MI,
	&m21998_MI,
	&m21999_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5544_TI;
extern TypeInfo t5545_TI;
extern TypeInfo t5546_TI;
static TypeInfo* t3736_ITIs[] = 
{
	&t5544_TI,
	&t5545_TI,
	&t5546_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3736_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5544_TI, 21},
	{ &t5545_TI, 28},
	{ &t5546_TI, 33},
	{ &t5065_TI, 34},
	{ &t5066_TI, 41},
	{ &t5067_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3736_0_0_0;
extern Il2CppType t3736_1_0_0;
struct t450;
extern TypeInfo t450_TI;
extern CustomAttributesCache t450__CustomAttributeCache_m2076;
extern CustomAttributesCache t450__CustomAttributeCache_m2077;
extern CustomAttributesCache t450__CustomAttributeCache_m2079;
TypeInfo t3736_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "ScriptableObject[]", "UnityEngine", t3736_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t450_TI, t3736_ITIs, t3736_VT, &EmptyCustomAttributesCache, &t3736_TI, &t3736_0_0_0, &t3736_1_0_0, t3736_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t450 *), -1, 0, 0, -1, 1048585, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3737_TI;



// Metadata Definition UnityEngine.Renderer[]
static MethodInfo* t3737_MIs[] =
{
	NULL
};
extern MethodInfo m22003_MI;
extern MethodInfo m22004_MI;
extern MethodInfo m22005_MI;
extern MethodInfo m22006_MI;
extern MethodInfo m22007_MI;
extern MethodInfo m22008_MI;
extern MethodInfo m22002_MI;
extern MethodInfo m22010_MI;
extern MethodInfo m22011_MI;
static MethodInfo* t3737_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22003_MI,
	&m5907_MI,
	&m22004_MI,
	&m22005_MI,
	&m22006_MI,
	&m22007_MI,
	&m22008_MI,
	&m5908_MI,
	&m22002_MI,
	&m22010_MI,
	&m22011_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5547_TI;
extern TypeInfo t5548_TI;
extern TypeInfo t5549_TI;
static TypeInfo* t3737_ITIs[] = 
{
	&t5547_TI,
	&t5548_TI,
	&t5549_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3737_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5547_TI, 21},
	{ &t5548_TI, 28},
	{ &t5549_TI, 33},
	{ &t2240_TI, 34},
	{ &t2245_TI, 41},
	{ &t2241_TI, 46},
	{ &t5065_TI, 47},
	{ &t5066_TI, 54},
	{ &t5067_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3737_0_0_0;
extern Il2CppType t3737_1_0_0;
struct t334;
extern TypeInfo t334_TI;
extern CustomAttributesCache t334__CustomAttributeCache_m1476;
extern CustomAttributesCache t334__CustomAttributeCache_m1477;
TypeInfo t3737_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Renderer[]", "UnityEngine", t3737_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t334_TI, t3737_ITIs, t3737_VT, &EmptyCustomAttributesCache, &t3737_TI, &t3737_0_0_0, &t3737_1_0_0, t3737_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t334 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3738_TI;



// Metadata Definition UnityEngine.Texture[]
static MethodInfo* t3738_MIs[] =
{
	NULL
};
extern MethodInfo m22015_MI;
extern MethodInfo m22016_MI;
extern MethodInfo m22017_MI;
extern MethodInfo m22018_MI;
extern MethodInfo m22019_MI;
extern MethodInfo m22020_MI;
extern MethodInfo m22014_MI;
extern MethodInfo m22022_MI;
extern MethodInfo m22023_MI;
static MethodInfo* t3738_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22015_MI,
	&m5907_MI,
	&m22016_MI,
	&m22017_MI,
	&m22018_MI,
	&m22019_MI,
	&m22020_MI,
	&m5908_MI,
	&m22014_MI,
	&m22022_MI,
	&m22023_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5550_TI;
extern TypeInfo t5551_TI;
extern TypeInfo t5552_TI;
static TypeInfo* t3738_ITIs[] = 
{
	&t5550_TI,
	&t5551_TI,
	&t5552_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3738_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5550_TI, 21},
	{ &t5551_TI, 28},
	{ &t5552_TI, 33},
	{ &t5065_TI, 34},
	{ &t5066_TI, 41},
	{ &t5067_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3738_0_0_0;
extern Il2CppType t3738_1_0_0;
struct t162;
extern TypeInfo t162_TI;
extern CustomAttributesCache t162__CustomAttributeCache_m2107;
extern CustomAttributesCache t162__CustomAttributeCache_m2108;
TypeInfo t3738_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Texture[]", "UnityEngine", t3738_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t162_TI, t3738_ITIs, t3738_VT, &EmptyCustomAttributesCache, &t3738_TI, &t3738_0_0_0, &t3738_1_0_0, t3738_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t162 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3739_TI;



// Metadata Definition UnityEngine.Texture2D[]
static MethodInfo* t3739_MIs[] =
{
	NULL
};
extern MethodInfo m22027_MI;
extern MethodInfo m22028_MI;
extern MethodInfo m22029_MI;
extern MethodInfo m22030_MI;
extern MethodInfo m22031_MI;
extern MethodInfo m22032_MI;
extern MethodInfo m22026_MI;
extern MethodInfo m22034_MI;
extern MethodInfo m22035_MI;
static MethodInfo* t3739_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22027_MI,
	&m5907_MI,
	&m22028_MI,
	&m22029_MI,
	&m22030_MI,
	&m22031_MI,
	&m22032_MI,
	&m5908_MI,
	&m22026_MI,
	&m22034_MI,
	&m22035_MI,
	&m5905_MI,
	&m5906_MI,
	&m22015_MI,
	&m5907_MI,
	&m22016_MI,
	&m22017_MI,
	&m22018_MI,
	&m22019_MI,
	&m22020_MI,
	&m5908_MI,
	&m22014_MI,
	&m22022_MI,
	&m22023_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5553_TI;
extern TypeInfo t5554_TI;
extern TypeInfo t5555_TI;
static TypeInfo* t3739_ITIs[] = 
{
	&t5553_TI,
	&t5554_TI,
	&t5555_TI,
	&t5550_TI,
	&t5551_TI,
	&t5552_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3739_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5553_TI, 21},
	{ &t5554_TI, 28},
	{ &t5555_TI, 33},
	{ &t5550_TI, 34},
	{ &t5551_TI, 41},
	{ &t5552_TI, 46},
	{ &t5065_TI, 47},
	{ &t5066_TI, 54},
	{ &t5067_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3739_0_0_0;
extern Il2CppType t3739_1_0_0;
struct t157;
extern TypeInfo t157_TI;
extern CustomAttributesCache t157__CustomAttributeCache_m1568;
extern CustomAttributesCache t157__CustomAttributeCache_m1683;
TypeInfo t3739_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Texture2D[]", "UnityEngine", t3739_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t157_TI, t3739_ITIs, t3739_VT, &EmptyCustomAttributesCache, &t3739_TI, &t3739_0_0_0, &t3739_1_0_0, t3739_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t157 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3740_TI;



// Metadata Definition UnityEngine.RenderTexture[]
static MethodInfo* t3740_MIs[] =
{
	NULL
};
extern MethodInfo m22039_MI;
extern MethodInfo m22040_MI;
extern MethodInfo m22041_MI;
extern MethodInfo m22042_MI;
extern MethodInfo m22043_MI;
extern MethodInfo m22044_MI;
extern MethodInfo m22038_MI;
extern MethodInfo m22046_MI;
extern MethodInfo m22047_MI;
static MethodInfo* t3740_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22039_MI,
	&m5907_MI,
	&m22040_MI,
	&m22041_MI,
	&m22042_MI,
	&m22043_MI,
	&m22044_MI,
	&m5908_MI,
	&m22038_MI,
	&m22046_MI,
	&m22047_MI,
	&m5905_MI,
	&m5906_MI,
	&m22015_MI,
	&m5907_MI,
	&m22016_MI,
	&m22017_MI,
	&m22018_MI,
	&m22019_MI,
	&m22020_MI,
	&m5908_MI,
	&m22014_MI,
	&m22022_MI,
	&m22023_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5556_TI;
extern TypeInfo t5557_TI;
extern TypeInfo t5558_TI;
static TypeInfo* t3740_ITIs[] = 
{
	&t5556_TI,
	&t5557_TI,
	&t5558_TI,
	&t5550_TI,
	&t5551_TI,
	&t5552_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3740_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5556_TI, 21},
	{ &t5557_TI, 28},
	{ &t5558_TI, 33},
	{ &t5550_TI, 34},
	{ &t5551_TI, 41},
	{ &t5552_TI, 46},
	{ &t5065_TI, 47},
	{ &t5066_TI, 54},
	{ &t5067_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3740_0_0_0;
extern Il2CppType t3740_1_0_0;
struct t453;
extern TypeInfo t453_TI;
extern CustomAttributesCache t453__CustomAttributeCache_m2109;
extern CustomAttributesCache t453__CustomAttributeCache_m2110;
TypeInfo t3740_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "RenderTexture[]", "UnityEngine", t3740_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t453_TI, t3740_ITIs, t3740_VT, &EmptyCustomAttributesCache, &t3740_TI, &t3740_0_0_0, &t3740_1_0_0, t3740_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t453 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3741_TI;



// Metadata Definition UnityEngine.ReflectionProbe[]
static MethodInfo* t3741_MIs[] =
{
	NULL
};
extern MethodInfo m22051_MI;
extern MethodInfo m22052_MI;
extern MethodInfo m22053_MI;
extern MethodInfo m22054_MI;
extern MethodInfo m22055_MI;
extern MethodInfo m22056_MI;
extern MethodInfo m22050_MI;
extern MethodInfo m22058_MI;
extern MethodInfo m22059_MI;
static MethodInfo* t3741_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22051_MI,
	&m5907_MI,
	&m22052_MI,
	&m22053_MI,
	&m22054_MI,
	&m22055_MI,
	&m22056_MI,
	&m5908_MI,
	&m22050_MI,
	&m22058_MI,
	&m22059_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5559_TI;
extern TypeInfo t5560_TI;
extern TypeInfo t5561_TI;
static TypeInfo* t3741_ITIs[] = 
{
	&t5559_TI,
	&t5560_TI,
	&t5561_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3741_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5559_TI, 21},
	{ &t5560_TI, 28},
	{ &t5561_TI, 33},
	{ &t5062_TI, 34},
	{ &t5063_TI, 41},
	{ &t5064_TI, 46},
	{ &t2240_TI, 47},
	{ &t2245_TI, 54},
	{ &t2241_TI, 59},
	{ &t5065_TI, 60},
	{ &t5066_TI, 67},
	{ &t5067_TI, 72},
	{ &t2182_TI, 73},
	{ &t2186_TI, 80},
	{ &t2183_TI, 85},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3741_0_0_0;
extern Il2CppType t3741_1_0_0;
struct t454;
extern TypeInfo t454_TI;
TypeInfo t3741_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "ReflectionProbe[]", "UnityEngine", t3741_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t454_TI, t3741_ITIs, t3741_VT, &EmptyCustomAttributesCache, &t3741_TI, &t3741_0_0_0, &t3741_1_0_0, t3741_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t454 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 86, 15, 19};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3742_TI;



// Metadata Definition UnityEngine.GUIElement[]
static MethodInfo* t3742_MIs[] =
{
	NULL
};
extern MethodInfo m22063_MI;
extern MethodInfo m22064_MI;
extern MethodInfo m22065_MI;
extern MethodInfo m22066_MI;
extern MethodInfo m22067_MI;
extern MethodInfo m22068_MI;
extern MethodInfo m22062_MI;
extern MethodInfo m22070_MI;
extern MethodInfo m22071_MI;
static MethodInfo* t3742_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22063_MI,
	&m5907_MI,
	&m22064_MI,
	&m22065_MI,
	&m22066_MI,
	&m22067_MI,
	&m22068_MI,
	&m5908_MI,
	&m22062_MI,
	&m22070_MI,
	&m22071_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5562_TI;
extern TypeInfo t5563_TI;
extern TypeInfo t5564_TI;
static TypeInfo* t3742_ITIs[] = 
{
	&t5562_TI,
	&t5563_TI,
	&t5564_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3742_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5562_TI, 21},
	{ &t5563_TI, 28},
	{ &t5564_TI, 33},
	{ &t5062_TI, 34},
	{ &t5063_TI, 41},
	{ &t5064_TI, 46},
	{ &t2240_TI, 47},
	{ &t2245_TI, 54},
	{ &t2241_TI, 59},
	{ &t5065_TI, 60},
	{ &t5066_TI, 67},
	{ &t5067_TI, 72},
	{ &t2182_TI, 73},
	{ &t2186_TI, 80},
	{ &t2183_TI, 85},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3742_0_0_0;
extern Il2CppType t3742_1_0_0;
struct t455;
extern TypeInfo t455_TI;
TypeInfo t3742_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "GUIElement[]", "UnityEngine", t3742_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t455_TI, t3742_ITIs, t3742_VT, &EmptyCustomAttributesCache, &t3742_TI, &t3742_0_0_0, &t3742_1_0_0, t3742_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t455 *), -1, 0, 0, -1, 1048577, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 86, 15, 19};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3743_TI;



// Metadata Definition UnityEngine.GUILayer[]
static MethodInfo* t3743_MIs[] =
{
	NULL
};
extern MethodInfo m22075_MI;
extern MethodInfo m22076_MI;
extern MethodInfo m22077_MI;
extern MethodInfo m22078_MI;
extern MethodInfo m22079_MI;
extern MethodInfo m22080_MI;
extern MethodInfo m22074_MI;
extern MethodInfo m22082_MI;
extern MethodInfo m22083_MI;
static MethodInfo* t3743_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22075_MI,
	&m5907_MI,
	&m22076_MI,
	&m22077_MI,
	&m22078_MI,
	&m22079_MI,
	&m22080_MI,
	&m5908_MI,
	&m22074_MI,
	&m22082_MI,
	&m22083_MI,
	&m5905_MI,
	&m5906_MI,
	&m19524_MI,
	&m5907_MI,
	&m19525_MI,
	&m19526_MI,
	&m19527_MI,
	&m19528_MI,
	&m19529_MI,
	&m5908_MI,
	&m19523_MI,
	&m19531_MI,
	&m19532_MI,
	&m5905_MI,
	&m5906_MI,
	&m19535_MI,
	&m5907_MI,
	&m19536_MI,
	&m19537_MI,
	&m19538_MI,
	&m19539_MI,
	&m19540_MI,
	&m5908_MI,
	&m19534_MI,
	&m19542_MI,
	&m19543_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5565_TI;
extern TypeInfo t5566_TI;
extern TypeInfo t5567_TI;
static TypeInfo* t3743_ITIs[] = 
{
	&t5565_TI,
	&t5566_TI,
	&t5567_TI,
	&t5062_TI,
	&t5063_TI,
	&t5064_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3743_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5565_TI, 21},
	{ &t5566_TI, 28},
	{ &t5567_TI, 33},
	{ &t5062_TI, 34},
	{ &t5063_TI, 41},
	{ &t5064_TI, 46},
	{ &t2240_TI, 47},
	{ &t2245_TI, 54},
	{ &t2241_TI, 59},
	{ &t5065_TI, 60},
	{ &t5066_TI, 67},
	{ &t5067_TI, 72},
	{ &t2182_TI, 73},
	{ &t2186_TI, 80},
	{ &t2183_TI, 85},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3743_0_0_0;
extern Il2CppType t3743_1_0_0;
struct t456;
extern TypeInfo t456_TI;
extern CustomAttributesCache t456__CustomAttributeCache_m2114;
TypeInfo t3743_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "GUILayer[]", "UnityEngine", t3743_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t456_TI, t3743_ITIs, t3743_VT, &EmptyCustomAttributesCache, &t3743_TI, &t3743_0_0_0, &t3743_1_0_0, t3743_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t456 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 86, 15, 19};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t473_TI;



// Metadata Definition UnityEngine.GUILayoutOption[]
static MethodInfo* t473_MIs[] =
{
	NULL
};
extern MethodInfo m22087_MI;
extern MethodInfo m22088_MI;
extern MethodInfo m22089_MI;
extern MethodInfo m22090_MI;
extern MethodInfo m22091_MI;
extern MethodInfo m22092_MI;
extern MethodInfo m22086_MI;
extern MethodInfo m22094_MI;
extern MethodInfo m22095_MI;
static MethodInfo* t473_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22087_MI,
	&m5907_MI,
	&m22088_MI,
	&m22089_MI,
	&m22090_MI,
	&m22091_MI,
	&m22092_MI,
	&m5908_MI,
	&m22086_MI,
	&m22094_MI,
	&m22095_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5568_TI;
extern TypeInfo t5569_TI;
extern TypeInfo t5570_TI;
static TypeInfo* t473_ITIs[] = 
{
	&t5568_TI,
	&t5569_TI,
	&t5570_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t473_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5568_TI, 21},
	{ &t5569_TI, 28},
	{ &t5570_TI, 33},
	{ &t2182_TI, 34},
	{ &t2186_TI, 41},
	{ &t2183_TI, 46},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t473_0_0_0;
extern Il2CppType t473_1_0_0;
struct t468;
extern TypeInfo t468_TI;
TypeInfo t473_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "GUILayoutOption[]", "UnityEngine", t473_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t468_TI, t473_ITIs, t473_VT, &EmptyCustomAttributesCache, &t473_TI, &t473_0_0_0, &t473_1_0_0, t473_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t468 *), -1, 0, 0, -1, 1048833, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2858_TI;



// Metadata Definition System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>[]
static MethodInfo* t2858_MIs[] =
{
	NULL
};
extern MethodInfo m22098_MI;
extern MethodInfo m22099_MI;
extern MethodInfo m22100_MI;
extern MethodInfo m22101_MI;
extern MethodInfo m22102_MI;
extern MethodInfo m22103_MI;
extern MethodInfo m22097_MI;
extern MethodInfo m22105_MI;
extern MethodInfo m22106_MI;
static MethodInfo* t2858_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22098_MI,
	&m5907_MI,
	&m22099_MI,
	&m22100_MI,
	&m22101_MI,
	&m22102_MI,
	&m22103_MI,
	&m5908_MI,
	&m22097_MI,
	&m22105_MI,
	&m22106_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5571_TI;
extern TypeInfo t5572_TI;
extern TypeInfo t5573_TI;
static TypeInfo* t2858_ITIs[] = 
{
	&t5571_TI,
	&t5572_TI,
	&t5573_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2858_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5571_TI, 21},
	{ &t5572_TI, 28},
	{ &t5573_TI, 33},
	{ &t5116_TI, 34},
	{ &t5117_TI, 41},
	{ &t5118_TI, 46},
	{ &t2182_TI, 47},
	{ &t2186_TI, 54},
	{ &t2183_TI, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2858_0_0_0;
extern Il2CppType t2858_1_0_0;
#include "t2859.h"
extern TypeInfo t2859_TI;
extern CustomAttributesCache t1259__CustomAttributeCache;
TypeInfo t2858_TI = 
{
	&g_mscorlib_dll_Image, NULL, "KeyValuePair`2[]", "System.Collections.Generic", t2858_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t2859_TI, t2858_ITIs, t2858_VT, &EmptyCustomAttributesCache, &t2858_TI, &t2858_0_0_0, &t2858_1_0_0, t2858_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t2859 ), -1, 0, 0, -1, 1057033, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 60, 9, 13};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2855_TI;



// Metadata Definition UnityEngine.GUILayoutUtility/LayoutCache[]
static MethodInfo* t2855_MIs[] =
{
	NULL
};
extern MethodInfo m22109_MI;
extern MethodInfo m22110_MI;
extern MethodInfo m22111_MI;
extern MethodInfo m22112_MI;
extern MethodInfo m22113_MI;
extern MethodInfo m22114_MI;
extern MethodInfo m22108_MI;
extern MethodInfo m22116_MI;
extern MethodInfo m22117_MI;
static MethodInfo* t2855_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22109_MI,
	&m5907_MI,
	&m22110_MI,
	&m22111_MI,
	&m22112_MI,
	&m22113_MI,
	&m22114_MI,
	&m5908_MI,
	&m22108_MI,
	&m22116_MI,
	&m22117_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5574_TI;
extern TypeInfo t5575_TI;
extern TypeInfo t5576_TI;
static TypeInfo* t2855_ITIs[] = 
{
	&t5574_TI,
	&t5575_TI,
	&t5576_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2855_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5574_TI, 21},
	{ &t5575_TI, 28},
	{ &t5576_TI, 33},
	{ &t2182_TI, 34},
	{ &t2186_TI, 41},
	{ &t2183_TI, 46},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2855_0_0_0;
extern Il2CppType t2855_1_0_0;
struct t469;
extern TypeInfo t469_TI;
TypeInfo t2855_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "LayoutCache[]", "", t2855_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t469_TI, t2855_ITIs, t2855_VT, &EmptyCustomAttributesCache, &t2855_TI, &t2855_0_0_0, &t2855_1_0_0, t2855_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t469 *), -1, 0, 0, -1, 1048837, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2871_TI;



// Metadata Definition UnityEngine.GUILayoutEntry[]
static MethodInfo* t2871_MIs[] =
{
	NULL
};
extern MethodInfo m22127_MI;
extern MethodInfo m22128_MI;
extern MethodInfo m22129_MI;
extern MethodInfo m22130_MI;
extern MethodInfo m22131_MI;
extern MethodInfo m22132_MI;
extern MethodInfo m22126_MI;
extern MethodInfo m22134_MI;
extern MethodInfo m22135_MI;
static MethodInfo* t2871_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22127_MI,
	&m5907_MI,
	&m22128_MI,
	&m22129_MI,
	&m22130_MI,
	&m22131_MI,
	&m22132_MI,
	&m5908_MI,
	&m22126_MI,
	&m22134_MI,
	&m22135_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t2873_TI;
extern TypeInfo t2879_TI;
extern TypeInfo t2874_TI;
static TypeInfo* t2871_ITIs[] = 
{
	&t2873_TI,
	&t2879_TI,
	&t2874_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t2871_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2873_TI, 21},
	{ &t2879_TI, 28},
	{ &t2874_TI, 33},
	{ &t2182_TI, 34},
	{ &t2186_TI, 41},
	{ &t2183_TI, 46},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2871_0_0_0;
extern Il2CppType t2871_1_0_0;
struct t474;
extern TypeInfo t474_TI;
TypeInfo t2871_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "GUILayoutEntry[]", "UnityEngine", t2871_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t474_TI, t2871_ITIs, t2871_VT, &EmptyCustomAttributesCache, &t2871_TI, &t2871_0_0_0, &t2871_1_0_0, t2871_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t474 *), -1, sizeof(t2871_SFs), 0, -1, 1048576, 1, false, false, false, false, false, true, false, false, false, true, false, false, 0, 0, 0, 0, 0, 47, 6, 10};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3744_TI;



// Metadata Definition UnityEngine.GUILayoutOption/Type[]
static MethodInfo* t3744_MIs[] =
{
	NULL
};
extern MethodInfo m22153_MI;
extern MethodInfo m22154_MI;
extern MethodInfo m22155_MI;
extern MethodInfo m22156_MI;
extern MethodInfo m22157_MI;
extern MethodInfo m22158_MI;
extern MethodInfo m22152_MI;
extern MethodInfo m22160_MI;
extern MethodInfo m22161_MI;
static MethodInfo* t3744_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22153_MI,
	&m5907_MI,
	&m22154_MI,
	&m22155_MI,
	&m22156_MI,
	&m22157_MI,
	&m22158_MI,
	&m5908_MI,
	&m22152_MI,
	&m22160_MI,
	&m22161_MI,
	&m5905_MI,
	&m5906_MI,
	&m19711_MI,
	&m5907_MI,
	&m19712_MI,
	&m19713_MI,
	&m19714_MI,
	&m19715_MI,
	&m19716_MI,
	&m5908_MI,
	&m19710_MI,
	&m19718_MI,
	&m19719_MI,
	&m5905_MI,
	&m5906_MI,
	&m19722_MI,
	&m5907_MI,
	&m19723_MI,
	&m19724_MI,
	&m19725_MI,
	&m19726_MI,
	&m19727_MI,
	&m5908_MI,
	&m19721_MI,
	&m19729_MI,
	&m19730_MI,
	&m5905_MI,
	&m5906_MI,
	&m19733_MI,
	&m5907_MI,
	&m19734_MI,
	&m19735_MI,
	&m19736_MI,
	&m19737_MI,
	&m19738_MI,
	&m5908_MI,
	&m19732_MI,
	&m19740_MI,
	&m19741_MI,
	&m5905_MI,
	&m5906_MI,
	&m19744_MI,
	&m5907_MI,
	&m19745_MI,
	&m19746_MI,
	&m19747_MI,
	&m19748_MI,
	&m19749_MI,
	&m5908_MI,
	&m19743_MI,
	&m19751_MI,
	&m19752_MI,
	&m5905_MI,
	&m5906_MI,
	&m19755_MI,
	&m5907_MI,
	&m19756_MI,
	&m19757_MI,
	&m19758_MI,
	&m19759_MI,
	&m19760_MI,
	&m5908_MI,
	&m19754_MI,
	&m19762_MI,
	&m19763_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5577_TI;
extern TypeInfo t5578_TI;
extern TypeInfo t5579_TI;
static TypeInfo* t3744_ITIs[] = 
{
	&t5577_TI,
	&t5578_TI,
	&t5579_TI,
	&t5104_TI,
	&t5105_TI,
	&t5106_TI,
	&t5107_TI,
	&t5108_TI,
	&t5109_TI,
	&t5110_TI,
	&t5111_TI,
	&t5112_TI,
	&t5113_TI,
	&t5114_TI,
	&t5115_TI,
	&t5116_TI,
	&t5117_TI,
	&t5118_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3744_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5577_TI, 21},
	{ &t5578_TI, 28},
	{ &t5579_TI, 33},
	{ &t5104_TI, 34},
	{ &t5105_TI, 41},
	{ &t5106_TI, 46},
	{ &t5107_TI, 47},
	{ &t5108_TI, 54},
	{ &t5109_TI, 59},
	{ &t5110_TI, 60},
	{ &t5111_TI, 67},
	{ &t5112_TI, 72},
	{ &t5113_TI, 73},
	{ &t5114_TI, 80},
	{ &t5115_TI, 85},
	{ &t5116_TI, 86},
	{ &t5117_TI, 93},
	{ &t5118_TI, 98},
	{ &t2182_TI, 99},
	{ &t2186_TI, 106},
	{ &t2183_TI, 111},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3744_0_0_0;
extern Il2CppType t3744_1_0_0;
#include "t477.h"
extern TypeInfo t477_TI;
TypeInfo t3744_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "Type[]", "", t3744_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t477_TI, t3744_ITIs, t3744_VT, &EmptyCustomAttributesCache, &t44_TI, &t3744_0_0_0, &t3744_1_0_0, t3744_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (int32_t), -1, 0, 0, -1, 261, 1, false, false, false, true, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 112, 21, 25};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3745_TI;



// Metadata Definition UnityEngine.GUISkin[]
static MethodInfo* t3745_MIs[] =
{
	NULL
};
extern MethodInfo m22164_MI;
extern MethodInfo m22165_MI;
extern MethodInfo m22166_MI;
extern MethodInfo m22167_MI;
extern MethodInfo m22168_MI;
extern MethodInfo m22169_MI;
extern MethodInfo m22163_MI;
extern MethodInfo m22171_MI;
extern MethodInfo m22172_MI;
static MethodInfo* t3745_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m4200_MI,
	&m5904_MI,
	&m5920_MI,
	&m5921_MI,
	&m4199_MI,
	&m5922_MI,
	&m5923_MI,
	&m5895_MI,
	&m5896_MI,
	&m5897_MI,
	&m5898_MI,
	&m5899_MI,
	&m5900_MI,
	&m5901_MI,
	&m5902_MI,
	&m5903_MI,
	&m4006_MI,
	&m5905_MI,
	&m5906_MI,
	&m22164_MI,
	&m5907_MI,
	&m22165_MI,
	&m22166_MI,
	&m22167_MI,
	&m22168_MI,
	&m22169_MI,
	&m5908_MI,
	&m22163_MI,
	&m22171_MI,
	&m22172_MI,
	&m5905_MI,
	&m5906_MI,
	&m21991_MI,
	&m5907_MI,
	&m21992_MI,
	&m21993_MI,
	&m21994_MI,
	&m21995_MI,
	&m21996_MI,
	&m5908_MI,
	&m21990_MI,
	&m21998_MI,
	&m21999_MI,
	&m5905_MI,
	&m5906_MI,
	&m19546_MI,
	&m5907_MI,
	&m19547_MI,
	&m19548_MI,
	&m19549_MI,
	&m19550_MI,
	&m19551_MI,
	&m5908_MI,
	&m19545_MI,
	&m19553_MI,
	&m19554_MI,
	&m5905_MI,
	&m5906_MI,
	&m19494_MI,
	&m5907_MI,
	&m19496_MI,
	&m19498_MI,
	&m19499_MI,
	&m19500_MI,
	&m19501_MI,
	&m5908_MI,
	&m19490_MI,
	&m19503_MI,
	&m19504_MI,
};
extern TypeInfo t5580_TI;
extern TypeInfo t5581_TI;
extern TypeInfo t5582_TI;
static TypeInfo* t3745_ITIs[] = 
{
	&t5580_TI,
	&t5581_TI,
	&t5582_TI,
	&t5544_TI,
	&t5545_TI,
	&t5546_TI,
	&t5065_TI,
	&t5066_TI,
	&t5067_TI,
	&t2182_TI,
	&t2186_TI,
	&t2183_TI,
};
static Il2CppInterfaceOffsetPair t3745_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t373_TI, 5},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t5580_TI, 21},
	{ &t5581_TI, 28},
	{ &t5582_TI, 33},
	{ &t5544_TI, 34},
	{ &t5545_TI, 41},
	{ &t5546_TI, 46},
	{ &t5065_TI, 47},
	{ &t5066_TI, 54},
	{ &t5067_TI, 59},
	{ &t2182_TI, 60},
	{ &t2186_TI, 67},
	{ &t2183_TI, 72},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3745_0_0_0;
extern Il2CppType t3745_1_0_0;
struct t463;
extern TypeInfo t463_TI;
extern CustomAttributesCache t463__CustomAttributeCache;
extern CustomAttributesCache t463__CustomAttributeCache_m_Font;
extern CustomAttributesCache t463__CustomAttributeCache_m_box;
extern CustomAttributesCache t463__CustomAttributeCache_m_button;
extern CustomAttributesCache t463__CustomAttributeCache_m_toggle;
extern CustomAttributesCache t463__CustomAttributeCache_m_label;
extern CustomAttributesCache t463__CustomAttributeCache_m_textField;
extern CustomAttributesCache t463__CustomAttributeCache_m_textArea;
extern CustomAttributesCache t463__CustomAttributeCache_m_window;
extern CustomAttributesCache t463__CustomAttributeCache_m_horizontalSlider;
extern CustomAttributesCache t463__CustomAttributeCache_m_horizontalSliderThumb;
extern CustomAttributesCache t463__CustomAttributeCache_m_verticalSlider;
extern CustomAttributesCache t463__CustomAttributeCache_m_verticalSliderThumb;
extern CustomAttributesCache t463__CustomAttributeCache_m_horizontalScrollbar;
extern CustomAttributesCache t463__CustomAttributeCache_m_horizontalScrollbarThumb;
extern CustomAttributesCache t463__CustomAttributeCache_m_horizontalScrollbarLeftButton;
extern CustomAttributesCache t463__CustomAttributeCache_m_horizontalScrollbarRightButton;
extern CustomAttributesCache t463__CustomAttributeCache_m_verticalScrollbar;
extern CustomAttributesCache t463__CustomAttributeCache_m_verticalScrollbarThumb;
extern CustomAttributesCache t463__CustomAttributeCache_m_verticalScrollbarUpButton;
extern CustomAttributesCache t463__CustomAttributeCache_m_verticalScrollbarDownButton;
extern CustomAttributesCache t463__CustomAttributeCache_m_ScrollView;
extern CustomAttributesCache t463__CustomAttributeCache_m_CustomStyles;
extern CustomAttributesCache t463__CustomAttributeCache_m_Settings;
TypeInfo t3745_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "GUISkin[]", "UnityEngine", t3745_MIs, NULL, NULL, NULL, &t20_TI, NULL, NULL, &t463_TI, t3745_ITIs, t3745_VT, &EmptyCustomAttributesCache, &t3745_TI, &t3745_0_0_0, &t3745_1_0_0, t3745_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t20), sizeof (t463 *), -1, sizeof(t3745_SFs), 0, -1, 1057025, 1, false, false, false, false, false, true, false, false, false, false, false, false, 0, 0, 0, 0, 0, 73, 12, 16};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
