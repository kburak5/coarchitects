﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2261;
struct t29;
#include "t57.h"

 void m11380 (t2261 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11381 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m11382 (t2261 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m11383 (t2261 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2261 * m11384 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
