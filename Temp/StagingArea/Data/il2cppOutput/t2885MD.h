﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2885;
struct t29;
struct t20;
#include "t477.h"

 void m15754 (t2885 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15755 (t2885 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15756 (t2885 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15757 (t2885 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m15758 (t2885 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
