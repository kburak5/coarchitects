﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1042;
struct t1019;
struct t781;

 void m4830 (t1042 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1019 * m4831 (t1042 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4832 (t1042 * __this, t1019 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4833 (t1042 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4834 (t1042 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4835 (t1042 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4836 (t1042 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4837 (t1042 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
