﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t783;
struct t7;
struct t781;
#include "t934.h"

 void m5148 (t783 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t783 * m4046 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t783 * m8329 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8330 (t783 * __this, t934  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5153 (t783 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8331 (t783 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
