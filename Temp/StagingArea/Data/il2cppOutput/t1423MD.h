﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1423;
struct t7;
struct t29;
struct t1424;
struct t1425;

 void m7798 (t1423 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7799 (t1423 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7800 (t1423 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7801 (t1423 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7802 (t1423 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7803 (t1423 * __this, t1425 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
