﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1468;
struct t967;
struct t7;
struct t1466;
struct t42;

 void m7968 (t1468 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7969 (t1468 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7970 (t1468 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7971 (t1468 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7972 (t1468 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7973 (t1468 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
