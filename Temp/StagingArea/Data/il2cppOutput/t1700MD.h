﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1700;
struct t1700_marshaled;

void t1700_marshal(const t1700& unmarshaled, t1700_marshaled& marshaled);
void t1700_marshal_back(const t1700_marshaled& marshaled, t1700& unmarshaled);
void t1700_marshal_cleanup(t1700_marshaled& marshaled);
