﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t10;
struct t7;

 void m10 (t10 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11 (t10 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12 (t10 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13 (t10 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
