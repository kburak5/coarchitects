﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2514;
struct t29;
struct t403;
struct t20;
struct t136;
struct t201;
struct t2512;
#include "t184.h"

 void m13307 (t2514 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13308 (t2514 * __this, t184  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13309 (t2514 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13310 (t2514 * __this, int32_t p0, t184  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13311 (t2514 * __this, t184  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13312 (t2514 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t184  m13313 (t2514 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13314 (t2514 * __this, int32_t p0, t184  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13315 (t2514 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13316 (t2514 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m13317 (t2514 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m13318 (t2514 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13319 (t2514 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13320 (t2514 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m13321 (t2514 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13322 (t2514 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13323 (t2514 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13324 (t2514 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13325 (t2514 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m13326 (t2514 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13327 (t2514 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13328 (t2514 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m13329 (t2514 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13330 (t2514 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13331 (t2514 * __this, t184  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13332 (t2514 * __this, t201* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m13333 (t2514 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m13334 (t2514 * __this, t184  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m13335 (t2514 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t184  m13336 (t2514 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
