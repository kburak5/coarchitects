﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t903;
struct t903_marshaled;

void t903_marshal(const t903& unmarshaled, t903_marshaled& marshaled);
void t903_marshal_back(const t903_marshaled& marshaled, t903& unmarshaled);
void t903_marshal_cleanup(t903_marshaled& marshaled);
