﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t877;
struct t875;
struct t29;

 void m3758 (t877 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3759 (t877 * __this, t875 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t875 * m3760 (t877 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3761 (t877 * __this, int32_t p0, t875 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3762 (t877 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
