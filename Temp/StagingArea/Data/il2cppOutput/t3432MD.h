﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3432;
struct t29;
struct t20;
#include "t1484.h"

 void m19030 (t3432 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m19031 (t3432 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m19032 (t3432 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19033 (t3432 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m19034 (t3432 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
