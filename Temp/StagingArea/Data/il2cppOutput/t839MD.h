﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t839;
struct t7;
struct t840;
#include "t842.h"

 void m3579 (t839 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3580 (t839 * __this, t7* p0, int32_t p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3581 (t839 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3582 (t839 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
