﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1427;
struct t1429;
struct t316;

 void m7778 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7779 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7780 (t29 * __this, t29 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m7781 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
