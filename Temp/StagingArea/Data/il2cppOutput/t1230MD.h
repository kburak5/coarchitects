﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1230;
struct t781;

 t781* m6504 (t29 * __this, uint8_t* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6505 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6506 (t29 * __this, uint8_t* p0, t781* p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6507 (t29 * __this, uint8_t* p0, t781* p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6508 (t29 * __this, uint8_t* p0, t781* p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m6509 (t29 * __this, t781* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6510 (t29 * __this, t781* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m6511 (t29 * __this, t781* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m6512 (t29 * __this, t781* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
