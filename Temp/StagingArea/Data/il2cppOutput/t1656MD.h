﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1656;
struct t29;
struct t316;
struct t1657;
struct t42;
struct t490;
struct t1146;
struct t629;

 void m9312 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9313 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m9314 (t29 * __this, t29 * p0, t42 * p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m9315 (t29 * __this, t29 * p0, t42 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m9316 (t29 * __this, t29 * p0, t42 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t490 * m9317 (t29 * __this, t29 * p0, t42 * p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m9318 (t29 * __this, t29 * p0, t42 * p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m9319 (t29 * __this, t29 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9320 (t29 * __this, t29 * p0, t42 * p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9321 (t29 * __this, t29 * p0, t42 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1146 * m9322 (t29 * __this, t1146 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m9323 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t629 * m9324 (t29 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
