﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3394;
struct t29;
struct t20;
#include "t1353.h"

 void m18840 (t3394 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18841 (t3394 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18842 (t3394 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18843 (t3394 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18844 (t3394 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
