﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2915;
struct t29;
struct t485;

 void m15971 (t2915 * __this, t485 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15972 (t2915 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15973 (t2915 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15974 (t2915 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m15975 (t2915 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
