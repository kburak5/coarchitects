﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2920;
struct t29;
struct t20;
#include "t384.h"

 void m16011 (t2920 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m16012 (t2920 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16013 (t2920 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16014 (t2920 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16015 (t2920 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
