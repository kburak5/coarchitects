﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t415;
struct t29;
struct t2;
struct t66;
struct t67;
#include "t35.h"

 void m1997 (t415 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2421 (t415 * __this, t2 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m2422 (t415 * __this, t2 * p0, t67 * p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2423 (t415 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
