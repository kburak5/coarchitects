﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonTest : MonoBehaviour {

	// Use this for initialization
	public void Start () {
	
		GameObject [] bS = GameObject.FindGameObjectsWithTag ("ButtonTest");
		foreach (GameObject bG in bS) {

			Text t = bG.transform.FindChild ("Text").GetComponent<Text>();
			t.text = "not Clicked";
		
		
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnPointerDown(){

		Button b = EventSystem.current.currentSelectedGameObject.GetComponent<Button>();
		Text t = b.transform.FindChild ("Text").GetComponent<Text>();
		t.text = "Clicked";

	}

	public void OnPointerUp(){

		Button b = EventSystem.current.currentSelectedGameObject.GetComponent<Button>();
		Text t = b.transform.FindChild ("Text").GetComponent<Text>();
		t.text = "not Clicked";

	}
}
