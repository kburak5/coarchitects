﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoadScene_3 : MonoBehaviour {

	public Button selectButton;
	
	// Use this for initialization
	void Start () {
		
		selectButton.onClick.AddListener (delegate {
			
			this.LoadLevel ("scene_3");
			
		});
		
	}
	
	// Update is called once per frame
	
	void LoadLevel ( string s ){
		
		Application.LoadLevel (s);
		
		
		
	}
}
