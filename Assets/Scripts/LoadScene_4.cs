﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoadScene_4 : MonoBehaviour {
	
	public Button doneButton;
	
	// Use this for initialization
	void Start () {
		
		doneButton.onClick.AddListener (delegate {
			
			this.LoadLevel ("scene_4");
			
		});
		
	}
	
	// Update is called once per frame
	
	void LoadLevel ( string s ){
		
		Application.LoadLevel (s);
		
		
		
	}
}
