﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoadScene_0 : MonoBehaviour {
	
	public Button selectButton;
	
	// Use this for initialization
	void Awake () {
		
		
		selectButton.onClick.AddListener (delegate {
			this.LoadScene ("scene_0");
		});
	}
	
	
	
	void LoadScene ( string s){
		
		Application.LoadLevel (s);
		
	}
	
	
}
