﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoadScene_2 : MonoBehaviour {

	public Button polygonButton;

	// Use this for initialization
	void Start () {

		polygonButton.onClick.AddListener (delegate {
			
			this.LoadLevel ("scene_2");
			
		});
	
	}
	
	// Update is called once per frame

	void LoadLevel ( string s ){

		Application.LoadLevel (s);



		}
}
